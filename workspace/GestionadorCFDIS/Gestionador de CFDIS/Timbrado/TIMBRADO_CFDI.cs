﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;
using HyperSoft.ElectronicDocumentLibrary.Base;
using HyperSoft.ElectronicDocumentLibrary.Certificate;
using HyperSoft.ElectronicDocumentLibrary.ConstanciaRetenciones;
using HyperSoft.ElectronicDocumentLibrary.Document;
using HyperSoft.ElectronicDocumentLibrary.ECodex;
using HyperSoft.ElectronicDocumentLibrary.Manage;
using HyperSoft.Shared;
//using HyperSoft.Ejemplo.Utilerias;
using Data = Timbrado.DATA;
using Informacion = HyperSoft.ElectronicDocumentLibrary.ECodex.Timbre.Informacion;
namespace Timbrado
{
    public class TIMBRADO_CFDI
    {
        #region Constantes

        //private const string strDirectorioBase = "C:\\descarga_xml";

        private const bool UseOwnTokenDefinition = true;
        private const bool SetDateTimeServerDefinition = true;

        public static string Line = string.Empty.PadRight(100, '=');

        public static readonly string LicenseCfdiData = "C:\\descarga_xml\\3_ALG040428RG4\\Licencia\\2021_02_18_licencia.license";

        #endregion

        #region Vars

        //private TabPage[] pages;

        private string generationDirectory;
        private ElectronicManage manage;
        private ElectronicCertificate certificate;
        private ElectronicDocument electronicDocument;
        private ConstanciaRetenciones constanciaRetenciones;
        private Proveedor proveedor;

        private DateTime dateTimeServer = DateTime.MinValue;
        private DateTime dateLastToken = DateTime.Now;

        string _strRfc = string.Empty;
        int _cuenta_id = 0;
        string _strDirectorioBase = string.Empty;

        #endregion

        public TIMBRADO_CFDI(string strRfc, int cuenta_id, string strDirectorioBase)
        {
            _strRfc = strRfc;
            _cuenta_id = cuenta_id;
            _strDirectorioBase = strDirectorioBase;
        }


        public bool TimbrarCfdi(out string fileName, ref string resultadoTexto)
        {

            CreateObjects();
            generationDirectory = CrearDirectorioCFDIsTimbrados(_strRfc, _cuenta_id);

            _strRfc = _strRfc.Replace("-", "");

            fileName = Path.Combine(this.generationDirectory, "4546-8888.xml");

            TimbrarParameters parameters = new TimbrarParameters().Initialization();
            try
            {

                STVCFDI cfdi = new STVCFDI();

                string path = _strDirectorioBase + "\\json2.json";

                if (System.IO.File.Exists(path))
                {
                    using (StreamReader jsonStream = System.IO.File.OpenText(path))
                    {
                        var json = jsonStream.ReadToEnd();
                        cfdi = JsonConvert.DeserializeObject<STVCFDI>(json);
                    }
                }

                //En este método se cargan los datos de la factura.
                Data.CargarDatosCfdi33(this.electronicDocument, cfdi);

                // Se crea el objeto que contiene la información retornada por el PAC
                Informacion informacion = new Informacion().Initialization();

                // Se asigna los parámetros a usar durante el proceso
                parameters.ElectronicDocument = this.electronicDocument;
                parameters.Informacion = informacion;
                parameters.TestMode = true;
                parameters.Rfc.Value = _strRfc;//"FUNK671228PH6";
                parameters.IdTransaccion.Value = long.MaxValue;

                // Se envia a timbrar el documento
                ProcessProviderResult result = this.proveedor.GenerarTimbre(parameters);

                Chronometer.Instance.Stop();

                // En este caso se verifica que el proceso fue correcto
                if (result == ProcessProviderResult.Ok)
                {
                    //this.txtUuidDescarga.Text = informacion.Timbre.Uuid.Value;
                    //this.txtUuidEstado.Text = informacion.Timbre.Uuid.Value;
                }

                // Se le da formato a la información retornada por el PAC
                resultadoTexto = this.FormatInformationTimbre(parameters.IdTransaccion.Value, informacion, result, this.electronicDocument.FingerPrintPac);

                // Se muestra en pantalla el resultado del proceso
                //MessageBox.Show(resultadoTexto);

                if (result != ProcessProviderResult.Ok)
                    return false;

                // Se guarda el documento, en este punto ya contiene los datos del timbre
                SaveOptions options = this.electronicDocument.Manage.Save.Options;
                this.electronicDocument.Manage.Save.Options = SaveOptions.EncodeApostrophe;
                try
                {
                    this.electronicDocument.SaveToFile(fileName);
                }
                catch
                {
                    MessageBox.Show(this.electronicDocument.ErrorText);
                    return false;
                }
                finally
                {
                    this.electronicDocument.Manage.Save.Options = options;
                }
                return true;
            }
            finally
            {
                parameters.Dispose();
                Chronometer.Instance.Stop();
            }
        }

        private string FormatInformationTimbre(long idTransaccion, Informacion informacion, ProcessProviderResult providerResult, string cadenaOrignal)
        {
            StringBuilder text = new StringBuilder();

            if (informacion.Timbre.IsAssigned)
            {
                text.AppendLine(string.Empty);
                text.AppendLine("INFORMACION DEL TIMBRE");
                text.AppendLine("=================================================");
                text.AppendLine("Versión                    : " + informacion.Timbre.Version.AsString());
                text.AppendLine("UUID                       : " + informacion.Timbre.Uuid.AsString());
                text.AppendLine("Fecha de timbrado          : " + informacion.Timbre.FechaTimbrado.AsString());
                text.AppendLine("Sello del CFD              : " + informacion.Timbre.SelloCfd.AsString());
                text.AppendLine("No. de Certificado del SAT : " + informacion.Timbre.NumeroCertificadoSat.AsString());
                text.AppendLine("Sello del SAT              : " + informacion.Timbre.SelloSat.AsString());
                text.AppendLine("Leyenda                    : " + informacion.Timbre.Leyenda.AsString());
                text.AppendLine("RFC del PAC                : " + informacion.Timbre.RfcProveedorCertificacion.AsString());
                text.AppendLine();
                text.AppendLine("CADENA ORIGINAL DEL COMPLEMENTO");
                text.AppendLine("=================================================");
                text.AppendLine("Cadena original    : " + cadenaOrignal);
                text.AppendLine();
            }

            if (idTransaccion != 0)
            {
                text.AppendLine(string.Empty);
                text.AppendLine("INFORMACION DE LA TRANSACCION");
                text.AppendLine("=================================================");
                text.AppendLine("Número de transacción         : " + idTransaccion);
            }

            if (informacion.Error.IsAssigned == false)
                return text.ToString();

            this.FormatTypeError(text, providerResult);

            text.AppendLine("ERROR QUE SE GENERO EN EL PROCESO");
            text.AppendLine("=================================================");
            text.AppendLine("Número      : " + informacion.Error.Numero.Value);
            text.AppendLine("Descripción : " + informacion.Error.Descripcion.Value);

            return text.ToString();
        }

        private void FormatTypeError(StringBuilder text, ProcessProviderResult providerResult)
        {
            if (providerResult == ProcessProviderResult.Ok)
                return;

            text.AppendLine();
            text.AppendLine();
            text.AppendLine("=================================================");
            switch (providerResult)
            {
                case ProcessProviderResult.ErrorInDocument:
                    text.AppendLine("EL ERROR SE PRESENTO AL GENERAR LOS PARAMETROS,");
                    text.AppendLine("ANTES DE EJECUTAR LA OPERACION CON EL PAC.");
                    break;

                case ProcessProviderResult.ErrorWithProvider:
                    text.AppendLine("EL ERROR FUE GENERADO POR EL PAC.");
                    break;

                case ProcessProviderResult.ErrorInConnectionWithProvider:
                    text.AppendLine("EL ERROR SE PRESENTO AL CONTACTAR EL SERVIDOR DEL PAC.");
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
            text.AppendLine("=================================================");
            text.AppendLine();
            text.AppendLine();
        }
        private void CreateObjects()
        {
            // Se crea el objeto que representa al PAC y para este ejemplo lo he configurado 
            // con los parámetros de prueba de la conexión. Para mayor información acerca de 
            // lo relación con el PAC póngase en contacto con el mismo.
            this.proveedor = new Proveedor().Initialization();

            // Instanciamos la clase TManageElectronicDocument
            this.manage = new ElectronicManage().Initialization();

            //  . EDL, a partir de la versión 2017.06.02, por defecto realiza una validación LITE del CFDI 3.3 contra el schema
            //  . Para que se realice una validación FULL, debes activar las siguientes líneas
            //  . Si deseas conocer más al respecto:
            //       www.facturando.mx/blog/index.php/2017/06/01/edl-validacion-de-un-cfdi-3-3/
            //this.manage.Save.Options |= SaveOptions.ValidateWithSchema;
            //this.manage.Save.Options -= SaveOptions.ValidateWithSchemaLite;


            // Se cargan a memoria el archivo de autoridades certificadoras de prueba
            // *** ESTE LÍNEA DE CODIGO DEBE SER ELIMINADA EN UN AMBIENTE DE PRODUCCIÓN ***
            //this.manage.CertificateAuthorityList.UseForTest();

            //Se crea la clase que va a ser usada en el proceso de firmado, se le pasa el certificado,
            //la llave privada y el password de la misma.
            this.certificate = new ElectronicCertificate();
            this.certificate.Load(
              Path.Combine(_strDirectorioBase, "Archivos\\FUNK671228PH6.cer"),
              Path.Combine(_strDirectorioBase, "Archivos\\FUNK671228PH6.key"),
              "12345678a");

            // Asignamos el certificado al objeto Manage, esta ultima, es la encargada de contener
            // y administrar TODOS los recursos que serán usados en el proceso
            this.manage.Save.AssignCertificate(this.certificate);


            // Se instancia la clase que es la encarga de llevar a cabo el proceso de generación de la FACTURA ELECTRONICA (CFDI)
            // y se le pasa el objeto que tiene todos los recursos necesarios para llevar a cabo dicho proceso.
            this.electronicDocument = new ElectronicDocument().Initialization();
            this.electronicDocument.AssignManage(this.manage);


            // Se instancia la clase que es la encarga de llevar a cabo el proceso de generación de la CONSTANCIA DE RETENCIONES Y PAGOS
            // y se le pasa el objeto que tiene todos los recursos necesarios para llevar a cabo dicho proceso.
            this.constanciaRetenciones = new ConstanciaRetenciones().Initialization();
            this.constanciaRetenciones.AssignManage(this.manage);

            // Directorio donde van a ser almacenado los XML generados
            this.generationDirectory = CrearDirectorioCFDIsTimbrados(_strRfc, _cuenta_id);
        }
        private string CrearDirectorioCFDIsTimbrados(string strRfc, int cuenta_id)
        {
            strRfc = strRfc.Replace("-", "");

            //Se crea la carpeta para guardar CFDIs timbrados
            string CFDIS_timbrados = string.Format("{0}\\{1}\\{2}", _strDirectorioBase, cuenta_id + "_" + strRfc, "CFDIs_timbrados");
            if (!Directory.Exists(CFDIS_timbrados))
            {
                Directory.CreateDirectory(CFDIS_timbrados);
            }
            return CFDIS_timbrados;
        }

    }
}
