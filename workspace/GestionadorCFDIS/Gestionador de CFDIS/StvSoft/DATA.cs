﻿using System;
using HyperSoft.ElectronicDocumentLibrary.Base;
using HyperSoft.ElectronicDocumentLibrary.Complemento.RecepcionPago;
using HyperSoft.ElectronicDocumentLibrary.ConstanciaRetenciones;
using HyperSoft.ElectronicDocumentLibrary.Document;
using Impuesto = HyperSoft.ElectronicDocumentLibrary.Document.Impuesto;
using Traslado = HyperSoft.ElectronicDocumentLibrary.Document.Traslado;

namespace StvSoft
{
   public static class DATA
    {
        public static void CargarDatosCfdi33(ElectronicDocument electronicDocument)
        {
            electronicDocument.Data.Clear();

            // Datos del comprobante ***************************************************************
            electronicDocument.Data.Version.Value = "3.3";
            electronicDocument.Data.Serie.Value = "C";
            electronicDocument.Data.Folio.Value = "3000";
            electronicDocument.Data.Fecha.Value = DateTime.Now;
            electronicDocument.Data.FormaPago.Value = "01";
            electronicDocument.Data.CondicionesPago.Value = "Contado";
            electronicDocument.Data.SubTotal.Value = 7200.00;
            electronicDocument.Data.Descuento.Value = 360.00;
            electronicDocument.Data.Moneda.Value = "MXN";
            //electronicDocument.Data.TipoCambioMx.Value = 1.00;
            //electronicDocument.Data.TipoCambioMx.Decimals = 0;
            //electronicDocument.Data.TipoCambioMx.Dot = false;
            electronicDocument.Data.Total.Value = 7934.40;
            electronicDocument.Data.TipoComprobante.Value = "I";
            electronicDocument.Data.MetodoPago.Value = "PUE";
            electronicDocument.Data.LugarExpedicion.Value = "01000";
            //electronicDocument.Data.Confirmacion.Value = "ECVH1";
            // *************************************************************************************

            // Información de los comprobantes fiscales relacionados *******************************
            //electronicDocument.Data.CfdiRelacionados.TipoRelacion.Value = "01";
            //CfdiRelacionado cfdiRelacionado = electronicDocument.Data.CfdiRelacionados.Add();
            //cfdiRelacionado.Uuid.Value = "3BBDC347-3925-4792-B592-5151C773258B";
            // *************************************************************************************

            // Datos del emisor ********************************************************************
            electronicDocument.Data.Emisor.Rfc.Value = "FUNK671228PH6";
            electronicDocument.Data.Emisor.Nombre.Value = "KARLA FUENTE NOLASCO";
            electronicDocument.Data.Emisor.RegimenFiscal.Value = "605";
            // *************************************************************************************

            // Datos del Receptor ******************************************************************
            electronicDocument.Data.Receptor.Rfc.Value = "AAA010101AAA";
            electronicDocument.Data.Receptor.Nombre.Value = "Receptor de prueba";
            //electronicDocument.Data.Receptor.ResinciaFiscal.Value = "USA";
            //electronicDocument.Data.Receptor.NumeroRegistroIdTributario.Value = "121585958";
            electronicDocument.Data.Receptor.UsoCfdi.Value = "G01";
            // *************************************************************************************

            // Concepto  No 1 **********************************************************************
            Concepto concepto = electronicDocument.Data.Conceptos.Add();
            concepto.ClaveProductoServicio.Value = "01010101";
            //concepto.NumeroIdentificacion.Value = "UT421510";
            concepto.Cantidad.Value = 10;
            concepto.ClaveUnidad.Value = "H87";
            //concepto.Unidad.Value = "Pieza";
            concepto.Descripcion.Value = "DVD";
            concepto.ValorUnitario.Value = 120;
            concepto.Importe.Value = 1200;
            concepto.Descuento.Value = 360;

            //// Impuestos trasladados del concepto
            TrasladoConcepto trasladoConcepto = concepto.Impuestos.Traslados.Add();
            trasladoConcepto.Base.Value = 840;
            trasladoConcepto.Impuesto.Value = "002";
            trasladoConcepto.TipoFactor.Value = "Tasa";
            trasladoConcepto.TasaCuota.Value = 0.160000;
            trasladoConcepto.Importe.Value = 134.4;

            //// Impuestos retenidos del concepto
            /*RetencionConcepto retencionConcepto = concepto.Impuestos.Retenciones.Add();
            retencionConcepto.Base.Value = 0;
            retencionConcepto.Impuesto.Value = "001";
            retencionConcepto.TipoFactor.Value = "Tasa";
            retencionConcepto.TasaCuota.Value = 0.160000;          
            retencionConcepto.Importe.Value = 0; */

            //// Información aduanera del concepto
            /*Importacion importacion = concepto.InformacionAduanera.Add();
            importacion.Numero.Value = "10  47  3807  8003832"; */

            //// Cuenta predial del concepto
            //concepto.CuentaPredial.Numero.Value = "15956011002";

            //// Partes del concepto
            /*Partida partida = concepto.Partes.Add();
            partida.ClaveProductoServicio.Value = "01010101";
            partida.NumeroIdentificacion.Value = "7501030283645";
            partida.Cantidad.Value = 10;
            partida.Unidad.Value = "Piezas";
            partida.Descripcion.Value = "Descripción de la parte";
            partida.ValorUnitario.Value = 100;
            partida.Importe.Value = 1000; */

            ////// Información aduanera de la parte del concepto
            //Importacion importacion = partida.InformacionAduanera.Add();
            //importacion = partida.InformacionAduanera.Add();
            //importacion.Numero.Value = "10  47  3807  8003832";
            // *************************************************************************************

            // Concepto  No 2 **********************************************************************
            concepto = electronicDocument.Data.Conceptos.Add();
            concepto.ClaveProductoServicio.Value = "01010101";
            //concepto.NumeroIdentificacion.Value = "UT421510";
            concepto.Cantidad.Value = 1;
            concepto.ClaveUnidad.Value = "H87";
            //concepto.Unidad.Value = "Pieza";
            concepto.Descripcion.Value = "Computadora armada";
            concepto.ValorUnitario.Value = 3000;
            concepto.Importe.Value = 3000;
            //concepto.Descuento.Value = 1;

            //// Impuestos trasladados del concepto
            trasladoConcepto = concepto.Impuestos.Traslados.Add();
            trasladoConcepto.Base.Value = 3000;
            trasladoConcepto.Impuesto.Value = "002";
            trasladoConcepto.TipoFactor.Value = "Tasa";
            trasladoConcepto.TasaCuota.Value = 0.160000;
            trasladoConcepto.Importe.Value = 480;
            // *************************************************************************************

            // Concepto  No 3 **********************************************************************
            concepto = electronicDocument.Data.Conceptos.Add();
            concepto.ClaveProductoServicio.Value = "01010101";
            //concepto.NumeroIdentificacion.Value = "UT421510";
            concepto.Cantidad.Value = 1;
            concepto.ClaveUnidad.Value = "H87";
            //concepto.Unidad.Value = "Pieza";
            concepto.Descripcion.Value = "Monitor de 19 \" marca AOC";
            concepto.ValorUnitario.Value = 3000;
            concepto.Importe.Value = 3000;
            //concepto.Descuento.Value = 1;

            //// Impuestos trasladados del concepto
            trasladoConcepto = concepto.Impuestos.Traslados.Add();
            trasladoConcepto.Base.Value = 3000;
            trasladoConcepto.Impuesto.Value = "002";
            trasladoConcepto.TipoFactor.Value = "Tasa";
            trasladoConcepto.TasaCuota.Value = 0.160000;
            trasladoConcepto.Importe.Value = 480;
            // *************************************************************************************

            // Impuestos trasladados ***************************************************************
            Traslado traslado = electronicDocument.Data.Impuestos.Traslados.Add();
            traslado.Tipo.Value = "002";
            traslado.TipoFactor.Value = "Tasa";
            traslado.TasaCuota.Value = 0.160000;
            traslado.Importe.Value = 1094.4;

            electronicDocument.Data.Impuestos.TotalTraslados.Value = 1094.4;
            // *************************************************************************************

            // Impuestos retenidos *****************************************************************
            /*HyperSoft.ElectronicDocumentLibrary.Document.Impuesto retencion = electronicDocument.Data.Impuestos.Retenciones.Add();
            retencion.Tipo.Value = "001";
            retencion.Importe.Value = 0;

            electronicDocument.Data.Impuestos.TotalRetenciones.Value = 0; */
            // *************************************************************************************
        }
        public static void CargarDatosCfdi33(ElectronicDocument electronicDocument, STVCFDI cfdi)
        {
            electronicDocument.Data.Clear();

            // Datos del comprobante ***************************************************************
            electronicDocument.Data.Version.Value = cfdi.version;
            electronicDocument.Data.Serie.Value = cfdi.serie;
            electronicDocument.Data.Folio.Value = cfdi.folio;
            electronicDocument.Data.Fecha.Value = DateTime.Now; //cfdi.fecha;
            electronicDocument.Data.FormaPago.Value = cfdi.formapago;
            electronicDocument.Data.CondicionesPago.Value = "Credito";//cfdi.condicionesdepago;
            electronicDocument.Data.SubTotal.Value = cfdi.subtotal;
            electronicDocument.Data.Descuento.Value = cfdi.descuento;
            electronicDocument.Data.Moneda.Value = cfdi.moneda;
            electronicDocument.Data.Total.Value = cfdi.total;
            electronicDocument.Data.TipoComprobante.Value = cfdi.tipodecomprobante; 
            electronicDocument.Data.MetodoPago.Value = cfdi.metodopago;
            electronicDocument.Data.LugarExpedicion.Value = cfdi.lugarexpedicion;

            electronicDocument.Data.TipoCambioMx.Value = cfdi.tipocambio;
            electronicDocument.Data.FormaPago.Value = cfdi.formapago;

            // Datos del emisor ********************************************************************
            //cfdi.emisor.rfc = cfdi.emisor.rfc.Replace("-", "");
            //electronicDocument.Data.Emisor.Rfc.Value = cfdi.emisor.rfc;
            //electronicDocument.Data.Emisor.Nombre.Value = cfdi.emisor.nombre;
            //electronicDocument.Data.Emisor.RegimenFiscal.Value = cfdi.emisor.regimenfiscal;
            // *************************************************************************************
            // Datos del Receptor ******************************************************************
            //cfdi.receptor.rfc = cfdi.receptor.rfc.Replace("-", "");

            //electronicDocument.Data.Receptor.Rfc.Value = cfdi.receptor.rfc;
            //electronicDocument.Data.Receptor.Nombre.Value = cfdi.receptor.nombre;
            //electronicDocument.Data.Receptor.UsoCfdi.Value = cfdi.receptor.usocfdi;
            // *************************************************************************************

            // Datos del emisor ********************************************************************
            electronicDocument.Data.Emisor.Rfc.Value = "FUNK671228PH6";
            electronicDocument.Data.Emisor.Nombre.Value = "KARLA FUENTE NOLASCO";
            electronicDocument.Data.Emisor.RegimenFiscal.Value = "605";
            // *************************************************************************************
            // Datos del Receptor ******************************************************************
            electronicDocument.Data.Receptor.Rfc.Value = "AAA010101AAA";
            electronicDocument.Data.Receptor.Nombre.Value = "Receptor de prueba";
            //electronicDocument.Data.Receptor.ResinciaFiscal.Value = "USA";
            //electronicDocument.Data.Receptor.NumeroRegistroIdTributario.Value = "121585958";
            electronicDocument.Data.Receptor.UsoCfdi.Value = "G01";
            // *************************************************************************************

            #region concepto
            int cantidad = cfdi.conceptos.Count;

            for (int i = 0; i <= cantidad - 1; i++)
            {
                // Concepto **************************************************************************
                Concepto concepto = electronicDocument.Data.Conceptos.Add();

                concepto.ClaveProductoServicio.Value = cfdi.conceptos[i].claveprodserv;
                concepto.Cantidad.Value = cfdi.conceptos[i].cantidad;
                concepto.ClaveUnidad.Value = cfdi.conceptos[i].claveunidad;
                concepto.Descuento.Value = cfdi.conceptos[i].descuento;
                concepto.ValorUnitario.Value = cfdi.conceptos[i].valorunitario;
                concepto.NumeroIdentificacion.Value = cfdi.conceptos[i].noidentificacion;
                concepto.Descripcion.Value = cfdi.conceptos[i].descripcion;
                concepto.Unidad.Value = cfdi.conceptos[i].unidad;
                concepto.Importe.Value = cfdi.conceptos[i].importe;


                //// Impuestos trasladados del concepto
                TrasladoConcepto trasladoConcepto = concepto.Impuestos.Traslados.Add();
                trasladoConcepto.Base.Value = 330; //cfdi.Impuestos.trasladados[0].basei;
                trasladoConcepto.Impuesto.Value = cfdi.Impuestos.trasladados[0].impuesto;
                trasladoConcepto.TipoFactor.Value = cfdi.Impuestos.trasladados[0].tipofactor;
                trasladoConcepto.TasaCuota.Value = 0.160000; //cfdi.Impuestos.trasladados[0].tasaocuota;
                trasladoConcepto.Importe.Value =  cfdi.Impuestos.trasladados[0].importe;

  
                // Partes del concepto
                Partida partida = concepto.Partes.Add();
                partida.ClaveProductoServicio.Value = cfdi.conceptos[0].claveprodserv;
                partida.Cantidad.Value = cfdi.conceptos[0].cantidad;
                partida.ValorUnitario.Value = cfdi.conceptos[0].valorunitario;
                partida.NumeroIdentificacion.Value = cfdi.conceptos[0].noidentificacion;
                partida.Descripcion.Value = cfdi.conceptos[0].descripcion;
                partida.Unidad.Value = cfdi.conceptos[0].unidad;
                partida.Importe.Value = cfdi.conceptos[0].importe;

            }

            electronicDocument.Data.Impuestos.TotalTraslados.Value = cfdi.total;
            #endregion

        }

    }
}
