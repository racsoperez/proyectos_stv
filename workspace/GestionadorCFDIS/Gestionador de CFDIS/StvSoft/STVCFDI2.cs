﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StvSoft
{
   public class STVCFDI2
    {
        public long IdCFDI { get; set; }
        public string status { get; set; }

        //SECCION ATRIBUTOS COMPROBANTE CFDI 3.3
        public string Version { get; set; }             //REQUERIDO
        public string Serie { get; set; }               //OPCIONAL
        public string Folio { get; set; }               //OPCIONAL
        public DateTime Fecha { get; set; }             //REQUERIDO
        public string Sello { get; set; }               //REQUERIDO
        public string FormaPago { get; set; }           //CONDICIONAL
        public string FormaPagoDescripcion { get; set; } //PDF-FORMA DE PAGO
        public string NoCertificado { get; set; }       //REQUERIDO
        public string Certificado { get; set; }         //REQUERIDO
        public string CondicionesDePago { get; set; }   //CONDICIONAL
        public double SubTotal { get; set; }            //REQUERIDO
        public double Descuento { get; set; }           //CONDICIONAL
        public string Moneda { get; set; }              //REQUERIDO
        public double TipoCambio { get; set; }          //CONDICIONAL
        public double Total { get; set; }               //REQUERIDO
        public string TipoDeComprobante { get; set; }   //REQUERIDO
        public string TipoDeComprobanteDescripcion { get; set; } //PDF-TIPO DE COMPROBANTE
        public string MetodoPago { get; set; }          //CONDICIONAL
        public string MetodoPagoDescripcion { get; set; } //PDF-METODO DE PAGO
        public string LugarExpedicion { get; set; }     //REQUERIDO
        public string LugarExpedicionDescripcion { get; set; } //PDF-LUGAR DE EXPEDICION
        public string Confirmacion { get; set; }        //CONDICIONAL
        public string TipoRelacion { get; set; }        //CONDICIONAL
        public string TipoRelacionDescripcion { get; set; } //PDF-TIPO DE RELACION
        public string CfdiRelacionadoUUID { get; set; } //CONDICIONAL
        public string TipoDocumento { get; set; }       //PDF-IMPORTE CON LETRA       
        public string ImporteLetra { get; set; }        //PDF-IMPORTE CON LETRA       
        public string OrdenCompra { get; set; }         //PDF-ORDEN DE COMPRA
        public string FolioInternoPedido { get; set; }  //PDF-FOLIO INTERNO/PEDIDO
        public string AgenteVendedor { get; set; }      //PDF-AGENTE/VENDEDOR
        public string Email { get; set; }               //EMAIL
        public string Comentarios1 { get; set; }        //PDF-COMENTARIOS 1 (Comentarios)
        public string Comentarios2 { get; set; }        //PDF-COMENTARIOS 2 (Ubicación Medidor OAIP, Carta de Crédito MIN, Comentario Disponible)
        public string Comentarios3 { get; set; }        //PDF-COMENTARIOS 3 (Número Medidor OAIP, Comentario Disponible)
        public string Comentarios4 { get; set; }        //PDF-COMENTARIOS 4 (Comentario Disponible)
        public string Comentarios5 { get; set; }        //PDF-COMENTARIOS 5 (Comentario Disponible)
        public double MontoBaseProdIVA0 { get; set; }   //EXCLUSIVO PICONG-MONTO BASE PRODUCTOS CON IVA 0%
        public double MontoBaseProdIVA { get; set; }    //EXCLUSIVO PICONG-MONTO BASE PRODUCTOS CON IVA

        //SECCION NODO EMISOR CFDI 3.3
        public string emisorRfc { get; set; }           //REQUERIDO
        public string emisorNombreRazonSocial { get; set; } //OPCIONAL
        public string emisorRegimenFiscal { get; set; } //REQUERIDO
        public string emisorRegimenFiscalDescripcion { get; set; } //PDF-REGIMEN FISCAL
        public string emisorCalle { get; set; }         //PDF-EMISOR CALLE
        public string emisorNoExterior { get; set; }    //PDF-EMISOR NO EXTERIOR
        public string emisorNoInterior { get; set; }    //PDF-EMISOR NO INTERIOR
        public string emisorColonia { get; set; }       //PDF-EMISOR COLONIA
        public string emisorLocalidad { get; set; }     //PDF-EMISOR LOCALIDAD
        public string emisorMunicipio { get; set; }     //PDF-EMISOR MUNICIPIO
        public string emisorEstado { get; set; }        //PDF-EMISOR ESTADO
        public string emisorPais { get; set; }          //PDF-EMISOR PAIS
        public string emisorCP { get; set; }            //PDF-EMISOR CP

        //SECCION LUGAR EXPEDICION/DOMICILIO SUCURSAL CFDI 3.3
        public string emisorCalleLugarExp { get; set; }         //PDF-EMISOR CALLE LUGAR EXPEDICION
        public string emisorNoExteriorLugarExp { get; set; }    //PDF-EMISOR NO EXTERIOR LUGAR EXPEDICION
        public string emisorNoInteriorLugarExp { get; set; }    //PDF-EMISOR NO INTERIOR LUGAR EXPEDICION
        public string emisorColoniaLugarExp { get; set; }       //PDF-EMISOR COLONIA LUGAR EXPEDICION
        public string emisorLocalidadLugarExp { get; set; }     //PDF-EMISOR LOCALIDAD LUGAR EXPEDICION
        public string emisorMunicipioLugarExp { get; set; }     //PDF-EMISOR MUNICIPIO LUGAR EXPEDICION
        public string emisorEstadoLugarExp { get; set; }        //PDF-EMISOR ESTADO LUGAR EXPEDICION
        public string emisorPaisLugarExp { get; set; }          //PDF-EMISOR PAIS LUGAR EXPEDICION
        public string emisorCPLugarExp { get; set; }            //PDF-EMISOR CP LUGAR EXPEDICION

        //SECCION NODO RECEPTOR CFDI 3.3
        public string receptorRfc { get; set; }                 //REQUERIDO
        public string receptorNombreRazonSocial { get; set; }   //OPCIONAL
        public string receptorResidenciaFiscal { get; set; }    //CONDICIONAL
        public string receptorNumRegIdTrib { get; set; }        //CONDICIONAL
        public string receptorUsoCFDI { get; set; }             //REQUERIDO
        public string receptorUsoCFDIDescripcion { get; set; }  //PDF-USO CFDI
        public string receptorCalle { get; set; }               //PDF-RECEPTOR CALLE
        public string receptorNoExterior { get; set; }          //PDF-RECEPTOR NO EXTERIOR
        public string receptorNoInterior { get; set; }          //PDF-RECEPTOR NO INTERIOR
        public string receptorColonia { get; set; }             //PDF-RECEPTOR COLONIA
        public string receptorLocalidad { get; set; }           //PDF-RECEPTOR LOCALIDAD
        public string receptorMunicipio { get; set; }           //PDF-RECEPTOR MUNICIPIO
        public string receptorEstado { get; set; }              //PDF-RECEPTOR ESTADO
        public string receptorPais { get; set; }                //PDF-RECEPTOR PAIS
        public string receptorCP { get; set; }                  //PDF-RECEPTOR CP

        //SECCION LUGAR ENTREGA/EXPORTACION CFDI 3.3
        public string receptorCalleLugarEnt { get; set; }       //PDF-RECEPTOR CALLE LUGAR ENTREGA
        public string receptorNoExteriorLugarEnt { get; set; }  //PDF-RECEPTOR NO EXTERIOR LUGAR ENTREGA
        public string receptorNoInteriorLugarEnt { get; set; }  //PDF-RECEPTOR NO INTERIOR LUGAR ENTREGA
        public string receptorColoniaLugarEnt { get; set; }     //PDF-RECEPTOR COLONIA LUGAR ENTREGA
        public string receptorLocalidadLugarEnt { get; set; }   //PDF-RECEPTOR LOCALIDAD LUGAR ENTREGA
        public string receptorMunicipioLugarEnt { get; set; }   //PDF-RECEPTOR MUNICIPIO LUGAR ENTREGA
        public string receptorEstadoLugarEnt { get; set; }      //PDF-RECEPTOR ESTADO LUGAR ENTREGA
        public string receptorPaisLugarEnt { get; set; }        //PDF-RECEPTOR PAIS LUGAR ENTREGA
        public string receptorCPLugarEnt { get; set; }          //PDF-RECEPTOR CP LUGAR ENTREGA

        //SECCION CONCEPTOS CFDI 3.3
        public List<Concepto> Conceptos { get; set; }

        public class Concepto
        {
            public string ClaveProdServ { get; set; }   //REQUERIDO
            public string ClaveProdServDescripcion { get; set; } //PDF-CLAVE PRODUCTO SERVICIO
            public string NoIdentificacion { get; set; }//OPCIONAL
            public double Cantidad { get; set; }        //REQUERIDO
            public string ClaveUnidad { get; set; }     //REQUERIDO
            public string ClaveUnidadDescripcion { get; set; } //PDF-CLAVE UNIDAD
            public string Unidad { get; set; }          //OPCIONAL
            public string Descripcion { get; set; }     //REQUERIDO
            public double ValorUnitario { get; set; }   //REQUERIDO
            public double Importe { get; set; }         //REQUERIDO
            public double Descuento { get; set; }       //OPCIONAL

            public List<ConceptoImpuestoTraslado> ConceptoImpuestosTraslados { get; set; }    //CONDICIONAL
            public List<ConceptoImpuestoRetencion> ConceptoImpuestosRetenciones { get; set; } //CONDICIONAL

            public string InfoAduaneraNoPedimento { get; set; } //CONDICIONAL
            public string CuentaPredialNo { get; set; } //CONDICIONAL
            //public double IVAProducto { get; set; }   //PDF-IVA DEL PRODUCTO (PICONG)
            public string CantidadReal { get; set; }    //PDF-INFORMATIVO (PICONG)
            public string PesoBruto { get; set; }       //PDF-INFORMATIVO (PICONG)
        }

        public class ConceptoImpuestoTraslado
        {
            public double Base { get; set; }            //REQUERIDO
            public string Impuesto { get; set; }        //REQUERIDO
            public string ImpuestoDescripcion { get; set; } //PDF-CONCEPTO IMPUESTO
            public string TipoFactor { get; set; }      //REQUERIDO
            public double TasaOCuota { get; set; }      //OPCIONAL
            public double Importe { get; set; }         //CONDICIONAL
        }

        public class ConceptoImpuestoRetencion
        {
            public double Base { get; set; }            //REQUERIDO
            public string Impuesto { get; set; }        //REQUERIDO
            public string ImpuestoDescripcion { get; set; } //PDF-CONCEPTO IMPUESTO
            public string TipoFactor { get; set; }      //REQUERIDO
            public double TasaOCuota { get; set; }      //REQUERIDO
            public double Importe { get; set; }         //REQUERIDO
        }

        //SECCION NODO IMPUESTOS CFDI 3.3
        public bool aplicaImpuestos { get; set; }               //CONTROL DE PLANTILLA (Incluye o NO Impuestos)
        public double TotalImpuestosRetenidos { get; set; }     //CONDICIONAL
        public double TotalImpuestosTrasladados { get; set; }   //CONDICIONAL

        public List<Impuesto> Impuestos { get; set; }   //CONDICIONAL

        public class Impuesto
        {
            public string TipoImpuesto { get; set; }    //PDF-TIPO IMPUESTO
            public string ImpuestoClave { get; set; }   //REQUERIDO
            public double Importe { get; set; }         //REQUERIDO
            public string TipoFactor { get; set; }      //REQUERIDO EN TRASLADO
            public double TasaOCuota { get; set; }      //REQUERIDO EN TRASLADO
        }

        //SECCION NODO COMPLEMENTO IMPUESTOS LOCALES CFDI 3.3
        public bool aplicaImpuestosLocales { get; set; }        //CONTROL DE PLANTILLA (Incluye o NO Impuestos Locales)
        public string impLocVersion { get; set; }               //PDF-TIPO IMPUESTO
        public double impLocTotaldeRetenciones { get; set; }    //CONDICIONAL
        public double impLocTotaldeTraslados { get; set; }      //CONDICIONAL
        public string impLocTipoImpuesto { get; set; }          //REQUERIDO
        public string impLocRetenido { get; set; }              //REQUERIDO
        public double impLocTasadeRetencion { get; set; }       //REQUERIDO
        public double impLocImporteRetencion { get; set; }      //REQUERIDO
        public string impLocTrasladado { get; set; }            //REQUERIDO
        public double impLocTasadeTraslado { get; set; }        //REQUERIDO
        public double impLocImporteTraslado { get; set; }       //REQUERIDO

        //SECCION NODO COMPLEMENTO SERVICIOS PARCIALES DE CONSTRUCCION CFDI 3.3
        public bool aplicaServiciosParciales { get; set; }      //CONTROL DE PLANTILLA (Incluye o NO Servicios Parciales de Construccion)
        public string servParcVersion { get; set; }             //REQUERIDO
        public string servParcNumPerLicoAut { get; set; }       //REQUERIDO
        public string servParcCalle { get; set; }               //REQUERIDO
        public string servParcNoExterior { get; set; }          //OPCIONAL
        public string servParcNoInterior { get; set; }          //OPCIONAL
        public string servParcColonia { get; set; }             //OPCIONAL
        public string servParcLocalidad { get; set; }           //OPCIONAL
        public string servParcReferencia { get; set; }          //OPCIONAL
        public string servParcMunicipio { get; set; }           //REQUERIDO
        public string servParcEstado { get; set; }              //REQUERIDO
        public string servParcCP { get; set; }                  //REQUERIDO

        //SECCION NODO COMPLEMENTO COMERCIO EXTERIOR (CABECERA) CFDI 3.3
        public bool aplicaComercioExterior { get; set; }        //CONTROL DE PLANTILLA (Incluye o NO Comercio Exterior)
        public string comExtVersion { get; set; }               //REQUERIDO
        public string comExtMotivoTraslado { get; set; }        //CONDICIONAL
        public string comExtMotivoTrasladoDescripcion { get; set; } //PDF-MOTIVO TRASLADO
        public string comExtTipoOperacion { get; set; }         //REQUERIDO
        public string comExtClaveDePedimento { get; set; }      //CONDICIONAL
        public string comExtCertificadoOrigen { get; set; }     //CONDICIONAL
        public string comExtNumCertificadoOrigen { get; set; }  //CONDICIONAL
        public string comExtNumExportadorConfiable { get; set; } //CONDICIONAL
        public string comExtIncoterm { get; set; }              //CONDICIONAL
        public string comExtIncotermDescripcion { get; set; }   //PDF-MOTIVO TRASLADO
        public string comExtSubdivision { get; set; }           //CONDICIONAL
        public string comExtObservaciones { get; set; }         //OPCIONAL
        public string comExtTipoCambioUSD { get; set; }         //CONDICIONAL
        public double comExtTotalUSD { get; set; }              //CONDICIONAL

        //SECCION NODO COMPLEMENTO COMERCIO EXTERIOR (EMISOR) CFDI 3.3
        public string comExtEmisorCurp { get; set; }            //CONDICIONAL
        public string comExtEmisorDomCalle { get; set; }        //REQUERIDO
        public string comExtEmisorDomNoExterior { get; set; }   //OPCIONAL
        public string comExtEmisorDomNoInterior { get; set; }   //OPCIONAL
        public string comExtEmisorDomColonia { get; set; }      //OPCIONAL
        public string comExtEmisorDomLocalidad { get; set; }    //OPCIONAL
        public string comExtEmisorDomReferencia { get; set; }   //OPCIONAL
        public string comExtEmisorDomMunicipio { get; set; }    //OPCIONAL
        public string comExtEmisorDomEstado { get; set; }       //REQUERIDO
        public string comExtEmisorDomPais { get; set; }         //REQUERIDO
        public string comExtEmisorDomCP { get; set; }           //REQUERIDO

        //SECCION NODO COMPLEMENTO COMERCIO EXTERIOR (PROPIETARIO) CFDI 3.3
        public string comExtEsPropietario { get; set; }         //CONTROL DE NODO
        public string comExtPropNumRegIdTrib { get; set; }      //REQUERIDO
        public string comExtPropResidenciaFiscal { get; set; }  //REQUERIDO

        //SECCION NODO COMPLEMENTO COMERCIO EXTERIOR (RECEPTOR) CFDI 3.3
        public string comExtRecepNumRegIdTrib { get; set; }     //CONDICIONAL
        public string comExtRecepDomCalle { get; set; }         //REQUERIDO
        public string comExtRecepDomNoExterior { get; set; }    //OPCIONAL
        public string comExtRecepDomNoInterior { get; set; }    //OPCIONAL
        public string comExtRecepDomColonia { get; set; }       //OPCIONAL
        public string comExtRecepDomLocalidad { get; set; }     //OPCIONAL
        public string comExtRecepDomReferencia { get; set; }    //OPCIONAL
        public string comExtRecepDomMunicipio { get; set; }     //OPCIONAL
        public string comExtRecepDomEstado { get; set; }        //REQUERIDO
        public string comExtRecepDomPais { get; set; }          //REQUERIDO
        public string comExtRecepDomCP { get; set; }            //REQUERIDO

        //SECCION NODO COMPLEMENTO COMERCIO EXTERIOR (DESTINATARIO) CFDI 3.3
        public string comExtDestNumRegIdTrib { get; set; }      //OPCIONAL
        public string comExtDestNombre { get; set; }            //OPCIONAL
        public string comExtDestDomCalle { get; set; }          //REQUERIDO
        public string comExtDestDomNoExterior { get; set; }     //OPCIONAL
        public string comExtDestDomNoInterior { get; set; }     //OPCIONAL
        public string comExtDestDomColonia { get; set; }        //OPCIONAL
        public string comExtDestDomLocalidad { get; set; }      //OPCIONAL
        public string comExtDestDomReferencia { get; set; }     //OPCIONAL
        public string comExtDestDomMunicipio { get; set; }      //OPCIONAL
        public string comExtDestDomEstado { get; set; }         //REQUERIDO
        public string comExtDestDomPais { get; set; }           //REQUERIDO
        public string comExtDestDomCP { get; set; }             //REQUERIDO

        //SECCION NODO COMPLEMENTO COMERCIO EXTERIOR (DETALLES/MERCANCIAS) CFDI 3.3
        public List<Mercancia> Mercancias { get; set; }

        public class Mercancia
        {
            public string NoIdentificacion { get; set; }        //REQUERIDO
            public string FraccionArancelaria { get; set; }     //CONDICIONAL
            public double CantidadAduana { get; set; }          //OPCIONAL
            public string UnidadAduana { get; set; }            //CONDICIONAL
            public string UnidadAduanaDescripcion { get; set; } //PDF-UNIDAD ADUANA
            public double ValorUnitarioAduana { get; set; }     //CONDICIONAL
            public double ValorDolares { get; set; }            //REQUERIDO
            public string Marca { get; set; }                   //REQUERIDO
            public string Modelo { get; set; }                  //OPCIONAL
            public string SubModelo { get; set; }               //OPCIONAL
            public string NumeroSerie { get; set; }             //OPCIONAL
        }

        //SECCION NODO COMPLEMENTO RECEPCION DE PAGOS (CABECERA) CFDI 3.3
        public bool aplicaRecepcionPagos { get; set; }              //CONTROL DE PLANTILLA (Incluye o NO Recepcion Pagos)
        public string recPagosVersion { get; set; }                 //REQUERIDO
        public DateTime recPagosFechaPago { get; set; }             //REQUERIDO
        public string recPagosFormaPagoP { get; set; }              //REQUERIDO
        public string recPagosFormaPagoPDescripcion { get; set; }   //PDF-FORMA DE PAGOP
        public string recPagosMonedaP { get; set; }                 //REQUERIDO
        public double recPagosTipoCambioP { get; set; }             //CONDICIONAL
        public double recPagosMonto { get; set; }                   //REQUERIDO
        public string recPagosNumOperacion { get; set; }            //CONDICIONAL
        public string recPagosRfcEmisorCtaOrd { get; set; }         //CONDICIONAL
        public string recPagosNomBancoOrdExt { get; set; }          //CONDICIONAL
        public string recPagosCtaOrdenante { get; set; }            //CONDICIONAL
        public string recPagosRfcEmisorCtaBen { get; set; }         //CONDICIONAL
        public string recPagosCtaBeneficiario { get; set; }         //CONDICIONAL
        public string recPagosTipoCadPago { get; set; }             //CONDICIONAL
        public string recPagosTipoCadPagoDescripcion { get; set; }  //PDF-CADENA PAGO
        public string recPagosCertPago { get; set; }                //CONDICIONAL
        public string recPagosCadPago { get; set; }                 //CONDICIONAL
        public string recPagosSelloPago { get; set; }               //CONDICIONAL

        //SECCION NODO COMPLEMENTO RECEPCION DE PAGOS (DETALLES) CFDI 3.3
        public List<RPDocumento> RPDocumentos { get; set; }

        public class RPDocumento
        {
            public string recPagosIdDocumento { get; set; }             //REQUERIDO
            public string recPagosSerie { get; set; }                   //OPCIONAL
            public string recPagosFolio { get; set; }                   //OPCIONAL
            public string recPagosMonedaDR { get; set; }                //REQUERIDO
            public double recPagosTipoCambioDR { get; set; }            //CONDICIONAL
            public string recPagosMetodoPagoDR { get; set; }            //REQUERIDO
            public string recPagosMetodoPagoDRDescripcion { get; set; } //PDF-METODO PAGO
            public string recPagosNumParcialidad { get; set; }          //CONDICIONAL
            public double recPagosImpSaldoAnt { get; set; }             //CONDICIONAL
            public double recPagosImpPagado { get; set; }               //CONDICIONAL
            public double recPagosImpSaldoInsoluto { get; set; }        //CONDICIONAL
        }

        //SECCION NODO COMPLEMENTO RECEPCION DE PAGOS (IMPUESTOS CABECERA) CFDI 3.3
        public double recPagosTotalImpuestosRetenidos { get; set; } //CONDICIONAL
        public double recPagosTotalImpuestosTrasladados { get; set; }//CONDICIONAL

        //SECCION NODO COMPLEMENTO RECEPCION DE PAGOS (IMPUESTOS DETALLES) CFDI 3.3
        public List<RPImpuesto> RPImpuestos { get; set; }

        public class RPImpuesto
        {
            public string recPagosTipoImpuesto { get; set; }            //REQUERIDO
            public string recPagosImpuestoRetenido { get; set; }        //REQUERIDO
            public double recPagosImporteRetenido { get; set; }         //REQUERIDO
            public string recPagosImpuestoTrasladado { get; set; }      //REQUERIDO
            public string recPagosTipoFactorTrasladado { get; set; }    //REQUERIDO
            public double recPagosTasaOCuotaTrasladado { get; set; }    //REQUERIDO
            public double recPagosImporteTrasladado { get; set; }       //REQUERIDO
        }

        //SECCION NODO TIMBRE FISCAL DIGITAL CFDI 3.3
        public string UUID { get; set; }
        public DateTime FechaTimbrado { get; set; }
        public string SelloCFD { get; set; }
        public string SelloSAT { get; set; }
        public string NoCertificadoSAT { get; set; }
        public string erpTRXID { get; set; }
        public string erpNoTransaccion { get; set; }

        //SECCION ADDENDA CABECERA
        public string versionAddenda { get; set; }
        public string addendaCabDato1 { get; set; }
        public string addendaCabDato2 { get; set; }
        public string addendaCabDato3 { get; set; }
        public string addendaCabDato4 { get; set; }
        public string addendaCabDato5 { get; set; }
        public string addendaCabDato6 { get; set; }
        public string addendaCabDato7 { get; set; }
        public string addendaCabDato8 { get; set; }
        public string addendaCabDato9 { get; set; }
        public string addendaCabDato10 { get; set; }

        //SECCION ADDENDA DETALLES
        public List<AddendaDetalle> AddendaDetalles { get; set; }

        public class AddendaDetalle
        {
            public string addendaDetDato1 { get; set; }
            public string addendaDetDato2 { get; set; }
            public string addendaDetDato3 { get; set; }
        }

        public string CampoId { get { return "ID_CFDI"; } }
        public string CampoBusqueda { get { return "UUID"; } }

        public STVCFDI2()
        {
            IdCFDI = -1;
            status = "";

            //SECCION ATRIBUTOS COMPROBANTE CFDI 3.3
            Version = "";
            Serie = "";
            Folio = "";
            Fecha = DateTime.MinValue;
            Sello = "";
            FormaPago = "";
            FormaPagoDescripcion = "";
            NoCertificado = "";
            Certificado = "";
            CondicionesDePago = "";
            SubTotal = 0.00;
            Descuento = 0.00;
            Moneda = "";
            TipoCambio = 0.00;
            Total = 0.00;
            TipoDeComprobante = "";
            TipoDeComprobanteDescripcion = "";
            MetodoPago = "";
            MetodoPagoDescripcion = "";
            LugarExpedicion = "";
            LugarExpedicionDescripcion = "";
            Confirmacion = "";
            TipoRelacion = "";
            TipoRelacionDescripcion = "";
            CfdiRelacionadoUUID = "";
            TipoDocumento = "";
            ImporteLetra = "";
            OrdenCompra = "";
            FolioInternoPedido = "";
            AgenteVendedor = "";
            Email = "";
            Comentarios1 = "";
            Comentarios2 = "";
            Comentarios3 = "";
            Comentarios4 = "";
            Comentarios5 = "";
            MontoBaseProdIVA0 = 0.00;
            MontoBaseProdIVA = 0.00;

            //SECCION NODO EMISOR CFDI 3.3
            emisorRfc = "";
            emisorNombreRazonSocial = "";
            emisorRegimenFiscal = "";
            emisorRegimenFiscalDescripcion = "";
            emisorCalle = "";
            emisorNoExterior = "";
            emisorNoInterior = "";
            emisorColonia = "";
            emisorLocalidad = "";
            emisorMunicipio = "";
            emisorEstado = "";
            emisorPais = "";
            emisorCP = "";

            //SECCION LUGAR EXPEDICION/DOMICILIO SUCURSAL CFDI 3.3
            emisorCalleLugarExp = "";
            emisorNoExteriorLugarExp = "";
            emisorNoInteriorLugarExp = "";
            emisorColoniaLugarExp = "";
            emisorLocalidadLugarExp = "";
            emisorMunicipioLugarExp = "";
            emisorEstadoLugarExp = "";
            emisorPaisLugarExp = "";
            emisorCPLugarExp = "";

            //SECCION NODO RECEPTOR CFDI 3.3
            receptorRfc = "";
            receptorNombreRazonSocial = "";
            receptorResidenciaFiscal = "";
            receptorNumRegIdTrib = "";
            receptorUsoCFDI = "";
            receptorUsoCFDIDescripcion = "";
            receptorCalle = "";
            receptorNoExterior = "";
            receptorNoInterior = "";
            receptorColonia = "";
            receptorLocalidad = "";
            receptorMunicipio = "";
            receptorEstado = "";
            receptorPais = "";
            receptorCP = "";

            //SECCION LUGAR ENTREGA/EXPORTACION CFDI 3.3
            receptorCalleLugarEnt = "";
            receptorNoExteriorLugarEnt = "";
            receptorNoInteriorLugarEnt = "";
            receptorColoniaLugarEnt = "";
            receptorLocalidadLugarEnt = "";
            receptorMunicipioLugarEnt = "";
            receptorEstadoLugarEnt = "";
            receptorPaisLugarEnt = "";
            receptorCPLugarEnt = "";

            //SECCION CONCEPTOS CFDI 3.3
            Conceptos = new List<Concepto>();

            //SECCION NODO IMPUESTOS CFDI 3.3
            aplicaImpuestos = false;
            TotalImpuestosRetenidos = 0.00;
            TotalImpuestosTrasladados = 0.00;
            Impuestos = new List<Impuesto>();

            //SECCION NODO COMPLEMENTO IMPUESTOS LOCALES CFDI 3.3
            aplicaImpuestosLocales = false;
            impLocVersion = "";
            impLocTotaldeRetenciones = 0.00;
            impLocTotaldeTraslados = 0.00;
            impLocTipoImpuesto = "";
            impLocRetenido = "";
            impLocTasadeRetencion = 0.00;
            impLocImporteRetencion = 0.00;
            impLocTrasladado = "";
            impLocTasadeTraslado = 0.00;
            impLocImporteTraslado = 0.00;

            //SECCION NODO COMPLEMENTO SERVICIOS PARCIALES DE CONSTRUCCION CFDI 3.3
            aplicaServiciosParciales = false;
            servParcVersion = "";
            servParcNumPerLicoAut = "";
            servParcCalle = "";
            servParcNoExterior = "";
            servParcNoInterior = "";
            servParcColonia = "";
            servParcLocalidad = "";
            servParcReferencia = "";
            servParcMunicipio = "";
            servParcEstado = "";
            servParcCP = "";

            //SECCION NODO COMPLEMENTO COMERCIO EXTERIOR (CABECERA) CFDI 3.3
            aplicaComercioExterior = false;
            comExtVersion = "";
            comExtMotivoTraslado = "";
            comExtMotivoTrasladoDescripcion = "";
            comExtTipoOperacion = "";
            comExtClaveDePedimento = "";
            comExtCertificadoOrigen = "";
            comExtNumCertificadoOrigen = "";
            comExtNumExportadorConfiable = "";
            comExtIncoterm = "";
            comExtIncotermDescripcion = "";
            comExtSubdivision = "";
            comExtObservaciones = "";
            comExtTipoCambioUSD = "";
            comExtTotalUSD = 0.00;

            //SECCION NODO COMPLEMENTO COMERCIO EXTERIOR (EMISOR) CFDI 3.3
            comExtEmisorCurp = "";
            comExtEmisorDomCalle = "";
            comExtEmisorDomNoExterior = "";
            comExtEmisorDomNoInterior = "";
            comExtEmisorDomColonia = "";
            comExtEmisorDomLocalidad = "";
            comExtEmisorDomReferencia = "";
            comExtEmisorDomMunicipio = "";
            comExtEmisorDomEstado = "";
            comExtEmisorDomPais = "";
            comExtEmisorDomCP = "";

            //SECCION NODO COMPLEMENTO COMERCIO EXTERIOR (PROPIETARIO) CFDI 3.3
            comExtEsPropietario = "";
            comExtPropNumRegIdTrib = "";
            comExtPropResidenciaFiscal = "";

            //SECCION NODO COMPLEMENTO COMERCIO EXTERIOR (RECEPTOR) CFDI 3.3
            comExtRecepNumRegIdTrib = "";
            comExtRecepDomCalle = "";
            comExtRecepDomNoExterior = "";
            comExtRecepDomNoInterior = "";
            comExtRecepDomColonia = "";
            comExtRecepDomLocalidad = "";
            comExtRecepDomReferencia = "";
            comExtRecepDomMunicipio = "";
            comExtRecepDomEstado = "";
            comExtRecepDomPais = "";
            comExtRecepDomCP = "";

            //SECCION NODO COMPLEMENTO COMERCIO EXTERIOR (DESTINATARIO) CFDI 3.3
            comExtDestNumRegIdTrib = "";
            comExtDestNombre = "";
            comExtDestDomCalle = "";
            comExtDestDomNoExterior = "";
            comExtDestDomNoInterior = "";
            comExtDestDomColonia = "";
            comExtDestDomLocalidad = "";
            comExtDestDomReferencia = "";
            comExtDestDomMunicipio = "";
            comExtDestDomEstado = "";
            comExtDestDomPais = "";
            comExtDestDomCP = "";

            //SECCION NODO COMPLEMENTO COMERCIO EXTERIOR (DETALLES/MERCANCIAS) CFDI 3.3
            Mercancias = new List<Mercancia>();

            //SECCION NODO COMPLEMENTO RECEPCION DE PAGOS (CABECERA) CFDI 3.3
            aplicaRecepcionPagos = false;
            recPagosVersion = "";
            recPagosFechaPago = DateTime.MinValue;
            recPagosFormaPagoP = "";
            recPagosFormaPagoPDescripcion = "";
            recPagosMonedaP = "";
            recPagosTipoCambioP = 0.00;
            recPagosMonto = 0.00;
            recPagosNumOperacion = "";
            recPagosRfcEmisorCtaOrd = "";
            recPagosNomBancoOrdExt = "";
            recPagosCtaOrdenante = "";
            recPagosRfcEmisorCtaBen = "";
            recPagosCtaBeneficiario = "";
            recPagosTipoCadPago = "";
            recPagosTipoCadPagoDescripcion = "";
            recPagosCertPago = "";
            recPagosCadPago = "";
            recPagosSelloPago = "";

            //SECCION NODO COMPLEMENTO RECEPCION DE PAGOS (DETALLES) CFDI 3.3
            RPDocumentos = new List<RPDocumento>();

            //SECCION NODO COMPLEMENTO RECEPCION DE PAGOS (IMPUESTOS CABECERA) CFDI 3.3
            recPagosTotalImpuestosRetenidos = 0.00;
            recPagosTotalImpuestosTrasladados = 0.00;

            //SECCION NODO COMPLEMENTO RECEPCION DE PAGOS (IMPUESTOS DETALLES) CFDI 3.3
            RPImpuestos = new List<RPImpuesto>();

            //SECCION NODO TIMBRE FISCAL DIGITAL CFDI 3.3
            UUID = "";
            FechaTimbrado = DateTime.MinValue;
            SelloCFD = "";
            SelloSAT = "";
            NoCertificadoSAT = "";
            erpTRXID = "";
            erpNoTransaccion = "";

            //SECCION ADDENDA CABECERA
            versionAddenda = "";
            addendaCabDato1 = "";
            addendaCabDato2 = "";
            addendaCabDato3 = "";
            addendaCabDato4 = "";
            addendaCabDato5 = "";
            addendaCabDato6 = "";
            addendaCabDato7 = "";
            addendaCabDato8 = "";
            addendaCabDato9 = "";
            addendaCabDato10 = "";

            //SECCION ADDENDA DETALLES
            AddendaDetalles = new List<AddendaDetalle>();
        }
    }
}
