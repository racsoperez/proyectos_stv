﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace StvSoft
{
   public class UTIL
    {
       public static STVCFDI CargarXMLFactura(string RutaArchivo)
       {
           STVCFDI CFDI = null;
           try
           {
               XDocument doc = XDocument.Load(RutaArchivo);
               XNamespace cfdi = doc.Root.Name.Namespace;
               CFDI = new STVCFDI();

               XElement comprobante = doc.Element(cfdi + "Comprobante");
               if (comprobante != null)
               {
                   XAttribute attComprobante = comprobante.Attribute("Version");
                   CFDI.version = (attComprobante != null) ? attComprobante.Value.ToString() : "";

                   attComprobante = comprobante.Attribute("Serie");
                   CFDI.serie = (attComprobante != null) ? attComprobante.Value.ToString() : "";

                   attComprobante = comprobante.Attribute("Folio");
                   CFDI.folio = (attComprobante != null) ? attComprobante.Value.ToString() : "";

                   attComprobante = comprobante.Attribute("Fecha");
                   if (attComprobante == null)
                       CFDI.fecha = CFDI.fecha;
                   else
                       CFDI.fecha = Convert.ToDateTime(attComprobante.Value);

                   //attComprobante = comprobante.Attribute("Sello");
                   //CFDI.sello = (attComprobante != null) ? attComprobante.Value.ToString() : "";

                   attComprobante = comprobante.Attribute("FormaPago");
                   CFDI.formapago = (attComprobante != null) ? attComprobante.Value.ToString() : "";

                   //attComprobante = comprobante.Attribute("NoCertificado");
                   //CFDI.nocertificado = (attComprobante != null) ? attComprobante.Value.ToString() : "";

                   //attComprobante = comprobante.Attribute("Certificado");
                   //CFDI.certificado = (attComprobante != null) ? attComprobante.Value.ToString() : "";

                   attComprobante = comprobante.Attribute("CondicionesDePago");
                   CFDI.condicionesdepago = (attComprobante != null) ? attComprobante.Value.ToString() : "";

                   attComprobante = comprobante.Attribute("SubTotal");
                   CFDI.subtotal = (attComprobante != null) ? Convert.ToDouble(attComprobante.Value) : 0.000000;

                   attComprobante = comprobante.Attribute("Descuento");
                   CFDI.descuento = (attComprobante != null) ? Convert.ToDouble(attComprobante.Value) : 0.000000;

                   attComprobante = comprobante.Attribute("TipoCambio");
                   CFDI.tipocambio = (attComprobante != null) ? Convert.ToDouble(attComprobante.Value) : 0.000000;

                   attComprobante = comprobante.Attribute("Moneda");
                   CFDI.moneda = (attComprobante != null) ? attComprobante.Value.ToString() : "";

                   attComprobante = comprobante.Attribute("Total");
                   CFDI.total = (attComprobante != null) ? Convert.ToDouble(attComprobante.Value) : 0.000000;

                   attComprobante = comprobante.Attribute("TipoDeComprobante");
                   CFDI.tipodecomprobante = (attComprobante != null) ? attComprobante.Value.ToString() : "";

                   attComprobante = comprobante.Attribute("MetodoPago");
                   CFDI.metodopago = (attComprobante != null) ? attComprobante.Value.ToString() : "";

                   attComprobante = comprobante.Attribute("LugarExpedicion");
                   CFDI.lugarexpedicion = (attComprobante != null) ? attComprobante.Value.ToString() : "";

                   // documentos relacionados para notas de credito       
                   CFDI.cfdirelacionados = new STVCFDI.CfdiRelacionados();
                   XElement CfdiRelacionados = comprobante.Element(cfdi + "CfdiRelacionados");
                   if (CfdiRelacionados != null)
                   {
                       XAttribute attCfdiRelacionados = CfdiRelacionados.Attribute("TipoRelacion");
                       CFDI.cfdirelacionados.tiporelacion = (attCfdiRelacionados != null) ? attCfdiRelacionados.Value.ToString() : "";


                       List<STVCFDI.CfdiRelacionado> lstCfdiRelacionado = new List<STVCFDI.CfdiRelacionado>();
                       //XElement CfdiRelacionado = CfdiRelacionados.Element(cfdi + "CfdiRelacionado");
                       foreach (XElement cfdirelacionado in ((XElement)CfdiRelacionados).Nodes())
                       {
                           STVCFDI.CfdiRelacionado cfdiRelacionado = new STVCFDI.CfdiRelacionado();

                           XAttribute att = cfdirelacionado.Attribute("UUID");
                           cfdiRelacionado.uuid = (att != null) ? (att.Value) : "";

                           lstCfdiRelacionado.Add(cfdiRelacionado);
                       }

                       if (lstCfdiRelacionado.Count > 0)
                       {
                           CFDI.cfdirelacionados.relacionado = new List<STVCFDI.CfdiRelacionado>();
                           CFDI.cfdirelacionados.relacionado = lstCfdiRelacionado;
                       }

                   }

                   XElement emisor = comprobante.Element(cfdi + "Emisor");
                   if (emisor != null)
                   {
                       CFDI.emisor = new STVCFDI.Emisor();

                       XAttribute attEmisor = emisor.Attribute("Rfc");
                       CFDI.emisor.rfc = (attEmisor != null) ? attEmisor.Value.ToString() : "";

                       attEmisor = emisor.Attribute("Nombre");
                       CFDI.emisor.nombre = (attEmisor != null) ? attEmisor.Value.ToString() : "";

                       attEmisor = emisor.Attribute("RegimenFiscal");
                       CFDI.emisor.regimenfiscal = (attEmisor != null) ? attEmisor.Value.ToString() : "";

                       attEmisor = emisor.Attribute("codigoPostal");
                       CFDI.emisor.cp = (attEmisor != null) ? attEmisor.Value.ToString() : "";
                   }

                   XElement receptor = comprobante.Element(cfdi + "Receptor");
                   if (receptor != null)
                   {
                       CFDI.receptor = new STVCFDI.Receptor();
                       XAttribute attReceptor = receptor.Attribute("Rfc");
                       CFDI.receptor.rfc = (attReceptor != null) ? attReceptor.Value.ToString() : "";

                       attReceptor = receptor.Attribute("Nombre");
                       CFDI.receptor.nombre = (attReceptor != null) ? attReceptor.Value.ToString() : "";

                       attReceptor = receptor.Attribute("UsoCFDI");
                       CFDI.receptor.usocfdi = (attReceptor != null) ? attReceptor.Value.ToString() : "";
                   }

                   XElement conceptos = comprobante.Element(cfdi + "Conceptos");
                   if (conceptos != null)
                   {
                       foreach (XElement concepto in ((XElement)conceptos).Nodes())
                       {
                           STVCFDI.concepto concepto_cfdi = new STVCFDI.concepto();

                           XAttribute att = concepto.Attribute("Cantidad");
                           concepto_cfdi.cantidad = (att != null) ? Convert.ToDouble(att.Value) : 0.000000;

                           att = concepto.Attribute("ClaveProdServ");
                           concepto_cfdi.claveprodserv = (att != null) ? (att.Value) : "";

                           att = concepto.Attribute("ClaveUnidad");
                           concepto_cfdi.claveunidad = (att != null) ? (att.Value) : "";

                           att = concepto.Attribute("Descripcion");
                           concepto_cfdi.descripcion = (att != null) ? (att.Value) : "";

                           att = concepto.Attribute("Descuento");
                           concepto_cfdi.descuento = (att != null) ? Convert.ToDouble(att.Value) : 0.000000;

                           att = concepto.Attribute("Importe");
                           concepto_cfdi.importe = (att != null) ? Convert.ToDouble(att.Value) : 0.000000;

                           att = concepto.Attribute("NoIdentificacion");
                           concepto_cfdi.noidentificacion = (att != null) ? (att.Value) : "";

                           att = concepto.Attribute("Unidad");
                           concepto_cfdi.unidad = (att != null) ? (att.Value) : "";

                           att = concepto.Attribute("ValorUnitario");
                           concepto_cfdi.valorunitario = (att != null) ? Convert.ToDouble(att.Value) : 0.000000;

                           // obtengo los impuestos retenidos y trasladados por concepto
                           XElement impuestos = concepto.Element(cfdi + "Impuestos");
                           if (impuestos != null)
                           {
                               XElement trasladados = impuestos.Element(cfdi + "Traslados");
                               if (trasladados != null)
                               {
                                   List<STVCFDI.conceptoimpuestotraslado> listImpuestosTrasladados = new List<STVCFDI.conceptoimpuestotraslado>();
                                   foreach (XElement trasladado in ((XElement)trasladados).Nodes())
                                   {
                                       STVCFDI.conceptoimpuestotraslado impuestoTrasladado = new STVCFDI.conceptoimpuestotraslado();

                                       att = trasladado.Attribute("Base");
                                       impuestoTrasladado.basei = (att != null) ? Convert.ToDouble(att.Value) : 0.000000;

                                       att = trasladado.Attribute("Impuesto");
                                       impuestoTrasladado.impuesto = (att != null) ? (att.Value) : "";

                                       att = trasladado.Attribute("TipoFactor");
                                       impuestoTrasladado.tipofactor = (att != null) ? (att.Value) : "";

                                       att = trasladado.Attribute("TasaOCuota");
                                       impuestoTrasladado.tasaocuota = (att != null) ? Convert.ToDouble(att.Value) : 0.000000;

                                       att = trasladado.Attribute("Importe");
                                       impuestoTrasladado.importe = (att != null) ? Convert.ToDouble(att.Value) : 0.000000;

                                       listImpuestosTrasladados.Add(impuestoTrasladado);
                                   }

                                   concepto_cfdi.conceptoimpuestostraslados = listImpuestosTrasladados;
                               }

                               XElement retenciones = impuestos.Element(cfdi + "Retenciones");
                               if (retenciones != null)
                               {
                                   List<STVCFDI.conceptoimpuestoretencion> listImpuestosRetencion = new List<STVCFDI.conceptoimpuestoretencion>();
                                   foreach (XElement retencion in ((XElement)retenciones).Nodes())
                                   {
                                       STVCFDI.conceptoimpuestoretencion impuestoRetencion = new STVCFDI.conceptoimpuestoretencion();

                                       att = retencion.Attribute("Base");
                                       impuestoRetencion.basei = (att != null) ? Convert.ToDouble(att.Value) : 0.000000;

                                       att = retencion.Attribute("Impuesto");
                                       impuestoRetencion.impuesto = (att != null) ? (att.Value) : "";

                                       att = retencion.Attribute("TipoFactor");
                                       impuestoRetencion.tipofactor = (att != null) ? (att.Value) : "";

                                       att = retencion.Attribute("TasaOCuota");
                                       impuestoRetencion.tasaocuota = (att != null) ? Convert.ToDouble(att.Value) : 0.000000;

                                       att = retencion.Attribute("Importe");
                                       impuestoRetencion.importe = (att != null) ? Convert.ToDouble(att.Value) : 0.000000;

                                       listImpuestosRetencion.Add(impuestoRetencion);
                                   }

                                   concepto_cfdi.conceptoimpuestosretenciones = listImpuestosRetencion;
                               }
                           }

                           if (concepto != null)
                           {
                               if (CFDI.conceptos == null) CFDI.conceptos = new List<STVCFDI.concepto>();

                               CFDI.conceptos.Add(concepto_cfdi);
                           }

                       }
                   }

                   XElement impuestosTrasladados = comprobante.Element(cfdi + "Impuestos");
                   if (impuestosTrasladados != null)
                   {
                       XAttribute att = impuestosTrasladados.Attribute("TotalImpuestosTrasladados");
                       CFDI.totalimpuestostrasladados = (att != null) ? Convert.ToDouble(att.Value) : 0.000000;

                       att = impuestosTrasladados.Attribute("totalImpuestosRetenidos");
                       CFDI.totalimpuestosretenidos = (att != null) ? Convert.ToDouble(att.Value) : 0.000000;
                   }

                   XElement complemento = comprobante.Element(cfdi + "Complemento");

                   if (complemento != null)
                   {
                       if (complemento.FirstNode != null)
                       {
                           XNamespace tfd = "";
                           XElement timbre = complemento.Element((tfd ?? "") + "TimbreFiscalDigital");

                           //Se recorren los nodos dentro del Elemento "complemento" para buscar el elemento "tfd"
                           foreach (XNode node in ((XElement)complemento).Nodes())
                           {
                               XNamespace Elemento = ((System.Xml.Linq.XElement)node).Name.Namespace;
                               if (Elemento.NamespaceName.ToString() == "http://www.sat.gob.mx/TimbreFiscalDigital")
                               {
                                   tfd = Elemento;
                                   timbre = complemento.Element((tfd ?? "") + "TimbreFiscalDigital");
                               }

                               if (timbre != null)
                               {
                                   CFDI.timbrefiscaldigital = new STVCFDI.TimbreFiscalDigital();

                                   XAttribute attTimbre = timbre.Attribute("UUID");
                                   CFDI.timbrefiscaldigital.uuid = (attTimbre != null) ? attTimbre.Value.ToString() : "";

                                   attTimbre = timbre.Attribute("FechaTimbrado");
                                   if (attTimbre == null)
                                       CFDI.timbrefiscaldigital.fechatimbrado = CFDI.timbrefiscaldigital.fechatimbrado;
                                   else
                                       CFDI.timbrefiscaldigital.fechatimbrado = Convert.ToDateTime(attTimbre.Value);

                                   //attTimbre = timbre.Attribute("SelloCFD");
                                   //CFDI.timbrefiscaldigital.sellocfd = (attTimbre != null) ? attTimbre.Value.ToString() : "";

                                   //attTimbre = timbre.Attribute("SelloSAT");
                                   //CFDI.timbrefiscaldigital.sellosat = (attTimbre != null) ? attTimbre.Value.ToString() : "";

                                   //attTimbre = timbre.Attribute("NoCertificadoSAT");
                                   //CFDI.timbrefiscaldigital.nocertificadosat = (attTimbre != null) ? attTimbre.Value.ToString() : "";
                               }

                               // Pagos
                               if (Elemento.NamespaceName.ToString() == "http://www.sat.gob.mx/Pagos")
                               {
                                   tfd = Elemento;
                                   timbre = complemento.Element((tfd ?? "") + "TimbreFiscalDigital");

                                   XAttribute att = complemento.Attribute("ImpPagado");
                                   XElement pagos = complemento.Element((tfd ?? "") + "Pagos");
                                   if (pagos != null)
                                   {

                                       //List<STVCFDI.RPDocumento> listRPDocumento = new List<STVCFDI.RPDocumento>();
                                       foreach (XElement pago in ((XElement)pagos).Nodes())
                                       {
                                           CFDI.pago = new STVCFDI.Pago();

                                           att = pago.Attribute("FechaPago");
                                           if (att == null)
                                               CFDI.pago.fechapago = CFDI.pago.fechapago;
                                           else
                                               CFDI.pago.fechapago = Convert.ToDateTime(att.Value);

                                           att = pago.Attribute("FormaDePagoP");
                                           CFDI.pago.formapagop = (att != null) ? (att.Value) : "";

                                           att = pago.Attribute("MonedaP");
                                           CFDI.pago.monedap = (att != null) ? (att.Value) : "";

                                           att = pago.Attribute("TipoCambioP");
                                           CFDI.pago.tipocambiop = (att != null) ? Convert.ToDouble(att.Value) : 0.000000;

                                           att = pago.Attribute("Monto");
                                           CFDI.pago.monto = (att != null) ? Convert.ToDouble(att.Value) : 0.000000;

                                           XElement doctoRelacionados = pagos.Element((tfd ?? "") + "Pago");
                                           if (doctoRelacionados != null)
                                           {
                                               List<STVCFDI.DoctoRelacionado> listRPDocumento = new List<STVCFDI.DoctoRelacionado>();
                                               foreach (XElement doctoRelacionado in ((XElement)doctoRelacionados).Nodes())
                                               {
                                                   STVCFDI.DoctoRelacionado documento = new STVCFDI.DoctoRelacionado();

                                                   att = doctoRelacionado.Attribute("IdDocumento");
                                                   documento.iddocumento = (att != null) ? (att.Value) : "";

                                                   att = doctoRelacionado.Attribute("Serie");
                                                   documento.serie = (att != null) ? (att.Value) : "";

                                                   att = doctoRelacionado.Attribute("Folio");
                                                   documento.folio = (att != null) ? (att.Value) : "";

                                                   att = doctoRelacionado.Attribute("MonedaDR");
                                                   documento.monedadr = (att != null) ? (att.Value) : "";

                                                   att = doctoRelacionado.Attribute("TipoCambioDR");
                                                   documento.tipocambiodr = (att != null) ? Convert.ToDouble(att.Value) : 0.000000;

                                                   att = doctoRelacionado.Attribute("MetodoPagoDR");
                                                   documento.metodopagodr = (att != null) ? (att.Value) : "";

                                                   att = doctoRelacionado.Attribute("NumParcialidad");
                                                   documento.numparcialidad = (att != null) ? (att.Value) : "";

                                                   att = doctoRelacionado.Attribute("ImpSaldoAnt");
                                                   documento.impsaldoant = (att != null) ? Convert.ToDouble(att.Value) : 0.000000;

                                                   att = doctoRelacionado.Attribute("ImpPagado");
                                                   documento.imppagado = (att != null) ? Convert.ToDouble(att.Value) : 0.000000;

                                                   att = doctoRelacionado.Attribute("ImpSaldoInsoluto");
                                                   documento.impsaldoinsoluto = (att != null) ? Convert.ToDouble(att.Value) : 0.000000;

                                                   listRPDocumento.Add(documento);
                                               }

                                               if (listRPDocumento.Count > 0)
                                               {
                                                   CFDI.pago.doctorelacionado = new List<STVCFDI.DoctoRelacionado>();

                                                   CFDI.pago.doctorelacionado = listRPDocumento;
                                               }

                                           }
                                       }
                                   }
                               }
                           }

                           //aki
                           if (timbre != null)
                           {
                               XAttribute attTimbre = timbre.Attribute("UUID");
                               CFDI.timbrefiscaldigital.uuid = (attTimbre != null) ? attTimbre.Value.ToString() : "";

                               attTimbre = timbre.Attribute("FechaTimbrado");
                               if (attTimbre == null)
                                   CFDI.timbrefiscaldigital.fechatimbrado = CFDI.timbrefiscaldigital.fechatimbrado;
                               else
                                   CFDI.timbrefiscaldigital.fechatimbrado = Convert.ToDateTime(attTimbre.Value);

                               //attTimbre = timbre.Attribute("SelloCFD");
                               //CFDI.timbrefiscaldigital.sellocfd = (attTimbre != null) ? attTimbre.Value.ToString() : "";

                               //attTimbre = timbre.Attribute("SelloSAT");
                               //CFDI.timbrefiscaldigital.sellosat = (attTimbre != null) ? attTimbre.Value.ToString() : "";

                               //attTimbre = timbre.Attribute("NoCertificadoSAT");
                               //CFDI.timbrefiscaldigital.nocertificadosat = (attTimbre != null) ? attTimbre.Value.ToString() : "";
                           }
                       }
                   }
               }
           }
           catch (Exception ex)
           {
               throw new Exception("Error al leer el XML: " + RutaArchivo + " - ", ex);
           }

           return CFDI;
       }
    }
}
