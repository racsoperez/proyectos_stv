﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
//Paquetes para la lectura y modificación del iniparser  documentación: https://github.com/rickyah/ini-parser/wiki/First-Steps
using IniParser;
using IniParser.Model;
using Newtonsoft.Json;


namespace StvSoft
{
  public class STV_CONFIGURACIONES_INI
    {
        public static void agregar_problemas_configuracion(string rfc, string ultimo_problema, string ultimo_problema_codigo, string ultimo_problema_fechahora)
        {
            var parser = new FileIniDataParser();
            IniData _D_data = parser.ReadFile("config.ini");

            string nombreSeccion = "gestionador_configuracion";

            KeyDataCollection keyCol = _D_data[nombreSeccion];

            if (keyCol.Count > 0)
            {
                //modificando los datos de las secciones
                _D_data[nombreSeccion]["ultimo_problema"] = ultimo_problema + " - RFC: " + rfc;
                _D_data[nombreSeccion]["ultimo_problema_codigo"] = ultimo_problema_codigo;
                _D_data[nombreSeccion]["ultimo_problema_fechahora"] = ultimo_problema_fechahora;
            }
            //guardamos los datos en el .ini
            parser.WriteFile("config.ini", _D_data);
        }

        public static void guardar_problemas_por_rfc(int cuenta, string rfc, string fecha_actualizacion, string ultimo_problema, string ultimo_problema_codigo, string ultimo_problema_fechahora)
        {
            var parser = new FileIniDataParser();
            IniData _D_data = parser.ReadFile("config.ini");

            string nombreSeccion = "gestionador_rfc_" + cuenta + "_" + rfc;

            KeyDataCollection keyCol = _D_data[nombreSeccion];

            if (keyCol.Count > 0)
            {
                //modificando los datos de las secciones
                _D_data[nombreSeccion]["rfc"] = rfc;
                _D_data[nombreSeccion]["ultima_cfdis"] = fecha_actualizacion;
                _D_data[nombreSeccion]["ultimo_problema"] = ultimo_problema;
                _D_data[nombreSeccion]["ultimo_problema_codigo"] = Convert.ToString(ultimo_problema_codigo);
                _D_data[nombreSeccion]["ultimo_problema_fechahora"] = ultimo_problema_fechahora;
            }
            //guardamos los datos en el .ini
            parser.WriteFile("config.ini", _D_data);
        }
        public static void guardar_total_subidos_RFC(int _cuenta, string _rfc, int _total_cfdis, string _fecha_hora, string _msg_error)
        {
            var parser = new FileIniDataParser();
            IniData _D_data = parser.ReadFile("config.ini");

            string nombreSeccion = "gestionador_rfc_" + _cuenta + "_" + _rfc;

            var objJson = new List<TOTAL_CFDIS>();

            KeyDataCollection keyCol = _D_data[nombreSeccion];

            if (keyCol.Count > 0)
            {
               
                if (_D_data[nombreSeccion]["cfdis_descargados"] == "")
                {
                    objJson = new List<TOTAL_CFDIS>();
                }
                else
                {
                    objJson = JsonConvert.DeserializeObject<List<TOTAL_CFDIS>>(_D_data[nombreSeccion]["cfdis_descargados"]);
                }

                TOTAL_CFDIS total_descargas = new TOTAL_CFDIS
                {
                  rfc = _rfc,
                  total_cfdis = _total_cfdis,
                  fecha_hora = _fecha_hora,
                  msg_error = _msg_error

                };

                objJson.Add(total_descargas);

                string _total = JsonConvert.SerializeObject(objJson);

                //modificando los datos de las secciones
                _D_data[nombreSeccion]["cfdis_descargados"] = _total;
            }
            //guardamos los datos en el .ini
            parser.WriteFile("config.ini", _D_data);
        }

}
    class TOTAL_CFDIS
    {
    
        public string rfc { get; set; }
        public int total_cfdis { get; set; }
        public string fecha_hora { get; set; }
        public string msg_error { get; set; }

        public TOTAL_CFDIS()
        {
            rfc = string.Empty;
            total_cfdis = 0;
            fecha_hora = string.Empty;
            msg_error = string.Empty;
        }
    }
}

