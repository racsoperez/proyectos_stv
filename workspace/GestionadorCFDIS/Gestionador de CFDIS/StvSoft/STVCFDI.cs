﻿using HyperSoft.ElectronicDocument.Download.Resumen;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace StvSoft
{
    public class STVCFDI
    {
        //SECCION ATRIBUTOS COMPROBANTE CFDI 3.3
        public string version { get; set; }             //REQUERIDO
        public string serie { get; set; }               //OPCIONAL
        public string folio { get; set; }               //OPCIONAL
        public DateTime fecha { get; set; }             //REQUERIDO
        //public string sello { get; set; }               //REQUERIDO
        public string formapago { get; set; }           //CONDICIONAL          
        //public string nocertificado { get; set; }       //REQUERIDO
        //public string certificado { get; set; }         //REQUERIDO
        public string condicionesdepago { get; set; }   //CONDICIONAL
        public double subtotal { get; set; }            //REQUERIDO
        public double descuento { get; set; }           //CONDICIONAL
        public string moneda { get; set; }              //REQUERIDO
        public double tipocambio { get; set; }          //CONDICIONAL
        public double total { get; set; }               //REQUERIDO
        public string tipodecomprobante { get; set; }   //REQUERIDO
        public string metodopago { get; set; }          //CONDICIONAL
        public string lugarexpedicion { get; set; }     //REQUERIDO
        public double totalimpuestostrasladados { get; set; }   //CONDICIONAL
        public double totalimpuestosretenidos { get; set; }   //CONDICIONAL

        public Impuesto Impuestos { get; set; }

        public class Impuesto
         {
            public List<conceptoimpuestotraslado> trasladados { get; set; }    //CONDICIONAL
            public List<conceptoimpuestoretencion> retenidos { get; set; } //CONDICIONAL
        }



        public Emisor emisor { get; set; }     //REQUERIDO
        public Receptor receptor { get; set; }     //REQUERIDO
        public List<concepto> conceptos { get; set; }
        public TimbreFiscalDigital timbrefiscaldigital { get; set; }
        public Pago pago { get; set; }     //REQUERIDO

        public CfdiRelacionados cfdirelacionados { get; set; }

        //SECCION NODO EMISOR CFDI 3.3
        public class CfdiRelacionados
        {
            public string tiporelacion { get; set; } //REQUERIDO          
            public List<CfdiRelacionado> relacionado { get; set; } //REQUERIDO
        }

        //SECCION NODO EMISOR CFDI 3.3
        public class CfdiRelacionado
        {
            public string uuid { get; set; }           //REQUERIDO          
        }

        //SECCION NODO EMISOR CFDI 3.3
        public class Emisor
        {
            public string rfc { get; set; }           //REQUERIDO
            public string nombre { get; set; } //OPCIONAL
            public string regimenfiscal { get; set; } //REQUERIDO           
            public string cp { get; set; } //REQUERIDO           
        }

        //SECCION NODO RECEPTOR CFDI 3.3
        public class Receptor
        {
            public string rfc { get; set; }           //REQUERIDO
            public string nombre { get; set; } //OPCIONAL
            public string usocfdi { get; set; } //REQUERIDO           
        }

        //SECCION CONCEPTOS CFDI 3.3


        public class concepto
        {
            public double cantidad { get; set; }        //REQUERIDO
            public string claveprodserv { get; set; }   //REQUERIDO
            public string claveunidad { get; set; }     //REQUERIDO
            public string descripcion { get; set; }     //REQUERIDO
            public double descuento { get; set; }       //OPCIONAL
            public double importe { get; set; }         //REQUERIDO
            public string noidentificacion { get; set; }//OPCIONAL
            public string unidad { get; set; }          //OPCIONAL
            public double valorunitario { get; set; }   //REQUERIDO            


            public List<conceptoimpuestotraslado> conceptoimpuestostraslados { get; set; }    //CONDICIONAL
            public List<conceptoimpuestoretencion> conceptoimpuestosretenciones { get; set; } //CONDICIONAL
        }

        public class conceptoimpuestotraslado
        {
            [DisplayName("base")]
            public double basei { get; set; }            //REQUERIDO
            public string impuesto { get; set; }        //REQUERIDO
            public string tipofactor { get; set; }      //REQUERIDO
            public double tasaocuota { get; set; }      //OPCIONAL
            public double importe { get; set; }         //CONDICIONAL
        }

        public class conceptoimpuestoretencion
        {
            [DisplayName("base")]
            public double basei { get; set; }            //REQUERIDO
            public string impuesto { get; set; }        //REQUERIDO
            public string tipofactor { get; set; }      //REQUERIDO
            public double tasaocuota { get; set; }      //OPCIONAL
            public double importe { get; set; }         //CONDICIONAL
        }



        //SECCION NODO TIMBRE FISCAL DIGITAL CFDI 3.3
        public class TimbreFiscalDigital
        {
            public string uuid { get; set; }
            public DateTime fechatimbrado { get; set; }
            //public string sellocfd { get; set; }
            //public string sellosat { get; set; }
            //public string nocertificadosat { get; set; }
        }

        public class Pago
        {
            public DateTime fechapago { get; set; }             //REQUERIDO
            public string formapagop { get; set; }              //REQUERIDO
            public string monedap { get; set; }                 //REQUERIDO
            public double tipocambiop { get; set; }             //CONDICIONAL
            public double monto { get; set; }                   //REQUERIDO

            public List<DoctoRelacionado> doctorelacionado { get; set; }
        }

        public class DoctoRelacionado
        {
            public string iddocumento { get; set; }             //REQUERIDO
            public string serie { get; set; }                   //OPCIONAL
            public string folio { get; set; }                   //OPCIONAL
            public string monedadr { get; set; }                //REQUERIDO
            public double tipocambiodr { get; set; }            //CONDICIONAL
            public string metodopagodr { get; set; }            //REQUERIDO
            public string numparcialidad { get; set; }          //CONDICIONAL
            public double impsaldoant { get; set; }             //CONDICIONAL
            public double imppagado { get; set; }               //CONDICIONAL
            public double impsaldoinsoluto { get; set; }        //CONDICIONAL
        }



        public STVCFDI()
        {
            //seccion atributos comprobante cfdi 3.3
            version = "";
            serie = "";
            folio = "";
            fecha = DateTime.MinValue;
            //sello = "";
            formapago = "";
            //nocertificado = "";
            //certificado = "";
            condicionesdepago = "";
            subtotal = 0.00;
            descuento = 0.00;
            moneda = "";
            tipocambio = 0.00;
            total = 0.00;
            tipodecomprobante = "";
            metodopago = "";
            lugarexpedicion = "";
            totalimpuestostrasladados = 0.00;
            

            //cfdirelacionados = new CfdiRelacionados() { relacionado = new List<CfdiRelacionado>() };
            //emisor = new Emisor();
            //receptor = new Receptor();
            //conceptos = new List<concepto>() { new concepto() { conceptoimpuestosretenciones = new List<conceptoimpuestoretencion>(), conceptoimpuestostraslados = new List<conceptoimpuestotraslado>() }  };
            //timbrefiscaldigital = new  TimbreFiscalDigital();
            //pago = new Pago() { doctorelacionado = new List<DoctoRelacionado>() };
        }
    }
    public class CFDI_STATUS
    {
        public string uuid { get; set; }
        public string status { get; set; }
        public DateTime fecha_cancelacion { get; set; }
        public string status_cancelacion { get; set; }
        public string proceso_cancelacion { get; set; }
        public string pac_rfc { get; set; }
        public string pac_nombre_comercial { get; set; }
        public string pac_razon_social { get; set; }

         //cfdi.Pac.Rfc, cfdi.Pac.NombreComercial, cfdi.Pac.NombreComercial
        public CFDI_STATUS()
        {
            uuid = "";
            status = "";
            fecha_cancelacion = DateTime.MinValue;
            status_cancelacion = "";
            proceso_cancelacion = "";
            pac_rfc = "";
            pac_nombre_comercial = "";
            pac_razon_social = "";
        }
    }
}
