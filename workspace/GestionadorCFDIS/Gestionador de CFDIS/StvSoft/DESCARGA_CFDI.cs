﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HyperSoft.ElectronicDocument.Download.Data;
using HyperSoft.ElectronicDocument.Download.Resumen;
using HyperSoft.ElectronicDocument.Download.Parameters;
using HyperSoft.ElectronicDocument.Download.Activation;
using HyperSoft.Shared;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
namespace StvSoft
{
  public class DESCARGA_CFDI
    {
      string _s_json = string.Empty;

      string _n_return = string.Empty;
      string _s_msgError = string.Empty;

      string _strDirectorioBase = string.Empty;
      string _strRutaArchivos = string.Empty;
      string _strRutaLicencia = string.Empty;
      string _strRfcIntegrador = string.Empty;
      string _strIdIntegrador = string.Empty;

      string _strRfc = string.Empty;
      string _strClaveCiec = string.Empty;

        int _nTipoDescarga = 1;
        DateTime _dtFechaIni = DateTime.MinValue;
        DateTime _dtFechaFin = DateTime.MinValue;

        int _cuenta_id = 0;
        int _E_tipoinstruccion = 0; 

        public static string Line = string.Empty.PadRight(100, '=');

        public DESCARGA_CFDI(string strRfcIntegrador, string strIdIntegrador, string strRfc, string strClaveCiec, DateTime dtFechaIni, DateTime dtFechaFin, string strDirectorioBase, int Cuenta_id,int E_tipoinstruccion)
        {
            _strRfcIntegrador = strRfcIntegrador;
            _strIdIntegrador = strIdIntegrador;

            _strRfc = strRfc;
            _strClaveCiec = strClaveCiec;

            _dtFechaIni = dtFechaIni;
            _dtFechaFin = dtFechaFin;
            _strDirectorioBase = strDirectorioBase;
            _cuenta_id = Cuenta_id;
            _E_tipoinstruccion = E_tipoinstruccion;

        }
        
        public string Descargar(ref string _n_return, ref string _s_msgError)
        {
            _strRutaLicencia = RutaLicencia();
            CargarLicencia();
            string strMesage = CheckLicense(ref _n_return, ref _s_msgError);
            if (strMesage == "")
            {
                Result result = descargar();
                strMesage = GuardarDocumentos(result, ref _n_return, ref _s_msgError).ToString();
            }
            return strMesage;
        }

        private string RutaLicencia()
        {
            //Se crea la carpeta para guardar la licencia si no ha sido creada
            string strLicenciaRfc = string.Format("{0}\\{1}\\{2}", _strDirectorioBase, _cuenta_id + "_" + _strRfc, "Licencia");
            if (!Directory.Exists(strLicenciaRfc))
            {
                Directory.CreateDirectory(strLicenciaRfc);
            }

            List<string> listaFechas = new List<string>();

            string _nombreArchivo = "";

            DirectoryInfo dir = new DirectoryInfo(strLicenciaRfc);
            FileInfo[] infoFicheros = dir.GetFiles("*.license");

            //obtenemos la fecha de creación de los archivos y la agregamos a una lista
            for (int i = 0; i <= (infoFicheros.Length) - 1; i++)
            {
                listaFechas.Add(infoFicheros[i].CreationTime.ToString("yyyy-MM-dd"));
            }

            string _fecha_mayor = listaFechas.Max();

            foreach (FileInfo Ficheros in infoFicheros)
            {
               string _fecha_archivo = Ficheros.CreationTime.ToString("yyyy-MM-dd");

               if (_fecha_mayor == _fecha_archivo)
                {
                    _nombreArchivo = _fecha_archivo.Replace("-", "_");
                    _nombreArchivo = _nombreArchivo + "_licencia.license";

                    if (!System.IO.File.Exists(dir + "\\" + _nombreArchivo))
                    {
                        System.IO.File.Move(dir + "\\" + Ficheros, dir + "\\" + _nombreArchivo);
                    }
                    break;
                }
            }

            return strLicenciaRfc + "\\" + _nombreArchivo;
        }

        public void CargarLicencia()
        {
            if (System.IO.File.Exists(_strRutaLicencia))
            {
                using (MemoryStream stream = new MemoryStream(System.IO.File.ReadAllBytes(_strRutaLicencia)))
                {
                    HyperSoft.ElectronicDocument.Download.Activation.Activation.LoadLicense(stream);
                    System.Threading.Thread.Sleep(2000);
                }
            }

            // Con esta licencia podrás guardar tus XML a disco
            //string licenceEdr = Path.Combine(_strRutaLicencia, "edr.license");
            //if (System.IO.File.Exists(licenceEdr))
            //    using (MemoryStream stream = new MemoryStream(System.IO.File.ReadAllBytes(licenceEdr)))
            //        HyperSoft.ElectronicDocument.Rename.Activation.Activation.LoadLicense(stream);
        }
        private string CheckLicense(ref string _n_return, ref string _s_msgError)
        {
            _s_msgError = "";
            _n_return = "";
            if (HyperSoft.ElectronicDocument.Download.Activation.Activation.ActivationStatus == HyperSoft.ElectronicDocument.Download.Activation.ActivationStatusType.EvaluationMode)
            {
                _s_msgError = string.Format(
                  "Estimado usuario:{0}{0}" +
                  "Para el correcto funcionamiento de este ejemplo, es necesario cargar{0}" +
                  "la licencia que activa la librería E.D.D., si no cuentas con una, por favor,{0}" +
                  "ponte en contacto con nosotros a los siguientes teléfonos:{0}{0}" +
                  "    - (+52) 55.53.89.76.55{0}" +
                  "    - (+52) 55.60.01.63.69{0}{0}" +
                  "Atte.{0}" +
                  "Facturando",
                  Environment.NewLine);

                _n_return = "11017";
                _s_msgError = _n_return + ": NOK_ERROR_LICENCIA: No se pudo encontrar la licencia para el RFC: " + _strRfc;

                STV_CONFIGURACIONES_INI.agregar_problemas_configuracion(_strRfc, _s_msgError, _n_return, DateTime.Now.ToString());
                STV_CONFIGURACIONES_INI.guardar_problemas_por_rfc(_cuenta_id, _strRfc, DateTime.Now.ToString(), _n_return, _s_msgError, DateTime.Now.ToString());
            }

            //Verifica que la licencia este activa
            if ((HyperSoft.ElectronicDocument.Download.Activation.Activation.ActivationStatus != ActivationStatusType.Validating) && (HyperSoft.ElectronicDocument.Download.Activation.Activation.ActivationStatus != ActivationStatusType.Licensed))
            {
                _s_msgError = _n_return + ": NOK_ERROR_LICENCIA: La licencia a expirado para el RFC: " + _strRfc;
                STV_CONFIGURACIONES_INI.agregar_problemas_configuracion(_strRfc, _s_msgError, _n_return, DateTime.Now.ToString());
                STV_CONFIGURACIONES_INI.guardar_problemas_por_rfc(_cuenta_id, _strRfc, DateTime.Now.ToString(), _n_return, _s_msgError, DateTime.Now.ToString());
            }
            return _s_msgError;
        }
        //por tiempo
        private HyperSoft.ElectronicDocument.Download.Parameters.ByTime crearParametros()
        {
            HyperSoft.ElectronicDocument.Download.Parameters.ByTime parameters = new HyperSoft.ElectronicDocument.Download.Parameters.ByTime();
          
            // Definimos los datos del integrador proporcionados por Facturando
            parameters.Integrador.Rfc = _strRfcIntegrador;
            parameters.Integrador.Identificador = _strIdIntegrador;

            // Definimos los datos del contribuyente que se va a conectar al servidor del SAT
            parameters.Login.Rfc = _strRfc;
            parameters.Login.Ciec = _strClaveCiec;

            // Definimos que deseamos consultar o descargar (EMITIDOS | RECIBIDOS)
            parameters.Tipo = (HyperSoft.ElectronicDocument.Download.Parameters.Tipo)_nTipoDescarga;

            // Definimos el tipo de proceso a ejecutar (CONSULTAR | DESCARGAR)
            parameters.Process = HyperSoft.ElectronicDocument.Download.Parameters.Process.Descargar;

            parameters.FechaInicial = _dtFechaIni;
            parameters.FechaFinal = _dtFechaFin;

            parameters.Complemento = HyperSoft.ElectronicDocument.Download.Parameters.Complemento.All;

            //Se descargan los xml de acuerdo a la instrucción
            if(_E_tipoinstruccion == 12)
            {
                parameters.Status = HyperSoft.ElectronicDocument.Download.Status.Cancelado;
            }
            else
            {
                parameters.Status = HyperSoft.ElectronicDocument.Download.Status.Vigente;
            }
            
            parameters.Total.MinValue = decimal.MinValue;
            parameters.Total.MaxValue = decimal.MaxValue;
            parameters.Download.Repository = HyperSoft.ElectronicDocument.Download.Parameters.DownloadRepository.Memory;

            return parameters;
        }
 
        private HyperSoft.ElectronicDocument.Download.Resumen.Result descargar()
        {
            HyperSoft.ElectronicDocument.Download.Parameters.ByTime parameters = crearParametros();

            HyperSoft.ElectronicDocument.Download.Edd edd = new HyperSoft.ElectronicDocument.Download.Edd();
            
            return edd.Execute(parameters);   
        }

        private StringBuilder GuardarDocumentos(Result result, ref string _n_return, ref string _s_msgError)
        {
            StringBuilder strError = new StringBuilder();
            // -- Si se generó un error lo mostramos en pantalla
            if (result.Error.Type != HyperSoft.ElectronicDocument.Download.ErrorType.NONE)
                strError.Append(mostrarError(result.Error, ref _n_return, ref _s_msgError));

                
            strError.Append(mostrarResumen());
            //if (result.Error.Type == HyperSoft.ElectronicDocument.Download.ErrorType.NONE)
            //{
            //    strError.Append(mostrarError(result.Error, ref _n_return, ref _s_msgError));
            //}

            strError.Append(mostrarCfdiDescarga(result.Cfdi, ref _n_return, ref _s_msgError));
            strError.AppendLine();

            foreach (var cfdi in result.Cfdi)
            {
                //Se crean las carpetas para guardar los xml de acuerdo a la fecha de emisión
                _strRutaArchivos = CrearCarpetasPorFecha(cfdi.FechaEmision, _strRfc, _cuenta_id.ToString(), _strDirectorioBase);

                string strFileName = string.Format("{0}\\{1}.xml", _strRutaArchivos, cfdi.Uuid);

                if (cfdi.XmlFile.Data != null)
                {
                    if (!System.IO.File.Exists(strFileName))
                    {
                        try
                        {
                            CFDI_STATUS status_cfdi = new CFDI_STATUS()
                            {
                                uuid = cfdi.Uuid,
                                status = StatusToXML(cfdi.Status),
                                fecha_cancelacion = Convert.ToDateTime(cfdi.FechaCancelacion),
                                status_cancelacion = StatusCancelacionToXML(cfdi.StatusCancelacion),
                                proceso_cancelacion = StatusProcesoCancelacionToXML(cfdi.StatusProcesoCancelacion),
                                pac_rfc = cfdi.Pac.Rfc,
                                pac_nombre_comercial  = cfdi.Pac.NombreComercial,
                                pac_razon_social = cfdi.Pac.RazonSocial,
                            };

                            //Se agrega el comentario al .xml
                            _s_json = "<!--" + JsonConvert.SerializeObject(status_cfdi) + "-->";

                            byte[] _json_in_bytes = Encoding.UTF8.GetBytes(_s_json.ToString());
                            byte[] _newXML = new byte[cfdi.XmlFile.Data.Length + _json_in_bytes.Length];

                            Array.Copy(cfdi.XmlFile.Data, 0, _newXML, 0, cfdi.XmlFile.Data.Length);
                            Array.Copy(_json_in_bytes, 0, _newXML, cfdi.XmlFile.Data.Length, _json_in_bytes.Length);

                            System.IO.File.WriteAllBytes(strFileName, _newXML);

                        }
                        catch
                        {
                            if (cfdi.XmlFile.Error != null)
                            {
                                strError.Append("CFDI:" + cfdi.Uuid + " - " + cfdi.XmlFile.Error.Message + "\n");
                                STV_CONFIGURACIONES_INI.agregar_problemas_configuracion(_strRfc, strError.ToString(), "17", DateTime.Now.ToString());
                                STV_CONFIGURACIONES_INI.guardar_problemas_por_rfc(_cuenta_id, _strRfc, DateTime.Now.ToString(), "17", strError.ToString(), DateTime.Now.ToString());
                            }
                        }
                    }
                }
                else
                {
                    if (cfdi.XmlFile.Error != null)
                    {
                        strError.Append("CFDI:" + cfdi.Uuid + " - " + cfdi.XmlFile.Error.Message + "\n");
                        //break;
                    }
                }
            }

            return strError;
        }

        private string mostrarError(HyperSoft.ElectronicDocument.Download.Error error, ref string _n_return, ref string _s_msgError)
        {
            if (error == null)
                return string.Empty;

            StringBuilder sb = new StringBuilder();

            //sb.AppendLine("ERROR").AppendLine(Line);
            sb.AppendLine("Proceso: " + error.Process);
            sb.AppendLine("Error  : " + error.Type);
            sb.AppendLine("Mensaje: " + error.Message);
            sb.AppendLine().AppendLine();

            //asignamos el error a la variable
            _s_msgError = error.Type + ": " + error.Message; // + error.Process + " : " + error.Type + " : " + error.Message
            _n_return = "18";

            if (error.InConnection != null)
            {
                sb.AppendLine("SE GENERÓ UN ERROR EN LA CONEXIÓN").AppendLine(Line);

                //asignamos el error a la variable
                _s_msgError = "NOK_CONEXION: " + "Error en la conexion " + error.InConnection.Type.ToString();
                _n_return = "18";

                // -- Obtenemos el error y la URL de la conexión donde ocurrió el error
                sb.AppendLine("Tipo   : " + error.InConnection.Type);
                sb.AppendLine("URL    : " + error.InConnection.Url);

                string message;
                string code;
                int codigo;

                // -- Obtenemos los datos de la excepción que se generó
                error.InConnection.GetWebExceptionData(out codigo, out code, out message);
                sb.AppendLine("Código : " + codigo);
                sb.AppendLine("Code   : " + code);
                sb.AppendLine("Mensaje: " + message);
            }

            if (error.InRename != null)
            {
                HyperSoft.ElectronicDocument.Rename.ErrorType errorType = (HyperSoft.ElectronicDocument.Rename.ErrorType)error.InRename.Type;

                sb.AppendLine("SE GENERÓ UN ERROR AL RENOMBRAR LOS ARCHIVOS").AppendLine(Line);

                sb.AppendLine("Error  : " + errorType);
                sb.AppendLine("Mensaje: " + error.InRename.Message);

                //asignamos el error a la variable
                _s_msgError = "Error al renombrar archivos: " + errorType;
                _n_return = "18";

                //Error = 20: Error al renombrar archivos
            }

            //Se guardan los errores en el .ini
            STV_CONFIGURACIONES_INI.agregar_problemas_configuracion(_strRfc, _s_msgError, _n_return, DateTime.Now.ToString());
            STV_CONFIGURACIONES_INI.guardar_problemas_por_rfc(_cuenta_id, _strRfc, DateTime.Now.ToString(), _s_msgError,  _n_return, DateTime.Now.ToString());
            
            return sb.ToString();
        }

        private string mostrarResumen()
        {
            StringBuilder sb = new StringBuilder();

            // -- Datos del proceso
            sb.AppendLine("RESUMEN").AppendLine(Line);
            sb.AppendLine(string.Format("Tipo             : {0}", ParameterTypeToTex(HyperSoft.ElectronicDocument.Download.Progress.Parameters.Type)));
            sb.AppendLine(string.Format("Proceso          : {0}", ProcessToText(HyperSoft.ElectronicDocument.Download.Progress.Parameters.Process)));
            // -- Fecha y hora en la que inició y terminó la consulta o descarga
            sb.AppendLine(string.Format("Fecha del proceso: {0} - {1}", HyperSoft.ElectronicDocument.Download.Progress.StartDate, HyperSoft.ElectronicDocument.Download.Progress.EndDate));
            // -- Tiempo total que tardo en ejecutarse la consulta o descarga
            sb.AppendLine(string.Format("Tiempo           : {0:N0} ms", HyperSoft.ElectronicDocument.Download.Progress.TimeElapsed));


            // -- De esta forma podemos saber si el proceso fue cancelado
            if (HyperSoft.ElectronicDocument.Download.Progress.Canceled)
            {
                sb.AppendLine().AppendLine().AppendLine(Line);
                sb.AppendLine("EL PROCESO FUE CANCELADO POR EL USUARIO");
                sb.AppendLine(Line).AppendLine();
            }


            // -- Mostramos el resultado de la consulta
            sb.AppendLine().AppendLine();
            sb.AppendLine("CONSULTA").AppendLine(Line);
            sb.AppendLine(string.Format("Encontrados: {0:N0}", HyperSoft.ElectronicDocument.Download.Progress.Query.Encontrados));
            sb.AppendLine(string.Format("Vigentes   : {0:N0}", HyperSoft.ElectronicDocument.Download.Progress.Query.Vigentes));
            sb.AppendLine(string.Format("Cancelados : {0:N0}", HyperSoft.ElectronicDocument.Download.Progress.Query.Cancelados));
            sb.AppendLine(string.Format("Tiempo     : {0:N0} ms", HyperSoft.ElectronicDocument.Download.Progress.Query.TimeElapsed));


            // -- Mostramos el resultado de la descarga
            if (HyperSoft.ElectronicDocument.Download.Progress.Parameters.Process == HyperSoft.ElectronicDocument.Download.Parameters.Process.Descargar)
            {
                sb.AppendLine().AppendLine();
                sb.AppendLine("DESCARGA").AppendLine(Line);
                sb.AppendLine(string.Format("UUDIs a descargar      : {0:N0}", HyperSoft.ElectronicDocument.Download.Progress.Download.UuidToDownload));
                sb.AppendLine(string.Format("UUDIs descargados      : {0:N0}", HyperSoft.ElectronicDocument.Download.Progress.Download.UuidDownloaded));
                sb.AppendLine(string.Format("Errores en la descarga : {0:N0}", HyperSoft.ElectronicDocument.Download.Progress.Download.Error));
                sb.AppendLine(string.Format("Tiempo                 : {0:N0} ms", HyperSoft.ElectronicDocument.Download.Progress.Download.TimeElapsed));
            }

            // -- Mostramos el resultado de guardar a disco
            if (HyperSoft.ElectronicDocument.Download.Progress.Download.Status != HyperSoft.ElectronicDocument.Download.ProcessStatus.None)
            {
                sb.AppendLine().AppendLine();
                sb.AppendLine("GUARDAR A DISCO").AppendLine(Line);
                sb.AppendLine(string.Format("XML a guardar     : {0:N0}", HyperSoft.ElectronicDocument.Download.Progress.Save.XmlToSave));
                sb.AppendLine(string.Format("XML guardados     : {0:N0}", HyperSoft.ElectronicDocument.Download.Progress.Save.XmlSaved));
                sb.AppendLine(string.Format("Errores al guardar: {0:N0}", HyperSoft.ElectronicDocument.Download.Progress.Save.Error));
                sb.AppendLine(string.Format("Tiempo            : {0:N0} ms", HyperSoft.ElectronicDocument.Download.Progress.Save.TimeElapsed));
                sb.AppendLine(Line);
            }

            return sb.ToString();
        }

        private string mostrarCfdiDescarga(IEnumerable<Cfdi> cfdis, ref string _n_return, ref string _s_msgError)
        {
            StringBuilder sb = new StringBuilder();

            int count = 1;
            foreach (Cfdi cfdi in cfdis)
            {
                sb.AppendLine(string.Format("CFDI - {0:N0}", count)).AppendLine(Line);
                sb.AppendLine(string.Format("Versión                 : {0}", cfdi.Version));
                sb.AppendLine(string.IsNullOrEmpty(cfdi.Serie)
                  ? string.Format("Serie y Folio           : {0}", cfdi.Folio)
                  : string.Format("Serie y Folio           : {0} - {1}", cfdi.Serie, cfdi.Folio));
                sb.AppendLine(string.Format("UUID                    : {0}", cfdi.Uuid));
                sb.AppendLine(string.Format("Efecto                  : {0}", EfectoToText(cfdi.Efecto)));
                sb.AppendLine(string.Format("Emisor                  : {0} - {1}", cfdi.RfcEmisor, cfdi.RazonSocialEmisor));
                sb.AppendLine(string.Format("Receptor                : {0} - {1}", cfdi.RfcReceptor, cfdi.RazonSocialReceptor));
                sb.AppendLine(string.Format("Fecha de emisión        : {0}", cfdi.FechaEmision));
                sb.AppendLine(string.Format("Fecha de certificación  : {0}", cfdi.FechaCertificacion));
                sb.AppendLine(string.Format("Forma de pago           : {0}", cfdi.FormaPago));
                sb.AppendLine(string.Format("Método de pago          : {0}", cfdi.MetodoPago));
                sb.AppendLine(string.Format("Moneda                  : {0}", cfdi.Moneda));
                sb.AppendLine(string.Format("SubTotal                : {0:N2}", cfdi.SubTotal));
                sb.AppendLine(string.Format("Descuento               : {0:N2}", cfdi.Descuento));
                sb.AppendLine(string.Format("Impuestos retenidos     : {0:N2}", cfdi.TotalImpuestosRetenidos));
                sb.AppendLine(string.Format("Impuestos trasladados   : {0:N2}", cfdi.TotalImpuestosTrasladados));
                sb.AppendLine(string.Format("Total                   : {0:N2}", cfdi.Total));
                sb.AppendLine(string.Format("Status                  : {0}", StatusToText(cfdi.Status)));
                sb.AppendLine(string.Format("Fecha de cancelación    : {0}", cfdi.FechaCancelacion));
                sb.AppendLine(string.Format("Status de la cancelación: {0}", StatusCancelacionToText(cfdi.StatusCancelacion)));
                sb.AppendLine(string.Format("Proceso de cancelación  : {0}", StatusProcesoCancelacionToText(cfdi.StatusProcesoCancelacion)));
                sb.AppendLine(string.Format("PAC                     : {0} - {1} - {2}", cfdi.Pac.Rfc, cfdi.Pac.NombreComercial, cfdi.Pac.RazonSocial));

                if (cfdi.XmlFile != null)
                {
                    sb.AppendLine(string.Format("Archivo                 : {0}", cfdi.XmlFile.FileName));
                    sb.AppendLine(string.Format("Status de archivo       : {0}", StatusXmlFileToText(cfdi.XmlFile.Status)));

                    // Con esta línea podemos acceder al archivo XML
                    // cfdi.XmlFile.Data
                    
                    mostrarError(cfdi.XmlFile.Error, ref _n_return, ref _s_msgError);

                     if (_s_msgError != string.Empty)
                    {
                        break;
                    }
                }

                sb.AppendLine(Line);

                count++;
            }
            STV_CONFIGURACIONES_INI.guardar_total_subidos_RFC(_cuenta_id, _strRfc, count, DateTime.Now.ToString(), _s_msgError);
            return sb.ToString().Trim();
        }

        private string CrearCarpetasPorFecha(DateTime _fecha, string _rfc, string _cuenta, string _directorio)
        {
            //_fecha = _fecha.Replace("/", "-");

            string _month = _fecha.Month.ToString();
            string _year = _fecha.Year.ToString();

            string fecha = _year + "_" + _month;

            //Carpeta donde se van a guardar los xml descargados.
            string strCFDIS_descargados_fecha = string.Format("{0}\\{1}\\{2}\\{3}", _directorio, _cuenta + "_" + _rfc, "CFDIs_descargados", fecha);
            if (!Directory.Exists(strCFDIS_descargados_fecha))
            {
                Directory.CreateDirectory(strCFDIS_descargados_fecha);
            }

            string strCFDIS_pendientes_fecha = string.Format("{0}\\{1}\\{2}\\{3}", _directorio, _cuenta + "_" + _rfc, "CFDIs_pendientes", fecha);
            if (!Directory.Exists(strCFDIS_pendientes_fecha))
            {
                Directory.CreateDirectory(strCFDIS_pendientes_fecha);
            }

            string strCFDIS_subidos_fecha = string.Format("{0}\\{1}\\{2}\\{3}", _directorio, _cuenta + "_" + _rfc, "CFDIs_subidos", fecha);
            if (!Directory.Exists(strCFDIS_subidos_fecha))
            {
                Directory.CreateDirectory(strCFDIS_subidos_fecha);
            }

            return strCFDIS_descargados_fecha;
        }

        private string ParameterTypeToTex(HyperSoft.ElectronicDocument.Download.Parameters.ParameterType parameterType)
        {
            switch (parameterType)
            {
                case HyperSoft.ElectronicDocument.Download.Parameters.ParameterType.ByTime:
                    return "Por fecha";
                case HyperSoft.ElectronicDocument.Download.Parameters.ParameterType.ByUuid:
                    return "Por UUID";
                case HyperSoft.ElectronicDocument.Download.Parameters.ParameterType.ByCanceledInPeriod:
                    return "Cancelados en un periodo";
                case HyperSoft.ElectronicDocument.Download.Parameters.ParameterType.ByNomina:
                    return "Nómina";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private string ProcessToText(HyperSoft.ElectronicDocument.Download.Parameters.Process process)
        {
            switch (process)
            {
                case HyperSoft.ElectronicDocument.Download.Parameters.Process.Consultar:
                    return "Consultar";
                case HyperSoft.ElectronicDocument.Download.Parameters.Process.Descargar:
                    return "Descargar";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private string EfectoToText(HyperSoft.ElectronicDocument.Download.Data.Efecto efecto)
        {
            switch (efecto)
            {
                case HyperSoft.ElectronicDocument.Download.Data.Efecto.Ingreso:
                    return "Ingreso";
                case HyperSoft.ElectronicDocument.Download.Data.Efecto.Egreso:
                    return "Egreso";
                case HyperSoft.ElectronicDocument.Download.Data.Efecto.Traslado:
                    return "Traslado";
                case HyperSoft.ElectronicDocument.Download.Data.Efecto.Pago:
                    return "Pago";
                case HyperSoft.ElectronicDocument.Download.Data.Efecto.Nomina:
                    return "Nómina";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private string StatusToText(HyperSoft.ElectronicDocument.Download.Data.Status status)
        {
            switch (status)
            {
                case HyperSoft.ElectronicDocument.Download.Data.Status.Vigente:
                    return "Vigente";
                case HyperSoft.ElectronicDocument.Download.Data.Status.Cancelado:
                    return "Cancelado";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private string StatusCancelacionToText(HyperSoft.ElectronicDocument.Download.Data.StatusCancelacion statusCancelacion)
        {
            switch (statusCancelacion)
            {
                case HyperSoft.ElectronicDocument.Download.Data.StatusCancelacion.None:
                    return string.Empty;
                case HyperSoft.ElectronicDocument.Download.Data.StatusCancelacion.NoCancelable:
                    return "No cancelable";
                case HyperSoft.ElectronicDocument.Download.Data.StatusCancelacion.CancelableSinAceptacion:
                    return "Cancelable sin aceptación";
                case HyperSoft.ElectronicDocument.Download.Data.StatusCancelacion.CancelableConAceptacion:
                    return "Cancelable con aceptación";
                case HyperSoft.ElectronicDocument.Download.Data.StatusCancelacion.EnProceso:
                    return "En proceso";
                case HyperSoft.ElectronicDocument.Download.Data.StatusCancelacion.SolicitudRechazada:
                    return "Solicitud rechazada";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private string StatusProcesoCancelacionToText(HyperSoft.ElectronicDocument.Download.Data.StatusProcesoCancelacion statusProcesoCancelacion)
        {
            switch (statusProcesoCancelacion)
            {
                case HyperSoft.ElectronicDocument.Download.Data.StatusProcesoCancelacion.None:
                    return string.Empty;
                case HyperSoft.ElectronicDocument.Download.Data.StatusProcesoCancelacion.EnProceso:
                    return "En proceso";
                case HyperSoft.ElectronicDocument.Download.Data.StatusProcesoCancelacion.PlazoVencido:
                    return "Plazo vencido";
                case HyperSoft.ElectronicDocument.Download.Data.StatusProcesoCancelacion.ConAceptacion:
                    return "Con aceptación";
                case HyperSoft.ElectronicDocument.Download.Data.StatusProcesoCancelacion.SinAceptacion:
                    return "Sin aceptación";
                case HyperSoft.ElectronicDocument.Download.Data.StatusProcesoCancelacion.SolicitudRechazada:
                    return "Solicitud rechazada";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private string StatusXmlFileToText(StatusDownload status)
        {
            switch (status)
            {
                case StatusDownload.Error: return "Error - Se generó un error desconocido.";
                case StatusDownload.ErrorOnSave: return "Error - Se generó un error al guardar el archivo.";
                case StatusDownload.Downloaded: return "Downloaded - El archivo fue descargado con éxito.";
                case StatusDownload.PreviouslyDownloaded: return "PreviouslyDownloaded - El archivo habia sido descargado previamente";
                case StatusDownload.NotApplicable: return "NotApplicable - EL SAT no permite descargar este tipo de archivos: Recibido y cancelado";
                case StatusDownload.DownloadCanceled: return "DownloadCanceled - Se cancelo la descarga del archivo.";
                case StatusDownload.Saved: return "Saved- El archivo fue guardado de disco.";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        #region
        private string StatusToXML(HyperSoft.ElectronicDocument.Download.Data.Status status)
        {
            switch (status)
            {
                case HyperSoft.ElectronicDocument.Download.Data.Status.Vigente:
                    return "1";
                case HyperSoft.ElectronicDocument.Download.Data.Status.Cancelado:
                    return "2";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        private string StatusCancelacionToXML(HyperSoft.ElectronicDocument.Download.Data.StatusCancelacion statusCancelacion)
        {
            switch (statusCancelacion)
            {
                case HyperSoft.ElectronicDocument.Download.Data.StatusCancelacion.None:
                    return string.Empty;
                case HyperSoft.ElectronicDocument.Download.Data.StatusCancelacion.NoCancelable:
                    return "1";
                case HyperSoft.ElectronicDocument.Download.Data.StatusCancelacion.CancelableSinAceptacion:
                    return "2";
                case HyperSoft.ElectronicDocument.Download.Data.StatusCancelacion.CancelableConAceptacion:
                    return "3";
                case HyperSoft.ElectronicDocument.Download.Data.StatusCancelacion.EnProceso:
                    return "4";
                case HyperSoft.ElectronicDocument.Download.Data.StatusCancelacion.SolicitudRechazada:
                    return "5";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        private string StatusProcesoCancelacionToXML(HyperSoft.ElectronicDocument.Download.Data.StatusProcesoCancelacion statusProcesoCancelacion)
        {
            switch (statusProcesoCancelacion)
            {
                case HyperSoft.ElectronicDocument.Download.Data.StatusProcesoCancelacion.None:
                    return string.Empty;
                case HyperSoft.ElectronicDocument.Download.Data.StatusProcesoCancelacion.EnProceso:
                    return "1";
                case HyperSoft.ElectronicDocument.Download.Data.StatusProcesoCancelacion.PlazoVencido:
                    return "2";
                case HyperSoft.ElectronicDocument.Download.Data.StatusProcesoCancelacion.ConAceptacion:
                    return "3";
                case HyperSoft.ElectronicDocument.Download.Data.StatusProcesoCancelacion.SinAceptacion:
                    return "4";
                case HyperSoft.ElectronicDocument.Download.Data.StatusProcesoCancelacion.SolicitudRechazada:
                    return "5";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        #endregion
    }
}
