﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestionador_de_CFDIS
{
    class STV_FWK_CRYPTO
    {
        public static List<byte> StringToByteList(string s_data)
        {

            List<byte> _L_byteList = Encoding.ASCII.GetBytes(s_data).ToList();

            return _L_byteList;
        }

        public static string ByteListToString(List<byte> L_byteList)
        {
            ASCIIEncoding encoding = new ASCIIEncoding();

            string _s_byteListToString = encoding.GetString(L_byteList.ToArray());

            return _s_byteListToString;
        }

        public static List<byte> HexstrToByteList(string s_hexstr)
        {
            List<byte> _L_hexstrToByteList = Enumerable.Range(0, s_hexstr.Length)
              .Where(x => x % 2 == 0)
              .Select(x => Convert.ToByte(s_hexstr.Substring(x, 2), 16))
              .ToList();

            return _L_hexstrToByteList;
        }

        public static string ByteListToHexstr(List<byte> L_byteList)
        {
            string _s_hexstring = BitConverter.ToString(L_byteList.ToArray());

            return _s_hexstring;
        }

        public static string Encrypt(string s_salt, int n_key, string s_data)
        {
            string _s_result = ByteListToHexstr(EncryptDecryptData(s_salt, n_key, s_data, null));

            return _s_result;
        }

        public static string Decrypt(string s_salt, int n_key, string s_data)
        {

            string _s_result = "";

            List<byte> _L_listaHexa = HexstrToByteList(s_data);

            List<byte> _L_lista = EncryptDecryptData(s_salt, n_key, "", _L_listaHexa);

            foreach (int _n_element in _L_lista)
            {
                _s_result = _s_result + Convert.ToChar(_n_element);
            }

            return _s_result;
        }

        public static List<byte> EncryptDecryptData(string s_salt, int n_key, string s_data, List<byte> L_data)
        {
            List<string> _L_keys = new List<string>{
                "xwasFycBUhIUzggf", 
                "n1rCkXNeajz7OekX", 
                "4o4HZ3VCEtzFIr5B", 
                "3l4ShAl9Qw3Lmar1", 
                "ES5Q6IJXAK6IQOq9", 
                "Qdbn3BHLg9fv3DXq", 
                "JVvZLJJS7W9x5WG2", 
                "fwhLB0xQKarp1KXW", 
                "eXduUAz8Cfr3F2Kq", 
                "fJVbohtnTG6Lpc9z",     
            };

            List<byte> _L_bytesKey = StringToByteList(_L_keys[n_key]);
            List<byte> _L_bytesSalt = StringToByteList(s_salt);
            List<byte> _L_bytesData = new List<byte>();

            if (s_data == "")
            {
                _L_bytesData.AddRange(L_data);
            }
            else
            {
                _L_bytesData = StringToByteList(s_data);
            }

            var _n_bytesDataCount = _L_bytesData.Count;

            var _n_bytesKey = _L_bytesKey.Count;

            var _n_bytesSalt = _L_bytesSalt.Count;

            Double _n_resultado = _n_bytesDataCount / _n_bytesKey;

            var _n_ceilBytesKey = 1 + Math.Ceiling((_n_resultado));

            if (_n_ceilBytesKey != 1)
            {
                // Si el ceil es diferente de 1
                for (int i = 0; i < _n_ceilBytesKey; i++)
                {
                    _L_bytesKey.AddRange(_L_bytesKey);
                }
            }
            else
            {
                // pass
            }

            _n_resultado = _n_bytesDataCount / _n_bytesSalt;

            var _n_ceilBytesSalt = 1 + Math.Ceiling((_n_resultado));

            if (_n_ceilBytesSalt != 1)
            {
                // Si el ceil es diferente de 1
                for (int i = 0; i < _n_ceilBytesSalt; i++)
                {
                    _L_bytesSalt.AddRange(_L_bytesSalt);
                }
            }
            else
            {
                // pass
            }

            List<byte> _L_encryptedData = new List<byte>();

            for (int i = 0; i < _L_bytesData.Count(); i++)
            {
                _n_resultado = (_L_bytesKey[i] ^ _L_bytesSalt[i] ^ _L_bytesData[i]);

                _L_encryptedData.Add(Convert.ToByte(_n_resultado));
            }

            return _L_encryptedData;
        }

    }
}
