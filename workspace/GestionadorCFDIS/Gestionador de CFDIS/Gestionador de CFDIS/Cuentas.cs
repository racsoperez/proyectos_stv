﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestionador_de_CFDIS
{
    public class CUENTAS
    {
        public int CuentaId { get; set; }
        public int EmpresaId { get; set; }
        public string Rfc { get; set; }
        public string ClaveCiec { get; set; }
        public DateTime Fecha_desde { get; set; }
        public DateTime Fecha_hasta { get; set; }
        public string Id_integrador { get; set; }
        public string Rfc_integrador { get; set; }
        public int s_id { get; set; }
        public int E_tipoinstruccion { get; set; }

        public CUENTAS()
        {
            CuentaId = 0;
            EmpresaId = 0;
            Rfc = string.Empty;
            ClaveCiec = string.Empty;
            Fecha_desde = DateTime.MinValue;
            Fecha_hasta = DateTime.MinValue;
            Id_integrador = string.Empty;
            Rfc_integrador = string.Empty;
            s_id = 0;
            E_tipoinstruccion = 0;
        }
    }
}

