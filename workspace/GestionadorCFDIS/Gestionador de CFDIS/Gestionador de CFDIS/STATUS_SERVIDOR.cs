﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Timers;
using System.Windows.Forms;
using System.Management;
using System.IO;

namespace Gestionador_de_CFDIS
{
   public static class STATUS_SERVIDOR
    {

        /*
         * Basado en el codigo: https://stackoverflow.com/a/10028263/2141126
         */

            [DllImport("psapi.dll", SetLastError = true)]
            [return: MarshalAs(UnmanagedType.Bool)]
            public static extern bool GetPerformanceInfo(
                [Out] out PerformanceInformation PerformanceInformation,
                [In] int Size);

            [StructLayout(LayoutKind.Sequential)]
            public struct PerformanceInformation
            {
                public int Size;
                public IntPtr CommitTotal;
                public IntPtr CommitLimit;
                public IntPtr CommitPeak;
                public IntPtr PhysicalTotal;
                public IntPtr PhysicalAvailable;
                public IntPtr SystemCache;
                public IntPtr KernelTotal;
                public IntPtr KernelPaged;
                public IntPtr KernelNonPaged;
                public IntPtr PageSize;
                public int HandlesCount;
                public int ProcessCount;
                public int ThreadCount;
            }

            public static Int64 GetPhysicalAvailableMemoryInMiB()
            {
                PerformanceInformation pi = new PerformanceInformation();
                if (GetPerformanceInfo(out pi, Marshal.SizeOf(pi)))
                {
                    return Convert.ToInt64((pi.PhysicalAvailable.ToInt64() * pi.PageSize.ToInt64() / 1048576));
                }
                else
                {
                    return -1;
                }

            }

            public static Int64 GetTotalMemoryInMiB()
            {
                PerformanceInformation pi = new PerformanceInformation();
                if (GetPerformanceInfo(out pi, Marshal.SizeOf(pi)))
                {
                    return Convert.ToInt64((pi.PhysicalTotal.ToInt64() * pi.PageSize.ToInt64() / 1048576));
                }
                else
                {
                    return -1;
                }

            }

        public static List<double> StatusHardDrive()
        {

            List<double> lst_hdd = new List<double>();    

            //Referir al namespace \\root\cimv2
            ManagementScope scope = new ManagementScope("\\root\\cimv2");
            //Crear un objeto para consultar una tabla del namespace
            ObjectQuery query = new ObjectQuery("SELECT * FROM Win32_LogicalDisk where drivetype=3");
            //Ejecutar el query
            ManagementObjectSearcher mos = new ManagementObjectSearcher(scope, query);

            //Iterar en los resultados del query
            foreach (ManagementObject item in mos.Get())
            {

                long hddSizeBytes = Int64.Parse(item["Size"].ToString());
                double hddSizeGBytes = hddSizeBytes / 1024 / 1024 / 1024;
                long hddFreeBytes = Int64.Parse(item["FreeSpace"].ToString());
                double hddFreeGBytes = hddFreeBytes / 1024 / 1024 / 1024;

                //MessageBox.Show("Tamaño = " + hddSizeGBytes + "Gb :" + "Libre = " + hddFreeGBytes +"Gb");

                lst_hdd.Add(hddSizeGBytes);
                lst_hdd.Add(hddFreeGBytes);
            }

            return lst_hdd;
        }
    }
}
