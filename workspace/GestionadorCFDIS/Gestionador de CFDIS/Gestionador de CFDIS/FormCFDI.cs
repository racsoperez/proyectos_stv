﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using StvSoft;
using NLog;
using System.IO;
using Newtonsoft.Json;
using System.Globalization;
using System.Threading;
using System.Timers;


namespace Gestionador_de_CFDIS
{
    public partial class FormCFDI : Form
    {
        public static Logger logger = LogManager.GetCurrentClassLogger();

        public STV_SERVICE _stvService = new STV_SERVICE();

        public FormCFDI()
        {
            InitializeComponent();
        }

        private void FormCFDI_Load(object sender, EventArgs e)
        {
            logger.Info("iniciando Gestionador CFDIs");
        }

        public void btnDescargarFE_Click(object sender, EventArgs e)
        {
            //Consola con = new Consola();
            //con.Show();

            memoResultado.Text = "";
            Program.testInternet();
            string _resultado = string.Empty;
            if (Program.result == "")
            {
               _stvService.Opciones(ref _resultado);

                memoResultado.Text = _resultado;
               //descargas(); //Emitidas
               //_stvService.subirArchivosXML(_stvService.strDirectorioBase.Trim());
            }
            else
            {
                MessageBox.Show(Program.result, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }  
        }
        private void btnSubirFE_Click(object sender, EventArgs e)
        {
            //_stvService.SubirArchivosXML(1,2);
        }
        public void Notificacion(string info, string rfc)
        {
            ShowInTaskbar = false;
            trayBar.Visible = true;
            trayBar.ShowBalloonTip(30000, "Stverticales", info + rfc, ToolTipIcon.Info);
        }

        private void btnTimbrar_Click(object sender, EventArgs e)
        {

   
           //List<double> lst = STATUS_SERVIDOR.StatusHardDrive();

           // MessageBox.Show("" + lst);
            //Cursor.Current = Cursors.WaitCursor;
            //memoResultado.Text = _stvService.TimbrarCFDI();
            //Cursor.Current = Cursors.Default;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Int64 phav = STATUS_SERVIDOR.GetPhysicalAvailableMemoryInMiB();
            Int64 tot = STATUS_SERVIDOR.GetTotalMemoryInMiB();
            decimal percentFree = ((decimal)phav / (decimal)tot) * 100;
            decimal percentOccupied = 100 - percentFree;
            lblDisponible.Text = phav.ToString() + " MB";
            lblTotal.Text = tot.ToString() + " MB";

            lblUsada.Text = Convert.ToString(tot - phav) + " MB";
            //lblLibre.Text = percentFree.ToString() + " %";
            //lblOcupada.Text = percentOccupied.ToString() + " %";
        }
    }
}
