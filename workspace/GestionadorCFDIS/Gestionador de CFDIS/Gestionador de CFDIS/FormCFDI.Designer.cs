﻿namespace Gestionador_de_CFDIS
{
    partial class FormCFDI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCFDI));
            this.memoResultado = new System.Windows.Forms.RichTextBox();
            this.trayBar = new System.Windows.Forms.NotifyIcon(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnSubirFE = new System.Windows.Forms.Button();
            this.btnDescargarFE = new System.Windows.Forms.Button();
            this.btnTimbrar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.lblDisponible = new System.Windows.Forms.Label();
            this.lblUsada = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // memoResultado
            // 
            this.memoResultado.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoResultado.Location = new System.Drawing.Point(12, 77);
            this.memoResultado.Name = "memoResultado";
            this.memoResultado.ReadOnly = true;
            this.memoResultado.Size = new System.Drawing.Size(861, 458);
            this.memoResultado.TabIndex = 26;
            this.memoResultado.Text = "";
            // 
            // trayBar
            // 
            this.trayBar.Icon = ((System.Drawing.Icon)(resources.GetObject("trayBar.Icon")));
            this.trayBar.Visible = true;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btnSubirFE
            // 
            this.btnSubirFE.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSubirFE.Image = global::Gestionador_de_CFDIS.Properties.Resources.baseline_arrow_circle_up_black_18dp;
            this.btnSubirFE.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSubirFE.Location = new System.Drawing.Point(215, 12);
            this.btnSubirFE.Name = "btnSubirFE";
            this.btnSubirFE.Size = new System.Drawing.Size(152, 40);
            this.btnSubirFE.TabIndex = 1;
            this.btnSubirFE.Text = "Subir xml";
            this.btnSubirFE.UseVisualStyleBackColor = true;
            this.btnSubirFE.Click += new System.EventHandler(this.btnSubirFE_Click);
            // 
            // btnDescargarFE
            // 
            this.btnDescargarFE.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDescargarFE.Image = global::Gestionador_de_CFDIS.Properties.Resources.baseline_arrow_circle_down_black_18dp;
            this.btnDescargarFE.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDescargarFE.Location = new System.Drawing.Point(12, 12);
            this.btnDescargarFE.Name = "btnDescargarFE";
            this.btnDescargarFE.Size = new System.Drawing.Size(152, 40);
            this.btnDescargarFE.TabIndex = 0;
            this.btnDescargarFE.Text = "     Descargar xml";
            this.btnDescargarFE.UseVisualStyleBackColor = true;
            this.btnDescargarFE.Click += new System.EventHandler(this.btnDescargarFE_Click);
            // 
            // btnTimbrar
            // 
            this.btnTimbrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTimbrar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTimbrar.Location = new System.Drawing.Point(415, 12);
            this.btnTimbrar.Name = "btnTimbrar";
            this.btnTimbrar.Size = new System.Drawing.Size(152, 40);
            this.btnTimbrar.TabIndex = 27;
            this.btnTimbrar.Text = "Timbrar CFDI";
            this.btnTimbrar.UseVisualStyleBackColor = true;
            this.btnTimbrar.Click += new System.EventHandler(this.btnTimbrar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label1.Location = new System.Drawing.Point(602, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 28;
            this.label1.Text = "Disponible :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label2.Location = new System.Drawing.Point(602, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 29;
            this.label2.Text = "Total :";
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblTotal.Location = new System.Drawing.Point(688, 29);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(13, 13);
            this.lblTotal.TabIndex = 33;
            this.lblTotal.Text = "0";
            // 
            // lblDisponible
            // 
            this.lblDisponible.AutoSize = true;
            this.lblDisponible.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblDisponible.Location = new System.Drawing.Point(688, 9);
            this.lblDisponible.Name = "lblDisponible";
            this.lblDisponible.Size = new System.Drawing.Size(13, 13);
            this.lblDisponible.TabIndex = 32;
            this.lblDisponible.Text = "0";
            // 
            // lblUsada
            // 
            this.lblUsada.AutoSize = true;
            this.lblUsada.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblUsada.Location = new System.Drawing.Point(688, 48);
            this.lblUsada.Name = "lblUsada";
            this.lblUsada.Size = new System.Drawing.Size(13, 13);
            this.lblUsada.TabIndex = 39;
            this.lblUsada.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label6.Location = new System.Drawing.Point(602, 48);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 38;
            this.label6.Text = "Usada :";
            // 
            // FormCFDI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.ClientSize = new System.Drawing.Size(899, 564);
            this.Controls.Add(this.lblUsada);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.lblDisponible);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnTimbrar);
            this.Controls.Add(this.memoResultado);
            this.Controls.Add(this.btnSubirFE);
            this.Controls.Add(this.btnDescargarFE);
            this.MinimumSize = new System.Drawing.Size(641, 465);
            this.Name = "FormCFDI";
            this.ShowIcon = false;
            this.Text = "FormCFDI";
            this.Load += new System.EventHandler(this.FormCFDI_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSubirFE;
        private System.Windows.Forms.RichTextBox memoResultado;
        public System.Windows.Forms.Button btnDescargarFE;
        private System.Windows.Forms.Timer timer1;
        public System.Windows.Forms.NotifyIcon trayBar;
        private System.Windows.Forms.Button btnTimbrar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label lblDisponible;
        private System.Windows.Forms.Label lblUsada;
        private System.Windows.Forms.Label label6;
    }
}