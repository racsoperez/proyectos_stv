﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestionador_de_CFDIS
{
    public class CFDI_PROBLEMA
    {
        public int s_id { get; set; }
        public int E_tipoinstruccion { get; set; }
        public string cfdi { get; set; }
        public int E_error_return { get; set; }
        public string s_error { get; set; }

        public CFDI_PROBLEMA()
        {
            
            s_id = 0;
            E_tipoinstruccion = 0;
            cfdi = "";
            E_error_return = 0;
            s_error = "";
        }
    }

   //public class RFC_PROCESAR
   // {
        //public int CuentaId { get; set; }
        //public int EmpresaId { get; set; }
        //public string Rfc { get; set; }
        //public string RazonSocial { get; set; }
        //public string ClaveCiec { get; set; }
        //public string Pagos { get; set; }
        //public DateTime LastFechaTimbrado { get; set; }

        //public RFC_PROCESAR()
        //{
        //    CuentaId = 0;
        //    EmpresaId = 0;
        //    Rfc = "";
        //    RazonSocial = "";
        //    ClaveCiec = "";
        //    Pagos = "";
        //    LastFechaTimbrado = DateTime.MinValue;
        //}
    //}



}
