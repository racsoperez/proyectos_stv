﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gestionador_de_CFDIS
{
   public class ERRORES_WS
    {
       public string Errores(int s_msgError)
       {
           string mensaje = "";
           if (s_msgError == 0)
           {
               //MessageBox.Show("Webservice ejecutado sin errores", "Mensaje",MessageBoxButtons.OK, MessageBoxIcon.Information);
               mensaje = "";
           }
           else if (s_msgError == 1)
           {
               //MessageBox.Show("Se realizó la operación pero existen mensajes de warning", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
               mensaje = s_msgError + " : Se realizó la operación pero existen mensajes de warning ";
           }
           else if (s_msgError == 10)
           {
               //MessageBox.Show("Webservice require información de login actualizada", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
               mensaje = s_msgError + " : Webservice require información de login actualizada ";
           }
           else if (s_msgError == 11)
           {
               //MessageBox.Show("Webservice require permisos que el usuario no tiene", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
               mensaje = s_msgError + " : Webservice require permisos que el usuario no tiene ";
           }
           else if (s_msgError == 12)
           {
               //MessageBox.Show("Webservice contiene información inválida", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
               mensaje = s_msgError + " : Webservice contiene información inválida ";
           }
           else if (s_msgError == 13)
           {
               //MessageBox.Show("Al ejecutar el webservice se encontraron condiciones incorrectas y no se pudo procesar", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
               mensaje = s_msgError + " : Al ejecutar el webservice se encontraron condiciones incorrectas y no se pudo procesar ";
           }
           else if (s_msgError == 14)
           {
               //MessageBox.Show("La ejecución del webservice require una secuencia que no se esta respetando", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
               mensaje = s_msgError + " : La ejecución del webservice require una secuencia que no se esta respetando ";
           }
           else if (s_msgError == 15)
           {
               //MessageBox.Show("No se encontro error alguno, sin embargo el request no se pudo ejecutar. El request debe repetirse", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
               mensaje = s_msgError + " : No se encontro error alguno, sin embargo el request no se pudo ejecutar. El request debe repetirse ";
           }
           else if (s_msgError == 16)
           {
              //MessageBox.Show("La información contenida en el webservice se basa en información que fue actualizada", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
               mensaje = s_msgError + " : La información contenida en el webservice se basa en información que fue actualizada ";
           }
           else if (s_msgError == 17)
           {
               //MessageBox.Show("Se encontró algún error no detallado", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
               mensaje = s_msgError + " : Se encontró algún error no detallado ";
           }
           else if (s_msgError == 18)
           {
               //MessageBox.Show("Error de librería o conexión de internet", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
               mensaje = s_msgError + " : Error de librería o conexión de internet ";
           }

           return mensaje;
       }
      
    }
}
