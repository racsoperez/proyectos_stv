﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;
using System.Globalization;
//para validar email
using System.Text.RegularExpressions;
using CodigoUnico;
using IniParser.Model;
using IniParser;

namespace Gestionador_de_CFDIS
{
    public partial class FormConfiguracion : Form
    {
        CodigoPC finger = new CodigoPC();

        int seg;
        int min;
        int hor;

        public FormConfiguracion()
        {
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if ((txtUsuario.Text == "") || (txtContrasena.Text == "") || (txtUrl.Text == ""))
            {
                MESSAGEBOX_TEMPORAL.Show("No puedes continuar sin terminar el formulario!!", "Aviso", 10, true);
            }
            else
            {
                if(validarEmail(txtUsuario.Text))
                {
                    //Encripta la contraseña
                    txtContrasena.Text = STV_FWK_CRYPTO.Encrypt(txtUsuario.Text,1,txtContrasena.Text);

                    //sirve para reemplazar los guiones(-).
                    txtContrasena.Text = txtContrasena.Text.Replace("-", "");

                    CAPTURAR_CONFIGURACIONES_INI.GuardarConfiguracionINI(txtUsuario.Text, txtContrasena.Text, txtUrl.Text);
                    txtUrl.Text = "";
                    txtUsuario.Text = "";
                    txtContrasena.Text = "";   
                }
                else
                {
                    MESSAGEBOX_TEMPORAL.Show("Escribe un email válido", "Aviso", 10, true);
                }
            }
        }

        private void FormConfiguracion_Load(object sender, EventArgs e)
        {
           txtUrl.Select();
                                         
           txtCodigo.Text = finger.Value(CodigoPC.fingerPrint);

           var parser = new FileIniDataParser();
           IniData _D_data = parser.ReadFile("config.ini");

           KeyDataCollection keyCol = _D_data["gestionador_configuracion"];

           txtUrl.Text = keyCol["url"];
           txtUsuario.Text = keyCol["usuario"];
           txtContrasena.Text = keyCol["pass"];

           txtContrasena.Text = STV_FWK_CRYPTO.Decrypt(txtUsuario.Text, 1, txtContrasena.Text);

           min = 0;
           seg = 15;
           hor = 0;
  
           timer1.Start();
        }

        private void FormConfiguracion_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private bool validarEmail(string email)
        {
            String expresion;
            expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (Regex.IsMatch(email, expresion))
            {
                if (Regex.Replace(email, expresion, String.Empty).Length == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
           
        private void timer1_Tick(object sender, EventArgs e)
        {
           
            string minutos = min.ToString();
            string horas = hor.ToString();
            string segundos = seg.ToString();

            if (hor < 10) { horas = "0" + hor.ToString(); }
            if (min < 10) { minutos = "0" + min.ToString(); }
            if (seg < 10) { segundos = "0" + seg.ToString(); }

            if(seg == 0 && min > 0)
            {
                min -= 1;
                seg = 60;
            }
            if(min == 0 && hor > 0 && seg == 0)
            {
                seg = 60;
                hor -= 1;
                min = 59;
            }
            if(seg <= 10)
            {
                if (lblTimer.Visible == true)
                {
                    lblTimer.Visible = false;
                    lblTimer.ForeColor = Color.Red;
                }
                else
                {
                    lblTimer.Visible = true;
                }
            }
            if(min == 0 && hor == 0 && seg ==0)
            {
                timer1.Stop();
                btnGuardar.PerformClick();
            }
            
            lblTimer.Text = horas + ":" + minutos + ":" + segundos;
            seg -= 1;
        }
    }
}
