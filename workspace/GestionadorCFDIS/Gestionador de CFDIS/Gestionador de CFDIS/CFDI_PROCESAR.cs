﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestionador_de_CFDIS
{
    public class CFDI_PROCESAR
    {
        public string json { get; set; }
        public string xml { get; set; }
        public string rutaArchivo { get; set; }

        public CFDI_PROCESAR()
        {
            json = "";
            xml = "";
            rutaArchivo = "";
        }
    }

    //public class Pagos
    //{
    //    public List<string> periodo { get; set; }
    //}

    //public class Dato
    //{
    //    public string claveciec { get; set; }
    //    public string rfc { get; set; }
    //    public DateTime lastfechatimbrado { get; set; }
    //    public int empresa_id { get; set; }
    //    public string razonsocial { get; set; }
    //    public Pagos pagos { get; set; }
    //}

    //public class Process
    //{
    //    public List<Dato> datos { get; set; }
    //    public string atributos { get; set; }
    //}
}
