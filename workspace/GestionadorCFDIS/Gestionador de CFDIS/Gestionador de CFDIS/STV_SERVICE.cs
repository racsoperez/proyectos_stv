﻿//librerias para los archivos .ini
using IniParser;
using IniParser.Model;
using Newtonsoft.Json;
using NLog;
using StvSoft;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using CodigoUnico;

namespace Gestionador_de_CFDIS
{
    public class STV_SERVICE
    {
        //instancia de fingerPrint para sacar el código único por computadora
         CodigoPC finger = new CodigoPC();


        public static Logger logger = LogManager.GetCurrentClassLogger();
        ERRORES_WS obtenerError = new ERRORES_WS();

        public string strDirectorioBase = "C:\\descarga_xml";

        public Acceso.loginResponseD_auth loginAuth = null;
        public Acceso.logoutD_auth logoutAuth = null;
        public Acceso.verifyD_auth verifyAuth = null;

        List<CUENTAS> lstInstrucion = new List<CUENTAS>();

        string _ultimo_cfdi = string.Empty;
        string _ultima_subida_exitosa = string.Empty;
        string _ultimo_problema_fechahora = string.Empty;
        int _n_return = 0;
        FormCFDI frm;

        public bool ConectarWS()
        {
            try
            {
                lstInstrucion = new List<CUENTAS>();
                var client = new Acceso.STV_AccesoPortTypeClient();

                #region Cambiar la url de acceso

                var parser = new FileIniDataParser();
                IniData _D_data = parser.ReadFile("config.ini");

                KeyDataCollection keyCol = _D_data["gestionador_configuracion"];
                
                string url = keyCol["url"];

                if(url == "" || url == null)
                {
                    MESSAGEBOX_TEMPORAL.Show("Tienes datos inválidos! ", "Aviso", 5, true);
                    return false;
                }
                else
                {
                    client.Endpoint.Address = new System.ServiceModel.EndpointAddress(url);
                }
                
                #endregion

                string s_codigo = finger.Value(CodigoPC.fingerPrint);
                string json_params = "";
                string json_data = "";
                string s_msgError = "";

                //se asigna el usuario y contraseña del .ini
                string _strUser = keyCol["usuario"];
                string _strPassword = keyCol["pass"];

                _strPassword = STV_FWK_CRYPTO.Decrypt(_strUser, 1, _strPassword);

                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                int return_login = client.login(s_codigo, json_params, _strUser, _strPassword, out loginAuth, out json_data, out s_msgError);

                string _mensaje = obtenerError.Errores(return_login);

                if (return_login > 0)
                {
                    logger.Error(string.Format("Conectar WebService - " + "Error: {0}", s_msgError));
                    CAPTURAR_CONFIGURACIONES_INI.AgregarProblemasConfiguracion(_mensaje + s_msgError, return_login.ToString(), DateTime.Now.ToString());
                    return false;
                }
                else
                {
                    dynamic json = JsonConvert.DeserializeObject(json_data);

                        foreach (var json_instrucciones in json.L_instrucciones)
                        {
                            //MessageBox.Show("" + jsonCuenta);
                            CUENTAS cuenta = new CUENTAS()
                            {
                                E_tipoinstruccion = json_instrucciones.E_tipoinstruccion,
                                ClaveCiec = json_instrucciones.s_param3,
                                Rfc = json_instrucciones.s_param2,
                                EmpresaId = json_instrucciones.s_param1,
                                CuentaId = json_instrucciones.s_param0,
                                Rfc_integrador = json_instrucciones.s_param7,
                                Id_integrador = json_instrucciones.s_param6,
                                Fecha_hasta = json_instrucciones.dt_param5 == null ? new DateTime(2000, 1, 1, 0, 0, 0) : Convert.ToDateTime(json_instrucciones.dt_param5),
                                Fecha_desde = json_instrucciones.dt_param4 == null ? new DateTime(2000, 1, 1, 0, 0, 0) : Convert.ToDateTime(json_instrucciones.dt_param4),
                                s_id = json_instrucciones.s_id,
                            };
                        lstInstrucion.Add(cuenta);
                        }
                    
                    return true;
                }    
            }
            catch (Exception ex)
            {
                logger.Error("Conectar WebService - " + string.Format("{0}", ex.Message));
                MESSAGEBOX_TEMPORAL.Show(ex.Message, "Aviso", 5, true);
                return false;
            }
        }//Termina conectarWS

        private void DesconectarWS()
        {
            if (loginAuth == null) return;

            string s_msgError = string.Empty;
            string json_params = string.Empty;
            string json_data = string.Empty;
            var client = new Acceso.STV_AccesoPortTypeClient();

            logoutAuth = new Acceso.logoutD_auth()
            {
                json_key = loginAuth.json_key,
                s_signature = loginAuth.s_signature,
                s_ver = loginAuth.s_ver,
                s_token = loginAuth.s_token,
                n_empresa_id = loginAuth.n_empresa_id,
                
            };

            client.logout(json_params, logoutAuth, out json_data, out s_msgError);

            if (s_msgError == string.Empty)
            {
                loginAuth = null;
            }
        }//Termina desconetarWS

        private bool SubirCFDIWs(CFDI_PROCESAR cfdi, CUENTAS rfc, out string strMessage, out int E_return)
        {
            try
            {
                string s_msgError = string.Empty;
                string json_data = string.Empty;
                string json_params = string.Empty;
                
               var subirAuth = new Acceso.subir_cfdiD_auth()
                {
                    json_key = loginAuth.json_key,
                    s_signature = loginAuth.s_signature,
                    s_ver = loginAuth.s_ver,
                    s_token = loginAuth.s_token,
                    n_empresa_id = loginAuth.n_empresa_id,
                };

                subirAuth.n_empresa_id = rfc.EmpresaId;

                var client = new Acceso.STV_AccesoPortTypeClient();

                int return_subir_cfdi = client.subir_cfdi(cfdi.xml, subirAuth, rfc.EmpresaId.ToString(), rfc.s_id.ToString(),rfc.Rfc,json_params, out json_data, out s_msgError);

                //string _mensaje = obtenerError.Errores(return_subir_cfdi);

                if (return_subir_cfdi > 0)
                {
                    //variable usada para saber si fue subido el xml a la nube.
                    bool _subir_cfdi;
                    E_return = return_subir_cfdi;
                    strMessage = s_msgError;

                    if (return_subir_cfdi == 16)//"UUID ya fue ingresado para la empresa"
                    {
                        logger.Info(string.Format("Subir cfdis WebService - " + "El archivo no fue cargado al portal: {0}. Info: {1}", cfdi.rutaArchivo, return_subir_cfdi.ToString() + strMessage));
                        _subir_cfdi = false;
                    }
                    else if(return_subir_cfdi == 1)//Se realizó la operación pero existen mensajes de warning
                    {
                        logger.Info(string.Format("Subir cfdis WebService - " + "El archivo no fue cargado al portal: {0}. Info: {1}", cfdi.rutaArchivo, return_subir_cfdi.ToString() + strMessage));
                        _subir_cfdi = true;
                    }
                    else //Error
                    { 
                        logger.Error(string.Format("Subir cfdis WebService - " + "El archivo no fue cargado al portal: {0}. Error: {1}", cfdi.rutaArchivo, return_subir_cfdi.ToString() + strMessage));
                        _subir_cfdi = false;
                    }

                    return _subir_cfdi;
                }
            }
            catch (Exception ex)
            {
                strMessage = string.Empty;
                E_return = 0;
                //builder_info.Append("Subir cfdis WebService - " + "El archivo no fue cargado al portal: " + cfdi.rutaArchivo + " Error: " + strMessage);
                logger.Error(string.Format("Subir cfdis WebService - " + "El archivo no fue cargado al portal: {0}. Error: {1}", cfdi.rutaArchivo, ex.Message));
                return false;
            }

            strMessage = string.Empty;
            E_return = 0;

            logger.Info("Subir cfdis WebService - " + "El archivo fue cargado al portal : " + cfdi.rutaArchivo);
            //builder_info.Append("Subir cfdis WebService - " + "El archivo fue cargado al portal: " + cfdi.rutaArchivo);
            
            //asiganamos el último xml subido
            _ultimo_cfdi = System.IO.Path.GetFileName(cfdi.rutaArchivo);

            //asignamos la fecha del último xml subido
            _ultima_subida_exitosa = DateTime.Now.ToString();

            string strRfc = rfc.Rfc.Replace("-", "");

            //se guarda en el ini la fecha de la última subida exitosa
            CAPTURAR_CONFIGURACIONES_INI.GuardarSubidaExitosaConfig(_ultima_subida_exitosa, _ultimo_cfdi + " - RFC: " + strRfc);

            //se guarda en el ini la fecha de la última subida exitosa por rfc
            CAPTURAR_CONFIGURACIONES_INI.GuardarSubidaExitosaRFC(rfc.CuentaId, strRfc, _ultimo_cfdi + " -- " + _ultima_subida_exitosa);

            return true;
        }//Termina subir subirCFDIWs

        public string TimbrarCFDI(CUENTAS instruccion)
        {
            logger.Info("TimbrarCFDI: Inicia el proceso de timbrado de cfdi.");
           
            bool _timbre_generado = false;
            string _resultado_texto = string.Empty;

            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            WebClient webClient = new WebClient();

            frm = new FormCFDI();
            try
            {
               string _strRfc = instruccion.Rfc.Replace("-", "");

                frm.Notificacion("Timbrar xml del RFC: ", _strRfc);
            
                string _url = "http://p01test.stverticales.mx" + instruccion.Id_integrador + "&s_token=" + loginAuth.s_token;

                if (ExisteURL(_url))
                {
                    webClient.DownloadFile(_url, strDirectorioBase + "\\json2.json");

                    OPCIONES_CFDI timbrado = new OPCIONES_CFDI(_strRfc, instruccion.CuentaId, strDirectorioBase);
                    //TIMBRADO_CFDI timbrado = new TIMBRADO_CFDI("ALG040428RG4", 3, strDirectorioBase);
                    _timbre_generado = timbrado.TimbrarCfdi(out _resultado_texto);
                }
            }
            catch (Exception ex)
            {
                logger.Error("TimbrarCFDI: " + ex.Message);
            }

            //ResponderInstruccion(instruccion,"","");
         
            return _resultado_texto;
        }//Termina TimbrarCFDI

        private string CancelarCFDI(CUENTAS instruccion)
        {
            logger.Info("CancelarCFDI: Inicia el proceso de cancelación de cfdi.");
            string _resultado_texto = string.Empty;

            frm = new FormCFDI();
            frm.Notificacion("Cancelar xml del RFC: ", instruccion.Rfc);

            try
            {
                OPCIONES_CFDI cancelar = new OPCIONES_CFDI(instruccion.Rfc, instruccion.CuentaId, strDirectorioBase);
                cancelar.CancelarMultiplesCfdi(out _resultado_texto);
            }
            catch (Exception ex)
            {
                logger.Error("CancelarCFDI: " + ex.Message);
                
            }

           //ResponderInstruccion(instruccion,"","");

            return _resultado_texto;
        }//Termina CancelarCFDI

        public string DescargarCFDI(CUENTAS instruccion)
        {
            logger.Info("DescargarCFDI: Inicia el proceso de descarga de cfdi.");
            string strResultado = string.Empty;

            string strRfc = instruccion.Rfc.Replace("-", "");
            string strCiec = STV_FWK_CRYPTO.Decrypt(instruccion.Rfc, 1, instruccion.ClaveCiec);

            //DateTime fecha1 = new DateTime(2021, 04, 01);
            //DateTime fecha2 = new DateTime(2021, 05, 10);

            string strRfcIntegrador = instruccion.Rfc_integrador;
            string strIdIntegrador = instruccion.Id_integrador;
            DateTime dt_desde =  instruccion.Fecha_desde;
            DateTime dt_hasta = instruccion.Fecha_hasta;
            int Cuenta_id = instruccion.CuentaId;
            int s_id = instruccion.s_id;
            int E_tipoinstruccion = instruccion.E_tipoinstruccion;

            //se agrega la sección con el rfc
            CAPTURAR_CONFIGURACIONES_INI.CrearSeccionRFC(Cuenta_id, strRfc);

            //STV_CONFIGURACIONES_INI.guardar_total_subidos_RFC(Cuenta_id,strRfc,0,DateTime.Now.ToString());
            #region se crean las carpetas
                //Se crea la carpeta para guardar la licencia
                string rfc_licencia = string.Format("{0}\\{1}\\{2}", strDirectorioBase, Cuenta_id + "_" + strRfc, "Licencia");
                if (!Directory.Exists(rfc_licencia))
                {
                    Directory.CreateDirectory(rfc_licencia);
                }
                //Se crea la carpeta para guardar CFDIs descargados
                string strCFDIS_descargados = string.Format("{0}\\{1}\\{2}", strDirectorioBase, instruccion.CuentaId + "_" + strRfc, "CFDIs_descargados");
                if (!Directory.Exists(strCFDIS_descargados))
                {
                    Directory.CreateDirectory(strCFDIS_descargados);
                }
                //Se crea la carpeta para guardar CFDIs subidos
                string strCFDIS_subidos = string.Format("{0}\\{1}\\{2}", strDirectorioBase, instruccion.CuentaId + "_" + strRfc, "CFDIs_subidos");
                if (!Directory.Exists(strCFDIS_subidos))
                {
                    Directory.CreateDirectory(strCFDIS_subidos);
                }
                //Se crea la carpeta para guardar CFDIs pendientes
                string strCFDIS_pendientes = string.Format("{0}\\{1}\\{2}", strDirectorioBase, instruccion.CuentaId + "_" + strRfc, "CFDIs_pendientes");
                if (!Directory.Exists(strCFDIS_pendientes))
                {
                    Directory.CreateDirectory(strCFDIS_pendientes);
                }
                #endregion

            string _n_return = string.Empty;
            string _s_msgError = string.Empty;
                
            //verificar si existe licencia para el rfc
            string[] files = Directory.GetFiles(rfc_licencia, "*.license");

            if (files.Length > 0)
                {
                    //existe licencia
                    try
                    {
                        //Se muestra la notificación de descarga de xml.
                        frm = new FormCFDI();

                        string _info = "";
                        if(E_tipoinstruccion == 10)
                        {
                            _info = "Descarga de xml vigentes del RFC: ";
                        }
                        else
                        {
                            _info = "Descarga de xml cancelados del RFC: ";
                        }

                        frm.Notificacion(_info, strRfc);

                        DESCARGA_CFDI descargaEmitidos = new DESCARGA_CFDI(strRfcIntegrador, strIdIntegrador, strRfc, strCiec, dt_desde, dt_hasta, strDirectorioBase, Cuenta_id, E_tipoinstruccion);
                        strResultado =  descargaEmitidos.Descargar(ref _n_return, ref _s_msgError);

                         logger.Info(strResultado);

                        //Si no hay error empieza a subir xml
                        if ((_s_msgError == string.Empty) || (_s_msgError == "NONE: "))
                         {
                             SubirArchivosXML(instruccion);
                           
                             if (MandarInstruccion(Cuenta_id + "_" + strRfc))
                             {
                                 _s_msgError = "OK";
                                 _n_return = "0";
                             }
                         }
                         ResponderInstruccion(instruccion, _s_msgError, _n_return);
                    }
                    catch (Exception ex) 
                    {
                        logger.Error("Descarga cfdi -- " + ex.ToString());
                    }
                }
            else
                {
                    string _message_error = "NOK_ERROR_LICENCIA, no se pudo encontrar la licencia para el RFC: " + strRfc;
                    logger.Error("Error 11017: " + _message_error);
                    CAPTURAR_CONFIGURACIONES_INI.AgregarProblemasConfiguracion(_message_error, "11017", DateTime.Now.ToString());
                    CAPTURAR_CONFIGURACIONES_INI.GuardarProblemasPorRFC(instruccion.CuentaId, strRfc, _message_error, "11017", DateTime.Now.ToString());
                    ResponderInstruccion(instruccion, _s_msgError, "11017");
                }

            return strResultado;

        }//Termina descargarCFDI

        private bool MandarInstruccion(string _rfc)
    {
             List<CFDI_PROCESAR> lstJson = new List<CFDI_PROCESAR>();

             string strPath = string.Format("{0}\\{1}\\{2}", strDirectorioBase, _rfc, "CFDIs_descargados");

              string[] _sub_carpetas = Directory.GetDirectories(strPath);

              foreach (var archivos in _sub_carpetas)
              {
                  DirectoryInfo d = new DirectoryInfo(archivos);
                  FileInfo[] Files = d.GetFiles("*.xml");

                  if (Files.Length > 0)
                  {
                      return false;
                  }
              }
              return true;
    }//termina MandarInstrucción

        private List<CFDI_PROCESAR> LeerArchivosXml(string strDirectorioBase, string strRfc, out bool bPendientes)
    {
            string path_XML = "";
            List<CFDI_PROCESAR> lstJson = new List<CFDI_PROCESAR>();
           
            path_XML = string.Format("{0}\\{1}\\{2}", strDirectorioBase, strRfc, "CFDIs_descargados");
            //path_XML = string.Format("{0}\\{1}\\{2}", strDirectorioBase, strRfc, "CFDIs_pendientes");

            string[] _sub_carpetas = Directory.GetDirectories(path_XML);

              foreach (var archivos in _sub_carpetas)
              {
                  DirectoryInfo d = new DirectoryInfo(archivos);
                  FileInfo[] Files = d.GetFiles("*.xml");

                  if (Files.Length == 0)
                  {
                      logger.Info("Leer archivos xml - " + "No hay ningún archivo .xml en la carpeta: " + archivos);
                      //si la carpeta esta vacía se elimina
                      Directory.Delete(archivos, true);
                  }

                  int nCount = 0;

                  foreach (FileInfo file in Files)
                  {
                      try
                      {
                          StvSoft.STVCFDI item = StvSoft.UTIL.CargarXMLFactura(file.FullName);

                          string output = JsonConvert.SerializeObject(item);
                          var cfdi = new CFDI_PROCESAR()
                          {
                              json = output.Replace("basei", "base"),
                              xml = System.IO.File.ReadAllText(file.FullName, Encoding.UTF8),
                              rutaArchivo = file.FullName,
                          };

                          lstJson.Add(cfdi);

                          if (nCount > 5000)
                          {
                              bPendientes = true;
                              return lstJson;
                          }

                          nCount++;
                      }
                      catch (Exception ex)
                      {
                          logger.Error("Leer archivos xml - Error :" + ex.ToString());
                      }
                  }
              }
          
          bPendientes = false;
          return lstJson;
    }//Termina leerArchivosXml
    
        private void SubirArchivosXML(CUENTAS instruccion)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                    string strRfc = instruccion.Rfc.Replace("-", "");
                    int Cuenta_id = instruccion.CuentaId;

                    logger.Info("Subir archivos xml -- " + "Inicia el proceso de subida de XML del RFC: " + strRfc);

                    //if (!Directory.Exists(strDirectorioBase + "\\" + Cuenta_id + "_" + strRfc))
                    //{
                    //    continue;
                    //}

                    //if (ConectarWS())
                    //{
                    start:
                        string strMessage = string.Empty;
                        int E_return = 0;
                        bool bPendientes = false;
                        foreach (CFDI_PROCESAR cfdi in LeerArchivosXml(strDirectorioBase, Cuenta_id + "_" + strRfc, out bPendientes))
                        {
                            if (SubirCFDIWs(cfdi, instruccion, out strMessage, out E_return))
                            {
                                //mover a carpeta de CFDIs subidos
                                if (File.Exists(cfdi.rutaArchivo))
                                {
                                    try
                                    {
                                        string carpeta_fecha = Path.GetFileName(Path.GetDirectoryName(cfdi.rutaArchivo));
                                        //string nombre_cfdi = Path.GetFileName(cfdi.rutaArchivo);

                                        string strFecha = string.Format("{0}\\{1}\\{2}\\{3}", strDirectorioBase, Cuenta_id + "_" + strRfc, "CFDIs_subidos", carpeta_fecha);
                                        if (!Directory.Exists(strFecha))
                                        {
                                            Directory.CreateDirectory(strFecha);
                                        }

                                        File.Move(cfdi.rutaArchivo, strFecha + "\\" + Path.GetFileName(cfdi.rutaArchivo));
                                        File.Delete(cfdi.rutaArchivo);

                                    }
                                    catch (Exception ex)
                                    {
                                        logger.Error(string.Format("{0}", ex.ToString()));
                                    }

                                }
                            }
                            else
                            {
                                if (strMessage != string.Empty)
                                {
                                    #region mover a pendientes
                                    string carpeta_fecha = Path.GetFileName(Path.GetDirectoryName(cfdi.rutaArchivo));

                                    string strPendientesFecha = string.Format("{0}\\{1}\\{2}\\{3}", strDirectorioBase, Cuenta_id + "_" + strRfc, "CFDIs_pendientes", carpeta_fecha);
                                    if (!Directory.Exists(strPendientesFecha))
                                    {
                                        Directory.CreateDirectory(strPendientesFecha);
                                    }

                                    _ultimo_cfdi = Path.GetFileName(cfdi.rutaArchivo);

                                    string _ruta = strPendientesFecha + "\\" + _ultimo_cfdi;

                                    //se agrega la sección con el rfc
                                    CAPTURAR_CONFIGURACIONES_INI.CrearSeccionRFC(Cuenta_id, strRfc);

                                    if (E_return == 16)
                                    {
                                        //Si el xml ya fue subido se elimina
                                        File.Delete(cfdi.rutaArchivo);
                                    }
                                    else
                                    {
                                        if (File.Exists(_ruta))
                                        {
                                            //Si el xml ya fue movido a pendientes se elimina
                                            File.Delete(cfdi.rutaArchivo);
                                        }
                                        else
                                        {
                                            //Se mueve el xml a la carpeta de pendientes
                                            File.Move(cfdi.rutaArchivo, strPendientesFecha + "\\" + _ultimo_cfdi);
                                            _ultimo_problema_fechahora = DateTime.Now.ToString();

                                            //se guardan los problemas generados al subir cada .xml                    
                                            CAPTURAR_CONFIGURACIONES_INI.AgregarDatosRfcConProblemas(Cuenta_id, strRfc, instruccion.s_id,instruccion.E_tipoinstruccion, _ultimo_cfdi, _n_return, strMessage);

                                            CAPTURAR_CONFIGURACIONES_INI.AgregarProblemasConfiguracion(strMessage, _n_return.ToString(), _ultimo_problema_fechahora);

                                            CAPTURAR_CONFIGURACIONES_INI.GuardarProblemasPorRFC(Cuenta_id, strRfc, strMessage, _n_return.ToString(), _ultimo_problema_fechahora);
                                        }
                                    }
                                    #endregion
                                }

                            }
                        }
                        if (bPendientes) goto start;
                        //desconectarWS();
                        logger.Info("Subir archivos xml - " + "Fin de subida de XML del RFC: " + strRfc);
                    //}
            }
            catch (Exception ex)
            {
                logger.Error("Subir archivos xml - Error: " + ex.ToString());
            }
            finally
            {
                Cursor.Current = Cursors.Default;    
            }
        }//Termina subirArchivosXML

        private void BorrarCarpetas(CUENTAS instruccion)
        {
            string strRfc = instruccion.Rfc.Replace("-", "");

            //Es el nombre de la carpeta a borrar (CFDIs_descargados, CFDIs_subidos).
            string s_param3 = instruccion.ClaveCiec;

            string _strBorrarRFC = string.Format("{0}\\{1}", strDirectorioBase, instruccion.CuentaId + "_" + strRfc);
            string _strBorrarCarpeta = string.Format("{0}\\{1}\\{2}", strDirectorioBase, instruccion.CuentaId + "_" + strRfc, s_param3);

            try
            {
                 if(s_param3 == "todas")
                 {
                     Directory.Delete(_strBorrarRFC, true);
                 }
                 else
                 {
                     Directory.Delete(_strBorrarCarpeta, true); 
                 }
            }
            catch(Exception ex)
            {
                logger.Error("Borrar carpetas :" + ex.Message);
            }

            //ResponderInstruccion(instruccion,"","");

        }//Fin borrarCarpetas
      
        private void ActualizarLicencia(CUENTAS instruccion)
        {
            string _s_mensage = "";
            string _E_return = "";

          string strRfc = instruccion.Rfc.Replace("-", "");

          //Crear carpeta del rfc
          string strCarpetaRFC = string.Format("{0}\\{1}", strDirectorioBase, instruccion.CuentaId + "_" + strRfc);
          if (!Directory.Exists(strCarpetaRFC))
          {
              Directory.CreateDirectory(strCarpetaRFC);
          }
          //Crear carpeta para la licencia
          string strCarpetaRFCLicencia = string.Format("{0}\\{1}\\{2}", strDirectorioBase, instruccion.CuentaId + "_" + strRfc, "Licencia");
          if (!Directory.Exists(strCarpetaRFCLicencia))
          {
              Directory.CreateDirectory(strCarpetaRFCLicencia);
          }

            //Descarga la licencia
            WebClient webClient = new WebClient();
            try
            {
                //la clave ciec es el s_param3 donde viene la url para descargar la licencia.  
                webClient.DownloadFile(instruccion.ClaveCiec, strCarpetaRFCLicencia + "\\archivo.license");
                _s_mensage = "OK";
                _E_return = "0";
            }
            catch (Exception ex)
            {
                _s_mensage = "NOK_ERROR_LICENCIA, no se pudo descargar la licencia para el RFC: " + strRfc;
                _E_return = "11017";
                logger.Error(_s_mensage, ex.Message);
                CAPTURAR_CONFIGURACIONES_INI.AgregarProblemasConfiguracion(_s_mensage, _E_return, DateTime.Now.ToString());
                CAPTURAR_CONFIGURACIONES_INI.GuardarProblemasPorRFC(instruccion.CuentaId, strRfc, _s_mensage, _E_return, DateTime.Now.ToString());
            }

            DirectoryInfo dir = new DirectoryInfo(strCarpetaRFCLicencia);
            FileInfo[] infoFicheros = dir.GetFiles("*.license");

            List<string> listaFechas = new List<string>();
            string _nombreArchivo = "";

            //obtenemos la fecha de creación de los archivos y la agregamos a una lista
            for (int i = 0; i <= (infoFicheros.Length) - 1; i++)
            {
                listaFechas.Add(infoFicheros[i].CreationTime.ToString("yyyy-MM-dd"));
            }

            string _fecha_mayor = listaFechas.Max();

            foreach (FileInfo Ficheros in infoFicheros)
            {
                string _fecha_archivo = Ficheros.CreationTime.ToString("yyyy-MM-dd");

                if (_fecha_mayor == _fecha_archivo)
                {
                    _nombreArchivo = _fecha_archivo.Replace("-", "_");
                    _nombreArchivo = _nombreArchivo + "_licencia.license";

                    if (!File.Exists(dir + "\\" + _nombreArchivo))
                    {
                        File.Move(dir + "\\" + Ficheros, dir + "\\" + _nombreArchivo);
                    }
                    break;
                }
            }

            ResponderInstruccion(instruccion,_s_mensage,_E_return);

        }//Termina ActualizarLicencia

        private void ResponderInstruccion(CUENTAS rfc, string instruccion_s_msgError, string instruccion_E_return)
        {
            try
            {
                string json_data = string.Empty;
                string json_params = string.Empty;
                string s_msgError = string.Empty;

                var responderAuth = new Acceso.responder_instruccionD_auth()
                {
                    json_key = loginAuth.json_key,
                    s_signature = loginAuth.s_signature,
                    s_ver = loginAuth.s_ver,
                    s_token = loginAuth.s_token,
                    n_empresa_id = loginAuth.n_empresa_id,
                };


                //instruccion_s_return = es el número de error 
                responderAuth.n_empresa_id = rfc.EmpresaId;

                var client = new Acceso.STV_AccesoPortTypeClient();

                int responder = client.responder_instruccion(rfc.s_id.ToString(), instruccion_E_return, instruccion_s_msgError, json_params, responderAuth, out json_data, out s_msgError);

                //string _mensaje = obtenerError.Errores(responder);

                if (responder > 0)
                {
                    logger.Error(string.Format("Responder instrucción - " + "Error: {0}", s_msgError));
                }
            }
            catch (Exception ex)
            {
                instruccion_s_msgError = "";
                logger.Error(string.Format("Responder instrucción - " + "Error: {0}", ex.Message));
            }
        }//Termina ResponderInstruccion

        private int TotalSubidos()
        {
            var parser2 = new FileIniDataParser();
            IniData data2 = parser2.ReadFile("config.ini");

            KeyDataCollection keyCol = data2["gestionador_configuracion"];

            //se asigna la frecuencia del .ini
            string _resultado = keyCol["total_subidos"];

            int total = Convert.ToInt32(_resultado);

            return total;
        }//Termina totalSubidos

        private bool ExisteURL(string url)
        { 
          try
          {
              HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
              request.Method = "HEAD";
              HttpWebResponse response = request.GetResponse() as HttpWebResponse;
              //Regresa true si el código de status = 200
              response.Close();
              return (response.StatusCode == HttpStatusCode.OK);
          }
          catch(Exception ex)
          {
              //Cualquier error regresa false;
              logger.Error("ExisteURL: " + ex.Message);
              return false;
          }
        }//Termina ExisteURL

        public void Opciones(ref string _resultado)
        {
            if (ConectarWS())
            {
                if (lstInstrucion.Count == 0)
                {
                    MESSAGEBOX_TEMPORAL.Show("No hay instrucciones!!", "Aviso", 5, true);
                    logger.Info("No hay instrucciones!!");
                }
                
                    foreach (CUENTAS instruccion in lstInstrucion)
                    {
                        Cursor.Current = Cursors.WaitCursor;

                        string strRfc = instruccion.Rfc.Replace("-", "");

                        switch (instruccion.E_tipoinstruccion)
                        {
                            case 1://NADA
                                   //"Operación dummy, para no hacer nada"
                                break;

                            case 10://ACTUALIZAR CFDIS
                                //"Operación común para actualizar CFDIs dt_desde dt_hasta"
                                _resultado = _resultado + DescargarCFDI(instruccion);
                                //_resultado = _resultado + CancelarCFDI(instruccion);
                                
                                break;
                            case 12://ACTUALIZA CFDIS CANCELADOS
                                    //"Operación común para actualizar CFDIs cancelados dt_desde dt_hasta"
                                    _resultado = _resultado + DescargarCFDI(instruccion);
                                break;

                            case 20://BORRAR CFDIS SUBIDOS
                                    //"Operación para borrar los CFDIs subidos dt_desde dt_hasta"
                                BorrarCarpetas(instruccion);
                                break;

                            case 110://ACTUALIZA LICENCIA
                                     //"Operación para requerir el archivo de la licencia"
                                ActualizarLicencia(instruccion);
                                break;

                            case 120://ACTUALIZAR LIBRERÍA CFDIS
                                     //"Operación para actualizar la librería de los CFDIs"
                                     //----
                                break;

                            case 200://ACTUALIZAR GESTIONADOR
                                     //"Operación para actualizar el gestionador de CFDIs"
                                     //----
                                break;

                            case 310://CONFIGURAR URL
                                     //"Operación para actualizar el url en el .ini
                                if (ExisteURL("http://p01test.stverticales.m/app_wbs/110gestionador/call/soap?WSDL"))
                                {
                                    CAPTURAR_CONFIGURACIONES_INI.AgregarURL("url a guardar");
                                }
                                else
                                {
                                    logger.Error(string.Format("La url no es válida!!"));
                                }
                                break;

                            case 320://CONFIGURAR EMAIL
                                     //"Operación para actualizar el email en el .ini"
                                CAPTURAR_CONFIGURACIONES_INI.AgregarUsuario("test@stverticales.mx");
                                break;

                            case 330://CONFIGURAR PASSWORD
                                     //"Operación para actualizar el password en el . ini"
                                CAPTURAR_CONFIGURACIONES_INI.AgregarPassword("1234");
                                break;

                            case 340://CONFIGURAR FRECUENCIA EN MINUTOS
                                     //"Operación para actualizar la frecuencia en el . ini"
                                CAPTURAR_CONFIGURACIONES_INI.AgregarFrecuenciaMinutos(10);
                                break;

                            case 410://ESTATUS SERVIDOR
                                     //"Operación en la que el Gestionador indicará el estatus del servidor"
                                     //----
                                break;

                            case 510://TIMBRAR CFDIS
                                     //"Operación para timbrar cfdis"
                                _resultado = _resultado + TimbrarCFDI(instruccion);
                                break;

                            default:
                                //----;
                                break;
                        }
                        
                        Cursor.Current = Cursors.Default;
                    }
                DesconectarWS();
            }
         
        }//Termina opciones

        public bool ProcesandoInstruccion()
        {
            bool _procesar;
            if(loginAuth == null)
            {
                _procesar = false;
            }
            else
            {
                _procesar = true;
            }

            return _procesar;
        }//Termina ExisteURL
    }
}