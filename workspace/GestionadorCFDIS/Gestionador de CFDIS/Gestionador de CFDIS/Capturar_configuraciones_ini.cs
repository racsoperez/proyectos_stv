﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
//Paquetes para la lectura y modificación del iniparser  documentación: https://github.com/rickyah/ini-parser/wiki/First-Steps
using IniParser;
using IniParser.Model;
using System.Timers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System.Threading;

namespace Gestionador_de_CFDIS
{
    /// <summary>
    /// Esta clase sirve para hacer CRUD en el .ini.
    /// </summary>
    ///<remarks>
    ///
    ///</remarks>
    class CAPTURAR_CONFIGURACIONES_INI
    {
        //http://p01test.stverticales.mx/app_wbs/110gestionador/call/soap?WSDL

        public static Logger logger = LogManager.GetCurrentClassLogger();

        ///<summary>
        ///Este método sirve para crear el archivo .ini y crear los campos de configuración.
        ///</summary>
        public static void CrearConfiguracionesINI()
        {
            //ruta debug donde se va guardar el archivo .ini
            string _path = System.AppDomain.CurrentDomain.BaseDirectory + "config.ini";

            //es el nombre de la sección en el .ini
            string _nombreSeccion = "gestionador_configuracion";

            // verifica si está creado el archivo .ini
            if (!File.Exists(_path))
            {
                StreamWriter sw = new StreamWriter(_path);
                sw.Close();
                var parser = new FileIniDataParser();
                IniData data = parser.ReadFile("config.ini");

                //se agrega la sección
                data.Sections.AddSection(_nombreSeccion);

                data[_nombreSeccion].AddKey("url", "");
                data[_nombreSeccion].AddKey("usuario", "");
                data[_nombreSeccion].AddKey("pass", "");
                data[_nombreSeccion].AddKey("frecuencia_min", "10");
                data[_nombreSeccion].AddKey("ultimo_problema", "");
                data[_nombreSeccion].AddKey("ultimo_problema_codigo", "");
                data[_nombreSeccion].AddKey("ultimo_problema_fechahora", "");
                data[_nombreSeccion].AddKey("ultima_subida_exitosa", "");
                data[_nombreSeccion].AddKey("ultima_cfdis","");
                data[_nombreSeccion].AddKey("total_subidos", "");

                //guardamos los datos en el .ini
                parser.WriteFile("config.ini", data);

            }
        }

        ///<summary>
        ///Este método sirve para guardar la información en el archivo .ini.
        ///</summary>
        ///<remarks>
        ///
        ///</remarks>
        ///<param name="_user">
        ///Parámetro usado para el usuario.
        ///</param>
        ///<param name="_pass">
        ///Parámetro usado para la contraseña.
        ///</param>
        ///<param name="_url">
        ///Parámetro usado para la url del WebService.
        ///</param>
        public static void GuardarConfiguracionINI(string _user, string _pass, string _url)
        {
            string nombreSeccion = "gestionador_configuracion";

            var parser = new FileIniDataParser();
            IniData data = parser.ReadFile("config.ini");

            //Verificar si el archivo de config.ini ya tiene datos. 
            KeyDataCollection keyCol = data[nombreSeccion];

            if (keyCol.Count > 0)
            {
                //modificando los datos de las secciones
                data[nombreSeccion]["usuario"] = _user;
                data[nombreSeccion]["pass"] = _pass;
                data[nombreSeccion]["url"] = _url;
            }
            else
            {
                //agregando los valores a las secciones
                data[nombreSeccion].AddKey("url", _url);
                data[nombreSeccion].AddKey("usuario", _user);
                data[nombreSeccion].AddKey("pass", _pass);
                data[nombreSeccion].AddKey("frecuencia_min", "10");
                data[nombreSeccion].AddKey("ultimo_problema", "");
                data[nombreSeccion].AddKey("ultimo_problema_codigo", "");
                data[nombreSeccion].AddKey("ultimo_problema_fechahora", "");
                data[nombreSeccion].AddKey("ultima_subida_exitosa", "");
                data[nombreSeccion].AddKey("ultima_cfdis", "");
                data[nombreSeccion].AddKey("total_subidos", "");
                
            }

            //guardar el archivo .ini
            parser.WriteFile("config.ini", data);

            //MessageBox.Show("La aplicación se reiniciará para iniciar con las nuevas credenciales", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
       
            MESSAGEBOX_TEMPORAL.Show("La aplicación se reiniciará para iniciar con las nuevas credenciales ", "Aviso", 10, true);
            Application.Restart();
            

        }

        ///<summary>
        ///Este método sirve para crear la sección en archivo .ini de acuerdo a la cuenta y rfc enviados por parametro.
        ///</summary>
        ///<param name="_cuenta">
        ///Parámetro usado para la cuenta.
        ///</param>
        ///<param name="_rfc">
        ///Parámetro usado para el rfc.
        ///</param>
        public static void CrearSeccionRFC(int _cuenta, string _rfc)
        {
            var parser = new FileIniDataParser();
            IniData data = parser.ReadFile("config.ini");

            string nombreSeccion = "gestionador_rfc_" + _cuenta + "_" + _rfc;

            //Verificar si el archivo de config.ini ya tiene datos. 
            KeyDataCollection keyCol = data[nombreSeccion];

            //El número 8 es la cantidad de campos en el ini
            if (keyCol.Count < 9)
            {
                try
                {
                    string _s_json = JsonConvert.SerializeObject(new List<string>());
                    string _contar_cfdis = JsonConvert.SerializeObject(new List<string>());

                    //se agrega la sección
                    data.Sections.AddSection(nombreSeccion);

                    //se agregan las keys
                    data[nombreSeccion].AddKey("rfc", _rfc);
                    data[nombreSeccion].AddKey("ultimo_ok_fechahora", "");
                    data[nombreSeccion].AddKey("ultimo_problema", "");
                    data[nombreSeccion].AddKey("ultimo_problema_codigo", "");
                    data[nombreSeccion].AddKey("ultimo_problema_fechahora", "");
                    data[nombreSeccion].AddKey("ultima_subida_exitosa", "");
                    data[nombreSeccion].AddKey("cfdis_conproblemas", _s_json);
                    data[nombreSeccion].AddKey("cfdis_descargados",_contar_cfdis);

                    //guardamos los datos en el .ini
                    parser.WriteFile("config.ini", data);
                }
                catch (Exception ex)
                {
                    logger.Error("Error : " + ex.Message);
                }
            }
        }

        ///<summary>
        ///Este método sirve para guardar los problemas por rfc en el archivo .ini.
        ///</summary>
        ///<param name="_cuenta">
        ///Parámetro usado para la cuenta.
        ///</param>
        ///<param name="_rfc">
        ///Parámetro usado para el rfc.
        ///</param>
        ///<param name="_ultimo_problema">
        ///Parámetro usado para guardar el último problema.
        ///</param>
        ///<param name="_ultimo_problema_codigo">
        ///Parámetro usado para guardar el último código del problema.
        ///</param>
        ///<param name="_ultimo_problema_fechahora">
        ///Parámetro usado para guardar la fecha del último problema.
        ///</param>
        public static void GuardarProblemasPorRFC(int _cuenta, string _rfc, string _ultimo_problema, string _ultimo_problema_codigo, string _ultimo_problema_fechahora)
        {
            var parser = new FileIniDataParser();
            IniData _D_data = parser.ReadFile("config.ini");

            string nombreSeccion = "gestionador_rfc_"+ _cuenta + "_" + _rfc;

            KeyDataCollection keyCol = _D_data[nombreSeccion];
            try
            {
                if (keyCol.Count > 0)
                {
                    //modificando los datos de las secciones
                    _D_data[nombreSeccion]["rfc"] = _rfc;
                    _D_data[nombreSeccion]["ultimo_problema"] = _ultimo_problema;
                    _D_data[nombreSeccion]["ultimo_problema_codigo"] = Convert.ToString(_ultimo_problema_codigo);
                    _D_data[nombreSeccion]["ultimo_problema_fechahora"] = _ultimo_problema_fechahora;
                }
                //guardamos los datos en el .ini
                parser.WriteFile("config.ini", _D_data);
            }
            catch(Exception ex)
            {
                logger.Error("Error : " + ex.Message);
            }
    
        }

        ///<summary>
        ///Este método sirve para guardar los problemas por rfc en un lista en el archivo .ini.
        ///</summary>
        ///<remarks>
        ///Datos que se guardan en la lista: _s_id, _E_tipoinstruccion, cfdi, _E_error_return, _s_error.
        ///</remarks>
        ///<param name="_cuenta">
        ///Parámetro usado para la cuenta.
        ///</param>
        ///<param name="_rfc">
        ///Parámetro usado para el rfc.
        ///</param>
        ///<param name="_s_id">
        ///Parámetro usado para guardar el _s_id de la instrucción.
        ///</param>
        ///<param name="_E_tipoinstruccion">
        ///Parámetro usado para guardar el tipo de instruccion.
        ///</param>
        ///<param name="_cfdi_uuid">
        ///Parámetro usado para guardar el uuid del archivo xml.
        ///</param>
        ///<param name="_E_error_return">
        ///Parámetro usado para guardar el número de error generado.
        ///</param>
        ///<param name="_s_error">
        ///Parámetro usado para guardar el mensaje de error generado.
        ///</param>
        public static void AgregarDatosRfcConProblemas(int _cuenta, string _rfc, int _s_id, int _E_tipoinstruccion, string _cfdi_uuid, int _E_error_return, string _s_error)
        {
            var parser = new FileIniDataParser();
            IniData _D_data = parser.ReadFile("config.ini");

            string nombreSeccion = "gestionador_rfc_" + _cuenta + "_" + _rfc;

            var objJson = new List<CFDI_PROBLEMA>();
            KeyDataCollection keyCol = _D_data[nombreSeccion];

            if (keyCol.Count > 0)
            {
               
                if (_D_data[nombreSeccion]["cfdis_conproblemas"] == "")
                {
                    objJson = new List<CFDI_PROBLEMA>();
                }
                else
                {
                    objJson = JsonConvert.DeserializeObject<List<CFDI_PROBLEMA>>(_D_data[nombreSeccion]["cfdis_conproblemas"]);
                }

                CFDI_PROBLEMA _cfdi_problema = new CFDI_PROBLEMA
                {
                    s_id = _s_id,
                    E_tipoinstruccion = _E_tipoinstruccion,
                    cfdi = _cfdi_uuid,
                    E_error_return = _E_error_return,
                    s_error = _s_error
                };

                objJson.Add(_cfdi_problema);

                string s_json = JsonConvert.SerializeObject(objJson);

                //modificando los datos de las secciones
                _D_data[nombreSeccion]["cfdis_conproblemas"] = s_json;
            }
            //guardamos los datos en el .ini
            parser.WriteFile("config.ini", _D_data);

            //{cfdi: uuid, problema: "problema encontrado", problema_codigo: [n_return del tipo del problema]}
        }

        ///<summary>
        ///Este método sirve para guardar los problemas en la sección de [gestionador_configuracion] en el archivo .ini.
        ///</summary>
        ///<param name="_ultimo_problema">
        ///Parámetro usado para guardar el último problema generado.
        ///</param>
        ///<param name="_ultimo_problema_codigo">
        ///Parámetro usado para guardar el código del último problema generado.
        ///</param>
        ///<param name="_ultimo_problema_fechahora">
        ///Parámetro usado para guardar la fecha del último problema que se generó.
        ///</param>
        public static void AgregarProblemasConfiguracion(string _ultimo_problema, string _ultimo_problema_codigo, string _ultimo_problema_fechahora)
        {
            var parser = new FileIniDataParser();
            IniData _D_data = parser.ReadFile("config.ini");

            string nombreSeccion = "gestionador_configuracion";

            KeyDataCollection keyCol = _D_data[nombreSeccion];

            if (keyCol.Count > 0)
            {
                //modificando los datos de las secciones
                _D_data[nombreSeccion]["ultimo_problema"] = _ultimo_problema;
                _D_data[nombreSeccion]["ultimo_problema_codigo"] = _ultimo_problema_codigo;
                _D_data[nombreSeccion]["ultimo_problema_fechahora"] = _ultimo_problema_fechahora;
            }
            //guardamos los datos en el .ini
            parser.WriteFile("config.ini", _D_data);
        }

        ///<summary>
        ///Este método sirve para guardar la frecuencia en el archivo .ini, en que se va estar ejecutando el Gestionador.
        ///</summary>
        ///<param name="frecuencia">
        ///Parámetro usado para guardar la frecuanecia en minutos en que se va ejecutar el gestionador.
        ///</param>
        public static void AgregarFrecuenciaMinutos(int _frecuencia)
        {
            var parser = new FileIniDataParser();
            IniData _D_data = parser.ReadFile("config.ini");

            string nombreSeccion = "gestionador_configuracion";

            KeyDataCollection keyCol = _D_data[nombreSeccion];
            
            if (keyCol.Count > 0)
            {
                //modificando los datos de las secciones
                _D_data[nombreSeccion]["frecuencia_min"] = Convert.ToString(_frecuencia);
            }
            //guardamos los datos en el .ini
            parser.WriteFile("config.ini", _D_data);
        }

        ///<summary>
        ///Este método sirve para guardar el url en el archivo .ini
        ///</summary>
        ///<remarks>
        ///
        ///</remarks>>
        ///<param name="url">
        ///Parámetro usado para guardar la url para conectarse al WebService.
        ///</param>
        public static void AgregarURL(string _url)
        {
            var parser = new FileIniDataParser();
            IniData data = parser.ReadFile("config.ini");

            string nombreSeccion = "gestionador_configuracion";

            KeyDataCollection keyCol = data[nombreSeccion];

            if (keyCol.Count > 0)
            {
                //modificando los datos de las secciones
                data[nombreSeccion]["url"] = _url;
            }
            //guardamos los datos en el .ini
            parser.WriteFile("config.ini", data);
        }

        ///<summary>
        ///Este método sirve para guardar el usuario en el archivo .ini.
        ///</summary>
        ///<param name="_user">
        ///Parámetro usado para guardar el usuario.
        ///</param>
        public static void AgregarUsuario(string _user)
        {
            var parser = new FileIniDataParser();
            IniData data = parser.ReadFile("config.ini");

            string nombreSeccion = "gestionador_configuracion";

            KeyDataCollection keyCol = data[nombreSeccion];

            if (keyCol.Count > 0)
            {
                //modificando los datos de las secciones
                data[nombreSeccion]["user"] = _user;
            }
            //guardamos los datos en el .ini
            parser.WriteFile("config.ini", data);
        }

        ///<summary>
        ///Este método sirve para guardar la contraseña en el archivo .ini.
        ///</summary>
        ///<param name="_pass">
        ///Parámetro usado para guardar la contraseña.
        ///</param>
        public static void AgregarPassword(string _pass)
        {
            var parser = new FileIniDataParser();
            IniData data = parser.ReadFile("config.ini");

            string nombreSeccion = "gestionador_configuracion";

            KeyDataCollection keyCol = data[nombreSeccion];

            if (keyCol.Count > 0)
            {
                //modificando los datos de las secciones
                data[nombreSeccion]["pass"] = _pass;
            }
            //guardamos los datos en el .ini
            parser.WriteFile("config.ini", data);
        }

        ///<summary>
        ///Este método sirve para guardar el último xml subido con éxito a la nube.
        ///</summary>
        ///<param name="_ultima_subida_exitosa">
        ///Parámetro usado para guardar la fecha y hora de la última subida exitosa.
        ///</param>
        ///<param name="_ultima_cfdis">
        ///Parámetro usado para guardar el uuid y rfc del último xml subido a la nube.
        ///</param>
        public static void GuardarSubidaExitosaConfig(string _ultima_subida_exitosa, string _ultima_cfdis)
        {
            var parser = new FileIniDataParser();
            IniData _D_data = parser.ReadFile("config.ini");

            string nombreSeccion = "gestionador_configuracion";

            KeyDataCollection keyCol = _D_data[nombreSeccion];

            if (keyCol.Count > 0)
            {
                //modificando los datos de las secciones
                _D_data[nombreSeccion]["ultima_cfdis"] = _ultima_cfdis;
                _D_data[nombreSeccion]["ultima_subida_exitosa"] = _ultima_subida_exitosa;
            }
            //guardamos los datos en el .ini
            parser.WriteFile("config.ini", _D_data);
        }

        ///<summary>
        ///Este método sirve para guardar el último xml subido con éxito a la nube, por rfc.
        ///</summary>
        ///<param name="_cuenta">
        ///Parámetro usado para la cuenta.
        ///</param>
        ///<param name="_rfc">
        ///Parámetro usado para el rfc.
        ///</param>
        ///<param name="_ultima_subida_exitosa">
        ///Parámetro usado para guardar la fecha del último xml subido a la nube.
        ///</param>
        public static void GuardarSubidaExitosaRFC(int _cuenta, string _rfc, string _ultima_subida_exitosa)
        {
            var parser = new FileIniDataParser();
            IniData _D_data = parser.ReadFile("config.ini");

            string nombreSeccion = "gestionador_rfc_" + _cuenta + "_" + _rfc;

            KeyDataCollection keyCol = _D_data[nombreSeccion];

            if (keyCol.Count > 0)
            {
                //modificando los datos de las secciones
                //_D_data[nombreSeccion]["ultima_cfdis"] = _ultima_cfdis;
                _D_data[nombreSeccion]["ultima_subida_exitosa"] = _ultima_subida_exitosa;
            }
            //guardamos los datos en el .ini
            parser.WriteFile("config.ini", _D_data);
        }

        ///<summary>
        ///Este método sirve para guardar el total de xml subidos a la nube.
        ///</summary>
        ///<param name="_total_subidos">
        ///Parámetro usado para guardar el total de xml subidos.
        ///</param>
        public static void GuardarTotalSubidos(int _total_subidos)
        {
            var parser = new FileIniDataParser();
            IniData _D_data = parser.ReadFile("config.ini");

            string nombreSeccion = "gestionador_configuracion";

            KeyDataCollection keyCol = _D_data[nombreSeccion];

            if (keyCol.Count > 0)
            {
                //modificando los datos de las secciones
                _D_data[nombreSeccion]["total_subidos"] = Convert.ToString(_total_subidos);
                
            }
            //guardamos los datos en el .ini
            parser.WriteFile("config.ini", _D_data);
        }
    }
}

//url = http://p01.stverticales.mx/app_wbs/100acceso/call/soap?WSDL
//usuario = jherrero@stverticales.mx
//pass = 1234


//url = http://p01test.stverticales.mx/app_wbs/110gestionador/call/soap?WSDL
//usuario = test@stverticales.mx
//pass = 3{Q}Z.L3INs{
//frecuencia_min = 1