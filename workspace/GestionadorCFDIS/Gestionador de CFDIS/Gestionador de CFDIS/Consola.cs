﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;


namespace Gestionador_de_CFDIS
{
    public partial class Consola : Form
    {

        public Consola()
        {
            InitializeComponent();
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            richConsola.Clear();
            timer1.Stop();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Consola_Load(object sender, EventArgs e)
        {
            timer1.Start();
            richConsola.Text = cargarArchivo();

        }

        public string cargarArchivo()
        {
            string path = System.AppDomain.CurrentDomain.BaseDirectory + "logs//log.txt";

            System.IO.StreamReader sr = new System.IO.StreamReader(path, System.Text.Encoding.Default);
            string texto = sr.ReadToEnd();
            sr.Close();
            richConsola.Text = texto;

            //mantener el scroll en la última línea
            richConsola.SelectionStart = richConsola.Text.Length;
            richConsola.ScrollToCaret();

            return richConsola.Text;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            richConsola.Text = cargarArchivo();
        }

    }
}
