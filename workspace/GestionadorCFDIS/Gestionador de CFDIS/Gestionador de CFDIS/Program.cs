﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
//Paquetes para la lectura y modificación del iniparser  documentación: https://github.com/rickyah/ini-parser/wiki/First-Steps
using IniParser;
using IniParser.Model;
using System.Timers;


namespace Gestionador_de_CFDIS
{
    static class Program
    {
        static STV_SERVICE _stvService;
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            // Application.Run(new Form1());

            //testInternet();

            _stvService = new STV_SERVICE();

            CAPTURAR_CONFIGURACIONES_INI.CrearConfiguracionesINI();

            var parser2 = new FileIniDataParser();
            IniData data2 = parser2.ReadFile("config.ini");

            KeyDataCollection keyCol = data2["gestionador_configuracion"];

            //se asigna la frecuencia del .ini
            int _resultado = Convert.ToInt32(keyCol["frecuencia_min"]);


            int _frecuencia = 0;

            try
            {
                //verifica si la frecuencia esta entre 1 y 60 minutos para poder ejecutarse
                //si no, guardar frecuencia de 10 minutos
                if ((_resultado >= 1) && (_resultado <= 60))
                {
                    _frecuencia = _resultado * 60000;
                }
                else
                {
                    _frecuencia = 10 * 60000;
                    CAPTURAR_CONFIGURACIONES_INI.AgregarFrecuenciaMinutos(10);
                }

                // Creamos el timer y le seteamos el intervalo
                System.Timers.Timer timer = new System.Timers.Timer();
                // 60000 = 1 minuto
                timer.Interval = _frecuencia;
                timer.Elapsed += OnTimedEvent;
                timer.Enabled = true;

                //verificamos si no hay datos en el .ini para iniciar en el form configuración y si hay, iniciamos con el formulario de inicio
                FormConfiguracion formConfig = new FormConfiguracion();
                if (keyCol.Count < 3)
                {
                    formConfig.Show();
                    Application.Run();
                }
                else if ((keyCol["usuario"] == "") || (keyCol["pass"] == "") || (keyCol["url"] == ""))
                {
                    formConfig.Show();
                    Application.Run();
                }
                else
                {
                    if (_stvService.ConectarWS())
                    {
                        Form1 main = new Form1();
                        main.Show();
                        Application.Run();
                    }
                    else
                    {
                        if (result == "")
                        {
                            MESSAGEBOX_TEMPORAL.Show("Tienes algunos datos inválidos!! ", "Aviso", 10, true);
                            formConfig.Show();
                            Application.Run();
                        }
                        else
                        {
                            MESSAGEBOX_TEMPORAL.Show(result, "Aviso", 10, true);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                MESSAGEBOX_TEMPORAL.Show(ex.Message,"Error",10, true);
            }
        }

        public static string result = "";
        public static void testInternet()
        {
            System.Uri Url= new System.Uri("http://www.google.com/");
            System.Net.WebRequest WebRequest;
            WebRequest = System.Net.WebRequest.Create(Url);
            System.Net.WebResponse objResp;

            try
            {
                objResp = WebRequest.GetResponse();
                result = "";
                objResp.Close();
                WebRequest = null;
            }
            catch(Exception)
            {
                result = "Verifica tu conexión a internet!!";
                WebRequest = null;
            }
        }

        private static void OnTimedEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            //FormCFDI frmcfdi = new FormCFDI();
            //frmcfdi.btnDescargarFE_Click(null, new EventArgs());

            string _resultado = string.Empty;

            if (_stvService.ProcesandoInstruccion())
            {

            }
            else
            {
                _stvService.Opciones(ref _resultado);
            }
        }   
    }
}
