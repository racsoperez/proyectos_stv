using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Coordinador_v1._0
{
    public partial class FormConfiguracion : Form
    {
        CONFIGURACIONES_INI O_configuracionIni;
        CODIGO_UNICO_PC o_codigo_unico;
        public FormConfiguracion(CONFIGURACIONES_INI O_configuracionIni)
        {
            this.O_configuracionIni = O_configuracionIni;
            InitializeComponent();
            o_codigo_unico = new CODIGO_UNICO_PC();
        }

        private void FormConfiguracion_Load(object sender, EventArgs e)
        {
            this.O_configuracionIni.validar_configuracion_ini();
            this.O_configuracionIni.validar_configuracion_contpaqi_ini();

            txtSitio.Select();
           
            txtSerie.Text = o_codigo_unico.Value(txtSerie.Text);

            txtSitio.Text = O_configuracionIni.LeerSitio();
            txtCuenta.Text = O_configuracionIni.LeerCuenta();
            txtUsuario.Text = O_configuracionIni.LeerUsuario();
            txtContrasena.Text = O_configuracionIni.LeerPass();

            txtUserContpaqi.Text = O_configuracionIni.LeerUsuarioContpaqi();
            txtContrasenaContpaqi.Text = O_configuracionIni.LeerPassContpaqi();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if ((txtCuenta.Text == "") || (txtUsuario.Text == "") || (txtContrasena.Text == "") || (txtSitio.Text == "" || (txtUserContpaqi.Text == "")))
            {
                MessageBox.Show("No puedes continuar sin llenar todos los campos!!", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                O_configuracionIni.ConfiguracionesINI(txtUsuario.Text, txtContrasena.Text, txtCuenta.Text, txtSitio.Text);
                O_configuracionIni.ConfiguracionesCONTPAQI(txtUserContpaqi.Text, txtContrasenaContpaqi.Text);
                this.Hide();
            }
        }

        private void FormConfiguracion_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close();
        }
    }
}
