﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coordinador_v1._0
{
    class CerrarPoliza_params
    {
        public CerrarPoliza_params_documento_bancario t_documentosbancarios { get; set; }

        public CerrarPoliza_params_cliente t_clientes { get; set; }

        //public List<int> diariosespeciales { get; set; }
        //public List<int> segmetosnegocio { get; set; }


        public CerrarPoliza_params()
        {
            t_documentosbancarios = new CerrarPoliza_params_documento_bancario();

            t_clientes = new CerrarPoliza_params_cliente();

            //diariosespeciales = new List<int>();

            //segmetosnegocio = new List<int>();

        }

    }

    
}
