﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Coordinador_v1._0
{
    public partial class FormComunicacion : Form
    {
        public AccesoService.loginResponseD_auth responseAuth = null;
        public AccesoService.logoutD_auth logoutAuth = null;
        public AccesoService.verifyD_auth verifyAuth = null;

        StvService _stvService = new StvService();
        public FormComunicacion()
        {
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            //verificamos si la sesión está activa
            if(Globales.responseAuth != null)
            {
                leerArchivoTexto();
                lblResultado.Text = "Exitosa";
            }
            else
            {
                lblResultado.Text = "Fallida";
            }
        }

        private void FormComunicacion_Load(object sender, EventArgs e)
        {
            leerArchivoTexto();
        }
        
        public void leerArchivoTexto()
        {
            //ruta debug
            string fileReader = System.AppDomain.CurrentDomain.BaseDirectory;
            //leer archivo de texto
            string[] lines = System.IO.File.ReadAllLines(fileReader + "\\STV.txt");

            //Fecha y hora de conexión
            lblHora.Text = lines[0];
            lblFecha.Text = lines[1];
            lblResultado.Text = lines[2];

            //Última conexión exitosa
            lblHoraUltimaC.Text = lines[0];
            lblFechaUltimaC.Text = lines[1];

        }

    }
}

