using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;


namespace Coordinador_v1._0
{
    public partial class Login : Form
    {
        public StvService O_stv_service = null;
        CONFIGURACIONES_INI O_configuracionIni;

        public Login()
        {
            InitializeComponent();
            this.O_configuracionIni = new CONFIGURACIONES_INI();
        }
        
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();

        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        //variables de hora y fecha
        public string hora;
        public string fecha;
        public string statusConexion = "Exitosa";

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            iniciar_proceso();
        }

        private void iniciar_proceso()
        {
            FormConfiguracion frmconfig = new FormConfiguracion(O_configuracionIni);

            O_stv_service = new StvService();

            Cursor.Current = Cursors.WaitCursor;
            try
            {
#if debug
                  txtUsuario.Text = "test@stverticales.mx";
                  txtContrasena.Text = "3{Q}Z.L3INs{";
#endif
                if (this.O_configuracionIni.validar_configuracion_ini() == true)
                {
                    if ((txtUsuario.Text == this.O_configuracionIni.LeerUsuario()) && (txtContrasena.Text == this.O_configuracionIni.LeerPass()))
                    {
                        if (O_stv_service.conectarWS(ref this.O_configuracionIni))
                        {
                            CrearArchivoTexto();
                            Form1 abrir = new Form1();
                            abrir.Show();
                            this.Hide();
                        }
                        else
                        {
                            //
                        }
                    }
                    else
                    {
                        MessageBox.Show("El nombre de usuario o contraseña es incorrecto!", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show("Usuario no registrado, Deseas registrarte?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (result == DialogResult.Yes)
                    {
                        frmconfig.ShowDialog();
                    }
                    else
                    {
                        //...
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Inicio de sesión", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtUsuario.Focus();
            }
        }
        private void Login_Load(object sender, EventArgs e)
        {
            O_configuracionIni.verificar_archivo_ini();
        }
        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void PanelBarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void txtContrasena_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                btnEnviar_Click(this, new EventArgs());
            }
        }
        public void CrearArchivoTexto()
        {
            //ruta debug
            string fileReader = System.AppDomain.CurrentDomain.BaseDirectory;

            string[] lineas = { hora, fecha, statusConexion };
            using (StreamWriter outputFile = new StreamWriter(fileReader + "\\STV.txt"))
            {
                foreach (string item in lineas)
                {
                    outputFile.WriteLine(item);
                }
                outputFile.Close();
            }
        }
        private void tmFechaHora_Tick(object sender, EventArgs e)
        {
            hora = DateTime.Now.ToLongDateString();
            fecha = DateTime.Now.ToString("HH:mm:ssss");
        }

        private void btnEnviar_Click_1(object sender, EventArgs e)
        {
            iniciar_proceso();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnCerrar_MouseHover(object sender, EventArgs e)
        {
            btnCerrar.BackColor = Color.Red;
        }

        private void btnCerrar_MouseLeave(object sender, EventArgs e)
        {
            btnCerrar.BackColor = Color.FromArgb(71, 140, 120);
        }
    }
}
