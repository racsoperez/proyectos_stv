﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Coordinador_v1._0.AccesoService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="https://stverticales.mx/", ConfigurationName="AccesoService.STV_AccesoPortType")]
    public interface STV_AccesoPortType {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://p01.stverticales.mx/app_wbs/010acceso/call/soaplogin", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        Coordinador_v1._0.AccesoService.loginResponse login(Coordinador_v1._0.AccesoService.loginRequest request);
        
        // CODEGEN: Generando contrato de mensaje, ya que la operación tiene múltiples valores de devolución.
        [System.ServiceModel.OperationContractAttribute(Action="http://p01.stverticales.mx/app_wbs/010acceso/call/soaplogin", ReplyAction="*")]
        System.Threading.Tasks.Task<Coordinador_v1._0.AccesoService.loginResponse> loginAsync(Coordinador_v1._0.AccesoService.loginRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://p01.stverticales.mx/app_wbs/010acceso/call/soaplogout", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        Coordinador_v1._0.AccesoService.logoutResponse logout(Coordinador_v1._0.AccesoService.logoutRequest request);
        
        // CODEGEN: Generando contrato de mensaje, ya que la operación tiene múltiples valores de devolución.
        [System.ServiceModel.OperationContractAttribute(Action="http://p01.stverticales.mx/app_wbs/010acceso/call/soaplogout", ReplyAction="*")]
        System.Threading.Tasks.Task<Coordinador_v1._0.AccesoService.logoutResponse> logoutAsync(Coordinador_v1._0.AccesoService.logoutRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://p01.stverticales.mx/app_wbs/010acceso/call/soapverify", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        Coordinador_v1._0.AccesoService.verifyResponse verify(Coordinador_v1._0.AccesoService.verifyRequest request);
        
        // CODEGEN: Generando contrato de mensaje, ya que la operación tiene múltiples valores de devolución.
        [System.ServiceModel.OperationContractAttribute(Action="http://p01.stverticales.mx/app_wbs/010acceso/call/soapverify", ReplyAction="*")]
        System.Threading.Tasks.Task<Coordinador_v1._0.AccesoService.verifyResponse> verifyAsync(Coordinador_v1._0.AccesoService.verifyRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://p01.stverticales.mx/app_wbs/010acceso/call/soapcoronavirus", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        [return: System.ServiceModel.MessageParameterAttribute(Name="result")]
        int coronavirus(int param_a, int param_b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://p01.stverticales.mx/app_wbs/010acceso/call/soapcoronavirus", ReplyAction="*")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="result")]
        System.Threading.Tasks.Task<int> coronavirusAsync(int param_a, int param_b);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.4084.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="https://stverticales.mx/")]
    public partial class loginResponseD_auth : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string s_verField;
        
        private string json_keyField;
        
        private string s_tokenField;
        
        private string s_signatureField;
        
        private int n_empresa_idField;
        
        /// <remarks/>
        public string s_ver {
            get {
                return this.s_verField;
            }
            set {
                this.s_verField = value;
                this.RaisePropertyChanged("s_ver");
            }
        }
        
        /// <remarks/>
        public string json_key {
            get {
                return this.json_keyField;
            }
            set {
                this.json_keyField = value;
                this.RaisePropertyChanged("json_key");
            }
        }
        
        /// <remarks/>
        public string s_token {
            get {
                return this.s_tokenField;
            }
            set {
                this.s_tokenField = value;
                this.RaisePropertyChanged("s_token");
            }
        }
        
        /// <remarks/>
        public string s_signature {
            get {
                return this.s_signatureField;
            }
            set {
                this.s_signatureField = value;
                this.RaisePropertyChanged("s_signature");
            }
        }
        
        /// <remarks/>
        public int n_empresa_id {
            get {
                return this.n_empresa_idField;
            }
            set {
                this.n_empresa_idField = value;
                this.RaisePropertyChanged("n_empresa_id");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.4084.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="https://stverticales.mx/")]
    public partial class verifyD_auth : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string s_verField;
        
        private string json_keyField;
        
        private string s_tokenField;
        
        private string s_signatureField;
        
        private int n_empresa_idField;
        
        /// <remarks/>
        public string s_ver {
            get {
                return this.s_verField;
            }
            set {
                this.s_verField = value;
                this.RaisePropertyChanged("s_ver");
            }
        }
        
        /// <remarks/>
        public string json_key {
            get {
                return this.json_keyField;
            }
            set {
                this.json_keyField = value;
                this.RaisePropertyChanged("json_key");
            }
        }
        
        /// <remarks/>
        public string s_token {
            get {
                return this.s_tokenField;
            }
            set {
                this.s_tokenField = value;
                this.RaisePropertyChanged("s_token");
            }
        }
        
        /// <remarks/>
        public string s_signature {
            get {
                return this.s_signatureField;
            }
            set {
                this.s_signatureField = value;
                this.RaisePropertyChanged("s_signature");
            }
        }
        
        /// <remarks/>
        public int n_empresa_id {
            get {
                return this.n_empresa_idField;
            }
            set {
                this.n_empresa_idField = value;
                this.RaisePropertyChanged("n_empresa_id");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.4084.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="https://stverticales.mx/")]
    public partial class logoutD_auth : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string s_verField;
        
        private string json_keyField;
        
        private string s_tokenField;
        
        private string s_signatureField;
        
        private int n_empresa_idField;
        
        /// <remarks/>
        public string s_ver {
            get {
                return this.s_verField;
            }
            set {
                this.s_verField = value;
                this.RaisePropertyChanged("s_ver");
            }
        }
        
        /// <remarks/>
        public string json_key {
            get {
                return this.json_keyField;
            }
            set {
                this.json_keyField = value;
                this.RaisePropertyChanged("json_key");
            }
        }
        
        /// <remarks/>
        public string s_token {
            get {
                return this.s_tokenField;
            }
            set {
                this.s_tokenField = value;
                this.RaisePropertyChanged("s_token");
            }
        }
        
        /// <remarks/>
        public string s_signature {
            get {
                return this.s_signatureField;
            }
            set {
                this.s_signatureField = value;
                this.RaisePropertyChanged("s_signature");
            }
        }
        
        /// <remarks/>
        public int n_empresa_id {
            get {
                return this.n_empresa_idField;
            }
            set {
                this.n_empresa_idField = value;
                this.RaisePropertyChanged("n_empresa_id");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="login", WrapperNamespace="https://stverticales.mx/", IsWrapped=true)]
    public partial class loginRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="https://stverticales.mx/", Order=0)]
        public string s_email;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="https://stverticales.mx/", Order=1)]
        public string s_password;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="https://stverticales.mx/", Order=2)]
        public string s_codigo;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="https://stverticales.mx/", Order=3)]
        public string json_params;
        
        public loginRequest() {
        }
        
        public loginRequest(string s_email, string s_password, string s_codigo, string json_params) {
            this.s_email = s_email;
            this.s_password = s_password;
            this.s_codigo = s_codigo;
            this.json_params = json_params;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="loginResponse", WrapperNamespace="https://stverticales.mx/", IsWrapped=true)]
    public partial class loginResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="https://stverticales.mx/", Order=0)]
        public int n_return;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="https://stverticales.mx/", Order=1)]
        public string s_msgError;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="https://stverticales.mx/", Order=2)]
        public Coordinador_v1._0.AccesoService.loginResponseD_auth D_auth;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="https://stverticales.mx/", Order=3)]
        public string json_data;
        
        public loginResponse() {
        }
        
        public loginResponse(int n_return, string s_msgError, Coordinador_v1._0.AccesoService.loginResponseD_auth D_auth, string json_data) {
            this.n_return = n_return;
            this.s_msgError = s_msgError;
            this.D_auth = D_auth;
            this.json_data = json_data;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="logout", WrapperNamespace="https://stverticales.mx/", IsWrapped=true)]
    public partial class logoutRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="https://stverticales.mx/", Order=0)]
        public Coordinador_v1._0.AccesoService.logoutD_auth D_auth;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="https://stverticales.mx/", Order=1)]
        public string json_params;
        
        public logoutRequest() {
        }
        
        public logoutRequest(Coordinador_v1._0.AccesoService.logoutD_auth D_auth, string json_params) {
            this.D_auth = D_auth;
            this.json_params = json_params;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="logoutResponse", WrapperNamespace="https://stverticales.mx/", IsWrapped=true)]
    public partial class logoutResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="https://stverticales.mx/", Order=0)]
        public int n_return;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="https://stverticales.mx/", Order=1)]
        public string s_msgError;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="https://stverticales.mx/", Order=2)]
        public string json_data;
        
        public logoutResponse() {
        }
        
        public logoutResponse(int n_return, string s_msgError, string json_data) {
            this.n_return = n_return;
            this.s_msgError = s_msgError;
            this.json_data = json_data;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="verify", WrapperNamespace="https://stverticales.mx/", IsWrapped=true)]
    public partial class verifyRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="https://stverticales.mx/", Order=0)]
        public Coordinador_v1._0.AccesoService.verifyD_auth D_auth;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="https://stverticales.mx/", Order=1)]
        public string json_params;
        
        public verifyRequest() {
        }
        
        public verifyRequest(Coordinador_v1._0.AccesoService.verifyD_auth D_auth, string json_params) {
            this.D_auth = D_auth;
            this.json_params = json_params;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="verifyResponse", WrapperNamespace="https://stverticales.mx/", IsWrapped=true)]
    public partial class verifyResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="https://stverticales.mx/", Order=0)]
        public int n_return;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="https://stverticales.mx/", Order=1)]
        public string s_msgError;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="https://stverticales.mx/", Order=2)]
        public string json_data;
        
        public verifyResponse() {
        }
        
        public verifyResponse(int n_return, string s_msgError, string json_data) {
            this.n_return = n_return;
            this.s_msgError = s_msgError;
            this.json_data = json_data;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface STV_AccesoPortTypeChannel : Coordinador_v1._0.AccesoService.STV_AccesoPortType, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class STV_AccesoPortTypeClient : System.ServiceModel.ClientBase<Coordinador_v1._0.AccesoService.STV_AccesoPortType>, Coordinador_v1._0.AccesoService.STV_AccesoPortType {
        
        public STV_AccesoPortTypeClient() {
        }
        
        public STV_AccesoPortTypeClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public STV_AccesoPortTypeClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public STV_AccesoPortTypeClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public STV_AccesoPortTypeClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        Coordinador_v1._0.AccesoService.loginResponse Coordinador_v1._0.AccesoService.STV_AccesoPortType.login(Coordinador_v1._0.AccesoService.loginRequest request) {
            return base.Channel.login(request);
        }
        
        public int login(string s_email, string s_password, string s_codigo, string json_params, out string s_msgError, out Coordinador_v1._0.AccesoService.loginResponseD_auth D_auth, out string json_data) {
            Coordinador_v1._0.AccesoService.loginRequest inValue = new Coordinador_v1._0.AccesoService.loginRequest();
            inValue.s_email = s_email;
            inValue.s_password = s_password;
            inValue.s_codigo = s_codigo;
            inValue.json_params = json_params;
            Coordinador_v1._0.AccesoService.loginResponse retVal = ((Coordinador_v1._0.AccesoService.STV_AccesoPortType)(this)).login(inValue);
            s_msgError = retVal.s_msgError;
            D_auth = retVal.D_auth;
            json_data = retVal.json_data;
            return retVal.n_return;
        }
        
        public System.Threading.Tasks.Task<Coordinador_v1._0.AccesoService.loginResponse> loginAsync(Coordinador_v1._0.AccesoService.loginRequest request) {
            return base.Channel.loginAsync(request);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        Coordinador_v1._0.AccesoService.logoutResponse Coordinador_v1._0.AccesoService.STV_AccesoPortType.logout(Coordinador_v1._0.AccesoService.logoutRequest request) {
            return base.Channel.logout(request);
        }
        
        public int logout(Coordinador_v1._0.AccesoService.logoutD_auth D_auth, string json_params, out string s_msgError, out string json_data) {
            Coordinador_v1._0.AccesoService.logoutRequest inValue = new Coordinador_v1._0.AccesoService.logoutRequest();
            inValue.D_auth = D_auth;
            inValue.json_params = json_params;
            Coordinador_v1._0.AccesoService.logoutResponse retVal = ((Coordinador_v1._0.AccesoService.STV_AccesoPortType)(this)).logout(inValue);
            s_msgError = retVal.s_msgError;
            json_data = retVal.json_data;
            return retVal.n_return;
        }
        
        public System.Threading.Tasks.Task<Coordinador_v1._0.AccesoService.logoutResponse> logoutAsync(Coordinador_v1._0.AccesoService.logoutRequest request) {
            return base.Channel.logoutAsync(request);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        Coordinador_v1._0.AccesoService.verifyResponse Coordinador_v1._0.AccesoService.STV_AccesoPortType.verify(Coordinador_v1._0.AccesoService.verifyRequest request) {
            return base.Channel.verify(request);
        }
        
        public int verify(Coordinador_v1._0.AccesoService.verifyD_auth D_auth, string json_params, out string s_msgError, out string json_data) {
            Coordinador_v1._0.AccesoService.verifyRequest inValue = new Coordinador_v1._0.AccesoService.verifyRequest();
            inValue.D_auth = D_auth;
            inValue.json_params = json_params;
            Coordinador_v1._0.AccesoService.verifyResponse retVal = ((Coordinador_v1._0.AccesoService.STV_AccesoPortType)(this)).verify(inValue);
            s_msgError = retVal.s_msgError;
            json_data = retVal.json_data;
            return retVal.n_return;
        }
        
        public System.Threading.Tasks.Task<Coordinador_v1._0.AccesoService.verifyResponse> verifyAsync(Coordinador_v1._0.AccesoService.verifyRequest request) {
            return base.Channel.verifyAsync(request);
        }
        
        public int coronavirus(int param_a, int param_b) {
            return base.Channel.coronavirus(param_a, param_b);
        }
        
        public System.Threading.Tasks.Task<int> coronavirusAsync(int param_a, int param_b) {
            return base.Channel.coronavirusAsync(param_a, param_b);
        }
    }
}
