using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace Coordinador_v1._0
{
    public class Errores
    {
    
    }

    public enum ERRORES_WS
    {
        OK = 0,
        OK_WARNING = 1,
        NOK_LOGIN = 10,
        NOK_PERMISOS = 11,
        NOK_DATOS_INVALIDOS = 12,
        NOK_CONDICIONES_INCORRECTAS = 13,
        NOK_SECUENCIA = 14,
        NOK_REPETIR = 15,
        NOK_INFORMACION_ACTUALIZADA = 16,
        NOK_ERROR = 17,
        NOK_CONEXION = 18
    }
    public class D_RETURN
    {
        public ERRORES_WS E_return { get; set; } = ERRORES_WS.OK;
        public string s_msgError { get; set; } = string.Empty;
        public string s_proceso { get; set; } = string.Empty;

        public string mensajeParaLog()
        {
            string _s_message = this.s_proceso + " : " + this.E_return.ToString() + " : " + this.s_msgError;

            return _s_message;
        }

    }
}
