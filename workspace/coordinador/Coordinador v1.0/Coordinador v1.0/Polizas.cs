using System;
using System.Drawing;
using System.Windows.Forms;
using Newtonsoft.Json;
using SDKCONTPAQNGLib;
using System.Globalization;
using System.Collections;
using System.Diagnostics;
using System.Threading;
using NLog;
using System.IO;

namespace Coordinador_v1._0
{
    public partial class Polizas : Form
    {
        public PolizasService.polizas_pendientesD_auth polizasAuth = null;
        public PolizasService.poliza_obtenerD_auth polizasAuthObtener = null;
        public PolizasService.poliza_cerrarD_auth polizasAuthCerrar = null;
        public PolizasService.validaciones_contpaqiD_auth validacionesAuth = null;
        public PolizasService.poliza_erroresD_auth polizaErroresAuth = null;

        Thread iniciar_hilo;
        CerrarPoliza_params _cerrar_poliza_params = new CerrarPoliza_params();
        public static Logger O_logger = LogManager.GetCurrentClassLogger();
        validar _valida_codigos = new validar();
        CONFIGURACIONES_INI O_configuracionIni;
        SDK sdk;

        delegate void Estado_Delegate(E_ESTADO E_estado, string tiempo= null, int? polizas = 0, int? movimientos = 0, int? ingresos = 0, string empresa = null);
        
        public Polizas()
        {
            O_configuracionIni = new CONFIGURACIONES_INI();
            sdk = new SDK();
            InitializeComponent();
            this.sdk.EstadoCoordinadorEvent += EstadoCoordinador;
        }

        public void EstadoCoordinador(E_ESTADO E_estado, string tiempo =null, int? polizas = null, int? movimientos = null, int? ingresos = null, string empresa = null)
        {
            if (InvokeRequired)
            {
                Invoke(new Estado_Delegate(EstadoCoordinador), E_estado, tiempo, polizas, movimientos, ingresos, empresa);
                return;
            }
            lblStatus.Text = E_estado.ToString();
            lblEmpresa.Text = empresa.ToString();
            lblTiempo.Text = tiempo.ToString();

            if (polizas.HasValue)
            {
                lblPolizas.Text = polizas.ToString();
            }
            if(movimientos.HasValue)
            {
                lblMovPoliza.Text = movimientos.ToString();
            }
            if(ingresos.HasValue)
            {
                lblIngresos.Text = ingresos.ToString();
            }
            Refresh();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {   
            if (sdk.Iniciar_busqueda_polizas())
            {
                if (iniciar_hilo != null && iniciar_hilo.IsAlive)
                {

                }
                else
                {
                    iniciar_hilo = new Thread(new ThreadStart(sdk.Validaciones_contpaqi));
                    iniciar_hilo.Start();
                }
            }
        }
  
        private void Polizas_Load(object sender, EventArgs e)
        {
            DateTime fechaIni = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            dtpHoraInicio.Value = fechaIni;
            EstadoCoordinador(E_ESTADO.PENDIENTE,"",0,0,0,"");
        }
      
        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            if (dtpFechaInicio.Value.Day == dt.Day && dtpFechaInicio.Value.Month == dt.Month && dtpFechaInicio.Value.Year == dt.Year && dtpHoraInicio.Value.Hour == dt.Hour && dtpHoraInicio.Value.Minute == dt.Minute && dtpHoraInicio.Value.Second == dt.Second)
            {
                Cursor.Current = Cursors.WaitCursor;    
               // registrar_encabezado_poliza_movimientos();
                MessageBox.Show("Funcional");
            }
           
        }

        private void Polizas_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.iniciar_hilo.Abort();
        }
    }
}

