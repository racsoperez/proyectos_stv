using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using NLog;

namespace Coordinador_v1._0
{
    public class StvService
    {
        CONFIGURACIONES_INI O_configuracionIni = new CONFIGURACIONES_INI();

        public static Logger O_logger = LogManager.GetCurrentClassLogger();

        CODIGO_UNICO_PC o_codigo_unico = new CODIGO_UNICO_PC();

        public bool conectarWS(ref CONFIGURACIONES_INI O_configuracionIni)
        {

        this.O_configuracionIni = O_configuracionIni;

        D_RETURN _D_return = new D_RETURN()
        {
            E_return = ERRORES_WS.OK,
            s_msgError = string.Empty,
            s_proceso = "Iniciar sesión"
        };

        try
        {
            var client = new AccesoService.STV_AccesoPortTypeClient();

            string url = this.O_configuracionIni.LeerSitio();

            if (url == "")
            {
               client.Endpoint.Address = new System.ServiceModel.EndpointAddress(url);
            }
            else
            {
                //ya existe un url asignado
            }

            string WBS_s_msgError = "";
            string json_parms = "";
            string s_codigo = o_codigo_unico.Value(CODIGO_UNICO_PC.fingerPrint);
            //string s_codigo = "47E4-502E-7F55-D43A-ECCC-4CE7-0DCD-D06C";
            string json_data= "ssss";

            _D_return.E_return = (ERRORES_WS)client.login(this.O_configuracionIni.LeerUsuario(), this.O_configuracionIni.LeerPass(), s_codigo, json_parms, out WBS_s_msgError, out Globales.responseAuth, out json_data);

            _D_return.s_msgError = WBS_s_msgError;

            if (_D_return.E_return > ERRORES_WS.OK_WARNING)
            {
                O_logger.Error(_D_return.mensajeParaLog());
                return false;
            }
            else
            {
                _D_return.s_proceso = "Iniciando sesión";
                O_logger.Info(_D_return.mensajeParaLog());
                return true;
            }
        }
        catch (Exception ex)
        {
            MessageBox.Show("Error : " + ex.Message);
            O_logger.Info(_D_return.mensajeParaLog());
            return false;
        }
    }
    }

    public enum E_ESTADO
    {
        PENDIENTE = 0,
        INICIANDO_PROCESO = 1,
        BUSCANDO_POLIZAS = 5,
        INICIANDO_SESION_CONTPAQI = 10,
        VALIDACIONES_CONTPAQI = 15,
        VALIDACIONES_TIPO_POLIZAS = 20,
        POLIZAS_PENDIENTES = 25,
        POLIZA_ERROR = 28,
        OBTENIENDO_POLIZA = 30,
        VALIDANDO_RFC = 35,
        CREANDO_ENCABEZADO_POLIZA = 40,
        REGISTRANDO_MOVIMIENTOS = 45,
        CREANDO_POLIZA = 50,
        ASOCIANDO_CFDIS = 55,
        REGISTRANDO_INGRESOS = 60,
        PROCESO_TERMINADO = 65,

    }
}
