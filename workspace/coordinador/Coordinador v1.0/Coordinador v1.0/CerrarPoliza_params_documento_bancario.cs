﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coordinador_v1._0
{
    public class CerrarPoliza_params_documento_bancario
    {

        public List<int> folios_documentopago { get; set; }
        public List<int> asientos_id { get; set; }
        public List<int> tipodocumentobancario_ventacontado_id { get; set; }


        public CerrarPoliza_params_documento_bancario()
        {
 
            folios_documentopago = new List<int>();
            asientos_id = new List<int>();
            tipodocumentobancario_ventacontado_id = new List<int>();
      
        }
    }

}
