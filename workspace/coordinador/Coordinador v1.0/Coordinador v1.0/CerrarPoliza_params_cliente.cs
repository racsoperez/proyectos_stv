﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coordinador_v1._0
{
    class CerrarPoliza_params_cliente
    {
        public List<string> rfcs { get; set; }

        public List<string> razonessociales { get; set; }

        public List<int> codigoscontpaqi { get; set; }



        public CerrarPoliza_params_cliente()
        {
            rfcs = new List<string>();

            razonessociales = new List<string>();

            codigoscontpaqi = new List<int>();


        }

    }
}
