using System;
using SDKCONTPAQNGLib;
using System.Windows.Forms;
using NLog;
using System.Threading;
using System.Diagnostics;
using Newtonsoft.Json;
using System.Globalization;
using System.Collections.Generic;

namespace Coordinador_v1._0
{
    public class SDK
    {
        public delegate void Estado_Delegate(E_ESTADO E_estado, string tiempo = "", int? polizas = 0, int? movimientos = 0, int? ingresos = 0, string empresa = "");
        public event Estado_Delegate EstadoCoordinadorEvent;

        public PolizasService.polizas_pendientesD_auth polizasAuth = null;
        public PolizasService.poliza_obtenerD_auth polizasAuthObtener = null;
        public PolizasService.poliza_cerrarD_auth polizasAuthCerrar = null;
        public PolizasService.validaciones_contpaqiD_auth validacionesAuth = null;
        public PolizasService.poliza_erroresD_auth polizaErroresAuth = null;

        PolizasService.STV_AccesoPortTypeClient servicio;

        public TSdkCliente lsdkCliente = new TSdkCliente();
        public TSdkSesion lsdkSesion = new TSdkSesion();
        public TSdkPoliza lsdkPoliza = new TSdkPoliza();
        public TSdkMovimientoPoliza lsdkMovto = new TSdkMovimientoPoliza();
        public TSdkTipoPoliza lsdkTipoPoliza = new TSdkTipoPoliza();
        public TSdkEmpresa lsdkEmpresa = new TSdkEmpresa();
        public TSdkCuentaCheque lsdkCuentaCheque = new TSdkCuentaCheque();
        public TSdkIngreso lsdkIngreso = new TSdkIngreso();
        public TSdkSegmentoNegocio lsdkSegNegocio = new TSdkSegmentoNegocio();
        public TSdkDiarioEspecial lsdkDiarioEspe = new TSdkDiarioEspecial();
        public TSdkTipoDocumento lsdkTipoDocumento = new TSdkTipoDocumento();
        public TSdkBanco lsdkBancos = new TSdkBanco();
        public TSdkMoneda lsdkMonedas = new TSdkMoneda();
        public TSdkCuenta lsdkCuentas = new TSdkCuenta();

        CONFIGURACIONES_INI O_configuracionIni;
        public static Logger O_logger = LogManager.GetCurrentClassLogger();
        validar _valida_codigos = new validar();
        CerrarPoliza_params _cerrar_poliza_params = new CerrarPoliza_params();
        Stopwatch sw = Stopwatch.StartNew();

        int _n_nummovto = 0;
        int _n_contar_movimientos = 0;
        int _n_contar_polizas = 0;
        int contador = 0;
        int _n_contar_ingresos = 0;
        string json_params = "123";
        string json_data_validaciones = "";
        string json_data_polizas_pendientes = "";
        string json_data_poliza_errores = "";
        string json_data_poliza_obtener = "";
        string WBS_s_msgError = "";

        string elapsedTime = "";

        int n_servidor_id = 0;
        int n_ejercicio = 0;
        string basededatosvalidaciones = "";
        int n_empresacontpaqi_id = 0;
        string s_basededatos = "";

        string _s_error_movimiento = "";
        string _s_error_poliza = "";
        string _s_error_ingreso = "";

        bool b_test = true;
        public SDK()
        {
            O_configuracionIni = new CONFIGURACIONES_INI();
            servicio = new PolizasService.STV_AccesoPortTypeClient();
        }

        public void cerrar_empresa()
        {
            //Método Para cerrar empresa
            lsdkSesion.cierraEmpresa();
        }

        public void finaliza_sdk()
        {
            //Método Finalizar Conexión SDK
            lsdkSesion.finalizaConexion();
        }

        public void inicio_sesion_compaqi()
        {
            if (O_configuracionIni.validar_configuracion_contpaqi_ini())
            {
                //Verificar la conexión con el SDK
                if (lsdkSesion.conexionActiva == 0)
                {
                    lsdkSesion.iniciaConexion();
                }
                if ((lsdkSesion.conexionActiva == 1) && (lsdkSesion.ingresoUsuario == 0))
                {
                    //lsdkSesion.firmaUsuarioParams("SUPERVISOR","");
                    lsdkSesion.firmaUsuarioParams(O_configuracionIni.LeerUsuarioContpaqi(), O_configuracionIni.LeerPassContpaqi());
                }
            }
            else
            {
                //
            }
        }

        public void Abre_Empresa(string empresa)
        {
            int lResult = lsdkSesion.abreEmpresa(empresa); //Método para abrir la empresa

            D_RETURN _D_return = new D_RETURN()
            {
                E_return = ERRORES_WS.OK,
                s_msgError = string.Empty,
                s_proceso = "Abrir empresa " + empresa
            };

            if (lResult == 0)
            {
                _D_return.s_msgError = lsdkSesion.UltimoMsjError;
                _D_return.E_return = ERRORES_WS.NOK_DATOS_INVALIDOS;
                MessageBox.Show("Error: " + _D_return.mensajeParaLog());
                O_logger.Error(_D_return.mensajeParaLog());
            }
            else
            {
                O_logger.Info(_D_return.mensajeParaLog());
            }
        }

        public bool Iniciar_busqueda_polizas()
        {
            bool _hay_polizas = false;

            D_RETURN _D_return = new D_RETURN()
            {
                E_return = ERRORES_WS.OK,
                s_msgError = string.Empty,
                s_proceso = "Buscar pólizas"
            };
            EstadoCoordinadorEvent(E_ESTADO.INICIANDO_PROCESO);

            O_logger.Info(_D_return.s_proceso + " : " + _D_return.E_return + " : Iniciando busqueda de pólizas!");
            
            try
            {
                sw.Start();
                Thread.Sleep(1000);

                #region //validaciones contpaqi D_auth
                validacionesAuth = new PolizasService.validaciones_contpaqiD_auth()
                {
                    json_key = Globales.responseAuth.json_key,
                    s_signature = Globales.responseAuth.s_signature,
                    s_token = Globales.responseAuth.s_token,
                    s_ver = Globales.responseAuth.s_ver,
                    n_empresa_id = Globales.responseAuth.n_empresa_id,
                };
                #endregion
                #region//polizas pendientes D_auth
                polizasAuth = new PolizasService.polizas_pendientesD_auth()
                {
                    json_key = Globales.responseAuth.json_key,
                    s_signature = Globales.responseAuth.s_signature,
                    s_token = Globales.responseAuth.s_token,
                    s_ver = Globales.responseAuth.s_ver,
                };
                #endregion
                #region//poliza_errores D_auth
                polizaErroresAuth = new PolizasService.poliza_erroresD_auth()
                {
                    json_key = Globales.responseAuth.json_key,
                    s_signature = Globales.responseAuth.s_signature,
                    s_token = Globales.responseAuth.s_token,
                    s_ver = Globales.responseAuth.s_ver,
                };
                #endregion
                #region //poliza cerrar D_auth
                polizasAuthCerrar = new PolizasService.poliza_cerrarD_auth()
                {
                    json_key = Globales.responseAuth.json_key,
                    s_signature = Globales.responseAuth.s_signature,
                    s_token = Globales.responseAuth.s_token,
                    s_ver = Globales.responseAuth.s_ver,
                    n_empresa_id = Globales.responseAuth.n_empresa_id,
                };
                #endregion
                #region //poliza obtener D_auth
                polizasAuthObtener = new PolizasService.poliza_obtenerD_auth()
                {
                    json_key = Globales.responseAuth.json_key,
                    s_signature = Globales.responseAuth.s_signature,
                    s_token = Globales.responseAuth.s_token,
                    s_ver = Globales.responseAuth.s_ver,
                    n_empresa_id = Globales.responseAuth.n_empresa_id,
                };
                #endregion

                EstadoCoordinadorEvent(E_ESTADO.BUSCANDO_POLIZAS);

                _D_return.E_return = (ERRORES_WS)servicio.validaciones_contpaqi(validacionesAuth, json_params, out WBS_s_msgError, out json_data_validaciones);

                _D_return.s_msgError = WBS_s_msgError;

                if (_D_return.E_return > ERRORES_WS.OK_WARNING)
                {
                    O_logger.Error(_D_return.mensajeParaLog());
                }
                else
                {
                    O_logger.Info(_D_return.mensajeParaLog());
                }

                if (json_data_validaciones == null || json_data_validaciones == "")
                {
                    EstadoCoordinadorEvent(E_ESTADO.PROCESO_TERMINADO);
                    MessageBox.Show(_D_return.s_msgError, "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    _hay_polizas = false;
                    EstadoCoordinadorEvent(E_ESTADO.PENDIENTE);
                }
                else
                {
                    _D_return.s_msgError = "Póliza econtrada!";
                    O_logger.Info(_D_return.mensajeParaLog());
                    _hay_polizas = true;
                }
            }
            catch (Exception ex)
            {
                _D_return.s_msgError = ex.Message;
                _D_return.s_proceso = "Exception";
                _D_return.E_return = ERRORES_WS.NOK_DATOS_INVALIDOS;

                O_logger.Error(_D_return.mensajeParaLog());
                MessageBox.Show("Mensaje: " + ex.Message);
                return _hay_polizas;
            }

            return _hay_polizas;
        }

        public void Validaciones_contpaqi()
        {
            EstadoCoordinadorEvent(E_ESTADO.VALIDACIONES_CONTPAQI);

            D_RETURN _D_return = new D_RETURN()
            {
                E_return = ERRORES_WS.OK,
                s_msgError = string.Empty,
                s_proceso = "WBS validaciones contpaqi"
            };

            dynamic json_val = JsonConvert.DeserializeObject(json_data_validaciones);

            foreach (var val in json_val.datos)
            {
                EstadoCoordinadorEvent(E_ESTADO.INICIANDO_SESION_CONTPAQI);
                //iniciar sesión en el SDK
                inicio_sesion_compaqi();

                if ((lsdkSesion.conexionActiva == 1) && (lsdkSesion.ingresoUsuario == 1))
                {
                    Abre_Empresa(val.empresacontpaqi.ToString());
                }
                else
                {
                    _D_return.s_proceso = "Contpaqi";
                    _D_return.s_msgError = lsdkSesion.UltimoMsjError; //"El usuario o contraseña de contpaqi es incorrecto!!";
                    _D_return.E_return = ERRORES_WS.NOK_DATOS_INVALIDOS;
                    O_logger.Error(_D_return.mensajeParaLog());
                    break;
                }

                //validaciones contapaqi
                EstadoCoordinadorEvent(E_ESTADO.VALIDACIONES_TIPO_POLIZAS);

                #region valida tipos poliza

                lsdkTipoPoliza.setSesion(lsdkSesion);

                foreach (var tipoPoliza in val.validacionescontpaqi.t_tipospoliza.codigoscontpaqi)
                {
                    int Resultado = 1;
                    if (!b_test)
                    {
                        Resultado = lsdkTipoPoliza.buscaPorNumero(tipoPoliza);
                    }
                    
                    if (Resultado != 0)
                    {
                        //Encontró
                        _valida_codigos.validacionescontpaqi.t_tipospoliza._L_msgsError.Add(1);
                    }
                    else
                    {
                        _valida_codigos.validacionescontpaqi.t_tipospoliza._L_msgsError.Add(0);
                        //No encontró
                    }
                    _valida_codigos.validacionescontpaqi.t_tipospoliza.codigoscontpaqi.Add(Convert.ToInt32(tipoPoliza));
                }
                #endregion

                #region valida documentos bancarios

                lsdkTipoDocumento.setSesion(lsdkSesion);

                foreach (var tipoDoc in val.validacionescontpaqi.t_tipodocumentosbancarios.codigoscontpaqi)
                {
                    int Resultado = 1;
                    if(!b_test)
                    {
                       Resultado = lsdkTipoDocumento.buscaPorCodigo(tipoDoc);
                    }
                    
                    if (Resultado != 0)
                    {
                        _valida_codigos.validacionescontpaqi.t_tipodocumentosbancarios._L_msgsError.Add(1);
                    }
                    else
                    {
                        _valida_codigos.validacionescontpaqi.t_tipodocumentosbancarios._L_msgsError.Add(0);
                    }
                    _valida_codigos.validacionescontpaqi.t_tipodocumentosbancarios.codigoscontpaqi.Add(Convert.ToInt32(tipoDoc));
                }
                #endregion

                #region valida diarios especiales

                lsdkDiarioEspe.setSesion(lsdkSesion);

                foreach (var diario in val.validacionescontpaqi.t_diariosespeciales.codigoscontpaqi)
                {
                    int ResDiario = 1;
                    if(!b_test)
                    {
                        ResDiario = lsdkDiarioEspe.buscaPorCodigo(diario);
                    }

                    if (ResDiario != 0)
                    {
                        _valida_codigos.validacionescontpaqi.t_diariosespeciales._L_msgsError.Add(1);
                    }
                    else
                    {
                        _valida_codigos.validacionescontpaqi.t_diariosespeciales._L_msgsError.Add(0);
                    }
                    _valida_codigos.validacionescontpaqi.t_diariosespeciales.codigoscontpaqi.Add(Convert.ToInt32(diario));
                }
                #endregion

                #region valida cuentas

                lsdkCuentaCheque.setSesion(lsdkSesion);

                foreach (var cheque in val.validacionescontpaqi.t_cuentasbancarias.codigoscontpaqi)
                {
                    int ResCheque = 1;
                    if(!b_test)
                    {
                        ResCheque = lsdkCuentaCheque.buscaPorCodigo(cheque);
                    }

                    if (ResCheque != 0)
                    {
                        _valida_codigos.validacionescontpaqi.t_cuentasbancarias._L_msgsError.Add(1);
                    }
                    else
                    {
                        _valida_codigos.validacionescontpaqi.t_cuentasbancarias._L_msgsError.Add(0);
                    }

                    _valida_codigos.validacionescontpaqi.t_cuentasbancarias.codigoscontpaqi.Add(Convert.ToInt32(cheque));
                }
                #endregion

                #region valida segmento de negocio

                lsdkSegNegocio.setSesion(lsdkSesion);
                foreach (var segmento in val.validacionescontpaqi.t_segmentosnegocio.codigoscontpaqi)
                {
                    int ResulSeg = 1;
                    if(!b_test)
                    { 
                        ResulSeg = lsdkSegNegocio.buscaPorCodigo(segmento);
                    }   

                    if (ResulSeg != 0)
                    {
                        _valida_codigos.validacionescontpaqi.t_segmentosnegocio._L_msgsError.Add(1);
                    }
                    else
                    {
                        _valida_codigos.validacionescontpaqi.t_segmentosnegocio._L_msgsError.Add(0);
                    }
                    _valida_codigos.validacionescontpaqi.t_segmentosnegocio.codigoscontpaqi.Add(Convert.ToInt32(segmento));
                }
                #endregion

                #region valida bancos

                lsdkBancos.setSesion(lsdkSesion);

                foreach (var tipoDoc in val.validacionescontpaqi.t_bancos.codigoscontpaqi)
                {
                    int Resultado = 1;
                    if(!b_test)
                    {
                       Resultado = lsdkBancos.buscaPorCodigo(tipoDoc);
                    }

                    if (Resultado != 0)
                    {
                        _valida_codigos.validacionescontpaqi.t_bancos._L_msgsError.Add(1);
                    }
                    else
                    {
                        _valida_codigos.validacionescontpaqi.t_bancos._L_msgsError.Add(0);
                    }
                    _valida_codigos.validacionescontpaqi.t_bancos.codigoscontpaqi.Add(Convert.ToInt32(tipoDoc));
                }
                #endregion

                #region valida monedas

                lsdkMonedas.setSesion(lsdkSesion);

                foreach (var moneda in val.validacionescontpaqi.t_monedas.codigoscontpaqi)
                {
                    int Resultado = 1;
                    if (!b_test)
                    { 
                        Resultado = lsdkMonedas.buscaPorNumero(moneda); 
                    }

                    if (Resultado != 0)
                    {
                        _valida_codigos.validacionescontpaqi.t_monedas._L_msgsError.Add(1);
                    }
                    else
                    {
                        // es 0
                        _valida_codigos.validacionescontpaqi.t_monedas._L_msgsError.Add(0);
                    }
                    _valida_codigos.validacionescontpaqi.t_monedas.codigoscontpaqi.Add(Convert.ToInt32(moneda));
                }
                #endregion

                #region valida cuentas contables

                lsdkCuentas.setSesion(lsdkSesion);

                foreach (var cuentaContable in val.validacionescontpaqi.t_cuentascontables.codigoscontpaqi)
                {
                    int Resultado = 1;
                    if(!b_test)
                    {
                        Resultado = lsdkCuentas.buscaPorCodigo(cuentaContable);
                    }
                    
                    if(!b_test)
                    {
                        if (Resultado != 0)
                        {
                            // Si encontró la cuenta contable se buscará que se pueda afectar
                            if (lsdkCuentas.EsAfectable == 1)
                            {
                                //Si es afectable se da por validada
                                _valida_codigos.validacionescontpaqi.t_cuentascontables._L_msgsError.Add(1);
                            }
                            else
                            {
                                //En caso de no ser afectable se mandará un 2. 
                                _valida_codigos.validacionescontpaqi.t_cuentascontables._L_msgsError.Add(2);
                            }
                        }
                        else
                        {
                            _valida_codigos.validacionescontpaqi.t_cuentascontables._L_msgsError.Add(0);
                        }
                    }
                    else
                    {
                        _valida_codigos.validacionescontpaqi.t_cuentascontables._L_msgsError.Add(1);
                    }
            
                    _valida_codigos.validacionescontpaqi.t_cuentascontables.codigoscontpaqi.Add(cuentaContable.ToString());
                }
                #endregion

                _valida_codigos.validacionescontpaqi.empresa_id = val.empresa_id;
                _valida_codigos.validacionescontpaqi.empresacontpaqi = val.empresacontpaqi;
                _valida_codigos.t_stv_cuentas.cuenta_id = val.t_stv_cuentas.cuenta_id;
                _valida_codigos.t_stv_cuentas.servidor_id = val.t_stv_cuentas.servidor_id;

                //asigna el id del servidor
                n_servidor_id = val.t_stv_cuentas.servidor_id;
                n_ejercicio = val.ejercicio;
                n_empresacontpaqi_id = val.empresacontpaqi_id;
                basededatosvalidaciones = val.empresacontpaqi.ToString();

                polizaErroresAuth.n_empresa_id = val.empresa_id;
                polizasAuth.n_empresa_id = 1;
                
            }

            string json_params = JsonConvert.SerializeObject(_valida_codigos);

            Polizas_pendientes(json_params);
            }
     
        public void Polizas_pendientes(string json_params)
        {
            D_RETURN _D_return = new D_RETURN()
            {
                E_return = ERRORES_WS.OK,
                s_msgError = string.Empty,
                s_proceso = "WBS Pólizas pendientes"
            };
  
            _D_return.E_return = (ERRORES_WS)servicio.polizas_pendientes(polizasAuth, json_params, out WBS_s_msgError, out json_data_polizas_pendientes);

            _D_return.s_msgError = WBS_s_msgError;

            if (_D_return.E_return > ERRORES_WS.OK_WARNING)
            {
                O_logger.Error(_D_return.mensajeParaLog());
            }
            else
            {
                O_logger.Info(_D_return.mensajeParaLog());
            }

            dynamic json = JsonConvert.DeserializeObject(json_data_polizas_pendientes);

            //verificar si está vacio el json
            if (json.s_error != "" || json.s_error == null)
            {
                //TODO: Aquí se mandará a llamar el web service para el generado de error
                _D_return.E_return = (ERRORES_WS)servicio.poliza_errores(polizaErroresAuth, _D_return.s_msgError, n_ejercicio, n_empresacontpaqi_id, json_params, out WBS_s_msgError, out json_data_poliza_errores);

                _D_return.s_msgError = WBS_s_msgError + " : " + json.s_error.ToString();
                _D_return.s_proceso = "WBS póliza errores ";

                if (_D_return.E_return > ERRORES_WS.OK_WARNING)
                {
                    O_logger.Error(_D_return.mensajeParaLog());
                    MessageBox.Show(WBS_s_msgError, "Aviso",MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    O_logger.Info(_D_return.mensajeParaLog());
                }
                EstadoCoordinadorEvent(E_ESTADO.PENDIENTE);
            }
            else
            {
                foreach (var jsonCuenta in json.datos)
                {
                    //iniciar sesión en el SDK
                    inicio_sesion_compaqi();

                    if ((lsdkSesion.conexionActiva == 1) && (lsdkSesion.ingresoUsuario == 1))
                    {
                        //se abre la empresa
                        Abre_Empresa(jsonCuenta.empresacontpaqi.ToString());
                    }

                    //se asigna el nombre de la base de datos
                    s_basededatos = jsonCuenta.empresacontpaqi.ToString();

                    #region valida rfc
                    //string empresaWS = jsonCuenta.rfc;
                    //string empresaSL = SDK.lsdkEmpresa.RFC;

                    //if (empresaWS == empresaSL)
                    //{
                    //    MessageBox.Show("Son iguales");
                    //}
                    //else
                    //{
                    //    MessageBox.Show("Los rfc's no coinciden : " + "wbService : " + empresaWS + " Servidor Local : " + empresaSL);
                    //}
                    #endregion

                    #region polizas cerradas
                    foreach (int poliza_cerrada in jsonCuenta.polizas_cerradas_ids)
                    {
                        //pasar Sesión iniciada
                        lsdkPoliza.setSesion(lsdkSesion);

                        lsdkEmpresa.setSesion(lsdkSesion);

                        //lblEmpresa.Text = lsdkEmpresa.Nombre;
                        EstadoCoordinadorEvent(E_ESTADO.OBTENIENDO_POLIZA, elapsedTime, _n_contar_polizas, _n_contar_movimientos, _n_contar_ingresos, lsdkEmpresa.Nombre);

                        //inicializar Objeto
                        lsdkPoliza.iniciarInfo();

                        int n_poliza_id = Convert.ToInt32(poliza_cerrada);

                        polizasAuthObtener.n_empresa_id = jsonCuenta.empresa_id;

                        _D_return.E_return = (ERRORES_WS)servicio.poliza_obtener(polizasAuthObtener, json_params, n_poliza_id, out WBS_s_msgError, out json_data_poliza_obtener);

                        _D_return.s_msgError = WBS_s_msgError;
                        _D_return.s_proceso = "WBS Obtener póliza";

                        if (_D_return.E_return > ERRORES_WS.OK_WARNING)
                        {
                            O_logger.Error(_D_return.mensajeParaLog());
                        }
                        else
                        {
                            O_logger.Info(_D_return.mensajeParaLog());
                        }

                        dynamic json2 = null;
                        json2 = JsonConvert.DeserializeObject(json_data_poliza_obtener);

                        Valida_Rfc(json2);
                        Encabezado_Poliza(json2);
                        Registrar_Movimientos(json2, n_poliza_id);
                        Crear_Poliza(n_poliza_id);
                        Asociar_Cfdis(json2);
                        Registrar_Ingresos(json2, n_poliza_id);
                        Documentos_Bancarios(json2);

                        #region valida rfc de cliente e insertarlo si no está registrado

                        //lsdkCliente.setSesion(lsdkSesion);
                        //int indice = 0;
                        //string NumCod = "";
                        //foreach (var cliente in json2.validacionescontpaqi.t_clientes.rfcs)
                        //{
                        //    EstadoCoordinadorEvent(E_ESTADO.VALIDANDO_RFC, elapsedTime, _n_contar_polizas, _n_contar_movimientos, _n_contar_ingresos, lsdkEmpresa.Nombre);
                        //    if (cliente == "XAXX010101000")
                        //    {
                        //        //Si el RFC del cliente es igual a Venta en publico en general se procede a comparar la razón social.
                        //        int nombre = lsdkCliente.consultaPorNombre_buscaPorLlave(json2.validacionescontpaqi.t_clientes.razonsocial[indice]);

                        //        if (nombre != 0)
                        //        {
                        //            //Econtró no insertar
                        //        }
                        //        else
                        //        {
                        //            //No econtró: insertar
                        //            lsdkCliente.iniciarInfo();

                        //            NumCod = lsdkCliente.getSiguienteCodigo();
                        //            int convertir = Convert.ToInt32(NumCod);
                        //            NumCod = Convert.ToString(convertir);

                        //            lsdkCliente.EsCliente = 1;
                        //            lsdkCliente.Codigo = NumCod;
                        //            lsdkCliente.FechaRegistro = DateTime.Now;
                        //            lsdkCliente.Nombre = json2.validacionescontpaqi.t_clientes.razonsocial[indice];

                        //            lsdkCliente.RFC = json2.validacionescontpaqi.t_clientes.rfcs[indice];

                        //            lsdkCliente.agregaCliente(lsdkCliente);

                        //            lsdkCliente.crea();

                        //            _cerrar_poliza_params.t_clientes.rfcs.Add(json2.validacionescontpaqi.t_clientes.rfcs[indice].ToString());
                        //            _cerrar_poliza_params.t_clientes.razonessociales.Add(json2.validacionescontpaqi.t_clientes.razonsocial[indice].ToString());
                        //            _cerrar_poliza_params.t_clientes.codigoscontpaqi.Add(Convert.ToInt32(NumCod));

                        //            _D_return.s_proceso = "Registrando RFC:";
                        //            _D_return.s_msgError = json2.validacionescontpaqi.t_clientes.rfcs[indice].ToString() + " registrado.";

                        //            O_logger.Info(_D_return.mensajeParaLog());
                        //        }

                        //    }
                        //    else
                        //    {
                        //        //Si no es igual al RFC venta publico en general se hace una busqueda por el RFC para ver si no está adentro del sistema CONTPAQi.
                        //        int rfc = lsdkCliente.consultaPorRFC_buscaPorLlave(cliente);

                        //        if (rfc != 0)
                        //        {
                        //            //Econtró, ahora se verificará que la razón social exista en CONTPAQi. si no lo insertará. 
                        //            int _n_razonsocial = lsdkCliente.consultaPorNombre_buscaPorLlave(json2.validacionescontpaqi.t_clientes.razonsocial[indice]);

                        //            if (_n_razonsocial != 0)
                        //            {

                        //                //Si existe entonces significa que el RFC y la razón social existen en CONTPAQi, no se inserta. 
                        //            }
                        //            else
                        //            {
                        //                //De lo contrario hará la creación de un nuevo cliente.
                        //                lsdkCliente.iniciarInfo();

                        //                NumCod = lsdkCliente.getSiguienteCodigo();
                        //                int convertir = Convert.ToInt32(NumCod);
                        //                NumCod = Convert.ToString(convertir);

                        //                lsdkCliente.EsCliente = 1;
                        //                lsdkCliente.Codigo = NumCod;
                        //                lsdkCliente.FechaRegistro = DateTime.Now;
                        //                lsdkCliente.Nombre = json2.validacionescontpaqi.t_clientes.razonsocial[indice];

                        //                lsdkCliente.RFC = json2.validacionescontpaqi.t_clientes.rfcs[indice];

                        //                lsdkCliente.agregaCliente(lsdkCliente);

                        //                int lRe = 0;
                        //                lRe = lsdkCliente.crea();

                        //                _cerrar_poliza_params.t_clientes.rfcs.Add(json2.validacionescontpaqi.t_clientes.rfcs[indice].ToString());
                        //                _cerrar_poliza_params.t_clientes.razonessociales.Add(json2.validacionescontpaqi.t_clientes.razonsocial[indice].ToString());
                        //                _cerrar_poliza_params.t_clientes.codigoscontpaqi.Add(Convert.ToInt32(NumCod));
                        //            }

                        //        }
                        //        else
                        //        {
                        //            //No encontrado
                        //            lsdkCliente.iniciarInfo();

                        //            NumCod = lsdkCliente.getSiguienteCodigo();
                        //            int convertir = Convert.ToInt32(NumCod);
                        //            NumCod = Convert.ToString(convertir);

                        //            lsdkCliente.EsCliente = 1;
                        //            lsdkCliente.Codigo = NumCod;
                        //            lsdkCliente.FechaRegistro = DateTime.Now;
                        //            lsdkCliente.Nombre = json2.validacionescontpaqi.t_clientes.razonsocial[indice];

                        //            lsdkCliente.RFC = json2.validacionescontpaqi.t_clientes.rfcs[indice];

                        //            lsdkCliente.agregaCliente(lsdkCliente);

                        //            int lRe = 0;
                        //            lRe = lsdkCliente.crea();

                        //            _cerrar_poliza_params.t_clientes.rfcs.Add(json2.validacionescontpaqi.t_clientes.rfcs[indice].ToString());
                        //            _cerrar_poliza_params.t_clientes.razonessociales.Add(json2.validacionescontpaqi.t_clientes.razonsocial[indice].ToString());
                        //            _cerrar_poliza_params.t_clientes.codigoscontpaqi.Add(Convert.ToInt32(NumCod));

                        //        }
                        //    }
                        //    indice++;
                        //}
                        #endregion
                        #region CREAR ENCABEZADO DE PÓLIZA

                        //lsdkPoliza.iniciarInfo();

                        //if (json2.encabezado.tipopoliza == 1)
                        //    lsdkPoliza.Tipo = ETIPOPOLIZA.TIPO_INGRESOS;          //diario=3 egresos=2 estadisticas= ingresos=1
                        //if (json2.encabezado.tipopoliza == 2)
                        //    lsdkPoliza.Tipo = ETIPOPOLIZA.TIPO_EGRESOS;
                        //if (json2.encabezado.tipopoliza == 3)
                        //    lsdkPoliza.Tipo = ETIPOPOLIZA.TIPO_DIARIO;
                        //if (json2.encabezado.tipopoliza == 4)
                        //    lsdkPoliza.Tipo = ETIPOPOLIZA.TIPO_ESTADISTICAS;
                        //if (json2.encabezado.tipopoliza == 5)
                        //    lsdkPoliza.Tipo = ETIPOPOLIZA.TIPO_ORDEN;

                        //DateTime dt;
                        //DateTime.TryParseExact(json2.encabezado.fecha.ToString(), "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);

                        //if (json2.encabezado.clase == 0)
                        //{
                        //    lsdkPoliza.Clase = ECLASEPOLIZA.CLASE_SINAFECTAR;
                        //}
                        //else
                        //{
                        //    lsdkPoliza.Clase = ECLASEPOLIZA.CLASE_AFECTAR;
                        //}

                        //lsdkPoliza.Impresa = json2.encabezado.poliza_impresa;
                        //lsdkPoliza.Diario = json2.encabezado.diarioespecial;
                        //lsdkPoliza.Fecha = dt; //DateTime.Today;
                        //lsdkPoliza.SistOrigen = ESISTORIGEN.ORIG_CONTPAQNG;
                        //lsdkPoliza.Ajuste = json2.encabezado.ajuste;
                        //lsdkPoliza.Concepto = json2.encabezado.concepto;

                        ////Se crea el GUID (Identificador global para CONTPAQi)
                        //string guid = System.Guid.NewGuid().ToString();

                        //lsdkPoliza.Guid = guid.ToUpper();

                        ////se asigna el valor del json a las variables
                        //var ejercicio = json2.encabezado.ejercicio;
                        //var periodo = json2.encabezado.periodo;
                        //var tipopoliza = json2.encabezado.tipopoliza;

                        //int ultimo = lsdkPoliza.getUltimoNumero(ejercicio, periodo, tipopoliza);
                        //lsdkPoliza.Numero = ultimo++; // n_poliza_id;
                        #endregion
                        #region Registrar Movimientos

                        //foreach (var i in json2.t_movimientos.referencias)
                        //{
                        //    _n_nummovto++;

                        //    var cargos = json2.t_movimientos.cargoabonos[contador];

                        //    lsdkMovto.setSesion(lsdkSesion);
                        //    lsdkMovto.iniciarInfo();

                        //    //Se llenan las propiedades del objeto movimientos
                        //    lsdkMovto.NumMovto = _n_nummovto;
                        //    lsdkMovto.CodigoCuenta = json2.t_movimientos.cuentascontables[contador];
                        //    if (Convert.ToInt32(cargos) == 0)
                        //    {
                        //        lsdkMovto.TipoMovto = ETIPOIMPORTEMOVPOLIZA.MOVPOLIZA_CARGO;
                        //    }
                        //    else
                        //    {
                        //        lsdkMovto.TipoMovto = ETIPOIMPORTEMOVPOLIZA.MOVPOLIZA_ABONO;
                        //    }
                        //    lsdkMovto.Importe = json2.t_movimientos.importes_cargos_abonos[contador];
                        //    lsdkMovto.ImporteME = json2.t_movimientos.importesmonedasextranjeras[contador];
                        //    lsdkMovto.Diario = json2.t_movimientos.diariosespeciales[contador];
                        //    lsdkMovto.SegmentoNegocio = json2.t_movimientos.segmentosnegocio[contador];
                        //    lsdkMovto.Concepto = json2.t_movimientos.descripciones[contador];
                        //    lsdkMovto.Referencia = json2.t_movimientos.referencias[contador];

                        //    //Se crea el GUID (Identificador global para CONTPAQi)
                        //    string guid_encabezado = System.Guid.NewGuid().ToString();

                        //    lsdkMovto.Guid = guid_encabezado.ToUpper();

                        //    //Se asocia el GUID a la lista. 
                        //    //Se reemplaza el guid que viene vacío por el del movimiento.
                        //    json2.t_movimientos.guids[contador] = lsdkMovto.Guid;

                        //    //crea movimiento  
                        //    int lResultMov = lsdkPoliza.agregaMovimiento(lsdkMovto);
                        //    if (lResultMov != 0)
                        //    {
                        //        //Se registró la póliza;
                        //        _n_contar_movimientos++;
                        //        contador++;
                        //    }
                        //    else
                        //    {
                        //        MessageBox.Show("Movimiento SKD: " + lsdkPoliza.getMensajeError());
                        //        _s_error_movimiento = lsdkPoliza.getMensajeError() + " Error encontrado al agregar el movimiento:" + _n_nummovto + " en la póliza con el ID: " + n_poliza_id.ToString();
                        //        _D_return.s_msgError = _s_error_movimiento;
                        //        _D_return.E_return = ERRORES_WS.NOK_ERROR;
                        //        _D_return.s_proceso = "Registrar movimiento";

                        //        O_logger.Error(_D_return.mensajeParaLog());
                        //        //TODO: Aquí meter el s_error con el error que haya encontrado en agregar el movimiento.
                        //    }
                        //    EstadoCoordinadorEvent(E_ESTADO.REGISTRANDO_MOVIMIENTOS, elapsedTime, _n_contar_polizas, _n_contar_movimientos, _n_contar_ingresos, lsdkEmpresa.Nombre);

                        //}
                        //contador = 0;//indice de los datos del json
                        //_n_nummovto = 0;
                        #endregion
                        #region Crear Póliza
                        //int folio = 0;
                        //folio = lsdkPoliza.Numero;

                        //int ResultPol = lsdkPoliza.crea();

                        //if (ResultPol != 0)
                        //{
                        //    //Se registró la póliza.;
                        //    _n_contar_polizas++;
                        //}
                        //else
                        //{
                        //    MessageBox.Show("Póliza SKD: " + lsdkPoliza.getMensajeError());
                        //    _s_error_poliza = lsdkPoliza.getMensajeError() + " Error encontrado al crear la póliza con el ID: " + n_poliza_id.ToString();
                        //    _D_return.s_msgError = _s_error_poliza;
                        //    _D_return.E_return = ERRORES_WS.NOK_ERROR;
                        //    _D_return.s_proceso = "Crear póliza";

                        //    O_logger.Error(_D_return.mensajeParaLog());
                        //}
                        //EstadoCoordinadorEvent(E_ESTADO.CREANDO_POLIZA, elapsedTime, _n_contar_polizas, _n_contar_movimientos, _n_contar_ingresos, lsdkEmpresa.Nombre);
                        #endregion
                        #region Asociación de CFDIs
                        ////Se crea el objeto para la asociación del CFDI.
                        //TSdkAsocCFDI asocCFDI = new TSdkAsocCFDI();

                        //foreach (var _s_uuid in json2.t_uuids.polizauuids)
                        //{
                        //    EstadoCoordinadorEvent(E_ESTADO.ASOCIANDO_CFDIS, elapsedTime, _n_contar_polizas, _n_contar_movimientos, _n_contar_ingresos, lsdkEmpresa.Nombre);
                        //    //Se asocia el UUID a la poliza.
                        //    asocCFDI.iniciarInfo();
                        //    asocCFDI.setSesion(lsdkSesion);

                        //    asocCFDI.GuidDocumento = lsdkPoliza.Guid;
                        //    asocCFDI.TipoAsoc = ETIPOASOCCFDI.TIPOASOC_POLIZA;
                        //    asocCFDI.agregaUUID(_s_uuid);

                        //    asocCFDI.crea();

                        //    int contador_guids = 0;
                        //    foreach (var _s_guids in json2.t_movimientos.guids)
                        //    {
                        //        //For para recorrer los GUIDs de los movimientos
                        //        if (json2.t_movimientos.uuids[contador_guids] == _s_uuid)
                        //        {
                        //            //Si el uuid es igual al que se trae de arriba entonces se hace la relación

                        //            asocCFDI.iniciarInfo();
                        //            asocCFDI.setSesion(lsdkSesion);

                        //            asocCFDI.GuidDocumento = _s_guids;
                        //            asocCFDI.TipoAsoc = ETIPOASOCCFDI.TIPOASOC_MOVTOPOLIZA;
                        //            asocCFDI.agregaUUID(_s_uuid);

                        //            asocCFDI.crea();
                        //        }
                        //        else
                        //        {
                        //            //No hace nada.
                        //        }

                        //        contador_guids++;
                        //    }
                        //}
                        #endregion
                        #region  Registrar Ingresos
                        //DateTime fechaAplicacion;
                        //DateTime fechaIngreso;
                        //foreach (var i in json2.t_documentosbancarios.referencias)
                        //{
                        //    int id = lsdkPoliza.Id;

                        //    DateTime.TryParseExact(json2.t_documentosbancarios.fechasaplicacion[contador].ToString(), "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out fechaAplicacion);
                        //    DateTime.TryParseExact(json2.t_documentosbancarios.fechas_documentosbancarios[contador].ToString(), "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out fechaIngreso);

                        //    lsdkIngreso.setSesion(lsdkSesion);
                        //    lsdkIngreso.iniciarInfo();
                        //    //--------------- Info Ingreso -----------------

                        //    lsdkIngreso.TipoDocumento = json2.t_documentosbancarios.tipodocumentos[contador];
                        //    //TODO: La fecha debe de ser la que se trae del Json.
                        //    lsdkIngreso.Fecha = fechaIngreso;
                        //    lsdkIngreso.FechaAplicacion = fechaAplicacion;

                        //    lsdkCliente.iniciarInfo();
                        //    int busca_codigo = lsdkCliente.consultaPorNombre_buscaPorLlave(json2.t_documentosbancarios.beneficiariospagadores[contador]);

                        //    lsdkIngreso.CodigoPersona = lsdkCliente.CodigoPersona;
                        //    lsdkIngreso.BeneficiarioPagador = json2.t_documentosbancarios.beneficiariospagadores[contador];

                        //    TSdkCuentaCheque lsdkcheques = new TSdkCuentaCheque();
                        //    lsdkcheques.setSesion(lsdkSesion);
                        //    int ResCheque = lsdkcheques.buscaPorCodigo(json2.t_documentosbancarios.idscuentascheques[contador]);
                        //    int _n_cuentacheques_id = lsdkcheques.Id;

                        //    // Este es el ID de la base de datos para cuenta de cheques no el código de CONTPAQi. 
                        //    lsdkIngreso.IdCuentaCheques = lsdkcheques.Id;
                        //    lsdkIngreso.Total = json2.t_documentosbancarios.totales[contador];
                        //    lsdkIngreso.Referencia = json2.t_documentosbancarios.referencias[contador];
                        //    lsdkIngreso.Origen = json2.t_documentosbancarios.documentosdeorigen[contador];
                        //    lsdkIngreso.BancoOrigen = json2.t_documentosbancarios.bancosorigen[contador];
                        //    lsdkIngreso.CuentaOrigen = json2.t_documentosbancarios.cuentasorigen[contador];
                        //    lsdkIngreso.MetodoDePago = json2.t_documentosbancarios.otrosmetodospago[contador];
                        //    //SDK.lsdkIngreso.Guid = json2.t_documentosbancarios.guids[contador];
                        //    //rfc, banco Extranjero, UUIDRep, NodoPago
                        //    lsdkIngreso.TipoCambio = json2.t_documentosbancarios.tiposcambios[contador];
                        //    lsdkIngreso.NumeroCheque = json2.t_documentosbancarios.numeroscheques[contador];
                        //    lsdkIngreso.CodigoMonedaTipoCambio = json2.t_documentosbancarios.codigosmonedas_tipocambio[contador];
                        //    lsdkIngreso.IdPoliza = id;
                        //    lsdkIngreso.EjercicioAp = fechaAplicacion.Year;
                        //    lsdkIngreso.PeriodoAp = fechaAplicacion.Month;
                        //    lsdkIngreso.EjercicioPol = dt.Year;
                        //    lsdkIngreso.PeriodoPol = dt.Month;
                        //    lsdkIngreso.TipoPol = tipopoliza;

                        //    #region obtener último folio de ingreso
                        //    TSdkTipoDocumento lsdkTipoDocu = new TSdkTipoDocumento();
                        //    lsdkTipoDocu.setSesion(lsdkSesion);
                        //    lsdkTipoDocu.iniciarInfo();

                        //    string _sig_folio_ingreso = lsdkTipoDocu.getSiguienteFolio(json2.t_documentosbancarios.tipodocumentos[contador]);
                        //    #endregion

                        //    lsdkIngreso.Folio = _sig_folio_ingreso;

                        //    _cerrar_poliza_params.t_documentosbancarios.folios_documentopago.Add(Convert.ToInt32(_sig_folio_ingreso));
                        //    lsdkIngreso.NumPol = folio;
                        //    int lResultIng = lsdkIngreso.crea();

                        //    if (lResultIng != 0)
                        //    {
                        //        //Se registró el ingreso
                        //        _n_contar_ingresos++;
                        //        contador++;
                        //    }
                        //    else
                        //    {
                        //        MessageBox.Show("Ingreso SKD: " + lsdkIngreso.getMensajeError());
                        //        _s_error_ingreso = lsdkIngreso.getMensajeError() + " Error encontrado en el ingreso:" + _sig_folio_ingreso + " en la póliza con el ID: " + n_poliza_id.ToString();
                        //        _D_return.s_msgError = _s_error_ingreso;
                        //        _D_return.E_return = ERRORES_WS.NOK_ERROR;
                        //        _D_return.s_proceso = "Registrar ingreso";

                        //        O_logger.Error(_D_return.mensajeParaLog());

                        //        //Si hubo error que elimine el ingreso
                        //        lsdkIngreso.borra();
                        //    }
                        //    EstadoCoordinadorEvent(E_ESTADO.REGISTRANDO_INGRESOS, elapsedTime, _n_contar_polizas, _n_contar_movimientos, _n_contar_ingresos, lsdkEmpresa.Nombre);
                        //}
                        //contador = 0;//indice de los datos del json
                        #endregion
                        //int n_folio = lsdkPoliza.Numero;
                        #region  documentos bancarios
                        //foreach (var asientos in json2.t_documentosbancarios.asientos_id)
                        //{
                        //    _cerrar_poliza_params.t_documentosbancarios.asientos_id.Add(Convert.ToInt32(json2.t_documentosbancarios.asientos_id[contador]));
                        //    //_cerrar_poliza_params.t_documetosbancarios.folios_documentopago.Add(Convert.ToInt32(mensage));
                        //    _cerrar_poliza_params.t_documentosbancarios.tipodocumentobancario_ventacontado_id.Add(Convert.ToInt32(json2.t_documentosbancarios.bancos_tiposdocumentos_venta_contado[contador]));

                        //    contador++;
                        //}
                        #endregion

                        contador = 0;

                        string json_para = JsonConvert.SerializeObject(_cerrar_poliza_params);

                        polizasAuthCerrar.n_empresa_id = jsonCuenta.empresa_id;
                        string _s_error = "";
                        if ((_s_error_ingreso != "") || (_s_error_movimiento != "") || (_s_error_poliza != ""))
                        {
                            //Si los errores son diferentes de vacío entonces significa que trajo algún error.
                            //Si se obtuvo error también se deberá borrar la póliza del sistema de CONTPAQi.
                            lsdkPoliza.borra();

                            _s_error = "Errores a nivel póliza: " + _s_error_poliza + " Errores a nivel movimientos: " + _s_error_movimiento + ", Errores a nivel ingreso: " + _s_error_ingreso;
                            _D_return.E_return = ERRORES_WS.NOK_ERROR;
                            _D_return.s_msgError = _s_error;
                            _D_return.s_proceso = "Cerrar póliza";
                            O_logger.Error(_D_return.mensajeParaLog());
                        }
                        else
                        {
                            //No hace nada, pass
                        }

                        //int PolizasCerradas = servicio.poliza_cerrar(polizasAuthCerrar, json_params, _s_error, n_poliza_id, n_folio, n_empresacontpaqi_id,n_servidor_id, n_result, out WBS_s_msgError, out json_data);

                        _cerrar_poliza_params = new CerrarPoliza_params();

                        //Se reinician los objetos para la proxima póliza. 
                        lsdkPoliza = new TSdkPoliza();
                        lsdkMovto = new TSdkMovimientoPoliza();
                        lsdkIngreso = new TSdkIngreso();
                    }
                    #endregion
                }
                #region Tiempo
                sw.Stop();
                TimeSpan ts = sw.Elapsed;

                // Formato a la cadena.
                elapsedTime = string.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
                EstadoCoordinadorEvent(E_ESTADO.PROCESO_TERMINADO, elapsedTime, _n_contar_polizas, _n_contar_movimientos, _n_contar_ingresos, lsdkEmpresa.Nombre);

                #endregion

                _D_return.E_return = ERRORES_WS.OK;
                _D_return.s_msgError = "Proceso terminado: ";
                _D_return.s_proceso = elapsedTime;

                O_logger.Info(_D_return.mensajeParaLog());

                MessageBox.Show("Proceso terminado!! Tiempo: " + elapsedTime, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                _n_contar_ingresos = 0;
                _n_contar_movimientos = 0;
                _n_contar_polizas = 0;
                elapsedTime = "";
                

                lsdkPoliza = new TSdkPoliza();
                lsdkMovto = new TSdkMovimientoPoliza();
                lsdkIngreso = new TSdkIngreso();
                //TODO: Verificar si es necesario que la sesión se limpie cuando acabe una poliza
                lsdkSesion = new TSdkSesion();

                EstadoCoordinadorEvent(E_ESTADO.PENDIENTE, elapsedTime, _n_contar_polizas, _n_contar_movimientos, _n_contar_ingresos, "");
               
            }
        }

        private void Valida_Rfc(dynamic json_valida_rfc)
        {
            D_RETURN _D_return = new D_RETURN()
            {
                E_return = ERRORES_WS.OK,
                s_msgError = string.Empty,
                s_proceso = "WBS Validando RFC"
            };

            lsdkCliente.setSesion(lsdkSesion);
            int indice = 0;
            string NumCod = "";
            foreach (var cliente in json_valida_rfc.validacionescontpaqi.t_clientes.rfcs)
            {
                EstadoCoordinadorEvent(E_ESTADO.VALIDANDO_RFC, elapsedTime, _n_contar_polizas, _n_contar_movimientos, _n_contar_ingresos, lsdkEmpresa.Nombre);
                if (cliente == "XAXX010101000")
                {
                    //Si el RFC del cliente es igual a Venta en publico en general se procede a comparar la razón social.
                    int nombre = lsdkCliente.consultaPorNombre_buscaPorLlave(json_valida_rfc.validacionescontpaqi.t_clientes.razonsocial[indice]);

                    if (nombre != 0)
                    {
                        //Econtró no insertar
                    }
                    else
                    {
                        //No econtró: insertar
                        lsdkCliente.iniciarInfo();

                        NumCod = lsdkCliente.getSiguienteCodigo();
                        int convertir = Convert.ToInt32(NumCod);
                        NumCod = Convert.ToString(convertir);

                        lsdkCliente.EsCliente = 1;
                        lsdkCliente.Codigo = NumCod;
                        lsdkCliente.FechaRegistro = DateTime.Now;
                        lsdkCliente.Nombre = json_valida_rfc.validacionescontpaqi.t_clientes.razonsocial[indice];

                        lsdkCliente.RFC = json_valida_rfc.validacionescontpaqi.t_clientes.rfcs[indice];

                        lsdkCliente.agregaCliente(lsdkCliente);

                        lsdkCliente.crea();

                        _cerrar_poliza_params.t_clientes.rfcs.Add(json_valida_rfc.validacionescontpaqi.t_clientes.rfcs[indice].ToString());
                        _cerrar_poliza_params.t_clientes.razonessociales.Add(json_valida_rfc.validacionescontpaqi.t_clientes.razonsocial[indice].ToString());
                        _cerrar_poliza_params.t_clientes.codigoscontpaqi.Add(Convert.ToInt32(NumCod));

                        _D_return.s_proceso = "Registrando RFC:";
                        _D_return.s_msgError = json_valida_rfc.validacionescontpaqi.t_clientes.rfcs[indice].ToString() + " registrado.";

                        O_logger.Info(_D_return.mensajeParaLog());
                    }

                }
                else
                {
                    //Si no es igual al RFC venta publico en general se hace una busqueda por el RFC para ver si no está adentro del sistema CONTPAQi.
                    int rfc = lsdkCliente.consultaPorRFC_buscaPorLlave(cliente);

                    if (rfc != 0)
                    {
                        //Econtró, ahora se verificará que la razón social exista en CONTPAQi. si no lo insertará. 
                        int _n_razonsocial = lsdkCliente.consultaPorNombre_buscaPorLlave(json_valida_rfc.validacionescontpaqi.t_clientes.razonsocial[indice]);

                        if (_n_razonsocial != 0)
                        {

                            //Si existe entonces significa que el RFC y la razón social existen en CONTPAQi, no se inserta. 
                        }
                        else
                        {
                            //De lo contrario hará la creación de un nuevo cliente.
                            lsdkCliente.iniciarInfo();

                            NumCod = lsdkCliente.getSiguienteCodigo();
                            int convertir = Convert.ToInt32(NumCod);
                            NumCod = Convert.ToString(convertir);

                            lsdkCliente.EsCliente = 1;
                            lsdkCliente.Codigo = NumCod;
                            lsdkCliente.FechaRegistro = DateTime.Now;
                            lsdkCliente.Nombre = json_valida_rfc.validacionescontpaqi.t_clientes.razonsocial[indice];

                            lsdkCliente.RFC = json_valida_rfc.validacionescontpaqi.t_clientes.rfcs[indice];

                            lsdkCliente.agregaCliente(lsdkCliente);

                            int lRe = 0;
                            lRe = lsdkCliente.crea();

                            _cerrar_poliza_params.t_clientes.rfcs.Add(json_valida_rfc.validacionescontpaqi.t_clientes.rfcs[indice].ToString());
                            _cerrar_poliza_params.t_clientes.razonessociales.Add(json_valida_rfc.validacionescontpaqi.t_clientes.razonsocial[indice].ToString());
                            _cerrar_poliza_params.t_clientes.codigoscontpaqi.Add(Convert.ToInt32(NumCod));
                        }

                    }
                    else
                    {
                        //No encontrado
                        lsdkCliente.iniciarInfo();

                        NumCod = lsdkCliente.getSiguienteCodigo();
                        int convertir = Convert.ToInt32(NumCod);
                        NumCod = Convert.ToString(convertir);

                        lsdkCliente.EsCliente = 1;
                        lsdkCliente.Codigo = NumCod;
                        lsdkCliente.FechaRegistro = DateTime.Now;
                        lsdkCliente.Nombre = json_valida_rfc.validacionescontpaqi.t_clientes.razonsocial[indice];

                        lsdkCliente.RFC = json_valida_rfc.validacionescontpaqi.t_clientes.rfcs[indice];

                        lsdkCliente.agregaCliente(lsdkCliente);

                        int lRe = 0;
                        lRe = lsdkCliente.crea();

                        _cerrar_poliza_params.t_clientes.rfcs.Add(json_valida_rfc.validacionescontpaqi.t_clientes.rfcs[indice].ToString());
                        _cerrar_poliza_params.t_clientes.razonessociales.Add(json_valida_rfc.validacionescontpaqi.t_clientes.razonsocial[indice].ToString());
                        _cerrar_poliza_params.t_clientes.codigoscontpaqi.Add(Convert.ToInt32(NumCod));

                    }
                }
                indice++;
            }
        }
        private void Encabezado_Poliza(dynamic json_encabezado_poliza)
        {
            lsdkPoliza.iniciarInfo();

            if (json_encabezado_poliza.encabezado.tipopoliza == 1)
                lsdkPoliza.Tipo = ETIPOPOLIZA.TIPO_INGRESOS;          //diario=3 egresos=2 estadisticas= ingresos=1
            if (json_encabezado_poliza.encabezado.tipopoliza == 2)
                lsdkPoliza.Tipo = ETIPOPOLIZA.TIPO_EGRESOS;
            if (json_encabezado_poliza.encabezado.tipopoliza == 3)
                lsdkPoliza.Tipo = ETIPOPOLIZA.TIPO_DIARIO;
            if (json_encabezado_poliza.encabezado.tipopoliza == 4)
                lsdkPoliza.Tipo = ETIPOPOLIZA.TIPO_ESTADISTICAS;
            if (json_encabezado_poliza.encabezado.tipopoliza == 5)
                lsdkPoliza.Tipo = ETIPOPOLIZA.TIPO_ORDEN;

            DateTime dt;
            DateTime.TryParseExact(json_encabezado_poliza.encabezado.fecha.ToString(), "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);

            if (json_encabezado_poliza.encabezado.clase == 0)
            {
                lsdkPoliza.Clase = ECLASEPOLIZA.CLASE_SINAFECTAR;
            }
            else
            {
                lsdkPoliza.Clase = ECLASEPOLIZA.CLASE_AFECTAR;
            }

            lsdkPoliza.Impresa = json_encabezado_poliza.encabezado.poliza_impresa;
            lsdkPoliza.Diario = json_encabezado_poliza.encabezado.diarioespecial;
            lsdkPoliza.Fecha = dt; //DateTime.Today;
            lsdkPoliza.SistOrigen = ESISTORIGEN.ORIG_CONTPAQNG;
            lsdkPoliza.Ajuste = json_encabezado_poliza.encabezado.ajuste;
            lsdkPoliza.Concepto = json_encabezado_poliza.encabezado.concepto;

            //Se crea el GUID (Identificador global para CONTPAQi)
            string guid = System.Guid.NewGuid().ToString();

            lsdkPoliza.Guid = guid.ToUpper();

            //se asigna el valor del json a las variables
            var ejercicio = json_encabezado_poliza.encabezado.ejercicio;
            var periodo = json_encabezado_poliza.encabezado.periodo;
            var tipopoliza = json_encabezado_poliza.encabezado.tipopoliza;

            int ultimo = lsdkPoliza.getUltimoNumero(ejercicio, periodo, tipopoliza);
            lsdkPoliza.Numero = ultimo++; // n_poliza_id;
        }
        private void Registrar_Movimientos(dynamic json_movimientos, int n_poliza_id)
        {
            D_RETURN _D_return = new D_RETURN()
            {
                E_return = ERRORES_WS.OK,
                s_msgError = string.Empty,
                s_proceso = "WBS Registrando movimientos"
            };

            foreach (var i in json_movimientos.t_movimientos.referencias)
            {
                _n_nummovto++;

                var cargos = json_movimientos.t_movimientos.cargoabonos[contador];

                lsdkMovto.setSesion(lsdkSesion);
                lsdkMovto.iniciarInfo();

                //Se llenan las propiedades del objeto movimientos
                lsdkMovto.NumMovto = _n_nummovto;
                lsdkMovto.CodigoCuenta = json_movimientos.t_movimientos.cuentascontables[contador];
                if (Convert.ToInt32(cargos) == 0)
                {
                    lsdkMovto.TipoMovto = ETIPOIMPORTEMOVPOLIZA.MOVPOLIZA_CARGO;
                }
                else
                {
                    lsdkMovto.TipoMovto = ETIPOIMPORTEMOVPOLIZA.MOVPOLIZA_ABONO;
                }
                lsdkMovto.Importe = json_movimientos.t_movimientos.importes_cargos_abonos[contador];
                lsdkMovto.ImporteME = json_movimientos.t_movimientos.importesmonedasextranjeras[contador];
                lsdkMovto.Diario = json_movimientos.t_movimientos.diariosespeciales[contador];
                lsdkMovto.SegmentoNegocio = json_movimientos.t_movimientos.segmentosnegocio[contador];
                lsdkMovto.Concepto = json_movimientos.t_movimientos.descripciones[contador];
                lsdkMovto.Referencia = json_movimientos.t_movimientos.referencias[contador];

                //Se crea el GUID (Identificador global para CONTPAQi)
                string guid_encabezado = System.Guid.NewGuid().ToString();

                lsdkMovto.Guid = guid_encabezado.ToUpper();

                //Se asocia el GUID a la lista. 
                //Se reemplaza el guid que viene vacío por el del movimiento.
                json_movimientos.t_movimientos.guids[contador] = lsdkMovto.Guid;

                //crea movimiento  
                int lResultMov = lsdkPoliza.agregaMovimiento(lsdkMovto);
                if (lResultMov != 0)
                {
                    //Se registró la póliza;
                    _n_contar_movimientos++;
                    contador++;
                }
                else
                {
                    MessageBox.Show("Movimiento SKD: " + lsdkPoliza.getMensajeError());
                    _s_error_movimiento = lsdkPoliza.getMensajeError() + " Error encontrado al agregar el movimiento:" + _n_nummovto + " en la póliza con el ID: " + n_poliza_id.ToString();
                    _D_return.s_msgError = _s_error_movimiento;
                    _D_return.E_return = ERRORES_WS.NOK_ERROR;
                    _D_return.s_proceso = "Registrar movimiento";

                    O_logger.Error(_D_return.mensajeParaLog());
                    //TODO: Aquí meter el s_error con el error que haya encontrado en agregar el movimiento.
                }
                EstadoCoordinadorEvent(E_ESTADO.REGISTRANDO_MOVIMIENTOS, elapsedTime, _n_contar_polizas, _n_contar_movimientos, _n_contar_ingresos, lsdkEmpresa.Nombre);

            }
            contador = 0;//indice de los datos del json
            _n_nummovto = 0;
        }
        private void Crear_Poliza(int n_poliza_id)
        {
            D_RETURN _D_return = new D_RETURN()
            {
                E_return = ERRORES_WS.OK,
                s_msgError = string.Empty,
                s_proceso = "WBS Creando Póliza"
            };

            //int folio = 0;
            //folio = lsdkPoliza.Numero;

            int ResultPol = lsdkPoliza.crea();

            if (ResultPol != 0)
            {
                //Se registró la póliza.;
                _n_contar_polizas++;
            }
            else
            {
                MessageBox.Show("Póliza SKD: " + lsdkPoliza.getMensajeError());
                _s_error_poliza = lsdkPoliza.getMensajeError() + " Error encontrado al crear la póliza con el ID: " + n_poliza_id.ToString();
                _D_return.s_msgError = _s_error_poliza;
                _D_return.E_return = ERRORES_WS.NOK_ERROR;
                _D_return.s_proceso = "Crear póliza";

                O_logger.Error(_D_return.mensajeParaLog());
            }
            EstadoCoordinadorEvent(E_ESTADO.CREANDO_POLIZA, elapsedTime, _n_contar_polizas, _n_contar_movimientos, _n_contar_ingresos, lsdkEmpresa.Nombre);
        }
        private void Asociar_Cfdis(dynamic json_asociar)
        {
            //Se crea el objeto para la asociación del CFDI.
            TSdkAsocCFDI asocCFDI = new TSdkAsocCFDI();

            foreach (var _s_uuid in json_asociar.t_uuids.polizauuids)
            {
                EstadoCoordinadorEvent(E_ESTADO.ASOCIANDO_CFDIS, elapsedTime, _n_contar_polizas, _n_contar_movimientos, _n_contar_ingresos, lsdkEmpresa.Nombre);
                //Se asocia el UUID a la poliza.
                asocCFDI.iniciarInfo();
                asocCFDI.setSesion(lsdkSesion);

                asocCFDI.GuidDocumento = lsdkPoliza.Guid;
                asocCFDI.TipoAsoc = ETIPOASOCCFDI.TIPOASOC_POLIZA;
                asocCFDI.agregaUUID(_s_uuid);

                asocCFDI.crea();

                int contador_guids = 0;
                foreach (var _s_guids in json_asociar.t_movimientos.guids)
                {
                    //For para recorrer los GUIDs de los movimientos
                    if (json_asociar.t_movimientos.uuids[contador_guids] == _s_uuid)
                    {
                        //Si el uuid es igual al que se trae de arriba entonces se hace la relación

                        asocCFDI.iniciarInfo();
                        asocCFDI.setSesion(lsdkSesion);

                        asocCFDI.GuidDocumento = _s_guids;
                        asocCFDI.TipoAsoc = ETIPOASOCCFDI.TIPOASOC_MOVTOPOLIZA;
                        asocCFDI.agregaUUID(_s_uuid);

                        asocCFDI.crea();
                    }
                    else
                    {
                        //No hace nada.
                    }

                    contador_guids++;
                }
            }
        }
        private void Registrar_Ingresos(dynamic json_ingresos, int n_poliza_id)
        {
            D_RETURN _D_return = new D_RETURN()
            {
                E_return = ERRORES_WS.OK,
                s_msgError = string.Empty,
                s_proceso = "WBS Registrando ingresos"
            };

            DateTime fechaAplicacion;
            DateTime fechaIngreso;
            DateTime dt;

            foreach (var i in json_ingresos.t_documentosbancarios.referencias)
            {
                var tipopoliza = json_ingresos.encabezado.tipopoliza;

                int id = lsdkPoliza.Id;

                int folio_poliza = 0;
                folio_poliza = lsdkPoliza.Numero;

                DateTime.TryParseExact(json_ingresos.encabezado.fecha.ToString(), "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);

                DateTime.TryParseExact(json_ingresos.t_documentosbancarios.fechasaplicacion[contador].ToString(), "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out fechaAplicacion);
                DateTime.TryParseExact(json_ingresos.t_documentosbancarios.fechas_documentosbancarios[contador].ToString(), "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out fechaIngreso);

                lsdkIngreso.setSesion(lsdkSesion);
                lsdkIngreso.iniciarInfo();
                //--------------- Info Ingreso -----------------

                lsdkIngreso.TipoDocumento = json_ingresos.t_documentosbancarios.tipodocumentos[contador];
                //TODO: La fecha debe de ser la que se trae del Json.
                lsdkIngreso.Fecha = fechaIngreso;
                lsdkIngreso.FechaAplicacion = fechaAplicacion;

                lsdkCliente.iniciarInfo();
                int busca_codigo = lsdkCliente.consultaPorNombre_buscaPorLlave(json_ingresos.t_documentosbancarios.beneficiariospagadores[contador]);

                lsdkIngreso.CodigoPersona = lsdkCliente.CodigoPersona;
                lsdkIngreso.BeneficiarioPagador = json_ingresos.t_documentosbancarios.beneficiariospagadores[contador];

                TSdkCuentaCheque lsdkcheques = new TSdkCuentaCheque();
                lsdkcheques.setSesion(lsdkSesion);
                int ResCheque = lsdkcheques.buscaPorCodigo(json_ingresos.t_documentosbancarios.idscuentascheques[contador]);
                int _n_cuentacheques_id = lsdkcheques.Id;

                // Este es el ID de la base de datos para cuenta de cheques no el código de CONTPAQi. 
                lsdkIngreso.IdCuentaCheques = lsdkcheques.Id;
                lsdkIngreso.Total = json_ingresos.t_documentosbancarios.totales[contador];
                lsdkIngreso.Referencia = json_ingresos.t_documentosbancarios.referencias[contador];
                lsdkIngreso.Origen = json_ingresos.t_documentosbancarios.documentosdeorigen[contador];
                lsdkIngreso.BancoOrigen = json_ingresos.t_documentosbancarios.bancosorigen[contador];
                lsdkIngreso.CuentaOrigen = json_ingresos.t_documentosbancarios.cuentasorigen[contador];
                lsdkIngreso.MetodoDePago = json_ingresos.t_documentosbancarios.otrosmetodospago[contador];
                //SDK.lsdkIngreso.Guid = json2.t_documentosbancarios.guids[contador];
                //rfc, banco Extranjero, UUIDRep, NodoPago
                lsdkIngreso.TipoCambio = json_ingresos.t_documentosbancarios.tiposcambios[contador];
                lsdkIngreso.NumeroCheque = json_ingresos.t_documentosbancarios.numeroscheques[contador];
                lsdkIngreso.CodigoMonedaTipoCambio = json_ingresos.t_documentosbancarios.codigosmonedas_tipocambio[contador];
                lsdkIngreso.IdPoliza = id;
                lsdkIngreso.EjercicioAp = fechaAplicacion.Year;
                lsdkIngreso.PeriodoAp = fechaAplicacion.Month;
                lsdkIngreso.EjercicioPol = dt.Year;
                lsdkIngreso.PeriodoPol = dt.Month;
                lsdkIngreso.TipoPol = tipopoliza;

                #region obtener último folio de ingreso
                TSdkTipoDocumento lsdkTipoDocu = new TSdkTipoDocumento();
                lsdkTipoDocu.setSesion(lsdkSesion);
                lsdkTipoDocu.iniciarInfo();

                string _sig_folio_ingreso = lsdkTipoDocu.getSiguienteFolio(json_ingresos.t_documentosbancarios.tipodocumentos[contador]);
                #endregion

                lsdkIngreso.Folio = _sig_folio_ingreso;

                _cerrar_poliza_params.t_documentosbancarios.folios_documentopago.Add(Convert.ToInt32(_sig_folio_ingreso));
                lsdkIngreso.NumPol = folio_poliza;
                int lResultIng = lsdkIngreso.crea();

                if (lResultIng != 0)
                {
                    //Se registró el ingreso
                    _n_contar_ingresos++;
                    contador++;
                }
                else
                {
                    MessageBox.Show("Ingreso SKD: " + lsdkIngreso.getMensajeError());
                    _s_error_ingreso = lsdkIngreso.getMensajeError() + " Error encontrado en el ingreso:" + _sig_folio_ingreso + " en la póliza con el ID: " + n_poliza_id.ToString();
                    _D_return.s_msgError = _s_error_ingreso;
                    _D_return.E_return = ERRORES_WS.NOK_ERROR;
                    _D_return.s_proceso = "Registrar ingreso";

                    O_logger.Error(_D_return.mensajeParaLog());

                    //Si hubo error que elimine el ingreso
                    lsdkIngreso.borra();
                }
                EstadoCoordinadorEvent(E_ESTADO.REGISTRANDO_INGRESOS, elapsedTime, _n_contar_polizas, _n_contar_movimientos, _n_contar_ingresos, lsdkEmpresa.Nombre);
            }
            contador = 0;//indice de los datos del json
        }
        private void Documentos_Bancarios(dynamic json_documentos_bancarios)
        {
            foreach (var asientos in json_documentos_bancarios.t_documentosbancarios.asientos_id)
            {
                _cerrar_poliza_params.t_documentosbancarios.asientos_id.Add(Convert.ToInt32(json_documentos_bancarios.t_documentosbancarios.asientos_id[contador]));
                //_cerrar_poliza_params.t_documetosbancarios.folios_documentopago.Add(Convert.ToInt32(mensage));
                _cerrar_poliza_params.t_documentosbancarios.tipodocumentobancario_ventacontado_id.Add(Convert.ToInt32(json_documentos_bancarios.t_documentosbancarios.bancos_tiposdocumentos_venta_contado[contador]));

                contador++;
            }
        }
    }
}
