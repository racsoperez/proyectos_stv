using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coordinador_v1._0
{
  public class valida_tipospoliza
    {
        public List<int> _L_msgsError { get; set; }
        public List<int> codigoscontpaqi { get; set; }

        public valida_tipospoliza()
        {
            _L_msgsError = new List<int>();
            codigoscontpaqi = new List<int>();
        }
    }

    public class valida_tipodocumentosbancarios
    {
        public List<int> _L_msgsError { get; set; }
        public List<int> codigoscontpaqi { get; set; }

        public valida_tipodocumentosbancarios()
        {
            _L_msgsError = new List<int>();
            codigoscontpaqi = new List<int>();
        }
    }

    public class valida_diariosespeciales
    {
        public List<int> _L_msgsError { get; set; }
        public List<int> codigoscontpaqi { get; set; }

        public valida_diariosespeciales()
        {
            _L_msgsError = new List<int>();
            codigoscontpaqi = new List<int>();
        }
    }
    public class valida_cuentasbancarias
    {
        public List<int> _L_msgsError { get; set; }
        public List<int> codigoscontpaqi { get; set; }

        public valida_cuentasbancarias()
        {
            _L_msgsError = new List<int>();
            codigoscontpaqi = new List<int>();
        }
    }
    public class valida_segmentosnegocio
    {
        public List<int> _L_msgsError { get; set; }
        public List<int> codigoscontpaqi { get; set; }

        public valida_segmentosnegocio()
        {
            _L_msgsError = new List<int>();
            codigoscontpaqi = new List<int>();
        }
    }
    public class valida_bancos
    {
        public List<int> _L_msgsError { get; set; }
        public List<int> codigoscontpaqi { get; set; }

        public valida_bancos()
        {
            _L_msgsError = new List<int>();
            codigoscontpaqi = new List<int>();
        }
    }
    public class valida_monedas
    {
        public List<int> _L_msgsError { get; set; }
        public List<int> codigoscontpaqi { get; set; }

        public valida_monedas()
        {
            _L_msgsError = new List<int>();
            codigoscontpaqi = new List<int>();
        }
    }

    public class valida_cuentascontables
    {
        public List<int> _L_msgsError { get; set; }
        public List<string> codigoscontpaqi { get; set; }

        public valida_cuentascontables()
        {
            _L_msgsError = new List<int>();
            codigoscontpaqi = new List<string>();
        }
    }

    public class cuentas
    {
        public int cuenta_id { get; set; }

        public int servidor_id { get; set; }
    }

}
