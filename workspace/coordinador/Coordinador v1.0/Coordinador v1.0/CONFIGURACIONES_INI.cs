using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
//librerias para los archivos .ini
using IniParser;
using IniParser.Model;
using System.Windows.Forms;

namespace Coordinador_v1._0
{
    public class CONFIGURACIONES_INI
    {
        private string _seccion_config_coordinador = "stv_coordinador_config";
        private string _seccion_contpaqi = "stv_contpaqi_firma";
        private string ARCHIVO_INI = "config.ini";
        private const string ARCHIVO_INI_BACKUP = "config.ini.bkp";

        private string _cuenta = string.Empty;
        private string _sitio = string.Empty;
        private string _usuario = string.Empty;
        private string _pass = string.Empty;

        private string _usuario_contpaqi = string.Empty;
        private string _pass_contpaqi = string.Empty;

        public void verificar_archivo_ini()
        {
            //ruta release
            string _path_ini = System.AppDomain.CurrentDomain.BaseDirectory + ARCHIVO_INI;
            // verifica si está creado el archivo .ini
            if (!File.Exists(_path_ini))
            {
                StreamWriter sw = new StreamWriter(_path_ini);
                sw.Close();
            }
            else
            {
                //no hacer nada
            }

            var parser = new FileIniDataParser();
            IniData data = parser.ReadFile(ARCHIVO_INI);

            if (data.Sections.ContainsSection(this._seccion_config_coordinador) || data.Sections.ContainsSection(this._seccion_config_coordinador))
            {
                // No hacer nada, la sección ya existe
            }
            else
            {
                //se agrega la sección
                data.Sections.AddSection(this._seccion_config_coordinador);
                data.Sections.AddSection(this._seccion_contpaqi);
            }

            //se agrega la sección de configuración
            data[this._seccion_config_coordinador].AddKey("usuario", "");
            data[this._seccion_config_coordinador].AddKey("pass", "");
            data[this._seccion_config_coordinador].AddKey("cuenta", "");
            data[this._seccion_config_coordinador].AddKey("sitio", "");

            //se agrega la sección de configuración contpaqi
            data[this._seccion_contpaqi].AddKey("user", "");
            data[this._seccion_contpaqi].AddKey("password", "");

            //guardamos los datos en el .ini
            parser.WriteFile(ARCHIVO_INI, data);

        }

        public bool validar_configuracion_ini()
        {
            bool _return = false;

            try
            {
                var parser = new FileIniDataParser();
                IniData data = parser.ReadFile(ARCHIVO_INI);

                KeyDataCollection seccionCoordinador = data[this._seccion_config_coordinador];

                if (seccionCoordinador.Count < 4)
                {
                    _return = false;
                }
                else if (
                    (seccionCoordinador["usuario"] == string.Empty)
                    || (seccionCoordinador["pass"] == string.Empty)
                    || (seccionCoordinador["cuenta"] == string.Empty)
                    || (seccionCoordinador["sitio"] == string.Empty)
                    )
                {
                    _return = false;
                }
                else
                { 
                    this._usuario = seccionCoordinador["usuario"];
                    this._pass = seccionCoordinador["pass"];
                    this._cuenta = seccionCoordinador["cuenta"];
                    this._sitio = seccionCoordinador["sitio"];

                    _return = true;
                }
            }
            catch (Exception)
            {
                return _return;
            }

            return _return;
        }

        public bool validar_configuracion_contpaqi_ini()
        {
            bool _return = false;

            try
            {
                var parser = new FileIniDataParser();
                IniData data = parser.ReadFile(ARCHIVO_INI);

                KeyDataCollection seccionContpai = data[this._seccion_contpaqi];

                if (seccionContpai.Count < 2)
                {
                    _return = false;
                }
                else if (seccionContpai["user"] == string.Empty)
                {
                    _return = false;
                }
                else
                {
                    this._usuario_contpaqi = seccionContpai["user"];
                    this._pass_contpaqi = seccionContpai["password"];

                    _return = true;
                }
            }
            catch (Exception)
            {
                return _return;
            }

            return _return;
        }

        public string LeerUsuario()
        {
            return this._usuario;
        }
        public string LeerPass()
        {
            return this._pass;
        }
        public string LeerSitio()
        {
            return this._sitio;
        }
        public string LeerCuenta()
        {
            return this._cuenta;
        }
        public string LeerUsuarioContpaqi()
        {
            return this._usuario_contpaqi;
        }
        public string LeerPassContpaqi()
        {
            return this._pass_contpaqi;
        }


        public void ConfiguracionesINI(string user, string pass, string cuenta, string sitio)
        {
            var parser = new FileIniDataParser();
            IniData _D_data = parser.ReadFile(ARCHIVO_INI); 
            KeyDataCollection keyCol = _D_data[this._seccion_config_coordinador];

            if (keyCol.Count > 0)
            {
                //modificando los datos de las secciones
                _D_data[this._seccion_config_coordinador]["usuario"] = user;
                _D_data[this._seccion_config_coordinador]["pass"] = pass;
                _D_data[this._seccion_config_coordinador]["cuenta"] = cuenta;
                _D_data[this._seccion_config_coordinador]["sitio"] = sitio;

                File.Delete(ARCHIVO_INI_BACKUP);
                File.Copy(ARCHIVO_INI, ARCHIVO_INI_BACKUP);
            }
            else
            {
                //agregando los valores a las secciones
                _D_data[this._seccion_config_coordinador].AddKey("usuario", user);
                _D_data[this._seccion_config_coordinador].AddKey("pass", pass);
                _D_data[this._seccion_config_coordinador].AddKey("cuenta", cuenta);
                _D_data[this._seccion_config_coordinador].AddKey("sitio", sitio);
            }

            //guardar el archivo .ini
            parser.WriteFile(ARCHIVO_INI, _D_data);

        }

        public void ConfiguracionesCONTPAQI(string usuario, string password)
        {
            var parser = new FileIniDataParser();
            IniData _D_data1 = parser.ReadFile(ARCHIVO_INI);
            KeyDataCollection keyCol = _D_data1[_seccion_contpaqi];

            if (keyCol.Count > 0)
            {
                //modificando los datos de las secciones
                _D_data1[_seccion_contpaqi]["user"] = usuario;
                _D_data1[_seccion_contpaqi]["password"] = password;

                File.Delete(ARCHIVO_INI_BACKUP);
                File.Copy(ARCHIVO_INI, ARCHIVO_INI_BACKUP);
            }
            else
            {
                //agregando los valores a las secciones
                _D_data1[_seccion_contpaqi].AddKey("user", usuario);
                _D_data1[_seccion_contpaqi].AddKey("password", password);
            }

            //guardar el archivo .ini
            parser.WriteFile(ARCHIVO_INI, _D_data1);
        }
    }
}
