﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coordinador_v1._0
{
    public class validacionescontpaqi
    {

        public valida_tipospoliza t_tipospoliza { get; set; }
        public valida_tipodocumentosbancarios t_tipodocumentosbancarios { get; set; }
        public valida_diariosespeciales t_diariosespeciales { get; set; }
        public valida_cuentasbancarias t_cuentasbancarias { get; set; }
        public valida_segmentosnegocio t_segmentosnegocio { get; set; }
        public valida_bancos t_bancos { get; set; }
        public valida_monedas t_monedas { get; set; }

        public int empresa_id { get; set; }

        public valida_cuentascontables t_cuentascontables { get; set; }

        public string empresacontpaqi { get; set; }



        public validacionescontpaqi()
        {
            t_tipospoliza = new valida_tipospoliza();

            t_tipodocumentosbancarios = new valida_tipodocumentosbancarios();

            t_diariosespeciales = new valida_diariosespeciales();

            t_cuentasbancarias = new valida_cuentasbancarias();

            t_segmentosnegocio = new valida_segmentosnegocio();

            t_bancos = new valida_bancos();

            t_monedas = new valida_monedas();

            t_cuentascontables = new valida_cuentascontables();


            empresa_id = 0;
            empresacontpaqi = string.Empty;

        }
    }
}
