# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
#########################################################################

def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    response.flash = T("Hello World")
    return dict(message=T('Welcome to web2py!'))


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def formas_print_reporte():
    _dbTable = db.tformularios
    
    _dbRows = db(
        ).select(
            _dbTable.id, 
            _dbTable.descripcion, 
            _dbTable.aplicacion_w2p, 
            _dbTable.controlador_w2p, 
            _dbTable.funcion_w2p, 
            _dbTable.bansuspender,
            orderby = [
                _dbTable.aplicacion_w2p, 
                _dbTable.controlador_w2p, 
                _dbTable.funcion_w2p, 
                ]
            )
    
    _rpt = STV_FWK_REPORT_PDF(
        E_tamanio = request.vars.pagina_tamanio, 
        E_orientacion = request.vars.pagina_orientacion, 
        )


    #response.logo_big = URL(a=D_stvFwkCfg.s_nameFwkApp, c='static', f='images/stv_logo_completo.png')
    #response.logo_pin = URL(a=D_stvFwkCfg.s_nameFwkApp, c='static', f='images/stv_logo_pin.png')
    
    _s_imgPath = os.path.join('applications', D_stvFwkCfg.s_nameFwkApp, 'static', 'images', 'stv_logo_completo.png')

    _D_encabezado = _rpt.helper_crearEncabezado(
        D_linea1 = Storage(s_texto = "Reporte de " + _dbTable._plural),
        D_linea2 = Storage(s_texto = request.vars.descripcion),
        s_rutaImagen = _s_imgPath,
        D_fecha = Storage(
            s_texto = datetime.datetime.strftime(request.browsernow, STV_FWK_APP.FORMAT.s_DATE),
            ),
        D_textoControl = Storage(s_texto = request.client + " " + request.application + "/" + request.controller + "/" + request.function),
        b_linea = True,
        )
    
    _D_pie = _rpt.helper_crearPie(
        D_linea1 = None,
        D_linea2 = None,
        #s_rutaImagen = _s_imgPath,
        D_fecha = Storage(s_texto = ""),
        D_textoControl = Storage(
            s_sysvar = "pagenumber",
            x_formato = lambda v, r: "Página %s" % v,
            ),
        b_linea = True,
        )

    _LD_infoColumnas = [
        Storage(
            n_ancho_pje = 0.3,
            #E_alineacion = E_ALINEACION.DERECHA,
            D_marco = None
            ),
        Storage(
            n_ancho_pje = 0.15,
            #E_alineacion = E_ALINEACION.CENTRO,
            D_marco = None
            ),
        Storage(
            n_ancho_pje = 0.15,
            #E_alineacion = E_ALINEACION.IZQUIERDA,
            D_marco = None
            ),
        Storage(
            n_ancho_pje = 0.40,
            #E_alineacion = E_ALINEACION.IZQUIERDA,
            D_marco = None
            ),
        ]

    _D_tablaReporte = _rpt.helper_crearTabla(
        LL_datos = [
            [
                Storage(
                    s_texto = _dbTable.descripcion.label, 
                    E_tipoletra = LETRA.E_TIPO.TIMES_BOLD,
                    E_alineacion = E_ALINEACION.CENTRO,
                    b_marco = False
                    ),
                Storage(
                    s_texto = _dbTable.aplicacion_w2p.label, 
                    E_tipoletra = LETRA.E_TIPO.TIMES_BOLD,
                    E_alineacion = E_ALINEACION.CENTRO,
                    ),
                Storage(
                    s_texto = _dbTable.controlador_w2p.label,
                    E_tipoletra = LETRA.E_TIPO.TIMES_BOLD,
                    E_alineacion = E_ALINEACION.CENTRO,
                    ),
                Storage(
                    s_texto = _dbTable.funcion_w2p.label,
                    E_tipoletra = LETRA.E_TIPO.TIMES_BOLD,
                    E_alineacion = E_ALINEACION.CENTRO,
                    )
                ], 
            ],
        LD_infoColumnas = _LD_infoColumnas,
        n_offsetAlto = _D_encabezado.n_offsetAlto + 3
        )
    
    _D_tablaAgruparAplicacionEncabezado = _rpt.helper_crearTabla(
        LL_datos = [
            [
                Storage(
                    s_campo = "aplicacion_w2p", 
                    E_tipoletra = LETRA.E_TIPO.TIMES_BOLD,
                    n_letratamanio = 12,
                    E_alineacion = E_ALINEACION.CENTRO,
                    x_formato = lambda v, r: "Aplicación: %s" % v,
                    b_marco = False
                    ),
                ], 
            ],
        LD_infoColumnas = [
            Storage(
                n_ancho_pje = 1,
                #E_alineacion = E_ALINEACION.DERECHA,
                D_marco = None
                )
            ],
        n_offsetAlto = 5
        )

    _D_tablaAgruparAplicacionPie = _rpt.helper_crearTabla(
        LL_datos = [
            [
                Storage(
                    s_campo = "aplicacion_w2p",
                    E_tipoletra = LETRA.E_TIPO.TIMES_BOLD,
                    #                    n_letratamanio = 12,
                    E_alineacion = E_ALINEACION.CENTRO,
                    x_formato = lambda v, r: "Total registros de aplicación '%s': %s" % (r['aplicacion_w2p'], v),
                    E_tipotexto = E_TIPOTEXTO.CUENTA,
                    b_marco = False
                    ),
                ],
            ],
        LD_infoColumnas = [
            Storage(
                n_ancho_pje = 1,
                # E_alineacion = E_ALINEACION.DERECHA,
                D_marco = None
                )
            ],
        n_offsetAlto = 5
        )

    _D_tablaAgruparControladorEncabezado = _rpt.helper_crearTabla(
        LL_datos = [
            [
                Storage(
                    s_campo = "controlador_w2p", 
                    E_tipoletra = LETRA.E_TIPO.TIMES_BOLD,
                    E_alineacion = E_ALINEACION.IZQUIERDA,
                    x_formato = lambda v, r: "Controlador: %s" % v,
                    b_marco = False
                    ),
                ], 
            ],
        LD_infoColumnas = [
            Storage(
                n_ancho_pje = 1,
                #E_alineacion = E_ALINEACION.DERECHA,
                D_marco = None
                )
            ],
        n_offsetAlto = 3.0,
        n_offsetDerecha = 10.0,
        )

    _D_tablaAgruparControladorPie = _rpt.helper_crearTabla(
        LL_datos = [
            [
                Storage(
                    s_campo = "controlador_w2p", 
                    E_tipoletra = LETRA.E_TIPO.TIMES_BOLD,
                    E_alineacion = E_ALINEACION.IZQUIERDA,
                    x_formato = lambda v, r: "Total registros de controlador '%s': %s" % (r['controlador_w2p'], v),
                    E_tipotexto = E_TIPOTEXTO.CUENTA,
                    b_marco = False
                    ),
                ], 
            ],
        LD_infoColumnas = [
            Storage(
                n_ancho_pje = 1,
                #E_alineacion = E_ALINEACION.DERECHA,
                D_marco = None
                )
            ],
        n_offsetAlto = 3.0,
        n_offsetDerecha = 10.0,
        )
    
    _rpt.agrupar(
        L_elementosEncabezado = _D_tablaAgruparAplicacionEncabezado.L_elementos,
        L_elementosPie = None,
        s_porCampo = "aplicacion_w2p"
        )

    _rpt.agrupar(
        L_elementosEncabezado = _D_tablaAgruparControladorEncabezado.L_elementos,
        L_elementosPie = _D_tablaAgruparControladorPie.L_elementos,
        s_porCampo = "controlador_w2p"
        )

    _rpt.agrupar(
        L_elementosEncabezado = None,
        L_elementosPie = _D_tablaAgruparAplicacionPie.L_elementos,
        s_porCampo = "aplicacion_w2p"
        )
        
    # Se agrega la página debajo de la fecha, es un movimiento custom
    
#     _D_posicionFecha = _D_encabezado.D_elementos.fecha.D_posicion
#     _D_posicionFecha.y = _D_encabezado.D_elementos.fecha.n_offsetAlto + 2
#     _D_posicionFecha[1] = _D_posicionFecha.y
#     
#     _D_textoPaigina = _rpt.helper_crearTexto(
#         D_posicion = copy(_D_posicionFecha),
#         D_texto = Storage(
#             s_texto = "Página", 
#             E_tipoletra = LETRA.E_TIPO.TIMES_BOLD,
#             E_alineacion = E_ALINEACION.IZQUIERDA,
#             b_marco = False
#             ),
#         )
#     
#     _D_posicionFecha.x += _D_textoPaigina.n_anchoTexto + 2.0
#     _D_posicionFecha[1] = _D_posicionFecha.y
# 
#     _D_textoPaiginaNumero = _rpt.helper_crearTexto(
#         D_posicion = copy(_D_posicionFecha),
#         D_texto = Storage(
#             s_sysvar = "pagenumber", 
#             E_tipoletra = LETRA.E_TIPO.TIMES_BOLD,
#             E_alineacion = E_ALINEACION.IZQUIERDA,
#             b_marco = False
#             ),
#         )
    
    _rpt.definir_paginaEncabezado(
        L_elementos = (
            _D_encabezado.L_elementos
            + _D_tablaReporte.L_elementos
            + _rpt.helper_crearRegla(n_offsetAlto = _D_tablaReporte.n_offsetAlto + 1).L_elementos
#             + _D_textoPaigina.L_elementos
#             + _D_textoPaiginaNumero.L_elementos
            ),
        )

#     _rpt.definir_reporteEncabezado(
#         L_elementos = (
#             _D_tablaReporte.L_elementos
#             + _rpt.helper_crearRegla(n_offsetAlto = _D_tablaReporte.n_offsetAlto)
#             ),
#         )
    
    _rpt.definir_paginaPie(
        L_elementos = (
            _D_pie.L_elementos 
            ),
        )
    
    _D_tablaDetalleReporte = _rpt.helper_crearTabla(
        LL_datos = [
            [
                Storage(s_campo = "descripcion"),
                Storage(
                    s_campo = "aplicacion_w2p", 
                    #E_tipoletra = LETRA.E_TIPO.TIMES_BOLD,
                    ),
                Storage(s_campo = "controlador_w2p"),
                Storage(
                    s_campo = "funcion_w2p", 
                    #E_alineacion = E_ALINEACION.IZQUIERDA
                    ),
                ],
            ],
        LD_infoColumnas = _LD_infoColumnas,
        n_offsetAlto = 1
        )    
    
    _rpt.definir_reporteDetalle(
        L_elementos = (
            _D_tablaDetalleReporte.L_elementos 
            ),
        )
    
    _D_tablaReportePie = _rpt.helper_crearTabla(
        LL_datos = [
            [
                Storage(
                    s_texto = "", 
                    E_tipoletra = LETRA.E_TIPO.TIMES_BOLD,
                    E_alineacion = E_ALINEACION.IZQUIERDA,
                    x_formato = lambda v, r: "Total registros '%s'" % v,
                    E_tipotexto = E_TIPOTEXTO.CUENTA,
                    b_marco = False
                    ),
                ], 
            ],
        LD_infoColumnas = [
            Storage(
                n_ancho_pje = 1,
                #E_alineacion = E_ALINEACION.DERECHA,
                D_marco = None
                )
            ],
        n_offsetAlto = 3.0,
        n_offsetDerecha = 10.0,
        )

 
    _rpt.definir_reportePie(
        L_elementos = (
            _D_tablaReportePie.L_elementos 
            ),
        )

    return _rpt.generar_pdf('archivo.pdf', 'titulo en Browser', _dbRows)


def imprimir():
    
    _D_returnVars = configuracion_imprimir(
        dbTabla = None,
        s_titulo = request.vars.titulo or "No Definido",
        x_offsetInArgs = 1,
        s_urlReporte = URL(f = request.vars.reporte, args = request.args)
        )

    return _D_returnVars


def formas():
    """ Definición de las Formas

    @descripcion Formas

    @keyword [arg0]: operación a realizar en el formulario.
    @keyword [arg1]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature

    """
    _s_action = request.args(0, request.stv_fwk_permissions.btn_none.code)
    _s_forma_id = request.args(1, 0)
    
    if 'descripcion' in request.vars:
        if not request.vars.descripcion:
            request._post_vars.descripcion = ">"
        else:
            pass
    else:
        pass

    _dbTable = db.tformularios    
    
    def al_validar(O_form):
        
        if not O_form.vars.controlador_w2p or (O_form.vars.controlador_w2p == "null"):
            O_form.errors.controlador_w2p = "Función no puede estar vacía"
        else:
            pass
        return O_form
    
    def actualizar_permisos_forma(O_form, s_controlador_w2p, s_rutina_w2p):
        _n_formulario_id = O_form.record_id or O_form.vars.id

        _D_rutina = None
        
        if O_form.vars.descripcion in (">", "_", "-"):
            
            _D_rutina = STV_FWK2_VALIDACIONES.RUTINA_W2P.INFORMACION(
                s_aplicacion_w2p = O_form.vars.aplicacion_w2p, 
                s_controlador_w2p = s_controlador_w2p, 
                s_rutina_w2p = s_rutina_w2p
                )
            
            _dbRow_formulario = db.tformularios(_n_formulario_id)
    
            if _D_rutina.s_descripcion:
    
    
                # Si tiene en descripcion un codigo
                _s_descripcion = ""
                for _n_index, _D_key in enumerate(_D_rutina.L_keywords):
                    if _n_index%2 == 0:
                        _s_descripcion += _D_key.descripcion + " > "
                    else:
                        pass
                
                _s_descripcion += _D_rutina.s_descripcion
                
            else:
                _s_descripcion = s_rutina_w2p
            
            _dbRow_formulario.descripcion = _s_descripcion
            _dbRow_formulario.update_record()
            
        else:
            pass
            
        if O_form.vars.actualizarpermisos:
            
            if not _D_rutina:
                _D_rutina = STV_FWK2_VALIDACIONES.RUTINA_W2P.INFORMACION(
                s_aplicacion_w2p = O_form.vars.aplicacion_w2p, 
                s_controlador_w2p = s_controlador_w2p, 
                s_rutina_w2p = s_rutina_w2p
                )
            else:
                pass
            
            _L_permisos = _D_rutina.L_permisos
    
            # Si ya tiene registros
            _dbRows = db(
                db.tformulario_permisos.formulario_id == O_form.record_id
                ).select(
                    db.tformulario_permisos.ALL
                    )
                
            if _dbRows and _L_permisos:
                for _dbRow in _dbRows:
                    _b_permiso_encontrado = False
                    for _D_permiso in _L_permisos:
                        if _D_permiso.permiso_id == _dbRow.permiso_id:
                            if _D_permiso.encontrado:
                                # Si el permiso ya se encontró anteriormente
                                _dbRow.delete_record()
                            else:
                                _dbRow.descripcion = _D_permiso.descripcion
                                _dbRow.icono = _D_permiso.icono
                                _dbRow.update_record()
                                _D_permiso.encontrado = True
                                _b_permiso_encontrado = True
                                break
                        else:
                            pass
                    if not _b_permiso_encontrado:
                        # Si la forma no tiene ese permiso, se elimina
                        _dbRow.delete_record()
                    else:
                        pass
            else:
                pass
    
            for _D_permiso in _L_permisos:
                if not _D_permiso.encontrado:
                    _D_insert_datos = Storage(
                        formulario_id = _n_formulario_id,
                        permiso_id = _D_permiso.permiso_id,
                        descripcion = _D_permiso.descripcion,
                        icono = _D_permiso.icono,
                        )
                    db.tformulario_permisos.insert(**_D_insert_datos)
    
        elif O_form.vars.temp_permisosiniciales and (O_form.vars.temp_permisosiniciales != "null"):
            
            _L_temp_permisosiniciales = O_form.vars.temp_permisosiniciales if isinstance(O_form.vars.temp_permisosiniciales, (list, tuple)) else [int(str) for str in O_form.vars.temp_permisosiniciales.split(',')]
            for _idPermiso in _L_temp_permisosiniciales:
                db.tformulario_permisos.insert(formulario_id = _n_formulario_id, permiso_id = _idPermiso, descripcion = request.stv_fwk_permissions.getLabel(int(_idPermiso)));
                
        else:
            pass
        
        return O_form   
        
    def al_aceptar(O_form, b_insertar):
        
        if "|" in O_form.vars.controlador_w2p:
            # Si tiene el caracter | definido, significa que esta usando el control conjunto de controlador rutina.
            
            _L_formas = O_form.vars.controlador_w2p.split(',')  # Las formas estan separadas por ","
            
            for _s_forma in _L_formas:
            
                _s_controlador_w2p, _s_rutina_w2p = _s_forma.split("|")  # El controlados y rutina estan separados por |
                if b_insertar:
                    O_form.record_id = db.tformularios.insert(
                        descripcion = O_form.vars.descripcion,
                        aplicacion_w2p = O_form.vars.aplicacion_w2p,
                        controlador_w2p = _s_controlador_w2p,
                        funcion_w2p = _s_rutina_w2p,
                        actualizarpermisos = O_form.vars.actualizarpermisos,
                        )
                    db.commit()
                else:
                    pass
                O_form = actualizar_permisos_forma(O_form, _s_controlador_w2p, _s_rutina_w2p)
        else:
            
            # Si no lo esta usando el caracter |, significa que esta usando el método standar de las formas, donde cada campo tiene un widget
            O_form = actualizar_permisos_forma(O_form, O_form.vars.controlador_w2p, O_form.vars.funcion_w2p)
            
        return O_form    
    
    request.stv_fwk_permissions.L_formButtonsAll.insert(5, request.stv_fwk_permissions.btn_print)

    _O_cat = STV_FWK_FORM(
        dbReference = db,
        dbTable = _dbTable, 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.descripcion, 
            _dbTable.aplicacion_w2p, 
            _dbTable.controlador_w2p, 
            _dbTable.funcion_w2p, 
            _dbTable.bansuspender
            ], 
        L_fieldsToSearch = [
            _dbTable.descripcion, 
            _dbTable.aplicacion_w2p, 
            _dbTable.controlador_w2p, 
            _dbTable.funcion_w2p
            ], 
        L_fieldsToSearchIfDigit = [_dbTable.id],
        b_applyPermissons = False
        )

    _L_fieldsForm = []
    _b_skipAction = False
    _b_insertar = False
    
    if (
        (_s_action == request.stv_fwk_permissions.btn_new.code)
        or (
            (_s_action == request.stv_fwk_permissions.btn_save.code)
            and not _s_forma_id
            )
        ):
        # Si la accion es nuevo o es grabar y no tiene el id definido
        
        _dbField_controladorrutina = _dbTable.controlador_w2p
        _dbField_controladorrutina.label = "Controlador y Rutinas no registradas"
        _dbField_controladorrutina.widget = lambda field, value: stv_widget_db_chosen_linked(
            field, value, 
            20, 
            'aplicacion_w2p', 
            URL(c='accesos', f='application_controladorrutinas_ver.json')
            )
        _dbField_controladorrutina.length = 400
        
        _dbTable.temp_permisosiniciales.writable = True  # Se capturan los permisos iniciales
        _L_fieldsForm = [
            _dbTable.descripcion,
            _dbTable.aplicacion_w2p,
            _dbField_controladorrutina,
            _dbTable.actualizarpermisos,
            _dbTable.temp_permisosiniciales
            ]
        _b_skipAction = True
        _b_insertar = True
        
    else:
        pass

    _O_cat.addRowContentEvent('onValidation', al_validar)
    _O_cat.addRowContentEvent('onAccepted', al_aceptar, b_insertar = _b_insertar)
    
    _O_cat.addSubTab(
                dbTableDetail = db.tformulario_permisos, 
                s_url = URL(f='forma_permisos', args=['master_'+str(request.function),request.args(1)]),
                s_idHtml = fn_createIDTabUnique(f = 'forma_permisos', s_masterIdentificator = str(request.function))  )

    _O_cat.addSubTab(
                dbTableDetail = db.tformulario_disenios, 
                s_url = URL(f='forma_disenios', args=['master_'+str(request.function),request.args(1)]),
                s_idHtml = fn_createIDTabUnique(f = 'forma_disenios', s_masterIdentificator = str(request.function))  )

    _O_cat.addPermissionConfig_option( 
                request.stv_fwk_permissions.btn_print.code, 
                s_optionCode = request.stv_fwk_permissions.form_print.code,
                s_optionLabel = 'Imprimir Catalogo',  
                s_optionIcon = '',   
                s_type = 'option',
                s_send = 'form',
                L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_print), # Copia las vistas en la que estara habilitada la opción como el caso de Print
                D_actions = Storage(
                    modal = Storage(
                        s_url = URL(
                            f='imprimir', 
                            args=["imprimir_todo"], 
                            vars=Storage(
                                reporte = "formas_print_reporte.pdf",
                                titulo = _dbTable._plural
                                )
                            ), 
                        s_field = "", 
                    ) ),
        )
    _O_cat.addPermissionConfig_option( 
                request.stv_fwk_permissions.btn_print.code, 
                s_optionCode = request.stv_fwk_permissions.form_print.code,
                s_optionLabel = 'Imprimir 2',  
                s_optionIcon = '',   
                s_type = 'divider',
                s_send = 'formWithFind',
                L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_print),
                D_actions = Storage( ),
        )
    _O_cat.addPermissionConfig_option( 
                request.stv_fwk_permissions.btn_print.code, 
                s_optionCode = request.stv_fwk_permissions.form_print.code,
                s_optionLabel = 'Imprimir Registro',  
                s_optionIcon = '',   
                s_type = 'option',
                s_send = 'formWithFind',
                L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_edit), # Copia las vistas en la que estara habilitada la opción como el caso de Edit
                D_actions = Storage(
                    modal = Storage(
                        s_url = URL(f='formas_print', args=['printrow']), 
                        s_field = "", 
                    ) ),
        )    
    
    _D_returnVars = _O_cat.process(
        L_formFactoryFields = _L_fieldsForm,
        b_skipAction = _b_skipAction
        )
    
    return _D_returnVars

def formas_report():
    
    _sMsgError = None
    _oForm = SQLFORM.factory(
        Field('titulo', 'string', length = 100, default = None,
          required = True,
          notnull = True, unique = False,
          widget = stv_widget_input, label = 'Título del Reporte', comment = None,
          writable = True, readable = True,
          represent = stv_represent_string),
        Field('fecha', 'date', default = request.browsernow,
          required = True,
          notnull = True,
          widget = stv_widget_inputDate, label = T('Date'), comment = None,
          writable = True, readable = True,
          represent = stv_represent_date),
        Field('fecha2', 'date', default = request.browsernow,
          required = True,
          notnull = True,
          widget = stv_widget_inputDate, label = T('Date'), comment = None,
          writable = True, readable = True,
          represent = stv_represent_date),
        table_name='t_print_ajax',
        buttons=[], _id=D_stvFwkCfg.D_uniqueHtmlIds.s_idTab+"form")
    
    return dict(
                oForm = _oForm,
                sTitle = 'Opciones de impresión',
                sMsg = "Verificando las opciones de impresión.",
                sMsgError = _sMsgError,
                sIdForm = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab+"_form",
                sUrlPrint = URL(f='formas_report', args=request.args)
                )

def formas_buscar():
    ''' Forma de búsqueda para formas formas_buscar.html 
    '''    
    _dbTable = db.tformularios
    
    _O_cat = STV_FWK_FORM(
                dbReference = db,
                dbTable = _dbTable, 
                L_visibleButtons = request.stv_fwk_permissions.L_formSearchButtons, 
                s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
                L_fieldsToShow = [_dbTable.id, _dbTable.descripcion, _dbTable.aplicacion_w2p, _dbTable.controlador_w2p, _dbTable.funcion_w2p, _dbTable.bansuspender], 
                L_fieldsToSearch = [_dbTable.descripcion, _dbTable.aplicacion_w2p, _dbTable.controlador_w2p, _dbTable.funcion_w2p], 
                L_fieldsToSearchIfDigit = [_dbTable.id],
                b_applyPermissons = False
                )
    
    _D_returnVars = _O_cat.process()
    
    return _D_returnVars

def forma_disenios():
    _dbTable = db.tformulario_disenios

    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_find)
    
    _O_cat = STV_FWK_FORM(
                dbReference = db,
                dbTable = _dbTable,
                dbTableMaster = db.tformularios, 
                xFieldLinkMaster = [_dbTable.formulario_id], 
                L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
                s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
                L_fieldsToShow = [_dbTable.id, _dbTable.versiondisenio, _dbTable.descripcion, _dbTable.bansuspender], 
                L_fieldsToSearch = [_dbTable.descripcion, _dbTable.versiondisenio], 
                L_fieldsToSearchIfDigit = [_dbTable.id],
                b_applyPermissons = False,                
                )
    
    _O_cat.addRowContentProperty('formHidden', {'verificacion_aceptada': str(0)})    
    _O_cat.addRowContentEvent('onValidation', _forma_disenios_validation)
    
    _D_returnVars = _O_cat.process()


    return _D_returnVars

def _forma_disenios_validation(O_form):
    _s_id = O_form.record_id
    _s_verif_aceptada = int(request.vars.verificacion_aceptada)
    
    if _s_verif_aceptada:
        pass
    else:
        O_form.verificar = True
        O_form.errors.id = "Favor de verificar operación"
    
    return O_form

def forma_disenios_imagen():
    return response.download(request, db)

''' Forma detalle para opciones de formas utilizando como vista por default formas_opciones.html '''
def forma_permisos():
    _dbTable = db.tformulario_permisos
    
    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_find)
    
    _O_cat = STV_FWK_FORM(
                dbReference = db,
                dbTable = _dbTable,
                dbTableMaster = db.tformularios, 
                xFieldLinkMaster = [_dbTable.formulario_id], 
                L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
                s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
                L_fieldsToShow = [_dbTable.id, _dbTable.permiso_id, _dbTable.descripcion, _dbTable.bansuspender], 
                L_fieldsToSearch = [_dbTable.descripcion], 
                L_fieldsToSearchIfDigit = [_dbTable.id, _dbTable.permiso_id],
                b_applyPermissons = False
                )

    _O_cat.addRowSearchResultsConfig('multiselect', True)

    # Si se desea crear un nuevo registro o editar uno actual...
    if _O_cat.D_args.s_action in (request.stv_fwk_permissions.btn_edit.code, request.stv_fwk_permissions.btn_new.code): 
        # ...establece el requerimiento para el permiso_id.
        if _O_cat.D_args.s_action in request.stv_fwk_permissions.btn_edit.code:
            # En caso de edición utiliza el argumento en la posicion 3 que define el id de elemento a editar, de lo contrario, cubriendo el caso de Nuevo, regresa un 0
            _n_permiso_id = db.tformulario_permisos(_O_cat.D_args.L_ids[0][0]).permiso_id
        else:
            _n_permiso_id = None
        
        _dbTable.permiso_id.requires = IS_IN_SET(request.stv_fwk_permissions.getListFiltered( _O_cat.D_args.L_idMaster[0],  _n_permiso_id), zero = None, error_message = (T('Select') + T('Form option')) )
    
    _D_returnVars = _O_cat.process()

    return _D_returnVars

''' Forma maestro para la configuración del menu utilizando como vista por default catconfiguracionmenu.html '''
def estructuras():
    _dbTable = db.tmenus
    
    _O_cat = STV_FWK_FORM(
                dbReference = db,
                dbReferenceSearch = db,
                dbTable = _dbTable, 
                L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
                s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
                L_fieldsToShow = [_dbTable.id, _dbTable.nombre, _dbTable.descripcion, _dbTable.bansuspender], 
                L_fieldsToSearch = [_dbTable.nombre, _dbTable.descripcion], 
                L_fieldsToSearchIfDigit = [_dbTable.id],
                b_applyPermissons = False
                )

    _O_cat.addSubTab(
                dbTableDetail = db.tmenu_submenus, 
                s_url = URL(f='estructura_submenus', args=['master_'+str(request.function),request.args(1)]),
                s_idHtml = fn_createIDTabUnique(f = 'estructura_submenus', s_masterIdentificator = str(request.function))  )

    _D_returnVars = _O_cat.process()

    return _D_returnVars

''' Forma detalle para submenus de menus utilizando como vista por default catconfiguracionmenu_submenu.html '''
def estructura_submenus():

    _s_menu_id = request.args(1,0)
    _dbTable = db.tmenu_submenus

    _dbRows = db(
        (_dbTable.menu_id == _s_menu_id)
        ).select(
            _dbTable.orden,
            orderby = ~_dbTable.orden,
            limitby = (0, 2)
            )
    if _dbRows:
        _dbTable.orden.default = _dbRows.first().orden + 100
    else:
        _dbTable.orden.default = 100

    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_find)
    
    _O_cat = STV_FWK_FORM(
                dbReference = db,
                dbReferenceSearch = db,
                dbTable = _dbTable,
                dbTableMaster = db.tmenus, 
                xFieldLinkMaster = [_dbTable.menu_id], 
                L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
                s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
                L_fieldsToShow = [_dbTable.id, _dbTable.orden, _dbTable.formulario_id, _dbTable.nombre, _dbTable.descripcion, _dbTable.argumentos, _dbTable.manejoargumentos, _dbTable.bansuspender], 
                L_fieldsToSearch = [_dbTable.nombre, _dbTable.descripcion, db.tformularios.descripcion, _dbTable.argumentos], 
                L_fieldsToSearchIfDigit = [_dbTable.id, _dbTable.orden, _dbTable.formulario_id],
                b_applyPermissons = False
                )
    
    _O_cat.addRowSearchResultsProperty(
        'L_leftJoins', 
        [db.tformularios.on(db.tformularios.id == _dbTable.formulario_id)]
        )
    
    _D_returnVars = _O_cat.process()

    return _D_returnVars

def aplicaciones():
    ''' Manejo de aplicaciones '''
    
    _dbTable = db.taplicaciones
        
    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_find)
    
    _O_cat = STV_FWK_FORM(
        dbReference = db,
        dbTable = _dbTable,
        dbTableMaster = None, 
        xFieldLinkMaster = [], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.descripcion, 
            _dbTable.nombrecorto, 
            _dbTable.aplicacion_w2p, 
            _dbTable.configuracion, 
            _dbTable.bansuspender
            ], 
        L_fieldsToSearch = [
            _dbTable.descripcion, 
            _dbTable.nombrecorto, 
            _dbTable.aplicacion_w2p
            ], 
        L_fieldsToSearchIfDigit = [_dbTable.id],
        b_applyPermissons = False
        )

    _O_cat.addRowContentProperty("formControlUpload", [
        Storage(
            fieldUpload = _dbTable.imagen, 
            fieldUploadFilename = _dbTable.imagennombrearchivo
            ),
        Storage(
            fieldUpload = _dbTable.imagenpin, 
            fieldUploadFilename = _dbTable.imagenpinnombrearchivo
            )
        ]);

    _O_cat.addSubTab(
        dbTableDetail = db.taplicacion_menus,
        s_url = URL(f='aplicacion_menus', args=['master_'+str(request.function), request.args(1)]),
        s_idHtml = fn_createIDTabUnique(f = 'aplicacion_menus', s_masterIdentificator = str(request.function))  
        )
    
    _D_returnVars = _O_cat.process()

    return _D_returnVars

def aplicacion_menus():
    ''' Detalle de estructuras para la aplicación '''

    _s_aplicacion_id = request.args(1, None)
    _dbTable = db.taplicacion_menus
    
    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_find)
    
    _dbRows = db(
        (_dbTable.aplicacion_id == _s_aplicacion_id)
        ).select(
            _dbTable.orden,
            orderby = ~_dbTable.orden,
            limitby = (0, 2)
            )
    if _dbRows:
        _dbTable.orden.default = _dbRows.first().orden + 100
    else:
        _dbTable.orden.default = 100
    
    _O_cat = STV_FWK_FORM(
        dbReference = db,
        dbTable = _dbTable,
        dbTableMaster = db.taplicaciones, 
        xFieldLinkMaster = [_dbTable.aplicacion_id], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.orden, 
            _dbTable.menu_id, 
            _dbTable.descripcion, 
            _dbTable.bansuspender
            ], 
        L_fieldsToSearch = [_dbTable.descripcion], 
        L_fieldsToSearchIfDigit = [_dbTable.id, _dbTable.menu_id],
        b_applyPermissons = False
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars

