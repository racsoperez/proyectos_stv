# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
#########################################################################


def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    response.flash = T("Hello World")
    return dict(message=T('Welcome to web2py!'))

''' Forma para captura de localidades '''
def perfiles():
    
    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_find)    
    
    _O_cat = STV_FWK_FORM(
                dbReference = db,
                dbTable = None, 
                L_visibleButtons = [request.stv_fwk_permissions.btn_none], 
                s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
                L_fieldsToShow = [], 
                L_fieldsToSearch = [], 
                L_fieldsToSearchIfDigit = [],
                b_applyPermissons = False
                )

    _O_cat.addSubTab(
                dbTableDetail = db.auth_group,
                s_url = URL(f='perfil_roles', args=[]),
                s_idHtml = fn_createIDTabUnique(f = 'perfil_roles', s_masterIdentificator = None)  )

    _D_returnVars = _O_cat.process(D_overrideView = request.stv_fwk_permissions.view_viewRegister)

    return dict(_D_returnVars)

''' Forma para captura de paises en localidades '''
def perfil_roles():
    _dbTable = db.auth_group

    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_find)
    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_signature)

    _O_cat = STV_FWK_FORM(
        dbReference = db,
        dbTable = _dbTable,
        dbTableMaster = None, 
        xFieldLinkMaster = [], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.role, 
            _dbTable.description, 
            _dbTable.bansuspender
            ], 
        L_fieldsToSearch = [_dbTable.role, _dbTable.description], 
        L_fieldsToSearchIfDigit = [_dbTable.id],
        b_applyPermissons = False
        )

    _O_cat.addRowSearchResults_addColumnBegin(
        es_typeColumn = STV_FWK_FORM.ES_TYPECOLUMN.S_LINKTAB,
        s_colname = "linktab",
        D_field = Storage(
            readable = True,
            represent = lambda value, dbRow: '<i class="fa fa-external-link-square" aria-hidden="true"></i>',
            ),
        m_classHtml = lambda D_column, dbRow: "stv_rowresults_linktab", 
        D_data = Storage(
            url = URL(f='perfil_rol_formas', args=['master_'+str(request.function)]), 
            tablinkedtitle = db.tformularios._plural, 
            tabcurrentfield = str(_dbTable.role),
            idhtml = fn_createIDTabUnique(
                f = 'perfil_rol_formas', 
                s_masterIdentificator = str(request.function)
                )
            )
        )

    _D_returnVars = _O_cat.process()

    _O_cat.addSubTab(
                dbTableDetail = db.tformularios, 
                s_url = URL(f='perfil_rol_formas', args=['master_'+str(request.function),request.args(1)]),
                s_idHtml = fn_createIDTabUnique(f = 'perfil_rol_formas', s_masterIdentificator = str(request.function))  )

    return (_D_returnVars)


def perfil_rol_formas():
    """ Asociación del perfil con la forma

    @descripcion Formas

    @keyword arg0: Perfil
    @keyword arg1: perfil_id
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature

    """
        
    _s_grupo_id = request.args(1, 0)
    _s_action = request.args(2, 'None')
    _s_forma_id = request.args(3, 0)
    
    _dbTable = db.tformularios

    def al_validar(O_form):
        if not O_form.vars.formulario_id:
            O_form.errors.formulario_id = "Formulario tiene que ser especificado"
        else:
            _L_formas_id = O_form.vars.formulario_id.split(",") 
            
            _s_errores = ""
            
            for _s_formulario_id in _L_formas_id:
                _dbRows = db( 
                    ( db.tformulario_permisos.formulario_id == _s_formulario_id ) 
                    & (db.tformulario_permisos.permiso_id == request.stv_fwk_permissions.btn_none.id) 
                    ).select('tformulario_permisos.id')
                if not _dbRows:
                    _s_errores += "La forma %s no contiene el permiso de Ninguno necesario para su uso en perfiles" % _s_formulario_id
                else:
                    pass
                
            if _s_errores:
                O_form.errors.formulario_id = _s_errores
            else:
                pass 
        return O_form
    
    def al_aceptar(O_form):
    
        _L_formas_id = O_form.vars.formulario_id.split(",") 

        for _s_formulario_id in _L_formas_id:
    
            if O_form.vars.aplicartodos:
                
                _dbRows = db( 
                    ( db.tformulario_permisos.formulario_id == _s_formulario_id )
                    &  (
                        ~db.tformulario_permisos.id.belongs( 
                            db( 
                                (db.auth_permission.group_id == _s_grupo_id)
                                )._select(
                                    db.auth_permission.record_id
                                    ) 
                            )
                        )
                    ).select(
                        db.tformulario_permisos.id
                        )            
                
            else:
                _dbRows = db( ( db.tformulario_permisos.formulario_id == _s_formulario_id ) & (db.tformulario_permisos.permiso_id == request.stv_fwk_permissions.btn_none.id) ).select('tformulario_permisos.id')
                
            if _dbRows:
                for _dbRow in _dbRows:
                    db.auth_permission.insert(group_id = _s_grupo_id, record_id = _dbRow.id);
            else:
                100/0 # Error, no se encontró el permiso de none en la forma a agregar
        
            
        return O_form
    
    # Referencia a la relacion db(db.auth_group.id == db.auth_permission.group_id)
    # Referencia a la relación db(db.tformulario_permisos.id == db.auth_permission.record_id)
    
    _L_btnsVisible = [
        request.stv_fwk_permissions.btn_none,
        request.stv_fwk_permissions.btn_new,
        request.stv_fwk_permissions.btn_save,
        request.stv_fwk_permissions.btn_cancel,
        #request.stv_fwk_permissions.btn_edit,
        request.stv_fwk_permissions.btn_remove,
        request.stv_fwk_permissions.btn_view,
        request.stv_fwk_permissions.btn_find,
        request.stv_fwk_permissions.btn_findall,
        ]
    
    _L_fieldsForm = [
        Field(
            'formulario_id', 'string', default=None,
            required=True, 
            requires = IS_IN_DB(
                db(
                    ~db.tformularios.id.belongs( db( (db.auth_permission.group_id == _s_grupo_id) & (db.tformulario_permisos.id == db.auth_permission.record_id) & (db.tformulario_permisos.permiso_id == 0) )._select(db.tformulario_permisos.formulario_id) )
                    ),
                'tformularios.id', db.tformularios._format
                ),
            ondelete='CASCADE', notnull=True, unique=False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 20), label=T('Form'), comment=None,
            writable=True, readable=False,
            represent=None
            ),
        Field(
            'aplicartodos', 'boolean', default=True,
            required = True,
            notnull = False,
            widget=stv_widget_inputCheckbox, label=('Aplicar todos los permisos'), comment = 'Aplicará todos los permisos de la forma.',
            writable = True, readable = True,
            represent = stv_represent_boolean
            ),    
        ]
        
    _O_cat = STV_FWK_FORM(
        dbReference = db(db.tformularios.id.belongs( db( (db.auth_permission.group_id == _s_grupo_id) & (db.tformulario_permisos.id == db.auth_permission.record_id) )._select(db.tformulario_permisos.formulario_id) )),
        dbTable = _dbTable,
        L_visibleButtons = _L_btnsVisible, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [_dbTable.id, _dbTable.descripcion, _dbTable.aplicacion_w2p, _dbTable.controlador_w2p, _dbTable.funcion_w2p], 
        L_fieldsToSearch = [_dbTable.descripcion, _dbTable.aplicacion_w2p, _dbTable.controlador_w2p, _dbTable.funcion_w2p], 
        L_fieldsToSearchIfDigit = [_dbTable.id],
        x_offsetInArgs = request.args[0:2],
        b_applyPermissons = False
        )

    _O_cat.addRowContentEvent('onValidation', al_validar)
    _O_cat.addRowContentEvent('onAccepted', al_aceptar)
    if (
        (_s_action == request.stv_fwk_permissions.btn_new.code)
        or (
            (_s_action == request.stv_fwk_permissions.btn_save.code)
            and not(_s_forma_id)
            )
        ):
        
        _D_returnVars = _O_cat.process(
            L_formFactoryFields = _L_fieldsForm,
            b_skipAction = True # La inserción se hace en los callbacks de al_aceptar
            )        
        
    elif (_s_action == request.stv_fwk_permissions.btn_remove.code):
        for _L_id in _O_cat.D_args.L_ids:
            _dbRows = db(db.tformulario_permisos.formulario_id == _L_id[0]).select(db.tformulario_permisos.id)
            for _dbRow in _dbRows:
                db((db.auth_permission.group_id == _s_grupo_id) & (db.auth_permission.record_id == _dbRow.id)).delete()
        _D_returnVars = _O_cat.process(L_formFactoryFields = _L_fieldsForm, b_skipAction = True, D_overrideView = request.stv_fwk_permissions.view_refresh)
    else:

        _O_cat.addRowSearchResults_addColumnBegin(
            es_typeColumn = STV_FWK_FORM.ES_TYPECOLUMN.S_LINKTAB,
            s_colname = "linktab",
            D_field = Storage(
                readable = True,
                represent = lambda value, dbRow: '<i class="fa fa-external-link-square" aria-hidden="true"></i>',
                ),
            m_classHtml = lambda D_column, dbRow: "stv_rowresults_linktab", 
            D_data = Storage(
                url = URL(f='perfil_rol_forma_permisos', args=request.args[:2] + ['master_'+str(request.function)]), 
                tablinkedtitle = db.auth_permission._plural, 
                tabcurrentfield = str(_dbTable.descripcion),
                idhtml = fn_createIDTabUnique(
                    f = 'perfil_rol_forma_permisos', 
                    s_masterIdentificator = str(request.function)
                    )
                )
            )
        
        _D_returnVars = _O_cat.process()

    return (_D_returnVars)


def _perfil_rol_forma_permisos_accepted(O_form):
    _s_groupId = request.args[5]
    _L_permisos = O_form.vars.record_id if isinstance(O_form.vars.record_id, (list, tuple)) else [str for str in O_form.vars.record_id.split(',')]
    for _s_idPermiso in _L_permisos:
        db.auth_permission.insert(record_id = _s_idPermiso, group_id = _s_groupId);
    pass

''' Forma detalle para formas de perfiles utilizando como vista por default catperfiles_formas.html '''
def perfil_rol_forma_permisos():
    _dbTable = db.auth_permission
    
    _s_idprofile = request.args[1]
    _s_idFormulario = request.args[3]
    
    if (len(request.args) <= 4):
        request.args.append('group_id')
        request.args.append(_s_idprofile)
    
    #record_id corresponde a tformulario_permisos.id
    _dbTable.record_id.represent = lambda v, r: ( 
                    db.tformulario_permisos(v).descripcion
                    )

    
    # En caso de edición utiliza el argumento en la posicion 3 que define el id de elemento a editar, de lo contrario, cubriendo el caso de Nuevo, regresa un 0
    if (len(request.args) > 6) and (request.args[6] == request.stv_fwk_permissions.btn_edit['code']):
        _qryPermiso_id = db.tformulario_permisos.id == db.auth_permission[request.args[7]].record_id
    else:
        _qryPermiso_id = db.tformulario_permisos.id < 0
    _dbTable.record_id.requires = IS_IN_DB( 
                                        db( ( (db.tformulario_permisos.baneliminar == False) & (db.tformulario_permisos.bansuspender == False) & (db.tformulario_permisos.formulario_id == _s_idFormulario) 
                                             & ( ~db.tformulario_permisos.id.belongs( db( (db.auth_permission.group_id == _s_idprofile) )._select(db.auth_permission.record_id) ) )
                                             ) | (_qryPermiso_id)
                                             ), 
                                        db.tformulario_permisos.id, 
                                        lambda r: ( 
                                            #( db(db.tformularios.id == db.tformulario_permisos[r.id].formulario_id).select().first().descripcion if db.tformulario_permisos[r.id] else T('None') ) 
                                            #+ ' / ' 
                                            #+ 
                                            r.descripcion)
                                        )
    _dbTable.record_id.label = T('Permission')
    
    _dbTable.name.writable = False
    _dbTable.name.readable = False
    _dbTable.table_name.writable = False
    _dbTable.table_name.readable = False
    _dbTable.group_id.writable = False
    _dbTable.group_id.readable = False
    _dbTable.bansuspender.writeable = False

    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_find)
    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_edit)
    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_signature)

    _O_cat = STV_FWK_FORM(
                dbReference = db,
                dbReferenceSearch = db( db.auth_permission.record_id.belongs( db( db.tformulario_permisos.formulario_id == _s_idFormulario )._select(db.tformulario_permisos.id) ) ),
                dbTable = _dbTable,
                dbTableMaster = db.auth_group, 
                xFieldLinkMaster = [_dbTable.group_id], 
                L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
                s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
                L_fieldsToShow = [_dbTable.id, _dbTable.record_id, _dbTable.bansuspender], 
                L_fieldsToSearch = [_dbTable.id, _dbTable.record_id], 
                L_fieldsToSearchIfDigit = [_dbTable.id, _dbTable.record_id],
                x_offsetInArgs = request.args[0:4],
                b_applyPermissons = False
                )

    # Si esta grabando y es un nuevo registro
    if (_O_cat.D_args.s_action in [request.stv_fwk_permissions.btn_save.code]) and (not _O_cat.D_args.n_id):
        _O_cat.addRowContentEvent('onAccepted', _perfil_rol_forma_permisos_accepted)
        _dbTable.record_id.widget = lambda f, v: stv_widget_db_chosen(f, v, 20)
        _D_returnVars = _O_cat.process(b_skipAction = True)
    # Si presiono el boton de nuevo
    elif (_O_cat.D_args.s_action in [request.stv_fwk_permissions.btn_new.code]):
        _dbTable.record_id.widget = lambda f, v: stv_widget_db_chosen(f, v, 20)
        _D_returnVars = _O_cat.process()        
    else:
        _D_returnVars = _O_cat.process()        
    
    return (_D_returnVars)  

def _usuarios_validation(form):
    _s_id = form.record_id
    D_stvFwkHelper.usuarios = Storage(emailchanged = False)
    # Si existe el id...
    if _s_id:
        # ...obten el registro del usuario.
        _row = db.auth_user(_s_id)
    # Si no tiene id (registro nuevo) o no se encontró el registro, o el email es diferente...
    if (not _s_id) or (not _row) or (_row.email != form.vars.email):
        # ...graba una flag para identificar que el email cambió si se graba el registro satisfactoriamente.
        D_stvFwkHelper.usuarios.emailchanged = True
    
    # Guardar el nombre de la foto.
    if request.vars.get('foto', '') != '':
        form.vars.fotoNombreArchivo = request.vars.foto.filename
    return form

def _usuarios_accepted(form):
    # Si existe la flag de que el email cambió...
    if D_stvFwkHelper.usuarios.emailchanged:
        # ...mandar el correo de recurperar password.
        db.commit() # Para que se actualicen los datos en la base de datos
        _s_id = form.vars.id
        if auth.email_reset_password(db.auth_user(_s_id)):
            request.stv_flashSuccess(
                s_title = "Notificación de email", 
                s_msg = "Correo mandado correctamente", 
                s_mode = 'stv_flash', 
                D_addProperties = {}
                )            
        else:
            if not (auth.settings.mailer):
                
                request.stv_flashError(
                    s_title = "Notificación de email", 
                    s_msg = "Error al mandar correo: mailer", 
                    s_mode = 'stv_flash', 
                    D_addProperties = {}
                    )            
            else:

                request.stv_flashError(
                    s_title = "Notificación de email", 
                    s_msg = "Error al mandar correo "+ str(auth.settings.mailer.error)+ "; "+ str(auth.settings.mailer.result), 
                    s_mode = 'stv_flash', 
                    D_addProperties = {}
                    )            
    return form
    
def foto():
    _bDisplay = False if request.vars.get('display') else True
    return response.download(request, db, attachment=_bDisplay, download_filename=request.vars.get('filename', None))

def fotomin():
    _bDisplay = False if request.vars.get('display') else True
    return response.download(request, db, attachment=_bDisplay, download_filename=request.vars.get('filename', None))

''' Forma maestro para perfiles utilizando como vista por default catperfiles.html '''
def usuarios():
    
    # Define una lista de los archivos a importar desde la configuración del sitio.
    importar_definiciones_aplicacion()
    importar_dbs_genericos()    

    _dbTable = db.auth_user
    
    _dbTable.password.writable = False
    _dbTable.password.readable = False
    
    #request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_remove)
    
    _O_cat = STV_FWK_FORM(
                dbReference = db,
                dbTable = _dbTable, 
                L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
                s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
                L_fieldsToShow = [_dbTable.id, _dbTable.first_name, _dbTable.last_name, _dbTable.email, _dbTable.zonahoraria_id, _dbTable.bansuspender], 
                L_fieldsToSearch = [_dbTable.first_name, _dbTable.last_name, _dbTable.email], 
                L_fieldsToSearchIfDigit = [_dbTable.id],
                b_applyPermissons = False
                )

    _O_cat.addSubTab(
                dbTableDetail = db.auth_membership, 
                s_url = URL(f='usuario_perfiles', args=['master_'+str(request.function),request.args(1)]),
                s_idHtml = fn_createIDTabUnique(f = 'usuario_perfiles', s_masterIdentificator = str(request.function))  )
    
    _O_cat.addRowContentEvent('onValidation', _usuarios_validation)
    _O_cat.addRowContentEvent('onAccepted', _usuarios_accepted)
    _D_returnVars = _O_cat.process()

    return (_D_returnVars)

def usuarios_buscar():
    ''' Forma de búsqueda para formas usuarios.html 
    '''
    _dbTable = db.auth_user
    
    _O_cat = STV_FWK_FORM(
                dbReference = db,
                dbTable = _dbTable, 
                L_visibleButtons = request.stv_fwk_permissions.L_formSearchButtons, 
                s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
                L_fieldsToShow = [_dbTable.id, _dbTable.first_name, _dbTable.last_name, _dbTable.email, _dbTable.bansuspender], 
                L_fieldsToSearch = [_dbTable.first_name, _dbTable.last_name, _dbTable.email], 
                L_fieldsToSearchIfDigit = [_dbTable.id],
                b_applyPermissons = False
                )
    
    _D_returnVars = _O_cat.process()
            
    return (_D_returnVars)

''' Forma detalle para formas de perfiles utilizando como vista por default catperfiles_formas.html '''
def usuario_perfiles():
    _dbTable = db.auth_membership

    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_find)

    _O_cat = STV_FWK_FORM(
                dbReference = db,
                dbReferenceSearch = db((_dbTable.group_id == db.auth_group.id) & (db.tcuentas.id == _dbTable.cuenta_id)),
                dbTable = _dbTable,
                dbTableMaster = db.auth_user, 
                xFieldLinkMaster = [_dbTable.user_id], 
                L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
                s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
                L_fieldsToShow = [_dbTable.id, _dbTable.cuenta_id, _dbTable.group_id, _dbTable.bansuspender], 
                L_fieldsToSearch = [_dbTable.id, _dbTable.cuenta_id, _dbTable.group_id, db.auth_group.role, db.tcuentas.nombrecorto], 
                L_fieldsToSearchIfDigit = [_dbTable.id, _dbTable.cuenta_id, _dbTable.group_id],
                b_applyPermissons = False
                )

    # El argumento en la posición 1 corresponde al id del maestro
    _n_usuario_id = _O_cat.D_args.L_idMaster[0]
        
    # Se asigna un widget de edición.
    _dbTable.group_id.widget = lambda f, v: stv_widget_db_chosen(f, v, 1)
    

    _D_returnVars = _O_cat.process()


    return (_D_returnVars)  


''' Forma detalle para usuarios por aplicación '''
def usuario_aplicaciones():
    _dbTable = db.tcuenta_aplicacion_usuarios
        
    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_find)

    _O_cat = STV_FWK_FORM(
                dbReference = db,
                dbTable = _dbTable,
                dbTableMaster = db.auth_user, 
                xFieldLinkMaster = [_dbTable.user_id], 
                L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
                s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
                L_fieldsToShow = [_dbTable.id, _dbTable.cuenta_aplicacion_id, _dbTable.descripcion, _dbTable.bansuspender], 
                L_fieldsToSearch = [_dbTable.descripcion], 
                L_fieldsToSearchIfDigit = [_dbTable.id, _dbTable.cuenta_aplicacion_id],
                b_applyPermissons = False
                )

    # El argumento en la posición 1 corresponde al id del maestro
    _n_usuario_id = _O_cat.D_args.L_idMaster[0]

    _qry = (db.tcuenta_aplicaciones.bansuspender == 0)

    # Si se desea crear un nuevo registro o editar uno actual...
    if _O_cat.D_args.s_action in (request.stv_fwk_permissions.btn_edit.code, request.stv_fwk_permissions.btn_save.code, request.stv_fwk_permissions.btn_new.code): 
        # En caso de edición se agrega el valor en edición
        _s_cuenta_aplicacion_id = _O_cat.D_args.L_ids[0][0] if _O_cat.D_args.L_ids else None
        if _O_cat.D_args.s_action in (request.stv_fwk_permissions.btn_edit.code):
            _n_editing_id = _dbTable(_s_cuenta_aplicacion_id).cuenta_aplicacion_id
        else:
            _n_editing_id = None
        _qry_sub_edit = (db.tcuenta_aplicaciones.id == _n_editing_id) if _n_editing_id else (db.tcuenta_aplicaciones.id < 0)

        # Se obtienen los renglones que se filtrarán de los resultados posibles a seleccionar que corresponde a los ids detalle que ya se encuentran en uso por el maestro.
        _rows = db( (_dbTable.user_id == _n_usuario_id) ).select(_dbTable.cuenta_aplicacion_id)
        _L_listIdsUsed = []
        for _row in _rows:
            if _row.cuenta_aplicacion_id != None:
                _L_listIdsUsed.append(_row.cuenta_aplicacion_id)
    
        _dbTable.cuenta_aplicacion_id.writable = True
        _dbTable.cuenta_aplicacion_id.requires = IS_IN_DB( 
                                db( 
                                    ( 
                                        (
                                            _qry &
                                            ( ~db.tcuenta_aplicaciones.id.belongs( _L_listIdsUsed ) ) 
                                        ) 
                                        | _qry_sub_edit  
                                    )
                                ), 
                                db.tcuenta_aplicaciones.id, 
                                lambda row: dbC(row.cuenta_id).nombrecorto + ' : ' + row.nombrecorto
                                )

    _dbTable.cuenta_aplicacion_id.readable = True

    _dbTable.user_id.writable = False
    _dbTable.user_id.readable = False

    _D_returnVars = _O_cat.process()
    
    return (_D_returnVars)


def logstables():
    _dbTable = db.tlogtableoperation
    
    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_new.code)
    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_remove.code)
    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_suspend.code)
    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_activate.code)
    
    _O_cat = STV_FWK_FORM(
                dbReference = db,
                dbTable = _dbTable, 
                s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
                L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
                L_fieldsToShow = [_dbTable.id, _dbTable.nombretabla, _dbTable.operacion, _dbTable.condicion, _dbTable.modificacion, _dbTable.notas, _dbTable.ip, _dbTable.operacionrealizada_en, _dbTable.operacionrealizada_por], 
                L_fieldsToSearch = [_dbTable.nombretabla, _dbTable.operacion, _dbTable.condicion, _dbTable.modificacion, _dbTable.notas, _dbTable.ip, _dbTable.operacionrealizada_en, _dbTable.operacionrealizada_por], 
                L_fieldsToSearchIfDigit = [_dbTable.id]
                )

    _D_returnVars = _O_cat.process(bApplyPermissons = False)

    
    return (_D_returnVars)


def _ver_controladorrutinas_disponibles_por_aplicacion(s_aplicacion_w2p):

    _D_return = FUNC_RETURN(
        LD_opciones = []
        )
    
    _dbRows = db(
        (db.tformularios.aplicacion_w2p == s_aplicacion_w2p)
        ).select(
            db.tformularios.controlador_w2p,
            db.tformularios.funcion_w2p,
            db.tformularios.bansuspender,
            orderby = [
                db.tformularios.controlador_w2p
                ]
            )
        
    _s_controlador_w2p_ultimoUsado = ""
    _L_opciones = []
    _L_rutinasYaUsadas = []
    _L_rutinasNoUsadas = []
    
    _L_controladores = STV_FWK2_VALIDACIONES.CONTROLADOR_W2P.OPCIONES(s_aplicacion_w2p)
    for _dbRow in _dbRows:
        
        if _dbRow.bansuspender:
            pass
        
        else:
            if _s_controlador_w2p_ultimoUsado != _dbRow.controlador_w2p:
                # Si el controlado no se ha utilizado
                
                for _s_opcion in _L_opciones:
                    # Por cada opción pendiente, que no se halla encontrado ya referenciada,
                    #  se agrega como no desabilitada
                    _L_rutinasNoUsadas.append(
                        {
                            'id': _s_controlador_w2p_ultimoUsado + "|" + _s_opcion,
                            'optionname': _s_controlador_w2p_ultimoUsado + " / " + _s_opcion,
                            'disabled': False
                            }
                        )
                _s_controlador_w2p_ultimoUsado = _dbRow.controlador_w2p

                # Se obtienen las opciones del controlador
                _L_opciones = STV_FWK2_VALIDACIONES.RUTINA_W2P.OPCIONES(
                    s_aplicacion_w2p, _s_controlador_w2p_ultimoUsado
                    )
                
                if _s_controlador_w2p_ultimoUsado in _L_controladores:
                    # Como el controlador ya se registró, se remueve de la lista de controladores a actualizar al final
                    _L_controladores.remove(_s_controlador_w2p_ultimoUsado)
                else:
                    pass
                    
            else:
                pass
            
            if _dbRow.funcion_w2p in _L_opciones:
                # Se empiezan a identificar las opciones que ya estan resgistradas en el sistema
                #  y se agregan como desabilidatas
                _L_rutinasYaUsadas.append(
                    {
                        'id': _s_controlador_w2p_ultimoUsado + "|" + _dbRow.funcion_w2p,
                        'optionname': _s_controlador_w2p_ultimoUsado + " / " + _dbRow.funcion_w2p,
                        'disabled': True
                        }
                    )
                _L_opciones.remove(_dbRow.funcion_w2p)

            else: 
                # Existe una forma que ya no esta en código, por lo que pudo ser renombrada o algo
                pass

    for _s_opcion in _L_opciones:
        # Por cada opción pendiente, que no se halla encontrado ya referenciada, se agrega como no desabilitada
        _L_rutinasNoUsadas.append(
            {
                'id': _s_controlador_w2p_ultimoUsado + "|" + _s_opcion,
                'optionname': _s_controlador_w2p_ultimoUsado + " / " + _s_opcion,
                'disabled': False
                }
            )

    for _s_controladorNoUsado in _L_controladores:
        # Para los controladores que no tienen ninguna rutina registrada en el sistema, se registran
        _L_opciones = STV_FWK2_VALIDACIONES.RUTINA_W2P.OPCIONES(s_aplicacion_w2p, _s_controladorNoUsado)
        for _s_opcion in _L_opciones:
            # Por cada opción pendiente, que no se halla encontrado ya referenciada, se agrega como no desabilitada
            _L_rutinasNoUsadas.append(
                {
                    'id': _s_controladorNoUsado + "|" + _s_opcion,
                    'optionname': _s_controladorNoUsado + " / " + _s_opcion,
                    'disabled': False
                    }
                )
    
    _D_return.LD_opciones = _L_rutinasNoUsadas + _L_rutinasYaUsadas

    return _D_return

        
def application_controladores_ver():
    
    _s_aplicacion_w2p = request.args(0, 'nada')
    _L_opciones = STV_FWK2_VALIDACIONES.CONTROLADOR_W2P.OPCIONES(_s_aplicacion_w2p)
    _LD_data = []

    for _s_opcion in _L_opciones:
        _LD_data.append(
            {'id': _s_opcion, 'optionname': _s_opcion}
            )

    return response.json(
        {'data': _LD_data}
        )


def application_controladorrutinas_ver():
    
    _s_aplicacion_w2p = request.args(0, 'nada')
    _D_result = _ver_controladorrutinas_disponibles_por_aplicacion(_s_aplicacion_w2p)
    return response.json(
        {'data': _D_result.LD_opciones}
        )


def application_rutinas_ver():
    
    _s_aplicacion_w2p = request.args(0, 'nada')
    _s_controlador_w2p = request.args(1, 'nada')
    _L_opciones = STV_FWK2_VALIDACIONES.RUTINA_W2P.OPCIONES(_s_aplicacion_w2p, _s_controlador_w2p)
    _LD_data = []

    _dbRows = db(
        (db.tformularios.aplicacion_w2p == _s_aplicacion_w2p)
        & (db.tformularios.controlador_w2p == _s_controlador_w2p)
        ).select(
            db.tformularios.funcion_w2p
            )
        
    _L_formulariosYaUsados = []
    for _dbRow in _dbRows:
        _L_formulariosYaUsados.append(_dbRow.funcion_w2p)

    for _s_opcion in _L_opciones:
        _LD_data.append(
            {
                'id'        : _s_opcion,
                'optionname': _s_opcion,
                'disabled'  : True if _s_opcion in _L_formulariosYaUsados else False
                }
            )

    return response.json(
        {'data': _LD_data}
        )
