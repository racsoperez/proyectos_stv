# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations
# __updated__ = '' 

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
#########################################################################

def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    #response.flash = {'type': 'info', 'title': 'stv', 'msg': 'hola', 'mode': 'swal'}
    
    response.flash = 'Hola bienvenidos'

    _db_rows = db(
        (db.tcuentas.id == db.tcuenta_aplicaciones.cuenta_id)
        ).select(
            db.tcuentas.id, 
            db.tcuentas.nombrecorto, 
            db.tcuenta_aplicaciones.id, 
            db.taplicaciones.aplicacion_w2p, 
            db.tcuenta_aplicaciones.nombrecorto, 
            left = [
                db.taplicaciones.on(
                    (db.taplicaciones.id == db.tcuenta_aplicaciones.aplicacion_id)
                    )
                ],
            orderby = [db.tcuentas.nombrecorto, db.tcuenta_aplicaciones.nombrecorto]
            )
    _db_tree = DB_Tree(_db_rows)
    _db_tree.addLevel([ 'tcuentas' ])
    _db_tree.addLevel([ 'tcuenta_aplicaciones', 'taplicaciones' ])
    
    _db_tree_applications = _db_tree.process()
    
    return dict(
                message = T('Welcome to web2py!'),
                dbTree = _db_tree_applications,
                )



@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def user():
    redirect(URL(a='acceso', c='default', f='user', vars=request.vars, args=request.args))


