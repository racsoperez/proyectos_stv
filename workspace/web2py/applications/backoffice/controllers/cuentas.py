# -*- coding: utf-8 -*-
# noinspection SpellCheckingInspection

def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    response.flash = T("Hello World")
    return dict(message=T('Welcome to web2py!'))


def cuentas_print_reporte():
    _dbTable = db.tcuentas

    _dbRows = db().select(_dbTable.id, _dbTable.nombre, _dbTable.nombrecorto, _dbTable.rfc)

    _rpt = STV_FWK_REPORT_PDF(
        n_pageSize_id = request.vars.paginatamanio, n_pageOrientation_id = request.vars.paginaorientacion,
        n_columnSeparationPoints = 2, n_columns = 17
        )

    _sImgPath = os.path.join('applications', 'abc_nomina', 'static', 'images', 'c01_logo.png')
    # _rpt.assignTitle_basic('Título', sSubTitle = "Subtítulo", sPathToImage = None);

    _lColumns = [
        Storage(
            size = 2, description = _dbTable.id.name.capitalize(), field = lambda x: x[_dbTable.id.name],
            alignData = 'right'
            ),
        Storage(
            size = 5, description = _dbTable.nombre.name.capitalize(), field = lambda x: x[_dbTable.nombre.name],
            alignData = 'left'
            ),
        Storage(
            size = 5, description = _dbTable.nombrecorto.name.capitalize(),
            field = lambda x: x[_dbTable.nombrecorto.name], alignData = 'left'
            ),
        Storage(
            size = 5, description = _dbTable.rfc.name.capitalize(), field = lambda x: x[_dbTable.rfc.name],
            alignData = 'left'
            ),
        ]

    _rpt.assignPageHeader(
                                    lColumns = _lColumns,
                                    )
    # _rpt.assignPageHeader(lColumns = _lColumns);

    return _rpt.generatePdf('archivo.pdf', 'titulo en Browser', _dbRows)


def cuentas_print():
    _dbTable = db.tcuentas

    _s_printType = None

    _O_cat = STV_FWK_FORM(
        dbReference = db,
        dbTable = None,
        L_visibleButtons = request.stv_fwk_permissions.L_formPrintButtons,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        b_applyPermissons = False,
        x_offsetInArgs = 1
        )

    _O_cat.updateTabOption("s_modalTitle", "Imprimir Empresas")

    if request.vars.get("fecha", False):
        _d_default = datetime.datetime.strptime(request.vars.fecha, '%d-%m-%Y')
    else:
        _d_default = request.browsernow

    _L_fieldsForm = [
        Field(
            'descripcion', 'string', length = 100,
            default = request.vars.get("descripcion", "Reporte de Empresas"),
            required = True,
            notnull = True,
            widget = stv_widget_input, label = 'Descripción', comment = '',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'fecha', 'date', default = _d_default,
            required = True, requires = IS_DATE(format = '%d-%m-%Y'),
            notnull = True,
            widget = stv_widget_inputDate, label = 'Fecha Reporte',
            comment = 'Fecha a imprimir en el reporte',
            writable = True, readable = True,
            represent = stv_represent_date
            ),

        ] + STV_FWK_REPORT_PDF.D_PAGEFIELDS.values() + STV_FWK_REPORT_PDF.D_SENDFIELDS.values()

    _D_returnVars = _O_cat.process(
        D_overrideView = request.stv_fwk_permissions.view_print,
        L_formFactoryFields = _L_fieldsForm,
        b_skipAction = True
        )

    _D_returnVars['htmlid_configuration'] = _O_cat.addSubTab(
        dbTableDetail = None,
        s_url = False,
        s_tabName = "Configuración",
        s_idHtml = None,
        s_type = "extended"
        )

    _D_returnVars['htmlid_page'] = _O_cat.addSubTab(
        s_tabName = "Página",
        s_type = "extended"
        )

    _D_returnVars['htmlid_send'] = _O_cat.addSubTab(
        s_tabName = "Envío",
        s_type = "extended"
        )

    if _D_returnVars.D_tabOptions.b_accepted:
        if _O_cat.D_args.s_action == request.stv_fwk_permissions.btn_print_print.code:
            _D_returnVars.D_tabOptions.D_redirect = Storage(
                s_url = URL(f = 'cuentas_print_reporte.pdf', args = request.args),
                x_config = dict(b_openNewWindowPDF = True),
                s_type = 'form',
                )

        elif _O_cat.D_args.s_action == request.stv_fwk_permissions.btn_print_save.code:
            _D_returnVars.D_tabOptions.D_redirect = Storage(
                s_url = URL(f = 'cuentas_print_reporte.pdf', args = request.args),
                x_config = dict(b_downloadPDF = True),
                s_type = 'form',
                )

    return _D_returnVars


def cuentas_imagen():
    return response.download(request, db)


def cuentas_imagenpin():
    return response.download(request, db)


def cuentas_archivos():

    if auth.is_logged_in():
        return response.download(request, db)
    else:
        return "Requiere estar registrado en el sistema"


def servidor_instruccion_archivo():
    """
    Link usado en instrucciones para bajar archivos configurados en las tablas
    
    @permiso sin_login:Permite acceder a la función sin estar logeado
    
    """

    _D_return = Storage(
        E_return = stvfwk2_e_RETURN.OK,
        s_msgError = "",
        )

    if not auth or not auth.is_logged_in():
        _D_return.E_return = stvfwk2_e_RETURN.NOK_LOGIN
        _D_return.s_msgError = "Requiere estar registrado en el sistema"

    elif not request.vars.s_instruccion_id:
        _D_return.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
        _D_return.s_msgError = "Requiere definir la instruccion"

    else:
        _dbRow = db.tservidor_instrucciones(request.vars.s_instruccion_id)
        if not _dbRow:
            _D_return.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
            _D_return.s_msgError = "Operación no existe"
        elif _dbRow.fecha < (request.browsernow - datetime.timedelta(days = 1)):
            _dbRow.estado = TSERVIDOR_INSTRUCCIONES.E_ESTADO.TIMEOUT
            _dbRow.update_record()
            _D_return.E_return = stvfwk2_e_RETURN.NOK_TIEMPO_EXCEDIDO
            _D_return.s_msgError = "Operación caduco, necesita generar instrucción nuevamente"
        else:
            return response.download(request, db)

    return str(simplejson.dumps(_D_return))


def cuentas_link():
    _dbTable = db.tcuentas

    response.view = "cuentas/cuentas_link.js"
    return Storage(dbRow = _dbTable[request.args(1)])


def cuentas_plus():
    _dbTable = db.tcuentas
    return Storage(dbRow = _dbTable[request.args(1)])


def cuentas():
    """ Forma maestro para cuentas utilizando como vista por default cuentas.html

    @return:
    @rtype:
    """

    def _configurarBotones():
        _O_cat.addPermissionConfig_option(
            request.stv_fwk_permissions.btn_print.code,
            s_optionCode = request.stv_fwk_permissions.form_print.code,
            s_optionLabel = 'Imprimir Catalogo',
            s_optionIcon = '',
            s_type = 'option',
            s_send = 'form',
            L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_print),
            D_actions = Storage(
                modal = Storage(
                    s_url = URL(f='cuentas_print', args=['printall']),
                    s_field = "",
                    )
                ),
            )
        _O_cat.addPermissionConfig_option(
            request.stv_fwk_permissions.btn_print.code,
            s_optionCode = request.stv_fwk_permissions.form_print.code,
            s_optionLabel = 'Imprimir 2',
            s_optionIcon = '',
            s_type = 'divider',
            s_send = 'formWithFind',
            L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_print),
            D_actions = Storage(),
            )
        _O_cat.addPermissionConfig_option(
            request.stv_fwk_permissions.btn_print.code,
            s_optionCode = request.stv_fwk_permissions.form_print.code,
            s_optionLabel = 'Imprimir Registro',
            s_optionIcon = '',
            s_type = 'option',
            s_send = 'formWithFind',
            L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_edit),
            D_actions = Storage(
                modal = Storage(
                    s_url = URL(f='cuentas_print', args=['printrow']),
                    s_field = "",
                    )
                ),
            )
        return

    def _aceptado(O_form):

        if O_form.vars.nombre_db and request.is_local:
            if D_stvSiteCfg and D_stvSiteCfg.s_nameConfigApp:
                _s_dirAppConfig = D_stvSiteCfg.s_nameConfigApp
            else:
                _s_dirAppConfig = '_app_config'

            _O_configuracion = AppConfig(
                os.path.join(request.folder, '..', _s_dirAppConfig, 'private', 'appconfig.ini'), reload = True
                )

            _s_nombreBaseDatos = _O_configuracion.take('localhost_db01.database_prefix') + O_form.vars.nombre_db

            _s_instruccion_sql = "SHOW DATABASES LIKE '%s';" % _s_nombreBaseDatos
            _dbRows_resultado = db.executesql(_s_instruccion_sql)

            if _dbRows_resultado:
                stv_flashInfo("Base de datos", "Base de datos ya existe")
            else:
                stv_flashSuccess(
                    "Base de datos",
                    "Base de datos no encontrada, requiere ser creada de forma manual: " + _s_nombreBaseDatos
                    )

                # _s_instruccion_sql = "CREATE DATABASE %s COLLATE %s;" % (_s_nombreBaseDatos, 'utf8_unicode_ci')
                # _dbRows_resultado = db.executesql(_s_instruccion_sql)
                # _s_instruccion_sql = "CREATE DATABASE %sbulk COLLATE %s;" % (_s_nombreBaseDatos, 'utf8_unicode_ci')
                # _dbRows_resultado = db.executesql(_s_instruccion_sql)

        else:
            pass

        return O_form

    _dbTable = db.tcuentas

    request.stv_fwk_permissions.L_formButtonsAll.insert(5, request.stv_fwk_permissions.btn_print)
    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_find)

    _O_cat = STV_FWK_FORM(
        dbReference = db,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.imagenpin,
            _dbTable.nombre,
            _dbTable.nombrecorto,
            _dbTable.rfc,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [_dbTable.nombre, _dbTable.nombrecorto, _dbTable.rfc],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        b_applyPermissons = False
        )

    _configurarBotones()

    _O_cat.addRowContentProperty("formControlUpload", [
        Storage(fieldUpload = _dbTable.imagen, fieldUploadFilename = _dbTable.imagennombrearchivo),
        Storage(fieldUpload = _dbTable.imagenpin, fieldUploadFilename = _dbTable.imagenpinnombrearchivo),
        Storage(fieldUpload = _dbTable.archivolicencia, fieldUploadFilename = _dbTable.archivolicencianombre)
        ])

    _O_cat.addRowContentEvent('onAccepted', _aceptado)

    _D_returnVars = _O_cat.process()

    _O_cat.addSubTab(
        dbTableDetail = db.tcuenta_usuarios,
        s_url = URL(f='cuenta_usuarios', args=['master_'+str(request.function), request.args(1)]),
        s_idHtml = fn_createIDTabUnique(f = 'cuenta_usuarios', s_masterIdentificator = str(request.function))
        )
    _O_cat.addSubTab(
        dbTableDetail = db.tcuenta_aplicaciones,
        s_url = URL(f='cuenta_aplicaciones', args=['master_'+str(request.function), request.args(1)]),
        s_idHtml = fn_createIDTabUnique(f = 'cuenta_aplicaciones', s_masterIdentificator = str(request.function))
        )
    _O_cat.addSubTab(
        dbTableDetail = db.tcuenta_domicilios,
        s_url = URL(f='cuenta_domicilios', args=['master_'+str(request.function), request.args(1)]),
        s_idHtml = fn_createIDTabUnique(f = 'cuenta_domicilios', s_masterIdentificator = str(request.function))
        )
    _O_cat.addSubTab(
        dbTableDetail = db.tcuenta_telefonos,
        s_url = URL(f='cuenta_telefonos', args=['master_'+str(request.function), request.args(1)]),
        s_idHtml = fn_createIDTabUnique(f = 'cuenta_telefonos', s_masterIdentificator = str(request.function))
        )
    _O_cat.addSubTab(
        dbTableDetail = db.tcuenta_empresas,
        s_url = URL(f='cuenta_empresas', args=['master_'+str(request.function), request.args(1)]),
        s_idHtml = fn_createIDTabUnique(f = 'cuenta_empresas', s_masterIdentificator = str(request.function))
        )
    _O_cat.addSubTab(
        dbTableDetail = db.tcuenta_servidorescontpaqi,
        s_url = URL(f='cuenta_servidorescontpaqi', args=['master_'+str(request.function), request.args(1)]),
        s_idHtml = fn_createIDTabUnique(f = 'cuenta_servidorescontpaqi', s_masterIdentificator = str(request.function))
        )
    _O_cat.addSubTab(
        dbTableDetail = db.tcuenta_perfilesusuarios,
        s_url = URL(f='cuenta_usuarios', args=['master_'+str(request.function), request.args(1)]),
        s_idHtml = fn_createIDTabUnique(f = 'cuenta_usuarios', s_masterIdentificator = str(request.function))
        )

    return _D_returnVars


def servidores():
    """ Formulario de servidores conectados al sistema.

    @descripcion Servidores del sistema

    @keyword [arg0]: operación a realizar en el formulario.
    @keyword [arg1]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_print
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    #@permiso special_permisson_01:[fas fa-money-check-alt] Uso Futuro
    """
    _D_return = FUNC_RETURN()

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 0,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_servidor_id = request.args(1, None),
        )

    _dbTabla = db.tservidores
    _D_useView = None
    _CLS_TABLA = TSERVIDORES.PROC()

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):
        # Si esta editando o guardando una edición, se configuran los campos
        _D_result = _CLS_TABLA.configuracampos_edicion(x_servidor = _D_args.s_servidor_id)
        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_result)
            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            pass

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _D_result = _CLS_TABLA.configuracampos_nuevo(s_servidor_id = _D_args.n_id)

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_result)
            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            pass

    else:
        pass

    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_find)

    _O_cat = STV_FWK_FORM(
        dbReference = db,
        dbTable = _dbTabla,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTabla.id,
            _dbTabla.descripcion, _dbTabla.codigo,
            _dbTabla.dt_grabazonah_leezonah, _dbTabla.dt_grabadirecto_leezonah, _dbTabla.dt_grabadirecto_leedirecto,
            _dbTabla.sesionesactivas,
            _dbTabla.serveruser_id, _dbTabla.contacto, _dbTabla.zonahoraria_id,
            _dbTabla.bansuspender,
            ],
        L_fieldsToSearch = [
            _dbTabla.descripcion, _dbTabla.codigo,
            _dbTabla.serveruser_id, _dbTabla.contacto,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTabla.id
            ],
        b_applyPermissons = False
        )

    _O_cat.addRowContentProperty(
        STV_FWK_FORM.ROWCONTENT.E_PROPERTY.FORM_CONTROLUPLOAD,
        [
            Storage(fieldUpload = _dbTabla.archivolibreria, fieldUploadFilename = _dbTabla.archivolibrerianombre),
            Storage(fieldUpload = _dbTabla.archivogestionador, fieldUploadFilename = _dbTabla.archivogestionadornombre),
            ]
        )

    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION, _CLS_TABLA.al_validar)
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED, _CLS_TABLA.AL_ACEPTAR)
    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE, _CLS_TABLA.ANTES_ELIMINAR,
        dbRow = None  # Llenado por el FWK
        )
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.AFTER_REMOVE, _CLS_TABLA.DESPUES_ELIMINAR)

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView,
        )

    _O_cat.addSubTab(
        dbTableDetail = db.tservidor_instrucciones,
        s_url = URL(
            f = 'servidor_instrucciones',
            args = ['master_' + str(request.function), _D_args.n_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'servidor_instrucciones',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = db.tservidor_conexiones,
        s_url = URL(
            f = 'servidor_log',
            args = ['master_' + str(request.function), _D_args.n_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'servidor_log',
            s_masterIdentificator = str(request.function)
            )
        )

    generaMensajeFlash(_dbTabla, _D_return)
    return _D_returnVars


def cuenta_empresas_ver():

    _s_cuenta_id = request.args(0, None)

    if _s_cuenta_id.isdigit() and int(_s_cuenta_id):
        # Primero se importa la información de la cuenta del cliente, para crear las referencias y todo a las tablas

        importar_definiciones_aplicacion()
        importar_dbs_genericos()
        importar_dbs_cuenta(s_remplaza_cuenta_id = _s_cuenta_id)

        if dbc01:
            _dbRows_cuenta_empresas = db(
                db.tcuenta_empresas.cuenta_id == _s_cuenta_id
                ).select(db.tcuenta_empresas.empresa_id)
            _L_empresas_ids = []
            for _dbRow_cuenta_empresa in _dbRows_cuenta_empresas:
                _L_empresas_ids.append(_dbRow_cuenta_empresa.empresa_id)

            _dbTable = dbc01.tempresas
            _dbRows_result = dbc01(
                _dbTable.id.belongs(_L_empresas_ids)
                ).select(
                    _dbTable.id,
                    _dbTable.nombrecorto,
                    _dbTable.rfc,
                    _dbTable.razonsocial
                    )
            _L_data = []
            for _dbRow in _dbRows_result:
                _L_data.append(
                    {
                        'id'        : _dbRow.id,
                        'optionname': (_dbTable._format % _dbRow.as_dict())
                        }
                    )

            return response.json({'data': _L_data})
        else:
            return response.json({'data': [], 'error': 'Argumentos inválidos'})
    else:
        return response.json({'data': [], 'error': 'Argumentos inválidos'})


def servidor_instrucciones():
    """ Instrucciones del servidor

    @descripcion Instrucciones

    @keyword arg0: Servidor
    @keyword arg1: servidor_id.
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.
    
    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    #@permiso btn_findall
    @permiso btn_signature
    
    """

    _D_return = FUNC_RETURN()
    # Define una lista de los archivos a importar desde la configuración del sitio.

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 2,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_servidor_id = request.args(1, 0),
        s_instruccion_id = request.args(3, None),
        E_tipoInstruccion = int(
            request.vars.get('nuevo_tipoinstruccion', TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.NADA)
            )
        )

    _dbTabla = db.tservidor_instrucciones
    _D_useView = None
    _CLS_TABLA = TSERVIDOR_INSTRUCCIONES.PROC(servidor_id = _D_args.s_servidor_id, user_id = auth.user_id)

    if _D_args.n_id and (_D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            )):
        # Si esta editando o guardando una edición, se configuran los campos
        _D_result = _CLS_TABLA.configuracampos_edicion(x_instruccion = _D_args.s_instruccion_id)
        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_result)
            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            pass

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _D_result = _CLS_TABLA.configuracampos_nuevo(
            s_instruccion_id = _D_args.n_id, E_tipoInstruccion = _D_args.E_tipoInstruccion
            )

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_result)
            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            pass

    elif _D_args.s_accion == request.stv_fwk_permissions.btn_view.code:

        _dbRow = _dbTabla(_D_args.n_id)
        _D_result = _CLS_TABLA.configuracampos_nuevo(
            s_instruccion_id = None, E_tipoInstruccion = _dbRow.tipoinstruccion,
            b_writable = False
            )
        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_result)
            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            pass

    else:
        pass

    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_findall)

    _O_cat = STV_FWK_FORM(
        dbReference = db,
        dbReferenceSearch = db,
        dbTable = _dbTabla,
        dbTableMaster = db.tservidores,
        xFieldLinkMaster = [_dbTabla.servidor_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTabla.id,
            _dbTabla.cuenta_id, _dbTabla.empresa_id, _dbTabla.user_id,
            _dbTabla.fecha, _dbTabla.tipoinstruccion, _dbTabla.servicio,
            _dbTabla.estado,
            _dbTabla.ultimo_resultado, _dbTabla.ultimo_fecha,
            _dbTabla.contador_errores, _dbTabla.numoperaciones,
            _dbTabla.ultimo_mensaje, _dbTabla.es_autoactualizable,
            ],
        L_fieldsToHide = [
            _dbTabla.ultimo_funcionalidad
            ],
        L_fieldsToSearch = [
            _dbTabla.fecha, _dbTabla.ultimo_mensaje,
            _dbTabla.s_param0, _dbTabla.s_param1, _dbTabla.s_param2, _dbTabla.s_param3,
            _dbTabla.dt_param4, _dbTabla.dt_param5, _dbTabla.s_param6, _dbTabla.s_param7,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTabla.id, _dbTabla.tipoinstruccion,
            _dbTabla.estado, _dbTabla.contador_errores,
            ],
        b_applyPermissons = False
        )

    _O_cat.addRowSearchResultsProperty(
        STV_FWK_FORM.ROWSEARCHRESULTS.PROPERTIES.ORDERBY, [~_dbTabla.editado_en]
        )

    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION, _CLS_TABLA.al_validar)
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED, _CLS_TABLA.AL_ACEPTAR)
    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE, _CLS_TABLA.ANTES_ELIMINAR,
        dbRow = None  # Llenado por el FWK
        )
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.AFTER_REMOVE, _CLS_TABLA.DESPUES_ELIMINAR)

    _O_cat.addRowContentProperty(
        STV_FWK_FORM.ROWCONTENT.E_PROPERTY.FORM_HIDDEN,
        {'nuevo_tipoinstruccion': _D_args.E_tipoInstruccion}
        )

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView,
        )

    _D_tipoinstrucciones = TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.GET_DICT()
    for _n_indice in sorted(_D_tipoinstrucciones):
        _O_cat.addPermissionConfig_option(
            request.stv_fwk_permissions.btn_new.code,
            s_optionCode = request.stv_fwk_permissions.btn_new.code,
            s_optionLabel = _D_tipoinstrucciones[_n_indice],
            s_optionIcon = '',
            s_type = 'option',
            s_send = '',
            L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_new),
            D_actions = Storage(
                D_data = Storage(nuevo_tipoinstruccion = _n_indice),
                )
            )

    generaMensajeFlash(_dbTabla, _D_return)
    return _D_returnVars


def servidor_log():
    """ Log de conexiones del servidor
    
    @keyword arg0: master de servidor_id.
    @keyword arg1: servidor_id.
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.
    
    @permiso btn_none
    #@permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    @permiso btn_find
    #@permiso btn_findall
    @permiso btn_signature
    
    """

    _dbTable = db.tservidor_conexiones
    _s_servidor_id = request.args(1, None)
    _s_action = request.args(2, None)
    _s_log_id = request.args(3, None)

    _dbQry = (
        (_dbTable.servidor_id == _s_servidor_id)
        | (_dbTable.servidor_id == None)
        )

    _O_cat = STV_FWK_FORM(
        dbReference = db,
        dbReferenceSearch = db(_dbQry),
        dbTable = _dbTable,
        dbTableMaster = None,
        xFieldLinkMaster = [],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.fecha,
            _dbTable.aplicacion_w2p,
            _dbTable.controlador_w2p,
            _dbTable.funcion_w2p,
            _dbTable.instruccion_id,
            _dbTable.ip,
            ],
        L_fieldsToSearch = [
            _dbTable.fecha,
            _dbTable.funcion_w2p,
            _dbTable.instruccion_id,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            _dbTable.instruccion_id,
            ],
        b_applyPermissons = False,
        x_offsetInArgs = request.args[0:2],
        )

    _O_cat.addRowSearchResultsProperty('orderBy', [~_dbTable.id])

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def cuentas_buscar():
    """ Forma de búsqueda para formas usuarios.html
    """
    _dbTable = db.tcuentas

    _O_cat = STV_FWK_FORM(
        dbReference = db,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formSearchButtons,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [_dbTable.id, _dbTable.nombre, _dbTable.nombrecorto, _dbTable.rfc, _dbTable.bansuspender],
        L_fieldsToSearch = [_dbTable.nombre, _dbTable.nombrecorto, _dbTable.rfc],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        b_applyPermissons = False
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def cuenta_domicilios():
    """ Forma detalle para opciones de formas utilizando como vista por default cuentas_opciones.html """
    _dbTable = db.tcuenta_domicilios

    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_find)

    _O_cat = STV_FWK_FORM(
        dbReference = db,
        dbTable = _dbTable,
        dbTableMaster = db.tcuentas,
        xFieldLinkMaster = [_dbTable.cuenta_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [_dbTable.id, _dbTable.nombrecorto, _dbTable.calleynumero, _dbTable.ciudad,
            _dbTable.bansuspender],
        L_fieldsToSearch = [_dbTable.id, _dbTable.codigopostal],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        b_applyPermissons = False
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def cuenta_telefonos():
    """ Forma detalle para opciones de formas utilizando como vista por default cuentas_opciones.html """

    _dbTable = db.tcuenta_telefonos

    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_find)

    _O_cat = STV_FWK_FORM(
                dbReference = db,
                dbTable = _dbTable,
                dbTableMaster = db.tcuentas,
                xFieldLinkMaster = [_dbTable.cuenta_id],
                L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
                s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
                L_fieldsToShow = [_dbTable.id, _dbTable.contacto, _dbTable.telefono, _dbTable.bansuspender],
                L_fieldsToSearch = [_dbTable.id],
                L_fieldsToSearchIfDigit = [_dbTable.id],
                b_applyPermissons = False
                )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def cuenta_aplicaciones():
    """ Forma detalle para opciones de formas utilizando como vista por default cuentas_opciones.html """

    _dbTable = db.tcuenta_aplicaciones

    _s_cuenta_id = request.args(1, None)

    # Primero se importa la información de la cuenta del cliente, para crear las referencias y todo a las tablas

    # Importar definiciones de la aplicación            
    importar_definiciones_aplicacion()

    # Importar definiciones de las bases de datos
    importar_dba(s_remplaza_cuenta_id = _s_cuenta_id)
    if dbc01:
        # Importar modelos de las aplicaciones
        importar_dbs_cuenta(s_remplaza_cuenta_id = _s_cuenta_id)

        # Se definien los requisitos y los represent
        _dbTable.empresa_id.widget = stv_widget_db_combobox
        _dbTable.empresa_id.requires = IS_IN_DB(dbc01, 'tempresas.id', dbc01.tempresas._format)
        _dbTable.empresa_id.represent = lambda n_value, odbRow: (
            dbc01.tempresas._format % dbc01(
                dbc01.tempresas.id == n_value
                ).select(
                    dbc01.tempresas.nombrecorto,
                    dbc01.tempresas.rfc
                    ).first().as_dict()
                ) if n_value else ""
    else:
        _dbTable.empresa_id.writable = False

    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_find)

    _O_cat = STV_FWK_FORM(
        dbReference = db,
        dbTable = _dbTable,
        dbTableMaster = db.tcuentas,
        xFieldLinkMaster = [_dbTable.cuenta_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.descripcion,
            _dbTable.nombrecorto,
            _dbTable.aplicacion_id,
            _dbTable.empresa_id,
            _dbTable.mostrar,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.descripcion,
            _dbTable.nombrecorto,
            ],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        b_applyPermissons = False
        )

    _O_cat.addRowContentProperty("formControlUpload", [
        Storage(
            fieldUpload = _dbTable.imagen,
            fieldUploadFilename = _dbTable.imagennombrearchivo
            ),
        Storage(
            fieldUpload = _dbTable.imagenpin,
            fieldUploadFilename = _dbTable.imagenpinnombrearchivo
            )
        ])

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def cuenta_usuarios():
    """ Forma detalle para usuarios por aplicación """
    _dbTable = db.tcuenta_usuarios

    _n_idcuenta = request.args(1, 0)

    # Si se desea crear un nuevo registro o editar uno actual...
    if (len(request.args) > 2) and (request.args[2] in request.stv_fwk_permissions.btn_edit['code']):
        db.auth_user._format = '%(first_name)s %(last_name)s [%(email)s]'
    else:
        db.auth_user._format = '%(id)s: %(first_name)s %(last_name)s [%(email)s]'

    _O_cat = STV_FWK_FORM(
        dbReference = db,
        dbReferenceSearch = db(_dbTable.user_id == db.auth_user.id),
        dbTable = _dbTable,
        dbTableMaster = db.tcuentas,
        xFieldLinkMaster = [_dbTable.cuenta_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [_dbTable.id, _dbTable.user_id, _dbTable.descripcion, _dbTable.bansuspender],
        L_fieldsToSearch = [_dbTable.descripcion, db.auth_user.first_name, db.auth_user.last_name, db.auth_user.email],
        L_fieldsToSearchIfDigit = [_dbTable.id, _dbTable.user_id],
        b_applyPermissons = False
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def _cuenta_empresas_validation(O_form):
    _s_empresa_id = O_form.vars.empresa_id
    _s_id = O_form.record_id

    if db(
            (db.tcuenta_empresas.empresa_id == _s_empresa_id)
            & (db.tcuenta_empresas.id != _s_id)
            ).count() > 0:
        O_form.errors.empresa_id = 'La empresa ya está registrada'
    else:
        pass
    return O_form


def cuenta_empresas():
    """ Forma detalle para definir las empresas que usara la cuenta.
    
    El uso especíifico es para poder identificar los RFCs que deben de usarse en los webservices al momento
    de bajar los CFDIs
    
    """
    _s_cuenta_id = request.args(1, None)

    if request.args(2, None) != request.stv_fwk_permissions.btn_new.code:
        _s_cuenta_empresa_id = request.args(3, None)
    else:
        _s_cuenta_empresa_id = None

    # Primero se importa la información de la cuenta del cliente, para crear las referencias y todo a las tablas

    importar_definiciones_aplicacion()
    importar_dbs_cuenta(s_remplaza_cuenta_id = _s_cuenta_id)

    _dbTable = db.tcuenta_empresas

    # Se evita el uso del boton buscar en la forma
    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_find)

    # Se definien los requisitos y los represent

    _dbRows = db(
        (_dbTable.cuenta_id == _s_cuenta_id)
        & (_dbTable.id != _s_cuenta_empresa_id)
        ).select(_dbTable.empresa_id.with_alias('id'))

    _dbTable.empresa_id.requires = IS_IN_DB(
        dbc01(~dbc01.tempresas.id.belongs(_dbRows)), 'tempresas.id', '%(id)s:%(nombrecorto)s [%(rfc)s]'
        )
    _dbTable.empresa_id.represent = \
        lambda n_value, odbRow: '%(id)s:%(nombrecorto)s [%(rfc)s]' \
                                % dbc01(dbc01.tempresas.id == n_value)\
                                    .select(dbc01.tempresas.id, dbc01.tempresas.nombrecorto, dbc01.tempresas.rfc)\
                                    .first().as_dict()

    _O_cat = STV_FWK_FORM(
        dbReference = db,
        dbTable = _dbTable,
        dbTableMaster = db.tcuentas,
        xFieldLinkMaster = [_dbTable.cuenta_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [_dbTable.id, _dbTable.empresa_id, _dbTable.bansuspender],
        L_fieldsToSearch = [_dbTable.id],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        b_applyPermissons = False
        )

    _D_returnVars = _O_cat.process()

    _O_cat.addSubTab(
        dbTableDetail = db.tcuenta_empresa_pagos,
        s_url = URL(
            f='cuenta_empresa_pagos',
            args=request.args[0:2] + ['master_'+str(request.function), request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'cuenta_empresa_pagos',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = db.tcuenta_empresa_certificados,
        s_url = URL(
            f='cuenta_empresa_certificados',
            args=request.args[0:2] + ['master_'+str(request.function), request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'cuenta_empresa_certificados',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = db.tcuenta_empresa_firmas,
        s_url = URL(
            f='cuenta_empresa_firmas',
            args=request.args[0:2] + ['master_'+str(request.function), request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'cuenta_empresa_firmas',
            s_masterIdentificator = str(request.function)
            )
        )

    return _D_returnVars


def cuenta_servidorescontpaqi():

    _dbTable = db.tcuenta_servidorescontpaqi

    _O_cat = STV_FWK_FORM(
        dbReference = db,
        dbReferenceSearch = db,
        dbTable = _dbTable,
        dbTableMaster = db.tcuentas,
        xFieldLinkMaster = [
            _dbTable.cuenta_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.nombreservidor,
            _dbTable.numeroserie,
            ],
        L_fieldsToSearch = [
            _dbTable.nombreservidor,
            _dbTable.numeroserie
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        b_applyPermissons = False
        )

    _O_cat.addRowContentEvent('onValidation', _cuenta_servidorescontpaqi_validation)

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def _cuenta_servidorescontpaqi_validation(O_form):
    """ Validación del código moneda contpaq

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """

    _dbTable = db.tcuenta_servidorescontpaqi

    _s_cuenta_id = request.args(1, None)

    # Validación: El ejercicio no puede duplicarse
    _dbRows = db(
        (_dbTable.cuenta_id == _s_cuenta_id)
        & (_dbTable.nombreservidor == O_form.vars.nombreservidor)
        & (_dbTable.id != O_form.record_id)  # Se ignora el registro que se esté modificando
        ).select(_dbTable.id)
    if _dbRows:
        O_form.errors.nombreservidor = "El nombre del servidor no puede duplicarse por cuenta."
    else:
        pass

    # Validación: El ejercicio no puede duplicarse
    _dbRows = db(
        (_dbTable.cuenta_id == _s_cuenta_id)
        & (_dbTable.numeroserie == O_form.vars.numeroserie)
        & (_dbTable.id != O_form.record_id)  # Se ignora el registro que se esté modificando
        ).select(_dbTable.id)
    if _dbRows:
        O_form.errors.numeroserie = "El número de serie no puede duplicarse por cuenta."
    else:
        pass

    return O_form


def cuenta_empresa_pagos():
    _dbTable = db.tcuenta_empresa_pagos

    # TODO Agregar validación de que cuenta_empresa_id, anio y mes no se pueden repetir

    _O_cat = STV_FWK_FORM(
        dbReference = db,
        dbTable = _dbTable,
        dbTableMaster = db.tcuenta_empresas,
        xFieldLinkMaster = [_dbTable.cuenta_empresa_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [_dbTable.id, _dbTable.anio, _dbTable.mes, _dbTable.comprobante, _dbTable.bansuspender],
        L_fieldsToSearch = [_dbTable.id],
        L_fieldsToSearchIfDigit = [_dbTable.id, _dbTable.anio, _dbTable.mes],
        x_offsetInArgs = request.args[0:2],
        b_applyPermissons = False
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def cuenta_empresa_certificados():
    _dbTable = db.tcuenta_empresa_certificados

    _O_cat = STV_FWK_FORM(
        dbReference = db,
        dbTable = _dbTable,
        dbTableMaster = db.tcuenta_empresas,
        xFieldLinkMaster = [_dbTable.cuenta_empresa_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.vigencia_cer,
            _dbTable.archivo_cer_nombre,
            _dbTable.archivo_cer,
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        x_offsetInArgs = request.args[0:2],
        b_applyPermissons = False
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def cuenta_empresa_firmas():
    _dbTable = db.tcuenta_empresa_firmas

    _O_cat = STV_FWK_FORM(
        dbReference = db,
        dbTable = _dbTable,
        dbTableMaster = db.tcuenta_empresas,
        xFieldLinkMaster = [_dbTable.cuenta_empresa_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.vigencia_firma,
            _dbTable.archivo_firma_nombre,
            _dbTable.archivo_firma,
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        x_offsetInArgs = request.args[0:2],
        b_applyPermissons = False
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def cuenta_perfilesusuarios():
    """ Forma detalle para opciones de formas utilizando como vista por default cuentas_opciones.html """

    _dbTable = db.tcuenta_perfilesusuarios

    _n_idcuenta = request.args(1, 0)

    _n_profile_id = _dbTable[request.args[3]].perfil_id \
        if (len(request.args) > 3) and (request.args[2] == request.stv_fwk_permissions.btn_edit.code) else None
    _dbTable.perfil_id.requires = IS_IN_DB(
        db(
            ~db.auth_group.id.belongs(
                db(_dbTable.cuenta_aplicacion_id == _n_idcuentaaplicacion)._select(_dbTable.perfil_id)
                )
            | (db.auth_group.id == _n_profile_id)
            ), 'auth_group.id', '%(role)s'
        )

    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_find)

    _O_cat = STV_FWK_FORM(
                dbReference = db,
                dbTable = _dbTable,
                dbTableMaster = db.tcuentas,
                xFieldLinkMaster = [_dbTable.cuenta_id],
                L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
                s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
                L_fieldsToShow = [_dbTable.id, _dbTable.perfil_id, _dbTable.descripcion, _dbTable.bansuspender],
                L_fieldsToSearch = [_dbTable.descripcion],
                L_fieldsToSearchIfDigit = [_dbTable.id, _dbTable.perfil_id],
                b_applyPermissons = False
                )

    _D_returnVars = _O_cat.process()

    return _D_returnVars
