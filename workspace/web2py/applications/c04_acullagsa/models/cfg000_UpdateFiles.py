# -*- coding: utf-8 -*-

''' Este archivo debe ser único por aplicación y no copiado, contiene las definiciones genéricas sobre las aplicaciones
principales y su uso
'''

# Código utilizado para actualizar la función stv_update en caso de requerirse y no usar su version compilada.
from gluon.custom_import import track_changes; track_changes(True)

# Imports
from gluon.storage import Storage
from applications._stv_fwk_r0v2.modules import stv_update


# Diccionario utilizado para la configuración del framework.
D_stvFwkCfg = Storage()
# Diccionario utilizado para la configuración del sitio.
D_stvSiteCfg = Storage()

D_stvFwkCfg.s_nameFwkApp = '_stv_fwk_r0v2' 
''' 
:var str D_stvFwkCfg.s_nameFwkApp: variable global que define el framework del cual obtener los archivos genéricos a utilizar
'''

# Define una lista de los archivos a importar desde el framework.
_L_fileFromFwk = [
    'cfg010_Definitions.py',
    'cfg030_Validaciones.py',  
    'db.py', 
    'db010_Genericas.py', 
    'db020_Usuarios.py', 
    'db110_Cuentas.py', 
    'db120_Menus.py'
    ]

# Importar los modelos definidos en el framework
stv_update.genericAppImport(D_stvFwkCfg.s_nameFwkApp, 'models', _L_fileFromFwk, globals())
