# -*- coding: utf-8 -*-

''' Este archivo debe ser único por aplicación y no copiado, contiene las definiciones genéricas sobre las aplicaciones
principales y su uso
'''

# Código utilizado para actualizar la función stv_update en caso de requerirse y no usar su version compilada.
from gluon.custom_import import track_changes; track_changes(True)

# Imports
from gluon.storage import Storage
from applications._stv_fwk_r0v2.modules import stv_update


# Diccionario utilizado para la configuración del framework.
D_stvFwkCfg = Storage()
# Diccionario utilizado para la configuración del sitio.
D_stvSiteCfg = Storage()

D_stvFwkCfg.s_nameFwkApp = '_stv_fwk_r0v2' 
''' 
:var str D_stvFwkCfg.s_nameFwkApp: variable global que define el framework del cual obtener los archivos genéricos a utilizar
'''

# Define una lista de los archivos a importar desde el framework.
_L_fileFromFwk = [
    'cfg010_Definitions.py', 
    'cfg030_Validaciones.py',     
    'cfg050_DefinitionsGC.py', 
    'cfg040_Reports.py',
    'cfg310_CFDI.py',
    'cfg311_CFDI_importar.py',
    'db.py', 
    'db010_Genericas.py', 
    'db020_Usuarios.py', 
    'db110_Cuentas.py', 
    'db120_Menus.py',
    'zz010_LastConfig.py'            
    ]

# TODO si no existen los archivos clas y widgets, importarlos del site-packages

# Importar los modelos definidos en el framework
stv_update.genericAppImport(D_stvFwkCfg.s_nameFwkApp, 'models', _L_fileFromFwk, globals())

'''
Se importan los archivos generales de las aplicaciones en el sitio que comparten la base de datos
'''

# Diccionario utilizado para la configuración del sitio.
D_stvSiteCfg = Storage()

# Se define la aplicación que contiene los archivos genéricos en el sitio.
D_stvSiteCfg.s_nameConfigApp = '_app_config' 

D_stvSiteCfg.s_dirSiteViews = os.path.join(request.folder, '..', D_stvSiteCfg.s_nameConfigApp, 'views') 

# Define una lista de los archivos a importar desde la configuración del sitio.
_L_fileFromConfig = [
    'cfga010g_Definitions.py', 
    'cfga010s_Definitions.py', 
    ]

# Importar los modelos definidos en el framework
stv_update.genericAppImport(D_stvSiteCfg.s_nameConfigApp, 'models', _L_fileFromConfig, globals())

# Se continua con la importación de las otra definiciones para la cuenta aplicación
importar_dbs_cuenta()