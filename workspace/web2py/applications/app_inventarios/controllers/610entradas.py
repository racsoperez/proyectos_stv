# -*- coding: utf-8 -*-


def movtospendientes():
    """ Formulario de Tareas Pendientes en el Almacén.

    Este fórmulario contendrá todas las tareas pendientes, movmientos abiertos en el almacén.

    @descripcion Movimientos pendientes

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Almacen
    @keyword arg3: empresa_almacen_id.
    @keyword [arg4]: operación a realizar en el formulario.
    @keyword [arg5]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_02:[fas fa-file-code] Cerrar movimiento
    """
    _D_return = Storage(
        E_return = CLASS_e_RETURN.OK,
        s_msgError = "",
        dbTable = dbc01.tempresa_inventariomovimientos,
        )

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 4,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_almacen_id = request.args(3, 0),
        )

    _dbTable = dbc01.tempresa_inventariomovimientos
    _D_useView = None
    _O_movto = TEMPRESA_INVENTARIOMOVIMIENTOS.InventarioInicial(
        s_empresa_id = _D_args.s_empresa_id,
        s_empresa_plaza_sucursal_almacen_id = _D_args.s_almacen_id
        )

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):
        # Si esta editando o guardando una edición
        _D_result = _O_movto.configuraCampos_edicion(x_empresa_invmovto = _D_args.n_id)
        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _D_result = _O_movto.configuraCampos_nuevo(
            s_empresa_inventariomovimiento_id = _D_args.n_id
            )

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            #Si entra acá es que ocurrió un error durante la obtención del concepto_id
            _D_return.E_return = _D_result.E_return
            _D_return.s_msgError = _D_result.s_msgError

            _D_useView = request.stv_fwk_permissions.view_refresh

        else:

            _dbTable.empresa_id.default = _D_args.s_empresa_id
            _dbTable.empresa_almacenesconcepto_id.default = _D_result.n_empresa_almacenconcepto_id

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.special_permisson_02.code,
            ):

        _D_result = _O_movto.checa_inconsistencias(
            x_empresa_invmovto = _D_args.n_id
            )

        if _D_result.E_return > CLASS_e_RETURN.OK:
            _D_return.E_return = _D_result.E_return
            _D_return.s_msgError = "Movimiento tiene incosistencias, favor de resolverlas: %s" % _D_result.s_msgError
        else:
            _dbRow_invMovto = _D_result.dbRow_invMovto

            _D_result = TEMPRESA_INVMOVTO_PRODUCTOS.AL_CERRAR_MOVIMIENTO(
                s_almacen_id = _D_args.s_almacen_id,
                s_invmovto_id = _D_args.n_id
                )

            if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.E_return = _D_result.E_return
                _D_return.s_msgError = _D_result.s_msgError
            else:

                _dbRow_invMovto.estatus_movimiento = TEMPRESA_INVENTARIOMOVIMIENTOS.ESTATUS_MOVIMIENTO.CERRADO
                _dbRow_invMovto.update_record()

        _D_useView = request.stv_fwk_permissions.view_refresh

    else:
        pass

    _dbQry = (
        _dbTable.estatus_movimiento != TEMPRESA_INVENTARIOMOVIMIENTOS.ESTATUS_MOVIMIENTO.CERRADO
        )

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_dbQry),
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_plaza_sucursal_almacenes,
        xFieldLinkMaster = [_dbTable.empresa_plaza_sucursal_almacen_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01,
            request.stv_fwk_permissions.special_permisson_02,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.empresa_almacenesconcepto_id,
            _dbTable.estatus_movimiento,
            _dbTable.fecha_movimiento,
            _dbTable.fecha_registro,
            _dbTable.descripcion,
            _dbTable.inconsistencias,
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        x_offsetInArgs = _D_args.n_offsetArgs - 2
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION,
        _O_movto.al_validar
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED,
        _O_movto.al_aceptar
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE,
        _O_movto.antes_eliminar,
        dbRow = None
        )

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_invmovto_productos,
        s_url = URL(
            f = 'inventarioinicial_productos',
            args = request.args[:4] + ['master_' + str(request.function), _D_args.n_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'inventarioinicial_productos',
            s_masterIdentificator = str(request.function)
            )
        )

    # if (_s_action == request.stv_fwk_permissions.btn_save.code) and (_D_returnVars.rowContent_form.accepted):
    #
    #     #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
    #     from gluon.storage import List
    #     request.args = List([request.stv_fwk_permissions.btn_view.code, _D_returnVars.rowContent_form.vars.id])
    #     _D_returnVars = movto_entrada_inventarioinicial()
    #
    # else:
    #     pass

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_02.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_02.code,
        s_permissionLabel = '',
        s_permissionIcon = '',
        s_type = 'button',
        s_send = '',
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            # modal = Storage(
            #     s_url = URL(
            #         f = 'rutina',
            #         args = request.args[:_D_args.n_offsetArgs]
            #         ),
            #     s_field = "",
            #     )
            ),
        )

    generaMensajeFlash(_dbTable, _D_return)

    return _D_returnVars


def inventarioinicial():
    """ Formulario de Entradas por inventario inicial.

    Al entrar al formulario si no existe inventario inicial y no existen movimientos en el almacen, crea el movimiento
    de inventario inicial y permite generar los movimientos.
    Sólo se permite un movimiento de entrada de inventario inicial por producto.

    @descripcion Entrada por inventario inicial

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Almacen
    @keyword arg3: empresa_almacen_id.
    @keyword [arg4]: operación a realizar en el formulario.
    @keyword [arg5]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_02:[fas fa-file-code] Cerrar movimiento
    """
    _D_return = Storage(
        E_return = CLASS_e_RETURN.OK,
        s_msgError = "",
        dbTable = dbc01.tempresa_inventariomovimientos,
        )

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 4,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_almacen_id = request.args(3, 0),
        )

    _dbTable = dbc01.tempresa_inventariomovimientos
    _D_useView = None
    _O_movto = TEMPRESA_INVENTARIOMOVIMIENTOS.InventarioInicial(
        s_empresa_id = _D_args.s_empresa_id,
        s_empresa_plaza_sucursal_almacen_id = _D_args.s_almacen_id
        )

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):
        # Si esta editando o guardando una edición
        _D_result = _O_movto.configuraCampos_edicion(x_empresa_invmovto = _D_args.n_id)
        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _D_result = _O_movto.configuraCampos_nuevo(
            s_empresa_inventariomovimiento_id = _D_args.n_id
            )

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            # Si entra acá es que ocurrió un error durante la obtención del concepto_id
            _D_return.E_return = _D_result.E_return
            _D_return.s_msgError = _D_result.s_msgError

            _D_useView = request.stv_fwk_permissions.view_refresh

        else:

            _dbTable.empresa_id.default = _D_args.s_empresa_id
            _dbTable.empresa_almacenesconcepto_id.default = _D_result.n_empresa_almacenconcepto_id

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.special_permisson_02.code,
            ):

        _D_result = _O_movto.checa_inconsistencias(
            x_empresa_invmovto = _D_args.n_id
            )

        if _D_result.E_return > CLASS_e_RETURN.OK:
            _D_return.E_return = _D_result.E_return
            _D_return.s_msgError = "Movimiento tiene incosistencias, favor de resolverlas: %s" % _D_result.s_msgError
        else:
            _dbRow_invMovto = _D_result.dbRow_invMovto

            _D_result = TEMPRESA_INVMOVTO_PRODUCTOS.AL_CERRAR_MOVIMIENTO(
                s_almacen_id = _D_args.s_almacen_id,
                s_invmovto_id = _D_args.n_id
                )

            if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.E_return = _D_result.E_return
                _D_return.s_msgError = _D_result.s_msgError
            else:

                _dbRow_invMovto.estatus_movimiento = TEMPRESA_INVENTARIOMOVIMIENTOS.ESTATUS_MOVIMIENTO.CERRADO
                _dbRow_invMovto.update_record()

        _D_useView = request.stv_fwk_permissions.view_refresh

    else:
        pass

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_dbTable.empresa_almacenesconcepto_id == _O_movto.n_empresa_almacenconcepto_id),
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_plaza_sucursal_almacenes,
        xFieldLinkMaster = [_dbTable.empresa_plaza_sucursal_almacen_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01,
            request.stv_fwk_permissions.special_permisson_02,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.estatus_movimiento,
            _dbTable.fecha_movimiento,
            _dbTable.fecha_registro,
            _dbTable.descripcion,
            _dbTable.inconsistencias,
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        x_offsetInArgs = _D_args.n_offsetArgs - 2
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION,
        _O_movto.al_validar
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED,
        _O_movto.al_aceptar
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE,
        _O_movto.antes_eliminar,
        dbRow = None
        )

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_invmovto_productos,
        s_url = URL(
            f = 'inventarioinicial_productos',
            args = request.args[:4] + ['master_' + str(request.function), _D_args.n_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'inventarioinicial_productos',
            s_masterIdentificator = str(request.function)
            )
        )

    # TODO al grabar inicial ir directo al detalle
    # if (_s_action == request.stv_fwk_permissions.btn_save.code) and (_D_returnVars.rowContent_form.accepted):
    #
    #     #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
    #     from gluon.storage import List
    #     request.args = List([request.stv_fwk_permissions.btn_view.code, _D_returnVars.rowContent_form.vars.id])
    #     _D_returnVars = movto_entrada_inventarioinicial()
    #
    # else:
    #     pass

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_02.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_02.code,
        s_permissionLabel = '',
        s_permissionIcon = '',
        s_type = 'button',
        s_send = '',
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            # modal = Storage(
            #     s_url = URL(
            #         f = 'rutina',
            #         args = request.args[:_D_args.n_offsetArgs]
            #         ),
            #     s_field = "",
            #     )
            ),
        )

    generaMensajeFlash(_dbTable, _D_return)

    return _D_returnVars


def inventarioinicial_productos():
    """ Formulario de productos de inventario inicial.

    @descripcion Productos

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Almacen
    @keyword arg3: empresa_almacen_id.
    @keyword arg4: Entrada Inventario Inicial
    @keyword arg5: inventariomovimiento_id.
    @keyword [arg6]: operación a realizar en el formulario.
    @keyword [arg7]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-file-code] Importar Productos
    """

    # noinspection DuplicatedCode
    _D_return = Storage(
        E_return = CLASS_e_RETURN.OK,
        s_msgError = "",
        )

    # noinspection DuplicatedCode
    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 6,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_almacen_id = request.args(3, 0),
        s_invmovto_id = request.args(5, 0),
        )

    _dbTable = dbc01.tempresa_invmovto_productos
    _D_useView = None
    _b_skipAction = False
    _L_formButtons = request.stv_fwk_permissions.L_formButtonsAll + [request.stv_fwk_permissions.special_permisson_01]
    _O_movtoProducto = TEMPRESA_INVMOVTO_PRODUCTOS.InventarioInicial(
        s_empresa_id = _D_args.s_empresa_id,
        s_empresa_plaza_sucursal_almacen_id = _D_args.s_almacen_id,
        s_invmovto_id = _D_args.s_invmovto_id
        )

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _D_result = _O_movtoProducto.configuraCampos_edicion(
            x_empresa_invmovto_producto = _D_args.s_invmovto_id
            )

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.E_return = CLASS_e_RETURN.OK_WARNING
            _D_return.s_msgError = (
                "Debido al estado actual del movimiento, solamente se permite actualizar las observaciones: %s"
                % _D_result.s_msgError
                )

        else:
            pass

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _D_result = _O_movtoProducto.configuraCampos_nuevo(
            s_empresa_invmovto_producto_id = _D_args.n_id
            )

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.E_return = CLASS_e_RETURN.OK_WARNING
            _D_return.s_msgError = _D_result.s_msgError
            _D_useView = request.stv_fwk_permissions.view_refresh
            _b_skipAction = True

        else:
            pass

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_none.code,
            ):
        _D_result = _O_movtoProducto.puede_crear()
        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _L_formButtons = request.stv_fwk_permissions.L_formButtonsMinimalUpdate
        else:
            pass

    else:
        pass

    _L_fieldsToShow = [
        _dbTable.id,
        dbc01.tempresa_prodserv.id.with_alias("Producto"),
        dbc01.tempresa_prodserv.codigo,
        dbc01.tempresa_prodserv.descripcion,
        _dbTable.producto_externo,
        _dbTable.cantidad,
        _dbTable.preciounitario,
        _dbTable.costototal_entrada,
        _dbTable.descripcion,
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_inventariomovimientos,
        xFieldLinkMaster = [
            _dbTable.empresa_inventariomovimiento_id
            ],
        L_visibleButtons = _L_formButtons,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        x_offsetInArgs = _D_args.n_offsetArgs - 2
        )

    _O_cat.addRowSearchResultsLeftJoin(
        dbc01.tempresa_prodserv.on(
            (dbc01.tempresa_prodserv.id == _dbTable.empresa_prodserv_id)
            )
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION,
        _O_movtoProducto.al_validar
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED,
        _O_movtoProducto.al_aceptar
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE,
        _O_movtoProducto.antes_eliminar,
        dbRow = None
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_type = "button",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_new),
        D_actions = Storage(
            D_data = Storage(),
            modal = Storage(
                s_url = URL(
                    a = 'app_timbrado',
                    c = '131almacenes_cfdis',
                    f = 'importar_excel',
                    args = _D_args.L_argsBasicos[:4],
                    vars = Storage(
                        master = request.function,
                        s_almacen_id = _D_args.s_almacen_id,
                        s_invmovto_id = _D_args.s_invmovto_id,
                        s_campoCantidad = dbc01.tempresa_invmovto_productos.cantidad_entrada.name
                        )
                    ),
                s_field = "",
                )
            )
        )

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView,
        b_skipAction = _b_skipAction
        )

    generaMensajeFlash(_dbTable, _D_return)

    return _D_returnVars


def porcompra():
    """ Formulario de Entradas por compra de Almacén.

    @descripcion Entrada por compra

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Almacen
    @keyword arg3: empresa_almacen_id.
    @keyword [arg4]: operación a realizar en el formulario.
    @keyword [arg5]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-file-code] Importar CFDI del proveedor
    @permiso special_permisson_02:[fas fa-ban] Cerrar movimientos
    """
    _D_return = Storage(
        E_return = CLASS_e_RETURN.OK,
        s_msgError = "",
        dbTable = dbc01.tempresa_inventariomovimientos,
        )

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 4,
        s_codigoDefault = request.stv_fwk_permissions.btn_new.code,
        s_empresa_id = request.args(1, 0),
        s_almacen_id = request.args(3, 0),
        )

    _dbTable = dbc01.tempresa_inventariomovimientos
    _D_useView = None
    _O_movto = TEMPRESA_INVENTARIOMOVIMIENTOS.PorCompra(
        s_empresa_id = _D_args.s_empresa_id,
        s_empresa_plaza_sucursal_almacen_id = _D_args.s_almacen_id
        )

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):
        # Si esta editando o guardando una edición
        _D_result = _O_movto.configuraCampos_edicion(x_empresa_invmovto = _D_args.n_id)
        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _D_result = _O_movto.configuraCampos_nuevo(
            s_empresa_inventariomovimiento_id = _D_args.n_id
            )

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            #Si entra acá es que ocurrió un error durante la obtención del concepto_id
            _D_return.E_return = _D_result.E_return
            _D_return.s_msgError = _D_result.s_msgError

            _D_useView = request.stv_fwk_permissions.view_refresh

        else:

            _dbTable.empresa_id.default = _D_args.s_empresa_id
            _dbTable.empresa_almacenesconcepto_id.default = _D_result.n_empresa_almacenconcepto_id

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.special_permisson_02.code,
            ):

        _D_result = _O_movto.checa_inconsistencias(
            x_empresa_invmovto = _D_args.n_id
            )

        if _D_result.E_return > CLASS_e_RETURN.OK:
            _D_return.E_return = _D_result.E_return
            _D_return.s_msgError = "Movimiento tiene incosistencias, favor de resolverlas: %s" % _D_result.s_msgError
        else:
            _dbRow_invMovto = _D_result.dbRow_invMovto

            _D_result = TEMPRESA_INVMOVTO_PRODUCTOS.AL_CERRAR_MOVIMIENTO(
                s_almacen_id = _D_args.s_almacen_id,
                s_invmovto_id = _D_args.n_id
                )

            if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.E_return = _D_result.E_return
                _D_return.s_msgError = _D_result.s_msgError
            else:

                _dbRow_invMovto.estatus_movimiento = TEMPRESA_INVENTARIOMOVIMIENTOS.ESTATUS_MOVIMIENTO.CERRADO
                _dbRow_invMovto.update_record()

        _D_useView = request.stv_fwk_permissions.view_refresh

    else:
        pass

    #
    # #_L_searchFilter se utiliza en la forma buscar para los CFDIs recibidos por proveedor.
    # _L_searchFilter = [
    #     Storage(
    #         s_input_name = str(_dbTable.empresa_proveedor_id),
    #         s_reference_name = str(dbc01.tcfdis.emisorproveedor_id),
    #         b_mandatory = True
    #         )
    #     ]
    #
    # _dbTable.cfdi_id.widget = lambda f, v: stv_widget_db_search(
    #     f,
    #     v,
    #     s_display = '%(serie)s %(folio)s',
    #     s_url = URL( a = 'app_almacenes', c = '600busquedas', f = 'buscar_cfdis_recibidos'),
    #     D_additionalAttributes = {
    #         'reference':dbc01.tcfdis.id,
    #         'L_searchFilter':_L_searchFilter,
    #         }
    #     )
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_dbTable.empresa_almacenesconcepto_id == _O_movto.n_empresa_almacenconcepto_id),
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_plaza_sucursal_almacenes,
        xFieldLinkMaster = [_dbTable.empresa_plaza_sucursal_almacen_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01,
            request.stv_fwk_permissions.special_permisson_02,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.estatus_movimiento,
            _dbTable.fecha_movimiento,
            _dbTable.fecha_registro,
            _dbTable.empresa_proveedor_id,
            _dbTable.tipodocumento,
            _dbTable.remision,
            _dbTable.cfdi_egreso_id,
            _dbTable.tc,
            _dbTable.ordencompra,
            _dbTable.folio,
            _dbTable.descripcion,
            _dbTable.inconsistencias,
            _dbTable.infoaduaneranopedimento,
            _dbTable.pais_id,
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        x_offsetInArgs = _D_args.n_offsetArgs - 2
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION,
        _O_movto.al_validar
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED,
        _O_movto.al_aceptar
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE,
        _O_movto.antes_eliminar,
        dbRow = None
        )

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView
        )


    # _O_cat.addRowContentEvent('onAccepted', TEMPRESA_INVENTARIOMOVIMIENTOS.ACEPTADO.ENTRADA.PORCOMPRA)

    # TODO, al aceptar nuevo, ir directo a editar detalle
    # if (_s_action == request.stv_fwk_permissions.btn_save.code) and (_D_returnVars.rowContent_form.accepted):
    #
    #     #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
    #     from gluon.storage import List
    #     request.args = List([request.stv_fwk_permissions.btn_view.code, _D_returnVars.rowContent_form.vars.id])
    #     _D_returnVars = movto_entrada_porcompra()
    #
    # else:
    #     _D_returnVars.update(
    #         dict(
    #             msg = 'Bienvenidos',
    #             dbFieldAlmacenes = STV_LIB_APP_ALMACENES.GET_DBFIELD_ALMACENES(
    #                 n_default = _s_empresa_plaza_sucursal_almacen_id or 0,
    #                 b_writable = False
    #                 ),
    #             L_breadcrub = [
    #                 STV_ALMACENES.BREADCRUB.BC_INDEX,
    #                 STV_ALMACENES.BREADCRUB.MOVTO.ENTRADA.BC_PORCOMPRA
    #                 ]
    #             )
    #         )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_invmovto_productos,
        s_url = URL(
            f = 'inventarioinicial_productos',
            args = request.args[:4] + ['master_' + str(request.function), _D_args.n_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'inventarioinicial_productos',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_type = "button",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_new),
        D_actions = Storage(
            D_data = Storage(),
            modal = Storage(
                s_url = URL(
                    a = 'app_timbrado',
                    c = '131almacenes_cfdis',
                    f = 'importar_cfdi',
                    args = _D_args.L_argsBasicos[:4],
                    vars = Storage(
                        master = request.function,
                        s_almacen_id = _D_args.s_almacen_id,
                        )
                    ),
                s_field = "",
                )
            )
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_02.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_02.code,
        s_permissionLabel = '',
        s_permissionIcon = '',
        s_type = 'button',
        s_send = '',
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            # modal = Storage(
            #     s_url = URL(
            #         f = 'rutina',
            #         args = request.args[:_D_args.n_offsetArgs]
            #         ),
            #     s_field = "",
            #     )
            ),
        )

    generaMensajeFlash(_dbTable, _D_return)

    return _D_returnVars


def movto_entrada_porcompra_resumen():
    '''Función para las entradas por compra por remisión 
    
        Args:
            arg0: master_[nombre función maestro]
            arg1: empresa_inventariomovimiento_id
            [arg2]: codigo de funcionalidad
            [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    '''
    
    _dbTable = dbc01.tempresa_invmovto_productos
    
    _s_empresa_inventariomovimiento_id = request.args(1, None)

    _s_action = request.args(2, request.stv_fwk_permissions.btn_none.code)

    _s_empresa_inventariomovimientoresumen_id = request.args(3, None)

    _dbTable.descripcion.label = "Descripción CFDI"

    _dbTable.cfdi_concepto_id.readable = True
    _dbTable.cfdi_concepto_id.writable = True

    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.empresa_producto_id,
        _dbTable.cfdi_concepto_id,
        _dbTable.producto_externo,
        _dbTable.cantidad_entrada,
        _dbTable.preciounitario,
        _dbTable.costototal,
        _dbTable.tipo_inconsistencia,        
        ]
    
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_inventariomovimientos,
        xFieldLinkMaster = [
            _dbTable.empresa_inventariomovimiento_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
        )
    
    _dbRow_cfdi_concepto = None
        
    if(_s_empresa_inventariomovimientoresumen_id):
        # Si se encuentra viendo o editando o grabando y se identifica el resumen en el CFDI, se crea un registro para mostrar la inconsistencia en la vista.
        # Así mismo si coincide el producto_externo con el noidentificación se desabilita la edicion del produto_externo
        
        _dbRow_movimientoMaestro = dbc01.tempresa_inventariomovimientos(_s_empresa_inventariomovimiento_id)
        
        if(_dbRow_movimientoMaestro and _dbRow_movimientoMaestro.cfdi_id):
            
            _dbRow_movimientoResumen = _dbTable(_s_empresa_inventariomovimientoresumen_id)

            _dbRows_cfdi_conceptos = dbc01(
                (dbc01.tcfdi_conceptos.cfdi_id == _dbRow_movimientoMaestro.cfdi_id)
                &(dbc01.tcfdi_conceptos.noidentificacion == _dbRow_movimientoResumen.producto_externo)
                ).select(
                    dbc01.tcfdi_conceptos.id,
                    dbc01.tcfdi_conceptos.empresa_prodserv_id,
                    dbc01.tcfdi_conceptos.noidentificacion,
                    dbc01.tcfdi_conceptos.cantidad,
                    dbc01.tcfdi_conceptos.valorunitario,
                    dbc01.tcfdi_conceptos.descripcion,
                    dbc01.tcfdi_conceptos.claveunidad,
                    dbc01.tcfdi_conceptos.importe,
                    dbc01.tempresa_invmovto_productos.cantidad_entrada.coalesce_zero().sum().with_alias('registrados'),
                    left = [
                        dbc01.tempresa_invmovto_productos.on(
                            (dbc01.tempresa_invmovto_productos.cfdi_concepto_id == dbc01.tcfdi_conceptos.id)
                            & (dbc01.tempresa_invmovto_productos.id != _s_empresa_inventariomovimientoresumen_id)
                            ),
                        ],
                    groupby = [
                        dbc01.tcfdi_conceptos.id,
                        dbc01.tcfdi_conceptos.empresa_prodserv_id,
                        dbc01.tcfdi_conceptos.noidentificacion,
                        dbc01.tcfdi_conceptos.cantidad,
                        dbc01.tcfdi_conceptos.valorunitario,
                        dbc01.tcfdi_conceptos.descripcion,
                        dbc01.tcfdi_conceptos.claveunidad,
                        dbc01.tcfdi_conceptos.importe,
                        ]
                    
                    )
            
            #Si no trajo concepto es porque no existe registro con el producto externo que se escribió en el formulario, se revisa si por producto aparece el concepto.    
            if not (_dbRows_cfdi_conceptos) and (_dbRow_movimientoResumen.empresa_producto_id):
                
                #Query para saber si existe un concepto con el empresa_producto_id en ese CFDI.
                _dbRows_cfdi_conceptos = dbc01(
                    (dbc01.tcfdi_conceptos.cfdi_id == _dbRow_movimientoMaestro.cfdi_id)
                    &(dbc01.tcfdi_conceptos.empresa_prodserv_id == _dbRow_movimientoResumen.empresa_producto_id)
                    ).select(
                        dbc01.tcfdi_conceptos.id,
                        dbc01.tcfdi_conceptos.empresa_prodserv_id,
                        dbc01.tcfdi_conceptos.noidentificacion,
                        dbc01.tcfdi_conceptos.cantidad,
                        dbc01.tcfdi_conceptos.valorunitario,
                        dbc01.tcfdi_conceptos.descripcion,
                        dbc01.tcfdi_conceptos.claveunidad,
                        dbc01.tcfdi_conceptos.importe,
                        dbc01.tempresa_invmovto_productos.cantidad_entrada.coalesce_zero().sum().with_alias('registrados'),
                        left = [
                            dbc01.tempresa_inventariomovimientos.on(
                                (dbc01.tempresa_inventariomovimientos.cfdi_id == dbc01.tcfdi_conceptos.cfdi_id)
                                ),
                            dbc01.tempresa_invmovto_productos.on(
                                (dbc01.tempresa_invmovto_productos.empresa_inventariomovimiento_id == dbc01.tempresa_inventariomovimientos.id)
                                & (dbc01.tempresa_invmovto_productos.empresa_producto_id == dbc01.tcfdi_conceptos.empresa_prodserv_id)
                                & (dbc01.tempresa_invmovto_productos.id != _s_empresa_inventariomovimientoresumen_id)
                                ),
                            ],
                        groupby = [
                            dbc01.tcfdi_conceptos.id,
                            dbc01.tcfdi_conceptos.empresa_prodserv_id,
                            dbc01.tcfdi_conceptos.noidentificacion,
                            dbc01.tcfdi_conceptos.cantidad,
                            dbc01.tcfdi_conceptos.valorunitario,
                            dbc01.tcfdi_conceptos.descripcion,
                            dbc01.tcfdi_conceptos.claveunidad,
                            dbc01.tcfdi_conceptos.importe,
                            ]
                        
                        )
                
            else:
                pass
                
            if len(_dbRows_cfdi_conceptos) == 1:
                if(_dbRow_movimientoResumen.producto_externo == _dbRows_cfdi_conceptos.last().tcfdi_conceptos.noidentificacion):
                    _dbTable.producto_externo.writable = False
                else:
                    pass
                _dbRow_cfdi_concepto = _dbRows_cfdi_conceptos.last()                    
                
            else:
                pass
        else:
            pass
    else:
        pass


    _O_cat.addRowContentEvent('onValidation', TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.ENTRADA.PORCOMPRA_RESUMEN)
   
    _D_returnVars = _O_cat.process()

    if ((_s_action == request.stv_fwk_permissions.btn_save.code) and (_D_returnVars.rowContent_form.accepted)):
        
        _D_result = TEMPRESA_INVMOVTO_PRODUCTOS.VALIDAR_ROW_ENTRADA_COMPRA(
            _D_returnVars.rowContent_form.vars.id
            )
        
        if _D_result.E_return != CLASS_e_RETURN.OK:
            stv_flashWarning("Inconsistencias en producto", _D_result.s_msgError)  
        else:
            pass

    else:
        #Si es cualquier opción en vez de salvar no hace nada
        pass
    
    _D_returnVars.dbRow_cfdi_concepto = _dbRow_cfdi_concepto
    
    return _D_returnVars

def pordevolucion():
    '''Función para las entradas por devolución
            
        Args:
            [arg0]: codigo de funcionalidad
            [arg1]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    '''
    
    _s_action = request.args(0, None)
    
    _dbTable = dbc01.tempresa_inventariomovimientos
    
    _s_empresa_id = db.tcuenta_aplicaciones(D_stvFwkCfg.cfg_cuenta_aplicacion_id).empresa_id
    _dbTable.empresa_id.default = _s_empresa_id
    
    _dbRows_conceptos = dbc01(
        (dbc01.tempresa_almacenesconceptos.empresa_id == _s_empresa_id)
        & (dbc01.tempresa_almacenesconceptos.tipoconcepto == TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.ENTRADA)
        & (dbc01.tempresa_almacenesconceptos.clasificacion == TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.DEVOLUCION_VENTA)
        ).select(
            dbc01.tempresa_almacenesconceptos.id,
            orderby = ~dbc01.tempresa_almacenesconceptos.id
            )
        
    if _dbRows_conceptos:
        _dbTable.empresa_almacenesconcepto_id.default = _dbRows_conceptos.first().id
        _dbTable.empresa_almacenesconcepto_id.writable = False
    else:
        #TODO  Mostrar error de que no existe un concepto de entrada por compra para la empresa
        pass
    
    #El Query tiene que mostrar las entradas que estén pendientes por asociar un CFDI. 
    _qry = (
        (_dbTable.empresa_id == _s_empresa_id)
        &(_dbTable.empresa_almacenesconcepto_id == _dbRows_conceptos.first().id)
        &(_dbTable.cfdi_id == None)
        )
    
    _L_dbFields = [
        _dbTable.id,
        _dbTable.empresa_proveedor_id,
        _dbTable.tipodocumento,
        _dbTable.serie,
        _dbTable.folio,
        _dbTable.remision,
        ]
    
    _L_formButtons = [
        request.stv_fwk_permissions.btn_none,
        request.stv_fwk_permissions.btn_new,
        request.stv_fwk_permissions.btn_save,
        request.stv_fwk_permissions.btn_cancel,
        request.stv_fwk_permissions.btn_edit,
        request.stv_fwk_permissions.btn_view,
        #request.stv_fwk_permissions.btn_print,
        #request.stv_fwk_permissions.btn_invalidate,
        #request.stv_fwk_permissions.btn_validate,
        #request.stv_fwk_permissions.btn_suspend,
        #request.stv_fwk_permissions.btn_activate,
        #request.stv_fwk_permissions.btn_remove,
        #request.stv_fwk_permissions.btn_restore,
        request.stv_fwk_permissions.btn_find,
        request.stv_fwk_permissions.btn_findall,
        #request.stv_fwk_permissions.btn_signature,
        ]
        
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = _L_formButtons, 
        s_tabId = STV_CFG_APP_ALMACENES.ID,
        L_fieldsToShow = _L_dbFields, 
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
    )
    
    
    
    if _s_action in (request.stv_fwk_permissions.btn_new.code, request.stv_fwk_permissions.btn_edit.code, request.stv_fwk_permissions.btn_save.code, request.stv_fwk_permissions.btn_view.code):
        # Acciones permitidas
        pass
    else:
        _s_action = request.stv_fwk_permissions.btn_new.code
    
    _D_returnVars = _O_cat.process(
        s_overrideCode = _s_action,  # Usar el action 
        b_skipAction = False,
        #L_formFactoryFields = _L_dbFields
        )

    if (_s_action == request.stv_fwk_permissions.btn_save.code) and (_D_returnVars.rowContent_form.accepted):
        #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
        from gluon.storage import List
        request.args = List([request.stv_fwk_permissions.btn_view.code, _D_returnVars.rowContent_form.vars.id])
        _D_returnVars = movto_entrada_porcompra()
         
    else:
        _D_returnVars.update(
            dict(
                msg = 'Bienvenidos',
                dbFieldAlmacenes = STV_LIB_APP_ALMACENES.GET_DBFIELD_ALMACENES(
                    n_default = request.vars.master_empresa_plaza_sucursal_almacen_id or 0,
                    b_writable = False
                    ),
                L_breadcrub = [
                    STV_ALMACENES.BREADCRUB.BC_INDEX,
                    STV_ALMACENES.BREADCRUB.MOVTO.ENTRADA.BC_PORDEVOLUCION
                    ]
                )
            )
        
#        _D_returnVars.D_useView.L_actionsCreateView += [request.stv_fwk_permissions.btn_new.code, request.stv_fwk_permissions.btn_edit.code]
        _D_returnVars.D_useView.E_buttonsBarPosition = request.stv_fwk_permissions.E_POSITION_DOWN

    if(request.vars.tipodocumento):
        if(int(request.vars.tipodocumento) == TEMPRESA_INVENTARIOMOVIMIENTOS.E_TIPODOCUMENTO.REMISION):
            _O_cat.addSubTab(
                dbTableDetail = dbc01.tempresa_invmovto_productos, 
                s_url = URL(f='movto_entrada_porcompra_remision', args=['master_'+str(request.function),request.args(1)]),
                s_idHtml = fn_createIDTabUnique(f = 'movto_entrada_porcompra_remision', s_masterIdentificator = str(request.function))  
                )
        else:
            #Ver que subtab abrirá directamente si es por factura
            pass
    else:
        pass
        
    return _D_returnVars

def _movto_entrada_pordevolucion_validation(O_form):
    '''Validación para las entradas por devolución
    '''
    
    if not O_form.vars.remision:
        O_form.errors.remision = "Es requerido"
    else:
        pass
    
    return O_form

def portraspaso():
    '''Función para las entradas por traspaso de almacén
        Args:
            [arg0]: codigo de funcionalidad
            [arg1]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    '''
    
    _s_action = request.args(0, request.stv_fwk_permissions.btn_none.code)
        
    _dbTable = dbc01.tempresa_inventariomovimientos
    
    _s_empresa_plaza_sucursal_almacen_id = str(request.vars.master_empresa_plaza_sucursal_almacen_id)
    
    _s_cuenta_aplicacion_id = str(D_stvFwkCfg.cfg_cuenta_aplicacion_id)

    _s_empresa_inventariomovimiento_id = request.args(1, None)
    
    _D_result_empresa_id = TCUENTA_APLICACIONES.GET_EMPRESA_ID_CACHE(
        _s_cuenta_aplicacion_id
        )
    
    if _D_result_empresa_id.E_return != CLASS_e_RETURN.OK:
        #Si entra acá es que ocurrió un error durante la obtención del concepto_id
        _s_empresa_id = None
        stv_flashError("Empresa ID no encontrado.", _D_result_empresa_id.s_msgError)    
    elif _D_result_empresa_id.n_empresa_id:
        _s_empresa_id = _D_result_empresa_id.n_empresa_id
    else:
        #hubo bronca, no trajo error pero tampoco trajo la empresa_id
        _s_empresa_id = None
        stv_flashError("Empresa ID no encontrado.", "Empresa no definida para almacenes.")     
        
        
    _D_result_concepto_id = TEMPRESA_ALMACENESCONCEPTOS.GET_ID_CACHE(
        n_empresa_id = _s_empresa_id, 
        n_tipoconcepto = TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.ENTRADA, 
        n_clasificacion = TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.TRASPASO_ENTRADA
        )
    
    # Por default el query se pone en false para no regresar nada en las búsquedas
    _qry = False
    
    if _D_result_concepto_id.E_return != CLASS_e_RETURN.OK:
        #Si entra acá es que ocurrió un error durante la obtención del concepto_id
        _n_concepto_id = None
        stv_flashError("Entrada por traspaso", _D_result_concepto_id.s_msgError)        
    elif _D_result_concepto_id.n_concepto_id:
        _n_concepto_id = _D_result_concepto_id.n_concepto_id
        _dbTable.empresa_almacenesconcepto_id.default = _n_concepto_id
        _dbTable.empresa_almacenesconcepto_id.writable = False

        _qry = (
            (_dbTable.empresa_id == _s_empresa_id)
            &(_dbTable.empresa_almacenesconcepto_id == _n_concepto_id)
            &(_dbTable.empresa_plaza_sucursal_almacen_id == _s_empresa_plaza_sucursal_almacen_id)
            )
        
#         ###TODO: ¿en este caso cuales son los pendientes que se tienen que mostrar?
#         if _s_action in (request.stv_fwk_permissions.btn_new.code, request.stv_fwk_permissions.btn_none.code):
#             #Se agrega este query.
# #             _qry &= (None)
#             pass
#         else:
#             #Si es buscar todo el Query no aplica, ya que se puede filtrar todas las salidas
#             pass
    else:
        #hubo bronca, no trajo error pero tampoco trajo concepto_id
        _n_concepto_id = None
        stv_flashError("Entrada por traspaso", "Concepto no defnido en Almacenes-Conceptos")        

    _dbTable.empresa_id.default = _s_empresa_id
    _dbTable.empresa_plaza_sucursal_almacen_id.default = _s_empresa_plaza_sucursal_almacen_id

    #Se asigna la fecha de registro con el día y hora actual. 
    _dbTable.fecha_registro.writable = False
    _dbTable.fecha_registro.default = request.browsernow
    
    _dbTable.ordencompra.label = 'Orden de traspaso'
    
    _dbTable.empresa_embarque_almacen_id_relacionado.label = 'Almacén origen'
    
    _dbTable.empresa_embarque_almacen_id_relacionado.required = True

    _dbTable.empresa_embarque_almacen_id_relacionado.notnull = True

    _dbTable.empresa_embarque_id.required = True

    _dbTable.empresa_embarque_id.notnull = True
    
    _n_concepto_salida_traspaso_id = TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.GET_CONCEPTO_ID(
        n_empresa_id = _s_empresa_id, 
        n_tipoconcepto = TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.SALIDA,
        n_clasificacion = TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.TRASPASO_SALIDA
        )
    
    
    #Se acomoda el widget para empresa_embarque_almacen_id_relacionado para que busque por entradas
    _dbTable.empresa_embarque_almacen_id_relacionado.widget = (
        lambda field, 
        value: stv_widget_db_chosen_linked(
            field,
            value,
            1,
            'empresa_embarque_id',
            URL(a='app_almacenes', c='610entradas', f='embarque_almacenes_entradas_ver.json'),
            ) 
        )


    #Lista que tendrá todos los embarques a los que puede acceder este almacén
    _L_embarques_id = []
    if(_s_action == request.stv_fwk_permissions.btn_new.code):
        #Si _s_action es igual al botón de nuevo entonces verfica si existen embarques abiertos/en_ruta almacenes y que su primer
        #almacén con estatus != de cerrado asociado al embarque sea el almacén actual.
        
        #Se consiguen los embarques que están en_ruta y tengan asociada una salida con este almacén como destino. 
        _dbRows_salidasembarques = dbc01(
            (dbc01.tempresa_embarques.empresa_id == _s_empresa_id )
            &(dbc01.tempresa_embarques.estatus.belongs(
                TEMPRESA_EMBARQUES.ESTATUS.ABIERTO,
                TEMPRESA_EMBARQUES.ESTATUS.RUTA
                )
              )
            &(dbc01.tempresa_embarque_almacenes.empresa_embarque_id == dbc01.tempresa_embarques.id )
            &(dbc01.tempresa_embarque_almacenes.estatus.belongs(
                TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.EN_ESPERA,
                TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CARGANDO,
                TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.DESCARGANDO,
                )
              )
            ).select(
                dbc01.tempresa_embarques.id,
                dbc01.tempresa_embarque_almacenes.ALL,
                orderby = [
                    dbc01.tempresa_embarques.id, 
                    dbc01.tempresa_embarque_almacenes.orden
                    ]
                )

        if (_dbRows_salidasembarques):
            
            _s_embarque_id = None
            
            for _dbRow_salidaembarque in _dbRows_salidasembarques:
                _dbRows_embarque_almacenes = dbc01(
                    (dbc01.tempresa_embarque_almacenes.empresa_embarque_id == _dbRow_salidaembarque.tempresa_embarques.id)
                    ).select(
                        dbc01.tempresa_embarque_almacenes.ALL,
                        orderby = [
                            (
                                dbc01.tempresa_embarque_almacenes.orden
                            )
                            ]
                        ).first()
                        
                # Vamos a navegar por los resultados para detectar el proximo almacen abierto en el embarque.
                if not(_s_embarque_id) or (_s_embarque_id != _dbRow_salidaembarque.tempresa_embarques.id):
                    # Si es el primer registro del embarque...
                    _s_embarque_id = _dbRow_salidaembarque.tempresa_embarques.id
                    if _s_empresa_plaza_sucursal_almacen_id == str(_dbRow_salidaembarque.tempresa_embarque_almacenes.empresa_plaza_sucursal_almacen_id):
                        # ...si también corresponde al almacén actual, quiere decir que el almcén actual es donde se encuentra el embarque.
                        if(
                            (_dbRows_embarque_almacenes.empresa_plaza_sucursal_almacen_id == _dbRow_salidaembarque.tempresa_embarque_almacenes.empresa_plaza_sucursal_almacen_id)
                            and
                            (_dbRows_embarque_almacenes.orden == _dbRow_salidaembarque.tempresa_embarque_almacenes.orden)
                            ):
                            #Si el primer almacén del embarque coincide con este almacén y es el mismo orden entonces no se dejará generar entrada
                            pass
                        else:
                            _L_embarques_id += [_s_embarque_id]
                            
                    else:
                        pass
                else:
                    pass
            
            _dbTable.empresa_embarque_id.requires = IS_IN_DB(
                dbc01(
                    (dbc01.tempresa_embarques.id.belongs(_L_embarques_id))
                    ),
                'tempresa_embarques.id',
                dbc01.tempresa_embarques._format
                )
        else:
            pass
        
    else:
        #el _s_action es diferente de nuevo, sigue su función normal.
        pass 
        
    _L_dbFields = [
        _dbTable.id,
        _dbTable.ordencompra,
        _dbTable.empresa_embarque_id,
        _dbTable.fecha_registro,
        _dbTable.fecha_movimiento,
        ]
    
    if _n_concepto_id:
        # Si concepto esta definido, permitir hacer todo
        _L_formButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_new,
            request.stv_fwk_permissions.btn_save,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_edit,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_remove,
            request.stv_fwk_permissions.btn_find,
            request.stv_fwk_permissions.btn_findall,
            ]
    else:
        # Si concepto no esta definido, no permitir hacer nada
        _L_formButtons = [
            request.stv_fwk_permissions.btn_none,
            ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = _L_formButtons, 
        s_tabId = STV_CFG_APP_ALMACENES.ID,
        L_fieldsToShow = _L_dbFields,
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
    )

    _O_cat.addRowContentEvent('onValidation', TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.ENTRADA.PORTRASPASO)         

    _O_cat.addRowContentEvent('onAccepted', TEMPRESA_INVENTARIOMOVIMIENTOS.ACEPTADO.ENTRADA.PORTRASPASO)         

    #Se crea el botón especial 1 para poder cerrar el embarque_almacén.
    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionLabel = "Cerrar embarque",
        s_permissionIcon = "fa fa-compass",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
        confirm = Storage(
            s_title = "Favor de confirmar operación", 
            s_text = "¿Está seguro de querer cerrar este embarque?", 
            s_type = "warning", 
            s_btnconfirm = "Aceptar", 
            s_btncancel = "Cancelar", 
            )   
            ),
        L_options = []
        )

    #Se utiliza el _b_skipAction a este nivel para hacer un bypass a Eliminar si llega a dar error.
    _b_skipAction = False
    #Se utiliza el overrideView a este nivel porque como se va a eliminar el registro tiene que hacerse antes del _O_cat.process()
    _D_overrideView = None
    
    if(_s_action == request.stv_fwk_permissions.btn_remove.code):
        
        _dbRow_movimiento_entrada_traspaso = dbc01.tempresa_inventariomovimientos(_s_empresa_inventariomovimiento_id)
        
        #Sacará los movimientos que tengan para este almacén y que sean diferentes al ID actual. 
        _dbRows_movimientos = dbc01(
            (dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id == _dbRow_movimiento_entrada_traspaso.empresa_embarque_almacen_id)
            &(dbc01.tempresa_inventariomovimientos.id != _dbRow_movimiento_entrada_traspaso.id)
            ).select(
                dbc01.tempresa_inventariomovimientos.id
                )
        
        #Si no existe ningún movimiento con este embarque_almacén en todo entonces se procede a cambiar el estatus del almacén a "en espera"
        if not _dbRows_movimientos:
            
            _dbRow_empresa_embarque_almacen = dbc01.tempresa_embarque_almacenes(_dbRow_movimiento_entrada_traspaso.empresa_embarque_almacen_id)
                
            if(_dbRow_empresa_embarque_almacen.estatus != TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CERRADO):
            
                dbc01(
                    (dbc01.tempresa_embarque_almacenes.id == _dbRow_movimiento_entrada_traspaso.empresa_embarque_almacen_id)
                    ).update(
                        estatus = TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.EN_ESPERA
                        )
                
                #Su almacén relacionado se va a "Ruta".
                dbc01(
                    (dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id == _dbRow_movimiento_entrada_traspaso.empresa_embarque_almacen_id_relacionado)
                    &(dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id_relacionado == _dbRow_movimiento_entrada_traspaso.empresa_embarque_almacen_id)
                    ).update(
                        estatus_movimiento = TEMPRESA_INVENTARIOMOVIMIENTOS.ESTATUS_MOVIMIENTO.RUTA
                        )
                    
                #Si no hubo más movimientos a parte del que se quiere eliminar se elimina pero también el almacen_embarque cambia su estatus a "en_espera"
                stv_flashSuccess("Movimiento traspaso", "Se eliminó correctamente el movimiento")
                 
                _D_overrideView = request.stv_fwk_permissions.view_refresh
                
            else:
                #Si el embarque tiene un estatus == a cerrado entonces no podrá cambiarse su estatus a "En_espera"
                stv_flashError("Movimiento traspaso", "El movimiento no se pudo eliminar ya que el almacén al cual pertenece ya cuenta con estatus 'Cerrado'") 
                
                #Se usa el _b_skipAction para hacerle bypass al eliminar
                _b_skipAction = True                
                
                _D_overrideView = request.stv_fwk_permissions.view_refresh

        else:
            #Si sí hay se despliega un mensaje diciendo que se elimina este movimiento pero el embarque no cambia el estatus
            
            #Su almacén relacionado se va a "Ruta".
            dbc01(
                (dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id == _dbRow_movimiento_entrada_traspaso.empresa_embarque_almacen_id_relacionado)
                &(dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id_relacionado == _dbRow_movimiento_entrada_traspaso.empresa_embarque_almacen_id)
                ).update(
                    estatus_movimiento = TEMPRESA_INVENTARIOMOVIMIENTOS.ESTATUS_MOVIMIENTO.RUTA
                    )
                
            stv_flashSuccess("Movimiento traspaso", "Se eliminó correctamente movimiento") 
            
            _D_overrideView = request.stv_fwk_permissions.view_refresh
            
    else:
        pass

    if(_D_overrideView):
        _D_returnVars = _O_cat.process(D_overrideView = _D_overrideView, b_skipAction = _b_skipAction)
    else:
        _D_returnVars = _O_cat.process()
        
    if(_s_action == request.stv_fwk_permissions.btn_new.code):

        if not _L_embarques_id:
            #Si no regresó ningún embarque mandará un error y regresará la vista.
            stv_flashError("Embarques.", "No se encontraron embarques con salidas para este almacén.")
            #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
            from gluon.storage import List
            request.args = List([request.stv_fwk_permissions.btn_none.code,])
            _D_returnVars = movto_entrada_portraspaso()
        else:
            #No pasa nada encontró embarques a mostrar
            pass
    else:
        #el _s_action es diferente de nuevo, sigue su función normal.
        pass 


    if (_s_action == request.stv_fwk_permissions.btn_save.code) and (_D_returnVars.rowContent_form.accepted):

        ###TODO: Hacer el cambio de estatus solo cuando venga el guardado desde "nuevo". 
       
        #Se hace una consulta al registro que se acaba de guardar
        _dbRow_inventariomovimiento = dbc01.tempresa_inventariomovimientos(_D_returnVars.rowContent_form.vars.id)
        
        #Si se grabó y se aceptó se cambia el estatus del embarque_almacen a "cargando"
        dbc01(
            (dbc01.tempresa_embarque_almacenes.id == _dbRow_inventariomovimiento.empresa_embarque_almacen_id)
            ).update(
                estatus = TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.DESCARGANDO
                )
            
            
        #Su almacén relacionado se va a "Entregado".
        dbc01(
            (dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id == _dbRow_inventariomovimiento.empresa_embarque_almacen_id_relacionado)
            &(dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id_relacionado == _dbRow_inventariomovimiento.empresa_embarque_almacen_id)
            ).update(
                estatus_movimiento = TEMPRESA_INVENTARIOMOVIMIENTOS.ESTATUS_MOVIMIENTO.ENTREGADO
                )
        
        #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
        from gluon.storage import List
        request.args = List([request.stv_fwk_permissions.btn_view.code, _D_returnVars.rowContent_form.vars.id])
        _D_returnVars = movto_entrada_portraspaso()
        
    else:
        _D_returnVars.update(
            dict(
                msg = 'Bienvenidos',
                dbFieldAlmacenes = STV_LIB_APP_ALMACENES.GET_DBFIELD_ALMACENES(
                    n_default = _s_empresa_plaza_sucursal_almacen_id or 0,
                    b_writable = False
                    ),
                L_breadcrub = [
                    STV_ALMACENES.BREADCRUB.BC_INDEX,
                    STV_ALMACENES.BREADCRUB.MOVTO.ENTRADA.BC_PORTRASPASO
                    ]
                )
            )
        
    if(_s_action in (request.stv_fwk_permissions.special_permisson_01.code,)):
    #Si se le dio clic a "cerrar almacen embarque" hará la lógica para pasar el estatus de cargando a cerrado.
        _s_entrada_traspaso_id = request.args(1, None)
        
        _dbRow_entrada = _dbTable(_s_entrada_traspaso_id)
        
        #Query para verificar que se tengan movimientos a nivel detalle de este movimiento de almacén.
        _dbRow_entrada_movtos = dbc01(
            (dbc01.tempresa_invmovto_productos.empresa_inventariomovimiento_id == _s_entrada_traspaso_id)
            ).select(
                dbc01.tempresa_invmovto_productos.id
                )
        
        if(_dbRow_entrada_movtos):
            #Si tiene movimientos entonces puede hacer la lógica para cambiar el estatus.
            
            _dbRow_embarque_almacenes = dbc01(
                (dbc01.tempresa_inventariomovimientos.id == _s_entrada_traspaso_id)
                &(dbc01.tempresa_embarque_almacenes.empresa_embarque_id == dbc01.tempresa_inventariomovimientos.empresa_embarque_id)
                &(dbc01.tempresa_embarque_almacenes.empresa_plaza_sucursal_almacen_id == _s_empresa_plaza_sucursal_almacen_id)
                ).select(
                    dbc01.tempresa_embarque_almacenes.ALL
                    ).last()
            
            if(_dbRow_embarque_almacenes):
                if(_dbRow_embarque_almacenes.estatus in (TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CARGANDO,TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.DESCARGANDO, TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.EN_ESPERA)):
                    
                    _dbRow_embarques = dbc01.tempresa_embarques(_dbRow_embarque_almacenes.empresa_embarque_id)
                    
                    #Si el embarque aún está abierto se pasará a en ruta.
                    if(_dbRow_embarques.estatus == TEMPRESA_EMBARQUES.ESTATUS.ABIERTO):
                        dbc01(
                            (dbc01.tempresa_embarques.id == _dbRow_embarques.id)
                            ).update(
                                estatus = TEMPRESA_EMBARQUES.ESTATUS.RUTA
                                )
                    else:
                        pass
                    
                    _dbRows_movimientos_salidas = dbc01(
                        (dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id_relacionado == _dbRow_entrada.empresa_embarque_almacen_id)
                        &(dbc01.tempresa_inventariomovimientos.empresa_almacenesconcepto_id == _n_concepto_salida_traspaso_id)
                        &(dbc01.tempresa_inventariomovimientos.estatus_movimiento == TEMPRESA_INVENTARIOMOVIMIENTOS.ESTATUS_MOVIMIENTO.RUTA)
                        ).select(
                            dbc01.tempresa_inventariomovimientos.id,
                            orderby = [dbc01.tempresa_inventariomovimientos.id]
                            )
                        
                    if not _dbRows_movimientos_salidas:
                        #Si no hay movimientos de salidas para este embarque_almacen_id que estén en ruta entonces procede a hacerse toda la lógica para cerrar
                        
                        #Se obtine el último almacén por orden de los almacenes por embarque
                        _dbRows_embarques_almacenes = dbc01(
                            (dbc01.tempresa_embarque_almacenes.empresa_embarque_id == _dbRow_embarque_almacenes.empresa_embarque_id)
                            &(dbc01.tempresa_embarque_almacenes.estatus != TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CERRADO)
                            ).select(
                                dbc01.tempresa_embarque_almacenes.ALL,
                                orderby = [
                                    dbc01.tempresa_embarque_almacenes.orden
                                    ]
                                )
                        
                        if len(_dbRows_embarques_almacenes) == 1:
                            #Si solo queda un solo almacén pendiente entonces entrará aquí
                            _dbRow = _dbRows_embarques_almacenes.last()
                            if(_dbRow.empresa_plaza_sucursal_almacen_id == int(_s_empresa_plaza_sucursal_almacen_id)):
                                #Si el último almacén pendiente es este almacén entonces cambiará el estatus del embarque a Cerrado
                                dbc01(
                                    (dbc01.tempresa_embarques.id == _dbRow_embarques.id)
                                    ).update(
                                        estatus = TEMPRESA_EMBARQUES.ESTATUS.CERRADO
                                        )
                                stv_flashSuccess("Estatus embarque", "Este fue el ultimo almacén pendiente para este embarque. Se cerró embarque correctamente.") 
                            else:
                                pass
                        else:
                            #Si la longitud de los almacenes pendientes no es == 1 entonces no cerrará el embarque
                            pass
                        
                        #Si el embarque_almacen tiene un estatus de "Cargando" se pasa a cerrado. 
                        dbc01(
                            (dbc01.tempresa_embarque_almacenes.id == _dbRow_embarque_almacenes.id)
                            ).update(
                                estatus = TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CERRADO
                                )
                          
                        stv_flashSuccess("Estatus embarque almacen", "Se cerró el traspaso de almacén") 
                        #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
                        from gluon.storage import List
                        request.args = List([request.stv_fwk_permissions.btn_findall.code,])
                        _D_returnVars = movto_entrada_portraspaso()
                    else:
                        stv_flashError("Estatus embarque almacen", "Este almacén no puede cerrarse porque tiene uno o más movimientos pendientes de entregar.")
                        #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
                        from gluon.storage import List
                        request.args = List([request.stv_fwk_permissions.btn_findall.code,])
                        _D_returnVars = movto_entrada_portraspaso()
                        pass
                else:
                    stv_flashError("Estatus embarque almacen", "Este almacén no está en estatus de 'Cargando' en almacenes o ya fue cerrado.")
                    #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
                    from gluon.storage import List
                    request.args = List([request.stv_fwk_permissions.btn_findall.code,])
                    _D_returnVars = movto_entrada_portraspaso()
                    
            else:
                pass
        else:
            #En caso de no tener movimientos se mostrará el error.
            stv_flashError("Estatus embarque almacen", "El detalle de este movimiento no cuenta con productos, no se puede cambiar el estatus.")
            #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
            from gluon.storage import List
            request.args = List([request.stv_fwk_permissions.btn_findall.code,])
            _D_returnVars = movto_entrada_portraspaso()
            
    else:
        pass        

    if _n_concepto_id:
        # Si concepto esta definido, permitir el detalle
        _O_cat.addSubTab(
            dbTableDetail = dbc01.tempresa_invmovto_productos, 
            s_url = URL(f='movto_entrada_portraspaso_resumen', args=['master_'+str(request.function),request.args(1)]),
            s_idHtml = fn_createIDTabUnique(f = 'movto_entrada_portraspaso_resumen', s_masterIdentificator = str(request.function))  
            )
    else:
        # de lo contrario, no mostrar el detalle
        pass
    
    return _D_returnVars

def movto_entrada_portraspaso_resumen():
    '''Función para los resumenes de entrada por traspaso 
    
        Args:
            arg0: master_[nombre función maestro]
            arg1: empresa_inventariomovimiento_id
            [arg2]: codigo de funcionalidad
            [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    '''
    
    _dbTable = dbc01.tempresa_invmovto_productos
    
    _s_empresa_inventariomovimiento_id = request.args(1, None)
    
    _s_action = request.args(2, request.stv_fwk_permissions.btn_none.code)

    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.empresa_producto_id,
        _dbTable.cantidad_entrada,
        _dbTable.observaciones,
        _dbTable.tipo_inconsistencia
        ]

    _L_formButtons = [
        request.stv_fwk_permissions.btn_none,
        request.stv_fwk_permissions.btn_save,
        request.stv_fwk_permissions.btn_cancel,
        request.stv_fwk_permissions.btn_edit,
        request.stv_fwk_permissions.btn_view,
        request.stv_fwk_permissions.btn_remove,
        request.stv_fwk_permissions.btn_find,
        request.stv_fwk_permissions.btn_findall,
        ]    


    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(),
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_inventariomovimientos,
        xFieldLinkMaster = [
            _dbTable.empresa_inventariomovimiento_id
            ],
        L_visibleButtons = _L_formButtons, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
        )
   
    _D_returnVars = _O_cat.process()

    if ((_s_action == request.stv_fwk_permissions.btn_save.code) and (_D_returnVars.rowContent_form.accepted)):
        
        _dbRow_maestro = dbc01.tempresa_inventariomovimientos(_s_empresa_inventariomovimiento_id )
        
        _D_result = TEMPRESA_INVMOVTO_PRODUCTOS.VALIDAR_ROW_ENTRADA_PORTRASPASO(
            _D_returnVars.rowContent_form.vars.id,
            _dbRow_maestro.empresa_id, 
            _dbRow_maestro)
        
        if _D_result.E_return != CLASS_e_RETURN.OK:
            stv_flashWarning("Inconsistencias en producto", _D_result.s_msgError)  
        else:
            pass

    else:
        #Si es cualquier opción en vez de salvar no hace nada
        pass

    return _D_returnVars

def otra():
    ''' Otro tipo de entradas

    @descripcion Entrada por otra

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature

    '''

    _dbTable = dbc01.tempresa_inventariomovimientos
    _s_empresa_id = request.args(1, None)
    _s_action = request.args(2, None)
    _s_movimiento_id = request.args(3, None)

    _dbQry = (
        dbc01.tempresa_inventariomovimientos.empresa_id == _s_empresa_id
        )

    TEMPRESA_INVENTARIOMOVIMIENTOS.ENTRADA.CONFIGURA_CAMPOS(
        s_empresa_id = _s_empresa_id,
        n_clasificacion = TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.OTRAS_ENTRADAS
        )

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_dbQry),
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas,
        xFieldLinkMaster = [_dbTable.empresa_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, _dbTable.empresa_proveedor_id, _dbTable.empresa_cliente_id,
            _dbTable.tipodocumento, _dbTable.remision, _dbTable.total,
            _dbTable.tc, _dbTable.ordencompra, _dbTable.empresa_plaza_sucursal_almacen_id,
            _dbTable.fecha_movimiento, _dbTable.fecha_registro, _dbTable.folio,
            _dbTable.descripcion, _dbTable.estatus_movimiento
            ],
        L_fieldsToSearch = [
            _dbTable.remision, _dbTable.tc, _dbTable.ordencompra,
            _dbTable.fecha_movimiento, _dbTable.fecha_registro, _dbTable.folio,
            _dbTable.descripcion
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id, _dbTable.remision, _dbTable.tc, _dbTable.ordencompra,
            _dbTable.folio
            ],
        )

    _D_returnVars = _O_cat.process()

    return (_D_returnVars)

def otra2():
    
    _s_empresa_plaza_sucursal_almacen_id = request.vars.master_empresa_plaza_sucursal_almacen_id
    
    _s_cuenta_aplicacion_id = D_stvFwkCfg.cfg_cuenta_aplicacion_id

    _s_empresa_inventariomovimiento_id = request.args(1, None)
    
    _D_result_empresa_id = TCUENTA_APLICACIONES.GET_EMPRESA_ID_CACHE(
        _s_cuenta_aplicacion_id
        )
        
    if _D_result_empresa_id.E_return != CLASS_e_RETURN.OK:
        #Si entra acá es que ocurrió un error durante la obtención del concepto_id
        _s_empresa_id = None
        stv_flashError("Empresa ID no encontrado.", _D_result_empresa_id.s_msgError)    
    elif _D_result_empresa_id.n_empresa_id:
        _s_empresa_id = _D_result_empresa_id.n_empresa_id
    else:
        #hubo bronca, no trajo error pero tampoco trajo la empresa_id
        _s_empresa_id = None
        stv_flashError("Empresa ID no encontrado.", "Empresa no definida para almacenes.")     
        
        
    _D_result_concepto_id = TEMPRESA_ALMACENESCONCEPTOS.GET_ID_CACHE(
        n_empresa_id = _s_empresa_id, 
        n_tipoconcepto = TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.ENTRADA, 
        n_clasificacion = TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.OTRAS_ENTRADAS
        )
    
    # Por default el query se pone en false para no regresar nada en las búsquedas
    _qry = False
    
    if _D_result_concepto_id.E_return != CLASS_e_RETURN.OK:
        #Si entra acá es que ocurrió un error durante la obtención del concepto_id
        _n_concepto_id = None
        stv_flashError("Otras entradas", _D_result_concepto_id.s_msgError)        
    elif _D_result_concepto_id.n_concepto_id:
        _n_concepto_id = _D_result_concepto_id.n_concepto_id
        _dbTable.empresa_almacenesconcepto_id.default = _n_concepto_id
        _dbTable.empresa_almacenesconcepto_id.writable = False

        _qry = (
            (_dbTable.empresa_id == _s_empresa_id)
            &(_dbTable.empresa_almacenesconcepto_id == _n_concepto_id)
            &(_dbTable.empresa_plaza_sucursal_almacen_id == _s_empresa_plaza_sucursal_almacen_id)
            )
        
        ###TODO: Mostrar los movimientos de salidas por otros. 
#         if _s_action in (request.stv_fwk_permissions.btn_new.code, request.stv_fwk_permissions.btn_none.code):
#             #Se agrega este query.
# #             _qry &= (None)
#             pass
#         else:
#             #Si es buscar todo el Query no aplica, ya que se puede filtrar todas las salidas
#             pass
    else:
        #hubo bronca, no trajo error pero tampoco trajo concepto_id
        _n_concepto_id = None
        stv_flashError("Otras entradas", "Concepto no definido en Almacenes-Conceptos")  
            
            
    _dbTable.empresa_id.default = _s_empresa_id
    _dbTable.empresa_plaza_sucursal_almacen_id.default = _s_empresa_plaza_sucursal_almacen_id

    #Se asigna la fecha de registro con el día y hora actual. 
    _dbTable.fecha_registro.writable = False
    _dbTable.fecha_registro.default = request.browsernow  
    
    _L_dbFields = [
        _dbTable.id,
        _dbTable.fecha_movimiento,
        _dbTable.remision,
        _dbTable.descripcion,
        _dbTable.observaciones, 
        ]
    
    _L_formButtons = [
        request.stv_fwk_permissions.btn_none,
        request.stv_fwk_permissions.btn_new,
        request.stv_fwk_permissions.btn_save,
        request.stv_fwk_permissions.btn_cancel,
        request.stv_fwk_permissions.btn_edit,
        request.stv_fwk_permissions.btn_view,
        request.stv_fwk_permissions.btn_find,
        request.stv_fwk_permissions.btn_findall,
        ]
        
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = _L_formButtons, 
        s_tabId = STV_CFG_APP_ALMACENES.ID,
        L_fieldsToShow = _L_dbFields, 
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
    )
    
    _O_cat.addRowContentEvent('onValidation', TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.ENTRADA.OTRA)
    
    _D_returnVars = _O_cat.process()

    if (_s_action == request.stv_fwk_permissions.btn_save.code) and (_D_returnVars.rowContent_form.accepted):
        #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
        from gluon.storage import List
        request.args = List([request.stv_fwk_permissions.btn_view.code, _D_returnVars.rowContent_form.vars.id])
        _D_returnVars = movto_entrada_otra()
         
    else:
        _D_returnVars.update(
            dict(
                msg = 'Bienvenidos',
                dbFieldAlmacenes = STV_LIB_APP_ALMACENES.GET_DBFIELD_ALMACENES(
                    n_default = _s_empresa_plaza_sucursal_almacen_id or 0,
                    b_writable = False
                    ),
                L_breadcrub = [
                    STV_ALMACENES.BREADCRUB.BC_INDEX,
                    STV_ALMACENES.BREADCRUB.MOVTO.ENTRADA.BC_OTRA
                    ]
                )
            )
        
    if _n_concepto_id:
        # Si concepto esta definido, permitir el detalle

        _O_cat.addSubTab(
            dbTableDetail = dbc01.tempresa_invmovto_productos, 
            s_url = URL(f='movto_entrada_otra_resumen', args=['master_'+str(request.function),request.args(1)]),
            s_idHtml = fn_createIDTabUnique(f = 'movto_entrada_otra_resumen', s_masterIdentificator = str(request.function))  
            )
    else:
        # de lo contrario, no mostrar el detalle
        pass
        
    return _D_returnVars

def _movto_entrada_otra_validation(O_form):
    '''Validación para los movimientos por otros tipo de entrada
    '''
    
    if not O_form.vars.remision:
        O_form.errors.remision = "La remisión es requerida."
    else:
        pass
    
    if not O_form.vars.descripcion:
        O_form.errors.descripcion = "La descripción es requerida."
    else:
        pass
    
    return O_form

def movto_entrada_otra_resumen():
    '''Función para los resumenes de movimientos por otros tipo de entradas
    
        Args:
            arg0: master_[nombre función maestro]
            arg1: empresa_inventariomovimiento_id
            [arg2]: codigo de funcionalidad
            [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    '''
    
    _dbTable = dbc01.tempresa_invmovto_productos

    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.empresa_producto_id,
        _dbTable.cantidad_entrada,
        ]
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(),
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_inventariomovimientos,
        xFieldLinkMaster = [
            _dbTable.empresa_inventariomovimiento_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
        )

    _O_cat.addRowContentEvent('onValidation', _movto_entrada_otra_resumen_validation)
   
    _D_returnVars = _O_cat.process()

    return _D_returnVars

def _movto_entrada_otra_resumen_validation(O_form):
    '''Validación para los resumenes de movimientos por otros tipo de entradas
    '''

    _n_cantidad = int(O_form.vars.cantidad_entrada or 0)
    
    _s_empresa_inventariomovimiento_id = request.args(1, None)
        
    if (_n_cantidad <= 0):
        O_form.errors.cantidad_entrada = "La cantidad que entrada no puede ser menor o igual a cero"
    else:
        pass
    
    _dbRow = dbc01(
        (dbc01.tempresa_invmovto_productos.empresa_inventariomovimiento_id == _s_empresa_inventariomovimiento_id)
        &(dbc01.tempresa_invmovto_productos.empresa_producto_id == O_form.vars.empresa_producto_id)
        &(dbc01.tempresa_invmovto_productos.empresa_producto_id != O_form.vars.record_id)
        ).select(
            dbc01.tempresa_invmovto_productos.id
            )
    if(_dbRow):
        O_form.errors.empresa_producto_id = "El producto no puede repetirse."
    else:
        pass

    return O_form


def embarque_almacenes_entradas_ver():
    ''' Función para las salidas por traspaso de almacén
        Args:
            arg0: embarque id
    
    '''
    
    _dbTable = dbc01.tempresa_embarque_almacenes
    
    _s_almacen_id = request.vars.master_empresa_plaza_sucursal_almacen_id
    
    _LD_data = []
    
    if (len(request.args) > 0) :
        
        _s_embarque_id = request.args(0, None)
        
        _dbRows_embarque_almacenes_abiertos = dbc01(
            (dbc01.tempresa_embarque_almacenes.empresa_embarque_id == _s_embarque_id)
            & (
                dbc01.tempresa_embarque_almacenes.estatus.belongs(
                    TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.EN_ESPERA,
                    TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CARGANDO,
                    TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.DESCARGANDO,
                    )
                )
            ).select(
                dbc01.tempresa_embarque_almacenes.ALL,
                orderby = (
                    dbc01.tempresa_embarque_almacenes.orden
                    )
                )
            
        if len(_dbRows_embarque_almacenes_abiertos) == 0:
            pass
        else:
            _dbRow_embarque_almacenes_abierto_primero = _dbRows_embarque_almacenes_abiertos.first()
            if _dbRow_embarque_almacenes_abierto_primero.empresa_plaza_sucursal_almacen_id != int(_s_almacen_id):
                pass
            else:
                _s_empresa_embarque_almacen_actual = _dbRow_embarque_almacenes_abierto_primero.id
        
                _dbRow_embarque  = dbc01.tempresa_embarques(_s_embarque_id)
                
                _n_concepto_salida_id = TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.GET_CONCEPTO_ID(
                    n_empresa_id = _dbRow_embarque.empresa_id, 
                    n_tipoconcepto = TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.SALIDA,
                    n_clasificacion = TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.TRASPASO_SALIDA
                    )
                
                _n_concepto_entrada_id = TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.GET_CONCEPTO_ID(
                    n_empresa_id = _dbRow_embarque.empresa_id, 
                    n_tipoconcepto = TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.ENTRADA,
                    n_clasificacion = TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.TRASPASO_ENTRADA
                    )
                
                _rows_result = dbc01(
                    (dbc01.tempresa_inventariomovimientos.empresa_embarque_id == _s_embarque_id)
                    &(dbc01.tempresa_inventariomovimientos.empresa_almacenesconcepto_id == _n_concepto_salida_id)
                    &(dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id_relacionado == _s_empresa_embarque_almacen_actual)
                    &(_dbTable.id == dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id)
                    ).select(
                        _dbTable.ALL,
                        orderby = _dbTable.orden,
                        )
                    
        if(_rows_result):     
            for _row in _rows_result:
                
                #Consulta para saber si el almacen ya fue utilizado en alguna entrada.
                _dbRow = dbc01(
                    (dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id_relacionado == _row.id)
                    &(dbc01.tempresa_inventariomovimientos.empresa_almacenesconcepto_id == _n_concepto_entrada_id)
                    &(dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id == _s_empresa_embarque_almacen_actual)
                    ).select(
                        dbc01.tempresa_inventariomovimientos.id
                        )
                
                if not (_dbRow):
                    #Si el almacén no ha sido utilizado en nignuna entrada entonces es posible asociarse aquí.
                    _LD_data.append({ 
                        'id':_row.id, 'optionname':(_dbTable._format(_row))})
                else:
                    #Si sí fue utilizado en alguna entrada entonces no se agrega a la lista de almacenes disponibles.
                    pass
                                
            return (response.json({'data': _LD_data}))
        else:
            return (response.json({'data': [], 'error': 'Argumentos inválidos'})) 
            pass
        
    else:
        return (response.json({'data': [], 'error': 'Argumentos inválidos'}))  