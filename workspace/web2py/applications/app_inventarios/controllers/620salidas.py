# -*- coding: utf-8 -*-

def movto_salida_porventa():
    '''Función para las salidas por venta

        Args:
            [arg0]: codigo de funcionalidad
            [arg1]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    '''
    
    _s_action = request.args(0, None)
    
    _dbTable = dbc01.tempresa_inventariomovimientos
    
    _s_empresa_id = db.tcuenta_aplicaciones(D_stvFwkCfg.cfg_cuenta_aplicacion_id).empresa_id
    _dbTable.empresa_id.default = _s_empresa_id
    
    _dbRows_conceptos = dbc01(
        (dbc01.tempresa_almacenesconceptos.empresa_id == _s_empresa_id)
        & (dbc01.tempresa_almacenesconceptos.tipoconcepto == TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.SALIDA)
        & (dbc01.tempresa_almacenesconceptos.clasificacion == TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.VENTA)
        ).select(
            dbc01.tempresa_almacenesconceptos.id,
            orderby = ~dbc01.tempresa_almacenesconceptos.id
            )
        
    if _dbRows_conceptos:
        _dbTable.empresa_almacenesconcepto_id.default = _dbRows_conceptos.first().id
        _dbTable.empresa_almacenesconcepto_id.writable = False
    else:
        #TODO  Mostrar error de que no existe un concepto de entrada por compra para la empresa
        pass
    
    #El Query tiene que mostrar las entradas que estén pendientes por asociar un CFDI. 
    _qry = (
        (_dbTable.empresa_id == _s_empresa_id)
        &(_dbTable.empresa_almacenesconcepto_id == _dbRows_conceptos.first().id)
        &(_dbTable.cfdi_id == None)
        )
    
    _L_dbFields = [
        _dbTable.id,
        _dbTable.empresa_proveedor_id,
        _dbTable.tipodocumento,
        _dbTable.serie,
        _dbTable.folio,
        _dbTable.remision,
        ]
    
    _L_formButtons = [
        request.stv_fwk_permissions.btn_none,
        request.stv_fwk_permissions.btn_new,
        request.stv_fwk_permissions.btn_save,
        request.stv_fwk_permissions.btn_cancel,
        request.stv_fwk_permissions.btn_edit,
        request.stv_fwk_permissions.btn_view,
        #request.stv_fwk_permissions.btn_print,
        #request.stv_fwk_permissions.btn_invalidate,
        #request.stv_fwk_permissions.btn_validate,
        #request.stv_fwk_permissions.btn_suspend,
        #request.stv_fwk_permissions.btn_activate,
        #request.stv_fwk_permissions.btn_remove,
        #request.stv_fwk_permissions.btn_restore,
        request.stv_fwk_permissions.btn_find,
        request.stv_fwk_permissions.btn_findall,
        #request.stv_fwk_permissions.btn_signature,
        ]
        
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = _L_formButtons, 
        s_tabId = STV_CFG_APP_ALMACENES.ID,
        L_fieldsToShow = _L_dbFields, 
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
    )
    
    
    _O_cat.addRowContentEvent('onValidation', _movto_salida_porventa_validation)
    
    if _s_action in (request.stv_fwk_permissions.btn_new.code, request.stv_fwk_permissions.btn_edit.code, request.stv_fwk_permissions.btn_save.code, request.stv_fwk_permissions.btn_view.code):
        # Acciones permitidas
        pass
    else:
        pass
        #_s_action = request.stv_fwk_permissions.btn_new.code
    
    _D_returnVars = _O_cat.process(
        s_overrideCode = _s_action,  # Usar el action 
        b_skipAction = False,
        #L_formFactoryFields = _L_dbFields
        )

    if (_s_action == request.stv_fwk_permissions.btn_save.code) and (_D_returnVars.rowContent_form.accepted):
        #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
        from gluon.storage import List
        request.args = List([request.stv_fwk_permissions.btn_view.code, _D_returnVars.rowContent_form.vars.id])
        _D_returnVars = movto_entrada_porcompra()
         
    else:
        _D_returnVars.update(
            dict(
                msg = 'Bienvenidos',
                dbFieldAlmacenes = STV_LIB_APP_ALMACENES.GET_DBFIELD_ALMACENES(
                    n_default = request.vars.master_empresa_plaza_sucursal_almacen_id or 0,
                    b_writable = False
                    ),
                L_breadcrub = [
                    STV_ALMACENES.BREADCRUB.BC_INDEX,
                    STV_ALMACENES.BREADCRUB.MOVTO.SALIDA.BC_PORVENTA
                    ]
                )
            )
        
#        _D_returnVars.D_useView.L_actionsCreateView += [request.stv_fwk_permissions.btn_new.code, request.stv_fwk_permissions.btn_edit.code]
        _D_returnVars.D_useView.E_buttonsBarPosition = request.stv_fwk_permissions.E_POSITION_DOWN

    if(request.vars.tipodocumento):
        if(int(request.vars.tipodocumento) == TEMPRESA_INVENTARIOMOVIMIENTOS.E_TIPODOCUMENTO.REMISION):
            _O_cat.addSubTab(
                dbTableDetail = dbc01.tempresa_invmovto_productos, 
                s_url = URL(f='movto_entrada_porcompra_remision', args=['master_'+str(request.function),request.args(1)]),
                s_idHtml = fn_createIDTabUnique(f = 'movto_entrada_porcompra_remision', s_masterIdentificator = str(request.function))  
                )
        else:
            #Ver que subtab abrirá directamente si es por factura
            pass
    else:
        pass
        
    return _D_returnVars

def _movto_salida_porventa_validation(O_form):
    '''Validación para las entradas por devolución
    '''
    
    if not O_form.vars.remision:
        O_form.errors.remision = "Es requerido"
    else:
        pass
    
    return O_form

def movto_salida_pordevolucion_compra():
    '''Función para las salidas por devolución por compra
        Args:
            [arg0]: codigo de funcionalidad
            [arg1]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    '''
    
    _s_action = request.args(0, request.stv_fwk_permissions.btn_none.code)
        
    _dbTable = dbc01.tempresa_inventariomovimientos
    
    _s_empresa_plaza_sucursal_almacen_id = request.vars.master_empresa_plaza_sucursal_almacen_id
    
    _s_cuenta_aplicacion_id = D_stvFwkCfg.cfg_cuenta_aplicacion_id

    _s_empresa_inventariomovimiento_id = request.args(1, None)
    
    #_L_searchFilter se utiliza en la forma buscar para los CFDIs recibidos por proveedor.
    _L_searchFilter = [
        Storage(
            s_input_name = str(_dbTable.empresa_proveedor_id),
            s_reference_name = str(dbc01.tcfdis.emisorproveedor_id),
            b_mandatory = True
            )
        ]
    
    _dbTable.cfdi_id.widget = lambda f, v: stv_widget_db_search(
        f, 
        v, 
        s_display = '%(serie)s %(folio)s', 
        s_url = URL( a = 'app_almacenes', c = '600busquedas', f = 'buscar_cfdis_recibidos', ),
        D_additionalAttributes = {
            'reference':dbc01.tcfdis.id,
            'L_searchFilter':_L_searchFilter,
            }
        )
    
    _dbTable.cfdi_egreso_id.widget = lambda f, v: stv_widget_db_search(
        f, 
        v, 
        s_display = '%(serie)s %(folio)s', 
        s_url = URL( a = 'app_almacenes', c = '600busquedas', f = 'buscar_cfdis_egreso_recibidos',),
        D_additionalAttributes = {
            'reference':dbc01.tcfdis.id,
            'L_searchFilter':_L_searchFilter,
            }
        )
    
    _D_result_empresa_id = TCUENTA_APLICACIONES.GET_EMPRESA_ID_CACHE(
        _s_cuenta_aplicacion_id
        )
    
    if _D_result_empresa_id.E_return != CLASS_e_RETURN.OK:
        #Si entra acá es que ocurrió un error durante la obtención del concepto_id
        _s_empresa_id = None
        stv_flashError("Empresa ID no encontrado.", _D_result_empresa_id.s_msgError)    
    elif _D_result_empresa_id.n_empresa_id:
        _s_empresa_id = _D_result_empresa_id.n_empresa_id
    else:
        #hubo bronca, no trajo error pero tampoco trajo la empresa_id
        _s_empresa_id = None
        stv_flashError("Empresa ID no encontrado.", "Empresa no definida para almacenes.")     
        
        
    _D_result_concepto_id = TEMPRESA_ALMACENESCONCEPTOS.GET_ID_CACHE(
        n_empresa_id = _s_empresa_id, 
        n_tipoconcepto = TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.SALIDA, 
        n_clasificacion = TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.DEVOLUCION_COMPRA
        )
    
    # Por default el query se pone en false para no regresar nada en las búsquedas
    _qry = False
    
    if _D_result_concepto_id.E_return != CLASS_e_RETURN.OK:
        #Si entra acá es que ocurrió un error durante la obtención del concepto_id
        _n_concepto_id = None
        stv_flashError("Salida por devolución de compra", _D_result_concepto_id.s_msgError)        
    elif _D_result_concepto_id.n_concepto_id:
        _n_concepto_id = _D_result_concepto_id.n_concepto_id
        _dbTable.empresa_almacenesconcepto_id.default = _n_concepto_id
        _dbTable.empresa_almacenesconcepto_id.writable = False

        _qry = (
            (_dbTable.empresa_id == _s_empresa_id)
            &(_dbTable.empresa_almacenesconcepto_id == _n_concepto_id)
            &(_dbTable.empresa_plaza_sucursal_almacen_id == _s_empresa_plaza_sucursal_almacen_id)
            )
        
        ###TODO: ¿Mostrar los que tienen pendientes de asociar CFDI de egreso?
        #El Query tiene que mostrar las salidas que estén pendientes por asociar un CFDI de egreso. 
        if _s_action in (request.stv_fwk_permissions.btn_new.code, request.stv_fwk_permissions.btn_none.code):
            #Se agrega este query.
            _qry &= (_dbTable.cfdi_egreso_id == None)
        else:
            #Si es buscar todo el Query no aplica, ya que se puede filtrar todas las entradas
            pass
    else:
        #hubo bronca, no trajo error pero tampoco trajo concepto_id
        _n_concepto_id = None
        stv_flashError("Salida por devolución de compra", "Concepto no defnido en Almacenes-Conceptos")        

    _dbTable.empresa_id.default = _s_empresa_id
    _dbTable.empresa_plaza_sucursal_almacen_id.default = _s_empresa_plaza_sucursal_almacen_id

    #Se asigna la fecha de registro con el día y hora actual. 
    _dbTable.fecha_registro.writable = False
    _dbTable.fecha_registro.default = request.browsernow

    _dbTable.empresa_proveedor_id.writable = True
    
    #Se asigna el tipo de documento por defecto ya que aquí no se hace por factura.
    _dbTable.tipodocumento.writable = False
    _dbTable.tipodocumento.default = TEMPRESA_INVENTARIOMOVIMIENTOS.E_TIPODOCUMENTO.REMISION
        
    _dbTable.observaciones.widget = stv_widget_input
    _dbTable.observaciones.comment = 'En este espacio se podrá poner una breve descripción de porqué se realizó la devolución.'
    _dbTable.observaciones.length = 100
    
    _dbTable.remision.comment = 'Agregar remisión en caso de no contarse con el CFDI de ingreso o egreso para esta salida.'
    
    _dbTable.cfdi_id.label = 'CFDI de ingreso relacionado'
    
    _L_dbFields = [
        _dbTable.id,
        _dbTable.empresa_proveedor_id,
        _dbTable.remision,
        _dbTable.cfdi_egreso_id,
        _dbTable.cfdi_id,
        _dbTable.fecha_registro,
        _dbTable.fecha_movimiento,
        ]
    
    if _n_concepto_id:
        # Si concepto esta definido, permitir hacer todo
        _L_formButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_new,
            request.stv_fwk_permissions.btn_save,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_edit,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_remove,
            request.stv_fwk_permissions.btn_find,
            request.stv_fwk_permissions.btn_findall,
            ]
    else:
        # Si concepto no esta definido, no permitir hacer nada
        _L_formButtons = [
            request.stv_fwk_permissions.btn_none,
            ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = _L_formButtons, 
        s_tabId = STV_CFG_APP_ALMACENES.ID,
        L_fieldsToShow = _L_dbFields,
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
    )

    _O_cat.addRowContentEvent('onValidation', TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.SALIDA.PORDEVOLUCION_COMPRA)
        
    _O_cat.addRowContentEvent('onAccepted', TEMPRESA_INVENTARIOMOVIMIENTOS.ACEPTADO.SALIDA.PORDEVOLUCION_COMPRA)

    _D_returnVars = _O_cat.process()

    if (_s_action == request.stv_fwk_permissions.btn_save.code) and (_D_returnVars.rowContent_form.accepted):
        
        #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
        from gluon.storage import List
        request.args = List([request.stv_fwk_permissions.btn_view.code, _D_returnVars.rowContent_form.vars.id])
        _D_returnVars = movto_salida_pordevolucion_compra()
        
    else:
        _D_returnVars.update(
            dict(
                msg = 'Bienvenidos',
                dbFieldAlmacenes = STV_LIB_APP_ALMACENES.GET_DBFIELD_ALMACENES(
                    n_default = _s_empresa_plaza_sucursal_almacen_id or 0,
                    b_writable = False
                    ),
                L_breadcrub = [
                    STV_ALMACENES.BREADCRUB.BC_INDEX,
                    STV_ALMACENES.BREADCRUB.MOVTO.SALIDA.BC_PORDEVOLUCION
                    ]
                )
            )
        
    if _n_concepto_id:
        # Si concepto esta definido, permitir el detalle

        _O_cat.addSubTab(
            dbTableDetail = dbc01.tempresa_invmovto_productos, 
            s_url = URL(f='movto_salida_pordevolucion_compra_resumen', args=['master_'+str(request.function),request.args(1)]),
            s_idHtml = fn_createIDTabUnique(f = 'movto_salida_pordevolucion_compra_resumen', s_masterIdentificator = str(request.function))  
            )
    else:
        # de lo contrario, no mostrar el detalle
        pass
    
    if(_s_empresa_inventariomovimiento_id):
        
        _dbRow_movto = _dbTable(_s_empresa_inventariomovimiento_id)

        if(_dbRow_movto and _dbRow_movto.cfdi_egreso_id):
            _O_cat.addSubTab(
                s_tabName = "CFDI egreso relacionado", 
                s_url = URL(a = 'app_timbrado', c= '250cfdi', f='cfdi_entradasalida_relacionado', args=['master_'+str(request.function), _dbRow_movto.cfdi_egreso_id]),
                s_idHtml = fn_createIDTabUnique(a = 'app_timbrado', c= '250cfdi', f = 'cfdi_entradasalida_relacionado', s_masterIdentificator = str(request.function))  
                )
            
        elif(_dbRow_movto and _dbRow_movto.cfdi_id):
            _O_cat.addSubTab(
                s_tabName = "CFDI relacionado", 
                s_url = URL(a = 'app_timbrado', c= '250cfdi', f='cfdi_entradasalida_relacionado', args=['master_'+str(request.function), _dbRow_movto.cfdi_id]),
                s_idHtml = fn_createIDTabUnique(a = 'app_timbrado', c= '250cfdi', f = 'cfdi_entradasalida_relacionado', s_masterIdentificator = str(request.function))  
                )    
        else:
            pass
    else:
        pass
    return _D_returnVars

def movto_salida_pordevolucion_compra_resumen():
    '''Función para los resumenes de salida por devolución de compra 
    
        Args:
            arg0: master_[nombre función maestro]
            arg1: empresa_inventariomovimiento_id
            [arg2]: codigo de funcionalidad
            [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    '''
    
    _dbTable = dbc01.tempresa_invmovto_productos
    
    _s_empresa_inventariomovimiento_id = request.args(1, None)

    _s_action = request.args(2, request.stv_fwk_permissions.btn_none.code)

    _s_empresa_inventariomovimientoresumen_id = request.args(3, None)

    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.empresa_producto_id,
        _dbTable.descripcion,
        _dbTable.producto_externo,
        _dbTable.cantidad_salida,
        _dbTable.preciounitario,
        _dbTable.costototal,
        _dbTable.tipo_inconsistencia,        
        ]
    
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(),
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_inventariomovimientos,
        xFieldLinkMaster = [
            _dbTable.empresa_inventariomovimiento_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
        )
    _dbRow_cfdi_concepto = None
    
    if (_s_empresa_inventariomovimientoresumen_id):
        # Si se encuentra viendo o editando o grabando y se identifica el resumen en el CFDI, se crea un registro para mostrar la inconsistencia en la vista.
        # Así mismo si coincide el producto_externo con el noidentificación se desabilita la edicion del produto_externo
        
        #Se verifica primero el de egreso porque es el que tiene mayor prioridad en este movimiento.
        _dbRow_movimientoMaestro = dbc01.tempresa_inventariomovimientos(_s_empresa_inventariomovimiento_id)
        
        if(_dbRow_movimientoMaestro and _dbRow_movimientoMaestro.cfdi_egreso_id):
            # Si existe el cfdi de egreso, se realiza la comparación para determinar si noidentificacion del cfdi_egreso coinciden.
            
            _dbRow_movimientoResumen = _dbTable(_s_empresa_inventariomovimientoresumen_id)

            _dbRows_cfdi_conceptos = dbc01(
                (dbc01.tcfdi_conceptos.cfdi_id == _dbRow_movimientoMaestro.cfdi_egreso_id)
                &(dbc01.tcfdi_conceptos.noidentificacion == _dbRow_movimientoResumen.producto_externo)
                ).select(
                    dbc01.tcfdi_conceptos.id,
                    dbc01.tcfdi_conceptos.empresa_prodserv_id,
                    dbc01.tcfdi_conceptos.noidentificacion,
                    dbc01.tcfdi_conceptos.cantidad,
                    dbc01.tcfdi_conceptos.valorunitario,
                    dbc01.tcfdi_conceptos.descripcion,
                    dbc01.tcfdi_conceptos.claveunidad,
                    dbc01.tcfdi_conceptos.importe,
                    dbc01.tempresa_invmovto_productos.cantidad_salida.coalesce_zero().sum().with_alias('registrados'),
                    left = [
                        dbc01.tempresa_inventariomovimientos.on(
                            (dbc01.tempresa_inventariomovimientos.cfdi_egreso_id == dbc01.tcfdi_conceptos.cfdi_id)
                            ),
                        dbc01.tempresa_invmovto_productos.on(
                            (dbc01.tempresa_invmovto_productos.empresa_inventariomovimiento_id == dbc01.tempresa_inventariomovimientos.id)
                            & (dbc01.tempresa_invmovto_productos.empresa_producto_id == dbc01.tcfdi_conceptos.empresa_prodserv_id)
                            & (dbc01.tempresa_invmovto_productos.id != _s_empresa_inventariomovimientoresumen_id)
                            ),
                        ],
                    groupby = [
                        dbc01.tcfdi_conceptos.id,
                        dbc01.tcfdi_conceptos.empresa_prodserv_id,
                        dbc01.tcfdi_conceptos.noidentificacion,
                        dbc01.tcfdi_conceptos.cantidad,
                        dbc01.tcfdi_conceptos.valorunitario,
                        dbc01.tcfdi_conceptos.descripcion,
                        dbc01.tcfdi_conceptos.claveunidad,
                        dbc01.tcfdi_conceptos.importe,
                        ]
                    
                    )
            
            #Si no trajo concepto es porque no existe registro con el producto externo que se escribió en el formulario, se revisa si por producto aparece el concepto.    
            if not (_dbRows_cfdi_conceptos) and (_dbRow_movimientoResumen.empresa_producto_id):
                
                #Query para saber si existe un concepto con el empresa_producto_id en ese CFDI.
                _dbRows_cfdi_conceptos = dbc01(
                    (dbc01.tcfdi_conceptos.cfdi_id == _dbRow_movimientoMaestro.cfdi_egreso_id)
                    &(dbc01.tcfdi_conceptos.empresa_prodserv_id == _dbRow_movimientoResumen.empresa_producto_id)
                    ).select(
                        dbc01.tcfdi_conceptos.id,
                        dbc01.tcfdi_conceptos.empresa_prodserv_id,
                        dbc01.tcfdi_conceptos.noidentificacion,
                        dbc01.tcfdi_conceptos.cantidad,
                        dbc01.tcfdi_conceptos.valorunitario,
                        dbc01.tcfdi_conceptos.descripcion,
                        dbc01.tcfdi_conceptos.claveunidad,
                        dbc01.tcfdi_conceptos.importe,
                        dbc01.tempresa_invmovto_productos.cantidad_salida.coalesce_zero().sum().with_alias('registrados'),
                        left = [
                            dbc01.tempresa_inventariomovimientos.on(
                                (dbc01.tempresa_inventariomovimientos.cfdi_egreso_id == dbc01.tcfdi_conceptos.cfdi_id)
                                ),
                            dbc01.tempresa_invmovto_productos.on(
                                (dbc01.tempresa_invmovto_productos.empresa_inventariomovimiento_id == dbc01.tempresa_inventariomovimientos.id)
                                & (dbc01.tempresa_invmovto_productos.empresa_producto_id == dbc01.tcfdi_conceptos.empresa_prodserv_id)
                                & (dbc01.tempresa_invmovto_productos.id != _s_empresa_inventariomovimientoresumen_id)
                                ),
                            ],
                        groupby = [
                            dbc01.tcfdi_conceptos.id,
                            dbc01.tcfdi_conceptos.empresa_prodserv_id,
                            dbc01.tcfdi_conceptos.noidentificacion,
                            dbc01.tcfdi_conceptos.cantidad,
                            dbc01.tcfdi_conceptos.valorunitario,
                            dbc01.tcfdi_conceptos.descripcion,
                            dbc01.tcfdi_conceptos.claveunidad,
                            dbc01.tcfdi_conceptos.importe,
                            ]
                        
                        )
                    
            else:
                pass
                
            if len(_dbRows_cfdi_conceptos) == 1:
                if(_dbRow_movimientoResumen.producto_externo == _dbRows_cfdi_conceptos.last().tcfdi_conceptos.noidentificacion):
                    _dbTable.producto_externo.writable = False
                else:
                    # Se pudiera asociar el producto_externo al noidentification en este punto, pero se le permitira al usuario capturarlo
                    pass
                _dbRow_cfdi_concepto = _dbRows_cfdi_conceptos.last()                    
                
            else:
                pass
            
        else:
            pass
        
    else:
        pass

    
    _O_cat.addRowContentEvent('onValidation', TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.SALIDA.PORDEVOLUCION_COMPRA_RESUMEN)
   
    _D_returnVars = _O_cat.process()

    if ((_s_action == request.stv_fwk_permissions.btn_save.code) and (_D_returnVars.rowContent_form.accepted)):
         
        _D_result = TEMPRESA_INVMOVTO_PRODUCTOS.VALIDAR_ROW_SALIDA_DEVOLUCION_COMPRA(
            _D_returnVars.rowContent_form.vars.id
            )
         
        if _D_result.E_return != CLASS_e_RETURN.OK:
            stv_flashWarning("Inconsistencias en producto", _D_result.s_msgError)  
        else:
            pass        
 
    else:
        #Si es cualquier opción en vez de salvar no hace nada
        pass

    _D_returnVars.dbRow_cfdi_concepto = _dbRow_cfdi_concepto

    return _D_returnVars

def embarque_almacenes_salidas_ver():
    ''' Función para las salidas por traspaso de almacén
        Args:
            arg0: embarque id
    
    '''
    _dbTable = dbc01.tempresa_embarque_almacenes
    
    _s_almacen_id = request.vars.master_empresa_plaza_sucursal_almacen_id
    
    _LD_data = []
    
    if (len(request.args) > 0) :
        
        _s_embarque_id = request.args(0, None)
        
        _rows_result = dbc01(
            (_dbTable.empresa_embarque_id == _s_embarque_id)
            & (_dbTable.empresa_plaza_sucursal_almacen_id != _s_almacen_id)
            & (
                _dbTable.estatus.belongs(
                    TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.EN_ESPERA,
                    TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CARGANDO,
                    TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.DESCARGANDO,
                    )
                )
            ).select(
                _dbTable.ALL,
                orderby = _dbTable.orden
                )
            
        for _row in _rows_result:
        
            _LD_data.append({ 
                'id':_row.id, 'optionname':(_dbTable._format(_row))})
                            
        return (response.json({'data': _LD_data}))
    else:
        return (response.json({'data': [], 'error': 'Argumentos inválidos'}))    


def movto_salida_portraspaso():
    '''Función para las salidas por traspaso de almacén
        Args:
            [arg0]: codigo de funcionalidad
            [arg1]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    '''
    
    _s_action = request.args(0, request.stv_fwk_permissions.btn_none.code)
        
    _dbTable = dbc01.tempresa_inventariomovimientos
    
    _s_empresa_plaza_sucursal_almacen_id = request.vars.master_empresa_plaza_sucursal_almacen_id
    
    _s_cuenta_aplicacion_id = D_stvFwkCfg.cfg_cuenta_aplicacion_id

    _s_empresa_inventariomovimiento_id = request.args(1, None)
    
    _D_result_empresa_id = TCUENTA_APLICACIONES.GET_EMPRESA_ID_CACHE(
        _s_cuenta_aplicacion_id
        )
        
    if _D_result_empresa_id.E_return != CLASS_e_RETURN.OK:
        #Si entra acá es que ocurrió un error durante la obtención del concepto_id
        _s_empresa_id = None
        stv_flashError("Empresa ID no encontrado.", _D_result_empresa_id.s_msgError)    
    elif _D_result_empresa_id.n_empresa_id:
        _s_empresa_id = _D_result_empresa_id.n_empresa_id
    else:
        #hubo bronca, no trajo error pero tampoco trajo la empresa_id
        _s_empresa_id = None
        stv_flashError("Empresa ID no encontrado.", "Empresa no definida para almacenes.")     
        
        
    _D_result_concepto_id = TEMPRESA_ALMACENESCONCEPTOS.GET_ID_CACHE(
        n_empresa_id = _s_empresa_id, 
        n_tipoconcepto = TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.SALIDA, 
        n_clasificacion = TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.TRASPASO_SALIDA
        )
    
    # Por default el query se pone en false para no regresar nada en las búsquedas
    _qry = False
    
    if _D_result_concepto_id.E_return != CLASS_e_RETURN.OK:
        #Si entra acá es que ocurrió un error durante la obtención del concepto_id
        _n_concepto_id = None
        stv_flashError("Salida por traspaso", _D_result_concepto_id.s_msgError)        
    elif _D_result_concepto_id.n_concepto_id:
        _n_concepto_id = _D_result_concepto_id.n_concepto_id
        _dbTable.empresa_almacenesconcepto_id.default = _n_concepto_id
        _dbTable.empresa_almacenesconcepto_id.writable = False

        _qry = (
            (_dbTable.empresa_id == _s_empresa_id)
            &(_dbTable.empresa_almacenesconcepto_id == _n_concepto_id)
            &(_dbTable.empresa_plaza_sucursal_almacen_id == _s_empresa_plaza_sucursal_almacen_id)
            )
        
       ##TODO: Mostrar los movimientos que tengan un almacén en embarque_almacenes abierto.
        if _s_action in (request.stv_fwk_permissions.btn_new.code, request.stv_fwk_permissions.btn_none.code):
            #Se agrega este query.
            _qry &= (dbc01.tempresa_embarque_almacenes.empresa_embarque_id == _dbTable.empresa_embarque_id)
            _qry &= (dbc01.tempresa_embarque_almacenes.empresa_plaza_sucursal_almacen_id == _dbTable.empresa_plaza_sucursal_almacen_id)
            _qry &= (dbc01.tempresa_embarque_almacenes.estatus != TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CERRADO)
            pass   
        else:
            #Si es buscar todo el Query no aplica, ya que se puede filtrar todas las salidas
            pass
    else:
        #hubo bronca, no trajo error pero tampoco trajo concepto_id
        _n_concepto_id = None
        stv_flashError("Salida por traspaso", "Concepto no definido en Almacenes-Conceptos")  

    _dbTable.empresa_embarque_almacen_id_relacionado.required = True

    _dbTable.empresa_embarque_almacen_id_relacionado.notnull = True

    _dbTable.empresa_embarque_id.required = True

    _dbTable.empresa_embarque_id.notnull = True
    
    _dbTable.empresa_id.default = _s_empresa_id
    _dbTable.empresa_plaza_sucursal_almacen_id.default = _s_empresa_plaza_sucursal_almacen_id

    #Se asigna la fecha de registro con el día y hora actual. 
    _dbTable.fecha_registro.writable = False
    _dbTable.fecha_registro.default = request.browsernow  
    
    #Se pone la fecha de salida editable.
    _dbTable.fecha_salida.writable = True
    
    _dbTable.empresa_embarque_almacen_id_relacionado.label = 'Almacén destino'
    
    _dbTable.estatus_movimiento.default = TEMPRESA_INVENTARIOMOVIMIENTOS.ESTATUS_MOVIMIENTO.CARGANDO
    _dbTable.estatus_movimiento.writable = False
    
    #Lista que tendrá todos los embarques a los que puede acceder este almacén
    _L_embarques_id = []
    if(_s_action == request.stv_fwk_permissions.btn_new.code):
        #Si _s_action es igual al botón de nuevo entonces verfica si existen embarques abiertos/en_ruta almacenes y que su primer
        #almacén con estatus != de cerrado asociado al embarque sea el almacén actual.
        
        #Se consiguen los embarques que están abiertos o en ruta y tienen este almacén sin cerrar.  
        _dbRows_embarques = dbc01(
            (dbc01.tempresa_embarques.empresa_id == _s_empresa_id)
            &(dbc01.tempresa_embarques.estatus.belongs(
                TEMPRESA_EMBARQUES.ESTATUS.ABIERTO,
                TEMPRESA_EMBARQUES.ESTATUS.RUTA
                )
              )
            &(dbc01.tempresa_embarque_almacenes.empresa_embarque_id == dbc01.tempresa_embarques.id)
            &(dbc01.tempresa_embarque_almacenes.empresa_plaza_sucursal_almacen_id == _s_empresa_plaza_sucursal_almacen_id)
            &(dbc01.tempresa_embarque_almacenes.estatus.belongs(
                TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.EN_ESPERA,
                TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CARGANDO,
                TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.DESCARGANDO
                )
              )
            ).select(
                dbc01.tempresa_embarques.id,
                groupby =[
                    dbc01.tempresa_embarques.id,
                    ]
                )
        if not(_dbRows_embarques):
            #No hace nada, _b_embarque_almacen seguirá en false y tirará error más adelante.
            pass
        else:
            
            for _dbRow_embarque in _dbRows_embarques:
                #De los embarques que se regresó se busca los almacenes que pertenezcan a ese embarque y que estén en estatus de
            #En_espera, Cargando, Descargando para ordenarlo por Orden o ID.
            
                _dbRows_embarque_almacenes = dbc01(
                    (dbc01.tempresa_embarque_almacenes.empresa_embarque_id == _dbRow_embarque.id)
                    &(dbc01.tempresa_embarque_almacenes.estatus.belongs(
                        TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.EN_ESPERA,
                        TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CARGANDO,
                        TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.DESCARGANDO
                        )
                    )
                    ).select(
                        dbc01.tempresa_embarque_almacenes.ALL,
                        orderby = [
                            (
                                dbc01.tempresa_embarque_almacenes.orden
                            )
                            ]
                        )
                    
                if len(_dbRows_embarque_almacenes) <= 1:
                    # Si es el ultimo almacén en el embarque o si no se encontró el almacen, no se pone como seleccionable
                    pass
                else:
                    _dbRow_embarque_almacenes = _dbRows_embarque_almacenes.first()
                    
                    if(_dbRow_embarque_almacenes.empresa_plaza_sucursal_almacen_id == int(_s_empresa_plaza_sucursal_almacen_id)):
                    #Si el primer embarque que regresó es en el que quiere hacerse el movimiento entonces el boleano pasará a true
                        
                        _L_embarques_id += [_dbRow_embarque_almacenes.empresa_embarque_id]
                        
                    else:
                        #No hace nada, sigue en false el boleano
                        pass
                
            #Final del for para _dbRow_embarques
            
        if _L_embarques_id:
            #Si regresó algún embarque entonces se gerará un combobox con todos los embarques que estén disponibles para ese momento
            _dbTable.empresa_embarque_id.requires = IS_IN_DB(
                dbc01(
                    (dbc01.tempresa_embarques.id.belongs(_L_embarques_id))
                    ),
                'tempresa_embarques.id',
                dbc01.tempresa_embarques._format
                )
        else:
            #No hace nada, _b_embarque_almacen seguirá en false y tirará error más adelante.
            pass
         
    else:
        #el _s_action es diferente de nuevo, sigue su función normal.
        pass 
    
    if(_s_action in (request.stv_fwk_permissions.btn_edit.code, request.stv_fwk_permissions.btn_save.code )):
        #Se evita la edición de todos los componenetes
        if(_s_empresa_inventariomovimiento_id):
            _dbTable.empresa_embarque_almacen_id_relacionado.writable = False
            _dbTable.empresa_embarque_id.writable = False
            _dbTable.fecha_movimiento.writable = False
            _dbTable.observaciones.writable = False
            _dbTable.folio.writable = False
        else:
            pass
    else:
        pass
        
    _L_dbFields = [
        _dbTable.id,
        _dbTable.empresa_embarque_almacen_id_relacionado,
        _dbTable.empresa_embarque_id,
        _dbTable.observaciones,
        _dbTable.fecha_registro,
        _dbTable.fecha_movimiento,
        _dbTable.estatus_movimiento
        ]
    
    _L_GroupBy = [
        _dbTable.id
        ]
    
    if _n_concepto_id:
        # Si concepto esta definido, permitir hacer todo
        _L_formButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_new,
            request.stv_fwk_permissions.btn_save,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_edit,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_remove,
            request.stv_fwk_permissions.btn_find,
            request.stv_fwk_permissions.btn_findall,
            ]
    else:
        # Si concepto no esta definido, no permitir hacer nada
        _L_formButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_findall
            ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = _L_formButtons, 
        s_tabId = STV_CFG_APP_ALMACENES.ID,
        L_fieldsToShow = _L_dbFields,
        L_fieldsToGroupBy= _L_GroupBy,
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
    )

    _O_cat.addRowContentEvent('onValidation', TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.SALIDA.PORTRASPASO)
    
    #Se crea el botón especial 1 para poder cerrar el traspaso.
    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionLabel = "Confirmar salida en ruta",
        s_permissionIcon = "fa fa-compass",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
        confirm = Storage(
            s_title = "Favor de confirmar operación", 
            s_text = "¿Está seguro de querer mover a 'En ruta'?", 
            s_type = "warning", 
            s_btnconfirm = "Aceptar", 
            s_btncancel = "Cancelar", 
            )   
            ),
        L_options = []
        )
    
    #Se crea el botón especial 2 para poder regesar el movimiento a "cargando".
    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_02.code,
        s_permissionLabel = "Cancelar salida en ruta",
        s_permissionIcon = "fa fa-compass",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
        confirm = Storage(
            s_title = "Favor de confirmar operación", 
            s_text = "¿Está seguro de querer mover a 'Cargando' el movimiento?", 
            s_type = "warning", 
            s_btnconfirm = "Aceptar", 
            s_btncancel = "Cancelar", 
            )   
            ),
        L_options = []
        )
    
    
    _b_skipAction = False
    #Se utiliza el overrideView a este nivel porque como se va a eliminar el registro tiene que hacerse antes del _O_cat.process()
    _D_overrideView = None
    if(_s_action == request.stv_fwk_permissions.btn_remove.code):
        
        _dbRow_movimiento_salida_traspaso = dbc01.tempresa_inventariomovimientos(_s_empresa_inventariomovimiento_id)
        
        #Sacará los movimientos que tengan para este almacén y que sean diferentes al ID actual. 
        _dbRows_movimientos = dbc01(
            (dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id_relacionado == _dbRow_movimiento_salida_traspaso.empresa_embarque_almacen_id)
            ).select(
                dbc01.tempresa_inventariomovimientos.id
                )
        
        if not _dbRows_movimientos:
            #Si esta salida NO cuenta con un movimiento hermano (entrada por traspaso), se deberá permitir borrar. 
            
            #Query para revisar si existen más salidas que pertenezcan a este almacén. 
            _dbRows_movimientos_salidas = dbc01(
                (dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id == _dbRow_movimiento_salida_traspaso.empresa_embarque_almacen_id)
                &(dbc01.tempresa_inventariomovimientos.empresa_almacenesconcepto_id == _n_concepto_id)
                &(dbc01.tempresa_inventariomovimientos.id != _s_empresa_inventariomovimiento_id)
                ).select(
                    dbc01.tempresa_inventariomovimientos.id
                    )

            if not _dbRows_movimientos_salidas:
                #Si no tiene más movimientos de salida asociados a este almacen de embarque, hará toda la lógica.
                
                _dbRow_empresa_embarque_almacen = dbc01.tempresa_embarque_almacenes(_dbRow_movimiento_salida_traspaso.empresa_embarque_almacen_id)
                
                if(_dbRow_empresa_embarque_almacen.estatus != TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CERRADO):
                    #Si el estatus de almacén es diferente de cerrado entonces se podrá eliminar sin problema el movimiento
                    dbc01(
                        (dbc01.tempresa_embarque_almacenes.id == _dbRow_movimiento_salida_traspaso.empresa_embarque_almacen_id)
                        ).update(
                            estatus = TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.EN_ESPERA
                            )
                    #Si no hubo más movimientos a parte del que se quiere eliminar se elimina pero también el almacen_embarque cambia su estatus a "en_espera"
                    stv_flashSuccess("Movimiento traspaso", "Se eliminó correctamente el movimiento y se devuleve el almacén del embarque a 'En espera'") 
                    _D_overrideView = request.stv_fwk_permissions.view_refresh
                    
                    _dbRows_embarque_almacenes = dbc01(
                        (dbc01.tempresa_embarque_almacenes.empresa_embarque_id == _dbRow_movimiento_salida_traspaso.empresa_embarque_id)
                        &(dbc01.tempresa_embarque_almacenes.estatus != TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.EN_ESPERA)
                        ).select(
                            dbc01.tempresa_embarque_almacenes.id
                            )
                        
                    if(_dbRows_embarque_almacenes):
                        #Sí tiene embarques con estatus diferente de en espera, no hace nada.
                        pass
                    else:
                        #No tiene embarque con estatus diferente de en espera, se procede a cambiar el estatus del embarque a "Abierto"
                        dbc01(
                            (dbc01.tempresa_embarques.id == _dbRow_movimiento_salida_traspaso.empresa_embarque_id)
                            ).update(
                                estatus = TEMPRESA_EMBARQUES.ESTATUS.ABIERTO
                                )
                        pass
                    
                else:
                    ###TODO: Preguntar a Jaime si esto todavía aplica.
                    
                    #Si el embarque tiene un estatus == a cerrado entonces no podrá cambiarse su estatus a "En_espera"
                    stv_flashError("Movimiento salida por traspaso", "El movimiento no se pudo eliminar ya que el almacén al cual pertenece ya cuenta con estatus 'Cerrado'") 
                    
                    #Se usa el _b_skipAction para hacerle bypass al eliminar
                    _b_skipAction = True                
                     
                    _D_overrideView = request.stv_fwk_permissions.view_refresh
                    
            else:
                #Si tiene mas movimientos entonces solo procede a borrar el movimiento pero no a cambiar el estatus del embarque.
                stv_flashSuccess("Movimiento salida por traspaso", "Se eliminó correctamente el movimiento.") 

                _D_overrideView = request.stv_fwk_permissions.view_refresh
                
        else:
            #Si esta salida ya cuenta con un movimiento hermano (entrada por traspaso) no se deberá permitir borrar. 
            stv_flashError("Movimiento salida por traspaso", "El movimiento no puede ser borrado porque ya cuenta con una entrada por traspaso asociada.") 
            
            #Se usa el _b_skipAction para hacerle bypass al eliminar
            _b_skipAction = True                
            
            _D_overrideView = request.stv_fwk_permissions.view_refresh

            
    else:
        pass

    if(_D_overrideView):
        _D_returnVars = _O_cat.process(D_overrideView = _D_overrideView, b_skipAction = _b_skipAction)
    else:
        _D_returnVars = _O_cat.process()
    
    if(_s_action == request.stv_fwk_permissions.btn_new.code):
        
        #Si _b_embarque_almacen es falso se mandará error y evitará que se agregue un registro nuevo
        if not _L_embarques_id:
            #Si no regresó ningún embarque mandará un error y regresará la vista.
            stv_flashError("Embarques.", "No se encontraron embarques pendientes para este almacén.")
            #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
            from gluon.storage import List
            request.args = List([request.stv_fwk_permissions.btn_none.code,])
            _D_returnVars = movto_salida_portraspaso()
            pass
        else:
            pass        
    else:
        #el _s_action es diferente de nuevo, sigue su función normal.
        pass 

    if (_s_action == request.stv_fwk_permissions.btn_save.code) and (_D_returnVars.rowContent_form.accepted):
        
        #Se hace una consulta al registro que se acaba de guardar
        _dbRow_inventariomovimiento = dbc01.tempresa_inventariomovimientos(_D_returnVars.rowContent_form.vars.id)
            
        #Aquí solo entrará cuando se guarde la fecha salida (solo visible desde editar)
        if(_D_returnVars.rowContent_form.vars.fecha_salida):
            #Si se guardó correctamente la fecha de salida
            
            _dbRows_inventariomovimiento_resumen = dbc01(
                dbc01.tempresa_invmovto_productos.empresa_inventariomovimiento_id == _dbRow_inventariomovimiento.id
                ).select(
                    dbc01.tempresa_invmovto_productos.ALL
                    )
            
            if(_dbRow_inventariomovimiento.estatus_movimiento == TEMPRESA_INVENTARIOMOVIMIENTOS.ESTATUS_MOVIMIENTO.CARGANDO):
                #Si está cargando el movimiento entonces pasará a estatus de "En ruta"
                _dbRow_inventariomovimiento.estatus_movimiento = TEMPRESA_INVENTARIOMOVIMIENTOS.ESTATUS_MOVIMIENTO.RUTA
                _dbRow_inventariomovimiento.update_record()
                
                
                _dbRow_embarque_almacen = dbc01.tempresa_embarque_almacenes(_dbRow_inventariomovimiento.empresa_embarque_almacen_id)
            
                if(_dbRow_embarque_almacen.estatus == TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CARGANDO):
                    # Si está cargando entonces pasará el estatus a "En ruta" el almacén del embarque
                   _dbRow_embarque_almacen.estatus = TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CERRADO
                   _dbRow_embarque_almacen.update_record()
                   
                else:
                    pass
                
                _dbRow_embarque = dbc01.tempresa_embarques(_dbRow_inventariomovimiento.empresa_embarque_id)

                if(_dbRow_embarque.estatus == TEMPRESA_EMBARQUES.ESTATUS.ABIERTO):
                    # Si está cargando entonces pasará el estatus a "En ruta" el almacén del embarque
                   _dbRow_embarque.estatus = TEMPRESA_EMBARQUES.ESTATUS.RUTA
                   _dbRow_embarque.update_record()
                else:
                    pass
                
            else:
                #No ya está en estatus de Ruta, no pasa nada
                pass
            
        else:
            #Si no guardó la fecha de salida es que viene desde nuevo
            dbc01(
                (dbc01.tempresa_embarque_almacenes.id == _dbRow_inventariomovimiento.empresa_embarque_almacen_id)
                ).update(
                    estatus = TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CARGANDO
                    )
        
        #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
        from gluon.storage import List
        request.args = List([request.stv_fwk_permissions.btn_view.code, _D_returnVars.rowContent_form.vars.id])
        _D_returnVars = movto_salida_portraspaso()
        
    else:
        _D_returnVars.update(
            dict(
                msg = 'Bienvenidos',
                dbFieldAlmacenes = STV_LIB_APP_ALMACENES.GET_DBFIELD_ALMACENES(
                    n_default = _s_empresa_plaza_sucursal_almacen_id or 0,
                    b_writable = False
                    ),
                L_breadcrub = [
                    STV_ALMACENES.BREADCRUB.BC_INDEX,
                    STV_ALMACENES.BREADCRUB.MOVTO.SALIDA.BC_PORTRASPASO
                    ]
                )
            )
        
        
    if(_s_action in (request.stv_fwk_permissions.special_permisson_01.code,)):
    #Si se le dio clic a "En ruta" hará la lógica para pasar el estatus de Almacenes a cerrado y el movimiento a "En ruta".
        _s_salida_traspaso_id = request.args(1, None)
        
        _dbRow_movimiento_salida_traspaso = dbc01.tempresa_inventariomovimientos(_s_salida_traspaso_id)
        
        #Bandera para saber si mostrar el success del cambio de estatus
        _b_cambioestatus = False
        
        _dbRows_movimientos_salidas = dbc01(
            (dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id == _dbRow_movimiento_salida_traspaso.empresa_embarque_almacen_id)
            &(dbc01.tempresa_inventariomovimientos.empresa_almacenesconcepto_id == _n_concepto_id)
            ).select(
                dbc01.tempresa_inventariomovimientos.id,
                orderby = [dbc01.tempresa_inventariomovimientos.id]
                )
        
        #Query para saber si se tiene una entrada pendiente para este almacén. Se buscará alguna salida que venga a este embarque.
        _dbRow_movimiento_entrada = dbc01(
            (dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id_relacionado == _dbRow_movimiento_salida_traspaso.empresa_embarque_almacen_id)
            &(dbc01.tempresa_inventariomovimientos.empresa_almacenesconcepto_id == _n_concepto_id)
            &(dbc01.tempresa_inventariomovimientos.estatus_movimiento == TEMPRESA_INVENTARIOMOVIMIENTOS.ESTATUS_MOVIMIENTO.RUTA)
            ).select(
                dbc01.tempresa_inventariomovimientos.id
                )
        
        if not _dbRow_movimiento_entrada:
            
            for _dbRow_movimiento_salida in _dbRows_movimientos_salidas:
            
                #Query para verificar que se tengan movimientos a nivel detalle de este movimiento de almacén.
                _dbRows_salida_movtos = dbc01(
                    (dbc01.tempresa_invmovto_productos.empresa_inventariomovimiento_id == _dbRow_movimiento_salida.id)
                    ).select(
                        dbc01.tempresa_invmovto_productos.id
                        )
                
                if(_dbRows_salida_movtos):
                    #Si tiene movimientos entonces puede hacer la lógica para cambiar el estatus.
                    
                    _dbRow_embarque_almacenes = dbc01.tempresa_embarque_almacenes(_dbRow_movimiento_salida_traspaso.empresa_embarque_almacen_id)
                    
                    if(_dbRow_movimiento_salida_traspaso.estatus_movimiento != TEMPRESA_INVENTARIOMOVIMIENTOS.ESTATUS_MOVIMIENTO.RUTA):
                        #Si el movimiento actual tiene su estatus diferente a "En_ruta" entonces se puede cambiar el estatus
                        dbc01(
                            (_dbTable.id == _dbRow_movimiento_salida.id)
                            ).update(
                                estatus_movimiento = TEMPRESA_INVENTARIOMOVIMIENTOS.ESTATUS_MOVIMIENTO.RUTA
                                )
        
                        if(_dbRow_embarque_almacenes.estatus in (TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CARGANDO, TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.DESCARGANDO, TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.EN_ESPERA)):
                            #Si el embarque_almacen tiene un estatus de "Cargando" se pasa a cerrado. 
                            dbc01(
                                (dbc01.tempresa_embarque_almacenes.id == _dbRow_embarque_almacenes.id)
                                ).update(
                                    estatus = TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CERRADO
                                    )
                            
                            _dbRow_embarque = dbc01.tempresa_embarques(_dbRow_embarque_almacenes.empresa_embarque_id)
                            
                            #Si el embarque aún está abierto se pasará a en ruta.
                            if(_dbRow_embarque.estatus == TEMPRESA_EMBARQUES.ESTATUS.ABIERTO):
                                dbc01(
                                    (dbc01.tempresa_embarques.id == _dbRow_embarque.id)
                                    ).update(
                                        estatus = TEMPRESA_EMBARQUES.ESTATUS.RUTA
                                        )
                            else:
                                pass
                            
                            _b_cambioestatus = True
                            
                        else:
                            pass
                            
                    else:
                        
                        stv_flashError("Estatus de la salida", "Esta salida ya tiene el estatus En ruta.")
                        #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
                        from gluon.storage import List
                        ###TODO: Preguntar a Jaime si está bien hacer un rollback a este nivel. 
                        dbc01.rollback()
                        _b_cambioestatus = False
                        request.args = List([request.stv_fwk_permissions.btn_findall.code,])
                        _D_returnVars = movto_salida_portraspaso()
                        
                        break
                        
                            
                else:
                    #En caso de no tener movimientos se mostrará el error.
                    stv_flashError("Estatus de la salida", "El detalle de este movimiento no cuenta con productos, no se puede cambiar el estatus.")
                    #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
                    from gluon.storage import List
                    _b_cambioestatus = False
                    ###TODO: Preguntar a Jaime si está bien hacer un rollback a este nivel. 
                    dbc01.rollback()
                    
                    request.args = List([request.stv_fwk_permissions.btn_findall.code,])
                    _D_returnVars = movto_salida_portraspaso()
                    
                    break
                
            if(_b_cambioestatus == True):
                stv_flashSuccess("Estatus de la salida", "Se pasó a 'En ruta' el movimiento de salida.") 
                #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
                from gluon.storage import List
                request.args = List([request.stv_fwk_permissions.btn_findall.code,])
                _D_returnVars = movto_salida_portraspaso()
            else:
                pass
        else:
            #En caso de tener entradas pendientes se mandará error
            stv_flashError("Estatus de la salida", "El almacén que trata de mandar a 'Ruta' tiene entradas pendientes, favor de hacerlas antes de este paso.")
            #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
            from gluon.storage import List
            request.args = List([request.stv_fwk_permissions.btn_findall.code,])
            _D_returnVars = movto_salida_portraspaso()
            _b_cambioestatus = False
            
    else:
        pass
        
    if _n_concepto_id:
        # Si concepto esta definido, permitir el detalle

        _O_cat.addSubTab(
            dbTableDetail = dbc01.tempresa_invmovto_productos, 
            s_url = URL(f='movto_salida_portraspaso_resumen', args=['master_'+str(request.function),request.args(1)]),
            s_idHtml = fn_createIDTabUnique(f = 'movto_salida_portraspaso_resumen', s_masterIdentificator = str(request.function))  
            )
    else:
        # de lo contrario, no mostrar el detalle
        pass
    
    if(_s_action in (request.stv_fwk_permissions.special_permisson_02.code,)):
    #Si se le dio clic a "Cambiar estatus a cargando" hará la lógica para pasar el estatus de Almacenes a cargando y el movimiento a "cargando".
        
        _s_salida_traspaso_id = request.args(1, None)
        
        _dbRow_movimiento_salida_traspaso = dbc01.tempresa_inventariomovimientos(_s_salida_traspaso_id)
        
        #Bandera para saber si mostrar el success del cambio de estatus
        _b_cambioestatus = False
        
        #Que traiga si hay salidas para este almacén que estén cerradas. 
        _dbRows_movimientos_salidas = dbc01(
            (dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id == _dbRow_movimiento_salida_traspaso.empresa_embarque_almacen_id)
            &(dbc01.tempresa_inventariomovimientos.empresa_almacenesconcepto_id == _n_concepto_id)
            &(dbc01.tempresa_inventariomovimientos.estatus_movimiento == TEMPRESA_INVENTARIOMOVIMIENTOS.ESTATUS_MOVIMIENTO.ENTREGADO)
            ).select(
                dbc01.tempresa_inventariomovimientos.id
                )
        
        if not _dbRows_movimientos_salidas:
            #Query que traerá los movimientos de salida que pertenecen a este empresa_embarque_almacen_id
            _dbRows_movimientos_salidas = dbc01(
                (dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id == _dbRow_movimiento_salida_traspaso.empresa_embarque_almacen_id)
                &(dbc01.tempresa_inventariomovimientos.empresa_almacenesconcepto_id == _n_concepto_id)
                ).select(
                    dbc01.tempresa_inventariomovimientos.ALL
                    )
            
            for _dbRow_movimiento_salida in _dbRows_movimientos_salidas:
               
                dbc01(
                    (dbc01.tempresa_inventariomovimientos.id == _dbRow_movimiento_salida.id)
                    ).update(
                        estatus_movimiento = TEMPRESA_INVENTARIOMOVIMIENTOS.ESTATUS_MOVIMIENTO.CARGANDO
                        )
                    
                dbc01(
                    (dbc01.tempresa_embarque_almacenes.id == _dbRow_movimiento_salida.empresa_embarque_almacen_id)
                    ).update(
                        estatus = TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CARGANDO
                        )
            
            _dbRows_embarque_almacenes = dbc01(
                (dbc01.tempresa_embarque_almacenes.empresa_embarque_id == _dbRow_movimiento_salida_traspaso.empresa_embarque_id)
                &(
                    dbc01.tempresa_embarque_almacenes.estatus.belongs(
                        TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CERRADO,
                        TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CARGANDO,
                        TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.DESCARGANDO
                        )
                    )
                ).select(
                    dbc01.tempresa_embarque_almacenes.id
                    )
                
            if not _dbRows_embarque_almacenes:
                dbc01(
                    (dbc01.tempresa_embarques.id == _dbRow_movimiento_salida.empresa_embarque_id)
                    ).update(
                        estatus = TEMPRESA_EMBARQUES.ESTATUS.ABIERTO
                        )
            else:
                pass
                
                    
            _b_cambioestatus = True
            
        
        else:
            stv_flashError("Estatus de la salida", "Esta salida no puede pasarse a 'cargando' porque tiene movimientos que ya fueron entregados.")
            #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
            from gluon.storage import List
            request.args = List([request.stv_fwk_permissions.btn_findall.code,])
            _D_returnVars = movto_salida_portraspaso()
            
        if(_b_cambioestatus == True):
            stv_flashSuccess("Estatus de la salida", "Se pasó a 'Cargando' el movimiento de salida.") 
            #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
            from gluon.storage import List
            request.args = List([request.stv_fwk_permissions.btn_findall.code,])
            _D_returnVars = movto_salida_portraspaso()
        else:
            #Hubo un error, no puede cambiarse el estatus.
            pass
            
    else:
        pass
    
    return _D_returnVars

def movto_salida_portraspaso_resumen():
    '''Función para los resumenes de salida por devolución de compra 
    
        Args:
            arg0: master_[nombre función maestro]
            arg1: empresa_inventariomovimiento_id
            [arg2]: codigo de funcionalidad
            [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    '''
    
    _dbTable = dbc01.tempresa_invmovto_productos
    
    _s_empresa_inventariomovimiento_id = request.args(1, None)
    
    _s_empresa_plaza_sucursal_almacen_id = request.vars.master_empresa_plaza_sucursal_almacen_id

    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.empresa_producto_id,
        _dbTable.cantidad_salida,
        _dbTable.observaciones
        ]
    
    #Row de tempresa_embarque_almacenes que pertenece a este movimiento.
    _dbRows_embarques_almacenes = dbc01(
        (dbc01.tempresa_inventariomovimientos.id == _s_empresa_inventariomovimiento_id)
        &(dbc01.tempresa_embarque_almacenes.id == dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id)
        ).select(
            dbc01.tempresa_embarque_almacenes.ALL
            )
    
    if(_dbRows_embarques_almacenes):
        
        _dbRow_embarques_almacenes = _dbRows_embarques_almacenes.last()
        
        if(_dbRow_embarques_almacenes.estatus == TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CERRADO):
            
            #Si el embarque_almacen tiene el estatus de cerrado no podrán agregarse más productos a esta salida.
            _L_formButtons =[
                request.stv_fwk_permissions.btn_none,
                request.stv_fwk_permissions.btn_findall
                ]
        else:
            
            #De lo contrario se permiten todos los botones
            _L_formButtons = [
                request.stv_fwk_permissions.btn_none,
                request.stv_fwk_permissions.btn_new,
                request.stv_fwk_permissions.btn_save,
                request.stv_fwk_permissions.btn_cancel,
                request.stv_fwk_permissions.btn_edit,
                request.stv_fwk_permissions.btn_view,
                request.stv_fwk_permissions.btn_remove,
                request.stv_fwk_permissions.btn_find,
                request.stv_fwk_permissions.btn_findall,
                ]
    else:
        pass
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(),
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_inventariomovimientos,
        xFieldLinkMaster = [
            _dbTable.empresa_inventariomovimiento_id
            ],
        L_visibleButtons = _L_formButtons, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
        )
   
    _D_returnVars = _O_cat.process()

    return _D_returnVars

def movto_salida_otra():
    '''Función para las salidas por otros tipo de salidas

        Args:
            [arg0]: codigo de funcionalidad
            [arg1]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    '''

    _s_action = request.args(0, request.stv_fwk_permissions.btn_none.code)
        
    _dbTable = dbc01.tempresa_inventariomovimientos
    
    _s_empresa_plaza_sucursal_almacen_id = request.vars.master_empresa_plaza_sucursal_almacen_id
    
    _s_cuenta_aplicacion_id = D_stvFwkCfg.cfg_cuenta_aplicacion_id

    _s_empresa_inventariomovimiento_id = request.args(1, None)
    
    _D_result_empresa_id = TCUENTA_APLICACIONES.GET_EMPRESA_ID_CACHE(
        _s_cuenta_aplicacion_id
        )
        
    if _D_result_empresa_id.E_return != CLASS_e_RETURN.OK:
        #Si entra acá es que ocurrió un error durante la obtención del concepto_id
        _s_empresa_id = None
        stv_flashError("Empresa ID no encontrado.", _D_result_empresa_id.s_msgError)    
    elif _D_result_empresa_id.n_empresa_id:
        _s_empresa_id = _D_result_empresa_id.n_empresa_id
    else:
        #hubo bronca, no trajo error pero tampoco trajo la empresa_id
        _s_empresa_id = None
        stv_flashError("Empresa ID no encontrado.", "Empresa no definida para almacenes.")     
        
        
    _D_result_concepto_id = TEMPRESA_ALMACENESCONCEPTOS.GET_ID_CACHE(
        n_empresa_id = _s_empresa_id, 
        n_tipoconcepto = TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.SALIDA, 
        n_clasificacion = TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.OTRAS_SALIDAS
        )
    
    # Por default el query se pone en false para no regresar nada en las búsquedas
    _qry = False
    
    if _D_result_concepto_id.E_return != CLASS_e_RETURN.OK:
        #Si entra acá es que ocurrió un error durante la obtención del concepto_id
        _n_concepto_id = None
        stv_flashError("Otras salidas", _D_result_concepto_id.s_msgError)        
    elif _D_result_concepto_id.n_concepto_id:
        _n_concepto_id = _D_result_concepto_id.n_concepto_id
        _dbTable.empresa_almacenesconcepto_id.default = _n_concepto_id
        _dbTable.empresa_almacenesconcepto_id.writable = False

        _qry = (
            (_dbTable.empresa_id == _s_empresa_id)
            &(_dbTable.empresa_almacenesconcepto_id == _n_concepto_id)
            &(_dbTable.empresa_plaza_sucursal_almacen_id == _s_empresa_plaza_sucursal_almacen_id)
            )
        
        ###TODO: Mostrar los movimientos de salidas por otros. 
#         if _s_action in (request.stv_fwk_permissions.btn_new.code, request.stv_fwk_permissions.btn_none.code):
#             #Se agrega este query.
# #             _qry &= (None)
#             pass
#         else:
#             #Si es buscar todo el Query no aplica, ya que se puede filtrar todas las salidas
#             pass
    else:
        #hubo bronca, no trajo error pero tampoco trajo concepto_id
        _n_concepto_id = None
        stv_flashError("Otras salidas", "Concepto no definido en Almacenes-Conceptos")  
            
            
    _dbTable.empresa_id.default = _s_empresa_id
    _dbTable.empresa_plaza_sucursal_almacen_id.default = _s_empresa_plaza_sucursal_almacen_id

    #Se asigna la fecha de registro con el día y hora actual. 
    _dbTable.fecha_registro.writable = False
    _dbTable.fecha_registro.default = request.browsernow  
    
    _L_dbFields = [
        _dbTable.id,
        _dbTable.fecha_movimiento,
        _dbTable.remision,
        _dbTable.descripcion,
        _dbTable.observaciones, 
        ]
    
    _L_formButtons = [
        request.stv_fwk_permissions.btn_none,
        request.stv_fwk_permissions.btn_new,
        request.stv_fwk_permissions.btn_save,
        request.stv_fwk_permissions.btn_cancel,
        request.stv_fwk_permissions.btn_edit,
        request.stv_fwk_permissions.btn_view,
        request.stv_fwk_permissions.btn_find,
        request.stv_fwk_permissions.btn_findall,
        ]
        
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = _L_formButtons, 
        s_tabId = STV_CFG_APP_ALMACENES.ID,
        L_fieldsToShow = _L_dbFields, 
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
    )
    
    _O_cat.addRowContentEvent('onValidation', TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.SALIDA.OTRA)
    
    _D_returnVars = _O_cat.process()

    if (_s_action == request.stv_fwk_permissions.btn_save.code) and (_D_returnVars.rowContent_form.accepted):
        #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
        from gluon.storage import List
        request.args = List([request.stv_fwk_permissions.btn_view.code, _D_returnVars.rowContent_form.vars.id])
        _D_returnVars = movto_salida_otra()
         
    else:
        _D_returnVars.update(
            dict(
                msg = 'Bienvenidos',
                dbFieldAlmacenes = STV_LIB_APP_ALMACENES.GET_DBFIELD_ALMACENES(
                    n_default = _s_empresa_plaza_sucursal_almacen_id or 0,
                    b_writable = False
                    ),
                L_breadcrub = [
                    STV_ALMACENES.BREADCRUB.BC_INDEX,
                    STV_ALMACENES.BREADCRUB.MOVTO.SALIDA.BC_OTRA
                    ]
                )
            )
        
    if _n_concepto_id:
        # Si concepto esta definido, permitir el detalle

        _O_cat.addSubTab(
            dbTableDetail = dbc01.tempresa_invmovto_productos, 
            s_url = URL(f='movto_salida_otra_resumen', args=['master_'+str(request.function),request.args(1)]),
            s_idHtml = fn_createIDTabUnique(f = 'movto_salida_otra_resumen', s_masterIdentificator = str(request.function))  
            )
    else:
        # de lo contrario, no mostrar el detalle
        pass
        
    return _D_returnVars

def _movto_salida_otra_validation(O_form):
    '''Validación para los movimientos por otros tipo de salidas
    '''
    
    if not O_form.vars.remision:
        O_form.errors.remision = "La remisión es requerida."
    else:
        pass
    
    if not O_form.vars.descripcion:
        O_form.errors.descripcion = "La descripción es requerida."
    else:
        pass
    
    return O_form


def movto_salida_otra_resumen():
    '''Función para los resumenes de movimientos por otros tipo de salidas
    
        Args:
            arg0: master_[nombre función maestro]
            arg1: empresa_inventariomovimiento_id
            [arg2]: codigo de funcionalidad
            [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    '''
    
    _dbTable = dbc01.tempresa_invmovto_productos

    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.empresa_producto_id,
        _dbTable.cantidad_salida,
        ]
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(),
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_inventariomovimientos,
        xFieldLinkMaster = [
            _dbTable.empresa_inventariomovimiento_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
        )

    _O_cat.addRowContentEvent('onValidation', _movto_salida_otra_resumen_validation)
   
    _D_returnVars = _O_cat.process()

    return _D_returnVars

def _movto_salida_otra_resumen_validation(O_form):
    '''Validación para los resumenes de movimientos por otros tipo de salidas
    '''
    
    _s_empresa_inventariomovimiento_id = request.args(1, None)

    _n_cantidad = int(O_form.vars.cantidad_salida or 0)
        
    if (_n_cantidad <= 0):
        O_form.errors.cantidad_salida = "La cantidad que salida no puede ser menor o igual a cero"
    else:
        pass
    
    _dbRow = dbc01(
        (dbc01.tempresa_invmovto_productos.empresa_inventariomovimiento_id == _s_empresa_inventariomovimiento_id)
        &(dbc01.tempresa_invmovto_productos.empresa_producto_id == O_form.vars.empresa_producto_id)
        &(dbc01.tempresa_invmovto_productos.empresa_producto_id != O_form.vars.record_id)
        ).select(
            dbc01.tempresa_invmovto_productos.id
            )
    if(_dbRow):
        O_form.errors.empresa_producto_id = "El producto no puede repetirse."
    else:
        pass

    return O_form