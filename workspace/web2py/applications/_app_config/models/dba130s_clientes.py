# -*- coding: utf-8 -*-

class TEMPRESA_CLIENTE_GUIASCLIENTES():
    
    @classmethod
    def CONFIGURAR_INTEGRIDAD_REFERENCIAL(cls):
        _dbTable = dbc01.tempresa_cliente_guiasclientes
        _dbTable.monedacontpaqi_id.type = 'reference tempresa_monedascontpaqi'
        _dbTable.monedacontpaqi_id.ondelete = 'NO ACTION'
        _dbTable.monedacontpaqi_id.requires = IS_IN_DB(
            dbc01(dbc01.tempresa_monedascontpaqi.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id), 
            'tempresa_monedascontpaqi.id', 
            dbc01.tempresa_monedascontpaqi._format
            )
        _dbTable.monedacontpaqi_id.widget = lambda f, v: stv_widget_db_chosen(f, v, 1)
        _dbTable.monedacontpaqi_id.represent = stv_represent_referencefield
        
        return    
    
    pass
dbc01.define_table(
    'tempresa_cliente_guiasclientes',
    Field('empresa_cliente_id', dbc01.tempresa_clientes, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Empresa', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('ejercicio', 'integer', default = None,
      required = True, requires = IS_IN_SET(VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id), zero = 0, error_message = ('Selecciona')),
      notnull = False, unique = False,
      widget = stv_widget_combobox, 
      label = ('Ejercicio inicial'), comment = None,
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id))),
    Field('moneda_id', 'integer', default=D_stvSiteHelper.dbconfigs.defaults.moneda_id,
      required=True, requires = IS_IN_DB(db01, 'tmonedas.id', db01.tmonedas._format),
      ondelete='NO ACTION', notnull=True, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label=('Moneda'), comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_cliente_guiasclientes.moneda_id)),
    
    #Este será el campo que se utilice en vez de moneda_id
    Field('monedacontpaqi_id', 'integer', default=D_stvSiteHelper.dbconfigs.defaults.moneda_contpaqi_id,
      required=True,
      ondelete='NO ACTION', notnull=True, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label=('Moneda CONTPAQi'), comment=None,
      writable=True, readable=True,
      represent=lambda v, r:  stv_represent_referencefield(v, r, dbc01.tempresa_cliente_guiasclientes.monedacontpaqi_id)),  
    
    Field('cuenta_cliente', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label=('Cuenta Cliente'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuenta_cliente_complemento', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label=('Cuenta Cliente Complemento'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuenta_anticipo', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label=('Cuenta Anticipo'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuenta_anticipo_complemento', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label=('Cuenta Anticipo Complemento'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    tSignature_dbc01,
    format='%(cuenta_cliente)s',
    singular='Guía Cliente',
    plural='Guías Clientes',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

class TEMPRESA_CLIENTE_CLASIFICACIONES():
    pass
dbc01.define_table(
    'tempresa_cliente_clasificaciones',
    Field('empresa_cliente_id', dbc01.tempresa_clientes, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Empresa', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('empresa_clasificacioncliente_id', dbc01.tempresa_clasificacionesclientes, default=None,
      required=True,
      ondelete='NO ACTION', notnull=True, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label=('Clasificación del cliente'), comment=None,
      writable=True, readable=True,
      represent = stv_represent_referencefield),    
    tSignature_dbc01,
    format='%(id)s',
    singular='Clasificación cliente',
    plural='Clasificaciones cliente',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )
