# -*- coding: utf-8 -*-

D_stvSiteCfg.conf = AppConfig(
    os.path.join(request.folder, '..', D_stvSiteCfg.s_nameConfigApp, 'private', 'appconfig.ini'), reload = True
    )

# Validar y estableces la cuenta_id en la que esta operando el usuario
D_stvFwkCfg.dbRow_cuenta_aplicacion = None
D_stvFwkCfg.dbRow_cuenta = None
D_stvFwkCfg.s_appUrlImagen = None
D_stvFwkCfg.s_appUrlImagenPin = None
D_stvFwkCfg.s_nombreDB_cuenta = "No_definido"

# Se usa para reconstruir los archivos de lógica de las tablas, suponiendo que las tablas estan correctas
D_stvSiteCfg.b_esReconstruirDefinicionesDAL = False

if D_stvFwkCfg.cfg_cuenta_aplicacion_id:
    # Este es el ID preferido para seleccionar la bade de datos
    D_stvFwkCfg.dbRow_cuenta_aplicacion = db.tcuenta_aplicaciones(D_stvFwkCfg.cfg_cuenta_aplicacion_id)
    # TODO verificar que el usuario tiene privilegios en esta aplicación
    
    if D_stvFwkCfg.dbRow_cuenta_aplicacion:
        # Se establecen las imagenes de la aplicacion
        D_stvFwkCfg.s_appUrlImagen = TCUENTA_APLICACIONES.GET_IMAGEN_URL(
            s_value = D_stvFwkCfg.dbRow_cuenta_aplicacion.imagen,
            s_filename = D_stvFwkCfg.dbRow_cuenta_aplicacion.imagennombrearchivo
            ) if D_stvFwkCfg.dbRow_cuenta_aplicacion.imagen else D_stvFwkCfg.s_appUrlImagen

        D_stvFwkCfg.s_appUrlImagenPin = TCUENTA_APLICACIONES.GET_IMAGENPIN_URL(
            s_value = D_stvFwkCfg.dbRow_cuenta_aplicacion.imagenpin,
            s_filename = D_stvFwkCfg.dbRow_cuenta_aplicacion.imagenpinnombrearchivo
            ) if D_stvFwkCfg.dbRow_cuenta_aplicacion.imagenpin else D_stvFwkCfg.s_appUrlImagenPin

        # Si el id de la cuenta es inválido, se usa el de la aplicación
        if not D_stvFwkCfg.cfg_cuenta_id or (D_stvFwkCfg.cfg_cuenta_id == '0'):
            D_stvFwkCfg.cfg_cuenta_id = D_stvFwkCfg.dbRow_cuenta_aplicacion.cuenta_id
        else:
            pass
        
        if not D_stvSiteHelper.dbconfigs.defaults.empresa_id:
            # TODO Mejora validar que el usuario tenga privilegios en la empresa
            D_stvSiteHelper.dbconfigs.defaults.empresa_id = D_stvFwkCfg.dbRow_cuenta_aplicacion.empresa_id
        else:
            pass

    else:
        pass
else:
    pass

if D_stvFwkCfg.cfg_cuenta_id:
    D_stvFwkCfg.dbRow_cuenta = db.tcuentas(D_stvFwkCfg.cfg_cuenta_id)
    if D_stvFwkCfg.dbRow_cuenta:

        if D_stvFwkCfg.dbRow_cuenta_aplicacion \
                and (D_stvFwkCfg.dbRow_cuenta.id != D_stvFwkCfg.dbRow_cuenta_aplicacion.cuenta_id):
            raise RuntimeError("La cuenta asociada y la cuenta aplicación no coinciden.")

        else:
            # Si existe la aplicacion no es espcificada o relación con la cuenta_aplicacion y la cuenta coinciden

            # TODO Verificar que el usuario tiene acceso a esta cuenta
            D_stvFwkCfg.s_nombreDB_cuenta = D_stvFwkCfg.dbRow_cuenta.nombre_db

            # Si no se encontraron las imagenes de la aplicación, se utilizan las de la cuenta
            D_stvFwkCfg.s_appUrlImagen = TCUENTA_APLICACIONES.GET_IMAGEN_URL(
                s_value = D_stvFwkCfg.dbRow_cuenta.imagen, s_filename = D_stvFwkCfg.dbRow_cuenta.imagennombrearchivo
                ) if not D_stvFwkCfg.s_appUrlImagen and D_stvFwkCfg.dbRow_cuenta.imagen \
                else D_stvFwkCfg.s_appUrlImagen

            D_stvFwkCfg.s_appUrlImagenPin = TCUENTA_APLICACIONES.GET_IMAGENPIN_URL(
                s_value = D_stvFwkCfg.dbRow_cuenta.imagenpin,
                s_filename = D_stvFwkCfg.dbRow_cuenta.imagenpinnombrearchivo
                ) if not D_stvFwkCfg.s_appUrlImagenPin and D_stvFwkCfg.dbRow_cuenta.imagenpin \
                else D_stvFwkCfg.s_appUrlImagenPin

    else:
        raise RuntimeError("La cuenta asociada no se reconoce correctamente.")

else:
    pass

db01 = None
dbc01 = None
dbc02 = None


def stv_conectaDAL(s_seccionIni):
    db01 = None
    dbc01 = None
    dbc02 = None
    try:
        _D_seccionIni = D_stvSiteCfg.conf.take(s_seccionIni)
        # uri contiene la conexión para db01
        db01 = DAL(
            _D_seccionIni['uri'],
            pool_size = int(_D_seccionIni['pool_size']), check_reserved = ['all'], db_codec = 'utf-8',
            fake_migrate = D_stvSiteCfg.b_esReconstruirDefinicionesDAL
            )

        if D_stvFwkCfg.s_nombreDB_cuenta:
            # uri_multi contiene la conexión para dbc01 y dbc02
            _s_namedb = _D_seccionIni['database_prefix'] + D_stvFwkCfg.s_nombreDB_cuenta

            dbc01 = DAL(
                _D_seccionIni['uri_multi'] + _s_namedb,
                pool_size = int(_D_seccionIni['pool_size']), check_reserved = ['all'], db_codec = 'utf-8',
                fake_migrate = D_stvSiteCfg.b_esReconstruirDefinicionesDAL
                )

            dbc02 = DAL(
                _D_seccionIni['uri_multi'] + _s_namedb + 'bulk',
                pool_size = int(_D_seccionIni['pool_size']), check_reserved = ['all'], db_codec = 'utf-8',
                fake_migrate = D_stvSiteCfg.b_esReconstruirDefinicionesDAL
                )
        else:
            # No se encuentran definidas bases de datos especificas de la cuenta
            pass

    except:
        request.stv_flashError(
            s_title = "Error de BD",
            s_msg = "No se encontro base de datos de referencia a la cuenta: " + str(D_stvFwkCfg.s_nombreDB_cuenta),
            s_mode = 'stv_flash',
            D_addProperties = {}
            )

        request.vars.update(
            mensaje = "No se encontro base de datos de referencia a la cuenta: " + str(D_stvFwkCfg.s_nombreDB_cuenta)
            )
        redirect(URL(f = 'user', args = ['logout'], vars = request.vars))

    return db01, dbc01, dbc02


if D_stvFwkCfg.b_esServidorPruebas:
    if D_stvSiteCfg.conf.take('p01test_db01.tipo_servidor') != "pruebas":
        raise RuntimeError("Servidor de pruebas o path no estan correctamente configurados")

    else:
        db01, dbc01, dbc02 = stv_conectaDAL("p01test_db01")

elif D_stvFwkCfg.b_esServidorProduccion:
    if D_stvSiteCfg.conf.take('p01_db01.tipo_servidor') != "produccion":
        raise RuntimeError("Servidor de producción o path no estan correctamente configurados")

    else:
        db01, dbc01, dbc02 = stv_conectaDAL("p01_db01")

elif D_stvFwkCfg.b_esServidorDesarrollo:
    if D_stvSiteCfg.conf.take('localhost_db01.tipo_servidor') != "desarrollo":
        raise RuntimeError("Servidor de desarrollo o path no estan correctamente configurados")

    else:
        db01, dbc01, dbc02 = stv_conectaDAL("localhost_db01")

else:
    raise RuntimeError("No se pudo identificar el origen de execución, agregar path al archivo ini")


' Definiendo los campos signature que deberán ser incluidos en todas las tablas '
tSignature_db01 = db01.Table(
    db01, 'signature',
    Field(
        'notas', 'text', default = None,
        required = False,
        notnull = False,
        label = 'Notas', comment = "Notas disponibles para el administrador",
        writable = False, readable = False,
        represent = stv_represent_text
        ),
    Field(
        'bansuspender', 'boolean', default = '0',
        required = True,
        notnull = True,
        label = 'Suspender', comment = None,
        writable = False, readable = True,
        represent = stv_represent_boolean
        ),
    Field(
        'baneliminar', 'boolean', default = '0',
        required = True,
        notnull = True,
        label = 'Borrado', comment = 'Bandera que identifica el registro como eliminado',
        writable = False, readable = False,
        represent = stv_represent_boolean
        ),
    Field(
        'creado_en', type = TDATETIME_GRABADIRECTO_LEERENZONAH, default = request.utcnow,
        required = True, requires = IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME),
        notnull = True,
        label = 'Creado en', comment = 'Fecha/hora cuando el registro fue creado',
        writable = False, readable = False,
        represent = stv_represent_datetime
        ),
    Field(
        'creado_por', 'integer', default = auth.user_id or 1,
        required = True,
        notnull = True,
        label = 'Creado Por', comment = 'Usuario que creo el registro',
        writable = False, readable = False,
        represent = stv_represent_userAsText
        ),
    Field(
        'editado_en', type = TDATETIME_GRABADIRECTO_LEERENZONAH, default = request.utcnow, update = request.utcnow,
        required = True, requires = IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME),
        notnull = True,
        label = 'Editado el', comment = 'Fecha/hora cuando el registro fue editado por última vez',
        writable = False, readable = False,
        represent = stv_represent_datetime
        ),
    Field(
        'editado_por', 'integer', update = auth.user_id or 1,
        required = True,
        notnull = True,
        label = 'Editado por', comment = 'Ultimo usuario que edito el registro',
        writable = False, readable = False,
        represent = stv_represent_userAsText
        ),
    )

if dbc01:
    ' Definiendo los campos signature que deberán ser incluidos en todas las tablas '
    tSignature_dbc01 = dbc01.Table(
        dbc01, 'signature',
        Field(
            'notas', 'text', default = None,
            required = False,
            notnull = False,
            label = 'Notas', comment = "Notas disponibles para el administrador",
            writable = False, readable = False,
            represent = stv_represent_text
            ),
        Field(
            'bansuspender', 'boolean', default = '0',
            required = True,
            notnull = True,
            label = 'Suspender', comment = None,
            writable = False, readable = True,
            represent = stv_represent_boolean
            ),
        Field(
            'baneliminar', 'boolean', default = '0',
            required = True,
            notnull = True,
            label = 'Borrado', comment = 'Bandera que identifica el registro como eliminado',
            writable = False, readable = False,
            represent = stv_represent_boolean
            ),
        Field(
            'creado_en', type = TDATETIME_GRABADIRECTO_LEERENZONAH, default = request.utcnow,
            required = True, requires = IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME),
            notnull = True,
            label = 'Creado en', comment = 'Fecha/hora cuando el registro fue creado',
            writable = False, readable = False,
            represent = stv_represent_datetime
            ),
        Field(
            'creado_por', 'integer', default = auth.user_id or 1,
            required = True,
            notnull = True,
            label = 'Creado Por', comment = 'Usuario que creo el registro',
            writable = False, readable = False,
            represent = stv_represent_userAsText
            ),
        Field(
            'editado_en', type = TDATETIME_GRABADIRECTO_LEERENZONAH, default = request.utcnow, update = request.utcnow,
            required = True, requires = IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME),
            notnull = True,
            label = 'Editado el', comment = 'Fecha/hora cuando el registro fue editado por última vez',
            writable = False, readable = False,
            represent = stv_represent_datetime
            ),
        Field(  # TODO poder asignar el scheduler como usuario y usarlo en este caso
            'editado_por', 'integer', update = auth.user_id or 1,
            required = True,
            notnull = True,
            label = 'Editado por', comment = 'Ultimo usuario que edito el registro',
            writable = False, readable = False,
            represent = stv_represent_userAsText
            ),
        )
else:
    pass

if dbc02:
    ' Definiendo los campos signature que deberán ser incluidos en todas las tablas '
    tSignature_dbc02 = dbc01.Table(
        dbc02, 'signature',
        Field(
            'notas', 'text', default = None,
            required = False,
            notnull = False,
            label = 'Notas', comment = "Notas disponibles para el administrador",
            writable = False, readable = False,
            represent = stv_represent_text
            ),
        Field(
            'bansuspender', 'boolean', default = '0',
            required = True,
            notnull = True,
            label = 'Suspender', comment = None,
            writable = False, readable = True,
            represent = stv_represent_boolean
            ),
        Field(
            'baneliminar', 'boolean', default = '0',
            required = True,
            notnull = True,
            label = 'Borrado', comment = 'Bandera que identifica el registro como eliminado',
            writable = False, readable = False,
            represent = stv_represent_boolean
            ),
        Field(
            'creado_en', type = TDATETIME_GRABADIRECTO_LEERENZONAH, default = request.utcnow,
            required = True, requires = IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME),
            notnull = True,
            label = 'Creado en', comment = 'Fecha/hora cuando el registro fue creado',
            writable = False, readable = False,
            represent = stv_represent_datetime
            ),
        Field(
            'creado_por', 'integer', default = auth.user_id or 1,
            required = True,
            notnull = True,
            label = 'Creado Por', comment = 'Usuario que creo el registro',
            writable = False, readable = False,
            represent = stv_represent_userAsText
            ),
        Field(
            'editado_en', type = TDATETIME_GRABADIRECTO_LEERENZONAH, default = request.utcnow, update = request.utcnow,
            required = True, requires = IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME),
            notnull = True,
            label = 'Editado el', comment = 'Fecha/hora cuando el registro fue editado por última vez',
            writable = False, readable = False,
            represent = stv_represent_datetime
            ),
        Field(
            'editado_por', 'integer', update = auth.user_id or 1,
            required = True,
            notnull = True,
            label = 'Editado por', comment = 'Ultimo usuario que edito el registro',
            writable = False, readable = False,
            represent = stv_represent_userAsText
            ),
        )
else:
    pass
