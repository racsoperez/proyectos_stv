# -*- coding: utf-8 -*-
        
class STV_FWK_POLIZA:
    _s_empresa_id = None  # Empresa
    
    def __init__(self, s_empresa_id):
        self._inicializar_variables()
        self._s_empresa_id = s_empresa_id
        return
    
    def _inicializar_variables(self):
        self._L_msgError = []  # Errores que no permiten y cancelan la creación de la Poliza
        self._s_aniocfdi = None  # Ejercicio correspondiente a la poliza, prepoliza y al CFDI
        self._dbRowPrepoliza = None  # Referencia a la información de la prepoliza
        self._D_contabilidad = None  # Referencia a la configuración contable de la sucursal
        self._n_poliza_id = None  # Referencial al Id del maestro de la poliza
        self._s_proceso = None  # String que determina el proceso que se esta ejecutando, usado para el try catch
        self._dt_fecha = None # Fecha de la póliza. 
        return    
    
    def agregar_prepoliza(self, s_prepoliza_id, b_esCancelado):
        """ Agrega la prepoliza a la poliza dependiendo de la configuración de la empresa
        
        Args:
            s_prepoliza_id: ID de la prepóliza.
            b_esCancelado: Valor booleando que indica que la prepoliza es de un CFDI cancelado o vigente.
        
        """
        self._inicializar_variables()        
        
        _D_return = Storage(
            L_msgError = [],
            E_return = CLASS_e_RETURN.NOK_ERROR,
            n_poliza_id = 0
            )
        
        self._s_prepoliza_id = s_prepoliza_id
        
        try:
        
            self._s_proceso = "Se obtiene información de la Prepoliza"
            _dbRowsPrepoliza = dbc01(
                (dbc01.tcfdi_prepolizas.id == self._s_prepoliza_id)
                & (dbc01.tcfdis.id == dbc01.tcfdi_prepolizas.cfdi_id)
                & (dbc01.tempresa_plaza_sucursal_cajaschicas.id == dbc01.tcfdis.empresa_plaza_sucursal_cajachica_id)
                & (dbc01.tempresa_plaza_sucursales.id == dbc01.tempresa_plaza_sucursal_cajaschicas.empresa_plaza_sucursal_id)
                & (dbc01.tcfdi_prepolizas.sat_status_cancelacion == (
                    TCFDI_PREPOLIZAS.SAT_STATUS.CANCELADO if b_esCancelado else TCFDI_PREPOLIZAS.SAT_STATUS.VIGENTE
                    )
                )
                & (dbc01.tempresa_plazas.id == dbc01.tempresa_plaza_sucursales.empresa_plaza_id)
                ).select(
                    dbc01.tcfdi_prepolizas.ALL,
                    dbc01.tcfdis.ALL,
                    dbc01.tcfdi_complementopagos.ALL,
                    dbc01.tempresa_plaza_sucursal_cajaschicas.ALL,
                    dbc01.tempresa_plaza_sucursales.ALL,
                    dbc01.tempresa_plazas.ALL,
                    left = [
                        dbc01.tcfdi_complementopagos.on(
                            dbc01.tcfdi_complementopagos.cfdi_id == dbc01.tcfdis.id
                            )
                        ],
                    )
                
            if len(_dbRowsPrepoliza) == 0:
                self._L_msgError.append("CFDI de Prepoliza no tiene caja chica identificada")
            elif len(_dbRowsPrepoliza) > 1:
                self._L_msgError.append("El CFDI esta relacionado con más de una Prepoliza, contacte al administrador")
            else:
                self._dbRowPrepoliza = _dbRowsPrepoliza.last()
                self._s_aniocfdi = str(self._dbRowPrepoliza.tcfdis.fecha.year)
                
                if self._dbRowPrepoliza.tcfdis.etapa not in (TCFDIS.E_ETAPA.CON_PREPOLIZA, TCFDIS.E_ETAPA.CON_POLIZA):
                    self._L_msgError.append("CFDI de Prepoliza no tiene etapa CON PREPOLIZA")
                elif not self._dbRowPrepoliza.tcfdi_prepolizas.empresa_tipopoliza_id:
                    self._L_msgError.append("Prepoliza no tiene el tipo de poliza definido")
                elif self._dbRowPrepoliza.tcfdi_prepolizas.poliza_id:
                    self._L_msgError.append("Prepoliza ya se encuentra asociada a poliza %d" % self._dbRowPrepoliza.tcfdi_prepolizas.poliza_id)
                elif str(self._s_empresa_id) != str(self._dbRowPrepoliza.tcfdis.empresa_id):
                    self._L_msgError.append("Empresa definida en CFDI no corresponde a empresa definida para la Poliza")                
                else:
                
                    self._s_proceso = "Se obtiene información de la Contabilidad de la sucursal"
                    #Se consulta el estilo de póliza que se está manejando.
                    _dbTable = dbc01.tempresa_plaza_sucursal_contabilidades
                
                    _dbQry = (_dbTable.empresa_plaza_sucursal_id == self._dbRowPrepoliza.tempresa_plaza_sucursal_cajaschicas.empresa_plaza_sucursal_id)
                        
                    self._D_contabilidad = self._getCamposPorEjercicio(
                        s_aniocfdi = self._s_aniocfdi,
                        dbTable = _dbTable,
                        dbQry = _dbQry, 
                        D_regresarCampos = Storage(
                            estilopoliza = 'estilopoliza',
                            tipopoliza_ingreso_id = 'empresa_tipopoliza_ingreso_id',
                            tipopoliza_pago_id = 'empresa_tipopoliza_pago_id',
                            tipopoliza_egreso_id = 'empresa_tipopoliza_egreso_id',
                            diario_id = 'empresa_diario_id',
                            concepto_ingreso = 'concepto_ingreso',
                            concepto_pago = 'concepto_pago',
                            concepto_egreso = 'concepto_egreso',
                            )
                        )
                    
                    if self._D_contabilidad:
                        
                        if(self._dbRowPrepoliza.tcfdis.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO):
                            self._D_contabilidad.tipopoliza_id = self._D_contabilidad.tipopoliza_ingreso_id
                            self._D_contabilidad.concepto = self._D_contabilidad.concepto_ingreso
                            if self._dbRowPrepoliza.tcfdi_prepolizas.tipo_contabilizacion_fecha_cancelacion == TCFDI_PREPOLIZAS.TIPO_CONTABILIZACION_FECHA_CANCELACION.EMISION:
                                self._dt_fecha = self._dbRowPrepoliza.tcfdis.fecha
                            else:
                                self._dt_fecha = self._dbRowPrepoliza.tcfdis.sat_fecha_cancelacion
                            
                        elif(self._dbRowPrepoliza.tcfdis.tipocomprobante_id == TTIPOSCOMPROBANTES.PAGO):
                            self._D_contabilidad.tipopoliza_id = self._D_contabilidad.tipopoliza_pago_id
                            self._D_contabilidad.concepto = self._D_contabilidad.concepto_pago
                            self._dt_fecha = self._dbRowPrepoliza.tcfdi_complementopagos.fechapago
            
                        elif(self._dbRowPrepoliza.tcfdis.tipocomprobante_id == TTIPOSCOMPROBANTES.EGRESO):
                            self._D_contabilidad.tipopoliza_id = self._D_contabilidad.tipopoliza_egreso_id
                            self._D_contabilidad.concepto = self._D_contabilidad.concepto_egreso
                            self._dt_fecha = self._dbRowPrepoliza.tcfdis.fecha
                             
                        else:
                            self._D_contabilidad.tipopoliza_id = 0
                            self._L_msgError.append("Tipo de comprobante en CFDI no identificado")
                            
                        if (self._D_contabilidad.tipopoliza_id != self._dbRowPrepoliza.tcfdi_prepolizas.empresa_tipopoliza_id):
                            self._L_msgError.append("Tipo de poliza inconsistente entre configuración actual y definido en prepoliza")
                        else:
                            # Todo parece ir bien
                            pass
                                            
                    else:
                        self._L_msgError.append("No se encontro información contable para la sucursal de la caja chica")
                    
            if not self._L_msgError:
                # Si no existen errores
                
                self._D_substituciones = Storage(
                    Ejercicio= self._s_aniocfdi,
                    Periodo= str(self._dbRowPrepoliza.tcfdis.fecha.month).zfill(2),
                    Mes= TGENERICAS.MES.D_TODOS[self._dbRowPrepoliza.tcfdis.fecha.month],
                    Dia= str(self._dbRowPrepoliza.tcfdis.fecha.day).zfill(2),
                    Plaza= str(self._dbRowPrepoliza.tempresa_plazas.nombrecorto),
                    Sucursal= str(self._dbRowPrepoliza.tempresa_plaza_sucursales.nombrecorto),
                    Cajachica= str(self._dbRowPrepoliza.tempresa_plaza_sucursal_cajaschicas.nombre),
                    CFDI= str(self._dbRowPrepoliza.tcfdis.serie) + str(self._dbRowPrepoliza.tcfdis.folio),                
                    )
                
                _dbRowsAsientos = dbc01(
                    (dbc01.tcfdi_prepoliza_asientoscontables.cfdi_prepoliza_id == self._s_prepoliza_id)
                    ).select(
                        dbc01.tcfdi_prepoliza_asientoscontables.poliza_id,
                        groupby = [
                            dbc01.tcfdi_prepoliza_asientoscontables.poliza_id
                            ]
                        )
    
                self._n_poliza_id = None
                    
                if self._D_contabilidad.estilopoliza == TEMPRESA_PLAZA_SUCURSAL_CONTABILIDADES.ESTILOPOLIZA.POR_CFDI:
                    
                    if len(_dbRowsAsientos) > 0:
                        # Si tiene mas de un asiento, se inserta el maestro de la poliza y se genera el n_poliza_id
                        _D_result = self._insertHeader(
                            n_estatus = TPOLIZAS.ESTATUS.EN_PROCESO
                            )
                                            
                    else:
                        self._L_msgError.append("No se identificaron asientos en la prepoliza")
                        
                elif self._D_contabilidad.estilopoliza == TEMPRESA_PLAZA_SUCURSAL_CONTABILIDADES.ESTILOPOLIZA.ACUMULADA_DESGLOZE_POR_CFDI:
                    
                    self._s_proceso = "Se obtiene la poliza de diario, si existe"
                    # Se verifica si ya existe una poliza de diario
                    _dbRowsPolizaDiaria = dbc01(
                        (dbc01.tpolizas.empresa_plaza_sucursal_cajachica_id == self._dbRowPrepoliza.tempresa_plaza_sucursal_cajaschicas.id)
                        & (dbc01.tpolizas.empresa_tipopoliza_id == self._D_contabilidad.tipopoliza_id)
                        & (dbc01.tpolizas.estatus == TPOLIZAS.ESTATUS.EN_PROCESO)
                        & (dbc01.tpolizas.dia == self._dt_fecha.date())
                        ).select(
                            dbc01.tpolizas.ALL,
                            orderby = [
                                dbc01.tpolizas.id
                                ]
                            )
                        
                    if not _dbRowsPolizaDiaria:
                        # Si no existe una poliza ya, se inserta el maestro de la poliza y se genera el n_poliza_id
                        _D_result = self._insertHeader(
                            n_estatus = TPOLIZAS.ESTATUS.EN_PROCESO
                            )
                        
                    else:
                        # de lo contrario se utiliza el último registro que coincida con la poliza
                        self._n_poliza_id = _dbRowsPolizaDiaria.last().id
                        
                else:
                    self._L_msgError.append("Estilo de poliza %d no soportado" % self._D_contabilidad.estilopoliza)
                        
                if self._n_poliza_id:
                    
                    _dbRowEmpresa = dbc01.tempresas(self._s_empresa_id)

                    self._actualizar_estatus(
                        s_prepoliza_id = self._s_prepoliza_id, 
                        s_cfdi_id = self._dbRowPrepoliza.tcfdis.id, 
                        s_poliza_id = self._n_poliza_id, 
                        n_cfdi_etapa = TCFDIS.E_ETAPA.CON_POLIZA,
                        b_limpiarAsientoContablePoliza = False,
                        dbRowEmpresa = _dbRowEmpresa,
                        b_esCancelado = b_esCancelado
                        )
  
                                        
                    _D_return.E_return = CLASS_e_RETURN.OK
                        
                else:
                    self._L_msgError.append("No se pudo generar el maestro de la poliza")
                    
        except Exception as _O_excepcion:
            _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
            self._L_msgError.append('Proceso %s; Excepcion: %s' % (self._s_proceso, str(_O_excepcion)))
            
            # Se regresa cualquier inserción a la base de datos dbc01
            dbc01.rollback()
        
        _D_return.L_msgError = self._L_msgError
        _D_return.n_poliza_id = self._n_poliza_id
        
        return _D_return
        
    def agregar_prepolizas_porFecha(self, d_fecha, s_cajachica_id = None):
        """ Agrega prepoliza de cierta fecha a polizas dependiendo de la configuración de la empresa
        
                Args:
                    d_fecha:
                    s_cajachica_id: ID de la caja chica.
        
        """
        return

    def agregar_prepolizas_porCajaChica(self, s_cajachica_id):
        """ Agrega prepoliza de cierta cajachica a polizas dependiendo de la configuración de la empresa
        
                Args:
                    s_cajachica_id: ID de la caja chica.
        
        """
        return
        
    def borrar(self, s_poliza_id):
        """ Borrar la poliza y sus asociaciones en las prepolizas, cambiando el estado de los CFDIs relacionados
       
               Args:
                   s_poliza_id: ID de la póliza.
                
        """
        self._inicializar_variables()   
        
        _D_return = Storage(
            L_msgError = [],
            E_return = CLASS_e_RETURN.NOK_ERROR,
            )
        
        _dbRowPoliza = dbc01.tpolizas(s_poliza_id)
            
        if not(_dbRowPoliza.estatus in (TPOLIZAS.ESTATUS.NO_DEFINIDO, TPOLIZAS.ESTATUS.EN_PROCESO, TPOLIZAS.ESTATUS.SIN_EXPORTAR)):
            self._L_msgError.append("Poliza ya se encuentra en estado [%s] que no permite eliminar Polizas" % TPOLIZAS.ESTATUS.get_dict()[_dbRowPoliza.estatus])
        else:
            
            _dbRowsCFDI = dbc01(
                (dbc01.tcfdi_prepolizas.poliza_id == s_poliza_id)
                ).select(
                    dbc01.tcfdi_prepolizas.ALL
                    )
                
            for _dbRowCFDI in _dbRowsCFDI:

                if _dbRowCFDI.sat_status_cancelacion == TCFDI_PREPOLIZAS.SAT_STATUS.CANCELADO:
                    dbc01.tcfdis(
                        _dbRowCFDI.cfdi_id
                        ).update_record(
                            etapa_cancelado = TCFDIS.E_ETAPA.SIN_CONTABILIZAR
                        )
                else:
                    dbc01.tcfdis(
                        _dbRowCFDI.cfdi_id
                        ).update_record(
                            etapa = TCFDIS.E_ETAPA.SIN_CONTABILIZAR
                        )

            dbc01(
                dbc01.tcfdi_prepolizas.poliza_id == s_poliza_id
                ).update(
                    poliza_id = None
                    )
                
            dbc01(
                dbc01.tcfdi_prepoliza_asientoscontables.poliza_id == s_poliza_id
                ).update(
                    poliza_id = None,
                    asiento_contable_poliza = None,
                    )
                
            dbc01(dbc01.tpolizas.id == s_poliza_id).delete()
                            
            _D_return.E_return = CLASS_e_RETURN.OK
        
        _D_return.L_msgError = self._L_msgError
        
        return _D_return

    def borrar_cfdi(self, s_cfdi_id):
        """ Borrar la poliza y sus asociaciones en las prepolizas, cambiando el estado de los CFDIs relacionados
        
            Args:
                s_cfdi_id: ID del CFDI
        """
        
        self._inicializar_variables()   
        
        _D_return = Storage(
            L_msgError = [],
            E_return = CLASS_e_RETURN.NOK_ERROR,
            )
        
        _dbRowsPrepoliza = dbc01(
            (dbc01.tcfdi_prepolizas.cfdi_id == s_cfdi_id)
            & (dbc01.tpolizas.id == dbc01.tcfdi_prepolizas.poliza_id)
            ).select(
                dbc01.tcfdi_prepolizas.ALL,
                dbc01.tpolizas.ALL
                )

        contadorPrepolizaCancelada = 0
        contadorPrepolizaIngreso = 0

        _n_prepolizasEnPoliza = 0

        _dbRowPrepolizaCancelada = None
        _dbRowPrepolizaIngreso = None

        for _dbRowPrepoliza in _dbRowsPrepoliza:
            if _dbRowPrepoliza.tcfdi_prepolizas.sat_status_cancelacion == TCFDI_PREPOLIZAS.SAT_STATUS.CANCELADO:
                _dbRowPrepolizaCancelada = _dbRowPrepoliza
                contadorPrepolizaCancelada = +1
            else:
                _dbRowPrepolizaIngreso = _dbRowPrepoliza
                contadorPrepolizaIngreso = +1
            
        if len(_dbRowsPrepoliza) == 0:
            _D_return._L_msgError.append("CFDI no tiene Prepoliza")
        elif (contadorPrepolizaIngreso > 1) or (contadorPrepolizaCancelada > 1):
            _D_return._L_msgError.append("CFDI esta relacionada con varias prepolizas canceladas o de ingreso. "
                                         "Contacte al administrador.")
        else:

            _dbRowPrepoliza = _dbRowsPrepoliza.last()

            if not(_dbRowPrepoliza.tpolizas.estatus in (TPOLIZAS.ESTATUS.NO_DEFINIDO, TPOLIZAS.ESTATUS.EN_PROCESO, TPOLIZAS.ESTATUS.SIN_EXPORTAR)):
                _D_return._L_msgError.append("Poliza ya se encuentra en estado [%s] que no permite eliminar Polizas" % TPOLIZAS.ESTATUS.get_dict()[_dbRowPrepoliza.tpolizas.estatus])
            else:

                _n_prepolizasEnPoliza = dbc01(
                    dbc01.tcfdi_prepolizas.poliza_id == _dbRowPrepoliza.tpolizas.id
                    ).count()

                if _n_prepolizasEnPoliza == 1:
                    # Se puede borrar la poliza ya que es el único registro en la Poliza

                    if _dbRowPrepolizaCancelada:
                        if _dbRowPrepolizaCancelada.tcfdi_prepolizas.tipo_contabilizacion_fecha_cancelacion == TCFDI_PREPOLIZAS.TIPO_CONTABILIZACION_FECHA_CANCELACION.CANCELACION:

                            _n_prepolizasEnPolizaCancelacion = dbc01(
                                dbc01.tcfdi_prepolizas.poliza_id == _dbRowPrepolizaCancelada.tpolizas.id,
                                ).count()

                            if _n_prepolizasEnPolizaCancelacion == 1:
                                _D_return_cancelado = self.borrar(_dbRowPrepolizaCancelada.tpolizas.id)
                            else:

                                self._actualizar_estatus(
                                    _dbRowPrepolizaCancelada.tcfdi_prepolizas.id,
                                    _dbRowPrepolizaCancelada.tcfdi_prepolizas.cfdi_id,
                                    s_poliza_id = None,
                                    n_cfdi_etapa = TCFDIS.E_ETAPA.SIN_CONTABILIZAR,
                                    b_limpiarAsientoContablePoliza = True,
                                    dbRowEmpresa = None,
                                    b_esCancelado = b_cancelado
                                    )
                        else:
                            pass

                        _D_return = self.borrar(_dbRowPrepolizaIngreso.tpolizas.id)
                    else:
                        _D_return = self.borrar(_dbRowPrepoliza.tpolizas.id)

                else:
                    # Si existes mas de una prepoliza asociada al CFDI, solo se elimina la relación
                    for _dbRowPrepoliza in _dbRowsPrepoliza:
                        if _dbRowPrepoliza.tcfdi_prepolizas.sat_status_cancelacion == TCFDI_PREPOLIZAS.SAT_STATUS.CANCELADO:
                            b_cancelado = True
                        else:
                            b_cancelado = False

                        self._actualizar_estatus(
                            _dbRowPrepoliza.tcfdi_prepolizas.id,
                            _dbRowPrepoliza.tcfdi_prepolizas.cfdi_id,
                            s_poliza_id = None,
                            n_cfdi_etapa = TCFDIS.E_ETAPA.SIN_CONTABILIZAR,
                            b_limpiarAsientoContablePoliza = True,
                            dbRowEmpresa = None,
                            b_esCancelado = b_cancelado
                            )

                    _D_return.E_return = CLASS_e_RETURN.OK
        
        return _D_return

    def exportar(self, s_poliza_id):
        """ Genera el archivo de texto para exportar la poliza y cambia el estado de los CFDIs relacionados asi como de la poliza
            
                Args:
                    s_poliza_id: ID de la póliza
        """
        
        self._inicializar_variables()   
        
        _D_return = Storage(
            L_msgError = [],
            E_return = CLASS_e_RETURN.NOK_ERROR,
            )  
        
        # Se selecciona la póliza para ver si ya tiene un archivo TXT generado para poder cambiar su estatus a exportado.
        _dbRowsPoliza_txt = dbc01(
            (dbc01.tpolizas.id == s_poliza_id)
            & (
                (dbc01.tpolizas.archivopoliza != None)
                & (dbc01.tpolizas.archivopoliza != "")
                )
            ).select(dbc01.tpolizas.archivopoliza)
        
        if not(_dbRowsPoliza_txt):
            _D_return.L_msgError = "Póliza no tiene generado TXT para poder exportarse"            
        else:
            try:
                #Se actualiza el estatus de la póliza
                dbc01.tpolizas(s_poliza_id).update_record(
                    estatus = TPOLIZAS.ESTATUS.EXPORTADO
                    )
            except:
                _D_return.L_msgError = "No se pudo exportar la póliza"
    
        return(_D_return)
    
    
    def cerrar(self, s_poliza_id):
        """ Genera el archivo de texto para exportar la poliza y cambia el estado de los CFDIs relacionados asi como de la poliza
        
                Args: 
                    s_poliza_id: ID de la póliza
        """
        
        self._inicializar_variables()   
        
        _D_return = Storage(
            L_msgError = [],
            E_return = CLASS_e_RETURN.NOK_ERROR,
            )  
        
        if(s_poliza_id):
            _dbRowPoliza_cfdis = dbc01(
                dbc01.tcfdi_prepolizas.poliza_id == s_poliza_id
                ).select(
                    dbc01.tcfdi_prepolizas.cfdi_id
                    )
            
            if not(_dbRowPoliza_cfdis):
                _D_return._L_msgError = "No se pudo actualizar el estado debido a que no tiene CFDIs anidados esta póliza"
                #No tiene CFDIS relacionados, no se puede generar la póliza.
                pass
            else:
                
                # Se actualizan los asientos de la póliza
            
                _n_num_asiento = 0
                
                # Se buscan todos los asientos de la póliza, incluyendo el mas reciente.     
                _dbRowsAsientosPoliza = dbc01(dbc01.tcfdi_prepoliza_asientoscontables.poliza_id == s_poliza_id
                    ).select(dbc01.tcfdi_prepoliza_asientoscontables.ALL,
                             orderby =[dbc01.tcfdi_prepoliza_asientoscontables.referencia,
                                       dbc01.tcfdi_prepoliza_asientoscontables.id])
                
                # Se barren uno por uno y se van creando. Cada que se agregue uno se recalcula. Para evitar problemas.        
                for _dbRowAsientoPoliza in _dbRowsAsientosPoliza:
                    _n_num_asiento += 1
                    _dbRowAsientoPoliza.update_record(
                        asiento_contable_poliza = str(_n_num_asiento).zfill(4),
                        )
                
                #Se actualiza el estatus de la póliza
                dbc01.tpolizas(s_poliza_id).update_record(
                    estatus = TPOLIZAS.ESTATUS.SIN_EXPORTAR
                    )
                _D_return.E_return = CLASS_e_RETURN.OK
        else:
            _D_return._L_msgError = "No se mandó una póliza"
                
        return(_D_return)
        
        
    def generar_txt(self, s_poliza_id):
        """ Genera el archivo de texto para exportar la poliza y cambia el estado de los CFDIs relacionados asi como de la poliza
        
                Args: 
                    s_poliza_id: ID de la póliza
        """
        
        self._inicializar_variables()   
        
        import tempfile
        import time
        
        _D_return = Storage(
            L_msgError = [],
            E_return = CLASS_e_RETURN.NOK_ERROR,
            )    
        _s_msgError = ""

        try:        
            
            (nFileDescriptor, sPathFilename) = tempfile.mkstemp('.txt', 'stv_poliza_')
                                
            oDestFile = os.fdopen(nFileDescriptor, 'w')
            
            #TODO asegurarse de que los numeros de asiento contable poliza en los asientos esta correctamente generado   
            
            _dbTableDiarioAsiento = dbc01.tempresa_diarios.with_alias('tempresa_diarios_porasiento')
                        
            _s_msgError = "Consulta de información"
            
            #Variables para la suma de los cargos y abonos
            _n_cargos = 0
            _n_abonos = 0
            
            #Se consiguen todas las prepolizas anidadas a la póliza ordenadas por su referencia. 
            _dbRowsPoliza = dbc01(
                (dbc01.tcfdi_prepoliza_asientoscontables.poliza_id == s_poliza_id)
                & (dbc01.tpolizas.id == s_poliza_id)
                & (dbc01.tempresa_tipospoliza.id == dbc01.tpolizas.empresa_tipopoliza_id)
                & (dbc01.tempresa_diarios.id == dbc01.tpolizas.empresa_diario_id)
                ).select(
                    dbc01.tpolizas.ALL,
                    dbc01.tcfdi_prepoliza_asientoscontables.ALL,
                    dbc01.tempresa_tipospoliza.id,
                    dbc01.tempresa_tipospoliza.codigo,
                    dbc01.tempresa_diarios.codigo,
                    dbc01.tempresa_diarios.verificacioncontpaqi,
                    dbc01.tcfdis.ALL,
                    dbc01.tcfdi_registropagos.ALL,
                    _dbTableDiarioAsiento.codigo,
                    _dbTableDiarioAsiento.verificacioncontpaqi,
                    dbc01.tempresa_segmentos.codigo,
                    dbc01.tempresa_segmentos.verificacioncontpaqi,
                    dbc01.tempresa_bancosdiariosespeciales.bancos_tipo_documentos_venta_contado,
                    dbc01.tempresa_clientes.cliente_codigo_contpaqi,
                    dbc01.tcfdi_complementopagos.ALL,
                    dbc01.tempresa_tipopoliza_ejercicios.clasepoliza,
                    dbc01.tempresa_tipopoliza_ejercicios.impresionpoliza,
                    dbc01.tempresa_bancoscontpaqi.codigo_contpaqi,
                    dbc01.tempresa_bancoscontpaqi.banco_id,
                    dbc01.tempresa_monedascontpaqi.moneda_contpaqi,
                    orderby= [
                        dbc01.tcfdi_prepoliza_asientoscontables.tipocreacion,
                        dbc01.tcfdi_prepoliza_asientoscontables.referencia,
                        dbc01.tcfdi_prepoliza_asientoscontables.id,
                        ],
                    left = [
                        dbc01.tcfdi_prepolizas.on(
                            dbc01.tcfdi_prepolizas.id == dbc01.tcfdi_prepoliza_asientoscontables.cfdi_prepoliza_id
                            ),
                        dbc01.tcfdis.on(
                            dbc01.tcfdis.id == dbc01.tcfdi_prepolizas.cfdi_id
                            ),                    
                        _dbTableDiarioAsiento.on(
                            _dbTableDiarioAsiento.id == dbc01.tcfdi_prepoliza_asientoscontables.empresa_diario_id
                            ),
                        dbc01.tempresa_segmentos.on(
                            dbc01.tempresa_segmentos.id == dbc01.tcfdi_prepoliza_asientoscontables.empresa_segmento_id
                            ),
                        dbc01.tcfdi_registropagos.on(
                            dbc01.tcfdi_registropagos.cfdi_id == dbc01.tcfdis.id
                            ),
                        #Este tabla solo tiene un registro por empresa ya que es de configuración.
                        dbc01.tempresa_bancosdiariosespeciales.on(
                            dbc01.tempresa_bancosdiariosespeciales.empresa_id == dbc01.tcfdis.empresa_id
                            ),
                        dbc01.tempresa_clientes.on(
                            dbc01.tempresa_clientes.id == dbc01.tcfdis.receptorcliente_id
                            ),
                        dbc01.tcfdi_complementopagos.on(
                            dbc01.tcfdi_complementopagos.cfdi_id == dbc01.tcfdis.id
                            ),
                        dbc01.tempresa_tipopoliza_ejercicios.on(
                            (dbc01.tempresa_tipopoliza_ejercicios.empresa_tipopoliza_id == dbc01.tempresa_tipospoliza.id)
                            &(dbc01.tempresa_tipopoliza_ejercicios.ejercicio == dbc01.tpolizas.dia.year())
                            ),
                        dbc01.tempresa_bancoscontpaqi.on(
                            (dbc01.tempresa_bancoscontpaqi.id == dbc01.tcfdi_registropagos.bancocontpaqi_id_cliente)
                            ),
                        dbc01.tempresa_monedascontpaqi.on(
                            (dbc01.tempresa_monedascontpaqi.empresa_id == dbc01.tcfdis.empresa_id)
                            &(dbc01.tempresa_monedascontpaqi.moneda_id == dbc01.tcfdis.moneda_id)
                            )
                        ],
                    ) 
                               
            if not (_dbRowsPoliza):
                _D_return.L_msgError.append("Referencia a poliza y/o tipo poliza no existe")
            else:
                            
                _s_msgError = "POLIZA"
                
                # Se manda a llamar la función para obtener el folio dependiendo el periodo y ejercicio
                _D_controlFolio = self._get_siguienteFolio(_dbRowsPoliza[0].tpolizas.fecha, _dbRowsPoliza[0].tempresa_tipospoliza.id)
                if (_D_controlFolio.L_msgError):
                    _D_return.L_msgError += _D_controlFolio.L_msgError
                else:
                    _n_folio = _D_controlFolio.n_folio
                    _L_listaDeUUIDs = []
                    _n_ultimoCFDIusado = None
         
                    _n_ultimoAsientousado = None
                    
                    #Se barren una por una las prepolizas
                    for _dbRowPoliza in _dbRowsPoliza:
                         
                        if _n_ultimoAsientousado != _dbRowPoliza.tcfdi_prepoliza_asientoscontables.id:
                            _n_ultimoAsientousado = _dbRowPoliza.tcfdi_prepoliza_asientoscontables.id
                            
                            _s_informacionTXT = ''
                            # Siempre que sea el primer asiento contable se agregará el encabezado
                            if (not _n_ultimoCFDIusado):
                                
                                # Se hace un salto inicial para evitar problemas en el cargado con el CONTPAQi
                                _s_informacionTXT += '\r\n'
                                # Póliza (Siempre va una P)
                                _s_msgError = "POLIZA"
                                _s_informacionTXT += ('P').ljust(3)
                
                                # Fecha 8 char
                                _s_msgError = "Fecha inválida"
                                _s_informacionTXT += (_dbRowPoliza.tpolizas.fecha.strftime("%Y%m%d")).ljust(8)
                                
                                # Tipo de póliza 4 char
                                _s_msgError = "Tipo de póliza inválida"
                                _s_informacionTXT += (str(_dbRowPoliza.tempresa_tipospoliza.codigo)).rjust(5)
                                               
                                # Folio poliza 9 char 
                                _s_msgError = "Folio de la póliza inválido"
                                _s_informacionTXT += (str(_n_folio)).rjust(10)
                                
                                # Clase 1 char
                                _s_msgError = "Clase inválida"
                                _s_informacionTXT += ' '+((str(_dbRowPoliza.tempresa_tipopoliza_ejercicios.clasepoliza)).ljust(2))
                                
                                # Diario especial 11 chars
                                _s_msgError = "Diario especial inválido"
                                _s_informacionTXT += (str(_dbRowPoliza.tempresa_diarios.codigo)).ljust(11)#Ljust es para justificar a la izquierda.
                                
                                # Concepto 100 chars
                                _s_msgError = "Concepto inválido"
                                # TODO optimizar esta sustitución
                                _s_informacionTXT += ((str(_dbRowPoliza.tpolizas.concepto)).replace('Ñ','N').replace('Á', 'A').replace('É', 'E').replace('Í', 'I').replace('Ó', 'O').replace('Ú', 'U').replace('ñ','n').replace('á','a').replace('é','e').replace('í','i').replace('ó','o').replace('ú','u')).ljust(101)
                                
                                # Origen del sistema 2 chars (Siempre tiene que ser 11)
                                _s_informacionTXT += ('11').ljust(3)
                                
                                # Impresa o no impresa
                                _s_informacionTXT += (str(_dbRowPoliza.tempresa_tipopoliza_ejercicios.impresionpoliza)).ljust(2)
                                
                                # Poliza Ajuste 1 chars (siempre lleva 0)
                                ###TODO: Revisar esto cuando se vean pólizas de ajuste
                                _s_informacionTXT += ('0').ljust(2)
                                
                                # GUID 36 chars (Identificador global, NO es el UUID)
                                _s_msgError = "GUID inválido"
                                _s_informacionTXT += (' ').ljust(37)
                                
                                #\r sirve para el salto de línea en formato TXT
                                _s_informacionTXT += '\r\n'
                                
                                pass
                            else:
                                # Si no es el primer registro, ya no imprimas el encabezado
                                pass
                            
                            if (_dbRowPoliza.tcfdis.id) and (_dbRowPoliza.tcfdis.id != _n_ultimoCFDIusado):
                                _n_ultimoCFDIusado = _dbRowPoliza.tcfdis.id
                                _L_listaDeUUIDs.append(("AD " + str(_dbRowPoliza.tcfdis.uuid)).ljust(37))
                                
                            else:
                                # No hacer nada, hasta que cambie el cfdi id
                                pass                        
    
                            # Movimiento (Siempre lleva 'M1')
                            _s_informacionTXT += ('M1').ljust(3)
                
                            #Cuenta contable 30 chars
                            _s_msgError = "Cuenta contable inválida"
                            _s_informacionTXT += ((str(_dbRowPoliza.tcfdi_prepoliza_asientoscontables.cuenta_contable)).replace('-','')).ljust(31)
                
                            #Referencia 20 chars
                            _s_msgError = "Referencia inválida"
                            _s_informacionTXT += (str(_dbRowPoliza.tcfdi_prepoliza_asientoscontables.referencia)).ljust(21)
                            
                            if(_dbRowPoliza.tcfdi_prepoliza_asientoscontables.importe_cargo != 0):
                                _n_cargo_abono = 0
                                _s_importe_cargo_abono = str(float(_dbRowPoliza.tcfdi_prepoliza_asientoscontables.importe_cargo))
                                _n_abonos += _dbRowPoliza.tcfdi_prepoliza_asientoscontables.importe_cargo
                                
                            elif(_dbRowPoliza.tcfdi_prepoliza_asientoscontables.importe_abono != 0):
                                _n_cargo_abono = 1
                                _s_importe_cargo_abono = str(float(_dbRowPoliza.tcfdi_prepoliza_asientoscontables.importe_abono))
                                _n_cargos += _dbRowPoliza.tcfdi_prepoliza_asientoscontables.importe_abono
    
                            else:#Este else se hace en caso de que haya un cargo en la cuenta complemento. 
                                _n_cargo_abono = 0
                                
                            #Cargo/Abono Cargo = 0 y Abono = 1
                            _s_informacionTXT += (str(_n_cargo_abono)).ljust(2)
                            
                            #Importe cargo X chars
                            _s_msgError = "Importe cargo/abono inválido"
                            _s_informacionTXT += (_s_importe_cargo_abono).ljust(21)
                
                            #Diario especial X chars
                            _s_msgError = "Diario especial inválido"
                            # TODO cual es el error aqui, ver ejemplos
                            if (_dbRowPoliza.tcfdi_prepoliza_asientoscontables.empresa_diario_id == None):
                                _s_diario_especial = '0'
                                _s_informacionTXT += (_s_diario_especial).ljust(11)
                            else: 
                                _s_diario_especial = str(_dbRowPoliza.tempresa_diarios_porasiento.codigo)
                                _s_informacionTXT += (_s_diario_especial + ' ').rjust(11)
                                
                            #Importe moneda extranjera
                            _s_msgError = "Importe moneda extranjera inválido"
                            _s_informacionTXT += (str(float(_dbRowPoliza.tcfdi_prepoliza_asientoscontables.importe_moneda_extranjera))).ljust(21)
                
                            #Descripción
                            _s_msgError = "Descripción inválido"
                            _s_informacionTXT += ((_dbRowPoliza.tcfdi_prepoliza_asientoscontables.descripcion).replace('Ñ','N').replace('Á', 'A').replace('É', 'E').replace('Í', 'I').replace('Ó', 'O').replace('Ú', 'U').replace('ñ','n').replace('á','a').replace('é','e').replace('í','i').replace('ó','o').replace('ú','u')).ljust(104)
                            
                            #Segmento de negocio 1 chars
                            _s_msgError = "Segmento de negocio inválido"
                            if (_dbRowPoliza.tempresa_segmentos.codigo == None):
                                _s_segmento_negocio = ' '
                            else: 
                                _s_segmento_negocio = str(_dbRowPoliza.tempresa_segmentos.codigo)
                                
                            _s_informacionTXT += (_s_segmento_negocio).ljust(2)
                                    
                            # GUID 100 chars (identificador global NO es el UUID)
                            _s_msgError = "GUID inválido"
                            _s_informacionTXT += (' ').ljust(37)
                            
                            # salto del linea
                            _s_informacionTXT += '\r\n'
                
                            oDestFile.write(_s_informacionTXT)
                
                            pass
                        else:
                            pass
                    #Se genera una columna extra abajo para anidar los CFDIS al TXT
                    oDestFile.write('\r\n'.join(_L_listaDeUUIDs))
                    
                    oDestFile.write('\r\n')
                    
                    _n_ultimoCFDIusado = None
                    
                    _n_utlimoRegistroPagoUsado = []
                    
                    #Se genera la ultima columna de pagos. 
                    for _dbRowPoliza in _dbRowsPoliza:
                        
                        if(_dbRowPoliza.tcfdis.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO):
                            
                            if(_n_ultimoCFDIusado != _dbRowPoliza.tcfdis.id) or (_dbRowPoliza.tcfdi_registropagos.id not in _n_utlimoRegistroPagoUsado):
                                
                                _s_informacionTXT = ''
                                 
                                #Se asigna a la varible el ultimo cfdi utilizado
                                _n_ultimoCFDIusado = _dbRowPoliza.tcfdis.id
                                
                                # Se asigna a la variable el ultimo id de registro de pago usado. 
                                _n_utlimoRegistroPagoUsado.append(_dbRowPoliza.tcfdi_registropagos.id)
                                
                                #Los siguientes datos está basado en el layout otorgado por Contpaqi
                                #Si la forma de pago es Por definir, Transeferencia, TDC, TDD o Efectivo entonces se usa el encabezado IN
                                if(_dbRowPoliza.tcfdi_registropagos.formapago_id in (
                                    TFORMASPAGO.TRANSFERENCIA, 
                                    TFORMASPAGO.TDC, 
                                    TFORMASPAGO.TDD, 
                                    TFORMASPAGO.CHEQUE,
                                    )):
                                    
                                    
                                    # Se busca el folio para los movimientos de pago.
                                    _D_controlFolioPagos = self._get_siguienteFolioPago(
                                        _dbRowsPoliza[0].tpolizas.fecha, 
                                        _dbRowsPoliza[0].tempresa_bancosdiariosespeciales,
                                        _dbRowsPoliza[0].tcfdis.tipocomprobante_id
                                        )
                                    
                                    if (_D_controlFolioPagos.L_msgError):
                                        _D_return.L_msgError += _D_controlFolioPagos.L_msgError
                                        
                                    else:
                                        _n_folioPagos = int(_D_controlFolioPagos.n_folio)
                                                                     
                                    # Ingreso char de 2
                                    _s_informacionTXT += 'IN'.ljust(3)
                                     
                                    # IdDocumentoDe char 5 #TODO, ver si se cambia
                                    _s_msgError = "IdDocumentoDe inválido."
                                    _s_informacionTXT += '04020'.ljust(6)
                                     
                                    # TipoDocumento char 30
                                    _s_msgError = "TipoDocumento inválido."
                                    _s_informacionTXT += (str(_D_controlFolioPagos.n_empresa_tipodocumentosbancarios_codigo) + ' ').rjust(31)
                                     
                                    # Folio char 20
                                    _s_msgError = "Folio inválido."
                                    _s_informacionTXT += (str(_n_folioPagos)+ ' ').rjust(21)
                                     
                                    # Fecha char 8
                                    _s_msgError = "Fecha inválida."
                                    _s_informacionTXT += (_dbRowPoliza.tpolizas.fecha.strftime("%Y%m%d")).ljust(9)
                                     
                                    # FechaAplicación char 8 TODO: Ver si esta fecha está correcta aquí
                                    _s_msgError = "FechaAplicación inválida."
                                    _s_informacionTXT += (_dbRowPoliza.tpolizas.fecha.strftime("%Y%m%d")).ljust(9)
                                     
                                    # Código Persona char 6
                                    _s_msgError = "Código Persona inválido."
                                    _s_informacionTXT += (str(_dbRowPoliza.tempresa_clientes.cliente_codigo_contpaqi) + ' ').rjust(7)
     
                                    # Beneficiario pagador char 200  TODO: ¿Este es el receptor o el emisor? De momento dejo el receptor
                                    _s_msgError = "Beneficiario pagador inválido."
                                    _s_informacionTXT += (str(_dbRowPoliza.tcfdis.receptornombrerazonsocial).decode('utf-8')).ljust(201).encode('utf-8')
                                     
                                    # IdCuentaCheques char 20 (Cuenta bancaria a la que es mandada el dinero)
                                    _s_msgError = "IdCuentaCheques inválida."    
                                    
                                    if (_dbRowPoliza.tcfdi_registropagos.formapago_id in(TFORMASPAGO.TDC, TFORMASPAGO.TDD,)):
                                        _dbRows_empresa_cuentasbancarias = dbc01(
                                            (dbc01.tempresa_cuentabancaria_terminales.id == _dbRowPoliza.tcfdi_registropagos.empresa_cuentabancaria_terminal_id)
                                            &(dbc01.tempresa_cuentasbancarias.id == dbc01.tempresa_cuentabancaria_terminales.empresa_cuentabancaria_id)
                                            ).select(
                                                dbc01.tempresa_cuentasbancarias.codigo_contpaqi
                                                )
                                    else: 
                                        _dbRows_empresa_cuentasbancarias = dbc01(
                                            (dbc01.tempresa_cuentasbancarias.id == _dbRowPoliza.tcfdi_registropagos.empresa_cuentabancaria_id)
                                            ).select(
                                                dbc01.tempresa_cuentasbancarias.codigo_contpaqi
                                                )
                                    
                                    if not(_dbRows_empresa_cuentasbancarias):
                                        _s_informacionTXT += ('2'+ ' ').ljust(21)
                                    else:
                                        _s_informacionTXT += (str(_dbRows_empresa_cuentasbancarias.last().codigo_contpaqi)+ ' ').ljust(21)


                                    # Código Moneda char 4 #TODO: Hacer el código de contpaq para la moneda
                                    _s_msgError = "Código Moneda inválida."
                                    _s_informacionTXT += (str(_dbRowPoliza.tempresa_monedascontpaqi.moneda_contpaqi) + ' ').rjust(5)
                                                                
                                    # Total char 20
                                    _s_msgError = "Total inválido."
                                    _s_informacionTXT += str(float(_dbRowPoliza.tcfdis.total)).ljust(21)
                                                                 
                                    # Referencia char 20
                                    #Si la referencia está vacía se pone el folio #TODO: Crear un folio auto incremental
                                    _s_msgError = "Referencia inválida."
                                    _s_informacionTXT += str(_n_folioPagos).ljust(21)
                                    
                                    # Origen char 5 # TODO: Origen del pago
                                    _s_msgError = "IdDocumentoDe Origen."
                                    _s_informacionTXT += ('11'+' ').rjust(6)

                                    # Banco Origen char 30 
                                    _s_msgError = "Banco origen inválido."
                                    _dbRowBancoOrigen = db01.tbancos(_dbRowPoliza.tempresa_bancoscontpaqi.banco_id)
                                    _s_informacionTXT += (str(_dbRowBancoOrigen.c_banco)+ ' ').ljust(21)
     
                                    # Cuenta Origen char 30
                                    _s_msgError = "Cuenta origen inválida."
                                    if(_dbRowPoliza.tcfdi_registropagos.cuenta_cliente == None):
                                        _s_informacionTXT += (' ').rjust(31)
                                    else:
                                        _s_informacionTXT += (str(_dbRowPoliza.tcfdi_registropagos.cuenta_cliente)+ ' ').ljust(31)
                                                                 
                                    # Otro método de pago char 5
                                    _s_msgError = "Forma de pago inválida."
                                    _s_informacionTXT += str(_dbRowPoliza.tcfdis.formapago).ljust(6)
                                                                 
                                    # GUID char 36 (identificador global NO es el UUID)
                                    _s_msgError = "GUID inválido." 
                                    _s_informacionTXT += (' ').ljust(37)
                                                                 
                                    # RFC char 13 TODO: En el TXT aquí siempre viene vacío
                                    _s_msgError = "RFC inválido."
                                    _s_informacionTXT += (' ').ljust(14)
                                                                 
                                    # BancoExtranjero char 60 
                                    _s_msgError = "BancoExtranjero inválido."
                                    _s_informacionTXT += ' '.ljust(61)
                                                                 
                                    # TipoCambio char 20
                                    _s_msgError = "TipoCambio inválido."
                                    _s_informacionTXT += str(float(_dbRowPoliza.tcfdis.tipocambio)).ljust(21)
                                                                 
                                    # NúmeroCheque char 100
                                    _s_msgError = "NúmeroCheque inválido."
                                    if(_dbRowPoliza.tcfdi_registropagos.cheque == None):
                                        _s_informacionTXT += (' ').ljust(101)
                                    else:
                                        _s_informacionTXT += ('              ' + str(_dbRowPoliza.tcfdi_registropagos.cheque)).ljust(101)

                                    # UUIDRep char 36
                                    _s_msgError = "UUIDRep inválido."
                                    _s_informacionTXT += ' '.ljust(37)
                                                                 
                                    # NodoPago char 3 (parece que aquí siempre va este dato)
                                    _s_msgError = "NodoPago inválido."
                                    _s_informacionTXT += ('0' + ' ').rjust(4)
                                                                 
                                    # Código Moneda Tipo Cambio char 4 (parece que aquí siempre va este dato)
                                    _s_msgError = "Código Moneda Tipo Cambio inválido."
                                    _s_informacionTXT += ('2'+ ' ').rjust(5)
                                     
                                    _s_msgError = "Inserción al TXT inválido."
                                    _s_informacionTXT += '\r\n' 
                                    oDestFile.write(_s_informacionTXT)
                                    
                                    #TODO: En progreso
                                    
                                    dbc01(
                                        dbc01.tcfdi_prepoliza_asientoscontables.id == _dbRowPoliza.tcfdi_prepoliza_asientoscontables.id
                                        ).update(
                                            empresa_tipodocumentobancario_folio = _n_folioPagos
                                            )
                                        
                                    dbc01(
                                        dbc01.tempresa_tipodocumentobancario_ejercicios.id == _D_controlFolioPagos.n_empresa_tipodocumentobancario_ejercicios_id
                                        ).update(
                                            folio = _n_folioPagos
                                            )
                                    
                                    
                                    pass
                                    
                                    _s_informacionTXT = ''
                                    oDestFile.write(_s_informacionTXT)
                                    
                                else:
                                    # Si no se identifica la forma de pago, no se inserta nada, y se continua
                                    pass
                            else:
                                # Si el row apunta al mismo cfdi que ya fue impreso, se continua con el siguiente
                                pass
                            
                        elif(_dbRowPoliza.tcfdis.tipocomprobante_id == TTIPOSCOMPROBANTES.PAGO):

                            if(_n_ultimoCFDIusado != _dbRowPoliza.tcfdis.id) or (_n_utlimoRegistroPagoUsado != _dbRowPoliza.tcfdi_registropagos.id):
                                if(_dbRowPoliza.tcfdi_registropagos.formapago_id in (
                                  TFORMASPAGO.TRANSFERENCIA, 
                                  TFORMASPAGO.TDC, 
                                  TFORMASPAGO.TDD, 
                                  TFORMASPAGO.CHEQUE,
                                  )):
                                    _dbRowsRegistroPagoComplemento = dbc01(
                                        dbc01.tcfdi_registropagos.cfdi_complementopago_id == _dbRowPoliza.tcfdi_complementopagos.id
                                        ).select(dbc01.tcfdi_registropagos.ALL).last()
                                    
                                    _s_informacionTXT = ''
                                     
                                    #Se asigna a la varible el ultimo cfdi utilizado
                                    _n_ultimoCFDIusado = _dbRowPoliza.tcfdis.id
                                    
                                    # Se asigna a la variable el ultimo id de registro de pago usado. 
                                    _n_utlimoRegistroPagoUsado = _dbRowPoliza.tcfdi_registropagos.id
                                    
                                    #Los siguientes datos está basado en el layout otorgado por Contpaqi
                                    # Se busca el folio para los movimientos de pago.
                                    _D_controlFolioPagos = self._get_siguienteFolioPago(
                                        _dbRowsPoliza[0].tpolizas.fecha, 
                                        _dbRowsPoliza[0].tempresa_bancosdiariosespeciales, ###TODO: ¿Por qué utilizo la posición 0 y no el _dbRow que viene en el for?
                                        _dbRowsPoliza[0].tcfdis.tipocomprobante_id
                                        )
                                        
                                    if (_D_controlFolioPagos.L_msgError):
                                        _D_return.L_msgError += _D_controlFolioPagos.L_msgError
                                        break
                                    else:
                                        _n_folioPagos = int(_D_controlFolioPagos.n_folio)
                                                                         
                                    # Ingreso char de 2
                                    _s_informacionTXT += 'IN'.ljust(3)
                                         
                                    # IdDocumentoDe char 5 #TODO, ver si se cambia
                                    _s_msgError = "IdDocumentoDe inválido."
                                    _s_informacionTXT += '04020'.ljust(6)
                                         
                                    # TipoDocumento char 30
                                    _s_msgError = "TipoDocumento inválido."
                                    _s_informacionTXT += (str(_D_controlFolioPagos.n_empresa_tipodocumentosbancarios_codigo) + ' ').rjust(31)
                                         
                                    # Folio char 20
                                    _s_msgError = "Folio inválido."
                                    _s_informacionTXT += (str(_n_folioPagos)+ ' ').rjust(21)
                                         
                                    # Fecha char 8
                                    _s_msgError = "Fecha inválida."
                                    _s_informacionTXT += (_dbRowPoliza.tpolizas.fecha.strftime("%Y%m%d")).ljust(9)
                                         
                                    # FechaAplicación char 8 TODO: Ver si esta fecha está correcta aquí
                                    _s_msgError = "FechaAplicación inválida."
                                    _s_informacionTXT += (_dbRowPoliza.tpolizas.fecha.strftime("%Y%m%d")).ljust(9)
                                         
                                    # Código Persona char 6
                                    _s_msgError = "Código Persona inválido."
                                    _s_informacionTXT += (str(_dbRowPoliza.tempresa_clientes.cliente_codigo_contpaqi) + ' ').rjust(7)
         
                                    # Beneficiario pagador char 200  TODO: ¿Este es el receptor o el emisor? De momento dejo el receptor
                                    _s_msgError = "Beneficiario pagador inválido."
                                    _s_informacionTXT += (str(_dbRowPoliza.tcfdis.receptornombrerazonsocial).decode('utf-8')).ljust(201).encode('utf-8')
                                         
                                    # IdCuentaCheques char 20 (Cuenta bancaria a la que es mandada el dinero)
                                    _s_msgError = "IdCuentaCheques inválida."
                                    if (_dbRowPoliza.tcfdi_registropagos.formapago_id in(TFORMASPAGO.TDC, TFORMASPAGO.TDD,)):
                                        _dbRows_empresa_cuentasbancarias = dbc01(
                                                (dbc01.tempresa_cuentabancaria_terminales.id == _dbRowPoliza.tcfdi_registropagos.empresa_cuentabancaria_terminal_id)
                                                &(dbc01.tempresa_cuentasbancarias.id == dbc01.tempresa_cuentabancaria_terminales.empresa_cuentabancaria_id)
                                                ).select(
                                                    dbc01.tempresa_cuentasbancarias.codigo_contpaqi
                                                    )
                                    else: 
                                        _dbRows_empresa_cuentasbancarias = dbc01(
                                                (dbc01.tempresa_cuentasbancarias.id == _dbRowPoliza.tcfdi_registropagos.empresa_cuentabancaria_id)
                                                ).select(
                                                    dbc01.tempresa_cuentasbancarias.codigo_contpaqi
                                                    )
                                    
                                    if not(_dbRows_empresa_cuentasbancarias):
                                        _s_informacionTXT += ('2'+ ' ').ljust(21)
                                    else:
                                        _s_informacionTXT += (str(_dbRows_empresa_cuentasbancarias.last().codigo_contpaqi)+ ' ').ljust(21)
                                                   
                                         
                                    # Código Moneda char 4 #TODO: Hacer el código de contpaq para la moneda
                                    _s_msgError = "Código Moneda inválida."
                                    _s_informacionTXT += (str(_dbRowPoliza.tempresa_monedascontpaqi.moneda_contpaqi) + ' ').rjust(5)
                                                                    
                                    # Total char 20
                                    _s_msgError = "Total inválido."
                                    _s_informacionTXT += str(float(_dbRowPoliza.tcfdi_complementopagos.monto)).ljust(21)
                                                                     
                                    # Referencia char 20
                                    #Si la referencia está vacía se pone el folio #TODO: Crear un folio auto incremental
                                    _s_msgError = "Referencia inválida."
                                    _s_informacionTXT += str(_n_folioPagos).ljust(21)
                                        
                                    # Origen char 5 # TODO: Origen del pago
                                    _s_msgError = "IdDocumentoDe Origen."
                                    _s_informacionTXT += ('11'+' ').rjust(6)
                                                                     
                                    # Banco Origen char 30 
                                    _s_msgError = "Banco origen inválido."
                                    _dbRowBancoOrigen_id = dbc01.tempresa_bancoscontpaqi(_dbRowsRegistroPagoComplemento.bancocontpaqi_id_cliente)
                                   
                                    _dbRowBancoOrigen = db01.tbancos(_dbRowBancoOrigen_id.banco_id)
    
                                    _dbRowBancoOrigen = db01.tbancos(_dbRowsRegistroPagoComplemento.bancocontpaqi_id_cliente)
                                    _s_informacionTXT += (str(_dbRowBancoOrigen.c_banco)+ ' ').ljust(21)
         
                                    # Cuenta Origen char 30
                                    _s_msgError = "Cuenta origen inválida."
                                    if((_dbRowsRegistroPagoComplemento.cuenta_cliente == None) or (_dbRowsRegistroPagoComplemento.cuenta_cliente == ' ')):
                                        _s_informacionTXT += (' ').rjust(31)
                                    else:
                                        _s_informacionTXT += (str(_dbRowsRegistroPagoComplemento.cuenta_cliente)+ ' ').ljust(31)
                                                                     
                                    # Otro método de pago char 5
                                    _s_msgError = "Forma de pago inválida."
                                    _s_informacionTXT += str(_dbRowPoliza.tcfdi_complementopagos.formapago).ljust(6)
                                                                     
                                    # GUID char 36 (identificador global NO es el UUID)
                                    _s_msgError = "GUID inválido." 
                                    _s_informacionTXT += (' ').ljust(37)
                                                                     
                                    # RFC char 13 TODO: En el TXT aquí siempre viene vacío
                                    _s_msgError = "RFC inválido."
                                    _s_informacionTXT += (' ').ljust(14)
                                                                     
                                    # BancoExtranjero char 60 
                                    _s_msgError = "BancoExtranjero inválido."
                                    _s_informacionTXT += ' '.ljust(61)
                                                                     
                                    # TipoCambio char 20
                                    _s_msgError = "TipoCambio inválido."
                                    _s_informacionTXT += '1.0'.ljust(21)
                                                                     
                                    # NúmeroCheque char 100
                                    _s_msgError = "NúmeroCheque inválido."
                                    if(_dbRowsRegistroPagoComplemento.cheque == None):
                                        _s_informacionTXT += (' ').ljust(101)
                                    else:
                                        _s_informacionTXT += ('              ' + str(_dbRowsRegistroPagoComplemento.cheque)).ljust(101)
    
                                    # UUIDRep char 36
                                    _s_msgError = "UUIDRep inválido."
                                    _s_informacionTXT += ' '.ljust(37)
                                                                     
                                    # NodoPago char 3 (parece que aquí siempre va este dato)
                                    _s_msgError = "NodoPago inválido."
                                    _s_informacionTXT += ('0' + ' ').rjust(4)
                                                                     
                                    # Código Moneda Tipo Cambio char 4 (parece que aquí siempre va este dato)
                                    _s_msgError = "Código Moneda Tipo Cambio inválido."
                                    _s_informacionTXT += ('2'+ ' ').rjust(5)
                                         
                                    _s_msgError = "Inserción al TXT inválido."
                                    _s_informacionTXT += '\r\n' 
                                    oDestFile.write(_s_informacionTXT)
                                        
                                    #TODO: En progreso
                                        
                                    dbc01(
                                        dbc01.tcfdi_prepoliza_asientoscontables.id == _dbRowPoliza.tcfdi_prepoliza_asientoscontables.id
                                        ).update(
                                            empresa_tipodocumentobancario_folio = _n_folioPagos
                                            )
                                            
                                    dbc01(
                                        dbc01.tempresa_tipodocumentobancario_ejercicios.id == _D_controlFolioPagos.n_empresa_tipodocumentobancario_ejercicios_id
                                        ).update(
                                            folio = _n_folioPagos
                                            )
                                else:
                                    pass       
                            else:
                                pass
                        else:
                            # Si es tipo de egreso no es necesario hacer este ciclo
                            break
                            pass
                        
                    oDestFile.close()
                    if(_n_cargos == _n_abonos):        
                    
                        _sHelper_sPathFilename = sPathFilename
                    
                        _sFilename = str(s_poliza_id).zfill(6) + time.strftime("_poliza_%Y%m%d") + (".txt")
                    
                        oStream = open(sPathFilename, 'rb')
                    
                        dbc01(dbc01.tpolizas.id == s_poliza_id).update(
                                archivopoliza = dbc01.tpolizas.archivopoliza.store(oStream), 
                                archivopolizanombre = _sFilename,
                                folio = _n_folio)
                        
                        dbc01(
                            dbc01.tempresa_tipopoliza_ejercicios.id == _D_controlFolio.n_empres_tipopoliza_ejercicios_id
                            ).update(
                                **{_D_controlFolio.dbField : (_n_folio + 1)}
                                )
                            
                        oStream.close()
                
                        pass
                    else:
                        oDestFile.close()
                        _D_return.L_msgError.append('No se pudo generar el TXT debido a que la póliza no está cuadrada')
                        pass
        
        except Exception as e:
    
                _D_return.L_msgError.append('Error al generar el archivo: ' + _s_msgError)
                # Si hay un error que se haga rollback del cambio que se hizo al hacer update a los folios
                dbc01.rollback()
        
        finally:
        
                os.remove(sPathFilename)
                _D_return.E_return = CLASS_e_RETURN.OK
    
        return (_D_return)

    def _getCamposPorEjercicio(self, s_aniocfdi, dbTable, dbQry, D_regresarCampos, dbFieldEjercicio = None):
        '''Función generica para obtener campos en base al ejercicio
        
            Args:
                dbTable: tabla a relacionar
                dbQry: query a utilizar
                D_regresarCampos: campos a regresar en la búsqueda
                [dbFieldEjercicio]: nombre del campo que contiene el ejercicio
        '''
        
        _D_return = D_regresarCampos
        
        if dbFieldEjercicio:
            _dbFieldEjercicio = dbFieldEjercicio
        else:
            _dbFieldEjercicio = dbTable['ejercicio']
        
        _LdbFields = []
        for _s_campo in D_regresarCampos:
            _LdbFields += [dbTable[D_regresarCampos[_s_campo]]]
    
        _dbRows_ejercicios = dbc01(
            dbQry
            & (_dbFieldEjercicio <= s_aniocfdi)
            ).select(
                *_LdbFields,
                orderby = ~_dbFieldEjercicio,
                limitby=(0, 1)
                )
        
        if (_dbRows_ejercicios):
            _dbRow_ejercicio = _dbRows_ejercicios.first()
            for _s_campo in D_regresarCampos:
                _D_return[_s_campo] = _dbRow_ejercicio[D_regresarCampos[_s_campo]] 
        else:
            # No hay cunetas contables detectadas
            _D_return = Storage()
    
        return _D_return

    def _insertHeader(self, n_estatus = TPOLIZAS.ESTATUS.EN_PROCESO):
        '''Función para insertar el header de la prepoliza contable
        
            Args:
        
        '''
        from string import Template   
                
        _D_return = Storage(
            E_return = CLASS_e_RETURN.NOK_ERROR,
            )
        
        _D_poliza_insert = Storage(
            empresa_tipopoliza_id = self._D_contabilidad.tipopoliza_id,
            empresa_diario_id = self._D_contabilidad.diario_id,
            concepto = Template(self._D_contabilidad.concepto).safe_substitute(self._D_substituciones),
            folio = 0, # Se define al momento de exportarla
            fecha = self._dt_fecha,
            dia = self._dt_fecha.date(),
            estatus = n_estatus,
            empresa_id = self._s_empresa_id,
            empresa_plaza_sucursal_cajachica_id = self._dbRowPrepoliza.tcfdis.empresa_plaza_sucursal_cajachica_id
            )             
                         
        #Se inserta a la tabla.
        self._n_poliza_id = dbc01.tpolizas.insert(**_D_poliza_insert)
        _D_return.E_return = CLASS_e_RETURN.OK
        
        return (_D_return)
    
    def _actualizar_estatus(
            self, 
            s_prepoliza_id, 
            s_cfdi_id, 
            s_poliza_id = None, 
            n_cfdi_etapa = TCFDIS.E_ETAPA.CON_PREPOLIZA,
            b_limpiarAsientoContablePoliza = False,
            dbRowEmpresa = None,
            b_esCancelado = False
            ):
        '''Función para actualizar el estatus de la del CFDI, asociar la prepoliza con la póliza contable y
        actualizar los asientos contables de la póliza.
            
        Args:
            s_prepoliza_id: Id de la prepoliza a actualizar el poliza_id, asi como en sus asientos
            s_cfdi_id: CFDI a actualizar su estapa
            s_poliza_id: Id de la poliza a actualizar
            n_cfdi_etapa: Etpa a actualizar en el CFDI
            dbRowEmpresa: Datos de la empresa
        '''
        
        dbc01.tcfdi_prepolizas(s_prepoliza_id).update_record(
            poliza_id = s_poliza_id
            ) 
        
        if dbRowEmpresa:
            
            if(
                (dbRowEmpresa.mostrarcfditotalcero == TEMPRESAS.MOSTRARCFDITOTALCERO.NOMOSTRAR) 
                and (self._dbRowPrepoliza.tcfdis.total == 0) 
                and (self._dbRowPrepoliza.tcfdis.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO)
                ):
                # Se verifica si la empresa está configurada para no mostrar los CFDIs en 0 en la póliza y si es un CFDI de tipo ingreso. 
                pass
            else:
                dbc01(
                    dbc01.tcfdi_prepoliza_asientoscontables.cfdi_prepoliza_id == s_prepoliza_id
                    ).update(
                        poliza_id = s_poliza_id
                        )
        
        else:
            
            if b_limpiarAsientoContablePoliza:
                dbc01(
                    dbc01.tcfdi_prepoliza_asientoscontables.cfdi_prepoliza_id == s_prepoliza_id
                    ).update(
                        poliza_id = s_poliza_id,
                        asiento_contable_poliza = None
                        )
                
            else:
                dbc01(
                    dbc01.tcfdi_prepoliza_asientoscontables.cfdi_prepoliza_id == s_prepoliza_id
                    ).update(
                        poliza_id = s_poliza_id
                        )

        if b_esCancelado:
            dbc01.tcfdis(s_cfdi_id).update_record(
                etapa_cancelado = n_cfdi_etapa
                )
        else:
            dbc01.tcfdis(s_cfdi_id).update_record(
                etapa = n_cfdi_etapa
                )
        
        return
        
    def _get_siguienteFolio(self, d_fecha, n_empresa_tipopoliza_id):
        """ Función para conseguir el folio por periodo o ejercicio
            
            Args:
                d_fecha: Fecha del cfdi
                n_empresa_tipopoliza_id: Id del tipo de poliza 
        """
        
        _D_return = Storage(
            n_folio = -1,
            n_empres_tipopoliza_ejercicios_id = 0,
            dbField = None,
            L_msgError = [],
            )

        _n_mes = int(str(d_fecha.month))
        _s_anio = str(d_fecha.year)
        
        _dbTable = dbc01.tempresa_tipopoliza_ejercicios
        
        _dbQry = (_dbTable.empresa_tipopoliza_id == n_empresa_tipopoliza_id)
        
        _D_foliado = self._getCamposPorEjercicio(
            s_aniocfdi = _s_anio,
            dbTable = _dbTable,
            dbQry = _dbQry, 
            D_regresarCampos = Storage(
                foliado = 'foliado',
                folio_ejercicio = 'folioejercicio',
                folio_periodo_1 = 'folioperiodo01',
                folio_periodo_2 = 'folioperiodo02',
                folio_periodo_3 = 'folioperiodo03',
                folio_periodo_4 = 'folioperiodo04',
                folio_periodo_5 = 'folioperiodo05',
                folio_periodo_6 = 'folioperiodo06',
                folio_periodo_7 = 'folioperiodo07',
                folio_periodo_8 = 'folioperiodo08',
                folio_periodo_9 = 'folioperiodo09',
                folio_periodo_10 = 'folioperiodo10',
                folio_periodo_11 = 'folioperiodo11',
                folio_periodo_12 = 'folioperiodo12',
                ejercicio = 'ejercicio',
                id = 'id'
                ))

        if _D_foliado:
            _qry_verificarFolio = (dbc01.tempresa_tipopoliza_ejercicios.empresa_tipopoliza_id == n_empresa_tipopoliza_id)
            _D_return.n_empres_tipopoliza_ejercicios_id = _D_foliado.id
            if(_D_foliado.foliado == TEMPRESA_TIPOPOLIZA_EJERCICIOS.FOLIADO.POR_EJERCICIO):
                _D_return.dbField = 'folioejercicio'
                _D_return.n_folio = _D_foliado.folio_ejercicio
                _qry_verificarFolio &= (dbc01.tpolizas.fecha.year == _s_anio)
            else:
                if(_n_mes == TGENERICAS.MES.ENERO):
                    _D_return.dbField = 'folioperiodo01'
                    _D_return.n_folio = _D_foliado.folio_periodo_1
                elif(_n_mes == TGENERICAS.MES.FEBRERO):
                    _D_return.dbField = 'folioperiodo02'
                    _D_return.n_folio = _D_foliado.folio_periodo_2
                elif(_n_mes == TGENERICAS.MES.MARZO):
                    _D_return.dbField = 'folioperiodo03'
                    _D_return.n_folio = _D_foliado.folio_periodo_3
                elif(_n_mes == TGENERICAS.MES.ABRIL):
                    _D_return.dbField = 'folioperiodo04'
                    _D_return.n_folio = _D_foliado.folio_periodo_4
                elif(_n_mes == TGENERICAS.MES.MAYO):
                    _D_return.dbField = 'folioperiodo05'
                    _D_return.n_folio = _D_foliado.folio_periodo_5
                elif(_n_mes == TGENERICAS.MES.JUNIO):
                    _D_return.dbField = 'folioperiodo06'
                    _D_return.n_folio = _D_foliado.folio_periodo_6
                elif(_n_mes == TGENERICAS.MES.JULIO):
                    _D_return.dbField = 'folioperiodo07'
                    _D_return.n_folio = _D_foliado.folio_periodo_7
                elif(_n_mes == TGENERICAS.MES.AGOSTO):
                    _D_return.dbField = 'folioperiodo08'
                    _D_return.n_folio = _D_foliado.folio_periodo_8
                elif(_n_mes == TGENERICAS.MES.SEPTIEMBRE):
                    _D_return.dbField = 'folioperiodo09'
                    _D_return.n_folio = _D_foliado.folio_periodo_9
                elif(_n_mes == TGENERICAS.MES.OCTUBRE):
                    _D_return.dbField = 'folioperiodo10'
                    _D_return.n_folio = _D_foliado.folio_periodo_10
                elif(_n_mes == TGENERICAS.MES.NOVIEMBRE):
                    _D_return.dbField = 'folioperiodo11'
                    _D_return.n_folio = _D_foliado.folio_periodo_11
                elif(_n_mes == TGENERICAS.MES.DICIEMBRE):
                    _D_return.dbField = 'folioperiodo12'
                    _D_return.n_folio = _D_foliado.folio_periodo_12
                else:
                    pass
                _qry_verificarFolio &= (dbc01.tpolizas.fecha.month == _n_mes)
                
            _dbRows_folioExiste = dbc01(
                (dbc01.tpolizas.folio >= _D_return.n_folio)
                & _qry_verificarFolio
                ).select(
                    dbc01.tpolizas.folio,
                    orderby = [
                        dbc01.tpolizas.folio
                        ]
                    )
            
            if (_dbRows_folioExiste):
                _dbRow_primerFolio = _dbRows_folioExiste.first()
                if _dbRow_primerFolio.folio == _D_return.n_folio:
                    for _dbRow in _dbRows_folioExiste:
                        _D_return.n_folio += 1
                        if _dbRow.folio != _D_return.n_folio:
                            # No hay consecutivo, por lo que el folio es valido
                            break
                        else:
                            pass
                else:
                    # El folio encontrado no esta siendo utilizado
                    pass
            else:
                # El folio encontrado es correcto para la siguiente poliza
                pass
        else:
            _D_return.L_msgError = ["Empresa no tiene foliado para este tipo de póliza"]
        
        return (_D_return)        


    def  _get_siguienteFolioPago(self, d_fecha, dbRowBancosDiario, n_tipocomprobante):
        """ Función para conseguir el folio por periodo o ejercicio
            
            Args:
                d_fecha: Fecha del cfdi
                dbRowBancosDiario: Row de los diarios especiales utilizado para los bancos.  
                n_tipocomprobante: Tipo de comprobante del documento (Ingreso, Egreso)
        """
        
        _D_return = Storage(
            n_folio = -1,
            n_empresa_tipodocumentosbancarios_codigo = 0,
            dbField = None,
            L_msgError = [],
            )
        
        # Si es tipo ingreso el comprobante se tomará el folio de documento venta de contado        
        if(n_tipocomprobante in (TTIPOSCOMPROBANTES.INGRESO, TTIPOSCOMPROBANTES.PAGO)):
            _dbRowFoliado = dbc01(
                dbc01.tempresa_tipodocumentosbancarios.id == dbRowBancosDiario.bancos_tipo_documentos_venta_contado
                ).select(
                    dbc01.tempresa_tipodocumentosbancarios.ALL
                    )
        #TODO: Ver si esto aplica o no aún
#         # Si es tipo ingreso el comprobante se tomará el folio de documento de cobranza
#         elif(n_tipocomprobante == TTIPOSCOMPROBANTES.PAGO):
#             _dbRowFoliado = dbc01(
#                 dbc01.tempresa_tipodocumentosbancarios.id == dbRowBancosDiario.bancos_tipo_documentos_cobranza
#                 ).select(
#                     dbc01.tempresa_tipodocumentosbancarios.ALL
#                     )
        else:
            #Para los egresos no es necesario este paso.
            pass
        
        if(_dbRowFoliado):
            
            _s_anio = str(d_fecha.year)
        
            _dbTable = dbc01.tempresa_tipodocumentobancario_ejercicios
            
            _dbQry = (_dbTable.empresa_tipodocumentobancario_id == _dbRowFoliado[0].id)
            
            _D_foliado = self._getCamposPorEjercicio(
                s_aniocfdi = _s_anio,
                dbTable = _dbTable,
                dbQry = _dbQry, 
                D_regresarCampos = Storage(
                    folio = 'folio',
                    id = 'id'
                    )
                )
            
            if _D_foliado:
                
                _dbRows_folioExiste = dbc01(
                    (dbc01.tcfdi_prepoliza_asientoscontables.empresa_tipodocumentobancario_folio >= _D_foliado.folio)
                    ).select(
                        dbc01.tcfdi_prepoliza_asientoscontables.empresa_tipodocumentobancario_folio,
                        orderby = [
                            dbc01.tcfdi_prepoliza_asientoscontables.empresa_tipodocumentobancario_folio
                            ]
                        )
            
                if (_dbRows_folioExiste):
                    _dbRow_primerFolio = _dbRows_folioExiste.first()
                    if _dbRow_primerFolio.empresa_tipodocumentobancario_folio == int(_D_foliado.folio):
                        for _dbRow in _dbRows_folioExiste:
                            _D_foliado.folio += 1
                            if _dbRow.empresa_tipodocumentobancario_folio != _D_foliado.folio:
                                # No hay consecutivo, por lo que el folio es valido
                                break
                            else:
                                pass
                    else:
                        # El folio encontrado no esta siendo utilizado
                        pass
                else:
                    # El folio encontrado es correcto para la siguiente poliza
                    pass
                
                _D_return.n_folio = _D_foliado.folio           
                _D_return.n_empresa_tipodocumentosbancarios_codigo = _dbRowFoliado[0].codigo_contpaqi
                _D_return.n_empresa_tipodocumentobancario_ejercicios_id = _D_foliado.id
    
            else:
                _D_return.L_msgError.append("No se encontró folio")
                
        else:
            _D_return.L_msgError.append("No se encontró el tipo de documento bancario")
        

        
        return (_D_return)    
        