# -*- coding: utf-8 -*-


class TEMPRESA_PLAZA_SUCURSAL_ALMACENES(Table):
    """ Definición de la tabla de tempresa_plaza_sucursal_almacenes """

    S_NOMBRETABLA = 'tempresa_plaza_sucursal_almacenes'

    class E_METODOCOSTEO:
        """ Se definen las opciónes del campo """
        NO_DEFINIDO = 0
        UEPS = 1
        PEPS = 2
        PROMEDIO = 3
        D_TODO = {
            NO_DEFINIDO: 'No definido',
            UEPS       : 'UEPS',
            PEPS       : 'PEPS',
            PROMEDIO   : 'Promedio',
            }
        
        @classmethod
        def get_dict(cls):
            return cls.D_TODO

    class E_USO:
        """ Se definen las opciónes del campo """
        NO_DEFINIDO = 0
        FIJO = 1
        TEMPORAL = 2
        D_TODO = {
            NO_DEFINIDO: 'No definido',
            FIJO       : 'Fijo',
            TEMPORAL   : 'Temporal',
            }

        @classmethod
        def get_dict(cls):
            return cls.D_TODO

    L_DBCAMPOS = [
        Field(
            'empresa_plaza_sucursal_id', dbc01.tempresa_plaza_sucursales, default = 1,
            required = False,
            requires = IS_IN_DB(
                dbc01(
                    dbc01.tempresa_plaza_sucursales.id.belongs(
                        TEMPRESA_PLAZA_SUCURSALES.OBTENER()
                        )
                    ),
                'tempresa_plaza_sucursales.id', dbc01.tempresa_plaza_sucursales._format
                ),
            ondelete = 'CASCADE', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Plaza : Sucursal', comment = None,
            writable = True, readable = True,
            represent = stv_represent_referencefield
            ),
        Field(
            'nombre', 'string', length = 100, default = None,
            required = True,
            notnull = True, unique = False,
            widget = stv_widget_input, label = 'Nombre del Almacén', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        STV_LIB_DB.DEFINIR_DBCAMPO_OTRADB(
            db01.tpaises, 'pais_id',
            dbQry = None, label = 'Pais', writable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_proveedores.pais_id)
            ),
        STV_LIB_DB.DEFINIR_DBCAMPO_OTRADB(
            db01.tpais_estados, 'pais_estado_id',
            dbQry = None, label = 'Estado', writable = True,
            widget = lambda field, value: stv_widget_db_chosen_linked(
                field, value, 1, 'pais_id',
                URL(a = 'app_generales', c = '005genericos_sat', f = 'localidad_pais_estados_ver.json')
                ),
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_proveedores.pais_estado_id)
            ),
        STV_LIB_DB.DEFINIR_DBCAMPO_OTRADB(
            db01.tpais_estado_municipios, 'pais_estado_municipio_id',
            dbQry = None, label = 'Municipio', writable = True,
            widget = lambda field, value: stv_widget_db_chosen_linked(
                field, value, 1, 'pais_estado_id',
                URL(a = 'app_generales', c = '005genericos_sat', f = 'localidad_pais_estado_mun_ver.json')
                ),
            represent = lambda v, r:
                stv_represent_referencefield(v, r, dbc01.tempresa_proveedores.pais_estado_municipio_id)
            ),
        Field(
            'ciudad', 'string', length = 30, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Ciudad', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'colonia', 'string', length = 30, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Colonia', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'calle', 'string', length = 30, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = "Calle", comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'numexterior', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Num. Exterior', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'numinterior', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Num. Interior', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'codigopostal', 'string', length = 5, default = None,
            required = False,
            requires = IS_EMPTY_OR(IS_LENGTH(maxsize = 5, minsize = 5, error_message = "requiere %(min)g números")),
            notnull = False, unique = False,
            widget = stv_widget_input, label = "Código Postal", comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'metodocosteo', 'integer', default = E_METODOCOSTEO.PROMEDIO,
            required = True, requires = IS_IN_SET(E_METODOCOSTEO.D_TODO, zero = 0, error_message = 'Selecciona'),
            notnull = True, unique = False,
            widget = stv_widget_combobox,
            label = 'Método de costeo', comment = 'Método de costeo que se utilizará en este almacen',
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_list(
                v, r, None, TEMPRESA_PLAZA_SUCURSAL_ALMACENES.E_METODOCOSTEO.D_TODO
                )
            ),
        Field(
            'uso', 'integer', default = E_USO.FIJO,
            required = True, requires = IS_IN_SET(E_USO.D_TODO, zero = 0, error_message = 'Selecciona'),
            notnull = True, unique = False,
            widget = stv_widget_combobox,
            label = 'Uso',
            comment = 'Uso de almacén, use el valor de temporal en caso de que la mercancía deba ser zero',
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_list(
                v, r, None, TEMPRESA_PLAZA_SUCURSAL_ALMACENES.E_USO.D_TODO
                )
            ),
        Field(
            'telefonos', 'string', length = 30, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Telefonos', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'email', 'string', length = 30, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Email', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'observaciones', 'text', default = None,
            required = False,
            notnull = False,
            label = 'Observaciones', comment = "",
            writable = True, readable = True,
            represent = stv_represent_text
            ),
        STV_LIB_DB.DEFINIR_DBCAMPO(
            dbc01.tempresa_series, 'empresa_serie_traslado_id',
            dbQry = (dbc01.tempresa_series.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id),
            label = 'Serie de Traslado', writable = True
            ),
        Field(
            'temporal_inivigencia', 'date', default = None,
            required = False, requires = IS_NULL_OR(IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE)),
            notnull = False,
            widget = stv_widget_inputDate, label = 'Inicio vigencia', comment = '',
            writable = False, readable = True,
            represent = stv_represent_date
            ),
        Field(
            'temporal_finvigencia', 'date', default = None,
            required = False, requires = IS_NULL_OR(IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE)),
            notnull = False,
            widget = stv_widget_inputDate, label = 'Fin vigencia', comment = '',
            writable = False, readable = True,
            represent = stv_represent_date
            ),

        tSignature_dbc01,
        ]

    @classmethod
    def OBTENER(
            cls,
            s_empresa_id = None,
            s_sucursal_id = None
            ):
        """

        @param s_empresa_id:
        @type s_empresa_id:
        @param s_sucursal_id:
        @type s_sucursal_id:
        @return:
        @rtype:
        """

        def buscar():
            _qryAlmacen = (
                dbc01.tempresa_plaza_sucursal_almacenes.empresa_plaza_sucursal_id == s_sucursal_id
                if s_sucursal_id else True
                )
            _qryEmpresa = (
                (
                    (dbc01.tempresa_plazas.empresa_id == s_empresa_id)
                    & (dbc01.tempresa_plaza_sucursales.empresa_plaza_id == dbc01.tempresa_plazas.id)
                    & (
                        dbc01.tempresa_plaza_sucursal_almacenes.empresa_plaza_sucursal_id
                        == dbc01.tempresa_plaza_sucursales.id
                        )
                    )
                if s_empresa_id else True
                )

            _dbRows = dbc01(
                _qryEmpresa & _qryAlmacen
                ).select(
                    dbc01.tempresa_plaza_sucursal_almacenes.id
                    )
            return [_dbRow.id for _dbRow in _dbRows]

        _dbTabla = dbc01.tempresa_plaza_sucursal_almacenes
        if not s_empresa_id:
            s_empresa_id = D_stvSiteHelper.dbconfigs.defaults.empresa_id
        else:
            pass
        _s_id_cache = "%s_%s_%s" % (_dbTabla._tablename, str(s_empresa_id), str(s_sucursal_id))
        _L_almacenes_ids = cache.ram(
            _s_id_cache, lambda: buscar(), time_expire = 86400
            )

        return _L_almacenes_ids

    # TODO Agregar metodos para manejo almacén y agregar el uso de vigencias en caso de temporal

    @classmethod
    def CONFIGURAR_INTEGRIDAD_REFERENCIAL(cls):
        _dbTabla = dbc01.tempresa_plaza_sucursal_almacenes

        return None

    pass


dbc01.define_table(
    TEMPRESA_PLAZA_SUCURSAL_ALMACENES.S_NOMBRETABLA, *TEMPRESA_PLAZA_SUCURSAL_ALMACENES.L_DBCAMPOS,
    format = '%(nombre)s',
    singular = 'Almacen',
    plural = 'Almacenes',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TEMPRESA_PLAZA_SUCURSAL_ALMACENES
    )
STV_FWK_LIB.DEFINIR_INTEGRIDADREFERENCIAL(
    db01.tpaises,
    dbc01.tempresa_plaza_sucursal_almacenes.pais_id,
    )
STV_FWK_LIB.DEFINIR_INTEGRIDADREFERENCIAL(
    db01.tpais_estados,
    dbc01.tempresa_plaza_sucursal_almacenes.pais_estado_id,
    )
STV_FWK_LIB.DEFINIR_INTEGRIDADREFERENCIAL(
    db01.tpais_estado_municipios,
    dbc01.tempresa_plaza_sucursal_almacenes.pais_estado_municipio_id,
    )
STV_FWK_LIB.DEFINIR_INTEGRIDADREFERENCIAL(
    dbc01.tempresa_series,
    dbc01.tempresa_plaza_sucursal_almacenes.empresa_serie_traslado_id,
    True
    )


class TEMPRESA_PLAZA_SUCURSAL_ALMACEN_ALMACENISTAS(Table):
    """ Definición de la tabla de tempresa_almacen_almacenistas """

    S_NOMBRETABLA = 'tempresa_plaza_sucursal_almacen_almacenistas'

    class E_ROL:
        """ Se definen las opciónes del campo """
        ALMACENISTA = 1
        AUXILIAR_ALMACEN = 2
        D_TODOS = {
            ALMACENISTA     : 'Almacenista',
            AUXILIAR_ALMACEN: 'Auxiliar Almacén',
            }
        
        @classmethod
        def get_dict(cls):
            return cls.D_TODOS

    L_DBCAMPOS = [
        Field(
            'empresa_plaza_sucursal_almacen_id', dbc01.tempresa_plaza_sucursal_almacenes, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'Almacen', comment = None,
            writable = False, readable = False,
            represent = None
            ),
        STV_LIB_DB.DEFINIR_DBCAMPO_OTRADB(
            db.auth_user, 'usuario_id',
            dbQry = None, label = 'Usuario de almacén', writable = True,
            widget = lambda f, v: stv_widget_db_search(
                f, v,
                s_display = '%(auth_user.first_name)s %(auth_user.last_name)s [%(auth_user.email)s]',
                s_url = URL(a = 'backoffice', c = 'accesos', f = 'usuarios_buscar'),
                D_additionalAttributes = {'reference': db.auth_user.id}
                ),
            represent = lambda v, r:
                stv_represent_referencefield(v, r, dbc01.tempresa_plaza_sucursal_almacen_almacenistas.usuario_id)
            ),
        Field(
            'rol', 'integer', default = E_ROL.AUXILIAR_ALMACEN,
            required = True, requires = IS_IN_SET(E_ROL.D_TODOS, zero = 0, error_message = 'Selecciona'),
            notnull = True, unique = False,
            widget = stv_widget_combobox,
            label = 'Rol (responsabilidad)', comment = 'Rol del empleado en el almacén',
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_list(
                v, r, None, TEMPRESA_PLAZA_SUCURSAL_ALMACEN_ALMACENISTAS.E_ROL.D_TODOS
                )
            ),
        Field(
            'fecharegistro', 'date', default = request.browsernow,
            required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
            notnull = False,
            widget = stv_widget_inputDate, label = 'Fecha de registro', comment = 'Fecha registro',
            writable = True, readable = True,
            represent = stv_represent_date
            ),
        Field(
            'fechafinal', 'date', default = request.browsernow,
            required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
            notnull = False,
            widget = stv_widget_inputDate, label = 'Fecha final', comment = 'Fecha final',
            writable = True, readable = True,
            represent = stv_represent_date
            ),
        tSignature_dbc01,
        ]

    @classmethod
    def GET_ALMACENES(cls, n_empresa_id, n_usuario_id, fn_formato = None):
        """Se consigue los almacenes en los que el usuario tiene permiso de acceder.
        
            n_usuario_id: ID del usuario.
        
        """
        
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,  # Siempre regresa OK
            s_msgError = "",
            D_almacenes = {},
            )
        
        _dbRows_almacenes = dbc01(
            (dbc01.tempresa_plaza_sucursal_almacen_almacenistas.usuario_id == n_usuario_id)
            & (
                dbc01.tempresa_plaza_sucursal_almacenes.id
                == dbc01.tempresa_plaza_sucursal_almacen_almacenistas.empresa_plaza_sucursal_almacen_id
                )
            & (dbc01.tempresa_plaza_sucursales.id == dbc01.tempresa_plaza_sucursal_almacenes.empresa_plaza_sucursal_id)
            & (dbc01.tempresa_plazas.id == dbc01.tempresa_plaza_sucursales.empresa_plaza_id)
            & (dbc01.tempresa_plazas.empresa_id == n_empresa_id)
            ).select(
                dbc01.tempresa_plaza_sucursal_almacenes.ALL,
                dbc01.tempresa_plaza_sucursales.ALL,
                dbc01.tempresa_plazas.ALL
                )
        
        if len(_dbRows_almacenes) == 0:
            _D_return.s_msgError = "No se encontraron almacenes"
        else:
            for _dbRow in _dbRows_almacenes:
                _D_row = Storage(_dbRow.as_dict())
                if fn_formato:
                    _s_texto = fn_formato(_D_row)
                else:
                    _s_texto = (
                        "%s [%s, %s]" % (
                            _D_row.tempresa_plaza_sucursal_almacenes['nombre'],
                            _D_row.tempresa_plaza_sucursales['nombrecorto'],
                            _D_row.tempresa_plazas['nombrecorto']
                            )
                        )
                            
                _D_return.D_almacenes[_D_row.tempresa_plaza_sucursal_almacenes['id']] = _s_texto
             
        return _D_return
    
    pass  # TEMPRESA_PLAZA_SUCURSAL_ALMACEN_ALMACENISTAS


dbc01.define_table(
    TEMPRESA_PLAZA_SUCURSAL_ALMACEN_ALMACENISTAS.S_NOMBRETABLA,
    *TEMPRESA_PLAZA_SUCURSAL_ALMACEN_ALMACENISTAS.L_DBCAMPOS,
    format = '%(almacen_id)s',
    singular = 'Almacenista',
    plural = 'Almacenistas',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TEMPRESA_PLAZA_SUCURSAL_ALMACEN_ALMACENISTAS
    )

# Se crea la referencia para su uso en los campos
STV_FWK_LIB.DEFINIR_INTEGRIDADREFERENCIAL(
    db.auth_user,
    dbc01.tempresa_plaza_sucursal_almacen_almacenistas.usuario_id,
    )


class TEMPRESA_ALMACENESCONCEPTOS(Table):
    """ Definición de la tabla de tempresa_almacenesconceptos """

    S_NOMBRETABLA = 'tempresa_almacenesconceptos'
    
    class E_TIPOCONCEPTO:
        """ Se definen las opciónes del campo """
        ENTRADA = 1
        SALIDA = 2
        D_TODOS = {
            ENTRADA: 'Entrada',
            SALIDA : 'Salida',
            }
        
        @classmethod
        def get_dict(cls):
            return cls.D_TODOS

    class E_TIPOCOSTO:
        """ Se definen las opciónes del campo """
        CAPTURADO = 1
        CALCULADO = 2
        D_TODOS = {
            CAPTURADO: 'Capturado',
            CALCULADO: 'Calculado',
            }
        
        @classmethod
        def get_dict(cls):
            return cls.D_TODOS

    class E_CLASIFICACION:
        """ Se definen las opciónes del campo """
        
        # Estas opciones pertenecen a los conceptos de Tipo Entrada.
        INVENTARIO_INICIAL = 1
        COMPRA = 2
        DEVOLUCION_VENTA = 3
        DEVOLUCION_INTERNA = 4
        TRASPASO_ENTRADA = 5
        OTRAS_ENTRADAS = 6
        
        # Estas opciones pertenecen a los conceptos de Tipo Salida
        VENTA = 7
        DEVOLUCION_COMPRA = 8
        CONSUMO_INTERNO = 9
        TRASPASO_SALIDA = 10
        OTRAS_SALIDAS = 11
        D_TODOS = {
            INVENTARIO_INICIAL: 'Inventario inicial',
            COMPRA            : 'Compra',
            DEVOLUCION_VENTA  : 'Devolución de venta',
            DEVOLUCION_INTERNA: 'Devolución interna',
            TRASPASO_ENTRADA  : 'Traspaso',
            OTRAS_ENTRADAS    : 'Otras entradas',

            VENTA             : 'Venta',
            DEVOLUCION_COMPRA : 'Devolución de compra',
            CONSUMO_INTERNO   : 'Consumo interno',
            TRASPASO_SALIDA   : 'Traspaso',
            OTRAS_SALIDAS     : 'Otras salidas',
            }

        @classmethod
        def GET_DICT(cls):
            return cls.D_TODOS

    L_DBCAMPOS = [
        Field(
            'empresa_id', dbc01.tempresas, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'Empresa', comment = None,
            writable = False, readable = False,
            represent = None
            ),
        Field(
            'tipoconcepto', 'integer', default = E_TIPOCONCEPTO.ENTRADA,
            required = True, requires = IS_IN_SET(E_TIPOCONCEPTO.D_TODOS, zero = 0, error_message = 'Selecciona'),
            notnull = True, unique = False,
            widget = stv_widget_combobox,
            label = 'Tipo de concepto', comment = 'Tipo del concepto a dar de alta',
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_list(
                v, r, None, TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.D_TODOS
                )
            ),
        Field(
            'codigoconcepto', 'string', length = 4, default = 'E-01',
            required = True,
            notnull = True, unique = False,
            widget = stv_widget_input, label = 'Código del concepto',
            comment = 'El código de concepto solo puede tener el siguiente formato "E-XX" o "S-XX" '
                      + 'siendo X el número de código que se quiera agregar, E tipo de código de "Entrada" '
                      + 'y S tipo de código de "Salida"',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'concepto', 'string', length = 50, default = None,
            required = True,
            notnull = True, unique = False,
            widget = stv_widget_input, label = 'Concepto', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'tipocosto', 'integer', default = 0,
            required = True, requires = IS_IN_SET(E_TIPOCOSTO.D_TODOS, zero = 0, error_message = 'Selecciona'),
            notnull = True, unique = False,
            widget = stv_widget_combobox,
            label = 'Tipo de costo', comment = 'Tipo de costo a dar de alta',
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCOSTO.D_TODOS)
            ),
        Field(
            'clasificacion', 'integer', default = None,
            required = False, requires = IS_IN_SET(E_CLASIFICACION.D_TODOS, zero = 0, error_message = 'Selecciona'),
            notnull = False, unique = False,
            widget = lambda field, value: stv_widget_db_chosen_linked(
                field, str(value), 1, 'tipoconcepto',
                URL(a = 'app_empresas', c = '125inventarios', f = 'clasificacion_ver.json')
                ),
            label = 'Tratamiento del concepto', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_list(
                v, r, None, TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.D_TODOS
                )
            ),
        tSignature_dbc01,
        ]

    @classmethod
    def GET_ID(cls, n_empresa_id, n_tipoconcepto, n_clasificacion):
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            n_concepto_id = None
            )

        # Query para obtener el ID del concepto de entrada por compra.
        _dbRows_conceptos = dbc01(
            (dbc01.tempresa_almacenesconceptos.empresa_id == n_empresa_id)
            & (dbc01.tempresa_almacenesconceptos.tipoconcepto == n_tipoconcepto)
            & (dbc01.tempresa_almacenesconceptos.clasificacion == n_clasificacion)
            & (dbc01.tempresa_almacenesconceptos.bansuspender == False)
            ).select(
                dbc01.tempresa_almacenesconceptos.id,
                orderby = ~dbc01.tempresa_almacenesconceptos.id
                )

        if len(_dbRows_conceptos) == 0:
            _D_return.s_msgError = (
                "No se ha encontrado el concepto para este tipo de movimiento, "
                + "favor de contactar con el administrador "
                )
            _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
        elif len(_dbRows_conceptos) > 1:
            _D_return.s_msgError = (
                "Se ha encontrado más de un concepto para este tipo de movimiento, "
                + "favor de contactar con el administrador"
                )
            _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
        else:
            _D_return.n_concepto_id = _dbRows_conceptos.first().id
             
        return _D_return
    
    @classmethod
    def GET_ID_CACHE(cls, n_empresa_id, n_tipoconcepto, n_clasificacion):
        """ Se consigue el concepto_id por empresa para un concepto en específico y se hace su cache.

        @param n_empresa_id: id de la empresa
        @type n_empresa_id:
        @param n_tipoconcepto: id del tipoConcepto
        @type n_tipoconcepto:
        @param n_clasificacion: id de la clasificacion
        @type n_clasificacion:
        @return: resultado de la generación del ID
        @rtype: Diccionario E_return, s_msgError y n_concepto_id
        """

        _s_cache_id = 'dba125s_almacenesconceptos_%s_%s_%s' % (
            str(n_empresa_id), str(n_tipoconcepto), str(n_clasificacion)
            )
        _D_results = cache.ram(
            _s_cache_id, 
            lambda: TEMPRESA_ALMACENESCONCEPTOS.GET_ID(n_empresa_id, n_tipoconcepto, n_clasificacion), 
            time_expire=86400
            )
        
        if _D_results.E_return != CLASS_e_RETURN.OK:
            cache.ram(_s_cache_id, None)
        else:
            pass
        
        return _D_results

    pass  # TEMPRESA_ALMACENESCONCEPTOS


dbc01.define_table(
    TEMPRESA_ALMACENESCONCEPTOS.S_NOMBRETABLA, *TEMPRESA_ALMACENESCONCEPTOS.L_DBCAMPOS,
    format = '%(concepto)s',
    singular = 'Concepto de almacén',
    plural = 'Conceptos de almacén',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TEMPRESA_ALMACENESCONCEPTOS
    )


class TEMPRESA_PRODCATEGORIAS:
    """ Definición de la tabla de tempresa_prodcategorias """

    @classmethod
    def get_padres_ids(
            cls,
            s_prodcategoria_id
            ):
        _dbRows = dbc01(
            (dbc01.tempresa_prodcategorias.id == s_prodcategoria_id)
            ).select(
                dbc01.tempresa_prodcategorias.id, 
                dbc01.tempresa_prodcategorias.padre_id, 
                )
        
        _L_prodCategorias_padres_ids = [s_prodcategoria_id]
        
        _n_timeout = 100  # Limite de niveles a buscar
        while _dbRows and (_n_timeout > 0):
            _n_timeout -= 1
            _dbRow = _dbRows.first()
            if _dbRow.padre_id:
                _L_prodCategorias_padres_ids.append(_dbRow.padre_id)
            else:
                break
            _dbRows = dbc01(
                (dbc01.tempresa_prodcategorias.id == _dbRow.padre_id)
                ).select(
                    dbc01.tempresa_prodcategorias.id, 
                    dbc01.tempresa_prodcategorias.padre_id, 
                    )
        
        return _L_prodCategorias_padres_ids
    
    pass


dbc01.define_table(
    'tempresa_prodcategorias',
    Field(
        'empresa_id', dbc01.tempresas, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Empresa', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'nombrecorto', 'string', length = 25, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Nombre Corto', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'padre_id', 'integer', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Estructura padre', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield_tree(v, r, dbc01.tempresa_prodcategorias.padre_id)
        ),
    Field(
        'orden', 'integer', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputInteger, label = 'Orden', comment = None,
        writable = True, readable = True,
        represent = stv_represent_number
        ),
    Field(
        'mostrarenpaginaweb', 'boolean', default = request.vars.get("mostrarenpaginaweb", 0),
        required = False,
        notnull = False,
        widget = stv_widget_inputCheckbox, label = 'Mostrar en página web',
        comment = "Seleccionar si se desea que esta categoría sea mostrada en la página web.",
        writable = True, readable = True,
        represent = stv_represent_boolean
        ),
    # >> Configuraciones especificas por cliente plugin: CASCOS
    Field(
        'uso_cascos1a1', 'boolean', default = False,
        required = False,
        notnull = False,
        widget = stv_widget_inputCheckbox, label = 'Clasificación para cascos 1 a 1',
        comment = 'Plugin para uso de Cascos de acumuladores en Productos.',
        writable = True, readable = True,
        represent = stv_represent_boolean
        ),
    # <<
    Field(
        'porcentajeutilidad', 'decimal(4,2)', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputPercentage, label = 'Porcentaje de utilidad', comment = None,
        writable = True, readable = True,
        represent = stv_represent_percentage
        ),
    tSignature_dbc01,
    format = '%(id)s: %(nombrecorto)s',
    singular = 'Categoria',
    plural = 'Categorias',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_OPERADORES:
    """ Operadores automotores de la empresa

    """

    @staticmethod
    def DBTABLA(s_id = None):
        if s_id is None:
            return dbc01.tempresa_operadores
        else:
            return dbc01.tempresa_operadores(s_id)

    class E_TIPOLICENCIA:
        NO_DEFINIDO = 0
        AUTOMOVILISTA = 1
        CHOFER = 2
        CHOFER_FEDERAL_TIPO_E = 3
        OTRO = 4
        _D_dict = None

        @classmethod
        def GET_DICT(cls):
            if not cls._D_dict:
                cls._D_dict = {
                    cls.NO_DEFINIDO          : 'No Definido',
                    cls.AUTOMOVILISTA        : 'Automovilista',
                    cls.CHOFER               : 'Chofer',
                    cls.CHOFER_FEDERAL_TIPO_E: 'Chofer Federal Tipo E',
                    cls.OTRO                 : 'Otro',
                    }
            else:
                pass
            return cls._D_dict

    @classmethod
    def PUEDE_CREAR(cls):
        """ Determina si puede o no crear un registro.

        Actualmente no hay condición requerida.
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        return _D_return

    @classmethod
    def PUEDE_EDITAR(cls, x_operador):
        """ Define si es posible editar un registro
        """
        _ = x_operador
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        # _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_operador, cls.DBTABLA())
        # n_id = _D_results.n_id
        # dbRow = _D_results.dbRow

        # if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
        #     _D_return.E_return = _D_results.E_return
        #     _D_return.s_msgError = _D_results.s_msgError
        #
        # else:
        #     pass

        return _D_return

    @classmethod
    def PUEDE_ELIMINAR(cls, x_operador):
        """ Determina si puede o no eliminar un registro

        Misma condición que puede_editar

        """
        # _D_return = Storage(
        #     E_return = CLASS_e_RETURN.OK,
        #     s_msgError = "",
        #     )

        return cls.PUEDE_EDITAR(x_operador)

    @classmethod
    def CONFIGURACAMPOS_NUEVO(
            cls,
            s_operador_base_id,
            s_empresa_id,
            ):
        """ Configura los campos cuando se intente crear un registro

        @param s_operador_base_id:
        @type s_operador_base_id:
        @param s_empresa_id:
        @type s_empresa_id:
        @return:
        @rtype:
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTable = cls.DBTABLA()

        _D_results = cls.PUEDE_CREAR()

        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.E_return = _D_results.E_return
            _D_return.s_msgError = _D_results.s_msgError

        elif s_operador_base_id:
            _dbRow_base = _dbTable(s_operador_base_id)

            # _dbTable.empresa_id.default = _dbRow_base.empresa_id
            _dbTable.nombrecorto.default = _dbRow_base.nombrecorto
            _dbTable.rfc.default = _dbRow_base.rfc
            # _dbTable.numlicencia.default = _dbRow_base.numlicencia
            _dbTable.tipolicencia.default = _dbRow_base.tipolicencia
            # _dbTable.fecha_finvigencia.default = _dbRow_base.fecha_finvigencia
            _dbTable.nombre.default = _dbRow_base.nombre
            _dbTable.numregidtriboperador.default = _dbRow_base.numregidtriboperador
            _dbTable.residencia_pais_id.default = _dbRow_base.residencia_pais_id
            _dbTable.dom_calle.default = _dbRow_base.dom_calle
            _dbTable.dom_numexterior.default = _dbRow_base.dom_numexterior
            _dbTable.dom_numinterior.default = _dbRow_base.dom_numinterior
            _dbTable.dom_colonia.default = _dbRow_base.dom_colonia
            _dbTable.dom_localidad.default = _dbRow_base.dom_localidad
            _dbTable.dom_referencia.default = _dbRow_base.dom_referencia
            _dbTable.dom_municipio.default = _dbRow_base.dom_municipio
            _dbTable.dom_pais_estado_id.default = _dbRow_base.dom_municipio
            _dbTable.dom_pais_id.default = _dbRow_base.dom_pais_id
            _dbTable.dom_codigopostal.default = _dbRow_base.dom_codigopostal
            # _dbTable.usuario_id.default = _dbRow_base.usuario_id

        else:
            pass

        _dbTable.empresa_id.writable = False
        _dbTable.nombrecorto.writable = True
        _dbTable.rfc.writable = True
        _dbTable.numlicencia.writable = True
        _dbTable.tipolicencia.writable = True
        _dbTable.fecha_finvigencia.writable = True
        _dbTable.nombre.writable = True
        _dbTable.numregidtriboperador.writable = True  # En caso de operador extranjero
        _dbTable.residencia_pais_id.writable = True  # En caso de operador extranjero
        _dbTable.dom_calle.writable = True
        _dbTable.dom_numexterior.writable = True
        _dbTable.dom_numinterior.writable = True
        _dbTable.dom_colonia.writable = True
        _dbTable.dom_localidad.writable = True
        _dbTable.dom_referencia.writable = True
        _dbTable.dom_municipio.writable = True
        _dbTable.dom_pais_estado_id.writable = True
        _dbTable.dom_pais_id.writable = True
        _dbTable.dom_codigopostal.writable = True
        _dbTable.usuario_id.writable = True

        # Campos que no deben ser editados, solo de forma autocalculada
        # No hay

        # Campos que todos los movimientos pueden editar
        _dbTable.notas.writable = True

        # Se definen los valores por default
        _dbTable.empresa_id.default = s_empresa_id
        # _dbTable.nombrecorto.
        # _dbTable.rfc.default = ""
        # _dbTable.numlicencia.default = ""
        # _dbTable.tipolicencia.default = TEMPRESA_OPERADORES.E_TIPOLICENCIA.CHOFER
        # _dbTable.fecha_finvigencia.default = ""
        # _dbTable.nombre.default = ""
        # _dbTable.numregidtriboperador.default = ""
        _dbTable.residencia_pais_id.default = TPAISES.MEX
        # _dbTable.dom_calle.default = ""
        # _dbTable.dom_numexterior.default = ""
        # _dbTable.dom_numinterior.default = ""
        # _dbTable.dom_colonia.default = ""
        # _dbTable.dom_localidad.default = ""
        # _dbTable.dom_referencia.default = ""
        # _dbTable.dom_municipio.default = ""
        _dbTable.dom_pais_estado_id.default = TPAIS_ESTADOS.SIN
        _dbTable.dom_pais_id.default = TPAISES.MEX
        # _dbTable.dom_codigopostal.default = ""
        # _dbTable.usuario_id.default = None

        return _D_return

    @classmethod
    def CONFIGURACAMPOS_EDICION(
            cls,
            x_operador,
            ):
        """ Dependiendo de condiciones, configura los campos que se pueden editar

        @param x_operador:
        @type x_operador:
        @return:
        @rtype:
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_operador, cls.DBTABLA())
        _n_id = _D_results.n_id
        _dbRow = _D_results.dbRow

        _D_results = cls.PUEDE_EDITAR(x_operador = _dbRow)

        if _D_results.E_return == CLASS_e_RETURN.OK:
            # Si puede editar regresa un ok y el movimiento esta abierto

            # Se manda vacío el argumento para no agregar defaults a los campos
            _D_results = cls.CONFIGURACAMPOS_NUEVO(
                s_operador_base_id = None,
                s_empresa_id = _dbRow.empresa_id,
                )
            _D_return.E_return = _D_results.E_return
            _D_return.s_msgError = _D_results.s_msgError

        else:
            # De lo contrario en cualquier otro caso, no puede editar nada de esta tabla
            pass

        return _D_return

    @classmethod
    def AL_VALIDAR(
            cls,
            O_form,
            ):
        """ Validar la información para poder grabar registro

        @param O_form:
        @type O_form:
        @return:
        @rtype:
        """
        _dbTable = cls.DBTABLA()

        # Dependiendo si es nuevo o editado, toma los valores a validar
        if not O_form.record_id:
            # Si esta creando el registro:
            # _s_transpinternac = (
            #     O_form.vars.get("transpinternac", None)
            #     or _dbTable.transpinternac.default
            #     )
            #
            # _D_results = TCFDI_COMPCARTAPORTE.PUEDE_CREAR(dbRow_cfdi)
            # if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            #     O_form.errors.id = _D_results.s_msgError
            #
            # else:
            #     pass
            pass
        else:
            # _s_transpinternac = O_form.vars.get("transpinternac", None)
            pass

        # Validaciones

        # if _s_transpinternac == TCFDI_COMPCARTAPORTE.TRANSPINTERNAC.SI:
        #     _s_entradasalidamerc = O_form.vars.get("entradasalidamerc", None)
        #     _s_viaentradasalida_id = O_form.vars.get("viaentradasalida_id", None)
        #     if not _s_entradasalidamerc or not _s_viaentradasalida_id:
        #         O_form.errors.transpinternac = (
        #             "CP114: Si es internacional, debe especificarse si es entrada o salida y la vía"
        #             )
        #     else:
        #         pass
        #
        #
        #
        # else:
        #     O_form.vars.entradasalidamerc = None
        #     O_form.vars.viaentradasalida_id = None

        if not O_form.errors:
            # Si no hay cliente, el autocalculo debe especificar el RFC genérico
            pass
        else:
            pass

        return O_form

    @classmethod
    def AL_ACEPTAR(
            cls,
            O_form,
            ):
        """ Al aceptar el registro deben hacerse los cálculos

        Se hacen todos los cálculos y aceptación de los cambios en el producto

        @param O_form:
        @type O_form:
        @return:
        @rtype:
        """
        _dbRow_operador = cls.DBTABLA(O_form.record_id or O_form.vars.id)

        if _dbRow_operador.residencia_pais_id == TPAISES.MEX:
            _dbRow_operador.numregidtriboperador = ""
            _dbRow_operador.update_record()
        else:
            pass

        return O_form

    @classmethod
    def ANTES_ELIMINAR(cls, L_errorsAction, dbRow):
        """ Antes de eliminar un movimiento de inventario de este tipo

        @param dbRow:
        @type dbRow:
        @param L_errorsAction:
        @type L_errorsAction:
        @return: errores
        @rtype: lista de diccionarios {'msg':'...'}
        """
        _D_results = cls.PUEDE_ELIMINAR(
            x_operador = dbRow
            )

        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            L_errorsAction.append({'msg': _D_results.s_msgError})
        else:
            pass

        return L_errorsAction

    pass


dbc01.define_table(
    'tempresa_operadores',
    Field(
        'empresa_id', dbc01.tempresas, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Empresa', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'nombrecorto', 'string', length = 100, default = "",
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Nombre Corto', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'rfc', 'string', length = 15, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'RFC', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'numlicencia', 'string', length = 50, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Num. Licencia', comment = 'Solo requerido si es operador',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'tipolicencia', 'integer',
        default = TEMPRESA_OPERADORES.E_TIPOLICENCIA.NO_DEFINIDO,
        required = False, requires = IS_IN_SET(
            TEMPRESA_OPERADORES.E_TIPOLICENCIA.GET_DICT(), zero = 0,
            error_message = 'Selecciona'
            ),
        notnull = False, unique = False,
        widget = stv_widget_combobox, label = 'Tipo Licencia', comment = '',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_list(
            v, r, None, TEMPRESA_OPERADORES.E_TIPOLICENCIA.GET_DICT()
            )
        ),
    Field(
        'fecha_finvigencia', 'date', default = None,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha, fin vigencia', comment = '',
        writable = False, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'nombre', 'string', length = 254, default = "",
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Nombre', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'numregidtriboperador', 'string', length = 50, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Num. Id en País',
        comment = (
            'Número de identificación o registro fiscal del país de residencia para los efectos fiscales del '
            + 'operador del autotransporte de carga federal en el que se trasladan los bienes o mercancías, '
            + 'cuando sea residente en el extranjero.'
            ),
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'residencia_pais_id', 'integer', default = TPAISES.MEX,
        required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tpaises.id', db01.tpaises._format)),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'País de residencia',
        comment = 'Residencia fiscal',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tempresa_operadores.residencia_pais_id
            )
        ),

    # Atributos del nodo domicilio
    Field(
        'dom_calle', 'string', length = 200, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Calle', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'dom_numexterior', 'string', length = 55, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Num. Exterior', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'dom_numinterior', 'string', length = 55, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Num. Interior', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'dom_colonia', 'string', length = 120, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Colonia', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'dom_localidad', 'string', length = 120, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Localidad', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'dom_referencia', 'string', length = 250, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Referencia', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'dom_municipio', 'string', length = 120, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Municipio', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'dom_pais_estado_id', 'integer', default = TPAIS_ESTADOS.SIN,
        required = False, requires = IS_IN_DB(db01, 'tpais_estados.id', db01.tpais_estados._format),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda field, value: stv_widget_db_chosen_linked(
            field, value, 1, 'dom_pais_id',
            URL(a = 'app_generales', c = '005genericos_sat', f = 'localidad_pais_estados_ver.json'),
            D_additionalAttributes = Storage(
                linkedTableFieldName = 'pais_id'
                )
            ),
        label = 'Estado', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tempresa_operadores.dom_pais_estado_id
            )
        ),
    Field(
        'dom_pais_id', 'integer', default = TPAISES.MEX,
        required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tpaises.id', db01.tpaises._format)),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Pais',
        comment = 'Residencia fiscal',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tempresa_operadores.dom_pais_id
            )
        ),
    Field(
        'dom_codigopostal', 'string', length = 12, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Código Postal', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'usuario_id', 'integer', default = None,
        required = False, requires = IS_NULL_OR(IS_IN_DB(db, 'auth_user.id', db.auth_user._format)),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_search(
            f, v,
            s_display = db.auth_user._format,
            s_url = URL(a = 'backoffice', c = 'accesos', f = 'usuarios_buscar'),
            D_additionalAttributes = {'reference': db.auth_user.id}
            ),
        label = 'Usuario operador', comment = 'Usuario en sistema',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tempresa_operadores.usuario_id
            )
        ),

    tSignature_dbc01,
    format = '%(nombrecorto)s',
    singular = 'Persona',
    plural = 'Personas',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_TRANSPORTES:
    """ Definición de la tabla de tempresa_transportes """

    @staticmethod
    def DBTABLA(s_id = None):
        if s_id is None:
            return dbc01.tempresa_transportes
        else:
            return dbc01.tempresa_transportes(s_id)

    class E_PERTENENCIA:
        """ Se definen las opciónes del campo """
        NO_DEFINIDO = 0
        PROPIO = 1
        ARRENDADO = 2

        @classmethod
        def GET_DICT(cls):
            return {
                # cls.NO_DEFINIDO: 'No definido', No debe ser opción No definido
                cls.PROPIO     : 'Propio',
                cls.ARRENDADO  : 'Arrendado',
                }

    @classmethod
    def PUEDE_CREAR(cls):
        """ Determina si puede o no crear un registro.

        Actualmente no hay condición requerida.
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        return _D_return

    @classmethod
    def PUEDE_EDITAR(cls, x_transporte):
        """ Define si es posible editar un registro
        """
        _ = x_transporte
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        return _D_return

    @classmethod
    def PUEDE_ELIMINAR(cls, x_transporte):
        """ Determina si puede o no eliminar un registro

        Misma condición que puede_editar

        """

        return cls.PUEDE_EDITAR(x_transporte)

    @classmethod
    def CONFIGURACAMPOS_NUEVO(
            cls,
            s_transporte_base_id,
            s_empresa_id,
            ):
        """ Configura los campos cuando se intente crear un registro

        @param s_transporte_base_id:
        @type s_transporte_base_id:
        @param s_empresa_id:
        @type s_empresa_id:
        @return:
        @rtype:
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTable = cls.DBTABLA()

        _D_results = cls.PUEDE_CREAR()

        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_results)

        elif s_transporte_base_id:
            _dbRow_base = _dbTable(s_transporte_base_id)

            # _dbTable.empresa_id.default = _dbRow_base.empresa_id
            _dbTable.nombrecorto.default = _dbRow_base.nombrecorto
            _dbTable.pertenencia.default = _dbRow_base.pertenencia
            _dbTable.idvehic_auto_configautotransporte_id.default = _dbRow_base.idvehic_auto_configautotransporte_id
            _dbTable.idvehic_placavm.default = _dbRow_base.idvehic_placavm
            _dbTable.idvehic_aniomodelovm.default = _dbRow_base.idvehic_aniomodelovm
            _dbTable.numtarjetacirculacion.default = _dbRow_base.numtarjetacirculacion
            _dbTable.numtarjetacirculacion_vigencia.default = _dbRow_base.numtarjetacirculacion_vigencia
            _dbTable.marca_id.default = _dbRow_base.marca_id
            _dbTable.empresa_proveedor_id.default = _dbRow_base.empresa_proveedor_id
            _dbTable.nombreaseg.default = _dbRow_base.nombreaseg
            _dbTable.numpolizaseguro.default = _dbRow_base.numpolizaseguro
            _dbTable.numpolizaseguro_vigencia.default = _dbRow_base.numpolizaseguro_vigencia

            _dbTable.arrendador_rfc.default = _dbRow_base.arrendador_rfc
            _dbTable.arrendador_nombre.default = _dbRow_base.arrendador_nombre
            _dbTable.arrendador_numregidtriboperador.default = _dbRow_base.arrendador_numregidtriboperador
            _dbTable.arrendador_residencia_pais_id.default = _dbRow_base.arrendador_residencia_pais_id
            _dbTable.arrendador_calle.default = _dbRow_base.arrendador_calle
            _dbTable.arrendador_numexterior.default = _dbRow_base.arrendador_numexterior
            _dbTable.arrendador_numinterior.default = _dbRow_base.arrendador_numinterior
            _dbTable.arrendador_colonia.default = _dbRow_base.arrendador_colonia
            _dbTable.arrendador_localidad.default = _dbRow_base.arrendador_localidad
            _dbTable.arrendador_referencia.default = _dbRow_base.arrendador_referencia
            _dbTable.arrendador_municipio.default = _dbRow_base.arrendador_municipio
            _dbTable.arrendador_pais_estado_id.default = _dbRow_base.arrendador_pais_estado_id
            _dbTable.arrendador_pais_id.default = _dbRow_base.arrendador_pais_id
            _dbTable.arrendador_codigopostal.default = _dbRow_base.arrendador_codigopostal

            _dbTable.mismoquearrendador.default = _dbRow_base.mismoquearrendador
            _dbTable.propietario_rfc.default = _dbRow_base.propietario_rfc
            _dbTable.propietario_nombre.default = _dbRow_base.propietario_nombre
            _dbTable.propietario_numregidtriboperador.default = _dbRow_base.propietario_numregidtriboperador
            _dbTable.propietario_residencia_pais_id.default = _dbRow_base.propietario_residencia_pais_id
            _dbTable.propietario_calle.default = _dbRow_base.propietario_calle
            _dbTable.propietario_numexterior.default = _dbRow_base.propietario_numexterior
            _dbTable.propietario_numinterior.default = _dbRow_base.propietario_numinterior
            _dbTable.propietario_colonia.default = _dbRow_base.propietario_colonia
            _dbTable.propietario_localidad.default = _dbRow_base.propietario_localidad
            _dbTable.propietario_referencia.default = _dbRow_base.propietario_referencia
            _dbTable.propietario_municipio.default = _dbRow_base.propietario_municipio
            _dbTable.propietario_pais_estado_id.default = _dbRow_base.propietario_pais_estado_id
            _dbTable.propietario_pais_id.default = _dbRow_base.propietario_pais_id
            _dbTable.propietario_codigopostal.default = _dbRow_base.propietario_codigopostal

        else:
            pass

        # _dbTable.empresa_id.writable = True
        _dbTable.nombrecorto.writable = True
        _dbTable.pertenencia.writable = True
        _dbTable.idvehic_auto_configautotransporte_id.writable = True
        _dbTable.idvehic_placavm.writable = True
        _dbTable.idvehic_aniomodelovm.writable = True
        _dbTable.numtarjetacirculacion.writable = True
        _dbTable.numtarjetacirculacion_vigencia.writable = True
        _dbTable.marca_id.writable = True
        _dbTable.empresa_proveedor_id.writable = True
        _dbTable.nombreaseg.writable = True
        _dbTable.numpolizaseguro.writable = True
        _dbTable.numpolizaseguro_vigencia.writable = True

        _dbTable.arrendador_rfc.writable = True
        _dbTable.arrendador_nombre.writable = True
        _dbTable.arrendador_numregidtriboperador.writable = True
        _dbTable.arrendador_residencia_pais_id.writable = True
        _dbTable.arrendador_calle.writable = True
        _dbTable.arrendador_numexterior.writable = True
        _dbTable.arrendador_numinterior.writable = True
        _dbTable.arrendador_colonia.writable = True
        _dbTable.arrendador_localidad.writable = True
        _dbTable.arrendador_referencia.writable = True
        _dbTable.arrendador_municipio.writable = True
        _dbTable.arrendador_pais_estado_id.writable = True
        _dbTable.arrendador_pais_id.writable = True
        _dbTable.arrendador_codigopostal.writable = True

        _dbTable.mismoquearrendador.writable = True
        _dbTable.propietario_rfc.writable = True
        _dbTable.propietario_nombre.writable = True
        _dbTable.propietario_numregidtriboperador.writable = True
        _dbTable.propietario_residencia_pais_id.writable = True
        _dbTable.propietario_calle.writable = True
        _dbTable.propietario_numexterior.writable = True
        _dbTable.propietario_numinterior.writable = True
        _dbTable.propietario_colonia.writable = True
        _dbTable.propietario_localidad.writable = True
        _dbTable.propietario_referencia.writable = True
        _dbTable.propietario_municipio.writable = True
        _dbTable.propietario_pais_estado_id.writable = True
        _dbTable.propietario_pais_id.writable = True
        _dbTable.propietario_codigopostal.writable = True

        # Campos que no deben ser editados, solo de forma autocalculada
        # No hay

        # Campos que todos los movimientos pueden editar
        _dbTable.notas.writable = True

        # Se definen los valores por default
        _dbTable.empresa_id.default = s_empresa_id
        # _dbTable.nombrecorto.default = ""
        # _dbTable.pertenencia.default = TEMPRESA_TRANSPORTES.E_PERTENENCIA.PROPIO
        # _dbTable.idvehic_auto_configautotransporte_id.default = None
        # _dbTable.idvehic_placavm.default = ""
        # _dbTable.idvehic_aniomodelovm.default = 0
        # _dbTable.numtarjetacirculacion.default = ""
        # _dbTable.numtarjetacirculacion_vigencia.default = None
        # _dbTable.marca_id.default = None
        # _dbTable.empresa_proveedor_id.default = None
        # _dbTable.nombreaseg.default = ""
        # _dbTable.numpolizaseguro.default = ""
        # _dbTable.numpolizaseguro_vigencia.default = ""

        # No aplica defaults para información de arrendador y propietario

        return _D_return

    @classmethod
    def CONFIGURACAMPOS_EDICION(
            cls,
            x_transporte,
            ):
        """ Dependiendo de condiciones, configura los campos que se pueden editar

        @param x_transporte:
        @type x_transporte:
        @return:
        @rtype:
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_transporte, cls.DBTABLA())
        _dbRow = _D_results.dbRow

        _D_results = cls.PUEDE_EDITAR(x_transporte = _dbRow)

        if _D_results.E_return == CLASS_e_RETURN.OK:
            # Si puede editar regresa un ok y el movimiento esta abierto

            # Se manda vacío el argumento para no agregar defaults a los campos
            _D_results = cls.CONFIGURACAMPOS_NUEVO(
                s_transporte_base_id = None,
                s_empresa_id = _dbRow.empresa_id,
                )
            _D_return.E_return = _D_results.E_return
            _D_return.s_msgError = _D_results.s_msgError

        else:
            # De lo contrario en cualquier otro caso, no puede editar nada de esta tabla.
            #  Todos los campos son writable = False
            pass

        return _D_return

    @classmethod
    def AL_VALIDAR(
            cls,
            O_form,
            ):
        """ Validar la información para poder grabar registro

        @param O_form:
        @type O_form:
        @return:
        @rtype:
        """
        _dbTable = cls.DBTABLA()

        _D_camposAChecar = Storage()  # Define los campos que deben ser verificados

        # Dependiendo si es nuevo o editado, toma los valores a validar
        if not O_form.record_id:
            # Si esta creando el registro:

            _D_results = cls.PUEDE_CREAR()
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                O_form.errors.id = _D_results.s_msgError
            else:
                pass

            _D_camposAChecar.nombrecorto = (
                O_form.vars.get("nombrecorto", _dbTable.nombrecorto.default)
                )

        else:
            _dbRow_transporte = _dbTable(O_form.record_id)

            _D_results = cls.PUEDE_EDITAR(
                x_transporte = _dbRow_transporte,
                )
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                O_form.errors.id = _D_results.s_msgError
            else:
                pass

            _D_camposAChecar.nombrecorto = (
                O_form.vars.get("nombrecorto", _dbRow_transporte.nombrecorto)
                )

        # Validaciones

        if not _D_camposAChecar.nombrecorto:
            O_form.errors.nombrecorto = "Requerido"
        else:
            pass

        if not O_form.errors:
            # Si no hay cliente, el autocalculo debe especificar el RFC genérico
            pass
        else:
            pass

        return O_form

    @classmethod
    def AL_ACEPTAR(
            cls,
            O_form,
            ):
        """ Al aceptar el registro deben hacerse los cálculos

        Se hacen todos los cálculos y aceptación de los cambios en el producto

        @param O_form:
        @type O_form:
        @return:
        @rtype:
        """
        _dbRow_transporte = cls.DBTABLA(O_form.record_id or O_form.vars.id)

        _D_results = cls.MAPEAR_CAMPOS(
            x_transporte = _dbRow_transporte
            )

        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            O_form.errors.id = _D_results.s_msgError

        elif _dbRow_transporte.pertenencia != TEMPRESA_TRANSPORTES.E_PERTENENCIA.ARRENDADO:
            _dbRow_transporte.empresa_proveedor_id = None

            _dbRow_transporte.arrendador_rfc = ""
            _dbRow_transporte.arrendador_nombre = ""
            _dbRow_transporte.arrendador_numregidtriboperador = ""
            _dbRow_transporte.arrendador_residencia_pais_id = None
            _dbRow_transporte.arrendador_calle = ""
            _dbRow_transporte.arrendador_numexterior = ""
            _dbRow_transporte.arrendador_numinterior = ""
            _dbRow_transporte.arrendador_colonia = ""
            _dbRow_transporte.arrendador_localidad = ""
            _dbRow_transporte.arrendador_referencia = ""
            _dbRow_transporte.arrendador_municipio = ""
            _dbRow_transporte.arrendador_pais_estado_id = None
            _dbRow_transporte.arrendador_pais_id = None
            _dbRow_transporte.arrendador_codigopostal = ""
            
            _dbRow_transporte.propietario_rfc = ""
            _dbRow_transporte.propietario_nombre = ""
            _dbRow_transporte.propietario_numregidtriboperador = ""
            _dbRow_transporte.propietario_residencia_pais_id = None
            _dbRow_transporte.propietario_calle = ""
            _dbRow_transporte.propietario_numexterior = ""
            _dbRow_transporte.propietario_numinterior = ""
            _dbRow_transporte.propietario_colonia = ""
            _dbRow_transporte.propietario_localidad = ""
            _dbRow_transporte.propietario_referencia = ""
            _dbRow_transporte.propietario_municipio = ""
            _dbRow_transporte.propietario_pais_estado_id = None
            _dbRow_transporte.propietario_pais_id = None
            _dbRow_transporte.propietario_codigopostal = ""

            _dbRow_transporte.update_record()

        else:
            pass

        return O_form

    @classmethod
    def ANTES_ELIMINAR(
            cls,
            L_errorsAction,
            dbRow
            ):
        """ Antes de eliminar un movimiento de inventario de este tipo

        @param dbRow:
        @type dbRow:
        @param L_errorsAction:
        @type L_errorsAction:
        @return: errores
        @rtype: lista de diccionarios {'msg':'...'}
        """
        _D_results = cls.PUEDE_ELIMINAR(
            x_transporte = dbRow
            )
        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            L_errorsAction.append({'msg': _D_results.s_msgError})
        else:
            pass

        return L_errorsAction

    @classmethod
    def MAPEAR_CAMPOS(
            cls,
            x_transporte
            ):
        """ Mapea la información del registro, autollenado de campos automaticos

        @param x_transporte:
        @type x_transporte:
        @return:
        @rtype:
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(
            x_transporte,
            cls.DBTABLA()
            )
        _dbRow_transporte = _D_results.dbRow

        if (
                (_dbRow_transporte.pertenencia == TEMPRESA_TRANSPORTES.E_PERTENENCIA.ARRENDADO)
                and _dbRow_transporte.mismoquearrendador
                ):
            _dbRow_transporte.propietario_rfc = _dbRow_transporte.arrendador_rfc
            _dbRow_transporte.propietario_nombre = _dbRow_transporte.arrendador_nombre
            _dbRow_transporte.propietario_numregidtriboperador = _dbRow_transporte.arrendador_numregidtriboperador
            _dbRow_transporte.propietario_residencia_pais_id = _dbRow_transporte.arrendador_residencia_pais_id
            _dbRow_transporte.propietario_calle = _dbRow_transporte.arrendador_calle
            _dbRow_transporte.propietario_numexterior = _dbRow_transporte.arrendador_numexterior
            _dbRow_transporte.propietario_numinterior = _dbRow_transporte.arrendador_numinterior
            _dbRow_transporte.propietario_colonia = _dbRow_transporte.arrendador_colonia
            _dbRow_transporte.propietario_localidad = _dbRow_transporte.arrendador_localidad
            _dbRow_transporte.propietario_referencia = _dbRow_transporte.arrendador_referencia
            _dbRow_transporte.propietario_municipio = _dbRow_transporte.arrendador_municipio
            _dbRow_transporte.propietario_pais_estado_id = _dbRow_transporte.arrendador_pais_estado_id
            _dbRow_transporte.propietario_pais_id = _dbRow_transporte.arrendador_pais_id
            _dbRow_transporte.propietario_codigopostal = _dbRow_transporte.arrendador_codigopostal

            _dbRow_transporte.update_record()

        else:
            pass

        return _D_return

    pass


dbc01.define_table(
    'tempresa_transportes',
    Field(
        'empresa_id', dbc01.tempresas, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Empresa', comment = None,
        writable = False, readable = True,
        represent = None
        ),
    Field(
        'nombrecorto', 'string', length = 100, default = "",
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Nombre Corto', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'pertenencia', 'integer', default = TEMPRESA_TRANSPORTES.E_PERTENENCIA.PROPIO,
        required = True,
        requires = IS_IN_SET(TEMPRESA_TRANSPORTES.E_PERTENENCIA.GET_DICT(), zero = 0, error_message = 'Selecciona'),
        notnull = False, unique = False,
        widget = stv_widget_combobox,
        label = 'Propietario', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_TRANSPORTES.E_PERTENENCIA.GET_DICT())
        ),
    Field(
        'idvehic_auto_configautotransporte_id', 'integer', default = None,
        required = False, requires = IS_NULL_OR(
            IS_IN_DB(db01, 'tauto_configautotransportes.id', db01.tauto_configautotransportes._format)
            ),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Conf. Vehicular', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tempresa_transportes.idvehic_auto_configautotransporte_id
            )
        ),
    Field(
        'idvehic_placavm', 'string', length = 60, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Placa Vehicular', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'idvehic_aniomodelovm', 'integer',
        required = False, default = int(request.browsernow.year),
        notnull = False, unique = False,
        widget = stv_widget_inputYear, label = 'Año Vehículo', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'numtarjetacirculacion', 'string', length = 100, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Num. Tarj. Circulación', comment = 'Solo requerido si es operador',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'numtarjetacirculacion_vigencia', 'date', default = None,
        required = False, requires = IS_NULL_OR(IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE)),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Num. Tarj. Cir. vigencia', comment = '',
        writable = False, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'marca_id', dbc01.tmarcas, default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Marca', comment = None,
        writable = False, readable = True,
        represent = stv_represent_referencefield
        ),
    Field(
        'empresa_proveedor_id', 'integer', default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Proveedor',
        comment = 'Proveedor del transporte.',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_transportes.empresa_proveedor_id)
        ),
    Field(
        'nombreaseg', 'string', length = 100, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Nombre Aseguradora', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'numpolizaseguro', 'string', length = 100, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Num. Poliza Seguro', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'numpolizaseguro_vigencia', 'date', default = None,
        required = False, requires = IS_NULL_OR(IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE)),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Num. Póliza Seguro vigencia', comment = '',
        writable = False, readable = True,
        represent = stv_represent_date
        ),
    
    # En caso de ser arrendado se requiere información del propietario y del arrendador
    Field(
        'arrendador_rfc', 'string', length = 15, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'RFC', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'arrendador_nombre', 'string', length = 254, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Nombre', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'arrendador_numregidtriboperador', 'string', length = 50, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Num. Id en País',
        comment = (
            'Número de identificación o registro fiscal del país de residencia para los efectos fiscales del '
            + 'operador del autotransporte de carga federal en el que se trasladan los bienes o mercancías, '
            + 'cuando sea residente en el extranjero.'
            ),
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'arrendador_residencia_pais_id', 'integer', default = TPAISES.MEX,
        required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tpaises.id', db01.tpaises._format)),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Residencia',
        comment = 'Residencia fiscal',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tcfdi_compcartaporte_figurastransporepersonas.residencia_pais_id
            )
        ),

    Field(
        'arrendador_calle', 'string', length = 200, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Calle', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'arrendador_numexterior', 'string', length = 55, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Num. Exterior', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'arrendador_numinterior', 'string', length = 55, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Num. Interior', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'arrendador_colonia', 'string', length = 120, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Colonia', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'arrendador_localidad', 'string', length = 120, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Localidad', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'arrendador_referencia', 'string', length = 250, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Referencia', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'arrendador_municipio', 'string', length = 120, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Municipio', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'arrendador_pais_estado_id', 'integer', default = TPAIS_ESTADOS.SIN,
        required = False, requires = IS_IN_DB(db01, 'tpais_estados.id', db01.tpais_estados._format),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda field, value: stv_widget_db_chosen_linked(
            field, value, 1, 'arrendador_pais_id',
            URL(a = 'app_generales', c = '005genericos_sat', f = 'localidad_pais_estados_ver.json'),
            D_additionalAttributes = Storage(
                linkedTableFieldName = 'pais_id'
                )
            ),
        label = 'Estado', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tcfdi_compcartaporte_figurastransporepersonas.dom_pais_estado_id
            )
        ),
    Field(
        'arrendador_pais_id', 'integer', default = TPAISES.MEX,
        required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tpaises.id', db01.tpaises._format)),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Pais',
        comment = 'Residencia fiscal',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tcfdi_compcartaporte_figurastransporepersonas.dom_pais_id
            )
        ),
    Field(
        'arrendador_codigopostal', 'string', length = 12, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Código Postal', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),

    # En caso de ser arrendado se requiere información del propietario y del arrendador
    Field(
        'mismoquearrendador', 'boolean', default = True,
        required = False,
        notnull = False,
        widget = stv_widget_inputCheckbox, label = 'Misma info. Arrendador',
        comment = "Seleccionar si se desea usar la misma información que el arrendador",
        writable = False, readable = True,
        represent = stv_represent_boolean
        ),
    Field(
        'propietario_rfc', 'string', length = 15, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'RFC', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'propietario_nombre', 'string', length = 254, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Nombre', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'propietario_numregidtriboperador', 'string', length = 50, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Num. Id en País',
        comment = (
            'Número de identificación o registro fiscal del país de residencia para los efectos fiscales del '
            + 'operador del autotransporte de carga federal en el que se trasladan los bienes o mercancías, '
            + 'cuando sea residente en el extranjero.'
            ),
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'propietario_residencia_pais_id', 'integer', default = TPAISES.MEX,
        required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tpaises.id', db01.tpaises._format)),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Residencia',
        comment = 'Residencia fiscal',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tcfdi_compcartaporte_figurastransporepersonas.residencia_pais_id
            )
        ),

    Field(
        'propietario_calle', 'string', length = 200, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Calle', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'propietario_numexterior', 'string', length = 55, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Num. Exterior', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'propietario_numinterior', 'string', length = 55, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Num. Interior', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'propietario_colonia', 'string', length = 120, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Colonia', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'propietario_localidad', 'string', length = 120, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Localidad', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'propietario_referencia', 'string', length = 250, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Referencia', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'propietario_municipio', 'string', length = 120, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Municipio', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'propietario_pais_estado_id', 'integer', default = TPAIS_ESTADOS.SIN,
        required = False, requires = IS_IN_DB(db01, 'tpais_estados.id', db01.tpais_estados._format),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda field, value: stv_widget_db_chosen_linked(
            field, value, 1, 'propietario_pais_id',
            URL(a = 'app_generales', c = '005genericos_sat', f = 'localidad_pais_estados_ver.json'),
            D_additionalAttributes = Storage(
                linkedTableFieldName = 'pais_id'
                )
            ),
        label = 'Estado', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tcfdi_compcartaporte_figurastransporepersonas.dom_pais_estado_id
            )
        ),
    Field(
        'propietario_pais_id', 'integer', default = TPAISES.MEX,
        required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tpaises.id', db01.tpaises._format)),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Pais',
        comment = 'Residencia fiscal',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tcfdi_compcartaporte_figurastransporepersonas.dom_pais_id
            )
        ),
    Field(
        'propietario_codigopostal', 'string', length = 12, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Código Postal', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    
    tSignature_dbc01,
    format = lambda r: '%s: %s' % (TEMPRESA_TRANSPORTES.E_PERTENENCIA.GET_DICT()[r.pertenencia], r.nombrecorto),
    singular = 'Transporte',
    plural = 'Transportes',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )
STV_FWK_LIB.DEFINIR_INTEGRIDADREFERENCIAL(
    dbTabla_referenciada = dbc01.tempresa_proveedores,
    dbCampo_haceReferencia = dbc01.tempresa_transportes.empresa_proveedor_id,
    b_actualizarTipo = True,
    dbQry = dbc01.tempresa_proveedores.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id
    )


class TEMP_TRA_PERMISOS:
    """ Definición de la tabla de temp_tra_permisos """

    @staticmethod
    def DBTABLA(s_id = None):
        if s_id is None:
            return dbc01.temp_tra_permisos
        else:
            return dbc01.temp_tra_permisos(s_id)

    @classmethod
    def PUEDE_CREAR(cls):
        """ Determina si puede o no crear un registro.

        Actualmente no hay condición requerida.
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        return _D_return

    @classmethod
    def PUEDE_EDITAR(cls, x_permiso):
        """ Define si es posible editar un registro
        """
        _ = x_permiso
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        return _D_return

    @classmethod
    def PUEDE_ELIMINAR(cls, x_permiso):
        """ Determina si puede o no eliminar un registro

        Misma condición que puede_editar

        """

        return cls.PUEDE_EDITAR(x_permiso)

    @classmethod
    def CONFIGURACAMPOS_NUEVO(
            cls,
            s_permiso_base_id,
            x_transporte,
            ):
        """ Configura los campos cuando se intente crear un registro

        @param s_permiso_base_id:
        @type s_permiso_base_id:
        @param x_transporte:
        @type x_transporte:
        @return:
        @rtype:
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTable = cls.DBTABLA()

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_transporte, dbc01.tempresa_transportes)
        _dbRow_transporte = _D_results.dbRow

        _D_results = cls.PUEDE_CREAR()

        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_results)

        elif s_permiso_base_id:
            _dbRow_base = _dbTable(s_permiso_base_id)

            # _dbTable.emp_transporte_id.default = _dbRow_base.emp_transporte_id
            _dbTable.descripcion.default = _dbRow_base.descripcion
            _dbTable.transporte_tipopermiso_id.default = _dbRow_base.transporte_tipopermiso_id
            _dbTable.numpermisosct.default = _dbRow_base.numpermisosct
            _dbTable.numpermisosct_vigencia.default = _dbRow_base.numpermisosct_vigencia

        else:
            pass

        # _dbTable.emp_transporte_id.writable = True
        _dbTable.descripcion.writable = True
        _dbTable.transporte_tipopermiso_id.writable = True
        _dbTable.numpermisosct.writable = True
        _dbTable.numpermisosct_vigencia.writable = True

        # Campos que no deben ser editados, solo de forma autocalculada
        # No hay

        # Campos que todos los movimientos pueden editar
        _dbTable.notas.writable = True

        # Se definen los valores por default
        _dbTable.emp_transporte_id.default = _dbRow_transporte.id
        # _dbTable.descripcion.default = ""
        # _dbTable.transporte_tipopermiso_id.default = None
        # _dbTable.numpermisosct.default = ""
        # _dbTable.numpermisosct_vigencia.default = None

        return _D_return

    @classmethod
    def CONFIGURACAMPOS_EDICION(
            cls,
            x_permiso,
            ):
        """ Dependiendo de condiciones, configura los campos que se pueden editar

        @param x_permiso:
        @type x_permiso:
        @return:
        @rtype:
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_permiso, cls.DBTABLA())
        _dbRow = _D_results.dbRow

        _D_results = cls.PUEDE_EDITAR(x_permiso = _dbRow)

        if _D_results.E_return == CLASS_e_RETURN.OK:
            # Si puede editar regresa un ok y el movimiento esta abierto

            # Se manda vacío el argumento para no agregar defaults a los campos
            _D_results = cls.CONFIGURACAMPOS_NUEVO(
                s_permiso_base_id = None,
                x_transporte = _dbRow.emp_transporte_id,
                )
            _D_return.E_return = _D_results.E_return
            _D_return.s_msgError = _D_results.s_msgError

        else:
            # De lo contrario en cualquier otro caso, no puede editar nada de esta tabla.
            #  Todos los campos son writable = False
            pass

        return _D_return

    @classmethod
    def AL_VALIDAR(
            cls,
            O_form,
            ):
        """ Validar la información para poder grabar registro

        @param O_form:
        @type O_form:
        @return:
        @rtype:
        """
        _dbTable = cls.DBTABLA()

        _D_camposAChecar = Storage()  # Define los campos que deben ser verificados

        # Dependiendo si es nuevo o editado, toma los valores a validar
        if not O_form.record_id:
            # Si esta creando el registro:

            _D_results = cls.PUEDE_CREAR()
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                O_form.errors.id = _D_results.s_msgError
            else:
                pass

            _D_camposAChecar.numpermisosct = (
                O_form.vars.get("numpermisosct", _dbTable.numpermisosct.default)
                )

        else:
            _dbRow_permiso = _dbTable(O_form.record_id)

            _D_results = cls.PUEDE_EDITAR(
                x_permiso = _dbRow_permiso,
                )
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                O_form.errors.id = _D_results.s_msgError
            else:
                pass

            _D_camposAChecar.numpermisosct = (
                O_form.vars.get("numpermisosct", _dbRow_permiso.numpermisosct)
                )

        # Validaciones

        if not _D_camposAChecar.numpermisosct:
            O_form.errors.numpermisosct = "Requerido"
        else:
            pass

        if not O_form.errors:
            # Si no hay cliente, el autocalculo debe especificar el RFC genérico
            pass
        else:
            pass

        return O_form

    @classmethod
    def AL_ACEPTAR(
            cls,
            O_form,
            ):
        """ Al aceptar el registro deben hacerse los cálculos

        Se hacen todos los cálculos y aceptación de los cambios en el producto

        @param O_form:
        @type O_form:
        @return:
        @rtype:
        """
        _dbRow_permiso = cls.DBTABLA(O_form.record_id or O_form.vars.id)

        _D_results = cls.MAPEAR_CAMPOS(
            x_permiso = _dbRow_permiso
            )

        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            O_form.errors.id = _D_results.s_msgError

        else:
            pass

        return O_form

    @classmethod
    def ANTES_ELIMINAR(
            cls,
            L_errorsAction,
            dbRow
            ):
        """ Antes de eliminar un movimiento de inventario de este tipo

        @param dbRow:
        @type dbRow:
        @param L_errorsAction:
        @type L_errorsAction:
        @return: errores
        @rtype: lista de diccionarios {'msg':'...'}
        """
        _D_results = cls.PUEDE_ELIMINAR(
            x_permiso = dbRow
            )
        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            L_errorsAction.append({'msg': _D_results.s_msgError})
        else:
            pass

        return L_errorsAction

    @classmethod
    def MAPEAR_CAMPOS(
            cls,
            x_permiso
            ):
        """ Mapea la información del registro, autollenado de campos automaticos

        @param x_permiso:
        @type x_permiso:
        @return:
        @rtype:
        """
        _ = x_permiso
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        # _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_permiso, cls.DBTABLA())
        # _dbRow_transporte = _D_results.dbRow

        return _D_return

    pass


dbc01.define_table(
    'temp_tra_permisos',
    Field(
        'emp_transporte_id', dbc01.tempresa_transportes, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Transporte', comment = None,
        writable = False, readable = True,
        represent = None
        ),
    Field(
        'descripcion', 'string', length = 200, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Descripción', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'transporte_tipopermiso_id', 'integer', default = None,
        required = False, requires = IS_NULL_OR(
            IS_IN_DB(
                db01(db01.ttransporte_tipospermisos.transporte_id == TTRANSPORTES.AUTO),
                'ttransporte_tipospermisos.id',
                db01.ttransporte_tipospermisos._format
                )
            ),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Tipo Permiso',
        comment = 'Identifica el tipo de permiso',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tcfdi_compcartaporte_autotransporte.transporte_tipopermiso_id
            )
        ),
    Field(
        'numpermisosct', 'string', length = 60, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Num. Permiso SCT', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'numpermisosct_vigencia', 'date', default = None,
        required = False, requires = IS_NULL_OR(IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE)),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Num. Permiso vigencia', comment = '',
        writable = False, readable = True,
        represent = stv_represent_date
        ),

    tSignature_dbc01,
    format = lambda r: '%s: %s [%s: %s]' % (
        dbc01.tempresa_transportes(r.emp_transporte_id).nombrecorto, r.descripcion,
        db01.ttransporte_tipospermisos(r.transporte_tipopermiso_id).c_tipopermiso, r.numpermisosct
        ),
    singular = 'Permiso',
    plural = 'Permisos',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )


dbc01.tempresa_prodcategorias.padre_id.type = 'reference tempresa_prodcategorias'
dbc01.tempresa_prodcategorias.padre_id.ondelete = 'NO ACTION'
dbc01.tempresa_prodcategorias.padre_id.requires = IS_IN_DB(
    dbc01(dbc01.tempresa_prodcategorias.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id),
    'tempresa_prodcategorias.id', '%(id)s: %(nombrecorto)s'
    )
dbc01.tempresa_prodcategorias.padre_id.widget = lambda f, v: stv_widget_db_chosen(f, v, 1)

# Se crea la referencia para su uso en los campos
dbc01.tempresa_prodcategorias._referenced_by.append(dbc01.tempresa_prodcategorias.padre_id)
dbc01.tempresa_prodcategorias._references.append(dbc01.tempresa_prodcategorias.padre_id)


class TEMPRESA_PRODCATEGORIA_ESPECIFICACIONES:
    """ Definición de la tabla de tempresa_prodcategoria_especificaciones """

    TIPOCAPTURA = STV_CUSTOM_WIDGET_DYNAMICINPUT_TIPOCAPTURA

    pass


dbc01.define_table(
    'tempresa_prodcategoria_especificaciones',
    Field(
        'categoria_id', dbc01.tempresa_prodcategorias, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Categoria', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'especificacion', 'string', length = 30, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Especificación', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'mensajecaptura', 'string', length = 30, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Mensaje de captura', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'tipocaptura', 'integer', default = 1,
        required = True, requires = IS_IN_SET(
            TEMPRESA_PRODCATEGORIA_ESPECIFICACIONES.TIPOCAPTURA.get_dict(), zero = 0, error_message = 'Selecciona'
            ),
        notnull = True, unique = False,
        widget = stv_widget_combobox, label = 'Tipo de captura',
        comment = 'Selecciona entre libre, Numerico u Opciones',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(
            v, r, None, TEMPRESA_PRODCATEGORIA_ESPECIFICACIONES.TIPOCAPTURA.get_dict()
            )
        ),
    Field(
        'opciones', 'text', default = None,
        required = True,
        notnull = False, unique = False,
        widget = stv_widget_text, label = 'Opciones', comment = 'Separar opciones por líneas (Enters)',
        writable = True, readable = True,
        represent = None
        ),
    Field(
        'capturaobligatoria', 'boolean', default = request.vars.get("capturaobligatoria", 0),
        required = False,
        notnull = False,
        widget = stv_widget_inputCheckbox, label = T('Obligatorio'),
        comment = "Seleccionar si es un campo obligado a llenar o no",
        writable = True, readable = True,
        represent = stv_represent_boolean
        ),
    tSignature_dbc01,
    format = '%(especificacion)s',
    singular = 'Especificación',
    plural = 'Especificaciones',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_PRODCATEGORIA_PROVEEDORES:
    """ Definición de la tabla de tempresa_prodcategoria_proveedores """

    pass


dbc01.define_table(
    'tempresa_prodcategoria_proveedores',
    Field(
        'categoria_id', dbc01.tempresa_prodcategorias, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Categoria', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'empresa_proveedor_id', dbc01.tempresa_proveedores, default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Proveedor', comment = None,
        writable = True, readable = True,
        represent = stv_represent_referencefield
        ),
    Field(
        'comentario', 'text', length = 150, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Comentario', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    tSignature_dbc01,
    format = '%(empresa_proveedor_id)s',
    singular = 'Proveedor',
    plural = 'Proveedores',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_PRODSERV_ESPECIFICACIONES:
    """ Definición de la tabla de tempresa_prodserv_especificaciones """
    pass


dbc01.define_table(
    'tempresa_prodserv_especificaciones',
    Field(
        'prodserv_id', dbc01.tempresa_prodserv, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Producto', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'prodcategoria_especificacion_id', dbc01.tempresa_prodcategoria_especificaciones, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Especificación', comment = None,
        writable = False, readable = True,
        represent = None
        ),
    Field(
        'tipocaptura_texto', 'string', length = 50, default = None,
        # Captura usada para libre, opciones y mutliopciones
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Valor', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'tipocaptura_float', 'decimal(14,4)', default = 0,  # Captura usada para número, moneda y fracción
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputFloat, label = 'Fracción', comment = None,
        writable = False, readable = False,  # Se usa readable cuando se muestra en pantalla
        represent = stv_represent_float
        ),
    Field(
        'tipocaptura_blobtext', 'text', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Texto', comment = None,
        writable = False, readable = False,  # Se usa readable cuando se muestra en pantalla
        represent = stv_represent_text
        ),
    Field(
        'tipocaptura_fecha', 'date', default = None,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha', comment = '',
        writable = False, readable = False,  # Se usa readable cuando se muestra en pantalla
        represent = stv_represent_date
        ),
    Field(
        'tipocaptura_hora', 'time', default = None,
        required = False,
        notnull = False,
        widget = stv_widget_input, label = 'Hora', comment = '',
        writable = False, readable = False,  # Se usa readable cuando se muestra en pantalla
        represent = stv_represent_timeAsText
        ),
    Field(
        'mensajeayuda', 'text', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Mensaje de ayuda',
        comment = 'Mensaje de ayuda para la captura de este dato',
        writable = True, readable = True,
        represent = stv_represent_number
        ),
    tSignature_dbc01,
    format = '%(prodserv_id)s',
    singular = 'Producto Especificacion',
    plural = 'Productos Especificaciones',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_EMBARQUES:
    """ Definición de la tabla de tempresa_embarques """

    @classmethod
    def VALIDACION(cls, O_form):
        """ Validación para los transportes.
        """
        _dbTable = dbc01.tempresa_embarques

        _s_fechainicio = O_form.vars.fechainicio
        _s_tipoembarque = O_form.vars.tipoembarque
        _s_nombrechofer = O_form.vars.chofer
        _s_guiaembarque = O_form.vars.guiaembarque
        _s_empresa_transporte_id = O_form.vars.empresa_transporte_id
        _s_id = O_form.record_id

        _dt_fecha_hoy = datetime.date.today()

        if _s_fechainicio > _dt_fecha_hoy:
            O_form.errors.fechainicio = "La fecha no puede ser mayor al día actual."
        else:
            pass

        if int(_s_tipoembarque) == TEMPRESA_EMBARQUES.TIPO_EMBARQUE.PAQUETERIA:
            if not _s_guiaembarque:
                O_form.errors.guiaembarque = "No puede ir vacía la guía de embarque si se eligió paquetería."
            else:
                pass

        else:
            if not _s_nombrechofer:
                O_form.errors.chofer = "El nombre del chofer no puede ir vacío."
            else:
                pass

        if not _s_empresa_transporte_id:
            O_form.errors.empresa_transporte_id = "No puede ir vacío el transporte."
        else:
            pass

        return O_form

    class TIPO_EMBARQUE:
        """ Se definen las opciónes del campo """
        # Este id debe corresponder a propio en la tabla tempresa_trasnportes
        PROPIO = TEMPRESA_TRANSPORTES.E_PERTENENCIA.PROPIO
        # Este id debe corresponder a proveedor en la tabla tempresa_trasnportes
        TRANSPORTISTA = TEMPRESA_TRANSPORTES.E_PERTENENCIA.ARRENDADO
        PAQUETERIA = 3

        @classmethod
        def GET_DICT(cls):
            return {
                cls.PROPIO       : 'Propio',
                cls.TRANSPORTISTA: 'Transportista',
                cls.PAQUETERIA   : 'Paqueteria'
                }

    class ESTATUS:
        """ Se definen las opciónes del campo """
        ABIERTO = 1
        RUTA = 2
        CERRADO = 3

        @classmethod
        def GET_DICT(cls):
            return {
                cls.ABIERTO: 'Abierto',
                cls.RUTA   : 'En ruta',
                cls.CERRADO: 'Cerrado'
                }

    pass


dbc01.define_table(
    'tempresa_embarques',
    Field(
        'empresa_id', dbc01.tempresas, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Empresa', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'tipoembarque', 'integer', default = TEMPRESA_EMBARQUES.TIPO_EMBARQUE.PROPIO,
        required = True,
        requires = IS_IN_SET(TEMPRESA_EMBARQUES.TIPO_EMBARQUE.GET_DICT(), zero = 0, error_message = 'Selecciona'),
        notnull = True, unique = False,
        widget = stv_widget_combobox, label = 'Tipo de embarque', comment = '',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_EMBARQUES.TIPO_EMBARQUE.GET_DICT())
        ),
    Field(
        'fechainicio', 'date', default = request.browsernow,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha inicio', comment = 'Fecha inicio del embarque.',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'empresa_transporte_id', dbc01.tempresa_transportes, default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda field, value: stv_widget_db_chosen_linked(
            field,
            value,
            1,
            'tipoembarque',
            URL(a = 'app_empresas', c = '120empresas', f = 'empresa_transportes_ver.json'),
            D_additionalAttributes = {
                'linkedTableFieldName': 'propietario'
                }
            ),
        label = 'Transportes', comment = None,
        writable = True, readable = True,
        represent = stv_represent_referencefield
        ),
    Field(
        'chofer', 'string', length = 60, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Nombre del chofer', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'guiaembarque', 'string', length = 20, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Guía de embarque', comment = "Guía de seguimiento del embarque.",
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 200, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Descripción',
        comment = 'En caso de no encontrar el transporte requerido, escribir una breve descripción del vehículo.',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'estatus', 'integer', default = TEMPRESA_EMBARQUES.ESTATUS.ABIERTO,
        required = True,
        requires = IS_IN_SET(TEMPRESA_EMBARQUES.ESTATUS.GET_DICT(), zero = 0, error_message = 'Selecciona'),
        notnull = True, unique = False,
        widget = stv_widget_combobox, label = 'Estatus', comment = '',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_EMBARQUES.ESTATUS.GET_DICT())
        ),
    tSignature_dbc01,
    format = 'Folio: %(id)s, Chofer: %(chofer)s, Guía de embarque: %(guiaembarque)s, Fecha inicio: %(fechainicio)s',
    singular = 'Embarque',
    plural = 'Embarques',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )
dbc01.tempresa_embarques.id.label = 'Folio'


class TEMPRESA_EMBARQUE_ALMACENES:
    """ Definición de la tabla de tempresa_embarque_almacenes """

    @classmethod
    def VALIDACION(cls, O_form):
        """ Validación para los almacenes de embarque.
        """
        _dbTable = dbc01.tempresa_embarque_almacenes

        _s_empresa_embarque_id = request.args(3, None)
        _s_orden = O_form.vars.orden
        _s_id = O_form.record_id

        # Se hace la condición porque _s_orden puede ir vacío
        if _s_orden:
            _dbRows = dbc01(
                (_dbTable.empresa_embarque_id == _s_empresa_embarque_id)
                & (_dbTable.orden == _s_orden)
                & (_dbTable.id != _s_id)  # Se ignora el registro que se esté modificando
                ).select(_dbTable.id)
            if _dbRows:
                O_form.errors.orden = "El número de orden no puede repetirse por embarque."
            else:
                pass
        else:
            pass

        return O_form

    class ESTATUS:
        """ Se definen las opciónes del campo """
        EN_ESPERA = 1
        CARGANDO = 2
        DESCARGANDO = 3
        CERRADO = 4

        @classmethod
        def GET_DICT(cls):
            return {
                cls.EN_ESPERA  : 'En espera',
                cls.CARGANDO   : 'Cargando',
                cls.DESCARGANDO: 'Descargando',
                cls.CERRADO    : 'Cerrado'
                }

    pass


dbc01.define_table(
    'tempresa_embarque_almacenes',
    Field(
        'empresa_embarque_id', dbc01.tempresa_embarques, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Embarque', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'empresa_plaza_sucursal_almacen_id', 'integer', default = None,
        required = True,
        ondelete = 'NO ACTION', notnull = True, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Almacén', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tempresa_embarque_almacenes.empresa_plaza_sucursal_almacen_id
            )
        ),
    Field(
        'orden', 'integer', default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputInteger, label = 'Orden',
        comment = 'Indica el orden en que se harán las cargas de producto en los almacenes.',
        writable = True, readable = True,
        represent = stv_represent_number
        ),
    Field(
        'estatus', 'integer', default = TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.EN_ESPERA,
        required = True,
        requires = IS_IN_SET(TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.GET_DICT(), zero = 0, error_message = 'Selecciona'),
        notnull = True, unique = False,
        widget = stv_widget_combobox, label = 'Estatus', comment = '',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.GET_DICT())
        ),
    Field(
        'fechasalida', 'date', default = request.browsernow,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha estimada de salida',
        comment = 'Fecha estimada en la que saldrá este embarque.',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    tSignature_dbc01,
    format = lambda r: (
        str(r.orden) + ": " + (
            dbc01.tempresa_plaza_sucursal_almacenes._format
            % dbc01.tempresa_plaza_sucursal_almacenes(r.empresa_plaza_sucursal_almacen_id).as_dict()
            )
        ),
    singular = 'Ruta (Almacen)',
    plural = 'Ruta (Almacenes)',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_INVENTARIOMOVIMIENTOS:
    """ Definición de la tabla de tempresa_inventariomovimientos """

    class E_TIPODOCUMENTO:
        """ Se definen las opciónes del campo """
        NO_DEFINIDO = 0
        FACTURA = 1
        REMISION = 2
        OTRO = 3

        @classmethod
        def get_dict(cls):
            return {
                    cls.NO_DEFINIDO: 'No definido',
                    cls.FACTURA: 'Factura',
                    cls.REMISION: 'Remisión',
                    cls.OTRO: 'Otro',
                    }
            
    class ESTATUS_MOVIMIENTO:
        """ Se definen las opciónes del campo """
        NO_DEFINIDO = 0
        NO_APLICA = 1
        CARGANDO = 2
        RUTA = 3
        ENTREGADO = 4
        EN_PROCESO = 5
        CERRADO = 6

        # En caso de embarques el movimiento se mueve como CARGANDO > RUTA > ENTREGADO
        # En caso de movimiento en almacenes se mueve como EN_PROCESO > CERRADO
         
        @classmethod
        def GET_DICT(cls):
            return {
                cls.NO_DEFINIDO: 'Abierto',
                cls.NO_APLICA: 'No aplica',
                cls.CARGANDO: 'Cargando',
                cls.RUTA: 'En ruta',
                cls.ENTREGADO: 'Entregado',
                cls.EN_PROCESO: 'En proceso',
                cls.CERRADO: 'Cerrado',
                }

        @classmethod
        def GET_L_ABIERTO(cls):
            """ Regresa una lista de elementos que identifican al movimiento como abierto, permitiendo hacer cambios

            @return:
            @rtype:
            """
            return [
                cls.NO_DEFINIDO,
                cls.NO_APLICA,
                cls.CARGANDO,
                cls.EN_PROCESO,
                ]

    @staticmethod
    def PUEDE_EDITAR(
            x_empresa_invmovto,
            ):
        """ Función genérica para verificar si el movimiento puede editarse

        @param x_empresa_invmovto:
        @type x_empresa_invmovto:
        @return:
        @rtype:
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTable = dbc01.tempresa_inventariomovimientos
        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_empresa_invmovto, _dbTable)
        _n_invMovimiento = _D_results.n_id
        _dbRow_invMovto = _D_results.dbRow

        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.E_return = _D_results.E_return
            _D_return.s_msgError = _D_results.s_msgError

        elif (
                not _dbRow_invMovto
                or (
                    _dbRow_invMovto.estatus_movimiento not in (
                        TEMPRESA_INVENTARIOMOVIMIENTOS.ESTATUS_MOVIMIENTO.GET_L_ABIERTO()
                        )
                    )
                ):
            _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
            _D_return.s_msgError = "Estatus del movimiento no permite modificaciones"

        else:
            pass

        return _D_return

    class InventarioInicial:
        """ Clase para manejo de un movimiento de tipo inventario inicial

        """

        E_TIPOCONCEPTO = TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.ENTRADA
        E_CLASIFICACION = TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.INVENTARIO_INICIAL

        def __init__(
                self,
                s_empresa_id,
                s_empresa_plaza_sucursal_almacen_id
                ):
            """

            @param s_empresa_id:
            @type s_empresa_id:
            @param s_empresa_plaza_sucursal_almacen_id:
            @type s_empresa_plaza_sucursal_almacen_id:
            """

            self._s_empresa_id = s_empresa_id
            self._s_empresa_plaza_sucursal_almacen_id = s_empresa_plaza_sucursal_almacen_id
            self._dbTable = dbc01.tempresa_inventariomovimientos
            self.n_empresa_almacenconcepto_id = None
            self._D_return = Storage(
                E_return = CLASS_e_RETURN.OK,
                s_msgError = "",
                )

            _D_results = TEMPRESA_INVENTARIOMOVIMIENTOS.IDENTIFICA_ALMACENCONCEPTO(
                s_empresa_id = self._s_empresa_id,
                E_tipoconcepto = self.E_TIPOCONCEPTO,
                E_clasificacion = self.E_CLASIFICACION,
                n_empresa_almacenconcepto_id = None
                )

            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                self._D_return.E_return = _D_results.E_return
                self._D_return.s_msgError = _D_results.s_msgError
            else:
                self.n_empresa_almacenconcepto_id = _D_results.n_empresa_almacenconcepto_id

            return

        # noinspection PyMethodParameters
        def decorador_verifica_condiciones(func):
            """ Decorador que verifica de forma generica que las condiciones se cumplan para la ejecución de la rutina

             @return:
             @rtype:
             """
            def funcion_compuesta(self, *args, **kwargs):
                if self._D_return.E_return > CLASS_e_RETURN.OK_WARNING:
                    return self._D_return
                else:
                    return func(self, *args, **kwargs)

            return funcion_compuesta

        def _puede_crear(self):
            """ Determina si puede o no crear un registro de este tipo de movimiento
            """
            _D_return = Storage(
                E_return = CLASS_e_RETURN.OK,
                s_msgError = "",
                )

            _dbRows_movtos = dbc01(
                (self._dbTable.empresa_plaza_sucursal_almacen_id == self._s_empresa_plaza_sucursal_almacen_id)
                & (self._dbTable.empresa_almacenesconcepto_id != self.n_empresa_almacenconcepto_id)
                ).select(
                    self._dbTable.id,
                    self._dbTable.empresa_almacenesconcepto_id,
                    self._dbTable.estatus_movimiento,
                    orderby = self._dbTable.id
                    )

            if len(_dbRows_movtos) > 0:
                _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                _D_return.s_msgError = "No se puede generar movimiento de inventario inicial, ya que existen otros " \
                                       "movimientos de otros tipos en el almacen [%s]" % \
                                       dbc01.tempresa_almacenesconceptos(_dbRows_movtos.first().id).concepto
            else:
                # Si se puede generar un movimiento de inventario inicial
                pass

            return _D_return

        def puede_editar(
                self,
                x_empresa_invmovto,
                ):
            """

            @param x_empresa_invmovto:
            @type x_empresa_invmovto:
            @return:
            @rtype:
            """
            _D_return = Storage(
                E_return = CLASS_e_RETURN.OK,
                s_msgError = "",
                )

            _D_results = TEMPRESA_INVENTARIOMOVIMIENTOS.PUEDE_EDITAR(x_empresa_invmovto)

            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.E_return = _D_results.E_return
                _D_return.s_msgError = _D_results.s_msgError

            else:
                # Tampoco podrá editarla si se crearon movimientos de otros tipos en el almacen
                _dbRows_movtos = dbc01(
                    (self._dbTable.empresa_plaza_sucursal_almacen_id == self._s_empresa_plaza_sucursal_almacen_id)
                    & (self._dbTable.empresa_almacenesconcepto_id != self.n_empresa_almacenconcepto_id)
                    ).select(
                    self._dbTable.id,
                    self._dbTable.empresa_almacenesconcepto_id,
                    self._dbTable.estatus_movimiento,
                    orderby = self._dbTable.id
                    )

                if len(_dbRows_movtos) > 0:
                    _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                    _D_return.s_msgError = (
                        "No se puede editar movimiento de inventario inicial, ya que existen otros "
                        + "movimientos de otros tipos en el almacen [%s]" %
                        dbc01.tempresa_almacenesconceptos(_dbRows_movtos.first().empresa_almacenesconcepto_id).concepto
                        )
                else:
                    # Si se puede generar un movimiento de inventario inicial
                    pass

                pass

            return _D_return

        def _puede_eliminar(self, x_empresa_invmovto):
            """ Determina si puede o no eliminar un registro de este tipo de movimiento
            """
            _D_return = Storage(
                E_return = CLASS_e_RETURN.OK,
                s_msgError = "",
                )

            _D_results = self.puede_editar(x_empresa_invmovto)

            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.E_return = _D_results.E_return
                _D_return.s_msgError = _D_results.s_msgError

            else:
                _dbRows_movtos = dbc01(
                    (self._dbTable.empresa_plaza_sucursal_almacen_id == self._s_empresa_plaza_sucursal_almacen_id)
                    & (self._dbTable.empresa_almacenesconcepto_id != self.n_empresa_almacenconcepto_id)
                    ).select(
                        self._dbTable.id,
                        self._dbTable.empresa_almacenesconcepto_id,
                        self._dbTable.estatus_movimiento,
                        orderby = self._dbTable.id
                        )

                if len(_dbRows_movtos) > 0:
                    _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                    _D_return.s_msgError = (
                        "No se puede eliminar movimiento de inventario inicial, ya que existen otros "
                        + "movimientos de otros tipos en el almacen [%s]" %
                        dbc01.tempresa_almacenesconceptos(_dbRows_movtos.first().id).concepto
                        )
                else:
                    # Si se puede generar un movimiento de inventario inicial
                    pass

            return _D_return

        @decorador_verifica_condiciones
        def configuraCampos_nuevo(
                self,
                s_empresa_inventariomovimiento_id,
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param s_empresa_inventariomovimiento_id: Si lo tiene definido, copia sus valores por defaulr
            @type s_empresa_inventariomovimiento_id:
            @return:
            @rtype:
            """
            _D_return = Storage(
                E_return = CLASS_e_RETURN.OK,
                s_msgError = "",
                )

            _D_results = self._puede_crear()

            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.E_return = _D_results.E_return
                _D_return.s_msgError = _D_results.s_msgError

            elif s_empresa_inventariomovimiento_id:
                _dbRow_base = self._dbTable(s_empresa_inventariomovimiento_id)

                self._dbTable.empresa_id.default = _dbRow_base.empresa_id
                # self._dbTable.empresa_proveedor_id.default = _dbRow_base.empresa_proveedor_id
                # self._dbTable.empresa_cliente_id.default = _dbRow_base.empresa_cliente_id
                # self._dbTable.tipodocumento.default = _dbRow_base.tipodocumento
                # self._dbTable.remision.default = _dbRow_base.remision
                # self._dbTable.cfdi_id.default = _dbRow_base.cfdi_id
                # self._dbTable.cfdi_egreso_id.default = _dbRow_base.cfdi_egreso_id
                # self._dbTable.total.default = _dbRow_base.total
                # self._dbTable.moneda_id.default = _dbRow_base.moneda_id
                # self._dbTable.tipocambio.default = _dbRow_base.tipocambio
                # self._dbTable.tc.default = _dbRow_base.tc
                # self._dbTable.ordencompra.default = _dbRow_base.ordencompra
                self._dbTable.empresa_plaza_sucursal_almacen_id.default = _dbRow_base.empresa_plaza_sucursal_almacen_id
                # self._dbTable.folio.default = _dbRow_base.folio
                # self._dbTable.empresa_embarque_almacen_id.default = _dbRow_base.empresa_embarque_almacen_id
                # self._dbTable.empresa_embarque_almacen_id_relacionado.default = _dbRow_base.empresa_embarque_almacen_id_relacionado
                # self._dbTable.empresa_embarque_id.default = _dbRow_base.empresa_embarque_id
                # self._dbTable.fecha_salida.default = _dbRow_base.fecha_salida

                # self._dbTable.empresa_almacenesconcepto_id.default = _dbRow_base.empresa_almacenesconcepto_id
                # self._dbTable.fecha_registro.default = _dbRow_base.fecha_registro
                # self._dbTable.inconsistencias.default = _dbRow_base.inconsistencias

                # self._dbTable.observaciones.default = _dbRow_base.observaciones
                # self._dbTable.fecha_movimiento.default = _dbRow_base.fecha_movimiento
                self._dbTable.descripcion.default = _dbRow_base.descripcion

            else:
                pass

            self._dbTable.empresa_proveedor_id.writable = False
            self._dbTable.empresa_cliente_id.writable = False
            self._dbTable.tipodocumento.writable = False
            self._dbTable.remision.writable = False
            self._dbTable.cfdi_id.writable = False
            self._dbTable.cfdi_egreso_id.writable = False
            self._dbTable.total.writable = False
            self._dbTable.moneda_id.writable = False
            self._dbTable.tipocambio.writable = False
            self._dbTable.tc.writable = False
            self._dbTable.ordencompra.writable = False
            self._dbTable.empresa_plaza_sucursal_almacen_id.writable = False
            self._dbTable.folio.writable = False
            self._dbTable.empresa_embarque_almacen_id.writable = False
            self._dbTable.empresa_embarque_almacen_id_relacionado.writable = False
            self._dbTable.empresa_embarque_id.writable = False
            self._dbTable.fecha_salida.writable = False

            # Campos que no deben ser editados, solo de forma autocalculada
            self._dbTable.empresa_almacenesconcepto_id.writable = False
            self._dbTable.fecha_registro.writable = False
            self._dbTable.inconsistencias.writable = False

            # Campos que todos los movimientos pueden editar
            self._dbTable.observaciones.writable = True
            self._dbTable.fecha_movimiento.writable = True
            self._dbTable.descripcion.writable = True

            # Se definen los valores por default
            self._dbTable.empresa_almacenesconcepto_id.default = self.n_empresa_almacenconcepto_id

            return _D_return

        @decorador_verifica_condiciones
        def configuraCampos_edicion(
                self,
                x_empresa_invmovto,
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param x_empresa_invmovto: es el registro del movimiento o su id
            @type x_empresa_invmovto:
            @return:
            @rtype:
            """
            _D_return = Storage(
                E_return = CLASS_e_RETURN.OK,
                s_msgError = "",
                )

            _D_results = self.puede_editar(x_empresa_invmovto)

            if _D_results.E_return == CLASS_e_RETURN.OK:
                # Si puede editar regresa un ok y el movimiento esta abierto

                # Se manda vacío el argumento para no agregar defaults a los campos
                _D_results = self.configuraCampos_nuevo(
                    s_empresa_inventariomovimiento_id = None,
                    )
                _D_return.E_return = _D_results.E_return
                _D_return.s_msgError = _D_results.s_msgError

            else:
                # De lo contrario en cualquier otro caso, solo se pueden editar las observaciones
                self._dbTable.observaciones.writable = True

            return _D_return

        def al_validar(self, O_form):
            """ Validar la información para poder grabar inventario inicial

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            if self._D_return.E_return > CLASS_e_RETURN.OK_WARNING:
                O_form.errors.id = self._D_return.s_msgError

            else:
                # Se actualizan los campos autocalculados
                # En este caso no hay

                if O_form.record_id:
                    # Se esta modificando el registro, la llamada a configuraCampos_edicion ya definió con writable
                    # que puede modificarse y que no.
                    pass

                else:
                    # Se esta creando el registro

                    _D_results = self._puede_crear()
                    if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                        O_form.errors.id = _D_results.s_msgError

                    else:
                        self._dbTable.empresa_almacenesconcepto_id.default = self.n_empresa_almacenconcepto_id

            return O_form

        def al_aceptar(self, O_form):
            """ Aceptación captura de inventario inicial producto

            Se hacen todos los cálculos y aceptación de los cambios en el producto

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            if self._D_return.E_return > CLASS_e_RETURN.OK_WARNING:
                O_form.errors.id = self._D_return.s_msgError
            else:
                pass

            return O_form

        def antes_eliminar(self, L_errorsAction, dbRow):
            """ Antes de eliminar un movimiento de inventario de este tipo

            @param dbRow:
            @type dbRow:
            @param L_errorsAction:
            @type L_errorsAction:
            @return: errores
            @rtype: lista de diccionarios {'msg':'...'}
            """

            if self._D_return.E_return > CLASS_e_RETURN.OK_WARNING:
                L_errorsAction.append({'msg': self._D_return.s_msgError})

            else:
                _D_results = self._puede_eliminar(dbRow)
                if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                    L_errorsAction.append({'msg': _D_results.s_msgError})
                else:
                    pass

            return L_errorsAction

        @decorador_verifica_condiciones
        def tiene_movimiento_activo(self):
            """

            @return:
            @rtype:
            """
            _D_return = Storage(
                E_return = CLASS_e_RETURN.OK,
                s_msgError = "",
                )

            _dbRows_movtosInvInicial = dbc01(
                (self._dbTable.empresa_almacenesconcepto_id == self.n_empresa_almacenconcepto_id)
                & (self._dbTable.empresa_plaza_sucursal_almacen_id == self._s_empresa_plaza_sucursal_almacen_id)
                & (self._dbTable.estatus_movimiento.belongs(TEMPRESA_INVENTARIOMOVIMIENTOS.ESTATUS_MOVIMIENTO.GET_L_ABIERTO()))
                ).select(
                    self._dbTable.id,
                    self._dbTable.estatus_movimiento,
                    orderby = ~self._dbTable.id
                    )

            if _dbRows_movtosInvInicial:
                _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                _D_return.s_msgError = "Existe un movimiento por entrada inventario inicial abierto"
            else:
                pass

            return _D_return

        def checa_inconsistencias(
                self,
                x_empresa_invmovto
                ):
            _D_return = Storage(
                E_return = CLASS_e_RETURN.OK,
                s_msgError = "",
                dbRow_invMovto = None
                )

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_empresa_invmovto, self._dbTable)
            _n_invMovimiento = _D_results.n_id
            _D_return.dbRow_invMovto = _D_results.dbRow

            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.E_return = _D_results.E_return
                _D_return.s_msgError = _D_results.s_msgError

            else:
                pass
            # TODO checar inconsistencias del movimiento como que la suma de los totales de los productos, no cuadran con el total del movimiento
            # que los productos tienen alguna inconsistencia, o falta de campo

            return _D_return

    class PorCompra:
        """ Clase para manejo de un movimiento de tipo inventario inicial

        """

        E_TIPOCONCEPTO = TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.ENTRADA
        E_CLASIFICACION = TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.COMPRA

        def __init__(
                self,
                s_empresa_id,
                s_empresa_plaza_sucursal_almacen_id
                ):
            """

            @param s_empresa_id:
            @type s_empresa_id:
            @param s_empresa_plaza_sucursal_almacen_id:
            @type s_empresa_plaza_sucursal_almacen_id:
            """

            self._s_empresa_id = s_empresa_id
            self._s_empresa_plaza_sucursal_almacen_id = s_empresa_plaza_sucursal_almacen_id
            self._dbTable = dbc01.tempresa_inventariomovimientos
            self.n_empresa_almacenconcepto_id = None
            self._D_return = Storage(
                E_return = CLASS_e_RETURN.OK,
                s_msgError = "",
                )

            _D_results = TEMPRESA_INVENTARIOMOVIMIENTOS.IDENTIFICA_ALMACENCONCEPTO(
                s_empresa_id = self._s_empresa_id,
                E_tipoconcepto = self.E_TIPOCONCEPTO,
                E_clasificacion = self.E_CLASIFICACION,
                n_empresa_almacenconcepto_id = None
                )

            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                self._D_return.E_return = _D_results.E_return
                self._D_return.s_msgError = _D_results.s_msgError
            else:
                self.n_empresa_almacenconcepto_id = _D_results.n_empresa_almacenconcepto_id

            return

        def decorador_verifica_condiciones(func):
            """ Decorador que verifica de forma generica que las condiciones se cumplan para la ejecución de la rutina

             @return:
             @rtype:
             """
            def funcion_compuesta(self, *args, **kwargs):
                if self._D_return.E_return > CLASS_e_RETURN.OK_WARNING:
                    return self._D_return
                else:
                    return func(self, *args, **kwargs)

            return funcion_compuesta

        def _puede_crear(self):
            """ Determina si puede o no crear un registro de este tipo de movimiento
            """
            _D_return = Storage(
                E_return = CLASS_e_RETURN.OK,
                s_msgError = "",
                )

            _O_invInicial = TEMPRESA_INVENTARIOMOVIMIENTOS.InventarioInicial(
                s_empresa_id = self._s_empresa_id,
                s_empresa_plaza_sucursal_almacen_id = self._s_empresa_plaza_sucursal_almacen_id
                )

            _D_result = _O_invInicial.tiene_movimiento_activo()

            if _D_result.E_return >= CLASS_e_RETURN.OK_WARNING:
                _D_return.E_return = _D_result.E_return
                _D_return.s_msgError = "No se puede generar movimiento de entrada por compra, %s " % _D_result.s_msgError
            else:
                # Si se puede generar un movimiento de inventario inicial
                pass

            return _D_return

        def puede_editar(
                self,
                x_empresa_invmovto,
                ):
            """

            @param x_empresa_invmovto:
            @type x_empresa_invmovto:
            @return:
            @rtype:
            """
            _D_return = Storage(
                E_return = CLASS_e_RETURN.OK,
                s_msgError = "",
                )

            _D_results = TEMPRESA_INVENTARIOMOVIMIENTOS.PUEDE_EDITAR(x_empresa_invmovto)

            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.E_return = _D_results.E_return
                _D_return.s_msgError = _D_results.s_msgError

            else:

                pass

            return _D_return

        def _puede_eliminar(self, x_empresa_invmovto):
            """ Determina si puede o no eliminar un registro de este tipo de movimiento
            """
            _D_return = Storage(
                E_return = CLASS_e_RETURN.OK,
                s_msgError = "",
                )

            _D_results = self.puede_editar(x_empresa_invmovto)

            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.E_return = _D_results.E_return
                _D_return.s_msgError = _D_results.s_msgError

            else:
                pass

            return _D_return

        @decorador_verifica_condiciones
        def configuraCampos_nuevo(
                self,
                s_empresa_inventariomovimiento_id,
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param s_empresa_inventariomovimiento_id: Si lo tiene definido, copia sus valores por defaulr
            @type s_empresa_inventariomovimiento_id:
            @return:
            @rtype:
            """
            _D_return = Storage(
                E_return = CLASS_e_RETURN.OK,
                s_msgError = "",
                )

            _D_results = self._puede_crear()

            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.E_return = _D_results.E_return
                _D_return.s_msgError = _D_results.s_msgError

            elif s_empresa_inventariomovimiento_id:
                _dbRow_base = self._dbTable(s_empresa_inventariomovimiento_id)

                self._dbTable.empresa_id.default = _dbRow_base.empresa_id
                self._dbTable.empresa_proveedor_id.default = _dbRow_base.empresa_proveedor_id
                # self._dbTable.empresa_cliente_id.default = _dbRow_base.empresa_cliente_id
                self._dbTable.tipodocumento.default = _dbRow_base.tipodocumento
                # self._dbTable.remision.default = _dbRow_base.remision
                # self._dbTable.cfdi_id.default = _dbRow_base.cfdi_id
                # self._dbTable.cfdi_egreso_id.default = _dbRow_base.cfdi_egreso_id
                # self._dbTable.total.default = _dbRow_base.total
                # self._dbTable.tc.default = _dbRow_base.tc
                # self._dbTable.ordencompra.default = _dbRow_base.ordencompra
                self._dbTable.empresa_plaza_sucursal_almacen_id.default = _dbRow_base.empresa_plaza_sucursal_almacen_id
                # self._dbTable.folio.default = _dbRow_base.folio
                # self._dbTable.empresa_embarque_almacen_id.default = _dbRow_base.empresa_embarque_almacen_id
                # self._dbTable.empresa_embarque_almacen_id_relacionado.default = _dbRow_base.empresa_embarque_almacen_id_relacionado
                # self._dbTable.empresa_embarque_id.default = _dbRow_base.empresa_embarque_id
                # self._dbTable.fecha_salida.default = _dbRow_base.fecha_salida

                # self._dbTable.infoaduaneranopedimento.default = _dbRow_base.infoaduaneranopedimento
                # self._dbTable.moneda_id.default = _dbRow_base.moneda_id
                # self._dbTable.tipocambio.default = _dbRow_base.tipocambio
                # self._dbTable.fechaarrivo.default = _dbRow_base.fechaarrivo
                # self._dbTable.puerto.default = _dbRow_base.puerto
                # self._dbTable.agente.default = _dbRow_base.agente
                # self._dbTable.pais_id.default = _dbRow_base.pais_id



                # self._dbTable.empresa_almacenesconcepto_id.default = _dbRow_base.empresa_almacenesconcepto_id
                # self._dbTable.fecha_registro.default = _dbRow_base.fecha_registro
                # self._dbTable.inconsistencias.default = _dbRow_base.inconsistencias

                # self._dbTable.observaciones.default = _dbRow_base.observaciones
                # self._dbTable.fecha_movimiento.default = _dbRow_base.fecha_movimiento
                self._dbTable.descripcion.default = _dbRow_base.descripcion

            else:
                pass

            self._dbTable.empresa_proveedor_id.writable = True
            self._dbTable.empresa_cliente_id.writable = False
            self._dbTable.tipodocumento.writable = True
            self._dbTable.remision.writable = True
            self._dbTable.cfdi_id.writable = False
            self._dbTable.cfdi_egreso_id.writable = True
            self._dbTable.total.writable = False
            self._dbTable.tc.writable = True
            self._dbTable.ordencompra.writable = True
            self._dbTable.empresa_plaza_sucursal_almacen_id.writable = False
            self._dbTable.folio.writable = True
            self._dbTable.empresa_embarque_almacen_id.writable = False
            self._dbTable.empresa_embarque_almacen_id_relacionado.writable = False
            self._dbTable.empresa_embarque_id.writable = False
            self._dbTable.fecha_salida.writable = False

            self._dbTable.infoaduaneranopedimento.writable = True
            self._dbTable.moneda_id.writable = True
            self._dbTable.tipocambio.writable = True
            self._dbTable.fechaarrivo.writable = True
            self._dbTable.puerto.writable = True
            self._dbTable.agente.writable = True
            self._dbTable.pais_id.writable = True

            # Campos que no deben ser editados, solo de forma autocalculada
            self._dbTable.empresa_almacenesconcepto_id.writable = False
            self._dbTable.fecha_registro.writable = False
            self._dbTable.inconsistencias.writable = False

            # Campos que todos los movimientos pueden editar
            self._dbTable.observaciones.writable = True
            self._dbTable.fecha_movimiento.writable = True
            self._dbTable.descripcion.writable = True

            # Se definen los valores por default
            self._dbTable.empresa_almacenesconcepto_id.default = self.n_empresa_almacenconcepto_id

            return _D_return

        @decorador_verifica_condiciones
        def configuraCampos_edicion(
                self,
                x_empresa_invmovto,
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param x_empresa_invmovto: es el registro del movimiento o su id
            @type x_empresa_invmovto:
            @return:
            @rtype:
            """
            _D_return = Storage(
                E_return = CLASS_e_RETURN.OK,
                s_msgError = "",
                )

            _D_results = self.puede_editar(x_empresa_invmovto)

            if _D_results.E_return == CLASS_e_RETURN.OK:
                # Si puede editar regresa un ok y el movimiento esta abierto

                # Se manda vacío el argumento para no agregar defaults a los campos
                _D_results = self.configuraCampos_nuevo(
                    s_empresa_inventariomovimiento_id = None,
                    )
                _D_return.E_return = _D_results.E_return
                _D_return.s_msgError = _D_results.s_msgError

            else:
                # De lo contrario en cualquier otro caso, solo se pueden editar las observaciones
                self._dbTable.observaciones.writable = True

            return _D_return

        def al_validar(self, O_form):
            """ Validar la información para poder grabar inventario inicial

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            if self._D_return.E_return > CLASS_e_RETURN.OK_WARNING:
                O_form.errors.id = self._D_return.s_msgError

            else:
                # Se actualizan los campos autocalculados
                # En este caso no hay

                if O_form.vars.cfdi_egreso_id:
                    # Query para encontrar el CFDI guardado en la tabla de CFDIs para obtener sus datos.
                    _dbRowCfdi = dbc01.tcfdis(O_form.vars.cfdi_id)

                    if _dbRowCfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO:
                        pass
                    else:
                        O_form.errors.cfdi_egreso_id = "El CFDI no es de tipo ingreso."

                    if D_stvFwkCfg.dbRow_empresa.receptorrfc == _dbRowCfdi.rfc:
                        pass
                    else:
                        O_form.errors.cfdi_egreso_id = "El RFC del receptor en el CFDI no coincide con el de la empresa."

                    if str(_dbRowCfdi.emisorproveedor_id) == str(O_form.vars.empresa_proveedor_id):
                        pass
                    elif not str(O_form.vars.empresa_proveedor_id):
                        O_form.vars.empresa_proveedor_id = _dbRowsCfdi.emisorproveedor_id
                    else:
                        O_form.errors.empresa_proveedor_id = "Proveedor no coincide con CFDI."
                else:
                    pass

                if int(O_form.vars.tipodocumento) == TEMPRESA_INVENTARIOMOVIMIENTOS.E_TIPODOCUMENTO.REMISION:
                    if O_form.vars.remision:
                        pass
                    else:
                        O_form.errors.remision = "Si se eligió tipo de documento remisión la remisión no puede ir vacía"

                elif int(O_form.vars.tipodocumento) == TEMPRESA_INVENTARIOMOVIMIENTOS.E_TIPODOCUMENTO.FACTURA:
                    if O_form.vars.cfdi_egreso_id:
                        pass
                    else:
                        O_form.errors.cfdi_egreso_id = "Si se eligió tipo de documento factura la factura no puede ir vacía"
                else:
                    pass

                if O_form.vars.empresa_proveedor_id:
                    pass
                else:
                    O_form.errors.empresa_proveedor_id = "El proveedor no puede estar vacío"


                if O_form.record_id:
                    # Se esta modificando el registro, la llamada a configuraCampos_edicion ya definió con writable
                    # que puede modificarse y que no.
                    pass

                else:
                    # Se esta creando el registro

                    _D_results = self._puede_crear()
                    if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                        O_form.errors.id = _D_results.s_msgError

                    else:
                        self._dbTable.empresa_almacenesconcepto_id.default = self.n_empresa_almacenconcepto_id

            return O_form

        def al_aceptar(self, O_form):
            """ Aceptación captura de inventario inicial producto

            Se hacen todos los cálculos y aceptación de los cambios en el producto

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            if self._D_return.E_return > CLASS_e_RETURN.OK_WARNING:
                O_form.errors.id = self._D_return.s_msgError
            else:
                pass

            return O_form

        def antes_eliminar(self, L_errorsAction, dbRow):
            """ Antes de eliminar un movimiento de inventario de este tipo

            @param dbRow:
            @type dbRow:
            @param L_errorsAction:
            @type L_errorsAction:
            @return: errores
            @rtype: lista de diccionarios {'msg':'...'}
            """

            if self._D_return.E_return > CLASS_e_RETURN.OK_WARNING:
                L_errorsAction.append({'msg': self._D_return.s_msgError})

            else:
                _D_results = self._puede_eliminar(dbRow)
                if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                    L_errorsAction.append({'msg': _D_results.s_msgError})
                else:
                    pass

            return L_errorsAction

        def checa_inconsistencias(
                self,
                x_empresa_invmovto
                ):
            _D_return = Storage(
                E_return = CLASS_e_RETURN.OK,
                s_msgError = "",
                dbRow_invMovto = None
                )

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_empresa_invmovto, self._dbTable)
            _n_invMovimiento = _D_results.n_id
            _D_return.dbRow_invMovto = _D_results.dbRow

            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.E_return = _D_results.E_return
                _D_return.s_msgError = _D_results.s_msgError

            else:
                pass
            # TODO checar inconsistencias del movimiento como que la suma de los totales de los productos, no cuadran con el total del movimiento
            # que los productos tienen alguna inconsistencia, o falta de campo

            return _D_return

        @classmethod
        def IMPORTAR_CFDI(cls):
            """ Se importa el cfdi recibido de un proveedor.

            @return:
            @rtype:
            """

    class ENTRADA:

        @classmethod
        def CONFIGURA_CAMPOS(
                cls,
                s_empresa_id,
                n_clasificacion,
                ):
            _dbTabla = dbc01.tempresa_inventariomovimientos

            _D_result_concepto_id = TEMPRESA_ALMACENESCONCEPTOS.GET_ID_CACHE(
                n_empresa_id = s_empresa_id,
                n_tipoconcepto = TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.ENTRADA,
                n_clasificacion = n_clasificacion
                )

            _dbTabla.empresa_almacenesconcepto_id.default = _D_result_concepto_id

            if n_clasificacion in (TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.OTRAS_ENTRADAS, TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.COMPRA):
                _dbTabla.tipodocumento.writable = True
                _dbTabla.remision.writable = True
                _dbTabla.total.writable = True
                _dbTabla.empresa_almacenesconcepto_id.writable = True
                _dbTabla.empresa_proveedor_id.writable = True
                _dbTabla.empresa_cliente_id.writable = True
                _dbTabla.cfdi_id.writable = True
                _dbTabla.cfdi_egreso_id.writable = True
                _dbTabla.moneda_id.writable = True
                _dbTabla.tipocambio.writable = True
                _dbTabla.tc.writable = True
                _dbTabla.ordencompra.writable = True
                _dbTabla.empresa_plaza_sucursal_almacen_id.writable = True
                _dbTabla.fecha_registro.writable = True
                _dbTabla.fecha_movimiento.writable = True
                _dbTabla.folio.writable = True
                _dbTabla.descripcion.writable = True
                _dbTabla.estatus_movimiento.writable = True
                _dbTabla.empresa_embarque_almacen_id.writable = True
                _dbTabla.empresa_embarque_almacen_id_relacionado.writable = True
                _dbTabla.empresa_embarque_id.writable = True
                _dbTabla.fecha_salida.writable = True
                _dbTabla.observaciones.writable = True
            else:
                pass

            return

    class VALIDACION:

        @classmethod
        def GET_EMPRESA_ID(cls, s_empresa_plaza_sucursal_almacen_id = None):
            
            _D_result_empresa_id = Storage(
                E_return = CLASS_e_RETURN.NOK_ERROR,
                s_msgError = "",
                n_empresa_id = None
                )
                
            _dbRows_empresa = dbc01(
                (dbc01.tempresa_plaza_sucursal_almacenes.id == s_empresa_plaza_sucursal_almacen_id)
                &(dbc01.tempresa_plaza_sucursales.id == dbc01.tempresa_plaza_sucursal_almacenes.empresa_plaza_sucursal_id)
                &(dbc01.tempresa_plazas.id == dbc01.tempresa_plaza_sucursales.empresa_plaza_id)
                ).select(
                    dbc01.tempresa_plazas.empresa_id
                    ).last()
            
            if _dbRows_empresa:
                _D_result_empresa_id.E_return = CLASS_e_RETURN.OK
                _D_result_empresa_id.n_empresa_id = _dbRows_empresa.empresa_id
            else:
                #hubo un error y la consulta no trajo la empresa_id
                pass
            
            if _D_result_empresa_id.E_return != CLASS_e_RETURN.OK:
                #Si entra acá es que ocurrió un error durante la obtención del concepto_id
                _s_empresa_id = None
                stv_flashError("Empresa ID no encontrado.", _D_result_empresa_id.s_msgError)        
            elif _D_result_empresa_id.n_empresa_id:
                _s_empresa_id = _D_result_empresa_id.n_empresa_id
            else:
                #hubo bronca, no trajo error pero tampoco trajo la empresa_id
                _s_empresa_id = None
                stv_flashError("Empresa ID no encontrado.", "Empresa no definida para almacenes.")
                
            return _s_empresa_id

        @classmethod
        def GET_CONCEPTO_ID(cls, n_empresa_id, n_tipoconcepto, n_clasificacion):

            _D_result_concepto_id = TEMPRESA_ALMACENESCONCEPTOS.GET_ID_CACHE(
                n_empresa_id, 
                n_tipoconcepto,
                n_clasificacion
                )
            
            if _D_result_concepto_id.E_return != CLASS_e_RETURN.OK:
                #Si entra acá es que ocurrió un error durante la obtención del concepto_id
                _n_concepto_id = None
                stv_flashError("Error al conseguir Concepto ID", _D_result_concepto_id.s_msgError)        
            elif _D_result_concepto_id.n_concepto_id:
                _n_concepto_id = _D_result_concepto_id.n_concepto_id
            else:
                #hubo bronca, no trajo error pero tampoco trajo concepto_id
                _n_concepto_id = None
                stv_flashError("Error al conseguir Concepto ID", "Concepto no definido en Almacenes-Conceptos")  
            return _n_concepto_id
        
        @classmethod
        def VERIFICAR_CONFLICTO_POLIZA(
                cls, 
                O_form, 
                s_empresa_plaza_sucursal_almacen_id, 
                s_id, 
                dt_form_fecha_movimiento
                ):
            """ Se verifica si alguno de los detalles de la poliza ya tiene movimientos, o si existe poliza con movimientos poteriores
            """
            
            # Se valida que no tenga productos detalle con poliza generada        
            _dbRows_movtoProductos_conPoliza = TEMPRESA_INVMOVTO_PRODUCTO_COSTEO.GET_CONPOLIZA_PORMOVTO(
                n_empresa_plaza_sucursal_almacen_id = s_empresa_plaza_sucursal_almacen_id,
                n_empresa_inventariomovimiento_id = s_id
                )
            
            if _dbRows_movtoProductos_conPoliza:
                O_form.errors.id = "No puede editar el movimiento, ya que tiene productos con poliza generada"
            else:
            
                # Se buscan los productos detalle que tienen movimientos posteriores con poliza generada
                _dbRows_movtoProductosPosteriores_conPoliza = TEMPRESA_INVMOVTO_PRODUCTO_COSTEO.GET_CONPOLIZAPOSTERIOR_PORMOVTO(
                    n_empresa_plaza_sucursal_almacen_id = s_empresa_plaza_sucursal_almacen_id,
                    n_empresa_inventariomovimiento_id = s_id,
                    dt_fecha_movimiento = dt_form_fecha_movimiento
                    )
                
                #si hay movimientos con fecha mayor a la introducida deberá desplegar error en la vista.    
                if _dbRows_movtoProductosPosteriores_conPoliza:
                    O_form.errors.fecha_movimiento = "Actualmente hay movimientos con póliza con fechas de movimientos mayores a la introducida."
                else:
                    pass
                                
            return O_form
        
        @classmethod
        def VERIFICAR_CONFLICTO_INVENTARIOINICIAL(
                cls,
                O_form,
                dt_form_fecha_movimiento,
                s_empresa_id,
                s_empresa_plaza_sucursal_almacen_id
                ):
            if dt_form_fecha_movimiento > request.browsernow:
                O_form.errors.fecha_movimiento = "No puede ser mayor a la fecha actual."
            else:
                #Query para obtener la fecha del inventario inicial si es que existe
                _D_result_inventarioinicial = TEMPRESA_INVMOVTO_PRODUCTO_COSTEO.GET_FECHA_INVENTARIOINICIAL(
                    n_empresa_id = s_empresa_id,
                    n_empresa_plaza_sucursal_almacen_id = s_empresa_plaza_sucursal_almacen_id
                    )
            
                if _D_result_inventarioinicial.E_return == CLASS_e_RETURN.OK:
                    if (
                        _D_result_inventarioinicial.dt_fecha_movimiento
                        and (_D_result_inventarioinicial.dt_fecha_movimiento >= dt_form_fecha_movimiento)
                        ):
                        O_form.errors.fecha_movimiento = "No puede ser anterior a la fecha de inventario inicial."
                    else:
                        pass
                else:
                    O_form.errors.fecha_movimiento = "Existe error para identificar inventario inicial. Consulte administrador."
                    pass
            
            return O_form
        
        class ENTRADA:
            
            @classmethod         
            def PORCOMPRA_RESUMEN(cls, O_form):
                """Validación para entrada_porcompra_resumen
                """
                 
                _dbT_resumenes = dbc01.tempresa_invmovto_productos
                _dbT_maestro = dbc01.tempresa_inventariomovimientos
                _dbT_prov_codigoprod = dbc01.tempresa_prodserv_codigosproveedores
                
                if O_form.record_id and not O_form.vars.producto_externo:
                    # Si esta editando y no tiene producto externo definido, tomar el producto externo del registro
                    O_form.vars.producto_externo = O_form.record.producto_externo
                else:
                    pass
                
                if not O_form.vars.producto_externo:
                    
                    _dbRows_proveedorCodigo = dbc01(
                        (_dbT_maestro.id == _dbT_resumenes.empresa_inventariomovimiento_id.default)
                        & (_dbT_prov_codigoprod.empresa_proveedor_id == _dbT_maestro.empresa_proveedor_id)
                        & (_dbT_prov_codigoprod.empresa_prodserv_id == O_form.vars.empresa_producto_id)
                        & (_dbT_prov_codigoprod.bansuspender == False)
                        ).select(
                            _dbT_prov_codigoprod.ALL,
                            _dbT_maestro.cfdi_id,
                            orderby = ~_dbT_prov_codigoprod.id
                            )
                    
                    if _dbRows_proveedorCodigo:
                        _dbRow = _dbRows_proveedorCodigo.first()
                        O_form.vars.producto_externo = _dbRow.codigo
                        if _dbRow.cfdi_id:
                            _dbRows_conceptosCFDI = dbc01(
                                (dbc01.tcfdi_conceptos.cfdi_id == _dbRow.cfdi_id)
                                & (dbc01.tcfdi_conceptos.noidentificacion == O_form.vars.producto_externo)
                                ).select(
                                    dbc01.tcfdi_conceptos.ALL
                                    )
                            if _dbRows_conceptosCFDI:
                                O_form.vars.descripcion = _dbRows_conceptosCFDI.first().descripcion
                                O_form.vars.cfdi_concepto_id = _dbRows_conceptosCFDI.first().id
                            else:
                                O_form.vars.descripcion = "Producto no encontrado en CFDI"
                                O_form.vars.cfdi_concepto_id = None
                        else:
                            O_form.vars.descripcion = "Producto no encontrado en CFDI"
                            O_form.vars.cfdi_concepto_id = None
                    else:
                        O_form.vars.descripcion = "Producto no encontrado en CFDI"
                        O_form.vars.cfdi_concepto_id = None
                    
                elif O_form.vars.empresa_producto_id:
                    
                    _dbRows_proveedorCodigo = dbc01(
                        (_dbT_maestro.id == _dbT_resumenes.empresa_inventariomovimiento_id.default)
                        & (_dbT_prov_codigoprod.empresa_proveedor_id == _dbT_maestro.empresa_proveedor_id)
                        & (_dbT_prov_codigoprod.empresa_prodserv_id == O_form.vars.empresa_producto_id)
                        & (_dbT_prov_codigoprod.bansuspender == False)
                        ).select(
                            _dbT_prov_codigoprod.ALL,
                            orderby = ~_dbT_prov_codigoprod.id
                            )

                    _dbRow_movto = _dbT_maestro(_dbT_resumenes.empresa_inventariomovimiento_id.default)

                    if _dbRows_proveedorCodigo:
                        _dbRow = _dbRows_proveedorCodigo.first()
                        if _dbRow.codigo != O_form.vars.producto_externo:
                            O_form.errors.producto_externo = "Código no coincide con el producto seleccionado en el sistema"
                        else:
                            pass
                    else:
                        
                        dbc01.tempresa_prodserv_codigosproveedores.insert(
                            empresa_prodserv_id = O_form.vars.empresa_producto_id,
                            empresa_proveedor_id = _dbRow_movto.empresa_proveedor_id,
                            codigo = O_form.vars.producto_externo
                            )
                        
                    if _dbRow_movto.cfdi_id:
                        _dbRows_conceptosCFDI = dbc01(
                            (dbc01.tcfdi_conceptos.cfdi_id == _dbRow_movto.cfdi_id)
                            & (dbc01.tcfdi_conceptos.noidentificacion == O_form.vars.producto_externo)
                            ).select(
                                dbc01.tcfdi_conceptos.ALL
                                )
                        if _dbRows_conceptosCFDI:
                            O_form.vars.descripcion = _dbRows_conceptosCFDI.first().descripcion
                            O_form.vars.cfdi_concepto_id = _dbRows_conceptosCFDI.first().id
                        else:
                            O_form.vars.descripcion = "Producto no encontrado en CFDI"
                            O_form.vars.cfdi_concepto_id = None
                    else:
                        O_form.vars.descripcion = "Producto no encontrado en CFDI"
                        O_form.vars.cfdi_concepto_id = None
                    pass

                    
                else:
                    O_form.errors.empresa_producto_id = "Producto no puede estar vacío"
                    pass
            
                if int(O_form.vars.cantidad_entrada or 0) <= 0:
                    O_form.errors.cantidad_entrada = "La cantidad que entra no puede ser menor o igual a cero"
                else:
                    pass
                
                
                return O_form
            

            
            @classmethod         
            def INVENTARIOINICIAL_RESUMEN(cls, O_form):
                """Validación para el inventario inicial resumen.
                """
                _n_cantidad = int(O_form.vars.cantidad_entrada or 0)
                    
                if _n_cantidad <= 0:
                    O_form.errors.cantidad_entrada = "La cantidad que entra no puede ser menor o igual a cero"
                else:
                    pass
                return O_form

            @classmethod
            def PORTRASPASO(cls, O_form):
                """Validación para las entradas por traspaso
                """
                _dbTable = dbc01.tempresa_inventariomovimientos
                
                _dt_form_fecha_movimiento = STV_LIB_DB.CONVERT_DATETIME(O_form.vars.fecha_movimiento)
                
                _s_empresa_plaza_sucursal_almacen_id = request.vars.master_empresa_plaza_sucursal_almacen_id
                
                _s_cuenta_aplicacion_id = D_stvFwkCfg.cfg_cuenta_aplicacion_id
                
                _s_empresa_id = TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.GET_EMPRESA_ID( 
                    s_empresa_plaza_sucursal_almacen_id = _s_empresa_plaza_sucursal_almacen_id
                    )
                                
                #Si se está editando _s_id tendrá un valor y entrará a esta validación.
                if O_form.record_id:
                    
                    O_form = TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.VERIFICAR_CONFLICTO_POLIZA(
                        O_form = O_form, 
                        s_empresa_plaza_sucursal_almacen_id = _s_empresa_plaza_sucursal_almacen_id, 
                        s_id = O_form.record_id, 
                        dt_form_fecha_movimiento = _dt_form_fecha_movimiento
                        )
                    
                else:
                    pass
                
                O_form = TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.VERIFICAR_CONFLICTO_INVENTARIOINICIAL(
                    O_form = O_form,
                    dt_form_fecha_movimiento = _dt_form_fecha_movimiento,
                    s_empresa_id = _s_empresa_id,
                    s_empresa_plaza_sucursal_almacen_id = _s_empresa_plaza_sucursal_almacen_id
                    )
                
                if not O_form.vars.ordencompra:
                    O_form.errors.ordencompra = "Es requereida una orden de salida para esta operación"
                else:
                    pass
                
                if not O_form.vars.empresa_embarque_id:
                    O_form.errors.empresa_embarque_id = "El embarque es requerido para poder hacer una entrada por traspaso."
                else:
                    
                    _dbRows_embarque_almacenes_abiertos = dbc01(
                        (dbc01.tempresa_embarque_almacenes.empresa_embarque_id == O_form.vars.empresa_embarque_id)
                        & (
                            dbc01.tempresa_embarque_almacenes.estatus.belongs(
                                TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.EN_ESPERA,
                                TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CARGANDO,
                                TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.DESCARGANDO,
                                )
                            )
                        ).select(
                            dbc01.tempresa_embarque_almacenes.ALL,
                            orderby = (
                                dbc01.tempresa_embarque_almacenes.orden
                                )
                            )
                        
                    if len(_dbRows_embarque_almacenes_abiertos) == 0:
                        O_form.errors.empresa_embarque_id = "El almacén no forma parte de este embarque, favor de revisarlo"
                    else:
                        _dbRow_embarque_almacenes_abierto_primero = _dbRows_embarque_almacenes_abiertos.first()
                        if _dbRow_embarque_almacenes_abierto_primero.empresa_plaza_sucursal_almacen_id != int(_s_empresa_plaza_sucursal_almacen_id):
                            O_form.errors.empresa_embarque_id = "El embarque no se encuentra (en el sistema) en el almacén para permitir movimientos, favor de cerrar el almacen del embarque ya visitado"
                        else:
                            _dbTable.empresa_embarque_almacen_id.default = _dbRow_embarque_almacenes_abierto_primero.id
 
                return O_form           
            
            @classmethod
            def OTRA(cls, O_form):
                """Validación para las otras entradas
                """
                _dbTable = dbc01.tempresa_inventariomovimientos
                
                _dt_form_fecha_movimiento = STV_LIB_DB.CONVERT_DATETIME(O_form.vars.fecha_movimiento)
                
                _s_empresa_plaza_sucursal_almacen_id = request.vars.master_empresa_plaza_sucursal_almacen_id
                
                _s_cuenta_aplicacion_id = D_stvFwkCfg.cfg_cuenta_aplicacion_id
                
                _s_empresa_id = TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.GET_EMPRESA_ID( 
                    s_empresa_plaza_sucursal_almacen_id = _s_empresa_plaza_sucursal_almacen_id
                    )
                                
                #Si se está editando _s_id tendrá un valor y entrará a esta validación.
                if O_form.record_id:
                    
                    O_form = TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.VERIFICAR_CONFLICTO_POLIZA(
                        O_form = O_form, 
                        s_empresa_plaza_sucursal_almacen_id = _s_empresa_plaza_sucursal_almacen_id, 
                        s_id = O_form.record_id, 
                        dt_form_fecha_movimiento = _dt_form_fecha_movimiento
                        )
                    
                else:
                    pass
                
                O_form = TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.VERIFICAR_CONFLICTO_INVENTARIOINICIAL(
                    O_form = O_form,
                    dt_form_fecha_movimiento = _dt_form_fecha_movimiento,
                    s_empresa_id = _s_empresa_id,
                    s_empresa_plaza_sucursal_almacen_id = _s_empresa_plaza_sucursal_almacen_id
                    )
                
                if not O_form.vars.remision:
                    O_form.errors.remision = "La remisión es requerida."
                else:
                    pass
                
                if not O_form.vars.descripcion:
                    O_form.errors.descripcion = "La descripción es requerida."
                else:
                    pass
                    
                return O_form  

        class SALIDA:
            
            @classmethod
            def PORDEVOLUCION_COMPRA(cls, O_form, s_empresa_id = None):
                """Validación para las salidas por devolución
                """
                _dbTable = dbc01.tempresa_inventariomovimientos
                                
                _dt_form_fecha_movimiento = STV_LIB_DB.CONVERT_DATETIME(O_form.vars.fecha_movimiento)
                
                _s_empresa_plaza_sucursal_almacen_id = request.vars.master_empresa_plaza_sucursal_almacen_id

                _s_empresa_id = TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.GET_EMPRESA_ID( 
                    s_empresa_plaza_sucursal_almacen_id = _s_empresa_plaza_sucursal_almacen_id
                    )
                                
                #Si se está editando _s_id tendrá un valor y entrará a esta validación.
                if O_form.record_id:
                    
                    O_form = TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.VERIFICAR_CONFLICTO_POLIZA(
                        O_form = O_form, 
                        s_empresa_plaza_sucursal_almacen_id = _s_empresa_plaza_sucursal_almacen_id, 
                        s_id = O_form.record_id, 
                        dt_form_fecha_movimiento = _dt_form_fecha_movimiento
                        )
                    
                else:
                    pass
                
                O_form = TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.VERIFICAR_CONFLICTO_INVENTARIOINICIAL(
                    O_form = O_form,
                    dt_form_fecha_movimiento = _dt_form_fecha_movimiento,
                    s_empresa_id = _s_empresa_id,
                    s_empresa_plaza_sucursal_almacen_id = _s_empresa_plaza_sucursal_almacen_id
                    )
                    
                #Se verifica primero el de egreso porque es el que tiene mayor prioridad en este movimiento.
                if O_form.vars.cfdi_egreso_id:
                    
                    _dbRowsCfdi = dbc01.tcfdis(O_form.vars.cfdi_egreso_id) 
                    
                    if _dbRowsCfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.EGRESO:
                        pass
                    else:
                        O_form.errors.cfdi_egreso_id = "El CFDI no es de tipo egreso."
                    
                    #Query para obtener el RFC de la empresa
                    _dbRowsRfc = dbc01.tempresas(_s_empresa_id)
                    
                    if _dbRowsCfdi.tiporelacion_id == TTIPOSRELACIONES.DEVOLUCION:
                        #No hay bronca, el CFDI es del tipo de relación correcta.
                        pass
                    else:
                        O_form.errors.cfdi_egreso_id = "El CFDI de egreso no tiene el tipo de relación 'Devolución'"
                    
                    if _dbRowsCfdi.receptorrfc == _dbRowsRfc.rfc:
                        pass
                    else:
                        O_form.errors.cfdi_egreso_id = "El RFC del receptor en el CFDI no coincide con el de la empresa."
                        
                    if str(_dbRowsCfdi.emisorproveedor_id) == str(O_form.vars.empresa_proveedor_id):
                        pass
                    elif not str(O_form.vars.empresa_proveedor_id):
                        O_form.vars.empresa_proveedor_id = _dbRowsCfdi.emisorproveedor_id
                    else:
                        O_form.errors.empresa_proveedor_id = "Proveedor no coincide con CFDI."
                        
                    if not O_form.vars.cfdi_id:
                        #Si no se ha agregado el CFDI de ingreso todo correcto
                        pass
                    else:
                        #Si tiene CFDI se iguala a nada ya que un CFDI de egreso puede tener relacionado 1 o más CFDI de ingreso.
                        O_form.vars.cfdi_id = None
                    
                    ### TODO: ¿Es necesario esta validación?
#                     #Query para ver si este CFDI de egreso tiene CFDIs de ingreso relacionados.
#                     _dbRows_cfdi_relacionados = dbc01(
#                         dbc01.tcfdi_relacionados.cfdi_id == O_form.vars.cfdi_egreso_id
#                         ).select(
#                             dbc01.tcfdi_relacionados.ALL
#                             )
#                            
#                     ### TODO: ¿Qué pasa si tiene más de un CFDI de ingreso relacionado este CFDI de egreso?
#                     if not(_dbRows_cfdi_relacionados):
#                         #Si no se encuentra el CFDI al que está relacionado en el sistema mostrar un error
#                         stv_flashWarning("Inconsistencias en CFDIs", "El CFDI de egreso no cuenta con un CFDI relacionado dentro del sistema. Favor de hablar con el administrador")
#                     else:
#                         #Encontró CFDI relacionado, sigue el proceso normal
#                         pass
                
                elif O_form.vars.cfdi_id:
                    #Query para encontrar el CFDI guardado en la tabla de CFDIs para obtener sus datos. 
                    _dbRowsCfdi = dbc01.tcfdis(O_form.vars.cfdi_id) 
                    
                    if O_form.record_id:
                        if O_form.vars.remision:
                            #Query para saber si el inventario movimiento ya cuenta con movimientos en el detalle.p
                            _dbRows_movimientosResmision = dbc01(
                                dbc01.tempresa_invmovto_productos.empresa_inventariomovimiento_id == O_form.record_id
                                ).select(
                                    dbc01.tempresa_invmovto_productos.id
                                    )
                            if _dbRows_movimientosResmision:
                                O_form.errors.cfdi_id = "No se puede capturar un CFDI de ingreso cuando ya se tienen movimientos capturados en el detalle. Solo se permiten CFDI de egreso."
                            else:
                                pass
                        else:
                            pass
                    else:
                        pass
                    
                    if _dbRowsCfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO:
                        pass
                    else:
                        O_form.errors.cfdi_id = "El CFDI no es de tipo ingreso."
                    
                    #Query para obtener el RFC de la empresa
                    _dbRowsRfc = dbc01.tempresas(_s_empresa_id)
                    
                    ###TODO: ¿Esto también aplica en esta lógica para devolución de compra?
                    if _dbRowsCfdi.receptorrfc == _dbRowsRfc.rfc:
                        pass
                    else:
                        O_form.errors.cfdi_id = "El RFC del receptor en el CFDI no coincide con el de la empresa."

                    ###Se cancela esta lógica debido a que el CFDI de ingreso no debe de ser mandante para el proveedor. 
#                     if(str(_dbRowsCfdi.emisorproveedor_id) == str(O_form.vars.empresa_proveedor_id)):
#                         pass
#                     elif not str(O_form.vars.empresa_proveedor_id):
#                         ###TODO: ¿Aun que sea CFDI de ingreso agregar el proveedor al formulario? Ya que este CFDI funciona como un helper nada más para el llenado
#                         O_form.vars.empresa_proveedor_id = _dbRowsCfdi.emisorproveedor_id
#                     else:
#                         O_form.errors.empresa_proveedor_id = "Proveedor no coincide con CFDI."
                    
                    if not O_form.vars.remision:
                        O_form.errors.remision = "La remisión no puede de ir vacía"
                    else:
                        pass
                    
                    if not str(O_form.vars.empresa_proveedor_id):
                        O_form.errors.empresa_proveedor_id = "El proveedor no puede estar vacío."
                    else:
                        pass
                
                elif O_form.vars.remision:
                    if O_form.vars.empresa_proveedor_id:
                        pass
                    else:
                        O_form.errors.empresa_proveedor_id = "El proveedor no puede estar vacío."
                else:
                    O_form.errors.remision = "La remisión es requerida."
                    pass

                return O_form
            
            @classmethod
            def PORDEVOLUCION_COMPRA_RESUMEN(cls, O_form):
                """Validación para las salidas por devolución por compra resumenes
                """
                _s_id = O_form.record_id
                 
                _dbT_resumenes = dbc01.tempresa_invmovto_productos
                _dbT_maestro = dbc01.tempresa_inventariomovimientos
                _dbT_prov_codigoprod = dbc01.tempresa_prodserv_codigosproveedores
                
                if not O_form.vars.producto_externo:
                    
                    _dbRows_proveedorCodigo = dbc01(
                        (_dbT_maestro.id == O_form.vars.empresa_inventariomovimiento_id)
                        & (_dbT_prov_codigoprod.empresa_proveedor_id == _dbT_maestro.empresa_proveedor_id)
                        & (_dbT_prov_codigoprod.empresa_prodserv_id == O_form.vars.empresa_producto_id)
                        & (_dbT_prov_codigoprod.bansuspender == False)
                        ).select(
                            _dbT_prov_codigoprod.ALL,
                            orderby = ~_dbT_prov_codigoprod.id
                            )
                    
                    if _dbRows_proveedorCodigo:
                        O_form.vars.producto_externo = _dbRows_proveedorCodigo.first().codigo
                    else:
                        pass
                elif O_form.vars.empresa_producto_id:
                    
                    _dbRows_proveedorCodigo = dbc01(
                        (_dbT_maestro.id == O_form.vars.empresa_inventariomovimiento_id)
                        & (_dbT_prov_codigoprod.empresa_proveedor_id == _dbT_maestro.empresa_proveedor_id)
                        & (_dbT_prov_codigoprod.empresa_prodserv_id == O_form.vars.empresa_producto_id)
                        & (_dbT_prov_codigoprod.bansuspender == False)
                        ).select(
                            _dbT_prov_codigoprod.ALL,
                            orderby = ~_dbT_prov_codigoprod.id
                            )
                    
                    if _dbRows_proveedorCodigo:
                        if _dbRows_proveedorCodigo.first().codigo != O_form.vars.producto_externo:
                            O_form.errors.producto_externo = "Código no coincide con el producto seleccionado en el sistema"
                        else:
                            pass
                    else:
                        
                        _dbRow_movto = _dbT_maestro(_dbT_resumenes.empresa_inventariomovimiento_id.default)
                        
                        dbc01.tempresa_prodserv_codigosproveedores.insert(
                            empresa_prodserv_id = O_form.vars.empresa_producto_id,
                            empresa_proveedor_id = _dbRow_movto.empresa_proveedor_id,
                            codigo = O_form.vars.producto_externo
                            )
                    
                    pass
                else:
                    O_form.errors.empresa_producto_id = "Producto no puede estar vacío"
                    pass
            
                if int(O_form.vars.cantidad_salida or 0) <= 0:
                    O_form.errors.cantidad_salida = "La cantidad que entra no puede ser menor o igual a cero"
                else:
                    pass
                
                return O_form  
            
            @classmethod
            def PORTRASPASO(cls, O_form):
                """Validación para las salidas por traspaso
                """
                
                _dbTable = dbc01.tempresa_inventariomovimientos
                                
                _s_empresa_plaza_sucursal_almacen_id = request.vars.master_empresa_plaza_sucursal_almacen_id
                
                _dt_form_fecha_movimiento = STV_LIB_DB.CONVERT_DATETIME(O_form.vars.fecha_movimiento)
                
                _s_empresa_id = TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.GET_EMPRESA_ID( 
                    s_empresa_plaza_sucursal_almacen_id = _s_empresa_plaza_sucursal_almacen_id
                    )
                                
                #Si se está editando _s_id tendrá un valor y entrará a esta validación.
                if O_form.record_id:
                    
                    _dbRow_movimiento = dbc01.tempresa_inventariomovimientos(O_form.record_id)
                    
                    _dt_form_fecha_movimiento = _dbRow_movimiento.fecha_movimiento
                    
                    O_form = TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.VERIFICAR_CONFLICTO_POLIZA(
                        O_form = O_form, 
                        s_empresa_plaza_sucursal_almacen_id = _s_empresa_plaza_sucursal_almacen_id, 
                        s_id = O_form.record_id, 
                        dt_form_fecha_movimiento = _dt_form_fecha_movimiento
                        )
                    
                    #Verificar si se guardó la fecha de salida. 
                    if O_form.vars.fecha_salida:
                        _dbRows_salida_movimientos = dbc01(
                            dbc01.tempresa_invmovto_productos.empresa_inventariomovimiento_id == O_form.record_id
                            ).select(
                                dbc01.tempresa_invmovto_productos.id
                                )
                        if not _dbRows_salida_movimientos:
                            O_form.errors.fecha_salida = "No puede capturarse la fecha de salida porque no hay articulos en este movimiento."
                            pass
                        else:
                            pass
                    else:
                        O_form.errors.fecha_salida = "Es necesario escribir una fecha de salida si se quiere editar este registro."
                        pass
                    
                else:
                
                    O_form = TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.VERIFICAR_CONFLICTO_INVENTARIOINICIAL(
                        O_form = O_form,
                        dt_form_fecha_movimiento = _dt_form_fecha_movimiento,
                        s_empresa_id = _s_empresa_id,
                        s_empresa_plaza_sucursal_almacen_id = _s_empresa_plaza_sucursal_almacen_id
                        )
                    
                    if not O_form.vars.empresa_embarque_id:
                        O_form.errors.empresa_embarque_id = "Se tiene que seleccionar un embarque para esta salida."
                    elif not O_form.vars.empresa_embarque_almacen_id_relacionado:
                        O_form.errors.empresa_embarque_almacen_id_relacionado = "Se tiene que seleccionar un almacén de destino"
                    else:
                        
                        _dbRows_embarque_almacenes_abiertos = dbc01(
                            (dbc01.tempresa_embarque_almacenes.empresa_embarque_id == O_form.vars.empresa_embarque_id)
                            & (
                                dbc01.tempresa_embarque_almacenes.estatus.belongs(
                                    TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.EN_ESPERA,
                                    TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CARGANDO,
                                    TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.DESCARGANDO,
                                    )
                                )
                            ).select(
                                dbc01.tempresa_embarque_almacenes.ALL,
                                orderby = (
                                    dbc01.tempresa_embarque_almacenes.orden
                                    )
                                )
                            
                        if len(_dbRows_embarque_almacenes_abiertos) == 0:
                            O_form.errors.empresa_embarque_id = "El almacén no forma parte de este embarque, favor de revisarlo. Contacte al administrador."
                        else:
                            _dbRow_embarque_almacenes_abierto_primero = _dbRows_embarque_almacenes_abiertos.first()
                            if _dbRow_embarque_almacenes_abierto_primero.empresa_plaza_sucursal_almacen_id != int(_s_empresa_plaza_sucursal_almacen_id):
                                O_form.errors.empresa_embarque_id = "El embarque no se encuentra fisicamente en el almacén para permitir este movimiento, favor de cerrar el almacen del embarque ya visitado"
                            else:
                                _dbTable.empresa_embarque_almacen_id.default = _dbRow_embarque_almacenes_abierto_primero.id

                return O_form 
               
            @classmethod
            def OTRA(cls, O_form):
                """Validación para las otras salidas
                """
                _dbTable = dbc01.tempresa_inventariomovimientos
                
                _dt_form_fecha_movimiento = STV_LIB_DB.CONVERT_DATETIME(O_form.vars.fecha_movimiento)
                
                _s_empresa_plaza_sucursal_almacen_id = request.vars.master_empresa_plaza_sucursal_almacen_id
                
                _s_cuenta_aplicacion_id = D_stvFwkCfg.cfg_cuenta_aplicacion_id
                
                _s_empresa_id = TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.GET_EMPRESA_ID( 
                    s_empresa_plaza_sucursal_almacen_id = _s_empresa_plaza_sucursal_almacen_id
                    )
                                
                #Si se está editando _s_id tendrá un valor y entrará a esta validación.
                if O_form.record_id:
                    
                    O_form = TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.VERIFICAR_CONFLICTO_POLIZA(
                        O_form = O_form, 
                        s_empresa_plaza_sucursal_almacen_id = _s_empresa_plaza_sucursal_almacen_id, 
                        s_id = O_form.record_id, 
                        dt_form_fecha_movimiento = _dt_form_fecha_movimiento
                        )
                    
                else:
                    pass
                
                O_form = TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.VERIFICAR_CONFLICTO_INVENTARIOINICIAL(
                    O_form = O_form,
                    dt_form_fecha_movimiento = _dt_form_fecha_movimiento,
                    s_empresa_id = _s_empresa_id,
                    s_empresa_plaza_sucursal_almacen_id = _s_empresa_plaza_sucursal_almacen_id
                    )
                
                if not O_form.vars.remision:
                    O_form.errors.remision = "La remisión es requerida."
                else:
                    pass
                
                if not O_form.vars.descripcion:
                    O_form.errors.descripcion = "La descripción es requerida."
                else:
                    pass
                    
                return O_form  

    class ACEPTADO:
        class ENTRADA:
            @classmethod
            def PORCOMPRA(cls, O_form):
                """Validación onAccepted para las entradas por compra
                """

                # TODO eso procede en la importación del CFDI
                _dbTable = dbc01.tempresa_inventariomovimientos
                
                _s_id = O_form.vars.id
                    
                _s_empresa_plaza_sucursal_almacen_id = request.vars.master_empresa_plaza_sucursal_almacen_id
                
                _s_cuenta_aplicacion_id = D_stvFwkCfg.cfg_cuenta_aplicacion_id
                
                _s_empresa_id = TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.GET_EMPRESA_ID( 
                    s_empresa_plaza_sucursal_almacen_id = _s_empresa_plaza_sucursal_almacen_id
                    )
                                
                _dbRows_mvtosDetalle = dbc01(
                    dbc01.tempresa_invmovto_productos.empresa_inventariomovimiento_id == _s_id
                    ).select(
                        dbc01.tempresa_invmovto_productos.ALL
                        )
                
                if O_form.vars.cfdi_id and not _dbRows_mvtosDetalle:
                    # Si esta definido el cfdi y no hay detalle, se insertan los productos del cfdi en el detalle de resumen
                    
                    _dbFieldCalculado = (
                            dbc01.tcfdi_conceptos.cantidad -
                            (dbc01.tempresa_invmovto_productos.cantidad_entrada.coalesce_zero().sum())
                        )
                    
                    _dbRowsConceptos = dbc01(
                            dbc01.tcfdi_conceptos.cfdi_id == O_form.vars.cfdi_id
                        ).select(
                            dbc01.tcfdi_conceptos.id,
                            dbc01.tcfdi_conceptos.empresa_prodserv_id,
                            dbc01.tcfdi_conceptos.noidentificacion,
                            dbc01.tcfdi_conceptos.cantidad,
                            dbc01.tcfdi_conceptos.valorunitario,
                            dbc01.tcfdi_conceptos.descripcion,
                            dbc01.tcfdi_conceptos.claveunidad,
                            dbc01.tcfdi_conceptos.importe,
                            dbc01.tempresa_invmovto_productos.cantidad_entrada.coalesce_zero().sum().with_alias('registrado'),
                            _dbFieldCalculado.with_alias('cantidad_pendiente'),
                            left = [
                                dbc01.tempresa_invmovto_productos.on(
                                    (dbc01.tempresa_invmovto_productos.cfdi_concepto_id == dbc01.tcfdi_conceptos.id)
                                    ),
                                ],
                            groupby = [
                                dbc01.tcfdi_conceptos.id,
                                dbc01.tcfdi_conceptos.empresa_prodserv_id,
                                dbc01.tcfdi_conceptos.noidentificacion,
                                dbc01.tcfdi_conceptos.cantidad,
                                dbc01.tcfdi_conceptos.valorunitario,
                                dbc01.tcfdi_conceptos.descripcion,
                                dbc01.tcfdi_conceptos.claveunidad,
                                ],
                            having = (_dbFieldCalculado > 0)
                            )
                        
                    for _dbRowConcepto in _dbRowsConceptos:
                        
                        if _dbRowConcepto.tcfdi_conceptos.claveunidad not in ("E48", "ACT"):
                            # Si el concepto no es servicio, ni bonificación, se considera producto y es insertado
                            
                            #Diccionario para insertar los movimientos a nivel resumenes y productos.
                            _D_insert_Helper = Storage (
                                empresa_inventariomovimiento_id = _s_id,
                                empresa_producto_id = _dbRowConcepto.tcfdi_conceptos.empresa_prodserv_id,
                                producto_externo = _dbRowConcepto.tcfdi_conceptos.noidentificacion,
                                cantidad_entrada = _dbRowConcepto.tcfdi_conceptos.cantidad - (_dbRowConcepto.registrado or 0),
                                preciounitario = _dbRowConcepto.tcfdi_conceptos.valorunitario,
                                cfdi_concepto_id = _dbRowConcepto.tcfdi_conceptos.id,
                                descripcion = _dbRowConcepto.tcfdi_conceptos.descripcion,
                                costototal = _dbRowConcepto.tcfdi_conceptos.importe
                                )
                            
                            dbc01.tempresa_invmovto_productos.insert(**_D_insert_Helper)
                        else:
                            pass
                        
                    _dbRows_mvtosDetalle = dbc01(
                        dbc01.tempresa_invmovto_productos.empresa_inventariomovimiento_id == _s_id
                        ).select(
                            dbc01.tempresa_invmovto_productos.ALL
                            )
                        
                    if not _dbRows_mvtosDetalle:
                        # Si no se inserto nada, el cfdi no contiene conceptos
                        stv_flashWarning("Inconsistencias en productos", "CFDI no contiene productos")
                    else:
                        pass  
                                
                else:
                    
                    pass
                
                _dbRow_maestro = dbc01.tempresa_inventariomovimientos(_s_id)
                _L_msgErrores = []
                for _dbRow_detalle in _dbRows_mvtosDetalle:
                    
                    
                    _D_results = TEMPRESA_INVMOVTO_PRODUCTOS.VALIDAR_ROW_ENTRADA_COMPRA(
                        _dbRow_detalle.id,
                        _s_empresa_id, 
                        _dbRow_maestro
                        )
                    
                    if _D_results.E_return != CLASS_e_RETURN.OK:
                        _L_msgErrores += [_D_results.s_msgError]
                    else:
                        pass
                
                if _L_msgErrores:
                    stv_flashWarning("Inconsistencias en productos", "; ".join(_L_msgErrores))  
                
                return O_form
            
            @classmethod
            def PORTRASPASO(cls, O_form):
                """Validación onAccepted para las entradas por traspaso
                """
                
                _dbTable = dbc01.tempresa_inventariomovimientos
                    
                _s_empresa_plaza_sucursal_almacen_id = request.vars.master_empresa_plaza_sucursal_almacen_id
                
                _s_cuenta_aplicacion_id = D_stvFwkCfg.cfg_cuenta_aplicacion_id
                
                _s_empresa_id = TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.GET_EMPRESA_ID( 
                    s_empresa_plaza_sucursal_almacen_id = _s_empresa_plaza_sucursal_almacen_id
                    )                
                _n_concepto_salida_id = TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.GET_CONCEPTO_ID(
                    n_empresa_id = _s_empresa_id, 
                    n_tipoconcepto = TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.SALIDA,
                    n_clasificacion = TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.TRASPASO_SALIDA
                    )
                
                _n_concepto_entrada_id = TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.GET_CONCEPTO_ID(
                    n_empresa_id = _s_empresa_id, 
                    n_tipoconcepto = TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.ENTRADA,
                    n_clasificacion = TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.TRASPASO_ENTRADA
                    )
            
                _dbRows_mvtosDetalle = dbc01(
                    dbc01.tempresa_invmovto_productos.empresa_inventariomovimiento_id == O_form.vars.id
                    ).select(
                        dbc01.tempresa_invmovto_productos.ALL
                        )
                
                if not _dbRows_mvtosDetalle:
                    
                    _dbT_movtos = dbc01.tempresa_inventariomovimientos.with_alias('T_movtos') 
                    _dbRows_movtos =  dbc01(
                        (
                            (_dbT_movtos.empresa_almacenesconcepto_id == _n_concepto_salida_id)
                            &(_dbT_movtos.empresa_embarque_id == O_form.vars.empresa_embarque_id)
                            &(_dbT_movtos.empresa_embarque_almacen_id_relacionado == _dbTable.empresa_embarque_almacen_id.default)
                            &(_dbT_movtos.empresa_embarque_almacen_id == O_form.vars.empresa_embarque_almacen_id_relacionado)
                            )
                          | (
                              (_dbT_movtos.empresa_almacenesconcepto_id == _n_concepto_entrada_id)
                              &(_dbT_movtos.empresa_embarque_id == O_form.vars.empresa_embarque_id)
                              &(_dbT_movtos.empresa_embarque_almacen_id_relacionado == O_form.vars.empresa_embarque_almacen_id_relacionado)
                              &(_dbT_movtos.empresa_embarque_almacen_id == _dbTable.empresa_embarque_almacen_id.default)
                              )
                        )._select(
                            _dbT_movtos.id
                            )
                    
                    _dbT_resumenes = dbc01.tempresa_invmovto_productos.with_alias('T_resumenes')
                    
                    _dbFieldCalculado = (
                        (_dbT_resumenes.cantidad_salida.coalesce_zero().sum()) -
                        (_dbT_resumenes.cantidad_entrada.coalesce_zero().sum())
                        )        
                    
                    _dbRowsConceptos = dbc01(
                            (_dbT_resumenes.empresa_inventariomovimiento_id.belongs(_dbRows_movtos))
                        ).select(
                            _dbT_resumenes.empresa_producto_id,
                            _dbT_resumenes.cantidad_salida.coalesce_zero().sum().with_alias('cantidad_salidas'),
                            _dbT_resumenes.cantidad_entrada.coalesce_zero().sum().with_alias('cantidad_entradas'),
                            _dbFieldCalculado.with_alias('cantidad_pendiente'),
                            groupby = [
                                _dbT_resumenes.empresa_producto_id,
                                ],
                            having = (_dbFieldCalculado > 0)
                            )
                        
                    for _dbRowConcepto in _dbRowsConceptos:
                        
                        #Diccionario para insertar los movimientos a nivel resumenes.
                        _D_insert_Helper = Storage (
                            empresa_inventariomovimiento_id = O_form.vars.id,
                            empresa_producto_id = _dbRowConcepto.T_resumenes.empresa_producto_id,
                            cantidad_entrada = (_dbRowConcepto.cantidad_salidas - _dbRowConcepto.cantidad_entradas),
                            )
                        
                        dbc01.tempresa_invmovto_productos.insert(**_D_insert_Helper)
            
                        
                    _dbRows_mvtosDetalle = dbc01(
                        dbc01.tempresa_invmovto_productos.empresa_inventariomovimiento_id == O_form.vars.id
                        ).select(
                            dbc01.tempresa_invmovto_productos.ALL
                            )
                        
                    if not _dbRows_mvtosDetalle:
                        # Si no se inserto nada, el cfdi no contiene conceptos
                        stv_flashWarning("Inconsistencias en productos", "No se insertaron productos, verificar si las salidas asociadas a este embarque tienen cantidad pendiente.")
                    else:
                        pass  
                                
                else:
                    pass
                
                _dbRow_maestro = dbc01.tempresa_inventariomovimientos(O_form.vars.id)
                _L_msgErrores = []
                for _dbRow_detalle in _dbRows_mvtosDetalle:
                      
                      
                    _D_results = TEMPRESA_INVMOVTO_PRODUCTOS.VALIDAR_ROW_ENTRADA_PORTRASPASO(
                        _dbRow_detalle.id,
                        _s_empresa_id, 
                        _dbRow_maestro)
                      
                    if _D_results.E_return != CLASS_e_RETURN.OK:
                        _L_msgErrores += [_D_results.s_msgError]
                    else:
                        pass
                  
                if _L_msgErrores:
                    stv_flashWarning("Inconsistencias en productos", "; ".join(_L_msgErrores))
                else:
                    pass
                
                return O_form
            
        class SALIDA:
            @classmethod
            def PORDEVOLUCION_COMPRA(cls, O_form):
                """Validación onAccepted para las entradas por compra
                """
                
                _dbTable = dbc01.tempresa_inventariomovimientos
                
                _s_id = O_form.vars.id
                    
                _s_empresa_plaza_sucursal_almacen_id = request.vars.master_empresa_plaza_sucursal_almacen_id
                
                _s_empresa_id = TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.GET_EMPRESA_ID( 
                    s_empresa_plaza_sucursal_almacen_id = _s_empresa_plaza_sucursal_almacen_id
                    )
                 
                _dbRows_mvtosDetalle = dbc01(
                    dbc01.tempresa_invmovto_productos.empresa_inventariomovimiento_id == _s_id
                    ).select(
                        dbc01.tempresa_invmovto_productos.ALL
                        )
                
                #Se verifica primero el de egreso porque es el que tiene mayor prioridad en este movimiento.
                if O_form.vars.cfdi_egreso_id and not _dbRows_mvtosDetalle:
                    # Si esta definido el cfdi y no hay detalle, se insertan los productos del cfdi en el detalle de resumen
                    
                    _dbRowsConceptos = dbc01(
                            dbc01.tcfdi_conceptos.cfdi_id == O_form.vars.cfdi_egreso_id
                        ).select(
                            dbc01.tcfdi_conceptos.id,
                            dbc01.tcfdi_conceptos.empresa_prodserv_id,
                            dbc01.tcfdi_conceptos.noidentificacion,
                            dbc01.tcfdi_conceptos.cantidad,
                            dbc01.tcfdi_conceptos.valorunitario,
                            dbc01.tcfdi_conceptos.descripcion,
                            dbc01.tcfdi_conceptos.claveunidad,
                            dbc01.tcfdi_conceptos.importe,
                            dbc01.tempresa_invmovto_productos.cantidad_salida.coalesce_zero().sum().with_alias('registrado'),
                            left = [
                                dbc01.tempresa_invmovto_productos.on(
                                    (dbc01.tempresa_invmovto_productos.cfdi_concepto_id == dbc01.tcfdi_conceptos.id)
                                    ),
                                ],
                            groupby = [
                                dbc01.tcfdi_conceptos.id,
                                dbc01.tcfdi_conceptos.empresa_prodserv_id,
                                dbc01.tcfdi_conceptos.noidentificacion,
                                dbc01.tcfdi_conceptos.cantidad,
                                dbc01.tcfdi_conceptos.valorunitario,
                                dbc01.tcfdi_conceptos.descripcion,
                                dbc01.tcfdi_conceptos.claveunidad,
                                ]
                            )
                        
                    for _dbRowConcepto in _dbRowsConceptos:
                        
                        ### TODO: Preguntar las claves de unidad permitidas en facturas de egreso. (ACT sí está permitida al contrario de una factura de ingreso)
                        if _dbRowConcepto.tcfdi_conceptos.claveunidad in "ACT":
                            # Si el concepto no es servicio, ni bonificación, se considera producto y es insertado
                            
                            #Diccionario para insertar los movimientos a nivel resumenes y productos.
                            _D_insert_Helper = Storage (
                                empresa_inventariomovimiento_id = _s_id,
                                empresa_producto_id = _dbRowConcepto.tcfdi_conceptos.empresa_prodserv_id,
                                producto_externo = _dbRowConcepto.tcfdi_conceptos.noidentificacion,
                                cantidad_salida = _dbRowConcepto.tcfdi_conceptos.cantidad - _dbRowConcepto.registrado, #La cantidad de salida es 0 si viene desde un CFDI de ingreso.
                                preciounitario = _dbRowConcepto.tcfdi_conceptos.valorunitario,
                                cfdi_concepto_id = _dbRowConcepto.tcfdi_conceptos.id,
                                descripcion = _dbRowConcepto.tcfdi_conceptos.descripcion,
                                costototal = _dbRowConcepto.tcfdi_conceptos.importe #Preguntar si es necesario estos campos a la hora de una salida?
                                )
                            
                            dbc01.tempresa_invmovto_productos.insert(**_D_insert_Helper)
                        else:
                            pass
                        
                    _dbRows_mvtosDetalle = dbc01(
                        dbc01.tempresa_invmovto_productos.empresa_inventariomovimiento_id == _s_id
                        ).select(
                            dbc01.tempresa_invmovto_productos.ALL
                            )
                
                    if not _dbRows_mvtosDetalle:
                        # Si no se inserto nada, el cfdi no contiene conceptos
                        stv_flashWarning("Inconsistencias en productos", "CFDI no contiene productos")
                    else:
                        pass  
                    
                elif O_form.vars.cfdi_id and not _dbRows_mvtosDetalle:
                    # Si esta definido el cfdi y no hay detalle, se insertan los productos del cfdi en el detalle de resumen
                    
                    _dbRowsConceptos = dbc01(
                            dbc01.tcfdi_conceptos.cfdi_id == O_form.vars.cfdi_id
                        ).select(
                            dbc01.tcfdi_conceptos.id,
                            dbc01.tcfdi_conceptos.empresa_prodserv_id,
                            dbc01.tcfdi_conceptos.noidentificacion,
                            dbc01.tcfdi_conceptos.cantidad,
                            dbc01.tcfdi_conceptos.valorunitario,
                            dbc01.tcfdi_conceptos.descripcion,
                            dbc01.tcfdi_conceptos.claveunidad,
                            dbc01.tcfdi_conceptos.importe,
                            )
                        
                    for _dbRowConcepto in _dbRowsConceptos:
                
                        if _dbRowConcepto.claveunidad not in ("E48", "ACT"):
                            # Si el concepto no es servicio, ni bonificación, se considera producto y es insertado
                            
                            #Diccionario para insertar los movimientos a nivel resumenes y productos.
                            _D_insert_Helper = Storage (
                                empresa_inventariomovimiento_id = _s_id,
                                empresa_producto_id = _dbRowConcepto.empresa_prodserv_id,
                                producto_externo = _dbRowConcepto.noidentificacion,
                                cantidad_salida = 0, #La cantidad de salida es 0 si viene desde un CFDI de ingreso.
                                preciounitario = _dbRowConcepto.valorunitario,
                                cfdi_concepto_id = _dbRowConcepto.id,
                                descripcion = _dbRowConcepto.descripcion,
                                )
                            
                            dbc01.tempresa_invmovto_productos.insert(**_D_insert_Helper)
                        else:
                            pass
                        
                    _dbRows_mvtosDetalle = dbc01(
                        dbc01.tempresa_invmovto_productos.empresa_inventariomovimiento_id == _s_id
                        ).select(
                            dbc01.tempresa_invmovto_productos.ALL
                            )
                        
                    if not _dbRows_mvtosDetalle:
                        # Si no se inserto nada, el cfdi no contiene conceptos
                        stv_flashWarning("Inconsistencias en productos", "CFDI no contiene productos")
                    else:
                        pass
                
                    # Después que el CFDI se utilizo para tomar los productos de la salida por devolución, se limpia.
                    dbc01.tempresa_inventariomovimientos(_s_id).update_record(cfdi_id=None)
                                
                else:
                    pass
                
                _dbRow_maestro = dbc01.tempresa_inventariomovimientos(_s_id)
                _L_msgErrores = []
                for _dbRow_detalle in _dbRows_mvtosDetalle:
                     
                     
                    _D_results = TEMPRESA_INVMOVTO_PRODUCTOS.VALIDAR_ROW_SALIDA_DEVOLUCION_COMPRA(
                        _dbRow_detalle.id,
                        _s_empresa_id, 
                        _dbRow_maestro)
                     
                    if _D_results.E_return != CLASS_e_RETURN.OK:
                        _L_msgErrores += [_D_results.s_msgError]
                    else:
                        pass
                         
                if _L_msgErrores:
                    stv_flashWarning("Inconsistencias en productos", "; ".join(_L_msgErrores))
                else:
                    pass  
                
                return O_form

    @classmethod
    def IDENTIFICA_ALMACENCONCEPTO(
            cls,
            s_empresa_id,
            E_tipoconcepto,
            E_clasificacion,
            n_empresa_almacenconcepto_id = None
            ):
        """ Obtiene el id del concepto en la empresa

        @param s_empresa_id:
        @type s_empresa_id:
        @param E_tipoconcepto:
        @type E_tipoconcepto:
        @param E_clasificacion:
        @type E_clasificacion:
        @param n_empresa_almacenconcepto_id:
        @type n_empresa_almacenconcepto_id:
        @return:
        @rtype:
        """

        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            n_empresa_almacenconcepto_id = n_empresa_almacenconcepto_id,
            )

        # Se identifica o reusa el uso del almacenconcepto_id
        if n_empresa_almacenconcepto_id:
            pass
        else:
            _D_results = TEMPRESA_ALMACENESCONCEPTOS.GET_ID_CACHE(
                n_empresa_id = s_empresa_id,
                n_tipoconcepto = E_tipoconcepto,
                n_clasificacion = E_clasificacion
                )
            if _D_results.E_return == CLASS_e_RETURN.OK:
                _D_return.n_empresa_almacenconcepto_id = _D_results.n_concepto_id
            else:
                _D_return.E_return = _D_result_concepto_id.E_return
                _D_return.s_msgError = _D_result_concepto_id.s_msgError

        return _D_return

    pass


dbc01.define_table(
    'tempresa_inventariomovimientos',
    Field(
        'empresa_id', dbc01.tempresas, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Empresa', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'empresa_almacenesconcepto_id', dbc01.tempresa_almacenesconceptos, default = None,
        required = True,
        ondelete = 'NO ACTION', notnull = True, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Concepto de Almacén', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_inventariomovimientos.empresa_almacenesconcepto_id)
        ),
    Field(
        'empresa_proveedor_id', dbc01.tempresa_proveedores, default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Proveedor', comment = None,
        writable = False, readable = True,
        represent = stv_represent_referencefield
        ),
    Field(
        'empresa_cliente_id', dbc01.tempresa_clientes, default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_search(
            f, v,
            s_display = '%(nombrecorto)s [%(codigo)s]',
            s_url = URL(a = 'app_empresas', c = '130clientes', f = 'empresa_clientes_buscar'),
            D_additionalAttributes = {'reference': dbc01.tempresa_clientes.id}
            ),
        label = 'Cliente', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tempresa_inventariomovimientos.empresa_cliente_id
            )
        ),
    Field(
        'tipodocumento', 'integer', default = 1,
        required = True, requires = IS_IN_SET(
            TEMPRESA_INVENTARIOMOVIMIENTOS.E_TIPODOCUMENTO.get_dict(), zero = 0, error_message = 'Selecciona'
            ),
        notnull = True, unique = False,
        widget = stv_widget_combobox,
        label = 'Tipo de documento', comment = 'Selecciona el tipo de documento con el cual se avalará esta entrada',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_INVENTARIOMOVIMIENTOS.E_TIPODOCUMENTO.get_dict())
        ),
    Field(
        'remision', 'string', length = 50, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Remisión', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'cfdi_id', 'integer', default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_search(
            f,
            v,
            s_display = '%(serie)s %(folio)s',
            s_url = URL(a = 'app_almacenes', c = '600busquedas', f = 'buscar_cfdis_recibidos'),
            D_additionalAttributes = {
                'reference': dbc01.tcfdis.id,
                }
            ),
        label = 'CFDI Relacionado (Serie/Folio/Tipo)', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_inventariomovimientos.cfdi_id)
        ),
    Field(
        'cfdi_egreso_id', 'integer', default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_search(
            f,
            v,
            s_display = '%(serie)s %(folio)s',
            s_url = URL(a = 'app_almacenes', c = '600busquedas', f = 'buscar_cfdis_egreso_recibidos'),
            D_additionalAttributes = {
                'reference': dbc01.tcfdis.id,
                }
            ),
        label = 'CFDI de egreso relacionado', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_inventariomovimientos.cfdi_egreso_id)
        ),
    Field(
        'total', 'decimal(14,4)', default = 0,
        required = True,
        notnull = False, unique = False,
        widget = stv_widget_inputMoney, label = 'Total',
        comment = 'Total de la entrada puesto en la remisión o factura',
        writable = False, readable = True,
        represent = stv_represent_money_ifnotzero
        ),
    Field(
        'tc', 'string', length = 50, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'TC', comment = 'Código de Ticket',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'observaciones', 'text', default = None,
        required = False,
        notnull = False, unique = False,
        widget = lambda v, r: stv_widget_text(v, r, D_additionalAttributes = {'n_rows': 2}), label = 'Observaciones',
        comment = None,
        writable = True, readable = True,
        represent = None
        ),
    Field(
        'ordencompra', 'string', length = 20, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Orden de compra', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'empresa_plaza_sucursal_almacen_id', dbc01.tempresa_plaza_sucursal_almacenes, default = None,
        required = True,
        ondelete = 'NO ACTION', notnull = True, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Almacén', comment = None,
        writable = False, readable = True,
        represent = stv_represent_referencefield
        ),
    Field(
        'fecha_movimiento', type = FIELD_UTC_DATETIME, default = request.browsernow,
        required = False, requires = IS_NULL_OR(IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME)),
        notnull = False,
        widget = stv_widget_inputDateTime, label = 'Fecha movimiento', comment = 'Fecha del movimiento físico',
        writable = False, readable = True,
        represent = stv_represent_datetime
        ),
    Field(
        'fecha_registro', type = FIELD_UTC_DATETIME, default = request.browsernow,
        required = False, requires = IS_NULL_OR(IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME)),
        notnull = False,
        widget = stv_widget_inputDateTime, label = 'Fecha de registro', comment = 'Fecha del registro del movimiento',
        writable = False, readable = True,
        represent = stv_represent_datetime
        ),
    Field(
        'folio', 'string', length = 50, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Folio bitacora', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'empresa_embarque_almacen_id', dbc01.tempresa_embarque_almacenes, default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1),
        label = 'Almacén de destino', comment = None,
        writable = False, readable = True,
        represent = stv_represent_referencefield
        ),
    # Este campo solo se usa para salidas por traspaso
    Field(
        'empresa_embarque_almacen_id_relacionado', dbc01.tempresa_embarque_almacenes, default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda field, value: stv_widget_db_chosen_linked(
            field,
            value,
            1,
            'empresa_embarque_id',
            URL(a = 'app_almacenes', c = '620salidas', f = 'embarque_almacenes_salidas_ver.json'),
            ),
        label = 'Almacén de destino', comment = None,
        writable = False, readable = True,
        represent = stv_represent_referencefield
        ),
    Field(
        'empresa_embarque_id', dbc01.tempresa_embarques, default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Embarque a usar', comment = None,
        writable = False, readable = True,
        represent = stv_represent_referencefield
        ),
    Field(
        'descripcion', 'text', length = 100, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Descripcion', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'inconsistencias', 'text', length = 200, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Inconsistencias', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'estatus_movimiento', 'integer', default = TEMPRESA_INVENTARIOMOVIMIENTOS.ESTATUS_MOVIMIENTO.NO_DEFINIDO,
        required = True, requires = IS_IN_SET(
            TEMPRESA_INVENTARIOMOVIMIENTOS.ESTATUS_MOVIMIENTO.GET_DICT(), zero = 0, error_message = 'Selecciona'
            ),
        notnull = True, unique = False,
        widget = stv_widget_combobox,
        label = 'Estatus del movimiento', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_list(
            v, r, None, TEMPRESA_INVENTARIOMOVIMIENTOS.ESTATUS_MOVIMIENTO.GET_DICT()
            )
        ),
    Field(
        'fecha_salida', type = FIELD_UTC_DATETIME, default = request.browsernow,
        required = False, requires = IS_NULL_OR(IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME)),
        notnull = False,
        widget = stv_widget_inputDateTime, label = 'Fecha de salida', comment = None,
        writable = False, readable = True,
        represent = stv_represent_datetime
        ),

    # En caso de importación son requeridos los siguiente campos
    # Campos necesarios cuando la entrada es por importación
    Field(
        'infoaduaneranopedimento', 'string', length = 30, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Info. Aduanera Pedimento', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'moneda_id', 'integer', default = D_stvSiteHelper.dbconfigs.defaults.moneda_id,
        required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tmonedas.id', db01.tmonedas._format)),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Moneda', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_inventariomovimientos.moneda_id)
        ),
    Field(
        'tipocambio', 'decimal(14,4)', default = 1,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputMoney, label = 'Tipo de cambio', comment = None,
        writable = False, readable = True,
        represent = stv_represent_money_ifnotzero
        ),
    Field(
        'fechaarrivo', type = FIELD_UTC_DATETIME, default = None,
        required = False, requires = IS_NULL_OR(IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME)),
        notnull = False,
        widget = stv_widget_inputDateTime, label = 'Fecha Arrivo', comment = '',
        writable = False, readable = True,
        represent = stv_represent_datetime
        ),
    Field(
        'puerto', 'string', length = 100, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Puerto', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'agente', 'string', length = 200, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Agente', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'pais_id', 'integer', default = None,
        required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tpaises.id', db01.tpaises._format)),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = T('País'), comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_inventariomovimientos.pais_id)
        ),

    tSignature_dbc01,
    format='%(id)s',
    singular='Inventario movimiento',
    plural='Inventarios movimientos',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )
dbc01.tempresa_inventariomovimientos.id.label = 'Folio'



class TEMPRESA_INVMOVTO_PRODUCTOS:
    """ Definición de la tabla de tempresa_invmovto_productos.

     Esta tabla concentra el resumen de los movimientos de los productos, justo antes del costeo. El costeo se realizará
     hasta que los movimientos se encuentren cerrados y se tenga toda la información necesaria para el costeo.
     """

    class Tipo_Inconsistencia:
        """ Se definen las opciónes del campo """
        SIN_INCONSISTENCIA = 0
        PRECIO = 1
        CANTIDAD = 2
        SIN_CFDI = 3
        PRODUCTO_ID = 4
        NO_EN_CFDI = 5
        CANTIDAD_ENTRADA = 6
        OTRA = 7

        @classmethod
        @cache("TEMPRESA_INVMOVTO_PRODUCTOS.Tipo_Inconsistencia", time_expire = 86400, cache_model = cache.ram)
        def GET_DICT(cls):
            # Revisar el manejo del cache
            return {
                cls.SIN_INCONSISTENCIA: 'Sin inconsistencia',
                cls.PRECIO: 'Diferencia en precio',
                cls.CANTIDAD: 'Cantidad mayor a CFDI',
                cls.SIN_CFDI: 'Sin CFDI',
                cls.PRODUCTO_ID: 'El producto no coincide',
                cls.NO_EN_CFDI: 'No se encuentra en CFDI',
                cls.CANTIDAD_ENTRADA: 'Cantidad mayor a la de la salida.',
                cls.OTRA: 'Otra',
                }

    @classmethod
    def VALIDAR_ROW_ENTRADA_COMPRA(cls, n_id, n_empresa_id = None, dbRow_maestro = None):
        """ Valida que el contenido del row se encuentre sin inconsistencias con la referencia al CFDI asociado, o al movimiento referenciado
        
        dbRow_maestro se utiliza para reusar la consulta y no estar haciendo la misma consulta multiples veces
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            n_tipoinconsistencia = TEMPRESA_INVMOVTO_PRODUCTOS.Tipo_Inconsistencia.SIN_INCONSISTENCIA,
            dbRow_maestro = None
            )        
        
        _dbRow = dbc01.tempresa_invmovto_productos(n_id)
        if not _dbRow:
            _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
            _D_return.s_msgError = "Registro no encontrado"
        else:
            if not dbRow_maestro:
                dbRow_maestro = dbc01.tempresa_inventariomovimientos(_dbRow.empresa_inventariomovimiento_id)
            else:
                pass
                
            if not dbRow_maestro or (dbRow_maestro.id != _dbRow.empresa_inventariomovimiento_id):
                _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                _D_return.s_msgError = "Movimiento del registro no encontrado o no coincide"
            else:
                _D_return.dbRow_maestro = dbRow_maestro
                
                if not n_empresa_id:
                    _n_empresa_id = dbc01.tempresa_almacenesconceptos(dbRow_maestro.empresa_almacenesconcepto_id).empresa_id
                else:
                    _n_empresa_id = n_empresa_id
                
                _D_result_concepto_id = TEMPRESA_ALMACENESCONCEPTOS.GET_ID_CACHE(
                    n_empresa_id = _n_empresa_id, 
                    n_tipoconcepto = TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.ENTRADA, 
                    n_clasificacion = TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.COMPRA
                    )
                _s_concepto_entrada_compra_id = _D_result_concepto_id.n_concepto_id
                
                
                if dbRow_maestro.empresa_almacenesconcepto_id != _s_concepto_entrada_compra_id:
                    _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                    _D_return.s_msgError = "Concepto de movimiento no corresponde a Entrada por Compra"
                else:
                    
                    # La primer inconsistencia que se checa es saber si tiene cfdi
                    if not dbRow_maestro.cfdi_id:
                        _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                        _D_return.s_msgError = "Concepto de movimiento no corresponde a Entrada por Compra"
                        _D_return.n_tipoinconsistencia = TEMPRESA_INVMOVTO_PRODUCTOS.Tipo_Inconsistencia.SIN_CFDI
                    else:
                        
                        if not _dbRow.producto_externo and _dbRow.empresa_producto_id:
                            # Si el movimiento no tiene producto externo, se le intenta asociar uno
                            _dbT_prov_codigoprod = dbc01.tempresa_prodserv_codigosproveedores
                            
                            _dbRows_proveedorCodigo = dbc01(
                                (_dbT_prov_codigoprod.empresa_proveedor_id == dbRow_maestro.empresa_proveedor_id)
                                & (_dbT_prov_codigoprod.empresa_prodserv_id == _dbRow.empresa_producto_id)
                                & (_dbT_prov_codigoprod.bansuspender == False)
                                ).select(
                                    _dbT_prov_codigoprod.ALL,
                                    orderby = ~_dbT_prov_codigoprod.id
                                    )
                            
                            if _dbRows_proveedorCodigo:
                                _dbRow.producto_externo = _dbRows_proveedorCodigo.first().codigo
                            else:
                                pass
                        else:
                            pass
                        
                        if _dbRow.producto_externo:
                            # Se busca el producto dentro de los conceptos del cfdi
                            _dbRows_conceptos = dbc01(
                                (dbc01.tcfdi_conceptos.cfdi_id == dbRow_maestro.cfdi_id)
                                & (dbc01.tcfdi_conceptos.noidentificacion == _dbRow.producto_externo)
                                ).select(
                                    dbc01.tcfdi_conceptos.id,
                                    dbc01.tcfdi_conceptos.empresa_prodserv_id,
                                    dbc01.tcfdi_conceptos.noidentificacion,
                                    dbc01.tcfdi_conceptos.cantidad,
                                    dbc01.tcfdi_conceptos.valorunitario,
                                    dbc01.tcfdi_conceptos.descripcion,
                                    dbc01.tcfdi_conceptos.claveunidad,
                                    dbc01.tempresa_invmovto_productos.cantidad_entrada.coalesce_zero().sum().with_alias('registrado'),
                                    left = [
                                        dbc01.tempresa_invmovto_productos.on(
                                            (dbc01.tempresa_invmovto_productos.cfdi_concepto_id == dbc01.tcfdi_conceptos.id)
                                            ),
                                        ],
                                    groupby = [
                                        dbc01.tcfdi_conceptos.id,
                                        dbc01.tcfdi_conceptos.empresa_prodserv_id,
                                        dbc01.tcfdi_conceptos.noidentificacion,
                                        dbc01.tcfdi_conceptos.cantidad,
                                        dbc01.tcfdi_conceptos.valorunitario,
                                        dbc01.tcfdi_conceptos.descripcion,
                                        dbc01.tcfdi_conceptos.claveunidad,
                                        ]
                                    
                                    )
                            
                            if len(_dbRows_conceptos) != 1:
                                # Si se encontró más de un registro o ningún registro, no se puede asociar con cfdi
                                _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                                _D_return.s_msgError = "No se pudo localizar en CFDI"
                                _D_return.n_tipoinconsistencia = TEMPRESA_INVMOVTO_PRODUCTOS.Tipo_Inconsistencia.NO_EN_CFDI
                            else:
                                _dbRow_concepto = _dbRows_conceptos.first()
                                _dbRow_concepto_cfdi = _dbRow_concepto.tcfdi_conceptos
                                
                                if _dbRow_concepto_cfdi.empresa_prodserv_id and not _dbRow.empresa_producto_id:
                                    _dbRow.empresa_producto_id = _dbRow_concepto_cfdi.empresa_prodserv_id
                                elif not _dbRow_concepto_cfdi.empresa_prodserv_id and _dbRow.empresa_producto_id:

                                    _dbRow_concepto_cfdi.empresa_prodserv_id = _dbRow.empresa_producto_id
                                    
                                    #Se hace el update en esta parte para evitar que se genere inconsistencia más adelante y se tenga que repetir este proceso.
                                    _dbRow_concepto_cfdi.update_record()
                                    
                                elif not _dbRow_concepto_cfdi.empresa_prodserv_id and not _dbRow.empresa_producto_id:
                                    
                                    _dbRow_cfdi = dbc01.tcfdis(dbRow_maestro.cfdi_id)
                                    
                                    _dbRows_proveedorCodigo = dbc01(
                                        (dbc01.tempresa_prodserv_codigosproveedores.empresa_proveedor_id == _dbRow_cfdi.emisorproveedor_id)
                                        & (dbc01.tempresa_prodserv_codigosproveedores.codigo == _dbRow_concepto_cfdi.noidentificacion)
                                        & (dbc01.tempresa_prodserv_codigosproveedores.bansuspender == False)
                                        ).select(
                                            dbc01.tempresa_prodserv_codigosproveedores.ALL,
                                            orderby = ~dbc01.tempresa_prodserv_codigosproveedores.id
                                            )
                                    
                                    if _dbRows_proveedorCodigo:
                                        _dbRow_proveedorCodigo = _dbRows_proveedorCodigo.first()
                                        dbc01(
                                            dbc01.tcfdi_conceptos.id == _dbRow_concepto_cfdi.id
                                            ).update(
                                                empresa_prodserv_id = _dbRow_proveedorCodigo.empresa_prodserv_id
                                                )
                                        _dbRow.empresa_producto_id = _dbRow_proveedorCodigo.empresa_prodserv_id
                                    else:
                                        pass
                                elif _dbRow_concepto_cfdi.empresa_prodserv_id != _dbRow.empresa_producto_id:
                                    _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                                    _D_return.s_msgError = "Producto de proveedor y producto de movimiento no coinciden a pesar de usar mismo codigo"
                                    _D_return.n_tipoinconsistencia = TEMPRESA_INVMOVTO_PRODUCTOS.Tipo_Inconsistencia.PRODUCTO_ID
                                else:
                                    pass
                                    
                                
                                if not _dbRow_concepto_cfdi.empresa_prodserv_id:
                                    _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                                    _D_return.s_msgError = "Producto de proveedor no mapeado a producto en el sistema"
                                    _D_return.n_tipoinconsistencia = TEMPRESA_INVMOVTO_PRODUCTOS.Tipo_Inconsistencia.PRODUCTO_ID
                                elif _dbRow_concepto_cfdi.valorunitario != _dbRow.preciounitario:
                                    if _dbRow.preciounitario:
                                        _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                                        _D_return.s_msgError = "Costo unitario de movimiento no corresponde a costo en CFDI"
                                        _D_return.n_tipoinconsistencia = TEMPRESA_INVMOVTO_PRODUCTOS.Tipo_Inconsistencia.PRECIO
                                    else:
                                        _dbRow.preciounitario = _dbRow_concepto_cfdi.valorunitario
                                elif _dbRow_concepto_cfdi.cantidad < _dbRow_concepto.registrado:
                                    _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                                    _D_return.s_msgError = "Cantidad de movimiento es mayor que cantidad en CFDI"
                                    _D_return.n_tipoinconsistencia = TEMPRESA_INVMOVTO_PRODUCTOS.Tipo_Inconsistencia.CANTIDAD
                                else:
                                    # Todo parece estar bien
                                    # TODO buscar en tempresa_prodserv_codigosproveedores que exista la relación, si no existe
                                    pass
                        else:
                            # Como no existe producto_externo no puede asociarse al CFDI
                            _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                            _D_return.s_msgError = "No se pudo localizar en CFDI"
                            _D_return.n_tipoinconsistencia = TEMPRESA_INVMOVTO_PRODUCTOS.Tipo_Inconsistencia.NO_EN_CFDI
                            pass
                              
            _dbRow.tipo_inconsistencia = _D_return.n_tipoinconsistencia
            _dbRow.update_record()
                            
        return _D_return    

    @classmethod
    def VALIDAR_ROW_SALIDA_DEVOLUCION_COMPRA(cls, n_id, n_empresa_id = None, dbRow_maestro = None):
        """ Valida que el contenido del row se encuentre sin inconsistencias con la referencia al CFDI asociado, o al movimiento referenciado
        
        dbRow_maestro se utiliza para reusar la consulta y no estar haciendo la misma consulta multiples veces
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            n_tipoinconsistencia = TEMPRESA_INVMOVTO_PRODUCTOS.Tipo_Inconsistencia.SIN_INCONSISTENCIA,
            dbRow_maestro = None
            )        
        
        _dbRow = dbc01.tempresa_invmovto_productos(n_id)
        if not _dbRow:
            _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
            _D_return.s_msgError = "Registro no encontrado"
        else:
            if not dbRow_maestro:
                dbRow_maestro = dbc01.tempresa_inventariomovimientos(_dbRow.empresa_inventariomovimiento_id)
            else:
                pass
                
            if not dbRow_maestro or (dbRow_maestro.id != _dbRow.empresa_inventariomovimiento_id):
                _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                _D_return.s_msgError = "Movimiento del registro no encontrado o no coincide"
            else:
                _D_return.dbRow_maestro = dbRow_maestro
                
                if not n_empresa_id:
                    _n_empresa_id = dbc01.tempresa_almacenesconceptos(dbRow_maestro.empresa_almacenesconcepto_id).empresa_id
                else:
                    _n_empresa_id = n_empresa_id
                
                _D_result_concepto_id = TEMPRESA_ALMACENESCONCEPTOS.GET_ID_CACHE(
                    n_empresa_id = _n_empresa_id, 
                    n_tipoconcepto = TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.SALIDA, 
                    n_clasificacion = TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.DEVOLUCION_COMPRA
                    )
                _s_concepto_salida_devolucion_compra_id = _D_result_concepto_id.n_concepto_id
                
                
                if dbRow_maestro.empresa_almacenesconcepto_id != _s_concepto_salida_devolucion_compra_id:
                    _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                    _D_return.s_msgError = "Concepto de movimiento no corresponde a Salida por devolución de compra"
                else:
                    
                    # La primer inconsistencia que se checa es saber si tiene cfdi de egreso
                    if not dbRow_maestro.cfdi_egreso_id:
                        _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                        _D_return.s_msgError = "Concepto de movimiento no corresponde a Salida por devolución de compra"
                        ###TODO: ¿Agregar tipo de inconsistencia SIN_CFDI_EGRESO?
                        _D_return.n_tipoinconsistencia = TEMPRESA_INVMOVTO_PRODUCTOS.Tipo_Inconsistencia.SIN_CFDI
                    else:
                        
                        if not _dbRow.producto_externo and _dbRow.empresa_producto_id:
                            # Si el movimiento no tiene producto externo, se le intenta asociar uno
                            _dbT_prov_codigoprod = dbc01.tempresa_prodserv_codigosproveedores
                            
                            _dbRows_proveedorCodigo = dbc01(
                                (_dbT_prov_codigoprod.empresa_proveedor_id == dbRow_maestro.empresa_proveedor_id)
                                & (_dbT_prov_codigoprod.empresa_prodserv_id == _dbRow.empresa_producto_id)
                                & (_dbT_prov_codigoprod.bansuspender == False)
                                ).select(
                                    _dbT_prov_codigoprod.ALL,
                                    orderby = ~_dbT_prov_codigoprod.id
                                    )
                            
                            if _dbRows_proveedorCodigo:
                                _dbRow.producto_externo = _dbRows_proveedorCodigo.first().codigo
                            else:
                                pass
                        else:
                            pass
                        
                        if _dbRow.producto_externo:
                            # Se busca el producto dentro de los conceptos del cfdi
                            _dbRows_conceptos = dbc01(
                                (dbc01.tcfdi_conceptos.cfdi_id == dbRow_maestro.cfdi_egreso_id)
                                & (dbc01.tcfdi_conceptos.noidentificacion == _dbRow.producto_externo)
                                ).select(
                                    dbc01.tcfdi_conceptos.id,
                                    dbc01.tcfdi_conceptos.empresa_prodserv_id,
                                    dbc01.tcfdi_conceptos.noidentificacion,
                                    dbc01.tcfdi_conceptos.cantidad,
                                    dbc01.tcfdi_conceptos.valorunitario,
                                    dbc01.tcfdi_conceptos.descripcion,
                                    dbc01.tcfdi_conceptos.claveunidad,
                                    dbc01.tempresa_invmovto_productos.cantidad_salida.coalesce_zero().sum().with_alias('registrado'),
                                    left = [
                                        dbc01.tempresa_invmovto_productos.on(
                                            (dbc01.tempresa_invmovto_productos.cfdi_concepto_id == dbc01.tcfdi_conceptos.id)
                                            ),
                                        ],
                                    groupby = [
                                        dbc01.tcfdi_conceptos.id,
                                        dbc01.tcfdi_conceptos.empresa_prodserv_id,
                                        dbc01.tcfdi_conceptos.noidentificacion,
                                        dbc01.tcfdi_conceptos.cantidad,
                                        dbc01.tcfdi_conceptos.valorunitario,
                                        dbc01.tcfdi_conceptos.descripcion,
                                        dbc01.tcfdi_conceptos.claveunidad,
                                        ]
                                    
                                    )
                            
                            if len(_dbRows_conceptos) != 1:
                                # Si se encontró más de un registro o ningún registro, no se puede asociar con cfdi
                                _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                                _D_return.s_msgError = "No se pudo localizar en CFDI"
                                _D_return.n_tipoinconsistencia = TEMPRESA_INVMOVTO_PRODUCTOS.Tipo_Inconsistencia.NO_EN_CFDI
                            else:
                                _dbRow_concepto = _dbRows_conceptos.first()
                                _dbRow_concepto_cfdi = _dbRow_concepto.tcfdi_conceptos
                                
                                if _dbRow_concepto_cfdi.empresa_prodserv_id and not _dbRow.empresa_producto_id:
                                    _dbRow.empresa_producto_id = _dbRow_concepto_cfdi.empresa_prodserv_id
                                elif not _dbRow_concepto_cfdi.empresa_prodserv_id and _dbRow.empresa_producto_id:
                                    dbc01(
                                        dbc01.tcfdi_conceptos.id == _dbRow_concepto_cfdi.id
                                        ).update(
                                            empresa_prodserv_id = _dbRow.empresa_producto_id
                                            )
                                elif not _dbRow_concepto_cfdi.empresa_prodserv_id and not _dbRow.empresa_producto_id:
                                    
                                    _dbRow_cfdi = dbc01.tcfdis(dbRow_maestro.cfdi_egreso_id)
                                    
                                    _dbRows_proveedorCodigo = dbc01(
                                        (dbc01.tempresa_prodserv_codigosproveedores.empresa_proveedor_id == _dbRow_cfdi.emisorproveedor_id)
                                        & (dbc01.tempresa_prodserv_codigosproveedores.codigo == _dbRow_concepto_cfdi.noidentificacion)
                                        & (dbc01.tempresa_prodserv_codigosproveedores.bansuspender == False)
                                        ).select(
                                            dbc01.tempresa_prodserv_codigosproveedores.ALL,
                                            orderby = ~dbc01.tempresa_prodserv_codigosproveedores.id
                                            )
                                    
                                    if _dbRows_proveedorCodigo:
                                        _dbRow_proveedorCodigo = _dbRows_proveedorCodigo.first()
                                        dbc01(
                                            dbc01.tcfdi_conceptos.id == _dbRow_concepto_cfdi.id
                                            ).update(
                                                empresa_prodserv_id = _dbRow_proveedorCodigo.empresa_prodserv_id
                                                )
                                        _dbRow.empresa_producto_id = _dbRow_proveedorCodigo.empresa_prodserv_id
                                    else:
                                        pass
                                elif _dbRow_concepto_cfdi.empresa_prodserv_id != _dbRow.empresa_producto_id:
                                    _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                                    _D_return.s_msgError = "Producto de proveedor y producto de movimiento no coinciden a pesar de usar mismo codigo"
                                    _D_return.n_tipoinconsistencia = TEMPRESA_INVMOVTO_PRODUCTOS.Tipo_Inconsistencia.PRODUCTO_ID
                                else:
                                    pass
                                    
                                
                                if not _dbRow_concepto_cfdi.empresa_prodserv_id:
                                    _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                                    _D_return.s_msgError = "Producto de proveedor no mapeado a producto en el sistema"
                                    _D_return.n_tipoinconsistencia = TEMPRESA_INVMOVTO_PRODUCTOS.Tipo_Inconsistencia.PRODUCTO_ID
                                elif _dbRow_concepto_cfdi.valorunitario != _dbRow.preciounitario:
                                    if _dbRow.preciounitario:
                                        _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                                        _D_return.s_msgError = "Costo unitario de movimiento no corresponde a costo en CFDI"
                                        _D_return.n_tipoinconsistencia = TEMPRESA_INVMOVTO_PRODUCTOS.Tipo_Inconsistencia.PRECIO
                                    else:
                                        _dbRow.preciounitario = _dbRow_concepto_cfdi.valorunitario
                                elif _dbRow_concepto_cfdi.cantidad < _dbRow_concepto.registrado:
                                    _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                                    _D_return.s_msgError = "Cantidad de movimiento es mayor que cantidad en CFDI"
                                    _D_return.n_tipoinconsistencia = TEMPRESA_INVMOVTO_PRODUCTOS.Tipo_Inconsistencia.CANTIDAD
                                else:
                                    # Todo parece estar bien
                                    pass
                        else:
                            # Como no existe producto_externo no puede asociarse al CFDI
                            _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                            _D_return.s_msgError = "No se pudo localizar en CFDI"
                            _D_return.n_tipoinconsistencia = TEMPRESA_INVMOVTO_PRODUCTOS.Tipo_Inconsistencia.NO_EN_CFDI
                            pass
                              
            _dbRow.tipo_inconsistencia = _D_return.n_tipoinconsistencia
            _dbRow.update_record()
                            
        return _D_return  
    
    ###TODO: Aún tengo que revisar bien el código.
    @classmethod
    def VALIDAR_ROW_ENTRADA_PORTRASPASO(cls, n_id, n_empresa_id = None, dbRow_maestro = None):
        """ Valida que el contenido del row se encuentre sin inconsistencias con la referencia de la salida para ese almacén.
        
        dbRow_maestro se utiliza para reusar la consulta y no estar haciendo la misma consulta multiples veces
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            n_tipoinconsistencia = TEMPRESA_INVMOVTO_PRODUCTOS.Tipo_Inconsistencia.SIN_INCONSISTENCIA,
            dbRow_maestro = None
            )
        
        _s_empresa_plaza_sucursal_almacen_id = request.vars.master_empresa_plaza_sucursal_almacen_id
        
        _dbRow = dbc01.tempresa_invmovto_productos(n_id)
        if not _dbRow:
            _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
            _D_return.s_msgError = "Registro no encontrado"
        else:
            if not dbRow_maestro:
                dbRow_maestro = dbc01.tempresa_inventariomovimientos(_dbRow.empresa_inventariomovimiento_id)
            else:
                pass
                
            if not dbRow_maestro or (dbRow_maestro.id != _dbRow.empresa_inventariomovimiento_id):
                _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                _D_return.s_msgError = "Movimiento del registro no encontrado o no coincide"
            else:
                _D_return.dbRow_maestro = dbRow_maestro
                
                if not n_empresa_id:
                    _n_empresa_id = dbc01.tempresa_almacenesconceptos(dbRow_maestro.empresa_almacenesconcepto_id).empresa_id
                else:
                    _n_empresa_id = n_empresa_id
                
                _n_concepto_salida_id = TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.GET_CONCEPTO_ID(
                    n_empresa_id = _n_empresa_id, 
                    n_tipoconcepto = TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.SALIDA,
                    n_clasificacion = TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.TRASPASO_SALIDA
                    )
                
                _n_concepto_entrada_id = TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.GET_CONCEPTO_ID(
                    n_empresa_id = _n_empresa_id, 
                    n_tipoconcepto = TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.ENTRADA,
                    n_clasificacion = TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.TRASPASO_ENTRADA
                    )
                
                
                if dbRow_maestro.empresa_almacenesconcepto_id != _n_concepto_entrada_id:
                    _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                    _D_return.s_msgError = "Concepto de movimiento no corresponde a Salida por devolución de compra"
                else:
                    
                    _dbT_movtos = dbc01.tempresa_inventariomovimientos.with_alias('T_movtos') 

                    _dbRows_movtos =  dbc01(
                        (
                            (_dbT_movtos.empresa_almacenesconcepto_id == _n_concepto_salida_id)
                            &(_dbT_movtos.empresa_embarque_id == dbRow_maestro.empresa_embarque_id)
                            &(_dbT_movtos.empresa_embarque_almacen_id_relacionado == dbRow_maestro.empresa_embarque_almacen_id)
                            &(_dbT_movtos.empresa_embarque_almacen_id == dbRow_maestro.empresa_embarque_almacen_id_relacionado)
                            )
                        | (
                            (_dbT_movtos.empresa_almacenesconcepto_id == _n_concepto_entrada_id)
                            &(_dbT_movtos.empresa_embarque_id == dbRow_maestro.empresa_embarque_id)
                            &(_dbT_movtos.empresa_embarque_almacen_id_relacionado == dbRow_maestro.empresa_embarque_almacen_id_relacionado)
                            &(_dbT_movtos.empresa_embarque_almacen_id == dbRow_maestro.empresa_embarque_almacen_id)
                            )
                        )._select(
                            _dbT_movtos.id
                            )
                    
                    _dbT_resumenes = dbc01.tempresa_invmovto_productos.with_alias('T_resumenes')
                    
                    #¿Sirve de algo este campo acá? 
                    _dbFieldCalculado = (
                        (_dbT_resumenes.cantidad_salida.coalesce_zero().sum()) -
                        (_dbT_resumenes.cantidad_entrada.coalesce_zero().sum())
                        )
                    
                    _dbRowsConceptos = dbc01(
                        (_dbT_resumenes.empresa_inventariomovimiento_id.belongs(_dbRows_movtos))
                        &(_dbT_resumenes.empresa_producto_id == _dbRow.empresa_producto_id)
                        ).select(
                            _dbT_resumenes.empresa_producto_id,
                            _dbT_resumenes.cantidad_salida.coalesce_zero().sum().with_alias('cantidad_salidas'),
                            _dbT_resumenes.cantidad_entrada.coalesce_zero().sum().with_alias('cantidad_entradas'),
                            _dbFieldCalculado.with_alias('cantidad_pendiente'),
                            groupby = [
                                _dbT_resumenes.empresa_producto_id,
                                ]
                            )
                    
                    ###TODO: Faltan considerar inconsistencias
                    if len(_dbRowsConceptos) != 1:
                        # Si se encontró más de un registro o ningún registro, no se puede asociar con cfdi
                        _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                        _D_return.s_msgError = "No se pudo localizar el producto en las salidas"
                        ###Crear inconsistencia: Producto no está en la salida.
                        _D_return.n_tipoinconsistencia = TEMPRESA_INVMOVTO_PRODUCTOS.Tipo_Inconsistencia.NO_EN_CFDI
                    else:
                        _dbRow_concepto = _dbRowsConceptos.first()   
                        
                        if _dbRow_concepto.cantidad_salidas != 0:
                            #Si la cantidad de salidas es diferente de 0 entonces hará la comparación normal para saber si la cantidad de entradas es mayor a las de salida     
                            if _dbRow_concepto.cantidad_entradas > _dbRow_concepto.cantidad_salidas:
                                _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                                _D_return.s_msgError = "Cantidad de entradas es mayor a las salidas del almacén ."
                                _D_return.n_tipoinconsistencia = TEMPRESA_INVMOVTO_PRODUCTOS.Tipo_Inconsistencia.CANTIDAD_ENTRADA
                            else:
                                # Todo parece estar bien
                                pass
                        else:
                            #Si la cantidad de salidas es igual a 0 entonces no existe ese producto en una salida por traspaso. 
                            _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                            _D_return.s_msgError = "No se pudo localizar el producto en las salidas."
                            _D_return.n_tipoinconsistencia = TEMPRESA_INVMOVTO_PRODUCTOS.Tipo_Inconsistencia.PRODUCTO_ID

                                  
            _dbRow.tipo_inconsistencia = _D_return.n_tipoinconsistencia
            _dbRow.update_record()
                            
        return _D_return  

    class InventarioInicial:
        """ Clase para manejo de productos de movimiento tipo inventario inicial

        """

        def __init__(
                self,
                s_empresa_id,
                s_empresa_plaza_sucursal_almacen_id,
                s_invmovto_id
                ):
            """ Función de inicialización de clase

            @param s_empresa_id:
            @type s_empresa_id:
            @param s_empresa_plaza_sucursal_almacen_id:
            @type s_empresa_plaza_sucursal_almacen_id:
            """

            self._s_empresa_id = s_empresa_id
            self._s_empresa_plaza_sucursal_almacen_id = s_empresa_plaza_sucursal_almacen_id
            self._s_invmovto_id = s_invmovto_id

            self._dbTable = dbc01.tempresa_invmovto_productos
            self._D_return = Storage(
                E_return = CLASS_e_RETURN.OK,
                s_msgError = "",
                )

            if not self._s_invmovto_id:
                self._D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                self._D_return.s_msgError = "El id del movimiento maestro no esta definido"
            else:
                pass

            return

        def decorador_verifica_condiciones(func):
            """ Decorador que verifica de forma generica que las condiciones se cumplan para la ejecución de la rutina

            @return:
            @rtype:
            """
            def funcion_compuesta(self, *args, **kwargs):
                if self._D_return.E_return > CLASS_e_RETURN.OK_WARNING:
                    return self._D_return
                else:
                    return func(self, *args, **kwargs)

            return funcion_compuesta

        def puede_crear(self):
            """ Determina si puede o no crear un producto de este tipo de movimiento

            Puede crear si el movimiento esta en un estado que permita el agregar productos

            """
            _D_return = Storage(
                E_return = CLASS_e_RETURN.OK,
                s_msgError = "",
                )

            _O_movto = TEMPRESA_INVENTARIOMOVIMIENTOS.InventarioInicial(
                s_empresa_id = self._s_empresa_id,
                s_empresa_plaza_sucursal_almacen_id = self._s_empresa_plaza_sucursal_almacen_id
                )

            _D_results = _O_movto.puede_editar(self._s_invmovto_id)

            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.E_return = _D_results.E_return
                _D_return.s_msgError = "Estatus del movimiento de inventario, no permite agregar productos: %s" % _D_results.s_msgError

            else:
                pass

            return _D_return

        def _puede_editar(
                self,
                x_empresa_invmovto_producto,
                ):
            """ Define si es posible editar el producto del movimiento

            Se puede editar solamente si el movimiento esta en un estado editable

            @param x_empresa_invmovto_producto:
            @type x_empresa_invmovto_producto:
            @return:
            @rtype:
            """
            _D_return = Storage(
                E_return = CLASS_e_RETURN.OK,
                s_msgError = "",
                )

            _O_movto = TEMPRESA_INVENTARIOMOVIMIENTOS.InventarioInicial(
                s_empresa_id = self._s_empresa_id,
                s_empresa_plaza_sucursal_almacen_id = self._s_empresa_plaza_sucursal_almacen_id
                )

            _D_results = _O_movto.puede_editar(self._s_invmovto_id)

            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.E_return = _D_results.E_return
                _D_return.s_msgError = "Estatus del movimiento de inventario, no permite editar productos: %s" % _D_results.s_msgError

            else:
                pass

            return _D_return

        def _puede_eliminar(self, x_empresa_invmovto):
            """ Determina si puede o no eliminar un registro de este tipo de movimiento

            Misma condición que puede_editar

            """
            # _D_return = Storage(
            #     E_return = CLASS_e_RETURN.OK,
            #     s_msgError = "",
            #     )

            return self._puede_editar(x_empresa_invmovto)

        @decorador_verifica_condiciones
        def configuraCampos_nuevo(
                self,
                s_empresa_invmovto_producto_id,
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param s_empresa_invmovto_producto_id: Si lo tiene definido, copia sus valores por default
            @type s_empresa_invmovto_producto_id:
            @return:
            @rtype:
            """
            _D_return = Storage(
                E_return = CLASS_e_RETURN.OK,
                s_msgError = "",
                )

            _D_results = self.puede_crear()

            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.E_return = _D_results.E_return
                _D_return.s_msgError = _D_results.s_msgError

            elif s_empresa_invmovto_producto_id:
                _dbRow_base = self._dbTable(s_empresa_invmovto_producto_id)

                # self._dbTable.empresa_inventariomovimiento_id.default = _dbRow_base.empresa_inventariomovimiento_id
                self._dbTable.empresa_prodserv_id.default = _dbRow_base.empresa_prodserv_id
                # self._dbTable.cfdi_concepto_id.default = _dbRow_base.cfdi_concepto_id
                # self._dbTable.cantidad.default = _dbRow_base.cantidad
                # self._dbTable.cantidad_entrada.default = _dbRow_base.cantidad_entrada
                # self._dbTable.cantidad_salida.default = _dbRow_base.cantidad_salida
                self._dbTable.preciounitario.default = _dbRow_base.preciounitario
                self._dbTable.producto_externo.default = _dbRow_base.producto_externo
                self._dbTable.descripcion.default = _dbRow_base.descripcion
                # self._dbTable.costototal_entrada.default = _dbRow_base.costototal_entrada
                # self._dbTable.costototal_salida.default = _dbRow_base.costototal_salida
                # self._dbTable.tipo_inconsistencia.default = _dbRow_base.tipo_inconsistencia
                # self._dbTable.observaciones.default = _dbRow_base.observaciones

            else:
                pass

            # self._dbTable.empresa_inventariomovimiento_id.writable = False
            self._dbTable.empresa_prodserv_id.writable = True
            # self._dbTable.cfdi_concepto_id.writable = False
            self._dbTable.cantidad.writable = True
            self._dbTable.preciounitario.writable = True
            self._dbTable.producto_externo.writable = True

            # Campos que no deben ser editados, solo de forma autocalculada
            self._dbTable.cantidad_entrada.writable = False
            self._dbTable.cantidad_salida.writable = False
            self._dbTable.costototal_entrada.writable = False
            self._dbTable.costototal_salida.writable = False
            self._dbTable.tipo_inconsistencia.writable = False

            # Campos que todos los movimientos pueden editar
            self._dbTable.descripcion.writable = True
            self._dbTable.observaciones.writable = True

            # Se definen los valores por default
            self._dbTable.empresa_inventariomovimiento_id.default = self._s_invmovto_id

            return _D_return

        @decorador_verifica_condiciones
        def configuraCampos_edicion(
                self,
                x_empresa_invmovto_producto,
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param x_empresa_invmovto_producto: es el registro del movimiento o su id
            @type x_empresa_invmovto_producto:
            @return:
            @rtype:
            """
            _D_return = Storage(
                E_return = CLASS_e_RETURN.OK,
                s_msgError = "",
                )

            _D_results = self._puede_editar(x_empresa_invmovto_producto)

            if _D_results.E_return == CLASS_e_RETURN.OK:
                # Si puede editar regresa un ok y el movimiento esta abierto

                # Se manda vacío el argumento para no agregar defaults a los campos
                _D_results = self.configuraCampos_nuevo(
                    s_empresa_invmovto_producto_id = None,
                    )
                _D_return.E_return = _D_results.E_return
                _D_return.s_msgError = _D_results.s_msgError

            else:
                # De lo contrario en cualquier otro caso, solo se pueden editar las observaciones
                self._dbTable.observaciones.writable = True

            return _D_return

        def al_validar(self, O_form):
            """ Validar la información para poder grabar inventario inicial

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            if self._D_return.E_return > CLASS_e_RETURN.OK_WARNING:
                O_form.errors.id = self._D_return.s_msgError

            else:

                if O_form.vars.empresa_prodserv_id and O_form.vars.preciounitario:
                    _dbRows = dbc01(
                        (self._dbTable.empresa_inventariomovimiento_id == self._s_invmovto_id)
                        & (self._dbTable.empresa_prodserv_id == O_form.vars.empresa_prodserv_id)
                        & (self._dbTable.preciounitario == O_form.vars.preciounitario)
                        & (self._dbTable.id != O_form.record_id)
                        ).select(
                            self._dbTable.ALL
                            )

                    if _dbRows:
                        O_form.errors.id = "El producto con el mismo precio ya esta definido en el movimiento; y no puede ser duplicado"
                    else:
                        pass

                else:
                    pass

                if O_form.record_id:
                    # Se esta modificando el registro, la llamada a configuraCampos_edicion ya definió con writable
                    # que puede modificarse y que no.
                    self._dbTable.cantidad_entrada.writable = True
                    O_form.vars.cantidad_entrada = O_form.vars.cantidad
                    pass

                else:
                    # Se esta creando el registro

                    _D_results = self.puede_crear()
                    if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                        O_form.errors.id = _D_results.s_msgError

                    else:
                        self._dbTable.empresa_inventariomovimiento_id.default = self._s_invmovto_id
                        self._dbTable.cantidad_entrada.default = O_form.vars.cantidad

            return O_form

        def al_aceptar(self, O_form):
            """ Aceptación captura de inventario inicial producto

            Se hacen todos los cálculos y aceptación de los cambios en el producto

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            if self._D_return.E_return > CLASS_e_RETURN.OK_WARNING:
                O_form.errors.id = self._D_return.s_msgError
            else:

                # Se hace uso de compute en campos que se autocalculan
                # _D_results = self.autocalculaCampos(O_form.vars.id)
                # if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                #     stv_flashError(
                #         self._dbTable._singular,
                #         "No se pudo hacer autocalculo de campos. %s" % _D_results.s_msgError,
                #         s_mode = 'stv_flash',
                #         D_addProperties = {}
                #         )
                # else:
                #     pass
                pass

            return O_form

        def antes_eliminar(self, L_errorsAction, dbRow):
            """ Antes de eliminar un movimiento de inventario de este tipo

            @param dbRow:
            @type dbRow:
            @param L_errorsAction:
            @type L_errorsAction:
            @return: errores
            @rtype: lista de diccionarios {'msg':'...'}
            """
            if self._D_return.E_return > CLASS_e_RETURN.OK_WARNING:
                L_errorsAction.append({'msg': self._D_return.s_msgError})
            else:
                _D_results = self._puede_eliminar(dbRow)
                if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                    L_errorsAction.append({'msg': _D_results.s_msgError})
                else:
                    pass

            return L_errorsAction

    @staticmethod
    def IMPORTAR_EXCEL(
            s_pathArchivo,
            s_nombreHoja = None,
            **D_otros
            ):
        """ Se identifica el registro a través de la columna producto; y se importan cantidad,
        precio unitario, producto externo y descripción.
        Si cantidad es cero, se ignora la importación del registro.
        Si el producto se intenta importar dos veces en el mismo archivo, se cancela la importación.
        Si el producto ya se importó anteriormente, se sobreescribe la información.

        @param s_pathArchivo:
        @type s_pathArchivo:
        @param s_nombreHoja:
        @type s_nombreHoja:
        @param D_otros: Contiene s_almacen_id, s_invmovto_id y s_campoCantidad a donde se va a copiar la cantidad definida en excel
        @type D_otros:
        @return:
        @rtype:
        """
        D_otros = Storage(D_otros)

        def alValidarRegistroExcel(
                D_registro,
                dbRow_encontrado,
                dbRows_encontrados,
                b_fueIdentificadoAntes,
                b_hayVariasCoincidenciasEnTabla,
                b_encontradoEnTabla
                ):
            _D_return = Storage(
                E_return = stvfwk2_e_RETURN.OK,
                s_msgError = "",
                E_resultado_validacion = IMPORTAR_EXCEL.E_RESULTADO_VALIDACION.OK
                )
            _s_campo = dbc01.tempresa_invmovto_productos.cantidad.name

            if _s_campo not in D_registro:
                _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                _D_return.s_msgError = "Campo %s no mapeado en registro de excel" % _s_campo

            elif b_fueIdentificadoAntes:
                _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                _D_return.s_msgError = "El producto %s se encuentra repetido en el excel" % str(D_registro.empresa_prodserv_id)
            else:
                if D_registro.cantidad == 0:
                    # Se ignora el regsitro si no tienen cantidad máxima definida
                    _D_return.E_resultado_validacion = IMPORTAR_EXCEL.E_RESULTADO_VALIDACION.IGNORARLO
                else:
                    D_registro[str(D_otros.s_campoCantidad)] = D_registro.cantidad
                    pass

            return _D_return

        def alAceptarRegistroExcel(
                D_registro,
                D_result,
                ):

            # Se inserta el producto si no existe en el almacén, al momento de cerrar el movimiento

            # if (
            #         (D_result.E_return <= stvfwk2_e_RETURN.OK_WARNING)
            #         and (D_result.E_resultado_validacion != IMPORTAR_EXCEL.E_RESULTADO_VALIDACION.IGNORARLO)
            #         ):
            #
            # else:
            #     pass

            return D_result

        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        _D_results = TEMPRESA_INVENTARIOMOVIMIENTOS.PUEDE_EDITAR(D_otros.s_invmovto_id)

        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.E_return = _D_results.E_return
            _D_return.s_msgError = _D_results.s_msgError

        else:
            _dbTabla = dbc01.tempresa_invmovto_productos
            _O_importar = IMPORTAR_EXCEL(
                dbTabla = _dbTabla,
                b_insertar_noRelacionados = True,
                b_ignorar_noRelacionados = False,
                b_insertarTodos = False,
                fnIdentificarHeader = None,
                alValidar = alValidarRegistroExcel,
                alAceptar = alAceptarRegistroExcel
                )
            _O_importar.agregar_campo_columna_relacion(
                n_id_columna = None,
                dbCampo = _dbTabla.empresa_inventariomovimiento_id,
                E_formato = IMPORTAR_EXCEL.E_FORMATO.NUMERICO,
                s_nombreColumna = None,
                n_columna = None,
                x_valorFijo = D_otros.s_invmovto_id,
                fnConversion = None
                )
            _O_importar.agregar_campo_columna_relacion(
                n_id_columna = None,
                dbCampo = _dbTabla.empresa_prodserv_id,
                E_formato = IMPORTAR_EXCEL.E_FORMATO.NUMERICO,
                s_nombreColumna = _dbTabla.empresa_prodserv_id.label,
                n_columna = None,
                x_valorFijo = None,
                fnConversion = None
                )

            _O_importar.agregar_campo_columna_importar(
                n_id_columna = None,
                dbCampo = _dbTabla.cantidad,
                E_formato = IMPORTAR_EXCEL.E_FORMATO.DECIMAL,
                s_nombreColumna = dbc01.tempresa_almacen_productos.cantidad.label,
                n_columna = None,
                x_valorFijo = None,
                fnConversion = None
                )
            _O_importar.agregar_campo_columna_importar(
                n_id_columna = None,
                dbCampo = _dbTabla.preciounitario,
                E_formato = IMPORTAR_EXCEL.E_FORMATO.DECIMAL,
                s_nombreColumna = _dbTabla.preciounitario.label,
                n_columna = None,
                x_valorFijo = None,
                fnConversion = None
                )
            _O_importar.agregar_campo_columna_importar(
                n_id_columna = None,
                dbCampo = _dbTabla.producto_externo,
                E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
                s_nombreColumna = _dbTabla.producto_externo.label,
                n_columna = None,
                x_valorFijo = None,
                fnConversion = None
                )

            _O_importar.agregar_campo_columna_importar(
                n_id_columna = None,
                dbCampo = _dbTabla.descripcion,
                E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
                s_nombreColumna = _dbTabla.descripcion.label,
                n_columna = None,
                x_valorFijo = None,
                fnConversion = None
                )

            _D_result = _O_importar.importar(
                s_pathArchivo = s_pathArchivo,
                s_nombreHoja = s_nombreHoja or 'Sheet1'
                )

            _D_return.E_return = _D_result.E_return
            _D_return.s_msgError = _D_result.s_msgError

        return _D_return

    @staticmethod
    def AL_CERRAR_MOVIMIENTO(
            s_almacen_id,
            s_invmovto_id
            ):

        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTabla = dbc01.tempresa_invmovto_productos

        _dbRows = dbc01(
            _dbTabla.empresa_inventariomovimiento_id == s_invmovto_id
            ).select(
                _dbTabla.ALL,
                dbc01.tempresa_almacen_productos.ALL,
                left = [
                    dbc01.tempresa_almacen_productos.on(
                        (dbc01.tempresa_almacen_productos.empresa_plaza_sucursal_almacen_id == s_almacen_id)
                        & (dbc01.tempresa_almacen_productos.empresa_prodserv_id == _dbTabla.empresa_prodserv_id)
                        )
                    ],
                orderby = _dbTabla.id
                )

        _n_id = 0
        _L_prodserv_ids_operados = []
        for _dbRow in _dbRows:
            _dbRow_invmovto_prod = _dbRow.tempresa_invmovto_productos
            _dbRow_almacen_prod = _dbRow.tempresa_almacen_productos
            if _n_id != _dbRow_invmovto_prod.id:
                _n_id = _dbRow_invmovto_prod.id

                if (
                        (_dbRow_invmovto_prod.empresa_prodserv_id in _L_prodserv_ids_operados)
                        or not _dbRow_almacen_prod.id
                        ):
                    # Si el producto en el almacén ya fue actualizado o si no es encontrado en el almacén,
                    # se obtiene la última información.
                    # Esto pasa por ejemplo, si se entra el mismo producto con diferentes precios

                    _D_result = TEMPRESA_ALMACEN_PRODUCTOS.VERIFICA_INSERTA(
                        s_almacen_id = s_almacen_id,
                        s_prodserv_id = _dbRow_invmovto_prod.empresa_prodserv_id,
                        cantidad = _dbRow_invmovto_prod.cantidad_entrada - _dbRow_invmovto_prod.cantidad_salida
                        )

                    if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
                        D_result.E_return = _D_result.E_return
                        D_result.s_msgError = _D_result.s_msgError
                        break

                    elif _D_result.dbRow:
                        # Si lo encontró, actualiza su cantidad
                        _D_result.dbRow.cantidad += _dbRow_invmovto_prod.cantidad_entrada - _dbRow_invmovto_prod.cantidad_salida
                        _D_result.dbRow.update_record()

                    else:
                        # Entonces se insertó y se configuró la cantidad directamente
                        pass

                else:
                    _dbRow_almacen_prod.cantidad += _dbRow_invmovto_prod.cantidad_entrada - _dbRow_invmovto_prod.cantidad_salida
                    _dbRow_almacen_prod.update_record()

                _L_prodserv_ids_operados.append(_dbRow_invmovto_prod.empresa_prodserv_id)

            else:
                _D_return.E_return = stvfwk2_e_RETURN.OK_WARNING
                _D_return.s_msgError += "Producto %s se encuentra repetido en almacén. " % str(_dbRow_invmovto_prod.empresa_prodserv_id)

        return _D_return

    @staticmethod
    def CONFIGURAR_INTEGRIDAD_REFERENCIAL():

        return

    pass


dbc01.define_table(
    'tempresa_invmovto_productos',
    Field(
        'empresa_inventariomovimiento_id', dbc01.tempresa_inventariomovimientos, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Inventario movimiento', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'empresa_prodserv_id', dbc01.tempresa_prodserv, default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_search(
            f, v,
            s_display = '%(descripcion)s',
            s_url = URL(a = 'app_almacenes', c = '600busquedas', f = 'buscar_producto'),
            D_additionalAttributes = {'reference': dbc01.tempresa_prodserv.id}
            ),
        label = 'Producto', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tempresa_invmovto_productos.empresa_prodserv_id
            )
        ),
        #Se descomentará cuando se cree la tabla maestro para esta relación
#     Field('empresa_producto_externo_id', 'integer', default=None,
#         required=True, requires = IS_NULL_OR(IS_IN_DB(dbc01,'tempresa_prodserv.id', dbc01.tempresa_prodserv._format)),
#         ondelete='NO ACTION', notnull=False, unique=False,
#         widget = lambda f, v: stv_widget_db_search(f, v, 
#             s_display = '%(descripcion)s [%(codigo)s]', 
#             s_url = URL( a = 'app_empresas', c = '120empresas', f = 'empresa_prodserv_buscar'),
#             D_additionalAttributes = {'reference':dbc01.tempresa_prodserv.id}
#             ),
    Field(
        'cfdi_concepto_id', 'integer', default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        # TODO crear plus que regrese los conceptos del CFDI que esten pendientes
        widget = lambda field, value: stv_widget_db_chosen_linked(
            field, value, 1, 'empresa_inventariomovimiento_id',
            URL(a = 'app_almacenes', c = '600busquedas', f = 'cfdi_conceptos_pendientes.json')
            ),
        label = 'CFDI Concepto', comment = None,
        writable = False, readable = True,
        represent = stv_represent_referencefield
        ),
    Field(
        'cantidad', 'decimal(14,4)', default = 0,
        required = True,
        notnull = False, unique = False,
        widget = stv_widget_inputFloat, label = 'Cant.', comment = None,
        writable = False, readable = True,
        represent = stv_represent_float
        ),
    Field(
        'cantidad_entrada', 'decimal(14,4)', default = 0,
        required = True,
        notnull = False, unique = False,
        widget = stv_widget_inputFloat, label = 'Cant. entrada', comment = None,
        writable = False, readable = True,
        represent = stv_represent_float
        ),
    Field(
        'cantidad_salida', 'decimal(14,4)', default = 0,
        required = True,
        notnull = False, unique = False,
        widget = stv_widget_inputFloat, label = 'Cant. salida', comment = None,
        writable = False, readable = True,
        represent = stv_represent_float
        ),
    Field(
        'preciounitario', 'decimal(14,4)', default = 0,
        required = True,
        notnull = False, unique = False,
        widget = stv_widget_inputMoney, label = 'Precio unitario', comment = None,
        writable = False, readable = True,
        represent = stv_represent_money_ifnotzero
        ),
    Field(
        'producto_externo', 'string', length = 150, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Producto externo', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'text', length = 150, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Descripción', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'costototal_entrada', 'decimal(14,4)', default = None,
        required = False,
        notnull = False, unique = False,
        compute = lambda r: r['cantidad_entrada'] * r['preciounitario'],
        widget = stv_widget_inputMoney, label = 'Costo total Ent.', comment = None,
        writable = False, readable = True,
        represent = stv_represent_money_ifnotzero
        ),
    Field(
        'costototal_salida', 'decimal(14,4)', default = None,
        required = False,
        notnull = False, unique = False,
        compute = lambda r: r['cantidad_salida'] * r['preciounitario'],
        widget = stv_widget_inputMoney, label = 'Costo total Sal.', comment = None,
        writable = False, readable = True,
        represent = stv_represent_money_ifnotzero
        ),
    Field(
        'tipo_inconsistencia', 'integer', default = TEMPRESA_INVMOVTO_PRODUCTOS.Tipo_Inconsistencia.OTRA,
        required = False, requires = IS_IN_SET(
            TEMPRESA_INVMOVTO_PRODUCTOS.Tipo_Inconsistencia.GET_DICT(), zero = 0,
            error_message = 'Selecciona'
            ),
        notnull = False, unique = False,
        widget = stv_widget_combobox,
        label = 'Tipo inconsistencia', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_list(
            v, r, None, TEMPRESA_INVMOVTO_PRODUCTOS.Tipo_Inconsistencia.GET_DICT()
            )
        ),
    Field(
        'observaciones', 'text', default = None,
        required = False,
        notnull = False, unique = False,
        widget = lambda v, r: stv_widget_text(v, r, D_additionalAttributes = {'n_rows': 2}), label = 'Observaciones',
        comment = None,
        writable = True, readable = True,
        represent = None
        ),
    tSignature_dbc01,
    format='%(id)s',
    singular='Producto',
    plural='Productos',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )


""" Definición de la tabla de tempresa_invmovto_producto_costeo """
class TEMPRESA_INVMOVTO_PRODUCTO_COSTEO:

    @classmethod
    def GET_CONPOLIZA_PORMOVTO(cls, n_empresa_plaza_sucursal_almacen_id, n_empresa_inventariomovimiento_id):
        _dbRows_prod_con_poliza = dbc01(
            (dbc01.tempresa_invmovto_producto_costeo.empresa_plaza_sucursal_almacen_id == n_empresa_plaza_sucursal_almacen_id)
            & (dbc01.tempresa_invmovto_producto_costeo.empresa_inventariomovimiento_id == n_empresa_inventariomovimiento_id)
            & (dbc01.tempresa_invmovto_producto_costeo.estatus.belongs(
                    TEMPRESA_INVMOVTO_PRODUCTO_COSTEO.ESTATUS.CON_POLIZA, 
                    TEMPRESA_INVMOVTO_PRODUCTO_COSTEO.ESTATUS.CERRADO
                    )
                )
            ).select(
                dbc01.tempresa_invmovto_producto_costeo.id,
                dbc01.tempresa_invmovto_producto_costeo.empresa_producto_id,
                orderby = dbc01.tempresa_invmovto_producto_costeo.empresa_producto_id,
                distinct = True
                )
        return _dbRows_prod_con_poliza
    
    @classmethod
    def GET_CONPOLIZAPOSTERIOR_PORMOVTO(cls, n_empresa_plaza_sucursal_almacen_id, n_empresa_inventariomovimiento_id, dt_fecha_movimiento):
        """ De la lista de productos detalle en el movimiento, regresa los que tiene poliza con fecha posterior
        
        """
        
        dbt_movtoproductos_posteriores = dbc01.tempresa_invmovto_producto_costeo.with_alias('tmovtoproductosposteriores')
        dbt_movtoproductos = dbc01.tempresa_invmovto_producto_costeo.with_alias('tmovtoproductos')
        
        _dbRows_prod_con_poliza = dbc01(
            (dbt_movtoproductos.empresa_plaza_sucursal_almacen_id == n_empresa_plaza_sucursal_almacen_id)
            & (dbt_movtoproductos.empresa_inventariomovimiento_id == n_empresa_inventariomovimiento_id)
            & (dbt_movtoproductos_posteriores.empresa_producto_id == dbt_movtoproductos.empresa_producto_id)
            & (dbt_movtoproductos_posteriores.empresa_plaza_sucursal_almacen_id == dbt_movtoproductos.empresa_plaza_sucursal_almacen_id)
            & (dbt_movtoproductos_posteriores.estatus.belongs(
                    TEMPRESA_INVMOVTO_PRODUCTO_COSTEO.ESTATUS.CON_POLIZA, 
                    TEMPRESA_INVMOVTO_PRODUCTO_COSTEO.ESTATUS.CERRADO
                    )
                )
            & (dbt_movtoproductos_posteriores.fecha_movimiento >= dt_fecha_movimiento)
            ).select(
                dbt_movtoproductos.id,
                dbt_movtoproductos.empresa_producto_id,
                orderby = dbt_movtoproductos.empresa_producto_id,
                distinct = True
                )
        return _dbRows_prod_con_poliza
    
    @classmethod
    def GET_CONPOLIZA_PORREMISION(cls, n_empresa_plaza_sucursal_almacen_id, n_remision, n_empresa_proveedor_id):
        _dbRows_prod_remision_con_poliza = dbc01(
            (dbc01.tempresa_inventariomovimientos.empresa_proveedor_id == n_empresa_proveedor_id)
            &(dbc01.tempresa_inventariomovimientos.empresa_plaza_sucursal_almacen_id == n_empresa_plaza_sucursal_almacen_id)
            &(dbc01.tempresa_inventariomovimientos.remision == n_remision)
            ).select(
                dbc01.tempresa_invmovto_producto_costeo.id,
                dbc01.tempresa_invmovto_producto_costeo.empresa_producto_id,
                left = [
                    dbc01.tempresa_invmovto_producto_costeo.on(
                        (dbc01.tempresa_invmovto_producto_costeo.empresa_inventariomovimiento_id == dbc01.tempresa_inventariomovimientos.id)
                        &(dbc01.tempresa_invmovto_producto_costeo.estatus.belongs(
                            TEMPRESA_INVMOVTO_PRODUCTO_COSTEO.ESTATUS.CON_POLIZA, 
                            TEMPRESA_INVMOVTO_PRODUCTO_COSTEO.ESTATUS.CERRADO
                            )
                        )
                    ),
                ],
                orderby = dbc01.tempresa_invmovto_producto_costeo.empresa_producto_id,
                distinct = True
                )
        return _dbRows_prod_remision_con_poliza
    
    @classmethod
    def GET_FECHA_INVENTARIOINICIAL(cls, n_empresa_id, n_empresa_plaza_sucursal_almacen_id):
        """ De la lista de productos detalle en el movimiento, regresa los que tiene poliza con fecha posterior
        
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            dt_fecha_movimiento = None
            )        
        
        _dbTable = dbc01.tempresa_inventariomovimientos
        
        _D_results = TEMPRESA_ALMACENESCONCEPTOS.GET_ID_CACHE(
            n_empresa_id = n_empresa_id, 
            n_tipoconcepto = TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.ENTRADA, 
            n_clasificacion = TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.INVENTARIO_INICIAL
            )
            
        if (_D_results.E_return != CLASS_e_RETURN.OK) or not _D_results.n_concepto_id:
            #Si entra acá es que ocurrió un error durante la obtención del concepto_id
            _D_return.E_return = _D_results.E_return
            _D_return.s_msgError = _D_results.s_msgError
        else:
            _n_concepto_inventarioInicial_id = _D_results.n_concepto_id
            _dbRows_inventarioInicial = dbc01(
                (_dbTable.empresa_almacenesconcepto_id == _n_concepto_inventarioInicial_id)
                &(_dbTable.empresa_plaza_sucursal_almacen_id == n_empresa_plaza_sucursal_almacen_id)     
                ).select(
                    _dbTable.fecha_movimiento,
                    orderby = [
                        _dbTable.fecha_movimiento
                        ]
                    )
            if _dbRows_inventarioInicial:
                _D_return.dt_fecha_movimiento = _dbRows_inventarioInicial.last().fecha_movimiento
            else:
                # Se queda la fecha_movimiento como null
                pass

        return _D_return    


    @classmethod
    def GET_COSTOPRODUCTO(cls, n_producto_id, n_almacen_id, T_estatus, b_filtrarMovtosCompletados):
        
        """Se consigue el costo del producto.
        
            n_producto_id: ID del producto.
            n_almacen_id: ID del almacén.
            T_estatus: Estatus del movimiento. (Activo, con póliza o cerrado)
        
        """
        
        _D_return = Storage(
            n_movto_capa_id = 0,
            n_costo = 0,
            n_tipocosteo = 0,
            L_error = [],
            )
        
        _dbRows_metodocosteo = dbc01(
            (dbc01.tempresa_plaza_sucursal_almacenes.id == n_almacen_id)
            ).select(
                dbc01.tempresa_plaza_sucursal_almacenes.metodocosteo
                ).last()
                
        if _dbRows_metodocosteo:
            
            if _dbRows_metodocosteo.metodocosteo == TEMPRESA_PLAZA_SUCURSAL_ALMACENES.E_METODOCOSTEO.PROMEDIO:
                
                _dbRows_promedio = dbc01(
                    (dbc01.tempresa_invmovto_producto_costeo.empresa_producto_id == n_producto_id)
                    &(dbc01.tempresa_invmovto_producto_costeo.empresa_plaza_sucursal_almacen_id == n_almacen_id)
                    &(dbc01.tempresa_invmovto_producto_costeo.estatus.belongs (T_estatus))
                    ).select(
                        (
                            (
                                (dbc01.tempresa_invmovto_producto_costeo.entrada_cantidad -
                                dbc01.tempresa_invmovto_producto_costeo.salida_cantidad)).coalesce_zero().sum().with_alias('cantidad')
                            ),
                        (
                            (
                                (dbc01.tempresa_invmovto_producto_costeo.entrada_cantidad *
                                dbc01.tempresa_invmovto_producto_costeo.entrada_costounitario)).coalesce_zero().sum().with_alias('costo_entradas')
                            ),
                        (
                            (
                                (dbc01.tempresa_invmovto_producto_costeo.salida_cantidad *
                                dbc01.tempresa_invmovto_producto_costeo.salida_costounitario)).coalesce_zero().sum().with_alias('costo_salidas')
                            ),
                        dbc01.tempresa_invmovto_producto_costeo.fecha_movimiento.max().with_alias('fecha_aplicacion'),
                        groupby= [
                            dbc01.tempresa_invmovto_producto_costeo.empresa_producto_id,
                            ],
                        )
                if len(_dbRows_promedio) == 0:
                    _D_return.L_error += ["Hubo un error en la consulta"]
                else:
                    #Regresar los datos pedidos en el return
                    pass
                
            elif _dbRows_metodocosteo.metodocosteo in (TEMPRESA_PLAZA_SUCURSAL_ALMACENES.E_METODOCOSTEO.PEPS, TEMPRESA_PLAZA_SUCURSAL_ALMACENES.E_METODOCOSTEO.UEPS):
                
                _dbTableMovtoCapa = dbc01.tempresa_invmovto_producto_costeo.with_alias('_T_movto_capa') 
                
                _dbField_cantidad = ((dbc01.tempresa_invmovto_producto_costeo.entrada_cantidad -
                                dbc01.tempresa_invmovto_producto_costeo.salida_cantidad)).coalesce_zero().sum().with_alias('cantidad')
                                
                if _dbRows_metodocosteo.metodocosteo == TEMPRESA_PLAZA_SUCURSAL_ALMACENES.E_METODOCOSTEO.PEPS:
                    _orderby = dbc01.tempresa_invmovto_producto_costeo.fecha_movimiento
                    
                else:
                    _orderby = ~dbc01.tempresa_invmovto_producto_costeo.fecha_movimiento
                                
                _dbRows_peps = dbc01(
                    (dbc01.tempresa_invmovto_producto_costeo.empresa_producto_id == n_producto_id)
                    &(dbc01.tempresa_invmovto_producto_costeo.empresa_plaza_sucursal_almacen_id == n_almacen_id)
                    &(dbc01.tempresa_invmovto_producto_costeo.estatus.belongs (T_estatus))
                    ).select(
                        _dbField_cantidad,
                        (
                            (
                                (dbc01.tempresa_invmovto_producto_costeo.entrada_cantidad *
                                dbc01.tempresa_invmovto_producto_costeo.entrada_costounitario)).coalesce_zero().sum().with_alias('costo_entradas')
                            ),
                        (
                            (
                                (dbc01.tempresa_invmovto_producto_costeo.salida_cantidad *
                                dbc01.tempresa_invmovto_producto_costeo.salida_costounitario)).coalesce_zero().sum().with_alias('costo_salidas')
                            ),    
                        dbc01.tempresa_invmovto_producto_costeo.fecha_movimiento.min().with_alias('fecha_aplicacion'),
                       orderby= [
                            _orderby
                            ],
                        groupby= [
                            dbc01.tempresa_invmovto_producto_costeo.movto_capa_id,
                            ],
                        left = [
                            _dbTableMovtoCapa.on(
                                #Preguntar a Jaime de donde se saca el _T_movto_padre.id
                                _dbTableMovtoCapa.movto_capa_id  == _T_movto_padre.id
                                ),
                            ],
                        having= (_dbField_cantidad < 0)
                            
                        )
                    
                if len(_dbRows_peps) == 0:
                    _D_return.L_error += ["Hubo un error en la consulta"]
                else:
                    pass

            else:
                #Método de costeo no contemplado
                pass
        else:
            #No se encontró método de costeo para este almacén
            pass
        return _D_return
    
    E_METODOCOSTEO = TEMPRESA_PLAZA_SUCURSAL_ALMACENES.E_METODOCOSTEO
            
    class ESTATUS:
        """ Se definen las opciónes del campo """
        ACTIVO = 1
        CON_POLIZA = 2
        CERRADO = 3
        
        @classmethod
        def get_dict(cls):
            return {
                cls.ACTIVO: 'Activo',
                cls.CON_POLIZA: 'Con poliza',
                cls.CERRADO: 'Cerrado',
                }  
            

            
            
    @classmethod
    def INSERTAR(cls, D_insert):
        """Método que inserta los movimientos de entrada o salida. 
        
            Args:
                D_insert: 
                    empresa_inventariomovimiento_id: ID de inventariomovimiento.
                    empresa_producto_id: Producto ID.
                    producto_externo: Clasificación del concepto a buscar. 
                    cantidad: Cantidad de entrada/salida.
                    preciounitario: Costo unitario del producto. 
                    descripcion: Descripción del producto.
                    empresa_plaza_sucursal_almacen_id: Almacén ID.
                    fecha_registro: Fechas de registro.
                    fecha_movimiento: Fecha de movimiento.
                    producto_externo: Código del producto externo. 
                    descripcion: Descripción del movto.
                    n_tipomovimiento: Tipo de movimiento (Entrada o salida).

        """
        
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,  # Siempre regresa OK
            L_msgError = [], #Lista de errores ya que en esta función puede regresar más de 1 error. 
            )
        
        if D_insert.n_tipomovimiento == TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.ENTRADA:
            
            _D_movto_insert = Storage(
                empresa_inventariomovimiento_id = D_insert.empresa_inventariomovimiento_id,
                empresa_plaza_sucursal_almacen_id = D_insert.empresa_plaza_sucursal_almacen_id,
                empresa_producto_id = D_insert.empresa_producto_id,
                entrada_costounitario = D_insert.preciounitario,
                entrada_cantidad = D_insert.cantidad,
                fecha_movimiento = D_insert.fecha_movimiento,
                fecha_registro = D_insert.fecha_registro,
                descripcion = D_insert.descripcion,
                producto_externo = D_insert.producto_externo
                )
            
        elif D_insert.n_tipomovimiento == TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.SALIDA:
            
            _D_movto_insert = Storage(
                empresa_inventariomovimiento_id = D_insert.empresa_inventariomovimiento_id,
                empresa_plaza_sucursal_almacen_id = D_insert.empresa_plaza_sucursal_almacen_id,
                empresa_producto_id = D_insert.empresa_producto_id,
                salida_costounitario = D_insert.preciounitario,
                salida_cantidad = D_insert.cantidad,
                fecha_movimiento = D_insert.fecha_movimiento,
                fecha_registro = D_insert.fecha_registro,
                descripcion = D_insert.descripcion,
                producto_externo = D_insert.producto_externo
                )
            
        else:
            # n_entrada_salida no es movimiento ni de entrada ni de salida
            _D_return.L_msgError += ("Tipo de movimiento desconocido: %s" % D_insert.n_tipomovimiento)
            _D_return.E_return = CLASS_e_RETURN_NOK_ERROR
            pass
    
        # Se inserta a la tabla.
        try:
            
            dbc01.tempresa_invmovto_producto_costeo.insert(**_D_movto_insert)
                    
        except:
            _D_return.L_msgError += ("No se pudo agregar el movimiento del producto: %s" % D_insert.empresa_producto_id)
            _D_return.E_return = CLASS_e_RETURN_NOK_ERROR
            
        return _D_return
    
    ###TODO: En progreso
    @classmethod
    def MODIFICAR(
            cls, 
            s_almacen_id,
            s_producto_id,
            d_precio_unitario,
            n_cantidad,
            d_fecha_registro,
            d_fecha_movto,
            n_entrada_salida,
            s_descripcion,
            s_producto_externo,
            s_id
            ):
        """Método que inserta los movimientos de entrada o salida. 
        
            Args:
                s_empresa_inventariomovimiento_id: Inventario movimiento maestro.
                s_almacen_id: Almacén ID.
                s_producto_id: Empresa producto id.
                d_precio_unitario: Precio unitario.
                n_cantidad: Cantidad de entrada o salida. 
                d_fecha_registro: Fechas de registro.
                d_fecha_movto: Fecha de movimiento.
                n_entrada_salida: Referencia para saber si se trata de entrada o salida. 
                s_descripcion: Descripción del movto.
        
        """
        
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,  # Siempre regresa OK
            s_msgError = "",
            n_concepto_id = None
            )
        
        if n_entrada_salida == TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.ENTRADA:
            _D_movto_insert = Storage(
                empresa_plaza_sucursal_almacen_id = s_almacen_id,
                empresa_producto_id = s_producto_id,
                entrada_costounitario = d_precio_unitario,
                entrada_cantidad = n_cantidad,
                fecha_movimiento = d_fecha_movto,
                fecha_registro = d_fecha_registro,
                descripcion = s_descripcion,
                producto_externo = s_producto_externo
                )
        elif n_entrada_salida == TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.SALIDA:
            _D_movto_insert = Storage(
                empresa_plaza_sucursal_almacen_id = s_almacen_id,
                empresa_producto_id = s_producto_id,
                salida_costounitario = d_precio_unitario,
                salida_cantidad = n_cantidad,
                fecha_movimiento = d_fecha_movto,
                fecha_registro = d_fecha_registro,
                descripcion = s_descripcion,
                producto_externo = s_producto_externo
    
                )
        else:
            #Tipo de movimiento no identificado.
            pass
    

    
        #Se actualiza en la tabla.
        try:
            
            dbc01(
                dbc01.tempresa_invmovto_producto_costeo.id == s_id
                ).update(
                    **_D_movto_insert
                    )   
        except:
            _D_return.L_msgError = ("No se pudo agregar el movimiento del producto: %s", n_empresa_producto_id)
        
        return _D_return
    
    pass
dbc01.define_table(
    'tempresa_invmovto_producto_costeo',
    Field('empresa_inventariomovimiento_id', dbc01.tempresa_inventariomovimientos, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Inventario movimiento', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('empresa_inventariomovimiento_resumen_id', dbc01.tempresa_invmovto_productos, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Inventario movimiento resumen', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('empresa_producto_id', dbc01.tempresa_prodserv, default=None,
        required=False,
        ondelete='NO ACTION', notnull=False, unique=False,
        widget = lambda f, v: stv_widget_db_search(f, v, 
            s_display = '%(descripcion)s', 
            s_url = URL( a = 'app_almacenes', c = '600busquedas', f = 'buscar_producto'),
            D_additionalAttributes = {'reference':dbc01.tempresa_prodserv.id}
            ),
        label= 'Producto', comment=None,
        writable=True, readable=True,
        represent=lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_invmovto_producto_costeo.empresa_producto_id)),
    Field('producto_externo', 'string', length = 150, default = None,
      required = False,
      notnull = False, unique = False,
      widget = stv_widget_input, label = 'Producto externo', comment = '',
      writable = True, readable = True,
      represent = stv_represent_string),
    Field(
        'fecha_movimiento', type = FIELD_UTC_DATETIME, default = request.browsernow,
        required = False, requires = IS_NULL_OR(IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME)),
        notnull = False,
        widget = stv_widget_inputDateTime, label = 'Fecha movimiento', comment = None,
        writable = False, readable = True,
        represent = stv_represent_datetime
        ),
    Field(
        'fecha_registro', type = FIELD_UTC_DATETIME, default = request.browsernow,
        required = False, requires = IS_NULL_OR(IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME)),
        notnull = False,
        widget = stv_widget_inputDateTime, label = 'Fecha de registro', comment = None,
        writable = False, readable = True,
        represent = stv_represent_datetime
        ),
#     Field('cantidad', 'integer', default=1,
#       required=True,
#       notnull=False, unique=False,
#       widget=stv_widget_input, label='Cantidad', comment=None,
#       writable=True, readable=True,
#       represent=stv_represent_number),
    Field('empresa_plaza_sucursal_almacen_id', dbc01.tempresa_plaza_sucursal_almacenes, default=1,
      required=True,
      ondelete='CASCADE', notnull=False, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label= 'Almacén', comment=None,
      writable=True, readable=True,
      represent = stv_represent_referencefield),
#     Field('preciounitario', 'decimal(14,4)', default=0,
#       required=True,
#       notnull=False, unique=False,
#       widget=stv_widget_inputMoney, label='Precio unitario', comment=None,
#       writable=True, readable=True,
#       represent=stv_represent_money_ifnotzero),
#     Field('importe', 'decimal(14,4)', default=0,
#       required=False,
#       notnull=False, unique=False,
#       widget=stv_widget_inputMoney, label='Importe', comment=None,
#       writable=True, readable=True,
#       represent=stv_represent_money_ifnotzero),
#     Field('descuento', 'decimal(14,4)', default=0,
#       required=False,
#       notnull=False, unique=False,
#       widget=stv_widget_inputMoney, label='Descuento', comment=None,
#       writable=True, readable=True,
#       represent=stv_represent_money_ifnotzero),
#     Field('totalneto', 'decimal(14,4)', default=0,
#       required=True,
#       notnull=False, unique=False,
#       widget=stv_widget_inputMoney, label='Total neto', comment=None,
#       writable=True, readable=True,
#       represent=stv_represent_money_ifnotzero),
    Field('movto_capa_id', 'integer', default=0,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Movimiento capa ID', comment=None,
      writable=True, readable=True,
      represent=stv_represent_number),
    Field('metodocosteo', 'integer', default = 3,
      required = False, requires = IS_IN_SET(TEMPRESA_INVMOVTO_PRODUCTO_COSTEO.E_METODOCOSTEO.get_dict(), zero = 0, error_message = 'Selecciona' ),
      notnull = False, unique = False,
      widget = stv_widget_combobox, 
      label = 'Método de costeo', comment = 'Método de costeo que se utilizará en este almacen',
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_INVMOVTO_PRODUCTO_COSTEO.E_METODOCOSTEO.get_dict())),   
    Field('entrada_cantidad', 'decimal(14,4)', default=0,
      required=False, 
      notnull=False, unique=False,
      widget=stv_widget_inputFloat, label='Cantidad de entrada', comment=None,
      writable=True, readable=True,  # Se usa readable cuando se muestra en pantalla
      represent=stv_represent_float),
    Field('entrada_costounitario', 'decimal(14,4)', default=0,
      required=False, 
      notnull=False, unique=False,
      widget=stv_widget_inputFloat, label='Costo unitario', comment=None,
      writable=True, readable=True,
      represent=stv_represent_float),
    Field('salida_cantidad', 'decimal(14,4)', default=0,
      required=False, 
      notnull=False, unique=False,
      widget=stv_widget_inputFloat, label='Cantidad de salida', comment=None,
      writable=True, readable=True,
      represent=stv_represent_float),
    Field('salida_costounitario', 'decimal(14,4)', default=0,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_inputFloat, label='Salida costo unitario', comment=None,
      writable=True, readable=True,
      represent=stv_represent_float),
    Field('estatus', 'integer', default = 1,
      required = False, requires = IS_IN_SET(TEMPRESA_INVMOVTO_PRODUCTO_COSTEO.ESTATUS.get_dict(), zero = 0, error_message = 'Selecciona' ),
      notnull = False, unique = False,
      widget = stv_widget_combobox, 
      label = 'Estatus', comment = None,
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_INVMOVTO_PRODUCTO_COSTEO.ESTATUS.get_dict())),
    Field('polizagenerada', 'boolean', default=request.vars.get("polizagenerada",0),
      required = False,
      notnull = False,
      widget=stv_widget_inputCheckbox, label= 'Póliza generada', comment = "Seleccionar si la póliza ya ha sido generada.",
      writable = True, readable = True,
      represent = stv_represent_boolean),
    Field('descripcion', 'text', length = 150, default = None,
      required = False,
      notnull = False, unique = False,
      widget = stv_widget_text, label = 'Descripcion', comment = None,
      writable = True, readable = True,
      represent = stv_represent_string),
    tSignature_dbc01,
    format='%(id)s',
    singular='Producto',
    plural='Productos',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

class TEMPRESA_PLAZA_SUCURSAL_ALMACEN_EJERCICIOS:
    pass
dbc01.define_table(
    'tempresa_plaza_sucursal_almacen_ejercicios',
    Field('empresa_plaza_sucursal_almacen_id', dbc01.tempresa_plaza_sucursal_almacenes, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Almacén', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('ejercicio', 'integer', default = None,
      required = True, requires = IS_IN_SET(VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id), zero = 0, error_message = 'Selecciona'),
      notnull = False, unique = False,
      widget = stv_widget_combobox, 
      label = 'Ejercicio inicial', comment = None,
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id))),
    Field('cuentacontable', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cuenta contable', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    tSignature_dbc01,
    format='%(ejercicio)s',
    singular='Guía almacén',
    plural='Guías almacén',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

class TEMPRESA_PLAZA_SUCURSAL_ALMACEN_EJERCICIO_GUIASCONTABLES:
    pass
dbc01.define_table(
    'tempresa_plaza_sucursal_almacen_ejercicio_guiascontables',
    Field('empresa_plaza_sucursal_almacen_ejercicio_id', dbc01.tempresa_plaza_sucursal_almacen_ejercicios, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Ejercicio', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('empresa_ingresoestructura_id', dbc01.tempresa_ingresosestructura, default=None,
      required=False,
      ondelete='NO ACTION', notnull=False, unique=False,
      widget=lambda f, v: stv_widget_db_chosen_tree(f, v, 1, D_additionalAttributes={
                                                    'dbTable_tree': dbc01.tempresa_ingresosestructura,
                                                    'dbField_order': dbc01.tempresa_ingresosestructura.orden,
                                                    'dbField_tree_groupby': dbc01.tempresa_ingresosestructura.padre_id,
                                                    'class_dbTree': DB_Tree,
                                                    'b_onlyLastNodes' : True,
                                                    }), 
      label= 'Estructura de ingreso', comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield_tree(v, r, dbc01.tempresa_ingresosestructura.padre_id, dbc01.tempresa_plaza_sucursal_almacen_ejercicio_guiascontables.empresa_ingresoestructura_id)),  
    Field('cuentacontable', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cuenta contable', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    tSignature_dbc01,
    format='%(empresa_ingresoestructura_id)s',
    singular='Guia Contable',
    plural='Guias Contables',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

class TEMPRESA_ALMACEN_LOG:

    class TIPOTRANSACCION:
        NO_IDENTIFICADA = 0
        CONSULTA = 1
        MOVIMIENTO = 2
        CALCULODESALDO = 3
        ELIMINAR = 4
        INSERTAR = 5
        ACTUALIZAR = 6

        @classmethod
        def GET_DICT(cls):
            return {
                cls.NO_IDENTIFICADA: 'No identificada',
                cls.CONSULTA: 'Consulta',
                cls.MOVIMIENTO: 'Movimiento',
                cls.CALCULODESALDO: 'Cálculo de Saldo',
                cls.ELIMINAR: 'Eliminar',
                cls.INSERTAR: 'Insertar',
                cls.ACTUALIZAR: 'Actualizar',
                }

    pass
dbc01.define_table(
    'tempresa_almacen_log',
    Field(
        'emp_pla_suc_almacen_id', dbc01.tempresa_plaza_sucursal_almacenes, default=None,
        required=True,
        ondelete='CASCADE', notnull=True, unique=False,
        label='Almacen', comment=None,
        writable=False, readable=False,
        represent=None
        ),
    Field(
        'fecha', type = FIELD_UTC_DATETIME, default = request.browsernow,
        required = False, requires = IS_NULL_OR(IS_DATETIME(format= STV_FWK_APP.FORMAT.s_DATETIME)),
        notnull = False,
        widget = stv_widget_inputDateTime, label = 'Fecha', comment = '',
        writable = False, readable = True,
        represent = stv_represent_datetime
        ),
    Field(
        'descripcion', 'string', length=140, default=None,
        required=False,
        notnull=False, unique=False,
        widget=stv_widget_input, label= 'Descripcion', comment=None,
        writable=True, readable=True,
        represent=stv_represent_string
        ),
    Field(
        'tipotransaccion', 'integer', default = TEMPRESA_ALMACEN_LOG.TIPOTRANSACCION.NO_IDENTIFICADA,
        required = False, requires=IS_IN_SET(TEMPRESA_ALMACEN_LOG.TIPOTRANSACCION.GET_DICT(), zero=0, error_message= 'Selecciona'),
        notnull = False, unique = False,
        widget = stv_widget_combobox, label = 'Tipo transacción', comment = '',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_ALMACEN_LOG.TIPOTRANSACCION.GET_DICT())
        ),
    Field(
        'errores', 'text', default=None,
        required = False,
        notnull = False,
        label= 'Errores', comment = "",
        writable = False, readable = True,
        represent = stv_represent_text
        ),
    Field(
        'warnings', 'text', default=None,
        required = False,
        notnull = False,
        label= 'Advertencias', comment = "",
        writable = False, readable = True,
        represent = stv_represent_text
        ),
    Field(
        'info', 'text', default=None,
        required = False,
        notnull = False,
        label= 'Informativo', comment = "",
        writable = False, readable = True,
        represent = stv_represent_text
        ),
    Field(
        'resultado', 'integer', default = CLASS_e_RETURN.OK,
        required = False, requires=IS_IN_SET(CLASS_e_RETURN.GET_DICT(), zero=0, error_message= 'Selecciona'),
        notnull = False, unique = False,
        widget = stv_widget_combobox, label = 'Resultado', comment = '',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, CLASS_e_RETURN.GET_DICT())
        ),
    Field(
        'comentarios', 'text', default=None,
        required = False,
        notnull = False,
        label= 'Comentarios', comment = "",
        writable = True, readable = True,
        represent = stv_represent_text
        ),
    tSignature_dbc01,
    format='%(fecha)s %(tipotransaccion)s',
    singular='Log',
    plural='Logs',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_ALMACEN_ORDENESCOMPRA:

    class ESTATUS:
        """ Se definen las opciónes del campo """
        NO_DEFINIDO = 0
        GENERANDO = 1
        PENDIENDE_APROBACION = 2
        RECHAZADA = 3
        CONCILIANDO = 4
        ESPERANDO_SURTIR = 5
        CERRADA = 6

        # En caso de embarques el movimiento se mueve como CARGANDO > RUTA > ENTREGADO
        # En caso de movimiento en almacenes se mueve como EN_PROCESO > CERRADO

        @classmethod
        def GET_DICT(cls):
            return {
                cls.NO_DEFINIDO         : 'Por definir',
                cls.GENERANDO           : 'Generando',
                cls.PENDIENDE_APROBACION: 'Pendiente Aprobación',
                cls.RECHAZADA           : 'Rechazada',
                cls.CONCILIANDO         : 'Conciliando',
                cls.ESPERANDO_SURTIR    : 'Esperando Surtir',
                cls.CERRADA             : 'Cerrada',
                }

        @classmethod
        def GET_L_ABIERTO(cls):
            """ Regresa una lista de elementos que identifican al movimiento como abierto, permitiendo hacer cambios

            @return:
            @rtype:
            """
            return [
                cls.NO_DEFINIDO,
                cls.GENERANDO,
                cls.RECHAZADA,
                ]

    @classmethod
    def CONFIGURA_CAMPOS_EDICION(
            cls,
            x_empresa_ordencompra,
            ):
        """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

        @param x_empresa_ordencompra:
        @type x_empresa_ordencompra:
        @return:
        @rtype:
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTable = dbc01.tempresa_almacen_ordenescompra

        if isinstance(x_empresa_ordencompra, (str, int, long)):
            _n_ordenCompra_id = x_empresa_ordencompra
            _dbRow_ordenCompra = _dbTable(_n_ordenCompra_id)
        else:
            _dbRow_ordenCompra = x_empresa_ordencompra
            _n_ordenCompra_id = _dbRow_ordenCompra.id

        if _dbRow_ordenCompra.estatus in TEMPRESA_ALMACEN_ORDENESCOMPRA.ESTATUS.GET_L_ABIERTO():

            _D_results = TEMPRESA_ALMACEN_ORDENESCOMPRA.CONFIGURA_CAMPOS_NUEVO(
                s_almacen_ordenescompra_id = None,
                )
            _D_return.E_return = _D_results.E_return
            _D_return.s_msgError = _D_results.s_msgError

        else:
            _dbTable.observaciones.writable = True

        return _D_return

    @classmethod
    def CONFIGURA_CAMPOS_NUEVO(
            cls,
            s_almacen_ordenescompra_id,
            ):
        """ Se configuran los campos en caso de un registro nuevo

        @param s_almacen_ordenescompra_id: Si lo tiene definido, copia sus valores por defaulr
        @type s_almacen_ordenescompra_id:
        @return:
        @rtype:
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            b_tieneMovimientosCosteo = False
            )

        _dbTable = dbc01.tempresa_almacen_ordenescompra

        if s_almacen_ordenescompra_id:
            _dbRow_ordenCompra_base = _dbTable(s_almacen_ordenescompra_id)

            _dbTable.empresa_plaza_sucursal_almacen_id.default = _dbRow_ordenCompra_base.empresa_plaza_sucursal_almacen_id
            # _dbTable.total.default = _dbRow_ordenCompra_base.total
            # _dbTable.observaciones.default = _dbRow_ordenCompra_base.observaciones
            # _dbTable.referencia.default = _dbRow_ordenCompra_base.referencia
            # _dbTable.fecha.default = _dbRow_ordenCompra_base.fecha
            # _dbTable.fecha_requerida.default = _dbRow_ordenCompra_base.fecha_requerida
            _dbTable.descripcion.default = _dbRow_ordenCompra_base.descripcion
            # _dbTable.inconsistencias.default = _dbRow_ordenCompra_base.inconsistencias
            # _dbTable.estatus.default = _dbRow_ordenCompra_base.estatus
            # _dbTable.fecha_aprobacion.default = _dbRow_ordenCompra_base.fecha_aprobacion
            # _dbTable.usuario_aprobador_id.default = _dbRow_ordenCompra_base.usuario_aprobador_id

        else:
            pass

        _dbTable.empresa_plaza_sucursal_almacen_id.writable = False
        _dbTable.total.writable = False
        _dbTable.observaciones.writable = True
        _dbTable.referencia.writable = True
        _dbTable.fecha.writable = False
        _dbTable.fecha_requerida.writable = True
        _dbTable.descripcion.writable = True
        _dbTable.inconsistencias.writable = False
        _dbTable.estatus.writable = False
        _dbTable.fecha_aprobacion.writable = False
        _dbTable.usuario_aprobador_id.writable = False

        return _D_return

    pass

dbc01.define_table(
    'tempresa_almacen_ordenescompra',
    Field(
        'empresa_plaza_sucursal_almacen_id', dbc01.tempresa_plaza_sucursal_almacenes, default = None,
        required = True,
        ondelete = 'NO ACTION', notnull = True, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Almacén', comment = None,
        writable = False, readable = True,
        represent = stv_represent_referencefield
        ),
    Field(
        'total', 'decimal(14,4)', default = 0,
        required = True,
        notnull = False, unique = False,
        widget = stv_widget_inputMoney, label = 'Total',
        comment = 'Total de la orden de compra',
        writable = False, readable = True,
        represent = stv_represent_money_ifnotzero
        ),
    Field(
        'observaciones', 'text', default = None,
        required = False,
        notnull = False, unique = False,
        widget = lambda v, r: stv_widget_text(v, r, D_additionalAttributes = {'n_rows': 2}), label = 'Observaciones',
        comment = None,
        writable = True, readable = True,
        represent = None
        ),
    Field(
        'referencia', 'string', length = 20, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Referencia', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'fecha', type = FIELD_UTC_DATETIME, default = request.browsernow,
        required = False, requires = IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME),
        notnull = False,
        widget = stv_widget_inputDateTime, label = 'Fecha', comment = 'Fecha del movimiento físico',
        writable = False, readable = True,
        represent = stv_represent_datetime
        ),
    Field(
        'fecha_requerida', type = FIELD_UTC_DATETIME, default = request.browsernow,
        required = False, requires = IS_NULL_OR(IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME)),
        notnull = False,
        widget = stv_widget_inputDateTime, label = 'Fecha de requisición', comment = 'Fecha en que la orden debe ser surtida',
        writable = False, readable = True,
        represent = stv_represent_datetime
        ),
    Field(
        'descripcion', 'text', length = 100, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Descripcion', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'inconsistencias', 'text', length = 200, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Inconsistencias', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'estatus', 'integer', default = TEMPRESA_ALMACEN_ORDENESCOMPRA.ESTATUS.GENERANDO,
        required = True, requires = IS_IN_SET(
            TEMPRESA_ALMACEN_ORDENESCOMPRA.ESTATUS.GET_DICT(), zero = 0, error_message = 'Selecciona'
            ),
        notnull = True, unique = False,
        widget = stv_widget_combobox,
        label = 'Estatus', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_list(
            v, r, None, TEMPRESA_ALMACEN_ORDENESCOMPRA.ESTATUS.GET_DICT()
            )
        ),
    Field(
        'fecha_aprobacion', type = FIELD_UTC_DATETIME, default = None,
        required = False, requires = IS_NULL_OR(IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME)),
        notnull = False,
        widget = stv_widget_inputDateTime, label = 'Fecha de aprobación', comment = None,
        writable = False, readable = True,
        represent = stv_represent_datetime
        ),
    Field(
        'usuario_aprobador_id', 'integer', default = None,
        required = False, requires = IS_NULL_OR(IS_IN_DB(db, 'auth_user.id', db.auth_user._format)),
        ondelete = 'NO ACTION', notnull = True, unique = False,
        widget = lambda f, v: stv_widget_db_search(
            f, v,
            s_display = '%(auth_user.first_name)s %(auth_user.last_name)s [%(auth_user.email)s]',
            s_url = URL(a = 'backoffice', c = 'accesos', f = 'usuarios_buscar'),
            D_additionalAttributes = {'reference': db.auth_user.id}
            ),
        label = T('Usuario de almacén'), comment = 'Usuarios de almacén',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_plaza_sucursal_almacen_almacenistas.usuario_id)
        ),
    tSignature_dbc01,
    format = '%(id)s',
    singular = 'Orden de Compra',
    plural = 'Órdenes de Compra',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_ALMACEN_PRODUCTOS:

    @staticmethod
    def CONFIGURA_CAMPOS_EDICION(
            x_empresa_almacen_producto,
            ):
        """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

        @param x_empresa_almacen_producto:
        @type x_empresa_almacen_producto:
        @return:
        @rtype:
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTable = dbc01.tempresa_almacen_productos

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_empresa_almacen_producto, _dbTable)
        _n_almacen_producto_id = _D_results.n_id
        _dbRow_almacen_producto = _D_results.dbRow

        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.E_return = _D_results.E_return
            _D_return.s_msgError = _D_results.s_msgError

        else:
            _D_results = TEMPRESA_ALMACEN_PRODUCTOS.CONFIGURA_CAMPOS_NUEVO(
                s_almacen_producto_id = None,
                )
            _D_return.E_return = _D_results.E_return
            _D_return.s_msgError = _D_results.s_msgError

            _dbRows_invMovtosConProducto = dbc01(
                (dbc01.tempresa_inventariomovimientos.empresa_plaza_sucursal_almacen_id == _dbRow_almacen_producto.empresa_plaza_sucursal_almacen_id)
                & (dbc01.tempresa_invmovto_productos.empresa_inventariomovimiento_id == dbc01.tempresa_inventariomovimientos.id)
                & (dbc01.tempresa_invmovto_productos.empresa_prodserv_id == _dbRow_almacen_producto.empresa_prodserv_id)
                ).select(
                    dbc01.tempresa_invmovto_productos.id
                    )

            if _dbRows_invMovtosConProducto:
                # Si ya existen movimientos con el producto del almacen, no se puede modificar
                _dbTable.empresa_prodserv_id.writable = False
            else:
                pass

        return _D_return

    @staticmethod
    def CONFIGURA_CAMPOS_NUEVO(
            s_almacen_producto_id,
            ):
        """ Se configuran los campos en caso de un registro nuevo

        @param s_almacen_producto_id: Si lo tiene definido, copia sus valores por defaulr
        @type s_almacen_producto_id:
        @return:
        @rtype:
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            b_tieneMovimientosCosteo = False
            )

        _dbTable = dbc01.tempresa_almacen_productos

        if s_almacen_producto_id:
            _dbRow_base = _dbTable(s_almacen_producto_id)

            # _dbTable.empresa_plaza_sucursal_almacen_id.default = _dbRow_base.empresa_plaza_sucursal_almacen_id
            # _dbTable.empresa_prodserv_id.default = empresa_prodserv_id.empresa_prodserv_id
            # _dbTable.cantidad.default = _dbRow_base.cantidad
            # _dbTable.cantidad_maxima.default = _dbRow_base.cantidad_maxima
            # _dbTable.cantidad_minima.default = _dbRow_base.cantidad_minima
            # _dbTable.infoadicional.default = _dbRow_base.infoadicional
            # _dbTable.observaciones.default = _dbRow_base.observaciones

        else:
            pass

        _dbTable.empresa_plaza_sucursal_almacen_id.writable = False
        _dbTable.empresa_prodserv_id.writable = True
        _dbTable.cantidad.writable = False
        _dbTable.cantidad_maxima.writable = True
        _dbTable.cantidad_minima.writable = True
        _dbTable.infoadicional.writable = True
        _dbTable.observaciones.writable = True

        return _D_return

    class BUSQUEDA_AVANZADA:

        class FILTROS:
            NO_DEFINIDO = 0
            TODOS = 1
            PROD_EN_ALMACEN = 2
            PROD_CANT_MENOR_MIN = 3
            PROD_CANT_MAYOR_MAX = 4

            @classmethod
            @cache("TEMPRESA_ALMACEN_PRODUCTOS.BUSQUEDA_AVANZADA.FILTROS", time_expire = 86400, cache_model = cache.ram)
            def GET_DICT(cls):
                return {
                    cls.TODOS              : 'Todos',
                    cls.PROD_EN_ALMACEN    : 'Sólo prod. en almacén',
                    cls.PROD_CANT_MENOR_MIN: 'Sólo prod. cant < min',
                    cls.PROD_CANT_MAYOR_MAX: 'Sólo prod. cant > max',
                    }

        @staticmethod
        def CONFIGURA(
                s_empresa_id,
                O_cat,
                **D_valoresDefault
                ):
            """

            @param s_empresa_id:
            @type s_empresa_id:
            @param O_cat:
            @type O_cat:
            @param D_valoresDefault:
            @type D_valoresDefault:
            @return:
            @rtype:
            """
            _D_return = Storage(
                E_return = CLASS_e_RETURN.OK,
                s_msgError = "",
                b_tieneMovimientosCosteo = False
                )

            _dbTable = dbc01.tempresa_almacen_productos
            # Variable que contendrá los campos a editar en la búsqueda avanzada.
            _L_dbFields = []

            D_valoresDefault = Storage(D_valoresDefault)

            # Se configura el uso del campo categorias para poder filtrar por varias clasificaciones.
            _dbField_categorias = dbc01.tempresa_prodserv.categoria_id.clone()
            _dbField_categorias.requires = IS_NULL_OR(
                IS_IN_DB(
                    dbc01(dbc01.tempresa_prodcategorias.empresa_id == s_empresa_id),
                    'tempresa_prodcategorias.id',
                    dbc01.tempresa_prodcategorias._format
                    )
                )
            _dbField_categorias.widget = lambda f, v: stv_widget_db_chosen_tree(
                f, v, 20, D_additionalAttributes = {
                    'dbTable_tree'        : dbc01.tempresa_prodcategorias,
                    'dbField_order'       : dbc01.tempresa_prodcategorias.orden,
                    'dbField_tree_groupby': dbc01.tempresa_prodcategorias.padre_id,
                    'class_dbTree'        : DB_Tree,
                    'b_onlyLastNodes'     : False,
                    'b_includeId'         : True,
                    'L_addFieldsInfo'     : [dbc01.tempresa_prodcategorias.uso_cascos1a1]
                    }
                )
            _dbField_categorias.length = 400

            # Se agregan campos sin modificaciones.
            _L_dbFields += [
                _dbField_categorias,
                dbc01.tempresa_prodserv.tiposeguimiento.clone(),
                Field(
                    'busqueda_predefinida', 'integer', default = TEMPRESA_ALMACEN_PRODUCTOS.BUSQUEDA_AVANZADA.FILTROS.TODOS,
                    required = True, requires = IS_IN_SET(
                        TEMPRESA_ALMACEN_PRODUCTOS.BUSQUEDA_AVANZADA.FILTROS.GET_DICT(), zero = 0, error_message = 'Selecciona'
                        ),
                    notnull = True, unique = False,
                    widget = stv_widget_combobox,
                    label = 'Búsqueda predefinida', comment = None,
                    writable = True, readable = True,
                    represent = lambda v, r: stv_represent_list(
                        v, r, None, TEMPRESA_ALMACEN_PRODUCTOS.BUSQUEDA_AVANZADA.FILTROS.GET_DICT()
                        )
                    ),
                ]

            # Se configura la edición de los campos.
            for _dbField in _L_dbFields:
                _dbField.writable = True
                _dbField.readable = True

            O_cat.cfgRow_advSearch(
                L_dbFields_formFactory_advSearch = _L_dbFields,
                dbRow_formFactory_advSearch = None,
                D_hiddenFields_formFactory_advSearch = {},
                s_style = 'bootstrap',
                L_buttons = []
                )

            return _D_return

        @staticmethod
        def GENERA_PREFILTRO(
                D_args,
                ):
            """

            @return:
            @rtype:
            """

            _D_return = Storage(
                E_return = CLASS_e_RETURN.OK,
                s_msgError = "",
                qry = True,
                dbTable = dbc01.tcfdi_registropagos
                )

            if D_args.s_accion == request.stv_fwk_permissions.btn_find.code:
                # Si esta procesando una búsqueda
                if request.vars.get('busqueda_predefinida', False):
                    # Si incluye un var llamado busqueda_predefinida, definido en la consulta avanzada
                    _E_busqueda = int(
                        request.vars.get(
                            'busqueda_predefinida',
                            TEMPRESA_ALMACEN_PRODUCTOS.BUSQUEDA_AVANZADA.FILTROS.TODOS
                            )
                        )

                    if _E_busqueda == TEMPRESA_ALMACEN_PRODUCTOS.BUSQUEDA_AVANZADA.FILTROS.TODOS:
                        pass

                    elif _E_busqueda == TEMPRESA_ALMACEN_PRODUCTOS.BUSQUEDA_AVANZADA.FILTROS.PROD_EN_ALMACEN:
                        _D_return.qry &= dbc01.tempresa_almacen_productos.empresa_plaza_sucursal_almacen_id == D_args.s_almacen_id

                    elif _E_busqueda == TEMPRESA_ALMACEN_PRODUCTOS.BUSQUEDA_AVANZADA.FILTROS.PROD_CANT_MENOR_MIN:
                        _D_return.qry &= dbc01.tempresa_almacen_productos.cantidad < dbc01.tempresa_almacen_productos.cantidad_minima

                    elif _E_busqueda == TEMPRESA_ALMACEN_PRODUCTOS.BUSQUEDA_AVANZADA.FILTROS.PROD_CANT_MAYOR_MAX:
                        _D_return.qry &= dbc01.tempresa_almacen_productos.cantidad > dbc01.tempresa_almacen_productos.cantidad_maxima

                    else:
                        pass
                else:
                    pass

            else:
                pass

            return _D_return

    @staticmethod
    def AL_VALIDAR(O_form):
        return O_form

    @staticmethod
    def AL_ACEPTAR(O_form):
        return O_form

    @staticmethod
    def ANTES_ELIMINAR(L_errorsAction, dbRow):
        """ Antes de eliminar un movimiento de inventario de este tipo

        @param dbRow:
        @type dbRow:
        @param L_errorsAction:
        @type L_errorsAction:
        @return: errores
        @rtype: lista de diccionarios {'msg':'...'}
        """

        if dbRow.cantidad != 0:
            L_errorsAction.append({'msg': "No puedes eliminar un producto del almacén, si hay existencias. "})
        else:
            pass

        return L_errorsAction

    @staticmethod
    def VERIFICA_INSERTA(s_almacen_id, s_prodserv_id, **D_camposInsertar):
        """ Verifica si el producto existe en el almacén, si no existe, lo crea

        @param s_almacen_id:
        @type s_almacen_id:
        @param s_prodserv_id:
        @type s_prodserv_id:
        @return:
        @rtype:
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            n_id = None,
            dbRow = None  # Si lo encontró, regresa el dbRow
            )

        # Se verifica que el registro exista en productos del almacén.
        _dbRows = dbc01(
            (dbc01.tempresa_almacen_productos.empresa_plaza_sucursal_almacen_id == s_almacen_id)
            & (dbc01.tempresa_almacen_productos.empresa_prodserv_id == s_prodserv_id)
            ).select(
            dbc01.tempresa_almacen_productos.ALL
            )

        if _dbRows:
            _D_return.n_id = _dbRows.first().id
            _D_return.dbRow = _dbRows.first()

        else:
            if D_camposInsertar:
                _D_camposInsertar = Storage(D_camposInsertar)
            else:
                _D_camposInsertar = Storage()
            _D_camposInsertar.empresa_plaza_sucursal_almacen_id = s_almacen_id
            _D_camposInsertar.empresa_prodserv_id = s_prodserv_id
            _D_return.n_id = dbc01.tempresa_almacen_productos.insert(**_D_camposInsertar)

        return _D_return

    @staticmethod
    def IMPORTAR_EXCEL(
            s_pathArchivo,
            s_nombreHoja = None,
            **D_otros
            ):
        """ Se identifica el registro a través de la columna id, y se importan cantidad_maxima, cantidad_minima e info adicional.
        Si cantidad_maxima es cero, se ignora la importación del registro.

        @param s_pathArchivo:
        @type s_pathArchivo:
        @param s_nombreHoja:
        @type s_nombreHoja:
        @param D_otros:
        @type D_otros:
        @return:
        @rtype:
        """

        def alValidarRegistroExcel(
                D_registro,
                dbRow_encontrado,
                dbRows_encontrados,
                b_fueIdentificadoAntes,
                b_hayVariasCoincidenciasEnTabla,
                b_encontradoEnTabla
                ):
            _D_return = Storage(
                E_return = stvfwk2_e_RETURN.OK,
                s_msgError = "",
                E_resultado_validacion = IMPORTAR_EXCEL.E_RESULTADO_VALIDACION.OK
                )
            _s_campo = dbc01.tempresa_almacen_productos.cantidad_maxima.name

            if _s_campo not in D_registro:
                _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                _D_return.s_msgError = "Campo %s no mapeado en registro de excel" % _s_campo

            else:
                if D_registro[_s_campo] == 0:
                    # Se ignora el regsitro si no tienen cantidad máxima definida
                    _D_return.E_resultado_validacion = IMPORTAR_EXCEL.E_RESULTADO_VALIDACION.IGNORARLO
                else:
                    pass

            return _D_return

        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        D_otros = Storage(D_otros)
        _dbTabla = dbc01.tempresa_almacen_productos
        _O_importar = IMPORTAR_EXCEL(
            dbTabla = _dbTabla,
            b_insertar_noRelacionados = True,
            b_ignorar_noRelacionados = False,
            b_insertarTodos = False,
            fnIdentificarHeader = None,
            alValidar = alValidarRegistroExcel,
            alAceptar = None
            )
        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = None,
            dbCampo = _dbTabla.empresa_plaza_sucursal_almacen_id,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.NUMERICO,
            s_nombreColumna = None,
            n_columna = None,
            x_valorFijo = D_otros.s_almacen_id,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = None,
            dbCampo = _dbTabla.empresa_prodserv_id,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.NUMERICO,
            s_nombreColumna = _dbTabla.id.label,
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.cantidad_maxima,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.DECIMAL,
            s_nombreColumna = _dbTabla.cantidad_maxima.label,
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.cantidad_minima,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.DECIMAL,
            s_nombreColumna = _dbTabla.cantidad_minima.label,
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.infoadicional,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = _dbTabla.infoadicional.label,
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _D_result = _O_importar.importar(
            s_pathArchivo = s_pathArchivo,
            s_nombreHoja = s_nombreHoja or 'Sheet1'
            )

        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

        return _D_return

    pass


dbc01.define_table(
    'tempresa_almacen_productos',
    Field(
        'empresa_plaza_sucursal_almacen_id', dbc01.tempresa_plaza_sucursal_almacenes, default = None,
        required = True,
        ondelete = 'NO ACTION', notnull = True, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Almacén', comment = None,
        writable = False, readable = True,
        represent = stv_represent_referencefield
        ),
    Field(
        'empresa_prodserv_id', dbc01.tempresa_prodserv, default=None,
        required=True,
        ondelete='NO ACTION', notnull=True, unique=False,
        widget = lambda f, v: stv_widget_db_search(
            f, v,
            s_display = dbc01.tempresa_prodserv._format,
            s_url = URL(a = 'app_inventarios', c = '600busquedas', f = 'productos'),
            D_additionalAttributes = {'reference': dbc01.tempresa_prodserv.id}
            ),
        label='Producto', comment=None,
        writable=False, readable=True,
        represent = lambda v, r: stv_represent_referencefield(v, r)
        ),
    Field(
        'cantidad', 'decimal(14,4)', default = 0,
        required = True,
        notnull = False, unique = False,
        widget = stv_widget_inputFloat, label = 'Cantidad', comment = None,
        writable = False, readable = True,
        represent = stv_represent_float
        ),
    Field(
        'cantidad_maxima', 'decimal(14,4)', default = 0,
        required = True,
        notnull = False, unique = False,
        widget = stv_widget_inputFloat, label = 'Cantidad Max.', comment = None,
        writable = False, readable = True,
        represent = stv_represent_float
        ),
    Field(
        'cantidad_minima', 'decimal(14,4)', default = 0,
        required = True,
        notnull = False, unique = False,
        widget = stv_widget_inputFloat, label = 'Cantidad Min.', comment = None,
        writable = False, readable = True,
        represent = stv_represent_float
        ),
    Field(
        'infoadicional', 'text', length = 200, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Info. Adicional', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'observaciones', 'text', default = None,
        required = False,
        notnull = False, unique = False,
        widget = lambda v, r: stv_widget_text(v, r, D_additionalAttributes = {'n_rows': 2}), label = 'Observaciones',
        comment = None,
        writable = True, readable = True,
        represent = stv_represent_text
        ),
    tSignature_dbc01,
    format = '%(descripcion)s [%(codigo)s]',
    singular = 'Producto',
    plural = 'Productos',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )



# Se crea la referencia para su uso en los campos
# Referencia a tempresa_embarque_almacenes
dbc01.tempresa_plaza_sucursal_almacenes._referenced_by.append(dbc01.tempresa_embarque_almacenes.empresa_plaza_sucursal_almacen_id)
dbc01.tempresa_plaza_sucursal_almacenes._references.append(dbc01.tempresa_embarque_almacenes.empresa_plaza_sucursal_almacen_id)

dbc01.tempresa_embarque_almacenes.empresa_plaza_sucursal_almacen_id.requires = IS_IN_DB(dbc01, 'tempresa_plaza_sucursal_almacenes.id', dbc01.tempresa_plaza_sucursal_almacenes._format)
