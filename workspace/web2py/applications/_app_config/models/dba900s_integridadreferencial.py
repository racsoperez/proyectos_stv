# -*- coding: utf-8 -*-

# Se crea la referencia para su uso en los campos
# Referencia a tempresa_cuentasbancarias
TEMPRESA_CUENTASBANCARIAS.CONFIGURAR_INTEGRIDAD_REFERENCIAL()
TEMPRESA_CLIENTE_GUIASCLIENTES.CONFIGURAR_INTEGRIDAD_REFERENCIAL()
TEMPRESA_PROVEEDOR_GUIASPROVEEDORES.CONFIGURAR_INTEGRIDAD_REFERENCIAL()
TEMPRESA_PROVEEDOR_PRECIOOFICIAL_PRODUCTOS.CONFIGURAR_INTEGRIDAD_REFERENCIAL()
TEMPRESA_PLAZA_GUIASCLIENTES.CONFIGURAR_INTEGRIDAD_REFERENCIAL()
TEMPRESA_PLAZA_GUIASPROVEEDORES.CONFIGURAR_INTEGRIDAD_REFERENCIAL()
TEMPRESA_PLAZA_SUCURSAL_GUIASCLIENTES.CONFIGURAR_INTEGRIDAD_REFERENCIAL()
TEMPRESA_PLAZA_SUCURSAL_GUIASPROVEEDORES.CONFIGURAR_INTEGRIDAD_REFERENCIAL()
TEMPRESA_EMPLEADO_CUENTAS.CONFIGURAR_INTEGRIDAD_REFERENCIAL()
TEMPRESA_RECURSOSCOMPROBAR.CONFIGURAR_INTEGRIDAD_REFERENCIAL()
TEMPRESA_FONDOSFIJOSCAJA.CONFIGURAR_INTEGRIDAD_REFERENCIAL()
TEMPRESA_EMPLEADO_GUIASCONTABLES.CONFIGURAR_INTEGRIDAD_REFERENCIAL()
TEMPRESA_PLAZA_SUCURSALES.CONFIGURAR_INTEGRIDAD_REFERENCIAL()
TEMPRESA_PLAZA_SUCURSAL_ALMACENES.CONFIGURAR_INTEGRIDAD_REFERENCIAL()
TEMPRESA_INVMOVTO_PRODUCTOS.CONFIGURAR_INTEGRIDAD_REFERENCIAL()

# Se agregan los indices en la tablas donde son requeridos y los campos que son requeridos
STV_FWK_LIB.AGREGAR_INDICES(
    dbc01, [
        dbc01.tcfdis.serie,
        dbc01.tcfdis.folio,
        dbc01.tcfdis.fecha,
        dbc01.tcfdis.fecha_str,
        dbc01.tcfdis.fechatimbrado,
        dbc01.tcfdis.total,
        dbc01.tcfdis.lugarexpedicion,
        dbc01.tcfdis.receptorrfc,
        dbc01.tcfdis.uuid,
        dbc01.tcfdis.sat_fecha_cancelacion,
        dbc01.tcfdis.tipocomprobante_id,
        dbc01.tcfdis.receptorcliente_id,
        dbc01.tcfdis.metodopago_id,
        dbc01.tcfdis.cfdi_complemento_id,

        dbc01.tcfdi_conceptos.cfdi_id,
        dbc01.tcfdi_conceptos.empresa_prodserv_id,

        dbc01.tcfdi_relacionados.cfdirelacionado_id,
        dbc01.tcfdi_relacionados.cfdi_id,

        dbc01.tcfdi_complementopagos.cfdi_id,

        dbc01.tcfdi_complementopago_doctosrelacionados.cfdirelacionado_id,
        dbc01.tcfdi_complementopago_doctosrelacionados.cfdi_complementopago_id,

        dbc01.tcfdisrx.folio,
        dbc01.tcfdisrx.saldo,
        dbc01.tcfdisrx.emisorproveedor_id,
        dbc01.tcfdisrx.fecha,
        dbc01.tcfdisrx.tipocomprobante_id,
        dbc01.tcfdisrx.metodopago_id,
        dbc01.tcfdisrx.uuid,
        dbc01.tcfdisrx.fechatimbrado,
        dbc01.tcfdisrx.sat_fecha_cancelacion,

        dbc01.tcfdirx_conceptos.empresa_prodserv_id,

        dbc01.tempresa_prodserv.codigo,
        dbc01.tempresa_prodserv.descripcion,

        dbc01.tempresa_clientes.codigo,
        dbc01.tempresa_clientes.razonsocial,
        dbc01.tempresa_clientes.nombrecorto,

        dbc01.tcfdi_movimientos.cfdi_ingreso_id,
        dbc01.tcfdi_movimientos.fecha,
        ]
    )

STV_FWK_LIB.AGREGAR_INDICES(
    db01, [
        db01.tprodservs.c_claveprodserv,
        db01.tprodservcp.c_claveprodservcp,
        ]
    )