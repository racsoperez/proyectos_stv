# -*- coding: utf-8 -*-

# Diarios especiales CONTPAQi
class TEMPRESA_DIARIOS:
    
    class E_TIPO:
        ''' Se definen las opciónes del campo '''
        DE_POLIZA = 1
        DE_MOVIMIENTOS = 2
        EFECTIVO_INGRESO = 3
        EFECTIVO_EGRESO = 4
        
        @classmethod
        def get_dict(cls):
            return {
                cls.DE_POLIZA: 'De póliza',
                cls.DE_MOVIMIENTOS: 'De Movimientos',
                cls.EFECTIVO_INGRESO: 'Efectivo Ingreso',
                cls.EFECTIVO_EGRESO: 'Efectivo Egreso',
                }
    
    pass
dbc01.define_table(
    'tempresa_diarios',
    Field('empresa_id', dbc01.tempresas, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Empresa', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('codigo', 'string', length=10, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Código CONTPAQi', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('nombre', 'string', length=50, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Nombre', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('tipo', 'integer', default = 1,
      required = False, requires=IS_IN_SET(TEMPRESA_DIARIOS.E_TIPO.get_dict(), zero=0, error_message= 'Selecciona'),
      notnull = False, unique = False,
      widget = stv_widget_combobox, label = 'Tipo Diario', comment = '',
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_DIARIOS.E_TIPO.get_dict())
      ),
    Field('tipo_documento_pago_contpaqi', 'string', length=2, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Tipo de documento de pago CONTPAQi', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    
    #Campos para ver si el registro ya fue verificado anteriormente en CONTPAQi
    Field('verificacioncontpaqi', 'boolean', default=0,
      required = False,
      notnull = False,
      widget=stv_widget_inputCheckbox, label=T('Verificación CONTPAQi'), comment = None,
      writable = True, readable = True,
      represent = stv_represent_boolean),
    Field('verificacioncontpaqi_fecha', type = FIELD_UTC_DATETIME, default=request.browsernow,
      required=False, requires = IS_NULL_OR(IS_DATETIME(format= STV_FWK_APP.FORMAT.s_DATETIME)),
      notnull=False,
      widget=stv_widget_inputDateTime, label='Fecha de verificación del CONTPAQi', comment=None,
      writable=True, readable=True,
      represent=stv_represent_datetime), 

    tSignature_dbc01,
    format='%(codigo)s: %(nombre)s',
    singular='Diario',
    plural='Diarios',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )


# Segmentos de negocio CONTPAQi
class TEMPRESA_SEGMENTOS:
    pass
dbc01.define_table(
    'tempresa_segmentos',
    Field('empresa_id', dbc01.tempresas, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Empresa', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('codigo', 'string', length=4, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Código CONTPAQi', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('nombre', 'string', length=50, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Nombre', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    
    #Campos para ver si el registro ya fue verificado anteriormente en CONTPAQi
    Field('verificacioncontpaqi', 'boolean', default=0,
      required = False,
      notnull = False,
      widget=stv_widget_inputCheckbox, label=T('Verificación CONTPAQi'), comment = None,
      writable = True, readable = True,
      represent = stv_represent_boolean),
    Field('verificacioncontpaqi_fecha', type = FIELD_UTC_DATETIME, default=request.browsernow,
      required=False, requires = IS_NULL_OR(IS_DATETIME(format= STV_FWK_APP.FORMAT.s_DATETIME)),
      notnull=False,
      widget=stv_widget_inputDateTime, label='Fecha de verificación del CONTPAQi', comment=None,
      writable=True, readable=True,
      represent=stv_represent_datetime), 

    tSignature_dbc01,
    format='%(codigo)s: %(nombre)s',
    singular='Segmento',
    plural='Segmentos',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

# Tipos de póliza CONTPAQi
class TEMPRESA_TIPOSPOLIZA:
    pass
dbc01.define_table(
    'tempresa_tipospoliza',
    Field('empresa_id', dbc01.tempresas, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Empresa', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('codigo', 'string', length=4, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Código CONTPAQi', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('nombre', 'string', length=50, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Nombre', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),

    tSignature_dbc01,
    format='%(codigo)s: %(nombre)s',
    singular='Tipo de Póliza',
    plural='Tipos de Póliza',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

# Tipos de póliza CONTPAQi
class TEMPRESA_TIPOPOLIZA_EJERCICIOS:
    class FOLIADO:
        POR_EJERCICIO = 1
        POR_PERIODO = 2
        
        @classmethod
        def get_dict(cls):
            return {
                cls.POR_EJERCICIO: 'Por Ejercicio',
                cls.POR_PERIODO: 'Por Periodo',
                }
            
    class CLASE_POLIZA:
        SIN_AFECTAR_SALDOS = 0
        AFECTANDO_SALDOS = 1
        
        @classmethod
        def get_dict(cls):
            return {
                cls.SIN_AFECTAR_SALDOS: 'Sin afectar saldos',
                cls.AFECTANDO_SALDOS: 'Afectando saldos',
                }
            
    class IMPRESION_POLIZA:
        POLIZA_NO_IMPRESA = 0
        POLIZA_IMPRESA = 1
        
        @classmethod
        def get_dict(cls):
            return {
                cls.POLIZA_NO_IMPRESA: 'Póliza no impresa',
                cls.POLIZA_IMPRESA: 'Póliza impresa',
                }

    pass
dbc01.define_table(
    'tempresa_tipopoliza_ejercicios',
    Field('empresa_tipopoliza_id', dbc01.tempresa_tipospoliza, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Tipo Póliza', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('ejercicio', 'integer', default = None,
      required = True, requires = IS_IN_SET(VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id), zero = 0, error_message = 'Selecciona'),
      notnull = False, unique = False,
      widget = stv_widget_combobox, 
      label = 'Ejercicio inicial', comment = None,
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id))),
    Field('folioejercicio', 'decimal(5,0)', default=1,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_inputFloat, label='Folio Ejercicio', comment='Especifica el siguiente folio a usar',
      writable=True, readable=True,
      represent=stv_represent_number),
    Field('folioperiodo01', 'decimal(5,0)', default=1,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_inputFloat, label='Folio Periodo 01', comment=None,
      writable=True, readable=True,
      represent=stv_represent_number),
    Field('folioperiodo02', 'decimal(5,0)', default=1,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_inputFloat, label='Folio Periodo 02', comment=None,
      writable=True, readable=True,
      represent=stv_represent_number),
    Field('folioperiodo03', 'decimal(5,0)', default=1,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_inputFloat, label='Folio Periodo 03', comment=None,
      writable=True, readable=True,
      represent=stv_represent_number),
    Field('folioperiodo04', 'decimal(5,0)', default=1,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_inputFloat, label='Folio Periodo 04', comment=None,
      writable=True, readable=True,
      represent=stv_represent_number),
    Field('folioperiodo05', 'decimal(5,0)', default=1,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_inputFloat, label='Folio Periodo 05', comment=None,
      writable=True, readable=True,
      represent=stv_represent_number),
    Field('folioperiodo06', 'decimal(5,0)', default=1,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_inputFloat, label='Folio Periodo 06', comment=None,
      writable=True, readable=True,
      represent=stv_represent_number),
    Field('folioperiodo07', 'decimal(5,0)', default=1,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_inputFloat, label='Folio Periodo 07', comment=None,
      writable=True, readable=True,
      represent=stv_represent_number),
    Field('folioperiodo08', 'decimal(5,0)', default=1,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_inputFloat, label='Folio Periodo 08', comment=None,
      writable=True, readable=True,
      represent=stv_represent_number),
    Field('folioperiodo09', 'decimal(5,0)', default=1,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_inputFloat, label='Folio Periodo 09', comment=None,
      writable=True, readable=True,
      represent=stv_represent_number),
    Field('folioperiodo10', 'decimal(5,0)', default=1,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_inputFloat, label='Folio Periodo 10', comment=None,
      writable=True, readable=True,
      represent=stv_represent_number),
    Field('folioperiodo11', 'decimal(5,0)', default=1,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_inputFloat, label='Folio Periodo 11', comment=None,
      writable=True, readable=True,
      represent=stv_represent_number),
    Field('folioperiodo12', 'decimal(5,0)', default=1,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_inputFloat, label='Folio Periodo 12', comment=None,
      writable=True, readable=True,
      represent=stv_represent_number),
    Field('foliado', 'integer', default = TEMPRESA_TIPOPOLIZA_EJERCICIOS.FOLIADO.POR_EJERCICIO,
      required = False, requires=IS_IN_SET(TEMPRESA_TIPOPOLIZA_EJERCICIOS.FOLIADO.get_dict(), zero=0, error_message= 'Selecciona'),
      notnull = False, unique = False,
      widget = stv_widget_combobox, label = 'Foliado', comment = '',
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_TIPOPOLIZA_EJERCICIOS.FOLIADO.get_dict())
      ),
    Field('clasepoliza', 'integer', default = TEMPRESA_TIPOPOLIZA_EJERCICIOS.CLASE_POLIZA.SIN_AFECTAR_SALDOS,
      required = True, requires=IS_IN_SET(TEMPRESA_TIPOPOLIZA_EJERCICIOS.CLASE_POLIZA.get_dict(), zero=0, error_message= 'Selecciona'),
      notnull = True, unique = False,
      widget = stv_widget_combobox, label = 'Exportar pólizas - Afectación', comment = 'Indicar si la póliza aplicará a saldos de inmediato, en cuanto sea exportada a CONTPAQi Contabilidad',
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_TIPOPOLIZA_EJERCICIOS.CLASE_POLIZA.get_dict())
      ),
    Field('impresionpoliza', 'integer', default = TEMPRESA_TIPOPOLIZA_EJERCICIOS.IMPRESION_POLIZA.POLIZA_NO_IMPRESA,
      required = True, requires=IS_IN_SET(TEMPRESA_TIPOPOLIZA_EJERCICIOS.IMPRESION_POLIZA.get_dict(), zero=0, error_message= 'Selecciona'),
      notnull = True, unique = False,
      widget = stv_widget_combobox, label = 'Exportar póliza - Impresión', comment = 'Indicar si la póliza se marcará al exportar como Impresa o Sin imprimir',
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_TIPOPOLIZA_EJERCICIOS.IMPRESION_POLIZA.get_dict())
      ),    
    tSignature_dbc01,
    format='%(ejercicio)s: %(folioejercicio)s',
    singular='Ejerciio',
    plural='Ejercicios',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_MONEDASCONTPAQI(Table):
    """ Definición de la tabla de empresa_codigomonedas_contpaqi """

    S_NOMBRETABLA = 'tempresa_monedascontpaqi'

    L_DBCAMPOS = [
        Field(
            'empresa_id', dbc01.tempresas, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'Empresa', comment = None,
            writable = False, readable = False,
            represent = None
            ),
        Field(
            'moneda_id', 'integer', default = D_stvSiteHelper.dbconfigs.defaults.moneda_id,
            required = True, requires = IS_IN_DB(db01, 'tmonedas.id', db01.tmonedas._format),
            ondelete = 'NO ACTION', notnull = True, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Moneda',
            comment = 'Moneda en la que se lleva la contabilidad de la empresa',
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresas.moneda_id)
            ),
        Field(
            'moneda_contpaqi', 'string', length = 4, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Código Moneda CONTPAQi',
            comment = 'Código de moneda utilziada en CONTPAQi',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'descripcion', 'string', length = 100, default = D_stvSiteHelper.dbconfigs.defaults.moneda_descripcion,
            required = True,
            notnull = True, unique = False,
            widget = stv_widget_input, label = 'Descripción', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        tSignature_dbc01,
        ]

    def __init__(self, dbRef, tablename, *fields, **args):
        Table.__init__(self, dbRef, tablename, *fields, **args)

        return

    @staticmethod
    def GETROWFORMATED(n_id):
        if n_id:
            if hasattr(dbc01.tempresa_monedascontpaqi._format, '__call__'):
                # ...ejecuta la función y regresa el resultado.
                return dbc01.tempresa_monedascontpaqi._format(dbc01.tempresa_monedascontpaqi(n_id))
            else:
                # ...de lo contrario haz el formato.
                return dbc01.tempresa_monedascontpaqi._format % dbc01.tempresa_monedascontpaqi(n_id).as_dict()
        else:
            return 'Nada'
        
    @staticmethod
    def DEFINIR_DEFAULTS():
        _s_empresa_id = D_stvSiteHelper.dbconfigs.defaults.empresa_id
        _s_cache_id = 'db_moneda_contpaqi_id_' + str(_s_empresa_id)
        D_stvSiteHelper.dbconfigs.defaults.moneda_contpaqi_id = STV_LIB_DB.CACHE_ASOCIAR_TABLA(
            _s_cache_id,
            n_time_expire = 86400,
            dbTabla = dbc01.tempresa_monedascontpaqi, 
            s_nombreCampo = 'moneda_id', 
            s_datoBuscar = TMONEDAS.MXP,
            s_msgError = "No encontrado",
            qry_maestro = (dbc01.tempresa_monedascontpaqi.empresa_id == _s_empresa_id), 
            s_proceso = "Buscando default de moneda_contpaq_id"
            ).n_id
        TEMPRESA_MONEDASCONTPAQI.MXP = D_stvSiteHelper.dbconfigs.defaults.moneda_contpaqi_id
        
        _s_cache_id = 'db_moneda_XXX_contpaqi_id_' + str(_s_empresa_id)
        TEMPRESA_MONEDASCONTPAQI.XXX = STV_LIB_DB.CACHE_ASOCIAR_TABLA(
            _s_cache_id,
            n_time_expire = 86400,
            dbTabla = dbc01.tempresa_monedascontpaqi, 
            s_nombreCampo = 'moneda_id', 
            s_datoBuscar = TMONEDAS.XXX,
            s_msgError = "No encontrado",
            qry_maestro = (dbc01.tempresa_monedascontpaqi.empresa_id == _s_empresa_id), 
            s_proceso = "Buscando default de moneda_contpaq_id"
            ).n_id
        return

    @staticmethod
    def DEFINIR_DBCAMPO(s_nombreCampo = 'monedacontpaqi_id', **D_params):
        D_params = Storage(D_params)
        _dbCampo = Field(
            s_nombreCampo, dbc01.tempresa_monedascontpaqi,
            default = D_params.default or D_stvSiteHelper.dbconfigs.defaults.moneda_contpaqi_id,
            required = D_params.required or False,
            requires = D_params.requires or IS_IN_DB(
                dbc01(dbc01.tempresa_monedascontpaqi.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id),
                'tempresa_monedascontpaqi.id',
                dbc01.tempresa_monedascontpaqi._format
                ),
            ondelete = 'NO ACTION', notnull = D_params.notnull or False, unique = False,
            widget = D_params.widget or (
                lambda f, v: stv_widget_db_chosen(f, v, 1)
                ),
            label = D_params.label or 'Moneda', comment = D_params.comment or "",
            writable = False, readable = True,
            represent = D_params.represent or stv_represent_referencefield
            )

        if not D_params.requires and not D_params.required:
            _dbCampo.requires = IS_NULL_OR(_dbCampo.requires)
        else:
            pass

        return _dbCampo

    pass


dbc01.define_table(
    TEMPRESA_MONEDASCONTPAQI.S_NOMBRETABLA, *TEMPRESA_MONEDASCONTPAQI.L_DBCAMPOS,
    format = '%(moneda_contpaqi)s: %(descripcion)s',
    singular = 'Moneda CONTPAQi',
    plural = 'Monedas CONTPAQi',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )
TEMPRESA_MONEDASCONTPAQI.DEFINIR_DEFAULTS()
STV_FWK_LIB.DEFINIR_INTEGRIDADREFERENCIAL(
    dbc01.tempresa_monedascontpaqi, dbc01.tempresas.monedacontpaqi_id,
    b_actualizarTipo = True,
    fn_widget = lambda f, v: stv_widget_db_chosen(f, v, 1),
    dbQry = dbc01.tempresa_monedascontpaqi.empresa_id == dbc01.tempresas.id
    )



class TEMPRESA_TIPODOCUMENTOSBANCARIOS:
    """ Definición de la tabla de empresa_tipo_documentos_bancarios """

    class TIPO_DOCUMENTO:
        INGRESO = 1
        EGRESO = 2
        
        @classmethod
        def get_dict(cls):
            return {
                cls.INGRESO: 'Ingresos',
                cls.EGRESO: 'Egresos',
                }
            
    pass
dbc01.define_table(
    'tempresa_tipodocumentosbancarios',
    Field('empresa_id', dbc01.tempresas, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Empresa', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('codigo_contpaqi', 'string', length=2, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Código CONTPAQi', comment= 'Código utilizado en CONTAPQi',
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('tipo', 'integer', default = TEMPRESA_TIPODOCUMENTOSBANCARIOS.TIPO_DOCUMENTO.INGRESO,
      required = False, requires=IS_IN_SET(TEMPRESA_TIPODOCUMENTOSBANCARIOS.TIPO_DOCUMENTO.get_dict(), zero=0, error_message= 'Selecciona'),
      notnull = False, unique = False,
      widget = stv_widget_combobox, label = 'Tipo', comment = '',
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_TIPODOCUMENTOSBANCARIOS.TIPO_DOCUMENTO.get_dict())
      ),
    Field('descripcion', 'string', length=25, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Descripción', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    tSignature_dbc01,
    format='%(codigo_contpaqi)s: %(descripcion)s',
    singular='Tipo de documento bancario',
    plural='Tipo de documentos bancarios',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

class TEMPRESA_TIPODOCUMENTOBANCARIO_EJERCICIO:
    pass
dbc01.define_table(
    'tempresa_tipodocumentobancario_ejercicios',
    Field('empresa_tipodocumentobancario_id', dbc01.tempresa_tipodocumentosbancarios, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Tipo Póliza', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('ejercicio', 'integer', default = None,
      required = True, requires = IS_IN_SET(VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id), zero = 0, error_message = 'Selecciona'),
      notnull = False, unique = False,
      widget = stv_widget_combobox, 
      label = 'Ejercicio inicial', comment = None,
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id))),
    Field('folio', 'integer', length= 20, default=1,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_inputFloat, label='Folio', comment='Especifica el siguiente folio a usar',
      writable=True, readable=True,
      represent=stv_represent_number),
    tSignature_dbc01,
    format='%(ejercicio)s: %(folio)s',
    singular='Ejerciio',
    plural='Ejercicios',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

""" Definición de la tabla de empresa_bancos_diarios_especiales """
class TEMPRESA_BANCOSDIARIOSESPECIALES:
    pass
dbc01.define_table(
    'tempresa_bancosdiariosespeciales',
    #TODO: Revisar si este campo sí va aquí. También revisar el nombre
    Field('empresa_id', dbc01.tempresas, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Empresa', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('bancos_venta_contado_diario_id', dbc01.tempresa_diarios, default=None,
      required=False, requires = IS_NULL_OR(IS_IN_DB(dbc01(dbc01.tempresa_diarios.tipo == TEMPRESA_DIARIOS().E_TIPO.EFECTIVO_INGRESO),'tempresa_diarios.id', dbc01.tempresa_diarios._format)),
      ondelete='NO ACTION', notnull=False, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label= 'Bancos, diario especial ventas contado', comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield(v, r)),
    Field('bancos_cobranza_diario_id', dbc01.tempresa_diarios, default=None,
      required=False, requires = IS_NULL_OR(IS_IN_DB(dbc01(dbc01.tempresa_diarios.tipo == TEMPRESA_DIARIOS().E_TIPO.EFECTIVO_INGRESO),'tempresa_diarios.id', dbc01.tempresa_diarios._format)),
      ondelete='NO ACTION', notnull=False, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label= 'Bancos, diario especial cobranza', comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield(v, r)),
    Field('bancos_tipo_documentos_venta_contado', dbc01.tempresa_tipodocumentosbancarios, default=None,
      required=False, requires = IS_NULL_OR(IS_IN_DB(dbc01(dbc01.tempresa_tipodocumentosbancarios.tipo == TEMPRESA_TIPODOCUMENTOSBANCARIOS().TIPO_DOCUMENTO.INGRESO),'tempresa_tipodocumentosbancarios.id', dbc01.tempresa_tipodocumentosbancarios._format)),
      ondelete='NO ACTION', notnull=False, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label= 'Bancos, tipo documento bancario ventas contado', comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield(v, r)),
    Field('bancos_tipo_documentos_cobranza', dbc01.tempresa_tipodocumentosbancarios, default=None,
      required=False, requires = IS_NULL_OR(IS_IN_DB(dbc01(dbc01.tempresa_tipodocumentosbancarios.tipo == TEMPRESA_TIPODOCUMENTOSBANCARIOS().TIPO_DOCUMENTO.INGRESO),'tempresa_tipodocumentosbancarios.id', dbc01.tempresa_tipodocumentosbancarios._format)),
      ondelete='NO ACTION', notnull=False, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label= 'Bancos, tipo documento bancarios cobranza', comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield(v, r)),
    tSignature_dbc01,
    format='%(descripcion)s',
    singular='Banco Diario Especial',
    plural='Bancos Diarios Especiales',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

""" Definición de la tabla de tempresa_bancoscontpaqi """
class TEMPRESA_BANCOSCONTPAQI:
    
    @classmethod
    def GETROWFORMATED(cls, n_id):
        if n_id:
            if hasattr(dbc01.tempresa_bancoscontpaqi._format, '__call__'):
                # ...ejecuta la función y regresa el resultado.
                return dbc01.tempresa_bancoscontpaqi._format(dbc01.tempresa_bancoscontpaqi(n_id))
            else:
                # ...de lo contrario haz el formato.
                return dbc01.tempresa_bancoscontpaqi._format % dbc01.tempresa_bancoscontpaqi(n_id).as_dict()
        else:
            return 'Nada'
        
    pass
dbc01.define_table(
    'tempresa_bancoscontpaqi',
    Field('empresa_id', dbc01.tempresas, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Empresa', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('codigo_contpaqi', 'string', length=5, default=None,
      required=True,
      notnull=True, unique=False,
      widget=stv_widget_input, label= 'Código CONTPAQi', comment= 'Código utilizado en CONTAPQi',
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('nombre', 'string', length=50, default=None,
      required=True,
      notnull=True, unique=False,
      widget=stv_widget_input, label= 'Nombre', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('banco_id', 'integer', default=None,
      required=True, requires = IS_IN_DB(db01, 'tbancos.id', db01.tbancos._format),
      ondelete='NO ACTION', notnull=True, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label= 'Banco', comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_bancoscontpaqi.banco_id)),
    Field('rfc', 'string', length=15, default=None,
      required=False, requires=IS_NULL_OR(IS_MATCH(D_stvFwkHelper.common.regexp_rfc, error_message='Expresión inválida formato AAAA-999999-XXX')),
      notnull=False, unique=False,
      widget=stv_widget_input, label='RFC', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    tSignature_dbc01,
    format='%(codigo_contpaqi)s: %(nombre)s',
    singular='Banco',
    plural='Bancos',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

# Se crea la referencia para su uso en los campos

# Referencia a tempresa_empleado_cuentas
dbc01.tempresa_bancoscontpaqi._referenced_by.append(dbc01.tempresa_empleado_cuentas.empresa_bancocontpaqi_id)
dbc01.tempresa_bancoscontpaqi._references.append(dbc01.tempresa_empleado_cuentas.empresa_bancocontpaqi_id)

dbc01.tempresa_empleado_cuentas.empresa_bancocontpaqi_id.requires = IS_IN_DB(dbc01, 'tempresa_bancoscontpaqi.id', dbc01.tempresa_bancoscontpaqi._format)



""" Definición de la tabla de empresa_contpaqiejercicios """
class TEMPRESA_ERRORESEXPORTACIONCONTPAQI:
    pass
dbc01.define_table(
    'tempresa_erroresexportacioncontpaqi',
    Field('empresa_id', dbc01.tempresas, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Empresa', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('ejercicio', 'integer',
      required=True, default = int(request.browsernow.year),
      notnull=True, unique=False,
      widget = stv_widget_input, label=T('Ejercicio inicial'), comment='Ejercicio',
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('fecha', type = FIELD_UTC_DATETIME, default=request.browsernow,
      required=True, requires = IS_NULL_OR(IS_DATETIME(format= STV_FWK_APP.FORMAT.s_DATETIME)),
      notnull=True,
      widget=stv_widget_inputDateTime, label='Fecha del error', comment=None,
      writable=True, readable=True,
      represent=stv_represent_datetime), 
    ###Se agrega este campo para poder buscar la fecha por día actual
    Field('dia', 'date', default = request.browsernow,
      required = False,
      notnull = False,
      widget = stv_widget_inputDate, label = 'Fecha', comment = '',
      writable = False, readable = True,
      represent = stv_represent_date),
    Field('empresa_contpaqiejercicio_id', dbc01.tempresa_contpaqiejercicios, default=None,
      required=False, requires = IS_IN_DB(dbc01, 'tempresa_contpaqiejercicios.id', dbc01.tempresa_contpaqiejercicios._format),
      ondelete='NO ACTION', notnull=False, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label= 'Base de datos CONTPAQi', comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield(v, r)),
    Field('descripcion', 'text', length=500, default=None,
      required=True,
      notnull=True, unique=False,
      widget=stv_widget_input, label= 'Descripción del error', comment=None,
      writable=True, readable=True,
      represent=stv_represent_text), 
    Field('webservice', 'string', length=80, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Web service', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),   
    format='%(ejercicio)s: %(basedatoscontpaqi)s',
    singular='Error exportación CONTPAQi',
    plural='Errores de exportación CONTPAQi',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_FECHASPOLIZAS:
    class TIPOFECHA:
        ''' Se definen las opciónes del campo '''
        EMISION = 1
        TIMBRADO = 2
        DOCUMENTO_BANCARIO = 3
        
        @classmethod
        def GET_DICT(cls):
            return {
                cls.EMISION: 'Fecha de emisión',
                cls.TIMBRADO: 'Fecha de timbrado',
                cls.DOCUMENTO_BANCARIO: 'Fecha del documento bancario',
                }
    
    pass
dbc01.define_table(
    'tempresa_fechaspolizas',
    Field('empresa_id', dbc01.tempresas, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Empresa', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('ejercicio', 'integer', default = None,
      required = True, requires = IS_IN_SET(VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id), zero = 0, error_message = 'Selecciona'),
      notnull = False, unique = False,
      widget = stv_widget_combobox, 
      label = 'Ejercicio inicial', comment = None,
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id))),
    Field('cfdi_emitido_ingreso', 'integer', default = TEMPRESA_FECHASPOLIZAS.TIPOFECHA.EMISION,
      required = False, requires=IS_IN_SET(TEMPRESA_FECHASPOLIZAS.TIPOFECHA.GET_DICT(), zero=0, error_message= 'Selecciona'),
      notnull = False, unique = False,
      widget = stv_widget_combobox, label = 'CFDI emitido ingreso', comment = '',
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_FECHASPOLIZAS.TIPOFECHA.GET_DICT())
      ),
    Field('cfdi_emitido_egreso', 'integer', default = TEMPRESA_FECHASPOLIZAS.TIPOFECHA.EMISION,
      required = False, requires=IS_IN_SET(TEMPRESA_FECHASPOLIZAS.TIPOFECHA.GET_DICT(), zero=0, error_message= 'Selecciona'),
      notnull = False, unique = False,
      widget = stv_widget_combobox, label = 'CFDI emitido egreso', comment = '',
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_FECHASPOLIZAS.TIPOFECHA.GET_DICT())
      ),
    Field('cfdi_emitido_pago', 'integer', default = TEMPRESA_FECHASPOLIZAS.TIPOFECHA.EMISION,
      required = False, requires=IS_IN_SET(TEMPRESA_FECHASPOLIZAS.TIPOFECHA.GET_DICT(), zero=0, error_message= 'Selecciona'),
      notnull = False, unique = False,
      widget = stv_widget_combobox, label = 'CFDI emitido pago', comment = '',
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_FECHASPOLIZAS.TIPOFECHA.GET_DICT())
      ),
    format='%(ejercicio)s',
    singular='Fecha póliza',
    plural='Fechas pólizas',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )
