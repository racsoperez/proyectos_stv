# -*- coding: utf-8 -*-


class TCFDI_COMPCARTAPORTE:
    """ Sólo puede ser relación uno a uno

    omawww.sat.gob.mx/tramitesyservicios/Paginas/documentos/
     Instructivo_de_llenado_del_CFDI_con_complemento_carta_porte.pdf

    Es una relación de 1 (CFDI) a [0,1].
    Se incluye el nodo Ubicaciones por relación 1 a 1. No tiene atributos por lo que no hay campos.
    Se incluye el nodo Mercancias por relación 1 a 1.
    Se incluye el nodo FiguraTransporte por relación 1 a [0,1].

    """

    @staticmethod
    def DBTABLA(s_id = None):
        if s_id is None:
            return dbc01.tcfdi_compcartaporte
        else:
            return dbc01.tcfdi_compcartaporte(s_id)

    class TRANSPINTERNAC:
        """ Se definen los valores posible del campo, en base a lo requerido por el SAT """
        NO_DEFINIDO = ''
        SI = 'Sí'
        NO = 'No'
        _D_dict = None

        @classmethod
        def GET_DICT(cls):
            if not cls._D_dict:
                cls._D_dict = {
                    cls.NO_DEFINIDO: 'No Definido',
                    cls.SI         : 'Sí',
                    cls.NO         : 'No',
                    }
            else:
                pass
            return cls._D_dict

    class ENTRADASALIDAMERC:
        """ Se definen los valores posible del campo, en base a lo requerido por el SAT """
        NO_DEFINIDO = ''
        ENTRADA = 'Entrada'
        SALIDA = 'Salida'
        _D_dict = None

        @classmethod
        def GET_DICT(cls):
            if not cls._D_dict:
                cls._D_dict = {
                    cls.NO_DEFINIDO: 'No Definido',
                    cls.ENTRADA    : 'Entrada',
                    cls.SALIDA     : 'Salida',
                    }
            else:
                pass
            return cls._D_dict

    @classmethod
    def PUEDE_CREAR(
            cls,
            x_cfdi
            ):
        """ Determina si puede o no crear un complemento Carta Porte de un CFDI de traslado.
        No se valida que el CFDI este en un estado de editar, ya que la carta porte
        se crea al mismo tiempo que el CFDI principal

        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi, dbc01.tcfdis)
        _dbRow_cfdi = _D_results.dbRow

        if _dbRow_cfdi.tipocomprobante_id in (TTIPOSCOMPROBANTES.TRASLADO, TTIPOSCOMPROBANTES.INGRESO):
            pass
        else:
            _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
            _D_return.s_msgError = "ccp10: No se puede crear carta porte un CDFI diferente a Traslado o Ingreso"

        return _D_return

    @classmethod
    def PUEDE_EDITAR(
            cls,
            x_cfdi_compCartaPorte
            ):
        """ Define si es posible editar el CFDI de traslado.
        @param x_cfdi_compCartaPorte:
        @type x_cfdi_compCartaPorte:

        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_compCartaPorte, dbc01.tcfdi_compcartaporte)
        _dbRow_cfdi_cartaporte = _D_results.dbRow

        _D_results = TCFDIS.PROC.PUEDE_EDITAR(_dbRow_cfdi_cartaporte.cfdi_id)

        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_results)

        else:
            pass

        return _D_return

    @classmethod
    def PUEDE_ELIMINAR(
            cls,
            x_cfdi_compCartaPorte
            ):
        """ Determina si puede o no eliminar un CFDI de traspaso

        Misma condición que puede_editar

        """

        return cls.PUEDE_EDITAR(x_cfdi_compCartaPorte)

    @classmethod
    def CONFIGURACAMPOS_NUEVO(
            cls,
            s_cfdi_compCartaPorte_id,
            x_cfdi
            ):
        """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

        @param s_cfdi_compCartaPorte_id:
        @type s_cfdi_compCartaPorte_id:
        @param x_cfdi:
        @type x_cfdi:
        @return:
        @rtype:
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTable = dbc01.tcfdi_compcartaporte

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi, dbc01.tcfdis)
        _dbRow_cfdi = _D_results.dbRow

        if _dbRow_cfdi:

            _D_results = cls.PUEDE_CREAR(_dbRow_cfdi)

        else:
            _D_results = Storage(
                E_return = CLASS_e_RETURN.OK_WARNING,
                s_msgError = "No se identificó el CFDI, puede ser normal, ya que se crean al mismo tiempo"
                )

        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.E_return = _D_results.E_return
            _D_return.s_msgError = _D_results.s_msgError

        elif s_cfdi_compCartaPorte_id:
            _dbRow_base = _dbTable(s_cfdi_compCartaPorte_id)

            # _dbTable.cfdi_id.default = _dbRow_base.cfdi_id
            _dbTable.versioncp.default = _dbRow_base.versioncp
            _dbTable.transpinternac.default = _dbRow_base.transpinternac
            _dbTable.entradasalidamerc.default = _dbRow_base.entradasalidamerc
            _dbTable.viaentradasalida.default = _dbRow_base.viaentradasalida
            _dbTable.viaentradasalida_id.default = _dbRow_base.viaentradasalida_id
            _dbTable.totaldistrec.default = _dbRow_base.totaldistrec
            _dbTable.merc_pesobrutototal.default = _dbRow_base.merc_pesobrutototal
            _dbTable.merc_unidadpeso.default = _dbRow_base.merc_unidadpeso
            _dbTable.merc_unidadpeso_id.default = _dbRow_base.merc_unidadpeso_id
            _dbTable.merc_pesonetototal.default = _dbRow_base.merc_pesonetototal
            _dbTable.merc_numtotalmercancias.default = _dbRow_base.merc_numtotalmercancias
            _dbTable.merc_cargoportasacion.default = _dbRow_base.merc_cargoportasacion
            _dbTable.figtransporte_cvetransporte.default = _dbRow_base.figtransporte_cvetransporte
            _dbTable.figtransporte_transporte_id.default = _dbRow_base.figtransporte_transporte_id

        else:
            pass

        _dbTable.cfdi_id.writable = False
        _dbTable.transpinternac.writable = False
        _dbTable.entradasalidamerc.writable = False
        _dbTable.viaentradasalida_id.writable = False
        _dbTable.merc_pesobrutototal.writable = False  # Usado en caso de Ferroviario o Aéreo
        _dbTable.merc_unidadpeso_id.writable = False  # Usado en caso de Ferroviario o Aéreo
        _dbTable.merc_pesonetototal.writable = False  # Usado en caso Ferroviario o Marítimo
        _dbTable.merc_cargoportasacion.writable = False  # Usado cuando es vías aérea
        _dbTable.figtransporte_transporte_id.writable = False

        # Campos que no deben ser editados, solo de forma autocalculada
        _dbTable.viaentradasalida.writable = False
        _dbTable.totaldistrec.writable = False
        _dbTable.merc_unidadpeso.writable = False
        _dbTable.figtransporte_cvetransporte.writable = False
        _dbTable.merc_numtotalmercancias.writable = False  # Número total de mercancías

        # Campos que todos los movimientos pueden editar
        # None

        # Se definen los valores por default
        # _dbTable.cfdi_id.default = dbRow_cfdi.id Ya que se crea al mismo tiempo que el CFDI, no se puede saber el id,
        #  sino hasta que se graba
        # _dbTable.versioncp.default = False
        _dbTable.transpinternac.default = TCFDI_COMPCARTAPORTE.TRANSPINTERNAC.NO
        _dbTable.entradasalidamerc.default = None
        _dbTable.viaentradasalida_id.default = None
        _dbTable.figtransporte_transporte_id.default = TTRANSPORTES.AUTO

        return _D_return

    @classmethod
    def CONFIGURACAMPOS_EDICION(
            cls,
            x_cfdi_compCartaPorte,
            ):
        """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

        @param x_cfdi_compCartaPorte:
        @type x_cfdi_compCartaPorte:
        @return:
        @rtype:
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_compCartaPorte, dbc01.tcfdi_compcartaporte)
        _dbRow_cfdi_compcartaporte = _D_results.dbRow

        _D_results = cls.PUEDE_EDITAR(
            x_cfdi_compCartaPorte = _dbRow_cfdi_compcartaporte
            )

        if _D_results.E_return == CLASS_e_RETURN.OK:
            # Si puede editar regresa un ok y el movimiento esta abierto

            # Se manda vacío el argumento para no agregar defaults a los campos
            _D_results = cls.CONFIGURACAMPOS_NUEVO(
                s_cfdi_compCartaPorte_id = None,
                x_cfdi = _dbRow_cfdi_compcartaporte.cfdi_id
                )
            _D_return.update(_D_results)

        else:
            # De lo contrario en cualquier otro caso, no puede editar nada de esta tabla
            pass

        return _D_return

    @classmethod
    def AL_VALIDAR(
            cls,
            O_form,
            b_validarParaFormaPrincipal = False
            ):
        """ Validar la información para poder grabar cfdi

        @param b_validarParaFormaPrincipal: Indica que se debe validar sin importar el id del maestro,
         ya que se validan e insertan al mismo tiempo
        @type b_validarParaFormaPrincipal:
        @param O_form:
        @type O_form:
        @return:
        @rtype:
        """
        _dbTable = dbc01.tcfdi_compcartaporte

        _D_camposAChecar = Storage()  # Define los campos que deben ser verificados

        # Dependiendo si es nuevo o editado, toma los valores a validar
        if not O_form.record_id:
            # Si esta creando el registro:

            _D_camposAChecar.cfdi_id = (
                O_form.vars.get("cfdi_id", None)
                or _dbTable.cfdi_id.default
                )

            if not b_validarParaFormaPrincipal:
                if _D_camposAChecar.cfdi_id:
                    _D_results = cls.PUEDE_CREAR(_D_camposAChecar.cfdi_id)
                    if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                        O_form.errors.id = _D_results.s_msgError
                    else:
                        pass
                else:
                    O_form.errors.id = "Problemas al crear el CFDI"
            else:
                pass

            _D_camposAChecar.transpinternac = (
                O_form.vars.get("transpinternac", _dbTable.transpinternac.default)
                )
            _D_camposAChecar.entradasalidamerc = (
                O_form.vars.get("entradasalidamerc", _dbTable.entradasalidamerc.default)
                )
            _D_camposAChecar.viaentradasalida_id = (
                O_form.vars.get("viaentradasalida_id", _dbTable.viaentradasalida_id.default)
                )

        else:
            _dbRow_cartaporte = None
            if b_validarParaFormaPrincipal:
                # Significa que record_id es el id del cfdi
                _dbRows_cartaPorte = dbc01(
                    dbc01.tcfdi_compcartaporte.cfdi_id == O_form.record_id
                    ).select(
                        dbc01.tcfdi_compcartaporte.ALL,
                        )
                if len(_dbRows_cartaPorte) > 1:
                    O_form.errors.id = "Se generó más de una carta porte para el CFDI, llame al administrador"
                elif len(_dbRows_cartaPorte) == 0:
                    O_form.errors.id = (
                        "No se generó la carta porte correctamemte, tendrá que eliminar el CFDI y crearlo nuevamente"
                        )
                else:
                    _dbRow_cartaporte = _dbRows_cartaPorte.first()
            else:
                _dbRow_cartaporte = dbc01.tcfdi_compcartaporte(O_form.record_id)

            if _dbRow_cartaporte:
                _D_camposAChecar.cfdi_id = (
                    O_form.vars.get("cfdi_id", None)
                    or _dbRow_cartaporte.cfdi_id
                    )

                _D_results = cls.PUEDE_EDITAR(
                    x_cfdi_compCartaPorte = _dbRow_cartaporte
                    )
                if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                    O_form.errors.id = _D_results.s_msgError
                else:
                    pass
            else:
                pass

            _D_camposAChecar.transpinternac = (
                O_form.vars.get("transpinternac", _dbRow_cartaporte.transpinternac)
                )
            _D_camposAChecar.entradasalidamerc = (
                O_form.vars.get("entradasalidamerc", _dbRow_cartaporte.entradasalidamerc)
                )
            _D_camposAChecar.viaentradasalida_id = (
                O_form.vars.get("viaentradasalida_id", _dbRow_cartaporte.viaentradasalida_id)
                )

        # Validaciones

        if _D_camposAChecar.transpinternac == TCFDI_COMPCARTAPORTE.TRANSPINTERNAC.SI:
            if not _D_camposAChecar.entradasalidamerc or not _D_camposAChecar.viaentradasalida_id:
                O_form.errors.transpinternac = (
                    "CP114: Si es internacional, debe especificarse si es entrada o salida y la vía"
                    )
            else:
                pass

            # TODOMejora verificar que conceptos tengan información aduanera CP116

        else:
            pass

        if not O_form.errors:
            # Si no hay cliente, el autocalculo debe especificar el RFC genérico
            pass
        else:
            pass

        return O_form

    @classmethod
    def AL_ACEPTAR(
            cls,
            O_form,
            **D_argsMapeoCalculoContabiliza
            ):
        """ Al aceptar el registro deben hacerse los cálculos

        Se hacen todos los cálculos y aceptación de los cambios en el producto

        @param O_form:
        @type O_form:
        @return:
        @rtype:
        """
        _ = D_argsMapeoCalculoContabiliza

        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTabla = dbc01.tcfdi_compcartaporte
        _dbRow_cartaporte = _dbTabla(O_form.record_id or O_form.vars.id)

        _D_results = cls.MAPEAR_CAMPOS(x_cfdi_compCartaPorte = _dbRow_cartaporte)
        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_results)

        else:
            _D_results = cls.CALCULAR_CAMPOS(x_cfdi_compCartaPorte = _dbRow_cartaporte)
            _D_return.update(_D_results)

        generaMensajeFlash(dbc01.tcfdi_compcartaporte, _D_return)

        return O_form

    @classmethod
    def ANTES_ELIMINAR(
            cls,
            L_errorsAction,
            dbRow
            ):
        """ Antes de eliminar un movimiento de inventario de este tipo

        @param dbRow:
        @type dbRow:
        @param L_errorsAction:
        @type L_errorsAction:
        @return: errores
        @rtype: lista de diccionarios {'msg':'...'}
        """
        _D_results = cls.PUEDE_ELIMINAR(dbRow)
        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            L_errorsAction.append({'msg': _D_results.s_msgError})
        else:
            pass

        return L_errorsAction

    @classmethod
    def MAPEAR_CAMPOS(
            cls,
            x_cfdi_compCartaPorte
            ):
        """ Remapea campos solamente cuando se graba el registro

        @param x_cfdi_compCartaPorte:
        @type x_cfdi_compCartaPorte:
        @return:
        @rtype:
        """

        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_compCartaPorte, dbc01.tcfdi_compcartaporte)
        _dbRow_cfdi_compcartaporte = _D_results.dbRow

        _D_results = TCFDI_COMPCARTAPORTE.PUEDE_EDITAR(
            x_cfdi_compCartaPorte = _dbRow_cfdi_compcartaporte
            )

        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_results)

        else:

            if _dbRow_cfdi_compcartaporte.figtransporte_transporte_id == TTRANSPORTES.AUTO:
                _dbRow_cfdi_compcartaporte.merc_pesobrutototal = 0
                _dbRow_cfdi_compcartaporte.merc_unidadpeso_id = 0
                _dbRow_cfdi_compcartaporte.merc_pesonetototal = 0
                _dbRow_cfdi_compcartaporte.merc_cargoportasacion = 0
                _dbRow_cfdi_compcartaporte.entradasalidamerc = None
                _dbRow_cfdi_compcartaporte.viaentradasalida_id = None

            else:
                pass

            # Si puede editar regresa un ok y el movimiento esta abierto
            _dbRow_cfdi_compcartaporte.viaentradasalida = db01.ttransportes(
                _dbRow_cfdi_compcartaporte.viaentradasalida_id
                ).c_cvetransporte if _dbRow_cfdi_compcartaporte.viaentradasalida_id else ""

            _dbRow_cfdi_compcartaporte.merc_unidadpeso = db01.tunidadespeso(
                _dbRow_cfdi_compcartaporte.merc_unidadpeso_id
                ).c_claveunidadpeso if _dbRow_cfdi_compcartaporte.merc_unidadpeso_id else ""

            _dbRow_cfdi_compcartaporte.figtransporte_cvetransporte = db01.ttransportes(
                _dbRow_cfdi_compcartaporte.figtransporte_transporte_id
                ).c_cvetransporte if _dbRow_cfdi_compcartaporte.figtransporte_transporte_id else ""

            _dbRow_cfdi_compcartaporte.update_record()

        return _D_return

    @classmethod
    def CALCULAR_CAMPOS(
            cls,
            x_cfdi_compCartaPorte
            ):
        """ Recalcula campos que tienen impacto cuando otros registros de otras tablas cambian
        Esta operación así mismo detecta y agrega las inconsistencias

        @param x_cfdi_compCartaPorte:
        @type x_cfdi_compCartaPorte:
        @return:
        @rtype:
        """

        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_compCartaPorte, dbc01.tcfdi_compcartaporte)
        _dbRow_cfdi_compcartaporte = _D_results.dbRow

        _n_num_mercancias = _dbRow_cfdi_compcartaporte.tcfdi_compcartaporte_mercancias.count()

        _dbRows_ubicaciones = _dbRow_cfdi_compcartaporte.tcfdi_compcartaporte_ubicaciones.select()
        _f_totalDistRecorrida = 0
        for _dbRow in _dbRows_ubicaciones:
            _f_totalDistRecorrida += _dbRow.distanciarecorrida or 0

        if (
                (_n_num_mercancias != _dbRow_cfdi_compcartaporte.merc_numtotalmercancias)
                or (_f_totalDistRecorrida != _dbRow_cfdi_compcartaporte.totaldistrec)
                ):
            _dbRow_cfdi_compcartaporte.merc_numtotalmercancias = _n_num_mercancias
            _dbRow_cfdi_compcartaporte.totaldistrec = _f_totalDistRecorrida
            _dbRow_cfdi_compcartaporte.update_record()
        else:
            pass

        return _D_return

    pass


dbc01.define_table(
    'tcfdi_compcartaporte',
    Field(
        'cfdi_id', dbc01.tcfdis, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'CFDI', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'versioncp', 'string', length = 50, default = '1.0',
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Vesion C.P.', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'transpinternac', 'string', length = 50, default = TCFDI_COMPCARTAPORTE.TRANSPINTERNAC.NO,
        required = False, requires = IS_IN_SET(
            TCFDI_COMPCARTAPORTE.TRANSPINTERNAC.GET_DICT(), zero = 0, error_message = 'Selecciona'
            ),
        notnull = False, unique = False,
        widget = stv_widget_combobox, label = 'Transporte Internacional', comment = '',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TCFDI_COMPCARTAPORTE.TRANSPINTERNAC.GET_DICT())
        ),
    Field(
        'entradasalidamerc', 'string', length = 50, default = None,
        required = False, requires = IS_NULL_OR(
            IS_IN_SET(TCFDI_COMPCARTAPORTE.ENTRADASALIDAMERC.GET_DICT(), zero = 0, error_message = 'Selecciona')
            ),
        notnull = False, unique = False,
        widget = stv_widget_combobox, label = 'Entrada o Salida', comment = '',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TCFDI_COMPCARTAPORTE.ENTRADASALIDAMERC.GET_DICT())
        ),
    Field(
        'viaentradasalida', 'string', length = 10, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Vía Entrada/Salida', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'viaentradasalida_id', 'integer', default = None,
        required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'ttransportes.id', db01.ttransportes._format)),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Vía Entrada/Salida', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tcfdi_compcartaporte.viaentradasalida_id)
        ),
    Field(
        'totaldistrec', 'decimal(14,4)', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Total Distancia Recorrida (km)',
        comment = 'Distancia recorrida en kilómetros',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_number(
            v, r, dbField = dbc01.tcfdi_compcartaporte.totaldistrec
            )
        ),

    # Atributos del nodo Ubicaciones, no tiene atributos por lo pronto

    # Atributos del nodo Mercancias
    Field(
        'merc_pesobrutototal', 'decimal(14,4)', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputFloat, label = 'Peso Bruto Total', comment = None,
        writable = False, readable = True,
        represent = stv_represent_float
        ),
    Field(
        'merc_unidadpeso', 'string', length = 100, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Unidad Peso', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'merc_unidadpeso_id', 'integer', default = None,
        required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tunidadespeso.id', db01.tunidadespeso._format)),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1),
        label = 'Unidad Peso', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tcfdi_compcartaporte.merc_unidadpeso_id
            )
        ),
    Field(
        'merc_pesonetototal', 'decimal(14,4)', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputFloat, label = 'Peso Neto Total', comment = None,
        writable = False, readable = True,
        represent = stv_represent_float
        ),
    Field(
        'merc_numtotalmercancias', 'integer', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputInteger, label = 'Num. Total Mercancias', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_number(
            v, r, dbField = dbc01.tcfdi_compcartaporte.merc_numtotalmercancias
            )
        ),
    Field(
        'merc_cargoportasacion', 'decimal(14,4)', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputMoney, label = 'Cargo por Tasación', comment = None,
        writable = False, readable = True,
        represent = stv_represent_money_ifnotzero
        ),

    # Atributos del nodo FiguraTransporte
    Field(
        'figtransporte_cvetransporte', 'string', length = 5, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Cve. Transporte', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'figtransporte_transporte_id', 'integer', default = None,
        required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'ttransportes.id', db01.ttransportes._format)),
        notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1),
        label = 'Cve. Transporte', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tcfdi_compcartaporte.figtransporte_transporte_id
            )
        ),

    tSignature_dbc01,
    format = '%(cfdi_id)s',
    singular = 'Complemento Carta Porte',
    plural = 'Complementos Cartas Porte',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )


class TCFDI_COMPCARTAPORTE_UBICACIONES:
    """

    Es una relación de 1 (Ubicaciones en tabla CartaPorte) a [1,Inf].
    Se incluye el nodo Origen por relación 1 a [0,1].
    Se incluye el nodo Destino por relación 1 a [0,1].
    Se incluye el nodo Domicilio por relación 1 a [0,1].

    """

    @staticmethod
    def DBTABLA(s_id = None):
        if s_id is None:
            return dbc01.tcfdi_compcartaporte_ubicaciones
        else:
            return dbc01.tcfdi_compcartaporte_ubicaciones(s_id)

    class NAVEGACIONTRAFICO:
        """ Se definen los valores posibles del campo, en base a lo requerido por el SAT """
        NO_DEFINIDO = ''
        ALTURA = 'Altura'
        CABOTAJE = 'Cabotaje'
        _D_dict = None

        @classmethod
        def GET_DICT(cls):
            if not cls._D_dict:
                cls._D_dict = {
                    cls.NO_DEFINIDO: 'No Definido',
                    cls.ALTURA     : 'Altura',
                    cls.CABOTAJE   : 'Cabotaje',
                    }
            else:
                pass
            return cls._D_dict

    @classmethod
    def PUEDE_CREAR(
            cls,
            x_cfdi_compCartaPorte
            ):
        """ Determina si puede o no crear un CFDI de traslado.

        Actualmente no hay condición requerida.
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _D_results = TCFDI_COMPCARTAPORTE.PUEDE_EDITAR(
            x_cfdi_compCartaPorte = x_cfdi_compCartaPorte
            )
        _D_return.update(_D_results)

        return _D_return

    @classmethod
    def PUEDE_EDITAR(
            cls,
            x_cfdi_compcartaporte_ubicacion,
            x_cfdi_compCartaPorte
            ):
        """ Define si es posible editar el CFDI de traslado.
        @param x_cfdi_compcartaporte_ubicacion:
        @type x_cfdi_compcartaporte_ubicacion:
        @param x_cfdi_compCartaPorte:
        @type x_cfdi_compCartaPorte:

        """
        _ = x_cfdi_compcartaporte_ubicacion
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _D_results = TCFDI_COMPCARTAPORTE.PUEDE_EDITAR(
            x_cfdi_compCartaPorte = x_cfdi_compCartaPorte
            )
        _D_return.update(_D_results)

        return _D_return

    @classmethod
    def PUEDE_ELIMINAR(
            cls,
            x_cfdi_compcartaporte_ubicacion,
            ):
        """ Determina si puede o no eliminar un CFDI de traspaso

        Misma condición que puede_editar

        """

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(
            x_cfdi_compcartaporte_ubicacion,
            dbc01.tcfdi_compcartaporte_ubicaciones
            )
        _dbRow_ubicacion = _D_results.dbRow

        _dbRow_cartaporte = dbc01.tcfdi_compcartaporte(_dbRow_ubicacion.cfdi_compcartaporte_id)

        return cls.PUEDE_EDITAR(
            x_cfdi_compcartaporte_ubicacion = _dbRow_ubicacion,
            x_cfdi_compCartaPorte = _dbRow_cartaporte
            )

    @classmethod
    def CONFIGURACAMPOS_NUEVO(
            cls,
            s_cfdi_compCartaPorte_ubicacion_id,
            x_cfdi_compCartaPorte
            ):
        """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

        @param x_cfdi_compCartaPorte:
        @type x_cfdi_compCartaPorte:
        @param s_cfdi_compCartaPorte_ubicacion_id:
        @type s_cfdi_compCartaPorte_ubicacion_id:
        @return:
        @rtype:
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTable = dbc01.tcfdi_compcartaporte_ubicaciones

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_compCartaPorte, dbc01.tcfdi_compcartaporte)
        _dbRow_cfdi_cartaPorte = _D_results.dbRow
        _dbRow_cfdi = dbc01.tcfdis(_dbRow_cfdi_cartaPorte.cfdi_id)

        _D_results = cls.PUEDE_CREAR(
            x_cfdi_compCartaPorte = _dbRow_cfdi_cartaPorte
            )

        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_results)

        elif s_cfdi_compCartaPorte_ubicacion_id:
            _dbRow_base = _dbTable(s_cfdi_compCartaPorte_ubicacion_id)

            # _dbTable.cfdi_compcartaporte_id.default = _dbRow_base.cfdi_compcartaporte_id
            # _dbTable.tipoestacion.default = _dbRow_base.tipoestacion
            # _dbTable.tipoestacion_id.default = _dbRow_base.tipoestacion_id
            _dbTable.distanciarecorrida.default = _dbRow_base.distanciarecorrida
            _dbTable.origen_idorigen.default = _dbRow_base.origen_idorigen
            _dbTable.origen_rfcremitente.default = _dbRow_base.origen_rfcremitente
            _dbTable.origen_nombreremitente.default = _dbRow_base.origen_nombreremitente
            _dbTable.origen_numregidtrib.default = _dbRow_base.origen_numregidtrib
            # _dbTable.origen_recidenciafiscal.default = _dbRow_base.origen_recidenciafiscal
            _dbTable.origen_pais_id.default = _dbRow_base.destino_pais_id
            # _dbTable.origen_numestacion.default = _dbRow_base.origen_numestacion
            # _dbTable.origen_estacion_id.default = _dbRow_base.origen_estacion_id
            # _dbTable.origen_nombreestacion.default = _dbRow_base.origen_nombreestacion
            # _dbTable.origen_navegaciontrafico.default = _dbRow_base.origen_navegaciontrafico
            # _dbTable.origen_fechahorasalida_str.default = _dbRow_base.origen_fechahorasalida_str
            _dbTable.origen_fechahorasalida.default = _dbRow_base.origen_fechahorasalida
            _dbTable.destino_iddestino.default = _dbRow_base.destino_iddestino
            _dbTable.destino_rfcdestinatario.default = _dbRow_base.destino_rfcdestinatario
            _dbTable.destino_nombredestinatario.default = _dbRow_base.destino_nombredestinatario
            _dbTable.destino_numregidtrib.default = _dbRow_base.destino_numregidtrib
            # _dbTable.destino_recidenciafiscal.default = _dbRow_base.destino_recidenciafiscal
            _dbTable.destino_pais_id.default = _dbRow_base.destino_pais_id
            # _dbTable.destino_numestacion.default = _dbRow_base.destino_numestacion
            # _dbTable.destino_estacion_id.default = _dbRow_base.destino_estacion_id
            # _dbTable.destino_nombreestacion.default = _dbRow_base.destino_nombreestacion
            # _dbTable.destino_navegaciontrafico.default = _dbRow_base.destino_navegaciontrafico
            # _dbTable.destino_fechahoraprogllegada_str.default = _dbRow_base.destino_fechahoraprogllegada_str
            _dbTable.destino_fechahoraprogllegada.default = _dbRow_base.destino_fechahoraprogllegada
            _dbTable.dom_emp_pla_suc_almacen_id.default = _dbRow_base.dom_emp_pla_suc_almacen_id
            _dbTable.dom_usardireccioncliente.default = _dbRow_base.dom_usardireccioncliente
            _dbTable.dom_pais_id.default = _dbRow_base.dom_pais_id
            # _dbTable.dom_pais.default = _dbRow_base.dom_pais
            _dbTable.dom_pais_estado_id.default = _dbRow_base.dom_pais_estado_id
            # _dbTable.dom_estado.default = _dbRow_base.dom_estado
            _dbTable.dom_municipio.default = _dbRow_base.dom_municipio
            _dbTable.dom_referencia.default = _dbRow_base.dom_referencia
            _dbTable.dom_localidad.default = _dbRow_base.dom_localidad
            _dbTable.dom_colonia.default = _dbRow_base.dom_colonia
            _dbTable.dom_calle.default = _dbRow_base.dom_calle
            _dbTable.dom_numexterior.default = _dbRow_base.dom_numexterior
            _dbTable.dom_numinterior.default = _dbRow_base.dom_numinterior
            _dbTable.dom_codigopostal.default = _dbRow_base.dom_codigopostal

        else:
            pass

        _dbTable.cfdi_compcartaporte_id.writable = False
        _dbTable.tipoestacion_id.writable = False
        _dbTable.distanciarecorrida.writable = True
        _dbTable.origen_idorigen.writable = True
        _dbTable.origen_rfcremitente.writable = True
        _dbTable.origen_nombreremitente.writable = True
        _dbTable.origen_numregidtrib.writable = True
        _dbTable.origen_pais_id.writable = True
        _dbTable.origen_estacion_id.writable = False
        _dbTable.origen_nombreestacion.writable = False
        _dbTable.origen_navegaciontrafico.writable = False
        _dbTable.origen_fechahorasalida.writable = True
        _dbTable.destino_iddestino.writable = True
        _dbTable.destino_rfcdestinatario.writable = True
        _dbTable.destino_nombredestinatario.writable = True
        _dbTable.destino_numregidtrib.writable = True
        _dbTable.destino_pais_id.writable = True
        _dbTable.destino_estacion_id.writable = False
        _dbTable.destino_nombreestacion.writable = False
        _dbTable.destino_navegaciontrafico.writable = False
        _dbTable.destino_fechahoraprogllegada.writable = True
        _dbTable.dom_emp_pla_suc_almacen_id.writable = True
        _dbTable.dom_usardireccioncliente.writable = bool(_dbRow_cfdi.receptorcliente_id)
        _dbTable.dom_pais_id.writable = True
        _dbTable.dom_pais_estado_id.writable = True
        _dbTable.dom_municipio.writable = True
        _dbTable.dom_referencia.writable = True
        _dbTable.dom_localidad.writable = True
        _dbTable.dom_colonia.writable = True
        _dbTable.dom_calle.writable = True
        _dbTable.dom_numexterior.writable = True
        _dbTable.dom_numinterior.writable = True
        _dbTable.dom_codigopostal.writable = True

        # Campos que no deben ser editados, solo de forma autocalculada
        _dbTable.tipoestacion.writable = False
        _dbTable.origen_recidenciafiscal.writable = False
        _dbTable.origen_numestacion.writable = False
        _dbTable.origen_fechahorasalida_str.writable = False
        _dbTable.destino_recidenciafiscal.writable = False
        _dbTable.destino_numestacion.writable = False
        _dbTable.destino_fechahoraprogllegada_str.writable = False
        _dbTable.dom_pais.writable = False
        _dbTable.dom_estado.writable = False

        # Campos que todos los movimientos pueden editar
        _dbTable.notas.writable = False

        # Se definen los valores por default

        _dbTable.cfdi_compcartaporte_id.default = _dbRow_cfdi_cartaPorte.id
        # _dbTable.distanciarecorrida.default = 0

        _n_origenes = dbc01(
            (_dbTable.cfdi_compcartaporte_id == _dbRow_cfdi_cartaPorte.id)
            & (_dbTable.origen_idorigen != "")
            ).count() + 1
        _dbTable.origen_idorigen.default = "O" + str(_n_origenes)
        # _dbTable.origen_rfcremitente.default = ""
        # _dbTable.origen_nombreremitente.default = ""
        # _dbTable.origen_numregidtrib.default = ""
        _dbTable.origen_pais_id.default = TPAISES.MEX
        # _dbTable.origen_fechahorasalida.default = _dbRow_cfdi.fecha + datetime.timedelta(hours = 6)

        _n_destinos = dbc01(
            (_dbTable.cfdi_compcartaporte_id == _dbRow_cfdi_cartaPorte.id)
            & (_dbTable.destino_iddestino != "")
            ).count() + 1
        _dbTable.destino_iddestino.default = "D" + str(_n_destinos)
        # _dbTable.destino_rfcdestinatario.default = ""
        # _dbTable.destino_nombredestinatario.default = ""
        # _dbTable.destino_numregidtrib.default = ""
        _dbTable.destino_pais_id.default = TPAISES.MEX
        # _dbTable.destino_fechahoraprogllegada.default = _dbRow_cfdi.fecha + datetime.timedelta(hours = 12)
        # _dbTable.dom_emp_pla_suc_almacen_id.default = None
        # _dbTable.dom_usardireccioncliente.default = False
        _dbTable.dom_pais_id.default = TPAISES.MEX
        # _dbTable.dom_pais_estado_id.default = TPAIS_ESTADOS.SIN
        # _dbTable.dom_referencia.default = ""
        # _dbTable.dom_localidad.default = ""
        # _dbTable.dom_colonia.default = ""
        # _dbTable.dom_calle.default = ""
        # _dbTable.dom_numexterior.default = ""
        # _dbTable.dom_numinterior.default = ""
        # _dbTable.dom_codigopostal.default = ""

        # TODOMejora soportarlo para transportes diferentes a auto
        # _dbTable.tipoestacion_id.default = None
        # _dbTable.origen_estacion_id.default = None
        # _dbTable.origen_nombreestacion.default = None
        # _dbTable.origen_navegaciontrafico.default = ''
        # _dbTable.destino_estacion_id.default = None
        # _dbTable.destino_nombreestacion.default = None
        # _dbTable.destino_navegaciontrafico.default = ''

        return _D_return

    @classmethod
    def CONFIGURACAMPOS_EDICION(
            cls,
            x_cfdi_compcartaporte_ubicacion,
            x_cfdi_compCartaPorte
            ):
        """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

        @param x_cfdi_compcartaporte_ubicacion:
        @type x_cfdi_compcartaporte_ubicacion:
        @param x_cfdi_compCartaPorte:
        @type x_cfdi_compCartaPorte:
        @return:
        @rtype:
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_compCartaPorte, dbc01.tcfdi_compcartaporte)
        _dbRow_cfdi_cartaPorte = _D_results.dbRow

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(
            x_cfdi_compcartaporte_ubicacion, dbc01.tcfdi_compcartaporte_ubicaciones
            )
        _dbRow_ubicacion = _D_results.dbRow

        _D_results = cls.PUEDE_EDITAR(
            x_cfdi_compcartaporte_ubicacion = _dbRow_ubicacion,
            x_cfdi_compCartaPorte = _dbRow_cfdi_cartaPorte
            )

        if _D_results.E_return == CLASS_e_RETURN.OK:
            # Si puede editar regresa un ok y el movimiento esta abierto

            # Se manda vacío el argumento para no agregar defaults a los campos
            _D_results = cls.CONFIGURACAMPOS_NUEVO(
                s_cfdi_compCartaPorte_ubicacion_id = None,
                x_cfdi_compCartaPorte = _dbRow_cfdi_cartaPorte
                )
            _D_return.update(_D_results)

        else:
            # De lo contrario en cualquier otro caso, no puede editar nada de esta tabla
            pass

        return _D_return

    @classmethod
    def AL_VALIDAR(
            cls,
            O_form,
            x_cfdi_compCartaPorte
            ):
        """ Validar la información para poder grabar cfdi

        @param x_cfdi_compCartaPorte:
        @type x_cfdi_compCartaPorte:
        @param O_form:
        @type O_form:
        @return:
        @rtype:
        """

        _dbTable = dbc01.tcfdi_compcartaporte_ubicaciones

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_compCartaPorte, dbc01.tcfdi_compcartaporte)
        _dbRow_cfdi_cartaPorte = _D_results.dbRow

        _D_camposAChecar = Storage()  # Define los campos que deben ser verificados

        # Dependiendo si es nuevo o editado, toma los valores a validar
        if not O_form.record_id:
            # Si esta creando el registro:

            _D_results = cls.PUEDE_CREAR(
                x_cfdi_compCartaPorte = _dbRow_cfdi_cartaPorte
                )
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                O_form.errors.id = _D_results.s_msgError
            else:
                pass

            _D_camposAChecar.origen_idorigen = \
                O_form.vars.get("origen_idorigen", _dbTable.origen_idorigen.default)

            _D_camposAChecar.origen_rfcremitente = \
                O_form.vars.get("origen_rfcremitente", _dbTable.origen_rfcremitente.default)

            _D_camposAChecar.origen_nombreremitente = \
                O_form.vars.get("origen_nombreremitente", _dbTable.origen_nombreremitente.default)

            _D_camposAChecar.origen_fechahorasalida = \
                O_form.vars.get("origen_fechahorasalida", _dbTable.origen_fechahorasalida.default)

            _D_camposAChecar.destino_iddestino = \
                O_form.vars.get("destino_iddestino", _dbTable.destino_iddestino.default)

            _D_camposAChecar.destino_rfcdestinatario = \
                O_form.vars.get("destino_rfcdestinatario", _dbTable.destino_rfcdestinatario.default)

            _D_camposAChecar.destino_nombredestinatario = \
                O_form.vars.get("destino_nombredestinatario", _dbTable.destino_nombredestinatario.default)

            _D_camposAChecar.destino_fechahoraprogllegada = \
                O_form.vars.get("destino_fechahoraprogllegada", _dbTable.destino_fechahoraprogllegada.default)

            _D_camposAChecar.dom_emp_pla_suc_almacen_id = \
                O_form.vars.get("dom_emp_pla_suc_almacen_id", _dbTable.dom_emp_pla_suc_almacen_id.default)

            _D_camposAChecar.dom_usardireccioncliente = \
                O_form.vars.get("dom_usardireccioncliente", _dbTable.dom_usardireccioncliente.default)

        else:

            _dbRow_ubicacion = _dbTable(O_form.record_id)

            _D_results = cls.PUEDE_EDITAR(
                x_cfdi_compcartaporte_ubicacion = _dbRow_ubicacion,
                x_cfdi_compCartaPorte = _dbRow_cfdi_cartaPorte
                )
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                O_form.errors.id = _D_results.s_msgError
            else:
                pass

            _D_camposAChecar.origen_idorigen = \
                O_form.vars.get("origen_idorigen", _dbRow_ubicacion.origen_idorigen)

            _D_camposAChecar.origen_rfcremitente = \
                O_form.vars.get("origen_rfcremitente", _dbRow_ubicacion.origen_rfcremitente)

            _D_camposAChecar.origen_nombreremitente = \
                O_form.vars.get("origen_nombreremitente", _dbRow_ubicacion.origen_nombreremitente)

            _D_camposAChecar.origen_fechahorasalida = \
                O_form.vars.get("origen_fechahorasalida", _dbRow_ubicacion.origen_fechahorasalida)

            _D_camposAChecar.destino_iddestino = \
                O_form.vars.get("destino_iddestino", _dbRow_ubicacion.destino_iddestino)

            _D_camposAChecar.destino_rfcdestinatario = \
                O_form.vars.get("destino_rfcdestinatario", _dbRow_ubicacion.destino_rfcdestinatario)

            _D_camposAChecar.destino_nombredestinatario = \
                O_form.vars.get("destino_nombredestinatario", _dbRow_ubicacion.destino_nombredestinatario)

            _D_camposAChecar.destino_fechahoraprogllegada = \
                O_form.vars.get("destino_fechahoraprogllegada", _dbRow_ubicacion.destino_fechahoraprogllegada)

            _D_camposAChecar.dom_emp_pla_suc_almacen_id = \
                O_form.vars.get("dom_emp_pla_suc_almacen_id", _dbRow_ubicacion.dom_emp_pla_suc_almacen_id)

            _D_camposAChecar.dom_usardireccioncliente = \
                O_form.vars.get("dom_usardireccioncliente", _dbRow_ubicacion.dom_usardireccioncliente)

        # Validaciones

        if _D_camposAChecar.dom_emp_pla_suc_almacen_id and _D_camposAChecar.dom_usardireccioncliente:
            O_form.errors.dom_emp_pla_suc_almacen_id = (
                "No puede usarse domicilio de sucursal y cliente en el mismo regustro"
                )
        else:
            _dbRow_cfdi = dbc01.tcfdis(_dbRow_cfdi_cartaPorte.cfdi_id)
            if _D_camposAChecar.dom_usardireccioncliente and not _dbRow_cfdi.receptorcliente_id:
                O_form.errors.dom_usardireccioncliente = "Cliente no definido en CFDI para copiar domicilio"
            else:
                pass

        if request.vars.get("esorigen", ""):

            if not O_form.vars.origen_idorigen:
                O_form.errors.origen_idorigen = "Origen ID es necesario si se define información origen"

            elif dbc01(
                    (_dbTable.cfdi_compcartaporte_id == _dbRow_cfdi_cartaPorte.id)
                    & (_dbTable.origen_idorigen == O_form.vars.origen_idorigen)
                    & (_dbTable.id != O_form.record_id)
                    ).count() > 0:
                O_form.errors.origen_idorigen = "Origen ID no puede duplicarse"

            else:
                pass

        else:
            O_form.vars.origen_idorigen = ""

        if request.vars.get("esdestino", ""):

            if not O_form.vars.destino_iddestino:
                O_form.errors.destino_iddestino = "Destino ID es necesario si se define información destino"

            elif dbc01(
                    (_dbTable.cfdi_compcartaporte_id == _dbRow_cfdi_cartaPorte.id)
                    & (_dbTable.destino_iddestino == O_form.vars.destino_iddestino)
                    & (_dbTable.id != O_form.record_id)
                    ).count() > 0:
                O_form.errors.destino_iddestino = "Destino ID no puede duplicarse"

            else:
                pass

        else:
            O_form.vars.destino_iddestino = ""

        if request.vars.get("esorigen", "") \
                or request.vars.get("esdestino", ""):
            pass
        else:
            O_form.errors.id = "Requiere especificar si es origen, destino o ambos"

        if not O_form.errors:
            # Si no hay cliente, el autocalculo debe especificar el RFC genérico
            pass
        else:
            pass

        return O_form

    @classmethod
    def AL_ACEPTAR(
            cls,
            O_form,
            **D_argsMapeoCalculoContabiliza
            ):
        """ Al aceptar el registro deben hacerse los cálculos

        Se hacen todos los cálculos y aceptación de los cambios en el producto

        @param D_argsMapeoCalculoContabiliza:
        @type D_argsMapeoCalculoContabiliza:
        @param O_form:
        @type O_form:
        @return:
        @rtype:
        """
        _ = D_argsMapeoCalculoContabiliza

        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTabla = dbc01.tcfdi_compcartaporte_ubicaciones

        _dbRow_ubicacion = _dbTabla(O_form.record_id or O_form.vars.id)

        _D_results = cls.MAPEAR_CAMPOS(_dbRow_ubicacion)

        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_results)

        else:

            _D_results = TCFDI_COMPCARTAPORTE.CALCULAR_CAMPOS(
                x_cfdi_compCartaPorte = _dbRow_ubicacion.cfdi_compcartaporte_id
                )

            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.update(_D_results)
            else:
                pass

        generaMensajeFlash(cls.DBTABLA(), _D_return)

        return O_form

    @classmethod
    def ANTES_ELIMINAR(
            cls,
            L_errorsAction,
            dbRow
            ):
        """ Antes de eliminar un movimiento de inventario de este tipo

        @param dbRow:
        @type dbRow:
        @param L_errorsAction:
        @type L_errorsAction:
        @return: errores
        @rtype: lista de diccionarios {'msg':'...'}
        """
        _D_results = cls.PUEDE_ELIMINAR(
            x_cfdi_compcartaporte_ubicacion = dbRow
            )
        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            L_errorsAction.append({'msg': _D_results.s_msgError})
        else:
            pass

        return L_errorsAction

    @classmethod
    def MAPEAR_CAMPOS(
            cls,
            x_cfdi_compcartaporte_ubicacion
            ):
        """ Remapea los campos, sólo usarlo al editar el registro

        @param x_cfdi_compcartaporte_ubicacion:
        @type x_cfdi_compcartaporte_ubicacion:
        @return:
        @rtype:
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(
            x_cfdi_compcartaporte_ubicacion,
            dbc01.tcfdi_compcartaporte_ubicaciones
            )
        _dbRow_ubicacion = _D_results.dbRow

        if not _dbRow_ubicacion.origen_idorigen:
            _dbRow_ubicacion.origen_rfcremitente = ""
            _dbRow_ubicacion.origen_nombreremitente = ""
            _dbRow_ubicacion.origen_numregidtrib = ""
            _dbRow_ubicacion.origen_pais_id = None
            _dbRow_ubicacion.origen_fechahorasalida = None
        else:
            pass

        if _dbRow_ubicacion.origen_idorigen and not _dbRow_ubicacion.destino_iddestino:
            _dbRow_ubicacion.destino_rfcdestinatario = ""
            _dbRow_ubicacion.destino_nombredestinatario = ""
            _dbRow_ubicacion.destino_numregidtrib = ""
            _dbRow_ubicacion.destino_pais_id = None
            _dbRow_ubicacion.destino_fechahoraprogllegada = None
        else:
            pass

        if _dbRow_ubicacion.dom_emp_pla_suc_almacen_id:
            _dbRow_almacen = dbc01.tempresa_plaza_sucursal_almacenes(_dbRow_ubicacion.dom_emp_pla_suc_almacen_id)
            _dbRow_sucursal = dbc01.tempresa_plaza_sucursales(_dbRow_almacen.empresa_plaza_sucursal_id)

            _dbRow_ubicacion.dom_pais_id = _dbRow_sucursal.pais_id
            _dbRow_ubicacion.dom_pais_estado_id = _dbRow_sucursal.pais_estado_id
            _dbRow_ubicacion.dom_municipio = db01.tpais_estado_municipios(
                _dbRow_sucursal.pais_estado_municipio_id
                ).descripcion if _dbRow_sucursal.pais_estado_municipio_id else ""
            _dbRow_ubicacion.dom_localidad = _dbRow_sucursal.ciudad
            _dbRow_ubicacion.dom_colonia = _dbRow_sucursal.colonia
            _dbRow_ubicacion.dom_calle = _dbRow_sucursal.calle
            _dbRow_ubicacion.dom_numexterior = _dbRow_sucursal.numexterior
            _dbRow_ubicacion.dom_numinterior = _dbRow_sucursal.numinterior
            _dbRow_ubicacion.dom_codigopostal = _dbRow_sucursal.codigopostal

        elif _dbRow_ubicacion.dom_usardireccioncliente:
            _dbRows_domCliente = dbc01(
                (dbc01.tcfdi_compcartaporte.id == _dbRow_ubicacion.cfdi_compcartaporte_id)
                & (dbc01.tcfdis.id == dbc01.tcfdi_compcartaporte.cfdi_id)
                & (dbc01.tempresa_cliente_domicilios.empresa_cliente_id == dbc01.tcfdis.receptorcliente_id)
                ).select(
                    dbc01.tempresa_cliente_domicilios.ALL,
                    orderby = dbc01.tempresa_cliente_domicilios.orden,
                    limitby = [0, 1]
                    )

            if _dbRows_domCliente:
                _dbRow_domCliente = _dbRows_domCliente.first()
                _dbRow_ubicacion.dom_pais_id = _dbRow_domCliente.pais_id
                _dbRow_ubicacion.dom_pais_estado_id = _dbRow_domCliente.pais_estado_id
                _dbRow_ubicacion.dom_municipio = db01.tpais_estado_municipios(
                    _dbRow_domCliente.pais_estado_municipio_id
                    ).descripcion if _dbRow_domCliente.pais_estado_municipio_id else ""
                _dbRow_ubicacion.dom_localidad = _dbRow_domCliente.ciudad
                _dbRow_ubicacion.dom_colonia = _dbRow_domCliente.colonia
                _dbRow_ubicacion.dom_calle = _dbRow_domCliente.calle
                _dbRow_ubicacion.dom_numexterior = _dbRow_domCliente.numexterior
                _dbRow_ubicacion.dom_numinterior = _dbRow_domCliente.numinterior
                _dbRow_ubicacion.dom_codigopostal = _dbRow_domCliente.codigopostal
            else:
                _D_return.E_return = CLASS_e_RETURN.OK_WARNING
                _D_return.s_msgError = "Cliente no tiene domicilio definido"
                _dbRow_ubicacion.dom_pais_id = None
                _dbRow_ubicacion.dom_pais_estado_id = None
                _dbRow_ubicacion.dom_municipio = ""
                _dbRow_ubicacion.dom_localidad = ""
                _dbRow_ubicacion.dom_colonia = ""
                _dbRow_ubicacion.dom_calle = ""
                _dbRow_ubicacion.dom_numexterior = ""
                _dbRow_ubicacion.dom_numinterior = ""
                _dbRow_ubicacion.dom_codigopostal = ""

        else:
            pass

        _dbRow_ubicacion.tipoestacion = db01.ttransporte_tiposestaciones(
            _dbRow_ubicacion.tipoestacion_id
            ).c_tipoestacion if _dbRow_ubicacion.tipoestacion_id else ""

        _dbRow_ubicacion.origen_recidenciafiscal = db01.tpaises(
            _dbRow_ubicacion.origen_pais_id
            ).c_pais if _dbRow_ubicacion.origen_pais_id else ""
        _dbRow_ubicacion.origen_numestacion = db01.ttransporte_estaciones(
            _dbRow_ubicacion.origen_estacion_id
            ).c_estacion if _dbRow_ubicacion.origen_estacion_id else ""

        _dbRow_ubicacion.origen_fechahorasalida_str = (
            _dbRow_ubicacion.origen_fechahorasalida.strftime('%Y-%m-%dT%H:%M:%S')
            ) if _dbRow_ubicacion.origen_fechahorasalida else ""

        _dbRow_ubicacion.destino_recidenciafiscal = db01.tpaises(
            _dbRow_ubicacion.destino_pais_id
            ).c_pais if _dbRow_ubicacion.destino_pais_id else ""
        _dbRow_ubicacion.destino_numestacion = db01.ttransporte_estaciones(
            _dbRow_ubicacion.destino_estacion_id
            ).c_estacion if _dbRow_ubicacion.destino_estacion_id else ""

        _dbRow_ubicacion.destino_fechahoraprogllegada_str = (
            _dbRow_ubicacion.destino_fechahoraprogllegada.strftime('%Y-%m-%dT%H:%M:%S')
            ) if _dbRow_ubicacion.destino_fechahoraprogllegada else ""

        _dbRow_ubicacion.dom_pais = db01.tpaises(
            _dbRow_ubicacion.dom_pais_id
            ).c_pais if _dbRow_ubicacion.dom_pais_id else ""
        _dbRow_ubicacion.dom_estado = db01.tpais_estados(
            _dbRow_ubicacion.dom_pais_estado_id
            ).c_estado if _dbRow_ubicacion.dom_pais_estado_id else ""

        _dbRow_ubicacion.update_record()

        return _D_return

    pass


dbc01.define_table(
    'tcfdi_compcartaporte_ubicaciones',
    Field(
        'cfdi_compcartaporte_id', dbc01.tcfdi_compcartaporte, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'CFDI Carta Porte', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'tipoestacion', 'string', length = 10, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Tipo Estación', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'tipoestacion_id', 'integer', default = None,
        required = False, requires = IS_NULL_OR(
            IS_IN_DB(db01, 'ttransporte_tiposestaciones.id', db01.ttransporte_tiposestaciones._format)
            ),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Tipo Estación', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tcfdi_compcartaporte_ubicaciones.tipoestacion_id
            )
        ),
    Field(
        'distanciarecorrida', 'decimal(14,4)', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Distancia Recorrida',
        comment = 'Distancia recorrida en kilómetros',
        writable = False, readable = True,
        represent = stv_represent_number_ifnotzero
        ),

    # Atributos del nodo Origen
    Field(
        'origen_idorigen', 'string', length = 10, default = "",
        required = False,
        # requires = IS_NULL_OR(IS_MATCH'^OR[0-9]{6}$', error_message='ID origen no es correcto'),
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'ID Origen', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'origen_rfcremitente', 'string', length = 15, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'RFC Remitente', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'origen_nombreremitente', 'string', length = 254, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Nombre Remitente', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'origen_numregidtrib', 'string', length = 254, default = "",
        required = False,
        # requires = IS_NULL_OR(IS_MATCH('^[^|]{6,40}$', error_message='Dato con formato incorrecto')),
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Num. ID o Reg. Fiscal',
        comment = 'Número de identificación o registro fiscal',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'origen_recidenciafiscal', 'string', length = 30, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Recidencia Fiscal', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'origen_pais_id', 'integer', default = None,
        required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tpaises.id', db01.tpaises._format)),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'País',
        comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tcfdi_compcartaporte_ubicaciones.origen_pais_id
            )
        ),
    Field(
        'origen_numestacion', 'string', length = 30, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Num. Estación', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'origen_estacion_id', 'integer', default = None,
        required = False, requires = IS_NULL_OR(
            IS_IN_DB(db01, 'ttransporte_estaciones.id', db01.ttransporte_estaciones._format)
            ),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Num. Estación',
        comment = 'Estación mapeada al catálogo',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tcfdi_compcartaporte_ubicaciones.origen_estacion_id
            )
        ),
    Field(
        'origen_nombreestacion', 'string', length = 60, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Nombre Estación', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'origen_navegaciontrafico', 'string', length = 50,
        default = TCFDI_COMPCARTAPORTE_UBICACIONES.NAVEGACIONTRAFICO.NO_DEFINIDO,
        required = False, requires = IS_IN_SET(
            TCFDI_COMPCARTAPORTE_UBICACIONES.NAVEGACIONTRAFICO.GET_DICT(),
            zero = 0, error_message = 'Selecciona'
            ),
        notnull = False, unique = False,
        widget = stv_widget_combobox, label = 'Navegación/Tráfico', comment = '',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_list(
            v, r, None, TCFDI_COMPCARTAPORTE_UBICACIONES.NAVEGACIONTRAFICO.GET_DICT()
            )
        ),
    Field(
        'origen_fechahorasalida_str', 'string', length = 50, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Fecha Hora Salida', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'origen_fechahorasalida', type = FIELD_UTC_DATETIME, default = None,
        required = False, requires = IS_NULL_OR(IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME)),
        notnull = False,
        widget = stv_widget_inputDateTime, label = 'Fecha Hora Salida', comment = '',
        writable = False, readable = True,
        represent = stv_represent_datetime
        ),

    # Atributos del nodo Destino
    Field(
        'destino_iddestino', 'string', length = 10, default = "",
        required = False,
        # requires = IS_NULL_OR(IS_MATCH('^OR[0-9]{6}$', error_message='ID origen no es correcto')),
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'ID Destino', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'destino_rfcdestinatario', 'string', length = 15, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'RFC Destinatario', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'destino_nombredestinatario', 'string', length = 254, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Nombre Destinatario', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'destino_numregidtrib', 'string', length = 254, default = "",
        required = False,
        # requires = IS_NULL_OR(IS_MATCH('^[^|]{6,40}$', error_message='Dato con formato incorrecto')),
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Num. ID o Reg. Fiscal',
        comment = 'Número de identificación o registro fiscal',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'destino_recidenciafiscal', 'string', length = 30, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Recidencia Fiscal', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'destino_pais_id', 'integer', default = None,
        required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tpaises.id', db01.tpaises._format)),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'País',
        comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tcfdi_compcartaporte_ubicaciones.destino_pais_id
            )
        ),
    Field(
        'destino_numestacion', 'string', length = 30, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Num. Estación', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'destino_estacion_id', 'integer', default = None,
        required = False, requires = IS_NULL_OR(
            IS_IN_DB(db01, 'ttransporte_estaciones.id', db01.ttransporte_estaciones._format)
            ),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Num. Estación',
        comment = 'Estación mapeada al catálogo',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tcfdi_compcartaporte_ubicaciones.destino_estacion_id
            )
        ),
    Field(
        'destino_nombreestacion', 'string', length = 60, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Nombre Estación', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'destino_navegaciontrafico', 'string', length = 50,
        default = TCFDI_COMPCARTAPORTE_UBICACIONES.NAVEGACIONTRAFICO.NO_DEFINIDO,
        required = False, requires = IS_IN_SET(
            TCFDI_COMPCARTAPORTE_UBICACIONES.NAVEGACIONTRAFICO.GET_DICT(),
            zero = 0, error_message = 'Selecciona'
            ),
        notnull = False, unique = False,
        widget = stv_widget_combobox, label = 'Navegación/Tráfico', comment = '',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_list(
            v, r, None, TCFDI_COMPCARTAPORTE_UBICACIONES.NAVEGACIONTRAFICO.GET_DICT()
            )
        ),
    Field(
        'destino_fechahoraprogllegada_str', 'string', length = 50, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Fecha Hora Salida', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'destino_fechahoraprogllegada', type = FIELD_UTC_DATETIME, default = None,
        required = False, requires = IS_NULL_OR(IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME)),
        notnull = False,
        widget = stv_widget_inputDateTime, label = 'Fecha Hora Prog. Llegada', comment = '',
        writable = False, readable = True,
        represent = stv_represent_datetime
        ),

    # Atributos del nodo Domicilio
    Field(
        'dom_emp_pla_suc_almacen_id', dbc01.tempresa_plaza_sucursal_almacenes, default = None,
        required = False,
        requires = IS_NULL_OR(
            IS_IN_DB(
                dbc01(
                    dbc01.tempresa_plaza_sucursal_almacenes.id.belongs(
                        TEMPRESA_PLAZA_SUCURSAL_ALMACENES.OBTENER()
                        )
                    ),
                'tempresa_plaza_sucursal_almacenes.id', dbc01.tempresa_plaza_sucursal_almacenes._format
                )
            ),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Almacén', comment = None,
        writable = False, readable = True,
        represent = stv_represent_referencefield
        ),
    # Field(
    #     'dom_sucursal_id', dbc01.tempresa_plaza_sucursales, default = None,
    #     required = False,
    #     ondelete = 'NO ACTION', notnull = False, unique = False,
    #     widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Sucursal', comment = None,
    #     writable = True, readable = True,
    #     represent = stv_represent_referencefield
    #     ),
    Field(
        'dom_usardireccioncliente', 'boolean', default = False,
        required = False,
        notnull = False,
        widget = stv_widget_inputCheckbox, label = 'Usar dirección cliente', comment = None,
        writable = False, readable = True,
        represent = stv_represent_boolean
        ),
    Field(
        'dom_pais_id', 'integer', default = None,
        required = False, requires = IS_IN_DB(db01, 'tpaises.id', db01.tpaises._format),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Pais', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tcfdi_compcartaporte_ubicaciones.dom_pais_id)
        ),
    Field(
        'dom_pais', 'string', length = 10, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Pais', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'dom_pais_estado_id', 'integer', default = None,
        required = False, requires = IS_IN_DB(db01, 'tpais_estados.id', db01.tpais_estados._format),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda field, value: stv_widget_db_chosen_linked(
            field, value, 1, 'dom_pais_id',
            URL(a = 'app_generales', c = '005genericos_sat', f = 'localidad_pais_estados_ver.json'),
            D_additionalAttributes = Storage(
                linkedTableFieldName = 'pais_id'
                )
            ),
        label = 'Estado', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tcfdi_compcartaporte_ubicaciones.dom_pais_estado_id
            )
        ),
    Field(
        'dom_estado', 'string', length = 30, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Estado', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'dom_municipio', 'string', length = 120, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Municipio', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'dom_referencia', 'string', length = 120, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Referencia', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'dom_localidad', 'string', length = 120, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Localidad', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'dom_colonia', 'string', length = 120, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Colonia', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'dom_calle', 'string', length = 200, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Calle', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'dom_numexterior', 'string', length = 55, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Num. Exterior', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'dom_numinterior', 'string', length = 55, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Num. Interior', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'dom_codigopostal', 'string', length = 12, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Código Postal', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),

    tSignature_dbc01,
    format = '%(origen_idorigen)s %(destino_iddestino)s',
    singular = 'Ubicación',
    plural = 'Ubicaciones',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )


class TCFDI_COMPCARTAPORTE_MERCANCIAS:
    """ Tabala resumen de las mercancías transportadas.

    Es una relación de 1 (Mercancias en tabla CartaPorte) a [1,Inf].
    Se incluye el nodo DetalleMercancia por relación 1 a [0,1].

    """

    @staticmethod
    def DBTABLA(s_id = None):
        if s_id is None:
            return dbc01.tcfdi_compcartaporte_mercancias
        else:
            return dbc01.tcfdi_compcartaporte_mercancias(s_id)

    class E_MATERIALPELIGROSO:
        """ Se definen las opciónes del campo """
        NADA = ''
        SI = 'Sí'
        NO = 'No'

        @classmethod
        def GET_DICT(cls):
            return {
                cls.SI: 'Sí',
                cls.NO: 'No',
                }

    @classmethod
    def PUEDE_CREAR(
            cls,
            x_cfdi_compCartaPorte
            ):
        """ Determina si puede o no crear un CFDI de traslado.

        Actualmente no hay condición requerida.
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _D_results = TCFDI_COMPCARTAPORTE.PUEDE_EDITAR(
            x_cfdi_compCartaPorte = x_cfdi_compCartaPorte
            )

        _D_return.update(_D_results)

        return _D_return

    @classmethod
    def PUEDE_EDITAR(
            cls,
            x_cfdi_compcartaporte_mercancia,
            x_cfdi_compCartaPorte
            ):
        """ Define si es posible editar el CFDI de traslado.
        @param x_cfdi_compcartaporte_mercancia:
        @type x_cfdi_compcartaporte_mercancia:
        @param x_cfdi_compCartaPorte:
        @type x_cfdi_compCartaPorte:

        """
        _ = x_cfdi_compcartaporte_mercancia
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _D_results = TCFDI_COMPCARTAPORTE.PUEDE_EDITAR(
            x_cfdi_compCartaPorte = x_cfdi_compCartaPorte
            )

        _D_return.update(_D_results)

        return _D_return

    @classmethod
    def PUEDE_ELIMINAR(
            cls,
            x_cfdi_compcartaporte_mercancia
            ):
        """ Determina si puede o no eliminar un CFDI de traspaso

        Misma condición que puede_editar

        """
        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(
            x_cfdi_compcartaporte_mercancia,
            cls.DBTABLA()
            )
        _dbRow_mercancia = _D_results.dbRow

        _dbRow_cartaporte = dbc01.tcfdi_compcartaporte(_dbRow_mercancia.cfdi_compcartaporte_id)

        return cls.PUEDE_EDITAR(
            x_cfdi_compcartaporte_mercancia = _dbRow_mercancia,
            x_cfdi_compCartaPorte = _dbRow_cartaporte
            )

    @classmethod
    def CONFIGURACAMPOS_NUEVO(
            cls,
            s_cfdi_compCartaPorte_mercancia_id,
            x_cfdi_compCartaPorte
            ):
        """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

        @param x_cfdi_compCartaPorte:
        @type x_cfdi_compCartaPorte:
        @param s_cfdi_compCartaPorte_mercancia_id:
        @type s_cfdi_compCartaPorte_mercancia_id:
        @return:
        @rtype:
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTable = cls.DBTABLA()

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_compCartaPorte, dbc01.tcfdi_compcartaporte)
        _dbRow_cfdi_cartaPorte = _D_results.dbRow

        _D_results = cls.PUEDE_CREAR(
            x_cfdi_compCartaPorte = _dbRow_cfdi_cartaPorte
            )

        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_results)

        elif s_cfdi_compCartaPorte_mercancia_id:
            _dbRow_base = _dbTable(s_cfdi_compCartaPorte_mercancia_id)

            # _dbTable.cfdi_compcartaporte_id.default = _dbRow_base.cfdi_compcartaporte_id
            # _dbTable.claveprodservcp.default = _dbRow_base.claveprodservcp
            _dbTable.prodservcp_id.default = _dbRow_base.prodservcp_id
            # _dbTable.clavestcc.default = _dbRow_base.clavestcc
            _dbTable.tferr_prodservstcc_id.default = _dbRow_base.tferr_prodservstcc_id
            _dbTable.descripcion.default = _dbRow_base.descripcion
            # _dbTable.cantidad.default = _dbRow_base.cantidad
            # _dbTable.claveunidad.default = _dbRow_base.claveunidad
            # _dbTable.unidad.default = _dbRow_base.unidad
            _dbTable.unidad_id.default = _dbRow_base.unidad_id
            _dbTable.dimensiones.default = _dbRow_base.dimensiones
            _dbTable.materialpeligroso.default = _dbRow_base.materialpeligroso
            # _dbTable.cvematerialpeligroso.default = _dbRow_base.cvematerialpeligroso
            _dbTable.cvematerialpeligroso_id.default = _dbRow_base.cvematerialpeligroso_id
            # _dbTable.embalaje.default = _dbRow_base.embalaje
            _dbTable.tipoembalaje_id.default = _dbRow_base.tipoembalaje_id
            _dbTable.descripembalaje.default = _dbRow_base.descripembalaje
            # _dbTable.pesoenkg.default = _dbRow_base.pesoenkg
            # _dbTable.valormercancia.default = _dbRow_base.valormercancia
            # _dbTable.moneda.default = _dbRow_base.moneda
            _dbTable.monedacontpaqi_id.default = _dbRow_base.monedacontpaqi_id
            # _dbTable.fraccionarancelaria.default = _dbRow_base.fraccionarancelaria
            # _dbTable.fraccionarancelaria_id.default = _dbRow_base.fraccionarancelaria_id
            # _dbTable.uuidcomercioext.default = _dbRow_base.uuidcomercioext
            # _dbTable.detallemerc_unidadpeso.default = _dbRow_base.detallemerc_unidadpeso
            _dbTable.detallemerc_unidadpeso_id.default = _dbRow_base.detallemerc_unidadpeso_id
            # _dbTable.detallemerc_pesobruto.default = _dbRow_base.detallemerc_pesobruto
            # _dbTable.detallemerc_pesoneto.default = _dbRow_base.detallemerc_pesoneto
            # _dbTable.detallemerc_pesotara.default = _dbRow_base.detallemerc_pesotara
            # _dbTable.detallemerc_numpiezas.default = _dbRow_base.detallemerc_numpiezas

        else:
            pass

        _dbTable.cfdi_compcartaporte_id.writable = False
        _dbTable.prodservcp_id.writable = True
        _dbTable.tferr_prodservstcc_id.writable = False
        _dbTable.descripcion.writable = True
        _dbTable.cantidad.writable = True
        _dbTable.unidad_id.writable = True
        _dbTable.dimensiones.writable = True
        _dbTable.materialpeligroso.writable = True
        _dbTable.cvematerialpeligroso_id.writable = True
        _dbTable.tipoembalaje_id.writable = True
        _dbTable.pesoenkg.writable = True
        _dbTable.pesoenkg.required = True  # Para mostrar el * en el label
        _dbTable.valormercancia.writable = True
        _dbTable.monedacontpaqi_id.writable = True
        _dbTable.fraccionarancelaria_id.writable = False  # Cuando es movimiento internacional
        _dbTable.uuidcomercioext.writable = False  # Cuando es salida internacional
        _dbTable.detallemerc_unidadpeso_id.writable = False  # Cuando es por mar
        _dbTable.detallemerc_pesobruto.writable = False  # Cuando es por mar
        _dbTable.detallemerc_pesoneto.writable = False  # Cuando es por mar
        _dbTable.detallemerc_pesotara.writable = False  # Cuando es por mar
        _dbTable.detallemerc_numpiezas.writable = False  # Cuando es por mar

        # Campos que no deben ser editados, solo de forma autocalculada
        _dbTable.claveprodservcp.writable = False
        _dbTable.clavestcc.writable = False
        _dbTable.claveunidad.writable = False
        _dbTable.unidad.writable = False
        _dbTable.cvematerialpeligroso.writable = False
        _dbTable.embalaje.writable = False
        _dbTable.descripembalaje.writable = False
        _dbTable.moneda.writable = False
        _dbTable.fraccionarancelaria.writable = False
        _dbTable.detallemerc_unidadpeso.writable = False

        # Campos que todos los movimientos pueden editar
        _dbTable.notas.writable = False

        # Se definen los valores por default
        # TODOMejora agregar información en los productos para poder asociar los campos directamente

        _dbTable.cfdi_compcartaporte_id.default = _dbRow_cfdi_cartaPorte.id
        # _dbTable.prodservcp_id.default = None
        # _dbTable.tferr_prodservstcc_id.default = None
        # _dbTable.descripcion.default = ""
        _dbTable.cantidad.default = 1
        # _dbTable.unidad_id.default = ""
        # _dbTable.dimensiones.default = ""
        _dbTable.materialpeligroso.default = cls.E_MATERIALPELIGROSO.NO
        # _dbTable.cvematerialpeligroso_id.default = None
        # _dbTable.tipoembalaje_id.default = None
        # _dbTable.pesoenkg.default = 0
        # _dbTable.valormercancia.default = 0
        # _dbTable.monedacontpaqi_id.default = D_stvSiteHelper.dbconfigs.defaults.moneda_contpaqi_id
        # _dbTable.fraccionarancelaria_id.default = None
        # _dbTable.uuidcomercioext.default = ""
        # _dbTable.detallemerc_unidadpeso_id.default = None
        # _dbTable.detallemerc_pesobruto.default = 0
        # _dbTable.detallemerc_pesoneto.default = 0
        # _dbTable.detallemerc_pesotara.default = 0
        # _dbTable.detallemerc_numpiezas.default = 0

        return _D_return

    @classmethod
    def CONFIGURACAMPOS_EDICION(
            cls,
            x_cfdi_compcartaporte_mercancia,
            x_cfdi_compCartaPorte
            ):
        """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

        @param x_cfdi_compcartaporte_mercancia:
        @type x_cfdi_compcartaporte_mercancia:
        @param x_cfdi_compCartaPorte:
        @type x_cfdi_compCartaPorte:
        @return:
        @rtype:
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_compCartaPorte, dbc01.tcfdi_compcartaporte)
        _dbRow_cfdi_cartaPorte = _D_results.dbRow

        _D_results = cls.PUEDE_EDITAR(
            x_cfdi_compcartaporte_mercancia = x_cfdi_compcartaporte_mercancia,
            x_cfdi_compCartaPorte = _dbRow_cfdi_cartaPorte
            )

        if _D_results.E_return == CLASS_e_RETURN.OK:
            # Si puede editar regresa un ok y el movimiento esta abierto

            # Se manda vacío el argumento para no agregar defaults a los campos
            _D_results = cls.CONFIGURACAMPOS_NUEVO(
                s_cfdi_compCartaPorte_mercancia_id = None,
                x_cfdi_compCartaPorte = _dbRow_cfdi_cartaPorte
                )
            _D_return.update(_D_results)

        else:
            # De lo contrario en cualquier otro caso, no puede editar nada de esta tabla
            pass

        return _D_return

    @classmethod
    def AL_VALIDAR(
            cls,
            O_form,
            x_cfdi_compCartaPorte
            ):
        """ Validar la información para poder grabar cfdi

        @param x_cfdi_compCartaPorte:
        @type x_cfdi_compCartaPorte:
        @param O_form:
        @type O_form:
        @return:
        @rtype:
        """
        _dbTable = cls.DBTABLA()

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_compCartaPorte, dbc01.tcfdi_compcartaporte)
        _dbRow_cfdi_cartaPorte = _D_results.dbRow

        _D_camposAChecar = Storage()  # Define los campos que deben ser verificados

        # Dependiendo si es nuevo o editado, toma los valores a validar
        if not O_form.record_id:
            # Si esta creando el registro:

            _D_results = cls.PUEDE_CREAR(
                x_cfdi_compCartaPorte = _dbRow_cfdi_cartaPorte
                )
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                O_form.errors.id = _D_results.s_msgError
            else:
                pass

            _D_camposAChecar.prodservcp_id = (
                O_form.vars.get("prodservcp_id", _dbTable.prodservcp_id.default)
                )

            _D_camposAChecar.materialpeligroso = (
                O_form.vars.get("materialpeligroso", _dbTable.materialpeligroso.default)
                )

            _D_camposAChecar.cvematerialpeligroso_id = (
                O_form.vars.get("cvematerialpeligroso_id", _dbTable.cvematerialpeligroso_id.default)
                )

            _D_camposAChecar.tipoembalaje_id = (
                O_form.vars.get("tipoembalaje_id", _dbTable.tipoembalaje_id.default)
                )

            _D_camposAChecar.pesoenkg = (
                O_form.vars.get("pesoenkg", _dbTable.pesoenkg.default)
                )

        else:

            _dbRow_mercancia = _dbTable(O_form.record_id)

            _D_results = cls.PUEDE_EDITAR(
                x_cfdi_compcartaporte_mercancia = _dbRow_mercancia,
                x_cfdi_compCartaPorte = _dbRow_cfdi_cartaPorte
                )
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                O_form.errors.id = _D_results.s_msgError
            else:
                pass

            _D_camposAChecar.prodservcp_id = (
                O_form.vars.get("prodservcp_id", _dbRow_mercancia.prodservcp_id)
                )

            _D_camposAChecar.materialpeligroso = (
                O_form.vars.get("materialpeligroso", _dbRow_mercancia.materialpeligroso)
                )

            _D_camposAChecar.cvematerialpeligroso_id = (
                O_form.vars.get("cvematerialpeligroso_id", _dbRow_mercancia.cvematerialpeligroso_id)
                )

            _D_camposAChecar.tipoembalaje_id = (
                O_form.vars.get("tipoembalaje_id", _dbRow_mercancia.tipoembalaje_id)
                )

            _D_camposAChecar.pesoenkg = (
                O_form.vars.get("pesoenkg", _dbRow_mercancia.pesoenkg)
                )

        # Validaciones

        if not _D_camposAChecar.pesoenkg:
            O_form.errors.pesoenkg = "Requerido"
        else:
            pass

        if _D_camposAChecar.prodservcp_id:
            _dbRow_prodservcp = db01.tprodservcp(_D_camposAChecar.prodservcp_id)
            if (
                    (_dbRow_prodservcp.materialpeligroso == TPRODSERVCP.E_MATERIALPELIGROSO.ESPELIGROSO)
                    or (
                        (_dbRow_prodservcp.materialpeligroso == TPRODSERVCP.E_MATERIALPELIGROSO.PUDIERASERPELIGROSO)
                        and (
                            _D_camposAChecar.materialpeligroso
                            == TCFDI_COMPCARTAPORTE_MERCANCIAS.E_MATERIALPELIGROSO.SI
                            )
                        )
                    ):
                if (
                        _D_camposAChecar.materialpeligroso
                        != TCFDI_COMPCARTAPORTE_MERCANCIAS.E_MATERIALPELIGROSO.SI
                        ):
                    O_form.errors.materialpeligroso = (
                        "Require ser definido como peligroso, ya que el tipo de producto dice que es peligroso"
                        )
                else:
                    pass

                if not _D_camposAChecar.cvematerialpeligroso_id:
                    O_form.errors.cvematerialpeligroso_id = "Es requerido si el material es peligroso"
                else:
                    pass

                if not _D_camposAChecar.tipoembalaje_id:
                    O_form.errors.tipoembalaje_id = "Es requerido si el material es peligroso"
                else:
                    pass

            else:
                # No es material peligroso
                if (
                        _D_camposAChecar.materialpeligroso
                        == TCFDI_COMPCARTAPORTE_MERCANCIAS.E_MATERIALPELIGROSO.SI
                        ):
                    O_form.errors.materialpeligroso = (
                        "Require ser definido como NO peligroso, "
                        + "ya que el tipo de producto dice que no puede ser peligroso"
                        )
                else:
                    pass

                if _D_camposAChecar.cvematerialpeligroso_id:
                    O_form.errors.cvematerialpeligroso_id = "Debe estar limpio, ya que no es peligroso"
                else:
                    pass

                if _D_camposAChecar.tipoembalaje_id:
                    O_form.errors.tipoembalaje_id = "Debe estar limpio, ya que no es peligroso"
                else:
                    pass

        else:
            # Requiere asociar el producto a un tipo de producto CP
            O_form.errors.prodservcp_id = "Requiere asociar un tipo de producto"

        if not O_form.errors:
            # Si no hay cliente, el autocalculo debe especificar el RFC genérico
            pass
        else:
            pass

        return O_form

    @classmethod
    def AL_ACEPTAR(
            cls,
            O_form,
            x_cfdi_compCartaPorte,
            **D_argsMapeoCalculoContabiliza
            ):
        """ Al aceptar el registro deben hacerse los cálculos

        Se hacen todos los cálculos y aceptación de los cambios en el producto

        @param x_cfdi_compCartaPorte:
        @type x_cfdi_compCartaPorte:
        @param D_argsMapeoCalculoContabiliza:
        @type D_argsMapeoCalculoContabiliza:
        @param O_form:
        @type O_form:
        @return:
        @rtype:
        """
        _ = D_argsMapeoCalculoContabiliza
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _dbRow_mercancia = cls.DBTABLA(O_form.record_id or O_form.vars.id)

        _D_results = cls.MAPEAR_CAMPOS(_dbRow_mercancia)

        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_results)

        else:
            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_compCartaPorte, dbc01.tcfdi_compcartaporte)
            _dbRow_cfdi_cartaPorte = _D_results.dbRow

            _D_results = TCFDI_COMPCARTAPORTE.CALCULAR_CAMPOS(
                x_cfdi_compCartaPorte = _dbRow_cfdi_cartaPorte
                )

            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.update(_D_results)
            else:
                pass

        generaMensajeFlash(cls.DBTABLA(), _D_return)

        return O_form

    @classmethod
    def ANTES_ELIMINAR(
            cls,
            L_errorsAction,
            dbRow
            ):
        """ Antes de eliminar un movimiento de inventario de este tipo

        @param dbRow:
        @type dbRow:
        @param L_errorsAction:
        @type L_errorsAction:
        @return: errores
        @rtype: lista de diccionarios {'msg':'...'}
        """
        _D_results = cls.PUEDE_ELIMINAR(
            x_cfdi_compcartaporte_mercancia = dbRow
            )
        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            L_errorsAction.append({'msg': _D_results.s_msgError})
        else:
            pass

        return L_errorsAction

    @classmethod
    def DESPUES_ELIMINAR(
            cls,
            L_errorsAction,
            x_cfdi_compCartaPorte
            ):
        """ Después de eliminar un registro

        @param x_cfdi_compCartaPorte:
        @type x_cfdi_compCartaPorte:
        @param L_errorsAction:
        @type L_errorsAction:
        @return: errores
        @rtype: lista de diccionarios {'msg':'...'}
        """

        _dbTable = dbc01.tcfdi_compcartaporte_mercancias

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_compCartaPorte, dbc01.tcfdi_compcartaporte)
        _dbRow_cfdi_cartaPorte = _D_results.dbRow

        _D_results = TCFDI_COMPCARTAPORTE.CALCULAR_CAMPOS(
            x_cfdi_compCartaPorte = _dbRow_cfdi_cartaPorte
            )

        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            L_errorsAction.append({'msg': _D_results.s_msgError})
        else:
            pass

        return L_errorsAction

    @classmethod
    def MAPEAR_CAMPOS(
            cls,
            x_cfdi_compcartaporte_mercancia
            ):
        """ Recalcula campos, haciendo la consulta de la carta porte a partir del CFDI

        @param x_cfdi_compcartaporte_mercancia:
        @type x_cfdi_compcartaporte_mercancia:
        @return:
        @rtype:
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(
            x_cfdi_compcartaporte_mercancia,
            cls.DBTABLA()
            )
        _dbRow_mercancia = _D_results.dbRow

        _dbRow_mercancia.claveprodservcp = db01.tprodservcp(
            _dbRow_mercancia.prodservcp_id
            ).c_claveprodservcp if _dbRow_mercancia.prodservcp_id else ""

        _dbRow_mercancia.clavestcc = db01.tferr_prodservstcc(
            _dbRow_mercancia.tferr_prodservstcc_id
            ).c_claveprodstcc if _dbRow_mercancia.tferr_prodservstcc_id else ""

        _dbRow_mercancia.claveunidad = db01.tunidades(
            _dbRow_mercancia.unidad_id
            ).c_claveunidad if _dbRow_mercancia.unidad_id else ""

        _dbRow_mercancia.cvematerialpeligroso = db01.tmaterialespeligrosos(
            _dbRow_mercancia.cvematerialpeligroso_id
            ).c_materialpeligroso if _dbRow_mercancia.cvematerialpeligroso_id else ""

        _dbRow_embalaje = db01.ttiposembalajes(_dbRow_mercancia.tipoembalaje_id)
        _dbRow_mercancia.embalaje = _dbRow_embalaje.c_tipoembalaje if _dbRow_embalaje else ""
        _dbRow_mercancia.descripembalaje = _dbRow_embalaje.descripcion if _dbRow_embalaje else ""

        _dbRow_monedaContpaq = dbc01.tempresa_monedascontpaqi(_dbRow_mercancia.monedacontpaqi_id)
        _dbRow_mercancia.moneda = db01.tmonedas(
            _dbRow_monedaContpaq.moneda_id
            ).c_moneda if _dbRow_monedaContpaq and _dbRow_monedaContpaq.moneda_id else ""

        # TODOMejora implementar la fraccion arancelaria
        # _dbRow_mercancia.fraccionarancelaria = db01.tXXX(
        #     _dbRow_mercancia.fraccionarancelaria_id
        #     ).c_XXX if _dbRow_mercancia.fraccionarancelaria_id else ""
        _dbRow_mercancia.fraccionarancelaria = ""

        _dbRow_mercancia.detallemerc_unidadpeso = db01.tunidadespeso(
            _dbRow_mercancia.detallemerc_unidadpeso_id
            ).c_claveunidadpeso if _dbRow_mercancia.detallemerc_unidadpeso_id else ""

        _dbRow_mercancia.update_record()

        return _D_return

    pass


dbc01.define_table(
    'tcfdi_compcartaporte_mercancias',
    Field(
        'cfdi_compcartaporte_id', dbc01.tcfdi_compcartaporte, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'CFDI Carta Porte', comment = None,
        writable = False, readable = True,
        represent = None
        ),

    # Atributos de nodo Mercancia
    Field(
        'claveprodservcp', 'string', length = 30, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Bienes Transp.', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'prodservcp_id', 'integer', default = None,
        required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tprodservcp.id', db01.tprodservcp._format)),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_search(
            f, v,
            s_display = '%(descripcion)s [%(c_claveprodservcp)s]',
            s_url = URL(a = 'app_generales', c = '005genericos_sat', f = 'prodservscp_buscar'),
            D_additionalAttributes = {'reference': db01.tprodservcp.id}
            ),
        label = 'Bienes Transp.', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tcfdi_compcartaporte_mercancias.prodservcp_id)
        ),

    Field(
        'clavestcc', 'string', length = 30, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Clave STCC', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'tferr_prodservstcc_id', 'integer', default = None,
        required = False,
        requires = IS_NULL_OR(IS_IN_DB(db01, 'tferr_prodservstcc.id', db01.tferr_prodservstcc._format)),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_search(
            f, v,
            s_display = dbc01.tcfdi_compcartaporte_mercancias._format,
            s_url = URL(a = 'app_generales', c = '005genericos_sat', f = 'prodservstcc_buscar'),
            D_additionalAttributes = {'reference': db01.tferr_prodservstcc.id}
            ),
        label = 'Prod. Serv. STCC', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tcfdi_compcartaporte_mercancias.tferr_prodservstcc_id
            )
        ),

    Field(
        'descripcion', 'string', length = 1000, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Descripción', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'cantidad', 'decimal(16,6)', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputFloat, label = 'Cantidad', comment = None,
        writable = False, readable = True,
        represent = stv_represent_float
        ),
    Field(
        'claveunidad', 'string', length = 30, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Clave Unidad', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'unidad', 'string', length = 100, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Unidad', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'unidad_id', 'integer', default = None,
        required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tunidades.id', db01.tunidades._format)),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_search(
            f, v, s_display = '%(nombre)s [%(c_claveunidad)s]',
            s_url = URL(
                a = 'app_generales',
                c = '005genericos_sat',
                f = 'unidades_buscar'
                ),
            D_additionalAttributes = {'reference': db01.tunidades.id}
            ),
        label = 'Unidad', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tcfdirx_conceptos.unidad_id
            )
        ),
    Field(
        'dimensiones', 'string', length = 100, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Dimensiones',
        comment = (
            'Se debe registrar la longitud, la altura y la anchura en centímetros o en pulgadas separados '
            + 'dichos valores por una diagonal, i.e. 30/40/30cm'
            ),
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'materialpeligroso', 'string', default = TCFDI_COMPCARTAPORTE_MERCANCIAS.E_MATERIALPELIGROSO.NO,
        required = False, requires = IS_IN_SET(
            TCFDI_COMPCARTAPORTE_MERCANCIAS.E_MATERIALPELIGROSO.GET_DICT(), zero = 0, error_message = 'Selecciona'
            ),
        notnull = False, unique = False,
        widget = stv_widget_combobox,
        label = 'Material peligroso', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_list(
            v, r,
            fnLambda = None,
            D_data = TCFDI_COMPCARTAPORTE_MERCANCIAS.E_MATERIALPELIGROSO.GET_DICT(),
            dbField = dbc01.tcfdi_compcartaporte_mercancias.materialpeligroso
            )
        ),
    Field(
        'cvematerialpeligroso', 'string', length = 30, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Cve. Material Peligroso', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'cvematerialpeligroso_id', 'integer', default = None,
        required = False, requires = IS_NULL_OR(
            IS_IN_DB(db01, 'tmaterialespeligrosos.id', db01.tmaterialespeligrosos._format)
            ),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_search(
            f, v,
            s_display = '%(nombre)s [%(c_materialpeligroso)s]',
            s_url = URL(
                a = 'app_generales',
                c = '005genericos_sat',
                f = 'materialespeligrosos_buscar'
                ),
            D_additionalAttributes = {'reference': db01.tmaterialespeligrosos.id}
            ),
        label = 'Cve. Material Peligroso',
        comment = 'En caso de cascos usados, usar M1719',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tcfdi_compcartaporte_mercancias.cvematerialpeligroso_id
            )
        ),
    Field(
        'embalaje', 'string', length = 30, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Embalaje', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'tipoembalaje_id', 'integer', default = TTIPOSEMBALAJES.NO_APLICA,
        required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'ttiposembalajes.id', db01.ttiposembalajes._format)),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Embalaje',
        comment = 'Cuando es a granel, debe seleccionarse Z01',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tcfdi_compcartaporte_mercancias.tipoembalaje_id
            )
        ),
    Field(
        'descripembalaje', 'string', length = 100, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Descrip. Embalaje', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'pesoenkg', 'decimal(14,4)', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputFloat, label = 'Peso en Kg.', comment = None,
        writable = False, readable = True,
        represent = stv_represent_float
        ),
    Field(
        'valormercancia', 'decimal(14,4)', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputMoney, label = 'Valor Mercancía', comment = None,
        writable = False, readable = True,
        represent = stv_represent_money_ifnotzero
        ),
    Field(
        'moneda', 'string', length = 10, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Moneda', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'monedacontpaqi_id', 'integer', default = D_stvSiteHelper.dbconfigs.defaults.moneda_contpaqi_id,
        required = False,
        requires = IS_NULL_OR(
            IS_IN_DB(
                dbc01(dbc01.tempresa_monedascontpaqi.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id),
                'tempresa_monedascontpaqi.id',
                dbc01.tempresa_monedascontpaqi._format
                )
            ),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Moneda', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tcfdi_compcartaporte_mercancias.monedacontpaqi_id
            )
        ),

    Field(
        'fraccionarancelaria', 'string', length = 10, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Fracción arancelaria', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'fraccionarancelaria_id', 'integer', default = None,
        required = False,
        # requires = IS_NULL_OR(IS_IN_DB(db01, 'tmonedas.id', db01.tmonedas._format)),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Fracción arancelaria', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tcfdi_compcartaporte_mercancias.fraccionarancelaria_id
            )
        ),
    Field(
        'uuidcomercioext', 'string', length = 50, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'UUID Comercio Ext.', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),

    # Nodo detalleMercancia que tiene relación [0,1]
    Field(
        'detallemerc_unidadpeso', 'string', length = 100, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Unidad Peso', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'detallemerc_unidadpeso_id', 'integer', default = None,
        required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tunidadespeso.id', db01.tunidadespeso._format)),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1),
        label = 'Unidad Peso', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tcfdi_compcartaporte_mercancias.detallemerc_unidadpeso_id
            )
        ),
    Field(
        'detallemerc_pesobruto', 'decimal(14,4)', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputFloat, label = 'Peso Bruto', comment = None,
        writable = False, readable = True,
        represent = stv_represent_float
        ),
    Field(
        'detallemerc_pesoneto', 'decimal(14,4)', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputFloat, label = 'Peso Neto', comment = None,
        writable = False, readable = True,
        represent = stv_represent_float
        ),
    Field(
        'detallemerc_pesotara', 'decimal(14,4)', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputFloat, label = 'Peso Tara', comment = None,
        writable = False, readable = True,
        represent = stv_represent_float
        ),
    Field(
        'detallemerc_numpiezas', 'integer', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputInteger, label = 'Num. Piezas', comment = None,
        writable = False, readable = True,
        represent = stv_represent_number
        ),

    tSignature_dbc01,
    format = '%(descripcion)s',
    singular = 'Mercancía',
    plural = 'Mercancías',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )


class TCFDI_COMPCARTAPORTE_MERCANCIA_TRANSPORTA:
    """
    Es una relación de 1 (Mercancia en tabla Mercancias) a [0,Inf].
    """

    @staticmethod
    def DBTABLA(s_id = None):
        if s_id is None:
            return dbc01.tcfdi_compcartaporte_mercancia_transporta
        else:
            return dbc01.tcfdi_compcartaporte_mercancia_transporta(s_id)

    @classmethod
    def PUEDE_CREAR(
            cls,
            x_cfdi_compCartaPorte_mercancia,
            ):
        """ Determina si puede o no crear un CFDI de traslado.

        Actualmente no hay condición requerida.
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(
            x_cfdi_compCartaPorte_mercancia,
            dbc01.tcfdi_compcartaporte_mercancias
            )
        _dbRow_mercancia = _D_results.dbRow

        _n_ubicacionesDestino = dbc01(
            (dbc01.tcfdi_compcartaporte_ubicaciones.cfdi_compcartaporte_id == _dbRow_mercancia.cfdi_compcartaporte_id)
            & (dbc01.tcfdi_compcartaporte_ubicaciones.destino_iddestino != "")
            ).count()

        if _n_ubicacionesDestino < 2:
            _D_return.E_return = CLASS_e_RETURN.NOK_CONDICIONES_INCORRECTAS
            _D_return.s_msgError = "CP188: Se requieren al menos dos destinos para crear este detalle"

        else:
            _D_results = TCFDI_COMPCARTAPORTE_MERCANCIAS.PUEDE_EDITAR(
                x_cfdi_compcartaporte_mercancia = _dbRow_mercancia,
                x_cfdi_compCartaPorte = _dbRow_mercancia.cfdi_compcartaporte_id
                )

            _D_return.update(_D_results)

        return _D_return

    @classmethod
    def PUEDE_EDITAR(
            cls,
            x_cfdi_compCartaPorte_mercancia_transporta,
            ):
        """ Define si es posible editar el CFDI de traslado.
        @param x_cfdi_compCartaPorte_mercancia_transporta:
        @type x_cfdi_compCartaPorte_mercancia_transporta:

        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(
            x_cfdi_compCartaPorte_mercancia_transporta,
            cls.DBTABLA()
            )
        _dbRow_mercanciaTransporta = _D_results.dbRow

        _dbRow_mercancia = dbc01.tcfdi_compcartaporte_mercancias(
            _dbRow_mercanciaTransporta.cfdi_compcartaporte_mercancia_id
            )

        _D_results = TCFDI_COMPCARTAPORTE_MERCANCIAS.PUEDE_EDITAR(
            x_cfdi_compcartaporte_mercancia = _dbRow_mercancia,
            x_cfdi_compCartaPorte = _dbRow_mercancia.cfdi_compcartaporte_id
            )

        _D_return.update(_D_results)

        return _D_return

    @classmethod
    def PUEDE_ELIMINAR(
            cls,
            x_cfdi_compCartaPorte_mercancia_transporta
            ):
        """ Determina si puede o no eliminar un CFDI de traspaso

        Misma condición que puede_editar

        """

        return cls.PUEDE_EDITAR(
            x_cfdi_compCartaPorte_mercancia_transporta = x_cfdi_compCartaPorte_mercancia_transporta
            )

    @classmethod
    def CONFIGURACAMPOS_NUEVO(
            cls,
            s_cfdi_compCartaPorte_mercancia_transporta_id,
            x_cfdi_compCartaPorte_mercancia
            ):
        """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

        @param x_cfdi_compCartaPorte_mercancia:
        @type x_cfdi_compCartaPorte_mercancia:
        @param s_cfdi_compCartaPorte_mercancia_transporta_id:
        @type s_cfdi_compCartaPorte_mercancia_transporta_id:
        @return:
        @rtype:
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTable = cls.DBTABLA()

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(
            x_cfdi_compCartaPorte_mercancia, dbc01.tcfdi_compcartaporte_mercancias
            )
        _dbRow_cfdi_mercancia = _D_results.dbRow

        _D_results = cls.PUEDE_CREAR(
            x_cfdi_compCartaPorte_mercancia = _dbRow_cfdi_mercancia
            )

        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_results)

        elif s_cfdi_compCartaPorte_mercancia_transporta_id:
            _dbRow_base = _dbTable(s_cfdi_compCartaPorte_mercancia_transporta_id)

            # _dbTable.cfdi_compcartaporte_mercancia_id.default = _dbRow_base.cfdi_compcartaporte_mercancia_id
            # _dbTable.cantidad.default = _dbRow_base.cantidad
            # _dbTable.idorigen.default = _dbRow_base.idorigen
            _dbTable.cfdi_compcartaporte_ubicacion_origen_id.default = (
                _dbRow_base.cfdi_compcartaporte_ubicacion_origen_id
                )
            # _dbTable.iddestino.default = _dbRow_base.iddestino
            _dbTable.cfdi_compcartaporte_ubicacion_destino_id.default = (
                _dbRow_base.cfdi_compcartaporte_ubicacion_destino_id
                )
            # _dbTable.cvetransporte.default = _dbRow_base.cvetransporte
            _dbTable.transporte_id.default = _dbRow_base.transporte_id

        else:
            pass

        _dbTable.cfdi_compcartaporte_mercancia_id.writable = False
        _dbTable.cantidad.writable = True
        _dbTable.cfdi_compcartaporte_ubicacion_origen_id.writable = True
        _dbTable.cfdi_compcartaporte_ubicacion_origen_id.requires = IS_IN_DB(
            dbc01(
                (
                    dbc01.tcfdi_compcartaporte_ubicaciones.cfdi_compcartaporte_id
                    == _dbRow_cfdi_mercancia.cfdi_compcartaporte_id
                    )
                & (dbc01.tcfdi_compcartaporte_ubicaciones.origen_idorigen != "")
                ),
            'tcfdi_compcartaporte_ubicaciones.id',
            '%(origen_idorigen)s: %(origen_fechahorasalida)s: %(dom_municipio)s, %(dom_calle)s'
            )

        _dbTable.cfdi_compcartaporte_ubicacion_destino_id.writable = True
        _dbTable.cfdi_compcartaporte_ubicacion_destino_id.requires = IS_IN_DB(
            dbc01(
                (
                    dbc01.tcfdi_compcartaporte_ubicaciones.cfdi_compcartaporte_id
                    == _dbRow_cfdi_mercancia.cfdi_compcartaporte_id
                    )
                & (dbc01.tcfdi_compcartaporte_ubicaciones.destino_iddestino != "")
                ),
            'tcfdi_compcartaporte_ubicaciones.id',
            '%(destino_iddestino)s: %(destino_fechahoraprogllegada)s: %(dom_municipio)s, %(dom_calle)s'
            )

        _dbTable.transporte_id.writable = False  # Solo existe en caso de Mar, Aero y Ferr

        # Campos que no deben ser editados, solo de forma autocalculada
        _dbTable.idorigen.writable = False
        _dbTable.iddestino.writable = False
        _dbTable.cvetransporte.writable = False

        # Campos que todos los movimientos pueden editar
        _dbTable.notas.writable = False

        # Se definen los valores por default

        _dbTable.cfdi_compcartaporte_mercancia_id.default = _dbRow_cfdi_mercancia.id
        # _dbTable.cantidad.default = 1
        # _dbTable.idorigen.default = ""
        # _dbTable.cfdi_compcartaporte_ubicacion_origen_id.default = None
        # _dbTable.iddestino.default = ""
        # _dbTable.cfdi_compcartaporte_ubicacion_destino_id.default = None
        # _dbTable.cvetransporte.default = ""
        # _dbTable.transporte_id.default = None

        return _D_return

    @classmethod
    def CONFIGURACAMPOS_EDICION(
            cls,
            x_cfdi_compCartaPorte_mercancia_transporta,
            x_cfdi_compCartaPorte_mercancia
            ):
        """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

        @param x_cfdi_compCartaPorte_mercancia:
        @type x_cfdi_compCartaPorte_mercancia:
        @param x_cfdi_compCartaPorte_mercancia_transporta:
        @type x_cfdi_compCartaPorte_mercancia_transporta:
        @return:
        @rtype:
        """
        _ = x_cfdi_compCartaPorte_mercancia_transporta
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(
            x_cfdi_compCartaPorte_mercancia, dbc01.tcfdi_compcartaporte_mercancias
            )
        _dbRow_cfdi_mercancia = _D_results.dbRow

        _D_results = cls.PUEDE_EDITAR(
            x_cfdi_compCartaPorte_mercancia_transporta = x_cfdi_compCartaPorte_mercancia,
            )

        if _D_results.E_return == CLASS_e_RETURN.OK:
            # Si puede editar regresa un ok y el movimiento esta abierto

            # Se manda vacío el argumento para no agregar defaults a los campos
            _D_results = cls.CONFIGURACAMPOS_NUEVO(
                s_cfdi_compCartaPorte_mercancia_transporta_id = None,
                x_cfdi_compCartaPorte_mercancia = _dbRow_cfdi_mercancia
                )
            _D_return.update(_D_results)

        else:
            # De lo contrario en cualquier otro caso, no puede editar nada de esta tabla
            pass

        return _D_return

    @classmethod
    def AL_VALIDAR(
            cls,
            O_form,
            x_cfdi_compCartaPorte_mercancia
            ):
        """ Validar la información para poder grabar cfdi

        @param x_cfdi_compCartaPorte_mercancia:
        @type x_cfdi_compCartaPorte_mercancia:
        @param O_form:
        @type O_form:
        @return:
        @rtype:
        """
        _dbTable = cls.DBTABLA()

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(
            x_cfdi_compCartaPorte_mercancia, dbc01.tcfdi_compcartaporte_mercancias
            )
        _dbRow_cfdi_mercancia = _D_results.dbRow

        _D_camposAChecar = Storage()  # Define los campos que deben ser verificados

        # Dependiendo si es nuevo o editado, toma los valores a validar
        if not O_form.record_id:
            # Si esta creando el registro:

            _D_results = cls.PUEDE_CREAR(
                x_cfdi_compCartaPorte_mercancia = _dbRow_cfdi_mercancia
                )
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                O_form.errors.id = _D_results.s_msgError
            else:
                pass

            _D_camposAChecar.cantidad = (
                O_form.vars.get("cantidad", _dbTable.cantidad.default)
                )

            _D_camposAChecar.cfdi_compcartaporte_ubicacion_origen_id = (
                O_form.vars.get(
                    "cfdi_compcartaporte_ubicacion_origen_id", _dbTable.cfdi_compcartaporte_ubicacion_origen_id.default
                    )
                )

            _D_camposAChecar.cfdi_compcartaporte_ubicacion_destino_id = (
                O_form.vars.get(
                    "cfdi_compcartaporte_ubicacion_destino_id",
                    _dbTable.cfdi_compcartaporte_ubicacion_destino_id.default
                    )
                )

        else:

            _dbRow_mercanciaTransporta = _dbTable(O_form.record_id)

            _D_results = cls.PUEDE_EDITAR(
                x_cfdi_compCartaPorte_mercancia_transporta = _dbRow_mercanciaTransporta,
                )
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                O_form.errors.id = _D_results.s_msgError
            else:
                pass

            _D_camposAChecar.cantidad = (
                O_form.vars.get("cantidad", _dbRow_mercanciaTransporta.cantidad)
                )

            _D_camposAChecar.cfdi_compcartaporte_ubicacion_origen_id = (
                O_form.vars.get(
                    "cfdi_compcartaporte_ubicacion_origen_id",
                    _dbRow_mercanciaTransporta.cfdi_compcartaporte_ubicacion_origen_id
                    )
                )

            _D_camposAChecar.cfdi_compcartaporte_ubicacion_destino_id = (
                O_form.vars.get(
                    "cfdi_compcartaporte_ubicacion_destino_id",
                    _dbRow_mercanciaTransporta.cfdi_compcartaporte_ubicacion_destino_id
                    )
                )

        # Validaciones

        if not _D_camposAChecar.cantidad:
            O_form.errors.cantidad = "Requerido"

        elif _D_camposAChecar.cantidad > _dbRow_cfdi_mercancia.cantidad:
            O_form.errors.cantidad = "Cantidad no puede ser mayor a la cantidad en la mercancía"

        else:
            pass

        if not _D_camposAChecar.cfdi_compcartaporte_ubicacion_origen_id:
            O_form.errors.cfdi_compcartaporte_ubicacion_origen_id = "CP189: Requerido"
        else:
            pass

        if not _D_camposAChecar.cfdi_compcartaporte_ubicacion_destino_id:
            O_form.errors.cfdi_compcartaporte_ubicacion_destino_id = "CP190: Requerido"
        else:
            pass

        if not O_form.errors:
            # Si no hay cliente, el autocalculo debe especificar el RFC genérico
            pass
        else:
            pass

        return O_form

    @classmethod
    def AL_ACEPTAR(
            cls,
            O_form,
            **D_argsMapeoCalculoContabiliza
            ):
        """ Al aceptar el registro deben hacerse los cálculos

        Se hacen todos los cálculos y aceptación de los cambios en el producto

        @param D_argsMapeoCalculoContabiliza:
        @type D_argsMapeoCalculoContabiliza:
        @param O_form:
        @type O_form:
        @return:
        @rtype:
        """
        _ = D_argsMapeoCalculoContabiliza

        _dbRow_mercanciaTransporta = cls.DBTABLA(O_form.record_id or O_form.vars.id)

        _D_results = cls.MAPEAR_CAMPOS(_dbRow_mercanciaTransporta)

        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            O_form.errors.id = _D_results.s_msgError

        else:
            pass

        return O_form

    @classmethod
    def ANTES_ELIMINAR(
            cls,
            L_errorsAction,
            dbRow
            ):
        """ Antes de eliminar un movimiento de inventario de este tipo

        @param dbRow:
        @type dbRow:
        @param L_errorsAction:
        @type L_errorsAction:
        @return: errores
        @rtype: lista de diccionarios {'msg':'...'}
        """
        _D_results = cls.PUEDE_ELIMINAR(
            x_cfdi_compCartaPorte_mercancia_transporta = dbRow
            )
        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            L_errorsAction.append({'msg': _D_results.s_msgError})
        else:
            pass

        return L_errorsAction

    @classmethod
    def DESPUES_ELIMINAR(
            cls,
            L_errorsAction,
            ):
        """ Después de eliminar un registro

        @param L_errorsAction:
        @type L_errorsAction:
        @return: errores
        @rtype: lista de diccionarios {'msg':'...'}
        """

        return L_errorsAction

    @classmethod
    def MAPEAR_CAMPOS(
            cls,
            x_cfdi_compCartaPorte_mercancia_transporta
            ):
        """ Recalcula campos, haciendo la consulta de la carta porte a partir del CFDI

        @param x_cfdi_compCartaPorte_mercancia_transporta:
        @type x_cfdi_compCartaPorte_mercancia_transporta:
        @return:
        @rtype:
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(
            x_cfdi_compCartaPorte_mercancia_transporta,
            cls.DBTABLA()
            )
        _dbRow_mercanciaTransporta = _D_results.dbRow

        _dbRow_mercanciaTransporta.idorigen = dbc01.tcfdi_compcartaporte_ubicaciones(
            _dbRow_mercanciaTransporta.cfdi_compcartaporte_ubicacion_origen_id
            ).origen_idorigen if _dbRow_mercanciaTransporta.cfdi_compcartaporte_ubicacion_origen_id else ""

        _dbRow_mercanciaTransporta.iddestino = dbc01.tcfdi_compcartaporte_ubicaciones(
            _dbRow_mercanciaTransporta.cfdi_compcartaporte_ubicacion_destino_id
            ).destino_iddestino if _dbRow_mercanciaTransporta.cfdi_compcartaporte_ubicacion_destino_id else ""

        _dbRow_mercanciaTransporta.cvetransporte = db01.ttransportes(
            _dbRow_mercanciaTransporta.transporte_id
            ).c_cvetransporte if _dbRow_mercanciaTransporta.transporte_id else ""

        _dbRow_mercanciaTransporta.update_record()

        return _D_return
    pass


dbc01.define_table(
    'tcfdi_compcartaporte_mercancia_transporta',
    Field(
        'cfdi_compcartaporte_mercancia_id', dbc01.tcfdi_compcartaporte_mercancias, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'CFDI Carta Porte - Mercancía', comment = None,
        writable = False, readable = True,
        represent = None
        ),

    # Atributos de nodo CantidadTransporta
    Field(
        'cantidad', 'decimal(16,6)', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputFloat, label = 'Cantidad', comment = None,
        writable = False, readable = True,
        represent = stv_represent_float
        ),
    Field(
        'idorigen', 'string', length = 10, default = None,
        required = False,
        # requires = IS_NULL_OR(IS_MATCH'^OR[0-9]{6}$', error_message='ID origen no es correcto'),
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'ID Origen', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'cfdi_compcartaporte_ubicacion_origen_id', dbc01.tcfdi_compcartaporte_ubicaciones, default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1),
        label = 'Origen', comment = None,
        writable = False, readable = True,
        represent = stv_represent_referencefield
        ),
    Field(
        'iddestino', 'string', length = 10, default = None,
        required = False,
        # requires = IS_NULL_OR(IS_MATCH'^OR[0-9]{6}$', error_message='ID origen no es correcto'),
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'ID Destino', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'cfdi_compcartaporte_ubicacion_destino_id', dbc01.tcfdi_compcartaporte_ubicaciones, default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1),
        label = 'Destino', comment = None,
        writable = False, readable = True,
        represent = stv_represent_referencefield
        ),
    Field(
        'cvetransporte', 'string', length = 5, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Clave transporte', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'transporte_id', 'integer', default = None,
        required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'ttransportes.id', db01.ttransportes._format)),
        notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1),
        label = 'Transporte', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tcfdi_compcartaporte_mercancia_transporta.transporte_id
            )
        ),

    tSignature_dbc01,
    format = '%(calle)s',
    singular = 'Transporta',
    plural = 'Transporta',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )


class TCFDI_COMPCARTAPORTE_AUTOTRANSPORTE:
    """ Tabla para manejar información del autotransporte.

    Es una relación de 1 (Mercancias definida en la tabla cartaporte) a [0,1].
    Se incluye el nodo IdentificacionVehicular por relación 1 a 1.
    Se incluye el nodo remolques por relación 1 a 1. No tiene atributos por lo que no hay campos.
    Se incluye el nodo remolque por relación 1 a [0,2].
    """

    @staticmethod
    def DBTABLA(s_id = None):
        if s_id is None:
            return dbc01.tcfdi_compcartaporte_autotransporte
        else:
            return dbc01.tcfdi_compcartaporte_autotransporte(s_id)

    @classmethod
    def PUEDE_CREAR(
            cls,
            x_cfdi_compCartaPorte
            ):
        """ Determina si puede o no crear un CFDI de traslado.

        Actualmente no hay condición requerida.
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_compCartaPorte, dbc01.tcfdi_compcartaporte)
        _dbRow_cfdi_cartaPorte = _D_results.dbRow

        _D_results = TCFDI_COMPCARTAPORTE.PUEDE_EDITAR(
            x_cfdi_compCartaPorte = _dbRow_cfdi_cartaPorte
            )

        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_results)

        elif _dbRow_cfdi_cartaPorte.tcfdi_compcartaporte_autotransporte.count() >= 1:
            _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
            _D_return.s_msgError = "Solo puede existir un Autotransporte por CFDI"

        else:
            pass

        return _D_return

    @classmethod
    def PUEDE_EDITAR(
            cls,
            x_cfdi_compcartaporte_autotransporte,
            x_cfdi_compCartaPorte
            ):
        """ Define si es posible editar el CFDI de traslado.
        @param x_cfdi_compcartaporte_autotransporte:
        @type x_cfdi_compcartaporte_autotransporte:
        @param x_cfdi_compCartaPorte:
        @type x_cfdi_compCartaPorte:

        """
        _ = x_cfdi_compcartaporte_autotransporte
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _D_results = TCFDI_COMPCARTAPORTE.PUEDE_EDITAR(
            x_cfdi_compCartaPorte = x_cfdi_compCartaPorte
            )

        _D_return.update(_D_results)

        return _D_return

    @classmethod
    def PUEDE_ELIMINAR(
            cls,
            x_cfdi_compcartaporte_autotransporte
            ):
        """ Determina si puede o no eliminar un CFDI de traspaso

        Misma condición que puede_editar

        """
        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(
            x_cfdi_compcartaporte_autotransporte,
            cls.DBTABLA()
            )
        _dbRow_auto = _D_results.dbRow

        _dbRow_cartaporte = dbc01.tcfdi_compcartaporte(_dbRow_auto.cfdi_compcartaporte_id)

        return cls.PUEDE_EDITAR(
            x_cfdi_compcartaporte_autotransporte = _dbRow_auto,
            x_cfdi_compCartaPorte = _dbRow_cartaporte
            )

    @classmethod
    def CONFIGURACAMPOS_NUEVO(
            cls,
            s_cfdi_compCartaPorte_autotransporte_id,
            x_cfdi_compCartaPorte,
            b_ejecutandoEdicion = False
            ):
        """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

        @param b_ejecutandoEdicion: indica si viene de la execución de edicion para omitir validatción de crear
        @type b_ejecutandoEdicion:
        @param x_cfdi_compCartaPorte:
        @type x_cfdi_compCartaPorte:
        @param s_cfdi_compCartaPorte_autotransporte_id:
        @type s_cfdi_compCartaPorte_autotransporte_id:
        @return:
        @rtype:
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTable = cls.DBTABLA()

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_compCartaPorte, dbc01.tcfdi_compcartaporte)
        _dbRow_cfdi_cartaPorte = _D_results.dbRow

        if not b_ejecutandoEdicion:
            _D_results = cls.PUEDE_CREAR(
                x_cfdi_compCartaPorte = _dbRow_cfdi_cartaPorte
                )
        else:
            pass

        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_results)

        elif s_cfdi_compCartaPorte_autotransporte_id:
            _dbRow_base = _dbTable(s_cfdi_compCartaPorte_autotransporte_id)

            # _dbTable.cfdi_compcartaporte_id.default = _dbRow_base.cfdi_compcartaporte_id
            # _dbTable.permsct.default = _dbRow_base.permsct
            _dbTable.transporte_tipopermiso_id.default = _dbRow_base.transporte_tipopermiso_id
            _dbTable.numpermisosct.default = _dbRow_base.numpermisosct
            _dbTable.nombreaseg.default = _dbRow_base.nombreaseg
            _dbTable.numpolizaseguro.default = _dbRow_base.numpolizaseguro
            # _dbTable.idvehic_configvehicular.default = _dbRow_base.idvehic_configvehicular
            _dbTable.idvehic_auto_configautotransporte_id.default = _dbRow_base.idvehic_auto_configautotransporte_id
            _dbTable.idvehic_placavm.default = _dbRow_base.idvehic_placavm
            _dbTable.idvehic_aniomodelovm.default = _dbRow_base.idvehic_aniomodelovm
            # _dbTable.remolque1_subtiporem.default = _dbRow_base.remolque1_subtiporem
            _dbTable.remolque1_auto_configautotransporte_id.default = _dbRow_base.remolque1_auto_configautotransporte_id
            _dbTable.remolque1_placa.default = _dbRow_base.remolque1_placa
            # _dbTable.remolque2_subtiporem.default = _dbRow_base.remolque2_subtiporem
            _dbTable.remolque2_auto_configautotransporte_id.default = _dbRow_base.remolque2_auto_configautotransporte_id
            _dbTable.remolque2_placa.default = _dbRow_base.remolque2_placa
            _dbTable.emp_tra_permiso_id.default = _dbRow_base.emp_tra_permiso_id

        else:
            pass

        _dbTable.cfdi_compcartaporte_id.writable = False
        _dbTable.transporte_tipopermiso_id.writable = True
        _dbTable.numpermisosct.writable = True
        _dbTable.nombreaseg.writable = True
        _dbTable.numpolizaseguro.writable = True
        _dbTable.idvehic_auto_configautotransporte_id.writable = True
        _dbTable.idvehic_placavm.writable = True
        _dbTable.idvehic_aniomodelovm.writable = True
        _dbTable.remolque1_auto_configautotransporte_id.writable = True
        _dbTable.remolque1_placa.writable = True
        _dbTable.remolque2_auto_configautotransporte_id.writable = True
        _dbTable.remolque2_placa.writable = True
        # Sólo toma los datos al crear el registro, si lo esta modificando no permite
        _dbTable.emp_tra_permiso_id.writable = True

        # Campos que no deben ser editados, solo de forma autocalculada
        _dbTable.permsct.writable = False
        _dbTable.idvehic_configvehicular.writable = False
        _dbTable.remolque1_subtiporem.writable = False
        _dbTable.remolque2_subtiporem.writable = False

        # Campos que todos los movimientos pueden editar
        _dbTable.notas.writable = False

        # Se definen los valores por default

        _dbTable.cfdi_compcartaporte_id.default = _dbRow_cfdi_cartaPorte.id
        _dbTable.transporte_tipopermiso_id.default = None
        # _dbTable.numpermisosct.default = ""
        # _dbTable.nombreaseg.default = ""
        # _dbTable.numpolizaseguro.default = ""
        _dbTable.idvehic_auto_configautotransporte_id.default = None
        # _dbTable.idvehic_placavm.default = ""
        # _dbTable.idvehic_aniomodelovm.default = None
        _dbTable.remolque1_auto_configautotransporte_id.default = None
        # _dbTable.remolque1_placa.default = ""
        _dbTable.remolque2_auto_configautotransporte_id.default = None
        # _dbTable.remolque2_placa.default = ""

        return _D_return

    @classmethod
    def CONFIGURACAMPOS_EDICION(
            cls,
            x_cfdi_compcartaporte_autotransporte,
            x_cfdi_compCartaPorte
            ):
        """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

        @param x_cfdi_compcartaporte_autotransporte:
        @type x_cfdi_compcartaporte_autotransporte:
        @param x_cfdi_compCartaPorte:
        @type x_cfdi_compCartaPorte:
        @return:
        @rtype:
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_compCartaPorte, dbc01.tcfdi_compcartaporte)
        _dbRow_cfdi_cartaPorte = _D_results.dbRow

        _D_results = cls.PUEDE_EDITAR(
            x_cfdi_compcartaporte_autotransporte = x_cfdi_compcartaporte_autotransporte,
            x_cfdi_compCartaPorte = _dbRow_cfdi_cartaPorte
            )

        if _D_results.E_return == CLASS_e_RETURN.OK:
            # Si puede editar regresa un ok y el movimiento esta abierto

            # Se manda vacío el argumento para no agregar defaults a los campos
            _D_results = cls.CONFIGURACAMPOS_NUEVO(
                s_cfdi_compCartaPorte_autotransporte_id = None,
                x_cfdi_compCartaPorte = _dbRow_cfdi_cartaPorte,
                b_ejecutandoEdicion = True
                )
            _D_return.update(_D_results)

        else:
            # De lo contrario en cualquier otro caso, no puede editar nada de esta tabla
            pass

        return _D_return

    @classmethod
    def AL_VALIDAR(
            cls,
            O_form,
            x_cfdi_compCartaPorte
            ):
        """ Validar la información para poder grabar cfdi

        @param x_cfdi_compCartaPorte:
        @type x_cfdi_compCartaPorte:
        @param O_form:
        @type O_form:
        @return:
        @rtype:
        """
        _dbTable = cls.DBTABLA()

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_compCartaPorte, dbc01.tcfdi_compcartaporte)
        _dbRow_cfdi_cartaPorte = _D_results.dbRow

        _D_camposAChecar = Storage()  # Define los campos que deben ser verificados

        # Dependiendo si es nuevo o editado, toma los valores a validar
        if not O_form.record_id:
            # Si esta creando el registro:

            _D_results = cls.PUEDE_CREAR(
                x_cfdi_compCartaPorte = _dbRow_cfdi_cartaPorte
                )
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                O_form.errors.id = _D_results.s_msgError
            else:
                pass

            # _D_camposAChecar.prodservcp_id = (
            #     O_form.vars.get("prodservcp_id", _dbTable.prodservcp_id.default)
            #     )

        else:

            _dbRow_auto = _dbTable(O_form.record_id)

            _D_results = cls.PUEDE_EDITAR(
                x_cfdi_compcartaporte_autotransporte = _dbRow_auto,
                x_cfdi_compCartaPorte = _dbRow_cfdi_cartaPorte
                )
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                O_form.errors.id = _D_results.s_msgError
            else:
                pass

            # _D_camposAChecar.prodservcp_id = (
            #     O_form.vars.get("prodservcp_id", _dbRow_mercancia.prodservcp_id)
            #     )

        # Validaciones

        # if not _D_camposAChecar.pesoenkg:
        #     O_form.errors.pesoenkg = "Requerido"
        # else:
        #     pass

        if not O_form.errors:
            # Si no hay cliente, el autocalculo debe especificar el RFC genérico
            pass
        else:
            pass

        return O_form

    @classmethod
    def AL_ACEPTAR(
            cls,
            O_form,
            x_cfdi_compCartaPorte,
            **D_argsMapeoCalculoContabiliza
            ):
        """ Al aceptar el registro deben hacerse los cálculos

        Se hacen todos los cálculos y aceptación de los cambios en el producto

        @param x_cfdi_compCartaPorte:
        @type x_cfdi_compCartaPorte:
        @param D_argsMapeoCalculoContabiliza:
        @type D_argsMapeoCalculoContabiliza:
        @param O_form:
        @type O_form:
        @return:
        @rtype:
        """
        _ = D_argsMapeoCalculoContabiliza

        _dbRow_autotransporte = cls.DBTABLA(O_form.record_id or O_form.vars.id)

        _D_results = cls.MAPEAR_CAMPOS(_dbRow_autotransporte)

        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            O_form.errors.id = _D_results.s_msgError

        else:
            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_compCartaPorte, dbc01.tcfdi_compcartaporte)
            _dbRow_cfdi_cartaPorte = _D_results.dbRow

            _D_results = TCFDI_COMPCARTAPORTE.CALCULAR_CAMPOS(
                x_cfdi_compCartaPorte = _dbRow_cfdi_cartaPorte
                )

            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                O_form.errors.id = _D_results.s_msgError
            else:
                pass

        return O_form

    @classmethod
    def ANTES_ELIMINAR(
            cls,
            L_errorsAction,
            dbRow
            ):
        """ Antes de eliminar un movimiento de inventario de este tipo

        @param dbRow:
        @type dbRow:
        @param L_errorsAction:
        @type L_errorsAction:
        @return: errores
        @rtype: lista de diccionarios {'msg':'...'}
        """
        _D_results = cls.PUEDE_ELIMINAR(
            x_cfdi_compcartaporte_autotransporte = dbRow
            )
        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            L_errorsAction.append({'msg': _D_results.s_msgError})
        else:
            pass

        return L_errorsAction

    @classmethod
    def MAPEAR_CAMPOS(
            cls,
            x_cfdi_compcartaporte_autotransporte
            ):
        """ Recalcula campos, haciendo la consulta de la carta porte a partir del CFDI

        @param x_cfdi_compcartaporte_autotransporte:
        @type x_cfdi_compcartaporte_autotransporte:
        @return:
        @rtype:
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(
            x_cfdi_compcartaporte_autotransporte,
            cls.DBTABLA()
            )
        _dbRow_autotransporte = _D_results.dbRow

        if _dbRow_autotransporte.emp_tra_permiso_id:
            _dbRow_empTraPermiso = dbc01.temp_tra_permisos(_dbRow_autotransporte.emp_tra_permiso_id)
            _dbRow_empTransporte = dbc01.tempresa_transportes(_dbRow_empTraPermiso.emp_transporte_id)

            _dbRow_autotransporte.idvehic_auto_configautotransporte_id = (
                _dbRow_empTransporte.idvehic_auto_configautotransporte_id
                )
            _dbRow_autotransporte.idvehic_placavm = (
                _dbRow_empTransporte.idvehic_placavm
                )
            _dbRow_autotransporte.idvehic_aniomodelovm = (
                _dbRow_empTransporte.idvehic_aniomodelovm
                )

            _dbRow_autotransporte.transporte_tipopermiso_id = (
                _dbRow_empTraPermiso.transporte_tipopermiso_id
                )
            _dbRow_autotransporte.numpermisosct = (
                _dbRow_empTraPermiso.numpermisosct
                )

            _dbRow_autotransporte.nombreaseg = (
                _dbRow_empTransporte.nombreaseg
                )
            _dbRow_autotransporte.numpolizaseguro = (
                _dbRow_empTransporte.numpolizaseguro
                )

            if _dbRow_empTransporte.pertenencia == TEMPRESA_TRANSPORTES.E_PERTENENCIA.PROPIO:
                # No se requieren agregar personas en TCFDI_COMPCARTAPORTE_FIGURATRANPORTEPERSONAS
                pass

            elif _dbRow_empTransporte.pertenencia == TEMPRESA_TRANSPORTES.E_PERTENENCIA.ARRENDADO:
                _dbTabla_personas = dbc01.tcfdi_compcartaporte_figurastransporepersonas

                if _dbRow_empTransporte.arrendador_rfc:
                    _D_informacionArrendatario = Storage(
                        cfdi_compcartaporte_id = _dbRow_autotransporte.cfdi_compcartaporte_id,
                        tipofiguratransporte = (
                            TCFDI_COMPCARTAPORTE_FIGURATRANPORTEPERSONAS.E_TIPOFIGURATRANSPORTE.ARRENDATARIO
                            ),
                        rfc = _dbRow_empTransporte.arrendador_rfc,
                        nombre = _dbRow_empTransporte.arrendador_nombre,
                        numregidtriboperador = _dbRow_empTransporte.arrendador_numregidtriboperador,
                        residencia_pais_id = _dbRow_empTransporte.arrendador_residencia_pais_id,
                        dom_calle = _dbRow_empTransporte.arrendador_calle,
                        dom_numexterior = _dbRow_empTransporte.arrendador_numexterior,
                        dom_numinterior = _dbRow_empTransporte.arrendador_numinterior,
                        dom_colonia = _dbRow_empTransporte.arrendador_colonia,
                        dom_localidad = _dbRow_empTransporte.arrendador_localidad,
                        dom_referencia = _dbRow_empTransporte.arrendador_referencia,
                        dom_municipio = _dbRow_empTransporte.arrendador_municipio,
                        dom_pais_estado_id = _dbRow_empTransporte.arrendador_pais_estado_id,
                        dom_pais_id = _dbRow_empTransporte.arrendador_pais_id,
                        dom_codigopostal = _dbRow_empTransporte.arrendador_codigopostal,
                        )

                    _dbRows_personas_arrendatarios = dbc01(
                        (_dbTabla_personas.cfdi_compcartaporte_id == _dbRow_autotransporte.cfdi_compcartaporte_id)
                        & (
                            _dbTabla_personas.tipofiguratransporte
                            == TCFDI_COMPCARTAPORTE_FIGURATRANPORTEPERSONAS.E_TIPOFIGURATRANSPORTE.ARRENDATARIO
                            )
                        & (_dbTabla_personas.rfc == _dbRow_empTransporte.arrendador_rfc)
                        ).select(
                            _dbTabla_personas.ALL
                            )

                    _x_persona = None
                    if _dbRows_personas_arrendatarios:
                        _dbRow_persona = _dbRows_personas_arrendatarios.first()
                        _dbRow_persona.update_record(**_D_informacionArrendatario)
                        _x_persona = _dbRow_persona
                    else:
                        _x_persona = _dbTabla_personas.insert(**_D_informacionArrendatario)

                    TCFDI_COMPCARTAPORTE_FIGURATRANPORTEPERSONAS.MAPEAR_CAMPOS(
                        x_cfdi_compcartaporte_figtranspersona = _x_persona
                        )

                else:
                    # No existe el RFC del arrendador para poder llenarlo automáticamente
                    pass

                if _dbRow_empTransporte.propietario_rfc:

                    _D_informacionPropietario = Storage(
                        cfdi_compcartaporte_id = _dbRow_autotransporte.cfdi_compcartaporte_id,
                        tipofiguratransporte = (
                            TCFDI_COMPCARTAPORTE_FIGURATRANPORTEPERSONAS.E_TIPOFIGURATRANSPORTE.PROPIETARIO
                            ),
                        rfc = _dbRow_empTransporte.propietario_rfc,
                        nombre = _dbRow_empTransporte.propietario_nombre,
                        numregidtriboperador = _dbRow_empTransporte.propietario_numregidtriboperador,
                        residencia_pais_id = _dbRow_empTransporte.propietario_residencia_pais_id,
                        dom_calle = _dbRow_empTransporte.propietario_calle,
                        dom_numexterior = _dbRow_empTransporte.propietario_numexterior,
                        dom_numinterior = _dbRow_empTransporte.propietario_numinterior,
                        dom_colonia = _dbRow_empTransporte.propietario_colonia,
                        dom_localidad = _dbRow_empTransporte.propietario_localidad,
                        dom_referencia = _dbRow_empTransporte.propietario_referencia,
                        dom_municipio = _dbRow_empTransporte.propietario_municipio,
                        dom_pais_estado_id = _dbRow_empTransporte.propietario_pais_estado_id,
                        dom_pais_id = _dbRow_empTransporte.propietario_pais_id,
                        dom_codigopostal = _dbRow_empTransporte.propietario_codigopostal,
                        )

                    _dbRows_personas_propietarios = dbc01(
                        (_dbTabla_personas.cfdi_compcartaporte_id == _dbRow_autotransporte.cfdi_compcartaporte_id)
                        & (
                            _dbTabla_personas.tipofiguratransporte
                            == TCFDI_COMPCARTAPORTE_FIGURATRANPORTEPERSONAS.E_TIPOFIGURATRANSPORTE.PROPIETARIO
                            )
                        & (_dbTabla_personas.rfc == _dbRow_empTransporte.propietario_rfc)
                        ).select(
                            _dbTabla_personas.ALL
                            )
                    _x_persona = None
                    if _dbRows_personas_propietarios:
                        _dbRow_persona = _dbRows_personas_propietarios.first()
                        _dbRow_persona.update_record(**_D_informacionPropietario)
                        _x_persona = _dbRow_persona
                    else:
                        _x_persona = _dbTabla_personas.insert(**_D_informacionPropietario)

                    TCFDI_COMPCARTAPORTE_FIGURATRANPORTEPERSONAS.MAPEAR_CAMPOS(
                        x_cfdi_compcartaporte_figtranspersona = _x_persona
                        )
                else:
                    # No existe el RFC del propietario para poder llenarlo automáticamente
                    pass

            else:
                # No se identifica la pertenencia en el vehículo por lo que no se toma ninguna acción
                pass

        else:
            # No existe el emp_tra_permiso_id para poder ser llenado automático
            pass

        _dbRow_autotransporte.permsct = db01.ttransporte_tipospermisos(
            _dbRow_autotransporte.transporte_tipopermiso_id
            ).c_tipopermiso if _dbRow_autotransporte.transporte_tipopermiso_id else ""

        _dbRow_autotransporte.idvehic_configvehicular = db01.tauto_configautotransportes(
            _dbRow_autotransporte.idvehic_auto_configautotransporte_id
            ).c_configautotransporte if _dbRow_autotransporte.idvehic_auto_configautotransporte_id else ""

        _dbRow_autotransporte.remolque1_subtiporem = db01.tauto_tiposremolques(
            _dbRow_autotransporte.remolque1_auto_configautotransporte_id
            ).c_subtiporem if _dbRow_autotransporte.remolque1_auto_configautotransporte_id else ""
        _dbRow_autotransporte.remolque2_subtiporem = db01.tauto_tiposremolques(
            _dbRow_autotransporte.remolque2_auto_configautotransporte_id
            ).c_subtiporem if _dbRow_autotransporte.remolque2_auto_configautotransporte_id else ""

        _dbRow_autotransporte.update_record()

        return _D_return

    pass


dbc01.define_table(
    'tcfdi_compcartaporte_autotransporte',
    Field(
        'cfdi_compcartaporte_id', dbc01.tcfdi_compcartaporte, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'CFDI Carta Porte', comment = None,
        writable = False, readable = True,
        represent = None
        ),

    # Atributos de nodo transporte
    Field(
        'permsct', 'string', length = 30, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Permiso SCT', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'transporte_tipopermiso_id', 'integer', default = None,
        required = False, requires = IS_NULL_OR(
            IS_IN_DB(
                db01(db01.ttransporte_tipospermisos.transporte_id == TTRANSPORTES.AUTO),
                'ttransporte_tipospermisos.id',
                db01.ttransporte_tipospermisos._format
                )
            ),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Tipo Permiso',
        comment = 'Identifica el tipo de permiso',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tcfdi_compcartaporte_autotransporte.transporte_tipopermiso_id
            )
        ),
    Field(
        'numpermisosct', 'string', length = 60, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Num. Permiso SCT', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'nombreaseg', 'string', length = 100, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Nombre Aseguradora', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'numpolizaseguro', 'string', length = 100, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Num. Poliza Seguro', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),

    # Atributos del nodo identificacionVehicular
    Field(
        'idvehic_configvehicular', 'string', length = 30, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Conf. Vehicular', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'idvehic_auto_configautotransporte_id', 'integer', default = None,
        required = False, requires = IS_NULL_OR(
            IS_IN_DB(db01, 'tauto_configautotransportes.id', db01.tauto_configautotransportes._format)
            ),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Conf. Vehicular', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tcfdi_compcartaporte_autotransporte.idvehic_auto_configautotransporte_id
            )
        ),
    Field(
        'idvehic_placavm', 'string', length = 60, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Placa Vehicular', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'idvehic_aniomodelovm', 'integer',
        required = False, default = int(request.browsernow.year),
        notnull = False, unique = False,
        widget = stv_widget_inputYear, label = 'Año Vehículo', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),

    # Atributos del nodo remolque
    Field(
        'remolque1_subtiporem', 'string', length = 30, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Subtipo Remolque 1', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'remolque1_auto_configautotransporte_id', 'integer', default = None,
        required = False, requires = IS_NULL_OR(
            IS_IN_DB(db01, 'tauto_tiposremolques.id', db01.tauto_tiposremolques._format)
            ),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Subtipo Remolque 1', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tcfdi_compcartaporte_autotransporte.remolque1_auto_configautotransporte_id
            )
        ),
    Field(
        'remolque1_placa', 'string', length = 60, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Placa 1', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),

    Field(
        'remolque2_subtiporem', 'string', length = 30, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Subtipo Remolque 2', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'remolque2_auto_configautotransporte_id', 'integer', default = None,
        required = False, requires = IS_NULL_OR(
            IS_IN_DB(db01, 'tauto_tiposremolques.id', db01.tauto_tiposremolques._format)
            ),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Subtipo Remolque 2', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tcfdi_compcartaporte_autotransporte.remolque2_auto_configautotransporte_id
            )
        ),
    Field(
        'remolque2_placa', 'string', length = 60, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Placa 2', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'emp_tra_permiso_id', dbc01.temp_tra_permisos, default = None,
        required = False,
        requires = IS_NULL_OR(
            IS_IN_DB(
                dbc01(
                    (dbc01.tempresa_transportes.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id)
                    & (dbc01.temp_tra_permisos.emp_transporte_id == dbc01.tempresa_transportes.id)
                    ),
                'temp_tra_permisos.id',
                dbc01.temp_tra_permisos._format
                )
            ),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Transporte/Permiso', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r)
        ),

    tSignature_dbc01,
    format = '%(calle)s',
    singular = 'Autotransporte',
    plural = 'Autotransportes',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )


class TCFDI_COMPCARTAPORTE_TRANSPORTEMARITIMO:
    """
    - Solo puede ser un registro
    """
    pass
# TODOMejora TCFDI_COMPCARTAPORTE_TRANSPORTEMARITIMO


class TCFDI_COMPCARTAPORTE_TRANSPORTEMARITIMO_CONTENEDORES:
    """
    N registros
    """
    pass
# TODOMejora TCFDI_COMPCARTAPORTE_TRANSPORTEMARITIMO_CONTENEDORES


class TCFDI_COMPCARTAPORTE_TRANSPORTEAEREO:
    """
    - Solo puede ser un registro
    """
    pass
# TODOMejora TCFDI_COMPCARTAPORTE_TRANSPORTEAEREO


class TCFDI_COMPCARTAPORTE_TRANSPORTEFERREOVIARIO:
    """
    - Solo puede ser un registro
    """
    pass
# TODOMejora TCFDI_COMPCARTAPORTE_TRANSPORTEFERREOVIARIO


class TCFDI_COMPCARTAPORTE_TRANSPORTEFERREOVIARIO_DERECHOSPASO:
    """
    N registros
    """
    pass
# TODOMejora TCFDI_COMPCARTAPORTE_TRANSPORTEFERREOVIARIO_DERECHOSPASO


class TCFDI_COMPCARTAPORTE_TRANSPORTEFERREOVIARIO_CARROS:
    """
    N registros
    """
    pass
# TODOMejora TCFDI_COMPCARTAPORTE_TRANSPORTEFERREOVIARIO_CARROS


class TCFDI_COMPCARTAPORTE_TRANSPORTEFERREOVIARIO_CARRO_CONTENEDORES:
    """
    N registros
    """
    pass
# TODOMejora TCFDI_COMPCARTAPORTE_TRANSPORTEFERREOVIARIO_CARRO_CONTENEDORES


class TCFDI_COMPCARTAPORTE_FIGURATRANPORTEPERSONAS:
    """
    Es una relación de 1 (FiguraTransporte definida en la tabla cartaporte) a [0,1].
    Se tratan los nodos Operadores, Propietario, Arrendatario, Notificado en la misma tabla [0, Inf]
    Se incluyen atributos del nodo Domicilio relacion de 1 a [0,1]
    Como usan la misma información los nodos, se diferencian con el valor del campo tipofiguratransporte
    """

    @staticmethod
    def DBTABLA(s_id = None):
        if s_id is None:
            return dbc01.tcfdi_compcartaporte_figurastransporepersonas
        else:
            return dbc01.tcfdi_compcartaporte_figurastransporepersonas(s_id)

    class E_TIPOFIGURATRANSPORTE:
        NO_DEFINIDO = 0
        OPERADOR = 1
        PROPIETARIO = 2
        ARRENDATARIO = 3
        NOTIFICADO = 4
        _D_dict = None

        @classmethod
        def GET_DICT(cls):
            if not cls._D_dict:
                cls._D_dict = {
                    # cls.NO_DEFINIDO : 'No Definido',
                    cls.OPERADOR    : 'Operador',
                    cls.PROPIETARIO : 'Propietario',
                    cls.ARRENDATARIO: 'Arrendatario',
                    cls.NOTIFICADO  : 'Notificado',
                    }
            else:
                pass
            return cls._D_dict

    @classmethod
    def PUEDE_CREAR(
            cls,
            x_cfdi_compCartaPorte
            ):
        """ Determina si puede o no crear un CFDI de traslado.

        Actualmente no hay condición requerida.
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _D_results = TCFDI_COMPCARTAPORTE.PUEDE_EDITAR(
            x_cfdi_compCartaPorte = x_cfdi_compCartaPorte
            )

        _D_return.update(_D_results)

        return _D_return

    @classmethod
    def PUEDE_EDITAR(
            cls,
            x_cfdi_compcartaporte_figtranspersona,
            x_cfdi_compCartaPorte
            ):
        """ Define si es posible editar el CFDI de traslado.
        @param x_cfdi_compcartaporte_figtranspersona:
        @type x_cfdi_compcartaporte_figtranspersona:
        @param x_cfdi_compCartaPorte:
        @type x_cfdi_compCartaPorte:

        """
        _ = x_cfdi_compcartaporte_figtranspersona
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _D_results = TCFDI_COMPCARTAPORTE.PUEDE_EDITAR(
            x_cfdi_compCartaPorte = x_cfdi_compCartaPorte
            )

        _D_return.update(_D_results)

        return _D_return

    @classmethod
    def PUEDE_ELIMINAR(
            cls,
            x_cfdi_compcartaporte_figtranspersona
            ):
        """ Determina si puede o no eliminar un CFDI de traspaso

        Misma condición que puede_editar

        """
        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(
            x_cfdi_compcartaporte_figtranspersona,
            cls.DBTABLA()
            )
        _dbRow_persona = _D_results.dbRow

        _dbRow_cartaporte = dbc01.tcfdi_compcartaporte(_dbRow_persona.cfdi_compcartaporte_id)

        return cls.PUEDE_EDITAR(
            x_cfdi_compcartaporte_figtranspersona = _dbRow_persona,
            x_cfdi_compCartaPorte = _dbRow_cartaporte
            )

    @classmethod
    def CONFIGURACAMPOS_NUEVO(
            cls,
            s_cfdi_compCartaPorte_figtranspersona_id,
            x_cfdi_compCartaPorte
            ):
        """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

        @param s_cfdi_compCartaPorte_figtranspersona_id:
        @type s_cfdi_compCartaPorte_figtranspersona_id:
        @param x_cfdi_compCartaPorte:
        @type x_cfdi_compCartaPorte:
        @return:
        @rtype:
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTable = cls.DBTABLA()

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_compCartaPorte, dbc01.tcfdi_compcartaporte)
        _dbRow_cfdi_cartaPorte = _D_results.dbRow

        _D_results = cls.PUEDE_CREAR(
            x_cfdi_compCartaPorte = _dbRow_cfdi_cartaPorte
            )

        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_results)

        elif s_cfdi_compCartaPorte_figtranspersona_id:
            _dbRow_base = _dbTable(s_cfdi_compCartaPorte_figtranspersona_id)

            # _dbTable.cfdi_compcartaporte_id.default = _dbRow_base.cfdi_compcartaporte_id
            _dbTable.tipofiguratransporte.default = _dbRow_base.tipofiguratransporte
            _dbTable.rfc.default = _dbRow_base.rfc
            _dbTable.numlicencia.default = _dbRow_base.numlicencia
            _dbTable.nombre.default = _dbRow_base.nombre
            _dbTable.numregidtriboperador.default = _dbRow_base.numregidtriboperador
            # _dbTable.residenciafiscal.default = _dbRow_base.residencia_pais_id
            _dbTable.residencia_pais_id.default = _dbRow_base.residencia_pais_id
            _dbTable.dom_calle.default = _dbRow_base.dom_calle
            _dbTable.dom_numexterior.default = _dbRow_base.dom_numexterior
            _dbTable.dom_numinterior.default = _dbRow_base.dom_numinterior
            _dbTable.dom_colonia.default = _dbRow_base.dom_colonia
            _dbTable.dom_localidad.default = _dbRow_base.dom_localidad
            _dbTable.dom_referencia.default = _dbRow_base.dom_referencia
            _dbTable.dom_municipio.default = _dbRow_base.dom_municipio
            # _dbTable.dom_estado.default = _dbRow_base.dom_municipio
            _dbTable.dom_pais_estado_id.default = _dbRow_base.dom_municipio
            # _dbTable.dom_pais.default = _dbRow_base.dom_municipio
            _dbTable.dom_pais_id.default = _dbRow_base.dom_pais_id
            _dbTable.dom_codigopostal.default = _dbRow_base.dom_codigopostal
            # _dbTable.operador_id.default = _dbRow_base.operador_id

        else:
            pass

        _dbTable.cfdi_compcartaporte_id.writable = False
        _dbTable.tipofiguratransporte.writable = True
        _dbTable.rfc.writable = True
        _dbTable.numlicencia.writable = True
        _dbTable.nombre.writable = True
        _dbTable.numregidtriboperador.writable = True  # En caso de operador extranjero
        _dbTable.residencia_pais_id.writable = True  # En caso de operador extranjero
        _dbTable.dom_calle.writable = True
        _dbTable.dom_numexterior.writable = True
        _dbTable.dom_numinterior.writable = True
        _dbTable.dom_colonia.writable = True
        _dbTable.dom_localidad.writable = True
        _dbTable.dom_referencia.writable = True
        _dbTable.dom_municipio.writable = True
        _dbTable.dom_pais_estado_id.writable = True
        _dbTable.dom_pais_id.writable = True
        _dbTable.dom_codigopostal.writable = True
        _dbTable.operador_id.writable = True

        # Campos que no deben ser editados, solo de forma autocalculada
        _dbTable.residenciafiscal.writable = False
        _dbTable.dom_estado.writable = False
        _dbTable.dom_pais.writable = False

        # Campos que todos los movimientos pueden editar
        _dbTable.notas.writable = True

        # Se definen los valores por default

        _dbTable.cfdi_compcartaporte_id.default = _dbRow_cfdi_cartaPorte.id
        _dbTable.tipofiguratransporte.default = (
            TCFDI_COMPCARTAPORTE_FIGURATRANPORTEPERSONAS.E_TIPOFIGURATRANSPORTE.OPERADOR
            )
        # _dbTable.rfc.default = ""
        # _dbTable.numlicencia.default = ""
        # _dbTable.nombre.default = ""
        # _dbTable.numregidtriboperador.default = ""
        _dbTable.residencia_pais_id.default = TPAISES.MEX
        # _dbTable.dom_calle.default = ""
        # _dbTable.dom_numexterior.default = ""
        # _dbTable.dom_numinterior.default = ""
        # _dbTable.dom_colonia.default = ""
        # _dbTable.dom_localidad.default = ""
        # _dbTable.dom_referencia.default = ""
        # _dbTable.dom_municipio.default = ""
        _dbTable.dom_pais_estado_id.default = TPAIS_ESTADOS.SIN
        _dbTable.dom_pais_id.default = TPAISES.MEX
        # _dbTable.dom_codigopostal.default = ""
        # _dbTable.operador_id.default = None

        return _D_return

    @classmethod
    def CONFIGURACAMPOS_EDICION(
            cls,
            x_cfdi_compcartaporte_figtranspersona,
            x_cfdi_compCartaPorte
            ):
        """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

        @param x_cfdi_compcartaporte_figtranspersona:
        @type x_cfdi_compcartaporte_figtranspersona:
        @param x_cfdi_compCartaPorte:
        @type x_cfdi_compCartaPorte:
        @return:
        @rtype:
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_compCartaPorte, dbc01.tcfdi_compcartaporte)
        _dbRow_cfdi_cartaPorte = _D_results.dbRow

        _D_results = cls.PUEDE_EDITAR(
            x_cfdi_compcartaporte_figtranspersona = x_cfdi_compcartaporte_figtranspersona,
            x_cfdi_compCartaPorte = _dbRow_cfdi_cartaPorte
            )

        if _D_results.E_return == CLASS_e_RETURN.OK:
            # Si puede editar regresa un ok y el movimiento esta abierto

            # Se manda vacío el argumento para no agregar defaults a los campos
            _D_results = cls.CONFIGURACAMPOS_NUEVO(
                s_cfdi_compCartaPorte_figtranspersona_id = None,
                x_cfdi_compCartaPorte = _dbRow_cfdi_cartaPorte
                )
            _D_return.update(_D_results)

        else:
            # De lo contrario en cualquier otro caso, no puede editar nada de esta tabla
            pass

        return _D_return

    @classmethod
    def AL_VALIDAR(
            cls,
            O_form,
            x_cfdi_compCartaPorte
            ):
        """ Validar la información para poder grabar cfdi

        @param x_cfdi_compCartaPorte:
        @type x_cfdi_compCartaPorte:
        @param O_form:
        @type O_form:
        @return:
        @rtype:
        """
        _dbTable = cls.DBTABLA()

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_compCartaPorte, dbc01.tcfdi_compcartaporte)
        _dbRow_cfdi_cartaPorte = _D_results.dbRow

        _D_camposAChecar = Storage()  # Define los campos que deben ser verificados

        # Dependiendo si es nuevo o editado, toma los valores a validar
        if not O_form.record_id:
            # Si esta creando el registro:

            _D_results = cls.PUEDE_CREAR(
                x_cfdi_compCartaPorte = _dbRow_cfdi_cartaPorte
                )
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                O_form.errors.id = _D_results.s_msgError
            else:
                pass

            # _D_camposAChecar.prodservcp_id = (
            #     O_form.vars.get("prodservcp_id", _dbTable.prodservcp_id.default)
            #     )

        else:

            _dbRow_persona = _dbTable(O_form.record_id)

            _D_results = cls.PUEDE_EDITAR(
                x_cfdi_compcartaporte_figtranspersona = _dbRow_persona,
                x_cfdi_compCartaPorte = _dbRow_cfdi_cartaPorte
                )
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                O_form.errors.id = _D_results.s_msgError
            else:
                pass

            # _D_camposAChecar.prodservcp_id = (
            #     O_form.vars.get("prodservcp_id", _dbRow_mercancia.prodservcp_id)
            #     )

        # Validaciones

        # if not _D_camposAChecar.pesoenkg:
        #     O_form.errors.pesoenkg = "Requerido"
        # else:
        #     pass

        if not O_form.errors:
            # Si no hay cliente, el autocalculo debe especificar el RFC genérico
            pass
        else:
            pass

        return O_form

    @classmethod
    def AL_ACEPTAR(
            cls,
            O_form,
            x_cfdi_compCartaPorte,
            **D_argsMapeoCalculoContabiliza
            ):
        """ Al aceptar el registro deben hacerse los cálculos

        Se hacen todos los cálculos y aceptación de los cambios en el producto

        @param x_cfdi_compCartaPorte:
        @type x_cfdi_compCartaPorte:
        @param D_argsMapeoCalculoContabiliza:
        @type D_argsMapeoCalculoContabiliza:
        @param O_form:
        @type O_form:
        @return:
        @rtype:
        """
        _ = D_argsMapeoCalculoContabiliza

        _dbRow_autotransporte = cls.DBTABLA(O_form.record_id or O_form.vars.id)

        _D_results = cls.MAPEAR_CAMPOS(_dbRow_autotransporte)

        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            O_form.errors.id = _D_results.s_msgError

        else:
            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_compCartaPorte, dbc01.tcfdi_compcartaporte)
            _dbRow_cfdi_cartaPorte = _D_results.dbRow

            _D_results = TCFDI_COMPCARTAPORTE.CALCULAR_CAMPOS(
                x_cfdi_compCartaPorte = _dbRow_cfdi_cartaPorte
                )

            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                O_form.errors.id = _D_results.s_msgError
            else:
                pass

        return O_form

    @classmethod
    def ANTES_ELIMINAR(
            cls,
            L_errorsAction,
            dbRow
            ):
        """ Antes de eliminar un movimiento de inventario de este tipo

        @param dbRow:
        @type dbRow:
        @param L_errorsAction:
        @type L_errorsAction:
        @return: errores
        @rtype: lista de diccionarios {'msg':'...'}
        """
        _D_results = cls.PUEDE_ELIMINAR(
            x_cfdi_compcartaporte_figtranspersona = dbRow
            )
        if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
            L_errorsAction.append({'msg': _D_results.s_msgError})
        else:
            pass

        return L_errorsAction

    @classmethod
    def MAPEAR_CAMPOS(
            cls,
            x_cfdi_compcartaporte_figtranspersona
            ):
        """ Recalcula campos, haciendo la consulta de la carta porte a partir del CFDI

        @param x_cfdi_compcartaporte_figtranspersona:
        @type x_cfdi_compcartaporte_figtranspersona:
        @return:
        @rtype:
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            )

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(
            x_cfdi_compcartaporte_figtranspersona,
            cls.DBTABLA()
            )
        _dbRow_persona = _D_results.dbRow

        if _dbRow_persona.operador_id:
            _dbRow_operador = dbc01.tempresa_operadores(_dbRow_persona.operador_id)

            _dbRow_persona.rfc = _dbRow_operador.rfc
            _dbRow_persona.numlicencia = _dbRow_operador.numlicencia
            _dbRow_persona.nombre = _dbRow_operador.nombre
            _dbRow_persona.numregidtriboperador = _dbRow_operador.numregidtriboperador
            _dbRow_persona.residencia_pais_id = _dbRow_operador.residencia_pais_id
            _dbRow_persona.dom_calle = _dbRow_operador.dom_calle
            _dbRow_persona.dom_numexterior = _dbRow_operador.dom_numexterior
            _dbRow_persona.dom_numinterior = _dbRow_operador.dom_numinterior
            _dbRow_persona.dom_colonia = _dbRow_operador.dom_colonia
            _dbRow_persona.dom_localidad = _dbRow_operador.dom_localidad
            _dbRow_persona.dom_referencia = _dbRow_operador.dom_referencia
            _dbRow_persona.dom_municipio = _dbRow_operador.dom_municipio
            _dbRow_persona.dom_pais_estado_id = _dbRow_operador.dom_pais_estado_id
            _dbRow_persona.dom_pais_id = _dbRow_operador.dom_pais_id
            _dbRow_persona.dom_codigopostal = _dbRow_operador.dom_codigopostal

        else:
            # No existe el transporte_id para poder ser llenado automático
            pass

        _dbRow_persona.residenciafiscal = db01.tpaises(
            _dbRow_persona.residencia_pais_id
            ).c_pais if _dbRow_persona.residencia_pais_id else ""

        _dbRow_persona.dom_estado = db01.tpais_estados(
            _dbRow_persona.dom_pais_estado_id
            ).c_estado if _dbRow_persona.dom_pais_estado_id else ""

        _dbRow_persona.dom_pais = db01.tpaises(
            _dbRow_persona.dom_pais_id
            ).c_pais if _dbRow_persona.dom_pais_id else ""

        _dbRow_persona.update_record()

        return _D_return

    pass


dbc01.define_table(
    'tcfdi_compcartaporte_figurastransporepersonas',
    Field(
        'cfdi_compcartaporte_id', dbc01.tcfdi_compcartaporte, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'CFDI Carta Porte', comment = None,
        writable = False, readable = True,
        represent = None
        ),

    Field(
        'tipofiguratransporte', 'integer',
        default = TCFDI_COMPCARTAPORTE_FIGURATRANPORTEPERSONAS.E_TIPOFIGURATRANSPORTE.OPERADOR,
        required = True, requires = IS_IN_SET(
            TCFDI_COMPCARTAPORTE_FIGURATRANPORTEPERSONAS.E_TIPOFIGURATRANSPORTE.GET_DICT(), zero = 0,
            error_message = 'Selecciona'
            ),
        notnull = False, unique = False,
        widget = stv_widget_combobox, label = 'Tipo Figura Transporte', comment = '',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_list(
            v, r, None, TCFDI_COMPCARTAPORTE_FIGURATRANPORTEPERSONAS.E_TIPOFIGURATRANSPORTE.GET_DICT()
            )
        ),

    Field(
        'rfc', 'string', length = 15, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'RFC', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'numlicencia', 'string', length = 50, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Num. Licencia', comment = 'Solo requerido si es operador',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'nombre', 'string', length = 254, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Nombre', comment = '',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'numregidtriboperador', 'string', length = 50, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Num. Id en País',
        comment = (
            'Número de identificación o registro fiscal del país de residencia para los efectos fiscales del '
            + 'operador del autotransporte de carga federal en el que se trasladan los bienes o mercancías, '
            + 'cuando sea residente en el extranjero.'
            ),
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'residenciafiscal', 'string', length = 30, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Recidencia Fiscal', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'residencia_pais_id', 'integer', default = TPAISES.MEX,
        required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tpaises.id', db01.tpaises._format)),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Residencia',
        comment = 'Residencia fiscal',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tcfdi_compcartaporte_figurastransporepersonas.residencia_pais_id
            )
        ),

    # Atributos del nodo domicilio
    Field(
        'dom_calle', 'string', length = 200, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Calle', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'dom_numexterior', 'string', length = 55, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Num. Exterior', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'dom_numinterior', 'string', length = 55, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Num. Interior', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'dom_colonia', 'string', length = 120, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Colonia', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'dom_localidad', 'string', length = 120, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Localidad', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'dom_referencia', 'string', length = 250, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Referencia', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'dom_municipio', 'string', length = 120, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Municipio', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'dom_estado', 'string', length = 30, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Estado', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'dom_pais_estado_id', 'integer', default = TPAIS_ESTADOS.SIN,
        required = False, requires = IS_IN_DB(db01, 'tpais_estados.id', db01.tpais_estados._format),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda field, value: stv_widget_db_chosen_linked(
            field, value, 1, 'dom_pais_id',
            URL(a = 'app_generales', c = '005genericos_sat', f = 'localidad_pais_estados_ver.json'),
            D_additionalAttributes = Storage(
                linkedTableFieldName = 'pais_id'
                )
            ),
        label = 'Estado', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tcfdi_compcartaporte_figurastransporepersonas.dom_pais_estado_id
            )
        ),
    Field(
        'dom_pais', 'string', length = 10, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Pais', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'dom_pais_id', 'integer', default = TPAISES.MEX,
        required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tpaises.id', db01.tpaises._format)),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Pais',
        comment = 'Residencia fiscal',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tcfdi_compcartaporte_figurastransporepersonas.dom_pais_id
            )
        ),
    Field(
        'dom_codigopostal', 'string', length = 12, default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Código Postal', comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'operador_id', dbc01.tempresa_operadores, default = None,
        required = False,
        requires = IS_NULL_OR(
            IS_IN_DB(
                dbc01(dbc01.tempresa_operadores.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id),
                'tempresa_operadores.id',
                dbc01.tempresa_operadores._format
                )
            ),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Operador', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r)
        ),

    tSignature_dbc01,
    format = '%(calle)s',
    singular = 'Persona',
    plural = 'Personas',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )
