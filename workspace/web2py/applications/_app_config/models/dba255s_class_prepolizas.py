# -*- coding: utf-8 -*-

class STV_FWK_PREPOLIZA:
    Template = None

    def __init__(self):
        return

    def _inicializar_variables(self):
        self._s_cfdi_id = None  # Contiene el ID del CFDI que esta siendo utilizado para la generación de la prepoliza
        self._s_proceso = ""  # Variable que identifica el proceso en la generación de la prepoliza, muy útil para identificar donde existe un error
        self._dbRowCFDI = None  # Variable de tipo dbRow que contiene muchas relaciones de tablas relacionadas con el CFDI principal
        self._s_aniocfdi = None  # Variable que guarda el año del cfdi utilizado en las tablas como condición del ejercicio
        self._D_substituciones = {}  # Variable que almacena las subsituciones para los Template de conecptos
        self._D_contabilidad = Storage(  # Variable utilizada para almacenar la configuración contable global
            tipopoliza_id = None,
            diario_id = None,
            segmento_id = None,
            concepto = None
            )
        self._n_prepoliza_id = None  # El id de la prepoliza generada
        self._n_num_concepto = 1  # Concecutivo para la generación de la preopoliza
        self._L_msgError = []  # Errores no recuperables que no permiten la inserción de la prepoliza
        self._L_msgError_asiento = []  # Errores que se insertan en registros de la prepoliza
        self._L_msgError_asiento_venta = []  # Errores que se insertan en los asientos de venta
        self._L_msgError_header = []  # Errores que se insertan en el encabezado de la póliza
        self._n_cargos = 0  # Variable para la suma de los cargos de la prepoliza
        self._n_abonos = 0  # Variable para la suma de los abonos de la prepoliza
        self._b_esCancelado = False  # Variable para saber si se contabilizará un cancelado.
        self._n_tipoContabilizacionCancelados = 0  # Variable para saber el tipo de contabilización de CFDI cancelados
        self._n_fechaContabilizacion = 0  # Variable para saber en que fecha quedará guardada la prepoliza.
        return

    def generar(self, s_cfdi_id, b_crearPoliza = True, b_cambiarEstado = True, b_esCancelado = False):

        _D_return = Storage(
            L_msgError = [],
            E_return = CLASS_e_RETURN.NOK_ERROR,
            n_prepoliza_id = 0
            )

        self._inicializar_variables()

        _dbRows = None
        self._s_cfdi_id = s_cfdi_id

        self._b_esCancelado = b_esCancelado

        try:

            if self._b_esCancelado:
                _dbRowsPrepoliza = dbc01(
                    (dbc01.tcfdi_prepolizas.cfdi_id == self._s_cfdi_id)
                    & (dbc01.tcfdi_prepolizas.sat_status_cancelacion == TCFDI_PREPOLIZAS.SAT_STATUS.CANCELADO)
                    & (dbc01.tcfdis.id == dbc01.tcfdi_prepolizas.cfdi_id)
                    ).select(
                    dbc01.tcfdi_prepolizas.ALL,
                    dbc01.tcfdis.ALL
                    )
            else:
                _dbRowsPrepoliza = dbc01(
                    (dbc01.tcfdi_prepolizas.cfdi_id == self._s_cfdi_id)
                    & (dbc01.tcfdi_prepolizas.sat_status_cancelacion == TCFDI_PREPOLIZAS.SAT_STATUS.VIGENTE)
                    & (dbc01.tcfdis.id == dbc01.tcfdi_prepolizas.cfdi_id)
                    ).select(
                    dbc01.tcfdi_prepolizas.ALL,
                    dbc01.tcfdis.ALL
                    )

            if len(_dbRowsPrepoliza) > 1:
                self._L_msgError.append("CFDI esta relacionado con varias prepolizas. Contacte al administrador.")
            else:

                if len(_dbRowsPrepoliza) == 1:
                    _dbRowPrepoliza = _dbRowsPrepoliza.last()

                    _dbRowPoliza = dbc01.tpolizas(_dbRowPrepoliza.tcfdi_prepolizas.poliza_id)
                    if _dbRowPrepoliza.tcfdis.rol != TCFDIS.E_ROL.EMISOR:
                        self._L_msgError.append(
                            "CFDI donde el Rol es diferente de emisión no puede contabilizarse por este proceso."
                            )
                    elif (_dbRowPoliza and
                          not (_dbRowPoliza.estatus in (
                                  TPOLIZAS.ESTATUS.NO_DEFINIDO, TPOLIZAS.ESTATUS.EN_PROCESO,
                                  TPOLIZAS.ESTATUS.SIN_EXPORTAR))
                    ):
                        self._L_msgError.append(
                            "Poliza ya se encuentra en estado [%s] que no permite actualizar Prepoliza" %
                            TPOLIZAS.ESTATUS.get_dict()[_dbRowPoliza.estatus]
                            )
                    else:

                        # TODO Incluidr validación de si el rol es emisor

                        # Si ya existe la pre-poliza y no hay problema para regenerarla, se borra la prepoliza anterior
                        dbc01(dbc01.tcfdi_prepolizas.cfdi_id == self._s_cfdi_id).delete()
                        dbc01.commit()

                else:
                    pass

                # Query para obtener las variables necesarias para las condiciones.
                self._s_proceso = "Se obtiene información del CFDI"
                _dbRows = dbc01(
                    (dbc01.tcfdis.id == self._s_cfdi_id)
                    & (dbc01.tempresas.id == dbc01.tcfdis.empresa_id)
                    & (dbc01.tempresa_plaza_sucursal_cajaschicas.id == dbc01.tcfdis.empresa_plaza_sucursal_cajachica_id)
                    ).select(
                    dbc01.tcfdis.ALL,
                    dbc01.tempresas.ALL,
                    dbc01.tempresa_plaza_sucursal_cajaschicas.ALL,
                    dbc01.tcfdi_registropagos.ALL,
                    dbc01.tempresa_cuentabancaria_terminales.ALL,
                    dbc01.tempresa_plazas.id,
                    dbc01.tempresa_plazas.nombrecorto,
                    dbc01.tempresa_plaza_sucursales.id,
                    dbc01.tempresa_plaza_sucursales.nombrecorto,
                    dbc01.tempresa_clientes.ALL,
                    dbc01.tempresa_bancosdiariosespeciales.ALL,
                    orderby = dbc01.tcfdi_registropagos.id,
                    left = [
                        dbc01.tcfdi_registropagos.on(
                            dbc01.tcfdi_registropagos.cfdi_id == dbc01.tcfdis.id
                            ),
                        dbc01.tempresa_cuentabancaria_terminales.on(
                            dbc01.tempresa_cuentabancaria_terminales.id == dbc01.tcfdi_registropagos.empresa_cuentabancaria_terminal_id
                            ),
                        dbc01.tempresa_plaza_sucursales.on(
                            dbc01.tempresa_plaza_sucursales.id == dbc01.tempresa_plaza_sucursal_cajaschicas.empresa_plaza_sucursal_id
                            ),
                        dbc01.tempresa_plazas.on(
                            dbc01.tempresa_plazas.id == dbc01.tempresa_plaza_sucursales.empresa_plaza_id
                            ),
                        dbc01.tempresa_clientes.on(
                            dbc01.tempresa_clientes.id == dbc01.tcfdis.receptorcliente_id
                            ),
                        dbc01.tempresa_bancosdiariosespeciales.on(
                            dbc01.tempresa_bancosdiariosespeciales.empresa_id == dbc01.tcfdis.empresa_id
                            )
                        ]
                    )

                if not _dbRows:
                    self._L_msgError.append("CFDI no contiene pago relacionado / caja chica identificada")
                else:
                    # Se referencían los resultados que contendrán la información del CFDI
                    self._dbRowCFDI = _dbRows.last()

                    # Se guarda el tipo de contabilización de CFDI.
                    self._n_tipoContabilizacionCancelados = self._dbRowCFDI.tempresas.tipocontabilizacioncancelados

                    # TODO Parche, si el cliente no esta asociado al CFDI, se busca y asocia                
                    if not self._dbRowCFDI.tempresa_clientes.id:
                        self._s_proceso = "No se identifico el cliente, se procede a relacionarlo"
                        _D_returnResult = self._getCliente(
                            self._dbRowCFDI.tcfdis.receptornombrerazonsocial,
                            self._dbRowCFDI.tcfdis.receptorrfc
                            )
                        self._dbRowCFDI.tempresa_clientes = _D_returnResult.dbRow_empresa_cliente
                    else:
                        # No hay problema
                        pass

            if not self._L_msgError:

                from string import Template
                self.Template = Template

                self._s_aniocfdi = str(self._dbRowCFDI.tcfdis.fecha.year)

                self._D_substituciones = Storage(
                    Ejercicio = self._s_aniocfdi,
                    Periodo = str(self._dbRowCFDI.tcfdis.fecha.month).zfill(2),
                    Mes = TGENERICAS.MES.D_TODOS[self._dbRowCFDI.tcfdis.fecha.month],
                    Dia = str(self._dbRowCFDI.tcfdis.fecha.day).zfill(2),
                    Plaza = str(self._dbRowCFDI.tempresa_plazas.nombrecorto),
                    Sucursal = str(self._dbRowCFDI.tempresa_plaza_sucursales.nombrecorto),
                    Cajachica = str(self._dbRowCFDI.tempresa_plaza_sucursal_cajaschicas.nombre),
                    CFDI = str(self._dbRowCFDI.tcfdis.serie) + str(self._dbRowCFDI.tcfdis.folio),
                    UUID = str(self._dbRowCFDI.tcfdis.uuid),
                    )

                if self._dbRowCFDI.tcfdis.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO:

                    _D_result = self._ingreso()
                    _D_return.E_return = _D_result.E_return

                # Aquí empieza la generación de prepoliza si es comprobante de PAGO
                elif self._dbRowCFDI.tcfdis.tipocomprobante_id == TTIPOSCOMPROBANTES.PAGO:

                    _D_result = self._pago()
                    _D_return.E_return = _D_result.E_return

                    # Empieza el CFDI de egreso
                elif self._dbRowCFDI.tcfdis.tipocomprobante_id == TTIPOSCOMPROBANTES.EGRESO:

                    _D_result = self._egreso()
                    _D_return.E_return = _D_result.E_return

                else:
                    # TODO que pasa con egresos o pagos
                    _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                    self._L_msgError.append("Prepoliza no se pudo generar para ingresos, egresos o pagos")

            else:
                # Hay errores
                pass

            if self._L_msgError:

                _D_return.L_msgError = self._L_msgError
                dbc01.rollback()

            elif not ((round(self._n_cargos, 2) == round(self._n_abonos, 2)) and (self._n_abonos != 0) and (
                    self._n_cargos != 0)):
                # si no trajo errores pero los cargos y abonos no cuadran se evitará el generado de la prepoliza.

                _D_return.L_msgError.append(
                    'La prepoliza y poliza no pudieron ser generadas debido a que los cargos y abonos no cuadran. '
                    )
                dbc01.rollback()

            elif self._L_msgError_asiento or self._L_msgError_header:
                # Se genera la prepoliza pero el CFDI se queda donde está ya que se generó de manera incorrecta
                _D_return.L_msgError.append(
                    "Se generó la prepóliza con errores, se evitó el generado de la póliza. "
                    + "Para más información revise la prepóliza dentro del CFDI en el apartado 'Prepoliza'. "
                    )
                _D_return.n_prepoliza_id = self._n_prepoliza_id
                _D_return.E_return = stvfwk2_e_RETURN.OK
                dbc01.commit()
            else:
                if b_cambiarEstado:

                    if self._b_esCancelado:
                        # Actualización del estatus del CFDI
                        dbc01.tcfdis(self._s_cfdi_id).update_record(
                            etapa_cancelado = TCFDIS.E_ETAPA.CON_PREPOLIZA
                            )
                    else:
                        # Actualización del estatus del CFDI
                        dbc01.tcfdis(self._s_cfdi_id).update_record(
                            etapa = TCFDIS.E_ETAPA.CON_PREPOLIZA
                            )

                else:
                    pass

                if b_crearPoliza:
                    # Generación de la póliza
                    _D_return = STV_FWK_POLIZA(self._dbRowCFDI.tempresas.id).agregar_prepoliza(
                        self._n_prepoliza_id, self._b_esCancelado
                        )
                else:
                    pass

                if _D_return.L_msgError:
                    dbc01.rollback()
                else:
                    _D_return.E_return = stvfwk2_e_RETURN.OK
                    # Todo se creo correctamente
                    _D_return.n_prepoliza_id = self._n_prepoliza_id
                    dbc01.commit()

        except Exception as _O_excepcion:
            _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
            _D_return.L_msgError.append('Proceso %s; Excepcion: %s' % (self._s_proceso, str(_O_excepcion)))

            # Se regresa cualquier inserción a la base de datos dbc01
            dbc01.rollback()

        return _D_return

    def _ingreso(self):
        """ Función para la generación de la prepoliza de tipo Ingreso
        """

        _D_return = Storage(
            E_return = CLASS_e_RETURN.NOK_ERROR,
            )
        _L_msgError_asiento_ingreso = []

        # Variable que contendrá las cuentas para generar los asientos contables por el ingreso
        _D_cuentasIngreso = Storage()

        if not self._b_esCancelado:
            # Se obtiene la información de los registro de pagos.
            _dbRowsRegistropagos = dbc01(
                (dbc01.tcfdi_registropagos.cfdi_id == self._s_cfdi_id)
                & (dbc01.tcfdi_registropagos.tipo_cfdi == TCFDI_REGISTROPAGOS.TIPO_CFDI.VIGENTE)
                ).select(
                dbc01.tcfdi_registropagos.ALL,
                )
        else:
            # Se obtiene la información de los registro de pagos.
            _dbRowsRegistropagos = dbc01(
                (dbc01.tcfdi_registropagos.cfdi_id == self._s_cfdi_id)
                & (dbc01.tcfdi_registropagos.tipo_cfdi == TCFDI_REGISTROPAGOS.TIPO_CFDI.CANCELADO)
                ).select(
                dbc01.tcfdi_registropagos.ALL,
                )

        if self._b_esCancelado:
            _dbRowRegistropago = _dbRowsRegistropagos.last()
            self._n_fechaContabilizacion = _dbRowRegistropago.tipo_contabilizacion_fecha_cancelacion
        else:
            self._n_fechaContabilizacion = TCFDI_PREPOLIZAS.TIPO_CONTABILIZACION_FECHA_CANCELACION.EMISION

        self._s_proceso = "Se inserta prepoliza maestro"
        _D_returnResult = self._insertHeader()

        _b_esAbono = False  # Se deja el valor de False por default.

        for _dbRowRegistropago in _dbRowsRegistropagos:

            self._s_proceso = "Se identifica la información para el primer asiento contable"

            if self._b_esCancelado:
                self._n_fechaContabilizacion = _dbRowRegistropago.tipo_contabilizacion_fecha_cancelacion
            else:
                self._n_fechaContabilizacion = TCFDI_PREPOLIZAS.TIPO_CONTABILIZACION_FECHA_CANCELACION.EMISION

            _dbRowBanco = db01.tbancos(_dbRowRegistropago.banco_id_cliente)
            if _dbRowBanco:
                _s_banco = _dbRowBanco.nombre
            else:
                _s_banco = "No identificado"

            _dbRowTeminal = dbc01.tempresa_cuentabancaria_terminales(
                _dbRowRegistropago.empresa_cuentabancaria_terminal_id
                )
            if _dbRowTeminal:
                _s_terminal = _dbRowTeminal.terminal
            else:
                _s_terminal = "No identificado"

            self._D_substituciones.update(
                Tarjeta = str(_dbRowRegistropago.tarjeta),
                Banco = str(_s_banco),
                Autorizacion = str(_dbRowRegistropago.autorizacion),
                Operacion = str(_dbRowRegistropago.operacion),
                Terminal = str(_s_terminal),
                Cheque = str(_dbRowRegistropago.cheque),
                Cuenta = str(_dbRowRegistropago.cuenta_cliente),
                Referencia = str(_dbRowRegistropago.referencia),
                Rastreo = str(_dbRowRegistropago.claverastero),
                RFC = str(self._dbRowCFDI.tcfdis.receptorrfc),
                RazonSocialCliente = str(self._dbRowCFDI.tcfdis.receptornombrerazonsocial),
                FechaDeposito = str(_dbRowRegistropago.fechadeposito)
                )

            # Método de pago: PUE    Forma de pago: Efectivo
            if self._dbRowCFDI.tcfdis.metodopago_id == TMETODOSPAGO.UNAEXHIBICION:

                if _dbRowRegistropago.formapago_id == TFORMASPAGO.EFECTIVO:

                    self._s_proceso = "En caso de efectivo, se procede a identificar la cuenta"
                    if self._b_esCancelado:
                        _dbRowRegistropago.tratoefectivo = TCFDI_REGISTROPAGOS.E_TRATOEFECTIVO.RETENIDOCAJA
                    else:
                        pass

                    if _dbRowRegistropago.tratoefectivo == TCFDI_REGISTROPAGOS.E_TRATOEFECTIVO.RETENIDOCAJA:

                        _D_returnResult = self._getCuentascontables_porEfectivo(
                            self._dbRowCFDI.tcfdis.monedacontpaqi_id
                            )

                        if not _D_returnResult.D_cuentas:
                            _L_msgError_asiento_ingreso.append(
                                "Cuenta contable de caja chica no definida. Caja chica id = %s; Sucursal id = %s; Plaza id = %s. " % (
                                    self._dbRowCFDI.tempresa_plaza_sucursal_cajaschicas.id,
                                    self._dbRowCFDI.tempresa_plaza_sucursales.id,
                                    self._dbRowCFDI.tempresa_plazas.id
                                    )
                                )
                        else:
                            # No hay problema, si se encontraron las cuentas para el cliente
                            _D_cuentasIngreso = _D_returnResult.D_cuentas

                            _D_cuentasIngreso.s_concepto = self.Template(
                                self._D_contabilidad.row_conceptos.ingreso_efectivo
                                ).safe_substitute(
                                self._D_substituciones
                                )
                            _D_cuentasIngreso.empresa_segmento_id = None  # No aplica segmento de negocio para este tipo de movimiento
                            _D_cuentasIngreso.empresa_diario_id = None
                            _s_tipodocumentobancario = None  # No aplica si es efectivo

                    else:
                        _D_returnResult = self._getCuentascontables_porBanco(
                            _dbRowRegistropago.empresa_cuentabancaria_id
                            )

                        if not _D_returnResult.D_cuentas:
                            _L_msgError_asiento_ingreso.append(
                                "Cuenta contable de cuenta bancaria no definida. Cuenta bancaria id = %s. " % _dbRowRegistropago.empresa_cuentabancaria_id
                                )
                        else:
                            # No hay problema, si se encontraron las cuentas para el cliente
                            _D_cuentasIngreso = _D_returnResult.D_cuentas

                            _D_cuentasIngreso.s_concepto = self.Template(
                                self._D_contabilidad.row_conceptos.ingreso_efectivo_depositado
                                ).safe_substitute(
                                self._D_substituciones
                                )

                            # Jamas debe entrar aqui
                        _D_cuentasIngreso.empresa_segmento_id = None  # No aplica segmento de negocio para este tipo de movimiento
                        _D_cuentasIngreso.empresa_diario_id = self._dbRowCFDI.tempresa_bancosdiariosespeciales.bancos_venta_contado_diario_id

                        _s_tipodocumentobancario = self._dbRowCFDI.tempresa_bancosdiariosespeciales.bancos_tipo_documentos_venta_contado


                # Método de pago: PUE    Forma de pago: Cheque o Transferencia
                elif _dbRowRegistropago.formapago_id in (TFORMASPAGO.CHEQUE, TFORMASPAGO.TRANSFERENCIA,):

                    self._s_proceso = "En caso de cheque o transferencia, se procede a identificar la cuenta"

                    _D_returnResult = self._getCuentascontables_porBanco(
                        _dbRowRegistropago.empresa_cuentabancaria_id
                        )

                    if not _D_returnResult.D_cuentas:
                        _L_msgError_asiento_ingreso.append(
                            "Cuenta contable de cuenta bancaria no definida. Cuenta bancaria id = %s. " % _dbRowRegistropago.empresa_cuentabancaria_id
                            )
                    else:
                        # No hay problema, si se encontraron las cuentas para el cliente
                        _D_cuentasIngreso = _D_returnResult.D_cuentas

                    if _dbRowRegistropago.formapago_id == TFORMASPAGO.CHEQUE:
                        _D_cuentasIngreso.s_concepto = self.Template(
                            self._D_contabilidad.row_conceptos.ingreso_cheque
                            ).safe_substitute(self._D_substituciones)
                    elif _dbRowRegistropago.formapago_id == TFORMASPAGO.TRANSFERENCIA:
                        _D_cuentasIngreso.s_concepto = self.Template(
                            self._D_contabilidad.row_conceptos.ingreso_transferencia
                            ).safe_substitute(
                            self._D_substituciones
                            )
                    else:
                        _D_cuentasIngreso.s_concepto = "Error, no debe entrar aqui"
                        # Jamas debe entrar aqui
                    _D_cuentasIngreso.empresa_segmento_id = None  # No aplica segmento de negocio para este tipo de movimiento
                    _D_cuentasIngreso.empresa_diario_id = self._dbRowCFDI.tempresa_bancosdiariosespeciales.bancos_venta_contado_diario_id

                    _s_tipodocumentobancario = self._dbRowCFDI.tempresa_bancosdiariosespeciales.bancos_tipo_documentos_venta_contado

                # Método de pago: PUE    Forma de pago: Tarjeta de credito o debito
                elif _dbRowRegistropago.formapago_id in (TFORMASPAGO.TDC, TFORMASPAGO.TDD):

                    self._s_proceso = "En caso de tarjeta, se procede a identificar la cuenta"

                    # Se hace el row con el registropago del For porque el registro de pago a nivel _dbRowCFDI se hace por el primer registro.
                    _dbRowCuentabancaria = dbc01.tempresa_cuentabancaria_terminales(
                        _dbRowRegistropago.empresa_cuentabancaria_terminal_id
                        )

                    _D_returnResult = self._getCuentascontables_porBanco(
                        _dbRowCuentabancaria.empresa_cuentabancaria_id
                        )

                    if not _D_returnResult.D_cuentas:
                        _L_msgError_asiento_ingreso.append(
                            "Cuenta contable de cuenta bancaria no definida. Cuenta bancaria id = %s. " % self._dbRowCFDI.tempresa_cuentabancaria_terminales.empresa_cuentabancaria_id
                            )
                    else:
                        # No hay problema, si se encontraron las cuentas para el cliente
                        _D_cuentasIngreso = _D_returnResult.D_cuentas

                    if _dbRowRegistropago.formapago_id == TFORMASPAGO.TDC:
                        _D_cuentasIngreso.s_concepto = self.Template(
                            self._D_contabilidad.row_conceptos.ingreso_tdc
                            ).safe_substitute(self._D_substituciones)
                    elif _dbRowRegistropago.formapago_id == TFORMASPAGO.TDD:
                        _D_cuentasIngreso.s_concepto = self.Template(
                            self._D_contabilidad.row_conceptos.ingreso_tdd
                            ).safe_substitute(self._D_substituciones)
                    else:
                        _D_cuentasIngreso.s_concepto = "Error, no debe entrar aqui"
                        # Jamas debe entrar aqui
                    _D_cuentasIngreso.empresa_segmento_id = None  # No aplica segmento de negocio para este tipo de movimiento
                    _D_cuentasIngreso.empresa_diario_id = self._dbRowCFDI.tempresa_bancosdiariosespeciales.bancos_venta_contado_diario_id

                    _s_tipodocumentobancario = self._dbRowCFDI.tempresa_bancosdiariosespeciales.bancos_tipo_documentos_venta_contado

                elif (_dbRowRegistropago.formapago_id == TFORMASPAGO.CONFUSION) and (
                        _dbRowRegistropago.diferencia == True):

                    # Si la moneda es la misma moneda de la empresa entonces hará todo esto. De lo contrario error.

                    _D_returnResult = self._getCuentascontables_diferencias(
                        self._dbRowCFDI.tcfdis.monedacontpaqi_id,
                        _dbRowRegistropago.monto
                        )

                    if not _D_returnResult.D_cuentas:
                        _L_msgError_asiento_ingreso.append(
                            "Cuenta contable de sobrantres o faltantes no se encuentra registrada. Caja chica id = %s; Sucursal id = %s; Plaza id = %s. " % (
                                self._dbRowCFDI.tempresa_plaza_sucursal_cajaschicas.id,
                                self._dbRowCFDI.tempresa_plaza_sucursales.id,
                                self._dbRowCFDI.tempresa_plazas.id
                                )
                            )

                    # Elif El valor absoluto del monto no puede ser mayor al definido en la empresa.
                    elif abs(_dbRowRegistropago.monto) > self._dbRowCFDI.tempresas.maximosobrantefaltante:
                        _L_msgError_asiento_ingreso.append(
                            "El valor del monto es mayor al permitido por la empresa para las diferencias. Empresa: %s. " % (
                                self._dbRowCFDI.tempresas.id)
                            )
                    else:
                        # No hay problema, si se encontraron las cuentas contables para las diferencias.

                        _D_cuentasIngreso = _D_returnResult.D_cuentas
                        _D_cuentasIngreso.empresa_segmento_id = self._D_contabilidad.segmento_id  # No aplica segmento de negocio para este tipo de movimiento
                        _D_cuentasIngreso.empresa_diario_id = None
                        _s_tipodocumentobancario = None
                        pass

                    if _dbRowRegistropago.monto > 0:
                        _D_cuentasIngreso.s_concepto = 'Diferencia faltante'

                        pass
                    else:
                        _D_cuentasIngreso.s_concepto = 'Diferencia sobrante'
                        _b_esAbono = True


                else:
                    self._L_msgError.append("Forma de pago no definida o inválida.")

                    # Si la forma de pago no esta contemplada, la variable de cuentasIngreso
                    # estará vacía, evitando generar asientos contables

                    pass
            elif self._dbRowCFDI.tcfdis.metodopago_id == TMETODOSPAGO.PARCIALIDADES:

                # Método de pago: PPD    Forma de pago: Crédito, Por definir

                # Si se ultiliza más de 1 registro de pago con distinta forma de pago entonces tronará siempre esta condición
                if self._dbRowCFDI.tcfdi_registropagos.formapago_id == TFORMASPAGO.POR_DEFINIR:

                    self._s_proceso = "En caso de credito, se procede a identificar la cuenta por cliente"
                    _D_returnResult = self._getCuentascontables_porCliente(self._dbRowCFDI)

                    if not _D_returnResult.D_cuentas:
                        _L_msgError_asiento_ingreso.append(
                            "Cuenta contable de cliente no definida. Cliente id = %s. " % self._dbRowCFDI.tcfdis.receptorcliente_id
                            )
                    else:
                        _D_cuentasIngreso = _D_returnResult.D_cuentas

                    _D_cuentasIngreso.s_concepto = self.Template(
                        self._D_contabilidad.row_conceptos.ingreso_cliente
                        ).safe_substitute(self._D_substituciones)
                    _D_cuentasIngreso.empresa_segmento_id = None  # No aplica segmento de negocio para este tipo de movimiento

                    _s_tipodocumentobancario = None  # No aplica en los movimientos de PPD
                else:
                    self._L_msgError.append("Forma de pago no definida")
                    # Forma de pago no contemplada en caso de venta a crédito
                    pass

            else:
                self._L_msgError.append("Método de pago no definido")
                # Método de pago no definido
                pass

                # Si no esta definido cuentas ingreso error msg de moneda posible issue

            if self._L_msgError:

                # Se regresa el error que exista en caso de no identificar la cuenta de ingreso o se detecten errores
                pass

            elif self._n_prepoliza_id:

                self._s_proceso = "Se inserta los primeros asientos con su cuenta de ingreso"

                _D_returnResult = self._insertAsientos_ingreso(
                    _D_cuentasIngreso,
                    D_monto = Storage(
                        monto = abs(_dbRowRegistropago.monto),
                        monedacontpaqi_id = self._dbRowCFDI.tcfdis.monedacontpaqi_id,
                        tipocambio = self._dbRowCFDI.tcfdis.tipocambio
                        ),
                    b_esAbono = _b_esAbono,
                    D_datosAdicionalesIngreso = Storage(
                        descripcion = _D_cuentasIngreso.s_concepto,
                        cfdi_ingreso_id = self._dbRowCFDI.tcfdis.id,
                        empresa_diario_id = _D_cuentasIngreso.empresa_diario_id,
                        empresa_segmento_id = _D_cuentasIngreso.empresa_segmento_id,
                        esingreso = (self._dbRowCFDI.tcfdis.metodopago_id == TMETODOSPAGO.UNAEXHIBICION),
                        escliente = (self._dbRowCFDI.tcfdis.metodopago_id == TMETODOSPAGO.PARCIALIDADES),
                        errores = "".join(_L_msgError_asiento_ingreso)
                        ),
                    D_datosAdicionalesComplemento = Storage(
                        descripcion = _D_cuentasIngreso.s_concepto,
                        cfdi_ingreso_id = self._dbRowCFDI.tcfdis.id,
                        empresa_diario_id = _D_cuentasIngreso.empresa_diario_id,
                        empresa_segmento_id = _D_cuentasIngreso.empresa_segmento_id,
                        empresa_tipodocumentobancario_id = _s_tipodocumentobancario,
                        esingreso = False,
                        escliente = False,
                        errores = "".join(_L_msgError_asiento_ingreso)
                        ),
                    )

                # Se graban los errores y se limpia la variable local para los nuevos errores de asientos
                self._L_msgError_asiento += _L_msgError_asiento_ingreso
                _L_msgError_asiento_ingreso = []

        # Hasta aquí acaba el For para el registro de los ingresos

        if self._L_msgError:

            # Se regresa el error que exista en caso de no identificar la cuenta de ingreso o se detecten errores
            pass

        elif self._n_prepoliza_id:

            self._s_proceso = "Se identifican los conceptos"

            # Empezando a barrer los conceptos
            _dbRows_cfdi_conceptos = dbc01(
                (dbc01.tcfdi_conceptos.cfdi_id == self._s_cfdi_id)
                & (dbc01.tempresa_prodserv.id == dbc01.tcfdi_conceptos.empresa_prodserv_id)
                ).select(
                dbc01.tcfdi_conceptos.ALL,
                dbc01.tempresa_prodserv.ALL,
                dbc01.tempresa_prodserv_ingresosestructuras.empresa_ingresoestructura_id,
                orderby = [
                    dbc01.tcfdi_conceptos.id,
                    dbc01.tempresa_prodserv.id,
                    ~dbc01.tempresa_prodserv_ingresosestructuras.ejercicio
                    ],
                left = [
                    dbc01.tempresa_prodserv_ingresosestructuras.on(
                        (dbc01.tempresa_prodserv_ingresosestructuras.empresa_prodserv_id == dbc01.tempresa_prodserv.id)
                        & (dbc01.tempresa_prodserv_ingresosestructuras.ejercicio <= self._s_aniocfdi)
                        )
                    ]
                )

            # Variable que acumulará los impuestos trasladados
            _D_impuestostrasladados = Storage()
            _n_last_cfdi_concepto_id = None

            for _dbRowCFDIConcepto in _dbRows_cfdi_conceptos:

                if _n_last_cfdi_concepto_id == _dbRowCFDIConcepto.tcfdi_conceptos.id:
                    # Como el ultimo procesado es igual al que se esta verificando
                    # debe ser ignorado y continuar con el siguiente
                    pass
                else:

                    _n_last_cfdi_concepto_id = _dbRowCFDIConcepto.tcfdi_conceptos.id

                    # Variable que contendrá las cuentas para generar los asientos contables por la venta
                    _D_cuentasVenta = Storage()

                    _n_empresa_ingresoestructura_id = _dbRowCFDIConcepto.tempresa_prodserv_ingresosestructuras.empresa_ingresoestructura_id

                    if not _n_empresa_ingresoestructura_id:
                        _L_msgError_asiento_ingreso.append(
                            "Producto o Servicio no tiene estructura de ingreso asociada. Producto/Servicio id = %s. " % _dbRowCFDIConcepto.tempresa_prodserv.id
                            )
                    else:
                        self._s_proceso = "Se identifican cuentas contables para la estructura de venta"

                        _D_returnResult = self._getCuentascontables_ventas(
                            _n_empresa_ingresoestructura_id,
                            )

                        if not _D_returnResult.D_cuentasVenta:
                            _L_msgError_asiento_ingreso.append(
                                "No se encontró información de contabilización para el ingreso estructura con id: %s " % _n_empresa_ingresoestructura_id
                                )
                        else:
                            _D_cuentasVenta = _D_returnResult.D_cuentasVenta

                    if not self._L_msgError:

                        self._s_proceso = "Se insertan los asientos por la venta"

                        self._D_substituciones.update(
                            Concepto = str(_dbRowCFDIConcepto.tcfdi_conceptos.noidentificacion),
                            Cantidad = str(_dbRowCFDIConcepto.tcfdi_conceptos.cantidad),
                            PrecioUnitario = '${:0,.2f}'.format(_dbRowCFDIConcepto.tcfdi_conceptos.valorunitario),
                            Importe = '${:0,.2f}'.format(_dbRowCFDIConcepto.tcfdi_conceptos.importe),
                            Descuento = '${:0,.2f}'.format(_dbRowCFDIConcepto.tcfdi_conceptos.descuento or 0),
                            Total = '${:0,.2f}'.format(
                                _dbRowCFDIConcepto.tcfdi_conceptos.importe - (
                                        _dbRowCFDIConcepto.tcfdi_conceptos.descuento or 0)
                                ),
                            )

                        _D_returnResult = self._insertAsientosVenta(
                            _D_cuentasVenta,
                            D_monto = Storage(
                                monto = _dbRowCFDIConcepto.tcfdi_conceptos.importe,
                                descuento = _dbRowCFDIConcepto.tcfdi_conceptos.descuento or 0,
                                monedacontpaqi_id = self._dbRowCFDI.tcfdis.monedacontpaqi_id,
                                tipocambio = self._dbRowCFDI.tcfdis.tipocambio
                                ),
                            b_esAbono = True,
                            D_datosAdicionalesVenta = Storage(
                                descripcion = self.Template(
                                    self._D_contabilidad.row_conceptos.ingreso_venta
                                    ).safe_substitute(
                                    self._D_substituciones
                                    ),
                                cfdi_ingreso_id = self._dbRowCFDI.tcfdis.id,
                                empresa_diario_id = _D_cuentasVenta.selected_diario_especial,
                                empresa_segmento_id = self._D_contabilidad.segmento_id,
                                esconcepto_porcobrar = (
                                        self._dbRowCFDI.tcfdis.metodopago_id == TMETODOSPAGO.PARCIALIDADES),
                                esconcepto_cobrado = (
                                        self._dbRowCFDI.tcfdis.metodopago_id == TMETODOSPAGO.UNAEXHIBICION),
                                errores = "".join(_L_msgError_asiento_ingreso)
                                ),
                            D_datosAdicionalesDescuento = Storage(
                                descripcion = self.Template(
                                    self._D_contabilidad.row_conceptos.ingreso_descuento
                                    ).safe_substitute(
                                    self._D_substituciones
                                    ),
                                cfdi_ingreso_id = self._dbRowCFDI.tcfdis.id,
                                empresa_diario_id = _D_cuentasVenta.selected_diario_especial,
                                empresa_segmento_id = self._D_contabilidad.segmento_id,
                                esconcepto_porcobrar = (
                                        self._dbRowCFDI.tcfdis.metodopago_id == TMETODOSPAGO.PARCIALIDADES),
                                esconcepto_cobrado = (
                                        self._dbRowCFDI.tcfdis.metodopago_id == TMETODOSPAGO.UNAEXHIBICION),
                                errores = "".join(_L_msgError_asiento_ingreso)
                                ),
                            )

                        # Se graban los errores y se limpia la variable local para los nuevos errores de asientos
                        self._L_msgError_asiento += _L_msgError_asiento_ingreso
                        _L_msgError_asiento_ingreso = []

                        # Empezando a barrer los conceptos
                        self._s_proceso = "Se obtienen los impuestos trasladados"

                        _dbRows_cfdi_conceptos_impuestostrasladados = dbc01(
                            (
                                    dbc01.tcfdi_concepto_impuestostrasladados.cfdi_concepto_id == _dbRowCFDIConcepto.tcfdi_conceptos.id)
                            ).select(
                            dbc01.tcfdi_concepto_impuestostrasladados.ALL,
                            )

                        if _dbRows_cfdi_conceptos_impuestostrasladados:
                            for _dbRow_impuestotrasladado in _dbRows_cfdi_conceptos_impuestostrasladados:
                                # Se define un index para poder agrupar los impuestos donde correspondan al mismo tipo y tasa
                                _s_index = str(_dbRow_impuestotrasladado.impuesto_id) + "_" + str(
                                    _dbRow_impuestotrasladado.tasaocuota
                                    )
                                if _s_index in _D_impuestostrasladados:
                                    _D_impuestostrasladados[
                                        _s_index].importe_acumulado += _dbRow_impuestotrasladado.importe or 0
                                else:
                                    _D_impuestostrasladados[_s_index] = Storage(
                                        impuesto_id = _dbRow_impuestotrasladado.impuesto_id,
                                        tasaocuota = _dbRow_impuestotrasladado.tasaocuota or 0,
                                        importe_acumulado = _dbRow_impuestotrasladado.importe or 0
                                        )

                        else:
                            # No existen impuestos trasladados
                            pass

                        # TODO agregar asientos en caso de moneda extranjera

                    else:
                        # Si no se encontraron las cuentas contables, se mantienen los errores, y se continua checando los otros asientos
                        pass

                    # En este punto termina el if de cuenta venta

                # En este punto termina el if de que se repite el concepto en el for

            # Cuando termina el for sobre todos los conceptos, se genera un asiento por el IVA

            for _s_indice in _D_impuestostrasladados:

                _D_cuentaIva = Storage()

                _D_impuestotrasladado = _D_impuestostrasladados[_s_indice]

                if _D_impuestotrasladado.impuesto_id == TIMPUESTOS.IVA:

                    self._s_proceso = "Se obtienen las cuentas de los impuestos"

                    _D_returnResult = self._getCuentascontablesIVA(_D_impuestotrasladado)

                    if not _D_returnResult.D_cuentasIVA:
                        _L_msgError_asiento_ingreso.append("No se encontró información de contabilización para el IVA")
                    else:
                        _D_cuentaIva = _D_returnResult.D_cuentasIVA

                    _D_monto = Storage(
                        porTrasladar = 0,
                        trasladado = 0,
                        monedacontpaqi_id = self._dbRowCFDI.tcfdis.monedacontpaqi_id,
                        tipocambio = self._dbRowCFDI.tcfdis.tipocambio
                        )

                    if self._dbRowCFDI.tcfdis.metodopago_id == TMETODOSPAGO.UNAEXHIBICION:
                        # Si se pagó en una exhibicion, no se inserta asiento de iva por trasladar
                        _D_monto.trasladado = _D_impuestotrasladado.importe_acumulado
                    elif self._dbRowCFDI.tcfdis.metodopago_id == TMETODOSPAGO.PARCIALIDADES:
                        # Si se paga a credito, no se inserta iva trasladado
                        _D_monto.porTrasladar = _D_impuestotrasladado.importe_acumulado
                    else:
                        self._L_msgError.append("No se encontró el método de pago")
                        break

                    self._s_proceso = "Se insertan los asientos de los impuestos"

                    self._D_substituciones.update(
                        Impuesto = 'IVA',
                        Tasa = '{:0,.2f}%'.format(_D_impuestotrasladado.tasaocuota * 100),
                        )

                    # Se procede a insertar el asiento por el IVA
                    _D_returnResult = self._insertAsientosImpuesto(
                        _D_cuentaIva,
                        D_monto = _D_monto,
                        D_esAbono = Storage(
                            porTrasladar = True,
                            trasladado = True
                            ),
                        D_datosAdicionalesPorTrasladar = Storage(
                            descripcion = self.Template(
                                self._D_contabilidad.row_conceptos.ingreso_impportrasladar
                                ).safe_substitute(
                                self._D_substituciones
                                ),
                            cfdi_ingreso_id = self._dbRowCFDI.tcfdis.id,
                            empresa_segmento_id = None,
                            impuesto_id = _D_impuestotrasladado.impuesto_id,
                            tasaocuota = _D_impuestotrasladado.tasaocuota,
                            errores = "".join(_L_msgError_asiento_ingreso)
                            ),
                        D_datosAdicionalesTrasladado = Storage(
                            descripcion = self.Template(
                                self._D_contabilidad.row_conceptos.ingreso_imptrasladado
                                ).safe_substitute(
                                self._D_substituciones
                                ),
                            cfdi_ingreso_id = self._dbRowCFDI.tcfdis.id,
                            empresa_segmento_id = None,
                            impuesto_id = _D_impuestotrasladado.impuesto_id,
                            tasaocuota = _D_impuestotrasladado.tasaocuota,
                            errores = "".join(_L_msgError_asiento_ingreso)
                            ),
                        )

                    # Se graban los errores y se limpia la variable local para los nuevos errores de asientos
                    self._L_msgError_asiento += _L_msgError_asiento_ingreso
                    _L_msgError_asiento_ingreso = []

                else:
                    # Impuesto no considerado
                    self._L_msgError.append("Impuesto no considerado: " + str(_D_impuestotrasladado.impuesto_id))

        else:
            # Se regresa el error que exista
            self._L_msgError.append("Prepoliza de ingreso no se pudo generar")
            pass

        return _D_return

    def _pago(self):
        """ Función para la generación de la prepoliza de tipo Pago
        """
        _D_return = Storage(
            L_msgError = [],
            E_return = CLASS_e_RETURN.NOK_ERROR
            )
        _L_msgError_asiento_pago = []
        _D_cuentasPago = Storage()

        self._s_proceso = "Se inserta prepoliza maestro"
        _D_result = self._insertHeader()

        if self._n_prepoliza_id:
            self._s_proceso = "Se identifica la información para el primer asiento contable"

            _n_last_cfdi_concepto_id = None
            _n_num_concepto = 1

            if not self._b_esCancelado:
                # Se obtiene la información de los registro de pagos.
                _dbRowsPagos = dbc01(
                    (dbc01.tcfdi_complementopagos.cfdi_id == self._s_cfdi_id)
                    ).select(
                    dbc01.tcfdi_complementopagos.ALL,
                    dbc01.tcfdi_registropagos.ALL,
                    dbc01.tempresa_cuentabancaria_terminales.ALL,
                    orderby = dbc01.tcfdi_complementopagos.id,
                    left = [
                        dbc01.tcfdi_registropagos.on(
                            (dbc01.tcfdi_registropagos.cfdi_complementopago_id == dbc01.tcfdi_complementopagos.id)
                            & (dbc01.tcfdi_registropagos.cfdi_id == self._s_cfdi_id)
                            & (dbc01.tcfdi_registropagos.tipo_cfdi == TCFDI_REGISTROPAGOS.TIPO_CFDI.VIGENTE)
                            ),
                        dbc01.tempresa_cuentabancaria_terminales.on(
                            dbc01.tempresa_cuentabancaria_terminales.id == dbc01.tcfdi_registropagos.empresa_cuentabancaria_terminal_id
                            ),
                        ]
                    )
            else:
                # Se obtiene la información de los registro de pagos.
                _dbRowsPagos = dbc01(
                    (dbc01.tcfdi_complementopagos.cfdi_id == self._s_cfdi_id)
                    ).select(
                    dbc01.tcfdi_complementopagos.ALL,
                    dbc01.tcfdi_registropagos.ALL,
                    dbc01.tempresa_cuentabancaria_terminales.ALL,
                    orderby = dbc01.tcfdi_complementopagos.id,
                    left = [
                        dbc01.tcfdi_registropagos.on(
                            (dbc01.tcfdi_registropagos.cfdi_complementopago_id == dbc01.tcfdi_complementopagos.id)
                            & (dbc01.tcfdi_registropagos.cfdi_id == self._s_cfdi_id)
                            & (dbc01.tcfdi_registropagos.tipo_cfdi == TCFDI_REGISTROPAGOS.TIPO_CFDI.CANCELADO)
                            ),
                        dbc01.tempresa_cuentabancaria_terminales.on(
                            dbc01.tempresa_cuentabancaria_terminales.id == dbc01.tcfdi_registropagos.empresa_cuentabancaria_terminal_id
                            ),
                        ]
                    )

            if self._b_esCancelado:
                self._n_fechaContabilizacion = _dbRowsPagos.last().tcfdi_registropagos.tipo_contabilizacion_fecha_cancelacion
            else:
                self._n_fechaContabilizacion = TCFDI_PREPOLIZAS.TIPO_CONTABILIZACION_FECHA_CANCELACION.EMISION



            # Se pasa por los complemetos que se tengan para generar los primeros asientos contables.
            for _dbRowPago in _dbRowsPagos:

                # Validación para ver si el CFDI de ingreso es de la misma empresa que el CFDI de pago.
                _dbRowsDoctoRelacionado = dbc01(
                    (dbc01.tcfdi_complementopago_doctosrelacionados.cfdi_complementopago_id == _dbRowPago.tcfdi_complementopagos.id)
                    & (dbc01.tcfdis.id == dbc01.tcfdi_complementopago_doctosrelacionados.cfdirelacionado_id)
                    & (dbc01.tcfdis.empresa_id != self._dbRowCFDI.tempresas.id)
                    ).select(
                    dbc01.tcfdis.ALL
                    )

                if _dbRowsDoctoRelacionado:
                    self._L_msgError.append('La empresa del CFDI relacionado no coincide con la del CFDI de pago')
                else:
                    pass

                self._D_substituciones.update(
                    Tarjeta = str(_dbRowPago.tcfdi_registropagos.tarjeta),
                    Banco = str(_dbRowPago.tcfdi_registropagos.banco_id_cliente),
                    Autorizacion = str(_dbRowPago.tcfdi_registropagos.autorizacion),
                    Operacion = str(_dbRowPago.tcfdi_registropagos.operacion),
                    Terminal = str(_dbRowPago.tcfdi_registropagos.empresa_cuentabancaria_terminal_id),
                    Cheque = str(_dbRowPago.tcfdi_registropagos.cheque),
                    Cuenta = str(_dbRowPago.tcfdi_registropagos.cuenta_cliente),
                    Referencia = str(_dbRowPago.tcfdi_registropagos.referencia),
                    Rastreo = str(_dbRowPago.tcfdi_registropagos.claverastero),
                    RFC = str(self._dbRowCFDI.tcfdis.receptorrfc),
                    RazonSocialCliente = str(self._dbRowCFDI.tcfdis.receptornombrerazonsocial),
                    FechaDeposito = str(self._dbRowCFDI.tcfdi_registropagos.fechadeposito)
                    )

                # Se obtiene la cuenta contable dependiendo la forma de pago.
                if _dbRowPago.tcfdi_registropagos.formapago_id == TFORMASPAGO.EFECTIVO:
                # TODO: cambiar complementopagos por la tabla registropagos
                    self._s_proceso = "En caso de efectivo, se procede a identificar la cuenta"
                    # Se saca la cuenta contable de la caja chica con el CFDI.

                    if _dbRowPago.tcfdi_registropagos.tratoefectivo == TCFDI_REGISTROPAGOS.E_TRATOEFECTIVO.RETENIDOCAJA:
                        _D_returnResult = self._getCuentascontables_porEfectivo(
                            _dbRowPago.tcfdi_complementopagos.monedacontpaqi_id
                            )

                        if not _D_returnResult.D_cuentas:
                            _L_msgError_asiento_pago.append(
                                "Cuenta contable de caja chica no definida. Caja chica id = %s; Sucursal id = %s; Plaza id = %s. " % (
                                    self._dbRowCFDI.tempresa_plaza_sucursal_cajaschicas.id,
                                    self._dbRowCFDI.tempresa_plaza_sucursales.id,
                                    self._dbRowCFDI.tempresa_plazas.id
                                    )
                                )
                        else:
                            # No hay problema, si se encontraron las cuentas para el cliente
                            _D_cuentasPago = _D_returnResult.D_cuentas

                            _D_cuentasPago.s_concepto = self.Template(
                                self._D_contabilidad.row_conceptos.pago_efectivo
                                ).safe_substitute(
                                self._D_substituciones
                                )
                            _D_cuentasPago.empresa_segmento_id = None  # No aplica segmento de negocio para este tipo de movimiento
                            _D_cuentasPago.empresa_diario_id = None
                            _s_tipodocumentobancario = None  # No aplica si es efectivo

                    else:
                        _D_returnResult = self._getCuentascontables_porBanco(
                            _dbRowPago.tcfdi_registropagos.empresa_cuentabancaria_id
                            )

                        if not _D_returnResult.D_cuentas:
                            _L_msgError_asiento_pago.append(
                                "Cuenta contable de cuenta bancaria no definida. Cuenta bancaria id = %s. " % _dbRowRegistropago.empresa_cuentabancaria_id
                                )
                        else:
                            # No hay problema, si se encontraron las cuentas para el cliente
                            _D_cuentasPago = _D_returnResult.D_cuentas

                            _D_cuentasPago.s_concepto = self.Template(
                                self._D_contabilidad.row_conceptos.pago_efectivo_depositado
                                ).safe_substitute(
                                self._D_substituciones
                                )

                            # Jamas debe entrar aqui
                        _D_cuentasPago.empresa_segmento_id = None  # No aplica segmento de negocio para este tipo de movimiento
                        _D_cuentasPago.empresa_diario_id = self._dbRowCFDI.tempresa_bancosdiariosespeciales.bancos_cobranza_diario_id


                # Método de pago: PUE    Forma de pago: Cheque o Transferencia
                elif _dbRowPago.tcfdi_registropagos.formapago_id in (TFORMASPAGO.CHEQUE, TFORMASPAGO.TRANSFERENCIA):

                    self._s_proceso = "En caso de cheque o transferencia, se procede a identificar la cuenta"

                    _D_returnResult = self._getCuentascontables_porBanco(
                        _dbRowPago.tcfdi_registropagos.empresa_cuentabancaria_id
                        )

                    if not _D_returnResult.D_cuentas:
                        _L_msgError_asiento_pago.append(
                            "Cuenta contable de cuenta bancaria no definida. Cuenta bancaria id = %s" % self._dbRowCFDI.tcfdi_registropagos.empresa_cuentabancaria_id
                            )
                    else:
                        # No hay problema, si se encontraron las cuentas para el cliente
                        _D_cuentasPago = _D_returnResult.D_cuentas

                    if _dbRowPago.tcfdi_registropagos.formapago_id == TFORMASPAGO.CHEQUE:
                        _D_cuentasPago.s_concepto = self.Template(
                            self._D_contabilidad.row_conceptos.pago_cheque
                            ).safe_substitute(self._D_substituciones)
                    elif _dbRowPago.tcfdi_registropagos.formapago_id == TFORMASPAGO.TRANSFERENCIA:
                        _D_cuentasPago.s_concepto = self.Template(
                            self._D_contabilidad.row_conceptos.pago_transferencia
                            ).safe_substitute(
                            self._D_substituciones
                            )
                    else:
                        _D_cuentasPago.s_concepto = "Error, no debe entrar aqui"
                        # Jamas debe entrar aqui
                    _D_cuentasPago.empresa_segmento_id = None  # No aplica segmento de negocio para este tipo de movimiento
                    _D_cuentasPago.empresa_diario_id = self._dbRowCFDI.tempresa_bancosdiariosespeciales.bancos_cobranza_diario_id

                # Método de pago: PUE    Forma de pago: Tarjeta de credito o debito
                elif _dbRowPago.tcfdi_registropagos.formapago_id in (TFORMASPAGO.TDC, TFORMASPAGO.TDD):

                    self._s_proceso = "En caso de tarjeta, se procede a identificar la cuenta"

                    _D_returnResult = self._getCuentascontables_porBanco(
                        _dbRowPago.tempresa_cuentabancaria_terminales.empresa_cuentabancaria_id
                        )

                    if not _D_returnResult.D_cuentas:
                        _L_msgError_asiento_pago.append(
                            "Cuenta contable de cuenta bancaria no definida. Cuenta bancaria id = %s" % self._dbRowCFDI.tempresa_cuentabancaria_terminales.empresa_cuentabancaria_id
                            )
                    else:
                        # No hay problema, si se encontraron las cuentas para el cliente
                        _D_cuentasPago = _D_returnResult.D_cuentas

                    if _dbRowPago.tcfdi_registropagos.formapago_id == TFORMASPAGO.TDC:
                        _D_cuentasPago.s_concepto = self.Template(
                            self._D_contabilidad.row_conceptos.pago_tdc
                            ).safe_substitute(self._D_substituciones)
                    elif _dbRowPago.tcfdi_registropagos.formapago_id == TFORMASPAGO.TDD:
                        _D_cuentasPago.s_concepto = self.Template(
                            self._D_contabilidad.row_conceptos.pago_tdd
                            ).safe_substitute(self._D_substituciones)
                    else:
                        _D_cuentasPago.s_concepto = "Error, no debe entrar aqui"
                        # Jamas debe entrar aqui
                    _D_cuentasPago.empresa_segmento_id = None  # No aplica segmento de negocio para este tipo de movimiento
                    _D_cuentasPago.empresa_diario_id = self._dbRowCFDI.tempresa_bancosdiariosespeciales.bancos_cobranza_diario_id

                else:
                    self._L_msgError.append("Forma de pago no definida")
                    # Si la forma de pago no esta contemplada, la variable de cuentasIngreso
                    # estará vacía, evitando generar asientos contables

                if self._L_msgError:
                    # Se regresa el error que exista en caso de no identificar la cuenta de ingreso o se detecten errores
                    pass

                else:

                    self._s_proceso = "Se inserta el primer asiento con la cuenta de ingreso"

                    _D_returnResult = self._insertAsientos_ingreso(
                        _D_cuentasPago,
                        D_monto = Storage(
                            monto = _dbRowPago.tcfdi_complementopagos.monto,
                            monedacontpaqi_id = _dbRowPago.tcfdi_complementopagos.monedacontpaqi_id,
                            tipocambio = _dbRowPago.tcfdi_complementopagos.tipocambio
                            ),
                        b_esAbono = False,
                        D_datosAdicionalesIngreso = Storage(
                            descripcion = _D_cuentasPago.s_concepto,
                            cfdi_ingreso_id = None,
                            empresa_diario_id = _D_cuentasPago.empresa_diario_id,
                            empresa_segmento_id = _D_cuentasPago.empresa_segmento_id,
                            esingreso = True,
                            escliente = False,
                            errores = "".join(_L_msgError_asiento_pago)
                            ),
                        D_datosAdicionalesComplemento = Storage(
                            descripcion = _D_cuentasPago.s_concepto,
                            cfdi_ingreso_id = None,
                            empresa_diario_id = _D_cuentasPago.empresa_diario_id,
                            empresa_segmento_id = _D_cuentasPago.empresa_segmento_id,
                            empresa_tipodocumentobancario_id = None,
                            esingreso = False,
                            escliente = False,
                            errores = "".join(_L_msgError_asiento_pago)
                            ),
                        )

                    # Se graban los errores y se limpia la variable local para los nuevos errores de asientos
                    self._L_msgError_asiento += _L_msgError_asiento_pago
                    _L_msgError_asiento_pago = []

                    # Se asocian los documentos relacionados pendientes de asociación, en caso de existir
                    TCFDI_COMPLEMENTOPAGOS.asociar_uuid(_dbRowPago.tcfdi_complementopagos.id)
                    # TODO, usar mensajes y codigos de regreso para crear notificaciones

                    # Se obtiene los documentos relacionados del Pago relacionado para sacar por sucursal/plaza de Documento relacionado e insertar sus asientos contables.
                    _dbRowsDoctosRelacionados = dbc01(
                        (
                                dbc01.tcfdi_complementopago_doctosrelacionados.cfdi_complementopago_id == _dbRowPago.tcfdi_complementopagos.id)
                        & (dbc01.tcfdis.id == dbc01.tcfdi_complementopago_doctosrelacionados.cfdirelacionado_id)
                        & (dbc01.tempresas.id == dbc01.tcfdis.empresa_id)
                        & (
                                dbc01.tempresa_plaza_sucursal_cajaschicas.id == dbc01.tcfdis.empresa_plaza_sucursal_cajachica_id)
                        ).select(
                        dbc01.tcfdis.ALL,
                        dbc01.tempresas.ALL,
                        dbc01.tempresa_plaza_sucursal_cajaschicas.ALL,
                        dbc01.tcfdi_registropagos.ALL,
                        dbc01.tempresa_cuentabancaria_terminales.ALL,
                        dbc01.tempresa_plazas.id,
                        dbc01.tempresa_plazas.nombrecorto,
                        dbc01.tempresa_plaza_sucursales.id,
                        dbc01.tempresa_plaza_sucursales.nombrecorto,
                        dbc01.tempresa_clientes.ALL,
                        dbc01.tcfdi_complementopago_doctosrelacionados.ALL,
                        orderby = dbc01.tcfdi_registropagos.id,
                        left = [
                            dbc01.tcfdi_registropagos.on(dbc01.tcfdi_registropagos.cfdi_id == dbc01.tcfdis.id),
                            dbc01.tempresa_cuentabancaria_terminales.on(
                                dbc01.tempresa_cuentabancaria_terminales.id == dbc01.tcfdi_registropagos.empresa_cuentabancaria_terminal_id
                                ),
                            dbc01.tempresa_plaza_sucursales.on(
                                dbc01.tempresa_plaza_sucursales.id == dbc01.tempresa_plaza_sucursal_cajaschicas.empresa_plaza_sucursal_id
                                ),
                            dbc01.tempresa_plazas.on(
                                dbc01.tempresa_plazas.id == dbc01.tempresa_plaza_sucursales.empresa_plaza_id
                                ),
                            dbc01.tempresa_clientes.on(dbc01.tempresa_clientes.id == dbc01.tcfdis.receptorcliente_id),
                            ]
                        )

                    if not _dbRowsDoctosRelacionados:
                        self._L_msgError.append(
                            "El Complemento pago %s no cuenta con CFDIs relacionados" % _dbRowPago.tcfdi_complementopagos.id
                            )
                        _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                        pass
                    else:
                        _L_errores_asientos = []
                        _L_errores = []
                        _D_registropago = Storage()

                        for _dbRowDoctoRel in _dbRowsDoctosRelacionados:

                            _D_cuentasPago = Storage()

                            self._s_proceso = "Se procede a identificar la cuenta por cliente en el documento relacionado"

                            _D_returnResult = self._getCuentascontables_porCliente(
                                _dbRowDoctoRel,
                                _dbRowPago.tcfdi_complementopagos.monedacontpaqi_id,
                                )

                            if not _D_returnResult.D_cuentas:
                                _L_msgError_asiento_pago.append(
                                    "Cuenta contable de cliente no definida. Cliente id = %s. " % self._dbRowCFDI.tcfdis.receptorcliente_id
                                    )
                            else:
                                _D_cuentasPago = _D_returnResult.D_cuentas

                            _D_cuentasPago.empresa_segmento_id = None  # No aplica segmento de negocio para este tipo de movimiento

                            if self._L_msgError:
                                break
                                pass
                            else:

                                _D_returnResult = self._insertAsientos_ingreso(
                                    _D_cuentasPago,
                                    D_monto = Storage(
                                        monto = _dbRowDoctoRel.tcfdi_complementopago_doctosrelacionados.imppagado,
                                        monedacontpaqi_id = _dbRowDoctoRel.tcfdi_complementopago_doctosrelacionados.monedacontpaqi_id,
                                        tipocambio = _dbRowDoctoRel.tcfdi_complementopago_doctosrelacionados.tipocambio
                                        ),
                                    b_esAbono = True,
                                    D_datosAdicionalesIngreso = Storage(
                                        descripcion = self.Template(
                                            self._D_contabilidad.row_conceptos.pago_cliente
                                            ).safe_substitute(
                                            self._D_substituciones
                                            ),
                                        cfdi_ingreso_id = _dbRowDoctoRel.tcfdis.id,
                                        empresa_diario_id = None,
                                        empresa_segmento_id = _D_cuentasPago.empresa_segmento_id,
                                        esingreso = False,
                                        escliente = True,
                                        errores = "".join(_L_msgError_asiento_pago)
                                        ),
                                    D_datosAdicionalesComplemento = Storage(
                                        descripcion = self.Template(
                                            self._D_contabilidad.row_conceptos.pago_cliente
                                            ).safe_substitute(
                                            self._D_substituciones
                                            ),
                                        cfdi_ingreso_id = _dbRowDoctoRel.tcfdis.id,
                                        empresa_diario_id = None,
                                        empresa_segmento_id = _D_cuentasPago.empresa_segmento_id,
                                        empresa_tipodocumentobancario_id = None,
                                        esingreso = False,
                                        escliente = False,
                                        errores = "".join(_L_msgError_asiento_pago)
                                        ),
                                    )

                                # Se graban los errores y se limpia la variable local para los nuevos errores de asientos
                                self._L_msgError_asiento += _L_msgError_asiento_pago
                                _L_msgError_asiento_pago = []

                            # TODO barrer por los conceptos y registrar los cambios de Ventas por estructura no cobradas a cobradas

                            _dbQry = (
                                    (
                                            dbc01.tcfdi_prepoliza_asientoscontables.cfdi_ingreso_id == _dbRowDoctoRel.tcfdis.id)
                                    & (dbc01.tcfdi_prepoliza_asientoscontables.esimpuesto_portrasladar == True)
                            )

                            # Se determinan los asientos contables relacionado a los impuestos para el documento relacionado
                            _dbField_sumCargos = dbc01.tcfdi_prepoliza_asientoscontables.importe_cargo.sum().with_alias(
                                'cargos'
                                )
                            _dbField_sumAbonos = dbc01.tcfdi_prepoliza_asientoscontables.importe_abono.sum().with_alias(
                                'abonos'
                                )

                            _dbFields_toSelect = [
                                dbc01.tcfdi_prepoliza_asientoscontables.impuesto_id,
                                dbc01.tcfdi_prepoliza_asientoscontables.tasaocuota,
                                _dbField_sumCargos,
                                _dbField_sumAbonos
                                ]

                            _dbRowsAsientosImpuestos = dbc01(_dbQry).select(
                                *_dbFields_toSelect,
                                groupby = [
                                    dbc01.tcfdi_prepoliza_asientoscontables.impuesto_id,
                                    dbc01.tcfdi_prepoliza_asientoscontables.tasaocuota,
                                    ]
                                )

                            _n_totalCargos = 0
                            _n_totalAbonos = 0

                            _n_proporcionPagoImpuestos = (_dbRowDoctoRel.tcfdis.totalimpuestostrasladados + _dbRowDoctoRel.tcfdis.totalimpuestosretenidos) / _dbRowDoctoRel.tcfdis.total

                            _D_impuestos = Storage()
                            # Se verifica si el documento relacionado trae impuestos de la prepoliza.
                            if _dbRowsAsientosImpuestos:
                                # No hace nada, tiene prepoliza generada el docto relacionado
                                pass
                            else:

                                # _D_returnGenerarPrepolizaDctoRelacionado = STV_FWK_PREPOLIZA().generar(_dbRowDoctoRel.tcfdis.id, b_crearPoliza = False, b_cambiarEstado = False)
                                # No importa el error regresado, ya que se cacha en la siguiente consulta

                                _dbRowsAsientosImpuestos = dbc01(_dbQry).select(
                                    *_dbFields_toSelect,
                                    groupby = [
                                        dbc01.tcfdi_prepoliza_asientoscontables.impuesto_id,
                                        dbc01.tcfdi_prepoliza_asientoscontables.tasaocuota,
                                        ]
                                    )

                                # Validación sobre _dbRowsAsientosImpuestos no es requerida, ya que existe el caso de que cuando la tasa es cero, no se crean asientos, por lo que no se regresan registros

                            for _dbRowAsientoImpuesto in _dbRowsAsientosImpuestos:

                                if _dbRowAsientoImpuesto.cargos:
                                    _n_totalCargos += _dbRowAsientoImpuesto.cargos
                                else:
                                    # Continuar sin sumar cargos
                                    pass
                                if _dbRowAsientoImpuesto.abonos:
                                    _n_totalAbonos += _dbRowAsientoImpuesto.abonos
                                else:
                                    # Continuar sin sumar abonos
                                    pass

                                _D_impuestos[
                                    str(_dbRowAsientoImpuesto.tcfdi_prepoliza_asientoscontables.impuesto_id)
                                    + "_" + str(_dbRowAsientoImpuesto.tcfdi_prepoliza_asientoscontables.tasaocuota)
                                    ] = Storage(
                                    impuesto_id = _dbRowAsientoImpuesto.tcfdi_prepoliza_asientoscontables.impuesto_id,
                                    tasaocuota = _dbRowAsientoImpuesto.tcfdi_prepoliza_asientoscontables.tasaocuota or 0,
                                    sumCargos = _dbRowAsientoImpuesto.cargos,
                                    sumAbonos = _dbRowAsientoImpuesto.abonos,
                                    sumTotal = _dbRowAsientoImpuesto.cargos - _dbRowAsientoImpuesto.abonos
                                    )
                            # Si no tiene con los documentos relacionados se investigan los impuestos de las facturas

                            _n_total = _n_totalCargos - _n_totalAbonos

                            for _s_impuesto in _D_impuestos:

                                _D_impuesto = _D_impuestos[_s_impuesto]
                                if (_n_total > 0) and (_D_impuesto.sumTotal > 0):
                                    # Si existen resultados mayores a cero se calcula el peso
                                    _n_peso = _D_impuesto.sumTotal / _n_total
                                else:
                                    # Si hay errores en el calculo del peso, se genera de forma equitativa entre los diferentes impuestos
                                    _n_peso = 1 / Decimal(len(_D_impuestos))

                                if _D_impuesto.impuesto_id == TIMPUESTOS.IVA:

                                    self._s_proceso = "Se obtienen las cuentas de los impuestos"

                                    _D_returnResult = self._getCuentascontablesIVA(_D_impuesto)

                                    if not _D_returnResult.D_cuentasIVA:
                                        _L_msgError_asiento_pago.append(
                                            "No se encontró información de contabilización para el IVA"
                                            )
                                    else:
                                        _D_cuentaIva = _D_returnResult.D_cuentasIVA

                                    self._s_proceso = "Se insertan los asientos de los impuestos"
                                    _D_impuesto.importe_acumulado = _n_proporcionPagoImpuestos * _n_peso * _dbRowDoctoRel.tcfdi_complementopago_doctosrelacionados.imppagado

                                    _D_cuentaIva.selected_cuenta_contable = _D_cuentaIva.cuentacontable_trasladado

                                    self._D_substituciones.update(
                                        Impuesto = 'IVA',
                                        Tasa = '{:0,.2f}%'.format(_D_impuesto.tasaocuota * 100),
                                        )

                                    # Se procede a insertar el asiento por el IVA
                                    _D_returnResult = self._insertAsientosImpuesto(
                                        _D_cuentaIva,
                                        D_monto = Storage(
                                            porTrasladar = _D_impuesto.importe_acumulado,
                                            trasladado = _D_impuesto.importe_acumulado,
                                            monedacontpaqi_id = _dbRowPago.tcfdi_complementopagos.monedacontpaqi_id,
                                            tipocambio = _dbRowPago.tcfdi_complementopagos.tipocambio
                                            ),
                                        D_esAbono = Storage(
                                            porTrasladar = False,
                                            trasladado = True
                                            ),
                                        D_datosAdicionalesPorTrasladar = Storage(
                                            descripcion = self.Template(
                                                self._D_contabilidad.row_conceptos.pago_impportrasladar
                                                ).safe_substitute(
                                                self._D_substituciones
                                                ),
                                            cfdi_ingreso_id = _dbRowDoctoRel.tcfdis.id,
                                            empresa_segmento_id = None,
                                            impuesto_id = _D_impuesto.impuesto_id,
                                            tasaocuota = _D_impuesto.tasaocuota,
                                            errores = "".join(_L_msgError_asiento_pago)
                                            ),
                                        D_datosAdicionalesTrasladado = Storage(
                                            descripcion = self.Template(
                                                self._D_contabilidad.row_conceptos.pago_imptrasladado
                                                ).safe_substitute(
                                                self._D_substituciones
                                                ),
                                            cfdi_ingreso_id = _dbRowDoctoRel.tcfdis.id,
                                            empresa_segmento_id = None,
                                            impuesto_id = _D_impuesto.impuesto_id,
                                            tasaocuota = _D_impuesto.tasaocuota,
                                            errores = "".join(_L_msgError_asiento_pago)
                                            ),
                                        )

                                    # Se graban los errores y se limpia la variable local para los nuevos errores de asientos
                                    self._L_msgError_asiento += _L_msgError_asiento_pago
                                    _L_msgError_asiento_pago = []

                                else:
                                    # Impuesto no considerado
                                    self._L_msgError.append("Impuesto no considerado: " + str(_D_impuesto.impuesto_id))

        else:

            self._L_msgError.append("No se pudo crear la prepoliza")

            # No se pudo crear el header de la prepoliza correctamente
            pass

        return _D_return

    def _egreso(self):
        """ Función para la generación de la prepoliza de tipo Egreso
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.NOK_ERROR,
            )
        _L_msgError_asiento_egreso = []

        self._s_proceso = "Se inserta prepoliza maestro"
        _D_returnResult = self._insertHeader()

        self._s_proceso = "Se identifica la información para el primer asiento contable"

        self._D_substituciones.update(
            Tarjeta = str(self._dbRowCFDI.tcfdi_registropagos.tarjeta),
            Banco = str(self._dbRowCFDI.tcfdi_registropagos.banco_id_cliente),
            Autorizacion = str(self._dbRowCFDI.tcfdi_registropagos.autorizacion),
            Operacion = str(self._dbRowCFDI.tcfdi_registropagos.operacion),
            Terminal = str(self._dbRowCFDI.tcfdi_registropagos.empresa_cuentabancaria_terminal_id),
            Cheque = str(self._dbRowCFDI.tcfdi_registropagos.cheque),
            Cuenta = str(self._dbRowCFDI.tcfdi_registropagos.cuenta_cliente),
            Referencia = str(self._dbRowCFDI.tcfdi_registropagos.referencia),
            Rastreo = str(self._dbRowCFDI.tcfdi_registropagos.claverastero),
            RFC = str(self._dbRowCFDI.tcfdis.receptorrfc),
            RazonSocialCliente = str(self._dbRowCFDI.tcfdis.receptornombrerazonsocial),
            FechaDeposito = str(self._dbRowCFDI.tcfdi_registropagos.fechadeposito)
            )

        if self._L_msgError:

            # Se regresa el error que exista en caso de no identificar la cuenta de ingreso o se detecten errores
            pass
        elif self._n_prepoliza_id:

            self._s_proceso = "Se identifican los conceptos del CFDI"

            # Se obtienene el id de ingreso del documento relacionado al egreso
            _dbRowsDoctosRelacionados = dbc01(
                (dbc01.tcfdi_relacionados.cfdi_id == self._s_cfdi_id)
                ).select(
                dbc01.tcfdi_relacionados.ALL,
                orderby = dbc01.tcfdi_relacionados.id,
                )
            if len(_dbRowsDoctosRelacionados) > 0:
                _n_cfdi_ingreso_id = _dbRowsDoctosRelacionados.last().id
            else:
                _n_cfdi_ingreso_id = None

            # Validación para ver si el CFDI de ingreso es de la misma empresa que el CFDI de egreso
            _dbRowsCFDIRelacionado = dbc01(
                (dbc01.tcfdis.id == _n_cfdi_ingreso_id)
                & (dbc01.tcfdis.empresa_id != self._dbRowCFDI.tempresas.id)
                ).select(
                dbc01.tcfdis.ALL
                )

            if _dbRowsCFDIRelacionado:
                self._L_msgError.append('La empresa del CFDI relacionado no coincide con la del CFDI de egreso')
            else:
                pass

            # Variable usada para evitar el procesamiento de un concepto con diferentes estructuras
            _n_last_cfdi_concepto_id = None

            # Empezando a barrer los conceptos
            _dbRows_cfdi_conceptos = dbc01(
                (dbc01.tcfdi_conceptos.cfdi_id == self._s_cfdi_id)
                & (dbc01.tempresa_prodserv.id == dbc01.tcfdi_conceptos.empresa_prodserv_id)
                ).select(
                dbc01.tcfdi_conceptos.ALL,
                dbc01.tempresa_prodserv.ALL,
                dbc01.tempresa_prodserv_ingresosestructuras.empresa_ingresoestructura_id,
                orderby = [
                    dbc01.tcfdi_conceptos.id,
                    dbc01.tempresa_prodserv.id,
                    ~dbc01.tempresa_prodserv_ingresosestructuras.ejercicio
                    ],
                left = [
                    dbc01.tempresa_prodserv_ingresosestructuras.on(
                        (dbc01.tempresa_prodserv_ingresosestructuras.empresa_prodserv_id == dbc01.tempresa_prodserv.id)
                        & (dbc01.tempresa_prodserv_ingresosestructuras.ejercicio <= self._s_aniocfdi)
                        )
                    ]
                )

            # Variable que acumulará los impuestos trasladados
            _D_impuestostrasladados = Storage()
            for _dbRowCFDIConcepto in _dbRows_cfdi_conceptos:

                if _n_last_cfdi_concepto_id == _dbRowCFDIConcepto.tcfdi_conceptos.id:
                    # Como el ultimo procesado es igual al que se esta verificando
                    # debe ser ignorado y continuar con el siguiente
                    pass
                else:

                    # Variable que contendrá las cuentas para generar los asientos contables por la venta
                    _D_cuentasVenta = Storage()

                    _n_last_cfdi_concepto_id = _dbRowCFDIConcepto.tcfdi_conceptos.id

                    _n_empresa_ingresoestructura_id = _dbRowCFDIConcepto.tempresa_prodserv_ingresosestructuras.empresa_ingresoestructura_id

                    if not _n_empresa_ingresoestructura_id:
                        _L_msgError_asiento_egreso.append(
                            "Producto o Servicio no tiene estructura de ingreso asociada. Producto/Servicio id = %s. " % _dbRowCFDIConcepto.tempresa_prodserv.id
                            )
                    else:
                        self._s_proceso = "Se identifican cuentas contables para la estructura de venta"

                        _D_returnResult = self._getCuentascontables_ventas(
                            _n_empresa_ingresoestructura_id,
                            _dbRowCFDIConcepto
                            )

                        if not _D_returnResult.D_cuentasVenta:
                            _L_msgError_asiento_egreso.append(
                                "No se encontró información de contabilización para el ingreso estructura con id: %s " % _n_empresa_ingresoestructura_id
                                )
                        else:
                            _D_cuentasVenta = _D_returnResult.D_cuentasVenta

                    if not self._L_msgError:

                        self._s_proceso = "Se insertan los asientos por la venta"

                        self._D_substituciones.update(
                            Concepto = str(_dbRowCFDIConcepto.tcfdi_conceptos.noidentificacion),
                            Cantidad = str(_dbRowCFDIConcepto.tcfdi_conceptos.cantidad),
                            PrecioUnitario = '${:0,.2f}'.format(_dbRowCFDIConcepto.tcfdi_conceptos.valorunitario),
                            Importe = '${:0,.2f}'.format(_dbRowCFDIConcepto.tcfdi_conceptos.importe),
                            Descuento = '${:0,.2f}'.format(_dbRowCFDIConcepto.tcfdi_conceptos.descuento or 0),
                            Total = '${:0,.2f}'.format(
                                _dbRowCFDIConcepto.tcfdi_conceptos.importe - (
                                        _dbRowCFDIConcepto.tcfdi_conceptos.descuento or 0)
                                ),
                            )

                        _D_returnResult = self._insertAsientosVenta(
                            _D_cuentasVenta,
                            D_monto = Storage(
                                monto = _dbRowCFDIConcepto.tcfdi_conceptos.importe,
                                descuento = _dbRowCFDIConcepto.tcfdi_conceptos.descuento or 0,
                                monedacontpaqi_id = self._dbRowCFDI.tcfdis.monedacontpaqi_id,
                                tipocambio = self._dbRowCFDI.tcfdis.tipocambio
                                ),
                            b_esAbono = False,
                            D_datosAdicionalesVenta = Storage(
                                descripcion = self.Template(
                                    self._D_contabilidad.row_conceptos.egreso_venta
                                    ).safe_substitute(
                                    self._D_substituciones
                                    ),
                                cfdi_ingreso_id = _n_cfdi_ingreso_id,
                                empresa_diario_id = _D_cuentasVenta.selected_diario_especial,
                                empresa_segmento_id = self._D_contabilidad.segmento_id,
                                esconcepto_porcobrar = False,
                                esconcepto_cobrado = False,
                                errores = "".join(_L_msgError_asiento_egreso)
                                ),
                            D_datosAdicionalesDescuento = Storage(
                                descripcion = self.Template(
                                    self._D_contabilidad.row_conceptos.egreso_descuento
                                    ).safe_substitute(
                                    self._D_substituciones
                                    ),
                                cfdi_ingreso_id = _n_cfdi_ingreso_id,
                                empresa_diario_id = _D_cuentasVenta.selected_diario_especial,
                                empresa_segmento_id = self._D_contabilidad.segmento_id,
                                esconcepto_porcobrar = False,
                                esconcepto_cobrado = False,
                                errores = "".join(_L_msgError_asiento_egreso)
                                ),
                            )

                        # Se graban los errores y se limpia la variable local para los nuevos errores de asientos
                        self._L_msgError_asiento += _L_msgError_asiento_egreso
                        _L_msgError_asiento_egreso = []

                        # Empezando a barrer los conceptos
                        self._s_proceso = "Se obtienen los impuestos trasladados"

                        _dbRows_cfdi_conceptos_impuestostrasladados = dbc01(
                            (
                                    dbc01.tcfdi_concepto_impuestostrasladados.cfdi_concepto_id == _dbRowCFDIConcepto.tcfdi_conceptos.id)
                            ).select(
                            dbc01.tcfdi_concepto_impuestostrasladados.ALL,
                            )

                        if _dbRows_cfdi_conceptos_impuestostrasladados:
                            for _dbRow_impuestotrasladado in _dbRows_cfdi_conceptos_impuestostrasladados:
                                # Se define un index para poder agrupar los impuestos donde correspondan al mismo tipo y tasa
                                _s_index = str(_dbRow_impuestotrasladado.impuesto_id) + "_" + str(
                                    _dbRow_impuestotrasladado.tasaocuota
                                    )
                                if _s_index in _D_impuestostrasladados:
                                    _D_impuestostrasladados[
                                        _s_index].importe_acumulado += _dbRow_impuestotrasladado.importe or 0
                                else:
                                    _D_impuestostrasladados[_s_index] = Storage(
                                        impuesto_id = _dbRow_impuestotrasladado.impuesto_id,
                                        tasaocuota = _dbRow_impuestotrasladado.tasaocuota or 0,
                                        importe_acumulado = _dbRow_impuestotrasladado.importe
                                        )
                        else:
                            # No existen impuestos trasladados
                            pass

                        # TODO agregar asientos en caso de moneda extranjera

                    else:
                        # Si no se encontraron las cuentas contables, se mantienen los errores, y se continua checando los otros asientos
                        pass

                    # En este punto termina el if de cuenta venta

                # En este punto termina el if de que se repite el concepto en el for

            # Cuando termina el for sobre todos los conceptos, se genera un asiento por el IVA

            self._s_proceso = "En caso de credito, se procede a identificar la cuenta por cliente"
            _D_returnResult = self._getCuentascontables_porCliente(self._dbRowCFDI)
            _D_cuentasIngreso = Storage()

            if not _D_returnResult.D_cuentas:
                _L_msgError_asiento_egreso.append(
                    "Cuenta contable de cliente no definida. Cliente id = %s. " % self._dbRowCFDI.tcfdis.receptorcliente_id
                    )
            else:
                _D_cuentasIngreso = _D_returnResult.D_cuentas
            _D_cuentasIngreso.empresa_segmento_id = None  # No aplica segmento de negocio para este tipo de movimiento

            _D_returnResult = self._insertAsientos_ingreso(
                _D_cuentasIngreso,
                D_monto = Storage(
                    monto = self._dbRowCFDI.tcfdis.total,
                    monedacontpaqi_id = self._dbRowCFDI.tcfdis.monedacontpaqi_id,
                    tipocambio = self._dbRowCFDI.tcfdis.tipocambio
                    ),
                b_esAbono = True,
                D_datosAdicionalesIngreso = Storage(
                    descripcion = self.Template(self._D_contabilidad.row_conceptos.egreso_cliente).safe_substitute(
                        self._D_substituciones
                        ),
                    cfdi_ingreso_id = _n_cfdi_ingreso_id,
                    empresa_diario_id = None,
                    empresa_segmento_id = _D_cuentasIngreso.empresa_segmento_id,
                    esingreso = False,
                    escliente = False,
                    errores = "".join(_L_msgError_asiento_egreso)
                    ),
                D_datosAdicionalesComplemento = Storage(
                    descripcion = self.Template(self._D_contabilidad.row_conceptos.egreso_cliente).safe_substitute(
                        self._D_substituciones
                        ),
                    cfdi_ingreso_id = _n_cfdi_ingreso_id,
                    empresa_diario_id = None,
                    empresa_segmento_id = _D_cuentasIngreso.empresa_segmento_id,
                    empresa_tipodocumentobancario_id = None,
                    esingreso = False,
                    escliente = False,
                    errores = "".join(_L_msgError_asiento_egreso)
                    ),
                )

            # Se graban los errores y se limpia la variable local para los nuevos errores de asientos
            self._L_msgError_asiento += _L_msgError_asiento_egreso
            _L_msgError_asiento_egreso = []

            for _s_indice in _D_impuestostrasladados:

                _D_impuestotrasladado = _D_impuestostrasladados[_s_indice]

                if _D_impuestotrasladado.impuesto_id == TIMPUESTOS.IVA:

                    self._s_proceso = "Se obtienen las cuentas de los impuestos"

                    _D_returnResult = self._getCuentascontablesIVA(_D_impuestotrasladado)

                    if not _D_returnResult.D_cuentasIVA:
                        _L_msgError_asiento_egreso.append("No se encontró información de contabilización para el IVA")
                    else:
                        _D_cuentaIva = _D_returnResult.D_cuentasIVA

                    if _D_cuentaIva:

                        _D_monto = Storage(
                            porTrasladar = 0,
                            trasladado = 0,
                            monedacontpaqi_id = self._dbRowCFDI.tcfdis.monedacontpaqi_id,
                            tipocambio = self._dbRowCFDI.tcfdis.tipocambio
                            )

                        ### TODO: ¿Aquí se deja la forma de pago del CFDI?

                        if self._dbRowCFDI.tcfdis.formapago_id == TFORMASPAGO.POR_DEFINIR:
                            # Si se paga a credito, no se inserta iva trasladado
                            _D_monto.porTrasladar = _D_impuestotrasladado.importe_acumulado * (-1)
                        else:
                            # Si se pagó en una exhibicion, no se inserta asiento de iva por trasladar
                            _D_monto.trasladado = _D_impuestotrasladado.importe_acumulado * (-1)

                        self._s_proceso = "Se insertan los asientos de los impuestos"

                        self._D_substituciones.update(
                            Impuesto = 'IVA',
                            Tasa = '{:0,.2f}%'.format(_D_impuestotrasladado.tasaocuota * 100),
                            )

                        # Se procede a insertar el asiento por el IVA
                        _D_returnResult = self._insertAsientosImpuesto(
                            _D_cuentaIva,
                            D_monto = _D_monto,
                            D_esAbono = Storage(
                                porTrasladar = True,
                                trasladado = True
                                ),
                            D_datosAdicionalesPorTrasladar = Storage(
                                descripcion = self.Template(
                                    self._D_contabilidad.row_conceptos.egreso_impportrasladar
                                    ).safe_substitute(
                                    self._D_substituciones
                                    ),
                                cfdi_ingreso_id = _n_cfdi_ingreso_id,
                                empresa_segmento_id = None,
                                impuesto_id = _D_impuestotrasladado.impuesto_id,
                                tasaocuota = _D_impuestotrasladado.tasaocuota,
                                errores = "".join(_L_msgError_asiento_egreso)
                                ),
                            D_datosAdicionalesTrasladado = Storage(
                                descripcion = self.Template(
                                    self._D_contabilidad.row_conceptos.egreso_imptrasladado
                                    ).safe_substitute(
                                    self._D_substituciones
                                    ),
                                cfdi_ingreso_id = _n_cfdi_ingreso_id,
                                empresa_segmento_id = None,
                                impuesto_id = _D_impuestotrasladado.impuesto_id,
                                tasaocuota = _D_impuestotrasladado.tasaocuota,
                                errores = "".join(_L_msgError_asiento_egreso)
                                ),
                            )

                        # Se graban los errores y se limpia la variable local para los nuevos errores de asientos
                        self._L_msgError_asiento += _L_msgError_asiento_egreso
                        _L_msgError_asiento_egreso = []

                    else:
                        # Tasa-impuesto no encontrado
                        self._L_msgError.append("Tasa no encontrada: " + str(_D_impuestotrasladado.tasaocuota))

                else:
                    # Impuesto no considerado
                    self._L_msgError.append("Impuesto no considerado: " + str(_D_impuestotrasladado.impuesto_id))

        else:
            # Se regresa el error que exista
            self._L_msgError.append("Prepoliza de ingreso no se pudo generar")
            pass

        return _D_return

    def _insertHeader(self):
        """Función para insertar el header de la prepoliza contable
        """

        _D_return = Storage(
            E_return = CLASS_e_RETURN.NOK_ERROR,
            )

        _dbTable = dbc01.tempresa_plaza_sucursal_contabilidades
        _dbQry = (
                _dbTable.empresa_plaza_sucursal_id == self._dbRowCFDI.tempresa_plaza_sucursal_cajaschicas.empresa_plaza_sucursal_id)

        self._D_contabilidad = self._getCamposPorEjercicio(
            dbTable = _dbTable,
            dbQry = _dbQry,
            D_regresarCampos = Storage(
                tipopoliza_ingreso_id = 'empresa_tipopoliza_ingreso_id',
                tipopoliza_pago_id = 'empresa_tipopoliza_pago_id',
                tipopoliza_egreso_id = 'empresa_tipopoliza_egreso_id',
                diario_id = 'empresa_diario_id',
                segmento_id = 'empresa_segmento_id',
                concepto_ingreso = 'concepto_ingreso',
                concepto_pago = 'concepto_pago',
                concepto_egreso = 'concepto_egreso',
                )
            )

        _dbRows_conceptosCuentasContables = dbc01(
            (dbc01.tempresa_conceptosasientoscontables.empresa_id == self._dbRowCFDI.tempresas.id)
            & (dbc01.tempresa_conceptosasientoscontables.bansuspender == False)
            ).select(
            dbc01.tempresa_conceptosasientoscontables.ALL,
            orderby = [
                dbc01.tempresa_conceptosasientoscontables.id
                ]
            )
        if _dbRows_conceptosCuentasContables:
            self._D_contabilidad.row_conceptos = _dbRows_conceptosCuentasContables.last()
        else:
            self._D_contabilidad.row_conceptos = None
            self._L_msgError.append("Conceptos no definidos en la empresa")

        if self._D_contabilidad:

            if self._dbRowCFDI.tcfdis.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO:
                self._D_contabilidad.tipopoliza_id = self._D_contabilidad.tipopoliza_ingreso_id
                self._D_contabilidad.concepto = self._D_contabilidad.concepto_ingreso

            elif self._dbRowCFDI.tcfdis.tipocomprobante_id == TTIPOSCOMPROBANTES.PAGO:
                self._D_contabilidad.tipopoliza_id = self._D_contabilidad.tipopoliza_pago_id
                self._D_contabilidad.concepto = self._D_contabilidad.concepto_pago

            elif self._dbRowCFDI.tcfdis.tipocomprobante_id == TTIPOSCOMPROBANTES.EGRESO:
                self._D_contabilidad.tipopoliza_id = self._D_contabilidad.tipopoliza_egreso_id
                self._D_contabilidad.concepto = self._D_contabilidad.concepto_egreso

            else:
                self._D_contabilidad.tipopoliza_id = 0
                # TODO que pasa con egresos o pagos
                _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                self._L_msgError.append("Tipo de comprobante no identificado")
                pass

            _D_prepoliza_insert = Storage(
                cfdi_id = self._dbRowCFDI.tcfdis.id,
                empresa_tipopoliza_id = self._D_contabilidad.tipopoliza_id,
                empresa_diario_id = self._D_contabilidad.diario_id,
                concepto = self.Template(str(self._D_contabilidad.concepto)).safe_substitute(self._D_substituciones),
                sat_status_cancelacion = TCFDI_PREPOLIZAS.SAT_STATUS.CANCELADO if self._b_esCancelado else TCFDI_PREPOLIZAS.SAT_STATUS.VIGENTE,
                tipo_contabilizacion_fecha_cancelacion = self._n_fechaContabilizacion
                )

            if self._D_contabilidad.tipopoliza_id == None:
                _D_prepoliza_insert.empresa_tipopoliza_id = 1
                self._L_msgError_header.append(
                    "Tipo de poliza para este ejercicio [%s] no identificado" % self._s_aniocfdi
                    )
            else:
                pass

            if self._D_contabilidad.diario_id == None:
                _D_prepoliza_insert.empresa_diario_id = 1
                self._L_msgError_header.append("Diario especial para ejercicio [%s] no identificado" % self._s_aniocfdi)
            else:
                pass

            # Se inserta a la tabla.
            if self._L_msgError_header:
                _D_prepoliza_insert.concepto = (
                        "Prepoliza de ejercicios anteriores - ejercicio [%s]" % self._s_aniocfdi)
            else:
                pass
            self._n_prepoliza_id = dbc01.tcfdi_prepolizas.insert(**_D_prepoliza_insert)
            _D_return.E_return = CLASS_e_RETURN.OK
        else:
            self._L_msgError.append("No se encontro información contable para la sucursal de la caja chica")

        return _D_return

    def _getCliente(self, s_receptorrazonsocial, s_receptorrfc):
        '''Función para agregar al cliente en el CFDI en caso de no tenerlo asociado

            Args:
                s_receptorrazonsocial: Razón social del receptor
                s_receptorrfc: RFC del receptor
        '''
        _D_return = Storage(
            L_msgError = [],
            E_return = CLASS_e_RETURN.NOK_ERROR,
            dbRow_empresa_cliente = None
            )

        _dbRows_empresa_cliente = dbc01(
            (dbc01.tempresa_clientes.razonsocial == s_receptorrazonsocial)
            & (dbc01.tempresa_clientes.rfc == s_receptorrfc)
            ).select(
            dbc01.tempresa_clientes.ALL
            )

        if len(_dbRows_empresa_cliente) == 1:
            # Se actualiza el cliente id en el cfdi
            _D_return.dbRow_empresa_cliente = _dbRows_empresa_cliente.last()
            dbc01.tcfdis(self._s_cfdi_id).update_record(
                receptorcliente_id = _D_return.dbRow_empresa_cliente.id
                )
            _D_return.E_return = CLASS_e_RETURN.OK
        else:
            self._L_msgError.append("Cliente no identificado")

        return _D_return

    def _getCamposPorEjercicio(self, dbTable, dbQry, D_regresarCampos, dbFieldEjercicio = None):
        '''Función generica para obtener campos en base al ejercicio

            Args:
                dbTable: tabla a relacionar
                dbQry: query a utilizar
                D_regresarCampos: campos a regresar en la búsqueda
                [dbFieldEjercicio]: nombre del campo que contiene el ejercicio
        '''

        _D_return = D_regresarCampos

        if dbFieldEjercicio:
            _dbFieldEjercicio = dbFieldEjercicio
        else:
            _dbFieldEjercicio = dbTable['ejercicio']

        _LdbFields = []
        for _s_campo in D_regresarCampos:
            _LdbFields += [dbTable[D_regresarCampos[_s_campo]]]

        _dbRows_ejercicios = dbc01(
            dbQry
            & (_dbFieldEjercicio <= self._s_aniocfdi)
            ).select(
            *_LdbFields,
            orderby = ~_dbFieldEjercicio,
            limitby = (0, 1)
            )

        if _dbRows_ejercicios:
            _dbRow_ejercicio = _dbRows_ejercicios.first()
            for _s_campo in D_regresarCampos:
                _D_return[_s_campo] = _dbRow_ejercicio[D_regresarCampos[_s_campo]]
        else:
            # No hay cuentas contables detectadas
            _D_return = Storage()

        return _D_return

    def _getCuentascontables_porEfectivo(self, s_monedacontpaqi_id):
        '''Función que regresa las cuentas contables en caso de pago en efectivo
        Esta función usa la caja chica para obtener las cuentas contables

            Args:
                s_monedacontpaqi_id: Moneda de la cual se obtendrán las cuentas
        '''
        _D_return = Storage(
            L_msgError = [],
            E_return = CLASS_e_RETURN.NOK_ERROR,
            D_cuentas = Storage(),
            )

        _dbTable = dbc01.tempresa_plaza_sucursal_cajachica_guias
        _dbQry = (
                (
                        _dbTable.empresa_plaza_sucursal_cajaschicas_id == self._dbRowCFDI.tempresa_plaza_sucursal_cajaschicas.id)
                & (_dbTable.monedacontpaqi_id == s_monedacontpaqi_id)
        )

        _D_return.D_cuentas = self._getCamposPorEjercicio(
            dbTable = _dbTable,
            dbQry = _dbQry,
            D_regresarCampos = Storage(
                cuenta_contable = 'cuenta_contable_cajachica',
                cuenta_contable_complementaria = 'cuenta_contable_cajachica_complemento',
                )
            )

        if _D_return.D_cuentas:
            _D_return.D_cuentas.s_origen = "Efectivo"
            _D_return.E_return = CLASS_e_RETURN.OK
        else:
            pass

        return _D_return

    def _getCuentascontables_diferencias(self, s_monedacontpaqi_id, n_monto):
        ''' Función que regresa las cuentas contables en caso de que haya alguna diferencia contable.

            Args:
                s_monedacontpaqi_id: Moneda de la cual se obtendrán las cuentas
        '''
        _D_return = Storage(
            L_msgError = [],
            E_return = CLASS_e_RETURN.NOK_ERROR,
            D_cuentas = Storage(),
            )

        _dbTable = dbc01.tempresa_plaza_sucursal_cajachica_guias
        _dbQry = (
                (
                        _dbTable.empresa_plaza_sucursal_cajaschicas_id == self._dbRowCFDI.tempresa_plaza_sucursal_cajaschicas.id)
                & (_dbTable.monedacontpaqi_id == s_monedacontpaqi_id)
        )

        # Si el monto es mayor o menor a cero definirá que cuenta contable traer.
        if n_monto > 0:
            # Si es mayor a cero se trae la cuenta de faltantes.
            _s_dbField = 'cuenta_contable_cajachica_faltante'
        else:
            # De lo contrario se traerá la de sobrante.
            _s_dbField = 'cuenta_contable_cajachica_sobrante'

        _D_return.D_cuentas = self._getCamposPorEjercicio(
            dbTable = _dbTable,
            dbQry = _dbQry,
            D_regresarCampos = Storage(
                cuenta_contable = _s_dbField,
                cuenta_contable_complementaria = 'cuenta_contable_cajachica_complemento',
                )
            )

        if _D_return.D_cuentas:
            _D_return.D_cuentas.s_origen = "Diferencia"
            _D_return.E_return = CLASS_e_RETURN.OK
        else:
            pass

        return _D_return

    def _getCuentascontables_porBanco(self, empresa_cuentabancaria_id):
        '''Función que regresa las cuentas contables aplicables a cheque o transferencia o tarjeta
        Usando la cuenta bancaria se obtienen las cuentas contables

            Args:
                empresa_cuentabancaria_id: Como hay dos maneras de obtener la cuenta bancaria, terminal o cuenta bancaria
        '''
        _D_return = Storage(
            L_msgError = [],
            E_return = CLASS_e_RETURN.NOK_ERROR,
            D_cuentas = Storage()
            )

        _dbTable = dbc01.tempresa_cuentabancaria_guias
        _dbQry = (
            (_dbTable.empresa_cuentabancaria_id == empresa_cuentabancaria_id)
        )

        _D_return.D_cuentas = self._getCamposPorEjercicio(
            dbTable = _dbTable,
            dbQry = _dbQry,
            D_regresarCampos = Storage(
                cuenta_contable = 'cuentacontable',
                cuenta_contable_complementaria = 'cuentacontable_complementaria',
                )
            )

        if _D_return.D_cuentas:
            _D_return.D_cuentas.s_origen = "Banco"
            _D_return.E_return = CLASS_e_RETURN.OK
        else:
            pass

        return _D_return

    def _getCuentascontables_porCliente(self, dbRowCFDI, s_monedacontpaqi_id = None):
        '''Función que regresa las cuentas contables aplicables al cliente
        Usando el código del cliente o el tipo de cliente se obtienen las cuentas contables

            Args:
                dbRowCFDI: a usar para la búsqueda de las cuentas
                s_monedacontpaqi_id: El id de la moneda
        '''
        _D_return = Storage(
            L_msgError = [],
            E_return = CLASS_e_RETURN.NOK_ERROR,
            D_cuentas = Storage(),
            )

        if s_monedacontpaqi_id:
            _s_monedacontpaqi_id = s_monedacontpaqi_id
        else:
            _s_monedacontpaqi_id = self._dbRowCFDI.tcfdis.monedacontpaqi_id

        _dbTable = dbc01.tempresa_cliente_guiasclientes
        _dbQry = (
                (_dbTable.empresa_cliente_id == self._dbRowCFDI.tcfdis.receptorcliente_id)
                & (_dbTable.monedacontpaqi_id == _s_monedacontpaqi_id)
        )
        _D_return.D_cuentas = self._getCamposPorEjercicio(
            dbTable = _dbTable,
            dbQry = _dbQry,
            D_regresarCampos = Storage(
                cuenta_contable = 'cuenta_cliente',
                cuenta_contable_complementaria = 'cuenta_cliente_complemento',
                cuenta_contable_anticipo = 'cuenta_anticipo',
                cuenta_contable_anticipo_complementaria = 'cuenta_anticipo_complemento',
                )
            )

        if _D_return.D_cuentas:
            _D_return.D_cuentas.s_origen = "Cliente"
            _D_return.E_return = CLASS_e_RETURN.OK
        else:
            # Si no regreso cuentas contables a nivel cliente, se busca a nivel sucursal
            _dbTable = dbc01.tempresa_plaza_sucursal_guiasclientes
            _dbQry = (
                    (_dbTable.empresa_plaza_sucursal_id == dbRowCFDI.tempresa_plaza_sucursales.id)
                    & (_dbTable.monedacontpaqi_id == _s_monedacontpaqi_id)
                    & (_dbTable.tipocliente == dbRowCFDI.tempresa_clientes.tipocliente)
            )
            _D_return.D_cuentas = self._getCamposPorEjercicio(
                dbTable = _dbTable,
                dbQry = _dbQry,
                D_regresarCampos = Storage(
                    cuenta_contable = 'cuenta_cliente',
                    cuenta_contable_complementaria = 'cuenta_cliente_complemento',
                    cuenta_contable_anticipo = 'cuenta_anticipo',
                    cuenta_contable_anticipo_complementaria = 'cuenta_anticipo_complemento',
                    )
                )

            if _D_return.D_cuentas:
                _D_return.D_cuentas.s_origen = "Cliente en Sucursal"
                _D_return.E_return = CLASS_e_RETURN.OK
            else:
                # Si no regreso cuentas contables a nivel cliente, se busca a nivel plaza

                _dbTable = dbc01.tempresa_plaza_guiasclientes
                _dbQry = (
                        (_dbTable.empresa_plaza_id == dbRowCFDI.tempresa_plazas.id)
                        & (_dbTable.monedacontpaqi_id == _s_monedacontpaqi_id)
                        & (_dbTable.tipocliente == dbRowCFDI.tempresa_clientes.tipocliente)
                )
                _dbField_cuentaContable = 'cuenta_cliente'
                _dbField_cuentaContableComplemento = 'cuenta_cliente_complemento'
                _dbField_cuentaContableAnticipo = 'cuenta_anticipo'
                _dbField_cuentaContableComplemento = 'cuenta_anticipo_complemento'

                _D_return.D_cuentas = self._getCamposPorEjercicio(
                    dbTable = _dbTable,
                    dbQry = _dbQry,
                    D_regresarCampos = Storage(
                        cuenta_contable = 'cuenta_cliente',
                        cuenta_contable_complementaria = 'cuenta_cliente_complemento',
                        cuenta_contable_anticipo = 'cuenta_anticipo',
                        cuenta_contable_anticipo_complementaria = 'cuenta_anticipo_complemento',
                        )
                    )

                if _D_return.D_cuentas:
                    _D_return.D_cuentas.s_origen = "Cliente en Plaza"
                    _D_return.E_return = CLASS_e_RETURN.OK
                else:
                    pass

        return _D_return

    def _insertAsientos_ingreso(
            self,
            D_cuentas,
            D_monto = Storage(
                monto = 0,
                monedacontpaqi_id = None,
                tipocambio = 1
                ),
            b_esAbono = False,
            D_datosAdicionalesIngreso = None,
            D_datosAdicionalesComplemento = None,
            ):
        """ Función que inserta el o los asientos de ingreso y su complemento en caso de diferente moneda

            Args:
                D_cuentas: cuentas a donde se dirigirá el ingreso
                D_monto:
                    monto: Monto a usar en el asiento
                    monedacontpaqi_id: Id de la moneda
                    tipocambio: Monto del tipo de cambio
                b_esAbono: True si es abono, de lo contratio es crédito
                D_datosAdicionalesIngreso: Datos con campo y valores a actualizar justo antes de la inserción
                D_datosAdicionalesComplemento: Datos con campo y valores a actualizar justo antes de la inserción
        """
        _D_return = Storage(
            L_msgError = [],
            E_return = CLASS_e_RETURN.NOK_ERROR,
            )
        # Se procede a insertar el asiento por el ingreso
        _D_prepoliza_asiento = Storage(
            cfdi_prepoliza_id = self._n_prepoliza_id,
            descripcion = "",
            asiento_contable = str(self._n_num_concepto).zfill(3),
            cuenta_contable = D_cuentas.cuenta_contable,
            referencia = str(self._dbRowCFDI.tcfdis.tipodecomprobante).strip() + ':' + str(
                self._dbRowCFDI.tcfdis.serie
                ).strip() + str(self._dbRowCFDI.tcfdis.folio).strip(),
            empresa_diario_id = None,  # Ingreso no lleva diario por ahora
            empresa_segmento_id = None,
            empresa_tipodocumentobancario_id = D_datosAdicionalesComplemento.empresa_tipodocumentobancario_id,
            importe_cargo = D_monto.monto,
            importe_abono = 0,
            cfdi_ingreso_id = None,
            esingreso = False,
            escliente = False,
            errores = ""
            )

        if D_monto.monto != 0:

            if b_esAbono:

                if not self._b_esCancelado:
                    _D_prepoliza_asiento.importe_abono = _D_prepoliza_asiento.importe_cargo
                    _D_prepoliza_asiento.importe_cargo = 0
                else:
                    if self._n_tipoContabilizacionCancelados == TEMPRESAS.TIPOCONTABILIZACIONCANCELADOS.INVERSO:
                        _D_prepoliza_asiento.importe_abono = 0
                        _D_prepoliza_asiento.importe_cargo = _D_prepoliza_asiento.importe_cargo
                    else:
                        _D_prepoliza_asiento.importe_abono = -abs(_D_prepoliza_asiento.importe_cargo)
                        _D_prepoliza_asiento.importe_cargo = 0

            else:
                if self._b_esCancelado:
                    if self._n_tipoContabilizacionCancelados == TEMPRESAS.TIPOCONTABILIZACIONCANCELADOS.INVERSO:
                        _D_prepoliza_asiento.importe_abono = _D_prepoliza_asiento.importe_cargo
                        _D_prepoliza_asiento.importe_cargo = 0
                    else:
                        _D_prepoliza_asiento.importe_abono = 0
                        _D_prepoliza_asiento.importe_cargo = -abs(_D_prepoliza_asiento.importe_cargo)
                else:
                    pass

            if D_datosAdicionalesIngreso:
                _D_prepoliza_asiento.update(D_datosAdicionalesIngreso)
            else:
                # No se actualizan los datos del insert
                pass

            if not _D_prepoliza_asiento.errores and not D_cuentas.cuenta_contable:
                _D_prepoliza_asiento.errores = "Cuenta contable de Ingreso en " + D_cuentas.s_origen + " no localizada. "
                self._L_msgError_asiento.append(_D_prepoliza_asiento.errores)
            else:
                pass
            # Se van guardando los cargos y abonos
            self._n_cargos += _D_prepoliza_asiento.importe_cargo
            self._n_abonos += _D_prepoliza_asiento.importe_abono

            dbc01.tcfdi_prepoliza_asientoscontables.insert(**_D_prepoliza_asiento)
            self._n_num_concepto += 1

            # Si la moneda del CFDI es diferente a la de la empresa
            if D_monto.monedacontpaqi_id != self._dbRowCFDI.tempresas.monedacontpaqi_id:
                # Se multiplica el total del CFDI por el tipo de cambio
                _n_total_monedaempresa = D_monto.monto * D_monto.tipocambio

                _D_prepoliza_asiento = Storage(
                    cfdi_prepoliza_id = self._n_prepoliza_id,
                    descripcion = "",
                    asiento_contable = str(self._n_num_concepto).zfill(3),
                    # Se utiliza la cuenta complementaria
                    cuenta_contable = D_cuentas.cuenta_contable_complementaria,
                    referencia = str(self._dbRowCFDI.tcfdis.tipodecomprobante).strip() + ':' + str(
                        self._dbRowCFDI.tcfdis.serie
                        ).strip() + str(self._dbRowCFDI.tcfdis.folio).strip(),
                    empresa_diario_id = None,
                    empresa_segmento_id = None,
                    empresa_tipodocumentobancario_id = None,
                    importe_cargo = _n_total_monedaempresa - D_monto.monto,
                    importe_abono = 0,
                    errores = ""
                    # El total menos la moneda de la empresa da el importe de la moneda extranjera
                    )

                if b_esAbono:

                    if not self._b_esCancelado:
                        _D_prepoliza_asiento.importe_abono = _D_prepoliza_asiento.importe_cargo
                        _D_prepoliza_asiento.importe_cargo = 0
                    else:
                        if self._n_tipoContabilizacionCancelados == TEMPRESAS.TIPOCONTABILIZACIONCANCELADOS.INVERSO:
                            _D_prepoliza_asiento.importe_abono = 0
                            _D_prepoliza_asiento.importe_cargo = _D_prepoliza_asiento.importe_cargo
                        else:
                            _D_prepoliza_asiento.importe_abono = -abs(_D_prepoliza_asiento.importe_cargo)
                            _D_prepoliza_asiento.importe_cargo = 0

                else:
                    if self._b_esCancelado:
                        _D_prepoliza_asiento.importe_abono = 0
                        _D_prepoliza_asiento.importe_cargo = -abs(_D_prepoliza_asiento.importe_cargo)

                if D_datosAdicionalesComplemento:
                    _D_prepoliza_asiento.update(D_datosAdicionalesComplemento)
                else:
                    # No se actualizan los datos del insert
                    pass

                if not _D_prepoliza_asiento.errores and not D_cuentas.cuenta_contable_complementaria:
                    _D_prepoliza_asiento.errores = "Cuenta contable complementaria de " + D_cuentas.s_origen + " no localizada. "
                    self._L_msgError_asiento.append(_D_prepoliza_asiento.errores)
                else:
                    pass
                # Se van guardando los cargos
                self._n_cargos += _D_prepoliza_asiento.importe_cargo
                self._n_abonos += _D_prepoliza_asiento.importe_abono
                dbc01.tcfdi_prepoliza_asientoscontables.insert(**_D_prepoliza_asiento)
                self._n_num_concepto += 1
            else:
                # No se requiere cargo complemento
                pass
        else:
            # No se inserta si el total del cfdi esta en zeros
            pass
        _D_return.E_return = CLASS_e_RETURN.OK

        return _D_return

    def _insertAsientosVenta(
            self,
            D_cuentas,
            D_monto = Storage(
                monto = 0,
                descuento = 0,
                monedacontpaqi_id = None,
                tipocambio = 1
                ),
            b_esAbono = True,
            D_datosAdicionalesVenta = None,
            D_datosAdicionalesDescuento = None,
            ):
        """Función que inserta el o los asientos de Venta y Descuento

            Args:
                D_cuentas: cuentas a donde se dirigirá el ingreso
                D_monto:
                    monto: Monto a usar en el asiento
                    descuento: Monto del decuento
                    monedacontpaqi_id: Id de la moneda
                    tipocambio: Monto del tipo de cambio
                b_esAbono: True si es abono, de lo contratio es crédito
                D_datosAdicionalesVenta: Datos con campo y valores a actualizar justo antes de la inserción
                D_datosAdicionalesDescuento: Datos con campo y valores a actualizar justo antes de la inserción
        """
        _D_return = Storage(
            L_msgError = [],
            E_return = CLASS_e_RETURN.NOK_ERROR,
            n_num_inserts = 0,
            )

        # Se procede a insertar el asiento por el ingreso
        _D_prepoliza_asiento = Storage(
            cfdi_prepoliza_id = self._n_prepoliza_id,
            asiento_contable = str(self._n_num_concepto).zfill(3),
            descripcion = "",
            cuenta_contable = D_cuentas.selected_cuenta_contable,
            referencia = str(self._dbRowCFDI.tcfdis.tipodecomprobante).strip() + ':' + str(
                self._dbRowCFDI.tcfdis.serie
                ).strip() + str(self._dbRowCFDI.tcfdis.folio).strip(),
            empresa_diario_id = None,
            empresa_segmento_id = None,
            importe_cargo = 0,  # N/A para asiento de Venta
            importe_abono = 0,  # se llena después
            cfdi_ingreso_id = None,
            esconcepto_porcobrar = False,
            esconcepto_cobrado = False,
            errores = ""
            )

        # Si la moneda del CFDI es diferente a la de la empresa
        if D_monto.monedacontpaqi_id != self._dbRowCFDI.tempresas.monedacontpaqi_id:
            _n_monto = D_monto.monto * D_monto.tipocambio
            _n_descuento = D_monto.descuento * D_monto.tipocambio
        else:
            _n_monto = D_monto.monto
            _n_descuento = D_monto.descuento

        if _n_monto == _n_descuento:
            # Si el monto es igual al deacuento, se registra un abono/credito por el subtotal
            _D_prepoliza_asiento.importe_abono = _n_monto
        elif D_cuentas.selected_cuenta_contable_descto == "":
            # Si la cuenta descuento esta vacía, se totaliza y se inserta un asiento
            _D_prepoliza_asiento.importe_abono = _n_monto - _n_descuento
        else:
            # De lo contrario, un asiento tiene el total, y otro asiento el descuento
            _D_prepoliza_asiento.importe_abono = _n_monto

        if _D_prepoliza_asiento.importe_abono != 0:

            if b_esAbono:
                if self._b_esCancelado:
                    if self._n_tipoContabilizacionCancelados == TEMPRESAS.TIPOCONTABILIZACIONCANCELADOS.INVERSO:
                        _D_prepoliza_asiento.importe_cargo = _D_prepoliza_asiento.importe_abono
                        _D_prepoliza_asiento.importe_abono = 0
                    else:
                        _D_prepoliza_asiento.importe_cargo = 0
                        _D_prepoliza_asiento.importe_abono = -abs(_D_prepoliza_asiento.importe_abono)
                else:
                    pass
            else:
                if not self._b_esCancelado:
                    _D_prepoliza_asiento.importe_cargo = _D_prepoliza_asiento.importe_abono
                    _D_prepoliza_asiento.importe_abono = 0
                else:
                    if self._n_tipoContabilizacionCancelados == TEMPRESAS.TIPOCONTABILIZACIONCANCELADOS.INVERSO:
                        _D_prepoliza_asiento.importe_cargo = 0
                        _D_prepoliza_asiento.importe_abono = _D_prepoliza_asiento.importe_abono
                    else:
                        _D_prepoliza_asiento.importe_cargo = -abs(_D_prepoliza_asiento.importe_abono)
                        _D_prepoliza_asiento.importe_abono = 0

            if D_datosAdicionalesVenta:
                _D_prepoliza_asiento.update(D_datosAdicionalesVenta)
            else:
                # No se actualizan los datos del insert
                pass

            if not _D_prepoliza_asiento.errores and not D_cuentas.selected_cuenta_contable:
                _D_prepoliza_asiento.errores = "Cuenta contable de Asiento Venta en " + D_cuentas.s_origen + " no localizada. "
                self._L_msgError_asiento.append(_D_prepoliza_asiento.errores)
            else:
                pass

            # Se van guardando los cargos
            self._n_cargos += _D_prepoliza_asiento.importe_cargo
            self._n_abonos += _D_prepoliza_asiento.importe_abono
            dbc01.tcfdi_prepoliza_asientoscontables.insert(**_D_prepoliza_asiento)
            self._n_num_concepto += 1

            if (D_cuentas.selected_cuenta_contable_descto == "") and (_n_monto == _n_descuento):
                # Si no existe la cuenta de descuento y el monto es igual al deacuento, se registra un credito/abono de la misma cantidad

                # Se procede a insertar el asiento por el descuento en el ingreso
                _n_tempParaCargo = _D_prepoliza_asiento.importe_abono
                _n_tempParaAbono = _D_prepoliza_asiento.importe_cargo
                _D_prepoliza_asiento.update(
                    descripcion = "",
                    asiento_contable = str(self._n_num_concepto).zfill(3),
                    referencia = str(self._dbRowCFDI.tcfdis.tipodecomprobante).strip() + ':' + str(
                        self._dbRowCFDI.tcfdis.serie
                        ).strip() + str(self._dbRowCFDI.tcfdis.folio).strip(),
                    empresa_diario_id = None,
                    empresa_segmento_id = None,
                    importe_cargo = _n_tempParaCargo,
                    importe_abono = _n_tempParaAbono,
                    cfdi_ingreso_id = None,
                    esconcepto_porcobrar = False,
                    esconcepto_cobrado = False,
                    )

                if D_datosAdicionalesDescuento:
                    _D_prepoliza_asiento.update(D_datosAdicionalesDescuento)
                else:
                    # No se actualizan los datos del insert
                    pass
                # Se van guardando los cargos
                self._n_cargos += _D_prepoliza_asiento.importe_cargo
                self._n_abonos += _D_prepoliza_asiento.importe_abono
                dbc01.tcfdi_prepoliza_asientoscontables.insert(**_D_prepoliza_asiento)
                self._n_num_concepto += 1
            else:
                pass

        else:
            # No se inserta asiento si el importe es zero
            pass

        if (
                (_n_descuento != 0)
                & (D_cuentas.selected_cuenta_contable_descto != "")
        ):

            # Se procede a insertar el asiento por el descuento en el ingreso
            _D_prepoliza_asiento = Storage(
                cfdi_prepoliza_id = self._n_prepoliza_id,
                descripcion = "",
                asiento_contable = str(self._n_num_concepto).zfill(3),
                cuenta_contable = D_cuentas.selected_cuenta_contable_descto,
                referencia = str(self._dbRowCFDI.tcfdis.tipodecomprobante).strip() + ':' + str(
                    self._dbRowCFDI.tcfdis.serie
                    ).strip() + str(self._dbRowCFDI.tcfdis.folio).strip(),
                empresa_diario_id = None,
                empresa_segmento_id = None,
                importe_cargo = 0,  # Depende de la cuenta se llena después
                importe_abono = 0,  # Depende de la cuenta se llena después
                cfdi_ingreso_id = None,
                esconcepto_porcobrar = False,
                esconcepto_cobrado = False,
                errores = ""
                )

            if _n_monto == _n_descuento:
                # Si el importe es el mismo, el abono debe de ser por el descuento
                _D_prepoliza_asiento.importe_cargo = _n_descuento
            elif D_cuentas.selected_cuenta_contable_descto == D_cuentas.selected_cuenta_contable:
                _D_prepoliza_asiento.importe_abono = _n_descuento * (-1)
            else:
                _D_prepoliza_asiento.importe_cargo = _n_descuento

            if (
                    (_D_prepoliza_asiento.importe_abono != 0)
                    or (_D_prepoliza_asiento.importe_cargo != 0)
            ):

                if b_esAbono:
                    # Mantener como se hizo el cálculo
                    if self._b_esCancelado:
                        _n_valuetemp = -abs(_D_prepoliza_asiento.importe_cargo)
                        if self._n_tipoContabilizacionCancelados == TEMPRESAS.TIPOCONTABILIZACIONCANCELADOS.INVERSO:
                            _D_prepoliza_asiento.importe_cargo = _D_prepoliza_asiento.importe_abono
                            _D_prepoliza_asiento.importe_abono = _n_valuetemp
                        else:
                            _D_prepoliza_asiento.importe_cargo = _n_valuetemp
                            _D_prepoliza_asiento.importe_abono = -abs(_D_prepoliza_asiento.importe_abono)

                    else:
                        pass
                else:
                    if not self._b_esCancelado:
                        _n_valuetemp = _D_prepoliza_asiento.importe_cargo
                        _D_prepoliza_asiento.importe_cargo = _D_prepoliza_asiento.importe_abono
                        _D_prepoliza_asiento.importe_abono = _n_valuetemp
                    else:
                        if self._n_tipoContabilizacionCancelados == TEMPRESAS.TIPOCONTABILIZACIONCANCELADOS.INVERSO:
                            _n_valuetemp = _D_prepoliza_asiento.importe_cargo
                            _D_prepoliza_asiento.importe_cargo = _n_valuetemp
                            _D_prepoliza_asiento.importe_abono = _D_prepoliza_asiento.importe_abono
                        else:
                            -abs(_n_valuetemp = _D_prepoliza_asiento.importe_cargo)
                            _D_prepoliza_asiento.importe_cargo = -abs(_D_prepoliza_asiento.importe_abono)
                            _D_prepoliza_asiento.importe_abono = _n_valuetemp

                if D_datosAdicionalesDescuento:
                    _D_prepoliza_asiento.update(D_datosAdicionalesDescuento)
                else:
                    # No se actualizan los datos del insert
                    pass
                # Se van guardando los cargos
                self._n_cargos += _D_prepoliza_asiento.importe_cargo
                self._n_abonos += _D_prepoliza_asiento.importe_abono
                dbc01.tcfdi_prepoliza_asientoscontables.insert(**_D_prepoliza_asiento)
                self._n_num_concepto += 1
            else:
                # No se insertan los asientos en caso de estar en cero
                pass
        else:
            # No requiere asiento por descuento
            pass
        _D_return.E_return = CLASS_e_RETURN.OK

        return _D_return

    def _insertAsientosImpuesto(
            self,
            D_cuentas,
            D_monto = Storage(
                porTrasladar = 0,
                trasladado = 0,
                monedacontpaqi_id = None,
                tipocambio = 1
                ),
            D_esAbono = Storage(
                porTrasladar = True,
                trasladado = False
                ),
            D_datosAdicionalesPorTrasladar = None,
            D_datosAdicionalesTrasladado = None,
            ):
        """Función que inserta el o los asientos de Impuestos

            Args:
                D_cuentas: cuentas a donde se dirigirá el ingreso
                D_monto:
                    monto: Monto a usar en el asiento
                    descuento: Monto del decuento
                    monedacontpaqi_id: Id de la moneda
                    tipocambio: Monto del tipo de cambio
                D_esAbono: True si es abono, de lo contratio es crédito
                    porTrasladar
                    trasladado
                D_datosAdicionalesVenta: Datos con campo y valores a actualizar justo antes de la inserción
                D_datosAdicionalesDescuento: Datos con campo y valores a actualizar justo antes de la inserción
        """

        _D_return = Storage(
            L_msgError = [],
            E_return = CLASS_e_RETURN.NOK_ERROR,
            n_num_inserts = 0,
            )

        # Si la moneda del CFDI es diferente a la de la empresa
        if D_monto.monedacontpaqi_id != self._dbRowCFDI.tempresas.monedacontpaqi_id:
            _n_montoTrasladado = D_monto.trasladado * D_monto.tipocambio
            _n_montoPorTrasladar = D_monto.porTrasladar * D_monto.tipocambio
        else:
            _n_montoTrasladado = D_monto.trasladado
            _n_montoPorTrasladar = D_monto.porTrasladar

        if _n_montoTrasladado:

            _D_prepoliza_asiento = Storage(
                cfdi_prepoliza_id = self._n_prepoliza_id,
                descripcion = "",
                asiento_contable = str(self._n_num_concepto).zfill(3),
                cuenta_contable = D_cuentas.cuentacontable_trasladado,
                referencia = str(self._dbRowCFDI.tcfdis.tipodecomprobante).strip() + ':' + str(
                    self._dbRowCFDI.tcfdis.serie
                    ).strip() + str(self._dbRowCFDI.tcfdis.folio).strip(),
                # TODO que poner aqui
                empresa_diario_id = None,  # TODO que poner aqui
                empresa_segmento_id = None,
                importe_cargo = 0,  # N/A para asiento de Venta
                importe_abono = _n_montoTrasladado,
                cfdi_ingreso_id = None,
                esimpuesto_trasladado = True,
                errores = ""
                )

            if D_esAbono.trasladado:
                if self._b_esCancelado:
                    if self._n_tipoContabilizacionCancelados == TEMPRESAS.TIPOCONTABILIZACIONCANCELADOS.INVERSO:
                        _D_prepoliza_asiento.importe_cargo = _D_prepoliza_asiento.importe_abono
                        _D_prepoliza_asiento.importe_abono = 0
                    else:
                        _D_prepoliza_asiento.importe_cargo = 0
                        _D_prepoliza_asiento.importe_abono = -abs(_D_prepoliza_asiento.importe_abono)
                else:
                    pass
            else:
                if not self._b_esCancelado:
                    _D_prepoliza_asiento.importe_cargo = _D_prepoliza_asiento.importe_abono
                    _D_prepoliza_asiento.importe_abono = 0
                else:
                    if self._n_tipoContabilizacionCancelados == TEMPRESAS.TIPOCONTABILIZACIONCANCELADOS.INVERSO:
                        _D_prepoliza_asiento.importe_cargo = 0
                        _D_prepoliza_asiento.importe_abono = _D_prepoliza_asiento.importe_abono
                    else:
                        _D_prepoliza_asiento.importe_cargo = -abs(_D_prepoliza_asiento.importe_abono)
                        _D_prepoliza_asiento.importe_abono = 0

            if D_datosAdicionalesTrasladado:
                _D_prepoliza_asiento.update(D_datosAdicionalesTrasladado)
            else:
                # No se actualizan los datos del insert
                pass

            if not _D_prepoliza_asiento.errores and not D_cuentas.cuentacontable_trasladado:
                _D_prepoliza_asiento.errores = "Cuenta contable del impuesto {:0,.2f} no encontrada. " % _D_prepoliza_asiento.tasaocuota
                self._L_msgError_asiento.append(_D_prepoliza_asiento.errores)
            else:
                pass
            self._n_cargos += _D_prepoliza_asiento.importe_cargo
            self._n_abonos += _D_prepoliza_asiento.importe_abono
            dbc01.tcfdi_prepoliza_asientoscontables.insert(**_D_prepoliza_asiento)
            self._n_num_concepto += 1
        else:
            # No se inserta registro para monto trasladado
            pass

        if _n_montoPorTrasladar:

            _D_prepoliza_asiento = Storage(
                cfdi_prepoliza_id = self._n_prepoliza_id,
                descripcion = "",
                asiento_contable = str(self._n_num_concepto).zfill(3),
                cuenta_contable = D_cuentas.cuentacontable_portrasladar,
                referencia = str(self._dbRowCFDI.tcfdis.tipodecomprobante).strip() + ':' + str(
                    self._dbRowCFDI.tcfdis.serie
                    ).strip() + str(self._dbRowCFDI.tcfdis.folio).strip(),
                empresa_diario_id = None,
                empresa_segmento_id = None,
                importe_cargo = _n_montoPorTrasladar,  # N/A para asiento de Venta
                importe_abono = 0,
                cfdi_ingreso_id = None,
                esimpuesto_portrasladar = True,
                errores = ""
                )

            if D_esAbono.porTrasladar:
                if not self._b_esCancelado:
                    _D_prepoliza_asiento.importe_abono = _D_prepoliza_asiento.importe_cargo
                    _D_prepoliza_asiento.importe_cargo = 0
                else:
                    if self._n_tipoContabilizacionCancelados == TEMPRESAS.TIPOCONTABILIZACIONCANCELADOS.INVERSO:
                        _D_prepoliza_asiento.importe_abono = 0
                        _D_prepoliza_asiento.importe_cargo = _D_prepoliza_asiento.importe_cargo
                    else:
                        _D_prepoliza_asiento.importe_abono = -abs(_D_prepoliza_asiento.importe_cargo)
                        _D_prepoliza_asiento.importe_cargo = 0
            else:
                pass

            if D_datosAdicionalesPorTrasladar:
                _D_prepoliza_asiento.update(D_datosAdicionalesPorTrasladar)
            else:
                # No se actualizan los datos del insert
                pass

            if not _D_prepoliza_asiento.errores and not D_cuentas.cuentacontable_portrasladar:
                _D_prepoliza_asiento.errores = "Cuenta contable del impuesto {:0,.2f} no encontrada. " % _D_prepoliza_asiento.tasaocuota
                self._L_msgError_asiento.append(_D_prepoliza_asiento.errores)
            else:
                pass
            # Se van guardando los cargos
            self._n_cargos += _D_prepoliza_asiento.importe_cargo
            self._n_abonos += _D_prepoliza_asiento.importe_abono
            dbc01.tcfdi_prepoliza_asientoscontables.insert(**_D_prepoliza_asiento)
            self._n_num_concepto += 1
        else:
            # No se inserta registro para monto trasladado
            pass

        _D_return.E_return = CLASS_e_RETURN.OK

        return _D_return

    def _getCuentascontables_ventas(
            self,
            n_empresa_ingresoestructura_id,
            dbRowCfdiConcepto = None,
            ):
        '''Función que regresa las cuentas contables en caso de pago en efectivo
        Esta función usa la caja chica para obtener las cuentas contables

            Args:
                n_empresa_ingresoestructura_id: Id del ingreso estructura
                dbRowCfdiConcepto: Registro con el contenido por concepto del CFDI requerido en caso de egreso         
        '''
        _D_return = Storage(
            L_msgError = [],
            E_return = CLASS_e_RETURN.NOK_ERROR,
            D_cuentasVenta = Storage(),
            )

        _dbTable = dbc01.tempresa_plaza_sucursal_contabilidad_guiascontables

        _dbQry = (
                (
                        dbc01.tempresa_plaza_sucursal_contabilidades.empresa_plaza_sucursal_id == self._dbRowCFDI.tempresa_plaza_sucursales.id)
                & (_dbTable.empresa_plaza_sucursal_contabilidad_id == dbc01.tempresa_plaza_sucursal_contabilidades.id)
                & (_dbTable.empresa_ingresoestructura_id == n_empresa_ingresoestructura_id)
        )

        _D_return.D_cuentasVenta = self._getCamposPorEjercicio(
            dbTable = _dbTable,
            dbQry = _dbQry,
            D_regresarCampos = Storage(
                cuenta_contable = 'cuentacontableingreso',
                cuenta_contable_nocobrado = 'cuentacontableingreso_nocobrado',
                empresa_diario_cuentacontableingreso_id = 'empresa_diario_cuentacontableingreso_id',
                cuenta_contable_descto = 'cuentacontabledescto',
                cuenta_contable_descto_nocobrado = 'cuentacontabledescto_nocobrado',
                empresa_diario_cuentacontabledescto_id = 'empresa_diario_cuentacontabledescto_id',

                cuentacontable_egresodescuento = 'cuentacontable_egresodescuento',
                empresa_diario_cuentacontableegresodescuento_id = 'empresa_diario_cuentacontableegresodescuento_id',
                cuentacontable_egresobonificacion = 'cuentacontable_egresobonificacion',
                empresa_diario_cuentacontableegresobonificacion_id = 'empresa_diario_cuentacontableegresobonificacion_id',
                cuentacontable_egresodevolucion = 'cuentacontable_egresodevolucion',
                empresa_diario_cuentacontableegresodevolucion_id = 'empresa_diario_cuentacontableegresodevolucion_id'
                ),
            dbFieldEjercicio = dbc01.tempresa_plaza_sucursal_contabilidades.ejercicio
            )

        if _D_return.D_cuentasVenta:
            _D_return.D_cuentasVenta.s_origen = "Sucursal"
        else:
            # Se busca la información de contabilización por empresa

            _dbTable = dbc01.tempresa_ingresoestructura_guiascontables
            _dbQry = (
                (
                        dbc01.tempresa_ingresoestructura_guiascontables.empresa_ingresoestructura_id == n_empresa_ingresoestructura_id)
            )
            _D_return.D_cuentasVenta = self._getCamposPorEjercicio(
                dbTable = _dbTable,
                dbQry = _dbQry,
                D_regresarCampos = Storage(
                    cuenta_contable = 'cuentacontableingreso',
                    cuenta_contable_nocobrado = 'cuentacontableingreso_nocobrado',
                    empresa_diario_cuentacontableingreso_id = 'empresa_diario_cuentacontableingreso_id',
                    cuenta_contable_descto = 'cuentacontabledescto',
                    cuenta_contable_descto_nocobrado = 'cuentacontabledescto_nocobrado',
                    empresa_diario_cuentacontabledescto_id = 'empresa_diario_cuentacontabledescto_id',

                    cuentacontable_egresodescuento = 'cuentacontable_egresodescuento',
                    empresa_diario_cuentacontableegresodescuento_id = 'empresa_diario_cuentacontableegresodescuento_id',
                    cuentacontable_egresobonificacion = 'cuentacontable_egresobonificacion',
                    empresa_diario_cuentacontableegresobonificacion_id = 'empresa_diario_cuentacontableegresobonificacion_id',
                    cuentacontable_egresodevolucion = 'cuentacontable_egresodevolucion',
                    empresa_diario_cuentacontableegresodevolucion_id = 'empresa_diario_cuentacontableegresodevolucion_id'
                    ),
                )

            if _D_return.D_cuentasVenta:
                _D_return.D_cuentasVenta.s_origen = "Empresa"
            else:
                pass

        if _D_return.D_cuentasVenta:

            if self._dbRowCFDI.tcfdis.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO:
                _D_return.D_cuentasVenta.selected_diario_especial = _D_return.D_cuentasVenta.empresa_diario_cuentacontableingreso_id
                _D_return.D_cuentasVenta.selected_diario_especial_descto = _D_return.D_cuentasVenta.empresa_diario_cuentacontabledescto_id

                if self._dbRowCFDI.tcfdis.metodopago_id == TMETODOSPAGO.UNAEXHIBICION:
                    _D_return.D_cuentasVenta.selected_cuenta_contable = _D_return.D_cuentasVenta.cuenta_contable
                    _D_return.D_cuentasVenta.selected_cuenta_contable_descto = _D_return.D_cuentasVenta.cuenta_contable_descto

                    _D_return.E_return = CLASS_e_RETURN.OK

                elif self._dbRowCFDI.tcfdis.metodopago_id == TMETODOSPAGO.PARCIALIDADES:

                    if _D_return.D_cuentasVenta.cuenta_contable_nocobrado:
                        _D_return.D_cuentasVenta.selected_cuenta_contable = _D_return.D_cuentasVenta.cuenta_contable_nocobrado
                    else:
                        _D_return.D_cuentasVenta.selected_cuenta_contable = _D_return.D_cuentasVenta.cuenta_contable

                    if _D_return.D_cuentasVenta.cuenta_contable_descto_nocobrado:
                        _D_return.D_cuentasVenta.selected_cuenta_contable_descto = _D_return.D_cuentasVenta.cuenta_contable_descto_nocobrado
                    else:
                        _D_return.D_cuentasVenta.selected_cuenta_contable_descto = _D_return.D_cuentasVenta.cuenta_contable_descto

                    _D_return.E_return = CLASS_e_RETURN.OK

                else:
                    self._L_msgError.append("No se encontró el método de pago")

            elif self._dbRowCFDI.tcfdis.tipocomprobante_id == TTIPOSCOMPROBANTES.EGRESO:

                if not dbRowCfdiConcepto:
                    self._L_msgError_asiento_venta.append("Concepto de egreso no identificado")
                else:

                    # Tipo de relación Nota de crédito
                    if self._dbRowCFDI.tcfdis.tiporelacion_id == TTIPOSRELACIONES.NOTACREDITO:
                        # El concepto para bonificar solo tiene la cuenta de bonificación activa.
                        if dbRowCfdiConcepto.tempresa_prodserv.tipo in (
                                TEMPRESA_PRODSERV.E_TIPO.PRODUCTO, TEMPRESA_PRODSERV.E_TIPO.SERVICIO):
                            _D_return.D_cuentasVenta.selected_cuenta_contable = _D_return.D_cuentasVenta.cuentacontable_egresodescuento
                            _D_return.D_cuentasVenta.selected_diario_especial = _D_return.D_cuentasVenta.empresa_diario_cuentacontableegresodescuento_id
                            _D_return.D_cuentasVenta.s_origen = "descuento"
                            _D_return.E_return = CLASS_e_RETURN.OK
                        elif dbRowCfdiConcepto.tempresa_prodserv.tipo == TEMPRESA_PRODSERV.E_TIPO.BONIFICACION:
                            _D_return.D_cuentasVenta.selected_cuenta_contable = _D_return.D_cuentasVenta.cuentacontable_egresobonificacion
                            _D_return.D_cuentasVenta.selected_diario_especial = _D_return.D_cuentasVenta.empresa_diario_cuentacontableegresobonificacion_id
                            _D_return.D_cuentasVenta.s_origen = "bonifiación"
                            _D_return.E_return = CLASS_e_RETURN.OK
                        else:
                            # No se econtró el tipo del producto
                            pass
                    # Tipo de relación: Devolución de mercancía
                    elif self._dbRowCFDI.tcfdis.tiporelacion_id == TTIPOSRELACIONES.DEVOLUCION:
                        if dbRowCfdiConcepto.tempresa_prodserv.tipo == TEMPRESA_PRODSERV.E_TIPO.BONIFICACION:
                            _D_return.D_cuentasVenta.selected_cuenta_contable = _D_return.D_cuentasVenta.cuentacontable_egresobonificacion
                            _D_return.D_cuentasVenta.selected_diario_especial = _D_return.D_cuentasVenta.empresa_diario_cuentacontableegresobonificacion_id
                            _D_return.D_cuentasVenta.s_origen = "bonifiación"
                            _D_return.E_return = CLASS_e_RETURN.OK
                        else:
                            _D_return.D_cuentasVenta.selected_cuenta_contable = _D_return.D_cuentasVenta.cuentacontable_egresodevolucion
                            _D_return.D_cuentasVenta.selected_diario_especial = _D_return.D_cuentasVenta.empresa_diario_cuentacontableegresodevolucion_id
                            _D_return.D_cuentasVenta.s_origen = "devolución"
                            _D_return.E_return = CLASS_e_RETURN.OK

                    else:
                        # TODO: Hay CFDI de Egreso sin tipo de relación, preguntarle a Juan que hacer en estos casos.
                        self._L_msgError_asiento_venta.append("No se encontró el tipo de relación")

            else:
                # El tipo de comprobante no se analiza por cuentas contables en sucursal
                self._L_msgError.append("No se encontró el tipo de comprobante")
        else:
            pass

        return _D_return

    def _getCuentascontablesIVA(
            self,
            D_impuestotrasladado
            ):
        '''Función que regresa las cuentas contables para los impuestos.
                Args:
                    D_impuestotrasladado:
                        tasaocuota: Tasa o cuota del impuesto.
                        importe_acumulado: Acumulado del impuesto.
                        impuesto_id: ID del impuesto.
        '''
        _D_return = Storage(
            L_msgError = [],
            E_return = CLASS_e_RETURN.NOK_ERROR,
            D_cuentasIVA = Storage()
            )

        # TODO: Falta platicar la lógica que de verdad llevará esta parte, esto es temporal.
        # Si su tasa o cuota es 0 significa que no necesita una cuenta contable de Impuesto
        if D_impuestotrasladado.tasaocuota == 0:
            _D_return.D_cuentasIVA.s_origen = "EXENTO"
            _D_return.D_cuentasIVA.cuentacontable_trasladado = 'exento'
            _D_return.D_cuentasIVA.cuentacontable_portrasladar = 'exento'
            _D_return.E_return = CLASS_e_RETURN.OK
        else:
            _dbTable = dbc01.tempresa_iva
            _dbQry = (
                    (_dbTable.empresa_id == self._dbRowCFDI.tempresas.id)
                    & (_dbTable.tasa == (D_impuestotrasladado.tasaocuota * 100))
            )
            _D_return.D_cuentasIVA = self._getCamposPorEjercicio(
                dbTable = _dbTable,
                dbQry = _dbQry,
                D_regresarCampos = Storage(
                    cuentacontable_trasladado = 'cuentacontable_ivatrasladado',
                    cuentacontable_portrasladar = 'cuentacontable_ivaportrasladar',
                    )
                )

            if _D_return.D_cuentasIVA:

                _D_return.D_cuentasIVA.s_origen = "IVA"
                _D_return.E_return = CLASS_e_RETURN.OK
            else:
                pass

        return _D_return
