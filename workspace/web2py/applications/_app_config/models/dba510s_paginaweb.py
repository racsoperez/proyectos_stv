# -*- coding: utf-8 -*-

class TEMPRESA_PAGINAWEB():
    
    class E_TIPO():
        
        NO_DEFINIDO = 0
        PRINCIPAL = 1
        REFERENCIA = 2
        OCULTA = 3
        
        @classmethod
        def get_dict(cls):
            return {
                cls.NO_DEFINIDO: ('No definido'),
                cls.PRINCIPAL: ('Principal'),
                cls.REFERENCIA: ('Referencia'),
                cls.OCULTA: ('Oculta')
                }
            
    pass
dbc01.define_table(
    'tempresa_paginaweb',
    Field('empresa_id', dbc01.tempresas, default=None,
        required=True,
        ondelete='CASCADE', notnull=True, unique=False,
        label='Empresa', comment=None,
        writable=False, readable=False,
        represent=None),
    Field('tipo', 'integer', default = 0,
      required = True, requires = IS_IN_SET(TEMPRESA_PAGINAWEB.E_TIPO.get_dict(), zero = 0, error_message = 'Selecciona' ),
      notnull = False, unique = False,
      widget = stv_widget_combobox, 
      label = ('Tipo de página'), comment = None,
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_PAGINAWEB.E_TIPO.get_dict())),
    Field('nombrecorto', 'string', length=20, default=None,
      required=True,
      notnull=False, unique=False,
      widget=stv_widget_input, label=T('Nombre corto'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('descripcion', 'string', length=200, default=None,
        required=False,
        notnull=False, unique=False,
        widget=stv_widget_input, label='Descripción', comment='Descripción de la página',
        writable=True, readable=True,
        represent=stv_represent_string),
    Field('logonombrearchivo', 'string', length=100, default=None,
      required=False,
      notnull=False, unique=False,
      widget = stv_widget_input, label=T('Logo de la página'), comment=None,
      writable=False, readable=False,
      represent=stv_represent_string),    
    Field('logo', type = 'upload',
      required = False, requires = IS_LENGTH(minsize=0, maxsize=2000, error_message=T('El archivo es demasiado grande.')),
      notnull = False,
      uploadfield = True, widget=lambda f, d, u=None: stv_widget_inputFile(f, d, URL(a='app_empresas', c='120empresas', f='empresa_paginaweb', vars=D_stvFwkCfg.D_cfg_cuenta_para_links), dbc01.tempresa_paginaweb.logonombrearchivo), 
      label = T('logo'), comment = 'El tamaño del archivo no debe ser mayor a 2MB',
      writable = True, readable = True, authorize = lambda record: auth.is_logged_in(),
      autodelete = True, represent = lambda v,r: stv_represent_image(v,r,URL(a='app_empresas', c='120empresas', f='empresa_paginaweb', vars=D_stvFwkCfg.D_cfg_cuenta_para_links)), 
      uploadfolder = os.path.join(request.folder, 'uploads', 'empresa_paginaweb'),
      uploadseparate = False, uploadfs = None ),
    tSignature_dbc01,
    format='%(nombrecorto)s',
    singular='Página Web',
    plural='Paginas Web',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

class TEMPRESA_PAGINAWEB_NOTICIAS():
    pass
dbc01.define_table(
    'tempresa_paginaweb_noticias',
    Field('empresa_paginaweb_id', dbc01.tempresa_paginaweb, default=None,
        required=True,
        ondelete='CASCADE', notnull=True, unique=False,
        label='Página web', comment=None,
        writable=False, readable=False,
        represent=None),
    Field('titulo', 'string', length=50, default=None,
      required=True,
      notnull=False, unique=False,
      widget=stv_widget_input, label=('Titulo'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('contenido', 'text', default=0,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_text, label='Contenido', comment=None,
      writable=True, readable=True,
      represent=stv_represent_text),
    Field('fechainicio', 'date', default=request.browsernow,
      required=False, requires = IS_DATE(format=(STV_FWK_APP.FORMAT.s_DATE)),
      notnull=False,
      widget=stv_widget_inputDate, label='Fecha inicial', comment=None,
      writable=True, readable=True,
      represent=stv_represent_date),
    Field('fechafin', 'date', default=request.browsernow,
      required=False, requires = IS_DATE(format=(STV_FWK_APP.FORMAT.s_DATE)),
      notnull=False,
      widget=stv_widget_inputDate, label='Fecha final', comment=None,
      writable=True, readable=True,
      represent=stv_represent_date),
    Field('link', 'string', length=100, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label=('Link'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('linkvideo', 'string', length=100, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label=('Link del vídeo'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('imagennombrearchivo', 'string', length=100, default=None,
      required=False,
      notnull=False, unique=False,
      widget = stv_widget_input, label=T('Nombre de la imagen'), comment=None,
      writable=False, readable=False,
      represent=stv_represent_string),    
    Field('imagen', type = 'upload',
      required = False, requires = IS_LENGTH(minsize=0, maxsize=2000, error_message=T('El archivo es demasiado grande.')),
      notnull = False,
      uploadfield = True, widget=lambda f, d, u=None: stv_widget_inputFile(f, d, URL(a='app_empresas', c='120empresas', f='empresa_paginaweb_noticias', vars=D_stvFwkCfg.D_cfg_cuenta_para_links), dbc01.tempresa_paginaweb_noticias.imagennombrearchivo), 
      label = T('Imagen'), comment = 'El tamaño del archivo no debe ser mayor a 2MB',
      writable = True, readable = True, authorize = lambda record: auth.is_logged_in(),
      autodelete = True, represent = lambda v,r: stv_represent_image(v,r,URL(a='app_empresas', c='120empresas', f='empresa_paginaweb_noticias', vars=D_stvFwkCfg.D_cfg_cuenta_para_links)), 
      uploadfolder = os.path.join(request.folder, 'uploads', 'empresa_paginaweb_noticias'),
      uploadseparate = False, uploadfs = None ),
    Field('linkimagen', 'string', length=100, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label=('Link de la imagen'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    tSignature_dbc01,
    format='%(titulo)s',
    singular='Noticia',
    plural='Noticias',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

class TEMPRESA_PAGINAWEB_TIPS():
    
    class E_TIPO():
        
        NO_DEFINIDO = 0
        TIP = 1
        TRUCO = 2
        SUGERENCIA = 3
        PREVENCION = 4
        MANTENIMIENTO = 5
        
        @classmethod
        def get_dict(cls):
            return {
                cls.NO_DEFINIDO: ('No definido'),
                cls.TIP: ('Tip'),
                cls.TRUCO: ('Truco'),
                cls.SUGERENCIA: ('Sugerencia'),
                cls.PREVENCION: ('Prevencion'),
                cls.MANTENIMIENTO: ('Mantenimiento')
                }
            
    E_TIPO = E_TIPO()
    
    pass
dbc01.define_table(
    'tempresa_paginaweb_tips',
    Field('empresa_paginaweb_id', dbc01.tempresa_paginaweb, default=None,
        required=True,
        ondelete='CASCADE', notnull=True, unique=False,
        label='Página web', comment=None,
        writable=False, readable=False,
        represent=None),
    Field('tipo', 'integer', default = 0,
      required = True, requires = IS_IN_SET(TEMPRESA_PAGINAWEB_TIPS.E_TIPO.get_dict(), zero = 0, error_message = 'Selecciona' ),
      notnull = False, unique = False,
      widget = stv_widget_combobox, 
      label = ('Tipo de consejo'), comment = None,
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_PAGINAWEB_TIPS.E_TIPO.get_dict())),
    Field('titulo', 'string', length=50, default=None,
      required=True,
      notnull=False, unique=False,
      widget=stv_widget_input, label=('Titulo'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('contenido', 'text', default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_text, label='Contenido', comment=None,
      writable=True, readable=True,
      represent=stv_represent_text),
    Field('linkvideo', 'string', length=100, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label=('Link del vídeo'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    tSignature_dbc01,
    format='%(titulo)s',
    singular='Tip ',
    plural='Tips',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_PAGINAWEB_PROMOCIONES():
    class E_TIPO():
        
        NO_DEFINIDO = 0
        PRODUCTO = 1
        MINIMOCOMPRA = 2
        PAQUETE = 3
        
        @classmethod
        def get_dict(cls):
            return {
                cls.NO_DEFINIDO: ('No definido'),
                cls.PRODUCTO: ('Producto'),
                cls.MINIMOCOMPRA: ('Minimo de compra'),
                cls.PAQUETE: ('Paquete'),
                }
            
    E_TIPO = E_TIPO()
    pass
dbc01.define_table(
    'tempresa_paginaweb_promociones',
    Field('empresa_paginaweb_id', dbc01.tempresa_paginaweb, default=None,
        required=True,
        ondelete='CASCADE', notnull=True, unique=False,
        label='Página web', comment=None,
        writable=False, readable=False,
        represent=None),
    Field('titulo', 'string', length=50, default=None,
      required=True,
      notnull=False, unique=False,
      widget=stv_widget_input, label=('Titulo'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('fechainicio', 'date', default=request.browsernow,
      required=False, requires = IS_DATE(format=(STV_FWK_APP.FORMAT.s_DATE)),
      notnull=False,
      widget=stv_widget_inputDate, label='Fecha inicial', comment='Fecha inicial de la promoción',
      writable=True, readable=True,
      represent=stv_represent_date),
    Field('fechafin', 'date', default=request.browsernow,
      required=False, requires = IS_DATE(format=(STV_FWK_APP.FORMAT.s_DATE)),
      notnull=False,
      widget=stv_widget_inputDate, label='Fecha final', comment='Fecha final de la promoción',
      writable=True, readable=True,
      represent=stv_represent_date),
    Field('descripcion', 'string', length=100, default=None,
      required=True,
      notnull=False, unique=False,
      widget=stv_widget_input, label=T('Descripción'), comment='Descripción de la promoción',
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('imagennombrearchivo', 'string', length=100, default=None,
      required=False,
      notnull=False, unique=False,
      widget = stv_widget_input, label=T('Nombre de la imagen'), comment=None,
      writable=False, readable=False,
      represent=stv_represent_string),    
    Field('imagen', type = 'upload',
      required = False, requires = IS_LENGTH(minsize=0, maxsize=2000, error_message=T('El archivo es demasiado grande.')),
      notnull = False,
      uploadfield = True, widget=lambda f, d, u=None: stv_widget_inputFile(f, d, URL(a='app_empresas', c='120empresas', f='empresa_paginaweb_promociones', vars=D_stvFwkCfg.D_cfg_cuenta_para_links), dbc01.tempresa_paginaweb_promociones.imagennombrearchivo), 
      label = T('Imagen promoción'), comment = 'El tamaño del archivo no debe ser mayor a 2MB',
      writable = True, readable = True, authorize = lambda record: auth.is_logged_in(),
      autodelete = True, represent = lambda v,r: stv_represent_image(v,r,URL(a='app_empresas', c='120empresas', f='empresa_paginaweb_promociones', vars=D_stvFwkCfg.D_cfg_cuenta_para_links)), 
      uploadfolder = os.path.join(request.folder, 'uploads', 'empresa_paginaweb_promociones'),
      uploadseparate = False, uploadfs = None ),
    Field('reglas', 'text', default=0,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_text, label='Reglas', comment='Reglas o restricciones de la promoción.',
      writable=True, readable=True,
      represent=stv_represent_text),
    Field('tipo', 'integer', default = 0,
      required = True, requires = IS_IN_SET(TEMPRESA_PAGINAWEB_PROMOCIONES.E_TIPO.get_dict(), zero = 0, error_message = 'Selecciona' ),
      notnull = False, unique = False,
      widget = stv_widget_combobox, 
      label = ('Tipo de promoción'), comment = None,
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_PAGINAWEB_PROMOCIONES.E_TIPO.get_dict())),
    Field('minimocompra', 'decimal(14,4)', default=0,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_inputMoney, label='Mínimo de compra', comment=None,
      writable=True, readable=True,
      represent=stv_represent_money_ifnotzero),
    tSignature_dbc01,
    format='%(titulo)s',
    singular='Promoción',
    plural='Promociones',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

class TEMPRESA_PAGINAWEB_PROMOCION_PRODUCTOS():
    pass
dbc01.define_table(
    'tempresa_paginaweb_promocion_productos',
    Field('tempresa_paginaweb_promocion_id', dbc01.tempresa_paginaweb_promociones, default=None,
        required=True,
        ondelete='CASCADE', notnull=True, unique=False,
        label='Página web promocion', comment=None,
        writable=False, readable=False,
        represent=None),
    Field('producto_id', 'integer', default=None,
        required=False, requires = IS_NULL_OR(IS_IN_DB(dbc01,'tempresa_prodserv.id','%(descripcion)s [%(codigo)s]')),
        ondelete='NO ACTION', notnull=False, unique=False,
        widget = lambda f, v: stv_widget_db_search(f, v, 
            s_display = '%(descripcion)s [%(codigo)s]', 
            s_url = URL( a = 'app_empresas', c = '120empresas', f = 'empresa_prodserv_buscar'),
            D_additionalAttributes = {'reference':dbc01.tempresa_prodserv.id}
            ), 
        label=('Producto'), comment=None,
        writable=True, readable=True,
        represent=lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_paginaweb_promocion_productos.producto_id)),
    Field('imagennombrearchivo', 'string', length=100, default=None,
      required=False,
      notnull=False, unique=False,
      widget = stv_widget_input, label=T('Nombre de la imagen'), comment=None,
      writable=False, readable=False,
      represent=stv_represent_string),    
    Field('imagen', type = 'upload',
      required = False, requires = IS_LENGTH(minsize=0, maxsize=2000, error_message=T('El archivo es demasiado grande.')),
      notnull = False,
      uploadfield = True, widget=lambda f, d, u=None: stv_widget_inputFile(f, d, URL(a='app_empresas', c='120empresas', f='empresa_paginaweb_promocion_productos', vars=D_stvFwkCfg.D_cfg_cuenta_para_links), dbc01.tempresa_paginaweb_promocion_productos.imagennombrearchivo), 
      label = T('Imagen promoción'), comment = 'El tamaño del archivo no debe ser mayor a 2MB',
      writable = True, readable = True, authorize = lambda record: auth.is_logged_in(),
      autodelete = True, represent = lambda v,r: stv_represent_image(v,r,URL(a='app_empresas', c='120empresas', f='empresa_paginaweb_promocion_productos', vars=D_stvFwkCfg.D_cfg_cuenta_para_links)), 
      uploadfolder = os.path.join(request.folder, 'uploads', 'empresa_paginaweb_promocion_productos'),
      uploadseparate = False, uploadfs = None ),
    tSignature_dbc01,
    format='%(id)s',
    singular='Producto',
    plural='Productos',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_PAGINAWEB_PROMOCION_CATEGORIAS():
    pass
dbc01.define_table(
    'tempresa_paginaweb_promocion_categorias',
    Field('tempresa_paginaweb_promocion_id', dbc01.tempresa_paginaweb_promociones, default=None,
        required=True,
        ondelete='CASCADE', notnull=True, unique=False,
        label='Página web promocion', comment=None,
        writable=False, readable=False,
        represent=None),
    Field('categoria_id', 'integer', default=None,
        required=False, requires= IS_NULL_OR(IS_IN_DB(dbc01,'tempresa_prodcategorias.id','%(nombrecorto)s')),
        ondelete='NO ACTION', notnull=False, unique=False,
        widget=lambda f, v: stv_widget_db_chosen_tree(f, v, 1, D_additionalAttributes={
            'dbTable_tree': dbc01.tempresa_prodcategorias,
            'dbField_order': dbc01.tempresa_prodcategorias.orden,
            'dbField_tree_groupby': dbc01.tempresa_prodcategorias.padre_id,
            'class_dbTree': DB_Tree,
            'b_onlyLastNodes' : True,
            'b_includeId' : True
            }), 
        label=('Categoria'), comment=None,
        writable=True, readable=True,
        represent=lambda v, r: stv_represent_referencefield_tree(v, r, dbc01.tempresa_prodcategorias.padre_id, dbc01.tempresa_paginaweb_promocion_categorias.categoria_id)),
    Field('imagennombrearchivo', 'string', length=100, default=None,
      required=False,
      notnull=False, unique=False,
      widget = stv_widget_input, label=T('Nombre de la imagen'), comment=None,
      writable=False, readable=False,
      represent=stv_represent_string),    
    Field('imagen', type = 'upload',
      required = False, requires = IS_LENGTH(minsize=0, maxsize=2000, error_message=T('El archivo es demasiado grande.')),
      notnull = False,
      uploadfield = True, widget=lambda f, d, u=None: stv_widget_inputFile(f, d, URL(a='app_empresas', c='120empresas', f='empresa_paginaweb_promocion_categorias', vars=D_stvFwkCfg.D_cfg_cuenta_para_links), dbc01.tempresa_paginaweb_promocion_categorias.imagennombrearchivo), 
      label = T('Imagen promoción'), comment = 'El tamaño del archivo no debe ser mayor a 2MB',
      writable = True, readable = True, authorize = lambda record: auth.is_logged_in(),
      autodelete = True, represent = lambda v,r: stv_represent_image(v,r,URL(a='app_empresas', c='120empresas', f='empresa_paginaweb_promocion_categorias', vars=D_stvFwkCfg.D_cfg_cuenta_para_links)), 
      uploadfolder = os.path.join(request.folder, 'uploads', 'empresa_paginaweb_promocion_categorias'),
      uploadseparate = False, uploadfs = None ),
    tSignature_dbc01,
    format='%(id)s',
    singular='Categoria',
    plural='Categorias',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

class TEMPRESA_PAGINAWEB_PROMOCION_MARCAS():
    pass
dbc01.define_table(
    'tempresa_paginaweb_promocion_marcas',
    Field('tempresa_paginaweb_promocion_id', dbc01.tempresa_paginaweb_promociones, default=None,
        required=True,
        ondelete='CASCADE', notnull=True, unique=False,
        label='Página web promocion', comment=None,
        writable=False, readable=False,
        represent=None),
    Field('marca_id', dbc01.tmarcas, default=None,
        required=True,
        ondelete='NO ACTION', notnull=False, unique=False,
        widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label=('Marca'), comment=None,
        writable=True, readable=True,
        represent=lambda v, r: stv_represent_referencefield(v, r)),
    Field('imagennombrearchivo', 'string', length=100, default=None,
      required=False,
      notnull=False, unique=False,
      widget = stv_widget_input, label=T('Nombre de la imagen'), comment=None,
      writable=False, readable=False,
      represent=stv_represent_string),    
    Field('imagen', type = 'upload',
      required = False, requires = IS_LENGTH(minsize=0, maxsize=2000, error_message=T('El archivo es demasiado grande.')),
      notnull = False,
      uploadfield = True, widget=lambda f, d, u=None: stv_widget_inputFile(f, d, URL(a='app_empresas', c='120empresas', f='empresa_paginaweb_promocion_categorias', vars=D_stvFwkCfg.D_cfg_cuenta_para_links), dbc01.tempresa_paginaweb_promocion_categorias.imagennombrearchivo), 
      label = T('Imagen promoción'), comment = 'El tamaño del archivo no debe ser mayor a 2MB',
      writable = True, readable = True, authorize = lambda record: auth.is_logged_in(),
      autodelete = True, represent = lambda v,r: stv_represent_image(v,r,URL(a='app_empresas', c='120empresas', f='empresa_paginaweb_promocion_categorias', vars=D_stvFwkCfg.D_cfg_cuenta_para_links)), 
      uploadfolder = os.path.join(request.folder, 'uploads', 'empresa_paginaweb_promocion_categorias'),
      uploadseparate = False, uploadfs = None ),
    tSignature_dbc01,
    format='%(id)s',
    singular='Marca',
    plural='Marcas',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )