# -*- coding: utf-8 -*-

""" Este archivo contiene las definiciónes de los modelos genéricos por cuenta.

Definiciones de modelos que se comparten entre las aplicaciones de una cuenta ya que se ubican en la base de datos específica por cuenta.
En general es información usada por el framework.
"""

""" Definición de los formularios disponibles en el sistema """
class TNOTIFICACIONES():
    class ESTATUS():
        ''' Se definen las opciónes del campo '''
        NO_DEFINIDO = 0
        NUEVO = 1
        EN_PROCESO = 2
        REPROCESAR = 3
        RESUELTO = 4
        
        @classmethod
        def get_dict(self):
            return {
                    self.NUEVO: ('Nuevo'),
                    self.EN_PROCESO: ('En Proceso'),
                    self.REPROCESAR: ('Reprocesar'),
                    self.RESUELTO: ('Resuelto'),
                    }
    ESTATUS = ESTATUS()
    
    class PROCESO_ID():
        ''' Se definen las opciónes del campo '''
        NO_DEFINIDO = 0
        IMPORTAR_CFDI = 1
        GENERAR_POLIZA = 2
        
        @classmethod
        def get_dict(self):
            return {
                    self.NO_DEFINIDO: ('No definido'),
                    self.IMPORTAR_CFDI: ('Importar CFDI'),
                    self.GENERAR_POLIZA: ('Generar Poliza'),
                    }
    PROCESO_ID = PROCESO_ID()    
    pass
dbc02.define_table(
    'tnotificaciones',
    Field('empresa_id', 'integer', default=None,
      required=True,
      ondelete='NO ACTION', notnull=True, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label=('Empresa'), comment='Empresa que tiene registrado el cfdi ya sea como emisor o receptor',
      writable=False, readable=True,
      represent=lambda v, r: stv_represent_referencefield(v, r)),    
    Field('titulo', 'string', length = 100, default = None,
      required = True,
      notnull = True, unique = False,
      widget = stv_widget_input, label = T('Título'), comment = 'Título de la notificación',
      writable = True, readable = True,
      represent = stv_represent_string),
    Field('descripcion', 'string', length = 200, default = None,
      required = False,
      notnull = False, unique = False,
      widget = stv_widget_input, label = T('Description'), comment = 'Descripción de la notificación',
      writable = True, readable = True,
      represent = stv_represent_string),
    Field('aplicacion_w2p', 'string', length = 100, default = None,
      required = True, requires=IS_IN_SET(STV_FWK2_VALIDACIONES.APLICACION_W2P.OPCIONES(), zero=0, error_message=('Selecciona')),
      notnull = True, unique = False,
      widget = stv_widget_combobox, label = T('Web2Py Application'), comment = 'Nombre de la aplicación web2py relacionada con esta notificación',
      writable = True, readable = True,
      represent = stv_represent_string),
    Field('controlador_w2p', 'string', length = 100, default = None,
      required = True,
      notnull = True, unique = False,
      widget=lambda field, value: stv_widget_db_chosen_linked(field, value, 1, 'aplicacion_w2p', URL(c='accesos', f='application_controladores_ver.json')),
      #widget = stv_widget_combobox, 
      label = T('Web2Py Controller'), comment = 'Nombre del controlador web2py relacionada con esta notificación',
      writable = True, readable = True,
      represent = stv_represent_string),
    Field('funcion_w2p', 'string', length = 100, default = None,
      required = True,
      notnull = True, unique = False,
      widget = stv_widget_input, label = T('Web2Py Function'), comment = 'Nombre de la función web2py relacionada con esta notificación',
      writable = True, readable = True,
      represent = stv_represent_string),
    Field('argumentos', 'string', length = 200, default = None,
      required = False,
      notnull = False, unique = False,
      widget = stv_widget_input, label = T('Argumentos'), comment = 'Argumentos separados por / usados para ligar la notificación a la apertura de una forma',
      writable = True, readable = True,
      represent = stv_represent_string),
    Field('referencia', 'text', default=None,
      required = False,
      notnull = False,
      label=('Referencia'), comment = "Texto de referencia",
      writable = False, readable = D_stvFwkCfg.b_isSupportUser,
      represent = stv_represent_text),
    Field('referencia2', 'text', default=None,
      required = False,
      notnull = False,
      label=('Referencia 2'), comment = "Texto para referencia adicional",
      writable = False, readable = D_stvFwkCfg.b_isSupportUser,
      represent = stv_represent_text),
    Field('ultimosql', 'text', default=None,
      required = False,
      notnull = False,
      label=('Último SQL'), comment = "Último SQL que generó la notificación",
      writable = False, readable = D_stvFwkCfg.b_isSupportUser,
      represent = stv_represent_text),
    Field('estatus', 'integer', default = 1,
      required = True, requires = IS_IN_SET(TNOTIFICACIONES.ESTATUS.get_dict(), zero = 0, error_message = 'Selecciona' ),
      notnull = False,
      widget = stv_widget_combobox, 
      label = ('Estatus'), comment = None,
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, TNOTIFICACIONES.ESTATUS.get_dict())),
    Field('proceso_id', 'integer', default = 0,
      required = True, requires = IS_IN_SET(TNOTIFICACIONES.PROCESO_ID.get_dict(), zero = 0, error_message = 'Selecciona' ),
      notnull = False,
      widget = stv_widget_combobox, 
      label = ('Proceso'), comment = None,
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, TNOTIFICACIONES.PROCESO_ID.get_dict())),

    Field('reprocesar_aplicacion_w2p', 'string', length = 100, default = None,
      required = True, requires=IS_IN_SET(STV_FWK2_VALIDACIONES.APLICACION_W2P.OPCIONES(), zero=0, error_message=('Selecciona')),
      notnull = True, unique = False,
      widget = stv_widget_combobox, label = T('Web2Py Application'), comment = 'Nombre de la aplicación web2py relacionada con el reproceso de esta notificación',
      writable = True, readable = True,
      represent = stv_represent_string),
    Field('reprocesar_controlador_w2p', 'string', length = 100, default = None,
      required = True,
      notnull = True, unique = False,
      widget=lambda field, value: stv_widget_db_chosen_linked(field, value, 1, 'aplicacion_w2p', URL(c='accesos', f='application_controladores_ver.json')),
      #widget = stv_widget_combobox, 
      label = T('Web2Py Controller'), comment = 'Nombre del controlador web2py relacionada con el reproceso de esta notificación',
      writable = True, readable = True,
      represent = stv_represent_string),
    Field('reprocesar_funcion_w2p', 'string', length = 100, default = None,
      required = True,
      notnull = True, unique = False,
      widget = stv_widget_input, label = T('Web2Py Function'), comment = 'Nombre de la función web2py relacionada con el reproceso de esta notificación',
      writable = True, readable = True,
      represent = stv_represent_string),
    Field('reprocesar_argumentos', 'string', length = 200, default = None,
      required = False,
      notnull = False, unique = False,
      widget = stv_widget_input, label = T('Argumentos'), comment = 'Argumentos separados por / usados para ligar el reproceso de la notificación a la apertura de una forma',
      writable = True, readable = True,
      represent = stv_represent_string),

    Field('user_id_asignado', 'integer', default = None,
        required = False,
        notnull = False,
        label = T('Created By'), comment = T('User that created this register'),
        writable = False, readable = D_stvFwkCfg.b_isSupportUser,
        represent = stv_represent_userAsText),
    
    Field('historia', 'text', default=None,
      required = False,
      notnull = False,
      label=('Hostoria'), comment = "Historia de la notidicación",
      writable = False, readable = D_stvFwkCfg.b_isSupportUser,
      represent = stv_represent_text),
    tSignature_dbc02,
    format = '%(titulo)s',
    singular = ('Notificación'),
    plural = ('Notificaciones'),
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )
TNOTIFICACIONES = TNOTIFICACIONES()

""" Definición de las opciones disponibles por formulario en el sistema """
class TNOTIFICACION_MENSAJES():
    class CODIGOERROR(CLASS_e_RETURN):
        pass
    class PRIORIDAD(CLASS_e_PRIORIDAD):
        pass
    pass
dbc02.define_table(
    'tnotificacion_mensajes',
    Field('notificacion_id', dbc02.tnotificaciones, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label=('Notificación'), comment=None,
      writable=False, readable=False,
      represent=None),
    
    Field('codigovalidacion', 'string', length = 100, default=None,
      required=True,
      notnull=True, unique=False,
      widget = stv_widget_input, label = ('Código Validación'), comment = ('Código único de validación'),
      writable = True, readable = True,
      represent = stv_represent_string),
    
    Field('descripcion', 'string', length = 100, default = None,
      required = True,
      notnull = True, unique = False,
      widget = stv_widget_input, label = ('Descripción'), comment = ('Descripción de la notificación'),
      writable = True, readable = True,
      represent = stv_represent_string),
    Field('excepcion', 'text', default=None,
      required = False,
      notnull = False,
      label=('Excepción'), comment = "Excepción que generó la notificación",
      writable = False, readable = D_stvFwkCfg.b_isSupportUser,
      represent = stv_represent_text),
    Field('ultimosql', 'text', default=None,
      required = False,
      notnull = False,
      label=('Último SQL'), comment = "Último SQL que generó la notificación",
      writable = False, readable = D_stvFwkCfg.b_isSupportUser,
      represent = stv_represent_text),

    Field('referencia_id', 'integer', default = 0,
      required = False, 
      notnull = False,
      widget = stv_widget_input, 
      label = ('Referencia ID'), comment = None,
      writable = True, readable = True,
      represent = stv_represent_number),

    Field('codigoerror', 'integer', default = TNOTIFICACION_MENSAJES.CODIGOERROR.OK_WARNING,
      required = False, requires = IS_IN_SET(TNOTIFICACION_MENSAJES.CODIGOERROR.GET_DICT(), zero = 0, error_message = 'Selecciona' ),
      notnull = False,
      widget = stv_widget_combobox, 
      label = ('Código Error'), comment = None,
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, TNOTIFICACION_MENSAJES.CODIGOERROR.GET_DICT())),

    Field('prioridad', 'integer', default = TNOTIFICACION_MENSAJES.PRIORIDAD.BAJA,
      required = True, requires = IS_IN_SET(TNOTIFICACION_MENSAJES.PRIORIDAD.GET_DICT(), zero = 0, error_message = 'Selecciona' ),
      notnull = False,
      widget = stv_widget_combobox, 
      label = ('Código Error'), comment = None,
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, TNOTIFICACION_MENSAJES.PRIORIDAD.GET_DICT())),
    Field('proceso', 'string', length = 200, default = "No definido",
      required = True,
      notnull = True, unique = False,
      widget = stv_widget_input, label = ('Proceso'), comment = ('Proceso durante la importación'),
      writable = True, readable = True,
      represent = stv_represent_string),
    
    tSignature_dbc02,
    format = '%(formulario_id)s / %(descripcion)s',
    singular = ('Diseño formulario'),
    plural = ('Diseño formularios'),
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )    
    
# Se crea la referencia para su uso en los campos
dbc01.tempresas._referenced_by.append(dbc02.tnotificaciones.empresa_id)
dbc01.tempresas._references.append(dbc02.tnotificaciones.empresa_id)

