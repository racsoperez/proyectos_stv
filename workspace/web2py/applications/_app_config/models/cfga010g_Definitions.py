# -*- coding: utf-8 -*-

""" Este archivo contiene las definiciónes genéricas de la sección de aplicaciones de la arquitectura.

Las definciones genércias se separan en dos tipos:
- Genéricas Multicuentas: son las definiciones que se comparten entre todas las cuentas
    * Catálogos del SAT
- Genéricas por Cuenta: son las definiciones que se encuentra en la base de datos específica por cuenta
    * Definición de Empresas
    * Definición de Clientes y Proveedores
    * Definición de Notificaciones

"""


D_stvSiteCfg.s_nameGenericApp = 'app_generales'
""" str: Nombre de la aplicación genérica multicuenta.
Aplicación que define y maneja los modelos genéricos multicuenta.
"""

D_stvSiteCfg.b_requestIsGenericApp = (request.application == D_stvSiteCfg.s_nameGenericApp)
""" bool: True si la aplicación del request actual corresponde a la aplicación genérica multicuenta.
"""

# Redireccionamiento principal en caso de que el usuario no este registrado
if request.is_scheduler:
    # Si esta corriendo el scheduler, ignora la protección de usuario
    pass

elif not auth.is_logged_in() and (request.function != 'userajax') and ('call/soap' not in request.url):
    # Si usuario no esta logeado, no es una llamada reviso de session ajax, y no es una llamada de web service

    if ('HTTP_X_REQUESTED_WITH' in request.env) and (request.env.HTTP_X_REQUESTED_WITH == 'XMLHttpRequest'):
        # Si es una llamada ajax
        request.vars['_next'] = request.url
        redirect(URL(a = 'acceso', c = 'default', f = 'usermain', args = ['login'], vars = request.vars))

    else:
        # Si es una llamada de una rutina principal
        request.vars['_next'] = request.url
        redirect(URL(a = 'acceso', c = 'default', f = 'user', args = ['login'], vars = request.vars))

else:
    # Código usado cuando el sistema se encuentre en mantenimiento, haciedo logout a cualquier usuario, excepto si es
    #  de stverticales; y mandando mensaje.
    # if not D_stvFwkCfg.b_isSupportUser and not D_stvFwkCfg.b_iswebservice:
    #     request.vars.update(_flash = "Sistema en mantenimiento, intentar nuevamente más tarde")
    #     redirect(URL(a = 'acceso', f = 'user', args = ['logout'], vars = request.vars))
    # else:
    #     pass
    pass


# Helper utilizado para el apoyo en las definiciones genéricas en los campos 
D_stvSiteHelper = Storage()
D_stvSiteHelper.dbconfigs = Storage()
D_stvSiteHelper.dbconfigs.defaults = Storage()  # Es usado para guardar los valores por default


# Encontrar si existe empresa definida en el argumento 1, para esto hay varias opciones
#   - Argumento 0 igual a "mater_empresas", usar el argumento 1
#   - Argumento 0 igual a "empresa_id", usar el argumento 1
#   - Argumento 0 termine con "_porempresa", usar el argumento 1
#   - Si existe en request.vars la variable master_empresa_plaza_sucursal_almacen_id
#       Usar método 
#           _s_empresa_id = TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.GET_EMPRESA_ID( 
#               s_empresa_plaza_sucursal_almacen_id = request.vars.master_empresa_plaza_sucursal_almacen_id
#               )
# Si no cumple alguna de las condiciones, default debe ser None

if request.args:
    
    if (
            request.args(0, 'none') in (
                'master_empresas', 'master_empresa_ingresos', 'master_empresa_inventarios',
                'master_empresa_organizacion', 'master_empresa_contpaqi', 'master_empresa_impuestos',
                'master_empresa_gastos', 'empresa_id'
                )
            ) \
            or (request.args(0).find("_porempresa") != -1):
        D_stvSiteHelper.dbconfigs.defaults.empresa_id = request.args(1, 0)
        
    else:
        D_stvSiteHelper.dbconfigs.defaults.empresa_id = 0

else:
    D_stvSiteHelper.dbconfigs.defaults.empresa_id = 0


def importar_dba(s_remplaza_cuenta_aplicacion_id = None, s_remplaza_cuenta_id = None):

    # Se respaldan los valores de cuenta_aplicacion y cuenta
    _temp_cuenta_aplicacion_id = D_stvFwkCfg.cfg_cuenta_aplicacion_id
    _temp_cuenta_id = D_stvFwkCfg.cfg_cuenta_id

    # En caso de estar definidos en los parámetros, se actualizan
    D_stvFwkCfg.cfg_cuenta_aplicacion_id = s_remplaza_cuenta_aplicacion_id \
        if s_remplaza_cuenta_aplicacion_id else D_stvFwkCfg.cfg_cuenta_aplicacion_id
    D_stvFwkCfg.cfg_cuenta_id = s_remplaza_cuenta_id if s_remplaza_cuenta_id else D_stvFwkCfg.cfg_cuenta_id

    # Define una lista de los archivos a importar desde la configuración del sitio.
    _L_fileFromConfig = ['dba.py']
    
    # Importar los modelos definidos en el framework
    stv_update.genericAppImport(D_stvSiteCfg.s_nameConfigApp, 'models', _L_fileFromConfig, globals())

    # Se restauran los parámetros de la cuenta
    D_stvFwkCfg.cfg_cuenta_aplicacion_id = _temp_cuenta_aplicacion_id
    D_stvFwkCfg.cfg_cuenta_id = _temp_cuenta_id

    return


def importar_dbs_genericos():
        
    # Define una lista de los archivos a importar desde la configuración del sitio.
    _L_fileFromConfig = [
                'dba.py', 
                'dba005_genericos_sat.py', 
                ]
    
    # Importar los modelos definidos en el framework
    stv_update.genericAppImport(D_stvSiteCfg.s_nameConfigApp, 'models', _L_fileFromConfig, globals())
    return


def importar_dbs_cuenta(s_remplaza_cuenta_aplicacion_id = None, s_remplaza_cuenta_id = None):

    # Se respaldan los valores de cuenta_aplicacion y cuenta
    _temp_cuenta_aplicacion_id = D_stvFwkCfg.cfg_cuenta_aplicacion_id
    _temp_cuenta_id = D_stvFwkCfg.cfg_cuenta_id

    # En caso de estar definidos en los parámetros, se actualizan
    D_stvFwkCfg.cfg_cuenta_aplicacion_id = s_remplaza_cuenta_aplicacion_id \
        if s_remplaza_cuenta_aplicacion_id else D_stvFwkCfg.cfg_cuenta_aplicacion_id
    D_stvFwkCfg.cfg_cuenta_id = s_remplaza_cuenta_id if s_remplaza_cuenta_id else D_stvFwkCfg.cfg_cuenta_id

    if D_stvFwkCfg.cfg_cuenta_aplicacion_id or D_stvFwkCfg.cfg_cuenta_id:
        # Define una lista de los archivos a importar desde la configuración del sitio.
        _L_fileFromConfig = [
            'dba.py',
            'cfga020g_Widgets.py',
            'cfga030g_Validaciones.py',
            'dba005_genericos_sat.py',
            'dba110_genericas.py',
            'dba120g_empresas.py',
            'dba130g_clientes.py',
            'dba140g_proveedores.py',
            'dba160g_bancos.py',
            'dba210g_notificaciones.py',
            ]

        # Importar los modelos definidos en el framework
        stv_update.genericAppImport(D_stvSiteCfg.s_nameConfigApp, 'models', _L_fileFromConfig, globals())

        if "importar_dbs_cuenta_especificas" in globals():
            importar_dbs_cuenta_especificas()
        else:
            # No importar las especificas a menos que existan
            pass
    else:
        # TODO regresar error de que no se tiene identificada la cuenta
        pass
    
    # Se restauran los parámetros de la cuenta
    D_stvFwkCfg.cfg_cuenta_aplicacion_id = _temp_cuenta_aplicacion_id
    D_stvFwkCfg.cfg_cuenta_id = _temp_cuenta_id

    return
