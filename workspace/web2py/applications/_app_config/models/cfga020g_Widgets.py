# -*- coding: utf-8 -*-

""" Widgets adicionales definidos específicamente para el cliente """

def stv_widget_inputMonthYear(field, value, D_additionalAttributes = None):
    D_additionalAttributes = D_additionalAttributes or Storage()
    D_additionalAttributes['_data-date-format'] = 'mm/yyyy'
    D_additionalAttributes['_autocomplete'] = "off"
    return stv_widget_input_common(
        field = field,
        value = value,
        sInputType = 'text',
        bAddTooltip = True,
        sAddClass = 'datepickermonthyear',
        sPlaceHolder = None,
        D_additionalAttributes = D_additionalAttributes,
        )


def stv_widget_inputYear(field, value, D_additionalAttributes = None):
    D_additionalAttributes = D_additionalAttributes or Storage()
    D_additionalAttributes['_data-date-format'] = 'yyyy'
    D_additionalAttributes['_autocomplete'] = "off"
    return stv_widget_input_common(
        field = field,
        value = value,
        sInputType = 'text',
        bAddTooltip = True,
        sAddClass = 'datepickeryear',
        sPlaceHolder = None,
        D_additionalAttributes = D_additionalAttributes,
        )


def stv_represent_money2campos(
        x_value, odbRow, fnLambda = None, dbField = None, dbField2 = None, D_additionalAttributes = None
        ):
    """

    @param x_value:
    @type x_value:
    @param odbRow:
    @type odbRow:
    @param fnLambda:
    @type fnLambda:
    @param dbField:
    @type dbField:
    @param dbField2: en caso de que x_value sea nulo, usa este campo en odbRow para mostrar el valor
    @type dbField2:
    @param D_additionalAttributes:
    @type D_additionalAttributes:
    @return:
    @rtype:
    """
    x_value = x_value or odbRow.get(str(dbField2), None)
    return stv_represent_money(
        x_value = x_value,
        odbRow = odbRow,
        fnLambda = fnLambda,
        dbField = dbField,
        D_additionalAttributes = D_additionalAttributes or Storage()
        )
