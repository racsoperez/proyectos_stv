# -*- coding: utf-8 -*-

""" Este archivo contiene las definiciónes de los modelos genéricos por cuenta.

Definiciones de modelos que se comparten entre las aplicaciones de una cuenta ya que se ubican en la base de datos
 específica por cuenta.
En general es información usada por el framework.
"""


class TEMPRESAS(Table):
    """ Definición de la tabla de empresas """

    S_NOMBRETABLA = 'tempresas'

    class MOSTRARCFDITOTALCERO:
        SIN_DEFINIR = 0
        MOSTRAR = 1
        NOMOSTRAR = 2
        
        @classmethod
        def get_dict(cls):
            return {
                cls.SIN_DEFINIR: 'Sin Definir',
                cls.MOSTRAR: 'Mostrar',
                cls.NOMOSTRAR: 'No mostrar',
                }

    class TIPOCONTABILIZACIONCANCELADOS:
        """     Configuración de tipo de contabilización que se quiere por CFDI cancelados.
            Si se elige INVERSO:
                La contabilización del CFDI será la misma contabilización que como se tiene en la prepoliza solo
                 que en los cargos serán abonos y viceversa.
            Si se elige NEGATIVO:
                La contabilización del CFDI solo se cambiará de signo respetando las misma posición
                 para cargos y abonos.
        """
        SIN_DEFINIR = 0
        INVERSO = 1
        NEGATIVO = 2

        @classmethod
        def get_dict(cls):
            return {
                cls.SIN_DEFINIR: 'Sin Definir',
                cls.INVERSO: 'Contabilización inversa',
                cls.NEGATIVO: 'Contabilización en negativo',
                }

    L_DBCAMPOS = [
        Field(
            'razonsocial', 'string', length = 80, default = None,
            required = True,
            notnull = True, unique = True,
            widget = stv_widget_input, label = 'Razón Social', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'nombrecorto', 'string', length = 20, default = None,
            required = True,
            notnull = True, unique = True,
            widget = stv_widget_input, label = 'Short Name', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'rfc', 'string', length = 15, default = None,
            required = True, requires = IS_MATCH(
                D_stvFwkHelper.common.regexp_rfc, error_message = 'Expresión inválida formato AAAA-999999-XXX'
                ),
            notnull = True, unique = True,
            widget = stv_widget_input, label = 'RFC', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'rfc_publico', 'string', length = 16, default = 'XAXX-010101-000',
            required = True, requires = IS_MATCH(
                D_stvFwkHelper.common.regexp_rfc, error_message = 'Expresión inválida formato AAAA-999999-XXX'
                ),
            notnull = True, unique = False,
            widget = stv_widget_input, label = 'RFC Venta Público',
            comment = 'RFC que contienen los clientes registrados para Venta en Público',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'rfc_publico_extranjeros', 'string', length = 16, default = 'XEXX-010101-000',
            # Se tuvo que poner una longitud de 16 para evitar problemas al asignar el valor por default
            required = True, requires = IS_MATCH(
                D_stvFwkHelper.common.regexp_rfc, error_message = 'Expresión inválida formato AAAA-999999-XXX'
                ),
            notnull = True, unique = False,
            widget = stv_widget_input, label = 'RFC Vta. Extranjeros',
            comment = 'RFC que contienen los clientes registrados para Venta en Público que son Extrangeros',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'moneda_id', 'integer', default = D_stvSiteHelper.dbconfigs.defaults.moneda_id,
            required = True, requires = IS_IN_DB(db01, 'tmonedas.id', db01.tmonedas._format),
            ondelete = 'NO ACTION', notnull = True, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Moneda',
            comment = 'Moneda en la que se lleva la contabilidad de la empresa',
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresas.moneda_id)
            ),

        # Este será el campo que se utilice en vez de moneda_id
        Field(
            'monedacontpaqi_id', 'integer', default = None,
            required = True,
            ondelete = 'NO ACTION', notnull = True, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Moneda CONTPAQi', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresas.monedacontpaqi_id)
            ),

        Field(
            'fecha_inicial', 'date', default = request.browsernow,
            required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
            notnull = False,
            widget = stv_widget_inputDate, label = 'Fecha inicial', comment = 'Fecha inicial',
            writable = True, readable = True,
            represent = stv_represent_date
            ),
        # Tab Domicilio Fiscal
        Field(
            'pais_id', 'integer', default = None,
            required = True, requires = IS_IN_DB(db01, 'tpaises.id', db01.tpaises._format),
            ondelete = 'NO ACTION', notnull = True, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Country', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresas.pais_id)
            ),
        Field(
            'pais_estado_id', 'integer', default = None,
            required = True, requires = IS_IN_DB(db01, 'tpais_estados.id', db01.tpais_estados._format),
            ondelete = 'NO ACTION', notnull = True, unique = False,
            widget = lambda field, value: stv_widget_db_chosen_linked(
                field, value, 1, 'pais_id',
                URL(a = 'app_generales', c = '005genericos_sat', f = 'localidad_pais_estados_ver.json')
                ),
            label = 'State', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresas.pais_estado_id)
            ),
        Field(
            'pais_estado_municipio_id', 'integer', default = None,
            required = True,
            requires = IS_IN_DB(db01, 'tpais_estado_municipios.id', db01.tpais_estado_municipios._format),
            ondelete = 'NO ACTION', notnull = True, unique = False,
            widget = lambda field, value: stv_widget_db_chosen_linked(
                field, value, 1, 'pais_estado_id',
                URL(a = 'app_generales', c = '005genericos_sat', f = 'localidad_pais_estado_mun_ver.json')
                ),
            label = 'Minicipality', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresas.pais_estado_municipio_id)
            ),
        Field(
            'ciudad', 'string', length = 30, default = None,
            required = True,
            notnull = True, unique = False,
            widget = stv_widget_input, label = 'City', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'colonia', 'string', length = 30, default = None,
            required = True,
            notnull = True, unique = False,
            widget = stv_widget_input, label = 'Town', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'calle', 'string', length = 30, default = None,
            required = True,
            notnull = True, unique = False,
            widget = stv_widget_input, label = 'Street', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'numexterior', 'string', length = 10, default = None,
            required = True,
            notnull = True, unique = False,
            widget = stv_widget_input, label = 'Num. Exterior', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'numinterior', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Num. Interior', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'codigopostal', 'string', length = 5, default = None,
            required = False,
            requires = IS_EMPTY_OR(IS_LENGTH(maxsize = 5, minsize = 5, error_message = T('enter %(min)g characters'))),
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Zip Code', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        # Tab Representante Legal
        Field(
            'apellidopaterno_rl', 'string', length = 27, default = None,
            required = True,
            notnull = True, unique = False,
            widget = stv_widget_input, label = 'Last Name', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'apellidomaterno_rl', 'string', length = 27, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Apellido Materno', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'nombre_rl', 'string', length = 27, default = None,
            required = True,
            notnull = True, unique = False,
            widget = stv_widget_input, label = 'Name', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'rfc_rl', 'string', length = 15, default = None,
            required = True, requires = IS_MATCH(
                D_stvFwkHelper.common.regexp_rfc, error_message = 'Expresión inválida formato AAAA-999999-XXX'
                ),
            notnull = True, unique = False,
            widget = stv_widget_input, label = 'RFC', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'curp_rl', 'string', length = 18, default = None,
            required = True, requires = IS_MATCH(
                D_stvFwkHelper.common.regexp_curp, error_message = 'Expresión inválida. Ej. BADD110313HCMLNS09'
                ),
            notnull = True, unique = False,
            widget = stv_widget_input, label = 'CURP', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'ife_rl', 'string', length = 18, default = None,
            required = False,
            requires = IS_EMPTY_OR(
                IS_LENGTH(maxsize = 18, minsize = 18, error_message = T('enter %(min)g characters'))
                ),
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'IFE', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'estudios_rl', 'string', length = 30, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Estudios', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'institucioneducativa_rl', 'string', length = 30, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Institución Educativa', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'cedulaprofesional_rl', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Cédula Profesional', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'actaconstitutiva_rl', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Acta Constitutiva', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'podernotarial_rl', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Poder Notarial', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'regimenfiscal_id', 'integer', default = None,
            required = False, requires = IS_IN_DB(db01, 'tregimenesfiscales.id', db01.tregimenesfiscales._format),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Régimen Fiscal SAT', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresas.regimenfiscal_id)
            ),
        Field(
            'claveciec', 'string', length = 20, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputPassword, label = 'Clave CIEC', comment = 'Clave CIEC asignada por el SAT',
            writable = True, readable = False,
            represent = stv_represent_string
            ),

        # Campo para la configuración de la empresa si desea ver los CFDI que sean 0 en los asientos de la poliza.
        # En Default se pondrá que no se muestren.
        Field(
            'mostrarcfditotalcero', 'integer', default = MOSTRARCFDITOTALCERO.NOMOSTRAR,
            required = False,
            requires = IS_IN_SET(MOSTRARCFDITOTALCERO.get_dict(), zero = 0, error_message = 'Selecciona'),
            notnull = False, unique = False,
            widget = stv_widget_combobox, label = 'Mostrar CFDIs total cero', comment = '',
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESAS.MOSTRARCFDITOTALCERO.get_dict())
            ),

        Field(
            'maximosobrantefaltante', 'decimal(8,2)', default = 1,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Máximo permitido faltante o sobrante de las cajas chicas.',
            comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),

        Field(
            'interesmoratoriopagare', 'decimal(8,2)', default = 5,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputPercentage, label = 'Interés moratorio',
            comment = 'Interés moratorio mensualusado en pagaré.',
            writable = True, readable = True,
            represent = stv_represent_percentage
            ),

        Field(
            'tipocontabilizacioncancelados', 'integer', default = TIPOCONTABILIZACIONCANCELADOS.INVERSO,
            required = False, requires = IS_IN_SET(
                TIPOCONTABILIZACIONCANCELADOS.get_dict(), zero = 0, error_message = 'Selecciona'
                ),
            notnull = False, unique = False,
            widget = stv_widget_combobox, label = 'Tipo de contabilización de los CFDIs cancelados', comment = '',
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESAS.TIPOCONTABILIZACIONCANCELADOS.get_dict())
            ),
        Field(
            'imagennombrearchivo', 'string', length = 100, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Nombre original del archivo', comment = None,
            writable = False, readable = False,
            represent = stv_represent_string
            ),
        Field(
            'imagen', type = 'upload',
            required = False,
            requires = IS_LENGTH(minsize = 0, maxsize = 1572864, error_message = 'El archivo es muy grande.'),
            notnull = False,
            uploadfield = True,
            widget = lambda f, d, u = None: stv_widget_inputFile(
                f, d, URL(
                    a = 'app_empresas', c = '120empresas', f = 'imagen',
                    vars = Storage(cfg_cuenta_aplicacion_id = D_stvFwkCfg.cfg_cuenta_aplicacion_id)
                    ),
                dbc01.tempresas.imagennombrearchivo
                ),
            label = 'Imagen', comment = 'Imagén que se usará en la facturación de CFDIs y en la página',
            writable = True, readable = True, authorize = lambda record: auth.is_logged_in(),
            autodelete = True,
            represent = lambda v, r: stv_represent_image(
                v, r, URL(
                    a = 'app_empresas', c = '120empresas', f = 'imagen',
                    vars = Storage(cfg_cuenta_aplicacion_id = D_stvFwkCfg.cfg_cuenta_aplicacion_id)
                    )
                ),
            uploadfolder = os.path.join(request.folder, 'uploads', 'empresas'),
            uploadseparate = False, uploadfs = None
            ),
        Field(
            'imagenpinnombrearchivo', 'string', length = 100, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Nombre original del archivo', comment = None,
            writable = False, readable = False,
            represent = stv_represent_string
            ),
        Field(
            'imagenpin', type = 'upload',
            required = False,
            requires = IS_LENGTH(minsize = 0, maxsize = 1572864, error_message = 'El archivo es muy grande.'),
            notnull = False,
            uploadfield = True,
            widget = lambda f, d, u = None: stv_widget_inputFile(
                f, d, URL(
                    a = 'app_empresas', c = '120empresas', f = 'imagenpin',
                    vars = Storage(cfg_cuenta_aplicacion_id = D_stvFwkCfg.cfg_cuenta_aplicacion_id)
                    ),
                dbc01.tempresas.imagenpinnombrearchivo
                ),
            label = 'Imagen Pin', comment = None,
            writable = True, readable = True, authorize = lambda record: auth.is_logged_in(),
            autodelete = True,
            represent = lambda v, r: stv_represent_image(
                v, r, URL(
                    a = 'app_empresas', c = '120empresas', f = 'imagenpin',
                    vars = Storage(cfg_cuenta_aplicacion_id = D_stvFwkCfg.cfg_cuenta_aplicacion_id)
                    )
                ),
            uploadfolder = os.path.join(request.folder, 'uploads', 'empresas'),
            uploadseparate = False, uploadfs = None
            ),
        tSignature_dbc01,
        ]

    def __init__(self, dbRef, tablename, *fields, **args):
        Table.__init__(self, dbRef, tablename, *fields, **args)

        if D_stvFwkCfg.dbRow_cuenta_aplicacion and D_stvFwkCfg.dbRow_cuenta_aplicacion.empresa_id:
            D_stvFwkCfg.dbRow_empresa = self(D_stvFwkCfg.dbRow_cuenta_aplicacion.empresa_id)

        elif D_stvSiteHelper.dbconfigs.defaults.empresa_id:
            D_stvFwkCfg.dbRow_empresa = self(D_stvSiteHelper.dbconfigs.defaults.empresa_id)

        else:
            D_stvFwkCfg.dbRow_empresa = None

        return
            
    pass


dbc01.define_table(
    TEMPRESAS.S_NOMBRETABLA, *TEMPRESAS.L_DBCAMPOS,
    format = '%(nombrecorto)s [%(rfc)s]',
    singular = 'Empresa',
    plural = 'Empresas',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TEMPRESAS
    )
STV_FWK_LIB.DEFINIR_INTEGRIDADREFERENCIAL(dbc01.tempresas, db.tcuenta_empresas.empresa_id)


class TEMPRESA_CONTPAQIEJERCICIOS:
    """ Definición de la tabla de empresa_contpaqiejercicios """

    @classmethod
    def DEFINIR_DEFAULT(cls):
        # Si no se puede crear el default regresa None
        try:
            _dbRows = dbc01(
                dbc01.tempresa_contpaqiejercicios.ejercicio <= request.utcnow.year
                ).select(
                    dbc01.tempresa_contpaqiejercicios.id,
                    dbc01.tempresa_contpaqiejercicios.ejercicio,
                    orderby = ~dbc01.tempresa_contpaqiejercicios.ejercicio,
                    limitby = (0, 1)
                    )
            if _dbRows:
                TEMPRESA_CONTPAQIEJERCICIOS.EJERCICIO_ACTUAL = cache.ram(
                    'db_ejercicios_ejercicio_actual', lambda: _dbRows.first().ejercicio, time_expire = 86400
                    )
            else:
                TEMPRESA_CONTPAQIEJERCICIOS.EJERCICIO_ACTUAL = None

        except Exception as _O_exception:
            TEMPRESA_CONTPAQIEJERCICIOS.EJERCICIO_ACTUAL = None
    pass


dbc01.define_table(
    'tempresa_contpaqiejercicios',
    Field(
        'empresa_id', dbc01.tempresas, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Empresa', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'ejercicio', 'integer',
        required = True, default = int(request.browsernow.year),
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Ejercicio inicial', comment = 'Ejercicio',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'empresacontpaqi', 'string', length = 80, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Base de datos CONTPAQi',
        comment = 'Este nombre debe de coincidir con el nombre que se tiene dada de alta la empresa en CONTPAQi',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'cuenta_servidorcontpaqi_id', 'integer', default = None,
        required = True,
        requires = IS_IN_DB(db, 'tcuenta_servidorescontpaqi.id', db.tcuenta_servidorescontpaqi._format),
        ondelete = 'NO ACTION', notnull = True, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Servidor', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tempresa_contpaqiejercicios.cuenta_servidorcontpaqi_id
            )
        ),
    tSignature_dbc01,
    format = '%(ejercicio)s: %(empresacontpaqi)s',
    singular = 'Base de datos',
    plural = 'Bases de datos',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )
TEMPRESA_CONTPAQIEJERCICIOS.DEFINIR_DEFAULT()


class TEMPRESA_CUENTASBANCARIAS:
    """ Definición de la tabla de empresas_cuentasbanacarias """

    class TIPOCOMISION:
        SIN_COMISION = 0
        FIJO = 1
        PORCENTAJE = 2
        
        @classmethod
        def get_dict(cls):
            return {
                cls.SIN_COMISION: 'Sin comisión',
                cls.FIJO        : 'Fijo',
                cls.PORCENTAJE  : 'Porcentaje',
                }
    
    @classmethod
    def CONFIGURAR_INTEGRIDAD_REFERENCIAL(cls):
        _dbTable = dbc01.tempresa_cuentasbancarias

        _dbTable.monedacontpaqi_id.type = 'reference tempresa_monedascontpaqi'
        _dbTable.monedacontpaqi_id.ondelete = 'NO ACTION'
        _dbTable.monedacontpaqi_id.requires = IS_IN_DB(
            dbc01(dbc01.tempresa_monedascontpaqi.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id),
            'tempresa_monedascontpaqi.id',
            dbc01.tempresa_monedascontpaqi._format
            )
        _dbTable.monedacontpaqi_id.widget = lambda f, v: stv_widget_db_chosen(f, v, 1)
        _dbTable.monedacontpaqi_id.represent = stv_represent_referencefield

        # Referencia a tempresa_cuentasbancarias
        dbc01.tempresa_bancoscontpaqi._referenced_by.append(_dbTable.empresa_bancocontpaqi_id)
        dbc01.tempresa_bancoscontpaqi._references.append(_dbTable.empresa_bancocontpaqi_id)

        _dbTable.empresa_bancocontpaqi_id.requires = IS_IN_DB(
            dbc01, 'tempresa_bancoscontpaqi.id', dbc01.tempresa_bancoscontpaqi._format
            )

        return None
    
    pass


dbc01.define_table(
    'tempresa_cuentasbancarias',
    Field(
        'empresa_id', dbc01.tempresas, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Empresa', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    # TODO: Ver si esto se sigue necesitando. Pregunto esto porque ya se tiene un detalle de guías para las cuentas.
    Field(
        'cuenta', 'string', length = 20, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Cuenta', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    # Este campo se dejará de utilizar para poderse usar el de monedasCONTPAQi
    Field(
        'moneda_id', 'integer', default = D_stvSiteHelper.dbconfigs.defaults.moneda_id,
        required = True, requires = IS_IN_DB(db01, 'tmonedas.id', db01.tmonedas._format),
        ondelete = 'NO ACTION', notnull = True, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Moneda', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_cuentasbancarias.moneda_id)
        ),

    # Este será el campo que se utilice en vez de moneda_id
    Field(
        'monedacontpaqi_id', 'integer', default = D_stvSiteHelper.dbconfigs.defaults.moneda_contpaqi_id,
        required = True,
        ondelete = 'NO ACTION', notnull = True, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Moneda CONTPAQi', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_cuentasbancarias.monedacontpaqi_id)
        ),

    Field(
        'sucursal', 'string', length = 20, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Sucursal', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'fechaalta', 'date', default = request.browsernow,
        required = True, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = True,
        widget = stv_widget_inputDate, label = 'Fecha Alta Banco', comment = 'Fecha Alta Banco',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'fechabaja', 'date',
        required = False, requires = IS_NULL_OR(IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE)),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha Baja Banco', comment = 'Fecha Baja Banco',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'tipocuenta', 'string', length = 20, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Tipo de Cuenta', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'firmante1', 'string', length = 50, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Firmante 1', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'firmante2', 'string', length = 50, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Firmante 2', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'firmante3', 'string', length = 50, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Firmante 3', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'tipocomision', 'integer', default = 0,
        required = False, requires = IS_IN_SET(
            TEMPRESA_CUENTASBANCARIAS().TIPOCOMISION.get_dict(), zero = 0, error_message = 'Selecciona'
            ),
        notnull = False, unique = False,
        widget = stv_widget_combobox, label = 'Tipo de comisión', comment = '',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_CUENTASBANCARIAS().TIPOCOMISION.get_dict())
        ),
    Field(
        'comisionfija', 'decimal(8,2)', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputMoney, label = 'Comisión fija', comment = None,
        writable = True, readable = True,
        represent = stv_represent_money_ifnotzero
        ),
    Field(
        'comisionporcentajecredito', 'decimal(4,2)', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputPercentage, label = 'Comisión % Credito', comment = None,
        writable = True, readable = True,
        represent = stv_represent_percentage
        ),
    Field(
        'comisionporcentajedebito', 'decimal(4,2)', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputPercentage, label = 'Comisión % Debito', comment = None,
        writable = True, readable = True,
        represent = stv_represent_percentage
        ),
    Field(
        'comisionporcentajepromocion', 'decimal(4,2)', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputPercentage, label = 'Comisión % Promo.', comment = None,
        writable = True, readable = True,
        represent = stv_represent_percentage
        ),
    Field(
        'cuentacontable', 'string', length = 30, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Cta. Contable', comment = 'Cuenta contable de la cuenta bancaria',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'cuentacontable_provisionescomisiones', 'string', length = 30, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Cta. Contable Provisiones',
        comment = 'Cuenta contable de provisones de comisiones',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'cuentacontable_comisiones', 'string', length = 30, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Cta. Contable Comisiones', comment = 'Cuenta contable de comisiones',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'cuentacontable_complementaria', 'string', length = 30, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Cta. Contable Complementaria',
        comment = (
            'Cuenta contable complementaria para su uso unicamente si la moneda de la cuenta, '
            + 'es diferente a la moneda de la definida en la empresa'
            ),
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'empresa_bancocontpaqi_id', 'integer', default = None,
        required = True,
        ondelete = 'NO ACTION', notnull = True, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Codigo banco CONTPAQi', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tempresa_cuentasbancarias.empresa_bancocontpaqi_id
            )
        ),
    Field(
        'codigo_contpaqi', 'string', length = 5, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Código CONTPAQi cuenta bancaria',
        comment = 'Código de cuenta bancaria utilizado en CONTAPQi',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    tSignature_dbc01,
    format = lambda r: (TEMPRESA_BANCOSCONTPAQI.GETROWFORMATED(
        r.empresa_bancocontpaqi_id
        ) + " : " + r.cuenta + " : " + TEMPRESA_MONEDASCONTPAQI.GETROWFORMATED(r.monedacontpaqi_id)),
    singular = 'Cuenta bancaria',
    plural = 'Cuentas bancarias',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_PLAZAS:
    
    @classmethod
    def getRowFormated(cls, n_id):
        if n_id:
            if hasattr(dbc01.tempresa_plazas._format, '__call__'):
                # ...ejecuta la función y regresa el resultado.
                return dbc01.tempresa_plazas._format(dbc01.tempresa_plazas(n_id))
            else:
                # ...de lo contrario haz el formato.
                return dbc01.tempresa_plazas._format % dbc01.tempresa_plazas(n_id).as_dict()
        else:
            return 'Nada'
    
    pass


dbc01.define_table(
    'tempresa_plazas',
    Field(
        'empresa_id', dbc01.tempresas, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Empresa', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'nombrecorto', 'string', length = 25, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Plaza', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    tSignature_dbc01,
    format = '%(nombrecorto)s',
    singular = 'Plaza',
    plural = 'Plazas',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_PLAZA_SUCURSALES:

    @staticmethod
    def DBTABLA(s_id = None):
        if s_id is None:
            return dbc01.tempresa_plaza_sucursales
        else:
            return dbc01.tempresa_plaza_sucursales(s_id)

    @classmethod
    def getRowFormated(cls, n_id):
        if n_id:
            if hasattr(dbc01.tempresa_plaza_sucursales._format, '__call__'):
                # ...ejecuta la función y regresa el resultado.
                return dbc01.tempresa_plaza_sucursales._format(dbc01.tempresa_plaza_sucursales(n_id))
            else:
                # ...de lo contrario haz el formato.
                return dbc01.tempresa_plaza_sucursales._format % dbc01.tempresa_plaza_sucursales(n_id).as_dict()
        else:
            return 'Nada'

    @classmethod
    def OBTENER(
            cls,
            s_empresa_id = None,
            ):
        """

        @param s_empresa_id:
        @type s_empresa_id:
        @return:
        @rtype:
        """

        def buscar():
            _qryEmpresa = (
                (dbc01.tempresa_plazas.empresa_id == s_empresa_id)
                & (dbc01.tempresa_plaza_sucursales.empresa_plaza_id == dbc01.tempresa_plazas.id)
                if s_empresa_id else True
                )

            _dbRows = dbc01(
                _qryEmpresa
                ).select(
                    dbc01.tempresa_plaza_sucursales.id
                    )
            return [_dbRow.id for _dbRow in _dbRows]

        _dbTabla = cls.DBTABLA()
        if not s_empresa_id:
            s_empresa_id = D_stvSiteHelper.dbconfigs.defaults.empresa_id
        else:
            pass
        _s_id_cache = "%s_%s" % (_dbTabla._tablename, str(s_empresa_id))
        _L_sucursales_ids = cache.ram(
            _s_id_cache, lambda: buscar(), time_expire = 86400
            )

        return _L_sucursales_ids

    @classmethod
    def CONFIGURAR_INTEGRIDAD_REFERENCIAL(cls):
        _dbTabla = dbc01.tempresa_plaza_sucursales

        return None

    pass


dbc01.define_table(
    'tempresa_plaza_sucursales',
    Field(
        'empresa_plaza_id', dbc01.tempresa_plazas, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Plaza', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'nombrecorto', 'string', length = 25, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Sucursal', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'pais_id', 'integer', default = None,
        required = False, requires = IS_IN_DB(db01, 'tpaises.id', db01.tpaises._format),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Country', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_plaza_sucursales.pais_id)
        ),
    Field(
        'pais_estado_id', 'integer', default = None,
        required = False, requires = IS_IN_DB(db01, 'tpais_estados.id', db01.tpais_estados._format),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda field, value: stv_widget_db_chosen_linked(
            field, value, 1, 'pais_id',
            URL(a = 'app_generales', c = '005genericos_sat', f = 'localidad_pais_estados_ver.json')
            ),
        label = 'State', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_plaza_sucursales.pais_estado_id)
        ),
    Field(
        'pais_estado_municipio_id', 'integer', default = None,
        required = False, requires = IS_IN_DB(db01, 'tpais_estado_municipios.id', db01.tpais_estado_municipios._format),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda field, value: stv_widget_db_chosen_linked(
            field, value, 1, 'pais_estado_id',
            URL(a = 'app_generales', c = '005genericos_sat', f = 'localidad_pais_estado_mun_ver.json')
            ),
        label = 'Minicipality', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tempresa_plaza_sucursales.pais_estado_municipio_id
            )
        ),
    Field(
        'ciudad', 'string', length = 30, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'City', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'colonia', 'string', length = 30, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Town', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'calle', 'string', length = 30, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Street', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'numexterior', 'string', length = 10, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Num. Exterior', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'numinterior', 'string', length = 10, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Num. Interior', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'codigopostal', 'string', length = 5, default = None,
        required = False,
        requires = IS_EMPTY_OR(IS_LENGTH(maxsize = 5, minsize = 5, error_message = T('enter %(min)g characters'))),
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Zip Code', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    tSignature_dbc01,
    format = lambda r: (TEMPRESA_PLAZAS.getRowFormated(r.empresa_plaza_id) + " : " + r.nombrecorto),
    singular = 'Sucursal',
    plural = 'Sucursales',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_ORGANIZACIONESTRUCTURA:
    pass


dbc01.define_table(
    'tempresa_organizacionestructura',
    Field(
        'empresa_id', dbc01.tempresas, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Empresa', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'nombrecorto', 'string', length = 25, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Nombre Corto', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'padre_id', 'integer', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Estructura padre', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield_tree(v, r, dbc01.tempresa_organizacionestructura.padre_id)
        ),
    Field(
        'orden', 'integer', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputInteger, label = 'Orden', comment = None,
        writable = True, readable = True,
        represent = stv_represent_number
        ),
    tSignature_dbc01,
    format = '%(id)s: %(nombrecorto)s',
    singular = 'Estructura de Organización',
    plural = 'Estructura de Organizacines',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )


dbc01.tempresa_organizacionestructura.padre_id.type = 'reference tempresa_organizacionestructura'
dbc01.tempresa_organizacionestructura.padre_id.ondelete = 'NO ACTION'
dbc01.tempresa_organizacionestructura.padre_id.requires = IS_IN_DB(
    dbc01, 'tempresa_organizacionestructura.id', '%(id)s: %(nombrecorto)s'
    )
dbc01.tempresa_organizacionestructura.padre_id.widget = lambda f, v: stv_widget_db_chosen(f, v, 1)

# Se crea la referencia para su uso en los campos
dbc01.tempresa_organizacionestructura._referenced_by.append(dbc01.tempresa_organizacionestructura.padre_id)
dbc01.tempresa_organizacionestructura._references.append(dbc01.tempresa_organizacionestructura.padre_id)


class TEMPRESA_ESQUEMASIMPUESTOSTRASLADADOS:
    """ Definición de la tabla de tipo de impuestos trasladados """

    class E_TIPOFACTOR:
        """ Se definen las opciónes del campo """
        TASA = "Tasa"
        CUOTA = "Cuota"
        _D_dict = None

        @classmethod
        def get_dict(cls):
            if not cls._D_dict:
                cls._D_dict = {
                    cls.TASA : 'Tasa',
                    cls.CUOTA: 'Cuota',
                    }
            else:
                pass
            return cls._D_dict

    pass


dbc01.define_table(
    'tempresa_esquemasimpuestostrasladados',
    Field(
        'empresa_id', dbc01.tempresas, default=None,
        required=True,
        ondelete='CASCADE', notnull=True, unique=False,
        label='Empresa', comment=None,
        writable=False, readable=False,
        represent=None
        ),
    Field(
        'impuesto_id', 'integer', default=TIMPUESTOS.IVA,
        required=False, requires = IS_NULL_OR(IS_IN_DB(db01, 'timpuestos.id', db01.timpuestos._format)),
        ondelete='NO ACTION', notnull=False, unique=False,
        widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label='Impuesto', comment=None,
        writable=True, readable=True,
        represent=lambda v, r: stv_represent_referencefield(v, r, dbc01.tcfdi_concepto_impuestostrasladados.impuesto_id)
        ),
    Field(
        'tipofactor', 'string', length = 10, default = TEMPRESA_ESQUEMASIMPUESTOSTRASLADADOS.E_TIPOFACTOR.TASA,
        required = False,
        requires = IS_IN_SET(TEMPRESA_ESQUEMASIMPUESTOSTRASLADADOS.E_TIPOFACTOR.get_dict()),
        notnull = False, unique = False,
        widget = stv_widget_combobox, label = 'Tipo Factor', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(
            v, r, fnLambda = None,
            D_data = TEMPRESA_ESQUEMASIMPUESTOSTRASLADADOS.E_TIPOFACTOR.get_dict(),
            dbField = dbc01.tempresa_esquemasimpuestostrasladados.tipofactor
            )
        ),
    Field(
        'nombre', 'string', length=100, default=None,
        required=False,
        notnull=False, unique=False,
        widget=stv_widget_input, label='Nombre',
        comment='Nombre del esquema de impuestos trasladados. Eg. Gravado General, Excento, Tasa cero, etc.',
        writable=True, readable=True,
        represent=stv_represent_string
        ),
    tSignature_dbc01,
    format='%(nombre)s [%(tipofactor)s]',
    singular='Esquema impuesto trasladado',
    plural='Esquemas de impuestos trasladados',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_PLAZA_SUCURSAL_ESQUEMASIMPTRAS:
    """ Definición de la tabla de esquemas de impuestos trasladados definido por sucursal """
    pass


dbc01.define_table(
    'tempresa_plaza_sucursal_esquemasimptras',
    Field(
        'empresa_plaza_sucursal_id', dbc01.tempresa_plaza_sucursales, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Plaza', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'ejercicio', 'integer', default = None,
        required = True,
        requires = IS_IN_SET(
            VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id), zero = 0,
            error_message = 'Selecciona'
            ),
        notnull = False, unique = False,
        widget = stv_widget_combobox,
        label = 'Ejercicio inicial', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(
            v, r, None, VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id)
            )
        ),
    Field(
        'empresa_esquemaimptras_id', dbc01.tempresa_esquemasimpuestostrasladados, default = None,
        required = True,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Esquema Imp. Tras.',
        comment = 'Esquema de impuestos trasladados',
        writable = True, readable = True,
        represent = stv_represent_referencefield
        ),
    Field(
        'descripcion', 'string', length = 500, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Descripcion', comment = 'Información adicional que se quiera definir.',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'tasaocuota', 'decimal(14,4)', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputFloat, label = 'Tasa o Cuota',
        comment = 'Si el esquema es tasa, usar punto decimal (Ej. 0.16), si es cuota usar la cantidad',
        writable = True, readable = True,
        represent = stv_represent_float
        ),
    tSignature_dbc01,
    format = lambda r: "%d : %s (%.2f%%)" % (
        r.ejercicio, dbc01.tempresa_esquemasimpuestostrasladados(r.empresa_esquemaimptras_id).nombre, r.tasaocuota
        ),
    singular = 'Esquema impuesto trasladado por sucursal',
    plural = 'Esquemas de impuestos trasladados por sucursal',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_PRODSERV:

    @staticmethod
    def DBTABLA(s_id = None):
        if s_id is None:
            return dbc01.tempresa_prodserv
        else:
            return dbc01.tempresa_prodserv(s_id)

    class E_TIPO:
        PRODUCTO = 'P'
        SERVICIO = 'S'
        BONIFICACION = 'B'
        
        @classmethod
        def get_dict(cls):
            return {
                cls.PRODUCTO: 'Producto',
                cls.SERVICIO: 'Servicio',
                cls.BONIFICACION: 'Bonificaciones o rebaja',
                }
    
    class E_TIPOSEGUIMIENTO:
        # No lleva "sin
        ESTANDAR = 1
        LOTIFICADO = 2
        NUMERO_SERIE = 3
        
        @classmethod
        def get_dict(cls):
            return {
                cls.ESTANDAR: 'Estandar',
                cls.LOTIFICADO: 'Lotificado',
                cls.NUMERO_SERIE: 'Número de serie',
                }
    
    class E_TIPOCALCULO:
        NO_DEFINIDO = 0
        PRECIO_OFICIAL = 1
        PRECIO_COTIZADO = 2
        PRECIO_FIJO = 3

        @classmethod
        def get_dict(cls):
            return {
                # cls.NO_DEFINIDO: 'No definido',
                cls.PRECIO_OFICIAL    : 'Precios oficiales',
                cls.PRECIO_COTIZADO   : 'Precios cotizados + % de utilidad',
                cls.PRECIO_FIJO       : 'Precios fijos + % de utilidad',
                }
            
    @classmethod
    def CONFIGURAR_INTEGRIDAD_REFERENCIAL(cls):
        return None

    # >> Configuraciones especificas por cliente plugin: CASCOS
    @classmethod
    def CONFIGURAR_CAMPO_CASCO1A1(cls, dbField = None):
        """ Configura un campo para seleccionar entre los productos que usan una categoría para cascos

        @param dbField:
        @type dbField:
        @return:
        @rtype:
        """
        _dbTable = dbc01.tempresa_prodserv

        dbField = dbField or _dbTable.empresa_prodserv_casco1a1_id

        # TODO crear cache de esta consulta
        _dbRows_categorias1a1 = dbc01(
            (dbc01.tempresa_prodcategorias.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id)
            & (dbc01.tempresa_prodcategorias.uso_cascos1a1 == True)
            ).select(dbc01.tempresa_prodcategorias.id)

        _L_categorias1a1 = []
        for _dbRow in _dbRows_categorias1a1:
            _L_categorias1a1.append(_dbRow.id)

        dbField.requires = IS_NULL_OR(
            IS_IN_DB(
                dbc01(_dbTable.categoria_id.belongs(_L_categorias1a1)),
                'tempresa_prodserv.id',
                _dbTable._format
                )
            )

        return None

    @classmethod
    def ES_CASCO(cls, s_empresa_prodserv_id):
        """ Determina si el producto o servicio esta asociado a una categoría de manejo de cascos

        TODO agregar resultado en cache

        @param s_empresa_prodserv_id:
        @type s_empresa_prodserv_id:
        @return:
        @rtype:
        """
        _D_return = FUNC_RETURN(
            n_prodcategoria_id = 0,
            b_resultado = False
            )
                
        _dbTable = dbc01.tempresa_prodserv
        
        _dbRows = dbc01(
            (_dbTable.id == s_empresa_prodserv_id)
            & (dbc01.tempresa_prodcategorias.id == _dbTable.categoria_id)
            & (dbc01.tempresa_prodcategorias.uso_cascos1a1 == True)
            ).select(dbc01.tempresa_prodcategorias.id)
            
        if _dbRows:
            _D_return.n_prodcategoria_id = _dbRows.first().id
            _D_return.b_resultado = True
        else:
            _D_return.n_prodcategoria_id = 0
            _D_return.b_resultado = False

        return _D_return
    # <<
    
    @classmethod
    def GET_PRECIOVENTA(
            cls,
            x_prodserv,
            f_precio = 0,
            f_tipocambio = 1
            ):
        """ Determina si precio esta en rango y lo regresa, de lo contrario regresa rango y ajusta el nuevo precio

        @param f_tipocambio: tipo de cambio a pesos mexicanos
        @type f_tipocambio:
        @param x_prodserv:
        @type x_prodserv:
        @param f_precio:
        @type f_precio:
        @return:
        @rtype:
        """
        _D_return = FUNC_RETURN(
            f_precioMinimo = 0,
            f_precioMaximo = 0,
            f_nuevoPrecio = 0,
            f_precioCasco1a1 = 0,  # Si aplica, regresa el precio del casco 1 a 1
            f_precioMinimo_mxn = 0,
            f_precioMaximo_mxn = 0,
            f_precioCasco1a1_mxn = 0,  # Si aplica, regresa el precio del casco 1 a 1
            )
                
        _dbTable = cls.DBTABLA()

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_prodserv, _dbTable)
        _dbRow_prodserv = _D_results.dbRow

        if _dbRow_prodserv.preciotipocalculo == TEMPRESA_PRODSERV.E_TIPOCALCULO.PRECIO_FIJO:
            _D_return.f_precioMinimo_mxn = _dbRow_prodserv.preciofijo or 0
            _D_return.f_precioMaximo_mxn = _D_return.f_precioMinimo_mxn * \
                Decimal(1 + (_dbRow_prodserv.porcentajeutilidad or 0) / 100)

        elif _dbRow_prodserv.preciotipocalculo == TEMPRESA_PRODSERV.E_TIPOCALCULO.PRECIO_OFICIAL:
            if not _dbRow_prodserv.empresa_proveedor_id:
                _D_return.agrega_error("Producto/Servicio esta configurado para precio oficial "
                    + "pero proveedor no definido")

            else:
                _dbT_listaPrecios = dbc01.tempresa_proveedor_preciosoficiales
                _dbT_listaProductos = dbc01.tempresa_proveedor_preciooficial_productos
                _dbRows_listaPrecio = dbc01(
                    (_dbT_listaPrecios.empresa_proveedor_id == _dbRow_prodserv.empresa_proveedor_id)
                    & (_dbT_listaPrecios.fechavigencia_inicial <= request.browsernow)
                    & (
                        (_dbT_listaPrecios.fechavigencia_final > request.browsernow)
                        | (_dbT_listaPrecios.fechavigencia_final == None)
                        )
                    ).select(
                        _dbT_listaPrecios.ALL,
                        _dbT_listaProductos.ALL,
                        left = [
                            _dbT_listaProductos.on(
                                (_dbT_listaProductos.empresa_proveedor_preciooficial_id == _dbT_listaPrecios.id)
                                & (_dbT_listaProductos.empresa_producto_id == _dbRow_prodserv.id)
                                )
                            ],
                        orderby = [~_dbT_listaPrecios.fechavigencia_inicial]
                        )

                if not _dbRows_listaPrecio:
                    _D_return.agrega_error("No existe lista de precios vigente para proveedor de producto [%s]%s. "
                        % (_dbRow_prodserv.codigo, _dbRow_prodserv.descripcion))

                elif len(_dbRows_listaPrecio) > 1:
                    _D_return.agrega_error("Existen más de un precio oficial para el producto [%s]%s. "
                        % (_dbRow_prodserv.codigo, _dbRow_prodserv.descripcion))

                elif not _dbRows_listaPrecio.first().tempresa_proveedor_preciooficial_productos.preciooficial:
                    _D_return.agrega_error("Existen un precio oficial vacío para el producto [%s]%s. "
                        % (_dbRow_prodserv.codigo, _dbRow_prodserv.descripcion))

                else:
                    _f_precioOfical = _dbRows_listaPrecio.first() \
                        .tempresa_proveedor_preciooficial_productos.preciooficial
                    # TODOMejora considerar tipo de cambio y unidad
                    _D_return.f_precioMinimo_mxn = _f_precioOfical * \
                        (1 - Decimal((_dbRow_prodserv.porcentajemaxdesc or 0) / 100))
                    _D_return.f_precioMaximo_mxn = _f_precioOfical

        else:
            _D_return.agrega_error("Tipo de determinación de precio del producto no soportado")

        if _dbRow_prodserv.empresa_prodserv_casco1a1_id:
            # Si tiene configurado caso 1 a 1 el producto, se llena la información del precio del casco
            _D_results = TEMPRESA_PRODSERV.GET_PRECIOVENTA(
                _dbRow_prodserv.empresa_prodserv_casco1a1_id,
                f_tipocambio = f_tipocambio
                )
            _D_return.f_precioCasco1a1_mxn = _D_results.f_nuevoPrecio
            _D_return.f_precioCasco1a1 = _D_return.f_precioCasco1a1_mxn / (f_tipocambio or 1)

        else:
            pass

        _D_return.f_precioMinimo = _D_return.f_precioMinimo_mxn / (f_tipocambio or 1)
        _D_return.f_precioMaximo = _D_return.f_precioMaximo_mxn / (f_tipocambio or 1)

        if (f_precio >= _D_return.f_precioMinimo) \
                and (f_precio <= _D_return.f_precioMaximo):
            _D_return.f_nuevoPrecio = f_precio

        elif cls.ES_CASCO(_dbRow_prodserv.id).b_resultado:
            _D_return.f_nuevoPrecio = _D_return.f_precioMinimo
            if f_precio > 0:
                _D_return.agrega_error("Precio fuera de rango, se ajusta para el tipo de casco")
            else:
                # No se identifica error, ya que no existía precio antes
                pass

        elif f_precio > 0:
            _D_return.f_nuevoPrecio = _D_return.f_precioMaximo
            _D_return.agrega_error("Precio fuera de rango, se ajusta para el producto")

        else:
            # Si el precio era cero, no hay problema se ajusta
            _D_return.f_nuevoPrecio = _D_return.f_precioMaximo

        return _D_return

    @classmethod
    def GET_PRECIO_EXCEDENTE(cls, x_prodserv, f_precio, f_tipocambio = 1):
        _D_return = FUNC_RETURN(
            f_precioMinimo = 0,
            f_precioMaximo = 0,
            f_nuevoPrecio = 0,
            f_precioMinimo_mxn = 0,
            f_precioMaximo_mxn = 0,
            )

        _dbTable = cls.DBTABLA()

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_prodserv, _dbTable)
        _dbRow_prodServ = _D_results.dbRow

        if not cls.ES_CASCO(_dbRow_prodServ.id).b_resultado:
            _D_return.agrega_error("No es identificado como casco el producto [%s]%s. "
                % (_dbRow_prodserv.codigo, _dbRow_prodserv.descripcion))

        elif _dbRow_prodServ.preciotipocalculo != TEMPRESA_PRODSERV.E_TIPOCALCULO.PRECIO_FIJO:
            _D_return.agrega_error("El tipo cálculo del precio para el casco no es precio fijo [%s]%s. "
                % (_dbRow_prodserv.codigo, _dbRow_prodserv.descripcion))

        else:
            _D_return.f_precioMinimo_mxn = _dbRow_prodServ.excedente_preciopropuesto
            _D_return.f_precioMaximo_mxn = _dbRow_prodServ.excedente_preciomaximo

        _D_return.f_precioMinimo = _D_return.f_precioMinimo_mxn / (f_tipocambio or 1)
        _D_return.f_precioMaximo = _D_return.f_precioMaximo_mxn / (f_tipocambio or 1)

        if (f_precio >= _D_return.f_precioMinimo) \
                and (f_precio <= _D_return.f_precioMaximo):
            _D_return.f_nuevoPrecio = f_precio
        else:
            _D_return.f_nuevoPrecio = _D_return.f_precioMinimo

        return _D_return
        
    pass


dbc01.define_table(
    'tempresa_prodserv',
    Field(
        'empresa_id', dbc01.tempresas, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Plaza', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'codigo', 'string', length = 100, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'No. Identificacion', comment = 'Código interno del producto o servicio',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'tipo', 'string', length = 1, default = TEMPRESA_PRODSERV.E_TIPO.PRODUCTO,
        required = True,
        requires = IS_IN_SET(TEMPRESA_PRODSERV.E_TIPO.get_dict(), zero = 0, error_message = 'Selecciona'),
        notnull = True, unique = False,
        widget = stv_widget_combobox, label = 'Tipo', comment = '',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_PRODSERV.E_TIPO.get_dict())
        ),
    Field(
        'descripcion', 'string', length = 500, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Descripción', comment = 'Descripción del producto o servicio',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'unidad_id', 'integer', default = None,
        required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tunidades.id', '%(nombre)s [%(c_claveunidad)s]')),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_search(
            f, v,
            s_display = '%(nombre)s [%(c_claveunidad)s]',
            s_url = URL(a = 'app_generales', c = '005genericos_sat', f = 'unidades_buscar'),
            D_additionalAttributes = {'reference': db01.tunidades.id}
            ),
        label = 'Unidad', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_prodserv.unidad_id)
        ),
    Field(
        'marca_id', dbc01.tmarcas, default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Marca', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r)
        ),
    Field(
        'prodserv_id', 'integer', default = None,
        required = False,
        requires = IS_NULL_OR(IS_IN_DB(db01, 'tprodservs.id', '%(descripcion)s [%(c_claveprodserv)s]')),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_search(
            f, v,
            s_display = '%(descripcion)s [%(c_claveprodserv)s]',
            s_url = URL(a = 'app_generales', c = '005genericos_sat', f = 'prodservs_buscar'),
            D_additionalAttributes = {'reference': db01.tprodservs.id}
            ),
        label = 'Producto o Servicio', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_prodserv.prodserv_id)
        ),
    Field(
        'categoria_id', 'integer', default = None,
        required = False, requires = IS_NULL_OR(IS_IN_DB(dbc01(), 'tempresa_prodcategorias.id', '%(nombrecorto)s')),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen_tree(
            f, v, 1, D_additionalAttributes = {
                'dbTable_tree'        : dbc01.tempresa_prodcategorias,
                'dbField_order'       : dbc01.tempresa_prodcategorias.orden,
                'dbField_tree_groupby': dbc01.tempresa_prodcategorias.padre_id,
                'class_dbTree'        : DB_Tree,
                'b_onlyLastNodes'     : True,
                'b_includeId'         : True,
                'L_addFieldsInfo'     : [dbc01.tempresa_prodcategorias.uso_cascos1a1]
                }
            ),
        label = 'Categoria', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield_tree(
            v, r, dbc01.tempresa_prodcategorias.padre_id, dbc01.tempresa_prodserv.categoria_id
            )
        ),
    Field(
        'tiposeguimiento', 'integer', default = 1,
        required = True,
        requires = IS_IN_SET(TEMPRESA_PRODSERV.E_TIPOSEGUIMIENTO.get_dict(), zero = 0, error_message = 'Selecciona'),
        notnull = False, unique = False,
        widget = stv_widget_combobox, label = 'Tipo de seguimiento',
        comment = 'Seleccionar como se le da seguimiento a este producto',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_PRODSERV.E_TIPOSEGUIMIENTO.get_dict())
        ),
    Field(
        'numeroserie', 'boolean', default = request.vars.get("numeroserie", 0),
        required = False,
        notnull = False,
        widget = stv_widget_inputCheckbox, label = T('Número de serie'),
        comment = "Seleccionar en caso de que este producto se maneje por número de serie.",
        writable = True, readable = True,
        represent = stv_represent_boolean
        ),
    Field(
        'preciotipocalculo', 'integer', default = TEMPRESA_PRODSERV.E_TIPOCALCULO.PRECIO_FIJO,
        required = True,
        requires = IS_IN_SET(TEMPRESA_PRODSERV.E_TIPOCALCULO.get_dict(), zero = 0, error_message = 'Selecciona'),
        notnull = False, unique = False,
        widget = stv_widget_combobox, label = 'Precio de venta (Tipo de calculo)',
        comment = 'Seleccionar como será el tipo de calculo de precio para el producto',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_PRODSERV.E_TIPOCALCULO.get_dict())
        ),
    Field(
        'preciofijo', 'decimal(14,4)', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputMoney, label = 'Precio Fijo', comment = "Precio mínimo del producto/servicio",
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_money(v, r, dbField = dbc01.tempresa_prodserv.preciofijo)
        ),

    Field(
        'porcentajeutilidad', 'decimal(4,2)', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputPercentage, label = 'Porcentaje de utilidad', comment = None,
        writable = True, readable = True,
        represent = stv_represent_percentage
        ),
    Field(
        'porcentajemaxdesc', 'decimal(4,2)', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputPercentage, label = 'Max. Descuento', comment = None,
        writable = True, readable = True,
        represent = stv_represent_percentage
        ),
    Field(
        'empresa_proveedor_id', 'integer', default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Proveedor precio oficial',
        comment = 'Proveedor para precios oficiales.',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_prodserv.empresa_proveedor_id)
        ),
    Field(
        'empresa_productoajuste_id', 'integer', default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda field, value: stv_widget_db_chosen_linked(
            field,
            value,
            1,
            'marca_id',
            URL(a = 'app_empresas', c = '120empresas', f = 'empresa_productosajustes_ver.json'),
            D_additionalAttributes = {
                'linkedTableFieldName': 'empresa_id'
                },
            ),
        label = 'Ajuste', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_prodserv.empresa_productoajuste_id)
        ),
    # >> Configuraciones especificas por cliente plugin: CASCOS
    Field(
        'empresa_prodserv_casco1a1_id', 'integer', default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1, D_additionalAttributes = Storage(vacio = 'No aplica')),
        label = 'Casco 1a1', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tempresa_prodserv.empresa_prodserv_casco1a1_id
            )
        ),
    Field(
        'excedente_preciomaximo', 'decimal(14,4)', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputMoney, label = '$ max. compra excedente',
        comment = 'Precio máximo de compra de cascos excedentes',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_money(v, r, dbField = dbc01.tempresa_prodserv.excedente_preciomaximo)
        ),
    Field(
        'excedente_preciopropuesto', 'decimal(14,4)', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputMoney, label = 'Precio propuesto excedente',
        comment = 'Precio mínimo y el propuesto de compra de cascos excedentes',
        writable = True, readable = True,
        represent = stv_represent_money
        ),
    # <<

    Field(
        'cvematerialpeligroso_id', 'integer', default = None,
        required = False, requires = IS_NULL_OR(
            IS_IN_DB(db01, 'tmaterialespeligrosos.id', db01.tmaterialespeligrosos._format)
            ),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_search(
            f, v,
            s_display = '%(nombre)s [%(c_materialpeligroso)s]',
            s_url = URL(
                a = 'app_generales',
                c = '005genericos_sat',
                f = 'materialespeligrosos_buscar'
                ),
            D_additionalAttributes = {'reference': db01.tmaterialespeligrosos.id}
            ),
        label = 'Cve. Material Peligroso (si aplica)',
        comment = 'En caso de cascos usados, usar M1719',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tempresa_prodserv.cvematerialpeligroso_id
            )
        ),
    tSignature_dbc01,
    format = '[%(codigo)s] %(descripcion)s',
    singular = 'Producto',
    plural = 'Productos',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )
TEMPRESA_PRODSERV.PAGO = _dbdefaults_genericos_sat_cache(
    dbRef = dbc01(dbc01.tempresa_prodserv.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id),
    dbTabla = dbc01.tempresa_prodserv,
    s_nombreCampo = 'descripcion',
    s_codigo = 'Pago',
    s_cacheIdAdicional = D_stvSiteHelper.dbconfigs.defaults.empresa_id
    )


db01.tpaises._referenced_by.append(dbc01.tempresas.pais_id)
db01.tpaises._references.append(dbc01.tempresas.pais_id)

db01.tmonedas._referenced_by.append(dbc01.tempresas.moneda_id)
db01.tmonedas._references.append(dbc01.tempresas.moneda_id)

db01.tmonedas._referenced_by.append(dbc01.tempresa_cuentasbancarias.moneda_id)
db01.tmonedas._references.append(dbc01.tempresa_cuentasbancarias.moneda_id)


dbc01.tempresas._referenced_by.append(db.tcuenta_aplicaciones.empresa_id)
dbc01.tempresas._references.append(db.tcuenta_aplicaciones.empresa_id)
""" Se relaciona la tabla de empresas con el campo de empresa_id en la detalle de la cuenta tcuenta_empresas """


class TEMPRESA_PRODSERV_ESQUEMASIMPTRAS:
    pass


dbc01.define_table(
    'tempresa_prodserv_esquemasimptras',
    Field(
        'empresa_prodserv_id', dbc01.tempresa_prodserv, default=None,
        required=True,
        ondelete='CASCADE', notnull=True, unique=False,
        label='Producto/Servicio', comment=None,
        writable=False, readable=False,
        represent=None
        ),
    Field(
        'esquemaimptras_id', dbc01.tempresa_esquemasimpuestostrasladados, default=None,
        required=False,
        ondelete='NO ACTION', notnull=False, unique=False,
        widget=lambda f, v: stv_widget_db_chosen(f, v, 1),
        label='Esquema de imp. tras.', comment='Esquema de impuestos trasladados',
        writable=True, readable=True,
        represent=lambda v, r: stv_represent_referencefield(v, r)
        ),    
    Field(
        'observacion', 'string', length=100, default=None,
        required=False,
        notnull=False, unique=False,
        widget=stv_widget_input, label='Observación', comment=None,
        writable=True, readable=True,
        represent=stv_represent_string
        ),    
    tSignature_dbc01,
    format='%(empresa_prodserv_id)s',
    singular='Esquema de Impuesto Trasladado',
    plural='Esquema de Impuestos Trasladados',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_PRODSERV_PRESENTACIONES:
    pass


dbc01.define_table(
    'tempresa_prodserv_presentaciones',
    Field(
        'empresa_prodserv_id', dbc01.tempresa_prodserv, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Producto/Servicio', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'numeroparte', 'string', length = 30, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Número de parte', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'observacion', 'string', length = 100, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Observación', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'marca_id', dbc01.tmarcas, default = None,
        required = True,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Fabricante', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r)
        ),
    Field(
        'codigobarrasexterno', 'string', length = 18, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Código de barras externo', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'codigobarrasinterno', 'string', length = 18, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Código de barras interno', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'unidad_id', 'integer', default = None,
        required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tunidades.id', '%(nombre)s [%(c_claveunidad)s]')),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_search(
            f, v,
            s_display = '%(nombre)s [%(c_claveunidad)s]',
            s_url = URL(a = 'app_generales', c = '005genericos_sat', f = 'unidades_buscar'),
            D_additionalAttributes = {'reference': db01.tunidades.id}
            ),
        label = 'Unidad', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_prodserv_presentaciones.unidad_id)
        ),
    Field(
        'contenido', 'integer', default = 1,
        required = True,
        notnull = False, unique = False,
        widget = stv_widget_inputInteger, label = 'Contenido', comment = None,
        writable = True, readable = True,
        represent = stv_represent_number
        ),
    tSignature_dbc01,
    format = '%(numeroparte)s',
    singular = 'Presentación',
    plural = 'Presentaciones',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_PRODSERV_TRATAMIENTOS:
    pass


dbc01.define_table(
    'tempresa_prodserv_tratamientos',
    Field(
        'empresa_prodserv_id', dbc01.tempresa_prodserv, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Producto/Servicio', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'almacenamiento', 'string', length = 100, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Almacenamiento', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'uso', 'string', length = 100, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Uso', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'transporte', 'string', length = 100, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Transporte', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'instalacion', 'string', length = 100, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Instalación', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    tSignature_dbc01,
    format = '%(id)s',
    singular = 'Tratamiento',
    plural = 'Tratamientos',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_PRODSERV_IMAGENES:
    pass


dbc01.define_table(
    'tempresa_prodserv_imagenes',
    Field(
        'empresa_prodserv_id', dbc01.tempresa_prodserv, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Producto/Servicio', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'imagennombrearchivo', 'string', length = 100, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Imagen del producto', comment = None,
        writable = False, readable = False,
        represent = stv_represent_string
        ),
    Field(
        'imagen', type = 'upload',
        required = False,
        requires = IS_LENGTH(minsize = 0, maxsize = 1572864, error_message = 'El archivo es demasiado grande.'),
        notnull = False,
        uploadfield = True, widget = lambda f, d, u = None: stv_widget_inputFile(
            f, d, URL(
                a = 'app_empresas', c = '120empresas', f = 'empresa_prodserv_imagenes',
                vars = D_stvFwkCfg.D_cfg_cuenta_para_links
                ), dbc01.tempresa_prodserv_imagenes.imagennombrearchivo
            ),
        label = 'Imagen', comment = None,
        writable = True, readable = True, authorize = lambda record: auth.is_logged_in(),
        autodelete = True, represent = lambda v, r: stv_represent_image(
            v, r, URL(
                a = 'app_empresas', c = '120empresas', f = 'empresa_prodserv_imagenes',
                vars = D_stvFwkCfg.D_cfg_cuenta_para_links
                )
            ),
        uploadfolder = os.path.join(request.folder, 'uploads', 'prodserv_imagenes'),
        uploadseparate = False, uploadfs = None
        ),
    Field(
        'orden', 'integer', default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputInteger, label = 'Order',
        comment = 'El número menor es el que tiene la prioridad más alta a la hora de mostrarse.',
        writable = True, readable = True,
        represent = None
        ),
    tSignature_dbc01,
    format = '%(id)s',
    singular = 'Imagen',
    plural = 'Imagenes',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_PRODSERV_CODIGOSPROVEEDORES:
    pass


dbc01.define_table(
    'tempresa_prodserv_codigosproveedores',
    Field(
        'empresa_prodserv_id', dbc01.tempresa_prodserv, default = None,
        required = True, requires = None,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Producto/Servicio', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'empresa_proveedor_id', 'integer', default = None,
        required = True,
        ondelete = 'NO ACTION', notnull = True, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Proveedor', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tempresa_prodserv_codigosproveedores.empresa_proveedor_id
            )
        ),
    Field(
        'codigo', 'string', length = 100, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Código del producto',
        comment = 'Código utilizado por el proveedor para este producto',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 100, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Desc. del Proveedor',
        comment = 'Descripción del producto definida por el proveedor',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'contenido', 'integer', default = 1,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputInteger, label = 'Contenido', comment = None,
        writable = True, readable = True,
        represent = stv_represent_number
        ),
    tSignature_dbc01,
    format = '%(id)s',
    singular = 'Código proveedor',
    plural = 'Códigos proveedores',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_EMPLEADOS:

    @classmethod
    def getRowFormated(cls, n_id):
        if n_id:
            if hasattr(dbc01.tempresa_empleados._format, '__call__'):
                # ...ejecuta la función y regresa el resultado.
                return dbc01.tempresa_empleados._format(dbc01.tempresa_empleados(n_id))
            else:
                # ...de lo contrario haz el formato.
                return dbc01.tempresa_empleados._format % dbc01.tempresa_empleados(n_id).as_dict()
        else:
            return 'Nada'
    pass


dbc01.define_table(
    'tempresa_empleados',
    Field(
        'empresa_id', dbc01.tempresas, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Empresa', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'apellidopaterno', 'string', length = 30, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Apellido paterno', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'apellidomaterno', 'string', length = 30, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Apellido materno', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'nombre', 'string', length = 30, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Nombre', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    tSignature_dbc01,
    format = '%(nombre)s %(apellidopaterno)s',
    singular = 'Empleado',
    plural = 'Empleados',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_EMPLEADO_CUENTAS:
    """ Definición de la tabla de tempresa_empleado_cuentas """

    @classmethod
    def CONFIGURAR_INTEGRIDAD_REFERENCIAL(cls):
        _dbTable = dbc01.tempresa_empleado_cuentas
        _dbTable.monedacontpaqi_id.type = 'reference tempresa_monedascontpaqi'
        _dbTable.monedacontpaqi_id.ondelete = 'NO ACTION'
        _dbTable.monedacontpaqi_id.requires = IS_IN_DB(
            dbc01(dbc01.tempresa_monedascontpaqi.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id), 
            'tempresa_monedascontpaqi.id', 
            dbc01.tempresa_monedascontpaqi._format
            )
        _dbTable.monedacontpaqi_id.widget = lambda f, v: stv_widget_db_chosen(f, v, 1)
        _dbTable.monedacontpaqi_id.represent = stv_represent_referencefield
        
        return  
        
    pass


dbc01.define_table(
    'tempresa_empleado_cuentas',
    Field(
        'empresa_empleado_id', dbc01.tempresa_empleados, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Empleado', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'empresa_bancocontpaqi_id', 'integer', default = None,
        required = True,
        ondelete = 'NO ACTION', notnull = True, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Banco', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tempresa_empleado_cuentas.empresa_bancocontpaqi_id
            )
        ),
    Field(
        'numerocuenta', 'string', length = 30, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Número de cuenta', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'clabe', 'string', length = 18, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'CLABE interbancaria', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'numerotarjeta', 'integer', length = 4, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Número de tarjeta', comment = "Últimos 4 digitos de la tarjeta.",
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'monedacontpaqi_id', 'integer', default = D_stvSiteHelper.dbconfigs.defaults.moneda_contpaqi_id,
        required = True,
        ondelete = 'NO ACTION', notnull = True, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Moneda CONTPAQi', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_empleado_cuentas.monedacontpaqi_id)
        ),
    tSignature_dbc01,
    format = lambda r:
    (TEMPRESA_BANCOSCONTPAQI.GETROWFORMATED(
        r.empresa_bancocontpaqi_id
        ) + " Número Cuenta: " + r.numerocuenta + " Moneda: " + TEMPRESA_MONEDASCONTPAQI.GETROWFORMATED(
        r.monedacontpaqi_id
        ))
    if r.numerocuenta
    else
    (TEMPRESA_BANCOSCONTPAQI.GETROWFORMATED(
        r.empresa_bancocontpaqi_id
        ) + " CLABE: " + r.clabe + " Moneda: " + TEMPRESA_MONEDASCONTPAQI.GETROWFORMATED(r.monedacontpaqi_id)),
    singular = 'Cuenta bancaria',
    plural = 'Cuentas bancarias',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_RECURSOSCOMPROBAR:
    """ Definición de la tabla de tempresa_recursoscomprobar """

    class ESTATUS:
        PENDIENTE = 1
        COMPROBADO = 2
        
        @classmethod
        def get_dict(cls):
            return {
                cls.PENDIENTE: 'Pendiente',
                cls.COMPROBADO: 'Comprobado',
                }

    @classmethod
    def CONFIGURAR_INTEGRIDAD_REFERENCIAL(cls):
        _dbTable = dbc01.tempresa_recursoscomprobar
        _dbTable.monedacontpaqi_id.type = 'reference tempresa_monedascontpaqi'
        _dbTable.monedacontpaqi_id.ondelete = 'NO ACTION'
        _dbTable.monedacontpaqi_id.requires = IS_IN_DB(
            dbc01(dbc01.tempresa_monedascontpaqi.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id), 
            'tempresa_monedascontpaqi.id', 
            dbc01.tempresa_monedascontpaqi._format
            )
        _dbTable.monedacontpaqi_id.widget = lambda f, v: stv_widget_db_chosen(f, v, 1)
        _dbTable.monedacontpaqi_id.represent = stv_represent_referencefield
        
        return  
                
    pass


dbc01.define_table(
    'tempresa_recursoscomprobar',
    Field(
        'empresa_id', dbc01.tempresas, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Empresa', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'empresa_empleado_id', 'integer', default = None,
        required = True, requires = IS_IN_DB(dbc01, 'tempresa_empleados.id', dbc01.tempresa_empleados._format),
        ondelete = 'NO ACTION', notnull = True, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Empleado', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tempresa_recursoscomprobar.empresa_empleado_id
            )
        ),
    Field(
        'fecha', 'date', default = request.browsernow,
        required = True, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = True,
        widget = stv_widget_inputDate, label = 'Fecha', comment = None,
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'concepto', 'string', length = 500, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Concepto', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'importe', 'decimal(14,4)', length = 15, default = 0,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_inputMoney, label = 'Importe', comment = None,
        writable = True, readable = True,
        represent = stv_represent_money_ifnotzero
        ),
    Field(
        'estatus', 'integer', default = TEMPRESA_RECURSOSCOMPROBAR.ESTATUS.PENDIENTE,
        required = True,
        requires = IS_IN_SET(TEMPRESA_RECURSOSCOMPROBAR.ESTATUS.get_dict(), zero = 0, error_message = 'Selecciona'),
        notnull = True, unique = False,
        widget = stv_widget_combobox, label = 'Estatus', comment = '',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_RECURSOSCOMPROBAR.ESTATUS.get_dict())
        ),
    Field(
        'formapago_id', 'integer', default = TFORMASPAGO.EFECTIVO,
        required = True, requires = IS_NULL_OR(IS_IN_DB(db01, 'tformaspago.id', db01.tformaspago._format)),
        ondelete = 'NO ACTION', notnull = True, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Forma entrega', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tcfdi_complementopagos.formapago_id)
        ),
    Field(
        'cheque', 'string', length = 10, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Cheque', comment = 'No. de cheque',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'empresa_cuentabancaria_id', dbc01.tempresa_cuentasbancarias, default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Cuenta bancaria',
        comment = 'Cuenta de donde se realiza el depósito',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r)
        ),
    Field(
        'referencia', 'string', length = 20, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Referencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'claverastero', 'string', length = 10, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Clave Rastreo', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'fechadeposito', 'date', default = None,
        required = False, requires = IS_NULL_OR(IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE)),
        notnull = False, unique = False,
        widget = stv_widget_inputDate, label = 'Fecha de depósito',
        comment = 'Fecha en la que fue depositado el dinero.',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'empresa_plaza_sucursal_cajachica_id', 'integer', default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Caja chica', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tempresa_recursoscomprobar.empresa_plaza_sucursal_cajachica_id
            )
        ),
    Field(
        'empresa_empleado_cuenta_id', dbc01.tempresa_empleado_cuentas, default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda field, value: stv_widget_db_chosen_linked(
            field,
            value,
            1,
            'empresa_empleado_id',
            URL(a = 'app_empresas', c = '120empresas', f = 'empresa_empleado_cuentas_ver.json'),
            ),
        label = 'Cuentas del empleado', comment = None,
        writable = True, readable = True,
        represent = stv_represent_referencefield
        ),
    Field(
        'monedacontpaqi_id', 'integer', default = D_stvSiteHelper.dbconfigs.defaults.moneda_contpaqi_id,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Moneda CONTPAQi', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_recursoscomprobar.monedacontpaqi_id)
        ),
    tSignature_dbc01,
    format = '%(concepto)s',
    singular = 'Recurso por cobrar',
    plural = 'Recursos por cobrar',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_EMPLEADO_GUIASCONTABLES:
    
    @classmethod
    def CONFIGURAR_INTEGRIDAD_REFERENCIAL(cls):
        _dbTable = dbc01.tempresa_empleado_guiascontables
        _dbTable.monedacontpaqi_id.type = 'reference tempresa_monedascontpaqi'
        _dbTable.monedacontpaqi_id.ondelete = 'NO ACTION'
        _dbTable.monedacontpaqi_id.requires = IS_IN_DB(
            dbc01(dbc01.tempresa_monedascontpaqi.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id), 
            'tempresa_monedascontpaqi.id', 
            dbc01.tempresa_monedascontpaqi._format
            )
        _dbTable.monedacontpaqi_id.widget = lambda f, v: stv_widget_db_chosen(f, v, 1)
        _dbTable.monedacontpaqi_id.represent = stv_represent_referencefield
        
        return  
        
    pass


dbc01.define_table(
    'tempresa_empleado_guiascontables',
    Field(
        'empresa_empleado_id', dbc01.tempresa_empleados, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Empleado', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'ejercicio', 'integer', default = None,
        required = True, requires = IS_IN_SET(
            VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id), zero = 0,
            error_message = 'Selecciona'
            ),
        notnull = False, unique = False,
        widget = stv_widget_combobox,
        label = 'Ejercicio inicial', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(
            v, r, None, VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id)
            )
        ),
    Field(
        'cuentacontable', 'string', length = 30, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Cuenta contable', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),

    Field(
        'monedacontpaqi_id', 'integer', default = D_stvSiteHelper.dbconfigs.defaults.moneda_contpaqi_id,
        required = True,
        ondelete = 'NO ACTION', notnull = True, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Moneda CONTPAQi', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r, dbc01.tempresa_plaza_guiasclientes.monedacontpaqi_id
            )
        ),

    Field(
        'cuentacontable_complemento', 'string', length = 30, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Cuenta complemento', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),

    tSignature_dbc01,
    format = '%(ejercicio)s',
    singular = 'Guía contable',
    plural = 'Guías contables',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_FONDOSFIJOSCAJA:
    """ Definición de la tabla de tempresa_fondosfijoscaja """

    @classmethod
    def CONFIGURAR_INTEGRIDAD_REFERENCIAL(cls):
        _dbTable = dbc01.tempresa_fondosfijoscaja
        _dbTable.monedacontpaqi_id.type = 'reference tempresa_monedascontpaqi'
        _dbTable.monedacontpaqi_id.ondelete = 'NO ACTION'
        _dbTable.monedacontpaqi_id.requires = IS_IN_DB(
            dbc01(dbc01.tempresa_monedascontpaqi.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id), 
            'tempresa_monedascontpaqi.id', 
            dbc01.tempresa_monedascontpaqi._format
            )
        _dbTable.monedacontpaqi_id.widget = lambda f, v: stv_widget_db_chosen(f, v, 1)
        _dbTable.monedacontpaqi_id.represent = stv_represent_referencefield
        
        return  
        
    pass


dbc01.define_table(
    'tempresa_fondosfijoscaja',
    Field(
        'empresa_id', dbc01.tempresas, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Empresa', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'ejercicio', 'integer', default = None,
        required = True, requires = IS_IN_SET(
            VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id), zero = 0,
            error_message = 'Selecciona'
            ),
        notnull = False, unique = False,
        widget = stv_widget_combobox,
        label = 'Ejercicio inicial', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(
            v, r, None, VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id)
            )
        ),
    Field(
        'empresa_empleado_id', 'integer', default = None,
        required = False, requires = IS_IN_DB(dbc01, 'tempresa_empleados.id', dbc01.tempresa_empleados._format),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Empleado', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_fondosfijoscaja.empresa_empleado_id)
        ),
    Field(
        'monedacontpaqi_id', 'integer', default = D_stvSiteHelper.dbconfigs.defaults.moneda_contpaqi_id,
        required = True,
        ondelete = 'NO ACTION', notnull = True, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Moneda', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_fondosfijoscaja.monedacontpaqi_id)
        ),
    Field(
        'importe', 'decimal(14,4)', length = 15, default = 0,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_inputMoney, label = 'Importe', comment = None,
        writable = True, readable = True,
        represent = stv_represent_money_ifnotzero
        ),
    Field(
        'cuentacontable', 'string', length = 30, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Cuenta contable', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'cuentacontable_complementaria', 'string', length = 30, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Cuenta contable complementaria', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    tSignature_dbc01,
    format = '%(ejercicio)s',
    singular = 'Fondo fijo de caja',
    plural = 'Fondos fijos de caja',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )
TEMPRESA_PRODSERV.CONFIGURAR_INTEGRIDAD_REFERENCIAL()

""" Definición de la tabla de tempresa_grupos """


class TEMPRESA_GRUPOS(Table):
    S_NOMBRETABLA = 'tempresa_grupos'
    L_DBCAMPOS = [
        Field(
            'empresa_id', dbc01.tempresas, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'Empresa', comment = None,
            writable = False, readable = False,
            represent = None
            ),
        Field(
            'grupodescripcion', 'string', length = 30, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Descripción del grupo', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'cliente', 'boolean', default = request.vars.get("cliente", False),
            required = False,
            notnull = False,
            widget = stv_widget_inputCheckbox, label = 'Es cliente', comment = None,
            writable = True, readable = True,
            represent = stv_represent_boolean
            ),
        Field(
            'proveedor', 'boolean', default = request.vars.get("proveedor", False),
            required = False,
            notnull = False,
            widget = stv_widget_inputCheckbox, label = 'Es proveedor', comment = None,
            writable = True, readable = True,
            represent = stv_represent_boolean
            ),
        tSignature_dbc01,
        ]

    pass  # TEMPRESA_GRUPOS


dbc01.define_table(
    TEMPRESA_GRUPOS.S_NOMBRETABLA, *TEMPRESA_GRUPOS.L_DBCAMPOS,
    format = '%(grupodescripcion)s',
    singular = 'Grupo',
    plural = 'Grupos',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TEMPRESA_GRUPOS
    )


class TEMPRESA_SERIES(Table):
    """ Definición de la tabla de empresas_series """

    S_NOMBRETABLA = 'tempresa_series'

    L_DBCAMPOS = [
        Field(
            'empresa_id', dbc01.tempresas, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'Empresa', comment = None,
            writable = False, readable = False,
            represent = None
            ),
        Field(
            'serie', 'string', length = 25, default = None,
            required = True,
            notnull = True, unique = False,
            widget = stv_widget_input, label = 'Serie', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'tipocomprobante_id', 'integer', default = None,
            required = True, requires = IS_IN_DB(db01, 'ttiposcomprobantes.id', db01.ttiposcomprobantes._format),
            ondelete = 'NO ACTION', notnull = True, unique = False,
            widget = stv_widget_db_combobox, label = T('Tipo Comprobante'), comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_series.tipocomprobante_id)
            ),
        Field(
            'folio', 'decimal(40,0)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Ultimo Folio Usado', comment = None,
            writable = False, readable = True,
            represent = stv_represent_number
            ),
        tSignature_dbc01,
        ]

    pass  # TEMPRESA_SERIES


dbc01.define_table(
    TEMPRESA_SERIES.S_NOMBRETABLA, *TEMPRESA_SERIES.L_DBCAMPOS,
    format='%(serie)s',
    singular='Serie',
    plural='Series',
    migrate=D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TEMPRESA_SERIES
    )
