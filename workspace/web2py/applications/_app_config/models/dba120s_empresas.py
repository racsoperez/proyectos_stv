# -*- coding: utf-8 -*-

class TEMPRESA_PLAZA_GUIASCLIENTES():
    class E_TIPOCLIENTE():
        CLIENTE_NACIONAL = 0
        CLIENTE_EXTRANJERO = 1
        CLIENTE_NACIONAL_PARTE_RELACIONADA = 2
        CLIENTE_EXTRANJERO_PARTE_RELACIONADA = 3
        
        @classmethod
        def get_dict(cls):
            return {
                    cls.CLIENTE_NACIONAL: ('Cliente nacional'),
                    cls.CLIENTE_EXTRANJERO: ('Cliente extranjero'),
                    cls.CLIENTE_NACIONAL_PARTE_RELACIONADA: ('Cliente nacional parte relacionada'),
                    cls.CLIENTE_EXTRANJERO_PARTE_RELACIONADA: ('Cliente extranjero parte relacionada'),
                    }

    @classmethod
    def CONFIGURAR_INTEGRIDAD_REFERENCIAL(cls):
        _dbTable = dbc01.tempresa_plaza_guiasclientes
        _dbTable.monedacontpaqi_id.type = 'reference tempresa_monedascontpaqi'
        _dbTable.monedacontpaqi_id.ondelete = 'NO ACTION'
        _dbTable.monedacontpaqi_id.requires = IS_IN_DB(
            dbc01(dbc01.tempresa_monedascontpaqi.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id), 
            'tempresa_monedascontpaqi.id', 
            dbc01.tempresa_monedascontpaqi._format
            )
        _dbTable.monedacontpaqi_id.widget = lambda f, v: stv_widget_db_chosen(f, v, 1)
        _dbTable.monedacontpaqi_id.represent = stv_represent_referencefield
        
        return    

    pass
dbc01.define_table(
    'tempresa_plaza_guiasclientes',
     Field('empresa_plaza_id', dbc01.tempresa_plazas, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Plaza', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('ejercicio', 'integer', default = None,
      required = True, requires = IS_IN_SET(VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id), zero = 0, error_message = ('Selecciona')),
      notnull = False, unique = False,
      widget = stv_widget_combobox, 
      label = ('Ejercicio inicial'), comment = None,
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id))),
    Field('moneda_id', 'integer', default=D_stvSiteHelper.dbconfigs.defaults.moneda_id,
      required=True, requires = IS_IN_DB(db01, 'tmonedas.id', db01.tmonedas._format), 
      ondelete='NO ACTION', notnull=True, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label=('Moneda'), comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_plaza_guiasclientes.moneda_id)),
    
    #Este será el campo que se utilice en vez de moneda_id
    Field('monedacontpaqi_id', 'integer', default=D_stvSiteHelper.dbconfigs.defaults.moneda_contpaqi_id,
      required=True,
      ondelete='NO ACTION', notnull=True, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label=('Moneda CONTPAQi'), comment=None,
      writable=True, readable=True,
      represent=lambda v, r:  stv_represent_referencefield(v, r, dbc01.tempresa_plaza_guiasclientes.monedacontpaqi_id)),  

    
    Field('tipocliente', 'integer', default = 0,
      required = False, requires=IS_IN_SET(TEMPRESA_PLAZA_GUIASCLIENTES.E_TIPOCLIENTE.get_dict(), zero=0, error_message=('Selecciona')),
      notnull = False, unique = False,
      widget = stv_widget_combobox, label = ('Tipo de Cliente'), comment = '',
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_PLAZA_GUIASCLIENTES.E_TIPOCLIENTE.get_dict())),
    Field('cuenta_cliente', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label=('Cuenta Cliente'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuenta_cliente_complemento', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label=('Cuenta Cliente Complemento'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuenta_anticipo', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label=('Cuenta Anticipo'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuenta_anticipo_complemento', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label=('Cuenta Anticipo Complemento'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    tSignature_dbc01,
    format='%(cuenta_cliente)s',
    singular='Guía Cliente',
    plural='Guías Clientes',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

class TEMPRESA_PLAZA_GUIASPROVEEDORES():
    class TIPOPROVEEDOR():
        PROVEEDOR_NACIONAL = 0
        PROVEEDOR_EXTRANJERO = 1
        PROVEEDOR_NACIONAL_PARTE_RELACIONADA = 2
        PROVEEDOR_EXTRANJERO_PARTE_RELACIONADA = 3
        
        @classmethod
        def get_dict(cls):
            return {
                    cls.PROVEEDOR_NACIONAL: ('Proveedor nacional'),
                    cls.PROVEEDOR_EXTRANJERO: ('Proveedor extranjero'),
                    cls.PROVEEDOR_NACIONAL_PARTE_RELACIONADA: ('Proveedor nacional parte relacionada'),
                    cls.PROVEEDOR_EXTRANJERO_PARTE_RELACIONADA: ('Proveedor extranjero parte relacionada'),
                    }
 
    @classmethod
    def CONFIGURAR_INTEGRIDAD_REFERENCIAL(cls):
        _dbTable = dbc01.tempresa_plaza_guiasproveedores
        _dbTable.monedacontpaqi_id.type = 'reference tempresa_monedascontpaqi'
        _dbTable.monedacontpaqi_id.ondelete = 'NO ACTION'
        _dbTable.monedacontpaqi_id.requires = IS_IN_DB(
            dbc01(dbc01.tempresa_monedascontpaqi.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id), 
            'tempresa_monedascontpaqi.id', 
            dbc01.tempresa_monedascontpaqi._format
            )
        _dbTable.monedacontpaqi_id.widget = lambda f, v: stv_widget_db_chosen(f, v, 1)
        _dbTable.monedacontpaqi_id.represent = stv_represent_referencefield
        
        return  
    
    pass
dbc01.define_table(
    'tempresa_plaza_guiasproveedores',
     Field('empresa_plaza_id', dbc01.tempresa_plazas, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Plaza', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('ejercicio', 'integer', default = None,
      required = True, requires = IS_IN_SET(VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id), zero = 0, error_message = ('Selecciona')),
      notnull = False, unique = False,
      widget = stv_widget_combobox, 
      label = ('Ejercicio inicial'), comment = None,
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id))),
    
    Field('moneda_id', 'integer', default=D_stvSiteHelper.dbconfigs.defaults.moneda_id,
      required=True, requires = IS_IN_DB(db01, 'tmonedas.id', db01.tmonedas._format), 
      ondelete='NO ACTION', notnull=True, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label=('Moneda'), comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_plaza_guiasproveedores.moneda_id)),
    
    #Este será el campo que se utilice en vez de moneda_id
    Field('monedacontpaqi_id', 'integer', default=D_stvSiteHelper.dbconfigs.defaults.moneda_contpaqi_id,
      required=True,
      ondelete='NO ACTION', notnull=True, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label=('Moneda CONTPAQi'), comment=None,
      writable=True, readable=True,
      represent=lambda v, r:  stv_represent_referencefield(v, r, dbc01.tempresa_plaza_guiasproveedores.monedacontpaqi_id)),  
    
    Field('tipoproveedor', 'integer', default = 0,
      required = False, requires=IS_IN_SET(TEMPRESA_PLAZA_GUIASPROVEEDORES.TIPOPROVEEDOR.get_dict(), zero=0, error_message=('Selecciona')),
      notnull = False, unique = False,
      widget = stv_widget_combobox, label = ('Tipo de Proveedor'), comment = '',
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_PLAZA_GUIASPROVEEDORES.TIPOPROVEEDOR.get_dict())),
    Field('cuenta_proveedor', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label=('Cuenta Proveedor'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuenta_proveedor_complemento', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label=('Cuenta Proveedor Complemento'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuenta_anticipo', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label=('Cuenta Anticipo'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuenta_anticipo_complemento', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label=('Cuenta Anticipo Complemento'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    tSignature_dbc01,
    format='%(cuenta_proveedor)s',
    singular='Guía Proveedor',
    plural='Guías Proveedores',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

class TEMPRESA_PLAZA_SUCURSAL_GUIASCLIENTES():
    class E_TIPOCLIENTE():
        CLIENTE_NACIONAL = 0
        CLIENTE_EXTRANJERO = 1
        CLIENTE_NACIONAL_PARTE_RELACIONADA = 2
        CLIENTE_EXTRANJERO_PARTE_RELACIONADA = 3
        
        @classmethod
        def get_dict(cls):
            return {
                    cls.CLIENTE_NACIONAL: ('Cliente nacional'),
                    cls.CLIENTE_EXTRANJERO: ('Cliente extranjero'),
                    cls.CLIENTE_NACIONAL_PARTE_RELACIONADA: ('Cliente nacional parte relacionada'),
                    cls.CLIENTE_EXTRANJERO_PARTE_RELACIONADA: ('Cliente extranjero parte relacionada'),
                    }

    @classmethod
    def CONFIGURAR_INTEGRIDAD_REFERENCIAL(cls):
        _dbTable = dbc01.tempresa_plaza_sucursal_guiasclientes
        _dbTable.monedacontpaqi_id.type = 'reference tempresa_monedascontpaqi'
        _dbTable.monedacontpaqi_id.ondelete = 'NO ACTION'
        _dbTable.monedacontpaqi_id.requires = IS_IN_DB(
            dbc01(dbc01.tempresa_monedascontpaqi.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id), 
            'tempresa_monedascontpaqi.id', 
            dbc01.tempresa_monedascontpaqi._format
            )
        _dbTable.monedacontpaqi_id.widget = lambda f, v: stv_widget_db_chosen(f, v, 1)
        _dbTable.monedacontpaqi_id.represent = stv_represent_referencefield
        
        return  
    
    pass
dbc01.define_table(
    'tempresa_plaza_sucursal_guiasclientes',
     Field('empresa_plaza_sucursal_id', dbc01.tempresa_plaza_sucursales, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Plaza', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('moneda_id', 'integer', default=D_stvSiteHelper.dbconfigs.defaults.moneda_id,
      required=True, requires = IS_IN_DB(db01, 'tmonedas.id', db01.tmonedas._format), 
      ondelete='NO ACTION', notnull=True, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label=('Moneda'), comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_plaza_sucursal_guiasclientes.moneda_id)),
    
    #Este será el campo que se utilice en vez de moneda_id
    Field('monedacontpaqi_id', 'integer', default=D_stvSiteHelper.dbconfigs.defaults.moneda_contpaqi_id,
      required=True,
      ondelete='NO ACTION', notnull=True, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label=('Moneda CONTPAQi'), comment=None,
      writable=True, readable=True,
      represent=lambda v, r:  stv_represent_referencefield(v, r, dbc01.tempresa_plaza_sucursal_guiasclientes.monedacontpaqi_id)),  
    
    Field('ejercicio', 'integer', default = None,
      required = True, requires = IS_IN_SET(VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id), zero = 0, error_message = ('Selecciona')),
      notnull = False, unique = False,
      widget = stv_widget_combobox, 
      label = ('Ejercicio inicial'), comment = None,
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id))),
    Field('tipocliente', 'integer', default = 0,
      required = False, requires=IS_IN_SET(TEMPRESA_PLAZA_SUCURSAL_GUIASCLIENTES.E_TIPOCLIENTE.get_dict(), zero=0, error_message=('Selecciona')),
      notnull = False, unique = False,
      widget = stv_widget_combobox, label = ('Tipo de Cliente'), comment = '',
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_PLAZA_SUCURSAL_GUIASCLIENTES.E_TIPOCLIENTE.get_dict())),
    Field('cuenta_cliente', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label=('Cuenta Cliente'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuenta_cliente_complemento', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label=('Cuenta Cliente Complemento'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuenta_anticipo', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label=('Cuenta Anticipo'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuenta_anticipo_complemento', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label=('Cuenta Anticipo Complemento'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    tSignature_dbc01,
    format='%(cuenta_cliente)s',
    singular='Guía Cliente',
    plural='Guías Clientes',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_PLAZA_SUCURSAL_GUIASPROVEEDORES():
    class TIPOPROVEEDOR():
        PROVEEDOR_NACIONAL = 0
        PROVEEDOR_EXTRANJERO = 1
        PROVEEDOR_NACIONAL_PARTE_RELACIONADA = 2
        PROVEEDOR_EXTRANJERO_PARTE_RELACIONADA = 3
        
        @classmethod
        def get_dict(cls):
            return {
                cls.PROVEEDOR_NACIONAL: ('Proveedor nacional'),
                cls.PROVEEDOR_EXTRANJERO: ('Proveedor extranjero'),
                cls.PROVEEDOR_NACIONAL_PARTE_RELACIONADA: ('Proveedor nacional parte relacionada'),
                cls.PROVEEDOR_EXTRANJERO_PARTE_RELACIONADA: ('Proveedor extranjero parte relacionada'),
                }

    @classmethod
    def CONFIGURAR_INTEGRIDAD_REFERENCIAL(cls):
        _dbTable = dbc01.tempresa_plaza_sucursal_guiasproveedores
        _dbTable.monedacontpaqi_id.type = 'reference tempresa_monedascontpaqi'
        _dbTable.monedacontpaqi_id.ondelete = 'NO ACTION'
        _dbTable.monedacontpaqi_id.requires = IS_IN_DB(
            dbc01(dbc01.tempresa_monedascontpaqi.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id), 
            'tempresa_monedascontpaqi.id', 
            dbc01.tempresa_monedascontpaqi._format
            )
        _dbTable.monedacontpaqi_id.widget = lambda f, v: stv_widget_db_chosen(f, v, 1)
        _dbTable.monedacontpaqi_id.represent = stv_represent_referencefield
        
        return  
    
    pass
dbc01.define_table(
    'tempresa_plaza_sucursal_guiasproveedores',
     Field('empresa_plaza_sucursal_id', dbc01.tempresa_plaza_sucursales, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Plaza', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('moneda_id', 'integer', default=D_stvSiteHelper.dbconfigs.defaults.moneda_id,
      required=True, requires = IS_IN_DB(db01, 'tmonedas.id', db01.tmonedas._format), 
      ondelete='NO ACTION', notnull=True, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label=('Moneda'), comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_plaza_sucursal_guiasproveedores.moneda_id)),
    
    #Este será el campo que se utilice en vez de moneda_id
    Field('monedacontpaqi_id', 'integer', default=D_stvSiteHelper.dbconfigs.defaults.moneda_contpaqi_id,
      required=True,
      ondelete='NO ACTION', notnull=True, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label=('Moneda CONTPAQi'), comment=None,
      writable=True, readable=True,
      represent=lambda v, r:  stv_represent_referencefield(v, r, dbc01.tempresa_plaza_sucursal_guiasproveedores.monedacontpaqi_id)), 
    
    Field('ejercicio', 'integer', default = None,
      required = True, requires = IS_IN_SET(VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id), zero = 0, error_message = ('Selecciona')),
      notnull = False, unique = False,
      widget = stv_widget_combobox, 
      label = ('Ejercicio inicial'), comment = None,
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id))),
    Field('tipoproveedor', 'integer', default = 0,
      required = False, requires=IS_IN_SET(TEMPRESA_PLAZA_SUCURSAL_GUIASPROVEEDORES.TIPOPROVEEDOR.get_dict(), zero=0, error_message=('Selecciona')),
      notnull = False, unique = False,
      widget = stv_widget_combobox, label = ('Tipo de Proveedor'), comment = '',
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_PLAZA_SUCURSAL_GUIASPROVEEDORES.TIPOPROVEEDOR.get_dict())),
    Field('cuenta_proveedor', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label=('Cuenta Proveedor'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuenta_proveedor_complemento', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label=('Cuenta Proveedor Complemento'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuenta_anticipo', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label=('Cuenta Anticipo'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuenta_anticipo_complemento', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label=('Cuenta Anticipo Complemento'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    tSignature_dbc01,
    format='%(cuenta_proveedor)s',
    singular='Guía Proveedor',
    plural='Guías Proveedores',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

class TEMPRESA_CUENTABANCARIA_TERMINALES():
    pass
dbc01.define_table(
    'tempresa_cuentabancaria_terminales',
    Field('empresa_cuentabancaria_id', dbc01.tempresa_cuentasbancarias, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Empresa', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('terminal', 'string', length=30, default=None,
      required=True,
      notnull=True, unique=False,
      widget=stv_widget_input, label='Terminal', comment=('Código de la terminal asignado por la empresa'),
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('numeroserie', 'string', length=100, default=None,
      required=True,
      notnull=True, unique=False,
      widget=stv_widget_input, label='Número de Serie', comment=('Número de serie de la terminal'),
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('empresa_plaza_sucursal_id', dbc01.tempresa_plaza_sucursales, default=None,
      required=False,
      ondelete='NO ACTION', notnull=False, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label=('Sucursal'), comment=None,
      writable=True, readable=True,
      represent = stv_represent_referencefield),
    tSignature_dbc01,
    format=lambda r: (TEMPRESA_BANCOSCONTPAQI.GETROWFORMATED(r.empresa_cuentabancaria_id.empresa_bancocontpaqi_id) + " : " + r.empresa_cuentabancaria_id.cuenta + " : " + r.terminal),
    singular='Terminal',
    plural='Terminales',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

class TEMPRESA_CUENTABANCARIA_GUIAS():
    pass
dbc01.define_table(
    'tempresa_cuentabancaria_guias',
    Field('empresa_cuentabancaria_id', dbc01.tempresa_cuentasbancarias, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Empresa', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('ejercicio', 'integer', default = None,
      required = True, requires = IS_IN_SET(VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id), zero = 0, error_message = ('Selecciona')),
      notnull = False, unique = False,
      widget = stv_widget_combobox, 
      label = ('Ejercicio inicial'), comment = None,
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id))),
    Field('cuentacontable', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Cta. Contable', comment='Cuenta contable de la cuenta bancaria',
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuentacontable_provisionescomisiones', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Cta. Contable Provisiones', comment='Cuenta contable de provisones de comisiones',
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuentacontable_comisiones', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Cta. Contable Comisiones', comment='Cuenta contable de comisiones',
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuentacontable_complementaria', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Cta. Contable Complementaria', comment='Cuenta contable complementaria para su uso unicamente si la moneda de la cuenta, es diferente a la moneda de la definida en la empresa',
      writable=True, readable=True,
      represent=stv_represent_string),
    tSignature_dbc01,
    format=lambda r: (TEMPRESA_BANCOSCONTPAQI.GETROWFORMATED(r.empresa_cuentabancaria_id.empresa_bancocontpaqi_id) + " : " + r.empresa_cuentabancaria_id.cuenta + " : " + r.terminal),
    singular='Guía Contable',
    plural='Guías Contables',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_INGRESOSESTRUCTURA():
    pass
dbc01.define_table(
    'tempresa_ingresosestructura',
    Field('empresa_id', dbc01.tempresas, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Empresa', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('nombrecorto', 'string', length=25, default=None,
      required=True,
      notnull=True, unique=False,
      widget=stv_widget_input, label='Nombre Corto', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('padre_id', 'integer', default=0,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Estructura padre', comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield_tree(v, r, dbc01.tempresa_ingresosestructura.padre_id)),
    Field('orden', 'integer', default=0,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Orden', comment=None,
      writable=True, readable=True,
      represent=stv_represent_number),
    tSignature_dbc01,
    format='%(id)s: %(nombrecorto)s',
    singular='Estructura Ingreso',
    plural='Estructura de Ingresos',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

dbc01.tempresa_ingresosestructura.padre_id.type = 'reference tempresa_ingresosestructura'
dbc01.tempresa_ingresosestructura.padre_id.ondelete = 'NO ACTION'
dbc01.tempresa_ingresosestructura.padre_id.requires = IS_IN_DB(dbc01, 'tempresa_ingresosestructura.id','%(id)s: %(nombrecorto)s')
dbc01.tempresa_ingresosestructura.padre_id.widget = lambda f, v: stv_widget_db_chosen(f, v, 1)

# Se crea la referencia para su uso en los campos
dbc01.tempresa_ingresosestructura._referenced_by.append(dbc01.tempresa_ingresosestructura.padre_id)
dbc01.tempresa_ingresosestructura._references.append(dbc01.tempresa_ingresosestructura.padre_id)


class TEMPRESA_GASTOSESTRUCTURA():
    pass


dbc01.define_table(
    'tempresa_gastosestructura',
    Field('empresa_id', dbc01.tempresas, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Empresa', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('nombrecorto', 'string', length=25, default=None,
      required=True,
      notnull=True, unique=False,
      widget=stv_widget_input, label='Nombre Corto', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('padre_id', 'integer', default=0,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Estructura padre', comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield_tree(v, r, dbc01.tempresa_gastosestructura.padre_id)),
    Field('orden', 'integer', default=0,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Orden', comment=None,
      writable=True, readable=True,
      represent=stv_represent_number),
    tSignature_dbc01,
    format='%(id)s: %(nombrecorto)s',
    singular='Estructura de Gasto',
    plural='Estructura de Gastos',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

dbc01.tempresa_gastosestructura.padre_id.type = 'reference tempresa_gastosestructura'
dbc01.tempresa_gastosestructura.padre_id.ondelete = 'NO ACTION'
dbc01.tempresa_gastosestructura.padre_id.requires = IS_IN_DB(dbc01, 'tempresa_gastosestructura.id','%(id)s: %(nombrecorto)s')
dbc01.tempresa_gastosestructura.padre_id.widget = lambda f, v: stv_widget_db_chosen(f, v, 1)

# Se crea la referencia para su uso en los campos
dbc01.tempresa_gastosestructura._referenced_by.append(dbc01.tempresa_gastosestructura.padre_id)
dbc01.tempresa_gastosestructura._references.append(dbc01.tempresa_gastosestructura.padre_id)

# Alias para manejo de padre a la tabla
dbt_e_ge_p = dbc01.tempresa_gastosestructura.with_alias('te_ge_p')


class TEMPRESA_CENTROCOSTOSESTRUCTURA():
    pass
dbc01.define_table(
    'tempresa_centrocostosestructura',
    Field('empresa_id', dbc01.tempresas, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Empresa', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('nombrecorto', 'string', length=25, default=None,
      required=True,
      notnull=True, unique=False,
      widget=stv_widget_input, label='Nombre Corto', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('padre_id', 'integer', default=0,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Estructura padre', comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield_tree(v, r, dbc01.tempresa_centrocostosestructura.padre_id)),
    Field('orden', 'integer', default=0,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Orden', comment=None,
      writable=True, readable=True,
      represent=stv_represent_number),
    tSignature_dbc01,
    format='%(id)s: %(nombrecorto)s',
    singular='Estructura de Centro de Costo',
    plural='Estructura de Centro de Costos',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

dbc01.tempresa_centrocostosestructura.padre_id.type = 'reference tempresa_centrocostosestructura'
dbc01.tempresa_centrocostosestructura.padre_id.ondelete = 'NO ACTION'
dbc01.tempresa_centrocostosestructura.padre_id.requires = IS_IN_DB(dbc01, 'tempresa_centrocostosestructura.id','%(id)s: %(nombrecorto)s')
dbc01.tempresa_centrocostosestructura.padre_id.widget = lambda f, v: stv_widget_db_chosen(f, v, 1)

# Se crea la referencia para su uso en los campos
dbc01.tempresa_centrocostosestructura._referenced_by.append(dbc01.tempresa_centrocostosestructura.padre_id)
dbc01.tempresa_centrocostosestructura._references.append(dbc01.tempresa_centrocostosestructura.padre_id)

# Alias para manejo de padre a la tabla
dbt_e_ge_p = dbc01.tempresa_centrocostosestructura.with_alias('te_cce_p')

class TEMPRESA_PRODSERV_INGRESOSESTRUCTURAS():
    
    pass
# No se puede repetir prodserv y ejercicio
dbc01.define_table(
    'tempresa_prodserv_ingresosestructuras',
    Field('empresa_prodserv_id', dbc01.tempresa_prodserv, default=None,
        required=True,
        ondelete='CASCADE', notnull=True, unique=False,
        label='Producto/Servicio', comment=None,
        writable=False, readable=False,
        represent=None),
    Field('empresa_ingresoestructura_id', dbc01.tempresa_ingresosestructura, default=None,
        required=False,
        ondelete='NO ACTION', notnull=False, unique=False,
        widget=lambda f, v: stv_widget_db_chosen_tree(f, v, 1, D_additionalAttributes={
            'dbTable_tree': dbc01.tempresa_ingresosestructura,
            'dbField_order': dbc01.tempresa_ingresosestructura.orden,
            'dbField_tree_groupby': dbc01.tempresa_ingresosestructura.padre_id,
            'class_dbTree': DB_Tree,
            'b_onlyLastNodes' : True,
            'b_includeId' : True
            }), 
        label=('Ingresos Estructura'), comment=None,
        writable=True, readable=True,
        represent=lambda v, r: stv_represent_referencefield_tree(v, r, dbc01.tempresa_ingresosestructura.padre_id, dbc01.tempresa_prodserv_ingresosestructuras.empresa_ingresoestructura_id)),
    Field('ejercicio', 'integer', default = None,
        required = True, requires = IS_IN_SET(VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id), zero = 0, error_message = ('Selecciona')),
        notnull = False, unique = False,
        widget = stv_widget_combobox, 
        label = ('Ejercicio inicial'), comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id))),
    tSignature_dbc01,
    format='%(empresa_ingresoestructura_id)s',
    singular='Producto',
    plural='Productos',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_CLASIFICACIONESPROVEEDORES():
    pass
dbc01.define_table(
    'tempresa_clasificacionesproveedores',
    Field('empresa_id', dbc01.tempresas, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Empresa', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('clasificacionproveedor', 'string', length=30, default=None,
      required=True,
      notnull=True, unique=False,
      widget=stv_widget_input, label=('Clasificación del proveedor'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string), 
    tSignature_dbc01,
    format='%(clasificacionproveedor)s',
    singular='Clasificación proveedor',
    plural='Clasificaciones proveedores',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

class TEMPRESA_CLASIFICACIONESCLIENTES():
    pass
dbc01.define_table(
    'tempresa_clasificacionesclientes',
    Field('empresa_id', dbc01.tempresas, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Empresa', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('clasificacioncliente', 'string', length=30, default=None,
      required=True,
      notnull=True, unique=False,
      widget=stv_widget_input, label=('Clasificación del cliente'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string), 
    tSignature_dbc01,
    format='%(clasificacioncliente)s',
    singular='Clasificación cliente',
    plural='Clasificaciones clientes',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )


""" Definición de la tabla de tempresa_productosajustes """
class TEMPRESA_PRODUCTOSAJUSTES():
    pass
dbc01.define_table(
    'tempresa_productosajustes',
    Field('empresa_id', dbc01.tempresas, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Empresa', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('marca_id', dbc01.tmarcas, default=None,
      required=False,
      ondelete='NO ACTION', notnull=False, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label=('Marca'), comment=None,
      writable=True, readable=True,
    represent= stv_represent_referencefield),
    Field('tipoajuste', 'string', length=50, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label=('Tipo de ajuste'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    tSignature_dbc01,
    format='%(tipoajuste)s',
    singular='Producto ajuste',
    plural='Productos ajustes',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

# Se crea la referencia para su uso en los campos
dbc01.tempresa_productosajustes._referenced_by.append(dbc01.tempresa_prodserv.empresa_productoajuste_id)
dbc01.tempresa_productosajustes._references.append(dbc01.tempresa_prodserv.empresa_productoajuste_id)
dbc01.tempresa_prodserv.empresa_productoajuste_id.requires = IS_IN_DB(dbc01, 'tempresa_productosajustes.id', dbc01.tempresa_productosajustes._format)


""" Definición de la tabla de tempresa_productosajustes """
class TEMPRESA_PRODUCTOAJUSTE_PORCENTAJES():
    pass
dbc01.define_table(
    'tempresa_productoajuste_porcentajes',
    Field('empresa_productoajuste_id', dbc01.tempresa_productosajustes, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Producto ajuste', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('mesuso_inicial', 'integer', default = 0,
      required = False,
      notnull = False, unique = False,
      widget = stv_widget_inputInteger, 
      label = ('Meses de uso inicial'), comment = None,
      writable = True, readable = True,
      represent = stv_represent_number),
    Field('mesuso_final', 'integer', default = 0,
      required = False,
      notnull = False, unique = False,
      widget = stv_widget_inputInteger, 
      label = ('Meses de uso final'), comment = None,
      writable = True, readable = True,
      represent = stv_represent_number),
    Field('ajusteporcentaje', 'decimal(4,2)', default=0,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_inputPercentage, label='Porcentaje de ajuste', comment=None,
      writable=True, readable=True,
      represent=stv_represent_percentage),
    tSignature_dbc01,
    format='%(tipoajuste)s',
    singular='Porcentaje',
    plural='Porcentajes',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )