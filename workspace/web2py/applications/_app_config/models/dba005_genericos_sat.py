# -*- coding: utf-8 -*-

""" Este archivo contiene las definiciónes genéricas del SAT que se comparten entre todas las cuentas.

La base de datos puede ser accedida por todas las cuentas.
"""


def _dbdefaults_genericos_sat_cache(
        dbTabla, s_nombreCampo, s_codigo, dbRef = None, s_cacheIdAdicional = None
        ):

    def buscar():
        """ Identifica el id buscando s_codigo en el s_nombreCampo de la dbTabla.

        @return:
        @rtype:
        """
        _dbRef = dbRef if dbRef else db01
        _dbRows = _dbRef(dbTabla[s_nombreCampo] == s_codigo).select(dbTabla.id)
        if _dbRows:
            _return_id = _dbRows.last().id
        else:
            _return_id = None
        return _return_id

    _s_idCache = "%s_%s_%s" % (dbTabla._tablename, str(s_cacheIdAdicional or ""), s_codigo)
    return cache.ram(_s_idCache, lambda: buscar(), time_expire = 86400)


class TPAISES:
    """ Definición de los formularios disponibles en el sistema """

    class VALIDACION_IDENTIDADTRIBUTARIA:
        """ Se definen las opciónes del campo """
        NADA = 0
        LISTA_DEL_SAT = 1
        
        @classmethod
        def get_dict(cls):
            return {
                cls.LISTA_DEL_SAT: 'Lista del SAT',
                }

    class AGRUPACIONES:
        """ Se definen las opciónes del campo """
        NADA = 0
        UNION_EUROPEA = 1
        TLCAN = 2
        
        @classmethod
        def get_dict(cls):
            return {
                cls.UNION_EUROPEA: 'Unión Europea',
                cls.TLCAN: 'TLCAN',
                }
    pass


db01.define_table(
    'tpaises',
    Field(
        'c_pais', 'string', length = 3, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'País', comment = 'Abreviatura SAT del pais',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 50, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Descripción', comment = 'Nombre del Pais',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'lada', 'string', length = 5, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Lada', comment = 'Lada para marcación por teléfono',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'formato_cp', 'string', length = 50, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Formato Código Postal',
        comment = 'Formato usado en código postal, usar expresiones regulares',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'formato_identidadtributaria', 'string', length = 100, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Formato Reg. Identidad Tributaria',
        comment = 'Formato usado para el registro en la identidad tributaria del país, usar expresiones regulares',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'validacion_identidadtributaria', 'integer', default = 0,
        required = False, requires = IS_IN_SET(
            TPAISES().VALIDACION_IDENTIDADTRIBUTARIA().get_dict(), zero = 0, error_message = 'Selecciona'
            ),
        notnull = False, unique = False,
        widget = stv_widget_combobox, label = 'Validación Reg. Identidad Tributaria',
        comment = 'Entidad en el país utilizada para validar el registro de identidad tributaria',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TPAISES().VALIDACION_IDENTIDADTRIBUTARIA().get_dict())
        ),
    Field(
        'agrupaciones', 'string', default = None,
        required = False,
        requires = IS_IN_SET(TPAISES().AGRUPACIONES().get_dict(), zero = 0, error_message = 'Selecciona'),
        notnull = False, unique = False,
        widget = lambda dbField, x_value: stv_widget_chosen(dbField, x_value, 20),
        label = 'Agrupaciones', comment = 'Agrupaciones o tratados en los que se encuentra el país',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TPAISES().AGRUPACIONES().get_dict())
        ),
    tSignature_db01,
    format = '%(descripcion)s [%(c_pais)s]',
    singular = 'País',
    plural = 'Paises',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )

TPAISES.MEX = _dbdefaults_genericos_sat_cache(db01.tpaises, 'c_pais', 'MEX')


class TPAIS_ESTADOS:
    pass


db01.define_table(
    'tpais_estados',
    Field(
        'pais_id', db01.tpaises, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'País', comment = None,
        writable = False, readable = False,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 100, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Descripción', comment = 'Nombre del Estado',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'c_estado', 'string', length = 3, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Estado', comment = 'Abreviatura SAT del estado',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'lada', 'string', length = 5, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Lada', comment = 'Lada para marcación por teléfono',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    tSignature_db01,
    format = '%(descripcion)s [%(c_estado)s]',
    singular = 'Estado',
    plural = 'Estados',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )
TPAIS_ESTADOS.SIN = _dbdefaults_genericos_sat_cache(db01.tpais_estados, 'c_estado', 'SIN')


class TPAIS_ESTADO_MUNICIPIOS:

    @classmethod
    def IMPORTAR_EXCEL(
            cls,
            s_pathArchivo,
            s_nombreHoja = None,
            **D_otros
            ):

        def convertir_estado_id(
                x_valor,
                D_columna,
                D_rowExcel
                ):
            _ = D_columna
            _ = D_rowExcel
            _n_id = _dbdefaults_genericos_sat_cache(db01.tpais_estados, 'c_estado', x_valor)
            return _n_id

        _ = D_otros
        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTabla = db01.tpais_estado_municipios
        _O_importar = IMPORTAR_EXCEL(
            dbTabla = _dbTabla,
            b_insertar_noRelacionados = True,
            b_ignorar_noRelacionados = False,
            b_insertarTodos = False,
            fnIdentificarHeader = None,
            alValidar = None,
            alAceptar = None
            )
        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = None,
            dbCampo = _dbTabla.c_municipio,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "c_Municipio",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = None,
            dbCampo = _dbTabla.descripcion,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Descripción",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = None,
            dbCampo = _dbTabla.pais_estado_id,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "c_Estado",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = convertir_estado_id
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.c_estado,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "c_Estado",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        # _O_importar.agregar_campo_columna_importar(
        #     n_id_columna = None,
        #     dbCampo = _dbTabla.fecha_inicovigencia,
        #     E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
        #     s_nombreColumna = "Fecha de inicio de vigencia",
        #     n_columna = None,
        #     x_valorFijo = None,
        #     fnConversion = None
        #     )
        #
        # _O_importar.agregar_campo_columna_importar(
        #     n_id_columna = None,
        #     dbCampo = _dbTabla.fecha_finvigencia,
        #     E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
        #     s_nombreColumna = "Fecha de fin de vigencia",
        #     n_columna = None,
        #     x_valorFijo = None,
        #     fnConversion = None
        #     )

        _D_result = _O_importar.importar(
            s_pathArchivo = s_pathArchivo,
            s_nombreHoja = s_nombreHoja or 'c_Municipio'
            )

        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

        return _D_return

    pass


db01.define_table(
    'tpais_estado_municipios',
    Field(
        'pais_estado_id', db01.tpais_estados, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Estado', comment = None,
        writable = False, readable = False,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 100, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Descripción', comment = 'Nombre del Municipio',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'c_municipio', 'string', length = 3, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Municipio', comment = 'Código SAT del municipio',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'c_estado', 'string', length = 5, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Estado', comment = 'Código SAT del estado',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    tSignature_db01,
    format = '%(descripcion)s',
    singular = 'Municipio',
    plural = 'Municipios',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TPAIS_ESTADO_LOCALIDADES:

    @classmethod
    def IMPORTAR_EXCEL(
            cls,
            s_pathArchivo,
            s_nombreHoja = None,
            **D_otros
            ):

        def convertir_estado_id(
                x_valor,
                D_columna,
                D_rowExcel
                ):
            _ = D_columna
            _ = D_rowExcel
            _n_id = _dbdefaults_genericos_sat_cache(db01.tpais_estados, 'c_estado', x_valor)
            return _n_id

        _ = D_otros
        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTabla = db01.tpais_estado_localidades
        _O_importar = IMPORTAR_EXCEL(
            dbTabla = _dbTabla,
            b_insertar_noRelacionados = True,
            b_ignorar_noRelacionados = False,
            b_insertarTodos = False,
            fnIdentificarHeader = None,
            alValidar = None,
            alAceptar = None
            )
        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = None,
            dbCampo = _dbTabla.c_localidad,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "c_Localidad",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = None,
            dbCampo = _dbTabla.descripcion,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Descripción",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = None,
            dbCampo = _dbTabla.pais_estado_id,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "c_Estado",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = convertir_estado_id
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.c_estado,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "c_Estado",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        # _O_importar.agregar_campo_columna_importar(
        #     n_id_columna = None,
        #     dbCampo = _dbTabla.fecha_inicovigencia,
        #     E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
        #     s_nombreColumna = "Fecha de inicio de vigencia",
        #     n_columna = None,
        #     x_valorFijo = None,
        #     fnConversion = None
        #     )
        #
        # _O_importar.agregar_campo_columna_importar(
        #     n_id_columna = None,
        #     dbCampo = _dbTabla.fecha_finvigencia,
        #     E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
        #     s_nombreColumna = "Fecha de fin de vigencia",
        #     n_columna = None,
        #     x_valorFijo = None,
        #     fnConversion = None
        #     )

        _D_result = _O_importar.importar(
            s_pathArchivo = s_pathArchivo,
            s_nombreHoja = s_nombreHoja or 'c_Localidad'
            )

        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

        return _D_return

    pass


db01.define_table(
    'tpais_estado_localidades',
    Field(
        'pais_estado_id', db01.tpais_estados, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Estado', comment = None,
        writable = False, readable = False,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 100, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Descripción', comment = 'Nombre de la localidad',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'c_localidad', 'string', length = 2, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Localidad', comment = 'Código SAT de la localidad',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'c_estado', 'string', length = 5, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Estado', comment = 'Código SAT del estado',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    tSignature_db01,
    format = '%(descripcion)s',
    singular = 'Localidad',
    plural = 'Localidades',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TADUANAS:
    """ Definición de los formularios disponibles en el sistema """
    pass


db01.define_table(
    'taduanas',
    Field(
        'c_aduana', 'string', length = 2, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Aduana', comment = 'Código SAT de la aduana',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 200, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Descripción', comment = 'Nombre de la aduana',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    tSignature_db01,
    format = '%(descripcion)s [%(c_aduana)s]',
    singular = 'Aduana',
    plural = 'Aduanas',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TPRODSERVS:
    """ Definición de los formularios disponibles en el sistema """
    pass


db01.define_table(
    'tprodservs',
    Field(
        'c_claveprodserv', 'string', length = 8, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Clave', comment = 'Clave del producto o servicio',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 200, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Descripción', comment = 'Descripción del producto o servicio',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'incluiriva', 'integer', default = 3,
        required = False,
        requires = IS_IN_SET(TGENERICAS.SI_NO_OPCIONAL.get_dict(), zero = 0, error_message = 'Selecciona'),
        notnull = False, unique = False,
        widget = stv_widget_combobox,
        label = 'Incluir IVA', comment = 'Incluir IVA trasladado',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TGENERICAS.SI_NO_OPCIONAL.get_dict())
        ),
    Field(
        'incluirieps', 'integer', default = 3,
        required = False,
        requires = IS_IN_SET(TGENERICAS.SI_NO_OPCIONAL.get_dict(), zero = 0, error_message = 'Selecciona'),
        notnull = False, unique = False,
        widget = stv_widget_combobox,
        label = 'Incluir IEPS', comment = 'Incluir IEPS trasladado',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TGENERICAS.SI_NO_OPCIONAL.get_dict())
        ),
    Field(
        'complemento', 'string', default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Complemento', comment = 'Complemento que debe incluir',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    tSignature_db01,
    format = '%(descripcion)s [%(c_claveprodserv)s]',
    singular = 'Producto o Servicio',
    plural = 'Productos o Servicios',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TUNIDADES:
    pass


db01.define_table(
    'tunidades',
    Field(
        'c_claveunidad', 'string', length = 3, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Unidad', comment = 'Código SAT de la unidad',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'nombre', 'string', length = 200, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Nombre', comment = 'Nombre de la unidad',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'text', default = None,
        required = False,
        notnull = False,
        label = 'Descripción', comment = "Descripción detallada del significado de la aduana",
        writable = True, readable = True,
        represent = stv_represent_text
        ),
    Field(
        'simbolo', 'string', length = 25, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Símbolo', comment = 'Símbolo usado en la unidad',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    tSignature_db01,
    format = '%(nombre)s [%(c_claveunidad)s]',
    singular = 'Unidad',
    plural = 'Unidades',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )

TUNIDADES.SERVICIO = _dbdefaults_genericos_sat_cache(db01.tunidades, 'c_claveunidad', 'E48')
TUNIDADES.BONIFICACION = _dbdefaults_genericos_sat_cache(db01.tunidades, 'c_claveunidad', 'ACT')
TUNIDADES.PIEZA = _dbdefaults_genericos_sat_cache(db01.tunidades, 'c_claveunidad', 'H87')


class TCODIGOSPOSTALES:
    pass


db01.define_table(
    'tcodigospostales',
    Field(
        'c_codigopostal', 'string', length = 5, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Codigo Postal', comment = 'Código Postal',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'pais_estado_id', db01.tpais_estados, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1),
        label = 'Estado', comment = None,
        writable = True, readable = True,
        represent = stv_represent_referencefield
        ),
    Field(
        'pais_estado_municipio_id', db01.tpais_estado_municipios, default = None,
        required = False,
        ondelete = 'CASCADE', notnull = False, unique = False,
        widget = lambda field, value: stv_widget_db_chosen_linked(
            field, value, 1, 'pais_estado_id', URL(c = '005genericos_sat', f = 'pais_estado_mun_ver.json')
            ),
        label = 'Municipio', comment = None,
        writable = True, readable = True,
        represent = stv_represent_referencefield
        ),
    Field(
        'pais_estado_localidad_id', db01.tpais_estado_localidades, default = None,
        required = False,
        ondelete = 'CASCADE', notnull = False, unique = False,
        widget = lambda field, value: stv_widget_db_chosen_linked(
            field, value, 1, 'pais_estado_id', URL(c = '005genericos_sat', f = 'pais_estado_loc_ver.json')
            ),
        label = 'Localidad', comment = None,
        writable = True, readable = True,
        represent = stv_represent_referencefield
        ),
    tSignature_db01,
    format = '%(c_codigopostal)s',
    singular = 'Código Postal',
    plural = 'Codigos Postales',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TIMPUESTOS:

    class LOCAL_FEDERAL:
        """ Se definen las opciónes del campo """
        NADA = 0
        LOCAL = 1
        FEDERAL = 2
        
        @classmethod
        def get_dict(cls):
            return {
                cls.LOCAL  : 'Local',
                cls.FEDERAL: 'Federal',
                }

    pass


db01.define_table(
    'timpuestos',
    Field(
        'c_impuesto', 'string', length = 3, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Impuesto', comment = 'Código SAT del impuesto',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 200, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Descripción', comment = 'Descripción del impuesto',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'retencion', 'integer', default = 0,
        required = True, requires = IS_IN_SET(TGENERICAS.SI_NO.get_dict(), zero = 0, error_message = 'Selecciona'),
        notnull = False,
        widget = stv_widget_combobox,
        label = 'Retención', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TGENERICAS.SI_NO.get_dict())
        ),
    Field(
        'traslado', 'integer', default = 0,
        required = True, requires = IS_IN_SET(TGENERICAS.SI_NO.get_dict(), zero = 0, error_message = 'Selecciona'),
        notnull = False,
        widget = stv_widget_combobox,
        label = 'Traslado', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TGENERICAS.SI_NO.get_dict())
        ),
    Field(
        'federal', 'integer', default = 1,
        required = True,
        requires = IS_IN_SET(TIMPUESTOS.LOCAL_FEDERAL.get_dict(), zero = 0, error_message = 'Selecciona'),
        notnull = False,
        widget = stv_widget_combobox,
        label = 'Federal', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TIMPUESTOS.LOCAL_FEDERAL.get_dict())
        ),
    tSignature_db01,
    format = '%(descripcion)s',
    singular = 'Impuesto',
    plural = 'Impuestos',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )
TIMPUESTOS.IVA = _dbdefaults_genericos_sat_cache(db01.timpuestos, 'c_impuesto', '002')
TIMPUESTOS.ISR = _dbdefaults_genericos_sat_cache(db01.timpuestos, 'c_impuesto', '001')
TIMPUESTOS.IEPS = _dbdefaults_genericos_sat_cache(db01.timpuestos, 'c_impuesto', '003')


class TIMPUESTO_ESTADOS:
    pass


db01.define_table(
    'timpuesto_estados',
    Field(
        'impuesto_id', db01.timpuestos, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1),
        label = 'Impuesto', comment = None,
        writable = False, readable = False,
        represent = stv_represent_referencefield
        ),
    Field(
        'pais_estado_id', db01.tpais_estados, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1),
        label = 'Estado', comment = None,
        writable = True, readable = True,
        represent = stv_represent_referencefield
        ),
    tSignature_db01,
    format = '%(c_codigopostal)s [%(c_estado)s]',
    singular = 'Impuesto - Estado',
    plural = 'Impuesto - Estados',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TMETODOSPAGO:
    pass


db01.define_table(
    'tmetodospago',
    Field(
        'c_metodopago', 'string', length = 3, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Método Pago', comment = 'Código SAT del método de pago',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 200, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Descripción', comment = 'Descripción del método de pago',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    tSignature_db01,
    format = '%(c_metodopago)s',
    singular = 'Método de Pago',
    plural = 'Métodos de Pago',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )
TMETODOSPAGO = TMETODOSPAGO()
TMETODOSPAGO.PARCIALIDADES = _dbdefaults_genericos_sat_cache(db01.tmetodospago, 'c_metodopago', 'PPD')
TMETODOSPAGO.UNAEXHIBICION = _dbdefaults_genericos_sat_cache(db01.tmetodospago, 'c_metodopago', 'PUE')


class TFORMASPAGO:
    class NOMBREBANCOEMISOR:
        """ Se definen las opciónes del campo """
        NADA = 0
        SI = 1
        NO = 2
        OPCIONAL = 3
        CONDICIONAL = 4

        @classmethod
        def get_dict(cls):
            return {
                cls.SI         : 'Si',
                cls.NO         : 'No',
                cls.OPCIONAL   : 'Opcional',
                cls.CONDICIONAL: (
                    'Si el RFC del emisor de la cuenta ordenante es XEXX010101000, este campo es obligatorio'
                    ),
                }

    pass


db01.define_table(
    'tformaspago',
    Field(
        'c_formapago', 'string', length = 2, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Forma Pago', comment = 'Código SAT de la forma de pago',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 200, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Descripción', comment = 'Descripción de la forma de pago',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'bancarizado', 'integer', default = 3,
        required = True,
        requires = IS_IN_SET(TGENERICAS.SI_NO_OPCIONAL.get_dict(), zero = 0, error_message = 'Selecciona'),
        notnull = True, unique = False,
        widget = stv_widget_combobox,
        label = 'Bancarizado', comment = 'Incluir IVA trasladado',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TGENERICAS.SI_NO_OPCIONAL.get_dict())
        ),
    Field(
        'numerooperacion', 'integer', default = 3,
        required = True,
        requires = IS_IN_SET(TGENERICAS.SI_NO_OPCIONAL.get_dict(), zero = 0, error_message = 'Selecciona'),
        notnull = True, unique = False,
        widget = stv_widget_combobox,
        label = 'Número de Operación', comment = 'Requiere número de operación',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TGENERICAS.SI_NO_OPCIONAL.get_dict())
        ),
    Field(
        'rfcctaordenande', 'integer', default = 3,
        required = True,
        requires = IS_IN_SET(TGENERICAS.SI_NO_OPCIONAL.get_dict(), zero = 0, error_message = 'Selecciona'),
        notnull = True, unique = False,
        widget = stv_widget_combobox,
        label = 'RFC Cta. Ordenante', comment = 'Requiere RFC del Emisor de la cuenta ordenante',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TGENERICAS.SI_NO_OPCIONAL.get_dict())
        ),
    Field(
        'ctaordenante', 'integer', default = 3,
        required = True,
        requires = IS_IN_SET(TGENERICAS.SI_NO_OPCIONAL.get_dict(), zero = 0, error_message = 'Selecciona'),
        notnull = True, unique = False,
        widget = stv_widget_combobox,
        label = 'Cta. Ordenante', comment = 'Requiere cuenta ordenante',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TGENERICAS.SI_NO_OPCIONAL.get_dict())
        ),
    Field(
        'formato_ctaordenante', 'string', length = 60, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Formato Cta. Ordenante', comment = 'Formato usado para la cuenta ordenante',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'rfcctabeneficiario', 'integer', default = 3,
        required = True,
        requires = IS_IN_SET(TGENERICAS.SI_NO_OPCIONAL.get_dict(), zero = 0, error_message = 'Selecciona'),
        notnull = True, unique = False,
        widget = stv_widget_combobox,
        label = 'RFC Cta. Beneficiario', comment = 'Requiere RFC del Emisor de la cuenta de beneficiario',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TGENERICAS.SI_NO_OPCIONAL.get_dict())
        ),
    Field(
        'ctabeneficiario', 'integer', default = 3,
        required = True,
        requires = IS_IN_SET(TGENERICAS.SI_NO_OPCIONAL.get_dict(), zero = 0, error_message = 'Selecciona'),
        notnull = True, unique = False,
        widget = stv_widget_combobox,
        label = 'Cta. Beneficiario', comment = 'Requiere cuenta de beneficiario',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TGENERICAS.SI_NO_OPCIONAL.get_dict())
        ),
    Field(
        'formato_ctabeneficiario', 'string', length = 60, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Formato Cta. Beneficiario',
        comment = 'Formato usado para la cuenta de beneficiario',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'tipocadenapago', 'integer', default = 3,
        required = True,
        requires = IS_IN_SET(TGENERICAS.SI_NO_OPCIONAL.get_dict(), zero = 0, error_message = 'Selecciona'),
        notnull = True, unique = False,
        widget = stv_widget_combobox,
        label = 'Tipo Cadena Pago', comment = '',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TGENERICAS.SI_NO_OPCIONAL.get_dict())
        ),
    Field(
        'nombrebancoemisor', 'integer', default = 3,
        required = True,
        requires = IS_IN_SET(TFORMASPAGO.NOMBREBANCOEMISOR.get_dict(), zero = 0, error_message = 'Selecciona'),
        notnull = True, unique = False,
        widget = stv_widget_combobox,
        label = 'Nombre del banco emisor',
        comment = 'Nombre del Banco emisor de la cuenta ordenante en caso de extranjero',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TFORMASPAGO.NOMBREBANCOEMISOR.get_dict())
        ),
    Field(
        'codigo_contpaqi', 'string', length = 2, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Código CONTPAQi', comment = 'Código utilizado en CONTAPQi',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    tSignature_db01,
    format = '%(descripcion)s [%(c_formapago)s]',
    singular = 'Forma de Pago',
    plural = 'Formas de Pago',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )
TFORMASPAGO.EFECTIVO = _dbdefaults_genericos_sat_cache(db01.tformaspago, 'c_formapago', '01')
TFORMASPAGO.TDD = _dbdefaults_genericos_sat_cache(db01.tformaspago, 'c_formapago', '28')
TFORMASPAGO.TDC = _dbdefaults_genericos_sat_cache(db01.tformaspago, 'c_formapago', '04')
TFORMASPAGO.CHEQUE = _dbdefaults_genericos_sat_cache(db01.tformaspago, 'c_formapago', '02')
TFORMASPAGO.TRANSFERENCIA = _dbdefaults_genericos_sat_cache(db01.tformaspago, 'c_formapago', '03')
TFORMASPAGO.POR_DEFINIR = _dbdefaults_genericos_sat_cache(db01.tformaspago, 'c_formapago', '99')
TFORMASPAGO.CONFUSION = _dbdefaults_genericos_sat_cache(db01.tformaspago, 'c_formapago', '24')


class TMONEDAS:

    @staticmethod
    def DEFINIR_DBCAMPO(
            s_nombreCampo = 'moneda_id',
            represent = None
            ):
        return Field(
            s_nombreCampo, 'integer', default = D_stvSiteHelper.dbconfigs.defaults.moneda_id,
            required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tmonedas.id', db01.tmonedas._format)),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Moneda', comment = None,
            writable = False, readable = True,
            represent = represent
            )

    @staticmethod
    def GETROWFORMATED(n_id):
        if n_id:
            if hasattr(db01.tmonedas._format, '__call__'):
                # ...ejecuta la función y regresa el resultado.
                return db01.tmonedas._format(db01.tmonedas(n_id))
            else:
                # ...de lo contrario haz el formato.
                return db01.tmonedas._format % db01.tmonedas(n_id).as_dict()
        else:
            return 'Nada'
    pass


db01.define_table(
    'tmonedas',
    Field(
        'c_moneda', 'string', length = 3, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Moneda', comment = 'Código SAT de la moneda',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 200, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Descripción', comment = 'Descripción de la moneda',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'decimales', 'integer', default = 0,
        required = True,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Decimales', comment = None,
        writable = True, readable = True,
        represent = stv_represent_number
        ),
    Field(
        'variacion', 'decimal(10,2)',
        required = False, default = 35,
        notnull = False, unique = False,
        widget = stv_widget_inputPercentage, label = '% Variación', comment = None,
        writable = True, readable = True,
        represent = stv_represent_percentage
        ),
    tSignature_db01,
    format = '%(descripcion)s [%(c_moneda)s]',
    singular = 'Moneda',
    plural = 'Monedas',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )

TMONEDAS.XXX = _dbdefaults_genericos_sat_cache(db01.tmonedas, 'c_moneda', 'XXX')
D_stvSiteHelper.dbconfigs.defaults.moneda_id = _dbdefaults_genericos_sat_cache(db01.tmonedas, 'c_moneda', 'MXN')
TMONEDAS.MXP = D_stvSiteHelper.dbconfigs.defaults.moneda_id
D_stvSiteHelper.dbconfigs.defaults.moneda_descripcion = cache.ram(
    'db_moneda_descripcion', lambda: db01.tmonedas(D_stvSiteHelper.dbconfigs.defaults.moneda_id).descripcion,
    time_expire = 86400
    )
TMONEDAS.MXP_DESCRIPCION = D_stvSiteHelper.dbconfigs.defaults.moneda_descripcion


class TTIPOSCOMPROBANTES(Table):
    S_NOMBRETABLA = 'ttiposcomprobantes'

    L_DBCAMPOS = [
        Field(
            'c_tipodecomprobante', 'string', length = 1, default = None,
            required = True,
            notnull = True, unique = True,
            widget = stv_widget_input, label = 'Tipo Comprobante', comment = 'Código SAT de tipo de comprobante',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'descripcion', 'string', length = 200, default = None,
            required = True,
            notnull = True, unique = True,
            widget = stv_widget_input, label = 'Descripción', comment = 'Descripción del tipo de comprobante',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'valmaximo', 'decimal(24,5)',
            required = True, default = 999999999999999999.99999,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Valor Máximo', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money
            ),
        Field(
            'valmaximo_ns', 'decimal(24,5)',
            required = True, default = 999999999999999999.99999,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Valor Máximo NS', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money
            ),
        Field(
            'valmaximo_nds', 'decimal(24,5)',
            required = True, default = 999999999999999999.99999,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Valor Máximo NdS', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money
            ),
        tSignature_db01,
        ]

    @staticmethod
    def DBTABLA():
        return db01.ttiposcomprobantes

    pass


db01.define_table(
    TTIPOSCOMPROBANTES.S_NOMBRETABLA, *TTIPOSCOMPROBANTES.L_DBCAMPOS,
    format = '%(descripcion)s [%(c_tipodecomprobante)s]',
    singular = 'Tipo Comprobante',
    plural = 'Tipos de Comprobantes',
    migrate = D_stvSiteCfg.b_requestIsGenericApp,
    table_class = TTIPOSCOMPROBANTES
    )
TTIPOSCOMPROBANTES.EGRESO = _dbdefaults_genericos_sat_cache(db01.ttiposcomprobantes, 'c_tipodecomprobante', 'E')
TTIPOSCOMPROBANTES.INGRESO = _dbdefaults_genericos_sat_cache(db01.ttiposcomprobantes, 'c_tipodecomprobante', 'I')
TTIPOSCOMPROBANTES.PAGO = _dbdefaults_genericos_sat_cache(db01.ttiposcomprobantes, 'c_tipodecomprobante', 'P')
TTIPOSCOMPROBANTES.TRASLADO = _dbdefaults_genericos_sat_cache(db01.ttiposcomprobantes, 'c_tipodecomprobante', 'T')
TTIPOSCOMPROBANTES.NOMINA = _dbdefaults_genericos_sat_cache(db01.ttiposcomprobantes, 'c_tipodecomprobante', 'N')


class TPATENTESADUANALES:
    pass


db01.define_table(
    'tpatentesaduanales',
    Field(
        'c_patenteaduanal', 'string', length = 4, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Patente Aduanal', comment = 'Código SAT de la patenteaduanal',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    tSignature_db01,
    format = '%(c_patenteaduanal)s',
    singular = 'Patente Aduanal',
    plural = 'Patentes Aduanales',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TTIPOSRELACIONES:

    @staticmethod
    def DBTABLA():
        return db01.ttiposrelaciones

    pass


db01.define_table(
    'ttiposrelaciones',
    Field(
        'c_tiporelacion', 'string', length = 2, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Tipo Relación', comment = 'Código SAT del tipo de relación',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 200, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Descripción', comment = 'Descripción del tipo de relación',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'aplicaingreso', 'boolean', default = True,
        required = False,
        notnull = False,
        widget = stv_widget_inputCheckbox, label = 'Aplica Ingreso', comment = None,
        writable = True, readable = True,
        represent = stv_represent_boolean
        ),
    Field(
        'aplicaegreso', 'boolean', default = True,
        required = False,
        notnull = False,
        widget = stv_widget_inputCheckbox, label = 'Aplica Egreso', comment = None,
        writable = True, readable = True,
        represent = stv_represent_boolean
        ),
    Field(
        'aplicapago', 'boolean', default = True,
        required = False,
        notnull = False,
        widget = stv_widget_inputCheckbox, label = 'Aplica Pago', comment = None,
        writable = True, readable = True,
        represent = stv_represent_boolean
        ),
    Field(
        'aplicatraslado', 'boolean', default = False,
        required = False,
        notnull = False,
        widget = stv_widget_inputCheckbox, label = 'Aplica Traslado', comment = None,
        writable = True, readable = True,
        represent = stv_represent_boolean
        ),
    Field(
        'aplicanomina', 'boolean', default = False,
        required = False,
        notnull = False,
        widget = stv_widget_inputCheckbox, label = 'Aplica Nomina', comment = None,
        writable = True, readable = True,
        represent = stv_represent_boolean
        ),
    tSignature_db01,
    format = '%(descripcion)s [%(c_tiporelacion)s]',
    singular = 'Tipo Relación',
    plural = 'Tipo Relaciones',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )

TTIPOSRELACIONES.NOTACREDITO = _dbdefaults_genericos_sat_cache(db01.ttiposrelaciones, 'c_tiporelacion', '01')
TTIPOSRELACIONES.NOTADEBITO = _dbdefaults_genericos_sat_cache(db01.ttiposrelaciones, 'c_tiporelacion', '02')
TTIPOSRELACIONES.DEVOLUCION = _dbdefaults_genericos_sat_cache(db01.ttiposrelaciones, 'c_tiporelacion', '03')
TTIPOSRELACIONES.SUSTITUCION = _dbdefaults_genericos_sat_cache(db01.ttiposrelaciones, 'c_tiporelacion', '04')
TTIPOSRELACIONES.TRASLADOFACTPREV = _dbdefaults_genericos_sat_cache(db01.ttiposrelaciones, 'c_tiporelacion', '05')
TTIPOSRELACIONES.FACTPORTRASPREV = _dbdefaults_genericos_sat_cache(db01.ttiposrelaciones, 'c_tiporelacion', '06')
TTIPOSRELACIONES.ANTICIPO = _dbdefaults_genericos_sat_cache(db01.ttiposrelaciones, 'c_tiporelacion', '07')


class TUSOSCFDI:
    pass


db01.define_table(
    'tusoscfdi',
    Field(
        'c_usocfdi', 'string', length = 3, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Uso CFDI', comment = 'Código SAT de uso CFDI',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 200, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Descripción', comment = 'Descripción del uso CFDI',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'aplicaperfisica', 'integer', default = 1,
        required = True, requires = IS_IN_SET(TGENERICAS.SI_NO.get_dict(), zero = 0, error_message = 'Selecciona'),
        notnull = False, unique = False,
        widget = stv_widget_combobox,
        label = 'Aplica P. Física', comment = 'Aplica para persona física',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TGENERICAS.SI_NO.get_dict())
        ),
    Field(
        'aplicapermoral', 'integer', default = 1,
        required = True, requires = IS_IN_SET(TGENERICAS.SI_NO.get_dict(), zero = 0, error_message = 'Selecciona'),
        notnull = False, unique = False,
        widget = stv_widget_combobox,
        label = 'Aplica P. Moral', comment = 'Aplica para persona moral',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TGENERICAS.SI_NO.get_dict())
        ),
    tSignature_db01,
    format = '%(descripcion)s [%(c_usocfdi)s]',
    singular = 'Uso CFDI',
    plural = 'Usos CFDI',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )

TUSOSCFDI.ADQUISICION_MERCANCIA = _dbdefaults_genericos_sat_cache(db01.tusoscfdi, 'c_usocfdi', 'G01')
TUSOSCFDI.DEVOLUCIONES_DESCUENTOS_BONIFICACIONES = _dbdefaults_genericos_sat_cache(db01.tusoscfdi, 'c_usocfdi', 'G02')
TUSOSCFDI.POR_DEFINIR = _dbdefaults_genericos_sat_cache(db01.tusoscfdi, 'c_usocfdi', 'P01')


class TREGIMENESFISCALES:
    pass


db01.define_table(
    'tregimenesfiscales',
    Field(
        'c_regimenfiscal', 'string', length = 3, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Régimen Fiscal', comment = 'Código SAT del régimen fiscal',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 200, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Descripción', comment = 'Descripción del régimen fiscal',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'aplicaperfisica', 'integer', default = 1,
        required = True, requires = IS_IN_SET(TGENERICAS.SI_NO.get_dict(), zero = 0, error_message = 'Selecciona'),
        notnull = False, unique = False,
        widget = stv_widget_combobox,
        label = 'Aplica P. Física', comment = 'Aplica para persona física',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TGENERICAS.SI_NO.get_dict())
        ),
    Field(
        'aplicapermoral', 'integer', default = 1,
        required = True, requires = IS_IN_SET(TGENERICAS.SI_NO.get_dict(), zero = 0, error_message = 'Selecciona'),
        notnull = False, unique = False,
        widget = stv_widget_combobox,
        label = 'Aplica P. Moral', comment = 'Aplica para persona moral',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TGENERICAS.SI_NO.get_dict())
        ),
    tSignature_db01,
    format = '%(descripcion)s [%(c_regimenfiscal)s]',
    singular = 'Régimen Fiscal',
    plural = 'Regimenes Fiscales',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )

TREGIMENESFISCALES.GRAL_PERSONAS_MORALES = _dbdefaults_genericos_sat_cache(
    db01.tregimenesfiscales, 'c_regimenfiscal', '601'
    )


class TTIMBRADOS:
    pass


db01.define_table(
    'ttimbrados',
    Field(
        'versiontimbrado', 'decimal(4,2)', default = 1.01,
        required = True, requires = IS_DECIMAL_IN_RANGE(0, 99.99),
        notnull = True, unique = True,
        widget = stv_widget_inputFloat, label = 'Versión', comment = 'Número de version de timbrado usada por el SAT',
        writable = True, readable = True,
        represent = stv_represent_float
        ),
    Field(
        'descripcion', 'string', length = 200, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Descripción', comment = 'Descripción del tipo de comprobante',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    tSignature_db01,
    format = '%(descripcion)s [%(version)s]',
    singular = 'Timbrado',
    plural = 'Timbrados',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TTABLASISR:
    """ Definición de la tabla de tTablaIsrSubsidio """
    pass


db01.define_table(
    'ttablasisr',
    Field(
        'ejercicio', 'integer',
        required = True, default = int(request.browsernow.year) + 1,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Año', comment = 'Ejercicio',
        writable = True, readable = True,
        represent = stv_represent_number
        ),
    tSignature_db01,
    format = '%(ejercicio)s',
    singular = 'Tabla de ISR y Subsidio',
    plural = 'Tablas de ISR y Subsidios',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TTABLAISR_MENSUAL:
    """ Definición de la tabla de tTabla_isrMensual """
    pass


db01.define_table(
    'ttablaisr_mensual',
    Field(
        'tablaisr_id', db01.ttablasisr, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Tabla ISR', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'limiteinferior', 'decimal(10,2)',
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_inputMoney, label = 'Límite inferior', comment = 'Límite inferior mensual',
        writable = True, readable = True,
        represent = stv_represent_money
        ),
    Field(
        'cuotafija', 'decimal(10,2)',
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_inputMoney, label = 'Cuota fija', comment = 'Cuota fija mensual',
        writable = True, readable = True,
        represent = stv_represent_money
        ),
    Field(
        'excedente', 'decimal(10,2)',
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_inputPercentage, label = '% sobre excedente', comment = '% sobre excedente mensual',
        writable = True, readable = True,
        represent = stv_represent_percentage
        ),
    tSignature_db01,
    format = '%(limiteinferior)s',
    singular = 'ISR Mensual',
    plural = 'ISR Mensual',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TTABLAISR_ANUAL:
    """ Definición de la tabla de tTabla_isrAnual """
    pass


db01.define_table(
    'ttablaisr_anual',
    Field(
        'tablaisr_id', db01.ttablasisr, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Tabla ISR', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'limiteinferior', 'decimal(10,2)',
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_inputMoney, label = 'Límite inferior', comment = 'Límite inferior mensual',
        writable = True, readable = True,
        represent = stv_represent_money
        ),
    Field(
        'cuotafija', 'decimal(10,2)',
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_inputMoney, label = 'Cuota fija', comment = 'Cuota fija mensual',
        writable = True, readable = True,
        represent = stv_represent_money
        ),
    Field(
        'excedente', 'decimal(10,2)',
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_inputPercentage, label = '% sobre excedente', comment = '% sobre excedente mensual',
        writable = True, readable = True,
        represent = stv_represent_percentage
        ),
    tSignature_db01,
    format = '%(limiteinferior)s',
    singular = 'ISR Anual',
    plural = 'ISR Anual',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TTABLAISR_SUBSIDIOMENSUAL:
    """ Definición de la tabla de tTabla_subsidioMensual """
    pass


db01.define_table(
    'ttablaisr_subsidiomensual',
    Field(
        'tablaisr_id', db01.ttablasisr, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Tabla ISR', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'limiteinferior', 'decimal(10,2)',
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_inputMoney, label = 'Límite inferior', comment = 'Límite inferior mensual',
        writable = True, readable = True,
        represent = stv_represent_money
        ),
    Field(
        'cuotafija', 'decimal(10,2)',
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_inputMoney, label = 'Cuota fija', comment = 'Cuota fija mensual',
        writable = True, readable = True,
        represent = stv_represent_money
        ),
    tSignature_db01,
    format = '%(limiteinferior)s',
    singular = 'Subsidio Mensual',
    plural = 'Subsidio Mensual',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TTABLAISR_SUBSIDIOANUAL:
    """ Definición de la tabla de tTabla_subsidioAnual """
    pass


db01.define_table(
    'ttablaisr_subsidioanual',
    Field(
        'tablaisr_id', db01.ttablasisr, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Tabla ISR', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'limiteinferior', 'decimal(10,2)',
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_inputMoney, label = 'Límite inferior', comment = 'Límite inferior mensual',
        writable = True, readable = True,
        represent = stv_represent_money
        ),
    Field(
        'cuotafija', 'decimal(10,2)',
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_inputMoney, label = 'Cuota fija', comment = 'Cuota fija mensual',
        writable = True, readable = True,
        represent = stv_represent_money
        ),
    tSignature_db01,
    format = '%(limiteinferior)s',
    singular = 'Subsidio Anual',
    plural = 'Subsidio Anual',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TUNIDADESMEDICINAFAMILIAR:
    """ Catalogo de Unidades Medicas Familiar"""
    pass


db01.define_table(
    'tunidadesmedicinafamiliar',
    Field(
        'codigo', 'string', length = 3, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Código', comment = 'Codigo del hospital',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'unidadmedica', 'string', length = 100, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Unidad Médica', comment = 'Unidad médica',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    tSignature_db01,
    format = '[%(codigo)s] - %(unidadmedica)s',
    singular = 'Unidad de Medicina Familiar',
    plural = 'Unidades de Medicina Familiar',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TCONCEPTOSSAT:
    """ Definición de la tabla de tConceptossat """

    class E_TIPO:
        """ Se definen las opciónes del campo """
        NADA = ""
        PERCEPCION = 'P'
        DEDUCCION = 'D'
        
        @classmethod
        def get_dict(cls):
            return {
                cls.PERCEPCION: 'Percepción',
                cls.DEDUCCION: 'Deducción',
                }
    pass


db01.define_table(
    'tconceptossat',
    Field(
        'tipo', 'string', length = 1, default = None,
        required = True,
        requires = IS_IN_SET(TCONCEPTOSSAT().E_TIPO().get_dict(), zero = None, error_message = 'Seleccionar'),
        notnull = True, unique = False,
        widget = stv_widget_combobox, label = 'Tipo de Concepto', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_string(TCONCEPTOSSAT().E_TIPO().get_dict().get(v, 'None'), r)
        ),
    Field(
        'c_concepto', 'string', length = 4, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Concepto', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 200, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Descripción', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    tSignature_db01,
    format = '%(tipo)s - %(c_concepto)s - %(descripcion)s',
    singular = 'Concepto SAT',
    plural = 'Conceptos SAT',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TBANCOS:
    """ Catalogo de Bancos"""

    @classmethod
    def getRowFormated(cls, n_id):
        if n_id:
            if hasattr(db01.tbancos._format, '__call__'):
                # ...ejecuta la función y regresa el resultado.
                return db01.tbancos._format(db01.tbancos(n_id))
            else:
                # ...de lo contrario haz el formato.
                return db01.tbancos._format % db01.tbancos(n_id).as_dict()
        else:
            return 'Nada'
    
    pass


db01.define_table(
    'tbancos',
    Field(
        'nombre', 'string', length = 60, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Nombre', comment = 'Nombre',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'c_banco', 'string', length = 3, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Banco', comment = 'Clave del banco',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'razonsocial', 'string', length = 120, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Razon Social', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    tSignature_db01,
    format = '%(nombre)s',
    singular = 'Banco',
    plural = 'Bancos',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TBANCOSTIPOSMOVIMIENTOS:
    """ Catalogo de tipo de movimientos"""

    @classmethod
    def getRowFormated(cls, n_id):
        if n_id:
            if hasattr(db01.tbancostiposmovimientos._format, '__call__'):
                # ...ejecuta la función y regresa el resultado.
                return db01.tbancostiposmovimientos._format(db01.tbancostiposmovimientos(n_id))
            else:
                # ...de lo contrario haz el formato.
                return db01.tbancostiposmovimientos._format % db01.tbancostiposmovimientos(n_id).as_dict()
        else:
            return 'Nada'
    
    pass


db01.define_table(
    'tbancostiposmovimientos',
    Field(
        'descripcion', 'string', length = 50, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Descripción', comment = 'Descripción del movimiento bancario',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'formapago_id', db01.tformaspago, default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1),
        label = 'Forma de pago', comment = 'En caso de abono, indicar si aplica la forma de pago',
        writable = True, readable = True,
        represent = stv_represent_referencefield
        ),
    tSignature_db01,
    format = '%(descripcion)s',
    singular = 'Tipo movimiento de banco',
    plural = 'Tipos de movimientos de banco',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TBANCOSTIPOMOVIMIENTO_CODIGOS:
    
    @classmethod    
    def getRowFormated(cls, n_id):
        if n_id:
            if hasattr(db01.tbancostipomovimiento_codigos._format, '__call__'):
                # ...ejecuta la función y regresa el resultado.
                return db01.tbancostipomovimiento_codigos._format(db01.tbancostipomovimiento_codigos(n_id))
            else:
                # ...de lo contrario haz el formato.
                return db01.tbancostipomovimiento_codigos._format % db01.tbancostipomovimiento_codigos(n_id).as_dict()
        else:
            return 'Nada'
    
    pass


db01.define_table(
    'tbancostipomovimiento_codigos',
    Field(
        'bancostipomovimiento_id', db01.tbancostiposmovimientos, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Tipos Móvimiento', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'banco_id', db01.tbancos, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1),
        label = 'Banco', comment = None,
        writable = True, readable = True,
        represent = stv_represent_referencefield
        ),
    Field(
        'codigomovimiento', 'string', length = 50, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Código de movimiento',
        comment = 'Código del movimiento en el estado de cuenta',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Descripción', comment = 'Descripción del código',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    tSignature_db01,
    format = '%(codigomovimiento)s',
    singular = 'Código',
    plural = 'Códigos',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TTRANSPORTES:

    @classmethod
    def IMPORTAR_EXCEL(
            cls,
            s_pathArchivo,
            s_nombreHoja = None,
            **D_otros
            ):
        _ = D_otros
        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTabla = db01.ttransportes
        _O_importar = IMPORTAR_EXCEL(
            dbTabla = _dbTabla,
            b_insertar_noRelacionados = True,
            b_ignorar_noRelacionados = False,
            b_insertarTodos = False,
            fnIdentificarHeader = None,
            alValidar = None,
            alAceptar = None
            )
        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = None,
            dbCampo = _dbTabla.c_cvetransporte,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Clave transporte",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.descripcion,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Descripción del tipo de transporte",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_inicovigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de inicio de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_finvigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de fin de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _D_result = _O_importar.importar(
            s_pathArchivo = s_pathArchivo,
            s_nombreHoja = s_nombreHoja or 'c_CveTransporte'
            )

        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

        return _D_return

    pass


db01.define_table(
    'ttransportes',
    Field(
        'c_cvetransporte', 'string', length = 5, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Clave transporte', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 200, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Descripción', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'fecha_inicovigencia', 'date', default = request.browsernow,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha inicio vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'fecha_finvigencia', 'date', default = None,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha fin vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    tSignature_db01,
    format = '%(descripcion)s [%(c_cvetransporte)s]',
    singular = 'Clave Transporte',
    plural = 'Claves Transportes',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )

TTRANSPORTES.AUTO = _dbdefaults_genericos_sat_cache(db01.ttransportes, 'c_cvetransporte', '01')
TTRANSPORTES.MAR = _dbdefaults_genericos_sat_cache(db01.ttransportes, 'c_cvetransporte', '02')
TTRANSPORTES.FERR = _dbdefaults_genericos_sat_cache(db01.ttransportes, 'c_cvetransporte', '03')
TTRANSPORTES.AEREO = _dbdefaults_genericos_sat_cache(db01.ttransportes, 'c_cvetransporte', '04')
TTRANSPORTES.DUCTO = _dbdefaults_genericos_sat_cache(db01.ttransportes, 'c_cvetransporte', '05')


class TTRANSPORTE_TIPOSESTACIONES:

    @classmethod
    def IMPORTAR_EXCEL(
            cls,
            s_pathArchivo,
            s_nombreHoja = None,
            **D_otros
            ):

        def convertir_transporte_id(
                x_valor,
                D_columna,
                D_rowExcel
                ):
            _ = D_columna
            _ = D_rowExcel
            _n_id = _dbdefaults_genericos_sat_cache(db01.ttransportes, 'c_cvetransporte', x_valor)
            return _n_id

        def alValidarRegistroExcel(
                D_registro,
                dbRow_encontrado,
                dbRows_encontrados,
                b_fueIdentificadoAntes,
                b_hayVariasCoincidenciasEnTabla,
                b_encontradoEnTabla
                ):
            _ = dbRow_encontrado
            _ = dbRows_encontrados
            _ = b_fueIdentificadoAntes
            _ = b_hayVariasCoincidenciasEnTabla
            _ = b_encontradoEnTabla
            # noinspection PyShadowingNames
            _D_return = Storage(
                E_return = stvfwk2_e_RETURN.OK,
                s_msgError = "",
                E_resultado_validacion = IMPORTAR_EXCEL.E_RESULTADO_VALIDACION.OK
                )
            _s_campoMaestro = 'transporte_id'

            if _s_campoMaestro not in D_otros:
                # No corresponde al maestro necesario en
                _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                _D_return.s_msgError = "Campo maestro %s no encontrado en vars" % _s_campoMaestro

            elif _s_campoMaestro not in D_registro:
                _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                _D_return.s_msgError = "Campo maestro %s no mapeado en registro de excel" % _s_campoMaestro

            else:
                if str(D_registro[_s_campoMaestro]) != str(D_otros[_s_campoMaestro]):
                    # Se ignora el regsitro de excel si no coincide con el padre
                    _D_return.E_resultado_validacion = IMPORTAR_EXCEL.E_RESULTADO_VALIDACION.IGNORARLO
                else:
                    pass

            return _D_return

        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTabla = db01.ttransporte_tiposestaciones
        _O_importar = IMPORTAR_EXCEL(
            dbTabla = _dbTabla,
            b_insertar_noRelacionados = True,
            b_ignorar_noRelacionados = False,
            b_insertarTodos = False,
            fnIdentificarHeader = None,
            alValidar = alValidarRegistroExcel,
            alAceptar = None
            )

        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = 'transporte_id',
            dbCampo = _dbTabla.transporte_id,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Clave transporte",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = convertir_transporte_id
            )
        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = None,
            dbCampo = _dbTabla.c_tipoestacion,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Clave de estación",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.descripcion,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Descripción del tipo de estación",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.c_cvetransporte,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Clave transporte",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_inicovigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de inicio de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_finvigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de fin de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _D_result = _O_importar.importar(
            s_pathArchivo = s_pathArchivo,
            s_nombreHoja = s_nombreHoja or 'c_TipoEstacion'
            )

        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

        return _D_return

    pass


db01.define_table(
    'ttransporte_tiposestaciones',
    Field(
        'transporte_id', db01.ttransportes, default = None,
        required = True,
        ondelete = 'NO ACTION', notnull = True, unique = False,
        label = 'Cve. Transporte', comment = None,
        writable = False, readable = False,
        represent = stv_represent_referencefield
        ),
    Field(
        'c_cvetransporte', 'string', length = 20, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Cve. Transporte.', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'c_tipoestacion', 'string', length = 5, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Clave transporte', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 200, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Descripción', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'fecha_inicovigencia', 'date', default = request.browsernow,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha inicio vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'fecha_finvigencia', 'date', default = None,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha fin vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    tSignature_db01,
    format = '%(descripcion)s [%(c_tipoestacion)s]',
    singular = 'Tipo Estación',
    plural = 'Tipos Estaciones',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TTRANSPORTE_ESTACIONES:

    @classmethod
    def IMPORTAR_EXCEL(
            cls,
            s_pathArchivo,
            s_nombreHoja = None,
            **D_otros
            ):

        def convertir_transporte_id(
                x_valor,
                D_columna,
                D_rowExcel
                ):
            _ = D_columna
            _ = D_rowExcel
            _n_id = _dbdefaults_genericos_sat_cache(db01.ttransportes, 'c_cvetransporte', x_valor)
            return _n_id

        def alValidarRegistroExcel(
                D_registro,
                **D_otrosAlValidar
                ):
            _ = D_otrosAlValidar
            # noinspection PyShadowingNames
            _D_return = Storage(
                E_return = stvfwk2_e_RETURN.OK,
                s_msgError = "",
                E_resultado_validacion = IMPORTAR_EXCEL.E_RESULTADO_VALIDACION.OK
                )
            _s_campoMaestro = 'transporte_id'

            if _s_campoMaestro not in D_otros:
                # No corresponde al maestro necesario en
                _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                _D_return.s_msgError = "Campo maestro %s no encontrado en vars" % _s_campoMaestro

            elif _s_campoMaestro not in D_registro:
                _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                _D_return.s_msgError = "Campo maestro %s no mapeado en registro de excel" % _s_campoMaestro

            else:
                if str(D_registro[_s_campoMaestro]) != str(D_otros[_s_campoMaestro]):
                    # Se ignora el regsitro de excel si no coincide con el padre
                    _D_return.E_resultado_validacion = IMPORTAR_EXCEL.E_RESULTADO_VALIDACION.IGNORARLO
                else:
                    pass

            return _D_return

        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTabla = db01.ttransporte_estaciones
        _O_importar = IMPORTAR_EXCEL(
            dbTabla = _dbTabla,
            b_insertar_noRelacionados = True,
            b_ignorar_noRelacionados = False,
            b_insertarTodos = False,
            fnIdentificarHeader = None,
            alValidar = alValidarRegistroExcel,
            alAceptar = None
            )

        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = 'transporte_id',
            dbCampo = _dbTabla.transporte_id,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Clave transporte",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = convertir_transporte_id
            )
        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = None,
            dbCampo = _dbTabla.c_estacion,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Clave identificación",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.c_cvetransporte,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Clave transporte",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.descripcion,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Descripción",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.nacionalidad,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Nacionalidad",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.designadoriata,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Designador IATA",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.lineaferrea,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Línea férrea",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_inicovigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de inicio de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_finvigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de fin de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _D_result = _O_importar.importar(
            s_pathArchivo = s_pathArchivo,
            s_nombreHoja = s_nombreHoja or 'c_Estaciones'
            )

        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

        return _D_return

    pass


db01.define_table(
    'ttransporte_estaciones',
    Field(
        'transporte_id', db01.ttransportes, default = None,
        required = True,
        ondelete = 'NO ACTION', notnull = True, unique = False,
        label = 'Cve. Transporte', comment = None,
        writable = False, readable = False,
        represent = stv_represent_referencefield
        ),
    Field(
        'c_cvetransporte', 'string', length = 20, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Cve. Transporte.', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'c_estacion', 'string', length = 20, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Cve. Ident.', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 200, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Descripción', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'nacionalidad', 'string', length = 200, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Nacionalidad', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'pais_id', db01.tpaises, default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1),
        label = 'Pais', comment = None,
        writable = True, readable = True,
        represent = stv_represent_referencefield
        ),
    Field(
        'designadoriata', 'string', length = 10, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Designador IATA', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'lineaferrea', 'string', length = 10, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Line Férrea', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'fecha_inicovigencia', 'date', default = request.browsernow,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha inicio vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'fecha_finvigencia', 'date', default = None,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha fin vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    tSignature_db01,
    format = '%(descripcion)s [%(c_tipoestacion)s]',
    singular = 'Estación',
    plural = 'Estaciones',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TUNIDADESPESO:
    """ Catálogo de unidades de medida y embalaje
    """

    class BANDERA:
        """ Se definen los valores posible del campo, en base a lo requerido por el SAT """
        NO_DEFINIDO = ''
        EMBALAJE = 'Embalaje'
        MASA = 'Masa'
        VOLUMEN = 'Volumen'
        _D_dict = None

        @classmethod
        def GET_DICT(cls):
            if not cls._D_dict:
                cls._D_dict = {
                    cls.NO_DEFINIDO: 'No Definido',
                    cls.EMBALAJE   : 'Embalaje',
                    cls.MASA       : 'Masa',
                    cls.VOLUMEN    : 'Volumen',
                    }
            else:
                pass
            return cls._D_dict

    @classmethod
    def IMPORTAR_EXCEL(
            cls,
            s_pathArchivo,
            s_nombreHoja = None,
            **D_otros
            ):
        _ = D_otros
        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTabla = db01.tunidadespeso
        _O_importar = IMPORTAR_EXCEL(
            dbTabla = _dbTabla,
            b_insertar_noRelacionados = True,
            b_ignorar_noRelacionados = False,
            b_insertarTodos = False,
            fnIdentificarHeader = None,
            alValidar = None,
            alAceptar = None
            )

        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = None,
            dbCampo = _dbTabla.c_claveunidadpeso,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Clave unidad",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.nombre,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Nombre",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.descripcion,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Descripción",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.nota,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Nota",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_inicovigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de inicio de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_finvigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de fin de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.simbolo,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Símbolo",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.bandera,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Bandera",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _D_result = _O_importar.importar(
            s_pathArchivo = s_pathArchivo,
            s_nombreHoja = s_nombreHoja or 'c_ClaveUnidadPeso'
            )

        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

        return _D_return

    pass


db01.define_table(
    'tunidadespeso',
    Field(
        'c_claveunidadpeso', 'string', length = 20, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Cve. Unidad Peso', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'nombre', 'string', length = 200, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Nombre', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 600, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Descripción', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'nota', 'text', default = None,
        required = False,
        notnull = False,
        widget=stv_widget_text, label = 'Nota', comment = None,
        writable = False, readable = False,
        represent = stv_represent_text
        ),
    Field(
        'fecha_inicovigencia', 'date', default = request.browsernow,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha inicio vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'fecha_finvigencia', 'date', default = None,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha fin vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'simbolo', 'string', length = 50, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Símbolo', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'bandera', 'string', length = 50, default = TUNIDADESPESO.BANDERA.NO_DEFINIDO,
        required = False, requires = IS_NULL_OR(IS_IN_SET(
            TUNIDADESPESO.BANDERA.GET_DICT(), zero = 0, error_message = 'Selecciona'
            )),
        notnull = False, unique = False,
        widget = stv_widget_combobox, label = 'BANDERA', comment = '',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TUNIDADESPESO.BANDERA.GET_DICT())
        ),
    tSignature_db01,
    format = '%(nombre)s [%(c_claveunidadpeso)s]',
    singular = 'Unidad Peso',
    plural = 'Unidades Peso',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TPRODSERVCP:
    """ Catálogo de productos y servicios carta porte
    """

    class E_MATERIALPELIGROSO:
        """ Se definen los valores posible del campo, en base a lo requerido por el SAT """
        NO_DEFINIDO = ''
        NOESPELIGROSO = '0'
        ESPELIGROSO = '1'
        PUDIERASERPELIGROSO = '0,1'
        _D_dict = None

        @classmethod
        def GET_DICT(cls):
            if not cls._D_dict:
                cls._D_dict = {
                    cls.NO_DEFINIDO        : 'No Definido',
                    cls.NOESPELIGROSO      : 'No es peligroso (0)',
                    cls.ESPELIGROSO        : 'Es peligroso (1)',
                    cls.PUDIERASERPELIGROSO: 'Pudiera ser peligroso (0,1)',
                    }
            else:
                pass
            return cls._D_dict

    @classmethod
    def IMPORTAR_EXCEL(
            cls,
            s_pathArchivo,
            s_nombreHoja = None,
            **D_otros
            ):

        _ = D_otros

        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTabla = db01.tprodservcp
        _O_importar = IMPORTAR_EXCEL(
            dbTabla = _dbTabla,
            b_insertar_noRelacionados = True,
            b_ignorar_noRelacionados = False,
            b_insertarTodos = False,
            fnIdentificarHeader = None,
            alValidar = None,
            alAceptar = None
            )

        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = None,
            dbCampo = _dbTabla.c_claveprodservcp,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Clave producto",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.descripcion,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Descripción",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.palabrassimilares,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Palabras similares",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.materialpeligroso,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Material peligroso",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_inicovigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de inicio de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_finvigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de fin de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _D_result = _O_importar.importar(
            s_pathArchivo = s_pathArchivo,
            s_nombreHoja = s_nombreHoja or 'c_ClaveProdServCP'
            )

        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

        return _D_return

    pass


db01.define_table(
    'tprodservcp',
    Field(
        'c_claveprodservcp', 'string', length = 20, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Cve. Prod. Serv. CP',
        comment = 'Clave de Producto o Servicio de Carta Porte',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 600, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Descripción', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'palabrassimilares', 'string', length = 600, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Palabras Similares', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'materialpeligroso', 'string', length = 50, default = TPRODSERVCP.E_MATERIALPELIGROSO.NO_DEFINIDO,
        required = False, requires = IS_NULL_OR(
            IS_IN_SET(
                TPRODSERVCP.E_MATERIALPELIGROSO.GET_DICT(), zero = 0, error_message = 'Selecciona'
                )
            ),
        notnull = False, unique = False,
        widget = stv_widget_combobox, label = 'Material Peligroso', comment = '',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TPRODSERVCP.E_MATERIALPELIGROSO.GET_DICT())
        ),
    Field(
        'fecha_inicovigencia', 'date', default = request.browsernow,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha inicio vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'fecha_finvigencia', 'date', default = None,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha fin vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    tSignature_db01,
    format = '%(descripcion)s [%(c_claveprodservcp)s]',
    singular = 'Clave Prod. Serv. para Carta Porte',
    plural = 'Claves Prod. Serv. para Carta Porte',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )
TPRODSERVCP.NO_EXISTE = _dbdefaults_genericos_sat_cache(db01.tprodservcp, 'c_claveprodservcp', '01010101')


class TMATERIALESPELIGROSOS:
    """ Catálogo de materiales peligrosos carta porte
    """

    @classmethod
    def IMPORTAR_EXCEL(
            cls,
            s_pathArchivo,
            s_nombreHoja = None,
            **D_otros
            ):
        _ = D_otros
        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTabla = db01.tmaterialespeligrosos
        _O_importar = IMPORTAR_EXCEL(
            dbTabla = _dbTabla,
            b_insertar_noRelacionados = True,
            b_ignorar_noRelacionados = False,
            b_insertarTodos = False,
            fnIdentificarHeader = None,
            alValidar = None,
            alAceptar = None
            )

        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = None,
            dbCampo = _dbTabla.c_materialpeligroso,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Clave material peligroso",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.descripcion,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Descripción",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.claseodiv,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Clase o div.",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.peligrosecundario,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Peligro secundario",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.grupoonu,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Grupo de emb/env ONU",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.dispocisionespecial,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Disp. espec.",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.cantlimitadas_1,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Cantidades limitadas y exceptuadas > 1",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.cantlimitadas_2,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Cantidades limitadas y exceptuadas > 2",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.embalajerig_instrucciones,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Embalajes/envases y RIG > Inst. de emb/env",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.embalajerig_disposicion,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Embalajes/envases y RIG > Disp. espec.",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.cisternascontenedores_instrucciones,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Cisternas portátiles y contenedores para graneles > Inst. de transp.",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.cisternascontenedores_disposicion,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Cisternas portátiles y contenedores para graneles > Disp. espec.",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_inicovigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de inicio de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_finvigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de fin de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _D_result = _O_importar.importar(
            s_pathArchivo = s_pathArchivo,
            s_nombreHoja = s_nombreHoja or 'c_MaterialPeligroso'
            )

        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

        return _D_return

    pass


db01.define_table(
    'tmaterialespeligrosos',
    Field(
        'c_materialpeligroso', 'string', length = 20, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Material Peligroso',
        comment = 'Clave del material peligroso',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 600, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Descripción', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'claseodiv', 'string', length = 100, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Clase o div.', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'peligrosecundario', 'string', length = 100, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Peligro Secundario', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'grupoonu', 'string', length = 100, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Grupo de emb/env ONU',
        comment = 'Grupo de embalaje/envase ONU',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'dispocisionespecial', 'string', length = 100, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Disp. Espec.', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'cantlimitadas_1', 'string', length = 100, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Cant. limitadas 1',
        comment = 'Cantidades limitadas y exceptuadas > limite1',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'cantlimitadas_2', 'string', length = 100, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Cant. limitadas 2',
        comment = 'Cantidades limitadas y exceptuadas > limite2',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'embalajerig_instrucciones', 'string', length = 100, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Embajales/envases y RIG Instrucciones',
        comment = 'Embalajes/envases y RIG > Inst. de emb/env',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'embalajerig_disposicion', 'string', length = 100, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Embajales/envases y RIG Disposición',
        comment = 'Embalajes/envases y RIG > Disp. espec.',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'cisternascontenedores_instrucciones', 'string', length = 100, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Cisternas/Contenedores para graneles Instrucciones',
        comment = 'Cisternas portátiles y contenedores para graneles > Inst. de transp.',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'cisternascontenedores_disposicion', 'string', length = 100, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Cisternas/Contenedores para graneles Disposición',
        comment = 'Cisternas portátiles y contenedores para graneles > Disp. espec.',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'fecha_inicovigencia', 'date', default = request.browsernow,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha inicio vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'fecha_finvigencia', 'date', default = None,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha fin vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    tSignature_db01,
    format = '%(descripcion)s [%(c_materialpeligroso)s]',
    singular = 'Material Peligroso',
    plural = 'Materiales Peligrosos',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TTIPOSEMBALAJES:

    @classmethod
    def IMPORTAR_EXCEL(
            cls,
            s_pathArchivo,
            s_nombreHoja = None,
            **D_otros
            ):
        _ = D_otros
        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTabla = db01.ttiposembalajes
        _O_importar = IMPORTAR_EXCEL(
            dbTabla = _dbTabla,
            b_insertar_noRelacionados = True,
            b_ignorar_noRelacionados = False,
            b_insertarTodos = False,
            fnIdentificarHeader = None,
            alValidar = None,
            alAceptar = None
            )
        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = None,
            dbCampo = _dbTabla.c_tipoembalaje,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Clave de designación",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.descripcion,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Descripción",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_inicovigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de inicio de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_finvigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de fin de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _D_result = _O_importar.importar(
            s_pathArchivo = s_pathArchivo,
            s_nombreHoja = s_nombreHoja or 'c_TipoEmbalaje'
            )

        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

        return _D_return

    pass


db01.define_table(
    'ttiposembalajes',
    Field(
        'c_tipoembalaje', 'string', length = 5, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Clave embalaje', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 500, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Descripción', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'fecha_inicovigencia', 'date', default = request.browsernow,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha inicio vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'fecha_finvigencia', 'date', default = None,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha fin vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    tSignature_db01,
    format = '%(descripcion)s [%(c_tipoembalaje)s]',
    singular = 'Tipo Embalaje',
    plural = 'Tipos Embalajes',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )
TTIPOSEMBALAJES.NO_APLICA = _dbdefaults_genericos_sat_cache(db01.ttiposembalajes, 'c_tipoembalaje', 'Z01')


class TTRANSPORTE_TIPOSPERMISOS:

    @classmethod
    def IMPORTAR_EXCEL(
            cls,
            s_pathArchivo,
            s_nombreHoja = None,
            **D_otros
            ):

        def convertir_transporte_id(
                x_valor,
                D_columna,
                D_rowExcel
                ):
            _ = D_columna
            _ = D_rowExcel
            _n_id = _dbdefaults_genericos_sat_cache(db01.ttransportes, 'c_cvetransporte', x_valor)
            return _n_id

        def alValidarRegistroExcel(
                D_registro,
                **D_otrosAlValidar
                ):
            _ = D_otrosAlValidar
            # noinspection PyShadowingNames
            _D_return = Storage(
                E_return = stvfwk2_e_RETURN.OK,
                s_msgError = "",
                E_resultado_validacion = IMPORTAR_EXCEL.E_RESULTADO_VALIDACION.OK
                )
            _s_campoMaestro = 'transporte_id'

            if _s_campoMaestro not in D_otros:
                # No corresponde al maestro necesario en
                _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                _D_return.s_msgError = "Campo maestro %s no encontrado en vars" % _s_campoMaestro

            elif _s_campoMaestro not in D_registro:
                _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                _D_return.s_msgError = "Campo maestro %s no mapeado en registro de excel" % _s_campoMaestro

            else:
                if str(D_registro[_s_campoMaestro]) != str(D_otros[_s_campoMaestro]):
                    # Se ignora el regsitro de excel si no coincide con el padre
                    _D_return.E_resultado_validacion = IMPORTAR_EXCEL.E_RESULTADO_VALIDACION.IGNORARLO
                else:
                    pass

            return _D_return

        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTabla = db01.ttransporte_tipospermisos
        _O_importar = IMPORTAR_EXCEL(
            dbTabla = _dbTabla,
            b_insertar_noRelacionados = True,
            b_ignorar_noRelacionados = False,
            b_insertarTodos = False,
            fnIdentificarHeader = None,
            alValidar = alValidarRegistroExcel,
            alAceptar = None
            )

        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = 'transporte_id',
            dbCampo = _dbTabla.transporte_id,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Clave transporte",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = convertir_transporte_id
            )
        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = None,
            dbCampo = _dbTabla.c_tipopermiso,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Clave",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.descripcion,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Descripción",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.c_cvetransporte,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Clave transporte",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_inicovigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de inicio de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_finvigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de fin de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _D_result = _O_importar.importar(
            s_pathArchivo = s_pathArchivo,
            s_nombreHoja = s_nombreHoja or 'c_TipoPermiso'
            )

        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

        return _D_return

    pass


db01.define_table(
    'ttransporte_tipospermisos',
    Field(
        'transporte_id', db01.ttransportes, default = None,
        required = True,
        ondelete = 'NO ACTION', notnull = True, unique = False,
        label = 'Cve. Transporte', comment = None,
        writable = False, readable = False,
        represent = stv_represent_referencefield
        ),
    Field(
        'c_tipopermiso', 'string', length = 10, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Clave permiso', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 500, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Descripción', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'c_cvetransporte', 'string', length = 20, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Cve. Transporte.', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'fecha_inicovigencia', 'date', default = request.browsernow,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha inicio vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'fecha_finvigencia', 'date', default = None,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha fin vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    tSignature_db01,
    format = '%(descripcion)s [%(c_tipopermiso)s]',
    singular = 'Tipo Permiso',
    plural = 'Tipos Permisos',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TCOLONIAS:

    @classmethod
    def IMPORTAR_EXCEL(
            cls,
            s_pathArchivo,
            s_nombreHoja = None,
            **D_otros
            ):

        def convertir_codigopostal_id(
                x_valor,
                D_columna,
                D_rowExcel
                ):
            _ = D_columna
            _ = D_rowExcel
            _n_codigopostal_id = _dbdefaults_genericos_sat_cache(db01.tcodigospostales, 'c_codigopostal', x_valor)
            return _n_codigopostal_id

        _ = D_otros
        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTabla = db01.tcolonias
        _O_importar = IMPORTAR_EXCEL(
            dbTabla = _dbTabla,
            b_insertar_noRelacionados = True,
            b_ignorar_noRelacionados = False,
            b_insertarTodos = False,
            fnIdentificarHeader = None,
            alValidar = None,
            alAceptar = None
            )
        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = None,
            dbCampo = _dbTabla.c_colonia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "c_Colonia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = None,
            dbCampo = _dbTabla.nombre,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Nombre del asentamiento",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = None,
            dbCampo = _dbTabla.c_codigopostal,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "c_CodigoPostal",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.codigopostal_id,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "c_CodigoPostal",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = convertir_codigopostal_id
            )
        # _O_importar.agregar_campo_columna_importar(
        #     n_id_columna = None,
        #     dbCampo = _dbTabla.fecha_inicovigencia,
        #     E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
        #     s_nombreColumna = "Fecha de inicio de vigencia",
        #     n_columna = None,
        #     x_valorFijo = None,
        #     fnConversion = None
        #     )
        #
        # _O_importar.agregar_campo_columna_importar(
        #     n_id_columna = None,
        #     dbCampo = _dbTabla.fecha_finvigencia,
        #     E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
        #     s_nombreColumna = "Fecha de fin de vigencia",
        #     n_columna = None,
        #     x_valorFijo = None,
        #     fnConversion = None
        #     )

        if not s_nombreHoja:
            _D_result = _O_importar.importar(
                s_pathArchivo = s_pathArchivo,
                s_nombreHoja = 'c_Colonia_1'
                )

            _D_result = _O_importar.importar(
                s_pathArchivo = s_pathArchivo,
                s_nombreHoja = 'c_Colonia_2'
                )

            _D_result = _O_importar.importar(
                s_pathArchivo = s_pathArchivo,
                s_nombreHoja = 'c_Colonia_3'
                )
        else:
            _D_result = _O_importar.importar(
                s_pathArchivo = s_pathArchivo,
                s_nombreHoja = s_nombreHoja
                )

        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

        return _D_return

    pass


db01.define_table(
    'tcolonias',
    Field(
        'c_colonia', 'string', length = 6, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Clave colonia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'nombre', 'string', length = 500, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Nombre', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'codigopostal_id', db01.tcodigospostales, default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        label = 'Código Postal', comment = None,
        writable = False, readable = False,
        represent = stv_represent_referencefield
        ),
    Field(
        'c_codigopostal', 'string', length = 5, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Codigo Postal', comment = 'Código Postal',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'fecha_inicovigencia', 'date', default = request.browsernow,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha inicio vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'fecha_finvigencia', 'date', default = None,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha fin vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    tSignature_db01,
    format = '%(c_colonia)s %(nombre)s [%(c_codigopostal)s]',
    singular = 'Colonia',
    plural = 'Colonias',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TAUTO_CONFIGAUTOTRANSPORTES:

    @staticmethod
    def DBTABLA():
        return db01.tauto_configautotransportes

    @classmethod
    def IMPORTAR_EXCEL(
            cls,
            s_pathArchivo,
            s_nombreHoja = None,
            **D_otros
            ):
        _ = D_otros
        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTabla = cls.DBTABLA()
        _O_importar = IMPORTAR_EXCEL(
            dbTabla = _dbTabla,
            b_insertar_noRelacionados = True,
            b_ignorar_noRelacionados = False,
            b_insertarTodos = False,
            fnIdentificarHeader = None,
            alValidar = None,
            alAceptar = None
            )
        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = None,
            dbCampo = _dbTabla.c_configautotransporte,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Clave nomenclatura",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.descripcion,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Descripción",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.numeroejes,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Número de ejes",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.numerollantas,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Número de llantas",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_inicovigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de inicio de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_finvigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de fin de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _D_result = _O_importar.importar(
            s_pathArchivo = s_pathArchivo,
            s_nombreHoja = s_nombreHoja or 'c_ConfigAutotransporte'
            )

        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

        return _D_return

    pass


db01.define_table(
    'tauto_configautotransportes',
    Field(
        'c_configautotransporte', 'string', length = 10, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Clave config. autotransporte', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 500, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Descripción', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'numeroejes', 'string', length = 20, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Número de ejes', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'numerollantas', 'string', length = 20, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Número de llantas', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),

    Field(
        'fecha_inicovigencia', 'date', default = request.browsernow,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha inicio vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'fecha_finvigencia', 'date', default = None,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha fin vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    tSignature_db01,
    format = '%(descripcion)s [%(c_configautotransporte)s]',
    singular = 'Config. Autotransporte',
    plural = 'Config. Autotransportes',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TAUTO_TIPOSREMOLQUES:

    @classmethod
    def IMPORTAR_EXCEL(
            cls,
            s_pathArchivo,
            s_nombreHoja = None,
            **D_otros
            ):
        _ = D_otros
        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTabla = db01.tauto_tiposremolques
        _O_importar = IMPORTAR_EXCEL(
            dbTabla = _dbTabla,
            b_insertar_noRelacionados = True,
            b_ignorar_noRelacionados = False,
            b_insertarTodos = False,
            fnIdentificarHeader = None,
            alValidar = None,
            alAceptar = None
            )
        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = None,
            dbCampo = _dbTabla.c_subtiporem,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Clave tipo remolque",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.descripcion,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Remolque o semirremolque",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_inicovigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de inicio de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_finvigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de fin de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _D_result = _O_importar.importar(
            s_pathArchivo = s_pathArchivo,
            s_nombreHoja = s_nombreHoja or 'c_SubTipoRem'
            )

        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

        return _D_return

    pass


db01.define_table(
    'tauto_tiposremolques',
    Field(
        'c_subtiporem', 'string', length = 10, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Clave tipo remolque', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 500, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Descripción', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),

    Field(
        'fecha_inicovigencia', 'date', default = request.browsernow,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha inicio vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'fecha_finvigencia', 'date', default = None,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha fin vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    tSignature_db01,
    format = '%(descripcion)s [%(c_subtiporem)s]',
    singular = 'Tipo Remolque',
    plural = 'Tipos de Remolques',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TMAR_CONFIGMARITIMAS:

    @classmethod
    def IMPORTAR_EXCEL(
            cls,
            s_pathArchivo,
            s_nombreHoja = None,
            **D_otros
            ):
        _ = D_otros
        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTabla = db01.tmar_configmaritimas
        _O_importar = IMPORTAR_EXCEL(
            dbTabla = _dbTabla,
            b_insertar_noRelacionados = True,
            b_ignorar_noRelacionados = False,
            b_insertarTodos = False,
            fnIdentificarHeader = None,
            alValidar = None,
            alAceptar = None
            )
        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = None,
            dbCampo = _dbTabla.c_configmaritima,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Clave configuración marítima",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.descripcion,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Descripción",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_inicovigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de inicio de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_finvigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de fin de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _D_result = _O_importar.importar(
            s_pathArchivo = s_pathArchivo,
            s_nombreHoja = s_nombreHoja or 'c_ConfigMaritima'
            )

        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

        return _D_return

    pass


db01.define_table(
    'tmar_configmaritimas',
    Field(
        'c_configmaritima', 'string', length = 10, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Clave config. maritima', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 500, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Descripción', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),

    Field(
        'fecha_inicovigencia', 'date', default = request.browsernow,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha inicio vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'fecha_finvigencia', 'date', default = None,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha fin vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    tSignature_db01,
    format = '%(descripcion)s [%(c_configmaritima)s]',
    singular = 'Config. Maritima',
    plural = 'Config. Maritimas',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TMAR_TIPOSCARGAS:

    @classmethod
    def IMPORTAR_EXCEL(
            cls,
            s_pathArchivo,
            s_nombreHoja = None,
            **D_otros
            ):
        _ = D_otros
        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTabla = db01.tmar_tiposcargas
        _O_importar = IMPORTAR_EXCEL(
            dbTabla = _dbTabla,
            b_insertar_noRelacionados = True,
            b_ignorar_noRelacionados = False,
            b_insertarTodos = False,
            fnIdentificarHeader = None,
            alValidar = None,
            alAceptar = None
            )
        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = None,
            dbCampo = _dbTabla.c_clavetipocarga,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Clave tipo carga",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.descripcion,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Descripción",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_inicovigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha inicio de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_finvigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha fin de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _D_result = _O_importar.importar(
            s_pathArchivo = s_pathArchivo,
            s_nombreHoja = s_nombreHoja or 'c_ClaveTipoCarga'
            )

        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

        return _D_return

    pass


db01.define_table(
    'tmar_tiposcargas',
    Field(
        'c_clavetipocarga', 'string', length = 10, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Clave tipo carga', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 500, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Descripción', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),

    Field(
        'fecha_inicovigencia', 'date', default = request.browsernow,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha inicio vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'fecha_finvigencia', 'date', default = None,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha fin vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    tSignature_db01,
    format = '%(descripcion)s [%(c_clavetipocarga)s]',
    singular = 'Tipo Carga',
    plural = 'Tipo Cargas',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TMAR_CONTENEDORESMARITIMOS:

    @classmethod
    def IMPORTAR_EXCEL(
            cls,
            s_pathArchivo,
            s_nombreHoja = None,
            **D_otros
            ):
        _ = D_otros
        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTabla = db01.tmar_contenedoresmaritimos
        _O_importar = IMPORTAR_EXCEL(
            dbTabla = _dbTabla,
            b_insertar_noRelacionados = True,
            b_ignorar_noRelacionados = False,
            b_insertarTodos = False,
            fnIdentificarHeader = None,
            alValidar = None,
            alAceptar = None
            )
        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = None,
            dbCampo = _dbTabla.c_contenedormaritimo,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Clave del contenedor marítimo",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.descripcion,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Descripción",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_inicovigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de inicio de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_finvigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de fin de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _D_result = _O_importar.importar(
            s_pathArchivo = s_pathArchivo,
            s_nombreHoja = s_nombreHoja or 'c_ContenedorMaritimo'
            )

        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

        return _D_return

    pass


db01.define_table(
    'tmar_contenedoresmaritimos',
    Field(
        'c_contenedormaritimo', 'string', length = 10, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Clave contenedor marítimo', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 500, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Descripción', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),

    Field(
        'fecha_inicovigencia', 'date', default = request.browsernow,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha inicio vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'fecha_finvigencia', 'date', default = None,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha fin vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    tSignature_db01,
    format = '%(descripcion)s [%(c_contenedormaritimo)s]',
    singular = 'Contenedor Marítimo',
    plural = 'Contenedores Marítimos',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TMAR_AUTORIZACIONESNAVIERAS:

    @classmethod
    def IMPORTAR_EXCEL(
            cls,
            s_pathArchivo,
            s_nombreHoja = None,
            **D_otros
            ):
        _ = D_otros
        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTabla = db01.tmar_autorizacionesnavieras
        _O_importar = IMPORTAR_EXCEL(
            dbTabla = _dbTabla,
            b_insertar_noRelacionados = True,
            b_ignorar_noRelacionados = False,
            b_insertarTodos = False,
            fnIdentificarHeader = None,
            alValidar = None,
            alAceptar = None
            )
        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = None,
            dbCampo = _dbTabla.c_numautorizacionnaviero,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Número de autorización",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_inicovigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Inicio de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_finvigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fin de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _D_result = _O_importar.importar(
            s_pathArchivo = s_pathArchivo,
            s_nombreHoja = s_nombreHoja or 'c_NumAutorizacionNaviero'
            )

        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

        return _D_return

    pass


db01.define_table(
    'tmar_autorizacionesnavieras',
    Field(
        'c_numautorizacionnaviero', 'string', length = 50, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Número Autorización Naviero', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),

    Field(
        'fecha_inicovigencia', 'date', default = request.browsernow,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha inicio vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'fecha_finvigencia', 'date', default = None,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha fin vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    tSignature_db01,
    format = '%(c_numautorizacionnaviero)s',
    singular = 'Autorización Naviero',
    plural = 'Autorizaciones Navieras',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TAERO_TRANSPORTESAEREOS:

    @classmethod
    def IMPORTAR_EXCEL(
            cls,
            s_pathArchivo,
            s_nombreHoja = None,
            **D_otros
            ):
        _ = D_otros
        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTabla = db01.taero_transportesaereos
        _O_importar = IMPORTAR_EXCEL(
            dbTabla = _dbTabla,
            b_insertar_noRelacionados = True,
            b_ignorar_noRelacionados = False,
            b_insertarTodos = False,
            fnIdentificarHeader = None,
            alValidar = None,
            alAceptar = None
            )
        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = None,
            dbCampo = _dbTabla.c_codigotransporteaereo,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Clave identificación",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.nacionalidad,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Nacionalidad",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.nombreaerolinia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Nombre de la aerolínea",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.designadoroaci,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Designador OACI",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_inicovigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de inicio de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_finvigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de fin de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _D_result = _O_importar.importar(
            s_pathArchivo = s_pathArchivo,
            s_nombreHoja = s_nombreHoja or 'c_CodigoTransporteAereo'
            )

        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

        return _D_return

    pass


db01.define_table(
    'taero_transportesaereos',
    Field(
        'c_codigotransporteaereo', 'string', length = 10, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Código Transporte Aéreo', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'nacionalidad', 'string', length = 50, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Nacionalidad', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'nombreaerolinia', 'string', length = 200, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Nombre Aerolínea', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'designadoroaci', 'string', length = 10, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Designador OACI', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),

    Field(
        'fecha_inicovigencia', 'date', default = request.browsernow,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha inicio vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'fecha_finvigencia', 'date', default = None,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha fin vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    tSignature_db01,
    format = '%(nombreaerolinia)s [%(c_codigotransporteaereo)s]',
    singular = 'Transporte Aéreo',
    plural = 'Transportes Aéreos',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TFERR_PRODSERVSTCC:

    @classmethod
    def IMPORTAR_EXCEL(
            cls,
            s_pathArchivo,
            s_nombreHoja = None,
            **D_otros
            ):
        _ = D_otros
        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTabla = db01.tferr_prodservstcc
        _O_importar = IMPORTAR_EXCEL(
            dbTabla = _dbTabla,
            b_insertar_noRelacionados = True,
            b_ignorar_noRelacionados = False,
            b_insertarTodos = False,
            fnIdentificarHeader = None,
            alValidar = None,
            alAceptar = None
            )
        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = None,
            dbCampo = _dbTabla.c_claveprodstcc,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Clave STCC",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.descripcion,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Descripción",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_inicovigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de inicio de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_finvigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de fin de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _D_result = _O_importar.importar(
            s_pathArchivo = s_pathArchivo,
            s_nombreHoja = s_nombreHoja or 'c_ClaveProdSTCC'
            )

        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

        return _D_return

    pass


db01.define_table(
    'tferr_prodservstcc',
    Field(
        'c_claveprodstcc', 'string', length = 10, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Clave Prod. Serv. STCC', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 200, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Descripción', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),

    Field(
        'fecha_inicovigencia', 'date', default = request.browsernow,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha inicio vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'fecha_finvigencia', 'date', default = None,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha fin vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    tSignature_db01,
    format = '%(descripcion)s [%(c_claveprodstcc)s]',
    singular = 'Prod. Serv. STCC',
    plural = 'Prod. Serv. STCC',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TFERR_TIPOSSERVICIOS:

    @classmethod
    def IMPORTAR_EXCEL(
            cls,
            s_pathArchivo,
            s_nombreHoja = None,
            **D_otros
            ):
        _ = D_otros
        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTabla = db01.tferr_tiposservicios
        _O_importar = IMPORTAR_EXCEL(
            dbTabla = _dbTabla,
            b_insertar_noRelacionados = True,
            b_ignorar_noRelacionados = False,
            b_insertarTodos = False,
            fnIdentificarHeader = None,
            alValidar = None,
            alAceptar = None
            )
        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = None,
            dbCampo = _dbTabla.c_tipodeservicio,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Clave",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.descripcion,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Descripción",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_inicovigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de inicio de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_finvigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de fin de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _D_result = _O_importar.importar(
            s_pathArchivo = s_pathArchivo,
            s_nombreHoja = s_nombreHoja or 'c_TipoDeServicio'
            )

        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

        return _D_return

    pass


db01.define_table(
    'tferr_tiposservicios',
    Field(
        'c_tipodeservicio', 'string', length = 10, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Clave Tipo de Servicio', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 200, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Descripción', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),

    Field(
        'fecha_inicovigencia', 'date', default = request.browsernow,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha inicio vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'fecha_finvigencia', 'date', default = None,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha fin vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    tSignature_db01,
    format = '%(descripcion)s [%(c_tipodeservicio)s]',
    singular = 'Tipo de Servicio',
    plural = 'Tipos de Servicios',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TFERR_DERECHOSPASO:

    class OTORGARECIBE:
        """ Se definen los valores posible del campo, en base a lo requerido por el SAT """
        NO_DEFINIDO = ''
        OTORGA = 'Otorga'
        RECIBE = 'Recibe'
        _D_dict = None

        @classmethod
        def GET_DICT(cls):
            if not cls._D_dict:
                cls._D_dict = {
                    cls.NO_DEFINIDO: 'No Definido',
                    cls.OTORGA     : 'Otorga',
                    cls.RECIBE     : 'Recibe',
                    }
            else:
                pass
            return cls._D_dict

    @classmethod
    def IMPORTAR_EXCEL(
            cls,
            s_pathArchivo,
            s_nombreHoja = None,
            **D_otros
            ):
        _ = D_otros
        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTabla = db01.tferr_derechospaso
        _O_importar = IMPORTAR_EXCEL(
            dbTabla = _dbTabla,
            b_insertar_noRelacionados = True,
            b_ignorar_noRelacionados = False,
            b_insertarTodos = False,
            fnIdentificarHeader = None,
            alValidar = None,
            alAceptar = None
            )
        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = None,
            dbCampo = _dbTabla.c_derechosdepaso,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Clave del derecho de paso",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.derechodepaso,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Derecho de paso",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.entre,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Entre",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.hasta,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Hasta",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.otorgarecibe,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Otorga/Recibe",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.concesionario,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Concesionario",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_inicovigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de inicio de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_finvigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de fin de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _D_result = _O_importar.importar(
            s_pathArchivo = s_pathArchivo,
            s_nombreHoja = s_nombreHoja or 'c_DerechosDePaso'
            )

        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

        return _D_return

    pass


db01.define_table(
    'tferr_derechospaso',
    Field(
        'c_derechosdepaso', 'string', length = 10, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Clave Derecho de Paso', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'derechodepaso', 'string', length = 10, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Derecho de paso', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),

    Field(
        'entre', 'string', length = 200, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Entre', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'hasta', 'string', length = 200, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Hasta', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'otorgarecibe', 'string', length = 50, default = TFERR_DERECHOSPASO.OTORGARECIBE.NO_DEFINIDO,
        required = False, requires = IS_NULL_OR(
            IS_IN_SET(
                TFERR_DERECHOSPASO.OTORGARECIBE.GET_DICT(), zero = 0, error_message = 'Selecciona'
                )
            ),
        notnull = False, unique = False,
        widget = stv_widget_combobox, label = 'Otorga/Recibe', comment = '',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TFERR_DERECHOSPASO.OTORGARECIBE.GET_DICT())
        ),
    Field(
        'concesionario', 'string', length = 600, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Concesionario', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),

    Field(
        'fecha_inicovigencia', 'date', default = request.browsernow,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha inicio vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'fecha_finvigencia', 'date', default = None,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha fin vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    tSignature_db01,
    format = '%(descripcion)s [%(c_derechosdepaso)s]',
    singular = 'Derecho de Paso',
    plural = 'Derechos de Paso',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TFERR_TIPOSCARROS:

    @classmethod
    def IMPORTAR_EXCEL(
            cls,
            s_pathArchivo,
            s_nombreHoja = None,
            **D_otros
            ):
        _ = D_otros
        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTabla = db01.tferr_tiposcarros
        _O_importar = IMPORTAR_EXCEL(
            dbTabla = _dbTabla,
            b_insertar_noRelacionados = True,
            b_ignorar_noRelacionados = False,
            b_insertarTodos = False,
            fnIdentificarHeader = None,
            alValidar = None,
            alAceptar = None
            )
        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = None,
            dbCampo = _dbTabla.c_tipocarro,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Clave",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.nombre,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Tipo de carro",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.descripcion,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Descripción",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_inicovigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de inicio de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_finvigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de fin de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _D_result = _O_importar.importar(
            s_pathArchivo = s_pathArchivo,
            s_nombreHoja = s_nombreHoja or 'c_TipoCarro'
            )

        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

        return _D_return

    pass


db01.define_table(
    'tferr_tiposcarros',
    Field(
        'c_tipocarro', 'string', length = 10, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Clave Tipo de Carro', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'nombre', 'string', length = 100, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Nombre', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 200, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Descripción', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),

    Field(
        'fecha_inicovigencia', 'date', default = request.browsernow,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha inicio vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'fecha_finvigencia', 'date', default = None,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha fin vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    tSignature_db01,
    format = '%(nombre)s [%(c_tipocarro)s]',
    singular = 'Tipo de Carro',
    plural = 'Tipos de Carros',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )


class TFERR_TIPOSCONTENEDORES:

    @classmethod
    def IMPORTAR_EXCEL(
            cls,
            s_pathArchivo,
            s_nombreHoja = None,
            **D_otros
            ):
        _ = D_otros
        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        _dbTabla = db01.tferr_tiposcontenedores
        _O_importar = IMPORTAR_EXCEL(
            dbTabla = _dbTabla,
            b_insertar_noRelacionados = True,
            b_ignorar_noRelacionados = False,
            b_insertarTodos = False,
            fnIdentificarHeader = None,
            alValidar = None,
            alAceptar = None
            )
        _O_importar.agregar_campo_columna_relacion(
            n_id_columna = None,
            dbCampo = _dbTabla.c_tipocontenedor,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Clave",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.tipocontenedor,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Tipo de contenedor",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )
        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.descripcion,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
            s_nombreColumna = "Descripción",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_inicovigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de inicio de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _O_importar.agregar_campo_columna_importar(
            n_id_columna = None,
            dbCampo = _dbTabla.fecha_finvigencia,
            E_formato = IMPORTAR_EXCEL.E_FORMATO.FECHA,
            s_nombreColumna = "Fecha de fin de vigencia",
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None
            )

        _D_result = _O_importar.importar(
            s_pathArchivo = s_pathArchivo,
            s_nombreHoja = s_nombreHoja or 'c_Contenedor'
            )

        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

        return _D_return

    pass


db01.define_table(
    'tferr_tiposcontenedores',
    Field(
        'c_tipocontenedor', 'string', length = 10, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Clave Tipo de Contenedor', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'tipocontenedor', 'string', length = 100, default = None,
        required = True,
        notnull = True, unique = True,
        widget = stv_widget_input, label = 'Tipo de contenedor', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', length = 200, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Descripción', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),

    Field(
        'fecha_inicovigencia', 'date', default = request.browsernow,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha inicio vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'fecha_finvigencia', 'date', default = None,
        required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha fin vigencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    tSignature_db01,
    format = '%(tipocontenedor)s [%(c_tipocontenedor)s]',
    singular = 'Tipo de Contenedor',
    plural = 'Tipos de Contenedores',
    migrate = D_stvSiteCfg.b_requestIsGenericApp
    )
