# -*- coding: utf-8 -*-


class TEMPRESA_REGISTROSPATRONALES(Table):
    """ Definición de la tabla de empresas_registrospatronales """

    S_NOMBRETABLA = 'tempresa_registrospatronales'

    class E_FRACCIONLEYIMMS:
        """ Se definen las opciónes del campo """
        NO_DEFINIDO = 0
        CLASE_I = 0
        CLASE_II = 1
        CLASE_III = 2
        CLASE_IV = 2
        CLASE_V = 2
        D_TODOS = {
            NO_DEFINIDO: 'No definido',
            CLASE_I    : 'Clase I',
            CLASE_II   : 'Clase II',
            CLASE_III  : 'Clase III',
            CLASE_IV   : 'Clase IV',
            CLASE_V    : 'Clase V',
            }
        
        @classmethod
        def GET_DICT(cls):
            return cls.D_TODOS

    L_DBCAMPOS = [
        Field(
            'empresa_id', dbc01.tempresas, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'Empresa', comment = None,
            writable = False, readable = False,
            represent = None
            ),
        Field(
            'registropatronal', 'string', length = 11, default = None,
            required = True,
            notnull = True, unique = True,
            widget = stv_widget_input, label = 'Registro Patronal', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'fechaaltaimss', 'date', default = request.browsernow,
            required = True, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
            notnull = True,
            widget = stv_widget_inputDate, label = 'Fecha Alta IMSS', comment = 'Fecha Alta IMSS',
            writable = True, readable = True,
            represent = stv_represent_date
            ),
        Field(
            'fechabajaimss', 'date',
            required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
            notnull = False,
            widget = stv_widget_inputDate, label = 'Fecha Baja IMSS', comment = 'Fecha Baja IMSS',
            writable = True, readable = True,
            represent = stv_represent_date
            ),
        Field(
            'fraccionleyimss', 'integer', default = 0,
            required = True, requires = IS_IN_SET(E_FRACCIONLEYIMMS.D_TODOS, zero = None, error_message = 'Selecciona'),
            notnull = True, unique = False,
            widget = stv_widget_combobox, label = 'Fracción de la Ley IMSS', comment = 'Fracción de la Ley IMSS',
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_string(
                TEMPRESA_REGISTROSPATRONALES.E_FRACCIONLEYIMMS.D_TODOS.get(v, 'None'), r
                )
            ),
        Field(
            'primariesgo', 'decimal(10,5)',
            required = True, requires = IS_FLOAT_IN_RANGE(0, 100, error_message = 'Fuera de rango %(min)g - %(max)g'),
            notnull = True, unique = False,
            widget = stv_widget_inputPercentage, label = 'Prima riesgo', comment = None,
            writable = True, readable = True,
            represent = stv_represent_percentage
            ),
        Field(
            'registroinfonacot', 'string', length = 18, default = None,
            required = True,
            notnull = True, unique = True,
            widget = stv_widget_input, label = 'Reg. INFONACOT', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        tSignature_dbc01,
        ]

    pass  # TEMPRESA_REGISTROSPATRONALES


dbc01.define_table(
    TEMPRESA_REGISTROSPATRONALES.S_NOMBRETABLA, *TEMPRESA_REGISTROSPATRONALES.L_DBCAMPOS,
    format = lambda r: "%s [%s]" % (
        r.registroPatronal,
        TEMPRESA_REGISTROSPATRONALES.E_FRACCIONLEYIMMS.D_TODOS.get(r.fraccionleyimss, ' ')
        ),
    singular = 'Registro patronal',
    plural = 'Registros patronales',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TEMPRESA_REGISTROSPATRONALES
    )


class TEMPRESA_PLAZA_SUCURSAL_CAJASCHICAS(Table):

    S_NOMBRETABLA = 'tempresa_plaza_sucursal_cajaschicas'

    L_DBCAMPOS = [
        Field(
            'empresa_plaza_sucursal_id', dbc01.tempresa_plaza_sucursales, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'Plaza : Sucursal', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r)
            ),
        Field(
            'nombre', 'string', length = 30, default = None,
            required = True,
            notnull = True, unique = False,
            widget = stv_widget_input, label = 'Nombre', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        # Para pagos en efectivo
        Field(
            'cuentacontable', 'string', length = 30, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Cuenta Contable', comment = 'Cuenta usada para pagos en efectivo',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'fondobase', 'decimal(10,2)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Fondo de caja', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),

        # Para pagos con terminal bancaria predeterminar a:
        Field(
            'empresa_cuentabancaria_terminal_id', dbc01.tempresa_cuentabancaria_terminales, default = None,
            required = False,
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Terminal Predeterminada',
            comment = 'Terminal predeterminada al momento de registrarse un pago con tarjeta',
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r)
            ),

        # Para pagos con cheques predeterminar a:
        Field(
            'empresa_cuentabancaria_id_cheques', dbc01.tempresa_cuentasbancarias, default = None,
            required = False,
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Cuenta Predeterminada Cheques',
            comment = 'Cuenta bancaria predeterminada al momento de registrarse un pago por cheque',
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r)
            ),

        # Para pagos con transferencias predeterminar a:
        Field(
            'empresa_cuentabancaria_id_transferencia', dbc01.tempresa_cuentasbancarias, default = None,
            required = False,
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Cuenta Predeterminada Transferencias',
            comment = 'Cuenta bancaria predeterminada al momento de registrarse un pago por transferencia electrónica',
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r)
            ),
        STV_LIB_DB.DEFINIR_DBCAMPO(
            dbc01.tempresa_series, 'empresa_serie_ingreso_id',
            dbQry = (dbc01.tempresa_series.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id),
            label = 'Serie de Ingreso', writable = True
            ),
        STV_LIB_DB.DEFINIR_DBCAMPO(
            dbc01.tempresa_series, 'empresa_serie_egreso_id',
            dbQry = (dbc01.tempresa_series.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id),
            label = 'Serie de Egreso', writable = True
            ),
        STV_LIB_DB.DEFINIR_DBCAMPO(
            dbc01.tempresa_series, 'empresa_serie_pago_id',
            dbQry = (dbc01.tempresa_series.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id),
            label = 'Serie de Pago', writable = True
            ),

        tSignature_dbc01,
        ]

    pass  # TEMPRESA_PLAZA_SUCURSAL_CAJASCHICAS


dbc01.define_table(
    TEMPRESA_PLAZA_SUCURSAL_CAJASCHICAS.S_NOMBRETABLA, *TEMPRESA_PLAZA_SUCURSAL_CAJASCHICAS.L_DBCAMPOS,
    format = lambda r: (TEMPRESA_PLAZA_SUCURSALES.getRowFormated(r.empresa_plaza_sucursal_id) + " : " + r.nombre),
    singular = 'Caja chica',
    plural = 'Cajas chicas',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TEMPRESA_PLAZA_SUCURSAL_CAJASCHICAS
    )

# Se crea la referencia para su uso en los campos
STV_FWK_LIB.DEFINIR_INTEGRIDADREFERENCIAL(
    dbc01.tempresa_plaza_sucursal_cajaschicas,
    dbc01.tempresa_recursoscomprobar.empresa_plaza_sucursal_cajachica_id,
    True
    )


class TEMPRESA_PLAZA_SUCURSAL_CONTABILIDADES:
    class ESTILOPOLIZA:
        NO_DEFINIDO = 0
        POR_CFDI = 1
        ACUMULADA_DESGLOZE_POR_CFDI = 2
        #ACUMULADA_DESGLOZE_POR_CUENTACONTABLE = 3
        
        @classmethod
        def get_dict(cls):
            return {
                cls.POR_CFDI : 'Póliza contable independiente por cada CFDI',
                cls.ACUMULADA_DESGLOZE_POR_CFDI : 'Póliza contable acumulada diaria con asientos independientes por cada CFDI',
                #cls.ACUMULADA_DESGLOZE_POR_CUENTACONTABLE : ('Póliza contable acumulada diaria con asientos concentrados por cuenta contable'),
                }

    pass


# Existe validación para no duplicar ejercicio-sucursal
dbc01.define_table(
    'tempresa_plaza_sucursal_contabilidades',
    Field('empresa_plaza_sucursal_id', dbc01.tempresa_plaza_sucursales, default=None,
      required=True,
      ondelete='NO ACTION', notnull=True, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label= 'Sucursal', comment=None,
      writable=False, readable=True,
      represent = stv_represent_referencefield),
    Field('ejercicio', 'integer', default = None,
      required = True, requires = IS_IN_SET(VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id), zero = 0, error_message = 'Selecciona'),
      notnull = False, unique = False,
      widget = stv_widget_combobox, 
      label = 'Ejercicio inicial', comment = None,
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id))),
    Field('empresa_tipopoliza_ingreso_id', dbc01.tempresa_tipospoliza, default=None,
      required=False,
      ondelete='NO ACTION', notnull=False, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label= 'Tipos de poliza CONTPAQi - CFDIs de Ingreso', comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield(v, r)),
    Field('empresa_tipopoliza_pago_id', dbc01.tempresa_tipospoliza, default=None,
      required=False,
      ondelete='NO ACTION', notnull=False, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label= 'Tipos de poliza CONTPAQi - CFDIs de Pago', comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield(v, r)),
    Field('empresa_tipopoliza_egreso_id', dbc01.tempresa_tipospoliza, default=None,
      required=False,
      ondelete='NO ACTION', notnull=False, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label= 'Tipos de poliza CONTPAQi - CFDIs de Egreso', comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield(v, r)), 
    
    Field('empresa_diario_id', dbc01.tempresa_diarios, default=None,
      required=False, requires = IS_NULL_OR(IS_IN_DB(dbc01(dbc01.tempresa_diarios.tipo == TEMPRESA_DIARIOS.E_TIPO.DE_POLIZA),'tempresa_diarios.id', dbc01.tempresa_diarios._format)),
      ondelete='NO ACTION', notnull=False, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label= 'Diarios especiales CONTPAQi', comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield(v, r)),
    Field('empresa_segmento_id', dbc01.tempresa_segmentos, default=None,
      required=False,
      ondelete='NO ACTION', notnull=False, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label= 'Segmentos de negocio CONTPAQi', comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield(v, r)),

    Field('estilopoliza', 'integer', default = TEMPRESA_PLAZA_SUCURSAL_CONTABILIDADES.ESTILOPOLIZA.POR_CFDI,
      required = True, requires=IS_IN_SET(TEMPRESA_PLAZA_SUCURSAL_CONTABILIDADES.ESTILOPOLIZA.get_dict(), zero=0, error_message= 'Selecciona'),
      notnull = True, unique = False,
      widget = stv_widget_combobox, label = 'Estilo de Póliza', comment = '',
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_PLAZA_SUCURSAL_CONTABILIDADES.ESTILOPOLIZA.get_dict())
      ),
    
    Field('concepto_ingreso', 'string', length=100, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Concepto', comment=('''Es posible utilizar la siguiente lista de comodines:
        $Ejercicio : Este comodin será sustituido por el ejercicio (año)  de la poliza.  Ejemplo 2019
        $Periodo   : Este comodin será sustituido por el Periodo (mes) de la poliza,   Ejemplos  01,02,03...12
        $Mes       : Este comodin será sustituido por el nombre del mes (Periodo)    Ejemplo  ENERO, FEBRERO, ... DICIEMBRE
        $Dia       : Este comodi será sustituido por el dia del mes al que pertenece la poliza   Ejemplo 01,02....31
        $Plaza     : Nombre de la plaza donde corresponde la poliza contable   Ejemplo  CEDIS, MATRIZ
        $Sucursal  : Nombre de la sucursal donde corresponde la poliza contable  Ejemplo  MAZATLAN
        $Cajachica : Nombre de la caja chica a la que pertenece la poliza contable 
        $CFDI      : Serie + Folio
      '''),
      writable=True, readable=True,
      represent=stv_represent_string),   
    Field('concepto_pago', 'string', length=100, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Concepto', comment=('''Es posible utilizar la siguiente lista de comodines:
        $Ejercicio : Este comodin será sustituido por el ejercicio (año)  de la poliza.  Ejemplo 2019
        $Periodo   : Este comodin será sustituido por el Periodo (mes) de la poliza,   Ejemplos  01,02,03...12
        $Mes       : Este comodin será sustituido por el nombre del mes (Periodo)    Ejemplo  ENERO, FEBRERO, ... DICIEMBRE
        $Dia       : Este comodi será sustituido por el dia del mes al que pertenece la poliza   Ejemplo 01,02....31
        $Plaza     : Nombre de la plaza donde corresponde la poliza contable   Ejemplo  CEDIS, MATRIZ
        $Sucursal  : Nombre de la sucursal donde corresponde la poliza contable  Ejemplo  MAZATLAN
        $Cajachica : Nombre de la caja chica a la que pertenece la poliza contable 
        $CFDI      : Serie + Folio
      '''),
      writable=True, readable=True,
      represent=stv_represent_string),   
    Field('concepto_egreso', 'string', length=100, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Concepto', comment=('''Es posible utilizar la siguiente lista de comodines:
        $Ejercicio : Este comodin será sustituido por el ejercicio (año)  de la poliza.  Ejemplo 2019
        $Periodo   : Este comodin será sustituido por el Periodo (mes) de la poliza,   Ejemplos  01,02,03...12
        $Mes       : Este comodin será sustituido por el nombre del mes (Periodo)    Ejemplo  ENERO, FEBRERO, ... DICIEMBRE
        $Dia       : Este comodi será sustituido por el dia del mes al que pertenece la poliza   Ejemplo 01,02....31
        $Plaza     : Nombre de la plaza donde corresponde la poliza contable   Ejemplo  CEDIS, MATRIZ
        $Sucursal  : Nombre de la sucursal donde corresponde la poliza contable  Ejemplo  MAZATLAN
        $Cajachica : Nombre de la caja chica a la que pertenece la poliza contable 
        $CFDI      : Serie + Folio
      '''),
      writable=True, readable=True,
      represent=stv_represent_string),   

    tSignature_dbc01,
    format='%(nombrecorto)s',
    singular='Contabilidad Sucursal',
    plural='Contabilidades Sucursal',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

TEMPRESA_PLAZA_SUCURSAL_CONTABILIDADES = TEMPRESA_PLAZA_SUCURSAL_CONTABILIDADES() 

class TEMPRESA_PLAZA_SUCURSAL_CAJACHICA_GUIAS:
    pass
dbc01.define_table(
    'tempresa_plaza_sucursal_cajachica_guias',
    Field('empresa_plaza_sucursal_cajaschicas_id', dbc01.tempresa_plaza_sucursal_cajaschicas, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Plaza', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('ejercicio', 'integer', default = None,
      required = True, requires = IS_IN_SET(VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id), zero = 0, error_message = 'Selecciona'),
      notnull = False, unique = False,
      widget = stv_widget_combobox, 
      label = 'Ejercicio inicial', comment = None,
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id))),
    Field('moneda_id', 'integer', default=D_stvSiteHelper.dbconfigs.defaults.moneda_id,
      required=True, requires = IS_IN_DB(db01, 'tmonedas.id', db01.tmonedas._format), 
      ondelete='NO ACTION', notnull=True, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label= 'Moneda', comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_plaza_sucursal_cajachica_guias.moneda_id)),

    #Este será el campo que se utilice en vez de moneda_id
    Field('monedacontpaqi_id', 'integer', default=D_stvSiteHelper.dbconfigs.defaults.moneda_contpaqi_id,
      required=True, requires = IS_IN_DB(dbc01, 'tempresa_monedascontpaqi.id', dbc01.tempresa_monedascontpaqi._format), 
      ondelete='NO ACTION', notnull=True, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label= 'Moneda CONTPAQi', comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_plaza_sucursal_cajachica_guias.monedacontpaqi_id)),

    
    Field('cuenta_contable_cajachica', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cuenta contable caja chica', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuenta_contable_cajachica_complemento', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cuenta contable caja chica complemento', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuenta_contable_cajachica_sobrante', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cuenta contable caja chica diferencia sobrante', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuenta_contable_cajachica_faltante', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cuenta contable caja chica diferencia faltante', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    tSignature_dbc01,
    format='%(cuenta_cliente)s',
    singular='Guía caja chica',
    plural='Guías cajas chicas',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

class TEMPRESA_INGRESOESTRUCTURA_GUIASCONTABLES:
    pass
dbc01.define_table(
    'tempresa_ingresoestructura_guiascontables',
    Field('empresa_ingresoestructura_id', dbc01.tempresa_ingresosestructura, default=None,
        required=True,
        ondelete='CASCADE', notnull=True, unique=False,
        label='Empresa Ingreso Est.', comment=None,
        writable=False, readable=False,
        represent=None),
    Field('ejercicio', 'integer', default = None,
      required = True, requires = IS_IN_SET(VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id), zero = 0, error_message = 'Selecciona'),
      notnull = False, unique = False,
      widget = stv_widget_combobox, 
      label = 'Ejercicio inicial', comment = None,
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id))),
    Field('cuentacontableingreso', 'string', length=30, default=None,
        required=False,
        notnull=False, unique=False,
        widget=stv_widget_input, label= 'Cta. Contable Ingreso (Factura Cobrada)', comment=None,
        writable=True, readable=True,
        represent=stv_represent_string),
    Field('cuentacontableingreso_nocobrado', 'string', length=30, default=None,
        required=False,
        notnull=False, unique=False,
        widget=stv_widget_input, label= 'Cta. Contable Ingreso (Factura No Cobrada)', comment=None,
        writable=True, readable=True,
        represent=stv_represent_string),
    Field('empresa_diario_cuentacontableingreso_id', dbc01.tempresa_diarios, default=None,
        required=False, requires = IS_NULL_OR(IS_IN_DB(dbc01(dbc01.tempresa_diarios.tipo != TEMPRESA_DIARIOS().E_TIPO.DE_POLIZA),'tempresa_diarios.id', dbc01.tempresa_diarios._format)),
        ondelete='NO ACTION', notnull=False, unique=False,
        widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label= 'Diarios cta. contable ingreso CONTPAQi', comment=None,
        writable=True, readable=True,
        represent=lambda v, r: stv_represent_referencefield(v, r)),

    Field('cuentacontabledescto', 'string', length=30, default=None,
        required=False,
        notnull=False, unique=False,
        widget=stv_widget_input, label= 'Cta. Contable Dcto. (Factura Cobrada)', comment= 'Si la cuenta contable para registrar el descuento, está vacía o es igual a la cuenta de ingreso, la contabilización de los ingresos serán neteados.',
        writable=True, readable=True,
        represent=stv_represent_string),
    Field('cuentacontabledescto_nocobrado', 'string', length=30, default=None,
        required=False,
        notnull=False, unique=False,
        widget=stv_widget_input, label= 'Cta. Contable Dcto. (Factura No Cobrada)', comment= 'Si la cuenta contable para registrar el descuento, está vacía o es igual a la cuenta de ingreso, la contabilización de los ingresos serán neteados.',
        writable=True, readable=True,
        represent=stv_represent_string),
    Field('empresa_diario_cuentacontabledescto_id', dbc01.tempresa_diarios, default=None,
        required=False, requires = IS_NULL_OR(IS_IN_DB(dbc01(dbc01.tempresa_diarios.tipo != TEMPRESA_DIARIOS().E_TIPO.DE_POLIZA),'tempresa_diarios.id', dbc01.tempresa_diarios._format)),
        ondelete='NO ACTION', notnull=False, unique=False,
        widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label= 'Diarios cta. contable dcto. CONTPAQi', comment=None,
        writable=True, readable=True,
        represent=lambda v, r: stv_represent_referencefield(v, r)),

    Field('cuentacontable_egresodescuento', 'string', length=30, default=None,
        required=False,
        notnull=False, unique=False,
        widget=stv_widget_input, label= 'Cta. Egreso Dcto. (Nota de Crédito)', comment=None,
        writable=True, readable=True,
        represent=stv_represent_string),
    Field('empresa_diario_cuentacontableegresodescuento_id', dbc01.tempresa_diarios, default=None,
        required=False, requires = IS_NULL_OR(IS_IN_DB(dbc01(dbc01.tempresa_diarios.tipo != TEMPRESA_DIARIOS().E_TIPO.DE_POLIZA),'tempresa_diarios.id', dbc01.tempresa_diarios._format)),
        ondelete='NO ACTION', notnull=False, unique=False,
        widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label= 'Diarios cta. contable egreso dcto. CONTPAQi', comment=None,
        writable=True, readable=True,
        represent=lambda v, r: stv_represent_referencefield(v, r)),

    Field('cuentacontable_egresobonificacion', 'string', length=30, default=None,
        required=False,
        notnull=False, unique=False,
        widget=stv_widget_input, label= 'Cta. Egreso Bonif. (Nota de Crédito)', comment=None,
        writable=True, readable=True,
        represent=stv_represent_string),
    Field('empresa_diario_cuentacontableegresobonificacion_id', dbc01.tempresa_diarios, default=None,
        required=False, requires = IS_NULL_OR(IS_IN_DB(dbc01(dbc01.tempresa_diarios.tipo != TEMPRESA_DIARIOS().E_TIPO.DE_POLIZA),'tempresa_diarios.id', dbc01.tempresa_diarios._format)),
        ondelete='NO ACTION', notnull=False, unique=False,
        widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label= 'Diarios cta. contable egreso bonificación CONTPAQi', comment=None,
        writable=True, readable=True,
        represent=lambda v, r: stv_represent_referencefield(v, r)),

    Field('cuentacontable_egresodevolucion', 'string', length=30, default=None,
        required=False,
        notnull=False, unique=False,
        widget=stv_widget_input, label= 'Cta. Egreso Devolución (Nota de Crédito)', comment=None,
        writable=True, readable=True,
        represent=stv_represent_string),
    Field('empresa_diario_cuentacontableegresodevolucion_id', dbc01.tempresa_diarios, default=None,
        required=False, requires = IS_NULL_OR(IS_IN_DB(dbc01(dbc01.tempresa_diarios.tipo != TEMPRESA_DIARIOS().E_TIPO.DE_POLIZA),'tempresa_diarios.id', dbc01.tempresa_diarios._format)),
        ondelete='NO ACTION', notnull=False, unique=False,
        widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label= 'Diarios cta. contable egreso devolución CONTPAQi', comment=None,
        writable=True, readable=True,
        represent=lambda v, r: stv_represent_referencefield(v, r)),

    tSignature_dbc01,
    format='%(empresa_ingresoestructura_id)s',
    singular='Guia Contable',
    plural='Guias Contables',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

class TEMPRESA_PLAZA_SUCURSAL_CONTABILIDAD_GUIASCONTABLES:
    pass
dbc01.define_table(
    'tempresa_plaza_sucursal_contabilidad_guiascontables',
    Field('empresa_plaza_sucursal_contabilidad_id', dbc01.tempresa_plaza_sucursal_contabilidades, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Empresa Serie Suc.', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('empresa_ingresoestructura_id', dbc01.tempresa_ingresosestructura, default=None,
      required=False,
      ondelete='NO ACTION', notnull=False, unique=False,
      widget=lambda f, v: stv_widget_db_chosen_tree(f, v, 1, D_additionalAttributes={
                                                    'dbTable_tree': dbc01.tempresa_ingresosestructura,
                                                    'dbField_order': dbc01.tempresa_ingresosestructura.orden,
                                                    'dbField_tree_groupby': dbc01.tempresa_ingresosestructura.padre_id,
                                                    'class_dbTree': DB_Tree,
                                                    'b_onlyLastNodes' : True,
                                                    }), 
      label= 'Ingreso Estructura', comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield_tree(v, r, dbc01.tempresa_ingresosestructura.padre_id, dbc01.tempresa_plaza_sucursal_contabilidad_guiascontables.empresa_ingresoestructura_id)),  
    Field('cuentacontableingreso', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cta. Contable Ingreso (Factura Cobrada)', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuentacontableingreso_nocobrado', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cta. Contable Ingreso (Factura No Cobrada)', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('empresa_diario_cuentacontableingreso_id', dbc01.tempresa_diarios, default=None,
      required=False, requires = IS_NULL_OR(IS_IN_DB(dbc01(dbc01.tempresa_diarios.tipo == TEMPRESA_DIARIOS().E_TIPO.DE_MOVIMIENTOS),'tempresa_diarios.id', dbc01.tempresa_diarios._format)),
      ondelete='NO ACTION', notnull=False, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label= 'Diarios cta. contable ingreso CONTPAQi', comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield(v, r)),

    Field('cuentacontabledescto', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cta. Contable Dcto. (Factura Cobrada)', comment= 'Si la cuenta contable para registrar el descuento, está vacía o es igual a la cuenta de ingreso, la contabilización de los ingresos serán neteados.',
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuentacontabledescto_nocobrado', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cta. Contable Dcto. (Factura No Cobrada)', comment= 'Si la cuenta contable para registrar el descuento, está vacía o es igual a la cuenta de ingreso, la contabilización de los ingresos serán neteados.',
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('empresa_diario_cuentacontabledescto_id', dbc01.tempresa_diarios, default=None,
      required=False, requires = IS_NULL_OR(IS_IN_DB(dbc01(dbc01.tempresa_diarios.tipo == TEMPRESA_DIARIOS().E_TIPO.DE_MOVIMIENTOS),'tempresa_diarios.id', dbc01.tempresa_diarios._format)),
      ondelete='NO ACTION', notnull=False, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label= 'Diarios cta. contable dcto. CONTPAQi', comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield(v, r)),

    Field('cuentacontable_egresodescuento', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cta. Egreso Dcto. (Nota de Crédito)', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('empresa_diario_cuentacontableegresodescuento_id', dbc01.tempresa_diarios, default=None,
      required=False, requires = IS_NULL_OR(IS_IN_DB(dbc01(dbc01.tempresa_diarios.tipo == TEMPRESA_DIARIOS().E_TIPO.DE_MOVIMIENTOS),'tempresa_diarios.id', dbc01.tempresa_diarios._format)),
      ondelete='NO ACTION', notnull=False, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label= 'Diarios cta. contable egreso dcto. CONTPAQi', comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield(v, r)),

    Field('cuentacontable_egresobonificacion', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cta. Egreso Bonif. (Nota de Crédito)', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('empresa_diario_cuentacontableegresobonificacion_id', dbc01.tempresa_diarios, default=None,
      required=False, requires = IS_NULL_OR(IS_IN_DB(dbc01(dbc01.tempresa_diarios.tipo == TEMPRESA_DIARIOS().E_TIPO.DE_MOVIMIENTOS),'tempresa_diarios.id', dbc01.tempresa_diarios._format)),
      ondelete='NO ACTION', notnull=False, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label= 'Diarios cta. contable egreso bonificación CONTPAQi', comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield(v, r)),

    Field('cuentacontable_egresodevolucion', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cta. Egreso Devolución (Nota de Crédito)', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('empresa_diario_cuentacontableegresodevolucion_id', dbc01.tempresa_diarios, default=None,
      required=False, requires = IS_NULL_OR(IS_IN_DB(dbc01(dbc01.tempresa_diarios.tipo == TEMPRESA_DIARIOS().E_TIPO.DE_MOVIMIENTOS),'tempresa_diarios.id', dbc01.tempresa_diarios._format)),
      ondelete='NO ACTION', notnull=False, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label= 'Diarios cta. contable egreso devolución CONTPAQi', comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield(v, r)),
    
    Field('cuentacontable_costoventa', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cta. contable Costo de ventas', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('empresa_diario_cuentacontablecostoventa_id', dbc01.tempresa_diarios, default=None,
      required=False, requires = IS_NULL_OR(IS_IN_DB(dbc01(dbc01.tempresa_diarios.tipo == TEMPRESA_DIARIOS().E_TIPO.DE_MOVIMIENTOS),'tempresa_diarios.id', dbc01.tempresa_diarios._format)),
      ondelete='NO ACTION', notnull=False, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label= 'Diarios cta. contable costo de venta CONTPAQi', comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield(v, r)),

    tSignature_dbc01,
    format='%(empresa_ingresoestructura_id)s',
    singular='Guia Contable',
    plural='Guias Contables',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

class TEMPRESA_IVA:
    pass
dbc01.define_table(
    'tempresa_iva',
    Field('empresa_id', dbc01.tempresas, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Empresa', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('ejercicio', 'integer', default = None,
      required = True, requires = IS_IN_SET(VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id), zero = 0, error_message = 'Selecciona'),
      notnull = False, unique = False,
      widget = stv_widget_combobox, 
      label = 'Ejercicio', comment = None,
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id))),
    Field('tasa', 'decimal(4,2)', default=0,
      required=True,
      notnull=True, unique=False,
      widget=stv_widget_inputPercentage, label='Tasa', comment=None,
      writable=True, readable=True,
      represent=stv_represent_percentage),
    Field('cuentacontable_ivatrasladado', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cta. Contable, IVA trasladado', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuentacontable_ivaportrasladar', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cta. Contable, IVA por trasladar', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuentacontable_ivaacreditable', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cta. Contable, IVA acreditable', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuentacontable_ivaacreditableimportaciones', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cta. Contable, IVA acreditable por importaciones', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuentacontable_ivaporacreditar', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cta. Contable, IVA por acreditar', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuentacontable_ivaporacreditarimportaciones', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cta. Contable, IVA por acreditar importaciones', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuentacontable_ivaafavorretenido', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cta. Contable, IVA a favor retenido', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuentacontable_ivaafavorporretener', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cta. Contable, IVA a favor por retener', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuentacontable_ivaretenido', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cta. Contable, IVA retenido', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuentacontable_ivaporretener', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cta. Contable, IVA por retener', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    tSignature_dbc01,
    format='%(tasa)s',
    singular='Tasa IVA',
    plural='Tasa IVAs',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

class TEMPRESA_ISR:
    pass
dbc01.define_table(
    'tempresa_isr',
    Field('empresa_id', dbc01.tempresas, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Empresa', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('ejercicio', 'integer', default = None,
      required = True, requires = IS_IN_SET(VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id), zero = 0, error_message = 'Selecciona'),
      notnull = False, unique = False,
      widget = stv_widget_combobox, 
      label = 'Ejercicio', comment = None,
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id))),
    Field('tasa', 'decimal(4,2)', default=0,
      required=True,
      notnull=True, unique=False,
      widget=stv_widget_inputPercentage, label='Tasa', comment=None,
      writable=True, readable=True,
      represent=stv_represent_percentage),
    Field('cuentacontable_israfavorretenido', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cta. Contable, ISR a favor retenido', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuentacontable_israfavorporretener', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cta. Contable, ISR a favor por retener', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuentacontable_isrretenido', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cta. Contable, ISR retenido', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuentacontable_isrporretener', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cta. Contable, ISR por retener', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    tSignature_dbc01,
    format='%(tasa)s',
    singular='Tasa ISR',
    plural='Tasa ISRs',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

class TEMPRESA_IEPS:
    pass
dbc01.define_table(
    'tempresa_ieps',
    Field('empresa_id', dbc01.tempresas, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Empresa', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('ejercicio', 'integer', default = None,
      required = True, requires = IS_IN_SET(VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id), zero = 0, error_message = 'Selecciona'),
      notnull = False, unique = False,
      widget = stv_widget_combobox, 
      label = 'Ejercicio', comment = None,
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id))),
    Field('tasa', 'decimal(4,2)', default=0,
      required=True,
      notnull=True, unique=False,
      widget=stv_widget_inputPercentage, label='Tasa', comment=None,
      writable=True, readable=True,
      represent=stv_represent_percentage),
    Field('cuentacontable_iepstrasladado', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cta. Contable, IEPS trasladado', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuentacontable_iepsportrasladar', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cta. Contable, IEPS por trasladar', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuentacontable_iepsacreditable', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cta. Contable, IEPS acreditable', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuentacontable_iepsacreditableimportaciones', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cta. Contable, IEPS acreditable por importaciones', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuentacontable_iepsporacreditar', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cta. Contable, IEPS por acreditar', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuentacontable_iepsporacreditarimportaciones', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cta. Contable, IEPS por acreditar importaciones', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    tSignature_dbc01,
    format='%(tasa)s',
    singular='Tasa IEPS',
    plural='Tasa IEPSs',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

""" Definición de la tabla de empresas_cuentasbanacarias """
class TEMPRESA_CONCEPTOSASIENTOSCONTABLES:
    pass
dbc01.define_table(
    'tempresa_conceptosasientoscontables',
    Field('empresa_id', dbc01.tempresas, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Empresa', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('descripcion', 'string', length=200, default=None,
      required=True,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Descripción', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),

    Field('ingreso_efectivo', 'string', length=200, default="Venta PUE, Efectivo",
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Ingreso Efectivo', comment="",
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('ingreso_tdd', 'string', length=200, default="Venta PUE, Tdébito#  $Tarjeta, $Banco Autorización# $Autorizacion Operación# $Operacion Tbancaria# $Terminal FechaDeposito: $FechaDeposito",
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Ingreso T. Debito', comment="",
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('ingreso_tdc', 'string', length=200, default="Venta PUE, Tcrédito#  $Tarjeta, $Banco Autorización# $Autorizacion Operación# $Operacion Tbancaria# $Terminal FechaDeposito: $FechaDeposito",
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Ingreso T. Cred.', comment="",
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('ingreso_cheque', 'string', length=200, default="Venta PUE, Cheque#  $Cheque, $Banco, FechaDeposito: $FechaDeposito",
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Ingreso Cheque', comment="",
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('ingreso_transferencia', 'string', length=200, default="Venta PUE, TElectrónica, Cuenta# $Cuenta, $Banco Referencia# $Referencia Clave rastreo# $Rastreo, FechaDeposito: $FechaDeposito",
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Ingreso Transferencia', comment="",
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('ingreso_cliente', 'string', length=200, default="Venta PPD, Cliente $RFC $RazonSocialCliente",
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Ingreso Cliente', comment="",
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('ingreso_venta', 'string', length=200, default="Venta de $Concepto Cant=$Cantidad PU=$PrecioUnitario Importe $Importe Descto=$Descuento Total=$Total",
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Ingreso Venta', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('ingreso_descuento', 'string', length=200, default="Dcto. s/Venta de $Concepto Cant=$Cantidad PU=$PrecioUnitario Importe $Importe Descto=$Descuento Total=$Total",
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Ingreso Descuento', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('ingreso_imptrasladado', 'string', length=200, default="Imp. Trasladado $Impuesto $Tasa",
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Ingreso Imp. Trasladado', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('ingreso_impportrasladar', 'string', length=200, default="Imp. por Trasladar $Impuesto $Tasa",
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Ingreso Imp. por Trasladar', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    
    Field('pago_efectivo', 'string', length=200, default="Pago PUE, Efectivo",
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Pago Efectivo', comment="",
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('pago_tdd', 'string', length=200, default="Pago PUE, Tdébito#  $Tarjeta, $Banco Autorización# $Autorizacion Operación# $Operacion Tbancaria# $Terminal, FechaDeposito: $FechaDeposito",
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Pago T. Debito', comment="",
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('pago_tdc', 'string', length=200, default="Pago PUE, Tcrédito#  $Tarjeta, $Banco Autorización# $Autorizacion Operación# $Operacion Tbancaria# $Terminal, FechaDeposito: $FechaDeposito",
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Pago T. Cred.', comment="",
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('pago_cheque', 'string', length=200, default="Pago PUE, Cheque#  $Cheque, $Banco, FechaDeposito: $FechaDeposito",
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Pago Cheque', comment="",
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('pago_transferencia', 'string', length=200, default="Pago PUE, TElectrónica, Cuenta# $Cuenta, $Banco Referencia# $Referencia Clave rastreo# $Rastreo, FechaDeposito: $FechaDeposito",
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Pago Transferencia', comment="",
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('pago_cliente', 'string', length=200, default="Pago a Cliente $RFC $RazonSocialCliente",
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Pago Cliente', comment="",
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('pago_imptrasladado', 'string', length=200, default="Imp. Trasladado $Impuesto $Tasa",
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Pago Imp. Trasladado', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('pago_impportrasladar', 'string', length=200, default="Imp. por Trasladar $Impuesto $Tasa",
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Pago Imp. por Trasladar', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    
    Field('egreso_cliente', 'string', length=200, default="NC PPD, Cliente $RFC $RazonSocialCliente",
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Egreso Cliente', comment="",
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('egreso_venta', 'string', length=200, default="NC de $Concepto Cant=$Cantidad PU=$PrecioUnitario Importe $Importe Descto=$Descuento Total=$Total",
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Egreso NC', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('egreso_descuento', 'string', length=200, default="Dcto. s/NC de $Concepto Cant=$Cantidad PU=$PrecioUnitario Importe $Importe Descto=$Descuento Total=$Total",
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Egreso Descuento', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('egreso_imptrasladado', 'string', length=200, default="Imp. Trasladado $Impuesto $Tasa",
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Egreso Imp. Trasladado', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('egreso_impportrasladar', 'string', length=200, default="Imp. por Trasladar $Impuesto $Tasa",
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Egreso Imp. por Trasladar', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    
    Field('ingreso_efectivo_depositado', 'string', length=200, default="VtaContado Efectivo, $FechaDeposito, Depositado,  Ref#$Referencia",
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Ingreso Efectivo, depositado en bancos', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    
    Field('pago_efectivo_depositado', 'string', length=200, default="Pago Efvo, $FechaDeposito, Depositado,  Ref#$Referencia",
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Pago Efectivo, depositado en bancos', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    
    tSignature_dbc01,
    format='%(descripcion)s',
    singular='Conceptos Contables',
    plural='Conceptos Contables',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )


db01.ttiposcomprobantes._referenced_by.append(dbc01.tempresa_series.tipocomprobante_id)
db01.ttiposcomprobantes._references.append(dbc01.tempresa_series.tipocomprobante_id)

class TEMPRESA_GASTOSCONTABILIDAD:
    pass
dbc01.define_table(
    'tempresa_gastoscontabilidad',
    Field('empresa_id', dbc01.tempresas, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Empresa', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('ejercicio', 'integer', default = None,
      required = True, requires = IS_IN_SET(VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id), zero = 0, error_message = 'Selecciona'),
      notnull = False, unique = False,
      widget = stv_widget_combobox, 
      label = 'Ejercicio', comment = None,
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id))),
    Field('empresa_organizacionestructura_id', dbc01.tempresa_organizacionestructura, default=None,
      required=True,
      ondelete='NO ACTION', notnull=True, unique=False,
      widget=lambda f, v: stv_widget_db_chosen_tree(
          f, v, 1, D_additionalAttributes={
                    'dbTable_tree': dbc01.tempresa_organizacionestructura,
                    'dbField_order': dbc01.tempresa_organizacionestructura.orden,
                    'dbField_tree_groupby': dbc01.tempresa_organizacionestructura.padre_id,
                    'class_dbTree': DB_Tree,
                    'b_onlyLastNodes' : True,
                    }
          ), 
      label= 'Organización', comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield_tree(v, r, dbc01.tempresa_organizacionestructura.padre_id, dbc01.tempresa_gastoscontabilidad.empresa_organizacionestructura_id)
      ),  
    tSignature_dbc01,
    format='%(ejercicio)s',
    singular='Contabilización',
    plural='Contabilizaciones',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

class TEMPRESA_GASTOCONTABILIDAD_GUIASCONTABLES:
    pass
dbc01.define_table(
    'tempresa_gastocontabilidad_guiascontables',
    Field('empresa_gastocontabilidad_id', dbc01.tempresa_gastoscontabilidad, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Empresa', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('empresa_gastoestructura_id', dbc01.tempresa_gastosestructura, default=None,
      required=True,
      ondelete='NO ACTION', notnull=True, unique=False,
      widget=lambda f, v: stv_widget_db_chosen_tree(
          f, v, 1, D_additionalAttributes={
                    'dbTable_tree': dbc01.tempresa_gastosestructura,
                    'dbField_order': dbc01.tempresa_gastosestructura.orden,
                    'dbField_tree_groupby': dbc01.tempresa_gastosestructura.padre_id,
                    'class_dbTree': DB_Tree,
                    'b_onlyLastNodes' : True,
                    }
          ), 
      label= 'Estructura de gasto', comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield_tree(v, r, dbc01.tempresa_gastosestructura.padre_id, dbc01.tempresa_gastocontabilidad_guiascontables.empresa_gastoestructura_id)
      ),
    Field('empresa_centrocotosestructura_id', dbc01.tempresa_centrocostosestructura, default=None,
      required=True,
      ondelete='NO ACTION', notnull=True, unique=False,
      widget=lambda f, v: stv_widget_db_chosen_tree(
          f, v, 1, D_additionalAttributes={
                    'dbTable_tree': dbc01.tempresa_centrocostosestructura,
                    'dbField_order': dbc01.tempresa_centrocostosestructura.orden,
                    'dbField_tree_groupby': dbc01.tempresa_centrocostosestructura.padre_id,
                    'class_dbTree': DB_Tree,
                    'b_onlyLastNodes' : True,
                    }
          ), 
      label= 'Centro costo ', comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield_tree(v, r, dbc01.tempresa_centrocostosestructura.padre_id, dbc01.tempresa_gastocontabilidad_guiascontables.empresa_centrocotosestructura_id)
      ),  
    Field('cuentacontable', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cuenta Contable', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    tSignature_dbc01,
    format='%(id)s',
    singular='Guía contable',
    plural='Guías contables',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )
