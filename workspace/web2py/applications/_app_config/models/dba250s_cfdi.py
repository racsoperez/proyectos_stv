# -*- coding: utf-8 -*-

# noinspection SpellCheckingInspection

class TCFDIS(Table):
    """ Tabla usada para tener los CFDIs con su información maestro.

    """

    S_NOMBRETABLA = 'tcfdis'

    class E_PROVEEDOR_ERROR:
        NO_DEFINIDO = 0
        OK = 1
        ERROR_EN_DOCUMENTO = 2
        ERROR_CON_PAC = 3
        ERROR_EN_CONEXION_PAC = 4
        D_TODOS = {
            NO_DEFINIDO          : 'No Definido',
            OK                   : 'OK',
            ERROR_EN_DOCUMENTO   : 'Err. en Doc.',
            ERROR_CON_PAC        : 'Err. con PAC',
            ERROR_EN_CONEXION_PAC: 'Err. en Conexión PAC',
            }

        @classmethod
        def get_dict(cls):
            return cls.D_TODOS

    class E_ERRORES_PAC:
        NO_DEFINIDO = 0
        HASH_DUPLICADO = 96
        D_TODOS = {
            NO_DEFINIDO   : 'No Definido',
            HASH_DUPLICADO: 'HASH duplicado',
            }

        @classmethod
        def get_dict(cls):
            return cls.D_TODOS

    class E_GENERADO_POR:
        """ Se definen las opciónes del campo """
        IMPORTADO = 0  # Importado por el servidor
        MANUAL = 1  # Creación manual en el sistema
        IMPORTADO_SISTEMA = 2  # Importado XML desde el sistema
        OTRO = 3
        D_TODOS = {
            IMPORTADO        : 'Imp. Servidor',
            MANUAL           : 'Manual',
            IMPORTADO_SISTEMA: 'Imp. Sistema',
            OTRO             : 'Otro',
            }

        @classmethod
        def get_dict(cls):
            return cls.D_TODOS

    class E_ROL:
        """ Se definen las opciónes del campo """
        NO_DEFINIDO = 0
        EMISOR = 1
        RECEPTOR = 2
        D_TODOS = {
            NO_DEFINIDO: 'No definido',
            EMISOR     : 'Emisor',
            RECEPTOR   : 'Receptor',
            }

        @classmethod
        def get_dict(cls):
            return cls.D_TODOS

    class E_ETAPA:
        """ Se definen las opciónes del campo """
        NO_DEFINIDO = 0
        SIN_CONTABILIZAR = 1  # Estado del CFDI sin contabilizar y sin prepoliza : default
        CON_PREPOLIZA = 2  # Sin contabilizar con prepoliza
        CON_POLIZA = 3  # Con poliza
        NO_CONTABILIZAR = 4  # El usuario decidió no usar el sistema para contabilizar el CFDI
        NO_CONTABILIZAR_CANCELADO = 5  # El CFDI no se contabilizó debido a que fue cancelado antes de contabilizar.
        D_TODOS = {
            NO_DEFINIDO              : 'No definido',
            SIN_CONTABILIZAR         : 'Sin contabilizar',
            CON_PREPOLIZA            : 'Con prepoliza',
            CON_POLIZA               : 'Con poliza',
            NO_CONTABILIZAR          : 'No contabilizar',
            NO_CONTABILIZAR_CANCELADO: 'No cont. por cancelación'
            }

        @classmethod
        def get_dict(cls):
            return cls.D_TODOS

    class E_ESTADO:
        """ Se definen las opciónes del estado, que determina el estado del CFDI para efectos del sistema """
        NO_DEFINIDO = ''  # Estado que indica que el CFDI esta en proceso de creación
        EN_PROCESO_FIRMA = 'F'  # Indica que el XML se generó y esta en proceso de firma
        VIGENTE = 'V'  # Estado que indica el CFDI ya fue creado y firmado
        EN_PROCESO_CANCELACION = 'A'  # En proceso de cancelación
        CANCELADO = 'C'  # Estado que indica que el CFDI esta cancelado
        TIMBRE_RECHAZADO = 'R'  # Estado que indica que el timbrado o cancelación fue rechazada
        CANCELACION_RECHAZADO = 'X'  # Estado que indica que el timbrado o cancelación fue rechazada
        D_TODOS = {
            NO_DEFINIDO           : 'Elaborando',
            EN_PROCESO_FIRMA      : 'Timbrando',
            VIGENTE               : 'Vigente',
            EN_PROCESO_CANCELACION: 'Cancelando',
            CANCELADO             : 'Cancelado',
            TIMBRE_RECHAZADO      : 'T. Rechazado',
            CANCELACION_RECHAZADO : 'C. Rechazado',
            }
        L_APLICA_CALCULOSALDO = [
            VIGENTE,
            EN_PROCESO_CANCELACION,
            CANCELACION_RECHAZADO,
            ]
        L_APLICA_CALCULOSALDO_PENDIENTETIMBRAR = [
            NO_DEFINIDO,
            EN_PROCESO_FIRMA,
            TIMBRE_RECHAZADO,
            ]
        L_APLICA_CALCULOSALDO_TIMBRADO = [
            VIGENTE,
            EN_PROCESO_CANCELACION,
            # CANCELADO,
            CANCELACION_RECHAZADO
            ]
        L_SINPROCESOPENDIENTE = [
            VIGENTE,
            CANCELADO,
            TIMBRE_RECHAZADO,
            CANCELACION_RECHAZADO
            ]

        @classmethod
        def GET_DICT(cls):
            return cls.D_TODOS

    class V_VENCIMIENTO:

        @classmethod
        def EVALUATE(cls, dbRow):
            _dbRow = STV_LIB_DB.OBTENER_ROW_DE_CONSULTA(dbRow, dbc01.tcfdis)
            return _dbRow.fecha + datetime.timedelta(days = _dbRow.credito_plazo or 0)

        @classmethod
        def GET_FIELDVIRTUAL(cls):
            _dbField = Field.Virtual(
                'v_vencimiento',
                f = cls.EVALUATE,
                ftype = 'decimal(14,4)',
                label = 'Vencimiento',
                # table_name = TCFDIS.S_NOMBRETABLA
                )
            _dbField.represent = stv_represent_date
            return _dbField

    class V_ESTATUSCXC:
        SIN_ANALIZAR = 0
        POR_VENCER = 10
        VENCIDA = 20
        PAGADA = 30
        FUERA_DE_RANGO = 40
        _D_dict = None

        @classmethod
        def GET_DICT(cls):
            if not cls._D_dict:
                cls._D_dict = {
                    cls.SIN_ANALIZAR  : 'Sin analizar',
                    cls.POR_VENCER    : 'Por vencer',
                    cls.VENCIDA       : 'Vencida',
                    cls.PAGADA        : 'Pagada',
                    cls.FUERA_DE_RANGO: 'Fuera de rango',
                    }
            else:
                pass
            return cls._D_dict

        @classmethod
        def EVALUATE(cls, dbRow):
            _dbRow = STV_LIB_DB.OBTENER_ROW_DE_CONSULTA(dbRow, dbc01.tcfdis)
            if _dbRow.saldo is None:
                if _dbRow.fecha < TCFDIS.GET_PRIMERDIA2020().dt_utc:
                    return cls.FUERA_DE_RANGO
                else:
                    return cls.SIN_ANALIZAR
            elif _dbRow.sat_status == TCFDIS.SAT_STATUS.NO_DEFINIDO:
                return cls.SIN_ANALIZAR
            elif _dbRow.saldo <= 0:
                return cls.PAGADA
            elif TCFDIS.V_DVENCIDO.EVALUATE(dbRow) > 0:
                return cls.VENCIDA
            else:
                return cls.POR_VENCER

        @classmethod
        def GET_FIELDVIRTUAL(cls):
            _dbField = Field.Virtual(
                'v_estatuscxc',
                f = cls.EVALUATE,
                ftype = 'integer',
                label = 'Estatus CxC',
                # table_name = TCFDIS.S_NOMBRETABLA
                )
            _dbField.represent = lambda v, r: stv_represent_list(v, r, None, TCFDIS.V_ESTATUSCXC.GET_DICT())
            return _dbField

    class V_DVENCIDO:

        @classmethod
        def EVALUATE(cls, dbRow):
            _dbRow = STV_LIB_DB.OBTENER_ROW_DE_CONSULTA(dbRow, dbc01.tcfdis)
            _n_dias = (request.browsernow - TCFDIS.V_VENCIMIENTO.EVALUATE(dbRow)).days \
                if _dbRow.saldo and (_dbRow.saldo > 0) else 0
            return _n_dias

        @classmethod
        def GET_FIELDVIRTUAL(cls):
            _dbField = Field.Virtual(
                'v_dvencido',
                f = cls.EVALUATE,
                ftype = 'integer',
                label = 'D. Vencidos',
                # table_name = TCFDIS.S_NOMBRETABLA
                )
            _dbField.represent = lambda v, r: stv_represent_number(v, r, dbField = dbc01.tcfdis.v_dvencido)
            return _dbField

    class SAT_STATUSCANCELACION:
        """ Se definen las opciónes del campo """
        NO_DEFINIDO = 0
        NO_CANCELABLE = 1
        CANCELABLE_SIN_ACEPTACION = 2
        CANCELABLE_CON_ACEPTACION = 3
        EN_PROCESO = 4
        SOLICITUD_RECHAZADA = 5
        D_TODOS = {
            NO_DEFINIDO              : 'No Definido',
            NO_CANCELABLE            : 'No Cancelable',
            CANCELABLE_SIN_ACEPTACION: 'Cancelable Sin Aceptación',
            CANCELABLE_CON_ACEPTACION: 'Cancelable Con Aceptación',
            EN_PROCESO               : 'En Proceso',
            SOLICITUD_RECHAZADA      : 'Solicitud Rechazada',
            }

        @classmethod
        def GET_DICT(cls):
            return cls.D_TODOS

    class SAT_STATUSPROCESOCANCELACION:
        """ Se definen las opciónes del campo """
        NO_DEFINIDO = 0
        EN_PROCESO = 1
        PLAZO_VENCIDO = 2
        CON_ACEPTACION = 3
        SIN_ACEPTACION = 4
        SOLICITUD_RECHAZADA = 5
        D_TODOS = {
            NO_DEFINIDO        : 'No Definido',
            EN_PROCESO         : 'En Proceso',
            PLAZO_VENCIDO      : 'Plazo Vencido',
            CON_ACEPTACION     : 'Con Aceptación',
            SIN_ACEPTACION     : 'Sin Aceptación',
            SOLICITUD_RECHAZADA: 'Solicitud Rechazada',
            }

        @classmethod
        def GET_DICT(cls):
            return cls.D_TODOS

    class SAT_STATUS:
        """ Se definen las opciónes del campo """
        NO_DEFINIDO = 0
        VIGENTE = 1
        CANCELADO = 2
        D_TODOS = {
            NO_DEFINIDO: 'No Definido',
            VIGENTE    : 'Vigente',
            CANCELADO  : 'Cancelado',
            }

        @classmethod
        def GET_DICT(cls):
            return cls.D_TODOS

    L_DBCAMPOS = [
        Field(
            'empresa_id', dbc01.tempresas, default = None,
            required = True,
            ondelete = 'NO ACTION', notnull = True, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = (
                'Empresa'), comment = 'Empresa que tiene registrado el cfdi ya sea como emisor o receptor',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r)
            ),
        Field(
            'versioncfdi', 'string', length = 50, default = '3.3',
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Vesion', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        # Serie debe estar relacionado a la serie de la empresa
        Field(
            'serie', 'string', length = 50, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Serie', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'empresa_serie_id', dbc01.tempresa_series, default = None,
            required = False,
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Serie', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r)
            ),
        Field(
            'empresa_plaza_sucursal_cajachica_id', dbc01.tempresa_plaza_sucursal_cajaschicas, default = None,
            required = False,
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Caja chica relacionada', comment = (
                'Caja chica relacionada'),
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r)
            ),
        # Serie debe estar relacionado al folio en la empresa del tipo de documento
        Field(
            'folio', 'string', length = 50, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Folio', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'fecha_str', 'string', length = 50, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Fecha', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        # Se graba como la fecha-hora local de emisión del CFDI usando la fecha-hora del CFDI
        # TODO se tiene que usar la fecha hora del CFDI, sin conversion
        Field(
            'fecha', type = TDATETIME_GRABADIRECTO_LEERDIRECTO, default = request.browsernow,
            required = False, requires = IS_NULL_OR(IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME)),
            notnull = False,
            widget = stv_widget_inputDateTime, label = 'Fecha', comment = '',
            writable = False, readable = True,
            represent = stv_represent_datetime
            ),
        Field(
            'dia', 'date', default = None,
            required = False, requires = IS_NULL_OR(IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE)),
            notnull = False,
            widget = stv_widget_inputDate, label = 'Fecha', comment = '',
            writable = False, readable = False,
            represent = stv_represent_date
            ),
        Field(
            'hora', 'time', default = None,  # Se graba como la hora local de emisión del CFDI
            required = False,
            notnull = False,
            widget = stv_widget_input, label = 'Hora', comment = '',
            writable = False, readable = False,
            represent = stv_represent_timeAsText
            ),
        Field(
            'formapago', 'string', length = 50, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Forma pago', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'formapago_id', 'integer', default = TFORMASPAGO.EFECTIVO,
            required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tformaspago.id', db01.tformaspago._format)),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Forma pago', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tcfdis.formapago_id)
            ),
        Field(
            'condicionesdepago', 'string', length = 200, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Condiciones de pago', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'subtotal', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'SubTotal', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_money_ifnotzero(v, r, dbField = dbc01.tcfdis.subtotal)
            ),
        Field(
            'descuento', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Descuento', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_money(v, r, dbField = dbc01.tcfdis.descuento)
            ),
        Field(
            'moneda', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Moneda', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'moneda_id', 'integer', default = D_stvSiteHelper.dbconfigs.defaults.moneda_id,
            required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tmonedas.id', db01.tmonedas._format)),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Moneda', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tcfdis.moneda_id)
            ),

        # Este será el campo que se utilice en vez de moneda_id
        Field(
            'monedacontpaqi_id', 'integer', default = D_stvSiteHelper.dbconfigs.defaults.moneda_contpaqi_id,
            required = False,
            requires = IS_NULL_OR(
                IS_IN_DB(
                    dbc01(dbc01.tempresa_monedascontpaqi.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id),
                    'tempresa_monedascontpaqi.id',
                    dbc01.tempresa_monedascontpaqi._format
                    )
                ),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Moneda', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tcfdis.monedacontpaqi_id)
            ),

        Field(
            'tipocambio', 'decimal(14,4)', default = 1,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Tipo de cambio', comment = None,
            writable = False, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        Field(
            'total', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Total', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_money(v, r, dbField = dbc01.tcfdis.total)
            ),
        Field(
            'totalpago', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Total Pago', comment = 'Total del pago en Pesos',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_money(v, r, dbField = dbc01.tcfdis.total)
            ),
        Field(
            'saldo', 'decimal(14,4)', default = None,  # default debe ser None, indicando que no ha sido analizado
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Saldo', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_money(v, r, dbField = dbc01.tcfdis.saldo)
            ),
        Field(
            'saldofechacalculo', type = TDATETIME_GRABADIRECTO_LEERENZONAH, default = None,
            # Se graba como la fecha-hora local de emisión del CFDI
            required = False, requires = IS_NULL_OR(IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME)),
            notnull = False,
            widget = stv_widget_inputDateTime, label = 'Fecha Saldo', comment = 'Fecha de recálculo de saldo',
            writable = False, readable = True,
            represent = stv_represent_datetime
            ),
        Field(
            'tipodecomprobante', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Tipo de Comprobante', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'tipocomprobante_id', 'integer', default = None,
            required = False,
            requires = IS_NULL_OR(IS_IN_DB(db01, 'ttiposcomprobantes.id', db01.ttiposcomprobantes._format)),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Tipo comprobante', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tcfdis.tipocomprobante_id)
            ),
        Field(
            'metodopago', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Método de pago', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'metodopago_id', 'integer', default = None,
            required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tmetodospago.id', db01.tmetodospago._format)),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Método de pago', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tcfdis.metodopago_id)
            ),
        Field(
            'lugarexpedicion', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Lugar exp.', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'lugarexpedicion_id', 'integer', default = None,
            required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tcodigospostales.id', '%(c_codigopostal)s')),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_search(
                f, v,
                s_display = '%(c_codigopostal)s',
                s_url = URL(a = 'app_generales', c = '005genericos_sat', f = 'codigospostales_buscar'),
                D_additionalAttributes = {'reference': db01.tcodigospostales.id}
                ),
            label = 'Lugar', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tcfdis.lugarexpedicion_id)
            ),
        Field(
            'totalimpuestostrasladados', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Total imp. trasladados', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_money(v, r, dbField = dbc01.tcfdis.totalimpuestostrasladados)
            ),
        Field(
            'totalimpuestosretenidos', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Total impuestos retenidos', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_money(v, r, dbField = dbc01.tcfdis.totalimpuestosretenidos)
            ),
        Field(
            'credito_plazo', 'integer', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputInteger, label = 'Plazo (días)', comment = 'Días de crédito',
            writable = False, readable = True,
            represent = stv_represent_number
            ),

        # Sección de CFDI Relacionados
        Field(
            'tiporelacion', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Tipo relación', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'tiporelacion_id', 'integer', default = None,
            required = False,
            requires = IS_NULL_OR(IS_IN_DB(db01, 'ttiposrelaciones.id', db01.ttiposrelaciones._format)),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Tipo relación', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tcfdis.tiporelacion_id)
            ),

        # Timbre fiscal digital
        Field(
            'uuid', 'string', length = 50, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'UUID', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'fechatimbrado_str', 'string', length = 50, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Fecha timbrado', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        # TODO se tiene que usar la fecha hora del CFDI, sin conversion
        Field(
            'fechatimbrado', type = TDATETIME_GRABADIRECTO_LEERDIRECTO, default = None,
            # Se graba como la fecha-hora local de emisión del CFDI
            required = False, requires = IS_NULL_OR(IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME)),
            notnull = False,
            widget = stv_widget_inputDateTime, label = 'Timbrado', comment = '',
            writable = False, readable = True,
            represent = stv_represent_datetime
            ),
        Field(
            'rol', 'integer', default = 0,
            required = False, requires = IS_IN_SET(E_ROL.get_dict(), zero = 0, error_message = 'Selecciona'),
            notnull = True, unique = False,
            widget = stv_widget_combobox,
            label = 'Rol', comment = 'Rol de la empresa en el CFDI',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TCFDIS.E_ROL.get_dict())
            ),
        Field(
            'etapa', 'integer', default = E_ETAPA.SIN_CONTABILIZAR,
            required = False, requires = IS_IN_SET(E_ETAPA.get_dict(), zero = 0, error_message = 'Selecciona'),
            notnull = True, unique = False,
            widget = stv_widget_combobox,
            label = 'Etapa', comment = 'Etapa de contabilización del CFDI',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TCFDIS.E_ETAPA.get_dict())
            ),
        Field(
            'etapa_cancelado', 'integer', default = E_ETAPA.SIN_CONTABILIZAR,
            required = False, requires = IS_IN_SET(E_ETAPA.get_dict(), zero = 0, error_message = 'Selecciona'),
            notnull = True, unique = False,
            widget = stv_widget_combobox,
            label = 'Etapa Cancelado', comment = 'Etapa de contabilización de la cancelación del CFDI',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TCFDIS.E_ETAPA.get_dict())
            ),
        Field(
            'estado', 'string', length = 1, default = E_ESTADO.NO_DEFINIDO,
            required = False, requires = IS_IN_SET(E_ESTADO.GET_DICT(), zero = 0, error_message = 'Selecciona'),
            notnull = True, unique = False,
            widget = stv_widget_combobox,
            label = 'Estado', comment = 'Rol de la empresa en el CFDI',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TCFDIS.E_ESTADO.GET_DICT())
            ),
        Field(
            'generado_por', 'integer', default = 0,
            required = False, requires = IS_IN_SET(
                E_GENERADO_POR.get_dict(), zero = 0, error_message = 'Selecciona'
                ),
            notnull = True, unique = False,
            widget = stv_widget_combobox,
            label = 'Generado', comment = 'De donde se generó el CFDI',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TCFDIS.E_GENERADO_POR.get_dict())
            ),

        Field(
            'errores', 'string', length = 200, default = '',
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Inconsistencias', comment = '',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_string(v, r, dbField = dbc01.tcfdis.errores)
            ),

        Field(
            'cfdi_complemento_id', 'integer', default = None,
            required = False,
            ondelete = 'NO ACTION', notnull = False, unique = False,
            label = 'CFDI Complemento',
            comment = 'En caso de ser un CFDI con complemento en otra factura, es el id del CFDI que hace complemento',
            writable = False, readable = True,
            represent = None
            ),
        Field(
            'facturacomplementos', 'integer', default = TEMPRESA_CLIENTES.E_FACTURARCOMPLEMENTOS.SIN_DEFINIR,
            required = False, requires = IS_IN_SET(
                TEMPRESA_CLIENTES.E_FACTURARCOMPLEMENTOS.GET_DICT(), zero = 0, error_message = 'Selecciona'
                ),
            notnull = False, unique = False,
            widget = stv_widget_combobox,
            label = 'Facturación de complementos', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_CLIENTES.E_FACTURARCOMPLEMENTOS.GET_DICT())
            ),
        Field(
            'emp_pla_suc_almacen_id', dbc01.tempresa_plaza_sucursal_almacenes, default = None,
            required = False,
            requires = IS_NULL_OR(
                IS_IN_DB(
                    dbc01(
                        dbc01.tempresa_plaza_sucursal_almacenes.id.belongs(
                            TEMPRESA_PLAZA_SUCURSAL_ALMACENES.OBTENER()
                            )
                        ),
                    'tempresa_plaza_sucursal_almacenes.id', dbc01.tempresa_plaza_sucursal_almacenes._format
                    )
                ),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1),
            label = 'Almacen preferido', comment = 'Almacén preferido para surtir',
            writable = False, readable = True,
            represent = stv_represent_referencefield
            ),

        # Emisor puede ser de tempresa (genera factura), proveedor (recibe factura) o cliente (si es pago)
        Field(
            'emisorrfc', 'string', length = 15, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Emisor RFC', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'emisornombrerazonsocial', 'string', length = 254, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Emisor razón social', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'emisorregimenfiscal', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Emisor Regimen Fiscal', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        # En caso de ser null, corresponde a la empresa_id definida
        Field(
            'emisorproveedor_id', dbc01.tempresa_proveedores, default = None,
            required = False,
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = (
                'Proveedor'), comment = 'En caso de recibir la factura, se relaciona la empresa que genera el cfdi',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r)
            ),
        # Receptor puede ser de tempresa (genera factura), proveedor (recibe factura) o cliente (si es pago)
        Field(
            'receptorrfc', 'string', length = 15, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Receptor RFC', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'receptornombrerazonsocial', 'string', length = 254, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Receptor razón social', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'receptorusocfdi', 'string', length = 15, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Receptor RFC', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'receptorusocfdi_id', 'integer', default = None,
            required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tusoscfdi.id', db01.tusoscfdi._format)),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Receptor uso CFDI', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tcfdis.receptorusocfdi_id)
            ),

        # En caso de ser null, corresponde a la empresa_id definida
        Field(
            'receptorcliente_id', dbc01.tempresa_clientes, default = None,
            required = False,
            requires = IS_NULL_OR(
                IS_IN_DB(
                    dbc01(dbc01.tempresa_clientes.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id),
                    'tempresa_clientes.id', dbc01.tempresa_clientes._format
                    )
                ),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_search(
                f, v,
                s_display = dbc01.tempresa_clientes._format,
                s_url = URL(a = 'app_empresas', c = '130clientes', f = 'empresa_clientes_buscar'),
                D_additionalAttributes = {'reference': dbc01.tempresa_clientes.id}
                ),
            label = 'Cliente',
            comment = 'En caso de ingreso, se relaciona la empresa a la que se dirige la factura (ingreso)',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r)
            ),
        Field(
            'sat_status', 'integer', default = SAT_STATUS.NO_DEFINIDO,
            required = False,
            requires = IS_IN_SET(SAT_STATUS.GET_DICT(), zero = 0, error_message = 'Selecciona'),
            notnull = False, unique = False,
            widget = stv_widget_combobox,
            label = 'SAT Estatus', comment = 'Status del CFDI de acuerdo al SAT',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TCFDIS.SAT_STATUS.GET_DICT())
            ),
        # TODO se tiene que usar la fecha hora del CFDI, sin conversion
        Field(
            'sat_fecha_cancelacion', type = TDATETIME_GRABADIRECTO_LEERDIRECTO, default = None,
            required = False, requires = IS_NULL_OR(IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME)),
            notnull = False,
            widget = stv_widget_inputDateTime, label = 'Fecha cancelación segun el SAT', comment = '',
            writable = False, readable = True,
            represent = stv_represent_datetime
            ),
        Field(
            'sat_status_cancelacion', 'integer', default = SAT_STATUSCANCELACION.NO_DEFINIDO,
            required = False,
            requires = IS_IN_SET(SAT_STATUSCANCELACION.GET_DICT(), zero = 0, error_message = 'Selecciona'),
            notnull = False, unique = False,
            widget = stv_widget_combobox,
            label = 'SAT Status Cancelación', comment = 'Estatus Cancelación del CFDI de acuerdo al SAT',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TCFDIS.SAT_STATUSCANCELACION.GET_DICT())
            ),
        Field(
            'sat_status_proceso_cancelacion', 'integer', default = SAT_STATUSPROCESOCANCELACION.NO_DEFINIDO,
            required = False,
            requires = IS_IN_SET(
                SAT_STATUSPROCESOCANCELACION.GET_DICT(), zero = 0, error_message = 'Selecciona'
                ),
            notnull = False, unique = False,
            widget = stv_widget_combobox,
            label = 'SAT Status Proceso Canc.', comment = 'Status Proceso Cancelación del CFDI de acuerdo al SAT',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TCFDIS.SAT_STATUSPROCESOCANCELACION.GET_DICT())
            ),

        # Emisor puede ser de tempresa (genera factura), proveedor (recibe factura) o cliente (si es pago)
        Field(
            'pac_rfc', 'string', length = 15, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'PAC RFC', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'pac_razon_social', 'string', length = 240, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'PAC Razon Social', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'pac_nombre_comercial', 'string', length = 120, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'PAC Nombre Comercial', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),

        Field(
            'cancelado_folio', 'string', length = 100, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Folio de Cancelación', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        tSignature_db01,
        ]

    def __init__(self, dbRef, tablename, *fields, **args):
        fields += (
            TCFDIS.V_VENCIMIENTO.GET_FIELDVIRTUAL(),
            TCFDIS.V_ESTATUSCXC.GET_FIELDVIRTUAL(),
            TCFDIS.V_DVENCIDO.GET_FIELDVIRTUAL()
            )
        Table.__init__(self, dbRef, tablename, *fields, **args)
        self.PROC.dbTabla = self

        return

    @staticmethod
    def DEFINIR_DBCAMPO(s_nombreCampo = 'cfdirelacionado_id', **D_params):
        D_params = Storage(D_params)
        _dbCampo = Field(
            s_nombreCampo, dbc01.tcfdis, default = D_params.default or None,
            required = D_params.required or False,
            requires = D_params.requires or IS_IN_DB(
                dbc01(dbc01.tcfdis.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id),
                'tcfdis.id',
                dbc01.tcfdis._format
                ),
            ondelete = 'NO ACTION', notnull = D_params.notnull or False, unique = False,
            widget = D_params.widget or (
                lambda f, v: stv_widget_db_search(
                    f, v, s_display = '%(serie)s %(folio)s',
                    s_url = URL(a = 'app_timbrado', c = '250cfdi', f = 'cfdi_buscar'),
                    D_additionalAttributes = {'reference': dbc01.tcfdis.id}
                    )
                ),
            label = D_params.label or 'CFDI Relacionado', comment = D_params.comment or "",
            writable = False, readable = True,
            represent = D_params.represent or stv_represent_referencefield
            )

        if not D_params.requires and not D_params.required:
            _dbCampo.requires = IS_NULL_OR(_dbCampo.requires)
        else:
            pass

        return _dbCampo

    @staticmethod
    def GET_PRIMERDIA2020():
        _dt_primerdia2020 = datetime.datetime(2020, 1, 1, 0, 0)
        _dt_primerdia2020_utc = TZONASHORARIAS.CONVERTIR_FECHAHORA_DEZONAH_AUTC(
            STV_FWK_APP.n_zonahoraria_id, _dt_primerdia2020
            )
        return Storage(
            dt_browser = _dt_primerdia2020,
            dt_utc = _dt_primerdia2020_utc
            )

    @staticmethod
    def OBTENER_ULTIMOFOLIO_SERIE(
            s_serie = None,
            s_empresa_serie_id = None
            ):
        """ Regresa el último folio utilizado que sea mayor, el último usado en CFDIs o el definido
        en la configuración de la serie
        """
        _n_ultimoFolioUsado = 0

        if not s_empresa_serie_id:
            if s_serie:
                _dbRow_serie = dbc01(dbc01.tempresa_series.serie == s_serie).select(dbc01.tempresa_series)
                if _dbRow_serie:
                    s_empresa_serie_id = _dbRow_serie.first().id
                else:
                    pass
            else:
                pass
        else:
            # Usar el valor para detemrinar la serie
            pass

        if s_empresa_serie_id:
            _dbRows_ultimocfdi_serie = dbc01(
                (dbc01.tcfdis.empresa_serie_id == s_empresa_serie_id)
                & (dbc01.tempresa_series.id == dbc01.tcfdis.empresa_serie_id)
                & (dbc01.tcfdis.folio != None)
                ).select(
                    dbc01.tcfdis.id,
                    dbc01.tcfdis.folio,
                    dbc01.tempresa_series.folio,
                    orderby = ~dbc01.tcfdis.fecha,
                    limitby = (0, 1)
                    )

            if _dbRows_ultimocfdi_serie:
                _dbRow_ultimocfdi_serie = _dbRows_ultimocfdi_serie.last()
                if str(_dbRow_ultimocfdi_serie.tcfdis.folio).isdigit():
                    _n_ultimoFolioUsadoCFDI = int(_dbRow_ultimocfdi_serie.tcfdis.folio)
                else:
                    # Usa los caracteres numéricos en el folio, en caso de contener texto
                    _n_ultimoFolioUsadoCFDI = int(
                        "".join([i for i in _dbRow_ultimocfdi_serie.tcfdis.folio if i.isdigit()])
                        )
                if _n_ultimoFolioUsadoCFDI > _dbRow_ultimocfdi_serie.tempresa_series.folio:
                    _n_ultimoFolioUsado = _n_ultimoFolioUsadoCFDI
                else:
                    _n_ultimoFolioUsado = int(tempresa_series.tcfdis.folio)
            else:
                pass
        else:
            pass
        return _n_ultimoFolioUsado

    @staticmethod
    def OBTENER_CFDISINGRESOS_AFECTADOS(
            x_cfdi,
            ):
        """ Obtiene todos los ids de ingresos que deben recalcularse debido a las referencia con el id
        """
        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi, dbc01.tcfdis)
        _dbRow = _D_results.dbRow

        _L_cfdis = []  # Ids de CFDIs de ingreso que se asocian con movimientos posteriores

        if _dbRow.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO:
            # Si es ingreso, se utiliza el CFDI del parametro
            _L_cfdis.append(_dbRow.id)

            if _dbRow.cfdi_complemento_id:
                _L_cfdis.append(_dbRow.cfdi_complemento_id)
            else:
                pass

        elif _dbRow.tipocomprobante_id in (TTIPOSCOMPROBANTES.PAGO, TTIPOSCOMPROBANTES.EGRESO):
            # Si es de pago o egreso, se utilizarán los ids especificados en los documentos relacionados

            _dbRows = []
            if _dbRow.tipocomprobante_id == TTIPOSCOMPROBANTES.PAGO:

                _dbRows = dbc01(
                    (dbc01.tcfdi_complementopagos.cfdi_id == _dbRow.id)
                    & (dbc01.tcfdi_complementopago_doctosrelacionados.cfdi_complementopago_id
                        == dbc01.tcfdi_complementopagos.id)
                    ).select(
                        dbc01.tcfdi_complementopago_doctosrelacionados.id,
                        dbc01.tcfdi_complementopago_doctosrelacionados.cfdirelacionado_id,
                        )

            elif _dbRow.tipocomprobante_id == TTIPOSCOMPROBANTES.EGRESO:
                _dbRows = dbc01(
                    (dbc01.tcfdi_relacionados.cfdi_id == _dbRow.id)
                    ).select(
                        dbc01.tcfdi_relacionados.id,
                        dbc01.tcfdi_relacionados.cfdirelacionado_id,
                        )

            else:
                raise "Este código no debe ejecutarse nunca"

            for _dbRow_cfdi in _dbRows:
                _L_cfdis.append(_dbRow_cfdi.cfdirelacionado_id)

        else:
            # No se reconoce el tipo de documento
            pass

        return _L_cfdis

    @staticmethod
    def OBTENER_CFDIS_POSTERIORES(
            x_cfdi,
            ):
        """ Obtiene los CFDIs activos posteriores al cfdi actual

        @param x_cfdi:
        @type x_cfdi:
        @return:
        @rtype:
        """
        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi, dbc01.tcfdis)
        _dbRow = _D_results.dbRow

        _L_cfdis = TCFDIS.OBTENER_CFDISINGRESOS_AFECTADOS(_dbRow)

        if _dbRow.tipocomprobante_id == TTIPOSCOMPROBANTES.PAGO:

            _dbField_fechaPagoMax = dbc01.tcfdi_complementopagos.fechapago.max()
            _dbRows_pagosMax = dbc01(
                dbc01.tcfdi_complementopagos.cfdi_id == _dbRow.id
                ).select(
                    _dbField_fechaPagoMax
                    )

            # En el caso de campos calculados, siempre regresa un registro
            _dt_fechaPagoMax = _dbRows_pagosMax.first()[_dbField_fechaPagoMax]

            if _dt_fechaPagoMax:

                if _dt_fechaPagoMax < _dbRow.fecha:
                    _D_return.s_msgError = "Pago creado con fecha anterior a CFDI de ingreso."
                    _D_return.E_return = stvfwk2_e_RETURN.OK_WARNING
                    _dt_fechaPagoMax = _dbRow.fecha
                else:
                    pass

                # Se procede a buscar los movimientos posteriores relacionados con los ingresos a la fecha
                #  del CFDI del parámetro
                _dbRows = dbc01(
                    dbc01.tcfdi_movimientos.cfdi_ingreso_id.belongs(_L_cfdis)
                    & (dbc01.tcfdi_movimientos.fecha > _dt_fechaPagoMax)
                    ).select(
                        dbc01.tcfdi_movimientos.id,
                        dbc01.tcfdi_movimientos.cfdi_id
                        )
            else:
                _dbRows = None

        else:

            # Se procede a buscar los movimientos posteriores relacionados con los ingresos a la fecha
            #  del CFDI del parámetro
            _dbRows = dbc01(
                dbc01.tcfdi_movimientos.cfdi_ingreso_id.belongs(_L_cfdis)
                & (dbc01.tcfdi_movimientos.fecha > _dbRow.fecha)
                ).select(
                    dbc01.tcfdi_movimientos.id,
                    dbc01.tcfdi_movimientos.cfdi_id
                    )

        return _dbRows

    @staticmethod
    def OBTENER_CASCOS1A1_DISPONIBLES_EN_INGRESO(
            s_cfdiIngreso_id,
            s_ignorar_concepto_id = None,
            s_ignorar_descuento_id = None,
            ):
        _D_return = FUNC_RETURN(
            n_cantidad_cascos1a1_posible = 0,
            n_cantidad_cascos1a1_usada = 0,
            )

        _dbRows_conceptos_puedenCasco1a1 = dbc01(
            (dbc01.tcfdi_conceptos.cfdi_id == s_cfdiIngreso_id)
            & (dbc01.tcfdi_conceptos.id != s_ignorar_concepto_id)
            & (dbc01.tempresa_prodserv.id == dbc01.tcfdi_conceptos.empresa_prodserv_id)
            ).select(
                dbc01.tcfdi_conceptos.id,
                dbc01.tcfdi_conceptos.cantidad,
                dbc01.tempresa_prodserv.empresa_prodserv_casco1a1_id,
                dbc01.tempresa_prodcategorias.uso_cascos1a1,
                left = [
                    dbc01.tempresa_prodcategorias.on(
                        dbc01.tempresa_prodcategorias.id == dbc01.tempresa_prodserv.categoria_id
                        )
                    ]
                )
        for _dbRow_concepto in _dbRows_conceptos_puedenCasco1a1:
            if _dbRow_concepto.tempresa_prodserv.empresa_prodserv_casco1a1_id > 0:
                # Si el producto permite uso de casco 1 a 1, se contabilizan las cantidades
                _D_return.n_cantidad_cascos1a1_posible += _dbRow_concepto.tcfdi_conceptos.cantidad

                _dbRows_descuentos = dbc01(
                    (dbc01.tcfdi_concepto_descuentos.cfdi_concepto_id == _dbRow_concepto.tcfdi_conceptos.id)
                    & (dbc01.tcfdi_concepto_descuentos.id != s_ignorar_descuento_id)
                    ).select(
                        dbc01.tcfdi_concepto_descuentos.id,
                        dbc01.tcfdi_concepto_descuentos.tipo,
                        dbc01.tcfdi_concepto_descuentos.cantidad_cascos,
                        )

                for _dbRow_descuento in _dbRows_descuentos:
                    if _dbRow_descuento.tipo == TCFDI_CONCEPTO_DESCUENTOS.E_TIPO.CASCO_1A1:
                        _D_return.n_cantidad_cascos1a1_usada += _dbRow_descuento.cantidad_cascos
                    else:
                        pass

            else:
                # El producto no referencia casco1a1
                pass
        return _D_return

    @classmethod
    def OBTENER_CASCOS1A1_DISPONIBLES_EN_EGRESO(cls, s_cfdi_id, s_ignorar_concepto_id = None):
        """ Se buscan los ingresos y egresos relacionados para determinar la cantidad de cascos ejercidos

        @param s_cfdi_id: es el if del cfdi de egreso
        @param s_ignorar_concepto_id: id del concepto a ignorar en el cálculo de cascos
        """
        _D_return = FUNC_RETURN(
            n_cantidad_cascos1a1_posible = 0,
            n_cantidad_cascos1a1_usada = 0,
            n_cantidad_cascos1a1_regresada = 0,
            b_aplicaCasco1a1 = False
            )

        _dbTabla = dbc01.tcfdis

        # Se buscan todos los ingresos del documento de egreso
        _dbRows_ingresos_cfdis = dbc01(
            (dbc01.tcfdi_relacionados.cfdi_id == s_cfdi_id)
            ).select(
                dbc01.tcfdi_relacionados.cfdirelacionado_id,
                )

        # Por cada ingreso relacionado (por el momento solo puede ser uno), se van contando los productos
        #  que pueden relacionarse con productos de caso1a1
        for _dbRow_ingreso_cfdi in _dbRows_ingresos_cfdis:

            _s_cfdiIngreso_id = _dbRow_ingreso_cfdi.cfdirelacionado_id
            _D_results = cls.OBTENER_CASCOS1A1_DISPONIBLES_EN_INGRESO(s_cfdi_id, s_ignorar_concepto_id)
            _D_return.n_cantidad_cascos1a1_posible = _D_results.n_cantidad_cascos1a1_posible
            _D_return.n_cantidad_cascos1a1_usada = _D_results.n_cantidad_cascos1a1_usada

            # TODOMejora, no se esta considerando si el egreso tiene más ingresos. En ese caso no se debería poder
            #  validar la cantidad de cascos 1a1
            _dbRows_conceptosEnEgresosDelIngreso = dbc01(
                (dbc01.tcfdi_relacionados.cfdirelacionado_id == _s_cfdiIngreso_id)
                & (dbc01.tcfdis.id == dbc01.tcfdi_relacionados.cfdi_id)
                & (dbc01.tcfdis.sat_status != TCFDIS.SAT_STATUS.CANCELADO)
                & (dbc01.tcfdi_conceptos.cfdi_id == dbc01.tcfdi_relacionados.cfdi_id)
                & (dbc01.tcfdi_conceptos.id != s_ignorar_concepto_id)
                & (dbc01.tempresa_prodserv.id == dbc01.tcfdi_conceptos.empresa_prodserv_id)
                & (dbc01.tempresa_prodcategorias.id == dbc01.tempresa_prodserv.categoria_id)
                ).select(
                    dbc01.tcfdi_conceptos.id,
                    dbc01.tcfdi_conceptos.cantidad,
                    dbc01.tcfdi_conceptos.aplicacasco_1a1,
                    dbc01.tempresa_prodserv.empresa_prodserv_casco1a1_id,
                    dbc01.tempresa_prodcategorias.uso_cascos1a1
                    )
            for _dbRow_concepto in _dbRows_conceptosEnEgresosDelIngreso:
                if _dbRow_concepto.tempresa_prodserv.empresa_prodserv_casco1a1_id > 0:
                    # Si el producto permite tiene casco 1a1 relacionado; se contabilizan las cantidades
                    _D_return.n_cantidad_cascos1a1_regresada += _dbRow_concepto.tcfdi_conceptos.cantidad

                elif _dbRow_concepto.tcfdi_conceptos.aplicacasco_1a1:
                    # Si el producto corresponde a una categoría de casco 1 a 1 y se aplico como casco 1 a 1
                    _D_return.n_cantidad_cascos1a1_usada += _dbRow_concepto.tcfdi_conceptos.cantidad

                else:
                    # El producto no referencia casco1a1
                    pass

        return _D_return

    def comando_refrescar(self, x_cfdi):
        _L_camposActualizar = STV_LIB_DB.COMANDO_REFRESCAR(
            x_cfdi,
            self,
            [
                self.subtotal,
                self.descuento,
                self.totalimpuestostrasladados,
                self.totalimpuestosretenidos,
                self.total,
                self.saldo,
                self.errores,
                ]
            )
        return _L_camposActualizar

    class PROC:

        dbTabla = None  # Se escribe con la inicialización
        dbTabla2 = None  # Se escribe con la inicialización de Tabla2, contendrá los campos adicionales

        def __init__(self, **D_defaults):
            """ Inicializa objecto de tipo PROC para control del registro actual

            @param D_defaults:
            @type D_defaults:
            """
            self._D_defaults = Storage(D_defaults)
            self._L_requeridos = ['empresa_id']
            self.dbTabla.notas.readable = True
            return

        def puede_crear(self, **D_defaults):
            """ Determina si puede o no crear un CFDI de pago.

            Actualmente no hay condición requerida.
            """
            _D_return = FUNC_RETURN()

            self._D_defaults.update(D_defaults or {})

            for _s_requerido in self._L_requeridos:
                if _s_requerido not in self._D_defaults:
                    _D_return.agrega_error("Campo %s es requerido para generar CFDI" % _s_requerido)

                else:
                    pass

            return _D_return

        @classmethod
        def PUEDE_EDITAR(cls, x_cfdi, b_checarComplemento = True):
            """ Regresa True si el estado permite modificar, de lo contrario False

            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi, cls.dbTabla)
            _dbRow = _D_results.dbRow

            if not _dbRow:
                _D_return.agrega_error("CFDI no identificado.")

            elif not _dbRow.uuid \
                    and (_dbRow.estado in (TCFDIS.E_ESTADO.NO_DEFINIDO, TCFDIS.E_ESTADO.TIMBRE_RECHAZADO)):

                _dbRows = TCFDIS.OBTENER_CFDIS_POSTERIORES(_dbRow)

                if _dbRows:
                    # Si existen registros, significa que tiene movimientos posteriores al CFDI del parámetro
                    #  que se intenta modificar
                    _L_cfdis = [str(_dbRow.cfdi_id) for _dbRow in _dbRows]
                    _D_return.agrega_error("CFDI con movimientos posteriores. CFDI ID %s" % ", ".join(_L_cfdis))

                else:
                    pass

            elif _dbRow.estado in TCFDIS.E_ESTADO.L_SINPROCESOPENDIENTE:
                _D_return.agrega_error("CFDI timbrado y no puede ser modificado.")

            else:
                # Si el CFDI está en un proceso con estado transitorio

                # Se verifica si existen instrucciones en proceso
                _dbRows_instrucciones = db(
                    (db.tservidor_instrucciones.cuenta_id == D_stvFwkCfg.dbRow_cuenta.id)
                    & (
                        db.tservidor_instrucciones.tipoinstruccion.belongs(
                            TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.L_CONCFDI_EN_PARAM3
                            )
                        )
                    & (db.tservidor_instrucciones.estado.belongs(TSERVIDOR_INSTRUCCIONES.E_ESTADO.L_ACTIVOS))
                    & (db.tservidor_instrucciones.s_param3 == _dbRow.id)
                    ).select(
                        db.tservidor_instrucciones.ALL,
                        )

                if _dbRows_instrucciones:
                    # TODO evaluar si el timeout esta done, para cancelar o terminar la instrucción
                    _D_return.agrega_error("CFDI en estado transitorio.")

                else:
                    if _dbRow.uuid:

                        if _dbRow.sat_status == TCFDIS.SAT_STATUS.VIGENTE:
                            _dbRow.estado = TCFDIS.E_ESTADO.VIGENTE
                            _D_return.agrega_error("CFDI en Vigente.")

                        elif _dbRow.sat_status == TCFDIS.SAT_STATUS.CANCELADO:
                            _dbRow.estado = TCFDIS.E_ESTADO.CANCELADO
                            _D_return.agrega_error("CFDI Cancelado.")

                        elif _dbRow.estado == TCFDIS.E_ESTADO.EN_PROCESO_CANCELACION:
                            _dbRow.estado = TCFDIS.E_ESTADO.VIGENTE
                            _D_return.agrega_advertencia("Como no existe instrucción pendiente, "
                                + "se regresa al estado anterior")

                        elif _dbRow.estado == TCFDIS.E_ESTADO.EN_PROCESO_FIRMA:
                            _dbRow.estado = TCFDIS.E_ESTADO.NO_DEFINIDO
                            _D_return.agrega_advertencia("Como no existe instrucción pendiente, "
                                + "se regresa al estado anterior")

                        else:
                            _D_return.agrega_error("CFDI en estado no reconocido, contacte al administrador")

                    else:
                        _dbRow.estado = TCFDIS.E_ESTADO.NO_DEFINIDO

                    _dbRow.update_record()

            if _D_return.E_return == stvfwk2_e_RETURN.OK:
                if _dbRow.cfdi_complemento_id and b_checarComplemento:
                    # Si todo va bien, se verifica lo mismo para el cfdi complemento
                    _D_results = cls.PUEDE_EDITAR(_dbRow.cfdi_complemento_id)
                    if _D_results.E_return > stvfwk2_e_RETURN.OK_WARNING:
                        _D_return.combinar(_D_results, "CFDI Complemento %d:" % _dbRow.cfdi_complemento_id)
                    else:
                        pass
                elif dbc01(dbc01.tcfdis.cfdi_complemento_id == _dbRow.id).count() > 0:
                    # Si el CFDI actual es un CFDI complemento, no puede ser editado
                    _D_return.agrega_error("CFDI complemento, su manejo es automático y no puede ser editado")
                else:
                    pass
            else:
                pass

            return _D_return

        @classmethod
        def PUEDE_ELIMINAR(cls, x_cfdi):
            """ Determina si puede o no eliminar un registro

            Misma condición que puede_editar, a lo mejor pudieran verse condiciones distintas

            """

            return cls.PUEDE_EDITAR(x_cfdi, b_checarComplemento = False)

        def configuracampos_nuevo(self, s_cfdi_id, b_llamadaDesdeEditar = False, **D_defaults):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param b_llamadaDesdeEditar:
            @type b_llamadaDesdeEditar:
            @param s_cfdi_id:
            @type s_cfdi_id:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            self._D_defaults.update(D_defaults or {})
            for _s_fieldName in self.dbTabla.fields:
                self.dbTabla[_s_fieldName].writable = False
            for _s_fieldName in self._D_defaults:
                self.dbTabla[_s_fieldName].default = self._D_defaults[_s_fieldName]

            if not b_llamadaDesdeEditar:
                _D_results = self.puede_crear()
            else:
                _D_results = _D_return

            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.combinar(_D_results)

            elif s_cfdi_id:
                _dbRow_base = self.dbTabla(s_cfdi_id)

                self.dbTabla.empresa_id.default = _dbRow_base.empresa_id
                self.dbTabla.versioncfdi.default = _dbRow_base.versioncfdi
                # self.dbTabla.serie.default = _dbRow_base.serie
                # self.dbTabla.empresa_serie_id.default = _dbRow_base.empresa_serie_id
                self.dbTabla.empresa_plaza_sucursal_cajachica_id.default = \
                    _dbRow_base.empresa_plaza_sucursal_cajachica_id
                # self.dbTabla.folio.default = _dbRow_base.folio
                # self.dbTabla.fecha_str.default = _dbRow_base.fecha_str
                # self.dbTabla.fecha.default = _dbRow_base.fecha
                # self.dbTabla.dia.default = _dbRow_base.dia
                # self.dbTabla.hora.default = _dbRow_base.hora
                # self.dbTabla.formapago.default = _dbRow_base.formapago
                self.dbTabla.formapago_id.default = _dbRow_base.formapago_id
                self.dbTabla.condicionesdepago.default = _dbRow_base.condicionesdepago
                # self.dbTabla.subtotal.default = _dbRow_base.subtotal
                # self.dbTabla.descuento.default = _dbRow_base.descuento
                # self.dbTabla.moneda.default = _dbRow_base.moneda
                # self.dbTabla.moneda_id.default = _dbRow_base.moneda_id
                # self.dbTabla.monedacontpaqi_id.default = _dbRow_base.monedacontpaqi_id
                # self.dbTabla.tipocambio.default = _dbRow_base.tipocambio
                # self.dbTabla.total.default = _dbRow_base.total
                # self.dbTabla.totalpago.default = _dbRow_base.totalpago
                # self.dbTabla.saldo.default = _dbRow_base.saldo
                # self.dbTabla.saldofechacalculo.default = _dbRow_base.saldofechacalculo
                # self.dbTabla.tipodecomprobante.default = _dbRow_base.tipodecomprobante
                self.dbTabla.tipocomprobante_id.default = _dbRow_base.tipocomprobante_id
                # self.dbTabla.metodopago.default = _dbRow_base.metodopago
                self.dbTabla.metodopago_id.default = _dbRow_base.metodopago_id
                # self.dbTabla.lugarexpedicion.default = _dbRow_base.lugarexpedicion
                self.dbTabla.lugarexpedicion_id.default = _dbRow_base.lugarexpedicion_id
                # self.dbTabla.totalimpuestostrasladados.default = _dbRow_base.totalimpuestostrasladados
                # self.dbTabla.totalimpuestosretenidos.default = _dbRow_base.totalimpuestosretenidos
                # self.dbTabla.credito_plazo.default = _dbRow_base.credito_plazo
                # self.dbTabla.tiporelacion.default = _dbRow_base.tiporelacion
                self.dbTabla.tiporelacion_id.default = _dbRow_base.tiporelacion_id
                # self.dbTabla.emisorrfc.default = _dbRow_base.emisorrfc
                # self.dbTabla.emisornombrerazonsocial.default = _dbRow_base.emisornombrerazonsocial
                # self.dbTabla.emisorregimenfiscal.default = _dbRow_base.emisorregimenfiscal
                # self.dbTabla.emisorproveedor_id.default = _dbRow_base.emisorproveedor_id
                # self.dbTabla.receptorrfc.default = _dbRow_base.receptorrfc
                # self.dbTabla.receptornombrerazonsocial.default = _dbRow_base.receptornombrerazonsocial
                # self.dbTabla.receptorusocfdi.default = _dbRow_base.receptorusocfdi
                self.dbTabla.receptorusocfdi_id.default = _dbRow_base.receptorusocfdi_id
                self.dbTabla.receptorcliente_id.default = _dbRow_base.receptorcliente_id
                # self.dbTabla.uuid.default = _dbRow_base.uuid
                # self.dbTabla.fechatimbrado_str.default = _dbRow_base.fechatimbrado_str
                # self.dbTabla.fechatimbrado.default = _dbRow_base.fechatimbrado
                # self.dbTabla.rol.default = _dbRow_base.rol
                # self.dbTabla.etapa.default = _dbRow_base.etapa
                # self.dbTabla.etapa_cancelado.default = _dbRow_base.etapa_cancelado
                # self.dbTabla.estado.default = _dbRow_base.estado
                # self.dbTabla.generado_por.default = _dbRow_base.generado_por
                # self.dbTabla.errores.default = _dbRow_base.errores
                # self.dbTabla.sat_status.default = _dbRow_base.sat_status
                # self.dbTabla.sat_fecha_cancelacion.default = _dbRow_base.sat_fecha_cancelacion
                # self.dbTabla.sat_status_cancelacion.default = _dbRow_base.sat_status_cancelacion
                # self.dbTabla.sat_status_proceso_cancelacion.default = _dbRow_base.sat_status_proceso_cancelacion
                # self.dbTabla.pac_rfc.default = _dbRow_base.pac_rfc
                # self.dbTabla.pac_razon_social.default = _dbRow_base.pac_razon_social
                # self.dbTabla.pac_nombre_comercial.default = _dbRow_base.pac_nombre_comercial
                # self.dbTabla.cancelado_folio.default = _dbRow_base.cancelado_folio
                # self.dbTabla.cfdi_complemento_id.default = _dbRow_base.cfdi_complemento_id
                # self.dbTabla.facturacomplementos.default = _dbRow_base.facturacomplementos
                self.dbTabla.emp_pla_suc_almacen_id.default = _dbRow_base.emp_pla_suc_almacen_id

                # self.dbTabla2.interesmoratoriopagare.default = _dbRow_base.interesmoratoriopagare
                # self.dbTabla2.ordencompra.default = _dbRow_base.ordencompra
                # self.dbTabla2.foliointernopedido.default = _dbRow_base.foliointernopedido
                # self.dbTabla2.agentevendedor.default = _dbRow_base.agentevendedor
                # self.dbTabla2.montobaseprodiva0.default = _dbRow_base.montobaseprodiva0
                # self.dbTabla2.montobaseprodiva.default = _dbRow_base.montobaseprodiva
                # self.dbTabla2.emisorcalle.default = _dbRow_base.emisorcalle
                # self.dbTabla2.emisornumexterior.default = _dbRow_base.emisornumexterior
                # self.dbTabla2.emisornuminterior.default = _dbRow_base.emisornuminterior
                # self.dbTabla2.emisorcolonia.default = _dbRow_base.emisorcolonia
                # self.dbTabla2.emisorlocalidad.default = _dbRow_base.emisorlocalidad
                # self.dbTabla2.emisormunicipio.default = _dbRow_base.emisormunicipio
                # self.dbTabla2.emisorestado.default = _dbRow_base.emisorestado
                # self.dbTabla2.emisorpais.default = _dbRow_base.emisorpais
                # self.dbTabla2.emisorcodigopostal.default = _dbRow_base.emisorcodigopostal

                # self.dbTabla2.receptoremails.default = _dbRow_base.receptoremails
                # self.dbTabla2.receptortelefonos.default = _dbRow_base.receptortelefonos
                # self.dbTabla2.receptorcalle.default = _dbRow_base.receptorcalle
                # self.dbTabla2.receptorreferencia.default = _dbRow_base.receptorreferencia
                # self.dbTabla2.receptornumexterior.default = _dbRow_base.receptornumexterior
                # self.dbTabla2.receptornuminterior.default = _dbRow_base.receptornuminterior
                # self.dbTabla2.receptorcolonia.default = _dbRow_base.receptorcolonia
                # self.dbTabla2.receptorlocalidad.default = _dbRow_base.receptorlocalidad
                # self.dbTabla2.receptormunicipio.default = _dbRow_base.receptormunicipio
                # self.dbTabla2.receptorestado.default = _dbRow_base.receptorestado
                # self.dbTabla2.receptorpais.default = _dbRow_base.receptorpais
                # self.dbTabla2.receptorcodigopostal.default = _dbRow_base.receptorcodigopostal

            else:
                pass

            self.dbTabla.empresa_id.writable = False
            self.dbTabla.empresa_plaza_sucursal_cajachica_id.writable = True
            self.dbTabla.fecha.writable = True
            self.dbTabla.formapago_id.writable = True
            self.dbTabla.condicionesdepago.writable = True
            self.dbTabla.moneda_id.writable = False
            self.dbTabla.monedacontpaqi_id.writable = True
            self.dbTabla.tipocambio.writable = True
            self.dbTabla.tipocomprobante_id.writable = True
            self.dbTabla.metodopago_id.writable = True
            self.dbTabla.lugarexpedicion_id.writable = False
            self.dbTabla.tiporelacion_id.writable = True
            self.dbTabla.tiporelacion_id.requires = IS_NULL_OR(
                IS_IN_DB(db01, 'ttiposrelaciones.id', db01.ttiposrelaciones._format)
                )
            self.dbTabla.tiporelacion_id.comment = "Configurar únicamente en el caso de agregar CFDIs relacionados"
            self.dbTabla.emisorproveedor_id.writable = False
            self.dbTabla.receptorusocfdi_id.writable = True
            self.dbTabla.receptorcliente_id.writable = True  # Se pone por default el cliente actual
            self.dbTabla.uuid.writable = False
            self.dbTabla.fechatimbrado.writable = False
            self.dbTabla.rol.writable = False
            self.dbTabla.etapa.writable = False
            self.dbTabla.etapa_cancelado.writable = False
            self.dbTabla.estado.writable = False
            self.dbTabla.generado_por.writable = False
            self.dbTabla.errores.writable = False
            self.dbTabla.sat_status.writable = False
            self.dbTabla.sat_fecha_cancelacion.writable = False
            self.dbTabla.sat_status_cancelacion.writable = False
            self.dbTabla.sat_status_proceso_cancelacion.writable = False
            self.dbTabla.pac_rfc.writable = False
            self.dbTabla.pac_razon_social.writable = False
            self.dbTabla.pac_nombre_comercial.writable = False
            self.dbTabla.cancelado_folio.writable = False
            self.dbTabla.cfdi_complemento_id.writable = False
            self.dbTabla.facturacomplementos.writable = False

            self.dbTabla.emp_pla_suc_almacen_id.writable = True

            self.dbTabla2.ordencompra.writable = True
            self.dbTabla2.foliointernopedido.writable = True
            self.dbTabla2.agentevendedor.writable = False
            self.dbTabla2.montobaseprodiva0.writable = False
            self.dbTabla2.montobaseprodiva.writable = False

            # Campos que no deben ser editados, solo de forma autocalculada
            self.dbTabla.versioncfdi.writable = False
            self.dbTabla.serie.writable = False
            self.dbTabla.empresa_serie_id.writable = False
            self.dbTabla.folio.writable = False
            self.dbTabla.fecha_str.writable = False
            self.dbTabla.dia.writable = False
            self.dbTabla.hora.writable = False
            self.dbTabla.formapago.writable = False
            self.dbTabla.subtotal.writable = False
            self.dbTabla.descuento.writable = False
            self.dbTabla.moneda.writable = False
            self.dbTabla.total.writable = False
            self.dbTabla.totalpago.writable = False
            self.dbTabla.saldo.writable = False
            self.dbTabla.saldofechacalculo.writable = False
            self.dbTabla.tipodecomprobante.writable = False
            self.dbTabla.metodopago.writable = False
            self.dbTabla.lugarexpedicion.writable = False
            self.dbTabla.totalimpuestostrasladados.writable = False
            self.dbTabla.totalimpuestosretenidos.writable = False
            self.dbTabla.credito_plazo.writable = False  # Se obtiene del cliente
            self.dbTabla.tiporelacion.writable = False
            self.dbTabla.emisorrfc.writable = False
            self.dbTabla.emisornombrerazonsocial.writable = False
            self.dbTabla.emisorregimenfiscal.writable = False
            self.dbTabla.receptorrfc.writable = False
            self.dbTabla.receptornombrerazonsocial.writable = False
            self.dbTabla.receptorusocfdi.writable = False
            self.dbTabla.fechatimbrado_str.writable = False

            self.dbTabla2.interesmoratoriopagare.writable = False
            self.dbTabla2.emisorcalle.writable = False
            self.dbTabla2.emisornumexterior.writable = False
            self.dbTabla2.emisornuminterior.writable = False
            self.dbTabla2.emisorcolonia.writable = False
            self.dbTabla2.emisorlocalidad.writable = False
            self.dbTabla2.emisormunicipio.writable = False
            self.dbTabla2.emisorestado.writable = False
            self.dbTabla2.emisorpais.writable = False
            self.dbTabla2.emisorcodigopostal.writable = False
            self.dbTabla2.receptoremails.writable = False
            self.dbTabla2.receptortelefonos.writable = False
            self.dbTabla2.receptorcalle.writable = False
            self.dbTabla2.receptorreferencia.writable = False
            self.dbTabla2.receptornumexterior.writable = False
            self.dbTabla2.receptornuminterior.writable = False
            self.dbTabla2.receptorcolonia.writable = False
            self.dbTabla2.receptorlocalidad.writable = False
            self.dbTabla2.receptormunicipio.writable = False
            self.dbTabla2.receptorestado.writable = False
            self.dbTabla2.receptorpais.writable = False
            self.dbTabla2.receptorcodigopostal.writable = False

            # Campos que todos los movimientos pueden editar
            self.dbTabla.notas.writable = True

            # Se definen los valores por default obligados
            self.dbTabla.versioncfdi.default = '3.3'
            self.dbTabla.empresa_id.default = self._D_defaults.empresa_id
            self.dbTabla.rol.default = TCFDIS.E_ROL.EMISOR
            self.dbTabla.sat_status.default = TCFDIS.SAT_STATUS.NO_DEFINIDO

            return _D_return

        def configuracampos_edicion(self, x_cfdi):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param x_cfdi: es el registro del movimiento o su id
            @type x_cfdi:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi, self.dbTabla)
            _dbRow = _D_results.dbRow

            # Se llenan los default obligados
            for _s_nombreCampo in self._L_requeridos:
                self._D_defaults[_s_nombreCampo] = _D_results.dbRow[_s_nombreCampo]

            _D_results = self.PUEDE_EDITAR(_dbRow)

            if _D_results.E_return == CLASS_e_RETURN.OK:
                # Si puede editar regresa un ok y el movimiento esta abierto

                # Se manda vacío el argumento para no agregar defaults a los campos
                _D_results = self.configuracampos_nuevo(
                    None,
                    b_llamadaDesdeEditar = True,
                    **self._D_defaults
                    )
                _D_return.combinar(_D_results)

                # Tipo de cambio no puede ser editado si ya hay conceptos
                if _dbRow.tcfdi_conceptos.count() > 0:
                    self.dbTabla.tipocambio.writable = False
                    self.dbTabla.monedacontpaqi_id.writable = False
                else:
                    pass

                # Si ya tiene CFDI relacionados, no se puede cambiar el tipo de relación
                if _dbRow.tcfdi_relacionados.count() > 0:
                    self.dbTabla.tiporelacion_id.writable = False
                else:
                    pass

            else:
                # De lo contrario en cualquier otro caso, solo se pueden editar las observaciones
                for _s_fieldName in self.dbTabla.fields:
                    self.dbTabla[_s_fieldName].writable = False

                self.dbTabla.notas.writable = True

            return _D_return

        def al_validar(self, O_form, dbRow = None, D_camposAChecar = None):
            """ Validar la información para poder grabar cfdi

            Las validaciones en esta función son genéricas, se espera que existan validaciones
             específicas por tipo de CFDI

            @param D_camposAChecar:
            @type D_camposAChecar:
            @param dbRow:
            @type dbRow:
            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            if not dbRow:
                _dbRow = self.dbTabla(O_form.record_id) if O_form.record_id else None
            else:
                _dbRow = dbRow
            if not D_camposAChecar:
                _D_camposAChecar = STV_FWK_LIB.OBTENER_CAMPOSACHECAR_PARA_VALIDAR(self.dbTabla, _dbRow)
            else:
                _D_camposAChecar = D_camposAChecar

            if not O_form.record_id:
                # Se esta creando el registro

                _D_results = self.puede_crear(**_D_camposAChecar)
                if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                    O_form.errors.id = _D_results.s_msgError
                else:
                    pass

            else:
                # Se esta modificando el registro, la llamada a configuraCampos_edicion ya definió con writable
                #  que puede modificarse y que no. Por lo que el código para verificar si puede o no editar
                #  no es requerido en este caso.

                if self.dbTabla.tiporelacion_id.writable and (_dbRow.tcfdi_relacionados.count() > 0):
                    if not _D_camposAChecar.tiporelacion_id:
                        O_form.errors.tiporelacion_id = "Tipo relación no puede estar vacío ya que hay CFDIs " \
                            + "relacionados"
                    else:
                        pass
                else:
                    pass

            # Validaciones

            if _D_camposAChecar.versioncfdi != "3.3":
                O_form.errors.id = "Versión del CFDI, debe ser 3.3"
            else:
                pass
            if not _D_camposAChecar.empresa_id:
                O_form.errors.id = "Empresa emisora debe ser especificada"
            else:
                pass
            if self.dbTabla.receptorusocfdi_id.writable and not _D_camposAChecar.receptorusocfdi_id:
                O_form.errors.receptorusocfdi_id = "Uso CFDI debe definirse"
            else:
                pass
            if not _D_camposAChecar.tipocomprobante_id:
                O_form.errors.tipocomprobante_id = "Tipo comprobante debe definirse"
            else:
                pass

            return O_form

        @classmethod
        def AL_ACEPTAR(cls, O_form, dbRow = None):
            """ Aceptación captura de inventario inicial producto

            Se hacen todos los cálculos y aceptación de los cambios en el producto

            @param dbRow:
            @type dbRow:
            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()
            dbRow = dbRow if dbRow else cls.dbTabla(O_form.record_id or O_form.vars.id)

            if not O_form.record_id:
                # Si se inserta
                pass

            else:  # Si se edita
                pass

            _D_results = cls.MAPEAR_CAMPOS(dbRow)
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.combinar(_D_results)

            else:
                _D_results = cls.CALCULAR_CAMPOS(dbRow)
                _D_return.combinar(_D_results)

            generaMensajeFlash(cls.dbTabla, _D_return)
            return O_form

        @classmethod
        def ANTES_ELIMINAR(cls, L_errorsAction, dbRow):
            """ Antes de eliminar un movimiento de inventario de este tipo

            @param dbRow:
            @type dbRow:
            @param L_errorsAction:
            @type L_errorsAction:
            @return: errores
            @rtype: lista de diccionarios {'msg':'...'}
            """
            _D_results = cls.PUEDE_ELIMINAR(dbRow)
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                L_errorsAction.append({'msg': _D_results.s_msgError})
            else:
                cls._dbRow_aEliminar = Storage(dbRow.as_dict())

            return L_errorsAction

        @classmethod
        def DESPUES_ELIMINAR(cls, L_errorsAction):
            """ Después de eliminar un registro

            @param L_errorsAction:
            @type L_errorsAction:
            @return: errores
            @rtype: lista de diccionarios {'msg':'...'}
            """
            _D_return = FUNC_RETURN()

            if cls._dbRow_aEliminar:
                if cls._dbRow_aEliminar.cfdi_complemento_id:
                    _D_results = cls.PUEDE_ELIMINAR(cls._dbRow_aEliminar.cfdi_complemento_id)
                    if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                        _D_return.combinar(_D_results, "Error al borrar complemento: ")
                    else:
                        dbc01.tcfdis(cls._dbRow_aEliminar.cfdi_complemento_id).delete_record()
                else:
                    pass

                _D_results = TCFDIS.EDOCTA(
                    n_empresa_id = cls._dbRow_aEliminar.empresa_id,
                    n_cliente_id = cls._dbRow_aEliminar.receptorcliente_id,
                    E_movto_generado_por = TCFDI_MOVIMIENTOS.E_GENERADO_POR.DOCUMENTO,
                    ).procesar_cfdi(cls._dbRow_aEliminar.id)
                _D_return.combinar(_D_results)

            else:
                _D_return.E_return = CLASS_e_RETURN.NOK_SECUENCIA
                _D_return.s_msgError += "Error en secuencia de llamado de funciones para borrado. "

            generaMensajeFlash(cls.dbTabla, _D_return)
            return L_errorsAction

        @classmethod
        def MAPEAR_CAMPOS(cls, x_cfdi, b_actualizarRegistro = True):
            """ Remapea campos solamente cuando se graba el registro

            @param b_actualizarRegistro:
            @type b_actualizarRegistro:
            @param x_cfdi:
            @type x_cfdi:
            @return:
            @rtype:
            """

            def mapear_serie(dbRow):

                # noinspection PyShadowingNames
                _D_return = FUNC_RETURN(
                    n_emp_plaz_suc_id = None,
                    empresa_serie_id = None
                    )

                _dbRow_cajachica = None

                if dbRow.tipocomprobante_id in (
                        TTIPOSCOMPROBANTES.INGRESO,
                        TTIPOSCOMPROBANTES.EGRESO,
                        TTIPOSCOMPROBANTES.PAGO
                        ):

                    if dbRow.empresa_plaza_sucursal_cajachica_id:
                        # Si se define la cajachica, de ahí se obtiene la seria
                        _dbRow_cajachica = dbc01.tempresa_plaza_sucursal_cajaschicas(
                            dbRow.empresa_plaza_sucursal_cajachica_id
                            )
                        _D_return.n_emp_plaz_suc_id = _dbRow_cajachica.empresa_plaza_sucursal_id

                        if dbRow.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO:
                            _D_return.empresa_serie_id = _dbRow_cajachica.empresa_serie_ingreso_id

                        elif dbRow.tipocomprobante_id == TTIPOSCOMPROBANTES.EGRESO:
                            _D_return.empresa_serie_id = _dbRow_cajachica.empresa_serie_egreso_id

                        else:  # dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.PAGO
                            _D_return.empresa_serie_id = _dbRow_cajachica.empresa_serie_pago_id

                    else:
                        _D_return.agrega_error("Serie no pudo ser identificada debido a que no está definida "
                            + "la caja chica")

                elif dbRow.tipocomprobante_id == TTIPOSCOMPROBANTES.TRASLADO:

                    if dbRow.emp_pla_suc_almacen_id:
                        # Si tiene asociado el almacén, de ahí se obtiene la serie
                        _dbRow_almacen = dbc01.tempresa_plaza_sucursal_almacenes(dbRow.emp_pla_suc_almacen_id)
                        _D_return.n_emp_plaz_suc_id = _dbRow_almacen.empresa_plaza_sucursal_id
                        _D_return.empresa_serie_id = _dbRow_almacen.empresa_serie_traslado_id
                    else:
                        _D_return.empresa_serie_id = None

                    if not _D_return.empresa_serie_id:
                        _D_return.agrega_error("Serie no pudo ser identificada debido a que no está definida "
                            + "en el almacén")
                    else:
                        pass

                else:
                    _D_return.agrega_error("Tipo de comprobante no soportado")

                return _D_return

            def mapear_lugarexpedicion(
                    dbRow,
                    n_emp_plaz_suc_id
                    ):

                # noinspection PyShadowingNames
                _D_return = FUNC_RETURN(
                    lugarexpedicion = "",
                    lugarexpedicion_id = None
                    )

                if not n_emp_plaz_suc_id:

                    if dbRow.empresa_plaza_sucursal_cajachica_id:
                        _dbRow_cajachica = dbc01.tempresa_plaza_sucursal_cajaschicas(
                            dbRow.empresa_plaza_sucursal_cajachica_id
                            )
                        n_emp_plaz_suc_id = _dbRow_cajachica.empresa_plaza_sucursal_id

                    elif dbRow.emp_pla_suc_almacen_id:
                        _dbRow_almacen = dbc01.tempresa_plaza_sucursal_almacenes(dbRow.emp_pla_suc_almacen_id)
                        n_emp_plaz_suc_id = _dbRow_almacen.empresa_plaza_sucursal_id

                    else:
                        pass
                else:
                    pass

                if n_emp_plaz_suc_id:
                    # Si esta definido el almacén, se usa el codigo postal del almacen, si no se usa el de la sucursal
                    _s_almacen_lugarexpedicion = dbc01.tempresa_plaza_sucursal_almacenes(
                        dbRow.emp_pla_suc_almacen_id
                        ).codigopostal if dbRow.emp_pla_suc_almacen_id else ""

                    _D_return.lugarexpedicion = dbc01.tempresa_plaza_sucursales(n_emp_plaz_suc_id).codigopostal \
                        if not _s_almacen_lugarexpedicion else _s_almacen_lugarexpedicion

                    _D_result = STV_LIB_DB.CACHE_ASOCIAR_TABLA(
                        s_cache_id = 'cfdi_asociar_lugarexpedicion' + str(_D_return.lugarexpedicion),
                        n_time_expire = 86400,
                        dbTabla = db01.tcodigospostales,
                        s_nombreCampo = 'c_codigopostal',
                        s_datoBuscar = _D_return.lugarexpedicion,
                        )
                    if _D_result.E_return == stvfwk2_e_RETURN.OK:
                        _D_return.lugarexpedicion_id = _D_result.n_id
                    else:
                        _D_return.lugarexpedicion_id = None
                        _D_return.agrega_error("No se identificó el código postal definido.")
                else:
                    _D_return.lugarexpedicion_id = None
                    _D_return.agrega_error("No se identifico la sucursal o no tiene código postal definido.")

                return _D_return

            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _dbRows_cfdis2 = _dbRow.tcfdis2.select()
            if _dbRows_cfdis2:
                _dbRow_cfdi2 = _dbRows_cfdis2.first()
            else:
                _n_cfdi2_id = dbc01.tcfdis2.insert(cfdi_id = _dbRow.id)
                _dbRow_cfdi2 = dbc01.tcfdis2(_n_cfdi2_id)

            _D_actualizar = Storage(
                versioncfdi = "3.3",
                empresa_serie_id = _dbRow.empresa_serie_id,
                serie = "",
                lugarexpedicion = _dbRow.lugarexpedicion,
                lugarexpedicion_id = _dbRow.lugarexpedicion_id,
                # folio = _dbRow.folio,  es un caso especial de obtener y grabar
                fecha = None,
                fecha_str = "",
                dia = None,
                hora = None,
                tipodecomprobante = "",
                formapago = "",
                metodopago = "",
                moneda_id = None,
                moneda = "",
                receptorusocfdi = "",
                tiporelacion = "",
                emisorrfc = "",
                emisornombrerazonsocial = "",
                emisorregimenfiscal = "",
                credito_plazo = 0,
                receptorrfc = "",
                receptornombrerazonsocial = "",
                fechatimbrado_str = "",
                facturacomplementos = TEMPRESA_CLIENTES.E_FACTURARCOMPLEMENTOS.MISMAFACTURA
                )

            _D_actualizar2 = Storage(
                interesmoratoriopagare = 0,
                # ordencompra = "",
                # foliointernopedido = "",
                # agentevendedor = "",
                emisorcalle = "",
                emisornumexterior = "",
                emisornuminterior = "",
                emisorcolonia = "",
                emisorlocalidad = "",
                emisormunicipio = "",
                emisorestado = "",
                emisorpais = "",
                emisorcodigopostal = "",
                receptoremails = "",
                receptortelefonos = "",
                receptorcalle = "",
                receptorreferencia = "",
                receptornumexterior = "",
                receptornuminterior = "",
                receptorcolonia = "",
                receptorlocalidad = "",
                receptormunicipio = "",
                receptorestado = "",
                receptorpais = "",
                receptorcodigopostal = "",
                )

            _n_emp_plaz_suc_id = None
            if not _dbRow.empresa_serie_id:
                # Si no se tiene la serie, debe recalcularse el lugar de expedición
                _D_actualizar.lugarexpedicion = None

                _D_results = mapear_serie(_dbRow)
                _D_return.combinar(_D_results)
                _D_actualizar.empresa_serie_id = _D_return.empresa_serie_id
                _n_emp_plaz_suc_id = _D_results.n_emp_plaz_suc_id

            else:
                _n_emp_plaz_suc_id = dbc01.tempresa_plaza_sucursal_cajaschicas(
                    _dbRow.empresa_plaza_sucursal_cajachica_id
                    ).empresa_plaza_sucursal_id

            _D_actualizar.serie = dbc01.tempresa_series(_D_actualizar.empresa_serie_id).serie \
                if _D_actualizar.empresa_serie_id else ""

            if not _dbRow.folio and _D_actualizar.empresa_serie_id:
                _dbRow.folio = TCFDIS.OBTENER_ULTIMOFOLIO_SERIE(
                    s_empresa_serie_id = _D_actualizar.empresa_serie_id
                    ) + 1
                _dbRow.update_record()
            else:
                pass

            if not _dbRow.lugarexpedicion and (_D_return.E_return <= CLASS_e_RETURN.OK_WARNING):
                _D_results = mapear_lugarexpedicion(dbRow = _dbRow, n_emp_plaz_suc_id = _n_emp_plaz_suc_id)
                _D_return.combinar(_D_results)
                _D_actualizar.lugarexpedicion = _D_return.lugarexpedicion
                _D_actualizar.lugarexpedicion_id = _D_return.lugarexpedicion_id
            else:
                pass

            _D_actualizar.fecha = _dbRow.fecha if isinstance(
                _dbRow.fecha, datetime.datetime
                ) else request.browsernow
            _D_actualizar.fecha_str = _D_actualizar.fecha.strftime('%Y-%m-%dT%H:%M:%S')
            _D_actualizar.dia = _D_actualizar.fecha.date()
            _D_actualizar.hora = _D_actualizar.fecha.time()

            _D_actualizar.tipodecomprobante = db01.ttiposcomprobantes(_dbRow.tipocomprobante_id).c_tipodecomprobante

            _D_actualizar.formapago = db01.tformaspago(_dbRow.formapago_id).c_formapago \
                if _dbRow.formapago_id else ""
            _D_actualizar.metodopago = db01.tmetodospago(_dbRow.metodopago_id).c_metodopago \
                if _dbRow.metodopago_id else ""
            _D_actualizar.moneda_id = dbc01.tempresa_monedascontpaqi(_dbRow.monedacontpaqi_id).moneda_id \
                if _dbRow.monedacontpaqi_id else None
            _D_actualizar.moneda = db01.tmonedas(_D_actualizar.moneda_id).c_moneda \
                if _D_actualizar.moneda_id else ""

            _D_actualizar.receptorusocfdi = db01.tusoscfdi(_dbRow.receptorusocfdi_id).c_usocfdi \
                if _dbRow.receptorusocfdi_id else ""
            _D_actualizar.tiporelacion = db01.ttiposrelaciones(_dbRow.tiporelacion_id).c_tiporelacion \
                if _dbRow.tiporelacion_id else ""

            _dbRow_empresa = dbc01.tempresas(_dbRow.empresa_id)

            _D_actualizar.emisorrfc = _dbRow_empresa.rfc
            _D_actualizar.emisornombrerazonsocial = _dbRow_empresa.razonsocial
            _D_actualizar.emisorregimenfiscal = db01.tregimenesfiscales(
                _dbRow_empresa.regimenfiscal_id).c_regimenfiscal if _dbRow_empresa.regimenfiscal_id else ""

            # Se usa el domicilio de la sucursal para grabar la información del emisor
            _dbRow_sucursal = dbc01.tempresa_plaza_sucursales(_n_emp_plaz_suc_id)
            _D_actualizar2.interesmoratoriopagare = _dbRow_empresa.interesmoratoriopagare
            _D_actualizar2.emisorcalle = _dbRow_sucursal.calle
            _D_actualizar2.emisornumexterior = _dbRow_sucursal.numexterior
            _D_actualizar2.emisornuminterior = _dbRow_sucursal.numinterior
            _D_actualizar2.emisorcolonia = _dbRow_sucursal.colonia
            _D_actualizar2.emisorlocalidad = _dbRow_sucursal.ciudad
            _D_actualizar2.emisormunicipio = db01.tpais_estado_municipios(
                _dbRow_sucursal.pais_estado_municipio_id).descripcion \
                if _dbRow_sucursal.pais_estado_municipio_id else ""
            _D_actualizar2.emisorestado = db01.tpais_estados(_dbRow_sucursal.pais_estado_id).c_estado \
                if _dbRow_sucursal.pais_estado_id else ""
            _D_actualizar2.emisorpais = db01.tpaises(_dbRow_sucursal.pais_id).c_pais \
                if _dbRow_sucursal.pais_id else ""
            _D_actualizar2.emisorcodigopostal = _dbRow_sucursal.codigopostal

            _dbRow_cliente = dbc01.tempresa_clientes(_dbRow.receptorcliente_id)

            _dbRows_cliente_domicilio = _dbRow_cliente.tempresa_cliente_domicilios.select(
                orderby = dbc01.tempresa_cliente_domicilios.orden,
                limitby = [0, 1]
                ) if _dbRow_cliente else None

            _D_actualizar.credito_plazo = _dbRow_cliente.credito_plazo if _dbRow_cliente else 0
            _D_actualizar.receptorrfc = _dbRow_cliente.rfc if _dbRow_cliente else ""
            _D_actualizar.receptornombrerazonsocial = _dbRow_cliente.razonsocial if _dbRow_cliente else ""
            _D_actualizar.facturacomplementos = _dbRow_cliente.facturacomplementos \
                if _dbRow_cliente else TEMPRESA_CLIENTES.E_FACTURARCOMPLEMENTOS.MISMAFACTURA

            if _dbRows_cliente_domicilio:
                _dbRow_cliente_domicilio = _dbRows_cliente_domicilio.first()

                _D_actualizar2.receptoremails = _dbRow_cliente_domicilio.receptoremails
                _D_actualizar2.receptortelefonos = _dbRow_cliente_domicilio.receptortelefonos
                _D_actualizar2.receptorcalle = _dbRow_cliente_domicilio.calle
                _D_actualizar2.receptorreferencia = _dbRow_cliente_domicilio.referencia
                _D_actualizar2.receptornumexterior = _dbRow_cliente_domicilio.numexterior
                _D_actualizar2.receptornuminterior = _dbRow_cliente_domicilio.numinterior
                _D_actualizar2.receptorcolonia = _dbRow_cliente_domicilio.colonia
                _D_actualizar2.receptorlocalidad = _dbRow_cliente_domicilio.ciudad
                _D_actualizar2.receptormunicipio = db01.tpais_estado_municipios(
                    _dbRow_cliente_domicilio.pais_estado_municipio_id).c_municipio \
                    if _dbRow_cliente_domicilio.pais_estado_municipio_id else ""
                _D_actualizar2.receptorestado = db01.tpais_estados(_dbRow_cliente_domicilio.pais_estado_id).c_estado \
                    if _dbRow_cliente_domicilio.pais_estado_id else ""
                _D_actualizar2.receptorpais = db01.tpaises(_dbRow_cliente_domicilio.pais_id).c_pais \
                    if _dbRow_cliente_domicilio.pais_id else ""
                _D_actualizar2.receptorcodigopostal = _dbRow_cliente_domicilio.codigopostal

            else:
                pass

            _D_actualizar.fechatimbrado_str = _dbRow.fechatimbrado.strftime('%Y-%m-%dT%H:%M:%S') \
                if _dbRow.uuid else ""

            if b_actualizarRegistro:
                for _s_nombreCampo in _D_actualizar:
                    if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                        _dbRow.update_record(**_D_actualizar)
                        break
                    else:
                        pass

                for _s_nombreCampo2 in _D_actualizar2:
                    if _D_actualizar2[_s_nombreCampo2] != _dbRow_cfdi2[_s_nombreCampo2]:
                        _dbRow_cfdi2.update_record(**_D_actualizar2)
                        break
                    else:
                        pass
            else:
                # Si no se desea actualizar el registro, se manda el diccionario con los valores calculados
                _D_return.D_actualizar = _D_actualizar

            return _D_return

        @classmethod
        def CALCULAR_CAMPOS(cls, x_cfdi, b_actualizarRegistro = True):
            """ Recalcula campos que tienen impacto cuando otros registros de otras tablas cambian
            Esta operación así mismo detecta y agrega las inconsistencias

            @param b_actualizarRegistro:
            @type b_actualizarRegistro:
            @param x_cfdi:
            @type x_cfdi:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage(
                subtotal = 0,
                descuento = 0,
                totalimpuestostrasladados = 0,
                totalimpuestosretenidos = 0,
                total = 0,  # Subtotal (suma de conceptos
                totalpago = 0,  # suma de pagos
                errores = "",  # Se resetean los errores
                )

            _f_sumarelacionados = 0
            _n_prodderv_casco1a1 = 0

            _dbRows_conceptos = _dbRow.tcfdi_conceptos.select()
            for _dbRow_concepto in _dbRows_conceptos:
                # TCFDI_CONCEPTOS.CALCULAR(_dbRow_concepto) Aun no se ve la necesidad de recalcular conceptos.
                # Si al recalcular el concepto, recalcula el CFDI, puede provocar recursividad
                _D_actualizar.subtotal += _dbRow_concepto.importe
                _D_actualizar.descuento += _dbRow_concepto.descuento or 0
                _dbRows_impTras = _dbRow_concepto.tcfdi_concepto_impuestostrasladados.select()
                for _dbRow_impTras in _dbRows_impTras:
                    _D_actualizar.totalimpuestostrasladados += _dbRow_impTras.importe
                _dbRows_impRet = _dbRow_concepto.tcfdi_concepto_impuestosretenidos.select()
                for _dbRow_impRet in _dbRows_impRet:
                    _D_actualizar.totalimpuestosretenidos += _dbRow_impRet.importe

            _D_actualizar.total = _D_actualizar.subtotal - _D_actualizar.descuento \
                                  + _D_actualizar.totalimpuestostrasladados \
                                  + _D_actualizar.totalimpuestosretenidos

            if _dbRow.tipocomprobante_id == TTIPOSCOMPROBANTES.PAGO:
                _dbRows_compPagos = _dbRow.tcfdi_complementopagos.select()
                for _dbRow_compPago in _dbRows_compPagos:
                    if _dbRow_compPago.sumarelacionados == 0:
                        # Si no existe el cálculo de sumarelacionados, se procede a calcularlo
                        _D_results = TCFDI_COMPLEMENTOPAGOS.PROC.CALCULAR_CAMPOS(
                            x_cfdi_comppago = _dbRow_compPago,
                            b_recalcularCFDI = False
                            )
                    else:
                        pass
                    _D_actualizar.totalpago += _dbRow_compPago.monto * (_dbRow_compPago.tipocambio or 1)
                    _f_sumarelacionados += (_dbRow_compPago.sumarelacionados or 0) * (_dbRow_compPago.tipocambio or 1)

                if _f_sumarelacionados != _D_actualizar.totalpago:
                    _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                    _D_return.s_msgError += "Suma de relacionados [%.2f] y suma de pagos [%.2f] no coincide. " \
                                            % (_f_sumarelacionados, _D_actualizar.totalpago)
                else:
                    pass

            elif _dbRow.tipocomprobante_id == TTIPOSCOMPROBANTES.EGRESO:
                _dbRows_rel = _dbRow.tcfdi_relacionados.select()
                _L_cfdiRelacionados_ingresos = []
                for _dbRow_rel in _dbRows_rel:
                    _f_sumarelacionados += _dbRow_rel.importe
                    _L_cfdiRelacionados_ingresos.append(_dbRow_rel.cfdirelacionado_id)

                if _f_sumarelacionados != _D_actualizar.total:
                    _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                    _D_return.s_msgError += "Suma de relacionados [%.2f] y suma de conceptos [%.2f] no coincide. " \
                                            % (_f_sumarelacionados, _D_actualizar.total)
                else:
                    pass

                if _dbRow.tiporelacion_id == TTIPOSRELACIONES.NOTACREDITO:

                    # TODOMejora Pudiera validarse el máximo descuento permitido si se definen politicas para encontrarlo
                    # _dbField_sumaTotalIngresos = dbc01.tcfdis.total.sum()
                    # _dbRows_relIngresos = dbc01(
                    #     dbc01.tcfdis.id.belongs(_L_cfdiRelacionados_ingresos)
                    #     ).select(
                    #         _dbField_sumaTotalIngresos
                    #         )
                    #
                    # _dbField_sumaTotalEgresos = dbc01.tcfdi_relacionados.importe.sum()
                    # _dbRows_relEgresos = dbc01(
                    #     dbc01.tcfdi_relacionados.cfdirelacionado_id.belongs(_L_cfdiRelacionados_ingresos)
                    #     ).select(
                    #         _dbField_sumaTotalEgresos
                    #         )
                    #
                    # if not _dbRows_relIngresos:
                    #     _D_return.agrega_error("No se puede determinar el descuento máximo permitido, "
                    #         + "si no existen documentos referenciados. ")
                    #
                    # else:
                    #     _f_sumEgresos = _dbRows_relEgresos.first()[_dbField_sumaTotalEgresos] \
                    #         if _dbRows_relEgresos else 0
                    #     _f_sumIngresos = _dbRows_relIngresos.first()[_dbField_sumaTotalIngresos]
                    #     _f_importeTotalIngreso = _f_sumIngresos - _f_sumEgresos
                    pass

                else:
                    pass

            else:
                pass

            _D_actualizar.errores = _D_return.s_msgError

            if b_actualizarRegistro:
                for _s_nombreCampo in _D_actualizar:
                    if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                        _dbRow.update_record(**_D_actualizar)

                        _D_results = TCFDIS.EDOCTA(
                            n_empresa_id = _dbRow.empresa_id,
                            n_cliente_id = _dbRow.receptorcliente_id,
                            E_movto_generado_por = TCFDI_MOVIMIENTOS.E_GENERADO_POR.DOCUMENTO,
                            ).procesar_cfdi(_dbRow)
                        _D_return.combinar(_D_results)

                        break
                    else:
                        pass
            else:
                # Si no se desea actualizar el registro, se manda el diccionario con los valores calculados
                _D_return.D_actualizar = _D_actualizar

            return _D_return

        @classmethod
        def IMPORTAR_CFDI(cls, s_xml, **D_params):
            """ Importa CFDI relacionado a la empresa.

            @return:
            @rtype:
            """
            _ = D_params
            _D_return = FUNC_RETURN()
            if D_stvFwkCfg.dbRow_empresa:
                _stv_importar_cfdi = STV_CFDI_INTEGRACION(
                    s_rfc = D_stvFwkCfg.dbRow_empresa.rfc,
                    s_empresa_id = D_stvFwkCfg.dbRow_empresa.id,
                    s_cuenta_id = D_stvFwkCfg.cfg_cuenta_id,
                    s_procesoIniciador = "importar_cfdi"
                    )
                _D_results = _stv_importar_cfdi.importar(
                    xml_cfdi = s_xml,
                    E_tipoImportacion = STV_CFDI_INTEGRACION.E_TIPOIMPORTACION.CFDI_EMITIDO,
                    E_generadoPor = TCFDIS.E_GENERADO_POR.IMPORTADO_SISTEMA
                    )
                _D_return.combinar(_D_results)

            else:
                _D_return.agrega_error("Empresa no identificada para importación")

            return _D_return

        pass  # PROC

    class PROC_DETALLE:

        dbTabla = None  # Se escribe con la inicialización

        def __init__(self, **D_defaults):
            """ Inicializa objecto de tipo PROC para control del registro actual

            @param D_defaults:
            @type D_defaults:
            """
            self._D_defaults = Storage(D_defaults)
            self._L_requeridos = ['cfdi_id']
            return

        def puede_crear(self, x_cfdi, D_requeridosMaestro = None, **D_defaults):
            """ Determina si puede o no crear un CFDI de traslado.

            Actualmente no hay condición requerida.

            """
            _D_return = FUNC_RETURN()

            self._D_defaults.update(D_defaults or {})

            for _s_requerido in self._L_requeridos:
                if _s_requerido not in self._D_defaults:
                    _D_return.agrega_error("Campo %s es requerido para generar CFDI" % _s_requerido)

                else:
                    pass

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi, dbc01.tcfdis)
            _dbRow_cfdi = _D_results.dbRow

            _D_results = TCFDIS.PROC.PUEDE_EDITAR(_dbRow_cfdi)
            _D_return.combinar(_D_results)

            if D_requeridosMaestro:
                for _s_nombreCampo in D_requeridosMaestro:
                    _D_info = D_requeridosMaestro[_s_nombreCampo]

                    if _dbRow_cfdi[_s_nombreCampo] in _D_info.L_valores:
                        pass
                    elif (len(_D_info.L_valores) == 0) and _dbRow_cfdi[_s_nombreCampo]:
                        # Un valor de lista vacío [], busca por que tenga un valor el campo
                        pass
                    else:
                        _D_return.agrega_error(_D_info.s_msgError)

            else:
                pass

            return _D_return

        @classmethod
        def PUEDE_EDITAR(cls, x_cfdi_detalle):
            """ Define si es posible editar el CFDI de traslado.
            @param x_cfdi_detalle:

            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_detalle, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_results = TCFDIS.PROC.PUEDE_EDITAR(_dbRow.cfdi_id)
            _D_return.combinar(_D_results)

            return _D_return

        @classmethod
        def PUEDE_ELIMINAR(
                cls,
                x_cfdi_detalle
                ):
            """ Determina si puede o no eliminar un registro

            Misma condición que puede_editar, a lo mejor pudieran verse condiciones distintas

            """

            return cls.PUEDE_EDITAR(x_cfdi_detalle)

        @classmethod
        def CONFIGURACAMPOS_DESDE_dbRow(cls, dbRow_base):
            # cls.dbTabla.cfdi_id.default = dbRow_base.cfdi_id
            # cls.dbTabla.campo1.default = dbRow_base.campo1
            # cls.dbTabla.campo2.default = dbRow_base.campo2
            return

        def configuracampos_nuevo(
                self,
                s_cfdi_detalle_id,
                b_llamadaDesdeEditar = False,
                **D_defaults
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param b_llamadaDesdeEditar: Indica si la llamada a esta función es desde otra capa,
             desactivando validación de puede_crear
            @type b_llamadaDesdeEditar:
            @param s_cfdi_detalle_id:
            @type s_cfdi_detalle_id:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            self._D_defaults.update(D_defaults or {})
            for _s_fieldName in self.dbTabla.fields:
                self.dbTabla[_s_fieldName].writable = False
            for _s_fieldName in self._D_defaults:
                self.dbTabla[_s_fieldName].default = self._D_defaults[_s_fieldName]

            if not b_llamadaDesdeEditar:
                _D_results = self.puede_crear(x_cfdi = self._D_defaults.cfdi_id)
            else:
                _D_results = _D_return

            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.combinar(_D_results)

            elif s_cfdi_detalle_id:
                _dbRow_base = self.dbTabla(s_cfdi_detalle_id)

                self.CONFIGURACAMPOS_DESDE_dbRow(_dbRow_base)

            else:
                # Si se requieren defaults en caso de no tener algún otro definido, ponerlo aqui
                pass

            self.dbTabla.cfdi_id.writable = False

            # Campos que todos los movimientos pueden editar
            self.dbTabla.notas.writable = False

            # Se definen los valores por default obligados
            self.dbTabla.cfdi_id.default = self._D_defaults.cfdi_id

            return _D_return

        def configuracampos_edicion(self, x_cfdi_detalle):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param x_cfdi_detalle:
            @type x_cfdi_detalle:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_detalle, self.dbTabla)
            _dbRow = _D_results.dbRow

            # Se llenan los default obligados
            for _s_nombreCampo in self._L_requeridos:
                self._D_defaults[_s_nombreCampo] = _D_results.dbRow[_s_nombreCampo]

            _D_results = self.PUEDE_EDITAR(_dbRow)

            if _D_results.E_return == CLASS_e_RETURN.OK:
                # Si puede editar regresa un ok y el movimiento esta abierto

                # Se manda vacío el argumento para no agregar defaults a los campos
                _D_results = self.configuracampos_nuevo(
                    None,
                    b_llamadaDesdeEditar = True,
                    **self._D_defaults
                    )
                _D_return.combinar(_D_results)

            else:
                # De lo contrario en cualquier otro caso, no puede editar nada de esta tabla
                for _s_fieldName in self.dbTabla.fields:
                    self.dbTabla[_s_fieldName].writable = False
                _D_return.agrega(CLASS_e_RETURN.OK_WARNING, _D_results.s_msgError, "No puede editar")

            return _D_return

        def al_validar(self, O_form, dbRow = None, D_camposAChecar = None):
            """ Validar la información para poder grabar cfdi

            Las validaciones en esta función son genéricas, se espera que existan validaciones
             específicas por tipo de CFDI

            @param D_camposAChecar:
            @type D_camposAChecar:
            @param dbRow:
            @type dbRow:
            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            dbRow = dbRow if dbRow else (self.dbTabla(O_form.record_id) if O_form.record_id else None)
            D_camposAChecar = D_camposAChecar if D_camposAChecar \
                else STV_FWK_LIB.OBTENER_CAMPOSACHECAR_PARA_VALIDAR(self.dbTabla, dbRow)

            if not O_form.record_id:
                # Se esta creando el registro

                _D_results = self.puede_crear(
                    x_cfdi = D_camposAChecar.cfdi_id,
                    **D_camposAChecar
                    )
                if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                    O_form.errors.id = _D_results.s_msgError
                else:
                    pass

            else:
                # Se esta modificando el registro, la llamada a configuraCampos_edicion ya definió con writable
                #  que puede modificarse y que no. Por lo que el código para verificar si puede o no editar
                #  no es requerido en este caso
                pass

            # Validaciones

            # if not _D_camposAChecar.campo1:
            #     O_form.errors.campo1 = "Campo1 no puede estar vacío"
            # else:
            #     pass

            return O_form

        @classmethod
        def AL_ACEPTAR(cls, O_form, dbRow = None):
            """ Al aceptar el registro deben hacerse los cálculos

            Se hacen todos los cálculos y aceptación de los cambios en el producto

            @param dbRow:
            @type dbRow:
            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()
            dbRow = dbRow if dbRow else cls.dbTabla(O_form.record_id or O_form.vars.id)

            if not O_form.record_id:
                # Si se inserta
                pass

            else:  # Si se edita
                pass

            _D_results = cls.MAPEAR_CAMPOS(dbRow)
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.combinar(_D_results)

            else:
                _D_results = cls.CALCULAR_CAMPOS(dbRow)
                _D_return.combinar(_D_results)

            generaMensajeFlash(cls.dbTabla, _D_return)
            return O_form

        @classmethod
        def ANTES_ELIMINAR(cls, L_errorsAction, dbRow):
            """ Antes de eliminar un movimiento de inventario de este tipo

            @param dbRow:
            @type dbRow:
            @param L_errorsAction:
            @type L_errorsAction:
            @return: errores
            @rtype: lista de diccionarios {'msg':'...'}
            """
            _D_results = cls.PUEDE_ELIMINAR(dbRow)
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                L_errorsAction.append({'msg': _D_results.s_msgError})
            else:
                cls._dbRow_aEliminar = Storage(dbRow.as_dict())

            return L_errorsAction

        @classmethod
        def DESPUES_ELIMINAR(cls, L_errorsAction):
            """ Después de eliminar un registro

            @param L_errorsAction:
            @type L_errorsAction:
            @return: errores
            @rtype: lista de diccionarios {'msg':'...'}
            """
            _D_return = FUNC_RETURN()

            if cls._dbRow_aEliminar:
                _D_results = TCFDIS.PROC.CALCULAR_CAMPOS(
                    x_cfdi = cls._dbRow_aEliminar.cfdi_id
                    )
                _D_return.combinar(_D_results)
            else:
                _D_return.E_return = CLASS_e_RETURN.NOK_SECUENCIA
                _D_return.s_msgError += "Error en secuencia de llamado de funciones para borrado. "

            generaMensajeFlash(cls.dbTabla, _D_return)
            return L_errorsAction

        @classmethod
        def MAPEAR_CAMPOS(cls, x_cfdi_detalle, b_actualizarRegistro = True):
            """ Remapea campos solamente cuando se graba el registro

            @param b_actualizarRegistro:
            @type b_actualizarRegistro:
            @param x_cfdi_detalle:
            @type x_cfdi_detalle:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_detalle, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage()

            if b_actualizarRegistro:
                for _s_nombreCampo in _D_actualizar:
                    if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                        _dbRow.update_record(**_D_actualizar)
                        # Mapear campos no implica recalculo de maestro
                        break
                    else:
                        pass
            else:
                # Si no se desea actualizar el registro, se manda el diccionario con los valores calculados
                _D_return.D_actualizar = _D_actualizar

            return _D_return

        @classmethod
        def CALCULAR_CAMPOS(cls, x_cfdi_detalle, b_actualizarRegistro = True, b_recalcularCFDI = True):
            """ Recalcula campos, haciendo la consulta de la carta porte a partir del CFDI

            @param b_recalcularCFDI:
            @type b_recalcularCFDI:
            @param b_actualizarRegistro:
            @type b_actualizarRegistro:
            @param x_cfdi_detalle:
            @type x_cfdi_detalle:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_detalle, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage()

            if b_actualizarRegistro:
                for _s_nombreCampo in _D_actualizar:
                    if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                        _dbRow.update_record(**_D_actualizar)

                        break
                    else:
                        pass
            else:
                # Si no se desea actualizar el registro, se manda el diccionario con los valores calculados
                _D_return.D_actualizar = _D_actualizar

            if b_recalcularCFDI:
                # Si hay actualizaciones, debe actualizarse el CFDI, no los conceptos
                _D_results = TCFDIS.PROC.CALCULAR_CAMPOS(x_cfdi = _dbRow.cfdi_id)
            else:
                pass

            return _D_return

        pass  # PROC_DETALLE

    @staticmethod
    def asociar_serie(s_empresa_id, s_serie):

        _s_cache_id = 'cfdi_asociar_empresa' + str(s_empresa_id) + 'serie' + str(s_serie)
        _D_returnsData = cache.ram(
            _s_cache_id,
            lambda: stv_asociar_tabla(
                dbc01.tempresa_series,
                'serie',
                s_serie,
                qry_maestro = (dbc01.tempresa_series.empresa_id == s_empresa_id),
                ),
            time_expire = 86400
            )
        if _D_returnsData.E_return != stvfwk2_e_RETURN.OK:
            cache.ram(_s_cache_id, None)
        else:
            pass
        return _D_returnsData

    @staticmethod
    def asociar_cajachica(s_empresa_id, s_serie_id, s_codigopostal):

        # Se asocia la caja chica con serie de ingreso
        _s_cache_id = 'cfdi_asociar_cajachica_empresa' + str(s_empresa_id) + 'serie' + str(s_serie_id)
        _D_returnsData = cache.ram(
            _s_cache_id,
            lambda: stv_asociar_tabla(
                dbc01.tempresa_plaza_sucursal_cajaschicas,
                'empresa_serie_ingreso_id',
                s_serie_id
                ),
            time_expire = 86400
            )

        if _D_returnsData.E_return != stvfwk2_e_RETURN.OK:
            cache.ram(_s_cache_id, None)

            # Se asocia la caja chica con serie de NC devolucion
            _D_returnsData = cache.ram(
                _s_cache_id,
                lambda: stv_asociar_tabla(
                    dbc01.tempresa_plaza_sucursal_cajaschicas,
                    'empresa_serie_pago_id',
                    s_serie_id
                    ),
                time_expire = 86400
                )
        else:
            pass

        if _D_returnsData.E_return != stvfwk2_e_RETURN.OK:
            cache.ram(_s_cache_id, None)

            # Se asocia la caja chica con serie de NC descuento
            _D_returnsData = cache.ram(
                _s_cache_id,
                lambda: stv_asociar_tabla(
                    dbc01.tempresa_plaza_sucursal_cajaschicas,
                    'empresa_serie_egreso_id',
                    s_serie_id
                    ),
                time_expire = 86400
                )
        else:
            pass

        if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
            # Correcto, serie encontrada
            pass
        else:
            cache.ram(_s_cache_id, None)

            # Se asocia la caja chica por código postal
            _s_cache_id = 'cfdi_asociar_cajachica_empresa' + str(s_empresa_id) + 'lugarexpedicion' + str(s_codigopostal)
            _D_returnsData = cache.ram(
                _s_cache_id,
                lambda: stv_asociar_tabla(
                    dbc01.tempresa_plaza_sucursales,
                    'codigopostal',
                    str(s_codigopostal),
                    qry_maestro = (
                            (dbc01.tempresa_plazas.empresa_id == s_empresa_id)
                            & (dbc01.tempresa_plaza_sucursales.empresa_plaza_id == dbc01.tempresa_plazas.id)
                            & (
                                dbc01.tempresa_plaza_sucursal_cajaschicas.empresa_plaza_sucursal_id
                                == dbc01.tempresa_plaza_sucursales.id
                                )
                            ),
                        ),
                time_expire = 86400
                )

            if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                # Correcto, serie encontrada
                pass
            else:
                cache.ram(_s_cache_id, None)
        return _D_returnsData

    @staticmethod
    def asociar_uuid(s_cfdi_id):
        """ Se busca y asocian ligas rotas en doctos relacionados y pagos :: doctos relacionados


        """

        _D_return = FUNC_RETURN(
            L_msgError = [],
            E_return = CLASS_e_RETURN.NOK_ERROR,
            n_updatedRows = 0
            )

        # Ejemplo puede usar
        # tcfdi_relacionados.id = 8419 de tcfdis.id = 47696
        # tcfdis.id = 47762

        # Se buscan primero en la tabla de tcfdi_relacionados

        _dbTableCFDIsBuscar = dbc01.tcfdis.with_alias('cfdisbuscar')
        _dbRows_cfdiRelacionados = dbc01(
            (dbc01.tcfdis.id == s_cfdi_id)
            & (dbc01.tcfdi_relacionados.uuid == dbc01.tcfdis.uuid)
            & (dbc01.tcfdi_relacionados.cfdirelacionado_id == None)
            & (_dbTableCFDIsBuscar.id == dbc01.tcfdi_relacionados.cfdi_id)
            & (_dbTableCFDIsBuscar.empresa_id == dbc01.tcfdis.empresa_id)
            ).select(
            dbc01.tcfdi_relacionados.ALL,
            _dbTableCFDIsBuscar.id,
            )

        if len(_dbRows_cfdiRelacionados) == 0:
            # CFDI asociado con el UUID no se encontró
            _D_return.L_msgError += [
                "No se encontrarón CFDIs con problemas en cfdi relacionados a este CFDI %s" % s_cfdi_id]
        else:
            # Si tiene solamente un registro, hay que actualizarlo
            for _dbRow in _dbRows_cfdiRelacionados:
                _D_return.n_updatedRows += dbc01(dbc01.tcfdi_relacionados.id == _dbRow.tcfdi_relacionados.id).update(
                    cfdirelacionado_id = s_cfdi_id
                    )

        _dbRows_cfdiPagosRelacionados = dbc01(
            (dbc01.tcfdis.id == s_cfdi_id)
            & (dbc01.tcfdi_complementopago_doctosrelacionados.iddocumento == dbc01.tcfdis.uuid)
            & (dbc01.tcfdi_complementopago_doctosrelacionados.cfdirelacionado_id == None)
            & (
                dbc01.tcfdi_complementopagos.id
                == dbc01.tcfdi_complementopago_doctosrelacionados.cfdi_complementopago_id
                )
            & (_dbTableCFDIsBuscar.id == dbc01.tcfdi_complementopagos.cfdi_id)
            & (_dbTableCFDIsBuscar.empresa_id == dbc01.tcfdis.empresa_id)
            ).select(
            dbc01.tcfdi_complementopago_doctosrelacionados.ALL,
            _dbTableCFDIsBuscar.id,
            )

        if len(_dbRows_cfdiPagosRelacionados) == 0:
            # CFDI asociado con el UUID no se encontró
            _D_return.L_msgError += [
                "No se encontrarón CFDIs de PAGO con problemas en cfdi relacionados a este CFDI %s" % s_cfdi_id]
        else:
            # Si tiene solamente un registro, hay que actualizarlo
            for _dbRow in _dbRows_cfdiPagosRelacionados:
                _D_return.n_updatedRows += dbc01(
                    dbc01.tcfdi_complementopago_doctosrelacionados.id
                    == _dbRow.tcfdi_complementopago_doctosrelacionados.id
                    ).update(cfdirelacionado_id = s_cfdi_id)

        _D_return.E_return = CLASS_e_RETURN.OK

        return _D_return

    class BUSQUEDA_AVANZADA:

        @staticmethod
        def CONFIGURA(
                O_cat,
                s_empresa_id = None,
                **D_valoresDefault
                ):
            """ Agrega el componente y lo configura para la búsqueda avanzada

            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _dbTabla = dbc01.tcfdis

            D_valoresDefault = Storage(D_valoresDefault)

            # Se configura el uso del campo categorias para poder filtrar por varias clasificaciones.
            _dbField_fecha = _dbTabla.fecha.clone()
            _dbField_fecha.default = None
            _dbField_fecha.widget = stv_widget_inputDateTimeRange

            _dbField_tipocomprobante_id = _dbTabla.tipocomprobante_id.clone()
            _dbField_tipocomprobante_id.adv_default = (
                    D_valoresDefault.tipocomprobante_id or _dbField_tipocomprobante_id.default
                    )
            _dbField_tipocomprobante_id.widget = lambda field, value: stv_widget_db_chosen(
                field, value, 20,
                )
            _dbField_tipocomprobante_id.length = 400

            # Variable que contendrá los campos a editar en la búsqueda avanzada.
            _L_dbFields = [
                _dbField_fecha,
                _dbField_tipocomprobante_id,
                _dbTabla.empresa_serie_id.clone(),  # TODOMejora si solo quiere ingresos, filtrar serie a solo ingresos
                _dbTabla.receptorrfc.clone(),
                _dbTabla.receptornombrerazonsocial.clone(),
                _dbTabla.folio.clone(),
                ]

            if not s_empresa_id:
                # Si no esta definida la empresa, se agrega el seleccionar empresa.
                _dbField_empresa_id = _dbTabla.empresa_id.clone()
                _dbField_empresa_id.notnull = False
                _L_dbFields.append(_dbField_empresa_id)
            else:
                pass

            # Se configura la edición de los campos.
            for _dbField in _L_dbFields:
                _dbField.writable = True
                _dbField.readable = True
                if _dbField.name in request.vars:
                    pass
                elif getattr(_dbField, 'adv_default', None) is not None:
                    request.vars[_dbField.name] = _dbField.adv_default
                else:
                    pass

            O_cat.cfgRow_advSearch(
                L_dbFields_formFactory_advSearch = _L_dbFields,
                dbRow_formFactory_advSearch = None,
                D_hiddenFields_formFactory_advSearch = {},
                s_style = 'bootstrap',
                L_buttons = []
                )

            return _D_return

        @staticmethod
        def GENERA_PREFILTRO(
                D_args,
                ):
            """ Función usada para generar un prefiltro en caso de ser necesario, ya que los campos
            no estan directamente relacionado a la tabla

            @return:
            @rtype:
            """
            _ = D_args
            _D_return = FUNC_RETURN(
                qry = True
                )
            _dbTabla = dbc01.tcfdis

            return _D_return

        pass  # BUSQUEDA_AVANZADA

    @staticmethod
    def CONFIGURAR_INTEGRIDAD_REFERENCIAL():

        return

    class INGRESO(PROC, object):
        """ Clase para manejo de CFDI de ingreso

        """

        def __init__(self, **D_defaults):
            """ Inicializa objecto de tipo PROC para control del registro actual

            @param D_defaults:
            @type D_defaults:
            """
            super(TCFDIS.INGRESO, self).__init__(**(D_defaults or Storage()))  # For Python 2.7
            # super().__init__(D_defaults or Storage())  # For Python 3
            self._L_requeridos += ['receptorcliente_id']
            return

        def configuracampos_nuevo(
                self,
                s_cfdi_id,
                b_llamadaDesdeEditar = False,
                **D_defaults
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar.

            La lógica es involucrar chequeos locales e involucrar la función genérica para configurar pines;
            luego utilizar lógica local para los ajustes finos a los pines.

            @param b_llamadaDesdeEditar: Indica si la llamada a esta función es desde otra capa,
             desactivando validación de puede_crear
            @type b_llamadaDesdeEditar:
            @param s_cfdi_id:
            @type s_cfdi_id:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            if not s_cfdi_id:
                # Se usan como default los ultimos valores usados
                _dbRows_cfdi_ultimo = dbc01(
                    (dbc01.tcfdis.empresa_id == self._D_defaults.empresa_id)
                    & (dbc01.tcfdis.receptorcliente_id == self._D_defaults.receptorcliente_id)
                    & (dbc01.tcfdis.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO)
                    & (dbc01.tcfdis.total != 0)
                    ).select(
                    dbc01.tcfdis.id,
                    orderby = ~dbc01.tcfdis.id,
                    limitby = (0, 1)
                    )
                if _dbRows_cfdi_ultimo:
                    s_cfdi_id = _dbRows_cfdi_ultimo.first().id
                else:
                    pass
            else:
                pass

            _D_results = super(TCFDIS.INGRESO, self).configuracampos_nuevo(
                s_cfdi_id = s_cfdi_id,
                b_llamadaDesdeEditar = b_llamadaDesdeEditar,
                **self._D_defaults
                )
            _D_return.combinar(_D_results)

            # Se configuran los campos especificos para el caso de esta clase
            self.dbTabla.empresa_id.writable = False
            self.dbTabla.empresa_plaza_sucursal_cajachica_id.writable = True
            self.dbTabla.fecha.writable = True
            self.dbTabla.formapago_id.writable = True
            self.dbTabla.condicionesdepago.writable = True
            self.dbTabla.monedacontpaqi_id.writable = True
            self.dbTabla.tipocambio.writable = True
            self.dbTabla.tipocomprobante_id.writable = False
            self.dbTabla.metodopago_id.writable = True
            self.dbTabla.lugarexpedicion_id.writable = False
            self.dbTabla.tiporelacion_id.writable = True
            self.dbTabla.tiporelacion_id.requires = IS_NULL_OR(
                IS_IN_DB(
                    db01(db01.ttiposrelaciones.aplicaingreso == True), 'ttiposrelaciones.id',
                    db01.ttiposrelaciones._format
                    )
                )
            self.dbTabla.tiporelacion_id.comment = "Configurar únicamente en el caso de agregar CFDIs relacionados"
            # TODO dbc01.tcfdis2
            # self.dbTabla.ordencompra.writable = False
            # self.dbTabla.foliointernopedido.writable = False
            # self.dbTabla.agentevendedor.writable = False
            # self.dbTabla.montobaseprodiva0.writable = False
            # self.dbTabla.montobaseprodiva.writable = False
            self.dbTabla.emisorproveedor_id.writable = False
            self.dbTabla.receptorusocfdi_id.writable = True
            self.dbTabla.receptorcliente_id.writable = False  # Se pone por default el cliente actual
            self.dbTabla.uuid.writable = False
            self.dbTabla.fechatimbrado.writable = False
            self.dbTabla.rol.writable = False
            self.dbTabla.etapa.writable = False
            self.dbTabla.etapa_cancelado.writable = False
            self.dbTabla.estado.writable = False
            self.dbTabla.generado_por.writable = False
            self.dbTabla.errores.writable = False
            self.dbTabla.sat_status.writable = False
            self.dbTabla.sat_fecha_cancelacion.writable = False
            self.dbTabla.sat_status_cancelacion.writable = False
            self.dbTabla.sat_status_proceso_cancelacion.writable = False
            self.dbTabla.pac_rfc.writable = False
            self.dbTabla.pac_razon_social.writable = False
            self.dbTabla.pac_nombre_comercial.writable = False
            self.dbTabla.cancelado_folio.writable = False
            self.dbTabla.cfdi_complemento_id.writable = False
            self.dbTabla.emp_pla_suc_almacen_id.writable = True

            # Campos que no deben ser editados, solo de forma autocalculada
            self.dbTabla.moneda_id.writable = False

            # Se configuran en configuracampos_nuevo del maestro

            # Se definen los valores por default sobreescribiendo los anteriores siendo especificos para esta clase
            self.dbTabla.empresa_id.default = self._D_defaults.empresa_id
            self.dbTabla.tipocomprobante_id.default = TTIPOSCOMPROBANTES.INGRESO
            self.dbTabla.receptorcliente_id.default = self._D_defaults.receptorcliente_id
            self.dbTabla.rol.default = TCFDIS.E_ROL.EMISOR
            self.dbTabla.generado_por.default = TCFDIS.E_GENERADO_POR.MANUAL

            _dbRow_cliente = dbc01.tempresa_clientes(self._D_defaults.receptorcliente_id)
            self.dbTabla.receptorusocfdi_id.default = _dbRow_cliente.receptorusocfdi_id \
                                                      or TUSOSCFDI.ADQUISICION_MERCANCIA

            return _D_return

        def configuracampos_edicion(
                self,
                x_cfdi,
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            Esta función NO debe llamar a la función genérica para configurar campos, ya que las condiciones de
            configuracionCampos_nuevo son muy específicas a esta clase. Si existen ajustes finos en la función
            genérica, estos deben copiarse a esta función específica de la clase

            @param x_cfdi: es el registro del movimiento o su id
            @type x_cfdi:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi, self.dbTabla)
            _dbRow = _D_results.dbRow

            _D_results = super(TCFDIS.INGRESO, self).configuracampos_edicion(_dbRow)
            _D_return.combinar(_D_results)

            if _D_results.E_return == CLASS_e_RETURN.OK:
                # Si puede editar regresa un ok y el movimiento esta abierto

                if not _dbRow.empresa_serie_id:
                    self.dbTabla.empresa_plaza_sucursal_cajachica_id.writable = True
                else:
                    self.dbTabla.empresa_plaza_sucursal_cajachica_id.writable = False

            else:
                # De lo contrario en cualquier otro caso, solo se pueden editar las observaciones
                pass

            return _D_return

        def al_validar(
                self,
                O_form,
                **D_params
                ):
            """ Validar la información para poder grabar cfdi.

            La lógica es tomar los campos necesarios para la validación específica, dependiendo de
            si es una inserción o edición. Luego llamar la validación genérica y completar los resultados
            con las validaciones específicas.

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _dbRow = self.dbTabla(O_form.record_id) if O_form.record_id else None
            _D_camposAChecar = STV_FWK_LIB.OBTENER_CAMPOSACHECAR_PARA_VALIDAR(self.dbTabla, _dbRow)

            # Validaciones

            O_form = super(TCFDIS.INGRESO, self).al_validar(O_form, _dbRow, D_camposAChecar = _D_camposAChecar)

            if int(_D_camposAChecar.tipocomprobante_id) != TTIPOSCOMPROBANTES.INGRESO:
                O_form.errors.id = "Esta validación es solamente para CFDIs de tipo pago"
            else:
                pass

            _dbRow_cliente = dbc01.tempresa_clientes(_D_camposAChecar.receptorcliente_id)
            if self.dbTabla.receptorusocfdi_id.writable \
                    and (str(_dbRow_cliente.receptorusocfdi_id) != str(_D_camposAChecar.receptorusocfdi_id)):
                O_form.errors.receptorusocfdi_id = "El receptor uso CFDI es diferente al definido para el cliente"
            else:
                pass

            if not _D_camposAChecar.empresa_plaza_sucursal_cajachica_id:
                O_form.errors.empresa_plaza_sucursal_cajachica_id = "Caja chica es requerida"
            else:
                pass

            return O_form

    class PAGO(PROC, object):
        """ Clase para manejo de CFDI de Pago

        """

        def __init__(self, **D_defaults):
            """ Inicializa objecto de tipo PROC para control del registro actual

            @param D_defaults:
            @type D_defaults:
            """
            super(TCFDIS.PAGO, self).__init__(**(D_defaults or Storage()))  # For Python 2.7
            # super().__init__(D_defaults or Storage())  # For Python 3
            self._L_requeridos += ['receptorcliente_id']
            return

        def configuracampos_nuevo(
                self,
                s_cfdi_id,
                b_llamadaDesdeEditar = False,
                **D_defaults
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar.

            La lógica es involucrar chequeos locales e involucrar la función genérica para configurar pines;
            luego utilizar lógica local para los ajustes finos a los pines.

            @param b_llamadaDesdeEditar: Indica si la llamada a esta función es desde otra capa,
             desactivando validación de puede_crear
            @type b_llamadaDesdeEditar:
            @param s_cfdi_id:
            @type s_cfdi_id:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            if not s_cfdi_id:
                # Se usan como default los ultimos valores usados
                _dbRows_cfdi_ultimo = dbc01(
                    (dbc01.tcfdis.empresa_id == self._D_defaults.empresa_id)
                    & (dbc01.tcfdis.receptorcliente_id == self._D_defaults.receptorcliente_id)
                    & (dbc01.tcfdis.tipocomprobante_id == TTIPOSCOMPROBANTES.PAGO)
                    ).select(
                        dbc01.tcfdis.id,
                        orderby = ~dbc01.tcfdis.id,
                        limitby = (0, 1)
                        )
                if _dbRows_cfdi_ultimo:
                    s_cfdi_id = _dbRows_cfdi_ultimo.first().id
                else:
                    pass
            else:
                pass

            _D_results = super(TCFDIS.PAGO, self).configuracampos_nuevo(
                s_cfdi_id = s_cfdi_id,
                b_llamadaDesdeEditar = b_llamadaDesdeEditar,
                **self._D_defaults
                )
            _D_return.combinar(_D_results)

            # Se configuran los campos especificos para el caso de esta clase
            self.dbTabla.empresa_id.writable = False
            self.dbTabla.empresa_plaza_sucursal_cajachica_id.writable = True
            self.dbTabla.fecha.writable = True
            self.dbTabla.formapago_id.writable = False
            self.dbTabla.condicionesdepago.writable = False
            self.dbTabla.moneda_id.writable = False
            self.dbTabla.monedacontpaqi_id.writable = False
            self.dbTabla.tipocambio.writable = False
            self.dbTabla.tipocomprobante_id.writable = False
            self.dbTabla.metodopago_id.writable = False
            self.dbTabla.lugarexpedicion_id.writable = False
            self.dbTabla.tiporelacion_id.writable = True
            self.dbTabla.tiporelacion_id.requires = IS_NULL_OR(
                IS_IN_DB(
                    db01(db01.ttiposrelaciones.aplicapago == True), 'ttiposrelaciones.id',
                    db01.ttiposrelaciones._format
                    )
                )
            self.dbTabla.tiporelacion_id.comment = "Configurar únicamente en el caso de agregar CFDIs relacionados"
            # TODO dbc01.tcfdis2
            # self.dbTabla.ordencompra.writable = False
            # self.dbTabla.foliointernopedido.writable = False
            # self.dbTabla.agentevendedor.writable = False
            # self.dbTabla.montobaseprodiva0.writable = False
            # self.dbTabla.montobaseprodiva.writable = False
            self.dbTabla.emisorproveedor_id.writable = False
            self.dbTabla.receptorusocfdi_id.writable = True
            self.dbTabla.receptorcliente_id.writable = False  # Se pone por default el cliente actual
            self.dbTabla.uuid.writable = False
            self.dbTabla.fechatimbrado.writable = False
            self.dbTabla.rol.writable = False
            self.dbTabla.etapa.writable = False
            self.dbTabla.etapa_cancelado.writable = False
            self.dbTabla.estado.writable = False
            self.dbTabla.generado_por.writable = False
            self.dbTabla.errores.writable = False
            self.dbTabla.sat_status.writable = False
            self.dbTabla.sat_fecha_cancelacion.writable = False
            self.dbTabla.sat_status_cancelacion.writable = False
            self.dbTabla.sat_status_proceso_cancelacion.writable = False
            self.dbTabla.pac_rfc.writable = False
            self.dbTabla.pac_razon_social.writable = False
            self.dbTabla.pac_nombre_comercial.writable = False
            self.dbTabla.cancelado_folio.writable = False
            self.dbTabla.cfdi_complemento_id.writable = False
            self.dbTabla.emp_pla_suc_almacen_id.writable = False

            # Campos que no deben ser editados, solo de forma autocalculada
            # Se configuran en configuracampos_nuevo del maestro

            # Se definen los valores por default sobreescribiendo los anteriores siendo especificos para esta clase
            self.dbTabla.empresa_id.default = self._D_defaults.empresa_id
            self.dbTabla.formapago_id.default = TFORMASPAGO.POR_DEFINIR
            self.dbTabla.monedacontpaqi_id.default = TEMPRESA_MONEDASCONTPAQI.XXX
            self.dbTabla.tipocambio.default = 0
            self.dbTabla.tipocomprobante_id.default = TTIPOSCOMPROBANTES.PAGO
            self.dbTabla.tiporelacion_id.default = None
            self.dbTabla.metodopago_id.default = None
            self.dbTabla.receptorusocfdi_id.default = TUSOSCFDI.POR_DEFINIR
            self.dbTabla.receptorcliente_id.default = self._D_defaults.receptorcliente_id
            self.dbTabla.rol.default = TCFDIS.E_ROL.EMISOR
            self.dbTabla.generado_por.default = TCFDIS.E_GENERADO_POR.MANUAL

            return _D_return

        def configuracampos_edicion(
                self,
                x_cfdi,
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            Esta función NO debe llamar a la función genérica para configurar campos, ya que las condiciones de
            configuracionCampos_nuevo son muy específicas a esta clase. Si existen ajustes finos en la función
            genérica, estos deben copiarse a esta función específica de la clase

            @param x_cfdi: es el registro del movimiento o su id
            @type x_cfdi:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi, self.dbTabla)
            _dbRow = _D_results.dbRow

            _D_results = super(TCFDIS.PAGO, self).configuracampos_edicion(_dbRow)
            _D_return.combinar(_D_results)

            if _D_results.E_return == CLASS_e_RETURN.OK:
                # Si puede editar regresa un ok y el movimiento esta abierto

                if not _dbRow.empresa_serie_id:
                    self.dbTabla.empresa_plaza_sucursal_cajachica_id.writable = True
                else:
                    self.dbTabla.empresa_plaza_sucursal_cajachica_id.writable = False

            else:
                # De lo contrario en cualquier otro caso, solo se pueden editar las observaciones
                pass

            return _D_return

        def al_validar(
                self,
                O_form,
                **D_params
                ):
            """ Validar la información para poder grabar cfdi.

            La lógica es tomar los campos necesarios para la validación específica, dependiendo de
            si es una inserción o edición. Luego llamar la validación genérica y completar los resultados
            con las validaciones específicas.

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _dbRow = self.dbTabla(O_form.record_id) if O_form.record_id else None
            _D_camposAChecar = STV_FWK_LIB.OBTENER_CAMPOSACHECAR_PARA_VALIDAR(self.dbTabla, _dbRow, O_form)

            # Validaciones

            O_form = super(TCFDIS.PAGO, self).al_validar(O_form, _dbRow, D_camposAChecar = _D_camposAChecar)

            if int(_D_camposAChecar.tipocomprobante_id) != TTIPOSCOMPROBANTES.PAGO:
                O_form.errors.id = "Esta validación es solamente para CFDIs de tipo pago"
            else:
                pass

            if not _D_camposAChecar.empresa_plaza_sucursal_cajachica_id:
                O_form.errors.empresa_plaza_sucursal_cajachica_id = "Caja chica es requerida"
            else:
                pass

            return O_form

        @classmethod
        def AL_ACEPTAR(
                cls,
                O_form,
                **D_params
                ):
            """ Aceptación de una inserción o cambio.

            Se hacen todos los cálculos y aceptación de los cambios en el producto con llamadas
            específicas a funciones de esta clase. NO usar función genérica de aceptación.

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()
            _dbRow = cls.dbTabla(O_form.record_id or O_form.vars.id)

            if not O_form.record_id:
                # Si se inserta

                # Se inserta un concepto de pago
                _D_concepto = Storage(
                    cfdi_id = O_form.vars.id,
                    orden = 100,
                    cantidad = 1,
                    descuento = 0,
                    empresa_prodserv_id = TEMPRESA_PRODSERV.PAGO,
                    valorunitario = 0,
                    valorunitario_capturado = 0
                    )
                _D_concepto.id = dbc01.tcfdi_conceptos.insert(**_D_concepto)
                TCFDI_CONCEPTOS.PAGO.MAPEAR_CAMPOS(_D_concepto.id)

            else:  # Si se edita
                pass

            O_form = super(TCFDIS.PAGO, cls).AL_ACEPTAR(O_form, _dbRow)

            generaMensajeFlash(cls.dbTabla , _D_return)
            return O_form

        @classmethod
        def MAPEAR_CAMPOS(
                cls,
                x_cfdi,
                **D_params
                ):
            """ Remapea campos solamente cuando se graba el registro

            Utilizar la funciónes genéricas a necesidad; antes o despues de lógica especíifica.

            @param x_cfdi:
            @type x_cfdi:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _dbRow.formapago_id = None
            _dbRow.metodopago_id = None
            _dbRow.monedacontpaqi_id = TEMPRESA_MONEDASCONTPAQI.XXX
            _dbRow.receptorusocfdi_id = TUSOSCFDI.POR_DEFINIR

            _D_results = super(TCFDIS.PAGO, cls).MAPEAR_CAMPOS(_dbRow, b_actualizarRegistro = False)
            _D_return.combinar(_D_results)

            # En el CFDI de pago todos los calculos son zero
            _D_actualizar = _D_results.D_actualizar
            _D_actualizar.update(
                formapago_id = None,
                metodopago_id = None,
                monedacontpaqi_id = TEMPRESA_MONEDASCONTPAQI.XXX,
                receptorusocfdi_id = TUSOSCFDI.POR_DEFINIR,
                credito_plazo = 0,
                tipocambio = 0,
                moneda = "XXX",
                condicionesdepago = "",
                )

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)
                    break
                else:
                    pass

            return _D_return

        @classmethod
        def CALCULAR_CAMPOS(
                cls,
                x_cfdi,
                **D_params
                ):
            """ Recalcula campos que tienen impacto cuando otros registros de otras tablas cambian
            Esta operación así mismo detecta y agrega las inconsistencias.

            Utilizar la funciónes genéricas a necesidad; antes o despues de lógica especíifica.

            @param x_cfdi:
            @type x_cfdi:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_results = super(TCFDIS.PAGO, cls).CALCULAR_CAMPOS(_dbRow, b_actualizarRegistro = False)
            _D_return.combinar(_D_results)

            # En el CFDI de pago todos los calculos son zero
            _D_actualizar = _D_results.D_actualizar
            _D_actualizar.update(
                subtotal = 0,
                descuento = 0,
                totalimpuestostrasladados = 0,
                totalimpuestosretenidos = 0,
                total = 0,
                totalpago = 0,
                errores = ""
                )

            _dbRows_compPagos = _dbRow.tcfdi_complementopagos.select()
            for _dbRow_comPago in _dbRows_compPagos:
                _dbRows_compPago_docsRel = _dbRow_comPago.tcfdi_complementopago_doctosrelacionados.select()
                _f_suma = 0
                for _dbRow_comPago_docRel in _dbRows_compPago_docsRel:
                    _f_suma += _dbRow_comPago_docRel.imppagado_monedapago
                if _f_suma != _dbRow_comPago.monto:
                    _D_actualizar.errores += ("Monto de pago %.2f e importe pagado %.2f en docs. "
                        + "relacionados no coincide") % (_dbRow_comPago.monto, _f_suma)
                else:
                    pass
                _D_actualizar.totalpago += _dbRow_comPago.monto * _dbRow_comPago.tipocambio

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)
                    break
                else:
                    pass

            return _D_return

    class EGRESO(PROC, object):
        """ Clase para manejo de CFDI de Egreso

        """

        def __init__(self, **D_defaults):
            """ Inicializa objecto de tipo PROC para control del registro actual

            @param D_defaults:
            @type D_defaults:
            """
            super(TCFDIS.EGRESO, self).__init__(**(D_defaults or Storage()))  # For Python 2.7
            # super().__init__(D_defaults or Storage())  # For Python 3
            self._L_requeridos += ['receptorcliente_id']
            return

        def configuracampos_nuevo(
                self,
                s_cfdi_id,
                b_llamadaDesdeEditar = False,
                **D_defaults
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar.

            La lógica es involucrar chequeos locales e involucrar la función genérica para configurar pines;
            luego utilizar lógica local para los ajustes finos a los pines.

            @param b_llamadaDesdeEditar: Indica si la llamada a esta función es desde otra capa,
             desactivando validación de puede_crear
            @type b_llamadaDesdeEditar:
            @param s_cfdi_id:
            @type s_cfdi_id:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            if not s_cfdi_id:
                # Se usan como default los ultimos valores usados
                _dbRows_cfdi_ultimo = dbc01(
                    (dbc01.tcfdis.empresa_id == self._D_defaults.empresa_id)
                    & (dbc01.tcfdis.receptorcliente_id == self._D_defaults.receptorcliente_id)
                    & (dbc01.tcfdis.tipocomprobante_id == TTIPOSCOMPROBANTES.EGRESO)
                    ).select(
                        dbc01.tcfdis.id,
                        orderby = ~dbc01.tcfdis.id,
                        limitby = (0, 1)
                        )
                if _dbRows_cfdi_ultimo:
                    s_cfdi_id = _dbRows_cfdi_ultimo.first().id
                else:
                    pass
            else:
                pass

            _D_results = super(TCFDIS.EGRESO, self).configuracampos_nuevo(
                s_cfdi_id = s_cfdi_id,
                b_llamadaDesdeEditar = b_llamadaDesdeEditar,
                **self._D_defaults
                )
            _D_return.combinar(_D_results)

            # Se configuran los campos especificos para el caso de esta clase
            self.dbTabla.empresa_id.writable = False
            self.dbTabla.empresa_plaza_sucursal_cajachica_id.writable = True
            self.dbTabla.fecha.writable = True
            self.dbTabla.formapago_id.writable = True
            self.dbTabla.condicionesdepago.writable = True
            self.dbTabla.moneda_id.writable = False
            self.dbTabla.monedacontpaqi_id.writable = True
            self.dbTabla.tipocambio.writable = True
            self.dbTabla.tipocomprobante_id.writable = False
            self.dbTabla.metodopago_id.writable = False
            self.dbTabla.lugarexpedicion_id.writable = False
            self.dbTabla.tiporelacion_id.writable = True
            self.dbTabla.tiporelacion_id.requires = IS_NULL_OR(
                IS_IN_DB(
                    db01(db01.ttiposrelaciones.aplicaegreso == True), 'ttiposrelaciones.id',
                    db01.ttiposrelaciones._format
                    )
                )
            self.dbTabla.tiporelacion_id.comment = "Configurar únicamente en el caso de agregar CFDIs relacionados"
            # TODO Usar dbc01.tcfdis2
            # self.dbTabla.ordencompra.writable = False
            # self.dbTabla.foliointernopedido.writable = False
            # self.dbTabla.agentevendedor.writable = False
            # self.dbTabla.montobaseprodiva0.writable = False
            # self.dbTabla.montobaseprodiva.writable = False
            self.dbTabla.emisorproveedor_id.writable = False
            self.dbTabla.receptorusocfdi_id.writable = True
            self.dbTabla.receptorcliente_id.writable = False  # Se pone por default el cliente actual
            self.dbTabla.uuid.writable = False
            self.dbTabla.fechatimbrado.writable = False
            self.dbTabla.rol.writable = False
            self.dbTabla.etapa.writable = False
            self.dbTabla.etapa_cancelado.writable = False
            self.dbTabla.estado.writable = False
            self.dbTabla.generado_por.writable = False
            self.dbTabla.errores.writable = False
            self.dbTabla.sat_status.writable = False
            self.dbTabla.sat_fecha_cancelacion.writable = False
            self.dbTabla.sat_status_cancelacion.writable = False
            self.dbTabla.sat_status_proceso_cancelacion.writable = False
            self.dbTabla.pac_rfc.writable = False
            self.dbTabla.pac_razon_social.writable = False
            self.dbTabla.pac_nombre_comercial.writable = False
            self.dbTabla.cancelado_folio.writable = False
            self.dbTabla.cfdi_complemento_id.writable = False
            self.dbTabla.emp_pla_suc_almacen_id.writable = False

            # Campos que no deben ser editados, solo de forma autocalculada
            # Se configuran en configuracampos_nuevo del maestro

            # Se definen los valores por default sobreescribiendo los anteriores siendo especificos para esta clase
            self.dbTabla.empresa_id.default = self._D_defaults.empresa_id
            self.dbTabla.formapago_id.default = TFORMASPAGO.POR_DEFINIR
            self.dbTabla.monedacontpaqi_id.default = TEMPRESA_MONEDASCONTPAQI.XXX
            self.dbTabla.tipocambio.default = 1
            self.dbTabla.tipocomprobante_id.default = TTIPOSCOMPROBANTES.EGRESO
            self.dbTabla.metodopago_id.default = TMETODOSPAGO.UNAEXHIBICION
            self.dbTabla.receptorusocfdi_id.default = TUSOSCFDI.POR_DEFINIR
            self.dbTabla.receptorcliente_id.default = self._D_defaults.receptorcliente_id
            self.dbTabla.rol.default = TCFDIS.E_ROL.EMISOR
            self.dbTabla.generado_por.default = TCFDIS.E_GENERADO_POR.MANUAL

            return _D_return

        def configuracampos_edicion(
                self,
                x_cfdi,
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            Esta función NO debe llamar a la función genérica para configurar campos, ya que las condiciones de
            configuracionCampos_nuevo son muy específicas a esta clase. Si existen ajustes finos en la función
            genérica, estos deben copiarse a esta función específica de la clase

            @param x_cfdi: es el registro del movimiento o su id
            @type x_cfdi:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi, self.dbTabla)
            _dbRow = _D_results.dbRow

            _D_results = super(TCFDIS.EGRESO, self).configuracampos_edicion(_dbRow)
            _D_return.combinar(_D_results)

            if _D_results.E_return == CLASS_e_RETURN.OK:
                # Si puede editar regresa un ok y el movimiento esta abierto

                if not _dbRow.empresa_serie_id:
                    self.dbTabla.empresa_plaza_sucursal_cajachica_id.writable = True
                else:
                    self.dbTabla.empresa_plaza_sucursal_cajachica_id.writable = False

            else:
                # De lo contrario en cualquier otro caso, solo se pueden editar las observaciones
                pass

            return _D_return

        def al_validar(
                self,
                O_form,
                **D_params
                ):
            """ Validar la información para poder grabar cfdi.

            La lógica es tomar los campos necesarios para la validación específica, dependiendo de
            si es una inserción o edición. Luego llamar la validación genérica y completar los resultados
            con las validaciones específicas.

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _dbRow = self.dbTabla(O_form.record_id) if O_form.record_id else None
            _D_camposAChecar = STV_FWK_LIB.OBTENER_CAMPOSACHECAR_PARA_VALIDAR(self.dbTabla, _dbRow)

            # Validaciones

            O_form = super(TCFDIS.EGRESO, self).al_validar(O_form, _dbRow, D_camposAChecar = _D_camposAChecar)

            if int(_D_camposAChecar.tipocomprobante_id) != TTIPOSCOMPROBANTES.EGRESO:
                O_form.errors.id = "Esta validación es solamente para CFDIs de tipo egreso"
            else:
                pass

            if not _D_camposAChecar.empresa_plaza_sucursal_cajachica_id:
                O_form.errors.empresa_plaza_sucursal_cajachica_id = "Caja chica es requerida"
            else:
                pass

            return O_form

    class TRASLADO(PROC, object):
        """ Clase para manejo de CFDI de traslado

        """

        def __init__(self, **D_defaults):
            """ Inicializa objecto de tipo PROC para control del registro actual

            @param D_defaults:
            @type D_defaults:
            """
            super(TCFDIS.TRASLADO, self).__init__(**(D_defaults or Storage()))  # For Python 2.7
            # super().__init__(D_defaults or Storage())  # For Python 3
            self._L_requeridos += ['emp_pla_suc_almacen_id']
            return

        def configuracampos_nuevo(
                self,
                s_cfdi_id,
                b_llamadaDesdeEditar = False,
                **D_defaults
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar.

            La lógica es involucrar chequeos locales e involucrar la función genérica para configurar pines;
            luego utilizar lógica local para los ajustes finos a los pines.

            @param b_llamadaDesdeEditar:
            @type b_llamadaDesdeEditar:
            @param s_cfdi_id:
            @type s_cfdi_id:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = super(TCFDIS.TRASLADO, self).configuracampos_nuevo(
                s_cfdi_id = s_cfdi_id,
                b_llamadaDesdeEditar = b_llamadaDesdeEditar,
                **self._D_defaults
                )
            _D_return.combinar(_D_results)

            # Se configuran los campos especificos para el caso de esta clase
            self.dbTabla.empresa_id.writable = False
            self.dbTabla.empresa_plaza_sucursal_cajachica_id.writable = False
            self.dbTabla.fecha.writable = True
            self.dbTabla.formapago_id.writable = False
            self.dbTabla.condicionesdepago.writable = False
            self.dbTabla.moneda_id.writable = False
            self.dbTabla.monedacontpaqi_id.writable = False
            self.dbTabla.tipocambio.writable = False
            self.dbTabla.tipocomprobante_id.writable = False
            self.dbTabla.metodopago_id.writable = False
            self.dbTabla.lugarexpedicion_id.writable = False
            self.dbTabla.tiporelacion_id.writable = True
            self.dbTabla.tiporelacion_id.requires = IS_NULL_OR(
                IS_IN_DB(
                    db01(db01.ttiposrelaciones.aplicatraslado == True), 'ttiposrelaciones.id',
                    db01.ttiposrelaciones._format
                    )
                )
            self.dbTabla.tiporelacion_id.comment = "Configurar únicamente en el caso de agregar CFDIs relacionados"
            # dbc01.tcfdis2
            # self.dbTabla.ordencompra.writable = False
            # self.dbTabla.foliointernopedido.writable = False
            # self.dbTabla.agentevendedor.writable = False
            # self.dbTabla.montobaseprodiva0.writable = False
            # self.dbTabla.montobaseprodiva.writable = False
            self.dbTabla.emisorproveedor_id.writable = False
            self.dbTabla.receptorusocfdi_id.writable = False
            self.dbTabla.receptorcliente_id.writable = True
            self.dbTabla.uuid.writable = False
            self.dbTabla.fechatimbrado.writable = False
            self.dbTabla.rol.writable = False
            self.dbTabla.etapa.writable = False
            self.dbTabla.etapa_cancelado.writable = False
            self.dbTabla.estado.writable = False
            self.dbTabla.generado_por.writable = False
            self.dbTabla.errores.writable = False
            self.dbTabla.sat_status.writable = False
            self.dbTabla.sat_fecha_cancelacion.writable = False
            self.dbTabla.sat_status_cancelacion.writable = False
            self.dbTabla.sat_status_proceso_cancelacion.writable = False
            self.dbTabla.pac_rfc.writable = False
            self.dbTabla.pac_razon_social.writable = False
            self.dbTabla.pac_nombre_comercial.writable = False
            self.dbTabla.cancelado_folio.writable = False
            self.dbTabla.cfdi_complemento_id.writable = False
            self.dbTabla.emp_pla_suc_almacen_id.writable = False  # Se llena con el valor por default

            # Campos que no deben ser editados, solo de forma autocalculada
            # Se configuran en configuracampos_nuevo del maestro

            # Se definen los valores por default sobreescribiendo los anteriores siendo especificos para esta clase
            self.dbTabla.empresa_id.default = self._D_defaults.empresa_id
            self.dbTabla.formapago_id.default = TFORMASPAGO.POR_DEFINIR
            self.dbTabla.monedacontpaqi_id.default = TEMPRESA_MONEDASCONTPAQI.XXX
            self.dbTabla.tipocambio.default = 0
            self.dbTabla.tipocomprobante_id.default = TTIPOSCOMPROBANTES.TRASLADO
            self.dbTabla.metodopago_id.default = None
            self.dbTabla.receptorusocfdi_id.default = TUSOSCFDI.POR_DEFINIR
            self.dbTabla.rol.default = TCFDIS.E_ROL.EMISOR
            self.dbTabla.generado_por.default = TCFDIS.E_GENERADO_POR.MANUAL
            self.dbTabla.emp_pla_suc_almacen_id.default = self._D_defaults.emp_pla_suc_almacen_id

            return _D_return

        def al_validar(
                self,
                O_form,
                **D_params
                ):
            """ Validar la información para poder grabar cfdi.

            La lógica es tomar los campos necesarios para la validación específica, dependiendo de
            si es una inserción o edición. Luego llamar la validación genérica y completar los resultados
            con las validaciones específicas.

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _dbRow = self.dbTabla(O_form.record_id) if O_form.record_id else None
            _D_camposAChecar = STV_FWK_LIB.OBTENER_CAMPOSACHECAR_PARA_VALIDAR(self.dbTabla, _dbRow)

            # Validaciones

            O_form = super(TCFDIS.TRASLADO, self).al_validar(O_form, _dbRow, D_camposAChecar = _D_camposAChecar)

            if int(_D_camposAChecar.tipocomprobante_id) != TTIPOSCOMPROBANTES.TRASLADO:
                O_form.errors.id = "Esta validación es solamente para CFDIs de tipo traslado"
            else:
                pass

            return O_form

        @classmethod
        def MAPEAR_CAMPOS(
                cls,
                x_cfdi,
                **D_params
                ):
            """ Remapea campos solamente cuando se graba el registro

            Utilizar la funciónes genéricas a necesidad; antes o despues de lógica especíifica.

            @param x_cfdi:
            @type x_cfdi:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _dbRow.formapago_id = None
            _dbRow.metodopago_id = None
            _dbRow.monedacontpaqi_id = TEMPRESA_MONEDASCONTPAQI.XXX
            _dbRow.receptorusocfdi_id = TUSOSCFDI.POR_DEFINIR

            _D_results = super(TCFDIS.TRASPASO, cls).MAPEAR_CAMPOS(_dbRow, b_actualizarRegistro = False)
            _D_return.combinar(_D_results)

            # En el CFDI de pago todos los calculos son zero
            _D_actualizar = _D_results.D_actualizar
            _D_actualizar.update(
                formapago_id = None,
                metodopago_id = None,
                monedacontpaqi_id = TEMPRESA_MONEDASCONTPAQI.XXX,
                receptorusocfdi_id = TUSOSCFDI.POR_DEFINIR,
                credito_plazo = 0,
                tipocambio = 0,
                moneda = "XXX",
                condicionesdepago = "",
                )

            if not _D_actualizar.receptorrfc:
                _dbRow_empresa = dbc01.tempresas(_dbRow.empresa_id)
                _D_actualizar.receptorrfc = _dbRow_empresa.rfc_publico
            else:
                pass

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)
                    break
                else:
                    pass

            return _D_return

        @classmethod
        def CALCULAR_CAMPOS(
                cls,
                x_cfdi,
                **D_params
                ):
            """ Recalcula campos que tienen impacto cuando otros registros de otras tablas cambian
            Esta operación así mismo detecta y agrega las inconsistencias.

            Utilizar la funciónes genéricas a necesidad; antes o despues de lógica especíifica.

            @param x_cfdi:
            @type x_cfdi:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_results = super(TCFDIS.TRASLADO, cls).CALCULAR_CAMPOS(_dbRow, b_actualizarRegistro = False)
            _D_return.combinar(_D_results)

            # En el CFDI de pago todos los calculos son zero
            _D_actualizar = _D_results.D_actualizar
            _D_actualizar.update(
                subtotal = 0,
                descuento = 0,
                totalimpuestostrasladados = 0,
                totalimpuestosretenidos = 0,
                total = 0,
                totalpago = 0,
                errores = ""
                )

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)
                    break
                else:
                    pass

            return _D_return

    class TIMBRADO:

        @classmethod
        def PUEDE_TIMBRAR(cls, x_cfdi):
            """ Identifica las características genéricas para evaluar si es posible timbrar o no

            @param x_cfdi:
            @type x_cfdi:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi, dbc01.tcfdis)
            _dbRow = _D_results.dbRow

            _dt_minFechaPosible = request.browsernow - datetime.timedelta(hours = 72)

            # Se recalcula el saldo del cliente al intentar timbrar
            _D_result = TEMPRESA_CLIENTES.OBTENER_SALDO_CLIENTE(
                x_cliente_id = _dbRow.receptorcliente_id,
                x_cfdi_id_ignorar = None,
                )

            if _dbRow.sat_status != TCFDIS.SAT_STATUS.NO_DEFINIDO:
                _D_return.agrega_error("CFDI ya fue timbrado, no puede volverse a timbrar.")

            elif _dbRow.errores:
                _D_return.agrega_error("CFDI contiene inconsistencias o errores que necesitan resolverse.")

            elif _dbRow.fecha < _dt_minFechaPosible:
                _D_return.agrega_error("Fecha de CFDI esta fuera de rango para poder ser firmada (Min fecha %s)"
                    % _dt_minFechaPosible.isoformat())

            elif _dbRow.tcfdi_conceptos.count() == 0:
                _D_return.agrega_error("CFDI debe contener conceptos.")

            elif _dbRow.tiporelacion_id \
                    and (_dbRow.tcfdi_relacionados.count() == 0):
                _D_return.agrega_error("Si se define tipo de relación, deben existir CFDIs relacionados.")

            elif (_dbRow.tiporelacion_id == TTIPOSRELACIONES.SUSTITUCION) \
                    and dbc01(
                        (dbc01.tcfdi_relacionados.cfdi_id == _dbRow.id)
                        & (dbc01.tcfdis.id == dbc01.tcfdi_relacionados.cfdirelacionado_id)
                        & (dbc01.tcfdis.sat_status != TCFDIS.SAT_STATUS.CANCELADO)
                        ).count() > 0:
                _D_return.agrega_error("CFDI referenciados en CFDI por sustitución deben estar cancelados.")

            elif _dbRow.tiporelacion_id \
                    and dbc01(
                        (dbc01.tcfdi_relacionados.cfdi_id == _dbRow.id)
                        & (dbc01.tcfdis.id == dbc01.tcfdi_relacionados.cfdirelacionado_id)
                        & (dbc01.tcfdis.sat_status != TCFDIS.SAT_STATUS.VIGENTE)
                        ).count() > 0:
                _D_return.agrega_error("Uno o más de los CFDI relacionados no esta Vigente.")

            else:

                if _dbRow.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO:

                    # TODO verificar que los descuentos 1a1 correspondan a los posibles en el ingreso

                    _dbRow_cliente = dbc01.tempresa_clientes(_dbRow.receptorcliente_id)

                    if not _dbRow_cliente:
                        _D_return.agrega_error("CFDI de ingreso requiere tener el cliente definido.")

                    else:
                        # Se recalcula el saldo del cliente al intentar timbrar
                        _D_result = TEMPRESA_CLIENTES.OBTENER_SALDO_CLIENTE(
                            x_cliente_id = _dbRow_cliente,
                            x_cfdi_id_ignorar = None,
                            )

                        if _D_result.f_saldoCliente > _dbRow_cliente.credito_limite:
                            _D_return.agrega_condicionesincorrectas("Saldo del cliente es mayor al límite, "
                                                                    + "no puede firmarse el CFDI")

                        elif (
                                (_D_result.f_saldoVencidoCliente > 5)
                                and (_dbRow_cliente.credito_politica != TEMPRESA_CLIENTES.CREDITO.E_POLITICA.ABIERTA)
                                ):
                            _D_return.agrega_condicionesincorrectas("El cliente presenta saldo vencido y "
                                                                    + "su política no permite timbrar")

                        elif (_D_result.f_saldoCliente + _dbRow.total) > _dbRow_cliente.credito_limite:
                            _D_return.agrega_condicionesincorrectas("Total del CFDI excede linea de crédito")

                        else:
                            pass

                elif _dbRow.tipocomprobante_id == TTIPOSCOMPROBANTES.EGRESO:
                    # TODO verificar que los descuentos 1a1 correspondan a los posibles en los ingresos
                    pass

                elif _dbRow.tipocomprobante_id == TTIPOSCOMPROBANTES.PAGO:
                    # Se verifica que los cfdis pago docto relacionados esten vigentes
                    _dbRows_compPagos_doctoRel = dbc01(
                        (dbc01.tcfdi_complementopagos.cfdi_id == _dbRow.id)
                        & (dbc01.tcfdi_complementopago_doctosrelacionados.cfdi_complementopago_id
                           == dbc01.tcfdi_complementopagos.id)
                        & (dbc01.tcfdis.id == dbc01.tcfdi_complementopago_doctosrelacionados.cfdirelacionado_id)
                        ).select(
                            dbc01.tcfdi_complementopago_doctosrelacionados.ALL,
                            dbc01.tcfdis.id, dbc01.tcfdis.sat_status, dbc01.tcfdis.saldo
                            )

                    if not _dbRows_compPagos_doctoRel:
                        _D_return.agrega_error("CFDI de pago no contiene referencias válidas a CFDIs.")

                    else:
                        # Dict usado para cubrir el caso de que se page al mismo cfdi de ingreso
                        #  en el mismo cfdi de pago
                        _D_ingresos_saldos = Storage()
                        for _dbRow_compPagos in _dbRows_compPagos_doctoRel:
                            if (_dbRow_compPagos.tcfdis.sat_status != TCFDIS.SAT_STATUS.VIGENTE)\
                                    or ((_dbRow_compPagos.tcfdis.saldo or 0) <= 0):
                                _D_return.agrega_error("CFDI %d referenciado en pago no se encuentra vigente "
                                                       + "o no tiene saldo mayor que 0." % _dbRow_compPagos.tcfdis.id)

                            else:
                                if _dbRow_compPagos.tcfdis.id not in _D_ingresos_saldos:
                                    _D_ingresos_saldos[_dbRow_compPagos.tcfdis.id] = _dbRow_compPagos.tcfdis
                                else:
                                    pass

                                _dbRow_compPago_doctoRel = _dbRow_compPagos.tcfdi_complementopago_doctosrelacionados
                                _dbRow_compPago_doctoRel.impsaldoant = _D_ingresos_saldos[_dbRow_compPagos.tcfdis.id].saldo
                                _dbRow_compPago_doctoRel.impsaldoinsoluto = _dbRow_compPago_doctoRel.impsaldoant \
                                                                            - _dbRow_compPago_doctoRel.imppagado
                                _dbRow_compPago_doctoRel.update_record()

                                _D_ingresos_saldos[_dbRow_compPagos.tcfdis.id].saldo = \
                                    _dbRow_compPago_doctoRel.impsaldoinsoluto

                                if _dbRow_compPago_doctoRel.impsaldoinsoluto < 0:
                                    _D_return.agrega_error(
                                        ("Saldo del CFDI %d (%.2f) referenciado en pago "
                                        + "es menor al pago especificado (%.2f).")
                                        % (
                                            _dbRow_compPagos.tcfdis.id, _dbRow_compPagos.tcfdis.saldo or 0,
                                            _dbRow_compPago_doctoRel.imppagado or 0
                                            )
                                        )
                                else:
                                    pass

                        pass  # for _dbRow_compPagos in _dbRows_compPagos_doctoRel
                    pass  # if not _dbRows_compPagos_doctoRel

                elif _dbRow.tipocomprobante_id == TTIPOSCOMPROBANTES.TRASLADO:
                    pass

                else:
                    _D_return.E_return = stvfwk2_e_RETURN.NOK_CONDICIONES_INCORRECTAS
                    _D_return.s_msgError = (
                        "Tipo de comprobante no identificado o no soportado %d"
                        ) % _dbRow.tipocomprobante_id

            return _D_return

        @classmethod
        def PREPARAR_TIMBRADO(cls, x_cfdi):
            """ Prepara la información en el CFDI para timbrar, principalmente se borra el detalle de impuestos
            si la tasa es cero.

            @param x_cfdi:
            @type x_cfdi:
            @return:
            @rtype:
            """

            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi, dbc01.tcfdis)
            _dbRow = _D_results.dbRow

            # Si la base es cero para los impuestos, se elimina el elemento de impuesto

            _dbRows_borrarImpTras = dbc01(
                (dbc01.tcfdi_conceptos.cfdi_id == _dbRow.id)
                & (dbc01.tcfdi_concepto_impuestostrasladados.cfdi_concepto_id == dbc01.tcfdi_conceptos.id)
                & (dbc01.tcfdi_concepto_impuestostrasladados.base == 0)
                ).select(dbc01.tcfdi_concepto_impuestostrasladados.id)

            if _dbRows_borrarImpTras:
                _L_ids = []
                for _dbRow_imp in _dbRows_borrarImpTras:
                    _L_ids.append(_dbRow_imp.id)

                dbc01(dbc01.tcfdi_concepto_impuestostrasladados.id.belongs(_L_ids)).delete()
            else:
                pass

            _dbRows_borrarImpRet = dbc01(
                (dbc01.tcfdi_conceptos.cfdi_id == _dbRow.id)
                & (dbc01.tcfdi_concepto_impuestosretenidos.cfdi_concepto_id == dbc01.tcfdi_conceptos.id)
                & (dbc01.tcfdi_concepto_impuestosretenidos.base == 0)
                ).select(dbc01.tcfdi_concepto_impuestosretenidos.id)

            if _dbRows_borrarImpRet:
                _L_ids = []
                for _dbRow_imp in _dbRows_borrarImpRet:
                    _L_ids.append(_dbRow_imp.id)

                dbc01(dbc01.tcfdi_concepto_impuestosretenidos.id.belongs(_L_ids)).delete()
            else:
                pass

            _n_rowsBorrados = len(_dbRows_borrarImpTras) + len(_dbRows_borrarImpRet)

            if _n_rowsBorrados > 0:
                _D_return.E_return = stvfwk2_e_RETURN.OK_WARNING
                _D_return.s_msgError = (
                    "Impuestos borrados debido a que la importe del concepto es cero (%d)"
                    ) % _n_rowsBorrados
            else:
                pass

            return _D_return

        @classmethod
        def TIMBRAR(cls, x_cfdi):

            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi, dbc01.tcfdis)
            _dbRow = _D_results.dbRow

            _D_results = STV_LIB_CFDI.REPRESENTAR_JSON(
                n_empresa_id = _dbRow.empresa_id,
                n_cfdi_id = _dbRow.id
                ).procesar(
                    b_grabar = True
                    )

            # Calcular CFDI

            if _D_results.E_return > stvfwk2_e_RETURN.OK_WARNING:
                # Se actualiza abajo el _D_return
                pass
            else:
                _D_results = cls.PUEDE_TIMBRAR(x_cfdi = _dbRow)

            if _D_results.E_return > stvfwk2_e_RETURN.OK_WARNING:
                # Se actualiza abajo el _D_return
                pass
            else:
                _D_results = cls.PREPARAR_TIMBRADO(x_cfdi = _dbRow)

            if _D_results.E_return > stvfwk2_e_RETURN.OK_WARNING:
                _D_return.combinar(_D_results)

            else:

                _dbRow.estado = TCFDIS.E_ESTADO.EN_PROCESO_FIRMA
                _dbRow.update_record()

                _D_results = TSERVIDOR_INSTRUCCIONES.AGREGAR_TIMBRAR_CANCELAR_CFDI(
                    s_cuenta_id = D_stvFwkCfg.dbRow_cuenta.id,
                    dbRow_cfdi = _dbRow,
                    E_tipoInstruccion = TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.TIMBRAR_CFDI,
                    s_rastro = "Firma manual"
                    )

                if _D_results.E_return > stvfwk2_e_RETURN.OK_WARNING:
                    _dbRow.estado = TCFDIS.E_ESTADO.NO_DEFINIDO
                    _dbRow.errores = _D_results.s_msgError
                    _dbRow.update_record()
                else:
                    pass
                _D_return.combinar(_D_results)

            return _D_return

        @classmethod
        def PUEDE_CANCELAR(cls, x_cfdi):
            """ Verifica las condiciones necesaria que permitan poder cancelar un CFDI

            @param x_cfdi: CFDI a cancelar
            @type x_cfdi: dbRow o id
            @return: diccionario con E_return y s_msgError
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi, dbc01.tcfdis)
            _dbRow = _D_results.dbRow

            if not _dbRow.uuid:
                _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                _D_return.s_msgError = "CFDI no tiene uuid asociado"

            elif _dbRow.estado not in TCFDIS.E_ESTADO.L_SINPROCESOPENDIENTE:
                _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                _D_return.s_msgError = \
                    ("CFDI se encuentra en proceso pendiente (%s), debe terminar el proceso anterior "
                    + "para poder cancelarlo") % TCFDIS.E_ESTADO.GET_DICT()[_dbRow.estado]

            else:

                if _dbRow.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO:
                    # Buscar pagos relacionados no cancelados
                    _dbRows_pagos = dbc01(
                        (dbc01.tcfdi_complementopago_doctosrelacionados.cfdirelacionado_id == _dbRow.id)
                        & (dbc01.tcfdi_complementopagos.id
                            == dbc01.tcfdi_complementopago_doctosrelacionados.cfdi_complementopago_id)
                        & (dbc01.tcfdis.id == dbc01.tcfdi_complementopagos.cfdi_id)
                        & (dbc01.tcfdis.sat_status == TCFDIS.SAT_STATUS.VIGENTE)
                        ).select(
                            dbc01.tcfdis.id,
                            dbc01.tcfdis.serie,
                            dbc01.tcfdis.folio,
                        )

                    if _dbRows_pagos:
                        _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                        _D_return.s_msgError += "CFDI tiene %d pagos que no han sido cancelados: " % len(_dbRows_pagos)
                        for _dbRow_pago in _dbRows_pagos:
                            _D_return.s_msgError += "%d:%s %s; " \
                                % (_dbRow_pago.id, _dbRow_pago.serie, _dbRow_pago.folio)
                        _D_return.s_msgError += "\n"
                    else:
                        pass

                    # Buscar egresos relacionados no cancelados
                    _dbRows_egresos = dbc01(
                        (dbc01.tcfdi_relacionados.cfdirelacionado_id == _dbRow.id)
                        & (dbc01.tcfdis.id == dbc01.tcfdi_relacionados.cfdi_id)
                        & (dbc01.tcfdis.sat_status == TCFDIS.SAT_STATUS.VIGENTE)
                        ).select(
                            dbc01.tcfdis.id,
                            dbc01.tcfdis.serie,
                            dbc01.tcfdis.folio,
                        )

                    if _dbRows_egresos:
                        _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                        _D_return.s_msgError += "CFDI tiene %d egresos que no han sido cancelados: " \
                            % len(_dbRows_egresos)
                        for _dbRow_egreso in _dbRows_egresos:
                            _D_return.s_msgError += "%d:%s %s; " \
                                % (_dbRow_egreso.id, _dbRow_egreso.serie, _dbRow_egreso.folio)
                        _D_return.s_msgError += "\n"
                    else:
                        pass

                    # Buscar ajustes saldo del CFDI
                    _dbRows_ajustesSaldo = dbc01(
                        (dbc01.tcfdi_manejosaldos.cfdi_id == _dbRow.id)
                        ).select(
                        dbc01.tcfdis.id,
                        dbc01.tcfdis.serie,
                        dbc01.tcfdis.folio,
                        )

                    if dbc01(dbc01.tcfdi_manejosaldos.cfdi_id == _dbRow.id).count() > 0:
                        _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                        _D_return.s_msgError += "CFDI no puede cancelarse porque tiene registros en ajustes de saldo\n"
                    else:
                        pass

                    # Buscar si otro CFDI ajusto saldo a este CFDI
                    _dbRows_ajustesSaldo = dbc01(
                        (dbc01.tcfdi_manejosaldos.cfdirelacionado_id == _dbRow.id)
                        ).select(
                        dbc01.tcfdis.id,
                        dbc01.tcfdis.serie,
                        dbc01.tcfdis.folio,
                        )

                    if dbc01(dbc01.tcfdi_manejosaldos.cfdi_id == _dbRow.id).count() > 0:
                        _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                        _D_return.s_msgError += "CFDI no puede cancelarse porque tiene referencias de ajustes " \
                            + "de saldo\n"
                    else:
                        pass

                else:
                    # Otros comprobantes no requieren validación
                    pass

            return _D_return

        @classmethod
        def CANCELAR(cls, x_cfdi):

            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi, dbc01.tcfdis)
            _dbRow = _D_results.dbRow

            # Calcular CFDI
            _D_results = cls.PUEDE_CANCELAR(x_cfdi = _dbRow)

            if _D_results.E_return > stvfwk2_e_RETURN.OK_WARNING:
                _D_return.combinar(_D_results)

            else:

                _dbRow.estado = TCFDIS.E_ESTADO.EN_PROCESO_CANCELACION
                _dbRow.update_record()

                _D_results = TSERVIDOR_INSTRUCCIONES.AGREGAR_TIMBRAR_CANCELAR_CFDI(
                    s_cuenta_id = D_stvFwkCfg.dbRow_cuenta.id,
                    dbRow_cfdi = _dbRow,
                    E_tipoInstruccion = TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.CANCELAR_CFDI,
                    s_rastro = "Firma manual"
                    )

                if _D_results.E_return > stvfwk2_e_RETURN.OK_WARNING:
                    _dbRow.estado = E_ESTADO.NO_DEFINIDO
                    _dbRow.errores = _D_results.s_msgError
                    _dbRow.update_record()
                else:
                    pass
                _D_return.combinar(_D_results)

            return _D_return

        pass  # PROC

    class EDOCTA:

        class E_PROCESO:
            NINGUNO = 0
            CLIENTE = 1
            CFDI = 2

        def __init__(self, n_empresa_id, n_cliente_id, E_movto_generado_por = None):
            self._n_return = stvfwk2_e_RETURN.OK
            self._n_empresa_id = int(n_empresa_id)
            self._n_cliente_id = int(n_cliente_id)
            self._n_cfdi_id = None  # Usado en la actualización por CFDI
            self._dbRow_cfdi = None  # Usado en la actualización por CFDI

            self._D_multireturn = FUNC_MULTIRETURN()

            # Contendrá los registros de los movimientos con el ID del ingreso relacionado como indice
            self._D_registros = Storage()

            self._dbRow_cliente = None
            self._D_saldos = Storage(
                f_saldo = 0,
                f_saldoVencido = 0,
                )
            self._dt_empezarde = TCFDIS.GET_PRIMERDIA2020().dt_utc
            self._dt_procesoInicio = datetime.datetime.now()
            self._dt_procesoFin = datetime.datetime.now()

            self._E_proceso = self.E_PROCESO.NINGUNO
            self._E_movto_generado_por = E_movto_generado_por or TCFDI_MOVIMIENTOS.E_GENERADO_POR.NO_DEFINIDO

            self._D_debug = FUNC_DEBUG()
            self._D_debug.entra_proceso('Inicialización')
            return

        def _addMovimiento(
                self,
                x_cfdi_ingreso,
                dbRow_cfdi,

                dt_fecha,
                E_accion,
                E_tipodocumento,
                s_descripcion,
                n_cfdi_complementopago_doctorelacionado_id = None,  # Si viene de un pago
                n_cfdi_relacionado_id = None,  # Si viene de un egreso
                n_cfdi_movimiento_emision_id = None,  # Si es una cancelación
                n_cfdi_manejosaldo_id = None,  # Si viene de un manejo de saldo

                dbRow_relacionado = None,  # Corresponde al dbRow del relacionado en egreso o pago

                f_importe = 0,  # Importe del movimiento a contabilizar en moneda del movto
                f_cargo = 0,  # Cargo en caso de ser cargo en moneda del movto
                f_abono = 0,  # Abono en caso de ser abono en moneda del movto
                f_saldoPrevio = 0,  # Saldo anterior en caso de saberlo en moneda del ingreso

                s_monedacontpaqi_id = None,  # Moneda usada en el movimiento
                f_tipocambio = 1,  # Tipo de cambio usado en el movimiento

                n_cfdi_movtoActualizar_emision = None,  # Se define en caso de requerir actualizarlo solamente
                n_cfdi_movtoActualizar_cancelacion = None  # Se define en caso de requerir actualizarlo solamente
                ):
            """ Registra un movimiento en la tabla de movimientos y agrega información a un dict del objeto.

            @param dt_fecha: fecha del movimiento principal.
            @param E_accion: TCFDI_MOVIMIENTOS.E_ACCION
            @param E_tipodocumento: Tipo de documento
            @param s_descripcion: descripción del movmiento.
            @param dbRow_cfdi: dbRow con la infromación del CFDI.
            @param x_cfdi_ingreso: id o row del ingreso relacionado.
            @param n_cfdi_complementopago_doctorelacionado_id: en caso de pagos, es el id del complemento
             de pago relacionado.
            @param n_cfdi_relacionado_id: en caso de egresos, es el id del documento relacionado.
            @param n_cfdi_movimiento_emision_id: en caso de egresos, es el id del detalle de referencias
             al documento referenciado.
            @param f_importe: importe del movimiento en caso de estar claramente definido, si no es posible
             determinarlo, debe ser cero.
            @param f_cargo: usado únicamente cuando se procesan CFDIs individuales.
            @param f_abono: usado únicamente cuando se procesan CFDIs individuales.
            @param f_saldo: usado únicamente cuando se procesan CFDIs individuales.
            """

            _D_return = FUNC_RETURN(
                n_cfdi_movimiento_id = n_cfdi_movimiento_emision_id,
                n_cfdi_movimientoCancelacion_id = None,
                f_saldoNuevo = f_saldoPrevio,
                f_importeNuevo = f_importe
                )

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(
                x_cfdi_ingreso, dbc01.tcfdis, b_regresarRegistro = False
                )
            _n_cfdiIngreso_id = _D_results.n_id
            _dbRow_cfdiIngreso = _D_results.dbRow

            # El control del estado de cuenta se basa en la lista del ingreso, por lo que es lo primero que se busca
            if not _n_cfdiIngreso_id:
                # Es muy probable que el CFDI de ingreso se ignoro, debido a que no esta en el rango de
                #  determinación de saldo
                pass

            elif _n_cfdiIngreso_id not in self._D_registros:
                # Si no existe, se crea el registro para control del ingreso

                if not _dbRow_cfdiIngreso:
                    _dbRow_cfdiIngreso = dbc01.tcfdis(_n_cfdiIngreso_id)
                else:
                    pass

                self._D_registros[_n_cfdiIngreso_id] = Storage(
                    dbRow = _dbRow_cfdiIngreso,  # Es el CFDI del ingreso
                    saldo = 0,
                    cargos = 0,  # Contendrá los cargos identificados en la primer etapa
                    abonos = 0,  # Contendrá los abonos identificados en la primer etapa
                    aplicadoscargos = 0,  # Contendrá los cargos identificados en fase 1 que ya fueron aplicados
                    aplicadosabonos = 0,  # Contendrá los abonos identificados en fase 1 que ya fueron aplicados
                    # Contendrá los cargos cuyo importe no identificado en fase 1 que ya fueron aplicados
                    variablescargos = 0,
                    # Contendrá los abonos cuyo importe no identificado en fase 1 que ya fueron aplicados
                    variablesabonos = 0,
                    # Todas las referencia de CFDIs relacionados con el ingreso, el key es el cfdi_id
                    referencias = Storage(),
                    # Referencias únicamente a los CFDIs que no pudo determinarse el monto del cargo/abono
                    referenciasNoAplicadas = Storage(),
                    referenciasRelacionados = Storage(),  # En caso de egresos se agrega la lista de egresos
                    referenciasPagos = Storage(),  # En caso de pagos se agrega la lista de egresos
                    # Es un consecutivo a utilizar para identificar el número del movimiento del CFDI de ingreso
                    numMovimiento = 0,
                    )

            else:
                # Ingreso ya fue agregado antes
                pass

            if _n_cfdiIngreso_id in self._D_registros:

                _D_registro = self._D_registros[_n_cfdiIngreso_id]

                if dbRow_cfdi.sat_status == TCFDIS.SAT_STATUS.VIGENTE:
                    # Se busca si el CFDI esta esta sustituido
                    _dbRows_sustitutos = dbc01(
                        (dbc01.tcfdi_relacionados.cfdirelacionado_id == dbRow_cfdi.id)
                        & (dbc01.tcfdis.id == dbc01.tcfdi_relacionados.cfdi_id)
                        & (
                            (dbc01.tcfdis.tiporelacion_id == TTIPOSRELACIONES.SUSTITUCION)
                            | (dbc01.tcfdis.tiporelacion == '04')
                            )
                        ).select(
                            dbc01.tcfdis.id,
                            dbc01.tcfdis.fecha,
                            dbc01.tcfdis.fechatimbrado,
                        )

                    if _dbRows_sustitutos:
                        dbRow_cfdi.sat_status = TCFDIS.SAT_STATUS.CANCELADO
                        dbRow_cfdi.sat_fecha_cancelacion = _dbRows_sustitutos.first().fechatimbrado
                        dbRow_cfdi.notas = (dbRow_cfdi.notas or "") + "Se cancela debido a susticución. "
                        # TODOMejora verificar el caso en que existan varios sutitutos

                    else:
                        pass
                else:
                    pass

                if str(_D_registro.dbRow.monedacontpaqi_id) != str(s_monedacontpaqi_id):
                    # Si la moneda del movimiento, es diferente a la moneda del ingreso
                    _f_factor = f_tipocambio / _D_registro.dbRow.tipocambio
                    _D_return.f_importeNuevo *= _f_factor
                    f_cargo *= _f_factor
                    f_abono *= _f_factor
                else:
                    pass

                if dbRow_cfdi.uuid:
                    # Si esta timbrado o cancelado, se actualiza el saldo
                    _D_return.f_saldoNuevo = f_saldoPrevio + f_cargo - f_abono
                else:
                    _D_return.f_saldoNuevo = 0

                if E_tipodocumento in (
                        TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.INGRESO,
                        TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DEBITO,
                        TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.TRASPASO_SALDO_A_FAVOR,
                        ):
                    _D_registro.cargos += _D_return.f_importeNuevo

                elif E_tipodocumento in (
                        TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.PAGO,
                        TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DEVOLUCION,
                        TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DESCUENTO,
                        TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.ANTICIPO,
                        TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.APLICACION_SALDO_A_FAVOR,
                        ):

                    _D_registro.abonos += _D_return.f_importeNuevo

                elif E_tipodocumento in (
                        TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.OTRO,
                        TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.NO_DEFINIDO,
                        ):
                    _D_registro.cargos += f_cargo
                    _D_registro.abonos += f_abono

                else:
                    self._D_multireturn.agrega_error(
                        "CFDI %d tipo de documento no identificado %s, Llamar al admministrador."
                        % dbRow_cfdi.id, TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.D_TODOS.get(E_tipodocumento, 0)
                        )
                    return _D_return

                # Todas las referencias se agregan a esta lista
                _D_registro.referencias[dbRow_cfdi.id] = dbRow_cfdi

                # Si el importe es 0, no se pudo determinar el monto a aplicar, por lo que se graba la referencia
                #  para su uso en la siguiente etapa para determinar el saldo a aplicar
                if (_D_return.f_importeNuevo == 0) and (E_tipodocumento != TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.INGRESO):
                    # Pueden existir ingresos con importe zero y deben ignorarse
                    _D_registro.referenciasNoAplicadas[dbRow_cfdi.id] = dbRow_cfdi
                else:
                    pass

                # Si es egreso, se agrega a la lista de egresos relacionados al ingreso
                if n_cfdi_relacionado_id:
                    _D_registro.referenciasRelacionados[n_cfdi_relacionado_id] = dbRow_relacionado
                else:
                    pass
                if n_cfdi_complementopago_doctorelacionado_id:
                    _D_registro.referenciasPagos[dbRow_cfdi.id] = dbRow_relacionado
                else:
                    pass

                _D_insertarMovimiento = Storage(
                    empresa_id = self._n_empresa_id,
                    cliente_id = self._n_cliente_id,
                    fecha = dt_fecha,
                    fechaingreso = _D_registro.dbRow.fecha,
                    accion = E_accion
                    if dbRow_cfdi.uuid
                    # Si sat status no esta definido se sobreescribe la accion a no timbrada
                    else TCFDI_MOVIMIENTOS.E_ACCION.NO_TIMBRADA,
                    tipodocumento = E_tipodocumento,
                    descripcion = s_descripcion,
                    cfdi_id = dbRow_cfdi.id,
                    cfdi_complementopago_doctorelacionado_id = n_cfdi_complementopago_doctorelacionado_id,
                    cfdi_relacionado_id = n_cfdi_relacionado_id,
                    cfdi_ingreso_id = _n_cfdiIngreso_id,
                    cfdi_movimiento_emision_id = _D_return.n_cfdi_movimiento_id,
                    cfdi_manejosaldo_id = n_cfdi_manejosaldo_id,
                    cargo = f_cargo,
                    abono = f_abono,
                    saldo = _D_return.f_saldoNuevo,
                    importe = _D_return.f_importeNuevo,
                    importe_monedapago = f_importe,
                    monedacontpaqi_id = s_monedacontpaqi_id or TEMPRESA_MONEDASCONTPAQI.MXP,
                    tipocambio = f_tipocambio,
                    generado_por = self._E_movto_generado_por
                    )

                if (self._E_proceso == self.E_PROCESO.CLIENTE) or (E_accion == TCFDI_MOVIMIENTOS.E_ACCION.EMISION):
                    # Si esta ejecutando el proceso de cálculo por cliente, o la acción a insertar es una emisión
                    if n_cfdi_movtoActualizar_emision:
                        dbc01(dbc01.tcfdi_movimientos.id == n_cfdi_movtoActualizar_emision)\
                            .update(**_D_insertarMovimiento)
                        _D_return.n_cfdi_movimiento_id = n_cfdi_movtoActualizar_emision

                    else:
                        if self._E_proceso == self.E_PROCESO.CFDI:
                            _D_insertarMovimiento.orden = dbc01(
                                dbc01.tcfdi_movimientos.cfdi_ingreso_id == _n_cfdiIngreso_id
                                ).count() + 1
                        else:
                            # El campo orden se llena en la fase 2
                            pass
                        _D_return.n_cfdi_movimiento_id = dbc01.tcfdi_movimientos.insert(**_D_insertarMovimiento)
                else:
                    # En caso contrario, si se esta haciendo el proceso por CFDI y es una cancelación,
                    #  debe de insertarse la cancelación únicamente
                    pass

                if dbRow_cfdi.sat_status == TCFDIS.SAT_STATUS.CANCELADO:

                    if dt_fecha >= dbRow_cfdi.sat_fecha_cancelacion:
                        _dt_fechaCancelacion = dt_fecha + datetime.timedelta(seconds = 1)
                    else:
                        _dt_fechaCancelacion = dbRow_cfdi.sat_fecha_cancelacion

                    _D_insertarMovimiento.fecha = _dt_fechaCancelacion
                    _D_insertarMovimiento.accion = TCFDI_MOVIMIENTOS.E_ACCION.CANCELACION
                    _D_insertarMovimiento.cfdi_movimiento_emision_id = _D_return.n_cfdi_movimiento_id

                    # No se rotan los cargos y abonos, ya que en el caso de hacer el proceso por cliente, cargos
                    #  y abonos se definen en la fase 2; y en el caso de ser por CFDI, se mandan al llamar esta función

                    if (self._E_proceso == self.E_PROCESO.CLIENTE) \
                            or (E_accion == TCFDI_MOVIMIENTOS.E_ACCION.CANCELACION):
                        # Si esta ejecutando el proceso de cálculo de saldo por cliente, o la acción corresponse
                        #  a una cancelación
                        if n_cfdi_movtoActualizar_cancelacion:
                            dbc01(dbc01.tcfdi_movimientos.id == n_cfdi_movtoActualizar_cancelacion) \
                                .update(**_D_insertarMovimiento)

                        else:
                            _D_return.n_cfdi_movimientoCancelacion_id = dbc01.tcfdi_movimientos.insert(
                                **_D_insertarMovimiento
                                )

                    else:
                        pass

                    if E_tipodocumento in (
                            TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.INGRESO,
                            TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DEBITO
                            ):
                        _D_registro.abonos += _D_return.f_importeNuevo

                    elif E_tipodocumento in (
                            TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.PAGO,
                            TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DEVOLUCION,
                            TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DESCUENTO,
                            TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.ANTICIPO,
                            ):
                        _D_registro.cargos += _D_return.f_importeNuevo

                    elif E_tipodocumento in (
                            TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.OTRO,
                            TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.NO_DEFINIDO,
                            ):
                        _D_registro.cargos += f_abono
                        _D_registro.abonos += f_cargo

                    else:
                        self._D_multireturn.agrega_error(
                            "CFDI %d tipo de documento no procesa cancelación %s, Llamar al admministrador."
                            % dbRow_cfdi.id, TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.D_TODOS[E_tipodocumento]
                            )
                        return _D_return

                else:
                    pass

            else:
                # No se encuentra referencia a el ingreso del movimiento
                pass

            return _D_return

        def procesar_cliente(self):
            """ Se procesan todos los CFDIs del cliente para determinar su saldo

            Se empieza a partir de TCFDIS.GET_PRIMERDIA2020

            @return:
            @rtype:
            """

            self._E_proceso = self.E_PROCESO.CLIENTE
            self._dt_procesoInicio = datetime.datetime.now()

            self._dbRow_cliente = dbc01.tempresa_clientes(self._n_cliente_id)

            self._dbRow_cliente.procesostatus = TEMPRESA_CLIENTES.E_PROCESOESTATUS.CFDIS_PROCESANDO
            self._dbRow_cliente.update_record()
            dbc01.commit()

            # Se limpian los movimientos del cliente
            dbc01(
                (dbc01.tcfdi_movimientos.cliente_id == self._n_cliente_id)
                ).delete()

            # Todos los CFDIs del cliente, el saldo debe ser None para usar este estatus en caso de no ser analizado
            dbc01(
                (dbc01.tcfdis.receptorcliente_id == self._n_cliente_id)
                & (dbc01.tcfdis.saldo != None)
                ).update(saldo = None)

            # Se toman todos los CFDIs del cliente
            _dbRows_cfdi = dbc01(
                (dbc01.tcfdis.receptorcliente_id == self._n_cliente_id)
                & (dbc01.tcfdis.fecha >= self._dt_empezarde)
                # & (dbc01.tcfdis.sat_status != TCFDIS.SAT_STATUS.NO_DEFINIDO)
                ).select(
                    dbc01.tcfdis.ALL,
                    orderby = [dbc01.tcfdis.fecha, dbc01.tcfdis.id]
                    )

            self._D_debug.agrega_mensaje("Se recorren todos los cfdis encontrados del cliente despues de la fecha")
            _b_primerIngreso = False
            for _dbRow_cfdi in _dbRows_cfdi:

                if _b_primerIngreso \
                        or (_dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO):
                    _b_primerIngreso = True
                    self._cliente_fase1_cfdi(_dbRow_cfdi)
                else:
                    pass

            self._dbRow_cliente.procesostatus = TEMPRESA_CLIENTES.E_PROCESOESTATUS.CFDIS_APLICANDO_SALDOS
            self._dbRow_cliente.update_record()
            dbc01.commit()

            _dbRows_movtos = dbc01(
                dbc01.tcfdi_movimientos.cliente_id == self._n_cliente_id
                ).select(
                    dbc01.tcfdi_movimientos.ALL,
                    orderby = [
                        dbc01.tcfdi_movimientos.fecha,
                        # dbc01.tcfdi_movimientos.fechaingreso,
                        dbc01.tcfdi_movimientos.id,
                        ]
                    )

            for _dbRow_movto in _dbRows_movtos:
                self._cliente_fase2_aplicacionsaldos(_dbRow_movto)

            for _s_cfdi_id in self._D_registros:
                _D_registro = self._D_registros[_s_cfdi_id]
                _D_registro.dbRow.saldo = _D_registro.saldo
                _D_registro.dbRow.saldofechacalculo = request.utcnow

                if (_D_registro.dbRow.tipocomprobante_id != TTIPOSCOMPROBANTES.INGRESO) \
                        and (_D_registro.dbRow.saldo != 0):
                    self._D_multireturn.agrega_error(
                        "CFDI %s, quedó con saldo '%.2f' sin aplicar."
                        % (_s_cfdi_id, _D_registro.dbRow.saldo)
                        )

                else:
                    # En caso de no existir plazo de crédito en el ingreso, se agrega el de default del CFDI
                    if _D_registro.dbRow.credito_plazo:
                        pass

                    else:
                        _D_registro.dbRow.credito_plazo = self._dbRow_cliente.credito_plazo

                _D_registro.dbRow.update_record()

            self._dbRow_cliente.procesostatus = TEMPRESA_CLIENTES.E_PROCESOESTATUS.SIN_PROCESO
            self._dbRow_cliente.update_record()

            _D_result = TEMPRESA_CLIENTES.OBTENER_SALDO_CLIENTE(
                x_cliente_id = self._dbRow_cliente,
                x_cfdi_id_ignorar = None,
                )

            self._dt_procesoFin = datetime.datetime.now()
            self._D_multireturn.agrega_info(
                "Proceso tomó %d segundos. Saldo de %.2f, saldo vencido de %.2f"
                % (
                    (self._dt_procesoFin - self._dt_procesoInicio).seconds,
                    _D_result.f_saldoCliente,
                    _D_result.f_saldoVencidoCliente
                    )
                )

            self._E_proceso = self.E_PROCESO.NINGUNO

            return self._D_multireturn

        def _cliente_fase1_cfdi(self, dbRow_cfdi):
            """ Etapa 1 que procesa la información del CFDI

            @param dbRow_cfdi: CFDI a procesar
            """

            if dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO:

                self._addMovimiento(
                    x_cfdi_ingreso = dbRow_cfdi,
                    dbRow_cfdi = dbRow_cfdi,

                    dt_fecha = dbRow_cfdi.fecha,
                    E_accion = TCFDI_MOVIMIENTOS.E_ACCION.EMISION,
                    E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.INGRESO,
                    s_descripcion = "Factura",

                    f_importe = dbRow_cfdi.total,
                    s_monedacontpaqi_id = dbRow_cfdi.monedacontpaqi_id,
                    f_tipocambio = dbRow_cfdi.tipocambio
                    )

                # Se obtienen los registros de traspaso aplicables al cfdi actual
                _dbRows_manejoSaldos = dbc01(
                    (dbc01.tcfdi_manejosaldos.cfdi_id == dbRow_cfdi.id)
                    ).select(
                        dbc01.tcfdi_manejosaldos.ALL,
                        )

                for _dbRow_manejoSaldo in _dbRows_manejoSaldos:

                    self._addMovimiento(
                        x_cfdi_ingreso = dbRow_cfdi,
                        dbRow_cfdi = dbRow_cfdi,

                        dt_fecha = _dbRow_manejoSaldo.fecha,
                        E_accion = TCFDI_MOVIMIENTOS.E_ACCION.EMISION,
                        E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.TRASPASO_SALDO_A_FAVOR,
                        s_descripcion = "Traspaso",

                        n_cfdi_manejosaldo_id = _dbRow_manejoSaldo.id,

                        f_importe = _dbRow_manejoSaldo.importe,
                        s_monedacontpaqi_id = dbRow_cfdi.monedacontpaqi_id,
                        f_tipocambio = dbRow_cfdi.tipocambio
                        )

                # Se obtienen los registros de aplicacion de saldo a favor aplicables al CFDI actual
                _dbRows_manejoSaldos = dbc01(
                    (dbc01.tcfdi_manejosaldos.cfdirelacionado_id == dbRow_cfdi.id)
                    ).select(
                        dbc01.tcfdi_manejosaldos.ALL,
                        )

                for _dbRow_manejoSaldo in _dbRows_manejoSaldos:

                    self._addMovimiento(
                        x_cfdi_ingreso = dbRow_cfdi,
                        dbRow_cfdi = dbRow_cfdi,

                        dt_fecha = _dbRow_manejoSaldo.fecha,
                        E_accion = TCFDI_MOVIMIENTOS.E_ACCION.EMISION,
                        E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.APLICACION_SALDO_A_FAVOR,
                        s_descripcion = "Aplicación",

                        n_cfdi_manejosaldo_id = _dbRow_manejoSaldo.id,

                        f_importe = _dbRow_manejoSaldo.importe,
                        s_monedacontpaqi_id = dbRow_cfdi.monedacontpaqi_id,
                        f_tipocambio = dbRow_cfdi.tipocambio
                        )

            elif dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.PAGO:

                dbRow_cfdi.totalpago = 0
                _dbRows_pagos = dbc01(
                    (dbc01.tcfdi_complementopagos.cfdi_id == dbRow_cfdi.id)
                    & (dbc01.tcfdi_complementopago_doctosrelacionados.cfdi_complementopago_id
                       == dbc01.tcfdi_complementopagos.id)
                    ).select(
                        dbc01.tcfdi_complementopago_doctosrelacionados.ALL,
                        dbc01.tcfdi_complementopagos.ALL,
                        )

                for _dbRow_pago in _dbRows_pagos:
                    self._cliente_fase1_cfdi_pago(dbRow_cfdi, _dbRow_pago)

                dbRow_cfdi.update_record()

            elif dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.EGRESO:

                _dbRows_relacionados = dbc01(
                    (dbc01.tcfdi_relacionados.cfdi_id == dbRow_cfdi.id)
                    ).select(
                        dbc01.tcfdi_relacionados.ALL,
                        )

                _s_descripcion = ""
                _E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.NO_DEFINIDO
                if dbRow_cfdi.receptorusocfdi_id == TUSOSCFDI.DEVOLUCIONES_DESCUENTOS_BONIFICACIONES:

                    if dbRow_cfdi.tiporelacion_id == TTIPOSRELACIONES.NOTACREDITO:

                        _s_descripcion = "Descuento"
                        _E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DESCUENTO

                    elif dbRow_cfdi.tiporelacion_id == TTIPOSRELACIONES.DEVOLUCION:

                        _s_descripcion = "Devolucion"
                        _E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DEVOLUCION

                    else:

                        # Basado en la retroalimentación del cliente, se asigna como descuento
                        _s_descripcion = "Descuento"
                        _E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DESCUENTO
                        self._D_multireturn.agrega_advertencia(
                            ("CFDI de Egreso %d, tipo relación '%s' no válido para devolucion/descuento/bonificación. "
                             + "Se asigna como descuento.")
                            % (dbRow_cfdi.id, dbRow_cfdi.tiporelacion_id)
                            )

                else:

                    if dbRow_cfdi.tiporelacion_id == TTIPOSRELACIONES.NOTADEBITO:

                        _s_descripcion = "Nota de Debito"
                        _E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DEBITO

                    elif dbRow_cfdi.tiporelacion_id == TTIPOSRELACIONES.ANTICIPO:

                        _s_descripcion = "Anticipo"
                        _E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.ANTICIPO

                    elif dbRow_cfdi.tiporelacion_id == TTIPOSRELACIONES.NOTACREDITO:

                        _s_descripcion = "Descuento"
                        _E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DESCUENTO
                        self._D_multireturn.agrega_advertencia(
                            "CFDI de Egreso %d, tipo relación %d no válido, pero se procede a manejarlo como descuento"
                            % (dbRow_cfdi.id, dbRow_cfdi.tiporelacion_id)
                            )

                    elif not dbRow_cfdi.tiporelacion_id:

                        _s_descripcion = "Descuento"
                        _E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DESCUENTO
                        self._D_multireturn.agrega_advertencia(
                            "CFDI de Egreso %d, tipo relación esta vacío, pero se procede a manejarlo como descuento"
                            % dbRow_cfdi.id
                            )

                    else:

                        self._D_multireturn.agrega_error(
                            "CFDI de Egreso %d, tipo relación %d no válido"
                            % (dbRow_cfdi.id, dbRow_cfdi.tiporelacion_id)
                            )

                if _s_descripcion and _E_tipodocumento:
                    _n_numRelacionados = len(_dbRows_relacionados)
                    for _dbRow_relacionado in _dbRows_relacionados:
                        self._cliente_fase1_cfdi_egreso(
                            dbRow_cfdi, _dbRow_relacionado, _s_descripcion, _E_tipodocumento, _n_numRelacionados
                            )
                else:
                    # El error ya se definió en la condición anterior
                    pass

            else:
                self._D_multireturn.agrega_error(
                    "Tipo de CFDI %d no válido %d" % (dbRow_cfdi.id, dbRow_cfdi.tipocomprobante_id)
                    )

            return

        def _cliente_fase1_cfdi_pago(self, dbRow_cfdi, dbRow_pago):
            """ Se registra información de la parte 1 cuando es pago.

            @param dbRow_cfdi: CFDI principal del pago.
            @param dbRow_pago: Registro con información del complemento de pago y su documento relacionado.
            """

            if not dbRow_pago.tcfdi_complementopago_doctosrelacionados.cfdirelacionado_id:
                # Si no tiene un id, se busca por el uuid del docto relacionado
                TCFDI_COMPLEMENTOPAGO_DOCTOSRELACIONADOS.ASOCIAR_UUID(
                    dbRow_pago.tcfdi_complementopago_doctosrelacionados
                    )

                _dbRow_compPago_docRel = dbc01.tcfdi_complementopago_doctosrelacionados(
                    dbRow_pago.tcfdi_complementopago_doctosrelacionados.id
                    )
                if not _dbRow_compPago_docRel.cfdirelacionado_id:
                    self._D_multireturn.agrega_advertencia(
                        "CFDI referenciado (%s) en pago %d, es ignorado, ya que no se encuentra la referencia"
                        % (dbRow_pago.tcfdi_complementopago_doctosrelacionados.iddocumento, dbRow_cfdi.id)
                        )
                    return
                else:
                    _cfdirelacionado_id = _dbRow_compPago_docRel.cfdirelacionado_id

            else:
                _cfdirelacionado_id = dbRow_pago.tcfdi_complementopago_doctosrelacionados.cfdirelacionado_id

            if _cfdirelacionado_id in self._D_registros:
                _dbRow_cfdiIngreso = self._D_registros[_cfdirelacionado_id].dbRow

                if _dbRow_cfdiIngreso.tipocomprobante_id != TTIPOSCOMPROBANTES.INGRESO:
                    self._D_multireturn.agrega_error(
                        "El CFDI referenciado %d en pago %d no es de ingreso"
                        % (dbRow_pago.tcfdi_complementopago_doctosrelacionados.id, dbRow_cfdi.id)
                        )
                    _dbRow_cfdiIngreso.receptorcliente_id = None
                    _dbRow_cfdiIngreso.update_record()

                elif _dbRow_cfdiIngreso.receptorcliente_id != self._n_cliente_id:
                    self._D_multireturn.agrega_error(
                        "El CFDI referenciado %d en pago %d no corresponden al mismo cliente"
                        % (dbRow_pago.tcfdi_complementopago_doctosrelacionados.id, dbRow_cfdi.id)
                        )

                else:

                    if dbRow_pago.tcfdi_complementopagos.fechapago < _dbRow_cfdiIngreso.fecha:
                        self._D_multireturn.agrega_advertencia(
                            ("CFDI %d referenciado (%s) en pago, tiene fecha posterior. "
                             + "Se ajusta fecha para manejo de saldo")
                            % (dbRow_cfdi.id, dbRow_pago.tcfdi_complementopago_doctosrelacionados.iddocumento)
                            )
                        _dt_fecha = _dbRow_cfdiIngreso.fecha + datetime.timedelta(seconds = 1)
                    else:
                        _dt_fecha = dbRow_pago.tcfdi_complementopagos.fechapago

                    dbRow_cfdi.totalpago += dbRow_pago.tcfdi_complementopago_doctosrelacionados.imppagado

                    self._addMovimiento(
                        x_cfdi_ingreso = _dbRow_cfdiIngreso,
                        dbRow_cfdi = dbRow_cfdi,

                        dt_fecha = _dt_fecha,
                        E_accion = TCFDI_MOVIMIENTOS.E_ACCION.EMISION,
                        E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.PAGO,
                        s_descripcion = "Pago",
                        n_cfdi_complementopago_doctorelacionado_id =
                        dbRow_pago.tcfdi_complementopago_doctosrelacionados.id,

                        dbRow_relacionado = dbRow_pago.tcfdi_complementopago_doctosrelacionados,

                        f_importe = dbRow_pago.tcfdi_complementopago_doctosrelacionados.imppagado,

                        s_monedacontpaqi_id = dbRow_pago.tcfdi_complementopagos.monedacontpaqi_id,
                        f_tipocambio = dbRow_pago.tcfdi_complementopagos.tipocambio
                        )
            else:
                self._D_multireturn.agrega_advertencia(
                    "El CFDI referenciado %d en pago %d esta fuera de rango en cálculo por lo que se ignora."
                    % (_cfdirelacionado_id, dbRow_cfdi.id)
                    )
                pass

            return

        def _cliente_fase1_cfdi_egreso(
                self,
                dbRow_cfdi,
                dbRow_relacionado,
                s_descripcion,
                E_tipodocumento,
                n_numRelacionados
                ):
            """ Se registra en la etapa 1 los movimientos relacionados a los egresos.

            @param dbRow_cfdi: CFDI principal del egreso.
            @param dbRow_relacionado: Registro con información del documento relacionado.
            @param s_descripcion: _addMovimiento::s_descripcion
            @param E_tipodocumento: TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO
            @param n_numRelacionados: número de documentos encontrados en la relación al que corresponde el actual
             dbRow_relacionado.

            """

            if not dbRow_relacionado.cfdirelacionado_id:
                TCFDI_RELACIONADOS.ASOCIAR_UUID(dbRow_relacionado.id)

                _dbRow_docRelacionado_actualizado = dbc01.tcfdi_relacionados(dbRow_relacionado.id)
                if not _dbRow_docRelacionado_actualizado.cfdirelacionado_id:
                    self._D_multireturn.agrega_advertencia(
                        "CFDI referenciado (%s) en egreso %d, es ignorado, ya que no se encuentra la referencia"
                        % (dbRow_relacionado.uuid, dbRow_cfdi.id)
                        )
                    return
                else:
                    _cfdirelacionado_id = _dbRow_docRelacionado_actualizado.cfdirelacionado_id

            else:
                _cfdirelacionado_id = dbRow_relacionado.cfdirelacionado_id

            if _cfdirelacionado_id in self._D_registros:
                _dbRow_cfdiIngreso = self._D_registros[_cfdirelacionado_id].dbRow

                if _dbRow_cfdiIngreso.tipocomprobante_id != TTIPOSCOMPROBANTES.INGRESO:
                    self._D_multireturn.agrega_error(
                        "El CFDI referenciado %d en egreso %d no es de ingreso" % (dbRow_relacionado.id, dbRow_cfdi.id)
                        )
                    _dbRow_cfdiIngreso.cfdirelacionado_id = None
                    _dbRow_cfdiIngreso.update_record()

                elif _dbRow_cfdiIngreso.receptorcliente_id != self._n_cliente_id:
                    self._D_multireturn.agrega_error(
                        "El CFDI referenciado %d en egreso %d no corresponden al mismo cliente"
                        % (dbRow_relacionado.id, dbRow_cfdi.id)
                        )

                else:

                    if dbRow_cfdi.fecha < _dbRow_cfdiIngreso.fecha:
                        self._D_multireturn.agrega_advertencia(
                            ("CFDI %d referenciado (%s) en egreso, tiene fecha posterior. "
                             + "Se ajusta fecha para manejo de saldo")
                            % (dbRow_cfdi.id, dbRow_relacionado.uuid)
                            )
                        _dt_fecha = _dbRow_cfdiIngreso.fecha + datetime.timedelta(seconds = 1)
                    else:
                        _dt_fecha = dbRow_cfdi.fecha

                    # Si solamente tiene un elemento referenciado, se puede aplicar el total del CFDI
                    #  a solamente un ingreso;
                    #  de lo contrario, no se sabe y se deja en cero para su posterior aplicación
                    if n_numRelacionados == 1:
                        _f_importe = dbRow_cfdi.total
                    elif dbRow_relacionado.fijarimporte:
                        _f_importe = dbRow_relacionado.importe
                    else:
                        _f_importe = 0

                    self._addMovimiento(
                        x_cfdi_ingreso = _dbRow_cfdiIngreso,
                        dbRow_cfdi = dbRow_cfdi,

                        dt_fecha = _dt_fecha,
                        E_accion = TCFDI_MOVIMIENTOS.E_ACCION.EMISION,
                        E_tipodocumento = E_tipodocumento,
                        s_descripcion = s_descripcion,
                        n_cfdi_relacionado_id = dbRow_relacionado.id,

                        dbRow_relacionado = dbRow_relacionado,

                        f_importe = _f_importe,
                        s_monedacontpaqi_id = dbRow_cfdi.monedacontpaqi_id,
                        f_tipocambio = dbRow_cfdi.tipocambio
                        )
            else:
                self._D_multireturn.agrega_advertencia(
                    "El CFDI referenciado %d en egreso %d esta fuera de rango en cálculo por lo que se ignora."
                    % (_cfdirelacionado_id, dbRow_cfdi.id)
                    )
                pass

            return

        def _cliente_fase2_aplicacionsaldos(self, dbRow_movto):
            """ Hace el cálculo de los saldos del cliente en base a la información de la tabla de movimientos.

            @param dbRow_movto: registro del movimiento
            """

            if dbRow_movto.cfdi_ingreso_id not in self._D_registros:
                self._D_multireturn.agrega_error(
                    "No se encontró el ingreso para análisis del movimiento %d, por lo que se ignora" % dbRow_movto.id
                    )
                return
            else:

                _D_registro = self._D_registros[dbRow_movto.cfdi_ingreso_id]
                _dbRow_cfdiIngreso = _D_registro.dbRow

                _D_registro.numMovimiento += 1
                dbRow_movto.orden = _D_registro.numMovimiento

                if dbRow_movto.tipodocumento == TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.INGRESO:
                    if _dbRow_cfdiIngreso.metodopago_id == TMETODOSPAGO.UNAEXHIBICION:
                        dbRow_movto.abono = dbRow_movto.importe
                        dbRow_movto.cargo = dbRow_movto.importe

                    elif dbRow_movto.accion == TCFDI_MOVIMIENTOS.E_ACCION.CANCELACION:
                        dbRow_movto.abono = dbRow_movto.importe

                    else:
                        # En caso de emisión o no timbrada se usa como cargo
                        dbRow_movto.cargo = dbRow_movto.importe

                elif dbRow_movto.tipodocumento == TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.PAGO:
                    if dbRow_movto.accion == TCFDI_MOVIMIENTOS.E_ACCION.CANCELACION:
                        dbRow_movto.cargo = dbRow_movto.importe

                    else:
                        # En caso de emisión o no timbrada se usa como abono
                        dbRow_movto.abono = dbRow_movto.importe

                elif dbRow_movto.tipodocumento in (
                        TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DEVOLUCION,
                        TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DESCUENTO,
                        TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.ANTICIPO,
                        ):
                    self._cliente_fase2_aplicacionsaldos_egreso_devdesant(dbRow_movto)

                elif dbRow_movto.tipodocumento == TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DEBITO:
                    self._D_multireturn.agrega_error(
                        "CFDI movimiento %d de tipo Debito no implementado aún" % dbRow_movto.id
                        )

                elif dbRow_movto.tipodocumento == TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.TRASPASO_SALDO_A_FAVOR:
                    dbRow_movto.cargo = dbRow_movto.importe

                elif dbRow_movto.tipodocumento == TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.APLICACION_SALDO_A_FAVOR:
                    dbRow_movto.abono = dbRow_movto.importe

                else:
                    self._D_multireturn.agrega_error("CFDI movimiento %d no reconocido" % dbRow_movto.id)

                if dbRow_movto.accion in (
                        TCFDI_MOVIMIENTOS.E_ACCION.EMISION,
                        TCFDI_MOVIMIENTOS.E_ACCION.CANCELACION
                        ):
                    # Si es un movimiento timbrado, acción tiene valor de emisión o cancelación
                    _D_registro.saldo += dbRow_movto.cargo - dbRow_movto.abono

                    dbRow_movto.saldo = _D_registro.saldo
                    dbRow_movto.update_record()

                    # El importe define si el total a aplicar se definió en la fase 1
                    if dbRow_movto.importe:
                        _D_registro.aplicadoscargos += dbRow_movto.cargo
                        _D_registro.aplicadosabonos += dbRow_movto.abono
                    else:
                        _D_registro.variablescargos += dbRow_movto.cargo
                        _D_registro.variablesabonos += dbRow_movto.abono

                else:
                    # Si el CFDI no ha sido timbrado, no se manejan saldos, solo importes
                    dbRow_movto.update_record()

            return

        def _cliente_fase2_aplicacionsaldos_egreso_devdesant(self, dbRow_movto):
            """ Se aplica el saldo para los egresos.

            @param dbRow_movto: dbRow del movimiento.
            """

            _D_registro = self._D_registros[dbRow_movto.cfdi_ingreso_id]
            _dbRow_relacionado = _D_registro.referenciasRelacionados[dbRow_movto.cfdi_relacionado_id]

            if dbRow_movto.accion == TCFDI_MOVIMIENTOS.E_ACCION.CANCELACION:
                # Si es un movimiento de cancelación, se busca el movimiento inicial y se cambian los importes

                _dbRow_movtoInicial = dbc01.tcfdi_movimientos(dbRow_movto.cfdi_movimiento_emision_id)
                dbRow_movto.cargo = _dbRow_movtoInicial.abono
                dbRow_movto.abono = 0
                # Este es el saldo del CFDI de egreso
                self._D_registros[dbRow_movto.cfdi_id].saldo += dbRow_movto.cargo

            else:
                if dbRow_movto.cfdi_id not in self._D_registros:
                    # Se utiliza el mismo control de registros para egresos, con el fin de controlar saldos, etc.

                    _dbRow_cfdi = _D_registro.referencias[dbRow_movto.cfdi_id]
                    self._D_registros[dbRow_movto.cfdi_id] = Storage()
                    self._D_registros[dbRow_movto.cfdi_id].dbRow = _dbRow_cfdi
                    self._D_registros[dbRow_movto.cfdi_id].saldo = _dbRow_cfdi.total

                else:
                    pass

                # Si el importe pudo ser determinado en la primer etapa,
                #  debido a que la referencia aplica a un solo ingreso, se aplica
                if dbRow_movto.importe > 0:
                    dbRow_movto.abono = dbRow_movto.importe
                    self._D_registros[dbRow_movto.cfdi_id].saldo -= dbRow_movto.abono
                elif self._D_registros[dbRow_movto.cfdi_id].saldo <= 0:
                    # Si el saldo en el CFDI de egreso es cero o menor que cero, se registra el warning

                    _dbRow_cfdi = _D_registro.referencias[dbRow_movto.cfdi_id]

                    if not (_dbRow_cfdi.sat_status == TCFDIS.SAT_STATUS.CANCELADO):
                        self._D_multireturn.agrega_advertencia(
                            "CFDI %d de Egreso tiene más referencias que saldo disponible" % dbRow_movto.cfdi_id
                            )
                    else:
                        pass

                elif (self._E_proceso == self.E_PROCESO.CFDI) or _dbRow_relacionado.fijarimporte:

                    # Si el proceso en ejecución es el cálculo del saldo del CFDI,
                    #  se usa el importe en el de egreso como el importe aplicado
                    dbRow_movto.abono = _dbRow_relacionado.importe

                    # Este es el saldo del CFDI de egreso
                    self._D_registros[dbRow_movto.cfdi_id].saldo -= dbRow_movto.abono

                else:

                    # Se determina el saldo efectivo de la factura:
                    _f_saldoIngresoCalculado = \
                        _D_registro.cargos \
                        - _D_registro.abonos \
                        + _D_registro.variablescargos \
                        - _D_registro.variablesabonos

                    if _f_saldoIngresoCalculado == 0:
                        # Si el saldo ingreso calculado es igual a 0, no mas abonos deben hacerse
                        pass

                    elif self._D_registros[dbRow_movto.cfdi_id].saldo > _f_saldoIngresoCalculado:
                        dbRow_movto.abono = _f_saldoIngresoCalculado

                    elif self._D_registros[dbRow_movto.cfdi_id].saldo > 0:
                        dbRow_movto.abono = self._D_registros[dbRow_movto.cfdi_id].saldo

                    else:
                        # Si el saldo de la factura es negativo, no se aplica
                        pass

                    # Este es el saldo del CFDI de egreso
                    self._D_registros[dbRow_movto.cfdi_id].saldo -= dbRow_movto.abono

            if dbRow_movto.abono != _dbRow_relacionado.importe:
                _dbRow_relacionado.importe = dbRow_movto.abono
                _dbRow_relacionado.update_record()
            else:
                pass

            return

        def procesar_cfdi(self, x_cfdi, x_cfdi_manejoSaldo = None):
            """ Procesa y en caso de no existir genera los movimientos relacionados al CFDI de ingreso.

            @param x_cfdi:
            @type x_cfdi:
            @param x_cfdi_manejoSaldo: Si es definido es prioridad para el movimiento del estado de cuenta,
             en vez de cfdi
            @type x_cfdi_manejoSaldo:
            @return:
            @rtype:
            """

            _D_return = FUNC_RETURN(
                b_movtoActualizo = False
                )

            self._E_proceso = self.E_PROCESO.CFDI
            self._dt_procesoInicio = datetime.datetime.now()

            self._dbRow_cliente = dbc01.tempresa_clientes(self._n_cliente_id)

            self._dbRow_cliente.procesostatus = TEMPRESA_CLIENTES.E_PROCESOESTATUS.CFDIS_PROCESANDO
            self._dbRow_cliente.update_record()
            dbc01.commit()

            try:
                _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi, dbc01.tcfdis)
                self._n_cfdi_id = _D_results.n_id
                self._dbRow_cfdi = _D_results.dbRow

                _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_manejoSaldo, dbc01.tcfdi_manejosaldos)
                _n_cfdiManejoSaldo_id = _D_results.n_id
                _dbRow_cfdiManejoSaldo = _D_results.dbRow

                if _n_cfdiManejoSaldo_id and not _dbRow_cfdiManejoSaldo:
                    self._D_debug.agrega_mensaje("Manejo de Saldo no detectado")
                    # Si no existe el registro, significa que fue eliminado, por lo que se eliminan sus movimientos
                    _n_rows = dbc01(
                        (dbc01.tcfdi_movimientos.cliente_id == self._n_cliente_id)
                        & (dbc01.tcfdi_movimientos.cfdi_id == self._n_cfdi_id)
                        & (dbc01.tcfdi_movimientos.cfdi_manejosaldo_id == _n_cfdiManejoSaldo_id)
                        ).delete()

                elif not self._dbRow_cfdi:
                    self._D_debug.agrega_mensaje("CFDI no detectado")
                    # Si no existe el registro, significa que fue eliminado, por lo que se eliminan sus movimientos
                    _n_rows = dbc01(
                        (dbc01.tcfdi_movimientos.cliente_id == self._n_cliente_id)
                        & (dbc01.tcfdi_movimientos.cfdi_id == self._n_cfdi_id)
                        ).delete()

                elif (self._dbRow_cfdi.sat_status == TCFDIS.SAT_STATUS.CANCELADO) \
                        and not self._dbRow_cfdi.sat_fecha_cancelacion:

                    _D_return.agrega_error("CFDI cancelado y no tiene fecha de cancelación. "
                                           + "No se puede calcular edo cta.")

                else:
                    self._D_debug.agrega_mensaje("CFDI detectado, buscando movtos posteriores")
                    # Se busca en los CFDIs relacionados si existen movimientos posteriores a la fecha del egreso/pago,
                    # si es así, no puede calcularse el saldo con este método.
                    if self._dbRow_cfdi.sat_status == TCFDIS.SAT_STATUS.CANCELADO:
                        # Si el CFDI se encuentra cancelado, se usa la fecha de cancelación
                        # No debe poder ser cancelado, si tiene un manejosaldo activo
                        _dt_fechaMovimiento = self._dbRow_cfdi.sat_fecha_cancelacion

                    elif _dbRow_cfdiManejoSaldo:
                        _dt_fechaMovimiento = _dbRow_cfdiManejoSaldo.fecha

                    elif self._dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.PAGO:
                        # Si es un CFDI de pago, se usa la fecha del primer pago
                        _dbRowPagos = dbc01(
                            dbc01.tcfdi_complementopagos.cfdi_id == self._dbRow_cfdi.id
                            ).select(
                            dbc01.tcfdi_complementopagos.id,
                            dbc01.tcfdi_complementopagos.fechapago,
                            orderby = [dbc01.tcfdi_complementopagos.fechapago]
                            )
                        if _dbRowPagos:
                            _dt_fechaMovimiento = _dbRowPagos.first().fechapago
                        else:
                            _dt_fechaMovimiento = self._dbRow_cfdi.fecha

                    else:
                        # En cualquier otro caso se usa la fecha de emisión del CFDI
                        _dt_fechaMovimiento = self._dbRow_cfdi.fecha

                    _D_result = None
                    if _dbRow_cfdiManejoSaldo:
                        self._D_debug.entra_proceso("Comienza el manejo saldo")
                        _D_result = self._procesar_cfdi_ingreso_manejoSaldo(
                            _n_cfdiManejoSaldo_id, _dbRow_cfdiManejoSaldo
                            )
                        _D_return.combinar(_D_result)

                    elif self._dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO:
                        self._D_debug.entra_proceso("Comienza a evaluar ingreso")
                        _D_result = self._procesar_cfdi_ingreso()
                        _D_return.combinar(_D_result)

                    elif self._dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.EGRESO:
                        self._D_debug.entra_proceso("Comienza a evaluar egreso")
                        _D_result = self._procesar_cfdi_egreso()
                        _D_return.combinar(_D_result)

                    elif self._dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.PAGO:
                        self._D_debug.entra_proceso("Comienza a evaluar pago")
                        _D_result = self._procesar_cfdi_pago()
                        _D_return.combinar(_D_result)

                    elif self._dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.NOMINA:
                        # No soportado aún e ignorado
                        pass

                    elif self._dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.TRASPASO:
                        # No soportado aún e ignorado
                        pass

                    else:
                        raise RuntimeError("Tipo comprobante en CFDI no reconocido.")

                    if _D_result and _D_result.b_movtoActualizo:

                        self._D_debug.entra_proceso("Se actualiza el movimiento")

                        _L_cfdiIngresosAfectados = TCFDIS.OBTENER_CFDISINGRESOS_AFECTADOS(self._dbRow_cfdi)

                        self._D_debug.agrega_mensaje("Se buscan los cfdi relacionados")
                        # Se buscan los movimientos posteriores que pudieran hacer conflicto con el cálculo del edo cta
                        _dbRows_relacionados = dbc01(
                            (dbc01.tcfdi_relacionados.cfdirelacionado_id.belongs(_L_cfdiIngresosAfectados))
                            & (dbc01.tcfdis.id == dbc01.tcfdi_relacionados.cfdi_id)
                            # Es muy probable que esta condición no sea necesaria
                            # & (dbc01.tcfdis.sat_status == TCFDIS.SAT_STATUS.VIGENTE)
                            & (dbc01.tcfdis.fecha > _dt_fechaMovimiento)
                            ).select(
                                dbc01.tcfdi_relacionados.ALL
                                )

                        if _dbRows_relacionados:
                            _D_return.agrega_advertencia(
                                "CFDI no puede generar estado de cuenta debido a que se detectaron "
                                + "egresos en cfdis relacionados posteriores. "
                                + "Requiere recalcular saldo del cliente."
                                )

                        else:

                            self._D_debug.agrega_mensaje("Se buscan los cfdi relacionados cancelados")
                            _dbRows_relacionados_cancelados = dbc01(
                                (dbc01.tcfdi_relacionados.cfdirelacionado_id.belongs(_L_cfdiIngresosAfectados))
                                & (dbc01.tcfdis.id == dbc01.tcfdi_relacionados.cfdi_id)
                                & (dbc01.tcfdis.sat_status == TCFDIS.SAT_STATUS.CANCELADO)
                                & (dbc01.tcfdis.sat_fecha_cancelacion > _dt_fechaMovimiento)
                                ).select(
                                    dbc01.tcfdi_relacionados.ALL
                                    )

                            if _dbRows_relacionados_cancelados:
                                _D_return.agrega_advertencia(
                                    "CFDI no puede generar estado de cuenta debido a que se detectaron "
                                    + "egresos cancelados en cfdis relacionados posteriores. "
                                    + "Requiere recalcular saldo del cliente."
                                    )

                            else:

                                self._D_debug.agrega_mensaje("Se buscan los cfdi pagos")
                                _dbRows_pagosRelacionados = dbc01(
                                    (dbc01.tcfdi_complementopago_doctosrelacionados.cfdirelacionado_id.belongs(
                                        _L_cfdiIngresosAfectados))
                                    & (dbc01.tcfdi_complementopagos.id
                                       == dbc01.tcfdi_complementopago_doctosrelacionados.cfdi_complementopago_id)
                                    & (dbc01.tcfdi_complementopagos.fechapago > _dt_fechaMovimiento)
                                    & (dbc01.tcfdis.id == dbc01.tcfdi_complementopagos.cfdi_id)
                                    # & (dbc01.tcfdis.sat_status == TCFDIS.SAT_STATUS.VIGENTE)
                                    ).select(
                                        dbc01.tcfdis.ALL
                                        )

                                if _dbRows_pagosRelacionados:
                                    _D_return.agrega_advertencia(
                                        "CFDI no puede generar estado de cuenta debido a que se detectaron "
                                        + "pagos en cfdis relacionados posteriores. "
                                        + "Requiere recalcular saldo del cliente."
                                        )

                                else:

                                    self._D_debug.agrega_mensaje("Se buscan los cfdi pagos cancelados")
                                    _dbRows_pagosRelacionados_cancelados = dbc01(
                                        (dbc01.tcfdi_complementopago_doctosrelacionados.cfdirelacionado_id.belongs(
                                            _L_cfdiIngresosAfectados))
                                        & (dbc01.tcfdi_complementopagos.id
                                           == dbc01.tcfdi_complementopago_doctosrelacionados.cfdi_complementopago_id)
                                        & (dbc01.tcfdis.id == dbc01.tcfdi_complementopagos.cfdi_id)
                                        & (dbc01.tcfdis.sat_status == TCFDIS.SAT_STATUS.CANCELADO)
                                        & (dbc01.tcfdis.sat_fecha_cancelacion > _dt_fechaMovimiento)
                                        ).select(
                                            dbc01.tcfdis.ALL
                                            )

                                    if _dbRows_pagosRelacionados_cancelados:
                                        _D_return.agrega_advertencia(
                                            "CFDI no puede generar estado de cuenta debido a que se detectaron "
                                            + "pagos cancelados en cfdis relacionados posteriores. "
                                            + "Requiere recalcular saldo del cliente."
                                            )

                                    else:
                                        pass
                    else:  # Else de: _D_result and _D_result.b_movtoActualizo
                        pass
            except Exception as _O_excepcion:
                _D_return.agrega(
                    stvfwk2_e_RETURN.NOK_LOGIN,
                    u'Proceso: %s, Excepción: %s %s %s'
                    % ("Estado Cuenta ", str(_O_excepcion.__doc__), str(_O_excepcion.args),
                    self._D_debug.leer_mensaje())
                    )

            finally:
                self._dbRow_cliente.procesostatus = TEMPRESA_CLIENTES.E_PROCESOESTATUS.SIN_PROCESO
                self._dbRow_cliente.update_record()

                _D_result = TEMPRESA_CLIENTES.OBTENER_SALDO_CLIENTE(
                    x_cliente_id = self._dbRow_cliente,
                    x_cfdi_id_ignorar = None,
                    )

                self._E_proceso = self.E_PROCESO.NINGUNO

            return _D_return

        def _procesar_cfdi_ingreso(self):
            """ Identifica y actualiza/crea los movimientos

            Esta rutina supone que la información del cliente esta actualizada y sobre esa base contabiliza,
            modificando únicamente los registros impactado por el CFDI

            """

            _D_return = FUNC_RETURN(
                L_movtos = [],
                b_movtoActualizo = False
                )

            _D_movtosIngresos = Storage()

            # Se usa un diccionario para un manejo similar a como se usan los movimientos en los pagos y egresos
            _D_movtosIngresos[self._n_cfdi_id] = Storage(
                dbRowMovtoEmision = None,
                n_cfdi_movimiento_emision_id = None,
                dbRowMovtoCancelacion = None,
                dbRow_cfdi_ingreso = self._dbRow_cfdi,
                )

            self._D_debug.agrega_mensaje("Mapean movimientos relacionados al cfdi de ingreso")
            # Se mapean los pagos con los movimientos, identificando si es emision o cancelacion
            _dbRows_movtos = dbc01(
                (dbc01.tcfdi_movimientos.cfdi_id == self._dbRow_cfdi.id)
                & dbc01.tcfdi_movimientos.tipodocumento.belongs([TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.INGRESO])
                ).select(
                    dbc01.tcfdi_movimientos.ALL,
                    orderby = dbc01.tcfdi_movimientos.id
                    )

            self._D_debug.agrega_mensaje("Se buscan los movimientos y se engloban en un diccionario")
            # Se buscan los movimientos y se engloban en un diccionario;
            #  solo puede existir un movto emision y uno de cancelación
            for _dbRow_movto in _dbRows_movtos:
                if _dbRow_movto.cfdi_id in _D_movtosIngresos:
                    _D_movtoIngreso = _D_movtosIngresos[_dbRow_movto.cfdi_id]
                    if (_dbRow_movto.accion in (TCFDI_MOVIMIENTOS.E_ACCION.EMISION,
                            TCFDI_MOVIMIENTOS.E_ACCION.NO_TIMBRADA)) \
                            and not _D_movtoIngreso.dbRowMovtoEmision:
                        _D_movtoIngreso.dbRowMovtoEmision = _dbRow_movto
                        _D_movtoIngreso.n_cfdi_movimiento_emision_id = _dbRow_movto.id

                    elif (_dbRow_movto.accion == TCFDI_MOVIMIENTOS.E_ACCION.CANCELACION) \
                            and not _D_movtoIngreso.dbRowMovtoCancelacion:

                        # Si el CFDI no esta cancelado, se cubre abajo el ajuste de saldo y movimientos
                        _D_movtoIngreso.dbRowMovtoCancelacion = _dbRow_movto

                    else:
                        _D_return.agrega_error(
                            "Error al verificar movimientos, existen varias emisiones/cancelaciones "
                            + "para el mismo CFDI. Favor de correr ajuste de saldos para el cliente."
                            )
                        break
                else:
                    # No debería poder entrar a este else, ya que la consulta es directa
                    #  y el diccionario apuntan al mismo cfdi
                    _D_return.agrega_error("CFDI no reconocido. Favor de correr ajuste de saldos del cliente.")
                    break

            self._D_debug.agrega_mensaje("Se mueve por el diccionario de movimiento para determinar "
                                         + "las acciones de ajuste")
            # Se mueve por el diccionario de movimiento para determinar las acciones de ajuste de saldos
            #  o creacion de movimientos
            for _D_movtoIngreso in _D_movtosIngresos.values():
                _f_importe = _D_movtoIngreso.dbRow_cfdi_ingreso.total

                # Se inserta el movimiento de ingreso

                self._D_debug.agrega_mensaje("Si no tiene movimiento de emision")

                self._D_debug.agrega_mensaje("Se intenta agregar el movimiento")
                _D_result = self._addMovimiento(
                    x_cfdi_ingreso = _D_movtoIngreso.dbRow_cfdi_ingreso,
                    dbRow_cfdi = self._dbRow_cfdi,  # Para efector practicos es el mismo de ingreso

                    dt_fecha = _D_movtoIngreso.dbRow_cfdi_ingreso.fecha,
                    E_accion = TCFDI_MOVIMIENTOS.E_ACCION.EMISION,
                    E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.INGRESO,
                    s_descripcion = "Factura",

                    f_importe = _f_importe,
                    f_cargo = _f_importe,
                    f_abono = _f_importe
                    if _D_movtoIngreso.dbRow_cfdi_ingreso.metodopago_id == TMETODOSPAGO.UNAEXHIBICION
                    else 0,
                    f_saldoPrevio = 0,  # Saldo se actualiza en el movimiento
                    s_monedacontpaqi_id = _D_movtoIngreso.dbRow_cfdi_ingreso.monedacontpaqi_id,
                    f_tipocambio = _D_movtoIngreso.dbRow_cfdi_ingreso.tipocambio,

                    n_cfdi_movtoActualizar_emision = _D_movtoIngreso.dbRowMovtoEmision.id
                    if _D_movtoIngreso.dbRowMovtoEmision else None
                    )
                _D_movtoIngreso.dbRow_cfdi_ingreso.saldo = _D_result.f_saldoNuevo
                _D_movtoIngreso.dbRow_cfdi_ingreso.update_record()

                _D_movtoIngreso.n_cfdi_movimiento_emision_id = _D_result.n_cfdi_movimiento_id
                _D_return.b_movtoActualizo = True

                if self._dbRow_cfdi.sat_status == TCFDIS.SAT_STATUS.CANCELADO:

                    # Se inserta el movimiento de cancelacion del pago

                    _D_results = self._addMovimiento(
                        x_cfdi_ingreso = _D_movtoIngreso.dbRow_cfdi_ingreso,
                        dbRow_cfdi = self._dbRow_cfdi,  # Para efector practicos es el mismo

                        dt_fecha = self._dbRow_cfdi.sat_fecha_cancelacion,
                        E_accion = TCFDI_MOVIMIENTOS.E_ACCION.CANCELACION,
                        E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.INGRESO,
                        s_descripcion = "Cancelación Factura",

                        n_cfdi_movimiento_emision_id = _D_movtoIngreso.n_cfdi_movimiento_emision_id,

                        f_importe = _f_importe,
                        f_cargo = 0,
                        f_abono = _f_importe,
                        f_saldoPrevio = _D_movtoIngreso.dbRow_cfdi_ingreso.saldo,
                        s_monedacontpaqi_id = _D_movtoIngreso.dbRow_cfdi_ingreso.monedacontpaqi_id,
                        f_tipocambio = _D_movtoIngreso.dbRow_cfdi_ingreso.tipocambio,

                        n_cfdi_movtoActualizar_cancelacion = _D_movtoIngreso.dbRowMovtoCancelacion.id
                        if _D_movtoIngreso.dbRowMovtoCancelacion else None
                        )
                    _D_movtoIngreso.dbRow_cfdi_ingreso.saldo -= _D_results.f_saldoNuevo
                    _D_movtoIngreso.dbRow_cfdi_ingreso.update_record()

                    _D_return.b_movtoActualizo = True

                elif _D_movtoIngreso.dbRowMovtoCancelacion:
                    # Si no esta cancelado el cfdi, y tiene movimiento de cancelación

                    if _D_movtoIngreso.dbRow_cfdi_ingreso.uuid:
                        # Si esta timbrado se actualiza el saldo

                        # Se actualiza el saldo del CFDI
                        # Nuevo saldo = Viejo Saldo + (Viejo Pago - Nuevo Pago)
                        _D_movtoIngreso.dbRow_cfdi_ingreso.saldo += _D_movtoIngreso.dbRowMovtoCancelacion.importe
                        _D_movtoIngreso.dbRow_cfdi_ingreso.update_record()
                    else:
                        pass

                    _D_movtoIngreso.dbRowMovtoCancelacion.delete_record()

                    _D_return.b_movtoActualizo = True

                else:
                    # No esta cancelado el CFDI del pago, con la ejecución de los vigentes es suficiente
                    pass

            return _D_return

        def _procesar_cfdi_pago(self):
            """ Identifica y actualiza/crea los movimientos

            Esta rutina supone que la información del cliente esta actualizada y sobre esa base contabiliza,
            modificando únicamente los registros impactado por el CFDI

            """

            _D_return = FUNC_RETURN(
                L_movtos = [],
                b_movtoActualizo = False
                )

            _D_movtosPagos = Storage()

            # Se obtienen todos los complementos de pago y sus docs relacionados del CFDI
            _dbRows_compPagos = dbc01(
                (dbc01.tcfdi_complementopagos.cfdi_id == self._dbRow_cfdi.id)
                & (dbc01.tcfdi_complementopago_doctosrelacionados.cfdi_complementopago_id
                   == dbc01.tcfdi_complementopagos.id)
                ).select(
                    dbc01.tcfdi_complementopagos.ALL,
                    dbc01.tcfdi_complementopago_doctosrelacionados.ALL
                    )

            # Se crea un diccionario con los complementos de pago
            for _dbRow_compPago in _dbRows_compPagos:
                if _dbRow_compPago.tcfdi_complementopago_doctosrelacionados.cfdirelacionado_id:
                    _D_movtosPagos[_dbRow_compPago.tcfdi_complementopago_doctosrelacionados.id] = Storage(
                        dbRowMovtoEmision = None,
                        n_cfdi_movimiento_emision_id = None,
                        dbRowMovtoCancelacion = None,
                        dbRow_cfdi_ingreso = None,
                        dbRow_cfdi_complementopago_doctorelacionado =
                        _dbRow_compPago.tcfdi_complementopago_doctosrelacionados,
                        dbRow_cfdi_complementopago = _dbRow_compPago.tcfdi_complementopagos,
                        )
                else:
                    _D_return.agrega_advertencia("No se puede generar estado de cuenta del pago, "
                                                 + "ya que no se tiene referencia al cfdi ingreso")
                    pass

            # Se mapean los pagos con los movimientos, identificando si es emision o cancelacion
            _dbRows_movtos = dbc01(
                dbc01.tcfdi_movimientos.cfdi_id == self._dbRow_cfdi
                ).select(
                    dbc01.tcfdi_movimientos.ALL,
                    orderby = dbc01.tcfdi_movimientos.id
                    )

            # Se navega por los registros de movtos para identificar si se tiene el movto de emisión y/o cancelación
            for _dbRow_movto in _dbRows_movtos:
                if _dbRow_movto.cfdi_complementopago_doctorelacionado_id in _D_movtosPagos:
                    _D_movtoPago = _D_movtosPagos[_dbRow_movto.cfdi_complementopago_doctorelacionado_id]
                    if (_dbRow_movto.accion in (TCFDI_MOVIMIENTOS.E_ACCION.EMISION,
                            TCFDI_MOVIMIENTOS.E_ACCION.NO_TIMBRADA)) \
                            and not _D_movtoPago.dbRowMovtoEmision:
                        _D_movtoPago.dbRowMovtoEmision = _dbRow_movto
                        _D_movtoPago.n_cfdi_movimiento_emision_id = _dbRow_movto.id

                    elif (_dbRow_movto.accion == TCFDI_MOVIMIENTOS.E_ACCION.CANCELACION) \
                            and not _D_movtoPago.dbRowMovtoCancelacion:

                        # Si el CFDI no esta cancelado, se cubre abajo el ajuste de saldo y movimientos
                        _D_movtoPago.dbRowMovtoCancelacion = _dbRow_movto

                    else:
                        _D_return.agrega_error(
                            "Error al verificar movimientos, existen varias emisiones/cancelaciones "
                            + "para el mismo CFDI. Favor de correr ajuste de saldos para el cliente."
                            )
                        break

                elif not _dbRow_movto.cfdi_ingreso_id:
                    # Si no existe CFDI de ingreso relacionado, no hay manera de resolver el saldo del movimiento
                    _dbRow_movto.delete_record()

                else:
                    # El movimiento no existe más y debe ser eliminado y el saldo del documento referenciado ajustado
                    _dbRow_cfdiIngreso = dbc01.tcfdis(_dbRow_movto.cfdi_ingreso_id)

                    self._D_debug.agrega_mensaje("Actualizando saldo de ingreso")

                    # Nuevo saldo = Viejo Saldo + (Viejo Pago - Nuevo Pago)
                    if _dbRow_movto.accion == TCFDI_MOVIMIENTOS.E_ACCION.EMISION:
                        # La emisión resto el saldo, como ya no existe, sube el saldo
                        _dbRow_cfdiIngreso.saldo += _dbRow_movto.importe

                    elif _dbRow_movto.accion == TCFDI_MOVIMIENTOS.E_ACCION.CANCELACION:
                        # La emisión resto el saldo, la cancelación subió el saldo, como y ano existe la cancelación,
                        #  se reduce el saldo
                        _dbRow_cfdiIngreso.saldo -= _dbRow_movto.importe

                    else:
                        _D_return.agrega_error("Accion en movimiento no identificada. "
                                               + "Favor de correr ajuste de saldos del cliente.")
                        break

                    _dbRow_cfdiIngreso.update_record()
                    _dbRow_movto.delete_record()

            # Se mueve por el diccionario de movimiento para determinar las acciones de ajuste de saldos
            #  o creacion de movimientos
            for _D_movtoPago in _D_movtosPagos.values():
                _f_importePagado = _D_movtoPago.dbRow_cfdi_complementopago_doctorelacionado.imppagado

                self._D_debug.agrega_mensaje("Creando movto pago emisión")

                _D_movtoPago.dbRow_cfdi_ingreso = dbc01.tcfdis(
                    _D_movtoPago.dbRow_cfdi_complementopago_doctorelacionado.cfdirelacionado_id
                    )
                _D_movtoPago.dbRow_cfdi_ingreso.saldo = _D_movtoPago.dbRow_cfdi_ingreso.saldo or 0

                _D_result = self._addMovimiento(
                    x_cfdi_ingreso = _D_movtoPago.dbRow_cfdi_ingreso,
                    dbRow_cfdi = self._dbRow_cfdi,

                    dt_fecha = _D_movtoPago.dbRow_cfdi_complementopago.fechapago,
                    E_accion = TCFDI_MOVIMIENTOS.E_ACCION.EMISION,
                    E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.PAGO,
                    s_descripcion = "Pago",
                    n_cfdi_complementopago_doctorelacionado_id =
                    _D_movtoPago.dbRow_cfdi_complementopago_doctorelacionado.id,

                    dbRow_relacionado = _D_movtoPago.dbRow_cfdi_complementopago_doctorelacionado,

                    f_importe = _f_importePagado,
                    f_cargo = 0,
                    f_abono = _f_importePagado,
                    f_saldoPrevio = _D_movtoPago.dbRow_cfdi_ingreso.saldo,
                    s_monedacontpaqi_id = _D_movtoPago.dbRow_cfdi_complementopago.monedacontpaqi_id,
                    f_tipocambio = _D_movtoPago.dbRow_cfdi_complementopago.tipocambio,

                    n_cfdi_movtoActualizar_emision = _D_movtoPago.dbRowMovtoEmision.id
                    if _D_movtoPago.dbRowMovtoEmision else None
                    )
                _D_movtoPago.dbRow_cfdi_ingreso.saldo -= _D_result.f_saldoNuevo
                _D_movtoPago.dbRow_cfdi_ingreso.update_record()

                _D_movtoPago.n_cfdi_movimiento_emision_id = _D_result.n_cfdi_movimiento_id
                _D_return.b_movtoActualizo = True

                if self._dbRow_cfdi.sat_status == TCFDIS.SAT_STATUS.CANCELADO:

                    # Se inserta el movimiento de cancelacion del pago

                    self._D_debug.agrega_mensaje("Creando movto pago cancelación")

                    if not _D_movtoPago.dbRow_cfdi_ingreso:
                        _D_movtoPago.dbRow_cfdi_ingreso = dbc01.tcfdis(
                            _D_movtoPago.dbRow_cfdi_complementopago_doctorelacionado.cfdirelacionado_id
                            )
                        _D_movtoPago.dbRow_cfdi_ingreso.saldo = _D_movtoPago.dbRow_cfdi_ingreso.saldo or 0
                    else:
                        pass

                    _D_results = self._addMovimiento(
                        x_cfdi_ingreso = _D_movtoPago.dbRow_cfdi_ingreso,
                        dbRow_cfdi = self._dbRow_cfdi,

                        dt_fecha = self._dbRow_cfdi.sat_fecha_cancelacion,
                        E_accion = TCFDI_MOVIMIENTOS.E_ACCION.CANCELACION,
                        E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.PAGO,
                        s_descripcion = "Cancelación Pago",
                        n_cfdi_complementopago_doctorelacionado_id =
                        _D_movtoPago.dbRow_cfdi_complementopago_doctorelacionado.id,
                        n_cfdi_movimiento_emision_id = _D_movtoPago.n_cfdi_movimiento_emision_id,

                        dbRow_relacionado = _D_movtoPago.dbRow_cfdi_complementopago_doctorelacionado,

                        f_importe = _f_importePagado,
                        f_cargo = _f_importePagado,
                        f_abono = 0,
                        f_saldoPrevio = _D_movtoPago.dbRow_cfdi_ingreso.saldo,
                        s_monedacontpaqi_id = _D_movtoPago.dbRow_cfdi_complementopago.monedacontpaqi_id,
                        f_tipocambio = _D_movtoPago.dbRow_cfdi_complementopago.tipocambio,

                        n_cfdi_movtoActualizar_cancelacion = _D_movtoPago.dbRowMovtoCancelacion.id
                        if _D_movtoPago.dbRowMovtoCancelacion else None
                        )

                    _D_movtoPago.dbRow_cfdi_ingreso.saldo += _D_results.f_saldoNuevo
                    _D_movtoPago.dbRow_cfdi_ingreso.update_record()

                    _D_return.b_movtoActualizo = True

                elif _D_movtoPago.dbRowMovtoCancelacion:
                    # Si no esta cancelado el cfdi, y tiene movimiento de cancelación

                    self._D_debug.agrega_mensaje("CFDI no esta cancelado, pero tiene movto cancelación")

                    # Se actualiza el saldo del CFDI
                    if not _D_movtoPago.dbRow_cfdi_ingreso:
                        _D_movtoPago.dbRow_cfdi_ingreso = dbc01.tcfdis(
                            _D_movtoPago.dbRow_cfdi_complementopago_doctorelacionado.cfdirelacionado_id
                            )
                        _D_movtoPago.dbRow_cfdi_ingreso.saldo = _D_movtoPago.dbRow_cfdi_ingreso.saldo or 0
                    else:
                        pass

                    if _D_movtoIngreso.dbRow_cfdi_ingreso.uuid:
                        # Si esta timbrado se actualiza el saldo

                        # Nuevo saldo = Viejo Saldo + (Viejo Pago - Nuevo Pago)
                        _D_movtoPago.dbRow_cfdi_ingreso.saldo -= _D_movtoPago.dbRowMovtoCancelacion.importe
                        _D_movtoPago.dbRow_cfdi_ingreso.update_record()
                    else:
                        pass

                    _D_movtoPago.dbRowMovtoCancelacion.delete_record()
                    _D_return.b_movtoActualizo = True

                else:
                    # No está cancelado el CFDI del pago, con la ejecusión de los vigentes es suficiente
                    pass

            return _D_return

        def _procesar_cfdi_egreso(self):
            """ Identifica y actualiza/crea los movimientos

            Esta rutina supone que la información del cliente esta actualizada y sobre esa base contabiliza,
            modificando únicamente los registros impactado por el CFDI

            """

            _D_return = FUNC_RETURN(
                L_movtos = [],
                b_movtoActualizo = False
                )

            _D_movtosEgresos = Storage()

            self._D_debug.agrega_mensaje("Query de doc rels egreso %s" % str(self._dbRow_cfdi.id))

            # Se obtienen los docs relacionados
            _dbRows_egresos = dbc01(
                (dbc01.tcfdi_relacionados.cfdi_id == self._dbRow_cfdi.id)
                ).select(
                    dbc01.tcfdi_relacionados.ALL,
                    )

            # Se crea un diccionario con los complementos de pago
            for _dbRow_egreso in _dbRows_egresos:
                if _dbRow_egreso.cfdirelacionado_id:
                    self._D_debug.agrega_mensaje("Dict de docs relacionados")
                    _D_movtosEgresos[_dbRow_egreso.id] = Storage(
                        dbRowMovtoEmision = None,
                        n_cfdi_movimiento_emision_id = None,
                        dbRowMovtoCancelacion = None,
                        dbRow_cfdi_ingreso = None,
                        dbRow_cfdi_relacionado = _dbRow_egreso,
                        )
                else:
                    # No se puede crear estado de cuenta de un movimiento que no tiene relacionado identificado
                    pass

            self._D_debug.agrega_mensaje("Query de movtos egreso")

            # Se mapean los pagos con los movimientos, identificando si es emision o cancelacion
            _dbRows_movtos = dbc01(
                dbc01.tcfdi_movimientos.cfdi_id == self._dbRow_cfdi.id
                ).select(
                    dbc01.tcfdi_movimientos.ALL,
                    orderby = dbc01.tcfdi_movimientos.id
                    )

            for _dbRow_movto in _dbRows_movtos:
                self._D_debug.agrega_mensaje("Revisando movtos de egreso")
                if _dbRow_movto.cfdi_relacionado_id in _D_movtosEgresos:
                    _D_movtoEgreso = _D_movtosEgresos[_dbRow_movto.cfdi_relacionado_id]
                    if (_dbRow_movto.accion in (TCFDI_MOVIMIENTOS.E_ACCION.EMISION,
                            TCFDI_MOVIMIENTOS.E_ACCION.NO_TIMBRADA)) \
                            and not _D_movtoEgreso.dbRowMovtoEmision:
                        _D_movtoEgreso.dbRowMovtoEmision = _dbRow_movto
                        _D_movtoEgreso.n_cfdi_movimiento_emision_id = _dbRow_movto.id
                    elif (_dbRow_movto.accion == TCFDI_MOVIMIENTOS.E_ACCION.CANCELACION) \
                            and not _D_movtoEgreso.dbRowMovtoCancelacion:

                        # Si el CFDI no esta cancelado, se cubre abajo el ajuste de saldo y movimientos
                        _D_movtoEgreso.dbRowMovtoCancelacion = _dbRow_movto

                    else:
                        _D_return.agrega_error(
                            "Error al verificar movimientos, existen varias emisiones/cancelaciones "
                            + "para el mismo CFDI. Favor de correr ajuste de saldos para el cliente."
                            )
                        break

                elif not _dbRow_movto.cfdi_ingreso_id:
                    # Si no tiene relacionado el movimiento, solo se elimina ya que no se puede actualizar el
                    #  saldo del ingreso
                    self._D_debug.agrega_mensaje("No se encontro movto ingreso, se elimina movto edo cta")
                    _dbRow_movto.delete_record()

                else:
                    # El movimiento no existe más y debe ser eliminado y el saldo del documento referenciado ajustado
                    _dbRow_cfdiIngreso = dbc01.tcfdis(_dbRow_movto.cfdi_ingreso_id)

                    self._D_debug.agrega_mensaje("Actualizando saldo de ingreso")

                    # Nuevo saldo = Viejo Saldo + (Viejo Pago - Nuevo Pago)
                    if _dbRow_movto.accion == TCFDI_MOVIMIENTOS.E_ACCION.EMISION:
                        # La emisión resto el saldo, como ya no existe, sube el saldo
                        _dbRow_cfdiIngreso.saldo += _dbRow_movto.importe

                    elif _dbRow_movto.accion == TCFDI_MOVIMIENTOS.E_ACCION.CANCELACION:
                        # La emisión resto el saldo, la cancelación subió el saldo, como y ano existe la cancelación,
                        #  se reduce el saldo
                        _dbRow_cfdiIngreso.saldo -= _dbRow_movto.importe

                    else:
                        _D_return.agrega_error(
                            "Accion en movimiento no identificada. Favor de correr ajuste de saldos del cliente."
                            )
                        break

                    _dbRow_cfdiIngreso.update_record()
                    _dbRow_movto.delete_record()

            self._D_debug.agrega_mensaje("Limpiando movtos egreso")

            _f_saldoEgreso = self._dbRow_cfdi.total
            # Se mueve por el diccionario de movimiento para determinar las acciones de ajuste de saldos
            #  o creacion de movimientos
            for _D_movtoEgreso in _D_movtosEgresos.values():

                self._D_debug.agrega_mensaje("Analizando movimientos de egreso")

                if _D_movtoEgreso.dbRow_cfdi_relacionado.fijarimporte:
                    # Si el movimiento tiene definido fijarimporte, usa la cantidad en el importe
                    _f_importeEgreso = _D_movtoEgreso.dbRow_cfdi_relacionado.importe

                elif _D_movtoEgreso.dbRow_cfdi_relacionado.importe > 0:
                    # Si el movimiento ya tiene un importe definido lo usa
                    _f_importeEgreso = _D_movtoEgreso.dbRow_cfdi_relacionado.importe

                else:
                    # Si no hay manera de saber el importe, se deja nulo para su manejo en caso de tener movimientos
                    _f_importeEgreso = 0

                _f_importeEgresoCancelacion = 0  # Se define el importe de cancelación en caso de existir

                self._D_debug.agrega_mensaje(
                    "Creando movto egreso emisión en CFDI Ingreso %s"
                    % str(_D_movtoEgreso.dbRow_cfdi_relacionado.cfdirelacionado_id)
                    )

                _D_movtoEgreso.dbRow_cfdi_ingreso = dbc01.tcfdis(
                    _D_movtoEgreso.dbRow_cfdi_relacionado.cfdirelacionado_id
                    )

                _D_movtoEgreso.dbRow_cfdi_ingreso.saldo = _D_movtoEgreso.dbRow_cfdi_ingreso.saldo or 0

                if not _f_importeEgreso:
                    # Si no hay manera de saber el importe, usa el saldo del cfdi relacionado
                    #  topado por el saldo del egreso
                    if _f_saldoEgreso >= _D_movtoEgreso.dbRow_cfdi_ingreso.saldo:
                        _f_importeEgreso = _D_movtoEgreso.dbRow_cfdi_ingreso.saldo
                    else:
                        _f_importeEgreso = _f_saldoEgreso

                    _D_movtoEgreso.dbRow_cfdi_relacionado.importe = _f_importeEgreso
                    _D_movtoEgreso.dbRow_cfdi_relacionado.update_record()
                else:
                    pass

                _D_result = self._addMovimiento(
                    x_cfdi_ingreso = _D_movtoEgreso.dbRow_cfdi_ingreso,
                    dbRow_cfdi = self._dbRow_cfdi,

                    dt_fecha = self._dbRow_cfdi.fecha,
                    E_accion = TCFDI_MOVIMIENTOS.E_ACCION.EMISION,
                    E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DESCUENTO,
                    s_descripcion = "Descuento",
                    n_cfdi_relacionado_id = _D_movtoEgreso.dbRow_cfdi_relacionado.id,

                    dbRow_relacionado = _D_movtoEgreso.dbRow_cfdi_relacionado,

                    f_importe = _f_importeEgreso,
                    f_cargo = 0,
                    f_abono = _f_importeEgreso,
                    f_saldoPrevio = _D_movtoEgreso.dbRow_cfdi_ingreso.saldo,
                    s_monedacontpaqi_id = self._dbRow_cfdi.monedacontpaqi_id,
                    f_tipocambio = self._dbRow_cfdi.tipocambio,

                    n_cfdi_movtoActualizar_emision = _D_movtoEgreso.dbRowMovtoEmision.id
                    if _D_movtoEgreso.dbRowMovtoEmision else None
                    )

                _D_movtoEgreso.dbRow_cfdi_ingreso.saldo -= _D_result.f_saldoNuevo
                _D_movtoEgreso.dbRow_cfdi_ingreso.update_record()

                _D_movtoEgreso.n_cfdi_movimiento_emision_id = _D_result.n_cfdi_movimiento_id
                _D_return.b_movtoActualizo = True

                if self._dbRow_cfdi.sat_status == TCFDIS.SAT_STATUS.CANCELADO:

                    _f_importeEgresoCancelacion = _f_importeEgreso

                    # Se inserta el movimiento de cancelacion del pago

                    self._D_debug.agrega_mensaje("Creando movto egreso cancelación")

                    if not _D_movtoEgreso.dbRow_cfdi_ingreso:
                        _D_movtoEgreso.dbRow_cfdi_ingreso = dbc01.tcfdis(
                            _D_movtoEgreso.dbRow_cfdi_relacionado.cfdirelacionado_id
                            )
                        _D_movtoEgreso.dbRow_cfdi_ingreso.saldo = _D_movtoEgreso.dbRow_cfdi_ingreso.saldo or 0
                    else:
                        pass

                    _D_results = self._addMovimiento(
                        x_cfdi_ingreso = _D_movtoEgreso.dbRow_cfdi_ingreso,
                        dbRow_cfdi = self._dbRow_cfd,

                        dt_fecha = self._dbRow_cfdi.sat_fecha_cancelacion,
                        E_accion = TCFDI_MOVIMIENTOS.E_ACCION.CANCELACION,
                        E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DESCUENTO,
                        s_descripcion = "Cancelación Descuento",
                        n_cfdi_relacionado_id = _D_movtoEgreso.dbRow_cfdi_relacionado.id,
                        n_cfdi_movimiento_emision_id = _D_movtoEgreso.n_cfdi_movimiento_emision_id,

                        dbRow_relacionado = _D_movtoEgreso.dbRow_cfdi_relacionado,

                        f_importe = _f_importeEgresoCancelacion,
                        f_cargo = _f_importeEgresoCancelacion,
                        f_abono = 0,
                        f_saldoPrevio = _D_movtoEgreso.dbRow_cfdi_ingreso.saldo,
                        s_monedacontpaqi_id = self._dbRow_cfdi.monedacontpaqi_id,
                        f_tipocambio = self._dbRow_cfdi.tipocambio,

                        n_cfdi_movtoActualizar_cancelacion = _D_movtoEgreso.dbRowMovtoCancelacion.id
                        if _D_movtoEgreso.dbRowMovtoCancelacion else None
                        )
                    _D_movtoEgreso.dbRow_cfdi_ingreso.saldo += _D_results.f_saldoNuevo
                    _D_movtoEgreso.dbRow_cfdi_ingreso.update_record()

                    _D_return.b_movtoActualizo = True

                elif _D_movtoEgreso.dbRowMovtoCancelacion:
                    # Si no esta cancelado el cfdi, y tiene movimiento de cancelacion

                    self._D_debug.agrega_mensaje("CFDI no esta cancelado, pero tiene movto cancelación")

                    # Se actualiza el saldo del CFDI
                    if not _D_movtoEgreso.dbRow_cfdi_ingreso:
                        _D_movtoEgreso.dbRow_cfdi_ingreso = dbc01.tcfdis(
                            _D_movtoEgreso.dbRow_cfdi_relacionado.cfdirelacionado_id
                            )
                        _D_movtoEgreso.dbRow_cfdi_ingreso.saldo = _D_movtoEgreso.dbRow_cfdi_ingreso.saldo or 0
                    else:
                        pass
                    
                    if _D_movtoIngreso.dbRow_cfdi_ingreso.uuid:
                        # Si esta timbrado se actualiza el saldo

                        # Nuevo saldo = Viejo Saldo + (Viejo Pago - Nuevo Pago)
                        _D_movtoEgreso.dbRow_cfdi_ingreso.saldo -= _D_movtoEgreso.dbRowMovtoCancelacion.importe
                        _D_movtoEgreso.dbRow_cfdi_ingreso.update_record()
                    else:
                        pass

                    _D_movtoEgreso.dbRowMovtoCancelacion.delete_record()

                else:
                    # No esta cancelado el CFDI del pago, con la ejecucción de los vigentes es suficiente
                    pass

                _f_saldoEgreso -= _f_importeEgreso + _f_importeEgresoCancelacion

            self._D_debug.agrega_mensaje("Errores egreso")

            if _f_saldoEgreso < 0:
                _D_return.agrega_error(
                    "CFDI de egreso fue mal calculado. Favor de correr ajuste de saldos del cliente."
                    )

            elif (_f_saldoEgreso > 0) and (self._dbRow_cfdi.sat_status != TCFDIS.SAT_STATUS.CANCELADO):
                _D_return.agrega_advertencia("CFDI de egreso no uso todo su saldo. Favor de verificar.")

            elif (_f_saldoEgreso != self._dbRow_cfdi.total) \
                    and (self._dbRow_cfdi.sat_status == TCFDIS.SAT_STATUS.CANCELADO):
                _D_return.agrega_advertencia("CFDI de egreso no fue calculado correctamente. "
                                             + "Favor de correr ajuste de saldos del cliente.")
            else:
                pass

            return _D_return

        def _procesar_cfdi_ingreso_manejoSaldo(self, n_id, dbRow):
            """ Identifica y actualiza/crea los movimientos

            Esta rutina supone que la información del cliente esta actualizada y sobre esa base contabiliza,
            modificando únicamente los registros impactado por el CFDI

            """
            _ = n_id
            _D_return = FUNC_RETURN(
                L_movtos = [],
                b_movtoActualizo = False
                )

            _D_movtosManejoSaldo = Storage()

            # Se crea un diccionario con los elementos para el estado de cuenta
            _D_movtosManejoSaldo[dbRow.id] = Storage(
                dbRowMovtoTraspaso = None,
                dbRowMovtoAplicacion = None,
                dbRow_cfdi_ingresoTraspaso = None,
                dbRow_cfdi_ingresoAplicacion = None,
                dbRow_cfdi_relacionado = dbRow,
                )

            # Se mapean los manejos de saldos con los movimientos, identificando si es emision o cancelacion
            _dbRows_movtos = dbc01(
                (dbc01.tcfdi_movimientos.cfdi_id == self._dbRow_cfdi.id)
                & (
                    dbc01.tcfdi_movimientos.tipodocumento.belongs(
                        TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.TRASPASO_SALDO_A_FAVOR,
                        TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.APLICACION_SALDO_A_FAVOR,
                        )
                    )
                ).select(
                    dbc01.tcfdi_movimientos.ALL,
                    orderby = dbc01.tcfdi_movimientos.id
                    )

            for _dbRow_movto in _dbRows_movtos:
                if _dbRow_movto.cfdi_manejosaldo_id in _D_movtosManejoSaldo:
                    _D_movtoManejoSaldo = _D_movtosManejoSaldo[_dbRow_movto.cfdi_manejosaldo_id]
                    if (_dbRow_movto.accion == TCFDI_MOVIMIENTOS.E_ACCION.EMISION) \
                            and (_dbRow_movto.tipodocumento
                                 == TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.TRASPASO_SALDO_A_FAVOR) \
                            and not _D_movtoManejoSaldo.dbRowMovtoTraspaso:
                        _D_movtoManejoSaldo.dbRowMovtoTraspaso = _dbRow_movto

                    elif (_dbRow_movto.accion == TCFDI_MOVIMIENTOS.E_ACCION.EMISION) \
                            and (_dbRow_movto.tipodocumento
                                 == TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.APLICACION_SALDO_A_FAVOR) \
                            and not _D_movtoManejoSaldo.dbRowMovtoAplicacion:

                        # Si el CFDI no esta cancelado, se cubre abajo el ajuste de saldo y movimientos
                        _D_movtoManejoSaldo.dbRowMovtoAplicacion = _dbRow_movto

                    else:
                        _D_return.agrega_error(
                            "Error al verificar movimientos, existen varios traspasos/aplicaciones "
                            + "para el mismo CFDI. Favor de correr ajuste de saldos para el cliente."
                            )
                        break
                else:
                    # El movimiento no existe más y debe ser eliminado y el saldo del documento referenciado ajustado
                    _dbRow_cfdiIngreso = dbc01.tcfdis(_dbRow_movto.cfdi_ingreso_id)
                    # Nuevo saldo = Saldo - importe
                    if _dbRow_movto.tipodocumento == TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.TRASPASO_SALDO_A_FAVOR:
                        # Ya que no encuentra el montoSaldo inicial, se hace rollback del cargo
                        _dbRow_cfdiIngreso.saldo -= _dbRow_movto.importe

                    elif _dbRow_movto.tipodocumento == TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.APLICACION_SALDO_A_FAVOR:
                        # Ya que no encuentra el montoSaldo inicial, se hace rollback del abono
                        _dbRow_cfdiIngreso.saldo += _dbRow_movto.importe

                    else:
                        _D_return.agrega_error(
                            "Accion en movimiento no identificada. Favor de correr ajuste de saldos del cliente."
                            )
                        break

                    _dbRow_cfdiIngreso.update_record()
                    _dbRow_movto.delete_record()

            # Se mueve por el diccionario de movimiento para determinar las acciones de ajuste de saldos
            #  o creacion de movimientos
            for _D_movtoManejoSaldo in _D_movtosManejoSaldo.values():

                _f_importeManejoSaldo = _D_movtoManejoSaldo.dbRow_cfdi_relacionado.importe or 0

                # Se inserta o modifica el movimiento de traspaso

                # El movimiento de traspado corresponde al CFDI del registro de ManejoSaldo
                _D_movtoManejoSaldo.dbRow_cfdi_ingresoTraspaso = dbc01.tcfdis(
                    _D_movtoManejoSaldo.dbRow_cfdi_relacionado.cfdi_id
                    )

                _D_result = self._addMovimiento(
                    x_cfdi_ingreso = _D_movtoManejoSaldo.dbRow_cfdi_ingresoTraspaso,
                    dbRow_cfdi = self._dbRow_cfdi,

                    dt_fecha = _D_movtoManejoSaldo.dbRow_cfdi_relacionado.fecha,
                    E_accion = TCFDI_MOVIMIENTOS.E_ACCION.EMISION,
                    E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.TRASPASO_SALDO_A_FAVOR,
                    s_descripcion = "Traspaso",
                    n_cfdi_relacionado_id = None,
                    n_cfdi_manejosaldo_id = _D_movtoManejoSaldo.dbRow_cfdi_relacionado.id,

                    f_importe = _f_importeManejoSaldo,
                    f_cargo = _f_importeManejoSaldo,
                    f_abono = 0,
                    f_saldoPrevio = _D_movtoManejoSaldo.dbRow_cfdi_ingresoTraspaso.saldo,
                    s_monedacontpaqi_id = _D_movtoManejoSaldo.dbRow_cfdi_ingresoTraspaso.monedacontpaqi_id,
                    f_tipocambio = _D_movtoManejoSaldo.dbRow_cfdi_ingresoTraspaso.tipocambio,

                    n_cfdi_movtoActualizar_emision = _D_movtoManejoSaldo.dbRowMovtoTraspaso.id
                    if _D_movtoManejoSaldo.dbRowMovtoTraspaso else None
                    )

                _D_movtoManejoSaldo.dbRow_cfdi_ingresoTraspaso.saldo += _D_result.f_saldoNuevo
                _D_movtoManejoSaldo.dbRow_cfdi_ingresoTraspaso.update_record()

                # Se inserta o modifica el movimiento de aplicacion de saldo del pago

                # Se cambia, el nuevo CFDI de ingreso debe ser el referenciado en el registro de Manejo Saldo
                _D_movtoManejoSaldo.dbRow_cfdi_ingresoAplicacion = dbc01.tcfdis(
                    _D_movtoManejoSaldo.dbRow_cfdi_relacionado.cfdirelacionado_id
                    )

                _D_results = self._addMovimiento(
                    x_cfdi_ingreso = _D_movtoManejoSaldo.dbRow_cfdi_ingresoAplicacion,
                    dbRow_cfdi = self._dbRow_cfdi,

                    dt_fecha = _D_movtoManejoSaldo.dbRow_cfdi_relacionado.fecha,
                    E_accion = TCFDI_MOVIMIENTOS.E_ACCION.EMISION,
                    E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.APLICACION_SALDO_A_FAVOR,
                    s_descripcion = "Aplicación",
                    n_cfdi_relacionado_id = None,
                    n_cfdi_manejosaldo_id = _D_movtoManejoSaldo.dbRow_cfdi_relacionado.id,
                    n_cfdi_movimiento_emision_id = _D_movtoManejoSaldo.n_cfdi_movimiento_emision_id,

                    f_importe = _f_importeManejoSaldo,
                    f_cargo = 0,
                    f_abono = _f_importeManejoSaldo,
                    f_saldoPrevio = _D_movtoManejoSaldo.dbRow_cfdi_ingresoAplicacion.saldo,
                    s_monedacontpaqi_id = _D_movtoManejoSaldo.dbRow_cfdi_ingresoAplicacion.monedacontpaqi_id,
                    f_tipocambio = _D_movtoManejoSaldo.dbRow_cfdi_ingresoAplicacion.tipocambio,

                    n_cfdi_movtoActualizar_emision = _D_movtoManejoSaldo.dbRowMovtoAplicacion.id
                    if _D_movtoManejoSaldo.dbRowMovtoAplicacion else None
                    )
                _D_movtoManejoSaldo.dbRow_cfdi_ingresoAplicacion.saldo -= _D_results.f_saldoNuevo
                _D_movtoManejoSaldo.dbRow_cfdi_ingresoAplicacion.update_record()

                _D_return.b_movtoActualizo = True

            return _D_return

        pass  # EDOCTA

    pass  # TCFDIS


dbc01.define_table(
    TCFDIS.S_NOMBRETABLA, *TCFDIS.L_DBCAMPOS,
    format = lambda row: '%(serie)s %(folio)s [%(tipodecomprobante)s]' % (row.as_dict()),
    singular = 'CFDI',
    plural = 'CFDIs',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TCFDIS
    )


class TCFDIS2(Table):
    """ Tabla soporte de CFDIS con el contenido parcial de campos extendidos de TCFDIS.

    """

    S_NOMBRETABLA = 'tcfdis2'

    L_DBCAMPOS = [
        Field(
            'cfdi_id', dbc01.tcfdis, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'CFDI', comment = None,
            writable = False, readable = False,
            represent = None
            ),

        Field(
            'interesmoratoriopagare', 'decimal(8,2)', default = 5,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputPercentage, label = 'Interés moratorio',
            comment = 'Interés moratorio mensualusado en pagaré.',
            writable = False, readable = True,
            represent = stv_represent_percentage
            ),

        # TODO Pendientes de revisión
        Field(
            'ordencompra', 'string', length = 10, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Orden compra', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'foliointernopedido', 'string', length = 10, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Folio interno pedido', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'agentevendedor', 'string', length = 100, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Agente vendedor', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'montobaseprodiva0', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Monto base prod. IVA 0', comment = None,
            writable = False, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        Field(
            'montobaseprodiva', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Monto base prod. IVA', comment = None,
            writable = False, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        Field(
            'emisorcalle', 'string', length = 100, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Emisor calle', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'emisornumexterior', 'string', length = 55, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Emisor Num. Exterior', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'emisornuminterior', 'string', length = 55, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Emisor Num. Interior', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'emisorcolonia', 'string', length = 120, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Emisor Colonia', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'emisorlocalidad', 'string', length = 120, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Emisor Localidad', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'emisormunicipio', 'string', length = 120, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Emisor Municipio', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'emisorestado', 'string', length = 30, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Emisor Estado', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'emisorpais', 'string', length = 30, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Emisor Pais', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'emisorcodigopostal', 'string', length = 30, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Emisor CP', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),

        Field(
            'receptoremails', 'string', length = 150, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Email(s)', comment = 'Emails separados por commas',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'receptortelefonos', 'string', length = 150, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Tel(s)', comment = 'Telefonos separados por |',
            writable = True, readable = True,
            represent = stv_represent_string
            ),

        Field(
            'receptorcalle', 'string', length = 100, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Receptor calle', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'receptorreferencia', 'string', length = 100, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Receptor referencia', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'receptornumexterior', 'string', length = 55, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Receptor Num. Exterior', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'receptornuminterior', 'string', length = 55, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Receptor Num. Interior', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'receptorcolonia', 'string', length = 120, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Receptor Colonia', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'receptorlocalidad', 'string', length = 120, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Receptor Localidad', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'receptormunicipio', 'string', length = 120, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Receptor Municipio', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'receptorestado', 'string', length = 30, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Receptor Estado', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'receptorpais', 'string', length = 30, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Receptor Pais', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'receptorcodigopostal', 'string', length = 30, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Receptor CP', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),

        # Información del timbre digital
        Field(
            'certificado', 'text', length = 3000, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_text, label = 'Certificado', comment = None,
            writable = False, readable = True,
            represent = stv_represent_text
            ),
        Field(
            'nocertificado', 'string', length = 50, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'No. Certificado', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'versiontimbre', 'string', length = 50, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Versión Certificado', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'rfcprovcertif', 'string', length = 50, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'RFC Proveedor Certificado', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'sellocfd', 'text', length = 3000, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_text, label = 'Sello CFD', comment = None,
            writable = False, readable = True,
            represent = stv_represent_text
            ),
        Field(
            'sellosat', 'text', length = 3000, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_text, label = 'Sello SAT', comment = None,
            writable = False, readable = True,
            represent = stv_represent_text
            ),
        Field(
            'nocertificadosat', 'string', length = 50, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'No. Certificado SAT', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),

        ]

    def __init__(self, dbRef, tablename, *fields, **args):
        Table.__init__(self, dbRef, tablename, *fields, **args)
        # Se agrega la referencia a la tabla con info adicional
        TCFDIS.PROC.dbTabla2 = self

        return

    pass  # TCFDIS2


dbc01.define_table(
    TCFDIS2.S_NOMBRETABLA, *TCFDIS2.L_DBCAMPOS,
    format = lambda row: '%(serie)s %(folio)s [%(tipodecomprobante)s]' % (row.as_dict()),
    singular = 'CFDI soporte',
    plural = 'CFDIs Soporte',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TCFDIS2
    )


class TCFDI_XMLS(Table):

    S_NOMBRETABLA = 'tcfdi_xmls'
    L_DBCAMPOS = [
        Field(
            'cfdi_id', 'integer', default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'CFDI', comment = None,
            writable = False, readable = False,
            represent = None
            ),
        Field(
            'xml_cfdi', 'text', default = None,
            required = False,
            notnull = False,
            label = T('XML'), comment = "XML del CFDI",
            writable = False, readable = True,
            represent = stv_represent_xml
            ),
        Field(
            'json_cfdi', 'text', default = None,
            required = False,
            notnull = False,
            label = T('Json'), comment = "Json del CFDI",
            writable = False, readable = True,
            represent = stv_represent_json
            ),
        tSignature_dbc02
        ]

    def __init__(self, dbRef, tablename, *fields, **args):
        Table.__init__(self, dbRef, tablename, *fields, **args)
        self.PROC.dbTabla = self
        return

    class PROC(TCFDIS.PROC_DETALLE, object):

        dbTabla = None  # Se escribe con la inicialización

        def __init__(self, **D_defaults):
            """ Inicializa objecto de tipo PROC para control del registro actual

            @param D_defaults:
            @type D_defaults:
            """
            super(TCFDI_XMLS.PROC, self).__init__(**D_defaults)
            return

        @classmethod
        def CONFIGURACAMPOS_DESDE_dbRow(cls, dbRow_base):
            # cls.dbTabla.cfdi_id.default = dbRow_base.cfdi_id
            # cls.dbTabla.xml_cfdi.default = dbRow_base.xml_cfdi
            # cls.json_cfdi.default = dbRow_base.json_cfdi
            return

        def configuracampos_nuevo(
                self,
                s_cfdi_xml_id,
                b_llamadaDesdeEditar = False,
                **D_defaults
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param s_cfdi_xml_id:
            @type s_cfdi_xml_id:
            @param b_llamadaDesdeEditar: Indica si la llamada a esta función es desde otra capa,
             desactivando validación de puede_crear
            @type b_llamadaDesdeEditar:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = super(TCFDI_XMLS.PROC, self).configuracampos_nuevo(
                s_cfdi_detalle_id = s_cfdi_xml_id,
                b_llamadaDesdeEditar = b_llamadaDesdeEditar,
                **D_defaults
                )
            _D_return.combinar(_D_results)

            self.dbTabla.cfdi_id.writable = False
            self.dbTabla.xml_cfdi.writable = False
            self.dbTabla.json_cfdi.writable = False

            # Campos que no deben ser editados, solo de forma autocalculada
            self.dbTabla.xml_cfdi.writable = False
            self.dbTabla.json_cfdi.writable = False

            # Campos que todos los movimientos pueden editar
            self.dbTabla.notas.writable = True

            # Se definen los valores por default obligados
            self.dbTabla.cfdi_id.default = self._D_defaults.cfdi_id

            return _D_return

    pass


dbc02.define_table(
    TCFDI_XMLS.S_NOMBRETABLA, *TCFDI_XMLS.L_DBCAMPOS,
    format = '%(cfdi_id)s',
    singular = 'CFDI XML',
    plural = 'CFDI XMLs',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TCFDI_XMLS
    )


class TCFDI_RELACIONADOS(Table):

    S_NOMBRETABLA = 'tcfdi_relacionados'
    L_DBCAMPOS = [
        Field(
            'cfdi_id', dbc01.tcfdis, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'CFDI', comment = None,
            writable = False, readable = False,
            represent = None
            ),
        Field(
            'uuid', 'string', length = 50, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'CFDI Relacionado UUID', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'cfdirelacionado_id', 'integer', default = None,  # Corresponde a tcfdi relacionado
            required = False,
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_search(
                f,
                v,
                s_display = '%(serie)s %(folio)s',
                s_url = URL(a = 'app_timbrado', c = '250cfdi', f = 'cfdi_buscar'),
                D_additionalAttributes = {'reference': dbc01.tcfdis.id}
                ),
            label = 'CFDI Relacionado', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(
                v, r, dbc01.tcfdi_relacionados.cfdirelacionado_id
                )
            ),

        Field(
            'fijarimporte', 'boolean', default = False,
            required = False,
            notnull = False,
            widget = stv_widget_inputCheckbox, label = 'Fijar Importe', comment = None,
            writable = True, readable = True,
            represent = stv_represent_boolean
            ),
        Field(
            'importe', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Importe', comment = 'Especifica el importe aplicado',
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        tSignature_dbc01,
        ]

    def __init__(self, dbRef, tablename, *fields, **args):
        Table.__init__(self, dbRef, tablename, *fields, **args)
        self.PROC.dbTabla = self
        return

    @staticmethod
    def ASOCIAR_UUID(s_cfdi_relacionado_id):
        """ Se busca y asocian ligas rotas en a CFDIs relacionados
        """

        _D_return = FUNC_RETURN(
            L_msgError = [],
            E_return = CLASS_e_RETURN.NOK_ERROR,
            n_updatedRows = 0
            )

        # Ejemplo puede usar
        # tcfdi_relacionados.id = 8419 de tcfdis.id = 47696
        # tcfdis.id = 47762
        _dbTabla = dbc01.tcfdi_relacionados

        _dbTableCFDIsBuscar = dbc01.tcfdis.with_alias('cfdisbuscar')
        _dbRows_cfdiRelacionados = dbc01(
            (dbc01.tcfdi_relacionados.id == s_cfdi_relacionado_id)
            & (dbc01.tcfdis.id == dbc01.tcfdi_relacionados.cfdi_id)
            & (_dbTableCFDIsBuscar.uuid == dbc01.tcfdi_relacionados.uuid)
            & (_dbTableCFDIsBuscar.empresa_id == dbc01.tcfdis.empresa_id)
            ).select(
            dbc01.tcfdi_relacionados.ALL,
            _dbTableCFDIsBuscar.id,
            )

        if len(_dbRows_cfdiRelacionados) > 1:
            # Error, no debería de regresar mas de un registro
            _D_return.L_msgError += [
                "Se encontraron más de un registro relacionado, contacte al administrador %s" % s_cfdi_relacionado_id]
            _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
        elif len(_dbRows_cfdiRelacionados) == 0:
            # CFDI asociado con el UUID no se encontró
            _D_return.L_msgError += ["No se encontraron cfdis relacionados"]
            _D_return.E_return = CLASS_e_RETURN.OK
        else:
            # Si tiene solamente un registro, hay que actualizarlo
            _dbRow = _dbRows_cfdiRelacionados.last()
            _D_return.n_updatedRows += dbc01(dbc01.tcfdi_relacionados.id == _dbRow.tcfdi_relacionados.id).update(
                cfdirelacionado_id = _dbRow.cfdisbuscar.id
                )
            _D_return.L_msgError += ["Se actualizo el regisro con la relación al CFDI"]
            _D_return.E_return = CLASS_e_RETURN.OK

        return _D_return

    @staticmethod
    def CONFIGURAR_INTEGRIDAD_REFERENCIAL():
        _dbTabla = dbc01.tcfdi_relacionados
        _dbTabla.cfdirelacionado_id.type = 'reference tcfdis'
        _dbTabla.cfdirelacionado_id.ondelete = 'NO ACTION'
        _dbTabla.cfdirelacionado_id.requires = IS_IN_DB(
            dbc01,
            'tcfdis.id',
            dbc01.tcfdis._format
            )

        return None

    class PROC(TCFDIS.PROC_DETALLE, object):

        dbTabla = None  # Se escribe con la inicialización

        def puede_crear(self, x_cfdi, **D_defaults):
            """ Determina si puede o no crear un CFDI de traslado.

            Actualmente no hay condición requerida.
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi, dbc01.tcfdis)
            _dbRow_cfdi = _D_results.dbRow

            _D_results = super(TCFDI_RELACIONADOS.PROC, self).puede_crear(
                x_cfdi = _dbRow_cfdi,
                D_requeridosMaestro = Storage(
                    tiporelacion_id = Storage(
                        L_valores = [],  # Una lista vacía significa cualquier valor
                        s_msgError = "Tipo Relación es requerido"
                        )
                    ),
                **D_defaults
                )
            _D_return.combinar(_D_results)

            return _D_return

        @classmethod
        def CONFIGURACAMPOS_DESDE_dbRow(cls, dbRow_base):
            # cls.dbTabla.cfdi_id.default = dbRow_base.cfdi_id
            # cls.dbTabla.uuid.default = dbRow_base.uuid
            # cls.dbTabla.cfdirelacionado_id.default = dbRow_base.cfdirelacionado_id
            # cls.dbTabla.fijarimporte.default = dbRow_base.fijarimporte
            # cls.dbTabla.importe.default = dbRow_base.importe
            return

        def configuracampos_nuevo(self, s_cfdi_relacion_id, b_llamadaDesdeEditar = False, **D_defaults):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param s_cfdi_relacion_id:
            @type s_cfdi_relacion_id:
            @param b_llamadaDesdeEditar:
            @type b_llamadaDesdeEditar:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = super(TCFDI_RELACIONADOS.PROC, self).configuracampos_nuevo(
                s_cfdi_detalle_id = s_cfdi_relacion_id,
                b_llamadaDesdeEditar = b_llamadaDesdeEditar,
                **D_defaults
                )
            _D_return.combinar(_D_results)

            self.dbTabla.cfdi_id.writable = False
            self.dbTabla.cfdirelacionado_id.writable = True
            self.dbTabla.fijarimporte.writable = True
            self.dbTabla.importe.writable = True

            # Campos que no deben ser editados, solo de forma autocalculada
            self.dbTabla.uuid.writable = False

            # Campos que todos los movimientos pueden editar
            self.dbTabla.notas.writable = False

            # Se definen los valores por default
            self.dbTabla.cfdi_id.default = self._D_defaults.cfdi_id
            # self.dbTabla.cfdirelacionado_id.default = None
            # self.dbTabla.fijarimporte.default = True
            # self.dbTabla.importe.default = 0

            return _D_return

        def configuracampos_edicion(
                self,
                x_cfdi_relacionado,
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param x_cfdi_relacionado:
            @type x_cfdi_relacionado:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_relacionado, self.dbTabla)
            _dbRow = _D_results.dbRow

            _dbRow_cfdi = dbc01(dbc01.tcfdis.id == _dbRow.cfdi_id)\
                .select(dbc01.tcfdis.id, dbc01.tcfdis.generado_por).first()

            if _dbRow_cfdi.generado_por == TCFDIS.E_GENERADO_POR.MANUAL:
                # Si el CFDI fue generado manual, no se puede editar
                _D_results = super(TCFDI_RELACIONADOS.PROC, self).configuracampos_edicion(
                    x_cfdi_detalle = _dbRow
                    )
                _D_return.combinar(_D_results)

            else:
                # Si fue importado por gestionador o a través del XML, se permite editar solo
                #  el importe fijo y la cantidad a aplicar

                for _s_fieldName in self.dbTabla.fields:
                    self.dbTabla[_s_fieldName].writable = False

                self.dbTabla.fijarimporte.writable = True
                self.dbTabla.importe.writable = True

            # No se permite cambiar el CFDI relacionado, si se desea cambiarlo debe eliminarse el registro
            self.dbTabla.cfdirelacionado_id.writable = False

            return _D_return

        def al_validar(
                self,
                O_form,
                dbRow = None,
                D_camposAChecar = None,
                ):
            """ Validar la información para poder grabar cfdi

            @param D_camposAChecar:
            @type D_camposAChecar:
            @param dbRow:
            @type dbRow:
            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            dbRow = dbRow if dbRow else self.dbTabla(O_form.record_id) if O_form.record_id else None
            D_camposAChecar = D_camposAChecar if D_camposAChecar \
                else STV_FWK_LIB.OBTENER_CAMPOSACHECAR_PARA_VALIDAR(self.dbTabla, dbRow)

            # Validaciones

            # Se llaman las validaciones del CFDI_relacionado, indicando que tiporelacion_id debe existsir
            O_form = super(TCFDI_RELACIONADOS.PROC, self).al_validar(
                O_form, dbRow, D_camposAChecar = D_camposAChecar,
                )

            if not O_form.record_id:
                # Si esta creando el registro:

                _dbRow_cfdiRelacionado = dbc01.tcfdis(D_camposAChecar.cfdirelacionado_id)
                _dbRow_cfdi = dbc01.tcfdis(D_camposAChecar.cfdi_id)

                if not _dbRow_cfdiRelacionado:
                    O_form.errors.cfdirelacionado_id = "CFDI relacionado debe estar definido"

                elif not _dbRow_cfdi.tiporelacion_id:
                    O_form.errors.id = "Tipo relación en CFDI es requerido"

                elif _dbRow_cfdiRelacionado.receptorcliente_id != _dbRow_cfdi.receptorcliente_id:
                    O_form.errors.id = "CFDI relacionado debe pertenecer al mismo cliente"

                else:
                    pass

            else:
                # Si esta editando el registro

                if self.dbTabla.cfdirelacionado_id.writable \
                        and (dbRow.cfdirelacionado_id != D_camposAChecar.cfdirelacionado_id):
                    O_form.errors.cfdirelacionado_id = "CFDI relacionado no debe poder ser modificado"

                else:
                    pass

            # Validaciones

            return O_form

        @classmethod
        def AL_ACEPTAR(
                cls,
                O_form,
                dbRow = None,
                ):
            """ Al aceptar el registro deben hacerse los cálculos

            Se hacen todos los cálculos y aceptación de los cambios en el producto

            @param dbRow:
            @type dbRow:
            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()
            dbRow = dbRow if dbRow else cls.dbTabla(O_form.record_id or O_form.vars.id)

            if not O_form.record_id:
                # Si se inserta
                pass
            else:  # Si se edita
                pass

            O_form = super(TCFDI_RELACIONADOS.PROC, cls).AL_ACEPTAR(O_form, dbRow)

            # En el aceptar de la clase padre, se manda llamar a calcular CFDI

            generaMensajeFlash(cls.dbTabla, _D_return)
            return O_form

        @classmethod
        def MAPEAR_CAMPOS(
                cls,
                x_cfdi_relacionado,
                **D_params
                ):
            """ Recalcula campos, haciendo la consulta de la carta porte a partir del CFDI

            @param x_cfdi_relacionado:
            @type x_cfdi_relacionado:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_relacionado, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage(
                uuid = "",
                fijarimporte = _dbRow.fijarimporte,
                importe = _dbRow.importe
                )

            _dbRow_cfdiRelacionado = dbc01.tcfdis(_dbRow.cfdirelacionado_id)
            _D_actualizar.uuid = _dbRow_cfdiRelacionado.uuid if _dbRow_cfdiRelacionado else ""

            if not _D_actualizar.fijarimporte and not _D_actualizar.importe:

                _dbRow_cfdi = dbc01.tcfdis(_dbRow.cfdi_id)

                if (_dbRow_cfdi.sat_status == TCFDIS.SAT_STATUS.NO_DEFINIDO) \
                        and (_dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.EGRESO) \
                        and (
                            _dbRow_cfdi.tiporelacion_id in (TTIPOSRELACIONES.NOTACREDITO, TTIPOSRELACIONES.NOTADEBITO)
                            ):

                    _dbField_sumaMontosEgreso = dbc01.tcfdi_relacionados.importe.sum()
                    _dbRows_cfdi_egreso = dbc01(
                        (dbc01.tcfdi_relacionados.cfdi_id == _dbRow.cfdi_id)
                        ).select(
                            _dbField_sumaMontosEgreso,
                            )

                    _dbRow_cfdi_egreso = _dbRows_cfdi_egreso.first()
                    _f_montoDisponible = _dbRow_cfdi.total - (_dbRow_cfdi_egreso[_dbField_sumaMontosEgreso] or 0)

                    _D_actualizar.importe = _dbRow_cfdiRelacionado.saldo \
                        if _f_montoDisponible > _dbRow_cfdiRelacionado.saldo else _f_montoDisponible

                    _D_actualizar.fijarimporte = True

                else:
                    pass

            else:
                pass

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)
                    # El mapear campos, no implica que se tenga que recalcular el CFDI
                    break
                else:
                    pass

            return _D_return

        pass  # TRASLADO

    class TRASLADO(PROC, object):

        def puede_crear(self, x_cfdi, **D_defaults):
            """ Determina si puede o no crear un CFDI de traslado.

            Actualmente no hay condición requerida.
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi, dbc01.tcfdis)
            _dbRow_cfdi = _D_results.dbRow

            _D_results = super(TCFDI_RELACIONADOS.TRASLADO, self).puede_crear(
                x_cfdi = _dbRow_cfdi,
                D_requeridosMaestro = Storage(
                    tiporelacion_id = Storage(
                        L_valores = [TTIPOSRELACIONES.TRASLADOFACTPREV, TTIPOSRELACIONES.DEVOLUCION],
                        s_msgError = "Tipo Relación por traslado o devolución son los únicos posibles en traslados"
                        )
                    ),
                **D_defaults
                )
            _D_return.combinar(_D_results)

            return _D_return

        def configuracampos_nuevo(
                self,
                s_cfdi_relacion_id,
                b_llamadaDesdeEditar = False,
                **D_defaults
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param s_cfdi_relacion_id:
            @type s_cfdi_relacion_id:
            @param b_llamadaDesdeEditar:
            @type b_llamadaDesdeEditar:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = super(TCFDI_RELACIONADOS.TRASLADO, self).configuracampos_nuevo(
                s_cfdi_detalle_id = s_cfdi_relacion_id,
                b_llamadaDesdeEditar = b_llamadaDesdeEditar,
                **D_defaults
                )
            _D_return.combinar(_D_results)

            self.dbTabla.cfdi_id.writable = False
            self.dbTabla.cfdirelacionado_id.writable = True
            self.dbTabla.fijarimporte.writable = False  # Para traslado no se usa
            self.dbTabla.importe.writable = False  # Para traslado no se usa

            # Campos que no deben ser editados, solo de forma autocalculada
            self.dbTabla.uuid.writable = False

            # Campos que todos los movimientos pueden editar
            self.dbTabla.notas.writable = False

            # Se definen los valores por default
            self.dbTabla.cfdi_id.default = self._D_defaults.cfdi_id
            # self.dbTabla.cfdirelacionado_id.default = None
            # self.dbTabla.fijarimporte.default = True
            # self.dbTabla.importe.default = 0

            return _D_return

        def al_validar(
                self,
                O_form,
                **D_params
                ):
            """ Validar la información para poder grabar cfdi

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _dbRow = self.dbTabla(O_form.record_id) if O_form.record_id else None
            _D_camposAChecar = STV_FWK_LIB.OBTENER_CAMPOSACHECAR_PARA_VALIDAR(self.dbTabla, _dbRow)

            # Validaciones

            # Se llaman las validaciones del CFDI_relacionado, indicando que tiporelacion_id debe existsir
            O_form = super(TCFDI_RELACIONADOS.TRASLADO, self).al_validar(
                O_form, _dbRow, D_camposAChecar = _D_camposAChecar,
                )

            if not O_form.errors:
                if not O_form.record_id:
                    # Si esta creando el registro:
                    _dbRow_cfdiRelacionado = dbc01.tcfdis(_D_camposAChecar.cfdirelacionado_id)
                    _dbRow_cfdi = dbc01.tcfdis(_D_camposAChecar.cfdi_id)

                    if _dbRow_cfdi.tiporelacion_id == TTIPOSRELACIONES.TRASLADOFACTPREV:
                        s_msgError = "Si tipo relacion es Traslado Fact. Prev.; "

                        if _dbRow_cfdiRelacionado.tipocomprobante_id != TTIPOSCOMPROBANTES.INGRESO:
                            O_form.errors.id = s_msgError + "CFDI relacionado debe ser de tipo ingreso"

                        elif _dbRow_cfdiRelacionado.sat_status == TCFDIS.SAT_STATUS.CANCELADO:
                            O_form.errors.id = s_msgError + "CFDI relacionado no debe estar cancelado"

                        elif _dbRow_cfdiRelacionado.sat_status != TCFDIS.SAT_STATUS.VIGENTE:
                            O_form.errors.id = s_msgError + "CFDI relacionado debe estar timbrado y vigente"

                        else:
                            pass

                    elif _dbRow_cfdi.tiporelacion_id == TTIPOSRELACIONES.DEVOLUCION:
                        s_msgError = "Si tipo relacion es Devolución; "

                        if _dbRow_cfdiRelacionado.tipocomprobante_id != TTIPOSCOMPROBANTES.EGRESO:
                            O_form.errors.id = s_msgError + "CFDI relacionado debe ser de tipo egreso"

                        elif _dbRow_cfdiRelacionado.sat_status == TCFDIS.SAT_STATUS.CANCELADO:
                            O_form.errors.id = s_msgError + "CFDI relacionado esta cancelado"

                        elif _dbRow_cfdiRelacionado.sat_status != TCFDIS.SAT_STATUS.VIGENTE:
                            O_form.errors.id = s_msgError + "CFDI relacionado no esta timbrado"

                        else:
                            pass

                    else:
                        O_form.errors.id = "Tipo relación de CFDI no permite agregar relacionados"

                else:
                    # Si esta editando el registro
                    pass
            else:
                pass

            # Validaciones

            return O_form

        @classmethod
        def AL_ACEPTAR(
                cls,
                O_form,
                **D_params
                ):
            """ Al aceptar el registro deben hacerse los cálculos

            Se hacen todos los cálculos y aceptación de los cambios en el producto

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()
            _dbRow = cls.dbTabla(O_form.record_id or O_form.vars.id)

            if not O_form.record_id:
                # Si se inserta

                # Se copian los productos del CFDI relacionado al CDFI de traspaso actual
                _dbRow_cfdiRelacionado = dbc01.tcfdis(_dbRow.cfdirelacionado_id)
                _dbRows_conceptos_relacionados = _dbRow_cfdiRelacionado.tcfdi_conceptos.select()

                for _dbRow_concepto in _dbRows_conceptos_relacionados:
                    if _dbRow_concepto.unidad_id not in (TUNIDADES.SERVICIO, TUNIDADES.BONIFICACION):
                        _D_concepto = Storage(
                            cfdi_id = _dbRow.cfdi_id,
                            orden = _dbRow_concepto.orden,
                            cantidad = _dbRow_concepto.cantidad,
                            # unidad_id = _dbRow.unidad_id, La unidad se importa del empresa_prodserv
                            empresa_prodserv_id = _dbRow_concepto.empresa_prodserv_id,
                            )
                        _D_concepto.id = dbc01.tcfdi_conceptos.insert(**_D_concepto)
                        TCFDI_CONCEPTOS.TRASLADO.AL_ACEPTAR(Storage(vars = _D_concepto))

                        # TODOMejora si es importación, exportación se tienen que agregar los impuestos
                    else:
                        pass

            else:  # Si se edita
                pass

            O_form = super(TCFDI_RELACIONADOS.TRASLADO, cls).AL_ACEPTAR(O_form, _dbRow)

            generaMensajeFlash(cls.dbTabla, _D_return)
            return O_form

        pass  # TRASLADO

    pass  # TCFDI_RELACIONADOS


dbc01.define_table(
    TCFDI_RELACIONADOS.S_NOMBRETABLA, *TCFDI_RELACIONADOS.L_DBCAMPOS,
    format = '%(cfdi_id)s',
    singular = 'CFDI relacionado',
    plural = 'CFDI relacionados',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TCFDI_RELACIONADOS
    )


class TCFDI_CONCEPTOS(Table):
    """ Corresponde a los conceptos de los CFDIs. """
    S_NOMBRETABLA = 'tcfdi_conceptos'
    L_DBCAMPOS = [
        Field(
            'cfdi_id', dbc01.tcfdis, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'CFDI', comment = None,
            writable = False, readable = False,
            represent = None
            ),
        Field(
            'orden', 'integer', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputInteger, label = 'Orden', comment = None,
            writable = False, readable = False,
            represent = stv_represent_number
            ),
        Field(
            'cantidad', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputFloat, label = 'Cantidad', comment = None,
            writable = True, readable = True,
            represent = stv_represent_float
            ),
        Field(
            'claveprodserv', 'string', length = 30, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Clave Prod./Serv.', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        # El campo prodserv_id debería ser el mismo que empresa_prodserv_id.prodserv_id; sin embargo se deja este
        # campo para posible problema entre la factura y el cambio en el prodserv relacionado al producto
        Field(
            'prodserv_id', 'integer', default = None,
            required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tprodservs.id', db01.tprodservs._format)),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_search(
                f, v,
                s_display = '%(descripcion)s [%(c_claveprodserv)s]',
                s_url = URL(a = 'app_generales', c = '005genericos_sat', f = 'prodservs_buscar'),
                D_additionalAttributes = {'reference': db01.tprodservs.id}
                ),
            label = 'Producto o Servicio', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tcfdi_conceptos.prodserv_id)
            ),
        Field(
            'claveunidad', 'string', length = 30, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Clave Unidad', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'unidad', 'string', length = 100, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Unidad', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'unidad_id', 'integer', default = TUNIDADES.PIEZA,
            required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tunidades.id', db01.tunidades._format)),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_search(
                f, v,
                s_display = '%(nombre)s [%(c_claveunidad)s]',
                s_url = URL(
                    a = 'app_generales',
                    c = '005genericos_sat',
                    f = 'unidades_buscar'
                    ),
                D_additionalAttributes = {'reference': db01.tunidades.id}
                ),
            label = 'Unidad', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(
                v, r, dbc01.tcfdi_conceptos.unidad_id
                )
            ),
        Field(
            'descripcion', 'string', length = 150, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Descripción', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'descuento', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Descuento', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_money_ifnotzero(v, r, dbField = dbc01.tcfdi_conceptos.descuento)
            ),
        Field(
            'importe', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Importe', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_money_ifnotzero(v, r, dbField = dbc01.tcfdi_conceptos.importe)
            ),

        Field(
            'noidentificacion', 'string', length = 30, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'No. Ident.', comment = 'Código de la empresa para el producto',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'empresa_prodserv_id', 'integer', default = None,
            required = False,
            requires = IS_NULL_OR(IS_IN_DB(dbc01, 'tempresa_prodserv.id', dbc01.tempresa_prodserv._format)),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_search(
                f, v,
                s_display = dbc01.tempresa_prodserv._format,
                s_url = URL(
                    a = 'app_empresas', c = '120empresas', f = 'empresa_prodserv_buscar',
                    args = ['empresa_id', D_stvFwkCfg.dbRow_empresa.id]
                    ),
                D_additionalAttributes = {'reference': dbc01.tempresa_prodserv.id}
                ),
            label = 'Emp. Producto/Servicio', comment = '',
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(
                v, r, dbc01.tcfdi_conceptos.empresa_prodserv_id
                )
            ),

        # Es el valor unitario afectado por la politica de precios y de manejo de cascos, usado para el XML
        Field(
            'valorunitario', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Valor Unitario', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),

        # Es el valorunitario capturado por el usuario
        Field(
            'valorunitario_capturado', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Valor Unitario', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),

        # Campos usados para determinar la cantidad ya ingresada a almacenes en caso de CFDIs de ingreso
        # En caso de requerir saber en que almacenes se ingresó, ver tabla tempresa_invmovto_productos
        # TODOMEJORA crear chequeo de calidad que verifique esta cantidad con la definida en tempresa_invmovto_productos
        Field(
            'cantidad_ingresada', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputFloat, label = 'Cantidad Ingresada', comment = None,
            writable = False, readable = True,
            represent = stv_represent_float
            ),

        # >> Configuraciones especificas por cliente plugin: CASCOS
        # Este campo aplica a conceptos de clasificación que corresponda a casco ya sea de 1a1 o excedente,
        # y se usa para determinar si corresponde a un casco entregado, para poder aplicarse
        # como descuento. Los conceptos con este checkbox se usaran para limitar la cantidad
        # de cascos 1 a 1 que se pueden considerar en el manejo del ingreso.
        Field(
            'cfdi_concepto_complemento_id', 'integer', default = None,
            required = False,
            ondelete = 'CASCADE', notnull = False, unique = False,
            label = (
                'CFDI Concepto Complemento'),
            comment = 'En caso de ser un producto con complemento, es el id del producto que hace complemento',
            writable = False, readable = False,
            represent = None
            ),
        Field(  # El uso de este campo es exclusivo para egresos
            'aplicacasco_1a1', 'boolean', default = False,
            required = False,
            notnull = False,
            widget = stv_widget_inputCheckbox, label = (
                'Aplica casco 1a1'), comment = 'El importe corresponde a una entrega de casco 1a1',
            writable = True, readable = True,
            represent = stv_represent_boolean
            ),

        # <<

        tSignature_dbc01,
        ]

    def __init__(self, dbRef, tablename, *fields, **args):
        Table.__init__(self, dbRef, tablename, *fields, **args)
        self.PROC.dbTabla = self
        return

    class PROC(TCFDIS.PROC_DETALLE, object):

        dbTabla = None  # Se escribe con la inicialización

        def puede_crear(self, x_cfdi, **D_defaults):
            """ Determina si puede o no crear un CFDI de traslado.

            Actualmente no hay condición requerida.
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi, dbc01.tcfdis)
            _dbRow_cfdi = _D_results.dbRow

            _D_results = super(TCFDI_CONCEPTOS.PROC, self).puede_crear(
                x_cfdi = _dbRow_cfdi,
                D_requeridosMaestro = Storage(
                    tipocomprobante_id = Storage(
                        L_valores = [
                            TTIPOSCOMPROBANTES.INGRESO, TTIPOSCOMPROBANTES.EGRESO, TTIPOSCOMPROBANTES.TRASLADO
                            ],  # PAGO no permite creación manual
                        s_msgError = "Tipo Comprobante debe ser Ingreso, Egreso o Traslado"
                        )
                    ),
                **D_defaults
                )
            _D_return.combinar(_D_results)

            return _D_return

        @classmethod
        def CONFIGURACAMPOS_DESDE_dbRow(cls, dbRow_base):
            cls.dbTabla.cfdi_id.default = dbRow_base.cfdi_id
            cls.dbTabla.cantidad.default = dbRow_base.cantidad
            # cls.dbTabla.claveprodserv.default = _dbRow_base.claveprodserv
            # cls.dbTabla.prodserv_id.default = _dbRow_base.prodserv_id
            # cls.dbTabla.claveunidad.default = _dbRow_base.claveunidad
            # cls.dbTabla.unidad.default = _dbRow_base.unidad
            cls.dbTabla.unidad_id.default = dbRow_base.unidad_id
            cls.dbTabla.descripcion.default = dbRow_base.descripcion
            # cls.dbTabla.descuento.default = dbRow_base.descuento
            # cls.dbTabla.importe.default = dbRow_base.importe
            # cls.dbTabla.noidentificacion.default = dbRow_base.noidentificacion
            cls.dbTabla.empresa_prodserv_id.default = dbRow_base.empresa_prodserv_id
            # cls.dbTabla.valorunitario.default = dbRow_base.valorunitario
            # cls.dbTabla.valorunitario_capturado.default = dbRow_base.valorunitario_capturado
            # cls.dbTabla.cantidad_ingresada.default = dbRow_base.cantidad_ingresada
            # cls.dbTabla.cfdi_concepto_complemento_id.default = dbRow_base.cfdi_concepto_complemento_id
            # cls.dbTabla.aplicacasco_1a1.default = dbRow_base.aplicacasco_1a1
            return

        def configuracampos_nuevo(
                self,
                s_cfdi_concepto_id,
                b_llamadaDesdeEditar = False,
                **D_defaults
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param b_llamadaDesdeEditar:
            @type b_llamadaDesdeEditar:
            @param s_cfdi_concepto_id:
            @type s_cfdi_concepto_id:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = super(TCFDI_CONCEPTOS.PROC, self).configuracampos_nuevo(
                s_cfdi_detalle_id = s_cfdi_concepto_id,
                b_llamadaDesdeEditar = b_llamadaDesdeEditar,
                **D_defaults
                )
            _D_return.combinar(_D_results)

            self.dbTabla.cfdi_id.writable = False
            self.dbTabla.orden.writable = True
            self.dbTabla.cantidad.writable = True
            self.dbTabla.empresa_prodserv_id.writable = True
            self.dbTabla.valorunitario.writable = False
            self.dbTabla.valorunitario_capturado.writable = False

            # Campos que no deben ser editados, solo de forma autocalculada
            self.dbTabla.claveprodserv.writable = False
            self.dbTabla.prodserv_id.writable = False
            self.dbTabla.unidad_id.writable = False
            self.dbTabla.descuento.writable = False
            self.dbTabla.claveunidad.writable = False
            self.dbTabla.unidad.writable = False
            self.dbTabla.descripcion.writable = False
            self.dbTabla.importe.writable = False
            self.dbTabla.noidentificacion.writable = False
            self.dbTabla.cantidad_ingresada.writable = False
            self.dbTabla.cfdi_concepto_complemento_id.writable = False
            self.dbTabla.aplicacasco_1a1.writable = False

            # Campos que todos los movimientos pueden editar
            self.dbTabla.notas.writable = True

            # Se definen los valores por default obligados
            self.dbTabla.cfdi_id.default = self._D_defaults.cfdi_id
            _n_siguienteOrden = (dbc01(dbc01.tcfdi_conceptos.cfdi_id == self.dbTabla.cfdi_id.default).count() + 1) * 100
            self.dbTabla.orden.default = self._D_defaults.orden if self._D_defaults.orden else _n_siguienteOrden

            return _D_return

        def al_validar(
                self,
                O_form,
                **D_params
                ):
            """ Validar la información para poder grabar cfdi

            Las validaciones en esta función son genéricas, se espera que existan validaciones específicas por
             tipo de CFDI

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _dbRow = self.dbTabla(O_form.record_id) if O_form.record_id else None
            _D_camposAChecar = STV_FWK_LIB.OBTENER_CAMPOSACHECAR_PARA_VALIDAR(self.dbTabla, _dbRow)

            # Se llaman las validaciones del CFDI_relacionado, indicando que tiporelacion_id debe existsir
            O_form = super(TCFDI_CONCEPTOS.PROC, self).al_validar(
                O_form, _dbRow, D_camposAChecar = _D_camposAChecar
                )

            # Validaciones

            if not _D_camposAChecar.empresa_prodserv_id:
                O_form.errors.empresa_prodserv_id = "Producto o Servicio debe especificarse"
            else:
                pass

            return O_form

        @classmethod
        def AL_ACEPTAR(
                cls,
                O_form,
                **D_params
                ):
            """ Al aceptar el registro deben hacerse los cálculos

            Se hacen todos los cálculos y aceptación de los cambios en el producto

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()
            _dbRow = cls.dbTabla(O_form.record_id or O_form.vars.id)

            _f_deltaCantidad = 0

            _D_results = cls.MAPEAR_CAMPOS(_dbRow)

            if _D_results.E_return > stvfwk2_e_RETURN.OK_WARNING:
                _D_return.combinar(_D_results)

            elif not O_form.record_id:
                # Si se esta insertando, se agregan los impuestos configurados al producto
                _f_deltaCantidad = _dbRow.cantidad

                _D_results = TCFDI_CONCEPTOS.REDEFINIR_IMPUESTOS(_dbRow)
                _D_return.combinar(_D_results)

            else:
                # Si no esta insertando, no generar impuestos
                _f_deltaCantidad = _dbRow.cantidad - O_form.record.cantidad

            if _f_deltaCantidad:
                # Si hubo cambio en cantidad, se redefine o ajustan las cantidades en cascos si es el caso

                # Adentro de la función se identifica si es Ingreso y no asocia casco
                _D_results = cls.REDEFINIR_CASCOS(_dbRow, _f_deltaCantidad)
                _D_return.combinar(_D_results)

            else:
                pass

            _D_results = cls.CALCULAR_CAMPOS(_dbRow)
            _D_return.combinar(_D_results)

            generaMensajeFlash(cls.dbTabla, _D_return)
            return O_form

        @classmethod
        def DESPUES_ELIMINAR(
                cls,
                L_errorsAction,
                ):
            """ Después de eliminar un registro

            @param L_errorsAction:
            @type L_errorsAction:
            @return: errores
            @rtype: lista de diccionarios {'msg':'...'}
            """

            def actualiza_dbRowCfdiConcepto(dbRow_conceptoComplemento, dbRow_eliminado):
                if dbRow_conceptoComplemento.cantidad <= dbRow_eliminado.cantidad:
                    dbRow_conceptoComplemento.delete_record()

                elif dbRow_conceptoComplemento.cantidad > dbRow_eliminado.cantidad:
                    dbRow_conceptoComplemento.cantidad -= Decimal(dbRow_eliminado.cantidad)
                    dbRow_conceptoComplemento.update_record()

                else:
                    pass
                return

            _D_return = FUNC_RETURN()

            if cls._dbRow_aEliminar:
                _dbRow_eliminado = cls._dbRow_aEliminar

                if _dbRow_eliminado.cfdi_concepto_complemento_id:
                    _dbRow_conceptoComplemento = dbc01.tcfdi_conceptos(_dbRow_eliminado.cfdi_concepto_complemento_id)

                    if _dbRow_conceptoComplemento:
                        _dbRow_complementoAEliminar = Storage(_dbRow_conceptoComplemento.as_dict())

                        if _dbRow_complementoAEliminar.cfdi_id != _dbRow_eliminado.cfdi_id:
                            # Si el concepto complemento esta en otro CFDI, se verifica que pueda ser editado
                            _D_results = TCFDIS.PROC.PUEDE_EDITAR(x_cfdi_complemento)
                            if _D_results.E_return > stvfwk2_e_RETURN.OK_WARNING:
                                _D_return.combinar(
                                    _D_results,
                                    "CFDI Complemento no puede ser afectado %d:" % _dbRow_complementoAEliminar.cfdi_id
                                    )
                            else:
                                # Si no hay errores, actualiza los conceptos
                                actualiza_dbRowCfdiConcepto(_dbRow_conceptoComplemento, _dbRow_eliminado)

                                if dbc01(
                                        dbc01.tcfdi_conceptos.cfdi_id == _dbRow_complementoAEliminar.cfdi_id
                                        ).count() > 0:
                                    # Si es diferente CFDI, y hay conceptos, se recalcula el CFDI
                                    _D_results = TCFDIS.PROC.CALCULAR_CAMPOS(
                                        x_cfdi = _dbRow_complementoAEliminar.cfdi_id
                                        )

                                else:
                                    # Si no quedan conceptos, se elimina el CFDI complemento
                                    dbc01.tcfdis(_dbRow_complementoAEliminar.cfdi_id).delete_record()

                            pass

                        else:
                            # Si el concepto complemento esta en el mismo CFDI, se actualiza
                            actualiza_dbRowCfdiConcepto(_dbRow_conceptoComplemento, _dbRow_eliminado)
                            # Abajo se recalcula el CFDI

                    else:
                        _D_return.agrega_error("CFDI complemento id identificado pero no encontrado en tabla %d. "
                            % _dbRow_eliminado.cfdi_concepto_complemento_id)

                else:
                    # Sin Concepto complemento
                    pass

                _D_results = TCFDIS.PROC.CALCULAR_CAMPOS(x_cfdi = _dbRow_eliminado.cfdi_id)
                _D_return.combinar(_D_results)
            else:
                _D_return.agregar(CLASS_e_RETURN.NOK_SECUENCIA, "Error en secuencia de llamado de "
                    + "funciones para borrado. ")

            generaMensajeFlash(cls.dbTabla, _D_return)
            return L_errorsAction

        @classmethod
        def MAPEAR_CAMPOS(
                cls,
                x_cfdi_concepto,
                **D_params
                ):
            """ Recalcula campos, haciendo la consulta de la carta porte a partir del CFDI

            @param x_cfdi_concepto:
            @type x_cfdi_concepto:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_concepto, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage(
                empresa_prodserv_id = None,
                prodserv_id = "",
                descripcion = "",
                noidentificacion = "",
                claveprodserv = "",
                unidad_id = None,
                claveunidad = "",
                unidad = "",
                )

            _dbRow_empresaProdServ = None
            if _dbRow.empresa_prodserv_id:
                # Si tiene definido el prodserv_id
                _dbRow_empresaProdServ = dbc01.tempresa_prodserv(_dbRow.empresa_prodserv_id)
                _D_actualizar.empresa_prodserv_id = _dbRow.empresa_prodserv_id

            elif _dbRow.noidentificacion:
                # Si tiene definido noidentificacion, significa que viene de importar exce;
                _dbRow_cfdi = dbc01.tcfdis(_dbRow.cfdi_id)
                _dbRows_empresaProdServ = dbc01(
                    (dbc01.tempresa_prodserv.empresa_id == _dbRow_cfdi.empresa_id)
                    & (dbc01.tempresa_prodserv.codigo == _dbRow.noidentificacion)
                    ).select(
                        dbc01.tempresa_prodserv.ALL
                        )

                if len(_dbRows_empresaProdServ) == 0:
                    _D_return.agrega_error("Producto no encontrado %s. " % _dbRow.noidentificacion)

                elif len(_dbRows_empresaProdServ) > 1:
                    _D_return.agrega_error(("Producto encontrado varias veces, favor de corregir "
                        + "catálogo de productos %d. ") % _dbRow.noidentificacion)

                else:
                    _dbRow_empresaProdServ = _dbRows_empresaProdServ.first()
                    _D_actualizar.empresa_prodserv_id = _dbRow_empresaProdServ.id

            else:
                pass

            _D_actualizar.prodserv_id = _dbRow_empresaProdServ.prodserv_id if _dbRow_empresaProdServ else None
            _D_actualizar.descripcion = _dbRow_empresaProdServ.descripcion if _dbRow_empresaProdServ else None
            _D_actualizar.noidentificacion = _dbRow_empresaProdServ.codigo if _dbRow_empresaProdServ else None

            _D_actualizar.claveprodserv = db01.tprodservs(
                _D_actualizar.prodserv_id
                ).c_claveprodserv if _D_actualizar.prodserv_id else ""

            _D_actualizar.unidad_id = _dbRow_empresaProdServ.unidad_id if _dbRow_empresaProdServ else None

            _dbRow_unidad = db01.tunidades(_D_actualizar.unidad_id) if _D_actualizar.unidad_id else None
            _D_actualizar.claveunidad = _dbRow_unidad.c_claveunidad if _dbRow_unidad else ""
            _D_actualizar.unidad = _dbRow_unidad.nombre if _dbRow_unidad else ""

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)
                    break
                else:
                    pass

            return _D_return

        @classmethod
        def CALCULAR_CAMPOS(
                cls,
                x_cfdi_concepto,
                **D_params
                ):
            """ Recalcula campos

            @param x_cfdi_concepto:
            @type x_cfdi_concepto:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_concepto, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage(
                importe = 0
                )

            _D_actualizar.importe = _dbRow.cantidad * _dbRow.valorunitario

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)

                    # Si hay actualizaciones, debe actualizarse el CFDI, no los conceptos
                    _dbRows_impuestosTras = _dbRow.tcfdi_concepto_impuestostrasladados.select()
                    for _dbRow_impuesto in _dbRows_impuestosTras:
                        _D_results = TCFDI_CONCEPTO_IMPUESTOSTRASLADADOS.PROC.CALCULAR_CAMPOS(
                            x_cfdi_concepto_imptras = _dbRow_impuesto,
                            b_recalcularCFDI = False
                            )
                    _D_results = TCFDIS.PROC.CALCULAR_CAMPOS(
                        x_cfdi = _dbRow.cfdi_id
                        )
                    break
                else:
                    pass

            return _D_return

        @classmethod
        def REDEFINIR_CASCOS(
                cls,
                dbRow_concepto,
                f_delta_cantidad,
                ):
            """ Crea y liga conceptos de cascos si se tienen las condiciones para hacerlo.

            @param f_delta_cantidad:
            @type f_delta_cantidad:
            @param dbRow_concepto:
            @type dbRow_concepto:
            @return:
            @rtype:
            """

            def agrega_concepto(n_cfdiParaComplementos_id, n_prodserv_casco_id):
                # noinspection PyShadowingNames
                _D_return = FUNC_RETURN()
                _dbRows_conceptoCasco_enCFDIComplemento = dbc01(
                    (dbc01.tcfdi_conceptos.cfdi_id == n_cfdiParaComplementos_id)
                    & (dbc01.tcfdi_conceptos.empresa_prodserv_id == n_prodserv_casco_id)
                    ).select(
                    dbc01.tcfdi_conceptos.ALL,
                    orderby = [~dbc01.tcfdi_conceptos.id]
                    )

                _n_cfidConcepto_complemento_id = None

                if _dbRows_conceptoCasco_enCFDIComplemento:
                    # Si ya existe algún producto con el mismo tipo de casco, se asocia el delta
                    _dbRow_conceptoCasco_enCFDIComplemento = _dbRows_conceptoCasco_enCFDIComplemento.first()
                    _dbRow_conceptoCasco_enCFDIComplemento.cantidad += f_delta_cantidad
                    if _dbRow_conceptoCasco_enCFDIComplemento.cantidad <= 0:
                        _D_return.agrega_error("Cantidad en complemento es inválida")
                        _dbRow_conceptoCasco_enCFDIComplemento.cantidad = 0
                    else:
                        pass
                    _dbRow_conceptoCasco_enCFDIComplemento.update_record()
                    _n_cfidConcepto_complemento_id = _dbRow_conceptoCasco_enCFDIComplemento.id
                    cls.AL_ACEPTAR(Storage(vars = _dbRow_conceptoCasco_enCFDIComplemento))

                else:
                    _D_fieldsInsertConceptoComplemento = Storage(
                        cfdi_id = n_cfdiParaComplementos_id,
                        orden = 1000,
                        cantidad = f_delta_cantidad,
                        empresa_prodserv_id = n_prodserv_casco_id,
                        )
                    _D_fieldsInsertConceptoComplemento.id = dbc01.tcfdi_conceptos.insert(
                        **_D_fieldsInsertConceptoComplemento
                        )
                    _n_cfidConcepto_complemento_id = _D_fieldsInsertConceptoComplemento.id
                    cls.AL_ACEPTAR(Storage(vars = _D_fieldsInsertConceptoComplemento))

                if not dbRow_concepto.cfdi_concepto_complemento_id:
                    dbRow_concepto.cfdi_concepto_complemento_id = _n_cfidConcepto_complemento_id
                    dbRow_concepto.update_record()
                else:
                    pass

                return _D_return

            _D_return = FUNC_RETURN(
                f_precioCasco_1a1 = 0
                )

            _dbRow_prodserv = dbc01.tempresa_prodserv(dbRow_concepto.empresa_prodserv_id)

            if not _dbRow_prodserv.empresa_prodserv_casco1a1_id:
                # El prodserv no tiene casco asociado que requiera manejar
                pass

            elif dbRow_concepto.cfdi_concepto_complemento_id \
                    and (dbc01(dbc01.tcfdi_conceptos.id == dbRow_concepto.cfdi_concepto_complemento_id).count() > 0):
                # Si ya existe la relación al complemento_id
                _dbRow_conceptoComplemento = dbc01.tcfdi_conceptos(dbRow_concepto.cfdi_concepto_complemento_id)
                _dbRow_conceptoComplemento.cantidad += f_delta_cantidad
                if _dbRow_conceptoComplemento.cantidad <= 0:
                    _D_return.agrega_error("Cantidad en complemento es inválida")
                    _dbRow_conceptoComplemento.cantidad = 0
                else:
                    pass
                _dbRow_conceptoComplemento.update_record()
                cls.AL_ACEPTAR(Storage(vars = _dbRow_conceptoComplemento))

            else:
                # En este punto se tiene que crear un concepto, o se modifica el ajusta el casco
                #  en el precio del producto
                dbRow_concepto.cfdi_concepto_complemento_id = None  # Previene problemas si se pierda la relación

                _n_cfdi_paraComplementos_id = None  # Indica el CFDI ID del maestro para el concepto de cascos

                _dbRows_cfdi = dbc01(
                    dbc01.tcfdis.id == dbRow_concepto.cfdi_id
                    ).select(
                    dbc01.tcfdis.id,
                    dbc01.tcfdis.facturacomplementos,
                    dbc01.tcfdis.cfdi_complemento_id,
                    dbc01.tcfdis.tipocomprobante_id,
                    )

                _dbRow_cfdi = _dbRows_cfdi.first()

                if _dbRow_cfdi.tipocomprobante_id != TTIPOSCOMPROBANTES.INGRESO:
                    # No aplica redefinición de cascos para comprobantes diferentes a ingresos
                    pass

                elif _dbRow_cfdi.facturacomplementos == TEMPRESA_CLIENTES.E_FACTURARCOMPLEMENTOS.MISMAFACTURA:
                    agrega_concepto(dbRow_concepto.cfdi_id, _dbRow_prodserv.empresa_prodserv_casco1a1_id)

                elif _dbRow_cfdi.facturacomplementos == TEMPRESA_CLIENTES.E_FACTURARCOMPLEMENTOS.FACTURASEPARADA:

                    if _dbRow_cfdi.cfdi_complemento_id \
                            and dbc01.tcfdis(_dbRow_cfdi.cfdi_complemento_id).count() > 0:
                        # Si ya existe el cfdi separado se utiliza
                        agrega_concepto(_dbRow_cfdi.cfdi_complemento_id, _dbRow_prodserv.empresa_prodserv_casco1a1_id)

                    else:
                        _dbRow_cfdi = dbc01.tcfdis(
                            dbRow_concepto.cfdi_id
                            )  # Ahora se toman todos los elementos del CFDI

                        _D_cfdi_insertar = Storage(
                            empresa_id = _dbRow_cfdi.empresa_id,
                            empresa_plaza_sucursal_cajachica_id = _dbRow_cfdi.empresa_plaza_sucursal_cajachica_id,
                            fecha = _dbRow_cfdi.fecha,
                            formapago_id = _dbRow_cfdi.formapago_id,
                            condicionesdepago = _dbRow_cfdi.condicionesdepago,
                            monedacontpaqi_id = _dbRow_cfdi.monedacontpaqi_id,
                            tipocomprobante_id = TTIPOSCOMPROBANTES.INGRESO,
                            metodopago_id = _dbRow_cfdi.metodopago_id,
                            credito_plazo = _dbRow_cfdi.credito_plazo,
                            # tiporelacion_id = _dbRow_cfdi.tiporelacion_id,
                            # emisorproveedor_id = _dbRow_cfdi.emisorproveedor_id,
                            receptorusocfdi_id = _dbRow_cfdi.receptorusocfdi_id,
                            receptorcliente_id = _dbRow_cfdi.receptorcliente_id,
                            rol = _dbRow_cfdi.rol,
                            generado_por = _dbRow_cfdi.generado_por,
                            )
                        _D_cfdi_insertar.id = dbc01.tcfdis.insert(
                            **_D_cfdi_insertar
                            )
                        _dbRow_cfdi.cfdi_complemento_id = _D_cfdi_insertar.id
                        _dbRow_cfdi.update_record()
                        TCFDIS.INGRESO.AL_ACEPTAR(Storage(vars = _D_cfdi_insertar))
                        _n_cfdi_paraComplementos_id = _D_cfdi_insertar.id

                        agrega_concepto(_D_cfdi_insertar.id, _dbRow_prodserv.empresa_prodserv_casco1a1_id)

                elif _dbRow_cfdi.facturacomplementos == TEMPRESA_CLIENTES.E_FACTURARCOMPLEMENTOS.AJUSTARPRECIO:
                    # Se tiene que ajustar el precio por la cantidad delta
                    # Este proceso se autoajusta al calcular el concepto, es de donde se manda llamar esta función
                    pass

                else:
                    agrega_concepto(dbRow_concepto.cfdi_id, _dbRow_prodserv.empresa_prodserv_casco1a1_id)

            return _D_return

        pass  # PROC

    class PROC_DETALLE:

        dbTabla = None  # Se escribe con la inicialización

        def __init__(self, **D_defaults):
            """ Inicializa objecto de tipo PROC para control del registro actual

            @param D_defaults:
            @type D_defaults:
            """
            self._D_defaults = Storage(D_defaults)
            self._L_requeridos = ['cfdi_concepto_id']
            return

        def puede_crear(self, x_cfdi_concepto, **D_defaults):
            """ Determina si puede o no crear un CFDI de traslado.

            Actualmente no hay condición requerida.
            """
            _D_return = FUNC_RETURN()

            self._D_defaults.update(D_defaults or {})

            for _s_requerido in self._L_requeridos:
                if _s_requerido not in self._D_defaults:
                    _D_return.agrega_error("Campo %s es requerido para generar CFDI" % _s_requerido)

                else:
                    pass

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_concepto, dbc01.tcfdi_conceptos)
            _dbRow_cfdi_concepto = _D_results.dbRow

            _D_results = TCFDI_CONCEPTOS.PROC.PUEDE_EDITAR(_dbRow_cfdi_concepto)
            _D_return.combinar(_D_results)

            return _D_return

        @classmethod
        def PUEDE_EDITAR(
                cls,
                x_cfdi_concepto_detalle
                ):
            """ Define si es posible editar el CFDI de traslado.
            @param x_cfdi_concepto_detalle:

            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_concepto_detalle, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_results = TCFDI_CONCEPTOS.PROC.PUEDE_EDITAR(_dbRow.cfdi_concepto_id)
            _D_return.combinar(_D_results)

            return _D_return

        @classmethod
        def PUEDE_ELIMINAR(
                cls,
                x_cfdi_concepto_detalle
                ):
            """ Determina si puede o no eliminar un registro

            Misma condición que puede_editar, a lo mejor pudieran verse condiciones distintas

            """

            return cls.PUEDE_EDITAR(x_cfdi_concepto_detalle)

        @classmethod
        def CONFIGURACAMPOS_DESDE_dbRow(cls, dbRow_base):
            # cls.dbTabla.cfdi_concepto_id.default = dbRow_base.cfdi_concepto_id
            # cls.dbTabla.campo1.default = dbRow_base.campo1
            # cls.dbTabla.campo2.default = dbRow_base.campo2
            return

        def configuracampos_nuevo(
                self,
                s_cfdi_concepto_detalle_id,
                b_llamadaDesdeEditar = False,
                **D_defaults
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param b_llamadaDesdeEditar: Indica si la llamada a esta función es desde otra capa,
             desactivando validación de puede_crear
            @type b_llamadaDesdeEditar:
            @param s_cfdi_concepto_detalle_id:
            @type s_cfdi_concepto_detalle_id:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            self._D_defaults.update(D_defaults)
            for _s_fieldName in self.dbTabla.fields:
                self.dbTabla[_s_fieldName].writable = False
            for _s_fieldName in self._D_defaults:
                self.dbTabla[_s_fieldName].default = self._D_defaults[_s_fieldName]

            if not b_llamadaDesdeEditar:
                _D_results = self.puede_crear(
                    x_cfdi_concepto = self._D_defaults.cfdi_concepto_id
                    )
            else:
                _D_results = _D_return

            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.combinar(_D_results)

            elif s_cfdi_concepto_detalle_id:
                _dbRow_base = self.dbTabla(s_cfdi_concepto_detalle_id)

                self.CONFIGURACAMPOS_DESDE_dbRow(_dbRow_base)

            else:
                pass

            self.dbTabla.cfdi_concepto_id.writable = False

            # Campos que no deben ser editados, solo de forma autocalculada

            # Campos que todos los movimientos pueden editar
            self.dbTabla.notas.writable = False

            # Se definen los valores por default obligados
            self.dbTabla.cfdi_concepto_id.default = self._D_defaults.cfdi_concepto_id

            return _D_return

        def configuracampos_edicion(
                self,
                x_cfdi_concepto_detalle,
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param x_cfdi_concepto_detalle:
            @type x_cfdi_concepto_detalle:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_concepto_detalle, self.dbTabla)
            _dbRow = _D_results.dbRow

            # Se llenan los default obligados
            for _s_nombreCampo in self._D_defaults:
                self._D_defaults[_s_nombreCampo] = _D_results.dbRow[_s_nombreCampo]

            _D_results = self.PUEDE_EDITAR(_dbRow)

            if _D_results.E_return == CLASS_e_RETURN.OK:
                # Si puede editar regresa un ok y el movimiento esta abierto

                # Se manda vacío el argumento para no agregar defaults a los campos
                _D_results = self.configuracampos_nuevo(
                    None,
                    b_llamadaDesdeEditar = True,
                    **self._D_defaults
                    )
                _D_return.combinar(_D_results)

            else:
                # De lo contrario en cualquier otro caso, no puede editar nada de esta tabla
                for _s_fieldName in self.dbTabla.fields:
                    self.dbTabla[_s_fieldName].writable = False
                _D_return.agrega(CLASS_e_RETURN.OK_WARNING, _D_results.s_msgError, "No puede editar")

            return _D_return

        def al_validar(
                self,
                O_form,
                dbRow = None,
                D_camposAChecar = None,
                ):
            """ Validar la información para poder grabar cfdi

            Las validaciones en esta función son genéricas, se espera que existan validaciones
             específicas por tipo de CFDI

            @param dbRow:
            @type dbRow:
            @param D_camposAChecar:
            @type D_camposAChecar:
            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            dbRow = dbRow if dbRow else self.dbTabla(O_form.record_id) if O_form.record_id else None
            D_camposAChecar = D_camposAChecar if D_camposAChecar \
                else STV_FWK_LIB.OBTENER_CAMPOSACHECAR_PARA_VALIDAR(self.dbTabla, dbRow)

            if not O_form.record_id:
                # Se esta creando el registro

                _D_results = self.puede_crear(
                    x_cfdi_concepto = D_camposAChecar.cfdi_concepto_id,
                    **D_camposAChecar
                    )
                if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                    O_form.errors.id = _D_results.s_msgError
                else:
                    pass

            else:
                # Se esta modificando el registro, la llamada a configuraCampos_edicion ya definió con writable
                # que puede modificarse y que no.

                _D_results = self.PUEDE_EDITAR(dbRow)
                if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                    O_form.errors.id = _D_results.s_msgError
                else:
                    pass

            # Validaciones

            # if not _D_camposAChecar.campo1:
            #     O_form.errors.campo1 = "Campo1 no puede estar vacío"
            # else:
            #     pass

            return O_form

        @classmethod
        def AL_ACEPTAR(
                cls,
                O_form,
                dbRow = None,
                b_calcularCampos = True
                ):
            """ Al aceptar el registro deben hacerse los cálculos

            Se hacen todos los cálculos y aceptación de los cambios en el producto

            @param b_calcularCampos: determina si se realza el cálculo de los campos autocalculados.
             Se utiliza para prevenir doblde recálculo de los campos, cuando se están insertando automaticamente.
            @type b_calcularCampos:
            @param dbRow:
            @type dbRow:
            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()
            dbRow = dbRow if dbRow else cls.dbTabla(O_form.record_id or O_form.vars.id)

            if not O_form.record_id:
                # Si se inserta
                pass

            else:  # Si se edita
                pass

            _D_results = cls.MAPEAR_CAMPOS(dbRow)
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.combinar(_D_results)

            elif b_calcularCampos:
                _D_results = cls.CALCULAR_CAMPOS(dbRow)
                _D_return.combinar(_D_results)

            else:
                pass

            generaMensajeFlash(cls.dbTabla, _D_return)
            return O_form

        @classmethod
        def ANTES_ELIMINAR(
                cls,
                L_errorsAction,
                dbRow,
                ):
            """ Antes de eliminar un movimiento de inventario de este tipo

            @param dbRow:
            @type dbRow:
            @param L_errorsAction:
            @type L_errorsAction:
            @return: errores
            @rtype: lista de diccionarios {'msg':'...'}
            """
            _D_return = FUNC_RETURN()

            _D_results = cls.PUEDE_ELIMINAR(dbRow)
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                L_errorsAction.append({'msg': _D_results.s_msgError})
            else:
                cls._dbRow_aEliminar = Storage(dbRow.as_dict())

            return L_errorsAction

        @classmethod
        def DESPUES_ELIMINAR(
                cls,
                L_errorsAction,
                ):
            """ Después de eliminar un registro

            @param L_errorsAction:
            @type L_errorsAction:
            @return: errores
            @rtype: lista de diccionarios {'msg':'...'}
            """
            _D_return = FUNC_RETURN()

            if cls._dbRow_aEliminar:
                # Aunque es detalle de concepto, modifica el cfdi y no el concepto
                _dbRow_cfdi = dbc01(
                    (dbc01.tcfdi_conceptos.id == cls._dbRow_aEliminar.cfdi_concepto_id)
                    & (dbc01.tcfdis.id == dbc01.tcfdi_conceptos.cfdi_id)
                    ).select(
                    dbc01.tcfdis.id, dbc01.tcfdis.tipocomprobante_id
                    ).first()

                if _dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.EGRESO:
                    _D_results = TCFDI_CONCEPTOS.EGRESO.CALCULAR_CAMPOS(
                        x_cfdi_concepto = cls._dbRow_aEliminar.cfdi_concepto_id
                        )
                elif _dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO:
                    _D_results = TCFDI_CONCEPTOS.INGRESO.CALCULAR_CAMPOS(
                        x_cfdi_concepto = cls._dbRow_aEliminar.cfdi_concepto_id
                        )
                else:
                    _D_results = TCFDI_CONCEPTOS.PROC.CALCULAR_CAMPOS(
                        x_cfdi_concepto = cls._dbRow_aEliminar.cfdi_concepto_id
                        )

                _D_return.combinar(_D_results)
            else:
                _D_return.E_return = CLASS_e_RETURN.NOK_SECUENCIA
                _D_return.s_msgError += "Error en secuencia de llamado de funciones para borrado. "

            generaMensajeFlash(cls.dbTabla, _D_return)
            return L_errorsAction

        @classmethod
        def MAPEAR_CAMPOS(
                cls,
                x_cfdi_concepto_detalle,
                b_actualizarRegistro = True
                ):
            """ Mapea campos, llenando campos automáticos

            @param b_actualizarRegistro:
            @type b_actualizarRegistro:
            @param x_cfdi_concepto_detalle:
            @type x_cfdi_concepto_detalle:
            @return:
            @rtype:
            """

            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_concepto_detalle, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage()

            if b_actualizarRegistro:
                for _s_nombreCampo in _D_actualizar:
                    if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                        _dbRow.update_record(**_D_actualizar)
                        break
                    else:
                        pass
            else:
                # Si no se desea actualizar el registro, se manda el diccionario con los valores calculados
                _D_return.D_actualizar = _D_actualizar

            return _D_return

        @classmethod
        def CALCULAR_CAMPOS(
                cls,
                x_cfdi_concepto_detalle,
                b_recalcularCFDI = True
                ):
            """ Calcula campos, llenando campos automáticos que tienen dependencia númerica de otras tablas

            @param b_recalcularCFDI:
            @type b_recalcularCFDI:
            @param x_cfdi_concepto_detalle:
            @type x_cfdi_concepto_detalle:
            @return:
            @rtype:
            """

            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_concepto_detalle, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage()

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)

                    if b_recalcularCFDI:
                        _dbRow_concepto = dbc01.tcfdi_conceptos(_dbRow.cfdi_concepto_id)
                        # Si hay actualizaciones, debe actualizarse el CFDI, no los conceptos
                        _D_results = TCFDIS.PROC.CALCULAR_CAMPOS(
                            x_cfdi = _dbRow_concepto.cfdi_id
                            )
                    else:
                        pass
                    break
                else:
                    pass

            return _D_return

    class INGRESO(PROC, object):
        """ No es necesaria, para ingresos es suficiente la clase principal del concepto.
         Si se requieren más adecuaciones se pudiera crear una clase única """

        def configuracampos_edicion(
                self,
                x_cfdi_concepto,
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param x_cfdi_concepto:
            @type x_cfdi_concepto:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_concepto, self.dbTabla)
            _dbRow = _D_results.dbRow

            # Se llenan los default obligados
            for _s_nombreCampo in self._L_requeridos:
                self._D_defaults[_s_nombreCampo] = _D_results.dbRow[_s_nombreCampo]

            _D_results = self.PUEDE_EDITAR(_dbRow)

            if _D_results.E_return == CLASS_e_RETURN.OK:
                # Si puede editar regresa un ok y el movimiento esta abierto

                # Se manda vacío el argumento para no agregar defaults a los campos
                _D_results = self.configuracampos_nuevo(
                    None,
                    b_llamadaDesdeEditar = True,
                    **self._D_defaults
                    )
                _D_return.combinar(_D_results)

                _dbRow_prodserv = dbc01.tempresa_prodserv(_dbRow.empresa_prodserv_id)
                self.dbTabla.valorunitario.writable = True \
                    if _dbRow_prodserv.tipo != TEMPRESA_PRODSERV.E_TIPO.PRODUCTO else False

            else:
                # De lo contrario en cualquier otro caso, no puede editar nada de esta tabla
                for _s_fieldName in self.dbTabla.fields:
                    self.dbTabla[_s_fieldName].writable = False
                _D_return.agrega(CLASS_e_RETURN.OK_WARNING, _D_results.s_msgError, "No puede editar")

            return _D_return

        @classmethod
        def CALCULAR_CAMPOS(
                cls,
                x_cfdi_concepto,
                **D_params
                ):
            """ Recalcula campos

            @param x_cfdi_concepto:
            @type x_cfdi_concepto:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_concepto, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _f_tipocambio = dbc01.tcfdis(_dbRow.cfdi_id).tipocambio

            _D_results = TEMPRESA_PRODSERV.GET_PRECIOVENTA(
                x_prodserv = _dbRow.empresa_prodserv_id,
                f_precio = _dbRow.valorunitario_capturado,
                f_tipocambio = _f_tipocambio
                )
            _D_return.combinar(_D_results)

            _D_actualizar = Storage(
                valorunitario_capturado = _D_results.f_nuevoPrecio,
                valorunitario = _D_results.f_nuevoPrecio,
                descuento = 0,
                importe = 0
                )

            if _D_results.f_precioCasco1a1:
                # Si el producto tiene relación de casco, es necesario conocer si el precio se ajusta en el concepto
                _dbRows_cfdi = dbc01(
                    dbc01.tcfdis.id == _dbRow.cfdi_id
                    ).select(
                        dbc01.tcfdis.id,
                        dbc01.tcfdis.facturacomplementos,
                        )

                if _dbRows_cfdi.first().facturacomplementos == TEMPRESA_CLIENTES.E_FACTURARCOMPLEMENTOS.AJUSTARPRECIO:
                    # Si el esquema modifica el precio del producto
                    _D_actualizar.valorunitario += _D_results.f_precioCasco1a1
                else:
                    pass

            else:
                pass

            _dbRows_descuentos = _dbRow.tcfdi_concepto_descuentos.select()
            for _dbRow_descuento in _dbRows_descuentos:
                _D_actualizar.descuento += _dbRow_descuento.descuento

            _D_actualizar.importe = _dbRow.cantidad * _D_actualizar.valorunitario

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)

                    # Si hay actualizaciones, debe actualizarse el CFDI, no los conceptos
                    _dbRows_impuestosTras = _dbRow.tcfdi_concepto_impuestostrasladados.select()
                    for _dbRow_impuesto in _dbRows_impuestosTras:
                        _D_results = TCFDI_CONCEPTO_IMPUESTOSTRASLADADOS.PROC.CALCULAR_CAMPOS(
                            x_cfdi_concepto_imptras = _dbRow_impuesto,
                            b_recalcularCFDI = False
                            )
                    _D_results = TCFDIS.PROC.CALCULAR_CAMPOS(
                        x_cfdi = _dbRow.cfdi_id
                        )
                    break
                else:
                    pass

            return _D_return

        pass  # Ingreso

    class PAGO(PROC, object):
        """ No es necesaria, para pagos solo es un registro que se crea de forma automática """

    class EGRESO(PROC, object):
        """ No es necesaria, para egresos no hay un caso especial """

        def configuracampos_edicion(
                self,
                x_cfdi_concepto,
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param x_cfdi_concepto:
            @type x_cfdi_concepto:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_concepto, self.dbTabla)
            _dbRow = _D_results.dbRow

            # Se llenan los default obligados
            for _s_nombreCampo in self._L_requeridos:
                self._D_defaults[_s_nombreCampo] = _D_results.dbRow[_s_nombreCampo]

            _D_results = self.PUEDE_EDITAR(_dbRow)

            if _D_results.E_return == CLASS_e_RETURN.OK:
                # Si puede editar regresa un ok y el movimiento esta abierto

                # Se manda vacío el argumento para no agregar defaults a los campos
                _D_results = self.configuracampos_nuevo(
                    None,
                    b_llamadaDesdeEditar = True,
                    **self._D_defaults
                    )
                _D_return.combinar(_D_results)

                self.dbTabla.valorunitario_capturado.writable = True

            else:
                # De lo contrario en cualquier otro caso, no puede editar nada de esta tabla
                for _s_fieldName in self.dbTabla.fields:
                    self.dbTabla[_s_fieldName].writable = False
                _D_return.agrega(CLASS_e_RETURN.OK_WARNING, _D_results.s_msgError, "No puede editar")

            return _D_return

        @classmethod
        def CALCULAR_CAMPOS(
                cls,
                x_cfdi_concepto,
                **D_params
                ):
            """ Recalcula campos

            @param x_cfdi_concepto:
            @type x_cfdi_concepto:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_concepto, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _dbRow_cfdi = dbc01.tcfdis(_dbRow.cfdi_id)

            if _dbRow_cfdi.tiporelacion_id == TTIPOSRELACIONES.NOTACREDITO:
                # Deberían ser puros servicios o bonificaciones
                # _D_results = TEMPRESA_PRODSERV.GET_PRECIOVENTA(
                #     x_prodserv = _dbRow.empresa_prodserv_id,
                #     f_precio = _dbRow.valorunitario_capturado,
                #     f_tipocambio = _dbRow_cfdi.tipocambio
                #     )
                # _D_return.combinar(_D_results)
                # _f_precioConcepto = _D_results.f_nuevoPrecio
                # TODO como determinar si el precio es correcto o limitado
                _f_precioConcepto = _dbRow.valorunitario_capturado

            else:
                # Si es devolución de mercancía, permitir configurar el precio; hasta que se tenga un control de
                #  que CFDI de ingreso se esta haciendo la devolución

                _f_precioConcepto = _dbRow.valorunitario_capturado

            # TODO si es un ajuste o garantía, debe permitir variación de precio? Cómo determinarlo

            _D_actualizar = Storage(
                valorunitario_capturado = _f_precioConcepto,
                valorunitario = _f_precioConcepto,
                descuento = 0,
                importe = 0
                )

            if _D_results.f_precioCasco1a1:
                # TODO revisar políticas que ayuden a determinar si la cantidad en precio 1a1 se puede o no editar
                pass

            else:
                pass

            _D_actualizar.importe = _dbRow.cantidad * _D_actualizar.valorunitario

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)

                    # Si hay actualizaciones, debe actualizarse el CFDI, no los conceptos
                    _dbRows_impuestosTras = _dbRow.tcfdi_concepto_impuestostrasladados.select()
                    for _dbRow_impuesto in _dbRows_impuestosTras:
                        _D_results = TCFDI_CONCEPTO_IMPUESTOSTRASLADADOS.PROC.CALCULAR_CAMPOS(
                            x_cfdi_concepto_imptras = _dbRow_impuesto,
                            b_recalcularCFDI = False
                            )
                    _D_results = TCFDIS.PROC.CALCULAR_CAMPOS(
                        x_cfdi = _dbRow.cfdi_id
                        )
                    _D_return.combinar(_D_results)
                    break
                else:
                    pass

            return _D_return

        pass  # Egreso

    class TRASLADO(PROC, object):

        def __init__(self, **D_defaults):
            """ Inicializa objecto de tipo PROC para control del registro actual

            @param D_defaults:
            @type D_defaults:
            """
            super(TCFDI_CONCEPTOS.TRASLADO, self).__init__(**D_defaults)
            return

        def configuracampos_nuevo(
                self,
                s_cfdi_concepto_id,
                b_llamadaDesdeEditar = False,
                **D_defaults
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param b_llamadaDesdeEditar:
            @type b_llamadaDesdeEditar:
            @param s_cfdi_concepto_id:
            @type s_cfdi_concepto_id:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = super(TCFDI_CONCEPTOS.PROC, self).configuracampos_nuevo(
                s_cfdi_detalle_id = s_cfdi_concepto_id,
                b_llamadaDesdeEditar = b_llamadaDesdeEditar,
                **D_defaults
                )
            _D_return.combinar(_D_results)

            # Se configuran los campos especificos para el caso de esta clase
            self.dbTabla.cfdi_id.writable = False
            self.dbTabla.orden.writable = True
            self.dbTabla.cantidad.writable = True
            self.dbTabla.descuento.writable = False
            self.dbTabla.empresa_prodserv_id.writable = True
            self.dbTabla.valorunitario.writable = False
            self.dbTabla.valorunitario_capturado.writable = False

            # Campos que no deben ser editados, solo de forma autocalculada
            # Se configuran en configuracampos_nuevo del maestro

            # Se definen los valores por default
            # Se definen los valores por default sobreescribiendo los anteriores siendo especificos para esta clase
            self.dbTabla.cfdi_id.default = self._D_defaults.cfdi_id
            # self.dbTabla.orden.default = 100
            # self.dbTabla.cantidad.default = 1
            # self.dbTabla.unidad_id.default = TUNIDADES.PIEZA
            # self.dbTabla.descuento.default = 0
            # self.dbTabla.empresa_prodserv_id.default = None

            return _D_return

        @classmethod
        def AL_ACEPTAR(
                cls,
                O_form,
                **D_params
                ):
            """ Al aceptar el registro deben hacerse los cálculos

            Se hacen todos los cálculos y aceptación de los cambios en el producto

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()
            _dbRow = cls.dbTabla(O_form.record_id or O_form.vars.id)

            O_form = super(TCFDI_CONCEPTOS.PROC, cls).AL_ACEPTAR(O_form, _dbRow)

            _dbRows_cartaPorte = dbc01(
                dbc01.tcfdi_compcartaporte.cfdi_id == _dbRow.cfdi_id
                ).select(dbc01.tcfdi_compcartaporte.ALL)

            _dbRows_mercancias = dbc01(
                (dbc01.tcfdi_compcartaporte.cfdi_id == _dbRow.cfdi_id)
                & (dbc01.tcfdi_compcartaporte_mercancias.cfdi_compcartaporte_id == dbc01.tcfdi_compcartaporte.id)
                & (dbc01.tcfdi_compcartaporte_mercancias.claveprodservcp == _dbRow.claveprodserv)
                ).select(dbc01.tcfdi_compcartaporte_mercancias.ALL)

            if not _dbRows_cartaPorte:
                _D_return.agrega_error("No existe carta porte asociada para asignar mercancias")

            else:
                if _dbRows_mercancias:
                    _dbRow_mercancia = _dbRows_mercancias.first()
                else:
                    _dbRow_mercancia = None

                _dbRow_cartaPorte = _dbRows_cartaPorte.first()

                _dbRow_prodserv = dbc01.tempresa_prodserv(_dbRow.empresa_prodserv_id)
                _n_cveMaterialPeligroso = _dbRow_prodserv.cvematerialpeligroso_id if _dbRow_prodserv else None

                _x_mercancia = None
                if _dbRow_mercancia and (_dbRow_mercancia.cvematerialpeligroso_id == _n_cveMaterialPeligroso):
                    # Si se encontró el tipo de mercancía y la misma configuración de material peligroso
                    if O_form.record_id:
                        # Si esta editando el producto, se suma el delta del cambio
                        _dbRow_mercancia.cantidad += _dbRow.cantidad - O_form.record.cantidad
                    else:
                        _dbRow_mercancia.cantidad += _dbRow.cantidad
                    _dbRow_mercancia.update_record()
                    _x_mercancia = _dbRow_mercancia

                else:
                    _dbRows_prodservcp = db01(
                        db01.tprodservcp.c_claveprodservcp == _dbRow.claveprodserv
                        ).select(
                            db01.tprodservcp.id,
                            db01.tprodservcp.descripcion,
                            )

                    if _dbRows_prodservcp:
                        _s_prodservcp_id = _dbRows_prodservcp.first().id
                        _s_prodservcp_desc = _dbRows_prodservcp.first().descripcion
                    else:
                        _s_prodservcp_id = TPRODSERVCP.NO_EXISTE
                        _s_prodservcp_desc = ""

                    _D_insertar = Storage(
                        cfdi_compcartaporte_id = _dbRow_cartaPorte.id,
                        prodservcp_id = _s_prodservcp_id,
                        materialpeligroso = (
                            TCFDI_COMPCARTAPORTE_MERCANCIAS.E_MATERIALPELIGROSO.SI if _n_cveMaterialPeligroso
                            else TCFDI_COMPCARTAPORTE_MERCANCIAS.E_MATERIALPELIGROSO.NO
                            ),
                        cvematerialpeligroso_id = _n_cveMaterialPeligroso,
                        descripcion = _s_prodservcp_desc,
                        cantidad = _dbRow.cantidad,
                        unidad_id = _dbRow.unidad_id,
                        )
                    _x_mercancia = dbc01.tcfdi_compcartaporte_mercancias.insert(**_D_insertar)

                _D_results = TCFDI_COMPCARTAPORTE_MERCANCIAS.AL_ACEPTAR(
                    O_form = Storage(vars = _x_mercancia),
                    x_cfdi_compCartaPorte = _dbRow_cartaPorte
                    )
                _D_return.combinar(_D_results)

            generaMensajeFlash(cls.dbTabla, _D_return)
            return O_form

        @classmethod
        def CALCULAR_CAMPOS(
                cls,
                x_cfdi_concepto,
                **D_params
                ):
            """ Recalcula campos, haciendo la consulta de la carta porte a partir del CFDI

            @param x_cfdi_concepto:
            @type x_cfdi_concepto:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_concepto, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage(
                valorunitario_capturado = 0,
                valorunitario = 0,
                descuento = 0,
                importe = 0
                )

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)
                    break
                else:
                    pass

            return _D_return

        @classmethod
        def IMPORTAR_EXCEL(
                cls,
                s_pathArchivo,
                s_nombreHoja = None,
                **D_otros
                ):
            """ Se identifica el registro a través de la columna [No. Ident.]; y se importa [Cantidad].
            Si cantidad es cero, se ignora la importación del registro.
            Si el producto se intenta importar dos veces en el mismo archivo, se cancela la importación.
            Si el producto ya se importó anteriormente, se sobreescribe la información.

            @param s_pathArchivo:
            @type s_pathArchivo:
            @param s_nombreHoja:
            @type s_nombreHoja:
            @param D_otros: Contiene s_cfdi_id
            @type D_otros:
            @return:
            @rtype:
            """
            D_otros = Storage(D_otros)

            def alValidarRegistroExcel(
                    D_registro,
                    dbRow_encontrado,
                    dbRows_encontrados,
                    b_fueIdentificadoAntes,
                    b_hayVariasCoincidenciasEnTabla,
                    b_encontradoEnTabla
                    ):
                _ = dbRow_encontrado
                _ = dbRows_encontrados
                _ = b_hayVariasCoincidenciasEnTabla
                _ = b_encontradoEnTabla
                # noinspection PyShadowingNames
                _D_return = FUNC_RETURN(
                    E_resultado_validacion = IMPORTAR_EXCEL.E_RESULTADO_VALIDACION.OK
                    )
                _s_campo = dbc01.tcfdi_conceptos.noidentificacion.name

                if _s_campo not in D_registro:
                    _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                    _D_return.s_msgError = "Campo %s no mapeado en registro de excel" % _s_campo

                elif b_fueIdentificadoAntes:
                    _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                    _D_return.s_msgError = "El producto %s se encuentra repetido en el excel" % str(
                        D_registro.noidentificacion
                        )
                else:
                    if D_registro.cantidad == 0:
                        # Se ignora el regsitro si no tienen cantidad máxima definida
                        _D_return.E_resultado_validacion = IMPORTAR_EXCEL.E_RESULTADO_VALIDACION.IGNORARLO
                    else:
                        pass

                return _D_return

            def alAceptarRegistroExcel(
                    D_registro,
                    D_result,
                    ):

                # noinspection PyShadowingNames
                _D_results = cls.MAPEAR_CAMPOS(x_cfdi_concepto = D_registro.id)
                D_result.update(_D_results)

                return D_result

            _D_return = FUNC_RETURN()

            _D_results = TCFDIS.PROC.PUEDE_EDITAR(x_cfdi = dbc01.tcfdis(D_otros.s_cfdi_id))

            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.combinar(_D_results)

            else:
                _dbTabla = dbc01.tcfdi_conceptos
                _O_importar = IMPORTAR_EXCEL(
                    dbTabla = _dbTabla,
                    b_insertar_noRelacionados = True,
                    b_ignorar_noRelacionados = False,
                    b_insertarTodos = False,
                    fnIdentificarHeader = None,
                    alValidar = alValidarRegistroExcel,
                    alAceptar = alAceptarRegistroExcel
                    )

                _O_importar.agregar_campo_columna_relacion(
                    n_id_columna = None,
                    dbCampo = _dbTabla.cfdi_id,
                    E_formato = IMPORTAR_EXCEL.E_FORMATO.NUMERICO,
                    s_nombreColumna = None,  # Se relaciona sin nombre columna, como id maestro
                    n_columna = None,
                    x_valorFijo = D_otros.s_cfdi_id,
                    fnConversion = None
                    )
                _O_importar.agregar_campo_columna_relacion(
                    n_id_columna = None,
                    dbCampo = _dbTabla.noidentificacion,
                    E_formato = IMPORTAR_EXCEL.E_FORMATO.TEXTO,
                    s_nombreColumna = _dbTabla.noidentificacion.label,
                    n_columna = None,
                    x_valorFijo = None,
                    fnConversion = None
                    )

                _O_importar.agregar_campo_columna_importar(
                    n_id_columna = None,
                    dbCampo = _dbTabla.cantidad,
                    E_formato = IMPORTAR_EXCEL.E_FORMATO.DECIMAL,
                    s_nombreColumna = dbc01.tempresa_almacen_productos.cantidad.label,
                    n_columna = None,
                    x_valorFijo = None,
                    fnConversion = None
                    )

                _D_result = _O_importar.importar(
                    s_pathArchivo = s_pathArchivo,
                    s_nombreHoja = s_nombreHoja or 'Sheet1'
                    )

                _D_return.combinar(_D_result)

            return _D_return

        pass

    @staticmethod
    def REDEFINIR_IMPUESTOS(
            dbRow_concepto
            ):
        """ Crea los detalles de impuestos, en caso de no existir.

        @param dbRow_concepto:
        @type dbRow_concepto:
        @return:
        @rtype:
        """
        _D_return = FUNC_RETURN()

        # Se busca la sucursal para poder determiar los impuestos definidos
        _dbRows_sucursales = dbc01(
            (dbc01.tcfdis.id == dbRow_concepto.cfdi_id)
            & (dbc01.tempresa_plaza_sucursal_cajaschicas.id == dbc01.tcfdis.empresa_plaza_sucursal_cajachica_id)
            & (dbc01.tempresa_plaza_sucursales.id
               == dbc01.tempresa_plaza_sucursal_cajaschicas.empresa_plaza_sucursal_id)
            ).select(
                dbc01.tempresa_plaza_sucursales.ALL
                )

        if not _dbRows_sucursales:
            _D_return.agrega_error("Sucursal o Caja Chica no asociada correctamente a CFDI. ")
        else:
            _dbRow_sucursal = _dbRows_sucursales.first()  # Solo puede tener un registro
            _dbRows_esquemasImp = dbc01(
                (dbc01.tempresa_prodserv.id == dbRow_concepto.empresa_prodserv_id)
                & (dbc01.tempresa_prodserv_esquemasimptras.empresa_prodserv_id == dbc01.tempresa_prodserv.id)
                & (dbc01.tempresa_prodserv_esquemasimptras.bansuspender == False)
                & (dbc01.tempresa_esquemasimpuestostrasladados.id
                   == dbc01.tempresa_prodserv_esquemasimptras.esquemaimptras_id)
                & (dbc01.tempresa_esquemasimpuestostrasladados.bansuspender == False)
                ).select(
                    dbc01.tempresa_prodserv.descripcion,
                    dbc01.tempresa_esquemasimpuestostrasladados.impuesto_id,
                    dbc01.tempresa_esquemasimpuestostrasladados.tipofactor,
                    dbc01.tempresa_plaza_sucursal_esquemasimptras.tasaocuota,
                    dbc01.tempresa_plaza_sucursal_esquemasimptras.ejercicio,
                    left = [
                        dbc01.tempresa_plaza_sucursal_esquemasimptras.on(
                            (dbc01.tempresa_plaza_sucursal_esquemasimptras.empresa_plaza_sucursal_id
                             == _dbRow_sucursal.id)
                            & (dbc01.tempresa_plaza_sucursal_esquemasimptras.empresa_esquemaimptras_id
                               == dbc01.tempresa_esquemasimpuestostrasladados.id)
                            & (dbc01.tempresa_plaza_sucursal_esquemasimptras.bansuspender == False)
                            & (dbc01.tempresa_plaza_sucursal_esquemasimptras.ejercicio <= request.utcnow.year)
                            ),
                        ],
                    orderby = [~dbc01.tempresa_plaza_sucursal_esquemasimptras.ejercicio],
                    limitby = [0, 1]
                )

            if not _dbRows_esquemasImp:
                _dbRow_prodServ = dbc01.tempresa_prodserv(dbRow_concepto.empresa_prodserv_id)
                _D_return.agrega_error(
                    "Producto id %s no cuenta con esquema de impuestos activo. "
                    % _dbRow_prodServ.descripcion
                    )

            elif not _dbRows_esquemasImp.first().tempresa_plaza_sucursal_esquemasimptras.tasaocuota:
                _D_return.agrega_error(
                    "Producto %s no cuenta con esquema de impuestos activo en sucursal %s. "
                    % (_dbRows_esquemasImp.first().tempresa_prodserv.descripcion, _dbRow_sucursal.nombrecorto)
                    )

            else:
                for _dbRow_impuesto in _dbRows_esquemasImp:
                    _n_impuesto_id = _dbRow_impuesto.tempresa_esquemasimpuestostrasladados.impuesto_id
                    _s_tipofactor = _dbRow_impuesto.tempresa_esquemasimpuestostrasladados.tipofactor
                    _f_tasaocuota = _dbRow_impuesto.tempresa_plaza_sucursal_esquemasimptras.tasaocuota
                    if dbc01(
                            (dbc01.tcfdi_concepto_impuestostrasladados.cfdi_concepto_id == dbRow_concepto.id)
                            & (dbc01.tcfdi_concepto_impuestostrasladados.impuesto_id == _n_impuesto_id)
                            & (dbc01.tcfdi_concepto_impuestostrasladados.tipofactor == _s_tipofactor)
                            & (dbc01.tcfdi_concepto_impuestostrasladados.tasaocuota == _f_tasaocuota)
                            ).count() == 0:
                        _D_insertar = Storage(
                            cfdi_concepto_id = dbRow_concepto.id,
                            impuesto_id = _n_impuesto_id,
                            tipofactor = _s_tipofactor,
                            tasaocuota = _f_tasaocuota,
                            )

                        _D_insertar.id = dbc01.tcfdi_concepto_impuestostrasladados.insert(**_D_insertar)
                        TCFDI_CONCEPTO_IMPUESTOSTRASLADADOS.PROC.AL_ACEPTAR(
                            O_form = Storage(vars = _D_insertar),
                            b_calcularCampos = False  # Se van a calcular en donde se manda llamar esta redefinición
                            )
                    else:
                        # No hay problema, en el recálculo se ajustan los impuestos
                        pass

                    pass  # for _dbRow_impuesto
        return _D_return

    @classmethod
    def determinar_cascos_cantidades(
            cls,
            ):
        return

    def comando_refrescar(self, x_cfdi_concepto):
        _L_camposActualizar = STV_LIB_DB.COMANDO_REFRESCAR(
            x_cfdi_concepto,
            self,
            [
                self.importe,
                self.descuento,
                ]
            )
        return _L_camposActualizar

    pass


dbc01.define_table(
    TCFDI_CONCEPTOS.S_NOMBRETABLA, *TCFDI_CONCEPTOS.L_DBCAMPOS,
    format = '%(descripcion)s',
    singular = 'CFDI concepto',
    plural = 'CFDI conceptos',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TCFDI_CONCEPTOS
    )
# TODO parece que hay que incluir información aduanera por concepto en caso de CFDI de traspaso internacional


class TCFDI_CONCEPTO_IMPUESTOSTRASLADADOS(Table):

    S_NOMBRETABLA = 'tcfdi_concepto_impuestostrasladados'

    E_TIPOFACTOR = TEMPRESA_ESQUEMASIMPUESTOSTRASLADADOS.E_TIPOFACTOR

    L_DBCAMPOS = [
        Field(
            'cfdi_concepto_id', dbc01.tcfdi_conceptos, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'CFDI Concepto', comment = None,
            writable = False, readable = False,
            represent = None
            ),
        Field(
            'base', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Base', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),

        Field(
            'impuesto', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Impuesto', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'impuesto_id', 'integer', default = None,
            required = True, requires = IS_IN_DB(db01, 'timpuestos.id', db01.timpuestos._format),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Impuesto', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(
                v, r, dbc01.tcfdi_concepto_impuestostrasladados.impuesto_id
                )
            ),
        Field(
            'tipofactor', 'string', length = 10, default = E_TIPOFACTOR.TASA,
            required = True,
            requires = IS_IN_SET(E_TIPOFACTOR.get_dict(), zero = 0, error_message = 'Selecciona'),
            notnull = False, unique = False,
            widget = stv_widget_combobox, label = 'Tipo Factor', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_list(
                v, r, fnLambda = None,
                D_data = TCFDI_CONCEPTO_IMPUESTOSTRASLADADOS.E_TIPOFACTOR.get_dict(),
                dbField = dbc01.tcfdi_concepto_impuestostrasladados.tipofactor
                )
            ),
        Field(
            'tasaocuota', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputFloat, label = 'Tasa o Cuota', comment = None,
            writable = True, readable = True,
            represent = stv_represent_float
            ),
        Field(
            'importe', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Importe', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        tSignature_dbc01
        ]

    def __init__(self, dbRef, tablename, *fields, **args):
        Table.__init__(self, dbRef, tablename, *fields, **args)
        self.PROC.dbTabla = self
        return

    class PROC(TCFDI_CONCEPTOS.PROC_DETALLE, object):

        dbTabla = None  # Se escribe con la inicialización

        @classmethod
        def CONFIGURACAMPOS_DESDE_dbRow(cls, dbRow_base):
            # cls.dbTabla.cfdi_concepto_id.default = dbRow_base.cfdi_concepto_id
            # cls.dbTabla.base.default = dbRow_base.base
            # cls.dbTabla.impuesto.default = dbRow_base.impuesto
            cls.dbTabla.impuesto_id.default = dbRow_base.impuesto_id
            cls.dbTabla.tipofactor.default = dbRow_base.tipofactor
            cls.dbTabla.tasaocuota.default = dbRow_base.tasaocuota
            # cls.dbTabla.importe.default = dbRow_base.importe
            return

        def configuracampos_nuevo(
                self,
                s_cfdi_concepto_imptras_id,
                b_llamadaDesdeEditar = False,
                **D_defaults
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param b_llamadaDesdeEditar: Indica si la llamada a esta función es desde otra capa,
             desactivando validación de puede_crear
            @type b_llamadaDesdeEditar:
            @param s_cfdi_concepto_imptras_id:
            @type s_cfdi_concepto_imptras_id:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = super(TCFDI_CONCEPTO_IMPUESTOSTRASLADADOS.PROC, self).configuracampos_nuevo(
                s_cfdi_concepto_detalle_id = s_cfdi_concepto_imptras_id,
                b_llamadaDesdeEditar = b_llamadaDesdeEditar,
                **D_defaults
                )
            _D_return.combinar(_D_results)

            self.dbTabla.cfdi_concepto_id.writable = False
            self.dbTabla.impuesto_id.writable = True
            self.dbTabla.tasaocuota.writable = True
            self.dbTabla.tipofactor.writable = True

            # Campos que no deben ser editados, solo de forma autocalculada
            self.dbTabla.base.writable = False
            self.dbTabla.impuesto.writable = False
            self.dbTabla.importe.writable = False

            # Campos que todos los movimientos pueden editar
            self.dbTabla.notas.writable = False

            # Se definen los valores por default obligados
            self.dbTabla.cfdi_concepto_id.default = self._D_defaults.cfdi_concepto_id

            return _D_return

        def al_validar(
                self,
                O_form,
                **D_params
                ):
            """ Validar la información para poder grabar cfdi

            Las validaciones en esta función son genéricas, se espera que existan validaciones
             específicas por tipo de CFDI

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _dbRow = self.dbTabla(O_form.record_id) if O_form.record_id else None
            _D_camposAChecar = STV_FWK_LIB.OBTENER_CAMPOSACHECAR_PARA_VALIDAR(self.dbTabla, _dbRow)

            # Se llaman las validaciones del CFDI_relacionado, indicando que tiporelacion_id debe existsir
            O_form = super(TCFDI_CONCEPTO_IMPUESTOSTRASLADADOS.PROC, self).al_validar(
                O_form, _dbRow, D_camposAChecar = _D_camposAChecar
                )

            # Validaciones
            if not _D_camposAChecar.impuesto_id:
                O_form.errors.impuesto_id = "Impuesto no puede estar vacío"
            else:
                pass

            if not _D_camposAChecar.tasaocuota:
                O_form.errors.tasaocuota = "Tasa/Cuota no puede estar vacío"
            else:
                pass

            return O_form

        @classmethod
        def DESPUES_ELIMINAR(
                cls,
                L_errorsAction,
                ):
            """ Después de eliminar un registro

            @param L_errorsAction:
            @type L_errorsAction:
            @return: errores
            @rtype: lista de diccionarios {'msg':'...'}
            """
            _D_return = FUNC_RETURN()

            if cls._dbRow_aEliminar:
                # Aunque es detalle de concepto, modifica el cfdi y no el concepto
                _dbRow_concepto = dbc01.tcfdi_conceptos(cls._dbRow_aEliminar.cfdi_concepto_id)
                _D_results = TCFDIS.PROC.CALCULAR_CAMPOS(
                    x_cfdi = _dbRow_concepto.cfdi_id
                    )
                _D_return.combinar(_D_results)
            else:
                _D_return.E_return = CLASS_e_RETURN.NOK_SECUENCIA
                _D_return.s_msgError += "Error en secuencia de llamado de funciones para borrado. "

            generaMensajeFlash(cls.dbTabla, _D_return)
            return L_errorsAction

        @classmethod
        def MAPEAR_CAMPOS(
                cls,
                x_cfdi_concepto_imptras,
                b_actualizarRegistro = True
                ):
            """ Mapea campos, llenando campos automáticos

            @param b_actualizarRegistro:
            @type b_actualizarRegistro:
            @param x_cfdi_concepto_imptras:
            @type x_cfdi_concepto_imptras:
            @return:
            @rtype:
            """

            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_concepto_imptras, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage(
                impuesto = "",
                )

            _D_actualizar.impuesto = db01.timpuestos(_dbRow.impuesto_id).c_impuesto if _dbRow.impuesto_id else ""

            if b_actualizarRegistro:
                for _s_nombreCampo in _D_actualizar:
                    if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                        _dbRow.update_record(**_D_actualizar)
                        break
                    else:
                        pass
            else:
                # Si no se desea actualizar el registro, se manda el diccionario con los valores calculados
                _D_return.D_actualizar = _D_actualizar

            return _D_return

        @classmethod
        def CALCULAR_CAMPOS(
                cls,
                x_cfdi_concepto_imptras,
                b_recalcularCFDI = True
                ):
            """ Calcula campos, llenando campos automáticos que tienen dependencia númerica de otras tablas

            @param b_recalcularCFDI:
            @type b_recalcularCFDI:
            @param x_cfdi_concepto_imptras:
            @type x_cfdi_concepto_imptras:
            @return:
            @rtype:
            """

            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_concepto_imptras, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage(
                base = 0,
                importe = 0,
                )

            _dbRow_concepto = dbc01.tcfdi_conceptos(_dbRow.cfdi_concepto_id)

            _D_actualizar.base = _dbRow_concepto.importe
            if _dbRow.tipofactor == cls.dbTabla.E_TIPOFACTOR.TASA:
                _D_actualizar.importe = _D_actualizar.base * (_dbRow.tasaocuota or 0)
            elif _dbRow.tipofactor == cls.dbTabla.E_TIPOFACTOR.CUOTA:
                _D_actualizar.importe = _dbRow.tasaocuota or 0
            else:
                _D_return.agrega_error("Tipo factor no soportado, es necesario configurarlo a nivel empresa")

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)

                    if b_recalcularCFDI:
                        # Si hay actualizaciones, debe actualizarse el CFDI, no los conceptos
                        _D_results = TCFDIS.PROC.CALCULAR_CAMPOS(
                            x_cfdi = _dbRow_concepto.cfdi_id
                            )
                    else:
                        pass
                    break
                else:
                    pass

            return _D_return

    pass  # TCFDI_CONCEPTO_IMPUESTOSTRASLADADOS


dbc01.define_table(
    TCFDI_CONCEPTO_IMPUESTOSTRASLADADOS.S_NOMBRETABLA, *TCFDI_CONCEPTO_IMPUESTOSTRASLADADOS.L_DBCAMPOS,
    format = '%(cfdi_concepto_id)s',
    singular = 'CFDI Concepto Impuesto Trasladado',
    plural = 'CFDI Concepto Impuestos Trasladados',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TCFDI_CONCEPTO_IMPUESTOSTRASLADADOS
    )


class TCFDI_CONCEPTO_IMPUESTOSRETENIDOS(Table):

    S_NOMBRETABLA = 'tcfdi_concepto_impuestosretenidos'

    E_TIPOFACTOR = TEMPRESA_ESQUEMASIMPUESTOSTRASLADADOS.E_TIPOFACTOR

    L_DBCAMPOS = [
        Field(
            'cfdi_concepto_id', dbc01.tcfdi_conceptos, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'CFDI Concepto', comment = None,
            writable = False, readable = False,
            represent = None
            ),
        Field(
            'base', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Base', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),

        Field(
            'impuesto', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Impuesto', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'impuesto_id', 'integer', default = None,
            required = True, requires = IS_IN_DB(db01, 'timpuestos.id', db01.timpuestos._format),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Impuesto', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(
                v, r, dbc01.tcfdi_concepto_impuestostrasladados.impuesto_id
                )
            ),
        Field(
            'tipofactor', 'string', length = 10, default = E_TIPOFACTOR.TASA,
            required = True,
            requires = IS_IN_SET(E_TIPOFACTOR.get_dict(), zero = 0, error_message = 'Selecciona'),
            notnull = False, unique = False,
            widget = stv_widget_combobox, label = 'Tipo Factor', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_list(
                v, r, fnLambda = None,
                D_data = TCFDI_CONCEPTO_IMPUESTOSTRASLADADOS.E_TIPOFACTOR.get_dict(),
                dbField = dbc01.tcfdi_concepto_impuestostrasladados.tipofactor
                )
            ),
        Field(
            'tasaocuota', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputFloat, label = 'Tasa o Cuota', comment = None,
            writable = True, readable = True,
            represent = stv_represent_float
            ),
        Field(
            'importe', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Importe', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        tSignature_dbc01
        ]

    def __init__(self, dbRef, tablename, *fields, **args):
        Table.__init__(self, dbRef, tablename, *fields, **args)
        self.PROC.dbTabla = self
        return

    class PROC(TCFDI_CONCEPTOS.PROC_DETALLE, object):

        dbTabla = None  # Se escribe con la inicialización

        @classmethod
        def CONFIGURACAMPOS_DESDE_dbRow(cls, dbRow_base):
            # cls.dbTabla.cfdi_concepto_id.default = dbRow_base.cfdi_concepto_id
            # cls.dbTabla.base.default = dbRow_base.base
            # cls.dbTabla.impuesto.default = dbRow_base.impuesto
            cls.dbTabla.impuesto_id.default = dbRow_base.impuesto_id
            cls.dbTabla.tipofactor.default = dbRow_base.tipofactor
            cls.dbTabla.tasaocuota.default = dbRow_base.tasaocuota
            # cls.dbTabla.importe.default = dbRow_base.importe
            return

        def configuracampos_nuevo(
                self,
                s_cfdi_concepto_imprete_id,
                b_llamadaDesdeEditar = False,
                **D_defaults
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param b_llamadaDesdeEditar: Indica si la llamada a esta función es desde otra capa,
             desactivando validación de puede_crear
            @type b_llamadaDesdeEditar:
            @param s_cfdi_concepto_imprete_id:
            @type s_cfdi_concepto_imprete_id:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = super(TCFDI_CONCEPTO_IMPUESTOSRETENIDOS.PROC, self).configuracampos_nuevo(
                s_cfdi_concepto_detalle_id = s_cfdi_concepto_imprete_id,
                b_llamadaDesdeEditar = b_llamadaDesdeEditar,
                **D_defaults
                )
            _D_return.combinar(_D_results)

            self.dbTabla.cfdi_concepto_id.writable = False
            self.dbTabla.impuesto_id.writable = True
            self.dbTabla.tasaocuota.writable = True
            self.dbTabla.tipofactor.writable = True

            # Campos que no deben ser editados, solo de forma autocalculada
            self.dbTabla.base.writable = False
            self.dbTabla.impuesto.writable = False
            self.dbTabla.importe.writable = False

            # Campos que todos los movimientos pueden editar
            self.dbTabla.notas.writable = False

            # Se definen los valores por default obligados
            self.dbTabla.cfdi_concepto_id.default = self._D_defaults.cfdi_concepto_id

            return _D_return

        def al_validar(
                self,
                O_form,
                **D_params
                ):
            """ Validar la información para poder grabar cfdi

            Las validaciones en esta función son genéricas, se espera que existan validaciones
             específicas por tipo de CFDI

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _dbRow = self.dbTabla(O_form.record_id) if O_form.record_id else None
            _D_camposAChecar = STV_FWK_LIB.OBTENER_CAMPOSACHECAR_PARA_VALIDAR(self.dbTabla, _dbRow)

            # Se llaman las validaciones del CFDI_relacionado, indicando que tiporelacion_id debe existsir
            O_form = super(TCFDI_CONCEPTO_IMPUESTOSRETENIDOS.PROC, self).al_validar(
                O_form, _dbRow, D_camposAChecar = _D_camposAChecar
                )

            # Validaciones
            if not _D_camposAChecar.impuesto_id:
                O_form.errors.impuesto_id = "Impuesto no puede estar vacío"
            else:
                pass

            if not _D_camposAChecar.tasaocuota:
                O_form.errors.tasaocuota = "Tasa/Cuota no puede estar vacío"
            else:
                pass

            return O_form

        @classmethod
        def DESPUES_ELIMINAR(
                cls,
                L_errorsAction,
                ):
            """ Después de eliminar un registro

            @param L_errorsAction:
            @type L_errorsAction:
            @return: errores
            @rtype: lista de diccionarios {'msg':'...'}
            """
            _D_return = FUNC_RETURN()

            if cls._dbRow_aEliminar:
                # Aunque es detalle de concepto, modifica el cfdi y no el concepto
                _dbRow_concepto = dbc01.tcfdi_conceptos(cls._dbRow_aEliminar.cfdi_concepto_id)
                _D_results = TCFDIS.PROC.CALCULAR_CAMPOS(
                    x_cfdi = _dbRow_concepto.cfdi_id
                    )
                _D_return.combinar(_D_results)
            else:
                _D_return.E_return = CLASS_e_RETURN.NOK_SECUENCIA
                _D_return.s_msgError += "Error en secuencia de llamado de funciones para borrado. "

            generaMensajeFlash(cls.dbTabla, _D_return)
            return L_errorsAction

        @classmethod
        def MAPEAR_CAMPOS(
                cls,
                x_cfdi_concepto_imprete,
                b_actualizarRegistro = True
                ):
            """ Mapea campos, llenando campos automáticos

            @param b_actualizarRegistro:
            @type b_actualizarRegistro:
            @param x_cfdi_concepto_imprete:
            @type x_cfdi_concepto_imprete:
            @return:
            @rtype:
            """

            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_concepto_imprete, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage(
                impuesto = "",
                )

            _D_actualizar.impuesto = db01.timpuestos(_dbRow.impuesto_id).c_impuesto if _dbRow.impuesto_id else ""

            if b_actualizarRegistro:
                for _s_nombreCampo in _D_actualizar:
                    if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                        _dbRow.update_record(**_D_actualizar)
                        break
                    else:
                        pass
            else:
                # Si no se desea actualizar el registro, se manda el diccionario con los valores calculados
                _D_return.D_actualizar = _D_actualizar

            return _D_return

        @classmethod
        def CALCULAR_CAMPOS(
                cls,
                x_cfdi_concepto_imprete,
                b_recalcularCFDI = True
                ):
            """ Calcula campos, llenando campos automáticos que tienen dependencia númerica de otras tablas

            @param b_recalcularCFDI:
            @type b_recalcularCFDI:
            @param x_cfdi_concepto_imprete:
            @type x_cfdi_concepto_imprete:
            @return:
            @rtype:
            """

            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_concepto_imprete, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage(
                base = 0,
                importe = 0,
                )

            _dbRow_concepto = dbc01.tcfdi_conceptos(_dbRow.cfdi_concepto_id)

            _D_actualizar.base = _dbRow_concepto.importe
            if _dbRow.tipofactor == cls.dbTabla.E_TIPOFACTOR.TASA:
                _D_actualizar.importe = _D_actualizar.base * (_dbRow.tasaocuota or 0)
            elif _dbRow.tipofactor == cls.dbTabla.E_TIPOFACTOR.CUOTA:
                _D_actualizar.importe = _dbRow.tasaocuota or 0
            else:
                _D_return.agrega_error("Tipo factor no soportado, es necesario configurarlo a nivel empresa")

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)

                    if b_recalcularCFDI:
                        # Si hay actualizaciones, debe actualizarse el CFDI, no los conceptos
                        _D_results = TCFDIS.PROC.CALCULAR_CAMPOS(
                            x_cfdi = _dbRow_concepto.cfdi_id
                            )
                    else:
                        pass
                    break
                else:
                    pass

            return _D_return

    pass  # TCFDI_CONCEPTO_IMPUESTOSRETENIDOS


dbc01.define_table(
    TCFDI_CONCEPTO_IMPUESTOSRETENIDOS.S_NOMBRETABLA, *TCFDI_CONCEPTO_IMPUESTOSRETENIDOS.L_DBCAMPOS,
    format = '%(cfdi_concepto_id)s',
    singular = 'CFDI Concepto Impuesto Retenido',
    plural = 'CFDI Concepto Impuestos Retenciones',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TCFDI_CONCEPTO_IMPUESTOSRETENIDOS
    )


class TCFDI_CONCEPTO_DESCUENTOS(Table):

    S_NOMBRETABLA = 'tcfdi_concepto_descuentos'

    E_TIPOFACTOR = TEMPRESA_ESQUEMASIMPUESTOSTRASLADADOS.E_TIPOFACTOR

    class E_TIPO:
        """ Se definen las opciónes del campo """
        NO_DEFINIDO = 0
        CASCO_1A1 = 1
        CASCO_EXCEDENTE = 2
        PRONTO_PAGO = 3
        CANTIDAD_COMPRA = 4
        PROMOCION = 5
        AMISTAD = 6
        OTRO = 7
        _D_dict = None

        @classmethod
        def get_dict(cls):
            if not cls._D_dict:
                cls._D_dict = {
                    # cls.NO_DEFINIDO   : 'No definido',
                    cls.CASCO_1A1      : 'Casco 1a1',
                    cls.CASCO_EXCEDENTE: 'Casco Excedente',
                    cls.PRONTO_PAGO    : 'Pronto Pago',
                    cls.CANTIDAD_COMPRA: 'Cantidad Compra',
                    cls.PROMOCION      : 'Promoción',
                    cls.AMISTAD        : 'Amistad',
                    cls.OTRO           : 'Otro',
                    }
            else:
                pass
            return cls._D_dict

    L_DBCAMPOS = [
        Field(
            'cfdi_concepto_id', dbc01.tcfdi_conceptos, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'CFDI Concepto', comment = None,
            writable = False, readable = False,
            represent = None
            ),
        Field(
            'tipo', 'integer', default = E_TIPO.OTRO,
            required = False, requires = IS_IN_SET(
                E_TIPO.get_dict(), zero = 0, error_message = 'Selecciona'
                ),
            notnull = False, unique = False,
            widget = stv_widget_combobox,
            label = 'Tipo', comment = 'Tipo de descuento',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(
                v, r, fnLambda = None,
                D_data = TCFDI_CONCEPTO_DESCUENTOS.E_TIPO.get_dict(),
                dbField = dbc01.tcfdi_concepto_descuentos.tipo
                )
            ),
        Field(
            'empresa_prodserv_casco_id', 'integer', default = None,
            required = False,
            requires = IS_NULL_OR(
                IS_IN_DB(
                    dbc01(
                        (dbc01.tempresa_prodserv.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id)
                        & (dbc01.tempresa_prodcategorias.id == dbc01.tempresa_prodserv.categoria_id)
                        & (dbc01.tempresa_prodcategorias.uso_cascos1a1 == True)
                        ),
                    'tempresa_prodserv.id',
                    dbc01.tempresa_prodserv._format
                    )
                ),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(
                f, v, 1,
                D_additionalAttributes = Storage(
                    vacio = 'No aplica',
                    L_addFieldsInfo = [
                        dbc01.tempresa_prodserv.preciofijo,
                        dbc01.tempresa_prodserv.excedente_preciopropuesto,
                        ]
                    )
                ),
            label = 'Casco', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(
                v, r, dbc01.tcfdi_concepto_descuentos.empresa_prodserv_casco_id
                )
            ),
        Field(
            'cantidad_cascos', 'decimal(14,4)', default = 1,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Cantidad Cascos', comment = None,
            writable = False, readable = True,
            represent = stv_represent_number_ifnotzero
            ),
        Field(
            'valor_casco', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Valor Casco', comment = None,
            writable = False, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        Field(
            'descuento', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Descuento', comment = None,
            writable = False, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        Field(
            'descripcion', 'string', length = 500, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Descripción', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        tSignature_dbc01,
        ]

    def __init__(self, dbRef, tablename, *fields, **args):
        Table.__init__(self, dbRef, tablename, *fields, **args)
        self.PROC.dbTabla = self
        return

    class PROC(TCFDI_CONCEPTOS.PROC_DETALLE, object):

        dbTabla = None  # Se escribe con la inicialización

        @classmethod
        def CONFIGURACAMPOS_DESDE_dbRow(cls, dbRow_base):
            cls.dbTabla.cfdi_concepto_id.default = dbRow_base.cfdi_concepto_id
            cls.dbTabla.tipo.default = dbRow_base.tipo
            cls.dbTabla.empresa_prodserv_casco_id.default = dbRow_base.empresa_prodserv_casco_id
            cls.dbTabla.cantidad_cascos.default = dbRow_base.cantidad_cascos
            # cls.dbTabla.valor_casco.default = dbRow_base.valor_casco
            cls.dbTabla.descuento.default = dbRow_base.descuento
            cls.dbTabla.descripcion.default = dbRow_base.descripcion
            return

        def configuracampos_nuevo(
                self,
                s_cfdi_concepto_descto_id,
                b_llamadaDesdeEditar = False,
                **D_defaults
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param b_llamadaDesdeEditar: Indica si la llamada a esta función es desde otra capa,
             desactivando validación de puede_crear
            @type b_llamadaDesdeEditar:
            @param s_cfdi_concepto_descto_id:
            @type s_cfdi_concepto_descto_id:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = super(TCFDI_CONCEPTO_DESCUENTOS.PROC, self).configuracampos_nuevo(
                s_cfdi_concepto_detalle_id = s_cfdi_concepto_descto_id,
                b_llamadaDesdeEditar = b_llamadaDesdeEditar,
                **D_defaults
                )
            _D_return.combinar(_D_results)

            self.dbTabla.cfdi_concepto_id.writable = False
            self.dbTabla.tipo.writable = True
            self.dbTabla.empresa_prodserv_casco_id.writable = True
            self.dbTabla.cantidad_cascos.writable = True
            self.dbTabla.descuento.writable = True
            self.dbTabla.valor_casco.writable = True  # Dependiendo del tipo puede ser editado en excedente
            self.dbTabla.descripcion.writable = True

            # Campos que no deben ser editados, solo de forma autocalculada

            # Campos que todos los movimientos pueden editar
            self.dbTabla.notas.writable = True

            # Se definen los valores por default obligados
            self.dbTabla.cfdi_concepto_id.default = self._D_defaults.cfdi_concepto_id

            return _D_return

        def al_validar(
                self,
                O_form,
                **D_params
                ):
            """ Validar la información para poder grabar cfdi

            Las validaciones en esta función son genéricas, se espera que existan validaciones
             específicas por tipo de CFDI

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _dbRow = self.dbTabla(O_form.record_id) if O_form.record_id else None
            _D_camposAChecar = STV_FWK_LIB.OBTENER_CAMPOSACHECAR_PARA_VALIDAR(self.dbTabla, _dbRow)

            # Se llaman las validaciones del CFDI_relacionado, indicando que tiporelacion_id debe existsir
            O_form = super(TCFDI_CONCEPTO_DESCUENTOS.PROC, self).al_validar(
                O_form, _dbRow, D_camposAChecar = _D_camposAChecar
                )

            # Validaciones
            if _D_camposAChecar.tipo in (
                    TCFDI_CONCEPTO_DESCUENTOS.E_TIPO.CASCO_1A1,
                    TCFDI_CONCEPTO_DESCUENTOS.E_TIPO.CASCO_EXCEDENTE
                    ):
                if not _D_camposAChecar.empresa_prodserv_casco_id:
                    O_form.errors.empresa_prodserv_id = "Producto o Servicio debe especificarse"
                elif not TEMPRESA_PRODSERV.ES_CASCO(_D_camposAChecar.empresa_prodserv_casco_id).b_resultado:
                    O_form.errors.empresa_prodserv_id = "El producto seleccionado no es casco"
                else:
                    pass

                if not _D_camposAChecar.cantidad_cascos:
                    O_form.errors.cantidad_cascos = "Cantidad de cascos es requerida"

                elif _D_camposAChecar.tipo == TCFDI_CONCEPTO_DESCUENTOS.E_TIPO.CASCO_1A1:
                    _dbRow_cfdi_concepto = dbc01(
                        dbc01.tcfdi_conceptos.id == _D_camposAChecar.cfdi_concepto_id
                        ).select(
                            dbc01.tcfdi_conceptos.id, dbc01.tcfdi_conceptos.cfdi_id
                            ).first()
                    _D_results = TCFDIS.OBTENER_CASCOS1A1_DISPONIBLES_EN_INGRESO(
                        s_cfdiIngreso_id = _dbRow_cfdi_concepto.cfdi_id,
                        s_ignorar_descuento_id = O_form.record_id  # Regresa None si esta creando el registro
                        )
                    _f_cantidad_disponible = _D_results.n_cantidad_cascos1a1_posible \
                        - _D_results.n_cantidad_cascos1a1_usada

                    if _D_camposAChecar.cantidad_cascos > _f_cantidad_disponible:
                        O_form.errors.cantidad_cascos = "Cantidad de cascos 1a1 excede la cantidad disponible %d" \
                            % _f_cantidad_disponible

                    else:
                        pass

                else:
                    # No hay problema de la cantidad de descuento por excedente que se puede usar
                    pass

            else:
                if not _D_camposAChecar.descripcion:
                    O_form.errors.descripcion = "Descripción del descuento es requerido"
                else:
                    pass

                if not _D_camposAChecar.descuento:
                    O_form.errors.descuento = "Descuento es requerido"
                else:
                    pass

            return O_form

        @classmethod
        def CALCULAR_CAMPOS(
                cls,
                x_cfdi_concepto_imprete,
                b_recalcularCFDI = True
                ):
            """ Calcula campos, llenando campos automáticos que tienen dependencia númerica de otras tablas

            @param b_recalcularCFDI:
            @type b_recalcularCFDI:
            @param x_cfdi_concepto_imprete:
            @type x_cfdi_concepto_imprete:
            @return:
            @rtype:
            """

            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_concepto_imprete, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage(
                valor_casco = 0,
                descripcion = _dbRow.descripcion,
                descuento = _dbRow.descuento
                )

            if _dbRow.empresa_prodserv_casco_id:
                # Si tiene definido el empresa_prodserv_casco_id
                _dbRow_prodServ = dbc01.tempresa_prodserv(_dbRow.empresa_prodserv_casco_id)
                _D_actualizar.descripcion = _dbRow_prodServ.descripcion

                _dbRow_concepto = dbc01.tcfdi_conceptos(_dbRow.cfdi_concepto_id)
                _f_tipocambio = dbc01.tcfdis(_dbRow_concepto.cfdi_id).tipocambio

                if _dbRow.tipo == TCFDI_CONCEPTO_DESCUENTOS.E_TIPO.CASCO_1A1:
                    _D_results = TEMPRESA_PRODSERV.GET_PRECIOVENTA(
                        x_prodserv = _dbRow.empresa_prodserv_casco_id,
                        f_precio = _dbRow.valor_casco,
                        f_tipocambio = _f_tipocambio
                        )
                    if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                        _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                        _D_return.s_msgError += _D_results.s_msgError
                        _D_actualizar.valor_casco = _D_results.f_precioMinimo
                    else:
                        # En caso de casco 1 a 1 el precio es fijo al minimo
                        _D_actualizar.valor_casco = _D_results.f_precioMinimo

                elif _dbRow.tipo == TCFDI_CONCEPTO_DESCUENTOS.E_TIPO.CASCO_EXCEDENTE:
                    _D_results = TEMPRESA_PRODSERV.GET_PRECIO_EXCEDENTE(
                        x_prodserv = _dbRow.empresa_prodserv_casco_id,
                        f_precio = _dbRow.valor_casco,
                        f_tipocambio = _f_tipocambio
                        )
                    if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                        _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
                        _D_return.s_msgError += _D_results.s_msgError
                        _D_actualizar.valor_casco = 0
                    else:
                        _D_actualizar.valor_casco = _D_results.f_nuevoPrecio

                else:
                    _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                    _D_return.s_msgError += "Tipo no correcto para definir ID de un casco. "
                    _D_actualizar.valor_casco = 0

                _D_actualizar.descuento = _dbRow.cantidad_cascos * _D_actualizar.valor_casco

            elif _dbRow.tipo in (
                    TCFDI_CONCEPTO_DESCUENTOS.E_TIPO.CASCO_1A1,
                    TCFDI_CONCEPTO_DESCUENTOS.E_TIPO.CASCO_EXCEDENTE):

                _D_actualizar.valor_casco = 0
                _D_actualizar.descuento = 0

            else:
                pass

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)

                    break
                else:
                    pass

            if b_recalcularCFDI:
                # Se actualizan los conceptos ya que puede darse el caso que el campo descuento
                #  se actualize por el mismo cálculo en javascript y la configuración del campo
                _D_results = TCFDI_CONCEPTOS.INGRESO.CALCULAR_CAMPOS(
                    x_cfdi_concepto = _dbRow.cfdi_concepto_id
                    )
            else:
                pass

            return _D_return

    pass  # TCFDI_CONCEPTO_DESCUENTOS


dbc01.define_table(
    TCFDI_CONCEPTO_DESCUENTOS.S_NOMBRETABLA, *TCFDI_CONCEPTO_DESCUENTOS.L_DBCAMPOS,
    format = '%(tipo)s %(descuento)f',
    singular = 'CFDI Concepto Descuento',
    plural = 'CFDI Concepto Descuentos',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TCFDI_CONCEPTO_DESCUENTOS
    )


class TCFDI_COMPLEMENTOPAGOS(Table):

    S_NOMBRETABLA = 'tcfdi_complementopagos'

    L_DBCAMPOS = [
        Field(
            'cfdi_id', dbc01.tcfdis, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'CFDI', comment = None,
            writable = False, readable = False,
            represent = None
            ),
        Field(
            'versionpago', 'string', length = 50, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Vesion', comment = '',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'fechapago_str', 'string', length = 50, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Fecha Pago', comment = '',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'fechapago', type = TDATETIME_GRABADIRECTO_LEERDIRECTO, default = request.browsernow,
            # Se graba como la fecha-hora local de emisión del CFDI
            required = False, requires = IS_NULL_OR(IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME)),
            notnull = False,
            widget = stv_widget_inputDateTime, label = 'Fecha Pago', comment = '',
            writable = False, readable = True,
            represent = stv_represent_datetime
            ),
        Field(
            'formapago', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Forma Pago', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'formapago_id', 'integer', default = TFORMASPAGO.EFECTIVO,
            required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tformaspago.id', db01.tformaspago._format)),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Forma Pago', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tcfdi_complementopagos.formapago_id)
            ),
        Field(
            'moneda', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Moneda', comment = '',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'moneda_id', 'integer', default = D_stvSiteHelper.dbconfigs.defaults.moneda_id,
            required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tmonedas.id', db01.tmonedas._format)),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Moneda', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tcfdis.moneda_id)
            ),

        # Este será el campo que se utilice en vez de moneda_id
        TEMPRESA_MONEDASCONTPAQI.DEFINIR_DBCAMPO('monedacontpaqi_id'),

        Field(
            'tipocambio', 'decimal(14,4)', default = 1,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Tipo de cambio', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        Field(
            'monto', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Monto', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),

        # Campo de apoyo para mostrar saldo pendiente por aplicar
        Field(
            'sumarelacionados', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Suma Relacionados', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_money(
                v, r, dbField = dbc01.tcfdi_complementopagos.sumarelacionados
                )
            ),
        tSignature_dbc01,
        ]

    def __init__(self, dbRef, tablename, *fields, **args):
        Table.__init__(self, dbRef, tablename, *fields, **args)
        self.PROC.dbTabla = self
        return

    @staticmethod
    def asociar_uuid(s_cfdi_complementopago_id):
        """ Se busca y asocian ligas rotas en a CFDIs relacionados a los pagos
        """

        _D_return = FUNC_RETURN(
            L_msgError = [],
            E_return = CLASS_e_RETURN.NOK_ERROR,
            n_updatedRows = 0
            )

        # Ejemplo puede usar
        # tcfdi_relacionados.id = 8419 de tcfdis.id = 47696
        # tcfdis.id = 47762

        _dbTableCFDIsBuscar = dbc01.tcfdis.with_alias('cfdisbuscar')
        _dbRows_cfdiRelacionados = dbc01(
            (dbc01.tcfdi_complementopago_doctosrelacionados.cfdi_complementopago_id == s_cfdi_complementopago_id)
            & (dbc01.tcfdi_complementopago_doctosrelacionados.cfdirelacionado_id == None)
            & (
                dbc01.tcfdi_complementopagos.id
                == dbc01.tcfdi_complementopago_doctosrelacionados.cfdi_complementopago_id
                )
            & (dbc01.tcfdis.id == dbc01.tcfdi_complementopagos.cfdi_id)
            & (_dbTableCFDIsBuscar.uuid == dbc01.tcfdi_complementopago_doctosrelacionados.iddocumento)
            & (_dbTableCFDIsBuscar.empresa_id == dbc01.tcfdis.empresa_id)
            ).select(
            dbc01.tcfdi_complementopago_doctosrelacionados.ALL,
            _dbTableCFDIsBuscar.id,
            )

        if len(_dbRows_cfdiRelacionados) == 0:
            # CFDI asociado con el UUID no se encontró
            _D_return.L_msgError += ["No se encontraron cfdis relacionados"]
            _D_return.E_return = CLASS_e_RETURN.OK
        else:
            # Se actualizarlan los documentos que requiren asignación
            for _dbRow in _dbRows_cfdiRelacionados:
                _D_return.n_updatedRows += dbc01(
                    dbc01.tcfdi_complementopago_doctosrelacionados.id
                    == _dbRow.tcfdi_complementopago_doctosrelacionados.id
                    ).update(cfdirelacionado_id = _dbRow.cfdisbuscar.id)
            _D_return.L_msgError += ["Se actualizo el registro con la relación al CFDI"]
            _D_return.E_return = CLASS_e_RETURN.OK

        return _D_return

    def comando_refrescar(self, x_compPago):
        _L_camposActualizar = STV_LIB_DB.COMANDO_REFRESCAR(
            x_compPago,
            self,
            [
                self.sumarelacionados,
                ]
            )
        return _L_camposActualizar

    class PROC(TCFDIS.PROC_DETALLE, object):

        dbTabla = None  # Se escribe con la inicialización

        def puede_crear(self, x_cfdi, **D_defaults):
            """ Determina si puede o no crear un CFDI de traslado.

            Actualmente no hay condición requerida.
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi, dbc01.tcfdis)
            _dbRow_cfdi = _D_results.dbRow

            _D_results = super(TCFDI_COMPLEMENTOPAGOS.PROC, self).puede_crear(
                x_cfdi = _dbRow_cfdi,
                D_requeridosMaestro = Storage(
                    tipocomprobante_id = Storage(
                        L_valores = [TTIPOSCOMPROBANTES.PAGO],
                        s_msgError = "Tipo Comprobante debe ser Pago"
                        )
                    ),
                **D_defaults
                )
            _D_return.combinar(_D_results)

            return _D_return

        @classmethod
        def CONFIGURACAMPOS_DESDE_dbRow(cls, dbRow_base):
            # cls.dbTabla.cfdi_id.default = dbRow_base.cfdi_id
            # cls.dbTabla.versionpago.default = dbRow_base.versionpago
            # cls.dbTabla.fechapago_str.default = dbRow_base.fechapago_str
            cls.dbTabla.fechapago.default = dbRow_base.fechapago
            # cls.dbTabla.formapago.default = dbRow_base.formapago
            cls.dbTabla.formapago_id.default = dbRow_base.formapago_id
            # cls.dbTabla.moneda.default = dbRow_base.moneda
            # cls.dbTabla.moneda_id.default = dbRow_base.moneda_id
            cls.dbTabla.monedacontpaqi_id.default = dbRow_base.monedacontpaqi_id
            cls.dbTabla.tipocambio.default = dbRow_base.tipocambio
            # cls.dbTabla.monto.default = dbRow_base.monto
            # cls.dbTabla.sumarelacionados.default = dbRow_base.sumarelacionados

            return

        def configuracampos_nuevo(self, s_cfdi_comppago_id, b_llamadaDesdeEditar = False, **D_defaults):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param b_llamadaDesdeEditar: Indica si la llamada a esta función es desde otra capa,
             desactivando validación de puede_crear
            @type b_llamadaDesdeEditar:
            @param s_cfdi_comppago_id:
            @type s_cfdi_comppago_id:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = super(TCFDI_COMPLEMENTOPAGOS.PROC, self).configuracampos_nuevo(
                s_cfdi_detalle_id = s_cfdi_comppago_id,
                b_llamadaDesdeEditar = b_llamadaDesdeEditar,
                **D_defaults
                )
            _D_return.combinar(_D_results)

            self.dbTabla.cfdi_id.writable = False
            self.dbTabla.fechapago.writable = True
            self.dbTabla.formapago_id.writable = True
            self.dbTabla.monedacontpaqi_id.writable = True
            self.dbTabla.tipocambio.writable = True
            self.dbTabla.monto.writable = True

            # Campos que no deben ser editados, solo de forma autocalculada
            self.dbTabla.versionpago.writable = False
            self.dbTabla.fechapago_str.writable = False
            self.dbTabla.formapago.writable = False
            self.dbTabla.moneda.writable = False
            self.dbTabla.moneda_id.writable = False
            self.dbTabla.sumarelacionados.writable = False

            # Campos que todos los movimientos pueden editar
            self.dbTabla.notas.writable = False

            # Se definen los valores por default obligados
            self.dbTabla.cfdi_id.default = self._D_defaults.cfdi_id
            self.dbTabla.versionpago.default = "1.0"

            return _D_return

        def configuracampos_edicion(self, x_cfdi_compPago):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param x_cfdi_compPago:
            @type x_cfdi_compPago:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_compPago, self.dbTabla)
            _dbRow = _D_results.dbRow

            _D_results = super(TCFDI_COMPLEMENTOPAGOS.PROC, self).configuracampos_edicion(
                x_cfdi_detalle = _dbRow
                )
            _D_return.combinar(_D_results)

            # Si ya tiene docrelacionados no puede cambiar el tipo de cambio ni la moneda
            if _dbRow.tcfdi_complementopago_doctosrelacionados.count() > 0:
                self.dbTabla.tipocambio.writable = False
                self.dbTabla.monedacontpaqi_id.writable = False
            else:
                pass

            return _D_return

        def al_validar(self, O_form, **D_params):
            """ Validar la información para poder grabar cfdi

            Las validaciones en esta función son genéricas, se espera que existan validaciones
             específicas por tipo de CFDI

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _dbRow = self.dbTabla(O_form.record_id) if O_form.record_id else None
            _D_camposAChecar = STV_FWK_LIB.OBTENER_CAMPOSACHECAR_PARA_VALIDAR(self.dbTabla, _dbRow, O_form)

            D_params = Storage(D_params)
            if not D_params.validacionDummy:
                # Se usa validacionDummy en D_paramas para poder validar capturas rápidas de pagos

                # Se llaman las validaciones del CFDI_relacionado, indicando que tiporelacion_id debe existsir
                O_form = super(TCFDI_COMPLEMENTOPAGOS.PROC, self).al_validar(
                    O_form, _dbRow, D_camposAChecar = _D_camposAChecar
                    )
            else:
                pass

            # Validaciones
            if not _D_camposAChecar.fechapago:
                O_form.errors.fechapago = "Fecha no puede estar vacío"
            elif _D_camposAChecar.fechapago > request.browsernow:
                O_form.errors.fechapago = "Fecha del pago no puede ser mayor a la fecha actual"
            else:
                pass
            if not _D_camposAChecar.formapago_id:
                O_form.errors.formapago_id = "Forma Pago no puede estar vacío"
            else:
                pass
            if not _D_camposAChecar.monedacontpaqi_id:
                O_form.errors.monedacontpaqi_id = "Moneda no puede estar vacío"
            else:
                pass
            if not Decimal(_D_camposAChecar.monto):
                O_form.errors.monto = "Monto no puede estar vacío"
            else:
                pass

            return O_form

        @classmethod
        def MAPEAR_CAMPOS(cls, x_cfdi_comppago, **D_params):
            """ Mapea campos, llenando campos automáticos

            @param x_cfdi_comppago:
            @type x_cfdi_comppago:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_comppago, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage(
                moneda_id = None,
                moneda = "",
                formapago = "",
                fechapago_str = ""
                )

            _D_actualizar.moneda_id = dbc01.tempresa_monedascontpaqi(_dbRow.monedacontpaqi_id).moneda_id \
                if _dbRow.monedacontpaqi_id else None
            _D_actualizar.moneda = db01.tmonedas(_D_actualizar.moneda_id).c_moneda \
                if _D_actualizar.moneda_id else None
            _D_actualizar.formapago = db01.tformaspago(_dbRow.formapago_id).c_formapago \
                if _dbRow.formapago_id else None
            _D_actualizar.fechapago_str = _dbRow.fechapago.strftime('%Y-%m-%dT%H:%M:%S')

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)
                    break
                else:
                    pass

            return _D_return

        @classmethod
        def CALCULAR_CAMPOS(
                cls,
                x_cfdi_comppago,
                b_actualizarRegistro = True,
                b_recalcularCFDI = True,
                **D_params
                ):
            """ Recalcula campos, haciendo la consulta de la carta porte a partir del CFDI

            @param b_recalcularCFDI:
            @type b_recalcularCFDI:
            @param b_actualizarRegistro:
            @type b_actualizarRegistro:
            @param x_cfdi_comppago:
            @type x_cfdi_comppago:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_comppago, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage(
                sumarelacionados = 0
                )

            _dbRows_docsRelacionados = _dbRow.tcfdi_complementopago_doctosrelacionados.select()

            for _dbRow_docRel in _dbRows_docsRelacionados:
                _D_actualizar.sumarelacionados += _dbRow_docRel.imppagado * (_dbRow.tipocambio or 1)

            if b_actualizarRegistro:
                for _s_nombreCampo in _D_actualizar:
                    if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                        _dbRow.update_record(**_D_actualizar)

                        break
                    else:
                        pass
            else:
                # Si no se desea actualizar el registro, se manda el diccionario con los valores calculados
                _D_return.D_actualizar = _D_actualizar

            if b_recalcularCFDI:
                # Debido a que puede cambiarse el mondo y aun asi no cambiar la suma de relacionados, siempre se
                #  recalcula el CFDI, para borrar errores
                _D_results = TCFDIS.PROC.CALCULAR_CAMPOS(x_cfdi = _dbRow.cfdi_id)
            else:
                pass

            return _D_return

        pass  # PROC

    pass  # TCFDI_COMPLEMENTOPAGOS


dbc01.define_table(
    TCFDI_COMPLEMENTOPAGOS.S_NOMBRETABLA, *TCFDI_COMPLEMENTOPAGOS.L_DBCAMPOS,
    format = '%(cfdi_id)s',
    singular = 'CFDI pago',
    plural = 'CFDI pagos',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TCFDI_COMPLEMENTOPAGOS,
    )


class TCFDI_COMPLEMENTOPAGO_DOCTOSRELACIONADOS(Table):

    S_NOMBRETABLA = 'tcfdi_complementopago_doctosrelacionados'
    L_DBCAMPOS = [
        Field(
            'cfdi_complementopago_id', dbc01.tcfdi_complementopagos, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'CFDI Complemento Pago', comment = None,
            writable = False, readable = False,
            represent = None
            ),
        Field(
            'iddocumento', 'string', length = 50, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'CFDI Relacionado UUID', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'cfdirelacionado_id', 'integer', default = None,  # Corresponde a tcfdi relacionado
            required = False,
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_search(
                f, v,
                s_display = '%(serie)s %(folio)s',
                s_url = URL(a = 'app_timbrado', c = '250cfdi', f = 'cfdi_buscar'),
                D_additionalAttributes = {'reference': dbc01.tcfdis.id}
                ),
            label = 'CFDI Relacionado', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(
                v, r, dbc01.tcfdi_complementopago_doctosrelacionados.cfdirelacionado_id
                )
            ),
        # Serie debe estar relacionado a la serie de la empresa
        Field(
            'serie', 'string', length = 50, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Serie', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'empresa_serie_id', dbc01.tempresa_series, default = None,
            required = False,
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Serie', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r)
            ),
        # Serie debe estar relacionado al folio en la empresa del tipo de documento
        Field(
            'folio', 'string', length = 50, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Folio', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'moneda', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Moneda', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'moneda_id', 'integer', default = D_stvSiteHelper.dbconfigs.defaults.moneda_id,
            required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tmonedas.id', db01.tmonedas._format)),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Moneda', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tcfdis.moneda_id)
            ),

        # Este será el campo que se utilice en vez de moneda_id
        Field(
            'monedacontpaqi_id', 'integer', default = D_stvSiteHelper.dbconfigs.defaults.moneda_contpaqi_id,
            required = False,
            requires = IS_NULL_OR(
                IS_IN_DB(dbc01, 'tempresa_monedascontpaqi.id', dbc01.tempresa_monedascontpaqi._format)
                ),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Moneda', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(
                v, r, dbc01.tcfdi_complementopago_doctosrelacionados.monedacontpaqi_id
                )
            ),

        Field(
            'tipocambio', 'decimal(14,4)', default = 1,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Tipo de cambio', comment = None,
            writable = False, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        Field(
            'metodopago', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Método de Pago', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'metodopago_id', 'integer', default = None,
            required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tmetodospago.id', db01.tmetodospago._format)),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Método de Pago', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(
                v, r, dbc01.tcfdi_complementopago_doctosrelacionados.metodopago_id
                )
            ),
        Field(
            'numparcialidad', 'integer', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputInteger, label = 'Número de Parcialidad',
            comment = 'Número de parcialidad, si es cero se recalcula',
            writable = False, readable = True,
            represent = stv_represent_number
            ),
        Field(
            'impsaldoant', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Saldo Anterior', comment = None,
            writable = False, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        Field(
            'imppagado', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Importe Pagado', comment = None,
            writable = False, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        Field(
            'impsaldoinsoluto', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Saldo Insoluto', comment = None,
            writable = False, readable = True,
            represent = stv_represent_money
            ),
        Field(
            'imppagado_monedapago', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Imp. Pagado Moneda Pago', comment = None,
            writable = False, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        tSignature_dbc01,
        ]

    def __init__(self, dbRef, tablename, *fields, **args):
        Table.__init__(self, dbRef, tablename, *fields, **args)
        self.PROC.dbTabla = self
        return

    @staticmethod
    def ASOCIAR_UUID(s_cfdi_complementopago_doctorelacionado_id):
        """ Se busca y asocian ligas rotas en a CFDIs relacionados
        """

        _D_return = FUNC_RETURN(
            L_msgError = [],
            E_return = CLASS_e_RETURN.NOK_ERROR,
            n_updatedRows = 0
            )

        # Ejemplo puede usar
        # tcfdi_relacionados.id = 8419 de tcfdis.id = 47696
        # tcfdis.id = 47762

        _dbTableCFDIsBuscar = dbc01.tcfdis.with_alias('cfdisbuscar')
        _dbRows_cfdiRelacionados = dbc01(
            (dbc01.tcfdi_complementopago_doctosrelacionados.id == s_cfdi_complementopago_doctorelacionado_id)
            & (
                dbc01.tcfdi_complementopagos.id
                == dbc01.tcfdi_complementopago_doctosrelacionados.cfdi_complementopago_id
                )
            & (dbc01.tcfdis.id == dbc01.tcfdi_complementopagos.cfdi_id)
            & (_dbTableCFDIsBuscar.uuid == dbc01.tcfdi_complementopago_doctosrelacionados.iddocumento)
            & (_dbTableCFDIsBuscar.empresa_id == dbc01.tcfdis.empresa_id)
            ).select(
            dbc01.tcfdi_complementopago_doctosrelacionados.ALL,
            _dbTableCFDIsBuscar.id,
            )

        if len(_dbRows_cfdiRelacionados) > 1:
            # Error, no debería de regresar mas de un registro
            _D_return.L_msgError += [
                "Se encontraron más de un registro relacionado, contacte al administrador %s" % s_cfdi_relacionado_id]
            _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
        elif len(_dbRows_cfdiRelacionados) == 0:
            # CFDI asociado con el UUID no se encontró
            _D_return.L_msgError += ["No se encontraron cfdis relacionados"]
            _D_return.E_return = CLASS_e_RETURN.OK
        else:
            # Si tiene solamente un registro, hay que actualizarlo
            _dbRow = _dbRows_cfdiRelacionados.last()
            _D_return.n_updatedRows += dbc01(
                dbc01.tcfdi_complementopago_doctosrelacionados.id == _dbRow.tcfdi_complementopago_doctosrelacionados.id
                ).update(cfdirelacionado_id = _dbRow.cfdisbuscar.id)
            _D_return.L_msgError += ["Se actualizo el regisro con la relación al CFDI"]
            _D_return.E_return = CLASS_e_RETURN.OK

        return _D_return

    @staticmethod
    def CONFIGURAR_INTEGRIDAD_REFERENCIAL():
        _dbTabla = dbc01.tcfdi_complementopago_doctosrelacionados
        _dbTabla.cfdirelacionado_id.type = 'reference tcfdis'
        _dbTabla.cfdirelacionado_id.ondelete = 'NO ACTION'
        _dbTabla.cfdirelacionado_id.requires = IS_IN_DB(
            dbc01,
            'tcfdis.id',
            dbc01.tcfdis._format
            )

        return None

    class PROC:

        dbTabla = None  # Se escribe con la inicialización

        def __init__(self, **D_defaults):
            """ Inicializa objecto de tipo PROC para control del registro actual

            @param D_defaults:
            @type D_defaults:
            """
            self._D_defaults = Storage(D_defaults)
            self._L_requeridos = ['cfdi_complementopago_id']
            return

        def puede_crear(self, x_cfdi_comppago, **D_defaults):
            """ Determina si puede o no crear un CFDI de traslado.

            Actualmente no hay condición requerida.

            """
            _D_return = FUNC_RETURN()

            self._D_defaults.update(D_defaults or {})

            for _s_requerido in self._L_requeridos:
                if _s_requerido not in self._D_defaults:
                    _D_return.agrega_error("Campo %s es requerido para generar CFDI" % _s_requerido)

                else:
                    pass

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_comppago, dbc01.tcfdi_complementopagos)
            _dbRow_cfdi_compPago = _D_results.dbRow

            _D_results = TCFDIS.PROC.PUEDE_EDITAR(_dbRow_cfdi_compPago.cfdi_id)
            _D_return.combinar(_D_results)

            return _D_return

        @classmethod
        def PUEDE_EDITAR(
                cls,
                x_cfdi_comppago_docrel
                ):
            """ Define si es posible editar el CFDI de traslado.
            @param x_cfdi_comppago_docrel:

            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_comppago_docrel, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_results = TCFDI_COMPLEMENTOPAGOS.PROC.PUEDE_EDITAR(_dbRow.cfdi_complementopago_id)
            _D_return.combinar(_D_results)

            return _D_return

        @classmethod
        def PUEDE_ELIMINAR(
                cls,
                x_cfdi_comppago_docrel
                ):
            """ Determina si puede o no eliminar un registro

            Misma condición que puede_editar, a lo mejor pudieran verse condiciones distintas

            """

            return cls.PUEDE_EDITAR(x_cfdi_comppago_docrel)

        @classmethod
        def CONFIGURACAMPOS_DESDE_dbRow(cls, dbRow_base):
            # cls.dbTabla.cfdi_complementopago_id.default = dbRow_base.cfdi_complementopago_id
            # cls.dbTabla.iddocumento.default = dbRow_base.iddocumento
            # cls.dbTabla.cfdirelacionado_id.default = dbRow_base.cfdirelacionado_id
            # cls.dbTabla.serie.default = dbRow_base.serie
            # cls.dbTabla.empresa_serie_id.default = dbRow_base.empresa_serie_id
            # cls.dbTabla.folio.default = dbRow_base.folio
            # cls.dbTabla.moneda.default = dbRow_base.moneda
            # cls.dbTabla.moneda_id.default = dbRow_base.moneda_id
            # cls.dbTabla.monedacontpaqi_id.default = dbRow_base.monedacontpaqi_id
            # cls.dbTabla.tipocambio.default = dbRow_base.tipocambio
            # cls.dbTabla.metodopago.default = dbRow_base.monto
            cls.dbTabla.metodopago_id.default = dbRow_base.metodopago_id
            # cls.dbTabla.numparcialidad.default = dbRow_base.numparcialidad
            # cls.dbTabla.impsaldoant.default = dbRow_base.impsaldoant
            cls.dbTabla.imppagado.default = dbRow_base.imppagado
            # cls.dbTabla.imppagado_monedapago.default = dbRow_base.imppagado_monedapago
            # cls.dbTabla.impsaldoinsoluto.default = dbRow_base.impsaldoinsoluto
            return

        def configuracampos_nuevo(
                self,
                s_cfdi_comppago_docrel_id,
                b_llamadaDesdeEditar = False,
                **D_defaults
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param b_llamadaDesdeEditar: Indica si la llamada a esta función es desde otra capa,
             desactivando validación de puede_crear
            @type b_llamadaDesdeEditar:
            @param s_cfdi_comppago_docrel_id:
            @type s_cfdi_comppago_docrel_id:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            self._D_defaults.update(D_defaults or {})
            for _s_fieldName in self.dbTabla.fields:
                self.dbTabla[_s_fieldName].writable = False
            for _s_fieldName in self._D_defaults:
                self.dbTabla[_s_fieldName].default = self._D_defaults[_s_fieldName]

            if not b_llamadaDesdeEditar:
                _D_results = self.puede_crear(x_cfdi_comppago = self._D_defaults.cfdi_complementopago_id)
            else:
                _D_results = _D_return

            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.combinar(_D_results)

            elif s_cfdi_comppago_docrel_id:
                _dbRow_base = self.dbTabla(s_cfdi_comppago_docrel_id)

                self.CONFIGURACAMPOS_DESDE_dbRow(_dbRow_base)

            else:
                # Si se requieren defaults en caso de no tener algún otro definido, ponerlo aqui
                pass

            self.dbTabla.cfdi_complementopago_id.writable = False
            self.dbTabla.cfdirelacionado_id.writable = True
            self.dbTabla.imppagado.writable = True
            self.dbTabla.numparcialidad.writable = False  # Se permite editar pero no en nuevo
            self.dbTabla.impsaldoant.writable = False  # Se permite editar pero no en nuevo
            self.dbTabla.impsaldoinsoluto.writable = False  # Se permite editar pero no en nuevo

            # Campos que no deben ser editados, solo de forma autocalculada
            self.dbTabla.iddocumento.writable = False
            self.dbTabla.serie.writable = False
            self.dbTabla.empresa_serie_id.writable = False
            self.dbTabla.metodopago_id.writable = False
            self.dbTabla.folio.writable = False
            self.dbTabla.moneda.writable = False
            self.dbTabla.moneda_id.writable = False
            self.dbTabla.monedacontpaqi_id.writable = False
            self.dbTabla.imppagado_monedapago.writable = False
            self.dbTabla.tipocambio.writable = False
            self.dbTabla.metodopago.writable = False

            # Campos que todos los movimientos pueden editar
            self.dbTabla.notas.writable = False

            # Se definen los valores por default obligados
            self.dbTabla.cfdi_complementopago_id.default = self._D_defaults.cfdi_complementopago_id

            return _D_return

        def configuracampos_edicion(
                self,
                x_cfdi_comppago_docrel,
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param x_cfdi_comppago_docrel:
            @type x_cfdi_comppago_docrel:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_comppago_docrel, self.dbTabla)
            _dbRow = _D_results.dbRow

            # Se llenan los default obligados
            for _s_nombreCampo in self._L_requeridos:
                self._D_defaults[_s_nombreCampo] = _D_results.dbRow[_s_nombreCampo]

            _D_results = self.PUEDE_EDITAR(_dbRow)

            if _D_results.E_return == CLASS_e_RETURN.OK:
                # Si puede editar regresa un ok y el movimiento esta abierto

                # Se manda vacío el argumento para no agregar defaults a los campos
                _D_results = self.configuracampos_nuevo(
                    s_cfdi_comppago_docrel_id = None,
                    b_llamadaDesdeEditar = True,
                    )
                _D_return.combinar(_D_results)

                # No permitir editarlo, ya que se asigna al crear el registro y todo se calcula en base a eso
                self.dbTabla.cfdirelacionado_id.writable = False
                self.dbTabla.numparcialidad.writable = True  # Se permite editar pero no en nuevo
                self.dbTabla.impsaldoant.writable = True  # Se permite editar pero no en nuevo
                self.dbTabla.impsaldoinsoluto.writable = True  # Se permite editar pero no en nuevo

            else:
                # De lo contrario en cualquier otro caso, no puede editar nada de esta tabla
                for _s_fieldName in self.dbTabla.fields:
                    self.dbTabla[_s_fieldName].writable = False
                _D_return.agrega(CLASS_e_RETURN.OK_WARNING, _D_results.s_msgError, "No puede editar")

            return _D_return

        def al_validar(
                self,
                O_form,
                **D_params
                ):
            """ Validar la información para poder grabar cfdi

            Las validaciones en esta función son genéricas, se espera que existan validaciones
             específicas por tipo de CFDI

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _dbRow = self.dbTabla(O_form.record_id) if O_form.record_id else None
            _D_camposAChecar = STV_FWK_LIB.OBTENER_CAMPOSACHECAR_PARA_VALIDAR(self.dbTabla, _dbRow, O_form)
            D_params = Storage(D_params)

            if not O_form.record_id:
                # Se esta creando el registro

                if not D_params.validacionDummy:
                    # Si se tiene complemento de pago definido, se verifica, pero si no se tiene
                    #  como es el caso de la inserción directa de pago. Se brinca esta validación.
                    _D_results = self.puede_crear(
                        x_cfdi_comppago = _D_camposAChecar.cfdi_complementopago_id,
                        **_D_camposAChecar
                        )
                    if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                        O_form.errors.id = _D_results.s_msgError
                    else:
                        pass
                else:
                    pass

            else:
                # Se esta modificando el registro, la llamada a configuraCampos_edicion ya definió con writable
                # que puede modificarse y que no.

                _D_results = self.PUEDE_EDITAR(_dbRow)
                if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                    O_form.errors.id = _D_results.s_msgError
                else:
                    pass

            # Validaciones

            if not _D_camposAChecar.cfdirelacionado_id:
                O_form.errors.cfdirelacionado_id = "CFDI relacionado no puede estar vacío"
            else:
                _dbRow_cfdiIngreso = dbc01.tcfdis(_D_camposAChecar.cfdirelacionado_id)
                if _dbRow_cfdiIngreso.sat_status != TCFDIS.SAT_STATUS.VIGENTE:
                    O_form.errors.cfdirelacionado_id = "CFDI relacionado debe estar vigente"
                elif _dbRow_cfdiIngreso.metodopago_id != TMETODOSPAGO.PARCIALIDADES:
                    O_form.errors.cfdirelacionado_id = "CFDI relacionado (%d) no es PPD y no permite pagos" \
                                                       % _dbRow_cfdiIngreso.id
                else:
                    pass

                if D_params.validacionDummy:
                    # Si no se tiene el complemento de pago. Se ignora la validación de fecha.
                    pass
                else:
                    _dbRow_cfdi_compPago = dbc01.tcfdi_complementopagos(_D_camposAChecar.cfdi_complementopago_id)
                    if _dbRow_cfdi_compPago.fechapago < _dbRow_cfdiIngreso.fecha:
                        O_form.errors.cfdirelacionado_id = "Fecha del pago no puede ser anterior a la fecha del " \
                            + "CFDI relacionado"
                    else:
                        pass
            # ImpPago si puede estar vacío, y si esta vacío se recalcula

            return O_form

        @classmethod
        def AL_ACEPTAR(
                cls,
                O_form,
                ):
            """ Al aceptar el registro deben hacerse los cálculos

            Se hacen todos los cálculos y aceptación de los cambios en el producto

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()
            _dbRow = cls.dbTabla(O_form.record_id or O_form.vars.id)

            _D_results = cls.MAPEAR_CAMPOS(_dbRow)
            _D_return.combinar(_D_results)
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                pass

            else:

                _D_results = cls.CALCULAR_CAMPOS(_dbRow)
                _D_return.combinar(_D_results)

            generaMensajeFlash(cls.dbTabla, _D_return)
            return O_form

        @classmethod
        def ANTES_ELIMINAR(
                cls,
                L_errorsAction,
                dbRow,
                ):
            """ Antes de eliminar un movimiento de inventario de este tipo

            @param dbRow:
            @type dbRow:
            @param L_errorsAction:
            @type L_errorsAction:
            @return: errores
            @rtype: lista de diccionarios {'msg':'...'}
            """
            _D_return = FUNC_RETURN()

            _D_results = cls.PUEDE_ELIMINAR(dbRow)
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                L_errorsAction.append({'msg': _D_results.s_msgError})
            else:
                cls._dbRow_aEliminar = Storage(dbRow.as_dict())

            return L_errorsAction

        @classmethod
        def DESPUES_ELIMINAR(
                cls,
                L_errorsAction,
                ):
            """ Después de eliminar un registro

            @param L_errorsAction:
            @type L_errorsAction:
            @return: errores
            @rtype: lista de diccionarios {'msg':'...'}
            """
            _D_return = FUNC_RETURN()

            if cls._dbRow_aEliminar:
                _D_results = TCFDI_COMPLEMENTOPAGOS.PROC.CALCULAR_CAMPOS(
                    cls._dbRow_aEliminar.cfdi_complementopago_id
                    )
                _D_return.combinar(_D_results)
            else:
                _D_return.E_return = CLASS_e_RETURN.NOK_SECUENCIA
                _D_return.s_msgError += "Error en secuencia de llamado de funciones para borrado. "

            generaMensajeFlash(cls.dbTabla, _D_return)
            return L_errorsAction

        @classmethod
        def MAPEAR_CAMPOS(
                cls,
                x_cfdi_comppago_docrel,
                ):
            """ Remapea campos solamente cuando se graba el registro

            @param x_cfdi_comppago_docrel:
            @type x_cfdi_comppago_docrel:
            @return:
            @rtype:
            """

            def calcular_montoAPagar(dbRow_compPago_docRel, dbRow_compPago, dbRow_cfdiIngreso):
                # noinspection PyShadowingNames
                _D_return = FUNC_RETURN(
                    f_montoAplicar = 0,
                    f_saldoCFDI = 0,
                    )

                # Se determina el monto del pago disponible
                _dbRows_cfdi_pago = dbc01(
                    (dbc01.tcfdi_complementopago_doctosrelacionados.cfdi_complementopago_id
                     == dbRow_compPago.id)
                    & (dbc01.tcfdi_complementopago_doctosrelacionados.id != dbRow_compPago_docRel.id)
                    ).select(
                    dbc01.tcfdi_complementopago_doctosrelacionados.imppagado,
                    dbc01.tcfdi_complementopago_doctosrelacionados.tipocambio,
                    dbc01.tcfdi_complementopago_doctosrelacionados.monedacontpaqi_id,
                    )

                _f_impPago_usado = 0  # Monto del pago usado en la moneda del pago
                for _dbRow_docRel in _dbRows_cfdi_pago:
                    _f_impPago_usado += _dbRow_docRel.imppagado * _dbRow_docRel.tipocambio \
                        / dbRow_compPago.tipocambio

                _f_montoDisponible = dbRow_compPago.monto - _f_impPago_usado

                # noinspection PyShadowingNames
                _D_return.f_saldoCFDI = dbRow_cfdiIngreso.saldo

                # Sería el saldo en moneda del pago
                _f_cfdiIngreso_saldo = _D_return.f_saldoCFDI * dbRow_cfdiIngreso.tipocambio \
                    / dbRow_compPago.tipocambio

                # Se establece el monto a aplicar en moneda del ingreso
                if _f_montoDisponible > _f_cfdiIngreso_saldo:
                    _D_return.f_montoAplicar = _f_cfdiIngreso_saldo * dbRow_compPago.tipocambio \
                        / dbRow_cfdiIngreso.tipocambio
                else:
                    # Se aplica el saldo en moneda del doc relacionado
                    _D_return.f_montoAplicar = _f_montoDisponible * dbRow_compPago.tipocambio \
                       / dbRow_cfdiIngreso.tipocambio

                return _D_return

            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_comppago_docrel, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage(
                iddocumento = "",
                serie = "",
                empresa_serie_id = None,
                folio = "",
                moneda = "",
                moneda_id = None,
                monedacontpaqi_id = None,
                imppagado = _dbRow.imppagado,
                impsaldoant = _dbRow.impsaldoant,
                tipocambio = 0,
                metodopago = "",
                metodopago_id = None,
                )

            _dbRow_cfdiIngreso = dbc01.tcfdis(_dbRow.cfdirelacionado_id)
            _dbRow_compPago = dbc01.tcfdi_complementopagos(_dbRow.cfdi_complementopago_id)

            if not _dbRow.imppagado:
                # Si el importe pagado es cero, se procede al cálculo del importe pagado acutomático
                _D_results = calcular_montoAPagar(_dbRow, _dbRow_compPago, _dbRow_cfdiIngreso)
                _D_actualizar.imppagado = _D_results.f_montoAplicar
                _D_actualizar.impsaldoant = _D_results.f_saldoCFDI
                _D_return.combinar(_D_results)

            elif not _dbRow.impsaldoant:
                # En el mapeo se calcula el saldo anterior, y antes de timbrar también
                _D_actualizar.impsaldoant = _dbRow_cfdiIngreso.saldo

            else:
                pass

            _D_actualizar.iddocumento = _dbRow_cfdiIngreso.uuid
            _D_actualizar.serie = _dbRow_cfdiIngreso.serie
            _D_actualizar.empresa_serie_id = _dbRow_cfdiIngreso.empresa_serie_id
            _D_actualizar.folio = _dbRow_cfdiIngreso.folio
            _D_actualizar.moneda = _dbRow_cfdiIngreso.moneda
            _D_actualizar.moneda_id = _dbRow_cfdiIngreso.moneda_id
            _D_actualizar.monedacontpaqi_id = _dbRow_cfdiIngreso.monedacontpaqi_id
            _D_actualizar.tipocambio = _dbRow_cfdiIngreso.tipocambio
            _D_actualizar.metodopago = _dbRow_cfdiIngreso.metodopago
            _D_actualizar.metodopago_id = _dbRow_cfdiIngreso.metodopago_id

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)
                    # Mapear campos no implica recalculo de maestro
                    break
                else:
                    pass

            return _D_return

        @classmethod
        def CALCULAR_CAMPOS(
                cls,
                x_cfdi_comppago_docrel,
                ):
            """ Recalcula campos, haciendo la consulta de la carta porte a partir del CFDI

            @param x_cfdi_comppago_docrel:
            @type x_cfdi_comppago_docrel:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_comppago_docrel, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage(
                impsaldoinsoluto = 0,
                numparcialidad = _dbRow.numparcialidad,
                imppagado_monedapago = 0
                )

            # Esto evita que se este calculando y que si se requiere recalcular, el usuario debe poner 0 en este campo
            if not _dbRow.numparcialidad or (_dbRow.numparcialidad == 0):

                _dbRows_pagos = dbc01(
                    (dbc01.tcfdi_complementopago_doctosrelacionados.cfdirelacionado_id == _dbRow.cfdirelacionado_id)
                    & (dbc01.tcfdi_complementopago_doctosrelacionados.id != _dbRow.id)
                    ).select(
                        dbc01.tcfdi_complementopago_doctosrelacionados.id,
                        )

                _D_actualizar.numparcialidad = len(_dbRows_pagos) + 1

            else:
                pass

            _D_actualizar.impsaldoinsoluto = _dbRow.impsaldoant - _dbRow.imppagado
            _dbRow_compPago = dbc01.tcfdi_complementopagos(_dbRow.cfdi_complementopago_id)
            _D_actualizar.imppagado_monedapago = _dbRow.imppagado / _dbRow_compPago.tipocambio

            if _dbRow.impsaldoant < _dbRow.imppagado:
                _D_return.E_return = CLASS_e_RETURN.OK_WARNING
                _D_return.s_msgError = "Saldo en Ingreso es menor al abono"
            else:
                pass

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)

                    # Si hay actualizaciones, debe actualizarse el CFDI, no los conceptos
                    _D_results = TCFDI_COMPLEMENTOPAGOS.PROC.CALCULAR_CAMPOS(_dbRow.cfdi_complementopago_id)
                    break
                else:
                    pass

            return _D_return

        pass  # PROC

    pass


dbc01.define_table(
    TCFDI_COMPLEMENTOPAGO_DOCTOSRELACIONADOS.S_NOMBRETABLA, *TCFDI_COMPLEMENTOPAGO_DOCTOSRELACIONADOS.L_DBCAMPOS,
    format = '%(id)s',
    singular = 'CFDI Docto. Relacionado',
    plural = 'CFDI Doctos. Relacionados',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TCFDI_COMPLEMENTOPAGO_DOCTOSRELACIONADOS
    )


class TCFDI_REGISTROPAGOS:
    """ Tabla de información adicional para el registro de los pagos

    Esta tabla se usa en relación 1:1 con TCFDIS en caso de cfdis de ingreso,
    teneindo el campo cfdi_complementopago_id igual a NULL.
    Esta tabla se usa en relación 1:1 con TCFDI_COMPLEMENTOPAGOS en caso de
    cfdois de pagos, teniendo definidos los campos cfdi_id y cfdi_complementopago_id
    """

    class E_TRATOEFECTIVO:
        """ Se definen las opciónes del campo """
        RETENIDOCAJA = 1
        DEPOSITADOBANCO = 2

        @classmethod
        def get_dict(cls):
            return {
                cls.RETENIDOCAJA   : 'Retenido en caja',
                cls.DEPOSITADOBANCO: 'Depositado a banco',
                }

    ESTATUS = TEMPRESA_ESTADOCUENTA_MOVIMIENTOS.ESTATUS

    class TIPO_CFDI:
        """ Se definen las opciónes del campo """
        VIGENTE = 0
        CANCELADO = 1

        @classmethod
        def get_dict(cls):
            return {
                cls.VIGENTE  : 'Vigente',
                cls.CANCELADO: 'Cancelado',
                }

    class BUSQUEDA_AVANZADA:

        @staticmethod
        def CONFIGURA(
                s_empresa_id,
                O_cat,
                **D_valoresDefault
                ):
            """

            @return:
            @rtype:
            """
            _ = s_empresa_id
            _D_return = FUNC_RETURN()

            _dbTabla = dbc01.tcfdi_registropagos
            # Variable que contendrá los campos a editar en la búsqueda avanzada.
            _L_dbFields = []

            D_valoresDefault = Storage(D_valoresDefault)

            # Se configura el uso del campo categorias para poder filtrar por varias clasificaciones.
            _dbField_fecha = _dbTabla.fechadeposito.clone()
            if D_valoresDefault.fechadeposito:
                _dbField_fecha.adv_default = [
                    _dbField_fecha.formatter(D_valoresDefault.fechadeposito[0]),
                    _dbField_fecha.formatter(D_valoresDefault.fechadeposito[1])
                    ]
            else:
                _dbField_fecha.adv_default = _dbField_fecha.default
            _dbField_fecha.widget = stv_widget_inputDateRange
            _dbField_fecha.comment = None

            _dbField_monto = Field(
                'monto', 'decimal(14,4)', default = None,
                required = False,
                notnull = False, unique = False,
                widget = stv_widget_inputMoneyRange, label = 'Monto', comment = None,
                writable = False, readable = True,
                represent = stv_represent_money_ifnotzero
                )
            _dbField_monto.adv_default = D_valoresDefault.monto or _dbField_monto.default

            _dbField_formapago = _dbTabla.formapago_id.clone()
            _dbField_formapago.adv_default = D_valoresDefault.formapago_id or _dbField_formapago.default
            _dbField_formapago.widget = lambda field, value: stv_widget_db_chosen(
                field, value, 20,
                )
            _dbField_formapago.length = 400

            _dbField_estatus = _dbTabla.estatus.clone()
            _dbField_estatus.adv_default = D_valoresDefault.estatus or _dbField_estatus.default
            _dbField_estatus.requires = IS_NULL_OR(_dbField_estatus.requires)
            _dbField_estatus.widget = lambda field, value: stv_widget_chosen(
                field, value, 20,
                )
            _dbField_estatus.length = 400

            _L_dbFields += [
                _dbField_fecha,
                _dbField_monto,
                _dbField_formapago,
                _dbField_estatus,
                ]

            # Se configura la edición de los campos.
            for _dbField in _L_dbFields:
                _dbField.writable = True
                _dbField.readable = True
                if _dbField.name in request.vars:
                    pass
                elif getattr(_dbField, 'adv_default', None) is not None:
                    request.vars[_dbField.name] = _dbField.adv_default
                else:
                    pass

            O_cat.cfgRow_advSearch(
                L_dbFields_formFactory_advSearch = _L_dbFields,
                dbRow_formFactory_advSearch = None,
                D_hiddenFields_formFactory_advSearch = {},
                s_style = 'bootstrap',
                L_buttons = []
                )

            return _D_return

        @staticmethod
        def GENERA_PREFILTRO(
                D_args,
                ):
            """

            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN(
                qry = True,
                dbTable = dbc01.tcfdi_registropagos
                )

            if D_args.s_accion == request.stv_fwk_permissions.btn_find.code:
                # Si esta procesando una búsqueda
                if request.vars.monto:
                    _qry_monto = True
                    # Esto se debe a que el monto no se esta actualizando en la tabla registropago para cfdis de pago
                    _qry_montoEnComplementoPago = True

                    if request.vars.monto[0]:
                        _qry_monto &= _D_return.dbTable.monto >= request.vars.monto[0]
                        _qry_montoEnComplementoPago &= dbc01.tcfdi_complementopagos.monto >= request.vars.monto[0]
                    else:
                        pass
                    if (
                            (request.vars.monto[1])
                            and (float(request.vars.monto[1]) > float(request.vars.monto[0]))
                            ):
                        _qry_monto &= _D_return.dbTable.monto <= request.vars.monto[1]
                        _qry_montoEnComplementoPago &= dbc01.tcfdi_complementopagos.monto <= request.vars.monto[1]
                    else:
                        pass

                    _D_return.qry &= _qry_monto | _qry_montoEnComplementoPago
                else:
                    pass

            else:
                pass

            return _D_return

    class TIPO_CONTABILIZACION_FECHA_CANCELACION:
        """ Se definen las opciónes del campo """
        EMISION = 0
        CANCELACION = 1

        @classmethod
        def get_dict(cls):
            return {
                cls.EMISION: 'Fecha emisión',
                cls.CANCELACION: 'Fecha cancelación',
                }

    pass


dbc01.define_table(
    'tcfdi_registropagos',
    Field(
        'cfdi_id', dbc01.tcfdis, default = None,
        required = True,
        ondelete = 'NO ACTION', notnull = True, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'CFDI', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r)
        ),
    Field(
        'cfdi_complementopago_id', dbc01.tcfdi_complementopagos, default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Complemento Pago', comment = None,
        writable = False, readable = False,
        represent = lambda v, r: stv_represent_referencefield(v, r)
        ),
    Field(
        'empresa_cuentabancaria_terminal_id', dbc01.tempresa_cuentabancaria_terminales, default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Terminal', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r)
        ),
    Field(
        'banco_id_cliente', 'integer', default = None,
        required = False, requires = IS_IN_DB(db01, 'tbancos.id', db01.tbancos._format),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Banco Cliente', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_cuentasbancarias.banco_id)
        ),
    Field(
        'tarjeta', 'string', length = 10, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Tajeta', comment = 'Últimos 4 dígitos de la tarjeta',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'autorizacion', 'string', length = 10, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Autorización', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'operacion', 'string', length = 10, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Operación', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'cheque', 'string', length = 10, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Cheque', comment = 'No. de cheque',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'empresa_cuentabancaria_id', dbc01.tempresa_cuentasbancarias, default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Cuenta depósito', comment = (
            'Cuenta donde se realiza el depósito'),
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r)
        ),
    Field(
        'cuenta_cliente', 'string', length = 20, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Cuenta', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'referencia', 'string', length = 20, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Referencia', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'claverastero', 'string', length = 10, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Clave Rastreo', comment = '',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'fechadeposito', 'date', default = None,
        required = False, requires = IS_NULL_OR(IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE)),
        notnull = False, unique = False,
        widget = stv_widget_inputDate, label = (
            'Fecha de depósito'), comment = 'Fecha en la que fue depositado el dinero.',
        writable = True, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'bancocontpaqi_id_cliente', dbc01.tempresa_bancoscontpaqi, default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Banco Cliente', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r)
        ),

    # Se agrega este campo para poder manejar más de 1 forma de pago en los CFDIs de ingreso.
    Field(
        'formapago_id', 'integer', default = None,
        required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tformaspago.id', db01.tformaspago._format)),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Forma Pago', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tcfdi_registropagos.formapago_id)
        ),

    Field(
        'monto', 'decimal(14,4)', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputMoney, label = 'Monto', comment = None,
        writable = True, readable = True,
        represent = stv_represent_money_ifnotzero
        ),

    Field(
        'diferencia', 'boolean', default = request.vars.get("sobrantefaltante", 0),
        required = False,
        notnull = False,
        widget = stv_widget_inputCheckbox, label = T('Es sobrante o faltante'), comment = None,
        writable = True, readable = True,
        represent = stv_represent_boolean
        ),

    Field(
        'tratoefectivo', 'integer', default = 0,
        required = False, requires = IS_IN_SET(
            TCFDI_REGISTROPAGOS.E_TRATOEFECTIVO.get_dict(), zero = 0, error_message = (
                'Selecciona')
            ),
        notnull = False, unique = False,
        widget = stv_widget_combobox,
        label = 'Trato del efectivo', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TCFDI_REGISTROPAGOS.E_TRATOEFECTIVO.get_dict())
        ),
    # TODOMejora un quality checkup debería checar por este campo y que debe existir un registro
    #  en tempresa_estadocuenta_conciliacionesbancarias relacionado
    Field(
        'estatus', 'integer', default = TCFDI_REGISTROPAGOS.ESTATUS.PENDIENTE,
        required = False, requires = IS_IN_SET(
            TCFDI_REGISTROPAGOS.ESTATUS.get_dict(), zero = 0, error_message = (
                'Selecciona')
            ),
        notnull = True, unique = False,
        widget = stv_widget_combobox,
        label = 'Estatus de la conciliación', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TCFDI_REGISTROPAGOS.ESTATUS.get_dict())
        ),
    Field(
        'descripcion', 'string', default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Descripción', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'tipo_cfdi', 'integer', default = TCFDI_REGISTROPAGOS.TIPO_CFDI.VIGENTE,
        required = False,
        requires = IS_IN_SET(TCFDI_REGISTROPAGOS.TIPO_CFDI.get_dict(), zero = 0, error_message = 'Selecciona'),
        notnull = True, unique = False,
        widget = stv_widget_combobox,
        label = 'Tipo de CFDI', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TCFDI_REGISTROPAGOS.TIPO_CFDI.get_dict())
        ),
    Field(
        'tipo_contabilizacion_fecha_cancelacion', 'integer',
        default = TCFDI_REGISTROPAGOS.TIPO_CONTABILIZACION_FECHA_CANCELACION.EMISION,
        required = False, requires = IS_IN_SET(
            TCFDI_REGISTROPAGOS.TIPO_CONTABILIZACION_FECHA_CANCELACION.get_dict(), zero = 0, error_message = (
                'Selecciona')
            ),
        notnull = False, unique = False,
        widget = stv_widget_combobox,
        label = 'Tipo de contabilización de la fecha de cancelación', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(
            v, r, None, TCFDI_REGISTROPAGOS.TIPO_CONTABILIZACION_FECHA_CANCELACION.get_dict()
            )
        ),
    tSignature_dbc01,
    format = '%(id)s',
    singular = 'Registro Pago',
    plural = 'Registros Pagos',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )


class TCFDI_MANEJOSALDOS(Table):
    """ En caso de que el CFDI tenga saldo a favor, esta tabla identifica a donde se aplicará ese saldo.

    Por lo pronto solo define un proceso de transferencia a otro CFDI, es posible que en un futuro se
    contemple otro proceso. Si se define otro tipo, verificar la cancelacion de CFDIs que actualmente checa que no tenga
    registros en esta tabla.

    También debe agregarse logica de cancelacion de este registro, ya que si existen movimientos posteriores,
     no deben eliminarse,
    solo cancelarse para no afectar estado de cuenta y saldos.

    """

    S_NOMBRETABLA = 'tcfdi_manejosaldos'

    class E_PROCESO:
        """ Se definen las opciónes del campo """
        NO_DEFINIDO = 0
        TRANSFERENCIA_A_CFDI = 1
        D_TODOS = {
            NO_DEFINIDO         : 'No definido',
            TRANSFERENCIA_A_CFDI: 'Transferencia a CFDI',
            }

        @classmethod
        def GET_DICT(cls):
            return cls.D_TODOS

    L_DBCAMPOS = [
        Field(
            'cfdi_id', dbc01.tcfdis, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'CFDI', comment = None,
            writable = False, readable = False,
            represent = None
            ),
        Field(
            'proceso', 'integer', default = E_PROCESO.NO_DEFINIDO,
            required = False,
            requires = IS_IN_SET(E_PROCESO.D_TODOS, zero = 0, error_message = 'Selecciona'),
            notnull = True, unique = False,
            widget = stv_widget_combobox,
            label = 'Proceso', comment = 'Proceso en el que se usará el total o parte del saldo a favor',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TCFDI_MANEJOSALDOS.E_PROCESO.D_TODOS)
            ),
        Field(
            'fecha', type = TDATETIME_GRABADIRECTO_LEERDIRECTO, default = request.browsernow,
            # Se graba como la fecha-hora local de emisión del CFDI
            required = True, requires = IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME),
            notnull = True,
            widget = stv_widget_inputDateTime, label = 'Fecha', comment = '',
            writable = True, readable = True,
            represent = stv_represent_datetime
            ),
        Field(
            'cfdirelacionado_id', dbc01.tcfdis, default = None,  # Corresponde a tcfdi relacionado
            required = False,
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_search(
                f,
                v,
                s_display = '%(serie)s %(folio)s',
                s_url = URL(
                    a = 'app_timbrado', c = '130clientes_cfdis', f = 'cliente_manejosaldo_cfdis_consaldo_buscar',
                    args = request.args[:6]
                    ),
                D_additionalAttributes = {}
                ),
            label = 'CFDI Receptor', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(
                v, r, dbc01.tcfdi_manejosaldos.cfdirelacionado_id
                )
            ),
        Field(
            'descripcion', 'string', length = 150, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Descripción', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'importe', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Importe', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        tSignature_dbc01,
        ]

    def __init__(self, dbRef, tablename, *fields, **args):
        Table.__init__(self, dbRef, tablename, *fields, **args)
        self.PROC.dbTabla = self

        return

    class PROC(TCFDIS.PROC_DETALLE, object):

        dbTabla = None  # Se escribe con la inicialización

        def puede_crear(self, x_cfdi, **D_defaults):
            """ Determina si puede o no crear un CFDI de pago.

            Actualmente no hay condición requerida.
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi, dbc01.tcfdis)
            _dbRow_cfdi = _D_results.dbRow

            self._D_defaults.update(D_defaults or {})

            for _s_requerido in self._L_requeridos:
                if _s_requerido not in self._D_defaults:
                    _D_return.agrega_error("Campo %s es requerido para generar Manejo de Saldo" % _s_requerido)

                else:
                    pass

            # TODOMejora checar si tiene permisos para ajustar saldo
            if not _dbRow_cfdi:
                _D_return.agrega_error("CFDI no identificado")

            elif _dbRow_cfdi.sat_status != TCFDIS.SAT_STATUS.VIGENTE:
                _D_return.agrega_error("CFDI no esta Vigente, no puede generar registro de manejo saldo")

            elif _dbRow_cfdi.saldo >= 0:
                _D_return.agrega_error("CFDI no tiene saldo a favor")

            else:
                pass

            return _D_return

        @classmethod
        def PUEDE_EDITAR(cls, x_cfdi_manejosaldo):
            """ Regresa True si el estado permite modificar, de lo contrario False

            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_manejosaldo, cls.dbTabla)
            _dbRow = _D_results.dbRow

            # TODOMejora checar si tiene permisos para ajustar saldo
            if not _dbRow:
                _D_return.agrega_error("Registro no identificado.")

            else:
                pass

            return _D_return

        @classmethod
        def CONFIGURACAMPOS_DESDE_dbRow(cls, dbRow_base):
            self.dbTabla.cfdi_id.default = _dbRow_base.cfdi_id
            self.dbTabla.proceso.default = _dbRow_base.proceso
            # self.dbTabla.fecha.default = _dbRow_base.fecha
            # self.dbTabla.cfdirelacionado_id.default = _dbRow_base.cfdirelacionado_id
            self.dbTabla.descripcion.default = _dbRow_base.descripcion
            # self.dbTabla.importe.default = _dbRow_base.importe
            return

        def configuracampos_nuevo(self, s_cfdi_manejosaldo_id, b_llamadaDesdeEditar = False, **D_defaults):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param b_llamadaDesdeEditar:
            @type b_llamadaDesdeEditar:
            @param s_cfdi_manejosaldo_id:
            @type s_cfdi_manejosaldo_id:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = super(TCFDI_MANEJOSALDOS.PROC, self).configuracampos_nuevo(
                s_cfdi_detalle_id = s_cfdi_relacion_id,
                b_llamadaDesdeEditar = b_llamadaDesdeEditar,
                **D_defaults
                )
            _D_return.combinar(_D_results)

            self.dbTabla.cfdi_id.writable = False
            self.dbTabla.proceso.writable = True
            self.dbTabla.fecha.writable = True
            self.dbTabla.cfdirelacionado_id.writable = True
            self.dbTabla.descripcion.writable = True
            self.dbTabla.importe.writable = True

            # Campos que no deben ser editados, solo de forma autocalculada

            # Campos que todos los movimientos pueden editar
            self.dbTabla.notas.writable = True

            # Se definen los valores por default obligados
            self.dbTabla.cfdi_id.default = self._D_defaults.cfdi_id

            return _D_return

        def al_validar(self, O_form, dbRow = None, D_camposAChecar = None):
            """ Validar la información para poder grabar cfdi

            Las validaciones en esta función son genéricas, se espera que existan validaciones
             específicas por tipo de CFDI

            @param D_camposAChecar:
            @type D_camposAChecar:
            @param dbRow:
            @type dbRow:
            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _dbRow = dbRow if dbRow else (self.dbTabla(O_form.record_id) if O_form.record_id else None)
            _D_camposAChecar = D_camposAChecar if D_camposAChecar \
                else STV_FWK_LIB.OBTENER_CAMPOSACHECAR_PARA_VALIDAR(self.dbTabla, _dbRow, O_form)

            if not O_form.record_id:
                # Se esta creando el registro

                _D_results = self.puede_crear(
                    x_cfdi = _D_camposAChecar.cfdi_id,
                    **_D_camposAChecar
                    )
                if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                    O_form.errors.id = _D_results.s_msgError
                else:
                    pass

            else:
                # Se está modificando el registro, la llamada a configuraCampos_edicion ya definió con writable
                #  que puede modificarse y que no. Por lo que el código para verificar si puede o no editar
                #  no es requerido en este caso.
                pass

            # Validaciones
            _dbRow_movtoCFDITraspaso = dbc01(
                (dbc01.tcfdi_movimientos.cfdi_ingreso_id == _D_camposAChecar.cfdi_id)
                & (dbc01.tcfdis.id == dbc01.tcfdi_movimientos.cfdi_ingreso_id)
                ).select(
                    dbc01.tcfdi_movimientos.fecha, dbc01.tcfdi_movimientos.saldo,
                    dbc01.tcfdis.saldo,
                    orderby = [~dbc01.tcfdi_movimientos.orden],
                    ).first()

            if not _dbRow_movtoCFDITraspaso:
                O_form.errors.id = "No se pudo determinar el estado de cuenta del CFDI, " \
                                   + "favor de correr saldos en el cliente"
            elif _dbRow_movtoCFDITraspaso.tcfdi_movimientos.fecha > _D_camposAChecar.fecha:
                O_form.errors.fecha = "Existe un movimiento en el CFDI con fecha mayor a la del manejo de saldo"

            elif _dbRow_movtoCFDITraspaso.tcfdi_movimientos.saldo >= 0:
                O_form.errors.importe = "CFDI no contiene saldo a favor disponible"

            else:
                pass

            _dbRow_movtoCFDIAplicacion = dbc01(
                (dbc01.tcfdi_movimientos.cfdi_ingreso_id == _D_camposAChecar.cfdirelacionado_id)
                & (dbc01.tcfdis.id == dbc01.tcfdi_movimientos.cfdi_ingreso_id)
                ).select(
                dbc01.tcfdi_movimientos.fecha, dbc01.tcfdi_movimientos.saldo,
                dbc01.tcfdis.saldo,
                orderby = [~dbc01.tcfdi_movimientos.orden],
                ).first()

            if not _dbRow_movtoCFDIAplicacion:
                O_form.errors.cfdirelacionado_id = "No se pudo determinar el estado de cuenta del CFDI Receptor, " \
                                   + "favor de correr saldos en el cliente"
            elif _dbRow_movtoCFDIAplicacion.tcfdi_movimientos.fecha > _D_camposAChecar.fecha:
                O_form.errors.fecha = "Existe un movimiento en el CFDI Receptor con fecha mayor " \
                                      + "a la del manejo de saldo"

            elif _dbRow_movtoCFDIAplicacion.tcfdi_movimientos.saldo <= 0:
                O_form.errors.importe = "CFDI Receptor no contiene saldo mayor de zero disponible"

            else:
                pass

            return O_form

        @classmethod
        def DESPUES_ELIMINAR(cls, L_errorsAction):
            """ Después de eliminar un registro

            @param L_errorsAction:
            @type L_errorsAction:
            @return: errores
            @rtype: lista de diccionarios {'msg':'...'}
            """
            _D_return = FUNC_RETURN()

            if cls._dbRow_aEliminar:

                _D_results = TCFDIS.EDOCTA(
                    n_empresa_id = cls._dbRow_aEliminar.empresa_id,
                    n_cliente_id = cls._dbRow_aEliminar.receptorcliente_id,
                    E_movto_generado_por = TCFDI_MOVIMIENTOS.E_GENERADO_POR.DOCUMENTO,
                    ).procesar_cfdi(cls._dbRow_aEliminar.cfdi_id, x_cfdi_manejoSaldo = cls._dbRow_aEliminar)
                _D_return.combinar(_D_results)

            else:
                _D_return.E_return = CLASS_e_RETURN.NOK_SECUENCIA
                _D_return.s_msgError += "Error en secuencia de llamado de funciones para borrado. "

            generaMensajeFlash(cls.dbTabla, _D_return)
            return L_errorsAction

        @classmethod
        def MAPEAR_CAMPOS(cls, x_cfdi_manejosaldo, **D_params):
            """ Remapea campos solamente cuando se graba el registro

            @param x_cfdi_manejosaldo:
            @type x_cfdi_manejosaldo:
            @return:
            @rtype:
            """

            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi_manejosaldo, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _dbRow_cfdi = dbc01.tcfdis(_dbRow.cfdi_id)

            # _D_actualizar = Storage()
            #
            # for _s_nombreCampo in _D_actualizar:
            #     if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
            #         _dbRow.update_record(**_D_actualizar)
            #         break
            #     else:
            #         pass

            _D_results = TCFDIS.EDOCTA(
                n_empresa_id = _dbRow_cfdi.empresa_id,
                n_cliente_id = _dbRow_cfdi.receptorcliente_id,
                E_movto_generado_por = TCFDI_MOVIMIENTOS.E_GENERADO_POR.DOCUMENTO,
                ).procesar_cfdi(_dbRow_cfdi, x_cfdi_manejoSaldo = _dbRow)
            _D_return.combinar(_D_results)

            return _D_return

        @classmethod
        def CALCULAR_CAMPOS(cls, x_cfdi_manejosaldo, **D_params):
            """ Recalcula campos que tienen impacto cuando otros registros de otras tablas cambian
            Esta operación así mismo detecta y agrega las inconsistencias

            @param x_cfdi_manejosaldo:
            @type x_cfdi_manejosaldo:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_results = super(TCFDI_MANEJOSALDOS.PROC, cls).CALCULAR_CAMPOS(
                _dbRow,
                b_actualizarRegistro = False,
                b_recalcularCFDI = False
                )
            _D_return.combinar(_D_results)

            return _D_return

        pass  # PROC

    pass  # TCFDI_MANEJOSALDOS


dbc01.define_table(
    TCFDI_MANEJOSALDOS.S_NOMBRETABLA, *TCFDI_MANEJOSALDOS.L_DBCAMPOS,
    format = '%(descripcion)s',
    singular = 'CFDI Manejo Saldo',
    plural = 'CFDI Manejo Saldos',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TCFDI_MANEJOSALDOS
    )


class TCFDI_MOVIMIENTOS(Table):
    """ Al calcular el saldo del cliente o actualizarlo, esta tabla registra los movimientos de saldo en el CFDI.

    Esta tabla se actualiza al generar el saldo del Cliente, y cada vez que se le asigna un CFDI.
    """

    S_NOMBRETABLA = 'tcfdi_movimientos'

    class E_ACCION:
        """ Se definen las opciónes del campo """
        NO_DEFINIDA = 0
        EMISION = 1
        CANCELACION = 2
        NO_TIMBRADA = 3
        D_TODOS = {
            NO_DEFINIDA: 'No definida',
            EMISION    : 'EMISION',
            CANCELACION: 'CANCELACION',
            NO_TIMBRADA: 'SIN TIMBRAR',
            }

        @classmethod
        def GET_DICT(cls):
            return cls.D_TODOS

    class E_TIPODOCUMENTO:
        """ Se definen las opciónes del campo """
        NO_DEFINIDO = 0
        INGRESO = 1
        PAGO = 2
        DEVOLUCION = 3
        DESCUENTO = 4
        ANTICIPO = 5
        DEBITO = 6
        OTRO = 7
        TRASPASO_SALDO_A_FAVOR = 8
        APLICACION_SALDO_A_FAVOR = 9
        D_TODOS = {
            NO_DEFINIDO             : 'No Definido',
            INGRESO                 : 'Ingreso - Factura',
            PAGO                    : 'Pago',
            DEVOLUCION              : 'Egreso - Devolucion',
            DESCUENTO               : 'Egreso - Descuento',
            ANTICIPO                : 'Ingreso - Anticipo',
            DEBITO                  : 'Egreso - Debito',
            TRASPASO_SALDO_A_FAVOR  : 'Traspaso saldo a favor',
            APLICACION_SALDO_A_FAVOR: 'Aplicación de saldo a favor',
            OTRO                    : 'Otro',
            }

        @classmethod
        def GET_DICT(cls):
            return cls.D_TODOS

    class E_GENERADO_POR:
        """ Se definen las opciónes del campo """
        NO_DEFINIDO = 0
        RECALCULO_CLIENTE = 1
        DOCUMENTO = 2
        IMPORTACION_CFDI = 3
        TIMBRADO = 4
        CANCELACION = 5
        D_TODOS = {
            NO_DEFINIDO      : 'No definido',
            RECALCULO_CLIENTE: 'Recálculo Cliente',
            DOCUMENTO        : 'Documento',
            IMPORTACION_CFDI : 'Importación',
            TIMBRADO         : 'Timbrado',
            CANCELACION      : 'Cancelación'
            }

        @classmethod
        def GET_DICT(cls):
            return cls.D_TODOS

    L_DBCAMPOS = [
        Field(
            'empresa_id', dbc01.tempresas, default = None,
            required = True,
            ondelete = 'NO ACTION', notnull = True, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Empresa', comment = '',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r)
            ),
        Field(
            'orden', 'integer', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputInteger, label = 'Orden', comment = None,
            writable = False, readable = True,
            represent = stv_represent_number
            ),
        TEMPRESA_CLIENTES.DEFINIR_DBCAMPO('cliente_id', required = True),
        # Field(
        #     'cliente_id', dbc01.tempresa_clientes, default = None,
        #     required = True,
        #     ondelete = 'NO ACTION', notnull = True, unique = False,
        #     widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Cliente', comment = '',
        #     writable = False, readable = True,
        #     represent = lambda v, r: stv_represent_referencefield(v, r)
        #     ),
        Field(
            'fecha', type = TDATETIME_GRABADIRECTO_LEERDIRECTO, default = None,  # Se graba como la fecha-hora local de emisión del CFDI
            required = True, requires = IS_NULL_OR(IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME)),
            notnull = True,
            widget = stv_widget_inputDateTime, label = 'Fecha', comment = '',
            writable = False, readable = True,
            represent = stv_represent_datetime
            ),
        Field(
            'fechaingreso', type = TDATETIME_GRABADIRECTO_LEERDIRECTO, default = None,
            # Se graba como la fecha-hora local de emisión del CFDI de ingreso relacionado para su uso en consultas
            required = True, requires = IS_NULL_OR(IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME)),
            notnull = True,
            widget = stv_widget_inputDateTime, label = 'Fecha Ingreso', comment = '',
            writable = False, readable = True,
            represent = stv_represent_datetime
            ),

        Field(
            'accion', 'integer', default = E_ACCION.NO_DEFINIDA,
            required = False, requires = IS_IN_SET(
                E_ACCION.GET_DICT(), zero = 0, error_message = (
                    'Selecciona')
                ),
            notnull = True, unique = False,
            widget = stv_widget_combobox,
            label = 'Acción', comment = 'Acción en el CFDI',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TCFDI_MOVIMIENTOS.E_ACCION.GET_DICT())
            ),
        Field(
            'tipodocumento', 'integer', default = E_TIPODOCUMENTO.NO_DEFINIDO,
            required = False, requires = IS_IN_SET(
                E_TIPODOCUMENTO.GET_DICT(), zero = 0, error_message = (
                    'Selecciona')
                ),
            notnull = True, unique = False,
            widget = stv_widget_combobox,
            label = 'Tipo Documento', comment = 'Tipo de documento',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.GET_DICT())
            ),
        Field(
            'descripcion', 'string', length = 150, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Descripcion', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),

        Field(
            'cfdi_id', dbc01.tcfdis, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'CFDI Origen', comment = 'CFDI que origina el movimiento',
            writable = False, readable = True,
            represent = None
            ),
        # En caso de CFDI de pago, se relaciona cada referencia
        Field(
            'cfdi_complementopago_doctorelacionado_id', dbc01.tcfdi_complementopago_doctosrelacionados, default = None,
            required = False,
            ondelete = 'CASCADE', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Comp. Pago', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r)
            ),

        # En caso de CFDI de egreso, se relaciona cada referencia
        Field(
            'cfdi_relacionado_id', dbc01.tcfdi_relacionados, default = None,
            required = False,
            ondelete = 'CASCADE', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Relacionado', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r)
            ),

        # Todos los movimientos estan mapeados a un CFDI de Ingreso
        Field(
            'cfdi_ingreso_id', dbc01.tcfdis, default = None,
            required = False,
            ondelete = 'CASCADE', notnull = False, unique = False,
            label = 'CFDI Ingreso', comment = 'CFDI del cual se maneja el estado de cuenta',
            writable = False, readable = True,
            represent = None
            ),

        Field(
            'cfdi_movimiento_emision_id', 'integer', default = None,
            required = False,
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = (
                'Movimiento Inicial'), comment = 'Utilizado en caso de cancelación',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r)
            ),
        Field(
            'cfdi_manejosaldo_id', dbc01.tcfdi_manejosaldos, default = None,
            required = False,
            ondelete = 'NO ACTION', notnull = False, unique = False,
            label = 'CFDI Manejo Saldo', comment = 'CFDI Manejo Saldo relacionado',
            writable = False, readable = True,
            represent = None
            ),

        Field(
            'nota', 'string', length = 150, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Nota', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),

        # Campos usados para contabilización de los conceptos
        Field(
            'cargo', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Cargo', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        Field(
            'abono', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Abono', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        Field(
            'saldo', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Saldo', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money
            ),

        Field(
            'importe', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Importe', comment = 'Importe del movimiento',
            writable = False, readable = True,
            represent = stv_represent_money_ifnotzero
            ),

        Field(
            'importe_monedapago', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Imp. Moneda Pago',
            comment = 'Importe del movimiento en moneda de pago',
            writable = False, readable = True,
            represent = stv_represent_money_ifnotzero
            ),

        Field(
            'monedacontpaqi_id', 'integer', default = D_stvSiteHelper.dbconfigs.defaults.moneda_contpaqi_id,
            required = False,
            requires = IS_NULL_OR(
                IS_IN_DB(
                    dbc01(dbc01.tempresa_monedascontpaqi.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id),
                    'tempresa_monedascontpaqi.id',
                    dbc01.tempresa_monedascontpaqi._format
                    )
                ),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Moneda', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tcfdis.monedacontpaqi_id)
            ),
        Field(
            'tipocambio', 'decimal(14,4)', default = 1,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Tipo de cambio', comment = None,
            writable = False, readable = True,
            represent = stv_represent_money_ifnotzero
            ),

        Field(
            'generado_por', 'integer', default = E_GENERADO_POR.NO_DEFINIDO,
            required = False, requires = IS_IN_SET(
                E_GENERADO_POR.GET_DICT(), zero = 0, error_message = 'Selecciona'
                ),
            notnull = True, unique = False,
            widget = stv_widget_combobox,
            label = 'Generado por', comment = 'Define el origén de este registro',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TCFDI_MOVIMIENTOS.E_GENERADO_POR.GET_DICT())
            ),

        tSignature_dbc01,
        ]

    @staticmethod
    def CONFIGURAR_INTEGRIDAD_REFERENCIAL():
        _dbTabla = dbc01.tcfdi_movimientos
        _dbTabla.cfdi_movimiento_emision_id.type = 'reference tcfdi_movimientos'
        _dbTabla.cfdi_movimiento_emision_id.ondelete = 'NO ACTION'
        _dbTabla.cfdi_movimiento_emision_id.requires = IS_IN_DB(
            dbc01,
            'tcfdi_movimientos.id',
            dbc01.tcfdi_movimientos._format
            )

        return None

    pass


dbc01.define_table(
    TCFDI_MOVIMIENTOS.S_NOMBRETABLA, *TCFDI_MOVIMIENTOS.L_DBCAMPOS,
    format = '%(cfdi_id)s',
    singular = 'CFDI movimientos',
    plural = 'CFDI movimientos',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TCFDI_MOVIMIENTOS
    )


class TCFDIS_TEMP_SINCONTABILIZAR:
    pass


dbc01.define_table(
    'tcfdis_temp_sincontabilizar',
    Field(
        'empresa_id', dbc01.tempresas, default = None,
        required = True,
        ondelete = 'NO ACTION', notnull = True, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = (
            'Empresa'), comment = 'Empresa que tiene registrado el cfdi ya sea como emisor o receptor',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r)
        ),
    Field(
        'empresa_plaza_sucursal_cajachica_id', dbc01.tempresa_plaza_sucursal_cajaschicas, default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Caja chica relacionada', comment = (
            'Caja chica relacionada'),
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r)
        ),
    Field(
        'dia', 'date', default = None,
        required = False, requires = IS_NULL_OR(IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE)),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha', comment = '',
        writable = False, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'generado_fechahora', type = TDATETIME_GRABADIRECTO_LEERDIRECTO, default = None,
        # Se graba como la fecha-hora local de emisión del CFDI
        required = False, requires = IS_NULL_OR(IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME)),
        notnull = False,
        widget = stv_widget_inputDateTime, label = 'Fecha', comment = '',
        writable = False, readable = True,
        represent = stv_represent_datetime
        ),
    Field(
        'ingresos', 'integer', default = 0,
        required = False,
        notnull = True, unique = False,
        widget = stv_widget_inputInteger,
        label = 'Ingresos', comment = 'Ingresos',
        writable = False, readable = True,
        represent = stv_represent_number
        ),
    Field(
        'egresos', 'integer', default = 0,
        required = False,
        notnull = True, unique = False,
        widget = stv_widget_inputInteger,
        label = 'Egresos', comment = 'Ingresos',
        writable = False, readable = True,
        represent = stv_represent_number
        ),
    Field(
        'pagos', 'integer', default = 0,
        required = False,
        notnull = True, unique = False,
        widget = stv_widget_inputInteger,
        label = 'Pagos', comment = 'Ingresos',
        writable = False, readable = True,
        represent = stv_represent_number
        ),
    Field(
        'ingresos_cancelados', 'integer', default = 0,
        required = False,
        notnull = True, unique = False,
        widget = stv_widget_inputInteger,
        label = 'Ingresos cancelados', comment = 'Ingresos',
        writable = False, readable = True,
        represent = stv_represent_number
        ),
    Field(
        'egresos_cancelados', 'integer', default = 0,
        required = False,
        notnull = True, unique = False,
        widget = stv_widget_inputInteger,
        label = 'Egresos cancelados', comment = 'Ingresos',
        writable = False, readable = True,
        represent = stv_represent_number
        ),
    Field(
        'pagos_cancelados', 'integer', default = 0,
        required = False,
        notnull = True, unique = False,
        widget = stv_widget_inputInteger,
        label = 'Pagos cancelados', comment = 'Ingresos',
        writable = False, readable = True,
        represent = stv_represent_number
        ),
    Field(
        'otros_cancelados', 'integer', default = 0,
        required = False,
        notnull = True, unique = False,
        widget = stv_widget_inputInteger,
        label = 'Otros cancelados', comment = 'Ingresos',
        writable = False, readable = True,
        represent = stv_represent_number
        ),
    Field(
        'otros', 'integer', default = 0,
        required = False,
        notnull = True, unique = False,
        widget = stv_widget_inputInteger,
        label = 'Otros', comment = 'Otros',
        writable = False, readable = True,
        represent = stv_represent_number
        ),
    tSignature_dbc01,
    format = '%(dia)s',
    singular = 'CFDIs Sin Contabilizar',
    plural = 'CFDIa Sin Contabilizar',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )

dbc01.tcfdis._referenced_by.append(dbc02.tcfdi_xmls.cfdi_id)
dbc01.tcfdis._references.append(dbc02.tcfdi_xmls.cfdi_id)

db01.ttiposcomprobantes._referenced_by.append(dbc01.tcfdis.tipocomprobante_id)
db01.ttiposcomprobantes._references.append(dbc01.tcfdis.tipocomprobante_id)

dbc01.tcfdi_conceptos._referenced_by.append(dbc01.tempresa_invmovto_productos.cfdi_concepto_id)
dbc01.tcfdi_conceptos._references.append(dbc01.tempresa_invmovto_productos.cfdi_concepto_id)
dbc01.tempresa_invmovto_productos.cfdi_concepto_id.type = 'reference tcfdi_conceptos'
dbc01.tempresa_invmovto_productos.cfdi_concepto_id.requires = IS_NULL_OR(
    IS_IN_DB(dbc01, 'tcfdi_conceptos.id', dbc01.tcfdi_conceptos._format)
    )
dbc01.tempresa_invmovto_productos.cfdi_concepto_id.widget = stv_widget_input

dbc01.tcfdi_conceptos._referenced_by.append(dbc01.tempresa_inventariomovimientos.cfdi_id)
dbc01.tcfdi_conceptos._references.append(dbc01.tempresa_inventariomovimientos.cfdi_id)
dbc01.tempresa_inventariomovimientos.cfdi_id.type = 'reference tcfdis'
dbc01.tempresa_inventariomovimientos.cfdi_id.requires = IS_NULL_OR(IS_IN_DB(dbc01, 'tcfdis.id', dbc01.tcfdis._format))

dbc01.tcfdi_registropagos._referenced_by.append(dbc01.tempresa_estadocuenta_conciliacionesbancarias.registropago_id)
dbc01.tcfdi_registropagos._references.append(dbc01.tempresa_estadocuenta_conciliacionesbancarias.registropago_id)
dbc01.tempresa_estadocuenta_conciliacionesbancarias.registropago_id.requires = IS_IN_DB(
    dbc01, 'tcfdi_registropagos.id', dbc01.tcfdi_registropagos._format
    )

TCFDI_COMPLEMENTOPAGO_DOCTOSRELACIONADOS.CONFIGURAR_INTEGRIDAD_REFERENCIAL()
TCFDI_RELACIONADOS.CONFIGURAR_INTEGRIDAD_REFERENCIAL()
TCFDI_MOVIMIENTOS.CONFIGURAR_INTEGRIDAD_REFERENCIAL()
TCFDIS.CONFIGURAR_INTEGRIDAD_REFERENCIAL()

# Consulta para mandar datos al sistema de Trevino
# SELECT t1.id, t1.serie, t1.folio, t1.tipodecomprobante, t2.agentevendedor, t1.receptorcliente_id,
# t1.receptornombrerazonsocial, t1.receptorrfc, t1.metodopago, t1.formapago, t1.condicionesdepago, t1.credito_plazo,
# t1.subtotal, t1.totalimpuestostrasladados, t1.descuento, t1.total, t1.saldo, t1.receptorusocfdi, t1.lugarexpedicion,
# t1.empresa_plaza_sucursal_cajachica_id, t1.uuid,
# (select count(id) from tcfdi_complementopago_doctosrelacionados as tc where tc.cfdirelacionado_id = t1.id) as NumPagos
# FROM `tcfdis` as t1 left join tcfdis2 as t2 on t2.cfdi_id = t1.id where t1.saldo != 0 and empresa_id = 1