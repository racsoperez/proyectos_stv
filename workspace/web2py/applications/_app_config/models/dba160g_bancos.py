# -*- coding: utf-8 -*-

""" Este archivo contiene las definiciones de los modelos genéricos de banco.

Definiciones de modelos que se comparten entre las aplicaciones de una cuenta ya que se ubican en la base de datos específica por cuenta.
En general es información usada por el framework.
"""

class TEMPRESA_ESTADOSCUENTA():
    
    class ESTATUS():
        PENDIENTE = 1
        CONCILIANDO = 2
        COMPLETADO = 2

        @classmethod
        def GET_DICT(cls):
            return {
                cls.PENDIENTE: ('Pendiente'),
                cls.CONCILIANDO: ('Conciliando'),
                cls.COMPLETADO: ('Completado'),
                }
            
            
    class TIPO_CONCILIACION():
        DEPOSITOS = 1
        RETIROS = 2
        TODOS = 3
        
        @classmethod
        def GET_DICT(cls):
            return {
                cls.DEPOSITOS: ('Depósitos'),
                cls.RETIROS: ('Retiros'),
                cls.TODOS: ('Todos'),
                }

    @classmethod
    def ACTUALIZAR_ESTATUS(cls, s_empresa_estadocuenta_id):
        _dbRows_pendientes = dbc01(
            (dbc01.tempresa_estadocuenta_movimientos.empresa_estadocuenta_id == s_empresa_estadocuenta_id)
            & (dbc01.tempresa_estadocuenta_movimientos.estatus == TEMPRESA_ESTADOCUENTA_MOVIMIENTOS.ESTATUS.PENDIENTE)
            ).select(
                dbc01.tempresa_estadocuenta_movimientos.id
                )
        if not _dbRows_pendientes:
            dbc01(
                dbc01.tempresa_estadoscuenta.id == s_empresa_estadocuenta_id
                ).update(
                    estatus = TEMPRESA_ESTADOCUENTA_MOVIMIENTOS.ESTATUS.COMPLETADO
                    )
        else:
            pass
        return

    pass
dbc01.define_table(
    'tempresa_estadoscuenta',
    Field('empresa_cuentabancaria_id', dbc01.tempresa_cuentasbancarias, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1),
      label=('Cuenta bancaria'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_referencefield),
    Field('fecha', 'string', default='',
      required=True,
      notnull=False,
      widget=stv_widget_inputMonthYear, label='Fecha', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field(
        'fecha_inicio', 'datetime', default = request.browsernow,
        required = False, requires = IS_DATE(format = (STV_FWK_APP.FORMAT.s_DATE)),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha primer movimiento', comment = None,
        writable = False, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'fecha_fin', 'datetime', default = request.browsernow,
        required = False, requires = IS_DATE(format = (STV_FWK_APP.FORMAT.s_DATE)),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha último movimiento', comment = None,
        writable = False, readable = True,
        represent = stv_represent_date
        ),
    Field('estadocuentanombrearchivo', 'string', length=250, default=None,
      required=False,
      notnull=False, unique=False,
      widget = stv_widget_input, label=T('Estado de cuenta'), comment=None,
      writable=False, readable=False,
      represent=stv_represent_string),
    Field('estadocuenta', type = 'upload',
      required = False, requires = IS_LENGTH(minsize=0, maxsize=1572864, error_message=T('El archivo es demasiado grande.')),
      notnull = False,
      uploadfield = True, widget=lambda f, d, u=None: stv_widget_inputFile(f, d, URL(a='app_empresas', c='160bancos', f='empresa_estadoscuenta'), dbc01.tempresa_estadoscuenta.estadocuentanombrearchivo), 
      label = ('Estado de cuenta'), comment = None,
      writable = True, readable = True, authorize = lambda record: auth.is_logged_in(),
      autodelete = True, represent = lambda v,r: stv_represent_file(v, r, URL(a='app_empresas', c='160bancos', f='estadocuenta_archivo', vars=D_stvFwkCfg.D_cfg_cuenta_para_links), dbc01.tempresa_estadoscuenta.estadocuentanombrearchivo, stv_represent_file_showOption_opendownload),
      uploadfolder = os.path.join(request.folder, 'uploads', 'estadoscuenta'),
      uploadseparate = False, uploadfs = None),
    Field('estatus', 'integer', default = TEMPRESA_ESTADOSCUENTA.ESTATUS.PENDIENTE,
      required = True, requires=IS_IN_SET(TEMPRESA_ESTADOSCUENTA.ESTATUS.GET_DICT(), zero=0, error_message=('Selecciona')),
      notnull = True, unique = False,
      widget = stv_widget_combobox, label = ('Estatus'), comment = '',
      writable = False, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_ESTADOSCUENTA.ESTATUS.GET_DICT())),
    Field('inconsistencia', 'text', length=500, default='',
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label=('Inconsistencias'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_text), 
    Field('tipo_conciliacion', 'integer', default = TEMPRESA_ESTADOSCUENTA.TIPO_CONCILIACION.DEPOSITOS,
      required = True, requires=IS_IN_SET(TEMPRESA_ESTADOSCUENTA.TIPO_CONCILIACION.GET_DICT(), zero=0, error_message=('Selecciona')),
      notnull = True, unique = False,
      widget = stv_widget_combobox, label = ('Tipo de conciliación'), comment = '',
      writable = False, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_ESTADOSCUENTA.TIPO_CONCILIACION.GET_DICT())),
    tSignature_dbc01,
    format='%(estadocuentanombrearchivo)s',
    singular='Estado de cuenta',
    plural='Estados de cuenta',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_ESTADOCUENTA_MOVIMIENTOS():
    
    class ESTATUS():
        ''' Se definen las opciónes del campo '''
        PENDIENTE = 0
        CONCILIADO_AUTOMATICO = 1
        CONCILIADO_MANUAL = 2
        IGNORADO = 3
        IGNORADO_MANUAL = 4

        @classmethod
        def get_dict(cls):
            return {
                cls.PENDIENTE: ('Pendiente'),
                cls.CONCILIADO_AUTOMATICO: ('Conciliado Aut.'),
                cls.CONCILIADO_MANUAL: ('Conciliado Man.'),
                cls.IGNORADO: ('Ignorado'),
                cls.IGNORADO_MANUAL: ('Ignorado Manual'),
                }

    class BUSQUEDA_AVANZADA:

        @staticmethod
        def CONFIGURA(
                s_empresa_id,
                O_cat,
                **D_valoresDefault
                ):
            """

            @return:
            @rtype:
            """
            _D_return = Storage(
                E_return = CLASS_e_RETURN.OK,
                s_msgError = "",
                )

            _dbTable = dbc01.tempresa_estadocuenta_movimientos
            # Variable que contendrá los campos a editar en la búsqueda avanzada.
            _L_dbFields = []

            D_valoresDefault = Storage(D_valoresDefault)

            # Se configura el uso del campo categorias para poder filtrar por varias clasificaciones.
            _dbField_fecha = _dbTable.fecha_operacion.clone()
            _dbField_fecha.default = None
            _dbField_fecha.adv_default = D_valoresDefault.fecha_operacion or _dbField_fecha.default
            _dbField_fecha.widget = stv_widget_inputDateRange

            _dbField_tipomovimiento = _dbTable.bancotipomovimiento_id.clone()
            _dbField_tipomovimiento.adv_default = D_valoresDefault.bancotipomovimiento_id or _dbField_tipomovimiento.default
            _dbField_tipomovimiento.widget = lambda field, value: stv_widget_db_chosen(
                field, value, 20,
                )
            _dbField_tipomovimiento.length = 400

            _dbField_estatus = _dbTable.estatus.clone()
            _dbField_estatus.adv_default = D_valoresDefault.estatus or _dbField_estatus.default
            _dbField_estatus.requires = IS_NULL_OR(_dbField_estatus.requires)
            _dbField_estatus.widget = lambda field, value: stv_widget_chosen(
                field, value, 20,
                )
            _dbField_estatus.length = 400

            _dbField_importe = Field(
                'importe', 'decimal(14,4)', default = None,
                required = False,
                notnull = False, unique = False,
                widget = stv_widget_inputMoneyRange, label = 'Importe', comment = None,
                writable = False, readable = True,
                represent = stv_represent_money_ifnotzero
                )
            _dbField_importe.adv_default = D_valoresDefault.importe or _dbField_importe.default

            _L_dbFields += [
                _dbField_fecha,
                _dbField_importe,
                _dbField_tipomovimiento,
                _dbField_estatus,
                Field(
                    'con_inconsistencias', 'boolean', default = False,
                    required = False,
                    notnull = False,
                    widget = stv_widget_inputCheckbox, label = ('Con inconsistencias'), comment = None,
                    writable = True, readable = True,
                    represent = stv_represent_boolean
                    ),
                ]

            # Se configura la edición de los campos.
            for _dbField in _L_dbFields:
                _dbField.writable = True
                _dbField.readable = True
                if _dbField.name in request.vars:
                    pass
                elif getattr(_dbField, 'adv_default', None) is not None:
                    request.vars[_dbField.name] = _dbField.adv_default
                else:
                    pass

            O_cat.cfgRow_advSearch(
                L_dbFields_formFactory_advSearch = _L_dbFields,
                dbRow_formFactory_advSearch = None,
                D_hiddenFields_formFactory_advSearch = {},
                s_style = 'bootstrap',
                L_buttons = []
                )

            return _D_return

        @staticmethod
        def GENERA_PREFILTRO(
                D_args,
                ):
            """

            @return:
            @rtype:
            """
            _D_return = Storage(
                E_return = CLASS_e_RETURN.OK,
                s_msgError = "",
                qry = True
                )
            _dbTable = dbc01.tempresa_estadocuenta_movimientos

            if D_args.s_accion == request.stv_fwk_permissions.btn_find.code:
                # Si esta procesando una búsqueda
                if request.vars.importe:
                    _qry_abono = True
                    _qry_cargo = True
                    if request.vars.importe[0] != '':
                        _qry_abono &= _dbTable.abono >= request.vars.importe[0]
                        _qry_cargo &= _dbTable.cargo >= request.vars.importe[0]
                    else:
                        pass
                    if (request.vars.importe[1] != '') and (float(request.vars.importe[1]) > float(request.vars.importe[0])):
                        _qry_abono &= _dbTable.abono <= request.vars.importe[1]
                        _qry_cargo &= _dbTable.cargo <= request.vars.importe[1]
                    else:
                        pass

                    _D_return.qry &= _qry_abono | _qry_cargo
                else:
                    pass

                if request.vars.con_inconsistencias:
                    _D_return.qry &= (_dbTable.inconsistencias != None) & (_dbTable.inconsistencias != '')
                else:
                    pass

            else:
                pass

            return _D_return
        pass

    pass


dbc01.define_table(
    'tempresa_estadocuenta_movimientos',
    Field('empresa_estadocuenta_id', dbc01.tempresa_estadoscuenta, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Estado de cuenta', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('fecha_operacion', 'datetime', default=request.browsernow,
      required=False, requires = IS_DATE(format=(STV_FWK_APP.FORMAT.s_DATE)),
      notnull=False,
      widget=stv_widget_inputDate, label='Fecha de operación', comment=None,
      writable=False, readable=True,
      represent=stv_represent_date), 
    Field('fecha_registro', 'datetime', default=request.browsernow,
      required=False, requires = IS_DATE(format=(STV_FWK_APP.FORMAT.s_DATE)),
      notnull=False,
      widget=stv_widget_inputDate, label='Fecha de registro', comment=None,
      writable=False, readable=True,
      represent=stv_represent_date),
    Field('concepto', 'string', length=500, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Concepto', comment=None,
      writable=False, readable=True,
      represent=stv_represent_string),
    Field('abono', 'decimal(14,4)', default=1,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_inputMoney, label='Depósito', comment=None,
      writable=False, readable=True,
      represent=stv_represent_money_ifnotzero),
    Field('cargo', 'decimal(14,4)', default=1,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_inputMoney, label='Retiro', comment=None,
      writable=False, readable=True,
      represent=stv_represent_money_ifnotzero),
    Field(
        'saldo', 'decimal(14,4)', default=1,
        required=False,
        notnull=False, unique=False,
        widget=stv_widget_inputMoney, label='Saldo', comment=None,
        writable=False, readable=True,
        represent=stv_represent_money_ifnotzero
        ),
    Field('bancotipomovimiento_id', 'integer', default=None,
      required=False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tbancostiposmovimientos.id', db01.tbancostiposmovimientos._format)),
      ondelete='NO ACTION', notnull=False, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label=T('Tipo movimiento'), comment=None,
      writable=False, readable=True,
      represent=lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_estadocuenta_movimientos.bancotipomovimiento_id)),
    Field('referencia', 'string', length = 20, default = None,
      required = False,
      notnull = False, unique = False,
      widget = stv_widget_input, label = ('Referencia'), comment = None,
      writable = False, readable = True,
      represent = stv_represent_string),
    Field('estatus', 'integer', default = TEMPRESA_ESTADOCUENTA_MOVIMIENTOS.ESTATUS.PENDIENTE,
      required = False, requires = IS_IN_SET(TEMPRESA_ESTADOCUENTA_MOVIMIENTOS.ESTATUS.get_dict(), zero = 0, error_message = 'Selecciona' ),
      notnull = True, unique = False,
      widget = stv_widget_combobox, 
      label = ('Estatus'), comment = None,
      writable = False, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_ESTADOCUENTA_MOVIMIENTOS.ESTATUS.get_dict())),
    Field('numero_movimiento', 'string', length = 20, default = None,
      required = False,
      notnull = False, unique = False,
      widget = stv_widget_input, label = ('Número de movimiento'), comment = None,
      writable = False, readable = True,
      represent = stv_represent_string),
    Field(
        'cuenta', 'string', default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = ('Cuenta'), comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'sucursal', 'string', default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = ('Sucursal'), comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'cheque', 'string', default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = ('Cheque'), comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'tipomovimiento', 'string', default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = ('Tipo Movmiento'), comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'descripcion', 'string', default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = ('Descripción'), comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'inconsistencias', 'string', default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = ('Inconsistencias'), comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    tSignature_dbc01,
    format='%(id)s',
    singular='Moviento',
    plural='Movimientos',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_ESTADOCUENTA_CONCILIACIONESBANCARIAS():
    pass
    
dbc01.define_table(
    'tempresa_estadocuenta_conciliacionesbancarias',
    Field('empresa_estadocuenta_movimiento_id', dbc01.tempresa_estadocuenta_movimientos, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label=('Movimiento del estado de cuenta'), comment=None,
      writable=False, readable=True,
      represent=lambda v, r: stv_represent_referencefield(v, r)),    
    Field('registropago_id', 'integer', default=None,
      required=True,
      ondelete='NO ACTION', notnull=True, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label=('Registro de pago'), comment=None,
      writable=True, readable=True,
      represent=lambda v, r:  stv_represent_referencefield(v, r, dbc01.tempresa_estadocuenta_conciliacionesbancarias.registropago_id)),  
    Field(
        'descripcion', 'string', default = "",
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = ('Descripción'), comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    tSignature_dbc01,
    format='%(id)s',
    singular='Conciliación bancaria',
    plural='Conciliacones bancarias',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

