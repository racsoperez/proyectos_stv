# -*- coding: utf-8 -*-

""" Este archivo contiene las definiciónes genéricas que se comparten entre todas las cuentas.

La base de datos puede ser accedida por todas las cuentas
"""




dbc01.define_table(
    'tmarcas',
    Field('nombrecorto', 'string', length=20, default=None,
      required=True,
      notnull=True, unique=True,
      widget=stv_widget_input, label=('Nombre Corto'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('descripcion', 'string', length=50, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label=('Descripcion'), comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    tSignature_dbc01,
    format='%(nombrecorto)s',
    singular='Marca',
    plural='Marcas',
    #migrate=True, fake_migrate=True
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

