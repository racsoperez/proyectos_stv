# -*- coding: utf-8 -*-

""" Este archivo contiene las definiciónes específicas para la aplicación.


"""

D_stvSiteCfg.s_nameBasedApp = 'app_empresas'
"""str: Nombre de la aplicación genérica por cuenta.

Aplicación que define y maneja los modelos genéricos por cuenta.
"""

D_stvSiteCfg.b_requestIsBasedApp = (request.application == D_stvSiteCfg.s_nameBasedApp)
"""bool: True si la aplicación del request actual corresponde a la aplicación genérica por cuenta.
"""


def importar_dbs_cuenta_especificas():
            
    # Define una lista de los archivos a importar desde la configuración del sitio.
    _L_fileFromConfig = [
                'cfga020s_Widgets.py',
                'dba120s_empresas.py', 
                'dba125s_inventarios.py',
                'dba130s_clientes.py', 
                'dba140s_proveedores.py', 
                'dba145s_contpaqi.py', 
                'dba150s_contabilidad.py', 
                'dba250s_cfdi.py', 
                'dba251s_cfdicp.py',
                'dba252s_cfdirx.py',
                'dba255s_prepolizas_polizas.py',
                'dba255s_class_prepolizas.py',
                'dba255s_class_polizas.py',
                'dba510s_paginaweb.py',
                'dba900s_integridadreferencial.py',
                ]
    
    # Importar los modelos definidos en el framework
    stv_update.genericAppImport(D_stvSiteCfg.s_nameConfigApp, 'models', _L_fileFromConfig, globals())
    
    