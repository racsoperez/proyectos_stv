# -*- coding: utf-8 -*-

""" Este archivo contiene las definiciónes de los modelos genéricos por cuenta.

Definiciones de modelos que se comparten entre las aplicaciones de una cuenta ya que se ubican en la base de
 datos específica por cuenta.
En general es información usada por el framework.
"""


class TEMPRESA_PROVEEDORES(Table):

    S_NOMBRETABLA = 'tempresa_proveedores'

    class E_TIPOPROVEEDOR:
        PROVEEDOR_NACIONAL = 0
        PROVEEDOR_EXTRANJERO = 1
        PROVEEDOR_NACIONAL_PARTE_RELACIONADA = 2
        PROVEEDOR_EXTRANJERO_PARTE_RELACIONADA = 3
        D_TODOS = {
            PROVEEDOR_NACIONAL                    : 'Proveedor nacional',
            PROVEEDOR_EXTRANJERO                  : 'Proveedor extranjero',
            PROVEEDOR_NACIONAL_PARTE_RELACIONADA  : 'Proveedor nacional parte relacionada',
            PROVEEDOR_EXTRANJERO_PARTE_RELACIONADA: 'Proveedor extranjero parte relacionada',
            }

    class E_PROCESOESTATUS:
        SIN_PROCESO = 0
        CFDIS_PROCESANDO = 1
        CFDIS_APLICANDO_SALDOS = 2
        D_TODOS = {
            SIN_PROCESO           : 'Sin proceso',
            CFDIS_PROCESANDO      : 'Procesando CFDIs',
            CFDIS_APLICANDO_SALDOS: 'Aplicando Saldos',
            }

    L_DBCAMPOS = [
        Field(
            'empresa_id', dbc01.tempresas, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'Empresa', comment = None,
            writable = False, readable = False,
            represent = None
            ),
        Field(
            'codigo', 'string', length = 100, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Código', comment = 'Código interno del proveedor',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'razonsocial', 'string', length = 80, default = None,
            required = True,
            notnull = True, unique = False,
            widget = stv_widget_input, label = 'Razón Social', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'nombrecorto', 'string', length = 20, default = None,
            required = True,
            notnull = True, unique = False,
            widget = stv_widget_input, label = 'Nombre Corto', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'tipoproveedor', 'integer', default = E_TIPOPROVEEDOR.PROVEEDOR_NACIONAL,
            required = False,
            requires = IS_IN_SET(E_TIPOPROVEEDOR.D_TODOS, zero = 0, error_message = 'Selecciona'),
            notnull = False, unique = False,
            widget = stv_widget_combobox, label = 'Tipo de Proveedor', comment = '',
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_PROVEEDORES.E_TIPOPROVEEDOR.D_TODOS)
            ),
        Field(
            'rfc', 'string', length = 15, default = None,
            required = True,
            requires = IS_MATCH(
                D_stvFwkHelper.common.regexp_rfc, error_message = 'Expresión inválida formato AAAA-999999-XXX'
                ),
            notnull = True, unique = False,
            widget = stv_widget_input, label = 'RFC', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        # Tab Domicilio Fiscal
        STV_LIB_DB.DEFINIR_DBCAMPO_OTRADB(
            db01.tpaises, 'pais_id',
            dbQry = None, label = 'Pais', writable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_proveedores.pais_id)
            ),
        STV_LIB_DB.DEFINIR_DBCAMPO_OTRADB(
            db01.tpais_estados, 'pais_estado_id',
            dbQry = None, label = 'Estado', writable = True,
            widget = lambda field, value: stv_widget_db_chosen_linked(
                field, value, 1, 'pais_id',
                URL(a = 'app_generales', c = '005genericos_sat', f = 'localidad_pais_estados_ver.json')
                ),
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_proveedores.pais_estado_id)
            ),
        STV_LIB_DB.DEFINIR_DBCAMPO_OTRADB(
            db01.tpais_estado_municipios, 'pais_estado_municipio_id',
            dbQry = None, label = 'Municipio', writable = True,
            widget = lambda field, value: stv_widget_db_chosen_linked(
                field, value, 1, 'pais_estado_id',
                URL(a = 'app_generales', c = '005genericos_sat', f = 'localidad_pais_estado_mun_ver.json')
                ),
            represent = lambda v, r:
                stv_represent_referencefield(v, r, dbc01.tempresa_proveedores.pais_estado_municipio_id)
            ),
        Field(
            'ciudad', 'string', length = 30, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Ciudad', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'colonia', 'string', length = 30, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Colonia', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'calle', 'string', length = 30, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Calle', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'numexterior', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Num. Exterior', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'numinterior', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Num. Interior', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'codigopostal', 'string', length = 5, default = None,
            required = False,
            requires = IS_EMPTY_OR(IS_LENGTH(maxsize = 5, minsize = 5, error_message = T('enter %(min)g characters'))),
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Código Postal', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        STV_LIB_DB.DEFINIR_DBCAMPO(
            dbc01.tempresa_grupos, 'empresa_grupo_id',
            dbQry = (
                (dbc01.tempresa_grupos.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id)
                & (dbc01.tempresa_grupos.proveedor == True)
                ),
            comment = 'Grupo al que pertenece.',
            label = 'Grupo', writable = True
            ),
        Field(
            'procesostatus', 'integer', default = E_PROCESOESTATUS.SIN_PROCESO,
            required = False,
            requires = IS_IN_SET(E_PROCESOESTATUS.D_TODOS, zero = 0, error_message = 'Selecciona'),
            notnull = False, unique = False,
            widget = stv_widget_combobox, label = 'Proceso', comment = '',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_PROVEEDORES.E_PROCESOESTATUS.D_TODOS)
            ),
        Field(
            'credito_plazo', 'integer', default = 30,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputInteger, label = 'Plazo (días)', comment = 'Días de crédito',
            writable = False, readable = True,
            represent = stv_represent_number
            ),
        tSignature_dbc01,
        ]

    pass  # TEMPRESA_PROVEEDORES


dbc01.define_table(
    TEMPRESA_PROVEEDORES.S_NOMBRETABLA, *TEMPRESA_PROVEEDORES.L_DBCAMPOS,
    format = '%(nombrecorto)s',
    singular = 'Proveedor',
    plural = 'Proveedores',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TEMPRESA_PROVEEDORES
    )

# Referencias
STV_FWK_LIB.DEFINIR_INTEGRIDADREFERENCIAL(
    db01.tpaises,
    dbc01.tempresa_proveedores.pais_id,
    )
STV_FWK_LIB.DEFINIR_INTEGRIDADREFERENCIAL(
    db01.tpais_estados,
    dbc01.tempresa_proveedores.pais_estado_id,
    )
STV_FWK_LIB.DEFINIR_INTEGRIDADREFERENCIAL(
    db01.tpais_estado_municipios,
    dbc01.tempresa_proveedores.pais_estado_municipio_id,
    )
STV_FWK_LIB.DEFINIR_INTEGRIDADREFERENCIAL(
    dbc01.tempresa_proveedores,
    dbc01.tempresa_prodserv_codigosproveedores.empresa_proveedor_id,
    True
    )
STV_FWK_LIB.DEFINIR_INTEGRIDADREFERENCIAL(
    dbc01.tempresa_proveedores,
    dbc01.tempresa_prodserv.empresa_proveedor_id,
    True
    )
