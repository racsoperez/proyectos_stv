# -*- coding: utf-8 -*-

class VALIDACIONES():
    
    class EJERCICIO():
        
        @classmethod
        def ES_VALIDO(cls, s_empresa_id, s_ejercicio):
            
            _s_mensajeError = ""
            
            # Validación para verificar que este ejercicio esté dado de alta en las bases de datos CONTPAQi
            _dbRows = dbc01(
                (dbc01.tempresa_contpaqiejercicios.empresa_id == s_empresa_id)
                &(dbc01.tempresa_contpaqiejercicios.ejercicio == s_ejercicio)
                ).select(
                    dbc01.tempresa_contpaqiejercicios.id
                    )
            if _dbRows:
                # Si encontró ejercicio entonces no pasa nada, se procede a insertar.
                pass
            else:
                _s_mensajeError = "El ejercicio insertado no está dado de alta en las bases de datos de CONTPAQi."

            return _s_mensajeError
        
        @classmethod
        def OPCIONES(cls, s_empresa_id):
            
            _L_ejercicios = dict()
            
            if s_empresa_id:
                # Validación para verificar que este ejercicio esté dado de alta en las bases de datos CONTPAQi
                _dbRows = dbc01(
                    (dbc01.tempresa_contpaqiejercicios.empresa_id == s_empresa_id)
                    ).select(
                        dbc01.tempresa_contpaqiejercicios.ejercicio,
                        groupby = [
                            dbc01.tempresa_contpaqiejercicios.ejercicio
                            ]
                        )
                    
                if _dbRows:
                    # Si encontró ejercicio entonces no pasa nada, se procede a insertar.
                    for _dbRow in _dbRows:
                        _L_ejercicios.update({str(_dbRow.ejercicio):str(_dbRow.ejercicio)})
                else:
                    # No hay ejercicios para devolver
                    pass
            else:
                pass

            return _L_ejercicios
    pass