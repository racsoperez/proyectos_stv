# -*- coding: utf-8 -*-

''' Widgets adicionales definidos específicamente para el cliente '''




class STV_CUSTOM_WIDGET_DYNAMICINPUT_TIPOCAPTURA():
    ''' Se definen las opciónes del campo '''
    
    LIBRE = 1
    NUMERO = 2
    OPCION = 3
    MULTIOPCION = 4
    MONEDA = 5
    FRACCION = 6
    FECHA = 7
    HORA = 8
    PORCENTAJE = 9
    
    @classmethod
    def get_dict(cls):
        return {
            cls.LIBRE: ('Libre'),
            cls.NUMERO: ('Numero'),
            cls.OPCION: ('Opción'),
            cls.MULTIOPCION: ('Multi opción'),
            cls.MONEDA: ('Moneda'),
            cls.FRACCION: ('Fracción'),
            cls.FECHA: ('Fecha'),
            cls.HORA: ('Hora'),
            cls.PORCENTAJE: ('Porcentaje'),
            }


def stv_custom_widget_inputDate_after(field, value):
    return stv_widget_input_common(
        field = field, 
        value = value, 
        sInputType = 'text', 
        bAddTooltip = True, 
        sAddOn = None,
        sAddClass = 'datepicker_date_after',
        sPlaceHolder = None
        )

def stv_custom_widget_inputDateTime_before(field, value):
    return stv_widget_input_common(
        field = field, 
        value = value, 
        sInputType = 'text', 
        bAddTooltip = True, 
        sAddOn = None,
        sAddClass = 'datepicker_datetime_before',
        sPlaceHolder = None
        )

def stv_custom_widget_dynamicinput(dbRow_definicion, dbRow_especificacion, O_form, D_additionalAttributes = Storage()):
    _s_widgetresult = ""
    if dbRow_especificacion:
        _D_additionalAttributes = D_additionalAttributes
        if dbRow_definicion.tipocaptura == STV_CUSTOM_WIDGET_DYNAMICINPUT_TIPOCAPTURA.LIBRE:
            _s_name = dbc01.tempresa_prodserv_especificaciones.tipocaptura_texto.name + "_" + str(dbRow_especificacion.id) 
            _D_additionalAttributes["stv_name"] = _s_name
            _s_widgetresult = stv_widget_input(
                field = dbc01.tempresa_prodserv_especificaciones.tipocaptura_texto, 
                value = O_form.vars[_s_name] or dbRow_especificacion.tipocaptura_texto,
                D_additionalAttributes = _D_additionalAttributes
                )
        elif dbRow_definicion.tipocaptura == STV_CUSTOM_WIDGET_DYNAMICINPUT_TIPOCAPTURA.NUMERO:
            _s_name = dbc01.tempresa_prodserv_especificaciones.tipocaptura_float.name + "_" + str(dbRow_especificacion.id)
            _D_additionalAttributes["stv_name"] = _s_name
            _s_widgetresult = stv_widget_inputInteger(
                field = dbc01.tempresa_prodserv_especificaciones.tipocaptura_float, 
                value = O_form.vars[_s_name] or dbRow_especificacion.tipocaptura_float,
                D_additionalAttributes = _D_additionalAttributes
                )
        elif dbRow_definicion.tipocaptura == STV_CUSTOM_WIDGET_DYNAMICINPUT_TIPOCAPTURA.OPCION:
            _s_name = dbc01.tempresa_prodserv_especificaciones.tipocaptura_texto.name + "_" + str(dbRow_especificacion.id)
            _D_additionalAttributes["stv_name"] = _s_name
            dbc01.tempresa_prodserv_especificaciones.tipocaptura_texto.requires = IS_IN_SET(dbRow_definicion.opciones.split("\n"), zero = 0, error_message = 'Selecciona' )
            _s_widgetresult = stv_widget_chosen(
                field = dbc01.tempresa_prodserv_especificaciones.tipocaptura_texto,
                value = O_form.vars[_s_name] or dbRow_especificacion.tipocaptura_texto,
                maxselect = 1,
                D_additionalAttributes = _D_additionalAttributes
                )
        elif dbRow_definicion.tipocaptura == STV_CUSTOM_WIDGET_DYNAMICINPUT_TIPOCAPTURA.MULTIOPCION:
            _s_name = dbc01.tempresa_prodserv_especificaciones.tipocaptura_texto.name + "_" + str(dbRow_especificacion.id)
            _D_additionalAttributes["stv_name"] = _s_name
            dbc01.tempresa_prodserv_especificaciones.tipocaptura_texto.requires = IS_IN_SET(dbRow_definicion.opciones.split("\n"), zero = 0, error_message = 'Selecciona' )
            _s_widgetresult = stv_widget_chosen(
                field = dbc01.tempresa_prodserv_especificaciones.tipocaptura_texto,
                value = O_form.vars[_s_name] or dbRow_especificacion.tipocaptura_texto,
                maxselect = 10,
                D_additionalAttributes = _D_additionalAttributes
                )
        elif dbRow_definicion.tipocaptura == STV_CUSTOM_WIDGET_DYNAMICINPUT_TIPOCAPTURA.MONEDA:
            _s_name = dbc01.tempresa_prodserv_especificaciones.tipocaptura_float.name + "_" + str(dbRow_especificacion.id)
            _D_additionalAttributes["stv_name"] = _s_name
            _s_widgetresult = stv_widget_inputMoney(
                field = dbc01.tempresa_prodserv_especificaciones.tipocaptura_float, 
                value = O_form.vars[_s_name] or dbRow_especificacion.tipocaptura_float,
                D_additionalAttributes = _D_additionalAttributes
                )
        elif dbRow_definicion.tipocaptura == STV_CUSTOM_WIDGET_DYNAMICINPUT_TIPOCAPTURA.FRACCION:
            _s_name = dbc01.tempresa_prodserv_especificaciones.tipocaptura_float.name + "_" + str(dbRow_especificacion.id)
            _D_additionalAttributes["stv_name"] = _s_name
            _s_widgetresult = stv_widget_inputFloat(
                field = dbc01.tempresa_prodserv_especificaciones.tipocaptura_float, 
                value = O_form.vars[_s_name] or dbRow_especificacion.tipocaptura_float,
                D_additionalAttributes = _D_additionalAttributes
                )
        elif dbRow_definicion.tipocaptura == STV_CUSTOM_WIDGET_DYNAMICINPUT_TIPOCAPTURA.FECHA:
            _s_name = dbc01.tempresa_prodserv_especificaciones.tipocaptura_fecha.name + "_" + str(dbRow_especificacion.id)
            _D_additionalAttributes["stv_name"] = _s_name
            _s_widgetresult = stv_widget_inputDate(
                field = dbc01.tempresa_prodserv_especificaciones.tipocaptura_fecha, 
                value = O_form.vars[_s_name] or dbRow_especificacion.tipocaptura_fecha,
                D_additionalAttributes = _D_additionalAttributes
                )
        elif dbRow_definicion.tipocaptura == STV_CUSTOM_WIDGET_DYNAMICINPUT_TIPOCAPTURA.HORA:
            _s_name = dbc01.tempresa_prodserv_especificaciones.tipocaptura_hora.name + "_" + str(dbRow_especificacion.id)
            _D_additionalAttributes["stv_name"] = _s_name
            _s_widgetresult = stv_widget_inputTime(
                field = dbc01.tempresa_prodserv_especificaciones.tipocaptura_hora, 
                value = O_form.vars[_s_name] or dbRow_especificacion.tipocaptura_hora,
                D_additionalAttributes = _D_additionalAttributes
                )
        elif dbRow_definicion.tipocaptura == STV_CUSTOM_WIDGET_DYNAMICINPUT_TIPOCAPTURA.PORCENTAJE:
            _s_name = dbc01.tempresa_prodserv_especificaciones.tipocaptura_float.name + "_" + str(dbRow_especificacion.id)
            _D_additionalAttributes["stv_name"] = _s_name
            _s_widgetresult = stv_widget_inputPercentage(
                field = dbc01.tempresa_prodserv_especificaciones.tipocaptura_float, 
                value = O_form.vars[_s_name] or dbRow_especificacion.tipocaptura_float,
                D_additionalAttributes = _D_additionalAttributes
                )
        else:
            # Default es captura libre
            _s_name = dbc01.tempresa_prodserv_especificaciones.tipocaptura_texto.name + "_" + str(dbRow_especificacion.id) 
            _D_additionalAttributes["stv_name"] = _s_name
            _s_widgetresult = stv_widget_input(
                field = dbc01.tempresa_prodserv_especificaciones.tipocaptura_texto, 
                value = O_form.vars[_s_name] or dbRow_especificacion.tipocaptura_texto,
                D_additionalAttributes = _D_additionalAttributes
                )
        
        if O_form.errors[_s_name]:
            _s_widgetresult += DIV(
                DIV(
                    O_form.errors[_s_name], 
                    _class='error',
                    errors=None, 
                    _id='%s__error' % _s_name),
                _class='error_wrapper')
        else:
            # No se agrega nada, ya que no hay errores
            pass
    else:
        pass
    return _s_widgetresult


def stv_custom_represent_dynamicinput(x_value, odbRow, fnLambda = None, D_additionalAttributes = dict()):
    _s_representresult = ""
    
    _dbRow_especificacion  = dbc01.tempresa_prodcategoria_especificaciones(odbRow.tempresa_prodserv_especificaciones.prodcategoria_especificacion_id)
    
    _D_additionalAttributes = D_additionalAttributes
    
    if (
        _dbRow_especificacion.tipocaptura in (
            STV_CUSTOM_WIDGET_DYNAMICINPUT_TIPOCAPTURA.LIBRE,
            STV_CUSTOM_WIDGET_DYNAMICINPUT_TIPOCAPTURA.OPCION,
            STV_CUSTOM_WIDGET_DYNAMICINPUT_TIPOCAPTURA.MULTIOPCION
            )
        ):
        _s_representresult = stv_represent_string(odbRow.tempresa_prodserv_especificaciones.tipocaptura_texto, odbRow, fnLambda, _D_additionalAttributes)
    elif _dbRow_especificacion.tipocaptura == STV_CUSTOM_WIDGET_DYNAMICINPUT_TIPOCAPTURA.NUMERO:
        _s_representresult = stv_represent_number(odbRow.tempresa_prodserv_especificaciones.tipocaptura_float, odbRow, fnLambda, _D_additionalAttributes)
    elif _dbRow_especificacion.tipocaptura == STV_CUSTOM_WIDGET_DYNAMICINPUT_TIPOCAPTURA.MONEDA:
        _s_representresult = stv_represent_money(odbRow.tempresa_prodserv_especificaciones.tipocaptura_float, odbRow, fnLambda, _D_additionalAttributes)
    elif _dbRow_especificacion.tipocaptura == STV_CUSTOM_WIDGET_DYNAMICINPUT_TIPOCAPTURA.FRACCION:
        _s_representresult = stv_represent_float(odbRow.tempresa_prodserv_especificaciones.tipocaptura_float, odbRow, fnLambda, _D_additionalAttributes)
    elif _dbRow_especificacion.tipocaptura == STV_CUSTOM_WIDGET_DYNAMICINPUT_TIPOCAPTURA.FECHA:
        _s_representresult = stv_represent_date(odbRow.tempresa_prodserv_especificaciones.tipocaptura_fecha, odbRow, fnLambda, _D_additionalAttributes)
    elif _dbRow_especificacion.tipocaptura == STV_CUSTOM_WIDGET_DYNAMICINPUT_TIPOCAPTURA.HORA:
        _s_representresult = stv_represent_string(odbRow.tempresa_prodserv_especificaciones.tipocaptura_hora, odbRow, fnLambda, _D_additionalAttributes)
    elif _dbRow_especificacion.tipocaptura == STV_CUSTOM_WIDGET_DYNAMICINPUT_TIPOCAPTURA.PORCENTAJE:
        _s_representresult = stv_represent_percentage(odbRow.tempresa_prodserv_especificaciones.tipocaptura_float, odbRow, fnLambda, _D_additionalAttributes)
    else:
        _s_representresult = stv_represent_string(x_value, odbRow, fnLambda, _D_additionalAttributes)
        
    return _s_representresult

