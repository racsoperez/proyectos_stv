# -*- coding: utf-8 -*-

""" Definición de la tabla de Polizas contables """


class TPOLIZAS():
    class ESTATUS():
        ''' Se definen las opciónes del campo '''
        NO_DEFINIDO = 0  # Estatus no definido
        EN_PROCESO = 1
        SIN_EXPORTAR = 2  # Estatus Póliza sin exportar
        EXPORTADO = 3  # Estatus Póliza exportada
        _D_dict = None

        @classmethod
        def get_dict(cls):
            if not cls._D_dict:
                cls._D_dict = Storage(
                    {
                        cls.NO_DEFINIDO: ('No definido'),
                        cls.EN_PROCESO: ('En proceso'),
                        # Abierta ya que pueden importarse mas CFDIs en la misma poliza
                        cls.SIN_EXPORTAR: ('Sin exportar'),  # Cerrada ya que esta lista para ser exportada
                        cls.EXPORTADO: ('Exportado'),
                        }
                    )
            else:
                pass
            return cls._D_dict

    class EXAUT_ESTATUS():
        ''' Se definen las opciónes del campo '''

        # Estas opciones pertenecen a los conceptos de Tipo Entrada.
        ENVIADA = 1
        ESPERANDO_CONFIRMACION = 2
        ERROR = 3

        @classmethod
        def GET_DICT(cls):
            return {
                cls.ENVIADA               : ('Poliza enviada'),
                cls.ESPERANDO_CONFIRMACION: ('Esperando confirmación'),
                cls.ERROR                 : ('Error al exportarse'),
                }

    pass


dbc01.define_table(
    'tpolizas',
    Field(
        'empresa_tipopoliza_id', dbc01.tempresa_tipospoliza, default = None,
        required = True,
        ondelete = 'NO ACTION', notnull = True, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = ('Tipo de poliza'), comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r)
        ),
    Field(
        'empresa_diario_id', dbc01.tempresa_diarios, default = None,
        required = True,
        ondelete = 'NO ACTION', notnull = True, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = ('Diario especial'), comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r)
        ),
    Field(
        'concepto', 'string', length = 100, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = ('Concepto'), comment = (''),
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'folio', 'integer', default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = ('Folio de la póliza'), comment = (''),
        writable = True, readable = True,
        represent = stv_represent_number
        ),
    Field(
        'fecha', type = FIELD_UTC_DATETIME, default = request.browsernow,
        required = False, requires = IS_NULL_OR(IS_DATETIME(format = (STV_FWK_APP.FORMAT.s_DATETIME))),
        notnull = False,
        widget = stv_widget_inputDateTime, label = ('Fecha Generación'), comment = '',
        writable = False, readable = True,
        represent = stv_represent_datetime
        ),
    Field(
        'dia', 'date', default = request.browsernow,
        required = False,
        notnull = False,
        widget = stv_widget_inputDate, label = ('Fecha'), comment = '',
        writable = False, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'archivopolizanombre', 'string', length = 100, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = T('Original Filename'), comment = None,
        writable = False, readable = False,
        represent = stv_represent_string
        ),
    Field(
        'archivopoliza', type = 'upload',
        required = False,
        requires = [IS_NULL_OR(IS_LENGTH(minsize = 0, maxsize = 1572864, error_message = T('File is too huge.')))],
        notnull = False,
        uploadfield = True, widget = lambda f, d, u = None: stv_widget_inputFile(
            f, d, URL(
                a = 'app_timbrado',
                c = '255prepolizas_polizas',
                f = 'poliza_archivo',
                vars = D_stvFwkCfg.D_cfg_cuenta_para_links
                ),
            dbc01.tpolizas.archivopolizanombre
            ),
        label = ('Archivo Poliza'), comment = 'Foto del usuario',
        writable = True, readable = True, authorize = lambda record: auth.is_logged_in(),
        autodelete = False, represent = lambda v, r: stv_represent_file(
            v, r,
            URL(
                a = 'app_timbrado', c = '255prepolizas_polizas',
                f = 'poliza_archivo',
                vars = D_stvFwkCfg.D_cfg_cuenta_para_links
                ),
            dbc01.tpolizas.archivopolizanombre,
            stv_represent_file_showOption_opendownload
            ),
        uploadfolder = os.path.join(request.folder, '..', 'app_timbrado', 'uploads', 'Poliza'),
        uploadseparate = False, uploadfs = None
        ),
    Field(
        'estatus', 'integer', length = 1, default = TPOLIZAS.ESTATUS.SIN_EXPORTAR,
        required = False, requires = IS_IN_SET(TPOLIZAS.ESTATUS.get_dict(), zero = 0, error_message = ('Selecciona')),
        notnull = True, unique = False,
        widget = stv_widget_combobox,
        label = ('Estatus'), comment = 'Estatus de la póliza',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TPOLIZAS.ESTATUS.get_dict())
        ),

    # Se agregan estos campos para poder separar los folios al momento de consultarlos en la pantalla de "Sin exportar"
    Field(
        'empresa_id', dbc01.tempresas, default = None,
        required = True,
        ondelete = 'NO ACTION', notnull = True, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = ('Empresa'),
        comment = 'Empresa a la cual pertenece esta póliza contable',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r)
        ),
    Field(
        'empresa_plaza_sucursal_cajachica_id', dbc01.tempresa_plaza_sucursal_cajaschicas, default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = ('Caja chica relacionada'),
        comment = ('Caja chica relacionada'),
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r)
        ),

    # Se agregan estos campos para poder consultar el estatus de la póliza con la exportación automática. 
    Field(
        'exaut_fecha', 'date', default = request.browsernow,
        required = False, requires = IS_DATE(format = (STV_FWK_APP.FORMAT.s_DATE)),
        notnull = False,
        widget = stv_widget_inputDate, label = 'Fecha de exportación autimatica.', comment = None,
        writable = None, readable = True,
        represent = stv_represent_date
        ),
    Field(
        'exaut_log', 'text', length = 250, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = ('Log de la exportación autimatica'), comment = None,
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'exaut_estatus', 'integer', length = 1, default = TPOLIZAS.EXAUT_ESTATUS.ESPERANDO_CONFIRMACION,
        required = False, requires = IS_IN_SET(
            TPOLIZAS.EXAUT_ESTATUS.GET_DICT(), zero = 0, error_message = (
                'Selecciona')
            ),
        notnull = False, unique = False,
        widget = stv_widget_combobox,
        label = ('Estatus'), comment = 'Estatus de la exportación autimatica.',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TPOLIZAS.EXAUT_ESTATUS.GET_DICT())
        ),
    Field(
        'cuenta_servidorcontpaqi_id', 'integer', default = None,
        required = False,
        requires = IS_IN_DB(db, 'tcuenta_servidorescontpaqi.id', db.tcuenta_servidorescontpaqi._format),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = T('Servidor'), comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r,
            dbc01.tempresa_contpaqiejercicios.cuenta_servidorcontpaqi_id
            )
        ),
    Field(
        'empresa_contpaqiejercicio_id', dbc01.tempresa_contpaqiejercicios, default = None,
        required = False,
        requires = IS_IN_DB(dbc01, 'tempresa_contpaqiejercicios.id', dbc01.tempresa_contpaqiejercicios._format),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = ('Base de datos CONTPAQi'), comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r)
        ),

    tSignature_dbc01,
    singular = 'Póliza',
    plural = 'Pólizas',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )
""" Definición de la tabla de Prepolizas contables """


class TCFDI_PREPOLIZAS():

    def validacion_cancelados(self, s_cfdi_id):
        """Validación para saber si una factura cancelada se puede contabilizar.
            params:
                s_cfdi_id: Id del CFDI cancelado.
        """
        _D_return = Storage(
            s_msjError = '',
            E_return = CLASS_e_RETURN.NOK_ERROR
            )

        _dbRowCfdi = dbc01.tcfdis(s_cfdi_id)

        if _dbRowCfdi.etapa == TCFDIS.E_ETAPA.CON_POLIZA:
            _D_return.E_return = CLASS_e_RETURN.OK
        else:
            _D_return.s_msjError = 'Este CFDI no puede contabilizarse ya que no cuenta con estatus "con póliza".'

        return _D_return

    E_ETAPA = TCFDIS.E_ETAPA

    SAT_STATUS = TCFDIS.SAT_STATUS

    TIPO_CONTABILIZACION_FECHA_CANCELACION = TCFDI_REGISTROPAGOS.TIPO_CONTABILIZACION_FECHA_CANCELACION

    pass


dbc01.define_table(
    'tcfdi_prepolizas',
    Field(
        'cfdi_id', dbc01.tcfdis, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = ('CFDI'), comment = None,
        writable = False, readable = False,
        represent = lambda v, r: stv_represent_referencefield(v, r)
        ),
    Field(
        'empresa_tipopoliza_id', dbc01.tempresa_tipospoliza, default = None,
        required = True,
        ondelete = 'NO ACTION', notnull = True, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = ('Tipo de poliza'), comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r)
        ),
    Field(
        'empresa_diario_id', dbc01.tempresa_diarios, default = None,
        required = True,
        ondelete = 'NO ACTION', notnull = True, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = ('Diario especial'), comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r)
        ),
    Field(
        'concepto', 'string', length = 100, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = ('Concepto'), comment = (''),
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'poliza_id', dbc01.tpolizas, default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = ('Poliza ID'), comment = None,
        writable = False, readable = False,
        represent = lambda v, r: stv_represent_referencefield(v, r)
        ),
    # TODO: Verificar si el campo etapa se utiliza en algún lado.
    Field(
        'etapa', 'integer', default = TCFDI_PREPOLIZAS.E_ETAPA.SIN_CONTABILIZAR,
        required = False, requires = IS_IN_SET(
            TCFDI_PREPOLIZAS.E_ETAPA.get_dict(), zero = 0, error_message = (
                'Selecciona')
            ),
        notnull = True, unique = False,
        widget = stv_widget_combobox,
        label = ('Etapa'), comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TCFDI_PREPOLIZAS.E_ETAPA.get_dict())
        ),
    Field(
        'sat_status_cancelacion', 'integer', default = TCFDI_PREPOLIZAS.SAT_STATUS.VIGENTE,
        required = False, requires = IS_IN_SET(
            TCFDI_PREPOLIZAS.SAT_STATUS.GET_DICT(), zero = 0, error_message = (
                'Selecciona')
            ),
        notnull = True, unique = False,
        widget = stv_widget_combobox,
        label = ('Estatus sat'), comment = 'Rol de la empresa en el CFDI',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TCFDI_PREPOLIZAS.SAT_STATUS.GET_DICT())
        ),
    Field(
        'fecha', type = FIELD_UTC_DATETIME, default = None,
        required = False, requires = IS_NULL_OR(IS_DATETIME(format = (STV_FWK_APP.FORMAT.s_DATETIME))),
        notnull = False,
        widget = stv_widget_inputDateTime, label = ('Fecha emisión/cancelación'), comment = '',
        writable = False, readable = True,
        represent = stv_represent_datetime
        ),
    Field(
        'tipo_contabilizacion_fecha_cancelacion', 'integer',
        default = TCFDI_PREPOLIZAS.TIPO_CONTABILIZACION_FECHA_CANCELACION.EMISION,
        required = False, requires = IS_IN_SET(
            TCFDI_PREPOLIZAS.TIPO_CONTABILIZACION_FECHA_CANCELACION.get_dict(), zero = 0, error_message = (
                'Selecciona')
            ),
        notnull = False, unique = False,
        widget = stv_widget_combobox,
        label = ('Tipo de contabilización de la fecha de cancelación'), comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(
            v, r, None, TCFDI_PREPOLIZAS.TIPO_CONTABILIZACION_FECHA_CANCELACION.get_dict()
            )
        ),
    tSignature_dbc01,
    singular = 'Prepoliza',
    plural = 'Prepolizas',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )

""" Definición de los Asientos contables """


class TCFDI_PREPOLIZA_ASIENTOSCONTABLES():
    class TIPOCREACION():
        ''' Se definen las opciónes del campo '''

        # Estas opciones pertenecen a los conceptos de Tipo Entrada.
        AUTOMATICO = 1
        MANUAL = 2

        @classmethod
        def GET_DICT(cls):
            return {
                cls.AUTOMATICO: ('Automatico'),
                cls.MANUAL: ('Manual'),
                }

    pass


dbc01.define_table(
    'tcfdi_prepoliza_asientoscontables',
    ### Este campo se permitió que se ingresara nulo para el manejo de los depósitos bancarios. 
    Field(
        'cfdi_prepoliza_id', dbc01.tcfdi_prepolizas, default = None,
        required = False,
        ondelete = 'CASCADE', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = ('Prepoliza'), comment = None,
        writable = False, readable = False,
        represent = lambda v, r: stv_represent_referencefield(v, r)
        ),
    Field(
        'descripcion', 'string', length = 100, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = ('Descripción'), comment = (''),
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'asiento_contable', 'string', length = 4, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = ('Asiento'), comment = (''),
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'asiento_contable_poliza', 'string', length = 4, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = ('Asiento'), comment = (''),
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'cuenta_contable', 'string', length = 30, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = ('Cuenta'), comment = (''),
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'referencia', 'string', length = 20, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = ('Referencia'), comment = (''),
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'empresa_diario_id', dbc01.tempresa_diarios, default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = ('Diario'), comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r)
        ),
    Field(
        'empresa_segmento_id', dbc01.tempresa_segmentos, default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = ('Segmento'), comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r)
        ),
    Field(
        'importe_cargo', 'decimal(20,4)', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputMoney, label = 'Cargo', comment = None,
        writable = True, readable = True,
        represent = stv_represent_money_ifnotzero
        ),
    Field(
        'importe_abono', 'decimal(20,4)', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputMoney, label = 'Abono', comment = None,
        writable = True, readable = True,
        represent = stv_represent_money_ifnotzero
        ),
    Field(
        'importe_moneda_extranjera', 'decimal(20,4)', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputMoney, label = 'Complemento', comment = None,
        writable = True, readable = True,
        represent = stv_represent_money_ifnotzero
        ),

    # Campos necesarios para relacionar el asiento con el impuesto generado del cfdi relacionado
    Field(
        'cfdi_ingreso_id', dbc01.tcfdis, default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = ('CFDI Ingreso'), comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r)
        ),

    Field(
        'impuesto_id', 'integer', default = None,
        required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'timpuestos.id', db01.timpuestos._format)),
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = ('Impuesto'), comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(
            v, r,
            dbc01.tcfdi_concepto_impuestostrasladados.impuesto_id
            )
        ),

    Field(
        'tasaocuota', 'decimal(14,4)', default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputMoney, label = 'Tasa o Cuota', comment = None,
        writable = False, readable = True,
        represent = stv_represent_money_ifnotzero
        ),

    Field(
        'errores', 'string', length = 200, default = '',
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = ('Errores'), comment = (''),
        writable = False, readable = True,
        represent = stv_represent_string
        ),

    Field(
        'esingreso', 'boolean', default = '0',
        required = True,
        notnull = False,
        label = ('Banco'), comment = None,
        writable = False, readable = True,
        represent = stv_represent_boolean
        ),
    Field(
        'escliente', 'boolean', default = '0',
        required = True,
        notnull = False,
        label = ('Cliente'), comment = None,
        writable = False, readable = True,
        represent = stv_represent_boolean
        ),
    Field(
        'esconcepto_porcobrar', 'boolean', default = '0',
        required = True,
        notnull = False,
        label = ('Concepto por cobrar'), comment = None,
        writable = False, readable = True,
        represent = stv_represent_boolean
        ),
    Field(
        'esconcepto_cobrado', 'boolean', default = '0',
        required = True,
        notnull = False,
        label = ('Concepto cobrado'), comment = None,
        writable = False, readable = True,
        represent = stv_represent_boolean
        ),
    Field(
        'esconcepto_dctoporcobrar', 'boolean', default = '0',
        required = True,
        notnull = False,
        label = ('Concepto dcto. por cobrar'), comment = None,
        writable = False, readable = True,
        represent = stv_represent_boolean
        ),
    Field(
        'esconcepto_dctocobrado', 'boolean', default = '0',
        required = True,
        notnull = False,
        label = ('Concepto dcto. cobrado'), comment = None,
        writable = False, readable = True,
        represent = stv_represent_boolean
        ),

    Field(
        'esimpuesto_trasladado', 'boolean', default = '0',
        required = True,
        notnull = False,
        label = ('Imp. Trasladado'), comment = None,
        writable = False, readable = True,
        represent = stv_represent_boolean
        ),
    Field(
        'esimpuesto_portrasladar', 'boolean', default = '0',
        required = True,
        notnull = False,
        label = ('Imp. por trasladar'), comment = None,
        writable = False, readable = True,
        represent = stv_represent_boolean
        ),

    Field(
        'esimpuesto_retenido', 'boolean', default = '0',
        required = True,
        notnull = False,
        label = ('Imp. Retenido'), comment = None,
        writable = False, readable = True,
        represent = stv_represent_boolean
        ),
    Field(
        'esimpuesto_porretener', 'boolean', default = '0',
        required = True,
        notnull = False,
        label = ('Imp. por retenes'), comment = None,
        writable = False, readable = True,
        represent = stv_represent_boolean
        ),

    Field(
        'poliza_id', dbc01.tpolizas, default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = ('Poliza ID'), comment = None,
        writable = False, readable = False,
        represent = lambda v, r: stv_represent_referencefield(v, r)
        ),

    Field(
        'empresa_tipodocumentobancario_id', dbc01.tempresa_tipodocumentosbancarios, default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = ('Tipo documento bancario'), comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r)
        ),

    Field(
        'empresa_tipodocumentobancario_folio', 'integer', length = 20, default = 0,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_inputFloat, label = 'Folio pago', comment = None,
        writable = True, readable = True,
        represent = stv_represent_number
        ),

    Field(
        'tipocreacion', 'integer', length = 1, default = TCFDI_PREPOLIZA_ASIENTOSCONTABLES.TIPOCREACION.AUTOMATICO,
        required = False, requires = IS_IN_SET(
            TCFDI_PREPOLIZA_ASIENTOSCONTABLES.TIPOCREACION.GET_DICT(), zero = 0,
            error_message = 'Selecciona'
            ),
        notnull = True, unique = False,
        widget = stv_widget_combobox,
        label = ('tipocreacion'), comment = 'Tipo de creación del campo en la tabla.',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_list(
            v, r, None,
            TCFDI_PREPOLIZA_ASIENTOSCONTABLES.TIPOCREACION.GET_DICT()
            )
        ),

    tSignature_dbc01,
    singular = 'Asiento',
    plural = 'Asientos',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )


def _prepoliza_generar_getCamposPorEjercicio(s_aniocfdi, dbTable, dbQry, D_regresarCampos, dbFieldEjercicio = None):
    '''Función generica para obtener las cuentas contables
    
        Args:
            s_maestro_id: id de la tabla maestro
            s_ejercicio: año del CFDI
            dbField_maestro_id: nombre del campo que contiene el id del maestro
            dbField_cuenta: nombre del campo que tiene la cuenta contable
            dbField_cuentacomplemento: nombre del campo que tiene la cuenta contable complemento
    
    '''

    _D_return = D_regresarCampos

    if dbFieldEjercicio:
        _dbFieldEjercicio = dbFieldEjercicio
    else:
        _dbFieldEjercicio = dbTable['ejercicio']

    _LdbFields = []
    for _s_campo in D_regresarCampos:
        _LdbFields += [dbTable[D_regresarCampos[_s_campo]]]

    _dbRows_ejercicios = dbc01(
        dbQry
        & (_dbFieldEjercicio <= s_aniocfdi)
        ).select(
        *_LdbFields,
        orderby = ~_dbFieldEjercicio,
        limitby = (0, 1)
        )

    if (_dbRows_ejercicios):
        _dbRow_ejercicio = _dbRows_ejercicios.first()
        for _s_campo in D_regresarCampos:
            _D_return[_s_campo] = _dbRow_ejercicio[D_regresarCampos[_s_campo]]
    else:
        # No hay cunetas contables detectadas
        _D_return = Storage()

    return _D_return


def _poliza_get_estilopoliza(s_poliza_id):
    '''Función para conseguir el estilo de la póliza
            
            s_poliza_id: ID de la póliza
            
    '''
    _D_return = Storage(
        S_msgError = "",
        # E_return = CLASS_e_RETURN.NOK_ERROR,
        n_foliado = 0,
        )

    # Se obtiene todos los datos de la póliza.
    _dbRowPoliza = dbc01(dbc01.tpolizas.id == s_poliza_id).select(dbc01.tpolizas.ALL)

    # Se consulta la sucursal para poder obtener el estilo de póliza que se está manejando.
    _dbRowSucursal_Poliza = dbc01(
        dbc01.tempresa_plaza_sucursal_cajaschicas.id == _dbRowPoliza[0].empresa_plaza_sucursal_cajachica_id
        ).select(dbc01.tempresa_plaza_sucursal_cajaschicas.empresa_plaza_sucursal_id)

    # El año se saca de la póliza que a su vez este se sacó del CFDI o los CFDIs relacionados.
    _s_anio = str(_dbRowPoliza[0].dia.year)

    # Se consulta el estilo de póliza que se está manejando.
    _dbTable = dbc01.tempresa_plaza_sucursal_contabilidades

    _dbQry = (_dbTable.empresa_plaza_sucursal_id == _dbRowSucursal_Poliza[0].empresa_plaza_sucursal_id)

    _D_return.n_foliado = _prepoliza_generar_getCamposPorEjercicio(
        s_aniocfdi = _s_anio,
        dbTable = _dbTable,
        dbQry = _dbQry,
        D_regresarCampos = Storage(
            estilopoliza = 'estilopoliza'
            )
        )

    return (_D_return)
