# -*- coding: utf-8 -*-

""" Se definen las tablas de CFDIs para recibidos (RX) """


# noinspection SpellCheckingInspection


class TCFDISRX(Table):
    """ Tabla usada para tener los CFDIs con su información maestro.
    
    """
    S_NOMBRETABLA = 'tcfdisrx'

    # Se copian subclases completas para mantener compatibilidad

    E_PROVEEDOR_ERROR = TCFDIS.E_PROVEEDOR_ERROR
    E_ERRORES_PAC = TCFDIS.E_ERRORES_PAC
    E_GENERADO_POR = TCFDIS.E_GENERADO_POR
    E_ROL = TCFDIS.E_ROL
    E_ETAPA = TCFDIS.E_ETAPA
    E_ESTADO = TCFDIS.E_ESTADO
    V_VENCIMIENTO = TCFDIS.V_VENCIMIENTO
    V_ESTATUSCXC = TCFDIS.V_ESTATUSCXC
    V_DVENCIDO = TCFDIS.V_DVENCIDO
    SAT_STATUSCANCELACION = TCFDIS.SAT_STATUSCANCELACION
    SAT_STATUSPROCESOCANCELACION = TCFDIS.SAT_STATUSPROCESOCANCELACION
    SAT_STATUS = TCFDIS.SAT_STATUS

    L_DBCAMPOS = [
        Field(
            'empresa_id', dbc01.tempresas, default = None,
            required = True,
            ondelete = 'NO ACTION', notnull = True, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1),
            label = 'Empresa', comment = 'Empresa que tiene registrado el cfdi ya sea como emisor o receptor',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r)
            ),
        Field(
            'versioncfdi', 'string', length = 50, default = '3.3',
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Vesion', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        # Serie debe estar relacionado a la serie de la empresa
        Field(
            'serie', 'string', length = 50, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Serie', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        # Serie debe estar relacionado al folio en la empresa del tipo de documento
        Field(
            'folio', 'string', length = 50, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Folio', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'fecha_str', 'string', length = 50, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Fecha', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),

        # Se graba como la fecha-hora local de emisión del CFDI
        Field(
            'fecha', type = FIELD_UTC_DATETIME, default = request.browsernow,
            required = False, requires = IS_NULL_OR(IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME)),
            notnull = False,
            widget = stv_widget_inputDateTime, label = 'Fecha', comment = '',
            writable = False, readable = True,
            represent = stv_represent_datetime
            ),
        Field(
            'dia', 'date', default = None,
            required = False, requires = IS_NULL_OR(IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE)),
            notnull = False,
            widget = stv_widget_inputDate, label = 'Fecha', comment = '',
            writable = False, readable = False,
            represent = stv_represent_date
            ),
        Field(
            'hora', 'time', default = None,  # Se graba como la hora local de emisión del CFDI
            required = False,
            notnull = False,
            widget = stv_widget_input, label = 'Hora', comment = '',
            writable = False, readable = False,
            represent = stv_represent_timeAsText
            ),
        Field(
            'formapago', 'string', length = 50, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Forma pago', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'formapago_id', 'integer', default = TFORMASPAGO.EFECTIVO,
            required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tformaspago.id', db01.tformaspago._format)),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Forma pago', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tcfdisrx.formapago_id)
            ),
        Field(
            'condicionesdepago', 'string', length = 200, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Condiciones de pago', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'subtotal', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'SubTotal', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_money_ifnotzero(v, r, dbField = dbc01.tcfdisrx.subtotal)
            ),
        Field(
            'descuento', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Descuento', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_money(v, r, dbField = dbc01.tcfdisrx.descuento)
            ),
        Field(
            'moneda', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Moneda', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        TMONEDAS.DEFINIR_DBCAMPO(
            'moneda_id',
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tcfdisrx.moneda_id)
            ),

        # Este será el campo que se utilice en vez de moneda_id
        TEMPRESA_MONEDASCONTPAQI.DEFINIR_DBCAMPO(
            'monedacontpaqi_id',
            represent = lambda v, r: stv_represent_referencefield(
                v, r, dbc01.tcfdirx_complementopago_doctosrelacionados.monedacontpaqi_id
                )
            ),

        Field(
            'tipocambio', 'decimal(14,4)', default = 1,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Tipo de cambio', comment = None,
            writable = False, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        Field(
            'total', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Total', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_money(v, r, dbField = dbc01.tcfdisrx.total)
            ),
        Field(
            'totalpago', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Total Pago', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_money(v, r, dbField = dbc01.tcfdisrx.total)
            ),
        Field(
            'saldo', 'decimal(14,4)', default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Saldo', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_money(v, r, dbField = dbc01.tcfdisrx.saldo)
            ),
        Field(
            'saldofechacalculo', type = TDATETIME_GRABADIRECTO_LEERENZONAH, default = None,
            # Se graba como la fecha-hora local de emisión del CFDI
            required = False, requires = IS_NULL_OR(IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME)),
            notnull = False,
            widget = stv_widget_inputDateTime, label = 'Fecha Saldo', comment = 'Fecha de recálculo de saldo',
            writable = False, readable = True,
            represent = stv_represent_datetime
            ),
        Field(
            'tipodecomprobante', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Tipo de Comprobante', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'tipocomprobante_id', 'integer', default = None,
            required = False,
            requires = IS_NULL_OR(IS_IN_DB(db01, 'ttiposcomprobantes.id', db01.ttiposcomprobantes._format)),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Tipo comprobante', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tcfdisrx.tipocomprobante_id)
            ),
        Field(
            'metodopago', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Método de pago', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'metodopago_id', 'integer', default = None,
            required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tmetodospago.id', db01.tmetodospago._format)),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Método de pago', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tcfdisrx.metodopago_id)
            ),
        Field(
            'lugarexpedicion', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Lugar exp.', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'lugarexpedicion_id', 'integer', default = None,
            required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tcodigospostales.id', '%(c_codigopostal)s')),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_search(
                f, v,
                s_display = '%(c_codigopostal)s',
                s_url = URL(a = 'app_generales', c = '005genericos_sat', f = 'codigospostales_buscar'),
                D_additionalAttributes = {'reference': db01.tcodigospostales.id}
                ),
            label = 'Lugar', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tcfdisrx.lugarexpedicion_id)
            ),
        Field(
            'totalimpuestostrasladados', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Total imp. trasladados', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_money(v, r, dbField = dbc01.tcfdisrx.totalimpuestostrasladados)
            ),
        Field(
            'totalimpuestosretenidos', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Total impuestos retenidos', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_money(v, r, dbField = dbc01.tcfdisrx.totalimpuestosretenidos)
            ),
        Field(
            'credito_plazo', 'integer', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputInteger, label = 'Plazo (días)', comment = 'Días de crédito',
            writable = False, readable = True,
            represent = stv_represent_number
            ),

        # Sección de CFDI Relacionados
        Field(
            'tiporelacion', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Tipo relación', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'tiporelacion_id', 'integer', default = None,
            required = False,
            requires = IS_NULL_OR(IS_IN_DB(db01, 'ttiposrelaciones.id', db01.ttiposrelaciones._format)),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Tipo relación', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tcfdisrx.tiporelacion_id)
            ),

        # Timbre fiscal digital
        Field(
            'uuid', 'string', length = 50, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'UUID', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'fechatimbrado_str', 'string', length = 50, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Fecha timbrado', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'fechatimbrado', type = FIELD_UTC_DATETIME, default = None,
            # Se graba como la fecha-hora local de emisión del CFDI
            required = False, requires = IS_NULL_OR(IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME)),
            notnull = False,
            widget = stv_widget_inputDateTime, label = 'Timbrado', comment = '',
            writable = False, readable = True,
            represent = stv_represent_datetime
            ),
        Field(
            'rol', 'integer', default = 0,
            required = False, requires = IS_IN_SET(E_ROL.D_TODOS, zero = 0, error_message = 'Selecciona'),
            notnull = True, unique = False,
            widget = stv_widget_combobox,
            label = 'Rol', comment = 'Rol de la empresa en el CFDI',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TCFDISRX.E_ROL.D_TODOS)
            ),
        Field(
            'etapa', 'integer', default = E_ETAPA.SIN_CONTABILIZAR,
            required = False, requires = IS_IN_SET(E_ETAPA.D_TODOS, zero = 0, error_message = 'Selecciona'),
            notnull = True, unique = False,
            widget = stv_widget_combobox,
            label = 'Etapa', comment = 'Rol de la empresa en el CFDI',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TCFDISRX.E_ETAPA.D_TODOS)
            ),
        Field(
            'etapa_cancelado', 'integer', default = E_ETAPA.SIN_CONTABILIZAR,
            required = False, requires = IS_IN_SET(E_ETAPA.D_TODOS, zero = 0, error_message = 'Selecciona'),
            notnull = True, unique = False,
            widget = stv_widget_combobox,
            label = 'Etapa', comment = 'Rol de la empresa en el CFDI',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TCFDISRX.E_ETAPA.D_TODOS)
            ),
        Field(
            'estado', 'string', length = 1, default = E_ESTADO.NO_DEFINIDO,
            required = False,
            requires = IS_IN_SET(E_ESTADO.D_TODOS, zero = 0, error_message = 'Selecciona'),
            notnull = True, unique = False,
            widget = stv_widget_combobox,
            label = 'Estado', comment = 'Rol de la empresa en el CFDI',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TCFDISRX.E_ESTADO.D_TODOS)
            ),
        Field(
            'generado_por', 'integer', default = 0,
            required = False,
            requires = IS_IN_SET(E_GENERADO_POR.D_TODOS, zero = 0, error_message = 'Selecciona'),
            notnull = True, unique = False,
            widget = stv_widget_combobox,
            label = 'Generado', comment = 'De donde se generó el CFDI',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TCFDISRX.E_GENERADO_POR.D_TODOS)
            ),

        Field(
            'errores', 'string', length = 200, default = '',
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Inconsistencias', comment = '',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_string(v, r, dbField = dbc01.tcfdisrx.errores)
            ),

        # Proveedor (recibe factura) o cliente (si es pago)
        Field(
            'emisorrfc', 'string', length = 15, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Emisor RFC', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'emisornombrerazonsocial', 'string', length = 120, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Emisor razón social', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'emisorregimenfiscal', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Emisor Regimen Fiscal', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'emisorproveedor_id', dbc01.tempresa_proveedores, default = None,
            required = False,
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = (
                'Proveedor'), comment = 'En caso de recibir la factura, se relaciona la empresa que genera el cfdi',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r)
            ),

        Field(
            'receptorrfc', 'string', length = 15, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Receptor RFC', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'receptornombrerazonsocial', 'string', length = 120, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Receptor razón social', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'receptorusocfdi', 'string', length = 15, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Receptor RFC', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'receptorusocfdi_id', 'integer', default = None,
            required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tusoscfdi.id', db01.tusoscfdi._format)),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Receptor uso CFDI', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tcfdisrx.receptorusocfdi_id)
            ),
        # Este campo deberá ser nulo y es para uso en caso de usar diferentes nombres comerciales la misma empresa
        #  a los que se le puedan facturar
        Field(
            'receptorcliente_id', dbc01.tempresa_clientes, default = None,
            required = False,
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = (
                'Cliente'),
            comment = 'En caso de ingreso, se relaciona la empresa a la que se dirige la factura (ingreso)',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r)
            ),

        # Información adicional del SAT
        Field(
            'sat_status', 'integer', default = SAT_STATUS.NO_DEFINIDO,
            required = False,
            requires = IS_IN_SET(SAT_STATUS.D_TODOS, zero = 0, error_message = 'Selecciona'),
            notnull = False, unique = False,
            widget = stv_widget_combobox,
            label = 'SAT Estatus', comment = 'Status del CFDI de acuerdo al SAT',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TCFDISRX.SAT_STATUS.D_TODOS)
            ),
        Field(
            'sat_fecha_cancelacion', type = FIELD_UTC_DATETIME, default = None,
            required = False, requires = IS_NULL_OR(IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME)),
            notnull = False,
            widget = stv_widget_inputDateTime, label = 'Fecha cancelación segun el SAT', comment = '',
            writable = False, readable = True,
            represent = stv_represent_datetime
            ),
        Field(
            'sat_status_cancelacion', 'integer', default = SAT_STATUSCANCELACION.NO_DEFINIDO,
            required = False,
            requires = IS_IN_SET(SAT_STATUSCANCELACION.D_TODOS, zero = 0, error_message = 'Selecciona'),
            notnull = False, unique = False,
            widget = stv_widget_combobox,
            label = 'SAT Status Cancelación', comment = 'Status Cancelación del CFDI de acuerdo al SAT',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TCFDISRX.SAT_STATUSCANCELACION.D_TODOS)
            ),
        Field(
            'sat_status_proceso_cancelacion', 'integer', default = SAT_STATUSPROCESOCANCELACION.NO_DEFINIDO,
            required = False,
            requires = IS_IN_SET(
                SAT_STATUSPROCESOCANCELACION.D_TODOS, zero = 0, error_message = 'Selecciona'
                ),
            notnull = False, unique = False,
            widget = stv_widget_combobox,
            label = 'SAT Status Proceso Canc.', comment = 'Status Proceso Cancelación del CFDI de acuerdo al SAT',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TCFDISRX.SAT_STATUSPROCESOCANCELACION.D_TODOS)
            ),

        Field(
            'pac_rfc', 'string', length = 15, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'PAC RFC', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'pac_razon_social', 'string', length = 240, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'PAC Razon Social', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'pac_nombre_comercial', 'string', length = 120, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'PAC Nombre Comercial', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'pac_proveedor_error', 'integer', default = E_PROVEEDOR_ERROR.NO_DEFINIDO,
            required = False, requires = IS_IN_SET(
                E_PROVEEDOR_ERROR.D_TODOS, zero = 0, error_message = 'Selecciona'
                ),
            notnull = False, unique = False,
            widget = stv_widget_combobox,
            label = 'Proveedor del error', comment = '',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TCFDISRX.E_PROVEEDOR_ERROR.D_TODOS)
            ),
        Field(
            'pac_error', 'integer', default = E_ERRORES_PAC.NO_DEFINIDO,
            required = False, requires = IS_IN_SET(
                E_ERRORES_PAC.D_TODOS, zero = 0, error_message = 'Selecciona'
                ),
            notnull = False, unique = False,
            widget = stv_widget_combobox,
            label = 'Error PAC', comment = '',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TCFDISRX.E_ERRORES_PAC.D_TODOS)
            ),

        Field(
            'cancelado_folio', 'string', length = 100, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Folio de Cancelación', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        tSignature_db01,
        ]

    def __init__(self, dbRef, tablename, *fields, **args):
        fields += (
            TCFDISRX.V_VENCIMIENTO.GET_FIELDVIRTUAL(),
            TCFDISRX.V_ESTATUSCXC.GET_FIELDVIRTUAL(),
            TCFDISRX.V_DVENCIDO.GET_FIELDVIRTUAL()
            )
        Table.__init__(self, dbRef, tablename, *fields, **args)
        self.PROC.dbTabla = self

        return

    @staticmethod
    def DEFINIR_DBCAMPO(s_nombreCampo = 'cfdirelacionado_id', **D_params):
        D_params = Storage(D_params)
        _dbCampo = Field(
            s_nombreCampo, dbc01.tcfdisrx, default = D_params.default or None,
            required = D_params.required or False,
            requires = D_params.requires or IS_IN_DB(
                dbc01(dbc01.tcfdisrx.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id),
                'tcfdisrx.id',
                dbc01.tcfdisrx._format
                ),
            ondelete = 'NO ACTION', notnull = D_params.notnull or False, unique = False,
            widget = D_params.widget or (
                lambda f, v: stv_widget_db_search(
                    f, v, s_display = '%(serie)s %(folio)s',
                    s_url = URL(a = 'app_timbrado', c = '250cfdirx', f = 'cfdi_buscar'),
                    D_additionalAttributes = {'reference': dbc01.tcfdisrx.id}
                    )
                ),
            label = D_params.label or 'CFDI Relacionado', comment = D_params.comment or "",
            writable = False, readable = True,
            represent = D_params.represent or stv_represent_referencefield
            )

        if not D_params.requires and not D_params.required:
            _dbCampo.requires = IS_NULL_OR(_dbCampo.requires)
        else:
            pass

        return _dbCampo

    GET_PRIMERDIA2020 = TCFDIS.GET_PRIMERDIA2020

    @classmethod
    def OBTENER_CFDISINGRESOS_AFECTADOS(cls, x_cfdirx):
        """ Obtiene todos los ids de ingresos que deben recalcularse
        """
        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx, dbc01.tcfdisrx)
        _dbRow = _D_results.dbRow

        _L_cfdis = []  # Ids de CFDIs de ingreso que se asocian con movimientos posteriores

        if _dbRow.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO:
            # Si es ingreso, se utiliza el CFDI del parametro
            _L_cfdis.append(_dbRow.id)
            if _dbRow.cfdi_complemento_id:
                _L_cfdis.append(_dbRow.cfdi_complemento_id)
            else:
                pass

        elif _dbRow.tipocomprobante_id in (TTIPOSCOMPROBANTES.PAGO, TTIPOSCOMPROBANTES.EGRESO):
            # Si es de pago o egreso, se utilizarán los ids especificados en los documentos relacionados

            _dbRows = []
            if _dbRow.tipocomprobante_id == TTIPOSCOMPROBANTES.PAGO:
                _dbRows = dbc01(
                    (dbc01.tcfdirx_complementopagos.cfdi_id == _dbRow.id)
                    & (
                            dbc01.tcfdirx_complementopago_doctosrelacionados.cfdi_complementopago_id
                            == dbc01.tcfdirx_complementopagos.id
                        )
                    ).select(
                        dbc01.tcfdirx_complementopago_doctosrelacionados.id,
                        dbc01.tcfdirx_complementopago_doctosrelacionados.cfdirelacionado_id,
                        )

            elif _dbRow.tipocomprobante_id == TTIPOSCOMPROBANTES.EGRESO:
                _dbRows = dbc01(
                    (dbc01.tcfdirx_relacionados.cfdi_id == _dbRow.id)
                    ).select(
                        dbc01.tcfdirx_relacionados.id,
                        dbc01.tcfdirx_relacionados.cfdirelacionado_id,
                        )

            else:
                raise "Este código no debe ejecutarse nunca"

            for _dbRow_cfdirx in _dbRows:
                _L_cfdis.append(_dbRow_cfdirx.cfdirelacionado_id)

        else:

            # No se reconoce el tipo de documento
            pass

        return _L_cfdis

    @classmethod
    def OBTENER_CFDIS_POSTERIORES(cls, x_cfdirx):
        """ Obtiene los CFDIs activos posteriores al cfdi actual

        @param x_cfdirx:
        @type x_cfdirx:
        @return:
        @rtype:
        """
        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx, dbc01.tcfdisrx)
        _dbRow = _D_results.dbRow

        _L_cfdis = cls.OBTENER_CFDISINGRESOS_AFECTADOS(_dbRow)

        if _dbRow.tipocomprobante_id == TTIPOSCOMPROBANTES.PAGO:

            _dbField_fechaPagoMax = dbc01.tcfdirx_complementopagos.fechapago.max()
            _dbRows_pagosMax = dbc01(
                dbc01.tcfdirx_complementopagos.cfdi_id == _dbRow.id
                ).select(
                _dbField_fechaPagoMax
                )

            # En el caso de campos calculados, siempre regresa un registro
            _dt_fechaPagoMax = _dbRows_pagosMax.first()[_dbField_fechaPagoMax]

            if _dt_fechaPagoMax:

                if _dt_fechaPagoMax < _dbRow.fecha:
                    _D_return.s_msgError = "Pago creado con fecha anterior a CFDI de ingreso."
                    _D_return.E_return = stvfwk2_e_RETURN.OK_WARNING
                    _dt_fechaPagoMax = _dbRow.fecha
                else:
                    pass

                # Se procede a buscar los movimientos posteriores relacionados con los ingresos a
                #  la fecha del CFDI del parámetro
                _dbRows = dbc01(
                    dbc01.tcfdirx_movimientos.cfdi_ingreso_id.belongs(_L_cfdis)
                    & (dbc01.tcfdirx_movimientos.fecha > _dt_fechaPagoMax)
                    ).select(
                    dbc01.tcfdirx_movimientos.id,
                    dbc01.tcfdirx_movimientos.cfdi_id
                    )
            else:
                _dbRows = None

        else:

            # Se procede a buscar los movimientos posteriores relacionados con los ingresos a la fecha
            #  del CFDI del parámetro
            _dbRows = dbc01(
                dbc01.tcfdirx_movimientos.cfdi_ingreso_id.belongs(_L_cfdis)
                & (dbc01.tcfdirx_movimientos.fecha > _dbRow.fecha)
                ).select(
                dbc01.tcfdirx_movimientos.id,
                dbc01.tcfdirx_movimientos.cfdi_id
                )

        return _dbRows

    class PROC:

        dbTabla = None  # Se escribe con la inicialización
        dbTabla2 = None  # Se escribe con la inicialización de Tabla2, contendrá los campos adicionales

        def __init__(self, **D_defaults):
            """ Inicializa objecto de tipo PROC para control del registro actual

            @param D_defaults:
            @type D_defaults:
            """
            self._D_defaults = Storage(D_defaults)
            self._L_requeridos = ['empresa_id']
            self.dbTabla.notas.readable = True
            return

        def puede_crear(self, **D_defaults):
            """ Determina si puede o no crear un CFDIRX.

            Actualmente no hay condición requerida.
            """
            _D_return = FUNC_RETURN()

            self._D_defaults.update(D_defaults or {})

            for _s_requerido in self._L_requeridos:
                if _s_requerido not in self._D_defaults:
                    _D_return.agrega_error("Campo %s es requerido para generar CFDI" % _s_requerido)

                else:
                    pass

            _D_return.agrega_error("Los CFDI recibidos no pueden ser creados, solo importarlos")

            return _D_return

        @classmethod
        def PUEDE_EDITAR(
                cls,
                x_cfdirx,
                ):
            """ Regresa True si el estado permite modificar, de lo contrario False

            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx, cls.dbTabla)
            _dbRow = _D_results.dbRow

            if not _dbRow:
                _D_return.agrega_error("CFDI no identificado.")

            elif not _dbRow.uuid \
                    and (_dbRow.estado in (TCFDISRX.E_ESTADO.NO_DEFINIDO, TCFDISRX.E_ESTADO.TIMBRE_RECHAZADO)):

                _dbRows = TCFDISRX.OBTENER_CFDIS_POSTERIORES(_dbRow)

                if _dbRows:
                    # Si existen registros, significa que tiene movimientos posteriores al CFDI del parámetro
                    #  que se intenta modificar
                    _L_cfdis = [str(_dbRow.cfdi_id) for _dbRow in _dbRows]
                    _D_return.agrega_error("CFDI con movimientos posteriores. CFDI ID %s" % ", ".join(_L_cfdis))

                else:
                    pass

            elif _dbRow.estado in TCFDISRX.E_ESTADO.L_SINPROCESOPENDIENTE:
                _D_return.agrega_error("CFDI timbrado y no puede ser modificado.")

            else:
                # Si el CFDI está en un proceso con estado transitorio

                # Se verifica si existen instrucciones en proceso
                _dbRows_instrucciones = db(
                    (db.tservidor_instrucciones.cuenta_id == D_stvFwkCfg.dbRow_cuenta.id)
                    & db.tservidor_instrucciones.tipoinstruccion.belongs(
                        TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.L_CONCFDI_EN_PARAM3
                        )
                    & db.tservidor_instrucciones.estado.belongs(TSERVIDOR_INSTRUCCIONES.E_ESTADO.L_ACTIVOS)
                    & (db.tservidor_instrucciones.s_param3 == _dbRow.id)
                    ).select(
                    db.tservidor_instrucciones.ALL,
                    )

                if _dbRows_instrucciones:
                    # TODO evaluar si el timeout esta done, para cancelar o terminar la instrucción
                    _D_return.agrega_error("CFDI en estado transitorio.")

                elif _dbRow.uuid:

                    if _dbRow.sat_status == TCFDISRX.SAT_STATUS.VIGENTE:
                        _dbRow.estado = TCFDISRX.E_ESTADO.VIGENTE
                        _D_return.agrega_error("CFDI en Vigente.")

                    elif _dbRow.sat_status == TCFDISRX.SAT_STATUS.CANCELADO:
                        _dbRow.estado = TCFDISRX.E_ESTADO.CANCELADO
                        _D_return.agrega_error("CFDI Cancelado.")

                    elif _dbRow.estado == TCFDISRX.E_ESTADO.EN_PROCESO_CANCELACION:
                        _dbRow.estado = TCFDISRX.E_ESTADO.VIGENTE
                        _D_return.agrega_advertencia(
                            "Como no existe instrucción pendiente, "
                            + "se regresa al estado anterior"
                            )

                    elif _dbRow.estado == TCFDISRX.E_ESTADO.EN_PROCESO_FIRMA:
                        _dbRow.estado = TCFDISRX.E_ESTADO.NO_DEFINIDO
                        _D_return.agrega_advertencia(
                            "Como no existe instrucción pendiente, "
                            + "se regresa al estado anterior"
                            )

                    else:
                        _D_return.agrega_error("CFDI en estado no reconocido, contacte al administrador")
                    _dbRow.update_record()

                else:
                    _dbRow.estado = TCFDISRX.E_ESTADO.NO_DEFINIDO
                    _dbRow.update_record()

            return _D_return

        @classmethod
        def PUEDE_ELIMINAR(cls, x_cfdirx):
            """ Determina si puede o no eliminar un registro

            Misma condición que puede_editar, a lo mejor pudieran verse condiciones distintas

            """

            return cls.PUEDE_EDITAR(x_cfdirx)

        def configuracampos_nuevo(
                self,
                b_llamadaDesdeEditar = False,
                **D_defaults
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param b_llamadaDesdeEditar:
            @type b_llamadaDesdeEditar:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            self._D_defaults.update(D_defaults or {})
            for _s_fieldName in self.dbTabla.fields:
                self.dbTabla[_s_fieldName].writable = False
            for _s_fieldName in self._D_defaults:
                self.dbTabla[_s_fieldName].default = self._D_defaults[_s_fieldName]

            if not b_llamadaDesdeEditar:
                _D_results = self.puede_crear()
            else:
                _D_results = _D_return

            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.combinar(_D_results)

            else:
                pass

            self.dbTabla.empresa_id.writable = False
            self.dbTabla.empresa_plaza_sucursal_cajachica_id.writable = False
            self.dbTabla.fecha.writable = False
            self.dbTabla.formapago_id.writable = False
            self.dbTabla.condicionesdepago.writable = False
            self.dbTabla.moneda_id.writable = False
            self.dbTabla.monedacontpaqi_id.writable = False
            self.dbTabla.tipocambio.writable = False
            self.dbTabla.tipocomprobante_id.writable = False
            self.dbTabla.metodopago_id.writable = False
            self.dbTabla.lugarexpedicion_id.writable = False
            self.dbTabla.tiporelacion_id.writable = False
            self.dbTabla.tiporelacion_id.requires = IS_NULL_OR(
                IS_IN_DB(db01, 'ttiposrelaciones.id', db01.ttiposrelaciones._format)
                )
            self.dbTabla.tiporelacion_id.comment = "Configurar únicamente en el caso de agregar CFDIs relacionados"
            self.dbTabla.emisorproveedor_id.writable = False
            self.dbTabla.receptorusocfdi_id.writable = False
            self.dbTabla.receptorcliente_id.writable = False  # Se pone por default el cliente actual
            self.dbTabla.uuid.writable = False
            self.dbTabla.fechatimbrado.writable = False
            self.dbTabla.rol.writable = False
            self.dbTabla.etapa.writable = False
            self.dbTabla.etapa_cancelado.writable = False
            self.dbTabla.estado.writable = False
            self.dbTabla.generado_por.writable = False
            self.dbTabla.errores.writable = False
            self.dbTabla.sat_status.writable = False
            self.dbTabla.sat_fecha_cancelacion.writable = False
            self.dbTabla.sat_status_cancelacion.writable = False
            self.dbTabla.sat_status_proceso_cancelacion.writable = False
            self.dbTabla.pac_rfc.writable = False
            self.dbTabla.pac_razon_social.writable = False
            self.dbTabla.pac_nombre_comercial.writable = False
            self.dbTabla.cancelado_folio.writable = False
            self.dbTabla.cfdi_complemento_id.writable = False
            self.dbTabla.facturacomplementos.writable = False

            self.dbTabla.emp_pla_suc_almacen_id.writable = False

            self.dbTabla2.ordencompra.writable = True
            self.dbTabla2.foliointernopedido.writable = True
            self.dbTabla2.agentevendedor.writable = False
            self.dbTabla2.montobaseprodiva0.writable = False
            self.dbTabla2.montobaseprodiva.writable = False

            # Campos que no deben ser editados, solo de forma autocalculada
            self.dbTabla.versioncfdi.writable = False
            self.dbTabla.serie.writable = False
            self.dbTabla.empresa_serie_id.writable = False
            self.dbTabla.folio.writable = False
            self.dbTabla.fecha_str.writable = False
            self.dbTabla.dia.writable = False
            self.dbTabla.hora.writable = False
            self.dbTabla.formapago.writable = False
            self.dbTabla.subtotal.writable = False
            self.dbTabla.descuento.writable = False
            self.dbTabla.moneda.writable = False
            self.dbTabla.total.writable = False
            self.dbTabla.totalpago.writable = False
            self.dbTabla.saldo.writable = False
            self.dbTabla.saldofechacalculo.writable = False
            self.dbTabla.tipodecomprobante.writable = False
            self.dbTabla.metodopago.writable = False
            self.dbTabla.lugarexpedicion.writable = False
            self.dbTabla.totalimpuestostrasladados.writable = False
            self.dbTabla.totalimpuestosretenidos.writable = False
            self.dbTabla.credito_plazo.writable = True  # Capturable por CFDI recibido
            self.dbTabla.tiporelacion.writable = False
            self.dbTabla.emisorrfc.writable = False
            self.dbTabla.emisornombrerazonsocial.writable = False
            self.dbTabla.emisorregimenfiscal.writable = False
            self.dbTabla.receptorrfc.writable = False
            self.dbTabla.receptornombrerazonsocial.writable = False
            self.dbTabla.receptorusocfdi.writable = False
            self.dbTabla.fechatimbrado_str.writable = False

            self.dbTabla2.interesmoratoriopagare.writable = False
            self.dbTabla2.emisorcalle.writable = False
            self.dbTabla2.emisornumexterior.writable = False
            self.dbTabla2.emisornuminterior.writable = False
            self.dbTabla2.emisorcolonia.writable = False
            self.dbTabla2.emisorlocalidad.writable = False
            self.dbTabla2.emisormunicipio.writable = False
            self.dbTabla2.emisorestado.writable = False
            self.dbTabla2.emisorpais.writable = False
            self.dbTabla2.emisorcodigopostal.writable = False
            self.dbTabla2.receptoremails.writable = False
            self.dbTabla2.receptortelefonos.writable = False
            self.dbTabla2.receptorcalle.writable = False
            self.dbTabla2.receptorreferencia.writable = False
            self.dbTabla2.receptornumexterior.writable = False
            self.dbTabla2.receptornuminterior.writable = False
            self.dbTabla2.receptorcolonia.writable = False
            self.dbTabla2.receptorlocalidad.writable = False
            self.dbTabla2.receptormunicipio.writable = False
            self.dbTabla2.receptorestado.writable = False
            self.dbTabla2.receptorpais.writable = False
            self.dbTabla2.receptorcodigopostal.writable = False

            # Campos que todos los movimientos pueden editar
            self.dbTabla.notas.writable = True

            # Se definen los valores por default obligados
            self.dbTabla.versioncfdi.default = '3.3'
            self.dbTabla.empresa_id.default = self._D_defaults.empresa_id
            self.dbTabla.rol.default = TCFDISRX.E_ROL.RECEPTOR
            self.dbTabla.sat_status.default = TCFDISRX.SAT_STATUS.NO_DEFINIDO

            return _D_return

        def configuracampos_edicion(
                self,
                x_cfdirx,
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param x_cfdirx: es el registro del movimiento o su id
            @type x_cfdirx:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx, self.dbTabla)
            _dbRow = _D_results.dbRow

            # Se llenan los default obligados
            for _s_nombreCampo in self._L_requeridos:
                self._D_defaults[_s_nombreCampo] = _D_results.dbRow[_s_nombreCampo]

            _D_results = self.PUEDE_EDITAR(_dbRow)

            if _D_results.E_return == CLASS_e_RETURN.OK:
                # Si puede editar regresa un ok y el movimiento esta abierto

                # Se manda vacío el argumento para no agregar defaults a los campos
                _D_results = self.configuracampos_nuevo(
                    b_llamadaDesdeEditar = True,
                    **self._D_defaults
                    )
                _D_return.combinar(_D_results)

            else:
                # De lo contrario en cualquier otro caso, solo se pueden editar las observaciones
                for _s_fieldName in self.dbTabla.fields:
                    self.dbTabla[_s_fieldName].writable = False

                # TODOMejora cuando se pueden modificar caracteristicas de la compra, como el plazo, etc.

                self.dbTabla.notas.writable = True

            return _D_return

        def al_validar(
                self,
                O_form,
                dbRow = None,
                D_camposAChecar = None
                ):
            """ Validar la información para poder grabar cfdi

            Las validaciones en esta función son genéricas, se espera que existan validaciones
             específicas por tipo de CFDI

            @param D_camposAChecar:
            @type D_camposAChecar:
            @param dbRow:
            @type dbRow:
            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            if not dbRow:
                _dbRow = self.dbTabla(O_form.record_id) if O_form.record_id else None
            else:
                _dbRow = dbRow
            if not D_camposAChecar:
                _D_camposAChecar = STV_FWK_LIB.OBTENER_CAMPOSACHECAR_PARA_VALIDAR(self.dbTabla, _dbRow)
            else:
                _D_camposAChecar = D_camposAChecar

            if not O_form.record_id:
                # Se esta creando el registro

                _D_results = self.puede_crear(**_D_camposAChecar)
                if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                    O_form.errors.id = _D_results.s_msgError
                else:
                    pass

            else:
                # Se esta modificando el registro, la llamada a configuraCampos_edicion ya definió con writable
                #  que puede modificarse y que no. Por lo que el código para verificar si puede o no editar
                #  no es requerido en este caso.

                pass

            # Validaciones

            if _D_camposAChecar.versioncfdi != "3.3":
                O_form.errors.id = "Versión del CFDI, debe ser 3.3"
            else:
                pass
            if not _D_camposAChecar.empresa_id:
                O_form.errors.id = "Empresa emisora debe ser especificada"
            else:
                pass

            # TODOMejora validar ordendecompra y agente, plazo, etc.

            return O_form

        @classmethod
        def AL_ACEPTAR(
                cls,
                O_form,
                dbRow = None,
                ):
            """ Aceptación captura de inventario inicial producto

            Se hacen todos los cálculos y aceptación de los cambios en el producto

            @param dbRow:
            @type dbRow:
            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()
            dbRow = dbRow if dbRow else cls.dbTabla(O_form.record_id or O_form.vars.id)

            if not O_form.record_id:
                # Si se inserta
                pass

            else:  # Si se edita
                pass

            _D_results = cls.MAPEAR_CAMPOS(dbRow)
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.combinar(_D_results)

            else:
                _D_results = cls.CALCULAR_CAMPOS(dbRow)
                _D_return.combinar(_D_results)

            generaMensajeFlash(cls.dbTabla, _D_return)
            return O_form

        @classmethod
        def ANTES_ELIMINAR(
                cls,
                L_errorsAction,
                dbRow
                ):
            """ Antes de eliminar un movimiento de inventario de este tipo

            @param dbRow:
            @type dbRow:
            @param L_errorsAction:
            @type L_errorsAction:
            @return: errores
            @rtype: lista de diccionarios {'msg':'...'}
            """
            _D_results = cls.PUEDE_ELIMINAR(dbRow)
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                L_errorsAction.append({'msg': _D_results.s_msgError})
            else:
                cls._dbRow_aEliminar = Storage(dbRow.as_dict())

            return L_errorsAction

        @classmethod
        def DESPUES_ELIMINAR(
                cls,
                L_errorsAction,
                ):
            """ Después de eliminar un registro

            @param L_errorsAction:
            @type L_errorsAction:
            @return: errores
            @rtype: lista de diccionarios {'msg':'...'}
            """
            _D_return = FUNC_RETURN()

            if cls._dbRow_aEliminar:
                pass
            else:
                _D_return.E_return = CLASS_e_RETURN.NOK_SECUENCIA
                _D_return.s_msgError += "Error en secuencia de llamado de funciones para borrado. "

            generaMensajeFlash(cls.dbTabla, _D_return)
            return L_errorsAction

        @classmethod
        def MAPEAR_CAMPOS(
                cls,
                x_cfdirx,
                b_actualizarRegistro = True
                ):
            """ Remapea campos solamente cuando se graba el registro

            @param b_actualizarRegistro:
            @type b_actualizarRegistro:
            @param x_cfdirx:
            @type x_cfdirx:
            @return:
            @rtype:
            """

            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _dbRows_cfdis2 = _dbRow.tcfdisrx2.select()
            if _dbRows_cfdis2:
                _dbRow_cfdi2 = _dbRows_cfdis2.first()
            else:
                _n_cfdi2_id = dbc01.tcfdisrx2.insert(cfdi_id = _dbRow.id)
                _dbRow_cfdi2 = dbc01.tcfdisrx2(_n_cfdi2_id)

            _D_actualizar = Storage(
                # TODOMejora agregar las opciones del credito si es que se definen por proveedor
                )

            _D_actualizar2 = Storage(
                )

            if b_actualizarRegistro:
                for _s_nombreCampo in _D_actualizar:
                    if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                        _dbRow.update_record(**_D_actualizar)
                        break
                    else:
                        pass

                for _s_nombreCampo2 in _D_actualizar2:
                    if _D_actualizar2[_s_nombreCampo2] != _dbRow_cfdi2[_s_nombreCampo2]:
                        _dbRow_cfdi2.update_record(**_D_actualizar2)
                        break
                    else:
                        pass
            else:
                # Si no se desea actualizar el registro, se manda el diccionario con los valores calculados
                _D_return.D_actualizar = _D_actualizar

            return _D_return

        @classmethod
        def CALCULAR_CAMPOS(
                cls,
                x_cfdirx,
                b_actualizarRegistro = True
                ):
            """ Recalcula campos que tienen impacto cuando otros registros de otras tablas cambian
            Esta operación así mismo detecta y agrega las inconsistencias

            @param b_actualizarRegistro:
            @type b_actualizarRegistro:
            @param x_cfdirx:
            @type x_cfdirx:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage(
                # errores = "",  # Se resetean los errores
                )

            if b_actualizarRegistro:
                for _s_nombreCampo in _D_actualizar:
                    if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                        _dbRow.update_record(**_D_actualizar)
                        break
                    else:
                        pass
            else:
                # Si no se desea actualizar el registro, se manda el diccionario con los valores calculados
                _D_return.D_actualizar = _D_actualizar

            return _D_return

        pass  # PROC

    class PROC_DETALLE:

        dbTabla = None  # Se escribe con la inicialización

        def __init__(self, **D_defaults):
            """ Inicializa objecto de tipo PROC para control del registro actual

            @param D_defaults:
            @type D_defaults:
            """
            self._D_defaults = Storage(D_defaults)
            self._L_requeridos = ['cfdi_id']
            return

        def puede_crear(self, x_cfdirx, D_requeridosMaestro = None, **D_defaults):
            """ Determina si puede o no crear un CFDI de traslado.

            Actualmente no hay condición requerida.

            """
            _D_return = FUNC_RETURN()

            self._D_defaults.update(D_defaults or {})

            for _s_requerido in self._L_requeridos:
                if _s_requerido not in self._D_defaults:
                    _D_return.agrega_error("Campo %s es requerido para generar CFDI" % _s_requerido)

                else:
                    pass

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx, dbc01.tcfdisrx)
            _dbRow_cfdi = _D_results.dbRow

            _D_results = TCFDISRX.PROC.PUEDE_EDITAR(_dbRow_cfdi)
            _D_return.combinar(_D_results)

            if D_requeridosMaestro:
                for _s_nombreCampo in D_requeridosMaestro:
                    _D_info = D_requeridosMaestro[_s_nombreCampo]

                    if _dbRow_cfdi[_s_nombreCampo] in _D_info.L_valores:
                        pass
                    elif (len(_D_info.L_valores) == 0) and _dbRow_cfdi[_s_nombreCampo]:
                        # Un valor de lista vacío [], busca por que tenga un valor el campo
                        pass
                    else:
                        _D_return.agrega_error(_D_info.s_msgError)

            else:
                pass

            return _D_return

        @classmethod
        def PUEDE_EDITAR(
                cls,
                x_cfdirx_detalle
                ):
            """ Define si es posible editar el CFDI de traslado.
            @param x_cfdirx_detalle:

            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx_detalle, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_results = TCFDISRX.PROC.PUEDE_EDITAR(_dbRow.cfdi_id)
            _D_return.combinar(_D_results)

            return _D_return

        @classmethod
        def PUEDE_ELIMINAR(
                cls,
                x_cfdirx_detalle
                ):
            """ Determina si puede o no eliminar un registro

            Misma condición que puede_editar, a lo mejor pudieran verse condiciones distintas

            """

            return cls.PUEDE_EDITAR(x_cfdirx_detalle)

        @classmethod
        def CONFIGURACAMPOS_DESDE_dbRow(cls, dbRow_base):
            # cls.dbTabla.cfdi_id.default = dbRow_base.cfdi_id
            # cls.dbTabla.campo1.default = dbRow_base.campo1
            # cls.dbTabla.campo2.default = dbRow_base.campo2
            return

        def configuracampos_nuevo(
                self,
                s_cfdirx_detalle_id,
                b_llamadaDesdeEditar = False,
                **D_defaults
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param b_llamadaDesdeEditar: Indica si la llamada a esta función es desde otra capa,
             desactivando validación de puede_crear
            @type b_llamadaDesdeEditar:
            @param s_cfdirx_detalle_id:
            @type s_cfdirx_detalle_id:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            self._D_defaults.update(D_defaults or {})
            for _s_fieldName in self.dbTabla.fields:
                self.dbTabla[_s_fieldName].writable = False
            for _s_fieldName in self._D_defaults:
                self.dbTabla[_s_fieldName].default = self._D_defaults[_s_fieldName]

            if not b_llamadaDesdeEditar:
                _D_results = self.puede_crear(x_cfdirx = self._D_defaults.cfdi_id)
            else:
                _D_results = _D_return

            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.combinar(_D_results)

            elif s_cfdirx_detalle_id:
                _dbRow_base = self.dbTabla(s_cfdirx_detalle_id)

                self.CONFIGURACAMPOS_DESDE_dbRow(_dbRow_base)

            else:
                # Si se requieren defaults en caso de no tener algún otro definido, ponerlo aqui
                pass

            self.dbTabla.cfdi_id.writable = False

            # Campos que todos los movimientos pueden editar
            self.dbTabla.notas.writable = False

            # Se definen los valores por default obligados
            self.dbTabla.cfdi_id.default = self._D_defaults.cfdi_id

            return _D_return

        def configuracampos_edicion(
                self,
                x_cfdirx_detalle,
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param x_cfdirx_detalle:
            @type x_cfdirx_detalle:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx_detalle, self.dbTabla)
            _dbRow = _D_results.dbRow

            # Se llenan los default obligados
            for _s_nombreCampo in self._L_requeridos:
                self._D_defaults[_s_nombreCampo] = _D_results.dbRow[_s_nombreCampo]

            _D_results = self.PUEDE_EDITAR(_dbRow)

            if _D_results.E_return == CLASS_e_RETURN.OK:
                # Si puede editar regresa un ok y el movimiento esta abierto

                # Se manda vacío el argumento para no agregar defaults a los campos
                _D_results = self.configuracampos_nuevo(
                    None,
                    b_llamadaDesdeEditar = True,
                    **self._D_defaults
                    )
                _D_return.combinar(_D_results)

            else:
                # De lo contrario en cualquier otro caso, no puede editar nada de esta tabla
                for _s_fieldName in self.dbTabla.fields:
                    self.dbTabla[_s_fieldName].writable = False
                _D_return.agrega(CLASS_e_RETURN.OK_WARNING, _D_results.s_msgError, "No puede editar")

            return _D_return

        def al_validar(
                self,
                O_form,
                dbRow = None,
                D_camposAChecar = None,
                ):
            """ Validar la información para poder grabar cfdi

            Las validaciones en esta función son genéricas, se espera que existan validaciones
             específicas por tipo de CFDI

            @param D_camposAChecar:
            @type D_camposAChecar:
            @param dbRow:
            @type dbRow:
            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            dbRow = dbRow if dbRow else self.dbTabla(O_form.record_id) if O_form.record_id else None
            D_camposAChecar = D_camposAChecar if D_camposAChecar \
                else STV_FWK_LIB.OBTENER_CAMPOSACHECAR_PARA_VALIDAR(self.dbTabla, dbRow)

            if not O_form.record_id:
                # Se esta creando el registro

                _D_results = self.puede_crear(
                    x_cfdi = D_camposAChecar.cfdi_id,
                    **D_camposAChecar
                    )
                if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                    O_form.errors.id = _D_results.s_msgError
                else:
                    pass

            else:
                # Se esta modificando el registro, la llamada a configuraCampos_edicion ya definió con writable
                #  que puede modificarse y que no. Por lo que el código para verificar si puede o no editar
                #  no es requerido en este caso
                pass

            # Validaciones

            # if not _D_camposAChecar.campo1:
            #     O_form.errors.campo1 = "Campo1 no puede estar vacío"
            # else:
            #     pass

            return O_form

        @classmethod
        def AL_ACEPTAR(
                cls,
                O_form,
                dbRow = None,
                ):
            """ Al aceptar el registro deben hacerse los cálculos

            Se hacen todos los cálculos y aceptación de los cambios en el producto

            @param dbRow:
            @type dbRow:
            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()
            dbRow = dbRow if dbRow else cls.dbTabla(O_form.record_id or O_form.vars.id)

            if not O_form.record_id:
                # Si se inserta
                pass

            else:  # Si se edita
                pass

            _D_results = cls.MAPEAR_CAMPOS(dbRow)
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.combinar(_D_results)

            else:
                _D_results = cls.CALCULAR_CAMPOS(dbRow)
                _D_return.combinar(_D_results)

            generaMensajeFlash(cls.dbTabla, _D_return)
            return O_form

        @classmethod
        def ANTES_ELIMINAR(
                cls,
                L_errorsAction,
                dbRow,
                ):
            """ Antes de eliminar un movimiento de inventario de este tipo

            @param dbRow:
            @type dbRow:
            @param L_errorsAction:
            @type L_errorsAction:
            @return: errores
            @rtype: lista de diccionarios {'msg':'...'}
            """
            _D_results = cls.PUEDE_ELIMINAR(dbRow)
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                L_errorsAction.append({'msg': _D_results.s_msgError})
            else:
                cls._dbRow_aEliminar = Storage(dbRow.as_dict())

            return L_errorsAction

        @classmethod
        def DESPUES_ELIMINAR(
                cls,
                L_errorsAction,
                ):
            """ Después de eliminar un registro

            @param L_errorsAction:
            @type L_errorsAction:
            @return: errores
            @rtype: lista de diccionarios {'msg':'...'}
            """
            _D_return = FUNC_RETURN()

            if cls._dbRow_aEliminar:
                _D_results = TCFDISRX.PROC.CALCULAR_CAMPOS(
                    x_cfdirx = cls._dbRow_aEliminar.cfdi_id
                    )
                _D_return.combinar(_D_results)
            else:
                _D_return.E_return = CLASS_e_RETURN.NOK_SECUENCIA
                _D_return.s_msgError += "Error en secuencia de llamado de funciones para borrado. "

            generaMensajeFlash(cls.dbTabla, _D_return)
            return L_errorsAction

        @classmethod
        def MAPEAR_CAMPOS(
                cls,
                x_cfdirx_detalle,
                b_actualizarRegistro = True
                ):
            """ Remapea campos solamente cuando se graba el registro

            @param b_actualizarRegistro:
            @type b_actualizarRegistro:
            @param x_cfdirx_detalle:
            @type x_cfdirx_detalle:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx_detalle, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage()

            if b_actualizarRegistro:
                for _s_nombreCampo in _D_actualizar:
                    if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                        _dbRow.update_record(**_D_actualizar)
                        # Mapear campos no implica recalculo de maestro
                        break
                    else:
                        pass
            else:
                # Si no se desea actualizar el registro, se manda el diccionario con los valores calculados
                _D_return.D_actualizar = _D_actualizar

            return _D_return

        @classmethod
        def CALCULAR_CAMPOS(
                cls,
                x_cfdirx_detalle,
                b_actualizarRegistro = True,
                b_recalcularCFDI = True
                ):
            """ Recalcula campos, haciendo la consulta de la carta porte a partir del CFDI

            @param b_recalcularCFDI:
            @type b_recalcularCFDI:
            @param b_actualizarRegistro:
            @type b_actualizarRegistro:
            @param x_cfdirx_detalle:
            @type x_cfdirx_detalle:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx_detalle, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage()

            if b_actualizarRegistro:
                for _s_nombreCampo in _D_actualizar:
                    if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                        _dbRow.update_record(**_D_actualizar)

                        break
                    else:
                        pass
            else:
                # Si no se desea actualizar el registro, se manda el diccionario con los valores calculados
                _D_return.D_actualizar = _D_actualizar

            if b_recalcularCFDI:
                # Si hay actualizaciones, debe actualizarse el CFDI, no los conceptos
                _D_results = TCFDISRX.PROC.CALCULAR_CAMPOS(x_cfdirx = _dbRow.cfdi_id)
            else:
                pass

            return _D_return

        pass  # PROC_DETALLE

    @classmethod
    def asociar_uuid(cls, s_cfdi_id):
        """ Se busca y asocian ligas rotas en doctos relacionados y pagos/doctos relacionados

        """

        _D_return = Storage(
            L_msgError = [],
            E_return = CLASS_e_RETURN.NOK_ERROR,
            n_updatedRows = 0
            )

        # Ejemplo puede usar
        # tcfdirx_relacionados.id = 8419 de tcfdisrx.id = 47696
        # tcfdisrx.id = 47762

        # Se buscan primero en la tabla de tcfdirx_relacionados

        _dbTableCFDIsBuscar = dbc01.tcfdisrx.with_alias('cfdisbuscar')
        _dbRows_cfdiRelacionados = dbc01(
            (dbc01.tcfdisrx.id == s_cfdi_id)
            & (dbc01.tcfdirx_relacionados.uuid == dbc01.tcfdisrx.uuid)
            & (dbc01.tcfdirx_relacionados.cfdirelacionado_id == None)
            & (_dbTableCFDIsBuscar.id == dbc01.tcfdirx_relacionados.cfdi_id)
            & (_dbTableCFDIsBuscar.empresa_id == dbc01.tcfdisrx.empresa_id)
            ).select(
            dbc01.tcfdirx_relacionados.ALL,
            _dbTableCFDIsBuscar.id,
            )

        if len(_dbRows_cfdiRelacionados) == 0:
            # CFDI asociado con el UUID no se encontró
            _D_return.L_msgError += [
                "No se encontrarón CFDIs con problemas en cfdi relacionados a este CFDI %s" % s_cfdi_id
                ]
        else:
            # Si tiene solamente un registro, hay que actualizarlo
            for _dbRow in _dbRows_cfdiRelacionados:
                _D_return.n_updatedRows += dbc01(
                    dbc01.tcfdirx_relacionados.id == _dbRow.tcfdirx_relacionados.id
                    ).update(
                    cfdirelacionado_id = s_cfdi_id
                    )

        _dbRows_cfdiPagosRelacionados = dbc01(
            (dbc01.tcfdisrx.id == s_cfdi_id)
            & (dbc01.tcfdirx_complementopago_doctosrelacionados.iddocumento == dbc01.tcfdisrx.uuid)
            & (dbc01.tcfdirx_complementopago_doctosrelacionados.cfdirelacionado_id == None)
            & (
                dbc01.tcfdirx_complementopagos.id
                == dbc01.tcfdirx_complementopago_doctosrelacionados.cfdi_complementopago_id
                )
            & (_dbTableCFDIsBuscar.id == dbc01.tcfdirx_complementopagos.cfdi_id)
            & (_dbTableCFDIsBuscar.empresa_id == dbc01.tcfdisrx.empresa_id)
            ).select(
                dbc01.tcfdirx_complementopago_doctosrelacionados.ALL,
                _dbTableCFDIsBuscar.id,
                )

        if len(_dbRows_cfdiPagosRelacionados) == 0:
            # CFDI asociado con el UUID no se encontró
            _D_return.L_msgError += [
                "No se encontrarón CFDIs de PAGO con problemas en cfdi relacionados a este CFDI %s" % s_cfdi_id]
        else:
            # Si tiene solamente un registro, hay que actualizarlo
            for _dbRow in _dbRows_cfdiPagosRelacionados:
                _D_return.n_updatedRows += dbc01(
                    dbc01.tcfdirx_complementopago_doctosrelacionados.id
                    == _dbRow.tcfdirx_complementopago_doctosrelacionados.id
                    ).update(cfdirelacionado_id = s_cfdi_id)

        _D_return.E_return = CLASS_e_RETURN.OK

        return _D_return

    # TODO trabajar Busqueda Avanzada
    class BUSQUEDA_AVANZADA:

        @staticmethod
        def CONFIGURA(
                O_cat,
                s_empresa_id = None,
                **D_valoresDefault
                ):
            """ Agrega el componente y lo configura para la búsqueda avanzada

            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _dbTabla = dbc01.tcfdisrx

            D_valoresDefault = Storage(D_valoresDefault)

            # Se configura el uso del campo categorias para poder filtrar por varias clasificaciones.
            _dbField_fecha = _dbTabla.fecha.clone()
            _dbField_fecha.default = None
            _dbField_fecha.widget = stv_widget_inputDateTimeRange

            _dbField_tipocomprobante_id = _dbTabla.tipocomprobante_id.clone()
            _dbField_tipocomprobante_id.adv_default = (
                    D_valoresDefault.tipocomprobante_id or _dbField_tipocomprobante_id.default
                )
            _dbField_tipocomprobante_id.widget = lambda field, value: stv_widget_db_chosen(
                field, value, 20,
                )
            _dbField_tipocomprobante_id.length = 400

            # Variable que contendrá los campos a editar en la búsqueda avanzada.
            _L_dbFields = [
                _dbField_fecha,
                _dbField_tipocomprobante_id,
                _dbTabla.empresa_serie_id.clone(),  # TODO si solo quiere ingresos, filtrar serie a solo ingresos
                _dbTabla.receptorrfc.clone(),
                _dbTabla.receptornombrerazonsocial.clone(),
                _dbTabla.folio.clone(),
                ]

            if not s_empresa_id:
                # Si no esta definida la empresa, se agrega el seleccionar empresa.
                _dbField_empresa_id = _dbTabla.empresa_id.clone()
                _dbField_empresa_id.notnull = False
                _L_dbFields.append(_dbField_empresa_id)
            else:
                pass

            # Se configura la edición de los campos.
            for _dbField in _L_dbFields:
                _dbField.writable = True
                _dbField.readable = True
                if _dbField.name in request.vars:
                    pass
                elif getattr(_dbField, 'adv_default', None) is not None:
                    request.vars[_dbField.name] = _dbField.adv_default
                else:
                    pass

            O_cat.cfgRow_advSearch(
                L_dbFields_formFactory_advSearch = _L_dbFields,
                dbRow_formFactory_advSearch = None,
                D_hiddenFields_formFactory_advSearch = {},
                s_style = 'bootstrap',
                L_buttons = []
                )

            return _D_return

        @staticmethod
        def GENERA_PREFILTRO(
                D_args,
                ):
            """ Función usada para generar un prefiltro en caso de ser necesario, ya que los campos
            no estan directamente relacionado a la tabla

            @return:
            @rtype:
            """
            _ = D_args
            _D_return = FUNC_RETURN(
                qry = True
                )
            _dbTabla = dbc01.tcfdisrx

            return _D_return

        pass  # BUSQUEDA_AVANZADA

    # TODO trabajar controlador
    @classmethod
    def CONTROLADOR(
            cls,
            s_action,
            s_cfdi,
            s_empresa_id = None,
            ):
        """ Se maneja el controlador del CFDI a este nivel, para poder ser reusado donde sea

        Args si args[0] = empresa_id:
            arg0: String empresa_id
            arg1: id de la empresa
            [arg2]: operación a realizar en el formulario
            [arg3]: id en caso de existir relación a un registro en la operación
        else:
            [arg0]: operación a realizar en el formulario
            [arg1]: id en caso de existir relación a un registro en la operación
        """
        _dbTable = dbc01.tcfdisrx

        from gluon.storage import List

        request.args = List()
        request.args.append(s_action)  # Se mete _s_action dentro de la lista con la función append.
        request.args.append(s_cfdi)

        if s_empresa_id:
            _s_empresa_id = s_empresa_id
            _s_action = s_action
            _s_cfdi_id = s_cfdi

            _dbQrySearch = (_dbTable.empresa_id == _s_empresa_id)

        else:
            _s_action = s_action
            _s_cfdi_id = s_cfdi
            _s_empresa_id = None

            _dbQrySearch = True

        _D_overrideView = None
        if _s_cfdi_id and (_s_action in (request.stv_fwk_permissions.special_permisson_01.code,)):
            # En caso de acción de permiso especial 01, se búsca asociar el CFDI.

            _D_result = TCFDISRX.asociar_uuid(_s_cfdi_id)

            if (_D_result.E_return == CLASS_e_RETURN.OK) \
                    and _D_result.L_msgError:
                for _s_error in _D_result.L_msgError:
                    stv_flashSuccess("Asociación por UUID", _s_error)

            else:
                for _s_error in _D_result.L_msgError:
                    stv_flashError("Asociación por UUID", _s_error)

            if _D_result.n_updatedRows:
                stv_flashSuccess("Asociación por UUID", "Se asociaron correctamente %d cfdis" % _D_result.n_updatedRows)
            else:
                pass

            _D_overrideView = request.stv_fwk_permissions.view_refresh

        elif (
                _s_cfdi_id
                and _s_action in (request.stv_fwk_permissions.btn_edit.code, request.stv_fwk_permissions.btn_save.code)
                ):

            _dbRow_cfdi = _dbTable(_s_cfdi_id)
            if _dbRow_cfdi.uuid and (_dbRow_cfdi.sat_status == TCFDISRX.SAT_STATUS.CANCELADO):
                _dbTable.cancelado_folio.writable = True
            else:
                _dbTable.cancelado_folio.writable = False

        else:
            # No hacer nada y continuar con el procesio normal.
            pass

        _O_cat = STV_FWK_FORM(
            dbReference = dbc01,
            dbReferenceSearch = dbc01(_dbQrySearch),
            dbTable = _dbTable,
            dbTableMaster = None,
            xFieldLinkMaster = [],
            L_visibleButtons = request.stv_fwk_permissions.L_formButtonsMinimal,
            s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
            L_fieldsToShow = [
                _dbTable.id,
                _dbTable.tipocomprobante_id,
                _dbTable.serie,
                _dbTable.folio,
                _dbTable.fecha,
                _dbTable.fechatimbrado,
                _dbTable.metodopago_id,
                _dbTable.formapago_id,
                _dbTable.total,
                _dbTable.monedacontpaqi_id,
                _dbTable.receptorrfc,
                _dbTable.receptornombrerazonsocial,
                _dbTable.emisorrfc,
                _dbTable.emisornombrerazonsocial,
                _dbTable.sat_status
                ],
            L_fieldsToSearch = [
                _dbTable.fecha,
                _dbTable.fechatimbrado,
                _dbTable.receptorrfc,
                _dbTable.uuid,
                ],
            L_fieldsToSearchIfDigit = [
                _dbTable.id,
                _dbTable.folio,
                dbc01.tcfdirx_relacionados.cfdirelacionado_id,
                dbc01.tcfdirx_complementopago_doctosrelacionados.cfdirelacionado_id,
                ],
            )

        _O_cat.addRowSearchResultsLeftJoin(
            dbc01.tcfdirx_relacionados.on(
                dbc01.tcfdirx_relacionados.cfdi_id == _dbTable.id
                )
            )

        _O_cat.addRowSearchResultsLeftJoin(
            dbc01.tcfdirx_complementopagos.on(
                dbc01.tcfdirx_complementopagos.cfdi_id == _dbTable.id
                )
            )

        _O_cat.addRowSearchResultsLeftJoin(
            dbc01.tcfdirx_complementopago_doctosrelacionados.on(
                dbc01.tcfdirx_complementopago_doctosrelacionados.cfdi_complementopago_id
                == dbc01.tcfdirx_complementopagos.id
                )
            )

        # Se agrega el boton especial 01 que funciona para re-asignar serie, caja chica y prepoliza
        #  en caso de ser necesario.
        _O_cat.addPermissionConfig(
            s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
            s_permissionLabel = "Asociación de CFDI",
            s_permissionIcon = "fa fa-compass",
            s_type = "button",
            s_send = "find",
            L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
            D_actions = Storage(
                js = Storage(
                    sfn_onPress = 'stv_fwk_button_press',
                    sfn_onSuccess = 'stv_fwk_button_success',
                    sfn_onError = 'stv_fwk_button_error',
                    ),
                swap = {},
                ),
            L_options = []
            )

        # Variable que contendrá los campos a editar en la búsqueda avanzada.
        _L_advSearch = []

        # Se configura el uso del campo día para poder buscar por rango de fechas.
        _dbField_fecha = _dbTable.fecha.clone()
        _dbField_fecha.widget = stv_widget_inputDateTimeRange
        _L_advSearch.append(_dbField_fecha)

        if not _s_empresa_id:
            # Si no esta definida la empresa, se agrega el seleccionar empresa.
            _dbField_empresa_id = _dbTable.empresa_id.clone()
            _dbField_empresa_id.notnull = False
            _L_advSearch.append(_dbField_empresa_id)

        _dbTable.tipocomprobante_id.requires = IS_IN_DB(
            db01(
                db01.ttiposcomprobantes.id.belongs(
                    TTIPOSCOMPROBANTES.EGRESO,
                    TTIPOSCOMPROBANTES.INGRESO,
                    TTIPOSCOMPROBANTES.PAGO,
                    TTIPOSCOMPROBANTES.TRASLADO,
                    )
                ),
            'ttiposcomprobantes.id', db01.ttiposcomprobantes._format
            )

        # Se agregan campos sin modificaciones.
        _L_advSearch += [
            _dbTable.serie.clone(),
            _dbTable.receptorrfc.clone(),
            _dbTable.receptornombrerazonsocial.clone(),
            _dbTable.folio.clone(),
            _dbTable.tipocomprobante_id.clone(),
            ]

        # Se configura la edición de los campos.
        for _dbField in _L_advSearch:
            _dbField.writable = True

        _dbRowsPrepolizasCanceladas = dbc01(
            (dbc01.tcfdirx_prepolizas.cfdi_id == _s_cfdi_id)
            & (dbc01.tcfdirx_prepolizas.sat_status_cancelacion == TCFDIRX_PREPOLIZAS.SAT_STATUS.CANCELADO)
            ).select(
            dbc01.tcfdirx_prepolizas.id
            )

        _O_cat.cfgRow_advSearch(
            L_dbFields_formFactory_advSearch = _L_advSearch,
            dbRow_formFactory_advSearch = None,
            D_hiddenFields_formFactory_advSearch = {},
            s_style = 'bootstrap',
            L_buttons = []
            )

        _D_returnVars = _O_cat.process(
            D_overrideView = _D_overrideView
            )

        _O_cat.addSubTab(
            dbTableDetail = dbc01.tcfdirx_conceptos,
            s_url = URL(
                a = 'app_timbrado',
                c = '250cfdirx',
                f = 'cfdi_conceptos',
                args = ['master_' + str(request.function), _s_cfdi_id]
                ),
            s_idHtml = fn_createIDTabUnique(
                a = 'app_timbrado',
                c = '250cfdirx',
                f = 'cfdi_conceptos',
                s_masterIdentificator = str(request.function)
                )
            )

        _O_cat.addSubTab(
            dbTableDetail = dbc01.tcfdirx_relacionados,
            s_url = URL(
                a = 'app_timbrado',
                c = '250cfdirx',
                f = 'cfdi_relacionados',
                args = ['master_' + str(request.function), _s_cfdi_id]
                ),
            s_idHtml = fn_createIDTabUnique(
                a = 'app_timbrado',
                c = '250cfdirx',
                f = 'cfdi_relacionados',
                s_masterIdentificator = str(request.function)
                )
            )

        _O_cat.addSubTab(
            dbTableDetail = dbc01.tcfdirx_complementopagos,
            s_url = URL(
                a = 'app_timbrado',
                c = '250cfdirx',
                f = 'cfdi_pagos',
                args = ['master_' + str(request.function), _s_cfdi_id]
                ),
            s_idHtml = fn_createIDTabUnique(
                a = 'app_timbrado',
                c = '250cfdirx',
                f = 'cfdi_pagos',
                s_masterIdentificator = str(request.function)
                )
            )

        _D_returnVars['htmlid_emisor'] = _O_cat.addSubTab(
            dbTableDetail = None,
            s_url = False,
            s_tabName = "Emisor",
            s_idHtml = None,
            s_type = "extended"
            )

        _D_returnVars['htmlid_receptor'] = _O_cat.addSubTab(
            dbTableDetail = None,
            s_url = False,
            s_tabName = "Receptor",
            s_idHtml = None,
            s_type = "extended"
            )

        _D_returnVars['htmlid_vendedores'] = _O_cat.addSubTab(
            dbTableDetail = None,
            s_url = False,
            s_tabName = "Vendedores",
            s_idHtml = None,
            s_type = "extended"
            )

        # _dbRow_xml = dbc02(dbc02.tcfdirx_xmls.cfdi_id == _s_cfdi_id).select(dbc02.tcfdirx_xmls.xml_cfdi).first()
        #
        # _D_returnVars.dbRow_xml = _dbRow_xml
        #
        # _D_returnVars['htmlid_xml'] = _O_cat.addSubTab(
        #     dbTableDetail = None,
        #     s_url = False,
        #     s_tabName = "XML",
        #     s_idHtml = None,
        #     s_type = "extended"
        #     )

        _O_cat.addSubTab(
            dbTableDetail = dbc02.tcfdirx_xmls,
            s_url = URL(
                a = 'app_timbrado',
                c = '250cfdirx',
                f = 'cfdi_xml',
                args = ['master_' + str(request.function), _s_cfdi_id]
                ),
            s_idHtml = fn_createIDTabUnique(
                a = 'app_timbrado',
                c = '250cfdirx',
                f = 'cfdi_xml',
                s_masterIdentificator = str(request.function)
                )
            )

        # _O_cat.addSubTab(
        #     dbTableDetail = dbc01.tcfdirx_prepolizas,
        #     s_url = URL(
        #         a = 'app_timbrado',
        #         c = '255prepolizas_polizas',
        #         f = 'prepoliza',
        #         args = ['master_' + str(request.function), _s_cfdi_id]
        #         ),
        #     s_idHtml = fn_createIDTabUnique(
        #         a = 'app_timbrado',
        #         c = '255prepolizas_polizas',
        #         f = 'prepoliza',
        #         s_masterIdentificator = str(request.function)
        #         )
        #     )

        # if _dbRowsPrepolizasCanceladas:
        #     _O_cat.addSubTab(
        #         s_tabName = "Prepoliza cancelada",
        #         s_url = URL(
        #             c = '255prepolizas_polizas', f = 'prepoliza_canceladas',
        #             args = ['master_' + str(request.function), _s_cfdi_id]
        #             ),
        #         s_idHtml = fn_createIDTabUnique(
        #             c = '255prepolizas_polizas', f = 'prepoliza_canceladas',
        #             s_masterIdentificator = str(request.function)
        #             )
        #         )
        # else:
        #     pass

        if _D_returnVars.dbRecord:

            _O_cat.addSubTab(
                dbTableDetail = dbc01.tcfdirx_movimientos,
                s_url = URL(
                    a = 'app_timbrado',
                    c = '132proveedores',
                    f = 'cfdis_movimientos',
                    args = ['empresa_id', _D_returnVars.dbRecord.empresa_id, 'empresa_proveedor_id',
                        _D_returnVars.dbRecord.emisorproveedor_id, 'master_' + str(request.function), s_cfdi]
                    ),
                s_idHtml = fn_createIDTabUnique(
                    a = 'app_timbrado',
                    c = '132proveedores',
                    f = 'cfdis_movimientos',
                    s_masterIdentificator = str(request.function)
                    )
                )

        else:
            pass

        return _D_returnVars

    @classmethod
    def CONFIGURAR_INTEGRIDAD_REFERENCIAL(cls):

        return

    @staticmethod
    def VERIFICAR_CONSISTENACIA_DATOS():
        """ Función que debe ejecutarse cada noche o cada determinado tiempo y que este verificando
        consistencia en los ultimos datos modificados

        @return:
        @rtype:
        """
        _D_return = RET_FUNC()

        # TODOMEJORA verificar que todos los CFDIs de RX tengan el rol de Receptor y los normales de Emisor
        # TODOMEJORA crear chequeo de calidad que verifique esta cantidad con la definida en tempresa_invmovto_productos

        return _D_return

    class TIMBRADO:

        @classmethod
        def PUEDE_TIMBRAR(cls, x_cfdirx):
            """ Identifica las características genéricas para evaluar si es posible timbrar o no

            @param x_cfdirx:
            @type x_cfdirx:
            @return:
            @rtype:
            """
            _ = x_cfdirx
            _D_return = FUNC_RETURN()

            _D_return.agrega_error("CFDIs recibidos no pueden ser timbrados.")

            return _D_return

        @classmethod
        def PREPARAR_TIMBRADO(cls, x_cfdirx):
            """ Prepara la información en el CFDI para timbrar, principalmente se borra el detalle de impuestos
            si la tasa es cero.

            @param x_cfdirx:
            @type x_cfdirx:
            @return:
            @rtype:
            """
            _ = x_cfdirx
            _D_return = FUNC_RETURN()

            _D_return.agrega_error("CFDIs recibidos no pueden ser timbrados.")

            return _D_return

        @classmethod
        def TIMBRAR(cls, x_cfdirx):

            _ = x_cfdirx
            _D_return = FUNC_RETURN()

            _D_return.agrega_error("CFDIs recibidos no pueden ser timbrados.")

            return _D_return

        @classmethod
        def PUEDE_CANCELAR(cls, x_cfdirx):
            """ Verifica las condiciones necesaria que permitan poder cancelar un CFDI

            @param x_cfdirx: CFDI a cancelar
            @type x_cfdirx: dbRow o id
            @return: diccionario con E_return y s_msgError
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx, dbc01.tcfdisrx)
            _dbRow = _D_results.dbRow

            if not _dbRow.uuid:
                _D_return.agrega_error("CFDI no tiene uuid asociado")

            elif _dbRow.estado not in TCFDISRX.E_ESTADO.L_SINPROCESOPENDIENTE:
                _D_return.agrega_error(
                    ("CFDI se encuentra en proceso pendiente (%s), debe terminar el proceso anterior "
                    + "para poder cancelarlo")
                    % TCFDISRX.E_ESTADO.D_TODOS[_dbRow.estado]
                    )

            else:

                if _dbRow.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO:
                    # Buscar pagos relacionados no cancelados
                    _dbRows_pagos = dbc01(
                        (dbc01.tcfdirx_complementopago_doctosrelacionados.cfdirelacionado_id == _dbRow.id)
                        & (dbc01.tcfdirx_complementopagos.id
                           == dbc01.tcfdirx_complementopago_doctosrelacionados.cfdi_complementopago_id)
                        & (dbc01.tcfdirxsrx.id == dbc01.tcfdirx_complementopagos.cfdi_id)
                        & (dbc01.tcfdisrx.sat_status == TCFDISRX.SAT_STATUS.VIGENTE)
                        ).select(
                        dbc01.tcfdisrx.id,
                        dbc01.tcfdisrx.serie,
                        dbc01.tcfdisrx.folio,
                        )

                    if _dbRows_pagos:
                        _D_return.agrega_error("CFDI tiene %d pagos que no han sido cancelados: " % len(_dbRows_pagos))
                        for _dbRow_pago in _dbRows_pagos:
                            _D_return.agrega_error("%d:%s %s; "
                                                   % (_dbRow_pago.id, _dbRow_pago.serie, _dbRow_pago.folio))
                        _D_return.agrega_error("\n")
                    else:
                        pass

                    # Buscar egresos relacionados no cancelados
                    _dbRows_egresos = dbc01(
                        (dbc01.tcfdirx_relacionados.cfdirelacionado_id == _dbRow.id)
                        & (dbc01.tcfdisrx.id == dbc01.tcfdirx_relacionados.cfdi_id)
                        & (dbc01.tcfdisrx.sat_status == TCFDISRX.SAT_STATUS.VIGENTE)
                        ).select(
                        dbc01.tcfdisrx.id,
                        dbc01.tcfdisrx.serie,
                        dbc01.tcfdisrx.folio,
                        )

                    if _dbRows_egresos:
                        _D_return.agrega_error("CFDI tiene %d egresos que no han sido cancelados: "
                                               % len(_dbRows_egresos))
                        for _dbRow_egreso in _dbRows_egresos:
                            _D_return.agrega_error("%d:%s %s; "
                                                    % (_dbRow_egreso.id, _dbRow_egreso.serie, _dbRow_egreso.folio))
                        _D_return.agrega_error("\n")
                    else:
                        pass

                    # Buscar ajustes saldo del CFDI
                    _dbRows_ajustesSaldo = dbc01(
                        (dbc01.tcfdirx_manejosaldos.cfdi_id == _dbRow.id)
                        ).select(
                        dbc01.tcfdisrx.id,
                        dbc01.tcfdisrx.serie,
                        dbc01.tcfdisrx.folio,
                        )

                    if dbc01(dbc01.tcfdirx_manejosaldos.cfdi_id == _dbRow.id).count() > 0:
                        _D_return.agrega_error("CFDI no puede cancelarse porque tiene registros en ajustes de saldo\n")
                    else:
                        pass

                    # Buscar si otro CFDI ajusto saldo a este CFDI
                    _dbRows_ajustesSaldo = dbc01(
                        (dbc01.tcfdirx_manejosaldos.cfdirelacionado_id == _dbRow.id)
                        ).select(
                        dbc01.tcfdisrx.id,
                        dbc01.tcfdisrx.serie,
                        dbc01.tcfdisrx.folio,
                        )

                    if dbc01(dbc01.tcfdirx_manejosaldos.cfdi_id == _dbRow.id).count() > 0:
                        _D_return.agrega_error("CFDI no puede cancelarse porque tiene referencias de ajustes "
                                               + "de saldo\n")
                    else:
                        pass

                else:
                    # Otros comprobantes no requieren validación
                    pass

            return _D_return

        @classmethod
        def CANCELAR(cls, x_cfdirx):

            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx, dbc01.tcfdisrx)
            _dbRow = _D_results.dbRow

            # Calcular CFDI
            _D_results = cls.PUEDE_CANCELAR(x_cfdirx = _dbRow)

            if _D_results.E_return > stvfwk2_e_RETURN.OK_WARNING:
                _D_return.combinar(_D_results)

            else:

                _dbRow.estado = TCFDISRX.E_ESTADO.EN_PROCESO_CANCELACION
                _dbRow.update_record()

                _D_results = TSERVIDOR_INSTRUCCIONES.AGREGAR_TIMBRAR_CANCELAR_CFDI(
                    s_cuenta_id = D_stvFwkCfg.dbRow_cuenta.id,
                    dbRow_cfdi = _dbRow,
                    E_tipoInstruccion = TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.CANCELAR_CFDI,
                    s_rastro = "Firma manual"
                    )

                if _D_results.E_return > stvfwk2_e_RETURN.OK_WARNING:
                    _dbRow.estado = E_ESTADO.NO_DEFINIDO
                    _dbRow.errores = _D_results.s_msgError
                    _dbRow.update_record()
                else:
                    pass
                _D_return.combinar(_D_results)

            return _D_return

        pass  # PROC

    class EDOCTA:

        class E_PROCESO:
            NINGUNO = 0
            PROVEEDOR = 1
            CFDI = 2

        def __init__(self, n_empresa_id, n_proveedor_id, E_movto_generado_por = None):
            self._n_return = stvfwk2_e_RETURN.OK
            self._n_empresa_id = int(n_empresa_id)
            self._n_proveedor_id = int(n_proveedor_id)

            self._D_multireturn = FUNC_MULTIRETURN()

            # Contendrá los registros de los movimientos con el ID del ingreso relacionado como indice
            self._D_registros = Storage()

            self._dbRow_proveedor = None
            self._D_saldos = Storage(
                f_saldo = 0,
                f_saldoVencido = 0,
                )
            self._dt_empezarde = TCFDISRX.GET_PRIMERDIA2020().dt_utc
            self._dt_procesoInicio = datetime.datetime.now()
            self._dt_procesoFin = datetime.datetime.now()

            self._E_proceso = self.E_PROCESO.NINGUNO
            self._E_movto_generado_por = E_movto_generado_por or TCFDIRX_MOVIMIENTOS.E_GENERADO_POR.NO_DEFINIDO

            self._D_debug = FUNC_DEBUG()
            self._D_debug.entra_proceso('Inicialización')
            return

        def _addMovimiento(
                self,
                x_cfdirx_ingreso,
                dbRow_cfdi,

                dt_fecha,
                E_accion,
                E_tipodocumento,
                s_descripcion,
                n_cfdi_complementopago_doctorelacionado_id = None,  # Si viene de un pago
                n_cfdi_relacionado_id = None,  # Si viene de un egreso
                n_cfdi_movimiento_emision_id = None,  # Si es una cancelación
                n_cfdi_manejosaldo_id = None,  # Si viene de un manejo de saldo

                dbRow_relacionado = None,  # Corresponde al dbRow del relacionado en egreso o pago

                f_importe = 0,  # Importe del movimiento a contabilizar en moneda del movto
                f_cargo = 0,  # Cargo en caso de ser cargo en moneda del movto
                f_abono = 0,  # Abono en caso de ser abono en moneda del movto
                f_saldoPrevio = 0,  # Saldo anterior en caso de saberlo en moneda del ingreso

                s_monedacontpaqi_id = None,  # Moneda usada en el movimiento
                f_tipocambio = 1  # Tipo de cambio usado en el movimiento
                ):
            """ Registra un movimiento en la tabla de movimientos y agrega información a un dict del objeto.

            @param dt_fecha: fecha del movimiento principal.
            @param E_accion: TCFDIRX_MOVIMIENTOS.E_ACCION
            @param E_tipodocumento: Tipo de documento
            @type E_tipodocumento: TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO
            @param s_descripcion: descripción del movmiento.
            @param dbRow_cfdi: dbRow con la infromación del CFDI.
            @param n_cfdi_ingreso_id: id del ingreso relacionado.
            @param n_cfdi_complementopago_doctorelacionado_id: en caso de pagos, es el id del complemento
             de pago relacionado.
            @param n_cfdi_relacionado_id: en caso de egresos, es el id del documento relacionado.
            @param n_cfdi_movimiento_emision_id: en caso de egresos, es el id del detalle de referencias
             al documento referenciado.
            @param f_importe: importe del movimiento en caso de estar claramente definido, si no es posible
             determinarlo, debe ser cero.
            @param f_cargo: usado únicamente cuando se procesan CFDIs individuales.
            @param f_abono: usado únicamente cuando se procesan CFDIs individuales.
            @param f_saldo: usado únicamente cuando se procesan CFDIs individuales.
            """

            _D_return = FUNC_RETURN(
                n_cfdi_movimiento_id = n_cfdi_movimiento_emision_id,
                n_cfdi_movimientoCancelacion_id = None,
                f_saldoNuevo = f_saldoPrevio,
                f_importeNuevo = f_importe
                )

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(
                x_cfdirx_ingreso, dbc01.tcfdisrx, b_regresarRegistro = False
                )
            _n_cfdiIngreso_id = _D_results.n_id
            _dbRow_cfdiIngreso = _D_results.dbRow

            # El control del estado de cuenta se basa en la lista del ingreso, por lo que es lo primero que se busca
            if not _n_cfdiIngreso_id:
                # Es muy probable que el CFDI de ingreso se ignoro, debido a que no esta en el rango de
                #  determinación de saldo
                pass

            elif _n_cfdiIngreso_id not in self._D_registros:
                # Si no existe, se crea el registro para control del ingreso

                if not _dbRow_cfdiIngreso:
                    _dbRow_cfdiIngreso = dbc01.tcfdisrx(_n_cfdiIngreso_id)
                else:
                    pass

                self._D_registros[_n_cfdiIngreso_id] = Storage(
                    dbRow = _dbRow_cfdiIngreso,  # Es el CFDI del ingreso
                    saldo = 0,
                    cargos = 0,  # Contendrá los cargos identificados en la primer etapa
                    abonos = 0,  # Contendrá los abonos identificados en la primer etapa
                    aplicadoscargos = 0,  # Contendrá los cargos identificados en fase 1 que ya fueron aplicados
                    aplicadosabonos = 0,  # Contendrá los abonos identificados en fase 1 que ya fueron aplicados
                    # Contendrá los cargos cuyo importe no identificado en fase 1 que ya fueron aplicados
                    variablescargos = 0,
                    # Contendrá los abonos cuyo importe no identificado en fase 1 que ya fueron aplicados
                    variablesabonos = 0,
                    # Todas las referencia de CFDIs relacionados con el ingreso, el key es el cfdi_id
                    referencias = Storage(),
                    # Referencias únicamente a los CFDIs que no pudo determinarse el monto del cargo/abono
                    referenciasNoAplicadas = Storage(),
                    referenciasRelacionados = Storage(),  # En caso de egresos se agrega la lista de egresos
                    referenciasPagos = Storage(),  # En caso de pagos se agrega la lista de egresos
                    )

            else:
                # Ingreso ya fue agregado antes
                pass

            if _n_cfdiIngreso_id in self._D_registros:

                _D_registro = self._D_registros[n_cfdi_ingreso_id]

                if str(_D_registro.dbRow.monedacontpaqi_id) != str(s_monedacontpaqi_id):
                    # Si la moneda del movimiento, es diferente a la moneda del ingreso
                    _f_factor = f_tipocambio / _D_registro.dbRow.tipocambio
                    _D_return.f_importeNuevo *= _f_factor
                    f_cargo *= _f_factor
                    f_abono *= _f_factor
                else:
                    pass

                if dbRow_cfdi.sat_status != TCFDISRX.SAT_STATUS.NO_DEFINIDO:
                    # Si esta timbrado o cancelado, se actualiza el saldo
                    _D_return.f_saldoNuevo = f_saldoPrevio + f_cargo - f_abono
                else:
                    _D_return.f_saldoNuevo = f_saldoPrevio

                if E_tipodocumento in (
                        TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.INGRESO,
                        TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.DEBITO,
                        TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.TRASPASO_SALDO_A_FAVOR,
                        ):
                    _D_registro.cargos += _D_return.f_importeNuevo

                elif E_tipodocumento in (
                        TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.PAGO,
                        TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.DEVOLUCION,
                        TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.DESCUENTO,
                        TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.ANTICIPO,
                        TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.APLICACION_SALDO_A_FAVOR,
                        ):

                    _D_registro.abonos += _D_return.f_importeNuevo

                elif E_tipodocumento in (
                        TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.OTRO,
                        TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.NO_DEFINIDO,
                        ):
                    _D_registro.cargos += f_cargo
                    _D_registro.abonos += f_abono

                else:
                    self._D_multireturn.agrega_error(
                        "CFDI %d tipo de documento no identificado %s, Llamar al admministrador."
                        % dbRow_cfdi.id, TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.D_TODOS[E_tipodocumento]
                        )
                    return _D_return

                # Todas las referencias se agregan a esta lista
                _D_registro.referencias[dbRow_cfdi.id] = dbRow_cfdi

                # Si el importe es 0, no se pudo determinar el monto a aplicar, por lo que se graba la referencia
                #  para su uso en la siguiente etapa para determinar el saldo a aplicar
                if (_D_return.f_importeNuevo == 0) and (E_tipodocumento != TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.INGRESO):
                    # Pueden existir ingresos con importe zero y deben ignorarse
                    _D_registro.referenciasNoAplicadas[dbRow_cfdi.id] = dbRow_cfdi
                else:
                    pass

                # Si es egreso, se agrega a la lista de egresos relacionados al ingreso
                if n_cfdi_relacionado_id:
                    _D_registro.referenciasRelacionados[n_cfdi_relacionado_id] = dbRow_relacionado
                elif n_cfdi_complementopago_doctorelacionado_id:
                    _D_registro.referenciasPagos[dbRow_cfdi.id] = dbRow_relacionado
                else:
                    pass

                _D_insertarMovimiento = Storage(
                    empresa_id = self._n_empresa_id,
                    proveedor_id = self._n_proveedor_id,
                    fecha = dt_fecha,
                    fechaingreso = _D_registro.dbRow.fecha,
                    accion = E_accion
                    if dbRow_cfdi.sat_status != TCFDISRX.SAT_STATUS.NO_DEFINIDO
                    # Si sat status no esta definido se sobreescribe la accion a no timbrada
                    else TCFDIRX_MOVIMIENTOS.E_ACCION.NO_TIMBRADA,
                    tipodocumento = E_tipodocumento,
                    descripcion = s_descripcion,
                    cfdi_id = dbRow_cfdi.id,
                    cfdi_complementopago_doctorelacionado_id = n_cfdi_complementopago_doctorelacionado_id,
                    cfdi_relacionado_id = n_cfdi_relacionado_id,
                    cfdi_ingreso_id = n_cfdi_ingreso_id,
                    cfdi_movimiento_emision_id = _D_return.n_cfdi_movimiento_id,
                    cfdi_manejosaldo_id = n_cfdi_manejosaldo_id,
                    cargo = f_cargo,
                    abono = f_abono,
                    saldo = _D_return.f_saldoNuevo,
                    importe = _D_return.f_importeNuevo,
                    importe_monedapago = f_importe,
                    monedacontpaqi_id = s_monedacontpaqi_id or TEMPRESA_MONEDASCONTPAQI.MXP,
                    tipocambio = f_tipocambio,
                    generado_por = self._E_movto_generado_por
                    )

                if (self._E_proceso == self.E_PROCESO.PROVEEDOR) or (E_accion == TCFDIRX_MOVIMIENTOS.E_ACCION.EMISION):
                    # Si esta ejecutando el proceso de cálculo por proveedor, o la acción a insertar es una emisión
                    _D_return.n_cfdi_movimiento_id = dbc01.tcfdirx_movimientos.insert(**_D_insertarMovimiento)
                else:
                    # En caso contrario, si se esta haciendo el proceso por CFDI y es una cancelación,
                    #  debe de insertarse la cancelación únicamente
                    pass

                if dbRow_cfdi.sat_status == TCFDISRX.SAT_STATUS.CANCELADO:
                    if dbRow_cfdi.fecha >= dbRow_cfdi.sat_fecha_cancelacion:
                        _dt_fechaCancelacion = dbRow_cfdi.fecha + datetime.timedelta(seconds = 1)
                    else:
                        _dt_fechaCancelacion = dbRow_cfdi.sat_fecha_cancelacion

                    _D_insertarMovimiento.fecha = _dt_fechaCancelacion
                    _D_insertarMovimiento.accion = TCFDIRX_MOVIMIENTOS.E_ACCION.CANCELACION
                    _D_insertarMovimiento.cfdi_movimiento_emision_id = _D_return.n_cfdi_movimiento_id

                    # No se rotan los cargos y abonos, ya que en el caso de hacer el proceso por proveedor, cargos
                    #  y abonos se definen en la fase 2; y en el caso de ser por CFDI, se mandan al llamar esta función

                    if (self._E_proceso == self.E_PROCESO.PROVEEDOR) \
                            or (E_accion == TCFDIRX_MOVIMIENTOS.E_ACCION.CANCELACION):
                        # Si esta ejecutando el proceso de cálculo de saldo por proveedor, o la acción corresponse
                        #  a una cancelación
                        _D_return.n_cfdi_movimientoCancelacion_id = dbc01.tcfdirx_movimientos.insert(
                            **_D_insertarMovimiento
                            )
                    else:
                        pass

                    if E_tipodocumento in (
                            TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.INGRESO,
                            TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.DEBITO
                            ):
                        _D_registro.abonos += _D_return.f_importeNuevo

                    elif E_tipodocumento in (
                            TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.PAGO,
                            TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.DEVOLUCION,
                            TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.DESCUENTO,
                            TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.ANTICIPO,
                            ):
                        _D_registro.cargos += _D_return.f_importeNuevo

                    elif E_tipodocumento in (
                            TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.OTRO,
                            TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.NO_DEFINIDO,
                            ):
                        _D_registro.cargos += f_abono
                        _D_registro.abonos += f_cargo

                    else:
                        self._D_multireturn.agrega_error(
                            "CFDI %d tipo de documento no procesa cancelación %s, Llamar al admministrador."
                            % dbRow_cfdi.id, TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.D_TODOS[E_tipodocumento]
                            )
                        return _D_return

                else:
                    pass

            else:
                # No se encuentra referencia a el ingreso del movimiento
                pass

            return _D_return

        def procesar_proveedor(self):
            """ Se procesan todos los CFDIs del proveedor para determinar su saldo

            Se empieza a partir de TCFDISRX.GET_PRIMERDIA2020

            @return:
            @rtype:
            """

            self._E_proceso = self.E_PROCESO.PROVEEDOR
            self._dt_procesoInicio = datetime.datetime.now()

            self._dbRow_proveedor = dbc01.tempresa_proveedores(self._n_proveedor_id)

            self._dbRow_proveedor.procesostatus = TEMPRESA_PROVEEDORES.E_PROCESOESTATUS.CFDIS_PROCESANDO
            self._dbRow_proveedor.update_record()
            dbc01.commit()

            # Se limpian los movimientos del proveedor
            dbc01(
                (dbc01.tcfdirx_movimientos.proveedor_id == self._n_proveedor_id)
                ).delete()

            # Todos los CFDIs del proveedor, el saldo debe ser None para usar este estatus en caso de no ser analizado
            dbc01(
                (dbc01.tcfdisrx.emisorproveedor_id == self._n_proveedor_id)
                & (dbc01.tcfdisrx.saldo != None)
                ).update(saldo = None)

            # Se toman todos los CFDIs del proveedor
            _dbRows_cfdi = dbc01(
                (dbc01.tcfdisrx.emisorproveedor_id == self._n_proveedor_id)
                & (dbc01.tcfdisrx.fecha >= self._dt_empezarde)
                # & (dbc01.tcfdisrx.sat_status != TCFDISRX.SAT_STATUS.NO_DEFINIDO)
                ).select(
                    dbc01.tcfdisrx.ALL,
                    orderby = [dbc01.tcfdisrx.fecha, dbc01.tcfdisrx.id]
                    )

            self._D_debug.agrega_mensaje("Se recorren todos los cfdis encontrados del cliente despues de la fecha")
            _b_primerIngreso = False
            for _dbRow_cfdi in _dbRows_cfdi:

                if _b_primerIngreso \
                        or (_dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO):
                    _b_primerIngreso = True
                    self._proveedor_fase1_cfdi(_dbRow_cfdi)
                else:
                    pass

            self._dbRow_proveedor.procesostatus = TEMPRESA_PROVEEDORES.E_PROCESOESTATUS.CFDIS_APLICANDO_SALDOS
            self._dbRow_proveedor.update_record()
            dbc01.commit()

            _dbRows_movtos = dbc01(
                dbc01.tcfdirx_movimientos.proveedor_id == self._n_proveedor_id
                ).select(
                    dbc01.tcfdirx_movimientos.ALL,
                    orderby = [
                        dbc01.tcfdirx_movimientos.fecha,
                        dbc01.tcfdirx_movimientos.fechaingreso
                        ]
                    )

            for _dbRow_movto in _dbRows_movtos:
                self._proveedor_fase2_aplicacionsaldos(_dbRow_movto)

            for _s_cfdi_id in self._D_registros:
                _D_registro = self._D_registros[_s_cfdi_id]
                _D_registro.dbRow.saldo = _D_registro.saldo
                _D_registro.dbRow.saldofechacalculo = request.utcnow

                if (_D_registro.dbRow.tipocomprobante_id != TTIPOSCOMPROBANTES.INGRESO) \
                        and (_D_registro.dbRow.saldo != 0):
                    self._D_multireturn.agrega_error(
                        "CFDI %s, quedó con saldo '%.2f' sin aplicar."
                        % (_s_cfdi_id, _D_registro.dbRow.saldo)
                        )

                else:
                    # En caso de no existir plazo de crédito en el ingreso, se agrega el de default del CFDI
                    if _D_registro.dbRow.credito_plazo:
                        pass

                    else:
                        _D_registro.dbRow.credito_plazo = self._dbRow_proveedor.credito_plazo

                _D_registro.dbRow.update_record()

            # self.actualizar_saldo_proveedor()  # TODOMejora, usar esta función en el proveedor

            self._dt_procesoFin = datetime.datetime.now()
            self._D_multireturn.agrega_info(
                "Proceso tomó %d segundos. Saldo de %.2f, saldo vencido de %.2f"
                % (
                    (self._dt_procesoFin - self._dt_procesoInicio).seconds,
                    self._f_saldoCliente,
                    self._f_saldoVencidoCliente
                    )
                )

            self._E_proceso = self.E_NINGUNO

            return self._D_multireturn

        def _proveedor_fase1_cfdi(self, dbRow_cfdi):
            """ Etapa 1 que procesa la información del CFDI

            @param dbRow_cfdi: CFDI a procesar
            """

            if dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO:

                self._addMovimiento(
                    x_cfdirx_ingreso = dbRow_cfdi,
                    dbRow_cfdi = dbRow_cfdi,

                    dt_fecha = dbRow_cfdi.fecha,
                    E_accion = TCFDIRX_MOVIMIENTOS.E_ACCION.EMISION,
                    E_tipodocumento = TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.INGRESO,
                    s_descripcion = "Factura",

                    f_importe = dbRow_cfdi.total,
                    s_monedacontpaqi_id = dbRow_cfdi.monedacontpaqi_id,
                    f_tipocambio = dbRow_cfdi.tipocambio
                    )

                # Se obtienen los registros de traspaso aplicables al cfdi actual
                _dbRows_manejoSaldos = dbc01(
                    (dbc01.tcfdirx_manejosaldos.cfdi_id == dbRow_cfdi.id)
                    ).select(
                        dbc01.tcfdirx_manejosaldos.ALL,
                        )

                for _dbRow_manejoSaldo in _dbRows_manejoSaldos:

                    self._addMovimiento(
                        x_cfdirx_ingreso = dbRow_cfdi,
                        dbRow_cfdi = dbRow_cfdi,

                        dt_fecha = _dbRow_manejoSaldo.fecha,
                        E_accion = TCFDIRX_MOVIMIENTOS.E_ACCION.EMISION,
                        E_tipodocumento = TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.TRASPASO_SALDO_A_FAVOR,
                        s_descripcion = "Traspaso",

                        n_cfdi_manejosaldo_id = _dbRow_manejoSaldo.id,

                        f_importe = _dbRow_manejoSaldo.importe,
                        s_monedacontpaqi_id = dbRow_cfdi.monedacontpaqi_id,
                        f_tipocambio = dbRow_cfdi.tipocambio
                        )

                # Se obtienen los registros de aplicacion de saldo a favor aplicables al CFDI actual
                _dbRows_manejoSaldos = dbc01(
                    (dbc01.tcfdirx_manejosaldos.cfdirelacionado_id == dbRow_cfdi.id)
                    ).select(
                        dbc01.tcfdirx_manejosaldos.ALL,
                        )

                for _dbRow_manejoSaldo in _dbRows_manejoSaldos:

                    self._addMovimiento(
                        x_cfdirx_ingreso = dbRow_cfdi,
                        dbRow_cfdi = dbRow_cfdi,

                        dt_fecha = _dbRow_manejoSaldo.fecha,
                        E_accion = TCFDIRX_MOVIMIENTOS.E_ACCION.EMISION,
                        E_tipodocumento = TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.APLICACION_SALDO_A_FAVOR,
                        s_descripcion = "Aplicación",

                        n_cfdi_manejosaldo_id = _dbRow_manejoSaldo.id,

                        f_importe = _dbRow_manejoSaldo.importe,
                        s_monedacontpaqi_id = dbRow_cfdi.monedacontpaqi_id,
                        f_tipocambio = dbRow_cfdi.tipocambio
                        )

            elif dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.PAGO:

                dbRow_cfdi.totalpago = 0
                _dbRows_pagos = dbc01(
                    (dbc01.tcfdirx_complementopagos.cfdi_id == dbRow_cfdi.id)
                    & (dbc01.tcfdirx_complementopago_doctosrelacionados.cfdi_complementopago_id
                       == dbc01.tcfdirx_complementopagos.id)
                    ).select(
                        dbc01.tcfdirx_complementopago_doctosrelacionados.ALL,
                        dbc01.tcfdirx_complementopagos.ALL,
                        )

                for _dbRow_pago in _dbRows_pagos:
                    self._proveedor_fase1_cfdi_pago(dbRow_cfdi, _dbRow_pago)

                dbRow_cfdi.update_record()

            elif dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.EGRESO:

                _dbRows_relacionados = dbc01(
                    (dbc01.tcfdirx_relacionados.cfdi_id == dbRow_cfdi.id)
                    ).select(
                        dbc01.tcfdirx_relacionados.ALL,
                        )

                _s_descripcion = ""
                _E_tipodocumento = TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.NO_DEFINIDO
                if dbRow_cfdi.receptorusocfdi_id == TUSOSCFDI.DEVOLUCIONES_DESCUENTOS_BONIFICACIONES:

                    if dbRow_cfdi.tiporelacion_id == TTIPOSRELACIONES.NOTACREDITO:

                        _s_descripcion = "Descuento"
                        _E_tipodocumento = TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.DESCUENTO

                    elif dbRow_cfdi.tiporelacion_id == TTIPOSRELACIONES.DEVOLUCION:

                        _s_descripcion = "Devolucion"
                        _E_tipodocumento = TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.DEVOLUCION

                    else:

                        # Basado en la retroalimentación del cliente, se asigna como descuento
                        _s_descripcion = "Descuento"
                        _E_tipodocumento = TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.DESCUENTO
                        self._D_multireturn.agrega_advertencia(
                            ("CFDI de Egreso %d, tipo relación '%s' no válido para devolucion/descuento/bonificación. "
                             + "Se asigna como descuento.")
                            % (dbRow_cfdi.id, dbRow_cfdi.tiporelacion_id)
                            )

                else:

                    if dbRow_cfdi.tiporelacion_id == TTIPOSRELACIONES.NOTADEBITO:

                        _s_descripcion = "Nota de Debito"
                        _E_tipodocumento = TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.DEBITO

                    elif dbRow_cfdi.tiporelacion_id == TTIPOSRELACIONES.ANTICIPO:

                        _s_descripcion = "Anticipo"
                        _E_tipodocumento = TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.ANTICIPO

                    elif dbRow_cfdi.tiporelacion_id == TTIPOSRELACIONES.NOTACREDITO:

                        _s_descripcion = "Descuento"
                        _E_tipodocumento = TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.DESCUENTO
                        self._D_multireturn.agrega_advertencia(
                            "CFDI de Egreso %d, tipo relación %d no válido, pero se procede a manejarlo como descuento"
                            % (dbRow_cfdi.id, dbRow_cfdi.tiporelacion_id)
                            )

                    elif not dbRow_cfdi.tiporelacion_id:

                        _s_descripcion = "Descuento"
                        _E_tipodocumento = TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.DESCUENTO
                        self._D_multireturn.agrega_advertencia(
                            "CFDI de Egreso %d, tipo relación esta vacío, pero se procede a manejarlo como descuento"
                            % dbRow_cfdi.id
                            )

                    else:

                        self._D_multireturn.agrega_error(
                            "CFDI de Egreso %d, tipo relación %d no válido"
                            % (dbRow_cfdi.id, dbRow_cfdi.tiporelacion_id)
                            )

                if _s_descripcion and _E_tipodocumento:
                    _n_numRelacionados = len(_dbRows_relacionados)
                    for _dbRow_relacionado in _dbRows_relacionados:
                        self._proveedor_fase1_cfdi_egreso(
                            dbRow_cfdi, _dbRow_relacionado, _s_descripcion, _E_tipodocumento, _n_numRelacionados
                            )
                else:
                    # El error ya se definió en la condición anterior
                    pass

            else:
                self._D_multireturn.agrega_error(
                    "Tipo de CFDI %d no válido %d" % (dbRow_cfdi.id, dbRow_cfdi.tipocomprobante_id)
                    )

            return

        def _proveedor_fase1_cfdi_pago(self, dbRow_cfdi, dbRow_pago):
            """ Se registra información de la parte 1 cuando es pago.

            @param dbRow_cfdi: CFDI principal del pago.
            @param dbRow_pago: Registro con información del complemento de pago y su documento relacionado.
            """

            if not dbRow_pago.tcfdirx_complementopago_doctosrelacionados.cfdirelacionado_id:
                # Si no tiene un id, se busca por el uuid del docto relacionado
                TCFDIRX_COMPLEMENTOPAGO_DOCTOSRELACIONADOS.ASOCIAR_UUID(
                    dbRow_pago.tcfdirx_complementopago_doctosrelacionados
                    )

                _dbRow_compPago_docRel = dbc01.tcfdirx_complementopago_doctosrelacionados(
                    dbRow_pago.tcfdirx_complementopago_doctosrelacionados.id
                    )
                if not _dbRow_compPago_docRel.cfdirelacionado_id:
                    self._D_multireturn.agrega_advertencia(
                        "CFDI referenciado (%s) en pago %d, es ignorado, ya que no se encuentra la referencia"
                        % (dbRow_pago.tcfdirx_complementopago_doctosrelacionados.iddocumento, dbRow_cfdi.id)
                        )
                    return
                else:
                    _cfdirelacionado_id = _dbRow_compPago_docRel.cfdirelacionado_id

            else:
                _cfdirelacionado_id = dbRow_pago.tcfdirx_complementopago_doctosrelacionados.cfdirelacionado_id

            if _cfdirelacionado_id in self._D_registros:
                _dbRow_cfdiIngreso = self._D_registros[_cfdirelacionado_id].dbRow

                if _dbRow_cfdiIngreso.tipocomprobante_id != TTIPOSCOMPROBANTES.INGRESO:
                    self._D_multireturn.agrega_error(
                        "El CFDI referenciado %d en pago %d no es de ingreso"
                        % (dbRow_pago.tcfdirx_complementopago_doctosrelacionados.id, dbRow_cfdi.id)
                        )
                    _dbRow_cfdiIngreso.receptorcliente_id = None
                    _dbRow_cfdiIngreso.update_record()

                elif _dbRow_cfdiIngreso.receptorcliente_id != self._n_cliente_id:
                    self._D_multireturn.agrega_error(
                        "El CFDI referenciado %d en pago %d no corresponden al mismo cliente"
                        % (dbRow_pago.tcfdirx_complementopago_doctosrelacionados.id, dbRow_cfdi.id)
                        )

                else:

                    if dbRow_pago.tcfdirx_complementopagos.fechapago < _dbRow_cfdiIngreso.fecha:
                        self._D_multireturn.agrega_advertencia(
                            ("CFDI %d referenciado (%s) en pago, tiene fecha posterior. "
                             + "Se ajusta fecha para manejo de saldo")
                            % (dbRow_cfdi.id, dbRow_pago.tcfdirx_complementopago_doctosrelacionados.iddocumento)
                            )
                        _dt_fecha = _dbRow_cfdiIngreso.fecha + datetime.timedelta(seconds = 1)
                    else:
                        _dt_fecha = dbRow_pago.tcfdirx_complementopagos.fechapago

                    dbRow_cfdi.totalpago += dbRow_pago.tcfdirx_complementopago_doctosrelacionados.imppagado

                    self._addMovimiento(
                        x_cfdirx_ingreso = _dbRow_cfdiIngreso,
                        dbRow_cfdi = dbRow_cfdi,

                        dt_fecha = _dt_fecha,
                        E_accion = TCFDIRX_MOVIMIENTOS.E_ACCION.EMISION,
                        E_tipodocumento = TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.PAGO,
                        s_descripcion = "Pago",
                        n_cfdi_complementopago_doctorelacionado_id =
                        dbRow_pago.tcfdirx_complementopago_doctosrelacionados.id,

                        dbRow_relacionado = dbRow_pago.tcfdirx_complementopago_doctosrelacionados,

                        f_importe = dbRow_pago.tcfdirx_complementopago_doctosrelacionados.imppagado,

                        s_monedacontpaqi_id = dbRow_pago.tcfdirx_complementopagos.monedacontpaqi_id,
                        f_tipocambio = dbRow_pago.tcfdirx_complementopagos.tipocambio
                        )
            else:
                self._D_multireturn.agrega_advertencia(
                    "El CFDI referenciado %d en pago %d esta fuera de rango en cálculo por lo que se ignora."
                    % (_cfdirelacionado_id, dbRow_cfdi.id)
                    )
                pass

            return

        def _proveedor_fase1_cfdi_egreso(
                self,
                dbRow_cfdi,
                dbRow_relacionado,
                s_descripcion,
                E_tipodocumento,
                n_numRelacionados
                ):
            """ Se registra en la etapa 1 los movimientos relacionados a los egresos.

            @param dbRow_cfdi: CFDI principal del egreso.
            @param dbRow_relacionado: Registro con información del documento relacionado.
            @param s_descripcion: _addMovimiento::s_descripcion
            @param E_tipodocumento: TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO
            @param n_numRelacionados: número de documentos encontrados en la relación al que corresponde el actual
             dbRow_relacionado.

            """

            if not dbRow_relacionado.cfdirelacionado_id:
                TCFDIRX_RELACIONADOS.ASOCIAR_UUID(dbRow_relacionado.id)

                _dbRow_docRelacionado_actualizado = dbc01.tcfdirx_relacionados(dbRow_relacionado.id)
                if not _dbRow_docRelacionado_actualizado.cfdirelacionado_id:
                    self._D_multireturn.agrega_advertencia(
                        "CFDI referenciado (%s) en egreso %d, es ignorado, ya que no se encuentra la referencia"
                        % (dbRow_relacionado.uuid, dbRow_cfdi.id)
                        )
                    return
                else:
                    _cfdirelacionado_id = _dbRow_docRelacionado_actualizado.cfdirelacionado_id

            else:
                _cfdirelacionado_id = dbRow_relacionado.cfdirelacionado_id

            if _cfdirelacionado_id in self._D_registros:
                _dbRow_cfdiIngreso = self._D_registros[_cfdirelacionado_id].dbRow

                if _dbRow_cfdiIngreso.tipocomprobante_id != TTIPOSCOMPROBANTES.INGRESO:
                    self._D_multireturn.agrega_error(
                        "El CFDI referenciado %d en egreso %d no es de ingreso" % (dbRow_relacionado.id, dbRow_cfdi.id)
                        )
                    _dbRow_cfdiIngreso.cfdirelacionado_id = None
                    _dbRow_cfdiIngreso.update_record()

                elif _dbRow_cfdiIngreso.receptorcliente_id != self._n_cliente_id:
                    self._D_multireturn.agrega_error(
                        "El CFDI referenciado %d en egreso %d no corresponden al mismo cliente"
                        % (dbRow_relacionado.id, dbRow_cfdi.id)
                        )

                else:

                    if dbRow_cfdi.fecha < _dbRow_cfdiIngreso.fecha:
                        self._D_multireturn.agrega_advertencia(
                            ("CFDI %d referenciado (%s) en egreso, tiene fecha posterior. "
                             + "Se ajusta fecha para manejo de saldo")
                            % (dbRow_cfdi.id, dbRow_relacionado.uuid)
                            )
                        _dt_fecha = _dbRow_cfdiIngreso.fecha + datetime.timedelta(seconds = 1)
                    else:
                        _dt_fecha = dbRow_cfdi.fecha

                    # Si solamente tiene un elemento referenciado, se puede aplicar el total del CFDI
                    #  a solamente un ingreso;
                    #  de lo contrario, no se sabe y se deja en cero para su posterior aplicación
                    if n_numRelacionados == 1:
                        _f_importe = dbRow_cfdi.total
                    elif dbRow_relacionado.fijarimporte:
                        _f_importe = dbRow_relacionado.importe
                    else:
                        _f_importe = 0

                    self._addMovimiento(
                        x_cfdirx_ingreso = _dbRow_cfdiIngreso,
                        dbRow_cfdi = dbRow_cfdi,

                        dt_fecha = _dt_fecha,
                        E_accion = TCFDIRX_MOVIMIENTOS.E_ACCION.EMISION,
                        E_tipodocumento = E_tipodocumento,
                        s_descripcion = s_descripcion,
                        n_cfdi_relacionado_id = dbRow_relacionado.id,

                        dbRow_relacionado = dbRow_relacionado,

                        f_importe = _f_importe,
                        s_monedacontpaqi_id = dbRow_cfdi.monedacontpaqi_id,
                        f_tipocambio = dbRow_cfdi.tipocambio
                        )
            else:
                self._D_multireturn.agrega_advertencia(
                    "El CFDI referenciado %d en egreso %d esta fuera de rango en cálculo por lo que se ignora."
                    % (_cfdirelacionado_id, dbRow_cfdi.id)
                    )
                pass

            return

        def _proveedor_fase2_aplicacionsaldos(self, dbRow_movto):
            """ Hace el cálculo de los saldos del cliente en base a la información de la tabla de movimientos.

            @param dbRow_movto: registro del movimiento
            """

            if dbRow_movto.cfdi_ingreso_id not in self._D_registros:
                self._D_multireturn.agrega_error(
                    "No se encontró el ingreso para análisis del movimiento %d, por lo que se ignora" % dbRow_movto.id
                    )
                return
            else:

                _dbRow_cfdiIngreso = self._D_registros[dbRow_movto.cfdi_ingreso_id].dbRow

                if dbRow_movto.tipodocumento == TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.INGRESO:
                    if _dbRow_cfdiIngreso.metodopago_id == TMETODOSPAGO.UNAEXHIBICION:
                        dbRow_movto.abono = dbRow_movto.importe
                        dbRow_movto.cargo = dbRow_movto.importe

                    elif dbRow_movto.accion == TCFDIRX_MOVIMIENTOS.E_ACCION.CANCELACION:
                        dbRow_movto.abono = dbRow_movto.importe

                    else:
                        # En caso de emisión o no timbrada se usa como cargo
                        dbRow_movto.cargo = dbRow_movto.importe

                elif dbRow_movto.tipodocumento == TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.PAGO:
                    if dbRow_movto.accion == TCFDIRX_MOVIMIENTOS.E_ACCION.CANCELACION:
                        dbRow_movto.cargo = dbRow_movto.importe

                    else:
                        # En caso de emisión o no timbrada se usa como abono
                        dbRow_movto.abono = dbRow_movto.importe

                elif dbRow_movto.tipodocumento in (
                        TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.DEVOLUCION,
                        TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.DESCUENTO,
                        TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.ANTICIPO,
                        ):
                    self._proveedor_fase2_aplicacionsaldos_egreso_devdesant(dbRow_movto)

                elif dbRow_movto.tipodocumento == TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.DEBITO:
                    self._D_multireturn.agrega_error(
                        "CFDI movimiento %d de tipo Debito no implementado aún" % dbRow_movto.id
                        )

                elif dbRow_movto.tipodocumento == TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.TRASPASO_SALDO_A_FAVOR:
                    dbRow_movto.cargo = dbRow_movto.importe

                elif dbRow_movto.tipodocumento == TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.APLICACION_SALDO_A_FAVOR:
                    dbRow_movto.abono = dbRow_movto.importe

                else:
                    self._D_multireturn.agrega_error("CFDI movimiento %d no reconocido" % dbRow_movto.id)

                if dbRow_movto.accion in (
                        TCFDIRX_MOVIMIENTOS.E_ACCION.EMISION,
                        TCFDIRX_MOVIMIENTOS.E_ACCION.CANCELACION
                        ):
                    # Si es un movimiento timbrado, acción tiene valor de emisión o cancelación
                    self._D_registros[dbRow_movto.cfdi_ingreso_id].saldo += dbRow_movto.cargo - dbRow_movto.abono

                    dbRow_movto.saldo = self._D_registros[dbRow_movto.cfdi_ingreso_id].saldo
                    dbRow_movto.update_record()

                    # El importe define si el total a aplicar se definió en la fase 1
                    if dbRow_movto.importe:
                        self._D_registros[dbRow_movto.cfdi_ingreso_id].aplicadoscargos += dbRow_movto.cargo
                        self._D_registros[dbRow_movto.cfdi_ingreso_id].aplicadosabonos += dbRow_movto.abono
                    else:
                        self._D_registros[dbRow_movto.cfdi_ingreso_id].variablescargos += dbRow_movto.cargo
                        self._D_registros[dbRow_movto.cfdi_ingreso_id].variablesabonos += dbRow_movto.abono

                else:
                    # Si el CFDI no ha sido timbrado, no se manejan saldos, solo importes
                    dbRow_movto.update_record()

            return

        def _proveedor_fase2_aplicacionsaldos_egreso_devdesant(self, dbRow_movto):
            """ Se aplica el saldo para los egresos.

            @param dbRow_movto: dbRow del movimiento.
            """

            _dbRow_relacionado = self._D_registros[dbRow_movto.cfdi_ingreso_id] \
                .referenciasRelacionados[dbRow_movto.cfdi_relacionado_id]

            if dbRow_movto.accion == TCFDIRX_MOVIMIENTOS.E_ACCION.CANCELACION:
                # Si es un movimiento de cancelación, se busca el movimiento inicial y se cambian los importes

                _dbRow_movtoInicial = dbc01.tcfdirx_movimientos(dbRow_movto.cfdi_movimiento_emision_id)
                dbRow_movto.cargo = _dbRow_movtoInicial.abono
                dbRow_movto.abono = 0
                # Este es el saldo del CFDI de egreso
                self._D_registros[dbRow_movto.cfdi_id].saldo += dbRow_movto.cargo

            else:
                if dbRow_movto.cfdi_id not in self._D_registros:
                    # Se utiliza el mismo control de registros para egresos, con el fin de controlar saldos, etc.

                    _dbRow_cfdi = self._D_registros[dbRow_movto.cfdi_ingreso_id].referencias[dbRow_movto.cfdi_id]
                    self._D_registros[dbRow_movto.cfdi_id] = Storage()
                    self._D_registros[dbRow_movto.cfdi_id].dbRow = _dbRow_cfdi
                    self._D_registros[dbRow_movto.cfdi_id].saldo = _dbRow_cfdi.total

                else:
                    pass

                # Si el importe pudo ser determinado en la primer etapa,
                #  debido a que la referencia aplica a un solo ingreso, se aplica
                if dbRow_movto.importe > 0:
                    dbRow_movto.abono = dbRow_movto.importe
                    self._D_registros[dbRow_movto.cfdi_id].saldo -= dbRow_movto.abono
                elif self._D_registros[dbRow_movto.cfdi_id].saldo <= 0:
                    # Si el saldo en el CFDI de egreso es cero o menor que cero, se registra el warning

                    _dbRow_cfdi = self._D_registros[dbRow_movto.cfdi_ingreso_id].referencias[dbRow_movto.cfdi_id]

                    if not (_dbRow_cfdi.sat_status == TCFDISRX.SAT_STATUS.CANCELADO):
                        self._D_multireturn.agrega_advertencia(
                            "CFDI %d de Egreso tiene más referencias que saldo disponible" % dbRow_movto.cfdi_id
                            )
                    else:
                        pass

                elif (self._E_proceso == self.E_CFDI) or _dbRow_relacionado.fijarimporte:

                    # Si el proceso en ejecución es el cálculo del saldo del CFDI,
                    #  se usa el importe en el de egreso como el importe aplicado
                    dbRow_movto.abono = _dbRow_relacionado.importe

                    # Este es el saldo del CFDI de egreso
                    self._D_registros[dbRow_movto.cfdi_id].saldo -= dbRow_movto.abono

                else:

                    # Se determina el saldo efectivo de la factura:
                    _f_saldoIngresoCalculado = \
                        self._D_registros[dbRow_movto.cfdi_ingreso_id].cargos \
                        - self._D_registros[dbRow_movto.cfdi_ingreso_id].abonos \
                        + self._D_registros[dbRow_movto.cfdi_ingreso_id].variablescargos \
                        - self._D_registros[dbRow_movto.cfdi_ingreso_id].variablesabonos

                    if _f_saldoIngresoCalculado == 0:
                        # Si el saldo ingreso calculado es igual a 0, no mas abonos deben hacerse
                        pass

                    elif self._D_registros[dbRow_movto.cfdi_id].saldo > _f_saldoIngresoCalculado:
                        dbRow_movto.abono = _f_saldoIngresoCalculado

                    else:
                        dbRow_movto.abono = self._D_registros[dbRow_movto.cfdi_id].saldo
                    # Este es el saldo del CFDI de egreso
                    self._D_registros[dbRow_movto.cfdi_id].saldo -= dbRow_movto.abono

            if dbRow_movto.abono != _dbRow_relacionado.importe:
                _dbRow_relacionado.importe = dbRow_movto.abono
                _dbRow_relacionado.update_record()
            else:
                pass

            return

        pass  # EDOCTA

    pass  # TCFDISRX


dbc01.define_table(
    TCFDISRX.S_NOMBRETABLA, *TCFDISRX.L_DBCAMPOS,
    format = lambda row: '%(serie)s %(folio)s [%(tipodecomprobante)s]' % (row.as_dict()),
    singular = 'CFDI Recibido',
    plural = 'CFDIs Recibidos',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TCFDISRX
    )


class TCFDISRX2(Table):
    """ Tabla soporte de CFDIS con el contenido parcial de campos extendidos de TCFDI.

    """

    S_NOMBRETABLA = 'tcfdisrx2'

    L_DBCAMPOS = [
        Field(
            'cfdi_id', dbc01.tcfdisrx, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'CFDI', comment = None,
            writable = False, readable = False,
            represent = None
            ),

        Field(
            'interesmoratoriopagare', 'decimal(8,2)', default = 5,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputPercentage, label = 'Interés moratorio',
            comment = 'Interés moratorio mensualusado en pagaré.',
            writable = False, readable = True,
            represent = stv_represent_percentage
            ),

        # TODO Pendientes de revisión
        Field(
            'ordencompra', 'string', length = 10, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Orden compra', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'foliointernopedido', 'string', length = 10, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Folio interno pedido', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'agentevendedor', 'string', length = 100, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Agente vendedor', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'montobaseprodiva0', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Monto base prod. IVA 0', comment = None,
            writable = False, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        Field(
            'montobaseprodiva', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Monto base prod. IVA', comment = None,
            writable = False, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        Field(
            'emisorcalle', 'string', length = 100, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Emisor calle', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'emisornumexterior', 'string', length = 55, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Emisor Num. Exterior', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'emisornuminterior', 'string', length = 55, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Emisor Num. Interior', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'emisorcolonia', 'string', length = 120, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Emisor Colonia', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'emisorlocalidad', 'string', length = 120, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Emisor Localidad', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'emisormunicipio', 'string', length = 120, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Emisor Municipio', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'emisorestado', 'string', length = 30, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Emisor Estado', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'emisorpais', 'string', length = 30, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Emisor Pais', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'emisorcodigopostal', 'string', length = 30, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Emisor CP', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),

        Field(
            'receptoremails', 'string', length = 150, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Email(s)', comment = 'Emails separados por commas',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'receptortelefonos', 'string', length = 150, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Tel(s)', comment = 'Telefonos separados por |',
            writable = True, readable = True,
            represent = stv_represent_string
            ),

        Field(
            'receptorcalle', 'string', length = 100, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Receptor calle', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ), Field(
            'receptorreferencia', 'string', length = 100, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Receptor referencia', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'receptornumexterior', 'string', length = 55, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Receptor Num. Exterior', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'receptornuminterior', 'string', length = 55, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Receptor Num. Interior', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'receptorcolonia', 'string', length = 120, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Receptor Colonia', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'receptorlocalidad', 'string', length = 120, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Receptor Localidad', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'receptormunicipio', 'string', length = 120, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Receptor Municipio', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'receptorestado', 'string', length = 30, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Receptor Estado', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'receptorpais', 'string', length = 30, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Receptor Pais', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'receptorcodigopostal', 'string', length = 30, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Receptor CP', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),

        # Información del timbre digital
        Field(
            'certificado', 'text', length = 3000, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_text, label = 'Certificado', comment = None,
            writable = False, readable = True,
            represent = stv_represent_text
            ),
        Field(
            'nocertificado', 'string', length = 50, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'No. Certificado', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'versiontimbre', 'string', length = 50, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Versión Certificado', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'rfcprovcertif', 'string', length = 50, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'RFC Proveedor Certificado', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'sellocfd', 'text', length = 3000, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_text, label = 'Sello CFD', comment = None,
            writable = False, readable = True,
            represent = stv_represent_text
            ),
        Field(
            'sellosat', 'text', length = 3000, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_text, label = 'Sello SAT', comment = None,
            writable = False, readable = True,
            represent = stv_represent_text
            ),
        Field(
            'nocertificadosat', 'string', length = 50, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'No. Certificado SAT', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),

        ]

    def __init__(self, dbRef, tablename, *fields, **args):
        Table.__init__(self, dbRef, tablename, *fields, **args)
        # Se agrega la referencia a la tabla con info adicional
        TCFDISRX.PROC.dbTabla2 = self

        return

    pass  # TCFDISRX2


dbc01.define_table(
    TCFDISRX2.S_NOMBRETABLA, *TCFDISRX2.L_DBCAMPOS,
    format = lambda row: '%(serie)s %(folio)s [%(tipodecomprobante)s]' % (row.as_dict()),
    singular = 'CFDI soporte',
    plural = 'CFDIs Soporte',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TCFDISRX2
    )


class TCFDIRX_XMLS:
    pass


dbc02.define_table(
    'tcfdirx_xmls',
    Field(
        'cfdi_id', 'integer', default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'CFDI', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'xml_cfdi', 'text', default = None,
        required = False,
        notnull = False,
        label = 'XML', comment = "XML del CFDI",
        writable = False, readable = False,
        represent = stv_represent_xml
        ),
    Field(
        'json_cfdi', 'text', default = None,
        required = False,
        notnull = False,
        label = 'Json', comment = "Json del CFDI",
        writable = False, readable = False,
        represent = stv_represent_json
        ),
    tSignature_dbc02,
    format = '%(cfdi_id)s',
    singular = 'CFDI XML',
    plural = 'CFDI XMLs',
    migrate = D_stvSiteCfg.b_requestIsBasedApp
    )


class TCFDIRX_RELACIONADOS(Table):
    S_NOMBRETABLA = 'tcfdirx_relacionados'
    L_DBCAMPOS = [
        Field(
            'cfdi_id', dbc01.tcfdisrx, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'CFDI', comment = None,
            writable = False, readable = False,
            represent = None
            ),
        Field(
            'uuid', 'string', length = 50, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'CFDI Relacionado UUID', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        TCFDISRX.DEFINIR_DBCAMPO(
            'cfdirelacionado_id',
            represent = lambda v, r: stv_represent_referencefield(
                v, r, dbField_paraReference.dbc01.tcfdirx_relacionados.cfdirelacionado_id
                )
            ),

        Field(
            'fijarimporte', 'boolean', default = False,
            required = False,
            notnull = False,
            widget = stv_widget_inputCheckbox, label = 'Fijar Importe', comment = None,
            writable = True, readable = True,
            represent = stv_represent_boolean
            ),
        Field(
            'importe', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Importe', comment = 'Especifica el importe aplicado',
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        tSignature_dbc01,
        ]

    def __init__(self, dbRef, tablename, *fields, **args):
        Table.__init__(self, dbRef, tablename, *fields, **args)
        self.PROC.dbTabla = self
        return

    @staticmethod
    def ASOCIAR_UUID(s_cfdirx_relacionado_id):
        """ Se busca y asocian ligas rotas en a CFDIs relacionados
        """

        _D_return = Storage(
            L_msgError = [],
            E_return = CLASS_e_RETURN.NOK_ERROR,
            n_updatedRows = 0
            )

        # Ejemplo puede usar
        # tcfdirx_relacionados.id = 8419 de tcfdisrx.id = 47696
        # tcfdisrx.id = 47762

        _dbTableCFDIsBuscar = dbc01.tcfdisrx.with_alias('cfdisbuscar')
        _dbRows_cfdiRelacionados = dbc01(
            (dbc01.tcfdirx_relacionados.id == s_cfdirx_relacionado_id)
            & (dbc01.tcfdisrx.id == dbc01.tcfdirx_relacionados.cfdi_id)
            & (_dbTableCFDIsBuscar.uuid == dbc01.tcfdirx_relacionados.uuid)
            & (_dbTableCFDIsBuscar.empresa_id == dbc01.tcfdisrx.empresa_id)
            ).select(
                dbc01.tcfdirx_relacionados.ALL,
                _dbTableCFDIsBuscar.id,
                )

        if len(_dbRows_cfdiRelacionados) > 1:
            # Error, no debería de regresar mas de un registro
            _D_return.L_msgError += [
                "Se encontraron más de un registro relacionado, contacte al administrador %s" % s_cfdi_relacionado_id
                ]
            _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
        elif len(_dbRows_cfdiRelacionados) == 0:
            # CFDI asociado con el UUID no se encontró
            _D_return.L_msgError += ["No se encontraron cfdis relacionados"]
            _D_return.E_return = CLASS_e_RETURN.OK
        else:
            # Si tiene solamente un registro, hay que actualizarlo
            _dbRow = _dbRows_cfdiRelacionados.last()
            _D_return.n_updatedRows += dbc01(
                dbc01.tcfdirx_relacionados.id == _dbRow.tcfdirx_relacionados.id
                ).update(
                    cfdirelacionado_id = _dbRow.cfdisbuscar.id
                    )
            _D_return.L_msgError += ["Se actualizó el regisro con la relación al CFDI"]
            _D_return.E_return = CLASS_e_RETURN.OK

        return _D_return

    @staticmethod
    def CONFIGURAR_INTEGRIDAD_REFERENCIAL():
        _dbTable = dbc01.tcfdirx_relacionados
        _dbTable.cfdirelacionado_id.type = 'reference tcfdisrx'
        _dbTable.cfdirelacionado_id.ondelete = 'NO ACTION'
        _dbTable.cfdirelacionado_id.requires = IS_IN_DB(
            dbc01,
            'tcfdisrx.id',
            dbc01.tcfdisrx._format
            )

        return None

    class PROC(TCFDISRX.PROC_DETALLE, object):

        dbTabla = None  # Se escribe con la inicialización

        def puede_crear(self, x_cfdirx, **D_defaults):
            """ Determina si puede o no crear un CFDI de traslado.

            Actualmente no hay condición requerida.
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx, dbc01.tcfdis)
            _dbRow_cfdirx = _D_results.dbRow

            _D_results = super(TCFDIRX_RELACIONADOS.PROC, self).puede_crear(
                x_cfdirx = _dbRow_cfdirx,
                D_requeridosMaestro = Storage(
                    tiporelacion_id = Storage(
                        L_valores = [],  # Una lista vacía significa cualquier valor
                        s_msgError = "Tipo Relación es requerido"
                        )
                    ),
                **D_defaults
                )
            _D_return.combinar(_D_results)

            return _D_return

        @classmethod
        def CONFIGURACAMPOS_DESDE_dbRow(cls, dbRow_base):
            # cls.dbTabla.cfdi_id.default = dbRow_base.cfdi_id
            # cls.dbTabla.uuid.default = dbRow_base.uuid
            # cls.dbTabla.cfdirelacionado_id.default = dbRow_base.cfdirelacionado_id
            # cls.dbTabla.fijarimporte.default = dbRow_base.fijarimporte
            # cls.dbTabla.importe.default = dbRow_base.importe
            return

        def configuracampos_nuevo(
                self,
                s_cfdirx_relacion_id,
                b_llamadaDesdeEditar = False,
                **D_defaults
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param s_cfdirx_relacion_id:
            @type s_cfdirx_relacion_id:
            @param b_llamadaDesdeEditar:
            @type b_llamadaDesdeEditar:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = super(TCFDIRX_RELACIONADOS.PROC, self).configuracampos_nuevo(
                s_cfdirx_detalle_id = s_cfdirx_relacion_id,
                b_llamadaDesdeEditar = b_llamadaDesdeEditar,
                **D_defaults
                )
            _D_return.combinar(_D_results)

            self.dbTabla.cfdi_id.writable = False
            self.dbTabla.cfdirelacionado_id.writable = True
            self.dbTabla.fijarimporte.writable = True
            self.dbTabla.importe.writable = True

            # Campos que no deben ser editados, solo de forma autocalculada
            self.dbTabla.uuid.writable = False

            # Campos que todos los movimientos pueden editar
            self.dbTabla.notas.writable = False

            # Se definen los valores por default
            self.dbTabla.cfdi_id.default = self._D_defaults.cfdi_id
            # self.dbTabla.cfdirelacionado_id.default = None
            # self.dbTabla.fijarimporte.default = True
            # self.dbTabla.importe.default = 0

            return _D_return

        def configuracampos_edicion(
                self,
                x_cfdirx_relacionado,
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param x_cfdirx_relacionado:
            @type x_cfdirx_relacionado:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = super(TCFDIRX_RELACIONADOS.PROC, self).configuracampos_edicion(
                x_cfdirx_detalle = x_cfdirx_relacionado
                )
            _D_return.combinar(_D_results)

            # No se permite cambiar el CFDI relacionado, si se desea cambiarlo debe eliminarse el registro
            self.dbTabla.cfdirelacionado_id.writable = False

            return _D_return

        def al_validar(
                self,
                O_form,
                dbRow = None,
                D_camposAChecar = None,
                ):
            """ Validar la información para poder grabar cfdi

            @param D_camposAChecar:
            @type D_camposAChecar:
            @param dbRow:
            @type dbRow:
            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            dbRow = dbRow if dbRow else self.dbTabla(O_form.record_id) if O_form.record_id else None
            D_camposAChecar = D_camposAChecar if D_camposAChecar \
                else STV_FWK_LIB.OBTENER_CAMPOSACHECAR_PARA_VALIDAR(self.dbTabla, dbRow)

            # Validaciones

            # Se llaman las validaciones del CFDI_relacionado, indicando que tiporelacion_id debe existsir
            O_form = super(TCFDIRX_RELACIONADOS.PROC, self).al_validar(
                O_form, dbRow, D_camposAChecar = D_camposAChecar,
                )

            if not O_form.record_id:
                # Si esta creando el registro:

                _dbRow_cfdiRelacionado = dbc01.tcfdisrx(D_camposAChecar.cfdirelacionado_id)
                _dbRow_cfdi = dbc01.tcfdis(D_camposAChecar.cfdi_id)

                if not _dbRow_cfdiRelacionado:
                    O_form.errors.cfdirelacionado_id = "CFDI relacionado debe estar definido"

                elif not _dbRow_cfdi.tiporelacion_id:
                    O_form.errors.id = "Tipo relación en CFDI es requerido"

                elif _dbRow_cfdiRelacionado.receptorcliente_id != _dbRow_cfdi.receptorcliente_id:
                    O_form.errors.id = "CFDI relacionado debe pertenecer al mismo cliente"

                else:
                    pass

            else:
                # Si esta editando el registro

                if self.dbTabla.cfdirelacionado_id.writable \
                        and (dbRow.cfdirelacionado_id != D_camposAChecar.cfdirelacionado_id):
                    O_form.errors.cfdirelacionado_id = "CFDI relacionado no debe poder ser modificado"

                else:
                    pass

            # Validaciones

            return O_form

        @classmethod
        def AL_ACEPTAR(
                cls,
                O_form,
                dbRow = None,
                ):
            """ Al aceptar el registro deben hacerse los cálculos

            Se hacen todos los cálculos y aceptación de los cambios en el producto

            @param dbRow:
            @type dbRow:
            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()
            dbRow = dbRow if dbRow else cls.dbTabla(O_form.record_id or O_form.vars.id)

            if not O_form.record_id:
                # Si se inserta
                pass
            else:  # Si se edita
                pass

            O_form = super(TCFDIRX_RELACIONADOS.PROC, cls).AL_ACEPTAR(O_form, dbRow)

            # En el aceptar de la clase padre, se manda llamar a calcular CFDI

            generaMensajeFlash(cls.dbTabla, _D_return)
            return O_form

        @classmethod
        def MAPEAR_CAMPOS(
                cls,
                x_cfdirx_relacionado,
                **D_params
                ):
            """ Recalcula campos, haciendo la consulta de la carta porte a partir del CFDI

            @param x_cfdirx_relacionado:
            @type x_cfdirx_relacionado:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx_relacionado, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage(
                uuid = "",
                fijarimporte = _dbRow.fijarimporte,
                importe = _dbRow.importe
                )

            _dbRow_cfdiRelacionado = dbc01.tcfdisrx(_dbRow.cfdirelacionado_id)
            _D_actualizar.uuid = _dbRow_cfdiRelacionado.uuid if _dbRow_cfdiRelacionado else ""

            if not _D_actualizar.fijarimporte and not _D_actualizar.importe:

                _dbRow_cfdi = dbc01.tcfdisrx(_dbRow.cfdi_id)

                if (_dbRow_cfdi.sat_status == TCFDISRX.SAT_STATUS.NO_DEFINIDO) \
                        and (_dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.EGRESO) \
                        and (
                            _dbRow_cfdi.tiporelacion_id in (TTIPOSRELACIONES.NOTACREDITO, TTIPOSRELACIONES.NOTADEBITO)
                        ):

                    _dbField_sumaMontosEgreso = dbc01.tcfdirx_relacionados.importe.sum()
                    _dbRows_cfdi_egreso = dbc01(
                        (dbc01.tcfdirx_relacionados.cfdi_id == _dbRow.cfdi_id)
                        ).select(
                        _dbField_sumaMontosEgreso,
                        )

                    _dbRow_cfdi_egreso = _dbRows_cfdi_egreso.first()
                    _f_montoDisponible = _dbRow_cfdi.total - (_dbRow_cfdi_egreso[_dbField_sumaMontosEgreso] or 0)

                    _D_actualizar.importe = _dbRow_cfdiRelacionado.saldo \
                        if _f_montoDisponible > _dbRow_cfdiRelacionado.saldo else _f_montoDisponible

                    _D_actualizar.fijarimporte = True

                else:
                    pass

            else:
                pass

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)
                    # El mapear campos, no implica que se tenga que recalcular el CFDI
                    break
                else:
                    pass

            return _D_return

        pass  # TRASLADO

    pass  # TCFDIRX_RELACIONADOS


dbc01.define_table(
    TCFDIRX_RELACIONADOS.S_NOMBRETABLA, *TCFDIRX_RELACIONADOS.L_DBCAMPOS,
    format = '%(cfdi_id)s',
    singular = 'CFDI relacionado',
    plural = 'CFDI relacionados',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TCFDIRX_RELACIONADOS
    )


class TCFDIRX_CONCEPTOS(Table):
    """ Corresponde a los conceptos de los CFDIs. """
    S_NOMBRETABLA = 'tcfdirx_conceptos'
    L_DBCAMPOS = [
        Field(
            'cfdi_id', dbc01.tcfdisrx, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'CFDI', comment = None,
            writable = False, readable = False,
            represent = None
            ),
        Field(
            'orden', 'integer', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputInteger, label = 'Orden', comment = None,
            writable = False, readable = False,
            represent = stv_represent_number
            ),
        Field(
            'cantidad', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputFloat, label = 'Cantidad', comment = None,
            writable = True, readable = True,
            represent = stv_represent_float
            ),
        Field(
            'claveprodserv', 'string', length = 30, default = None,
            required = True,
            notnull = True, unique = False,
            widget = stv_widget_input, label = 'Clave Prod./Serv.', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        # El campo prodserv_id debería ser el mismo que empresa_prodserv_id.prodserv_id;
        #  sin embargo se deja este campo para
        #  posible problema entre la factura y el cambio en el prodserv relacionado al producto
        Field(
            'prodserv_id', 'integer', default = None,
            required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tprodservs.id', db01.tprodservs._format)),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_search(
                f, v, s_display = '%(descripcion)s [%(c_claveprodserv)s]',
                s_url = URL(a = 'app_generales', c = '005genericos_sat', f = 'prodservs_buscar'),
                D_additionalAttributes = {'reference': db01.tprodservs.id}
                ),
            label = 'Producto o Servicio', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tcfdirx_conceptos.prodserv_id)
            ),
        Field(
            'claveunidad', 'string', length = 30, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Clave Unidad', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'unidad', 'string', length = 100, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Unidad', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'unidad_id', 'integer', default = None,
            required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tunidades.id', db01.tunidades._format)),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_search(
                f, v, s_display = '%(nombre)s [%(c_claveunidad)s]',
                s_url = URL(
                    a = 'app_generales',
                    c = '005genericos_sat',
                    f = 'unidades_buscar'
                    ),
                D_additionalAttributes = {'reference': db01.tunidades.id}
                ),
            label = 'Unidad', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(
                v, r, dbc01.tcfdirx_conceptos.unidad_id
                )
            ),
        Field(
            'descripcion', 'string', length = 150, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Descripción', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'descuento', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Descuento', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        Field(
            'importe', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Importe', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),

        Field(
            'noidentificacion', 'string', length = 30, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'No. Ident.', comment = 'Código de la empresa para el producto',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'empresa_prodserv_id', 'integer', default = None,
            required = False,
            requires = IS_NULL_OR(IS_IN_DB(dbc01, 'tempresa_prodserv.id', dbc01.tempresa_prodserv._format)),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Emp. Producto/Servicio', comment = '',
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(
                v, r, dbc01.tcfdirx_conceptos.empresa_prodserv_id
                )
            ),

        Field(
            'valorunitario', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Valor Unitario', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),

        # Campos usados para determinar la cantidad ya ingresada a almacenes en caso de CFDIs de ingreso
        # En caso de requerir saber en que almacenes se ingresó, ver tabla tempresa_invmovto_productos
        Field(
            'cantidad_ingresada', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputFloat, label = 'Cantidad Ingresada', comment = None,
            writable = False, readable = True,
            represent = stv_represent_float
            ),

        tSignature_dbc01,
        ]

    def __init__(self, dbRef, tablename, *fields, **args):
        Table.__init__(self, dbRef, tablename, *fields, **args)
        self.PROC.dbTabla = self
        return

    class PROC(TCFDISRX.PROC_DETALLE, object):

        dbTabla = None  # Se escribe con la inicialización

        def puede_crear(self, x_cfdirx, **D_defaults):
            """ Determina si puede o no crear un CFDI de traslado.

            Actualmente no hay condición requerida.
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx, dbc01.tcfdis)
            _dbRow = _D_results.dbRow

            _D_results = super(TCFDIRX_CONCEPTOS.PROC, self).puede_crear(
                x_cfdirx = _dbRow,
                D_requeridosMaestro = Storage(
                    tipocomprobante_id = Storage(
                        L_valores = [
                            TTIPOSCOMPROBANTES.INGRESO, TTIPOSCOMPROBANTES.EGRESO, TTIPOSCOMPROBANTES.TRASLADO
                            ],  # PAGO no permite creación manual
                        s_msgError = "Tipo Comprobante debe ser Ingreso, Egreso o Traslado"
                        )
                    ),
                **D_defaults
                )
            _D_return.combinar(_D_results)

            return _D_return

        @classmethod
        def CONFIGURACAMPOS_DESDE_dbRow(cls, dbRow_base):
            cls.dbTabla.cfdi_id.default = dbRow_base.cfdi_id
            cls.dbTabla.cantidad.default = dbRow_base.cantidad
            # cls.dbTabla.claveprodserv.default = _dbRow_base.claveprodserv
            # cls.dbTabla.prodserv_id.default = _dbRow_base.prodserv_id
            # cls.dbTabla.claveunidad.default = _dbRow_base.claveunidad
            # cls.dbTabla.unidad.default = _dbRow_base.unidad
            cls.dbTabla.unidad_id.default = dbRow_base.unidad_id
            cls.dbTabla.descripcion.default = dbRow_base.descripcion
            # cls.dbTabla.descuento.default = dbRow_base.descuento
            # cls.dbTabla.importe.default = dbRow_base.importe
            # cls.dbTabla.noidentificacion.default = dbRow_base.noidentificacion
            cls.dbTabla.empresa_prodserv_id.default = dbRow_base.empresa_prodserv_id
            # cls.dbTabla.valorunitario.default = dbRow_base.valorunitario
            # cls.dbTabla.valorunitario_capturado.default = dbRow_base.valorunitario_capturado
            # cls.dbTabla.cantidad_ingresada.default = dbRow_base.cantidad_ingresada
            # cls.dbTabla.cfdi_concepto_complemento_id.default = dbRow_base.cfdi_concepto_complemento_id
            # cls.dbTabla.aplicacasco_1a1.default = dbRow_base.aplicacasco_1a1
            return

        def configuracampos_nuevo(
                self,
                s_cfdirx_concepto_id,
                b_llamadaDesdeEditar = False,
                **D_defaults
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param b_llamadaDesdeEditar:
            @type b_llamadaDesdeEditar:
            @param s_cfdirx_concepto_id:
            @type s_cfdirx_concepto_id:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = super(TCFDIRX_CONCEPTOS.PROC, self).configuracampos_nuevo(
                s_cfdirx_detalle_id = s_cfdirx_concepto_id,
                b_llamadaDesdeEditar = b_llamadaDesdeEditar,
                **D_defaults
                )
            _D_return.combinar(_D_results)

            self.dbTabla.cfdi_id.writable = False
            self.dbTabla.orden.writable = True
            self.dbTabla.cantidad.writable = True
            self.dbTabla.empresa_prodserv_id.writable = True
            self.dbTabla.valorunitario.writable = False

            # Campos que no deben ser editados, solo de forma autocalculada
            self.dbTabla.claveprodserv.writable = False
            self.dbTabla.prodserv_id.writable = False
            self.dbTabla.unidad_id.writable = False
            self.dbTabla.descuento.writable = False
            self.dbTabla.claveunidad.writable = False
            self.dbTabla.unidad.writable = False
            self.dbTabla.descripcion.writable = False
            self.dbTabla.importe.writable = False
            self.dbTabla.noidentificacion.writable = False
            self.dbTabla.cantidad_ingresada.writable = False

            # Campos que todos los movimientos pueden editar
            self.dbTabla.notas.writable = True

            # Se definen los valores por default obligados
            self.dbTabla.cfdi_id.default = self._D_defaults.cfdi_id
            _n_siguienteOrden = (dbc01(dbc01.tcfdirx_conceptos.cfdi_id == self.dbTabla.cfdi_id.default).count() + 1) \
                                * 100
            self.dbTabla.orden.default = self._D_defaults.orden if self._D_defaults.orden else _n_siguienteOrden

            return _D_return

        def al_validar(
                self,
                O_form,
                **D_params
                ):
            """ Validar la información para poder grabar cfdi

            Las validaciones en esta función son genéricas, se espera que existan validaciones específicas por
             tipo de CFDI

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _dbRow = self.dbTabla(O_form.record_id) if O_form.record_id else None
            _D_camposAChecar = STV_FWK_LIB.OBTENER_CAMPOSACHECAR_PARA_VALIDAR(self.dbTabla, _dbRow)

            # Se llaman las validaciones del CFDI_relacionado, indicando que tiporelacion_id debe existsir
            O_form = super(TCFDIRX_CONCEPTOS.PROC, self).al_validar(
                O_form, _dbRow, D_camposAChecar = _D_camposAChecar
                )

            # Validaciones

            if not _D_camposAChecar.empresa_prodserv_id:
                O_form.errors.empresa_prodserv_id = "Producto o Servicio debe especificarse"
            else:
                pass

            return O_form

        @classmethod
        def AL_ACEPTAR(
                cls,
                O_form,
                **D_params
                ):
            """ Al aceptar el registro deben hacerse los cálculos

            Se hacen todos los cálculos y aceptación de los cambios en el producto

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()
            _dbRow = cls.dbTabla(O_form.record_id or O_form.vars.id)

            _D_results = cls.MAPEAR_CAMPOS(_dbRow)

            if _D_results.E_return > stvfwk2_e_RETURN.OK_WARNING:
                _D_return.combinar(_D_results)

            elif not O_form.record_id:
                # Si se esta insertando, en layers anteriores debería de prevenirse
                pass

            else:
                # Si no esta insertando
                pass

            _D_results = cls.CALCULAR_CAMPOS(_dbRow)
            _D_return.combinar(_D_results)

            generaMensajeFlash(cls.dbTabla, _D_return)
            return O_form

        @classmethod
        def MAPEAR_CAMPOS(
                cls,
                x_cfdirx_concepto,
                **D_params
                ):
            """ Remapea campos, haciendo la consulta de la carta porte a partir del CFDI

            @param x_cfdirx_concepto:
            @type x_cfdirx_concepto:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx_concepto, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage(
                empresa_prodserv_id = None,
                )

            _dbRow_cfdi = dbc01.tcfdis(_dbRow.cfdi_id)

            if _dbRow.noidentificacion:
                _D_returnsData = STV_LIB_DB.ASOCIAR_TABLA(
                    dbTabla = dbc01.tempresa_prodserv_codigosproveedores,
                    s_nombreCampo = 'codigo',
                    s_datoBuscar = _dbRow.noidentificacion,
                    qry_maestro = (dbc01.tempresa_prodserv_codigosproveedores.empresa_proveedor_id
                                   == _dbRow_cfdi.emisorproveedor_id),
                    s_nombreCampoId = 'empresa_prodserv_id'
                    )
            else:
                # Si el proveedor no tiene identificado el noidentificacion, se procede a
                #  buscar por descripción
                _D_returnsData = STV_LIB_DB.ASOCIAR_TABLA(
                    dbTabla = dbc01.tempresa_prodserv_codigosproveedores,
                    s_nombreCampo = 'codigo',
                    s_datoBuscar = _dbRow.descripcion,
                    qry_maestro = (dbc01.tempresa_prodserv_codigosproveedores.empresa_proveedor_id
                                   == _dbRow_cfdi.emisorproveedor_id),
                    s_nombreCampoId = 'empresa_prodserv_id'
                    )

            if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                _D_actualizar.empresa_prodserv_id = _D_returnsData.n_id

            else:
                # Si no se a asociado, se busca por el codigo del producto
                _D_returnsData = STV_LIB_DB.ASOCIAR_TABLA(
                    dbTabla = dbc01.tempresa_prodserv,
                    s_nombreCampo = 'codigo',
                    s_datoBuscar = _dbRow.noidentificacion,
                    qry_maestro = (dbc01.tempresa_prodserv.empresa_id == _dbRow_cfdi._s_empresa_id),
                    )

                if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                    _D_actualizar.empresa_prodserv_id = _D_returnsData.n_id

                else:
                    _D_return.agrega_error("Producto %s no encontrado. \n" % _dbRow.noidentificacion)

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)
                    break
                else:
                    pass

            return _D_return

        @classmethod
        def CALCULAR_CAMPOS(
                cls,
                x_cfdirx_concepto,
                **D_params
                ):
            """ Recalcula campos

            @param x_cfdirx_concepto:
            @type x_cfdirx_concepto:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx_concepto, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage()

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)

                    break
                else:
                    pass

            return _D_return

        pass  # PROC

    class PROC_DETALLE:

        dbTabla = None  # Se escribe con la inicialización

        def __init__(self, **D_defaults):
            """ Inicializa objecto de tipo PROC para control del registro actual

            @param D_defaults:
            @type D_defaults:
            """
            self._D_defaults = Storage(D_defaults)
            self._L_requeridos = ['cfdi_concepto_id']
            return

        def puede_crear(self, x_cfdirx_concepto, **D_defaults):
            """ Determina si puede o no crear un CFDI de traslado.

            Actualmente no hay condición requerida.
            """
            _D_return = FUNC_RETURN()

            self._D_defaults.update(D_defaults or {})

            for _s_requerido in self._L_requeridos:
                if _s_requerido not in self._D_defaults:
                    _D_return.agrega_error("Campo %s es requerido para generar CFDI" % _s_requerido)

                else:
                    pass

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx_concepto, dbc01.tcfdirx_conceptos)
            _dbRow_cfdirx_concepto = _D_results.dbRow

            _D_results = TCFDIRX_CONCEPTOS.PROC.PUEDE_EDITAR(_dbRow_cfdirx_concepto)
            _D_return.combinar(_D_results)

            return _D_return

        @classmethod
        def PUEDE_EDITAR(
                cls,
                x_cfdirx_concepto_detalle
                ):
            """ Define si es posible editar el CFDI de traslado.
            @param x_cfdirx_concepto_detalle:

            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx_concepto_detalle, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_results = TCFDIRX_CONCEPTOS.PROC.PUEDE_EDITAR(_dbRow.cfdi_concepto_id)
            _D_return.combinar(_D_results)

            return _D_return

        @classmethod
        def PUEDE_ELIMINAR(cls, x_cfdirx_concepto_detalle):
            """ Determina si puede o no eliminar un registro

            Misma condición que puede_editar, a lo mejor pudieran verse condiciones distintas

            """

            return cls.PUEDE_EDITAR(x_cfdirx_concepto_detalle)

        @classmethod
        def CONFIGURACAMPOS_DESDE_dbRow(cls, dbRow_base):
            # cls.dbTabla.cfdi_concepto_id.default = dbRow_base.cfdi_concepto_id
            # cls.dbTabla.campo1.default = dbRow_base.campo1
            # cls.dbTabla.campo2.default = dbRow_base.campo2
            return

        def configuracampos_nuevo(
                self,
                s_cfdirx_concepto_detalle_id,
                b_llamadaDesdeEditar = False,
                **D_defaults
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param b_llamadaDesdeEditar: Indica si la llamada a esta función es desde otra capa,
             desactivando validación de puede_crear
            @type b_llamadaDesdeEditar:
            @param s_cfdirx_concepto_detalle_id:
            @type s_cfdirx_concepto_detalle_id:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            self._D_defaults.update(D_defaults)
            for _s_fieldName in self.dbTabla.fields:
                self.dbTabla[_s_fieldName].writable = False
            for _s_fieldName in self._D_defaults:
                self.dbTabla[_s_fieldName].default = self._D_defaults[_s_fieldName]

            if not b_llamadaDesdeEditar:
                _D_results = self.puede_crear(
                    x_cfdirx_concepto = self._D_defaults.cfdi_concepto_id
                    )
            else:
                _D_results = _D_return

            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.combinar(_D_results)

            elif s_cfdirx_concepto_detalle_id:
                _dbRow_base = self.dbTabla(s_cfdirx_concepto_detalle_id)

                self.CONFIGURACAMPOS_DESDE_dbRow(_dbRow_base)

            else:
                pass

            self.dbTabla.cfdi_concepto_id.writable = False

            # Campos que no deben ser editados, solo de forma autocalculada

            # Campos que todos los movimientos pueden editar
            self.dbTabla.notas.writable = False

            # Se definen los valores por default obligados
            self.dbTabla.cfdi_concepto_id.default = self._D_defaults.cfdi_concepto_id

            return _D_return

        def configuracampos_edicion(self, x_cfdirx_concepto_detalle):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param x_cfdirx_concepto_detalle:
            @type x_cfdirx_concepto_detalle:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx_concepto_detalle, self.dbTabla)
            _dbRow = _D_results.dbRow

            # Se llenan los default obligados
            for _s_nombreCampo in self._D_defaults:
                self._D_defaults[_s_nombreCampo] = _D_results.dbRow[_s_nombreCampo]

            _D_results = self.PUEDE_EDITAR(_dbRow)

            if _D_results.E_return == CLASS_e_RETURN.OK:
                # Si puede editar regresa un ok y el movimiento esta abierto

                # Se manda vacío el argumento para no agregar defaults a los campos
                _D_results = self.configuracampos_nuevo(
                    None,
                    b_llamadaDesdeEditar = True,
                    **self._D_defaults
                    )
                _D_return.combinar(_D_results)

            else:
                # De lo contrario en cualquier otro caso, no puede editar nada de esta tabla
                for _s_fieldName in self.dbTabla.fields:
                    self.dbTabla[_s_fieldName].writable = False
                _D_return.agrega(CLASS_e_RETURN.OK_WARNING, _D_results.s_msgError, "No puede editar")

            return _D_return

        def al_validar(self, O_form, dbRow = None, D_camposAChecar = None):
            """ Validar la información para poder grabar cfdi

            Las validaciones en esta función son genéricas, se espera que existan validaciones
             específicas por tipo de CFDI

            @param dbRow:
            @type dbRow:
            @param D_camposAChecar:
            @type D_camposAChecar:
            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            dbRow = dbRow if dbRow else self.dbTabla(O_form.record_id) if O_form.record_id else None
            D_camposAChecar = D_camposAChecar if D_camposAChecar \
                else STV_FWK_LIB.OBTENER_CAMPOSACHECAR_PARA_VALIDAR(self.dbTabla, dbRow)

            if not O_form.record_id:
                # Se esta creando el registro

                _D_results = self.puede_crear(
                    x_cfdi_concepto = D_camposAChecar.cfdi_concepto_id,
                    **D_camposAChecar
                    )
                if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                    O_form.errors.id = _D_results.s_msgError
                else:
                    pass

            else:
                # Se esta modificando el registro, la llamada a configuraCampos_edicion ya definió con writable
                # que puede modificarse y que no.

                _D_results = self.PUEDE_EDITAR(dbRow)
                if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                    O_form.errors.id = _D_results.s_msgError
                else:
                    pass

            # Validaciones

            # if not _D_camposAChecar.campo1:
            #     O_form.errors.campo1 = "Campo1 no puede estar vacío"
            # else:
            #     pass

            return O_form

        @classmethod
        def AL_ACEPTAR(cls, O_form, dbRow = None, b_calcularCampos = True):
            """ Al aceptar el registro deben hacerse los cálculos

            Se hacen todos los cálculos y aceptación de los cambios en el producto

            @param b_calcularCampos: determina si se realza el cálculo de los campos autocalculados.
             Se utiliza para prevenir doblde recálculo de los campos, cuando se están insertando automaticamente.
            @type b_calcularCampos:
            @param dbRow:
            @type dbRow:
            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()
            dbRow = dbRow if dbRow else cls.dbTabla(O_form.record_id or O_form.vars.id)

            if not O_form.record_id:
                # Si se inserta
                pass

            else:  # Si se edita
                pass

            _D_results = cls.MAPEAR_CAMPOS(dbRow)
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.combinar(_D_results)

            elif b_calcularCampos:
                _D_results = cls.CALCULAR_CAMPOS(dbRow)
                _D_return.combinar(_D_results)

            else:
                pass

            generaMensajeFlash(cls.dbTabla, _D_return)
            return O_form

        @classmethod
        def ANTES_ELIMINAR(cls, L_errorsAction, dbRow):
            """ Antes de eliminar un movimiento de inventario de este tipo

            @param dbRow:
            @type dbRow:
            @param L_errorsAction:
            @type L_errorsAction:
            @return: errores
            @rtype: lista de diccionarios {'msg':'...'}
            """
            _D_return = FUNC_RETURN()

            _D_results = cls.PUEDE_ELIMINAR(dbRow)
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                L_errorsAction.append({'msg': _D_results.s_msgError})
            else:
                cls._dbRow_aEliminar = Storage(dbRow.as_dict())

            return L_errorsAction

        @classmethod
        def DESPUES_ELIMINAR(cls, L_errorsAction):
            """ Después de eliminar un registro

            @param L_errorsAction:
            @type L_errorsAction:
            @return: errores
            @rtype: lista de diccionarios {'msg':'...'}
            """
            _D_return = FUNC_RETURN()

            if cls._dbRow_aEliminar:
                # Aunque es detalle de concepto, modifica el cfdi y no el concepto
                _dbRow_cfdi = dbc01(
                    (dbc01.tcfdirx_conceptos.id == cls._dbRow_aEliminar.cfdi_concepto_id)
                    & (dbc01.tcfdisrx.id == dbc01.tcfdirx_conceptos.cfdi_id)
                    ).select(
                    dbc01.tcfdisrx.id, dbc01.tcfdisrx.tipocomprobante_id
                    ).first()

                if _dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.EGRESO:
                    _D_results = TCFDIRX_CONCEPTOS.EGRESO.CALCULAR_CAMPOS(
                        x_cfdirx_concepto = cls._dbRow_aEliminar.cfdi_concepto_id
                        )
                elif _dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO:
                    _D_results = TCFDIRX_CONCEPTOS.INGRESO.CALCULAR_CAMPOS(
                        x_cfdirx_concepto = cls._dbRow_aEliminar.cfdi_concepto_id
                        )
                else:
                    _D_results = TCFDIRX_CONCEPTOS.PROC.CALCULAR_CAMPOS(
                        x_cfdirx_concepto = cls._dbRow_aEliminar.cfdi_concepto_id
                        )

                _D_return.combinar(_D_results)
            else:
                _D_return.E_return = CLASS_e_RETURN.NOK_SECUENCIA
                _D_return.s_msgError += "Error en secuencia de llamado de funciones para borrado. "

            generaMensajeFlash(cls.dbTabla, _D_return)
            return L_errorsAction

        @classmethod
        def MAPEAR_CAMPOS(cls, x_cfdirx_concepto_detalle, b_actualizarRegistro = True):
            """ Mapea campos, llenando campos automáticos

            @param b_actualizarRegistro:
            @type b_actualizarRegistro:
            @param x_cfdirx_concepto_detalle:
            @type x_cfdirx_concepto_detalle:
            @return:
            @rtype:
            """

            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx_concepto_detalle, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage()

            if b_actualizarRegistro:
                for _s_nombreCampo in _D_actualizar:
                    if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                        _dbRow.update_record(**_D_actualizar)
                        break
                    else:
                        pass
            else:
                # Si no se desea actualizar el registro, se manda el diccionario con los valores calculados
                _D_return.D_actualizar = _D_actualizar

            return _D_return

        @classmethod
        def CALCULAR_CAMPOS(cls, x_cfdirx_concepto_detalle, b_recalcularCFDI = True):
            """ Calcula campos, llenando campos automáticos que tienen dependencia númerica de otras tablas

            @param b_recalcularCFDI:
            @type b_recalcularCFDI:
            @param x_cfdirx_concepto_detalle:
            @type x_cfdirx_concepto_detalle:
            @return:
            @rtype:
            """

            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx_concepto_detalle, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage()

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)

                    if b_recalcularCFDI:
                        _dbRow_concepto = dbc01.tcfdirx_conceptos(_dbRow.cfdi_concepto_id)
                        # Si hay actualizaciones, debe actualizarse el CFDI, no los conceptos
                        _D_results = TCFDISRX.PROC.CALCULAR_CAMPOS(
                            x_cfdirx = _dbRow_concepto.cfdi_id
                            )
                    else:
                        pass
                    break
                else:
                    pass

            return _D_return

        pass  # PROC_DETALLE

    pass  # TCFDIRX_CONCEPTOS


dbc01.define_table(
    TCFDIRX_CONCEPTOS.S_NOMBRETABLA, *TCFDIRX_CONCEPTOS.L_DBCAMPOS,
    format = '%(descripcion)s',
    singular = 'CFDI concepto',
    plural = 'CFDI conceptos',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TCFDIRX_CONCEPTOS
    )


class TCFDIRX_CONCEPTO_IMPUESTOSTRASLADADOS(Table):
    S_NOMBRETABLA = 'tcfdirx_concepto_impuestostrasladados'
    E_TIPOFACTOR = TEMPRESA_ESQUEMASIMPUESTOSTRASLADADOS.E_TIPOFACTOR
    L_DBCAMPOS = [
        Field(
            'cfdi_concepto_id', dbc01.tcfdirx_conceptos, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'CFDI Concepto', comment = None,
            writable = False, readable = False,
            represent = None
            ),
        Field(
            'base', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Base', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),

        Field(
            'impuesto', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Impuesto', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'impuesto_id', 'integer', default = None,
            required = True, requires = IS_IN_DB(db01, 'timpuestos.id', db01.timpuestos._format),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Impuesto', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(
                v, r, dbc01.tcfdirx_concepto_impuestostrasladados.impuesto_id
                )
            ),
        Field(
            'tipofactor', 'string', length = 10, default = E_TIPOFACTOR.TASA,
            required = True,
            requires = IS_IN_SET(E_TIPOFACTOR.get_dict(), zero = 0, error_message = 'Selecciona'),
            notnull = False, unique = False,
            widget = stv_widget_combobox, label = 'Tipo Factor', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_list(
                v, r, fnLambda = None,
                D_data = TCFDIRX_CONCEPTO_IMPUESTOSTRASLADADOS.E_TIPOFACTOR.get_dict(),
                dbField = dbc01.tcfdirx_concepto_impuestostrasladados.tipofactor
                )
            ),
        Field(
            'tasaocuota', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputFloat, label = 'Tasa o Cuota', comment = None,
            writable = True, readable = True,
            represent = stv_represent_float
            ),
        Field(
            'importe', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Importe', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        tSignature_dbc01
        ]

    def __init__(self, dbRef, tablename, *fields, **args):
        Table.__init__(self, dbRef, tablename, *fields, **args)
        self.PROC.dbTabla = self
        return

    class PROC(TCFDIRX_CONCEPTOS.PROC_DETALLE, object):

        dbTabla = None  # Se escribe con la inicialización

        @classmethod
        def CONFIGURACAMPOS_DESDE_dbRow(cls, dbRow_base):
            # cls.dbTabla.cfdi_concepto_id.default = dbRow_base.cfdi_concepto_id
            # cls.dbTabla.base.default = dbRow_base.base
            # cls.dbTabla.impuesto.default = dbRow_base.impuesto
            cls.dbTabla.impuesto_id.default = dbRow_base.impuesto_id
            cls.dbTabla.tipofactor.default = dbRow_base.tipofactor
            cls.dbTabla.tasaocuota.default = dbRow_base.tasaocuota
            # cls.dbTabla.importe.default = dbRow_base.importe
            return

        def configuracampos_nuevo(
                self,
                s_cfdirx_concepto_imptras_id,
                b_llamadaDesdeEditar = False,
                **D_defaults
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param b_llamadaDesdeEditar: Indica si la llamada a esta función es desde otra capa,
             desactivando validación de puede_crear
            @type b_llamadaDesdeEditar:
            @param s_cfdirx_concepto_imptras_id:
            @type s_cfdirx_concepto_imptras_id:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = super(TCFDIRX_CONCEPTO_IMPUESTOSTRASLADADOS.PROC, self).configuracampos_nuevo(
                s_cfdirx_concepto_detalle_id = s_cfdirx_concepto_imptras_id,
                b_llamadaDesdeEditar = b_llamadaDesdeEditar,
                **D_defaults
                )
            _D_return.combinar(_D_results)

            self.dbTabla.cfdi_concepto_id.writable = False
            self.dbTabla.impuesto_id.writable = True
            self.dbTabla.tasaocuota.writable = True
            self.dbTabla.tipofactor.writable = True

            # Campos que no deben ser editados, solo de forma autocalculada
            self.dbTabla.base.writable = False
            self.dbTabla.impuesto.writable = False
            self.dbTabla.importe.writable = False

            # Campos que todos los movimientos pueden editar
            self.dbTabla.notas.writable = False

            # Se definen los valores por default obligados
            self.dbTabla.cfdi_concepto_id.default = self._D_defaults.cfdi_concepto_id

            return _D_return

        def al_validar(
                self,
                O_form,
                **D_params
                ):
            """ Validar la información para poder grabar cfdi

            Las validaciones en esta función son genéricas, se espera que existan validaciones
             específicas por tipo de CFDI

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _dbRow = self.dbTabla(O_form.record_id) if O_form.record_id else None
            _D_camposAChecar = STV_FWK_LIB.OBTENER_CAMPOSACHECAR_PARA_VALIDAR(self.dbTabla, _dbRow)

            # Se llaman las validaciones del CFDI_relacionado, indicando que tiporelacion_id debe existsir
            O_form = super(TCFDIRX_CONCEPTO_IMPUESTOSTRASLADADOS.PROC, self).al_validar(
                O_form, _dbRow, D_camposAChecar = _D_camposAChecar
                )

            # Validaciones
            if not _D_camposAChecar.impuesto_id:
                O_form.errors.impuesto_id = "Impuesto no puede estar vacío"
            else:
                pass

            if not _D_camposAChecar.tasaocuota:
                O_form.errors.tasaocuota = "Tasa/Cuota no puede estar vacío"
            else:
                pass

            return O_form

        @classmethod
        def DESPUES_ELIMINAR(cls, L_errorsAction):
            """ Después de eliminar un registro

            @param L_errorsAction:
            @type L_errorsAction:
            @return: errores
            @rtype: lista de diccionarios {'msg':'...'}
            """
            _D_return = FUNC_RETURN()

            if cls._dbRow_aEliminar:
                # Aunque es detalle de concepto, modifica el cfdi y no el concepto
                _dbRow_concepto = dbc01.tcfdirx_conceptos(cls._dbRow_aEliminar.cfdi_concepto_id)
                _D_results = TCFDISRX.PROC.CALCULAR_CAMPOS(
                    x_cfdirx = _dbRow_concepto.cfdi_id
                    )
                _D_return.combinar(_D_results)
            else:
                _D_return.E_return = CLASS_e_RETURN.NOK_SECUENCIA
                _D_return.s_msgError += "Error en secuencia de llamado de funciones para borrado. "

            generaMensajeFlash(cls.dbTabla, _D_return)
            return L_errorsAction

        @classmethod
        def MAPEAR_CAMPOS(cls, x_cfdirx_concepto_imptras, b_actualizarRegistro = True):
            """ Mapea campos, llenando campos automáticos

            @param b_actualizarRegistro:
            @type b_actualizarRegistro:
            @param x_cfdirx_concepto_imptras:
            @type x_cfdirx_concepto_imptras:
            @return:
            @rtype:
            """

            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx_concepto_imptras, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage()

            if b_actualizarRegistro:
                for _s_nombreCampo in _D_actualizar:
                    if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                        _dbRow.update_record(**_D_actualizar)
                        break
                    else:
                        pass
            else:
                # Si no se desea actualizar el registro, se manda el diccionario con los valores calculados
                _D_return.D_actualizar = _D_actualizar

            return _D_return

        @classmethod
        def CALCULAR_CAMPOS(cls, x_cfdirx_concepto_imptras, b_recalcularCFDI = True):
            """ Calcula campos, llenando campos automáticos que tienen dependencia númerica de otras tablas

            @param b_recalcularCFDI:
            @type b_recalcularCFDI:
            @param x_cfdirx_concepto_imptras:
            @type x_cfdirx_concepto_imptras:
            @return:
            @rtype:
            """

            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx_concepto_imptras, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage()

            _dbRow_concepto = dbc01.tcfdirx_conceptos(_dbRow.cfdi_concepto_id)

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)

                    if b_recalcularCFDI:
                        # Si hay actualizaciones, debe actualizarse el CFDI, no los conceptos
                        _D_results = TCFDISRX.PROC.CALCULAR_CAMPOS(
                            x_cfdirx = _dbRow_concepto.cfdi_id
                            )
                    else:
                        pass
                    break
                else:
                    pass

            return _D_return

        pass  # PROC

    pass  # TCFDIRX_CONCEPTO_IMPUESTOSTRASLADADOS


dbc01.define_table(
    TCFDIRX_CONCEPTO_IMPUESTOSTRASLADADOS.S_NOMBRETABLA, *TCFDIRX_CONCEPTO_IMPUESTOSTRASLADADOS.L_DBCAMPOS,
    format = '%(cfdi_concepto_id)s',
    singular = 'CFDI Concepto Impuesto Trasladado',
    plural = 'CFDI Concepto Impuestos Trasladados',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TCFDIRX_CONCEPTO_IMPUESTOSTRASLADADOS
    )


class TCFDIRX_CONCEPTO_IMPUESTOSRETENIDOS(Table):
    S_NOMBRETABLA = 'tcfdirx_concepto_impuestosretenidos'

    E_TIPOFACTOR = TEMPRESA_ESQUEMASIMPUESTOSTRASLADADOS.E_TIPOFACTOR

    L_DBCAMPOS = [
        Field(
            'cfdi_concepto_id', dbc01.tcfdirx_conceptos, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'CFDI Concepto', comment = None,
            writable = False, readable = False,
            represent = None
            ),
        Field(
            'base', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Base', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),

        Field(
            'impuesto', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Impuesto', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'impuesto_id', 'integer', default = None,
            required = True, requires = IS_IN_DB(db01, 'timpuestos.id', db01.timpuestos._format),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Impuesto', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(
                v, r, dbc01.tcfdirx_concepto_impuestostrasladados.impuesto_id
                )
            ),
        Field(
            'tipofactor', 'string', length = 10, default = E_TIPOFACTOR.TASA,
            required = True,
            requires = IS_IN_SET(E_TIPOFACTOR.get_dict(), zero = 0, error_message = 'Selecciona'),
            notnull = False, unique = False,
            widget = stv_widget_combobox, label = 'Tipo Factor', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_list(
                v, r, fnLambda = None,
                D_data = TCFDIRX_CONCEPTO_IMPUESTOSTRASLADADOS.E_TIPOFACTOR.get_dict(),
                dbField = dbc01.tcfdirx_concepto_impuestostrasladados.tipofactor
                )
            ),
        Field(
            'tasaocuota', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputFloat, label = 'Tasa o Cuota', comment = None,
            writable = True, readable = True,
            represent = stv_represent_float
            ),
        Field(
            'importe', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Importe', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        tSignature_dbc01
        ]

    def __init__(self, dbRef, tablename, *fields, **args):
        Table.__init__(self, dbRef, tablename, *fields, **args)
        self.PROC.dbTabla = self
        return

    class PROC(TCFDIRX_CONCEPTOS.PROC_DETALLE, object):

        dbTabla = None  # Se escribe con la inicialización

        @classmethod
        def CONFIGURACAMPOS_DESDE_dbRow(cls, dbRow_base):
            # cls.dbTabla.cfdi_concepto_id.default = dbRow_base.cfdi_concepto_id
            # cls.dbTabla.base.default = dbRow_base.base
            # cls.dbTabla.impuesto.default = dbRow_base.impuesto
            cls.dbTabla.impuesto_id.default = dbRow_base.impuesto_id
            cls.dbTabla.tipofactor.default = dbRow_base.tipofactor
            cls.dbTabla.tasaocuota.default = dbRow_base.tasaocuota
            # cls.dbTabla.importe.default = dbRow_base.importe
            return

        def configuracampos_nuevo(
                self,
                s_cfdirx_concepto_impret_id,
                b_llamadaDesdeEditar = False,
                **D_defaults
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param b_llamadaDesdeEditar: Indica si la llamada a esta función es desde otra capa,
             desactivando validación de puede_crear
            @type b_llamadaDesdeEditar:
            @param s_cfdirx_concepto_impret_id:
            @type s_cfdirx_concepto_impret_id:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = super(TCFDIRX_CONCEPTO_IMPUESTOSRETENIDOS.PROC, self).configuracampos_nuevo(
                s_cfdirx_concepto_detalle_id = s_cfdirx_concepto_impret_id,
                b_llamadaDesdeEditar = b_llamadaDesdeEditar,
                **D_defaults
                )
            _D_return.combinar(_D_results)

            self.dbTabla.cfdi_concepto_id.writable = False
            self.dbTabla.impuesto_id.writable = True
            self.dbTabla.tasaocuota.writable = True
            self.dbTabla.tipofactor.writable = True

            # Campos que no deben ser editados, solo de forma autocalculada
            self.dbTabla.base.writable = False
            self.dbTabla.impuesto.writable = False
            self.dbTabla.importe.writable = False

            # Campos que todos los movimientos pueden editar
            self.dbTabla.notas.writable = False

            # Se definen los valores por default obligados
            self.dbTabla.cfdi_concepto_id.default = self._D_defaults.cfdi_concepto_id

            return _D_return

        def al_validar(
                self,
                O_form,
                **D_params
                ):
            """ Validar la información para poder grabar cfdi

            Las validaciones en esta función son genéricas, se espera que existan validaciones
             específicas por tipo de CFDI

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _dbRow = self.dbTabla(O_form.record_id) if O_form.record_id else None
            _D_camposAChecar = STV_FWK_LIB.OBTENER_CAMPOSACHECAR_PARA_VALIDAR(self.dbTabla, _dbRow)

            # Se llaman las validaciones del CFDI_relacionado, indicando que tiporelacion_id debe existsir
            O_form = super(TCFDIRX_CONCEPTO_IMPUESTOSRETENIDOS.PROC, self).al_validar(
                O_form, _dbRow, D_camposAChecar = _D_camposAChecar
                )

            # Validaciones
            if not _D_camposAChecar.impuesto_id:
                O_form.errors.impuesto_id = "Impuesto no puede estar vacío"
            else:
                pass

            if not _D_camposAChecar.tasaocuota:
                O_form.errors.tasaocuota = "Tasa/Cuota no puede estar vacío"
            else:
                pass

            return O_form

        @classmethod
        def DESPUES_ELIMINAR(cls, L_errorsAction):
            """ Después de eliminar un registro

            @param L_errorsAction:
            @type L_errorsAction:
            @return: errores
            @rtype: lista de diccionarios {'msg':'...'}
            """
            _D_return = FUNC_RETURN()

            if cls._dbRow_aEliminar:
                # Aunque es detalle de concepto, modifica el cfdi y no el concepto
                _dbRow_concepto = dbc01.tcfdirx_conceptos(cls._dbRow_aEliminar.cfdi_concepto_id)
                _D_results = TCFDISRX.PROC.CALCULAR_CAMPOS(
                    x_cfdirx = _dbRow_concepto.cfdi_id
                    )
                _D_return.combinar(_D_results)
            else:
                _D_return.E_return = CLASS_e_RETURN.NOK_SECUENCIA
                _D_return.s_msgError += "Error en secuencia de llamado de funciones para borrado. "

            generaMensajeFlash(cls.dbTabla, _D_return)
            return L_errorsAction

        @classmethod
        def MAPEAR_CAMPOS(cls, x_cfdirx_concepto_impret, b_actualizarRegistro = True):
            """ Mapea campos, llenando campos automáticos

            @param b_actualizarRegistro:
            @type b_actualizarRegistro:
            @param x_cfdirx_concepto_impret:
            @type x_cfdirx_concepto_impret:
            @return:
            @rtype:
            """

            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx_concepto_impret, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage()

            if b_actualizarRegistro:
                for _s_nombreCampo in _D_actualizar:
                    if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                        _dbRow.update_record(**_D_actualizar)
                        break
                    else:
                        pass
            else:
                # Si no se desea actualizar el registro, se manda el diccionario con los valores calculados
                _D_return.D_actualizar = _D_actualizar

            return _D_return

        @classmethod
        def CALCULAR_CAMPOS(cls, x_cfdirx_concepto_impret, b_recalcularCFDI = True):
            """ Calcula campos, llenando campos automáticos que tienen dependencia númerica de otras tablas

            @param b_recalcularCFDI:
            @type b_recalcularCFDI:
            @param x_cfdirx_concepto_impret:
            @type x_cfdirx_concepto_impret:
            @return:
            @rtype:
            """

            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx_concepto_impret, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage()

            _dbRow_concepto = dbc01.tcfdirx_conceptos(_dbRow.cfdi_concepto_id)

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)

                    if b_recalcularCFDI:
                        # Si hay actualizaciones, debe actualizarse el CFDI, no los conceptos
                        _D_results = TCFDISRX.PROC.CALCULAR_CAMPOS(
                            x_cfdirx = _dbRow_concepto.cfdi_id
                            )
                    else:
                        pass
                    break
                else:
                    pass

            return _D_return

        pass  # PROC

    pass  # TCFDIRX_CONCEPTO_IMPUESTOSRETENIDOS


dbc01.define_table(
    TCFDIRX_CONCEPTO_IMPUESTOSRETENIDOS.S_NOMBRETABLA, *TCFDIRX_CONCEPTO_IMPUESTOSRETENIDOS.L_DBCAMPOS,
    format = '%(cfdi_concepto_id)s',
    singular = 'CFDI Concepto Impuesto Retenido',
    plural = 'CFDI Concepto Impuestos Retenciones',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TCFDIRX_CONCEPTO_IMPUESTOSRETENIDOS
    )


class TCFDIRX_COMPLEMENTOPAGOS(Table):
    S_NOMBRETABLA = 'tcfdirx_complementopagos'

    L_DBCAMPOS = [
        Field(
            'cfdi_id', dbc01.tcfdisrx, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'CFDI', comment = None,
            writable = False, readable = False,
            represent = None
            ),
        Field(
            'versionpago', 'string', length = 50, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Vesion', comment = '',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'fechapago_str', 'string', length = 50, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Fecha Pago', comment = '',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'fechapago', type = FIELD_UTC_DATETIME, default = None,
            # Se graba como la fecha-hora local de emisión del CFDI
            required = False, requires = IS_NULL_OR(IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME)),
            notnull = False,
            widget = stv_widget_inputDateTime, label = 'Fecha Pago', comment = '',
            writable = False, readable = True,
            represent = stv_represent_datetime
            ),
        Field(
            'formapago', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Forma Pago', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'formapago_id', 'integer', default = TFORMASPAGO.EFECTIVO,
            required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tformaspago.id', db01.tformaspago._format)),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Forma Pago', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tcfdirx_complementopagos.formapago_id)
            ),
        Field(
            'moneda', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Moneda', comment = '',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        TMONEDAS.DEFINIR_DBCAMPO(
            'moneda_id',
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tcfdisrx.moneda_id)
            ),

        # Este será el campo que se utilice en vez de moneda_id
        TEMPRESA_MONEDASCONTPAQI.DEFINIR_DBCAMPO(
            'monedacontpaqi_id',
            represent = lambda v, r: stv_represent_referencefield(
                v, r, dbc01.tcfdirx_complementopago_doctosrelacionados.monedacontpaqi_id
                )
            ),

        Field(
            'tipocambio', 'decimal(14,4)', default = 1,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Tipo de cambio', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        Field(
            'monto', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Monto', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),

        # Campo de apoyo para mostrar saldo pendiente por aplicar
        Field(
            'sumarelacionados', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Suma Relacionados', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_money_ifnotzero(
                v, r, dbField = dbc01.tcfdirx_complementopagos.sumarelacionados
                )
            ),
        tSignature_dbc01,
        ]

    def __init__(self, dbRef, tablename, *fields, **args):
        Table.__init__(self, dbRef, tablename, *fields, **args)
        self.PROC.dbTabla = self
        return

    @staticmethod
    def asociar_uuid(s_cfdirx_complementopago_id):
        """ Se busca y asocian ligas rotas en a CFDIs relacionados a los pagos
        """

        _D_return = Storage(
            L_msgError = [],
            E_return = CLASS_e_RETURN.NOK_ERROR,
            n_updatedRows = 0
            )

        # Ejemplo puede usar
        # tcfdirx_relacionados.id = 8419 de tcfdisrx.id = 47696
        # tcfdisrx.id = 47762

        _dbTableCFDIsBuscar = dbc01.tcfdisrx.with_alias('cfdisbuscar')
        _dbRows_cfdiRelacionados = dbc01(
            (dbc01.tcfdirx_complementopago_doctosrelacionados.cfdi_complementopago_id == s_cfdirx_complementopago_id)
            & (dbc01.tcfdirx_complementopago_doctosrelacionados.cfdirelacionado_id == None)
            & (
                dbc01.tcfdirx_complementopagos.id
                == dbc01.tcfdirx_complementopago_doctosrelacionados.cfdi_complementopago_id
                )
            & (dbc01.tcfdisrx.id == dbc01.tcfdirx_complementopagos.cfdi_id)
            & (_dbTableCFDIsBuscar.uuid == dbc01.tcfdirx_complementopago_doctosrelacionados.iddocumento)
            & (_dbTableCFDIsBuscar.empresa_id == dbc01.tcfdisrx.empresa_id)
            ).select(
                dbc01.tcfdirx_complementopago_doctosrelacionados.ALL,
                _dbTableCFDIsBuscar.id,
                )

        if len(_dbRows_cfdiRelacionados) == 0:
            # CFDI asociado con el UUID no se encontró
            _D_return.L_msgError += ["No se encontraron cfdis relacionados"]
            _D_return.E_return = CLASS_e_RETURN.OK
        else:
            # Se actualizarlan los documentos que requiren asignación
            for _dbRow in _dbRows_cfdiRelacionados:
                _D_return.n_updatedRows += dbc01(
                    dbc01.tcfdirx_complementopago_doctosrelacionados.id
                    == _dbRow.tcfdirx_complementopago_doctosrelacionados.id
                    ).update(cfdirelacionado_id = _dbRow.cfdisbuscar.id)
            _D_return.L_msgError += ["Se actualizo el registro con la relación al CFDI"]
            _D_return.E_return = CLASS_e_RETURN.OK

        return _D_return

    class PROC(TCFDISRX.PROC_DETALLE, object):

        dbTabla = None  # Se escribe con la inicialización

        def puede_crear(self, x_cfdirx, **D_defaults):
            """ Determina si puede o no crear un CFDI de traslado.

            Actualmente no hay condición requerida.
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx, dbc01.tcfdis)
            _dbRow_cfdi = _D_results.dbRow

            _D_results = super(TCFDIRX_COMPLEMENTOPAGOS.PROC, self).puede_crear(
                x_cfdi = _dbRow_cfdi,
                D_requeridosMaestro = Storage(
                    tipocomprobante_id = Storage(
                        L_valores = [TTIPOSCOMPROBANTES.PAGO],
                        s_msgError = "Tipo Comprobante debe ser Pago"
                        )
                    ),
                **D_defaults
                )
            _D_return.combinar(_D_results)

            return _D_return

        @classmethod
        def CONFIGURACAMPOS_DESDE_dbRow(cls, dbRow_base):
            # cls.dbTabla.cfdi_id.default = dbRow_base.cfdi_id
            # cls.dbTabla.versionpago.default = dbRow_base.versionpago
            # cls.dbTabla.fechapago_str.default = dbRow_base.fechapago_str
            cls.dbTabla.fechapago.default = dbRow_base.fechapago
            # cls.dbTabla.formapago.default = dbRow_base.formapago
            cls.dbTabla.formapago_id.default = dbRow_base.formapago_id
            # cls.dbTabla.moneda.default = dbRow_base.moneda
            # cls.dbTabla.moneda_id.default = dbRow_base.moneda_id
            cls.dbTabla.monedacontpaqi_id.default = dbRow_base.monedacontpaqi_id
            cls.dbTabla.tipocambio.default = dbRow_base.tipocambio
            # cls.dbTabla.monto.default = dbRow_base.monto
            # cls.dbTabla.sumarelacionados.default = dbRow_base.sumarelacionados

            return

        def configuracampos_nuevo(
                self,
                s_cfdirx_comppago_id,
                b_llamadaDesdeEditar = False,
                **D_defaults
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param b_llamadaDesdeEditar: Indica si la llamada a esta función es desde otra capa,
             desactivando validación de puede_crear
            @type b_llamadaDesdeEditar:
            @param s_cfdirx_comppago_id:
            @type s_cfdirx_comppago_id:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = super(TCFDIRX_COMPLEMENTOPAGOS.PROC, self).configuracampos_nuevo(
                s_cfdirx_detalle_id = s_cfdirx_comppago_id,
                b_llamadaDesdeEditar = b_llamadaDesdeEditar,
                **D_defaults
                )
            _D_return.combinar(_D_results)

            self.dbTabla.cfdi_id.writable = False
            self.dbTabla.fechapago.writable = True
            self.dbTabla.formapago_id.writable = True
            self.dbTabla.monedacontpaqi_id.writable = True
            self.dbTabla.tipocambio.writable = True
            self.dbTabla.monto.writable = True

            # Campos que no deben ser editados, solo de forma autocalculada
            self.dbTabla.versionpago.writable = False
            self.dbTabla.fechapago_str.writable = False
            self.dbTabla.formapago.writable = False
            self.dbTabla.moneda.writable = False
            self.dbTabla.moneda_id.writable = False
            self.dbTabla.sumarelacionados.writable = False

            # Campos que todos los movimientos pueden editar
            self.dbTabla.notas.writable = False

            # Se definen los valores por default obligados
            self.dbTabla.cfdi_id.default = self._D_defaults.cfdi_id
            self.dbTabla.versionpago.default = "1.0"

            return _D_return

        def configuracampos_edicion(
                self,
                x_cfdirx_compPago,
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param x_cfdirx_compPago:
            @type x_cfdirx_compPago:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx_compPago, self.dbTabla)
            _dbRow = _D_results.dbRow

            _D_results = super(TCFDIRX_COMPLEMENTOPAGOS.PROC, self).configuracampos_edicion(
                x_cfdirx_detalle = _dbRow
                )
            _D_return.combinar(_D_results)

            # Si ya tiene docrelacionados no puede cambiar el tipo de cambio ni la moneda
            if _dbRow.tcfdirx_complementopago_doctosrelacionados.count() > 0:
                self.dbTabla.tipocambio.writable = False
                self.dbTabla.monedacontpaqi_id.writable = False
            else:
                pass

            return _D_return

        def al_validar(
                self,
                O_form,
                **D_params
                ):
            """ Validar la información para poder grabar cfdi

            Las validaciones en esta función son genéricas, se espera que existan validaciones
             específicas por tipo de CFDI

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _dbRow = self.dbTabla(O_form.record_id) if O_form.record_id else None
            _D_camposAChecar = STV_FWK_LIB.OBTENER_CAMPOSACHECAR_PARA_VALIDAR(self.dbTabla, _dbRow, O_form)

            D_params = Storage(D_params)
            if not D_params.validacionDummy:
                # Se usa validacionDummy en D_paramas para poder validar capturas rápidas de pagos

                # Se llaman las validaciones del CFDI_relacionado, indicando que tiporelacion_id debe existsir
                O_form = super(TCFDIRX_COMPLEMENTOPAGOS.PROC, self).al_validar(
                    O_form, _dbRow, D_camposAChecar = _D_camposAChecar
                    )
            else:
                pass

            # Validaciones
            if not _D_camposAChecar.fechapago:
                O_form.errors.fechapago = "Fecha no puede estar vacío"
            elif _D_camposAChecar.fechapago > request.browsernow:
                O_form.errors.fechapago = "Fecha del pago no puede ser mayor a la fecha actual"
            else:
                pass
            if not _D_camposAChecar.formapago_id:
                O_form.errors.formapago_id = "Forma Pago no puede estar vacío"
            else:
                pass
            if not _D_camposAChecar.monedacontpaqi_id:
                O_form.errors.monedacontpaqi_id = "Moneda no puede estar vacío"
            else:
                pass
            if not Decimal(_D_camposAChecar.monto):
                O_form.errors.monto = "Monto no puede estar vacío"
            else:
                pass

            return O_form

        @classmethod
        def MAPEAR_CAMPOS(
                cls,
                x_cfdirx_comppago,
                **D_params
                ):
            """ Mapea campos, llenando campos automáticos

            @param x_cfdirx_comppago:
            @type x_cfdirx_comppago:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx_comppago, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage()

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)
                    break
                else:
                    pass

            return _D_return

        @classmethod
        def CALCULAR_CAMPOS(
                cls,
                x_cfdirx_comppago,
                b_actualizarRegistro = True,
                b_recalcularCFDI = True,
                **D_params
                ):
            """ Recalcula campos, haciendo la consulta de la carta porte a partir del CFDI

            @param x_cfdirx_comppago:
            @type x_cfdirx_comppago:
            @param b_recalcularCFDI:
            @type b_recalcularCFDI:
            @param b_actualizarRegistro:
            @type b_actualizarRegistro:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx_comppago, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage(
                sumarelacionados = 0
                )

            _dbRows_docsRelacionados = _dbRow.tcfdirx_complementopago_doctosrelacionados.select()

            for _dbRow_docRel in _dbRows_docsRelacionados:
                _D_actualizar.sumarelacionados += _dbRow_docRel.imppagado_monedapago or 0

            if b_actualizarRegistro:
                for _s_nombreCampo in _D_actualizar:
                    if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                        _dbRow.update_record(**_D_actualizar)

                        break
                    else:
                        pass
            else:
                # Si no se desea actualizar el registro, se manda el diccionario con los valores calculados
                _D_return.D_actualizar = _D_actualizar

            if b_recalcularCFDI:
                # Debido a que puede cambiarse el mondo y aun asi no cambiar la suma de relacionados, siempre se
                #  recalcula el CFDI, para borrar errores
                _D_results = TCFDISRX.PROC.CALCULAR_CAMPOS(x_cfdirx = _dbRow.cfdi_id)
            else:
                pass

            return _D_return

        pass  # PROC

    pass  # TCFDIRX_COMPLEMENTOPAGOS


dbc01.define_table(
    TCFDIRX_COMPLEMENTOPAGOS.S_NOMBRETABLA, *TCFDIRX_COMPLEMENTOPAGOS.L_DBCAMPOS,
    format = '%(cfdi_id)s',
    singular = 'CFDI pago',
    plural = 'CFDI pagos',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TCFDIRX_COMPLEMENTOPAGOS
    )


class TCFDIRX_COMPLEMENTOPAGO_DOCTOSRELACIONADOS(Table):
    S_NOMBRETABLA = 'tcfdirx_complementopago_doctosrelacionados'
    L_DBCAMPOS = [
        Field(
            'cfdi_complementopago_id', dbc01.tcfdirx_complementopagos, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'CFDI Complemento Pago', comment = None,
            writable = False, readable = False,
            represent = None
            ),
        Field(
            'iddocumento', 'string', length = 50, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'CFDI Relacionado UUID', comment = '',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        TCFDISRX.DEFINIR_DBCAMPO(
            'cfdirelacionado_id',
            represent = lambda v, r: stv_represent_referencefield(
                v, r, dbField_paraReference.dbc01.tcfdirx_relacionados.cfdirelacionado_id
                )
            ),
        Field(
            'serie', 'string', length = 50, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Serie', comment = '',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'folio', 'string', length = 50, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Folio', comment = '',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'moneda', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Moneda', comment = '',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        TMONEDAS.DEFINIR_DBCAMPO(
            'moneda_id',
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tcfdisrx.moneda_id)
            ),

        # Este será el campo que se utilice en vez de moneda_id
        TEMPRESA_MONEDASCONTPAQI.DEFINIR_DBCAMPO(
            'monedacontpaqi_id',
            represent = lambda v, r: stv_represent_referencefield(
                v, r, dbc01.tcfdirx_complementopago_doctosrelacionados.monedacontpaqi_id
                )
            ),

        Field(
            'tipocambio', 'decimal(14,4)', default = 1,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Tipo de cambio', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        Field(
            'metodopago', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Método de Pago', comment = '',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'metodopago_id', 'integer', default = None,
            required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tmetodospago.id', db01.tmetodospago._format)),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Método de Pago', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(
                v, r, dbc01.tcfdirx_complementopago_doctosrelacionados.metodopago_id
                )
            ),
        Field(
            'numparcialidad', 'integer', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Número de Parcialidad', comment = '',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'impsaldoant', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Saldo Anterior', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        Field(
            'imppagado', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Importe Pagado', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        Field(
            'impsaldoinsoluto', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Saldo Insoluto', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        Field(
            'imppagado_monedapago', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Imp. Pagado Moneda Pago', comment = None,
            writable = False, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        tSignature_dbc01,
        ]

    def __init__(self, dbRef, tablename, *fields, **args):
        Table.__init__(self, dbRef, tablename, *fields, **args)
        self.PROC.dbTabla = self
        return

    @staticmethod
    def ASOCIAR_UUID(x_cfdirx_comppago_docrel):
        """ Se busca y asocian ligas rotas en a CFDIs relacionados
        """
        _D_return = FUNC_RETURN(
            n_updatedRows = 0
            )

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx_comppago_docrel, cls.dbTabla)
        _dbRow = _D_results.dbRow

        # Ejemplo puede usar
        # tcfdirx_relacionados.id = 8419 de tcfdisrx.id = 47696
        # tcfdisrx.id = 47762

        _dbTableCFDIsBuscar = dbc01.tcfdisrx.with_alias('cfdisbuscar')
        _dbRows_cfdiRelacionados = dbc01(
            (dbc01.tcfdirx_complementopagos.id == _dbRow.cfdi_complementopago_id)
            & (dbc01.tcfdisrx.id == dbc01.tcfdirx_complementopagos.cfdi_id)
            & (_dbTableCFDIsBuscar.uuid == _dbRow.iddocumento)
            & (_dbTableCFDIsBuscar.empresa_id == dbc01.tcfdisrx.empresa_id)
            ).select(
                dbc01.tcfdirx_complementopagos.ALL,
                _dbTableCFDIsBuscar.id,
                )

        if len(_dbRows_cfdiRelacionados) > 1:
            # Error, no debería de regresar mas de un registro
            _D_return.agrega_error("Se encontraron más de un registro relacionado, contacte al administrador %s"
                                   % s_cfdi_relacionado_id)

        elif len(_dbRows_cfdiRelacionados) == 0:
            # CFDI asociado con el UUID no se encontró
            _D_return.agrega_advertencia("No se encontraron cfdis relacionados")

        else:
            # Si tiene solamente un registro, hay que actualizarlo
            _D_return.n_updatedRows += dbc01(
                dbc01.tcfdirx_complementopago_doctosrelacionados.id
                == _dbRow.id
                ).update(cfdirelacionado_id = _dbRows_cfdiRelacionados.first().cfdisbuscar.id)

        return _D_return

    @staticmethod
    def CONFIGURAR_INTEGRIDAD_REFERENCIAL():

        return None

    class PROC:

        dbTabla = None  # Se escribe con la inicialización

        def __init__(self, **D_defaults):
            """ Inicializa objecto de tipo PROC para control del registro actual

            @param D_defaults:
            @type D_defaults:
            """
            self._D_defaults = Storage(D_defaults)
            self._L_requeridos = ['cfdi_complementopago_id']
            return

        def puede_crear(self, x_cfdirx_comppago, **D_defaults):
            """ Determina si puede o no crear un CFDI de traslado.

            Actualmente no hay condición requerida.

            """
            _D_return = FUNC_RETURN()

            self._D_defaults.update(D_defaults or {})

            for _s_requerido in self._L_requeridos:
                if _s_requerido not in self._D_defaults:
                    _D_return.agrega_error("Campo %s es requerido para generar CFDI" % _s_requerido)

                else:
                    pass

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx_comppago, dbc01.tcfdirx_complementopagos)
            _dbRow_cfdi_compPago = _D_results.dbRow

            _D_results = TCFDISRX.PROC.PUEDE_EDITAR(_dbRow_cfdi_compPago.cfdi_id)
            _D_return.combinar(_D_results)

            return _D_return

        @classmethod
        def PUEDE_EDITAR(cls, x_cfdirx_comppago_docrel):
            """ Define si es posible editar el CFDI de traslado.
            @param x_cfdirx_comppago_docrel:

            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx_comppago_docrel, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_results = TCFDIRX_COMPLEMENTOPAGOS.PROC.PUEDE_EDITAR(_dbRow.cfdi_complementopago_id)
            _D_return.combinar(_D_results)

            return _D_return

        @classmethod
        def PUEDE_ELIMINAR(cls, x_cfdirx_comppago_docrel):
            """ Determina si puede o no eliminar un registro

            Misma condición que puede_editar, a lo mejor pudieran verse condiciones distintas

            """

            return cls.PUEDE_EDITAR(x_cfdirx_comppago_docrel)

        @classmethod
        def CONFIGURACAMPOS_DESDE_dbRow(cls, dbRow_base):
            # cls.dbTabla.cfdi_complementopago_id.default = dbRow_base.cfdi_complementopago_id
            # cls.dbTabla.iddocumento.default = dbRow_base.iddocumento
            # cls.dbTabla.cfdirelacionado_id.default = dbRow_base.cfdirelacionado_id
            # cls.dbTabla.serie.default = dbRow_base.serie
            # cls.dbTabla.empresa_serie_id.default = dbRow_base.empresa_serie_id
            # cls.dbTabla.folio.default = dbRow_base.folio
            # cls.dbTabla.moneda.default = dbRow_base.moneda
            # cls.dbTabla.moneda_id.default = dbRow_base.moneda_id
            # cls.dbTabla.monedacontpaqi_id.default = dbRow_base.monedacontpaqi_id
            # cls.dbTabla.tipocambio.default = dbRow_base.tipocambio
            # cls.dbTabla.metodopago.default = dbRow_base.monto
            cls.dbTabla.metodopago_id.default = dbRow_base.metodopago_id
            # cls.dbTabla.numparcialidad.default = dbRow_base.numparcialidad
            # cls.dbTabla.impsaldoant.default = dbRow_base.impsaldoant
            cls.dbTabla.imppagado.default = dbRow_base.imppagado
            # cls.dbTabla.imppagado_monedapago.default = dbRow_base.imppagado_monedapago
            # cls.dbTabla.impsaldoinsoluto.default = dbRow_base.impsaldoinsoluto
            return

        def configuracampos_nuevo(self, s_cfdirx_comppago_docrel_id, b_llamadaDesdeEditar = False, **D_defaults):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param b_llamadaDesdeEditar: Indica si la llamada a esta función es desde otra capa,
             desactivando validación de puede_crear
            @type b_llamadaDesdeEditar:
            @param s_cfdirx_comppago_docrel_id:
            @type s_cfdirx_comppago_docrel_id:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            self._D_defaults.update(D_defaults or {})
            for _s_fieldName in self.dbTabla.fields:
                self.dbTabla[_s_fieldName].writable = False
            for _s_fieldName in self._D_defaults:
                self.dbTabla[_s_fieldName].default = self._D_defaults[_s_fieldName]

            if not b_llamadaDesdeEditar:
                _D_results = self.puede_crear(x_cfdirx_comppago = self._D_defaults.cfdi_complementopago_id)
            else:
                _D_results = _D_return

            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.combinar(_D_results)

            elif s_cfdirx_comppago_docrel_id:
                _dbRow_base = self.dbTabla(s_cfdirx_comppago_docrel_id)

                self.CONFIGURACAMPOS_DESDE_dbRow(_dbRow_base)

            else:
                # Si se requieren defaults en caso de no tener algún otro definido, ponerlo aqui
                pass

            self.dbTabla.cfdi_complementopago_id.writable = False
            self.dbTabla.cfdirelacionado_id.writable = True
            self.dbTabla.imppagado.writable = True
            self.dbTabla.numparcialidad.writable = False  # Se permite editar pero no en nuevo
            self.dbTabla.impsaldoant.writable = False  # Se permite editar pero no en nuevo
            self.dbTabla.impsaldoinsoluto.writable = False  # Se permite editar pero no en nuevo

            # Campos que no deben ser editados, solo de forma autocalculada
            self.dbTabla.iddocumento.writable = False
            self.dbTabla.serie.writable = False
            self.dbTabla.empresa_serie_id.writable = False
            self.dbTabla.metodopago_id.writable = False
            self.dbTabla.folio.writable = False
            self.dbTabla.moneda.writable = False
            self.dbTabla.moneda_id.writable = False
            self.dbTabla.monedacontpaqi_id.writable = False
            self.dbTabla.imppagado_monedapago.writable = False
            self.dbTabla.tipocambio.writable = False
            self.dbTabla.metodopago.writable = False

            # Campos que todos los movimientos pueden editar
            self.dbTabla.notas.writable = False

            # Se definen los valores por default obligados
            self.dbTabla.cfdi_complementopago_id.default = self._D_defaults.cfdi_complementopago_id

            return _D_return

        def configuracampos_edicion(self, x_cfdirx_comppago_docrel):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param x_cfdirx_comppago_docrel:
            @type x_cfdirx_comppago_docrel:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx_comppago_docrel, self.dbTabla)
            _dbRow = _D_results.dbRow

            # Se llenan los default obligados
            for _s_nombreCampo in self._L_requeridos:
                self._D_defaults[_s_nombreCampo] = _D_results.dbRow[_s_nombreCampo]

            _D_results = self.PUEDE_EDITAR(_dbRow)

            if _D_results.E_return == CLASS_e_RETURN.OK:
                # Si puede editar regresa un ok y el movimiento esta abierto

                # Se manda vacío el argumento para no agregar defaults a los campos
                _D_results = self.configuracampos_nuevo(
                    s_cfdirx_comppago_docrel_id = None,
                    b_llamadaDesdeEditar = True,
                    )
                _D_return.combinar(_D_results)

                # No permitir editarlo, ya que se asigna al crear el registro y todo se calcula en base a eso
                self.dbTabla.cfdirelacionado_id.writable = False
                self.dbTabla.numparcialidad.writable = True  # Se permite editar pero no en nuevo
                self.dbTabla.impsaldoant.writable = True  # Se permite editar pero no en nuevo
                self.dbTabla.impsaldoinsoluto.writable = True  # Se permite editar pero no en nuevo

            else:
                # De lo contrario en cualquier otro caso, no puede editar nada de esta tabla
                for _s_fieldName in self.dbTabla.fields:
                    self.dbTabla[_s_fieldName].writable = False
                _D_return.agrega(CLASS_e_RETURN.OK_WARNING, _D_results.s_msgError, "No puede editar")

            return _D_return

        def al_validar(self, O_form):
            """ Validar la información para poder grabar cfdi

            Las validaciones en esta función son genéricas, se espera que existan validaciones
             específicas por tipo de CFDI

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _dbRow = self.dbTabla(O_form.record_id) if O_form.record_id else None
            _D_camposAChecar = STV_FWK_LIB.OBTENER_CAMPOSACHECAR_PARA_VALIDAR(self.dbTabla, _dbRow)

            if not O_form.record_id:
                # Se esta creando el registro

                _D_results = self.puede_crear(
                    x_cfdirx_comppago = _D_camposAChecar.cfdi_complementopago_id, **D_camposAChecar
                    )
                if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                    O_form.errors.id = _D_results.s_msgError
                else:
                    pass

            else:
                # Se esta modificando el registro, la llamada a configuraCampos_edicion ya definió con writable
                # que puede modificarse y que no.

                _D_results = self.PUEDE_EDITAR(_dbRow)
                if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                    O_form.errors.id = _D_results.s_msgError
                else:
                    pass

            # Validaciones

            if not _D_camposAChecar.cfdirelacionado_id:
                O_form.errors.cfdirelacionado_id = "CFDI relacionado no puede estar vacío"
            else:
                _dbRow_cfdiIngreso = dbc01.tcfdis(_dbRow.cfdirelacionado_id)
                _dbRow_cfdi_compPago = dbc01.tcfdirx_complementopagos(_D_camposAChecar.cfdi_complementopago_id)
                if _dbRow_cfdiIngreso.sat_status != TCFDISRX.SAT_STATUS.VIGENTE:
                    O_form.errors.cfdirelacionado_id = "CFDI relacionado debe estar vigente"
                elif _dbRow_cfdi_compPago.fechapago < _dbRow_cfdiIngreso.fecha:
                    O_form.errors.cfdirelacionado_id = "Fecha del pago no puede ser anterior a la fecha del " \
                        + "CFDI relacionado"
                else:
                    pass
            if not _D_camposAChecar.metodopago_id:
                O_form.errors.metodopago_id = "Metodo de Pago no puede estar vacío"
            else:
                pass
            # ImpPago si puede estar vacío, y si esta vacío se recalcula

            return O_form

        @classmethod
        def AL_ACEPTAR(cls, O_form):
            """ Al aceptar el registro deben hacerse los cálculos

            Se hacen todos los cálculos y aceptación de los cambios en el producto

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()
            _dbRow = cls.dbTabla(O_form.record_id or O_form.vars.id)

            _D_results = cls.MAPEAR_CAMPOS(_dbRow)
            _D_return.combinar(_D_results)
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                pass

            else:

                _D_results = cls.CALCULAR_CAMPOS(_dbRow)
                _D_return.combinar(_D_results)

            generaMensajeFlash(cls.dbTabla, _D_return)
            return O_form

        @classmethod
        def ANTES_ELIMINAR(cls, L_errorsAction, dbRow):
            """ Antes de eliminar un movimiento de inventario de este tipo

            @param dbRow:
            @type dbRow:
            @param L_errorsAction:
            @type L_errorsAction:
            @return: errores
            @rtype: lista de diccionarios {'msg':'...'}
            """
            _D_return = FUNC_RETURN()

            _D_results = cls.PUEDE_ELIMINAR(dbRow)
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                L_errorsAction.append({'msg': _D_results.s_msgError})
            else:
                cls._dbRow_aEliminar = Storage(dbRow.as_dict())

            return L_errorsAction

        @classmethod
        def DESPUES_ELIMINAR(cls, L_errorsAction):
            """ Después de eliminar un registro

            @param L_errorsAction:
            @type L_errorsAction:
            @return: errores
            @rtype: lista de diccionarios {'msg':'...'}
            """
            _D_return = FUNC_RETURN()

            if cls._dbRow_aEliminar:
                _D_results = TCFDIRX_COMPLEMENTOPAGOS.PROC.CALCULAR_CAMPOS(
                    cls._dbRow_aEliminar.cfdi_complementopago_id
                    )
                _D_return.combinar(_D_results)
            else:
                _D_return.E_return = CLASS_e_RETURN.NOK_SECUENCIA
                _D_return.s_msgError += "Error en secuencia de llamado de funciones para borrado. "

            generaMensajeFlash(cls.dbTabla, _D_return)
            return L_errorsAction

        @classmethod
        def MAPEAR_CAMPOS(cls, x_cfdirx_comppago_docrel):
            """ Remapea campos solamente cuando se graba el registro

            @param x_cfdirx_comppago_docrel:
            @type x_cfdirx_comppago_docrel:
            @return:
            @rtype:
            """

            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx_comppago_docrel, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage()

            if not _dbRow.cfdirelacionado_id:
                TCFDIRX_COMPLEMENTOPAGO_DOCTOSRELACIONADOS.ASOCIAR_UUID(_dbRow)
            else:
                pass

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)
                    # Mapear campos no implica recalculo de maestro
                    break
                else:
                    pass

            return _D_return

        @classmethod
        def CALCULAR_CAMPOS(
                cls,
                x_cfdirx_detalle,
                ):
            """ Recalcula campos, haciendo la consulta de la carta porte a partir del CFDI

            @param x_cfdirx_detalle:
            @type x_cfdirx_detalle:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdirx_detalle, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage()

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)

                    # Si hay actualizaciones, debe actualizarse el CFDI, no los conceptos
                    _D_results = TCFDIRX_COMPLEMENTOPAGOS.PROC.CALCULAR_CAMPOS(_dbRow.cfdi_complementopago_id)
                    break
                else:
                    pass

            return _D_return

        pass  # PROC

    pass  # TCFDIRX_COMPLEMENTOPAGO_DOCTOSRELACIONADOS


dbc01.define_table(
    TCFDIRX_COMPLEMENTOPAGO_DOCTOSRELACIONADOS.S_NOMBRETABLA, *TCFDIRX_COMPLEMENTOPAGO_DOCTOSRELACIONADOS.L_DBCAMPOS,
    format = '%(id)s',
    singular = 'CFDI Docto. Relacionado',
    plural = 'CFDI Doctos. Relacionados',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TCFDIRX_COMPLEMENTOPAGO_DOCTOSRELACIONADOS
    )


class TCFDIRX_MOVIMIENTOS(Table):
    """ Al calcular el saldo del proveedor o actualizarlo, esta tabla registra los movimientos de saldo en el CFDI.

    Esta tabla se actualiza al generar el saldo del Proveedor, y cada vez que se le asigna un CFDI.
    """

    S_NOMBRETABLA = 'tcfdirx_movimientos'

    E_ACCION = TCFDI_MOVIMIENTOS.E_ACCION
    E_TIPODOCUMENTO = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO
    E_GENERADO_POR = TCFDI_MOVIMIENTOS.E_GENERADO_POR

    L_DBCAMPOS = [
        Field(
            'empresa_id', dbc01.tempresas, default = None,
            required = True,
            ondelete = 'NO ACTION', notnull = True, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Empresa', comment = '',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r)
            ),
        STV_LIB_DB.DEFINIR_DBCAMPO(
            dbc01.tempresa_proveedores, 'proveedor_id',
            dbQry = (dbc01.tempresa_proveedores.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id),
            label = 'Proveedor', writable = True
            ),
        Field(
            'fecha', type = FIELD_UTC_DATETIME, default = None,  # Se graba como la fecha-hora local de emisión del CFDI
            required = True, requires = IS_NULL_OR(IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME)),
            notnull = True,
            widget = stv_widget_inputDateTime, label = 'Fecha', comment = '',
            writable = False, readable = True,
            represent = stv_represent_datetime
            ),
        Field(
            'fechaingreso', type = FIELD_UTC_DATETIME, default = None,
            # Se graba como la fecha-hora local de emisión del CFDI de ingreso relacionado para su uso en consultas
            required = True, requires = IS_NULL_OR(IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME)),
            notnull = True,
            widget = stv_widget_inputDateTime, label = 'Fecha Ingreso', comment = '',
            writable = False, readable = True,
            represent = stv_represent_datetime
            ),

        Field(
            'accion', 'integer', default = E_ACCION.NO_DEFINIDA,
            required = False, requires = IS_IN_SET(
                E_ACCION.D_TODOS, zero = 0, error_message = (
                    'Selecciona')
                ),
            notnull = True, unique = False,
            widget = stv_widget_combobox,
            label = 'Acción', comment = 'Acción en el CFDI',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TCFDIRX_MOVIMIENTOS.E_ACCION.D_TODOS)
            ),
        Field(
            'tipodocumento', 'integer', default = E_TIPODOCUMENTO.NO_DEFINIDO,
            required = False,
            requires = IS_IN_SET(E_TIPODOCUMENTO.D_TODOS, zero = 0, error_message = 'Selecciona'),
            notnull = True, unique = False,
            widget = stv_widget_combobox,
            label = 'Tipo Documento', comment = 'Tipo de documento',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TCFDIRX_MOVIMIENTOS.E_TIPODOCUMENTO.D_TODOS)
            ),
        Field(
            'descripcion', 'string', length = 150, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Descripcion', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),

        Field(
            'cfdi_id', dbc01.tcfdisrx, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'CFDI Origen', comment = 'CFDI que origina el movimiento',
            writable = False, readable = True,
            represent = None
            ),
        # En caso de CFDI de pago, se relaciona cada referencia
        Field(
            'cfdi_complementopago_doctorelacionado_id', dbc01.tcfdirx_complementopago_doctosrelacionados,
            default = None,
            required = False,
            ondelete = 'CASCADE', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Comp. Pago', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r)
            ),

        # En caso de CFDI de egreso, se relaciona cada referencia
        Field(
            'cfdi_relacionado_id', dbc01.tcfdirx_relacionados, default = None,
            required = False,
            ondelete = 'CASCADE', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Relacionado', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r)
            ),

        # Todos los movimientos estan mapeados a un CFDI de Ingreso
        Field(
            'cfdi_ingreso_id', dbc01.tcfdisrx, default = None,
            required = False,
            ondelete = 'CASCADE', notnull = False, unique = False,
            label = 'CFDI Ingreso', comment = 'CFDI del cual se maneja el estado de cuenta',
            writable = False, readable = True,
            represent = None
            ),

        Field(
            'cfdi_movimiento_emision_id', 'integer', default = None,
            required = False,
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = (
                'Movimiento Inicial'), comment = 'Utilizado en caso de cancelación',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r)
            ),

        Field(
            'nota', 'string', length = 150, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Nota', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),

        # Campos usados para contabilización de los conceptos
        Field(
            'cargo', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Cargo', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        Field(
            'abono', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Abono', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        Field(
            'saldo', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Saldo', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money
            ),

        Field(
            'importe', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Importe', comment = 'Importe del movimiento en caso de conocerlo',
            writable = False, readable = True,
            represent = stv_represent_money_ifnotzero
            ),

        Field(
            'generado_por', 'integer', default = E_GENERADO_POR.NO_DEFINIDO,
            required = False,
            requires = IS_IN_SET(E_GENERADO_POR.D_TODOS, zero = 0, error_message = 'Selecciona'),
            notnull = True, unique = False,
            widget = stv_widget_combobox,
            label = 'Generado por', comment = 'Define el origén de este registro',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TCFDIRX_MOVIMIENTOS.E_GENERADO_POR.D_TODOS)
            ),

        tSignature_dbc01,
        ]

    @staticmethod
    def CONFIGURAR_INTEGRIDAD_REFERENCIAL():
        _dbTable = dbc01.tcfdirx_movimientos
        _dbTable.cfdi_movimiento_emision_id.type = 'reference tcfdirx_movimientos'
        _dbTable.cfdi_movimiento_emision_id.ondelete = 'NO ACTION'
        _dbTable.cfdi_movimiento_emision_id.requires = IS_IN_DB(
            dbc01,
            'tcfdirx_movimientos.id',
            dbc01.tcfdirx_movimientos._format
            )

        return None

    pass  # TCFDIRX_MOVIMIENTOS


dbc01.define_table(
    TCFDIRX_MOVIMIENTOS.S_NOMBRETABLA, *TCFDIRX_MOVIMIENTOS.L_DBCAMPOS,
    format = '%(cfdi_id)s',
    singular = 'CFDI movimientos',
    plural = 'CFDI movimientos',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TCFDIRX_MOVIMIENTOS
    )


class TCFDIRX_MANEJOSALDOS(Table):
    """ En caso de que el CFDI tenga saldo a favor, esta tabla identifica a donde se aplicará ese saldo.

    Por lo pronto solo define un proceso de transferencia a otro CFDI, es posible que en un futuro se
    contemple otro proceso. Si se define otro tipo, verificar la cancelacion de CFDIs que actualmente checa que no tenga
    registros en esta tabla.

    También debe agregarse logica de cancelacion de este registro, ya que si existen movimientos posteriores,
     no deben eliminarse,
    solo cancelarse para no afectar estado de cuenta y saldos.

    # TODO no se puede abonar saldo a facturas con diferente moneda, porque no se puede determinar de forma oficial
     el tipo de cambio.
    """

    S_NOMBRETABLA = 'tcfdirx_manejosaldos'

    class E_PROCESO:
        """ Se definen las opciónes del campo """
        NO_DEFINIDO = 0
        TRANSFERENCIA_A_CFDI = 1
        D_TODOS = {
            NO_DEFINIDO         : 'No definido',
            TRANSFERENCIA_A_CFDI: 'Transferencia a CFDI',
            }

    L_DBCAMPOS = [
        Field(
            'cfdi_id', dbc01.tcfdisrx, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'CFDI', comment = None,
            writable = False, readable = False,
            represent = None
            ),
        Field(
            'proceso', 'integer', default = E_PROCESO.NO_DEFINIDO,
            required = False,
            requires = IS_IN_SET(E_PROCESO.D_TODOS, zero = 0, error_message = 'Selecciona'),
            notnull = True, unique = False,
            widget = stv_widget_combobox,
            label = 'Proceso', comment = 'Proceso en el que se usará el total o parte del saldo a favor',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TCFDIRX_MANEJOSALDOS.E_PROCESO.D_TODOS)
            ),
        Field(
            'fecha', type = FIELD_UTC_DATETIME, default = request.browsernow,
            # Se graba como la fecha-hora local de emisión del CFDI
            required = True, requires = IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME),
            notnull = True,
            widget = stv_widget_inputDateTime, label = 'Fecha', comment = '',
            writable = True, readable = True,
            represent = stv_represent_datetime
            ),
        Field(
            'cfdirelacionado_id', dbc01.tcfdisrx, default = None,  # Corresponde a tcfdi relacionado
            required = False,
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_search(
                f,
                v,
                s_display = '%(serie)s %(folio)s',
                s_url = URL(
                    a = 'app_timbrado', c = '130clientes_cfdis', f = 'cliente_manejosaldo_cfdis_consaldo_buscar',
                    args = request.args[:6]
                    ),
                D_additionalAttributes = {}
                ),
            label = 'CFDI Receptor', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(
                v, r, dbc01.tcfdirx_manejosaldos.cfdirelacionado_id
                )
            ),
        Field(
            'descripcion', 'string', length = 150, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Descripción', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'importe', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Importe', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        tSignature_dbc01,
        ]

    pass


dbc01.define_table(
    TCFDIRX_MANEJOSALDOS.S_NOMBRETABLA, *TCFDIRX_MANEJOSALDOS.L_DBCAMPOS,
    format = '%(descripcion)s',
    singular = 'CFDI Manejo Saldo',
    plural = 'CFDI Manejo Saldos',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TCFDIRX_MANEJOSALDOS
    )


STV_FWK_LIB.DEFINIR_INTEGRIDADREFERENCIAL(
    dbTabla_referenciada = dbc01.tcfdisrx,
    dbCampo_haceReferencia = dbc02.tcfdirx_xmls.cfdi_id
    )
STV_FWK_LIB.DEFINIR_INTEGRIDADREFERENCIAL(
    dbTabla_referenciada = db01.ttiposcomprobantes,
    dbCampo_haceReferencia = dbc01.tcfdisrx.tipocomprobante_id
    )
STV_FWK_LIB.DEFINIR_INTEGRIDADREFERENCIAL(
    dbTabla_referenciada = dbc01.tcfdisrx,
    dbCampo_haceReferencia = dbc02.tcfdirx_xmls.cfdi_id
    )

STV_FWK_LIB.DEFINIR_INTEGRIDADREFERENCIAL(
    dbTabla_referenciada = dbc01.tcfdirx_conceptos,
    dbCampo_haceReferencia = dbc01.tempresa_invmovto_productos.cfdi_concepto_id,
    b_actualizarTipo = True,
    fn_widget = stv_widget_input
    )

STV_FWK_LIB.DEFINIR_INTEGRIDADREFERENCIAL(
    dbTabla_referenciada = dbc01.tcfdisrx,
    dbCampo_haceReferencia = dbc01.tempresa_inventariomovimientos.cfdi_id,
    b_actualizarTipo = True
    )

TCFDIRX_COMPLEMENTOPAGO_DOCTOSRELACIONADOS.CONFIGURAR_INTEGRIDAD_REFERENCIAL()
TCFDIRX_RELACIONADOS.CONFIGURAR_INTEGRIDAD_REFERENCIAL()
TCFDIRX_MOVIMIENTOS.CONFIGURAR_INTEGRIDAD_REFERENCIAL()
TCFDISRX.CONFIGURAR_INTEGRIDAD_REFERENCIAL()
