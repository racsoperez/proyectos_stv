# -*- coding: utf-8 -*-


class TEMPRESA_PROVEEDOR_GUIASPROVEEDORES:
    
    @classmethod
    def CONFIGURAR_INTEGRIDAD_REFERENCIAL(cls):
        _dbTable = dbc01.tempresa_proveedor_guiasproveedores
        _dbTable.monedacontpaqi_id.type = 'reference tempresa_monedascontpaqi'
        _dbTable.monedacontpaqi_id.ondelete = 'NO ACTION'
        _dbTable.monedacontpaqi_id.requires = IS_IN_DB(
            dbc01(dbc01.tempresa_monedascontpaqi.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id), 
            'tempresa_monedascontpaqi.id', 
            dbc01.tempresa_monedascontpaqi._format
            )
        _dbTable.monedacontpaqi_id.widget = lambda f, v: stv_widget_db_chosen(f, v, 1)
        _dbTable.monedacontpaqi_id.represent = stv_represent_referencefield
        
        return     
    
    pass
dbc01.define_table(
    'tempresa_proveedor_guiasproveedores',
    Field('empresa_proveedor_id', dbc01.tempresa_proveedores, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Empresa', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('ejercicio', 'integer', default = None,
      required = True, requires = IS_IN_SET(VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id), zero = 0, error_message = 'Selecciona'),
      notnull = False, unique = False,
      widget = stv_widget_combobox, 
      label = 'Ejercicio inicial', comment = None,
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, VALIDACIONES.EJERCICIO.OPCIONES(D_stvSiteHelper.dbconfigs.defaults.empresa_id))),
    Field('moneda_id', 'integer', default=D_stvSiteHelper.dbconfigs.defaults.moneda_id,
      required=True, requires = IS_IN_DB(db01, 'tmonedas.id', db01.tmonedas._format), 
      ondelete='NO ACTION', notnull=True, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label= 'Moneda', comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_proveedor_guiasproveedores.moneda_id)),

    #Este será el campo que se utilice en vez de moneda_id
    Field('monedacontpaqi_id', 'integer', default=D_stvSiteHelper.dbconfigs.defaults.moneda_contpaqi_id,
      required=True,
      ondelete='NO ACTION', notnull=True, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label= 'Moneda CONTPAQi', comment=None,
      writable=True, readable=True,
      represent=lambda v, r:  stv_represent_referencefield(v, r, dbc01.tempresa_proveedor_guiasproveedores.monedacontpaqi_id)),  
    
    Field('cuenta_proveedor', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cuenta Proveedor', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuenta_proveedor_complemento', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cuenta Proveedor Complemento', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuenta_anticipo', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cuenta Anticipo', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('cuenta_anticipo_complemento', 'string', length=30, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label= 'Cuenta Anticipo Complemento', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    tSignature_dbc01,
    format='%(cuenta_proveedor)s',
    singular='Guía Proveedor',
    plural='Guías Proveedores',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_PROVEEDOR_CLASIFICACIONES:
    pass
dbc01.define_table(
    'tempresa_proveedor_clasificaciones',
    Field('empresa_proveedor_id', dbc01.tempresa_proveedores, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Proveedor', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('empresa_clasificacionproveedor_id', dbc01.tempresa_clasificacionesproveedores, default=None,
      required=True,
      ondelete='NO ACTION', notnull=True, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label= 'Clasificación del proveedor', comment=None,
      writable=True, readable=True,
      represent = stv_represent_referencefield),    
    tSignature_dbc01,
    format='%(id)s',
    singular='Clasificación proveedor',
    plural='Clasificaciones proveedor',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )

class TEMPRESA_PROVEEDOR_PRECIOSOFICIALES:
    
    @classmethod
    def ULTIMO_PRECIOOFICIAL(cls, s_proveedor_id, s_id):
        
        _D_result = Storage(
            E_return = CLASS_e_RETURN.NOK_ERROR,
            dbRow_ultimo_preciooficial = None, #Variable donde contendrá el último precio oficial guardado
            )
        
        #Si no trae _s_empresa_proveedor_preciooficial_id significa que viene desde nuevo.
        _dbRows_preciosoficiales = dbc01(
            (dbc01.tempresa_proveedor_preciosoficiales.empresa_proveedor_id == s_proveedor_id)
            &(dbc01.tempresa_proveedor_preciosoficiales.id != s_id) #Que sea diferente del ID que se acaba de aceptar
            ).select(
                dbc01.tempresa_proveedor_preciosoficiales.ALL,
                orderby =[
                    dbc01.tempresa_proveedor_preciosoficiales.fechavigencia_inicial
                    ]
                )
        
        if _dbRows_preciosoficiales:
            _D_result.dbRow_ultimo_preciooficial = _dbRows_preciosoficiales.last()
            _D_result.E_return = CLASS_e_RETURN.OK
        else:
            pass
        
        return _D_result
    pass
dbc01.define_table(
    'tempresa_proveedor_preciosoficiales',
    Field('empresa_proveedor_id', dbc01.tempresa_proveedores, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Proveedor', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('fechavigencia_inicial', type = FIELD_UTC_DATETIME, default=request.browsernow,
      required=True, requires = IS_NULL_OR(IS_DATETIME(format= STV_FWK_APP.FORMAT.s_DATETIME)),
      notnull=True,
      widget=stv_widget_inputDateTime, label='Fecha vigencia inicial', comment=None,
      writable=True, readable=True,
      represent=stv_represent_datetime), 
    Field('fechavigencia_final', type = FIELD_UTC_DATETIME, default=None,
      required=False, requires = IS_NULL_OR(IS_DATETIME(format= STV_FWK_APP.FORMAT.s_DATETIME)),
      notnull = False,
      widget = stv_widget_inputDateTime, label = 'Fecha vigencia final', comment = None,
      writable = True, readable = True,
      represent = stv_represent_datetime),
    tSignature_dbc01,
    format='%(id)s',
    singular='Precio oficial',
    plural='Precios oficiales',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_PROVEEDOR_PRECIOOFICIAL_PRODUCTOS:
    
    @classmethod
    def INSERTAR_PRODUCTOS(cls, dbRows_productos, s_empresa_proveedor_preciooficial_id):
        """Función para insertar los productos.
            
            Args:
                dbRows_productos: Query con los productos a insertar.
                s_empresa_proveedor_preciooficial_id: ID de empresa_proveedor_preciosoficiales
            
        """
        
        _L_productos_id = []
        
        for _dbRow_producto in dbRows_productos:
            
            if _dbRow_producto.tempresa_prodserv.id not in _L_productos_id:
            #Se verifica que no se haya guardado este producto ya en el detalle y que contenga código del proveedor

                if _dbRow_producto.tempresa_prodserv_codigosproveedores.codigo:
                    _s_codigoexterno = _dbRow_producto.tempresa_prodserv_codigosproveedores.codigo
                else:
                    _s_codigoexterno = ""
                    
                #Diccionario para insertar los productos.
                _D_insert_Helper = Storage (
                    empresa_proveedor_preciooficial_id = s_empresa_proveedor_preciooficial_id,
                    empresa_producto_id = _dbRow_producto.tempresa_prodserv.id,
                    unidad_id = _dbRow_producto.tempresa_prodserv.unidad_id,
                    codigoproveedor = _s_codigoexterno,
                    monedacontpaqi_id = D_stvSiteHelper.dbconfigs.defaults.moneda_contpaqi_id
                    )
                
                dbc01.tempresa_proveedor_preciooficial_productos.insert(**_D_insert_Helper)
                
                #Se guarda el producto ID para que no pueda repetirse
                _L_productos_id.append(_dbRow_producto.tempresa_prodserv.id)
                
            else:
                
                pass
            
        return

    @classmethod
    def CONFIGURAR_INTEGRIDAD_REFERENCIAL(cls):
        _dbTable = dbc01.tempresa_proveedor_preciooficial_productos
        _dbTable.monedacontpaqi_id.type = 'reference tempresa_monedascontpaqi'
        _dbTable.monedacontpaqi_id.ondelete = 'NO ACTION'
        _dbTable.monedacontpaqi_id.requires = IS_IN_DB(
            dbc01(dbc01.tempresa_monedascontpaqi.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id), 
            'tempresa_monedascontpaqi.id', 
            dbc01.tempresa_monedascontpaqi._format
            )
        _dbTable.monedacontpaqi_id.widget = lambda f, v: stv_widget_db_chosen(f, v, 1)
        _dbTable.monedacontpaqi_id.represent = stv_represent_referencefield
        
        return    
     
    pass
dbc01.define_table(
    'tempresa_proveedor_preciooficial_productos',
    Field('empresa_proveedor_preciooficial_id', dbc01.tempresa_proveedor_preciosoficiales, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Precio oficial', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('empresa_producto_id', dbc01.tempresa_prodserv, default=None,
        required=False,
        ondelete='NO ACTION', notnull=False, unique=False,
        widget = lambda f, v: stv_widget_db_search(f, v,
            s_display = '%(descripcion)s', 
            s_url = URL( a = 'app_almacenes', c = '600busquedas', f = 'buscar_producto'),
            D_additionalAttributes = {'reference':dbc01.tempresa_prodserv.id}
            ),
        label= 'Producto', comment=None,
        writable=True, readable=True,
        represent=lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_proveedor_preciooficial_productos.empresa_producto_id)),
    Field('unidad_id', 'integer', default=None,
        required=False, requires = IS_NULL_OR(IS_IN_DB(db01,'tunidades.id','%(nombre)s [%(c_claveunidad)s]')),
        ondelete='NO ACTION', notnull=False, unique=False,
        widget = lambda f, v: stv_widget_db_search(f, v, 
            s_display = '%(nombre)s [%(c_claveunidad)s]', 
            s_url = URL( a = 'app_generales', c = '005genericos_sat', f = 'unidades_buscar'),
            D_additionalAttributes = {'reference':db01.tunidades.id}
            ), 
        label= 'Unidad', comment=None,
        writable=True, readable=True,
        represent=lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_proveedor_preciooficial_productos.unidad_id)),  
    Field('codigoproveedor', 'text', length = 100, default = None,
      required = False,
      notnull = False, unique = False,
      widget = stv_widget_input, label = 'Código del proveedor', comment = None,
      writable = True, readable = True,
      represent = stv_represent_string),    
    Field('preciooficial', 'decimal(14,4)', default=0,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_inputMoney, label='Precio oficial', comment='Precio oficial del proveedor',
      writable=True, readable=True,
      represent=stv_represent_money_ifnotzero),       
    Field('moneda_id', 'integer', default=D_stvSiteHelper.dbconfigs.defaults.moneda_id,
      required=True, requires = IS_IN_DB(db01, 'tmonedas.id', db01.tmonedas._format), 
      ondelete='NO ACTION', notnull=True, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label= 'Moneda', comment=None,
      writable=True, readable=True,
      represent=lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_proveedor_preciooficial_productos.moneda_id)),

    #Este será el campo que se utilice en vez de moneda_id
    Field('monedacontpaqi_id', 'integer', default=D_stvSiteHelper.dbconfigs.defaults.moneda_contpaqi_id,
      required=True,
      ondelete='NO ACTION', notnull=True, unique=False,
      widget=lambda f, v: stv_widget_db_chosen(f, v, 1), label= 'Moneda CONTPAQi', comment=None,
      writable=True, readable=True,
      represent=lambda v, r:  stv_represent_referencefield(v, r, dbc01.tempresa_proveedor_preciooficial_productos.monedacontpaqi_id)),  
    
    tSignature_dbc01,
    format='%(descripcion)s',
    singular='Producto',
    plural='Productos',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_PROVEEDOR_COTIZACIONES:
    
    class ESTATUS:
        ''' Se definen las opciónes del campo '''
        BORRADOR = 1
        CANCELADO = 2
        ACTIVO = 3
        
        @classmethod
        def get_dict(cls):
            return {
                cls.BORRADOR: 'Borrador',
                cls.CANCELADO: 'Cancelado',
                cls.ACTIVO: 'Activo',
                }  
            
    @classmethod
    def ULTIMA_COTIZACION(cls, s_proveedor_id, s_id):
        
        _D_result = Storage(
            E_return = CLASS_e_RETURN.NOK_ERROR,
            dbRow_ultima_cotizacion = None, #Variable donde contendrá el último precio oficial guardado
            )
        
        #Si no trae _s_empresa_proveedor_preciooficial_id significa que viene desde nuevo.
        _dbRows_cotizaciones = dbc01(
            (dbc01.tempresa_proveedor_cotizaciones.empresa_proveedor_id == s_proveedor_id)
            &(dbc01.tempresa_proveedor_cotizaciones.id != s_id) #Que sea diferente del ID que se acaba de aceptar
            ).select(
                dbc01.tempresa_proveedor_cotizaciones.ALL,
                orderby =[
                    dbc01.tempresa_proveedor_cotizaciones.fechavigencia_inicial
                    ]
                )
        
        if _dbRows_cotizaciones:
            _D_result.dbRow_ultima_cotizacion = _dbRows_cotizaciones.last()
            _D_result.E_return = CLASS_e_RETURN.OK
        else:
            pass
        
        return _D_result
    pass
dbc01.define_table(
    'tempresa_proveedor_cotizaciones',
    Field('empresa_proveedor_id', dbc01.tempresa_proveedores, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Proveedor', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('fechavigencia_inicial', type = FIELD_UTC_DATETIME, default=request.browsernow,
      required=True, requires = IS_DATETIME(format= STV_FWK_APP.FORMAT.s_DATETIME),
      notnull=True,
      widget=stv_widget_inputDateTime, label='Fecha vigencia inicial', comment=None,
      writable=True, readable=True,
      represent=stv_represent_datetime), 
    Field('fechavigencia_final', type = FIELD_UTC_DATETIME, default=None,
      required=True, requires = IS_DATETIME(format= STV_FWK_APP.FORMAT.s_DATETIME),
      notnull = True,
      widget = stv_widget_inputDateTime, label = 'Fecha vigencia final', comment = None,
      writable = True, readable = True,
      represent = stv_represent_datetime),
    Field('estatus', 'integer', default = 1,
      required = False, requires = IS_IN_SET(TEMPRESA_PROVEEDOR_COTIZACIONES.ESTATUS.get_dict(), zero = 0, error_message = 'Selecciona' ),
      notnull = False, unique = False,
      widget = stv_widget_combobox, 
      label = 'Estatus', comment = None,
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_PROVEEDOR_COTIZACIONES.ESTATUS.get_dict())),
    Field('nombre_cotizar', 'text', length = 100, default = None,
      required = False,
      notnull = False, unique = False,
      widget = stv_widget_input, label = 'Nombre para cotización', comment = None,
      writable = True, readable = True,
      represent = stv_represent_string),    
    Field('email_cotizar', 'text', length = 150, default = None,
      required = False, requires=IS_NULL_OR(IS_MATCH(D_stvFwkHelper.common.regexp_email, error_message='E-mail invalido.')),
      notnull = False, unique = False,
      widget = stv_widget_inputEmail, label = 'E-mail para cotización', comment = None,
      writable = True, readable = True,
      represent = stv_represent_string),
    Field('numero_cotizar', 'text', length = 10, default = None,
      required = False,
      notnull = False, unique = False,
      widget = stv_widget_input, label = 'Número tel. para cotización', comment = None,
      writable = True, readable = True,
      represent = stv_represent_string),   
    Field('nombre_ordenar', 'text', length = 100, default = None,
      required = False,
      notnull = False, unique = False,
      widget = stv_widget_input, label = 'Nombre para ordernar', comment = None,
      writable = True, readable = True,
      represent = stv_represent_string),    
    Field('email_ordenar', 'text', length = 150, default = None,
      required = False, requires=IS_NULL_OR(IS_MATCH(D_stvFwkHelper.common.regexp_email, error_message='E-mail invalido.')),
      notnull = False, unique = False,
      widget = stv_widget_inputEmail, label = 'E-mail para ordenar', comment = None,
      writable = True, readable = True,
      represent = stv_represent_string),
    Field('numero_ordenar', 'text', length = 10, default = None,
      required = False,
      notnull = False, unique = False,
      widget = stv_widget_input, label = 'Número tel. para ordenar', comment = None,
      writable = True, readable = True,
      represent = stv_represent_string),   
    Field('observacion', 'string', length=100, default=None,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_input, label='Observación', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string), 
    tSignature_dbc01,
    format='%(id)s',
    singular='Cotización',
    plural='Cotizaciones',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )


class TEMPRESA_PROVEEDOR_COTIZACION_PRODUCTOS:
    
    class ESTATUS:
        ''' Se definen las opciónes del campo '''
        PENDIENTE_ACTUALIZAR = 0
        NO_MANEJA = 1
        COTIZADO = 2
        POR_EVENTO = 3
        
        @classmethod
        def get_dict(cls):
            return {
                cls.PENDIENTE_ACTUALIZAR: 'Pendiente actualizar',
                cls.NO_MANEJA: 'No maneja',
                cls.COTIZADO: 'Cotizado',
                cls.POR_EVENTO: 'Cotizado por evento'
                }  
            
    @classmethod
    def PRODUCTOS_NUEVOS(cls, s_empresa_id, s_proveedor_id, L_productos_pasados_id):
        """Función para regresar los productos que no estén dados de alta en esta tabla.
            
            Args:
                dbRows_productos: Query con los productos a insertar.
                s_empresa_proveedor_preciooficial_id: ID de empresa_proveedor_preciosoficiales
            
        """
        _D_result = Storage(
            E_return = CLASS_e_RETURN.NOK_ERROR,
            dbRows_productos_nuevos = None, #Variable donde contendrá los productos nuevos
            )
        
        _dbRows_productos_nuevos = dbc01(
            (dbc01.tempresa_prodserv.empresa_id == s_empresa_id)
            &(dbc01.tempresa_prodserv.preciotipocalculo == TEMPRESA_PRODSERV.E_TIPOCALCULO.PRECIO_COTIZADO)
            &(~dbc01.tempresa_prodserv.id.belongs(L_productos_pasados_id))
            ).select(
                dbc01.tempresa_prodserv.ALL,
                dbc01.tempresa_prodserv_codigosproveedores.ALL,
                orderby =[
                    dbc01.tempresa_prodserv.id,
                    dbc01.tempresa_prodserv_codigosproveedores.id
                    ],
                left =[
                    dbc01.tempresa_prodserv_codigosproveedores.on(
                        (dbc01.tempresa_prodserv_codigosproveedores.empresa_prodserv_id == dbc01.tempresa_prodserv.id)
                        &(dbc01.tempresa_prodserv_codigosproveedores.empresa_proveedor_id == s_proveedor_id)
                        &(dbc01.tempresa_prodserv_codigosproveedores.contenido == 1) #TODO: ¿Hay bronca si dejo esto como 1 en vez de crear una constante? 
                        ), 
                    ]
                )
        
        if _dbRows_productos_nuevos:
            _D_result.dbRows_productos_nuevos = _dbRows_productos_nuevos
            _D_result.E_return = CLASS_e_RETURN.OK
        else:
            pass
        
        return _D_result
            
    @classmethod
    def INSERTAR_PRODUCTOS(cls, dbRows_productos, s_empresa_proveedor_cotizacion_id):
        """Función para insertar los productos.
            
            Args:
                dbRows_productos: Query con los productos a insertar.
                s_empresa_proveedor_preciooficial_id: ID de empresa_proveedor_preciosoficiales
            
        """
        
        _L_productos_id = []
        
        for _dbRow_producto in dbRows_productos:
            
            
            if _dbRow_producto.tempresa_prodserv.id not in _L_productos_id:
            #Se verifica que no se haya guardado este producto ya en el detalle y que contenga código del proveedor

                if _dbRow_producto.tempresa_prodserv_codigosproveedores.codigo:
                    _s_codigoexterno = _dbRow_producto.tempresa_prodserv_codigosproveedores.codigo
                else:
                    _s_codigoexterno = ""
                    
                #Diccionario para insertar los productos.
                _D_insert_Helper = Storage (
                    tempresa_proveedor_cotizacion_id = s_empresa_proveedor_cotizacion_id,
                    empresa_producto_id = _dbRow_producto.tempresa_prodserv.id,
                    unidad_id = _dbRow_producto.tempresa_prodserv.unidad_id,
                    codigoproveedor = _s_codigoexterno,
                    estatus = TEMPRESA_PROVEEDOR_COTIZACION_PRODUCTOS.ESTATUS.PENDIENTE_ACTUALIZAR,
                    )
                
                dbc01.tempresa_proveedor_cotizacion_productos.insert(**_D_insert_Helper)
                
                #Se guarda el producto ID para que no pueda repetirse
                _L_productos_id.append(_dbRow_producto.tempresa_prodserv.id)
                
            else:
                
                pass
            
        return

    pass
dbc01.define_table(
    'tempresa_proveedor_cotizacion_productos',
    Field('tempresa_proveedor_cotizacion_id', dbc01.tempresa_proveedor_cotizaciones, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Cotización', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('empresa_producto_id', dbc01.tempresa_prodserv, default=None,
        required=False,
        ondelete='NO ACTION', notnull=False, unique=False,
        widget = lambda f, v: stv_widget_db_search(f, v,
            s_display = '%(descripcion)s', 
            s_url = URL( a = 'app_almacenes', c = '600busquedas', f = 'buscar_producto'),
            D_additionalAttributes = {'reference':dbc01.tempresa_prodserv.id}
            ),
        label= 'Producto', comment=None,
        writable=True, readable=True,
        represent=lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_proveedor_preciooficial_productos.empresa_producto_id)),
    Field('unidad_id', 'integer', default=None,
        required=False, requires = IS_NULL_OR(IS_IN_DB(db01,'tunidades.id','%(nombre)s [%(c_claveunidad)s]')),
        ondelete='NO ACTION', notnull=False, unique=False,
        widget = lambda f, v: stv_widget_db_search(f, v, 
            s_display = '%(nombre)s [%(c_claveunidad)s]', 
            s_url = URL( a = 'app_generales', c = '005genericos_sat', f = 'unidades_buscar'),
            D_additionalAttributes = {'reference':db01.tunidades.id}
            ), 
        label= 'Unidad', comment=None,
        writable=True, readable=True,
        represent=lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_proveedor_preciooficial_productos.unidad_id)),  
    Field('codigoproveedor', 'text', length = 100, default = None,
      required = False,
      notnull = False, unique = False,
      widget = stv_widget_input, label = 'Código del proveedor', comment = None,
      writable = True, readable = True,
      represent = stv_represent_string),
    Field('estatus', 'integer', default = TEMPRESA_PROVEEDOR_COTIZACION_PRODUCTOS.ESTATUS.PENDIENTE_ACTUALIZAR,
      required = False, requires = IS_IN_SET(TEMPRESA_PROVEEDOR_COTIZACION_PRODUCTOS.ESTATUS.get_dict(), zero = 0, error_message = 'Selecciona' ),
      notnull = False, unique = False,
      widget = stv_widget_combobox, 
      label = 'Estatus', comment = None,
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_PROVEEDOR_COTIZACION_PRODUCTOS.ESTATUS.get_dict())),    
    Field('cantidad_menudeo', 'integer', default=0,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_inputInteger, label='Cantidad de menudeo', comment=None,
      writable=True, readable=True,
      represent=stv_represent_number),    
    Field('precio_menudeo', 'decimal(14,4)', default=0,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_inputMoney, label='Precio de menudeo', comment=None,
      writable=True, readable=True,
      represent=stv_represent_money_ifnotzero),
    Field('cantidad_mediomayoreo', 'integer', default=0,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_inputInteger, label='Cantidad de medio mayoreo', comment=None,
      writable=True, readable=True,
      represent=stv_represent_number),    
    Field('precio_mediomayoreo', 'decimal(14,4)', default=0,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_inputMoney, label='Precio de medio mayoreo', comment=None,
      writable=True, readable=True,
      represent=stv_represent_money_ifnotzero),
    Field('cantidad_mayoreo', 'integer', default=0,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_inputInteger, label='Cantidad de mayoreo', comment=None,
      writable=True, readable=True,
      represent=stv_represent_number),    
    Field('precio_mayoreo', 'decimal(14,4)', default=0,
      required=False,
      notnull=False, unique=False,
      widget=stv_widget_inputMoney, label='Precio de mayoreo', comment=None,
      writable=True, readable=True,
      represent=stv_represent_money_ifnotzero),
    tSignature_dbc01,
    format='%(descripcion)s',
    singular='Producto',
    plural='Productos',
    migrate=D_stvSiteCfg.b_requestIsBasedApp
    )