# -*- coding: utf-8 -*-

""" Este archivo contiene las definiciónes de los modelos genéricos por cuenta.

Definiciones de modelos que se comparten entre las aplicaciones de una cuenta ya que se ubican
 en la base de datos específica por cuenta.
En general es información usada por el framework.
"""


class TEMPRESA_CLIENTES(Table):

    S_NOMBRETABLA = 'tempresa_clientes'

    class E_TIPOCLIENTE:
        CLIENTE_NACIONAL = 0
        CLIENTE_EXTRANJERO = 1
        CLIENTE_NACIONAL_PARTE_RELACIONADA = 2
        CLIENTE_EXTRANJERO_PARTE_RELACIONADA = 3
        D_TODOS = {
            CLIENTE_NACIONAL                    : 'Cliente nacional',
            CLIENTE_EXTRANJERO                  : 'Cliente extranjero',
            CLIENTE_NACIONAL_PARTE_RELACIONADA  : 'Cliente nacional parte relacionada',
            CLIENTE_EXTRANJERO_PARTE_RELACIONADA: 'Cliente extranjero parte relacionada',
            }

        @classmethod
        def get_dict(cls):
            return cls.D_TODOS

    class CREDITO:
        class E_POLITICA:
            SIN_POLITICA = 0
            CERRADA = 1
            ABIERTA = 2
            D_TODOS = {
                SIN_POLITICA: 'Sin política',
                CERRADA     : 'Linea de crédito cerrada si presenta saldos vencidos',
                ABIERTA     : 'Linea de crédito abierta con saldos vencidos',
                }

            @classmethod
            def GET_DICT(cls):
                return cls.D_TODOS

    class E_PROCESOESTATUS:
        SIN_PROCESO = 0
        CFDIS_PROCESANDO = 1
        CFDIS_APLICANDO_SALDOS = 2
        D_TODOS = {
            SIN_PROCESO           : 'Sin proceso',
            CFDIS_PROCESANDO      : 'Procesando CFDIs',
            CFDIS_APLICANDO_SALDOS: 'Aplicando saldos de CFDIs',
            }

        @classmethod
        def GET_DICT(cls):
            return cls.D_TODOS

    class E_FACTURARCOMPLEMENTOS:
        SIN_DEFINIR = 0
        MISMAFACTURA = 1
        FACTURASEPARADA = 2
        AJUSTARPRECIO = 3
        D_TODOS = {
            SIN_DEFINIR    : 'Sin definir',
            MISMAFACTURA   : 'Facturar productos y complementos en la misma factura',
            FACTURASEPARADA: 'Facturar productos y complementos por separado',
            AJUSTARPRECIO  : 'Ajusta precio para incluir precio del complemento',
            }

        @classmethod
        def GET_DICT(cls):
            return cls.D_TODOS

    L_DBCAMPOS = [
        Field(
            'empresa_id', dbc01.tempresas, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'Empresa', comment = None,
            writable = False, readable = False,
            represent = None
            ),
        Field(
            'codigo', 'string', length = 100, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Código', comment = 'Código interno del cliente',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'razonsocial', 'string', length = 254, default = None,
            required = True,
            notnull = True, unique = False,
            widget = stv_widget_input, label = 'Razón Social', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'openbravo_id', 'string', length = 32, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Open Bravo ID', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'nombrecorto', 'string', length = 60, default = None,
            required = True,
            notnull = True, unique = False,
            widget = stv_widget_input, label = 'Nombre Corto', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'tipocliente', 'integer', default = E_TIPOCLIENTE.CLIENTE_NACIONAL,
            required = False,
            requires = IS_IN_SET(E_TIPOCLIENTE.D_TODOS, zero = 0, error_message = 'Selecciona'),
            notnull = False, unique = False,
            widget = stv_widget_combobox, label = 'Tipo de Cliente', comment = '',
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_CLIENTES.E_TIPOCLIENTE.D_TODOS)
            ),
        Field(  # TODO incluir formas en que clientes fueron ingresados para requerir mas o menos información
            'clientedepiso', 'integer', default = 0,
            required = False,
            # requires = IS_IN_SET(E_TIPOCLIENTE.D_TODOS, zero = 0, error_message = 'Selecciona'),
            notnull = False, unique = False,
            widget = stv_widget_combobox, label = 'Cliente de piso',
            comment = 'Identifica como fue ingresado el cliente',
            writable = True, readable = True,
            # represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_CLIENTES.E_TIPOCLIENTE.D_TODOS)
            ),
        Field(
            'rfc', 'string', length = 15, default = None,
            required = True, requires = IS_MATCH(
                D_stvFwkHelper.common.regexp_rfc, error_message = 'Expresión inválida formato AAAA-999999-XXX'
                ),
            notnull = True, unique = False,
            widget = stv_widget_input, label = 'RFC', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'cliente_codigo_contpaqi', 'string', length = 100, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Código CONTPAQi del cliente',
            comment = 'Código utilizado dentro del CONTAPQi del cliente',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'empresa_grupo_id', 'integer', default = None,
            required = False,
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Grupo',
            comment = 'Grupo al que pertenece este cliente.',
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_clientes.empresa_grupo_id)
            ),

        # Campo para ver si el registro ya fue verificado anteriormente en CONTPAQi
        Field(
            'verificacioncontpaqi', 'boolean', default = 0,
            required = False,
            notnull = False,
            widget = stv_widget_inputCheckbox, label = 'Verificación CONTPAQi', comment = None,
            writable = True, readable = True,
            represent = stv_represent_boolean
            ),

        # Campos usados para el manejo de crédito
        Field(
            'creditoactivo', 'boolean', default = False,
            required = False,
            notnull = False,
            widget = stv_widget_inputCheckbox, label = 'Crédito Activo', comment = None,
            writable = True, readable = True,
            represent = stv_represent_boolean
            ),
        Field(
            'fechaprimeraventa', 'date', default = request.browsernow,
            required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
            notnull = False,
            widget = stv_widget_inputDate, label = 'Fecha Primera Venta', comment = None,
            writable = False, readable = True,
            represent = stv_represent_date
            ),
        Field(
            'credito_limite', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Límite de crédito', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        Field(
            'credito_plazo', 'integer', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputInteger, label = 'Plazo (días)', comment = 'Días de crédito',
            writable = True, readable = True,
            represent = stv_represent_number
            ),
        Field(
            'credito_diacorte', 'integer', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputInteger, label = 'Día de corte', comment = 'Dia límite para ajuste de intereses',
            writable = True, readable = True,
            represent = stv_represent_number
            ),
        Field(
            'credito_diapago', 'integer', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputInteger, label = 'Día de pago', comment = 'Dia límite para pagar saldo',
            writable = True, readable = True,
            represent = stv_represent_number
            ),
        Field(
            'credito_interesanual', 'decimal(4,2)', default = 5,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputPercentage, label = 'Interes moratorio anual',
            comment = 'Porcentaje de interes anual en caso de incumplir el pago del crédito, '
                      + 'ajustado y cobrado mensualmente',
            writable = True, readable = True,
            represent = stv_represent_percentage
            ),
        Field(
            'credito_politica', 'integer', default = CREDITO.E_POLITICA.SIN_POLITICA,
            required = False,
            requires = IS_IN_SET(CREDITO.E_POLITICA.GET_DICT(), zero = 0, error_message = 'Selecciona'),
            notnull = False, unique = False,
            widget = stv_widget_combobox,
            label = 'Política', comment = 'Política en el manejo del crédito',
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_CLIENTES.CREDITO.E_POLITICA.GET_DICT())
            ),
        Field(
            'credito_saldo', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Saldo Total', comment = 'Último saldo calculado',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_money_ifnotzero(
                v, r, dbField = dbc01.tempresa_clientes.credito_saldo
                )
            ),
        Field(
            'credito_saldopendtimbrar', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'S. Pend. Timbrar', comment = 'Saldo Pendiente de Timbrar',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_money_ifnotzero(
                v, r, dbField = dbc01.tempresa_clientes.credito_saldopendtimbrar
                )
            ),
        Field(
            'credito_saldotimbrado', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Saldo Timbrado', comment = 'Saldo Timbrado',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_money_ifnotzero(
                v, r, dbField = dbc01.tempresa_clientes.credito_saldotimbrado
                )
            ),
        Field(
            'credito_saldovencido', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Saldo Vencido', comment = 'Último saldo vencido calculado',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_money_ifnotzero(
                v, r, dbField = dbc01.tempresa_clientes.credito_saldovencido
                )
            ),

        # Lista Negra
        Field(  # Una vez activada la lista negra, se require permiso para removerlo de la lista
            'listanegra_esta', 'boolean', default = False,
            required = False,
            notnull = False,
            widget = stv_widget_inputCheckbox, label = 'Cliente en lista negra', comment = None,
            writable = True, readable = True,
            represent = stv_represent_boolean
            ),
        Field(  # Se define al momento de activar la lista negra
            'listanegra_fecha', 'date', default = request.browsernow,
            required = False, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
            notnull = False,
            widget = stv_widget_inputDate, label = 'Fecha Ingreso a lista negra', comment = None,
            writable = False, readable = True,
            represent = stv_represent_date
            ),
        Field(
            'listanegra_saldo', 'decimal(14,4)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputMoney, label = 'Saldo', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        Field(
            'listanegra_observaciones', 'text', default = None,
            required = False,
            notnull = False,
            label = 'Observaciones', comment = "",
            writable = True, readable = True,
            represent = stv_represent_text
            ),

        Field(
            'procesostatus', 'integer', default = E_PROCESOESTATUS.SIN_PROCESO,
            required = False,
            requires = IS_IN_SET(E_PROCESOESTATUS.GET_DICT(), zero = 0, error_message = 'Selecciona'),
            notnull = False, unique = False,
            widget = stv_widget_combobox, label = 'Proceso', comment = '',
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_CLIENTES.E_PROCESOESTATUS.GET_DICT())
            ),

        Field(
            'observaciones', 'text', default = None,
            required = False,
            notnull = False,
            label = 'Observaciones', comment = "",
            writable = True, readable = True,
            represent = stv_represent_text
            ),

        Field(
            'facturacomplementos', 'integer', default = E_FACTURARCOMPLEMENTOS.MISMAFACTURA,
            required = False, requires = IS_IN_SET(
                E_FACTURARCOMPLEMENTOS.GET_DICT(), zero = 0, error_message = 'Selecciona'
                ),
            notnull = False, unique = False,
            widget = stv_widget_combobox,
            label = 'Facturación de complementos', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_CLIENTES.E_FACTURARCOMPLEMENTOS.GET_DICT())
            ),

        Field(
            'receptorusocfdi_id', 'integer', default = TUSOSCFDI.ADQUISICION_MERCANCIA,
            required = False, requires = IS_NULL_OR(IS_IN_DB(db01, 'tusoscfdi.id', db01.tusoscfdi._format)),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Uso CFDI', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_clientes.receptorusocfdi_id)
            ),
        tSignature_dbc01,
        ]

    def __init__(self, dbRef, tablename, *fields, **args):
        Table.__init__(self, dbRef, tablename, *fields, **args)
        self.PROC.dbTabla = self
        return

    class PROC:

        dbTabla = None  # Se escribe con la inicialización

        def __init__(self, **D_defaults):
            """ Inicializa objecto de tipo PROC para control del registro actual

            @param D_defaults:
            @type D_defaults:
            """
            self._D_defaults = Storage(D_defaults)
            self._L_requeridos = ['empresa_id']
            self.dbTabla.notas.readable = True
            return

        def puede_crear(self, **D_defaults):
            """ Determina si puede o no crear un registro.

            Actualmente no hay condición requerida.
            """
            _D_return = FUNC_RETURN()

            self._D_defaults.update(D_defaults or {})

            for _s_requerido in self._L_requeridos:
                if _s_requerido not in self._D_defaults:
                    _D_return.agrega_error(
                        "Campo {campo} es requerido para generar {tabla}".format(
                            campo = _s_requerido,
                            tabla = self.dbTabla._singular
                            )
                        )

                else:
                    pass

            return _D_return

        @classmethod
        def PUEDE_EDITAR(cls, x_cliente):
            """ Regresa OK si el estado permite modificar, de lo contrario False

            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cliente, cls.dbTabla)
            _dbRow = _D_results.dbRow

            if not _dbRow:
                _D_return.agrega_error("{tabla} no identificado.".format(tabla = cls.dbTabla._singular))

            else:
                pass

            return _D_return

        @classmethod
        def PUEDE_ELIMINAR(cls, x_cliente):
            """ Determina si puede o no eliminar un registro

            Misma condición que puede_editar, a lo mejor pudieran verse condiciones distintas

            """

            return cls.PUEDE_EDITAR(x_cliente)

        def configuracampos_nuevo(
                self,
                s_cliente_id,
                b_llamadaDesdeEditar = False,
                **D_defaults
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param b_llamadaDesdeEditar:
            @type b_llamadaDesdeEditar:
            @param s_cliente_id:
            @type s_cliente_id:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            self._D_defaults.update(D_defaults or {})
            for _s_fieldName in self.dbTabla.fields:
                self.dbTabla[_s_fieldName].writable = False
            for _s_fieldName in self._D_defaults:
                self.dbTabla[_s_fieldName].default = self._D_defaults[_s_fieldName]

            if not b_llamadaDesdeEditar:
                _D_results = self.puede_crear()
            else:
                _D_results = _D_return

            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.combinar(_D_results)

            elif s_cliente_id:
                _dbRow_base = self.dbTabla(s_cliente_id)

                self.dbTabla.empresa_id.default = _dbRow_base.empresa_id
                # self.dbTabla.codigo.default = _dbRow_base.codigo
                # self.dbTabla.razonsocial.default = _dbRow_base.razonsocial
                # self.dbTabla.nombrecorto.default = _dbRow_base.nombrecorto
                self.dbTabla.tipocliente.default = _dbRow_base.tipocliente
                # self.dbTabla.rfc.default = _dbRow_base.rfc
                # self.dbTabla.cliente_codigo_contpaqi.default = _dbRow_base.cliente_codigo_contpaqi
                self.dbTabla.empresa_grupo_id.default = _dbRow_base.empresa_grupo_id
                # self.dbTabla.verificacioncontpaqi.default = _dbRow_base.verificacioncontpaqi
                # self.dbTabla.credito_limite.default = _dbRow_base.credito_limite
                # self.dbTabla.credito_plazo.default = _dbRow_base.credito_plazo
                # self.dbTabla.credito_politica.default = _dbRow_base.credito_politica
                # self.dbTabla.credito_saldo.default = _dbRow_base.credito_saldo
                # self.dbTabla.credito_saldopendtimbrar.default = _dbRow_base.credito_saldopendtimbrar
                # self.dbTabla.credito_saldotimbrado.default = _dbRow_base.credito_saldotimbrado
                # self.dbTabla.credito_saldovencido.default = _dbRow_base.credito_saldovencido
                # self.dbTabla.procesostatus.default = _dbRow_base.procesostatus
                # self.dbTabla.observaciones.default = _dbRow_base.observaciones
                # self.dbTabla.facturacomplementos.default = _dbRow_base.facturacomplementos
                self.dbTabla.receptorusocfdi_id.default = _dbRow_base.receptorusocfdi_id

            else:
                pass

            self.dbTabla.empresa_id.writable = False
            self.dbTabla.codigo.writable = True
            self.dbTabla.razonsocial.writable = True
            self.dbTabla.nombrecorto.writable = True
            self.dbTabla.tipocliente.writable = True
            self.dbTabla.rfc.writable = True
            self.dbTabla.cliente_codigo_contpaqi.writable = True
            self.dbTabla.empresa_grupo_id.writable = True
            self.dbTabla.verificacioncontpaqi.writable = True
            self.dbTabla.credito_limite.writable = False
            self.dbTabla.credito_plazo.writable = False
            self.dbTabla.credito_politica.writable = False
            self.dbTabla.observaciones.writable = True
            self.dbTabla.facturacomplementos.writable = True
            self.dbTabla.receptorusocfdi_id.writable = True

            # Campos que no deben ser editados, solo de forma autocalculada
            self.dbTabla.credito_saldo.writable = False
            self.dbTabla.credito_saldopendtimbrar.writable = False
            self.dbTabla.credito_saldotimbrado.writable = False
            self.dbTabla.credito_saldovencido.writable = False
            self.dbTabla.procesostatus.writable = False

            # Campos que todos los movimientos pueden editar
            self.dbTabla.notas.writable = True

            # Se definen los valores por default obligados
            self.dbTabla.empresa_id.default = self._D_defaults.empresa_id

            self.dbTabla.empresa_grupo_id.requires = IS_IN_DB(
                dbc01(
                    (dbc01.tempresa_grupos.empresa_id == self._D_defaults.empresa_id)
                    & (dbc01.tempresa_grupos.cliente == True)
                    ),
                'tempresa_grupos.id',
                dbc01.tempresa_grupos._format
                )

            return _D_return

        def configuracampos_edicion(
                self,
                x_cliente,
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param x_cliente: es el registro del movimiento o su id
            @type x_cliente:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cliente, self.dbTabla)
            _dbRow = _D_results.dbRow

            # Se llenan los default obligados
            for _s_nombreCampo in self._L_requeridos:
                self._D_defaults[_s_nombreCampo] = _D_results.dbRow[_s_nombreCampo]

            _D_results = self.PUEDE_EDITAR(_dbRow)

            if _D_results.E_return == CLASS_e_RETURN.OK:
                # Si puede editar regresa un ok y el movimiento esta abierto

                # Se manda vacío el argumento para no agregar defaults a los campos
                _D_results = self.configuracampos_nuevo(
                    None,
                    b_llamadaDesdeEditar = True,
                    **self._D_defaults
                    )
                _D_return.combinar(_D_results)

                # TODOMejora la información de crédito sólo se pudiera modificar con ciertos permisos
                self.dbTabla.credito_limite.writable = True
                self.dbTabla.credito_plazo.writable = True
                self.dbTabla.credito_politica.writable = True

            else:
                # De lo contrario en cualquier otro caso, solo se pueden editar las observaciones
                for _s_fieldName in self.dbTabla.fields:
                    self.dbTabla[_s_fieldName].writable = False

                self.dbTabla.notas.writable = True

            return _D_return

        def al_validar(
                self,
                O_form,
                ):
            """ Validar la información para poder grabar

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _dbRow = self.dbTabla(O_form.record_id) if O_form.record_id else None
            _D_camposAChecar = STV_FWK_LIB.OBTENER_CAMPOSACHECAR_PARA_VALIDAR(self.dbTabla, _dbRow, O_form)
            _b_cambioRfc = False

            if not O_form.record_id:
                # Se esta creando el registro

                _D_results = self.puede_crear(**_D_camposAChecar)
                if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                    O_form.errors.id = _D_results.s_msgError
                else:
                    pass

                _b_cambioRfc = True

            else:
                # Se esta modificando el registro, la llamada a configuraCampos_edicion ya definió con writable
                #  que puede modificarse y que no. Por lo que el código para verificar si puede o no editar
                #  no es requerido en este caso.

                _b_cambioRfc = _D_camposAChecar.rfc != O_form.record.rfc

            # Validaciones

            if not _D_camposAChecar.empresa_id:
                O_form.errors.id = "Empresa debe ser especificada"
            else:
                pass
            if not _D_camposAChecar.receptorusocfdi_id:
                O_form.errors.receptorusocfdi_id = "Uso CFDI debe definirse"
            else:
                pass
            if not _D_camposAChecar.rfc:
                O_form.errors.rfc = 'RFC no puede estar vacío'

            elif _b_cambioRfc:
                _dbRow_empresa = dbc01.tempresas(_D_camposAChecar.empresa_id)
                if _D_camposAChecar.rfc in (_dbRow_empresa.rfc_publico, _dbRow_empresa.rfc_publico_extranjeros):
                    # No hay problema con el RFC, ahora se verifica que el nombre no se repita

                    if dbc01(
                            (dbc01.tempresa_clientes.empresa_id == _D_camposAChecar.empresa_id)
                            & (dbc01.tempresa_clientes.razonsocial == _D_camposAChecar.razonsocial)
                            & (dbc01.tempresa_clientes.id != O_form.record_id)
                            ).count() > 0:
                        O_form.errors.razonsocial = 'Ya existe razonsocial con otro cliente usando RFC público'
                    else:
                        # No existe problema en la validación
                        pass

                    if _D_camposAChecar.cliente_codigo_contpaqi and dbc01(
                            (dbc01.tempresa_clientes.empresa_id == _D_camposAChecar.empresa_id)
                            & (
                                dbc01.tempresa_clientes.cliente_codigo_contpaqi
                                == _D_camposAChecar.cliente_codigo_contpaqi
                                )
                            & (dbc01.tempresa_clientes.rfc != _D_camposAChecar.rfc)
                            & (dbc01.tempresa_clientes.id != O_form.record_id)
                            ).count() > 0:
                        O_form.errors.cliente_codigo_contpaqi = 'El código Contpaqi no puede repetirse entre '\
                                                                'RFC publico general y RFC de un cliente '\
                                                                'con RFC propio'
                    else:
                        pass

                else:
                    # Si es un RFC único
                    if dbc01(
                            (dbc01.tempresa_clientes.empresa_id == _D_camposAChecar.empresa_id)
                            & (dbc01.tempresa_clientes.rfc == _D_camposAChecar.rfc)
                            & (dbc01.tempresa_clientes.id != O_form.record_id)
                            ).count() > 0:
                        O_form.errors.rfc = 'RFC ya existe con otro cliente'
                    else:
                        # No existe problema en la validación
                        pass

                    # Validación para ver si el Código contapqi se está repitiendo en RFC distintos a
                    #  Publicos o extranjeros
                    if _D_camposAChecar.cliente_codigo_contpaqi and dbc01(
                            (dbc01.tempresa_clientes.empresa_id == _D_camposAChecar.empresa_id)
                            & (
                                dbc01.tempresa_clientes.cliente_codigo_contpaqi
                                == _D_camposAChecar.cliente_codigo_contpaqi
                                )
                            & (dbc01.tempresa_clientes.id != O_form.record_id)
                            ).count() > 0:
                        O_form.errors.cliente_codigo_contpaqi = 'El código CONTPAQi no puede repetirse '\
                                                                'en diferentes RFC'
                    else:
                        # No existe problema en la validación
                        pass

            else:
                pass

            return O_form

        @classmethod
        def AL_ACEPTAR(cls, O_form):
            """ Aceptación captura de inventario inicial producto

            Se hacen todos los cálculos y aceptación de los cambios en el producto

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()
            dbRow = cls.dbTabla(O_form.record_id or O_form.vars.id)

            if not O_form.record_id:
                # Si se inserta
                pass

            else:  # Si se edita
                pass

            _D_results = cls.MAPEAR_CAMPOS(dbRow)
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.combinar(_D_results)

            else:
                _D_results = cls.CALCULAR_CAMPOS(dbRow)
                _D_return.combinar(_D_results)

            generaMensajeFlash(cls.dbTabla, _D_return)
            return O_form

        @classmethod
        def ANTES_ELIMINAR(cls, L_errorsAction, dbRow):
            """ Antes de eliminar un movimiento de inventario de este tipo

            @param dbRow:
            @type dbRow:
            @param L_errorsAction:
            @type L_errorsAction:
            @return: errores
            @rtype: lista de diccionarios {'msg':'...'}
            """
            _D_results = cls.PUEDE_ELIMINAR(dbRow)
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                L_errorsAction.append({'msg': _D_results.s_msgError})
            else:
                cls._dbRow_aEliminar = Storage(dbRow.as_dict())

            return L_errorsAction

        @classmethod
        def DESPUES_ELIMINAR(cls, L_errorsAction):
            """ Después de eliminar un registro

            @param L_errorsAction:
            @type L_errorsAction:
            @return: errores
            @rtype: lista de diccionarios {'msg':'...'}
            """
            _D_return = FUNC_RETURN()

            if cls._dbRow_aEliminar:
                pass
            else:
                _D_return.E_return = CLASS_e_RETURN.NOK_SECUENCIA
                _D_return.s_msgError += "Error en secuencia de llamado de funciones para borrado. "

            generaMensajeFlash(cls.dbTabla, _D_return)
            return L_errorsAction

        @classmethod
        def MAPEAR_CAMPOS(cls, x_cliente):
            """ Remapea campos solamente cuando se graba el registro

            @param x_cliente:
            @type x_cliente:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cliente, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage()

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)
                    break
                else:
                    pass

            return _D_return

        @classmethod
        def CALCULAR_CAMPOS(cls, x_cliente):
            """ Recalcula campos que tienen impacto cuando otros registros de otras tablas cambian
            Esta operación así mismo detecta y agrega las inconsistencias

            @param x_cliente:
            @type x_cliente:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cliente, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage()

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)
                    break
                else:
                    pass

            return _D_return

    def comando_refrescar(self, x_cliente):
        _L_camposActualizar = STV_LIB_DB.COMANDO_REFRESCAR(
            x_cliente,
            self,
            [
                self.credito_saldo,
                self.credito_saldovencido,
                self.credito_saldopendtimbrar,
                self.credito_saldotimbrado,
                ]
            )
        return _L_camposActualizar

    @classmethod
    def OBTENER_SALDO_CLIENTE(cls, x_cliente_id, x_cfdi_id_ignorar = None):

        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            f_saldoCliente = 0,
            f_saldoVencidoCliente = 0,
            f_saldoPendienteTimbrar = 0,
            f_saldoTimbrado = 0   
            )

        if isinstance(x_cliente_id, (str, int)):
            _dbRow_cliente = dbc01.tempresa_clientes(x_cliente_id)
        else:
            _dbRow_cliente = x_cliente_id
        
        if not x_cfdi_id_ignorar:
            _n_cfdi_ignorar = None
        elif isinstance(x_cfdi_id_ignorar, (str, int)):
            _n_cfdi_ignorar = x_cfdi_id_ignorar
        else:
            # Se espera que sea un dbRow
            _n_cfdi_ignorar = x_cfdi_id_ignorar.id
            
        if _dbRow_cliente.procesostatus != TEMPRESA_CLIENTES.E_PROCESOESTATUS.SIN_PROCESO:
            _D_return.s_msgError = "No se puede grabar saldo, si el cliente esta en proceso"
            _D_return.E_return = stvfwk2_e_RETURN.OK_WARNING
        else:
            pass
        
        _dbRows = dbc01(
            (dbc01.tcfdis.receptorcliente_id == _dbRow_cliente.id)
            & (dbc01.tcfdis.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO)
            & (dbc01.tcfdis.sat_status == TCFDIS.SAT_STATUS.VIGENTE)
            & (dbc01.tcfdis.saldo != 0)
            & (dbc01.tcfdis.id != _n_cfdi_ignorar)
            & (dbc01.tcfdis.fecha >= TCFDIS.GET_PRIMERDIA2020().dt_browser)
            ).select(
                dbc01.tcfdis.id,
                dbc01.tcfdis.total,
                dbc01.tcfdis.saldo,
                dbc01.tcfdis.fecha,
                dbc01.tcfdis.credito_plazo,
                dbc01.tcfdis.estado,
                dbc01.tcfdis.sat_status,
                )
            
        for _dbRow in _dbRows:
            
            _D_return.f_saldoCliente += _dbRow.saldo
            _dt_fechavencimiento = (
                _dbRow.fecha + datetime.timedelta(days = _dbRow.credito_plazo or 30)
                )
            
            if _dt_fechavencimiento < request.browsernow:
                _D_return.f_saldoVencidoCliente += _dbRow.saldo
            else:
                pass
            
            _D_return.f_saldoTimbrado += _dbRow.saldo

        _dbRows_sinTimbrar = dbc01(
            (dbc01.tcfdis.receptorcliente_id == _dbRow_cliente.id)
            & (dbc01.tcfdis.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO)
            & (dbc01.tcfdis.sat_status == TCFDIS.SAT_STATUS.NO_DEFINIDO)
            & (dbc01.tcfdis.total != 0)
            & (dbc01.tcfdis.id != _n_cfdi_ignorar)
            & (dbc01.tcfdis.fecha >= TCFDIS.GET_PRIMERDIA2020().dt_browser)
            ).select(
            dbc01.tcfdis.id,
            dbc01.tcfdis.total,
            dbc01.tcfdis.saldo,
            dbc01.tcfdis.fecha,
            dbc01.tcfdis.credito_plazo,
            dbc01.tcfdis.estado,
            dbc01.tcfdis.sat_status,
            )

        for _dbRow in _dbRows_sinTimbrar:

            _D_return.f_saldoCliente += _dbRow.total

            # Si el CFDI no esta timbrado, el saldo en el CFDI es zero
            _D_return.f_saldoPendienteTimbrar += _dbRow.total

        _dbRow_cliente.credito_saldo = _D_return.f_saldoCliente
        _dbRow_cliente.credito_saldovencido = _D_return.f_saldoVencidoCliente
        _dbRow_cliente.credito_saldopendtimbrar = _D_return.f_saldoPendienteTimbrar
        _dbRow_cliente.credito_saldotimbrado = _D_return.f_saldoTimbrado
        if not _n_cfdi_ignorar:
            # Si no se esta ignorando algún registro, se graba el saldo del cliente
            if _D_return.f_saldoCliente and not _dbRow_cliente.credito_plazo:
                _dbRow_cliente.credito_plazo = 30
            else:
                pass
            _dbRow_cliente.update_record()
        else:
            pass

        return _D_return

    @staticmethod
    def DEFINIR_DBCAMPO(s_nombreCampo = 'cliente_id', **D_params):
        D_params = Storage(D_params)
        _dbCampo = Field(
            s_nombreCampo, dbc01.tempresa_clientes, default = D_params.default or None,
            required = D_params.required or False,
            requires = D_params.requires or IS_IN_DB(
                dbc01(dbc01.tempresa_clientes.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id),
                'tempresa_clientes.id',
                dbc01.tempresa_clientes._format
                ),
            ondelete = 'NO ACTION', notnull = D_params.notnull or False, unique = False,
            widget = D_params.widget or (lambda f, v: stv_widget_db_chosen(f, v, 1)),
            label = D_params.label or 'Cliente', comment = D_params.comment or "",
            writable = False, readable = True,
            represent = D_params.represent or stv_represent_referencefield
            )

        if not D_params.requires and not D_params.required:
            _dbCampo.requires = IS_NULL_OR(_dbCampo.requires)
        else:
            pass

        return _dbCampo

    pass


dbc01.define_table(
    TEMPRESA_CLIENTES.S_NOMBRETABLA, *TEMPRESA_CLIENTES.L_DBCAMPOS,
    format = '[%(rfc)s] %(razonsocial)s',
    singular = 'Cliente',
    plural = 'Clientes',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TEMPRESA_CLIENTES
    )


class TEMPRESA_CLIENTE_DOMICILIOS(Table):

    S_NOMBRETABLA = 'tempresa_cliente_domicilios'

    class E_TIPO:
        """ Se definen las opciónes del campo """
        NO_DEFINIDO = 0
        FISCAL = 1
        ALMACEN = 2

        @classmethod
        def GET_DICT(cls):
            return {
                cls.NO_DEFINIDO: 'No definido',
                cls.FISCAL     : 'Fiscal',
                cls.ALMACEN    : 'Almacén',
                }

    L_DBCAMPOS = [
        Field(
            'empresa_cliente_id', dbc01.tempresa_clientes, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'Empresa', comment = None,
            writable = False, readable = False,
            represent = None
            ),
        Field(
            'descripcion', 'string', length = 150, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Descripción', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'nombrecontacto', 'string', length = 150, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Contacto', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'emails', 'text', default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Email(s)', comment = 'Emails separados por commas',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'telefonos', 'string', length = 150, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Tel(s)', comment = 'Telefonos separados por |',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'pais_id', 'integer', default = None,
            required = False, requires = IS_IN_DB(db01, 'tpaises.id', db01.tpaises._format),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Pais', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(
                v, r, dbc01.tempresa_cliente_domicilios.pais_id
                )
            ),
        Field(
            'pais_estado_id', 'integer', default = None,
            required = False, requires = IS_IN_DB(db01, 'tpais_estados.id', db01.tpais_estados._format),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda field, value: stv_widget_db_chosen_linked(
                field, value, 1, 'pais_id',
                URL(a = 'app_generales', c = '005genericos_sat', f = 'localidad_pais_estados_ver.json')
                ),
            label = 'Estado', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, dbc01.tempresa_clientes.pais_estado_id)
            ),
        Field(
            'pais_estado_municipio_id', 'integer', default = None,
            required = False,
            requires = IS_IN_DB(db01, 'tpais_estado_municipios.id', db01.tpais_estado_municipios._format),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda field, value: stv_widget_db_chosen_linked(
                field, value, 1, 'pais_estado_id',
                URL(a = 'app_generales', c = '005genericos_sat', f = 'localidad_pais_estado_mun_ver.json')
                ),
            label = 'Municipio', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(
                v, r, dbc01.tempresa_cliente_domicilios.pais_estado_municipio_id
                )
            ),
        Field(
            'ciudad', 'string', length = 120, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Ciudad', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'colonia', 'string', length = 120, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Colonia', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'calle', 'string', length = 100, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Calle', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'referencia', 'string', length = 100, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Referencia', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'numexterior', 'string', length = 55, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Num. Exterior', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'numinterior', 'string', length = 55, default = "",
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Num. Interior', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'codigopostal', 'string', length = 5, default = None,
            required = False,
            requires = IS_EMPTY_OR(IS_LENGTH(maxsize = 5, minsize = 5, error_message = T('enter %(min)g characters'))),
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Código Postal', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'orden', 'integer', default = 100,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputInteger, label = 'Orden',
            comment = 'Orden menor es el usado por el sistema para facturas y correo',
            writable = True, readable = True,
            represent = stv_represent_number
            ),

        Field(
            'tipo', 'integer', default = E_TIPO.FISCAL,
            required = False, requires = IS_IN_SET(E_TIPO.GET_DICT(), zero = 0, error_message = 'Selecciona'),
            notnull = False, unique = False,
            widget = stv_widget_combobox, label = 'Tipo',
            comment = '',
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_list(
                v, r, None, TEMPRESA_CLIENTE_DOMICILIOS.E_TIPO.GET_DICT()
                )
            ),
        tSignature_dbc01,
        ]

    pass


dbc01.define_table(
    TEMPRESA_CLIENTE_DOMICILIOS.S_NOMBRETABLA, *TEMPRESA_CLIENTE_DOMICILIOS.L_DBCAMPOS,
    format = '%(calle)s',
    singular = 'Domicilio',
    plural = 'Domicilios',
    migrate = D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TEMPRESA_CLIENTE_DOMICILIOS
    )


class TEMPRESA_CLIENTE_LOG(Table):

    S_NOMBRETABLA = 'tempresa_cliente_log'

    class TIPOTRANSACCION:
        NO_IDENTIFICADA = 0
        CONSULTA = 1
        MOVIMIENTO = 2
        CALCULODESALDO = 3
        
        @classmethod
        def GET_DICT(cls):
            return {
                cls.NO_IDENTIFICADA: 'No identificada',
                cls.CONSULTA       : 'Consulta',
                cls.MOVIMIENTO     : 'Movimiento',
                cls.CALCULODESALDO : 'Cálculo de Saldo',
                }

    L_DBCAMPOS = [
        Field(
            'empresa_cliente_id', dbc01.tempresa_clientes, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'Cliente', comment = None,
            writable = False, readable = False,
            represent = None
            ),
        Field(
            'fecha', type = TDATETIME_GRABADIRECTO_LEERENZONAH, default = request.utcnow,
            required = False, requires = IS_NULL_OR(IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME)),
            notnull = False,
            widget = stv_widget_inputDateTime, label = 'Fecha', comment = '',
            writable = False, readable = True,
            represent = stv_represent_datetime
            ),
        Field(
            'descripcion', 'string', length = 140, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Descripcion', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'tipotransaccion', 'integer', default = TIPOTRANSACCION.NO_IDENTIFICADA,
            required = False, requires = IS_IN_SET(
                TIPOTRANSACCION.GET_DICT(), zero = 0, error_message = 'Selecciona'
                ),
            notnull = False, unique = False,
            widget = stv_widget_combobox, label = 'Tipo transacción', comment = '',
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TEMPRESA_CLIENTE_LOG.TIPOTRANSACCION.GET_DICT())
            ),
        Field(
            'errores', 'text', default = None,
            required = False,
            notnull = False,
            label = 'Errores', comment = "",
            writable = False, readable = True,
            represent = stv_represent_text
            ),
        Field(
            'warnings', 'text', default = None,
            required = False,
            notnull = False,
            label = 'Advertencias', comment = "",
            writable = False, readable = True,
            represent = stv_represent_text
            ),
        Field(
            'info', 'text', default = None,
            required = False,
            notnull = False,
            label = 'Informativo', comment = "",
            writable = False, readable = True,
            represent = stv_represent_text
            ),
        Field(
            'resultado', 'integer', default = CLASS_e_RETURN.OK,
            required = False, requires = IS_IN_SET(CLASS_e_RETURN.GET_DICT(), zero = 0, error_message = 'Selecciona'),
            notnull = False, unique = False,
            widget = stv_widget_combobox, label = 'Resultado', comment = '',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, CLASS_e_RETURN.GET_DICT())
            ),
        Field(
            'comentarios', 'text', default = None,
            required = False,
            notnull = False,
            label = 'Comentarios', comment = "",
            writable = True, readable = True,
            represent = stv_represent_text
            ),
        tSignature_dbc01,
        ]
    
    pass


dbc01.define_table(
    TEMPRESA_CLIENTE_LOG.S_NOMBRETABLA, *TEMPRESA_CLIENTE_LOG.L_DBCAMPOS,
    format='%(fecha)s %(tipotransaccion)s',
    singular='Log',
    plural='Logs',
    migrate=D_stvSiteCfg.b_requestIsBasedApp,
    table_class = TEMPRESA_CLIENTE_LOG
    )

db01.tpaises._referenced_by.append(dbc01.tempresa_cliente_domicilios.pais_id)
db01.tpaises._references.append(dbc01.tempresa_cliente_domicilios.pais_id)
