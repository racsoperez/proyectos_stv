select f.c_invoice_id, f.ad_client_id, f.ad_org_id, org.name as sucursal, f.c_bpartner_id,
       cliente.name as clienteNombre, cliente.value as clienteCodigo, cliente.taxid as clienteRFC,
       f.isactive, series.prefix, f.documentno, f.docstatus, f.docaction, doct.name, doct.printname, doct.docbasetype,
       o.documentno as ordenNo,
       o.description as ordenDescripcion, salesRep.name as facturaVendedor, o.datepromised as ordenPromesa,
       f.dateinvoiced, clienteLoc.name as clienteDom, clienteLoc.phone as clienteTel, pago.description as pagoDesc,
       pago.netdays as pagoCreditoDias,
       (
           select sum(fconcepto2.linenetamt)
           from c_invoiceline as fconcepto2
           where fconcepto2.c_invoice_id = f.c_invoice_id AND fconcepto2.em_vtapiso_casflag = 'N'
           ) as subtotal,
       impuesto.taxamt,
       (
           select sum(fconcepto.em_vtapiso_descuentofe)
           from c_invoiceline as fconcepto
           where fconcepto.c_invoice_id = f.c_invoice_id
           ) as descuento,
       f.grandtotal as total, f.totalpaid as totalPagado,
       (
           select sum(pagoDoctoRel.amount)
           from fin_payment_schedule as pagoComp, fin_payment_scheduledetail as pagoDoctoRel
           where pagoComp.c_invoice_id = f.c_invoice_id and pagoDoctoRel.fin_payment_schedule_invoice = pagoComp.fin_payment_schedule_id
           and pagoDoctoRel.isinvoicepaid = 'Y'
           ) as pagoSumRelacionadosPPD,
       (
           select sum(pagoDoctoRel3.amount)
           from fin_payment_schedule as pagoComp3, fin_payment_scheduledetail as pagoDoctoRel3
           where pagoComp3.c_invoice_id = f.c_invoice_id and pagoDoctoRel3.fin_payment_schedule_invoice = pagoComp3.fin_payment_schedule_id
           and pagoDoctoRel3.isinvoicepaid = 'N'
           ) as saldoPPD,
       (
           select count(pagoDoctoRel2.fin_payment_schedule_invoice)
           from fin_payment_schedule as pagoComp2, fin_payment_scheduledetail as pagoDoctoRel2
           where pagoComp2.c_invoice_id = f.c_invoice_id and pagoDoctoRel2.fin_payment_schedule_invoice = pagoComp2.fin_payment_schedule_id
           ) as numeroPagos,
       f.daystilldue, f.em_fet_docstatus, f.em_fet_correoelectronico,
       f.em_fet_correoalternativo, f.em_fet_formadepago, metodopago.name as metodoPago, metodopago.description as metodoPagoDesc,
       f.em_fet_fechacancel, f.em_vtadist_iscargocasco,
       f.em_fet_rfcprovcertif, f.em_fet_uso_cfdi, f.em_fet_foliofiscal, f.updated
from c_invoice as f
left join c_bpartner as cliente on cliente.c_bpartner_id = f.c_bpartner_id
left join ad_org as org on f.ad_org_id = org.ad_org_id
left join c_doctype as doct on doct.c_doctype_id = f.c_doctype_id
left join c_order as o on o.c_order_id = f.c_order_id
left join ad_user as salesRep on salesRep.ad_user_id = f.salesrep_id
left join c_bpartner_location as clienteLoc on clienteLoc.c_bpartner_location_id = f.c_bpartner_location_id
left join c_paymentterm as pago on pago.c_paymentterm_id = f.c_paymentterm_id
left join ad_sequence as series on series.ad_sequence_id = doct.docnosequence_id
left join fin_paymentmethod as metodopago on metodopago.fin_paymentmethod_id = f.fin_paymentmethod_id
left join c_invoicetax as impuesto on impuesto.c_invoice_id = f.c_invoice_id and impuesto.taxamt > 0
where f.ad_client_id = '797B28B6CE0E4540BB06A70069CC3AC5'
order by f.dateinvoiced desc