select c.ad_client_id, c.c_bpartner_id, c.isactive, c.isemployee, c.issalesrep, c.isvendor, c.iscustomer,
       c.value, c.name,
       c.name2, c.description, c.ad_language, c.taxid, c.firstsale, c.actuallifetimevalue,
       c.so_creditlimit, c.so_creditused,
       pterm.isactive as ptermIsActive, pterm.name as ptermname, pterm.description as ptermDesc, pterm.netdays as ptermNetDays,
       pterm.value as ptermValue, pterm.isvalid as ptermIsValid, pterm.em_fet_metododepago as ptermMetodoPago,
       c.referenceno, c.salesrep_id, c.em_dmbp_listanegra, c.em_dmbp_tipocliente, c.em_dmbp_coldeuda, c.em_dmbp_coldias,
       c.em_dmbp_address1, c.em_dmbp_city, c.em_dmbp_apostal, c.em_dmbp_c_country_id, c.em_dmbp_address2, c.em_dmbp_tdirm_numex,
       c.em_dmbp_tdirm_numin, c.em_dmbp_phone, c.em_dmbp_ispiso, creg.name as regionName, c.em_dmbp_escargousado,
       c.em_dmbp_email, c.em_fet_razonsocial, c.em_fet_desglosarieps, c.em_vtadist_diarev, c.em_vtadist_diacob,
       c.em_rutas_rutas_id, c.em_rutas_rutasreparto_id, c.em_fet_uso_cfdi, c.em_fet_email_rp,
       (
           select count(f.c_invoice_id)
           from c_invoice as f
           where f.c_bpartner_id = c.c_bpartner_id
           ) as numFacturas,
       c.updated, salesRep.name as vendedor
       from c_bpartner as c
left join c_paymentterm as pterm on pterm.c_paymentterm_id = c.c_paymentterm_id
left join c_region as creg on creg.c_region_id = c.em_dmbp_c_region_id
left join ad_user as salesRep on salesRep.ad_user_id = c.salesrep_id
where c.ad_client_id = '797B28B6CE0E4540BB06A70069CC3AC5' and c.iscustomer = 'Y'
order by c.updated desc

-- Se ignoran los RFCs duplicados