# Paso 1. Se limpia el campo src.stv_id en el source que indicará los registros que ya fueron actualizados
#  Nota: columnas rfc y stv_id deben existir en la tabla espejo de open bravo

update taob_clientes AS src
set src.stv_id = null
where src.stv_id is not null;

update taob_clientes
set rfc = CONCAT(SUBSTRING(`taxid`, 1, 3 + (CHAR_LENGTH(`taxid`)-12)), '-', SUBSTRING(`taxid`, -9, 6), '-', SUBSTRING(`taxid`, -3))
where taxid is not null;

update taob_empresaclientes AS src
set src.procesostatus = 0
where src.procesostatus != 0;


# Paso 2. Se actualiza la información de los clientes donde el RFC coincida y no sea genérico, usando el 8 en procesoStatus;
#  y actualizando el stv_id en la tabla src, para identificar los clientes que no fueron actualizados

UPDATE taob_empresaclientes AS dst, taob_clientes AS src
SET
    dst.openbravo_id = src.c_bpartner_id,
    dst.bansuspender = if(src.isactive = 'Y', 'F', 'Y'),
    dst.codigo = src.value,
    dst.razonsocial = if(src.em_fet_razonsocial is null, src.name, src.em_fet_razonsocial),
    dst.notas = src.description,
    dst.fechaprimeraventa = CAST(src.firstsale AS DATE),
    dst.credito_limite = src.so_creditlimit,
    dst.creditoactivo = if(src.ptermisactive = 'Y', 'T', 'F'),
    dst.credito_plazo = src.ptermnetdays,
    dst.listanegra_esta = if(src.em_dmbp_listanegra = 'Y', 'T', 'F'),
    dst.clientedepiso = if(src.em_dmbp_ispiso = 'Y', 1, 0),
    dst.receptorusocfdi_id = (select uso.id from tusoscfdi as uso where uso.c_usocfdi = src.em_fet_uso_cfdi),
    dst.editado_en = UTC_TIMESTAMP(),
    dst.procesostatus = 8,
    src.stv_id = dst.id
WHERE src.iscustomer = 'Y' and src.rfc = dst.rfc and dst.rfc not in ('XAXA-010101-000', 'XAXX-010101-000', 'XXXX-XXXXXX-XXX');


# Paso 3: Se actualizan los clientes con RFC genérico y que la razón social sea la misma, usando el 9 en proceso status;
#  y actualizando el stv_id en la tabla src, para identificar los clientes que no fueron actualizados

UPDATE taob_empresaclientes AS dst, taob_clientes AS src
SET
    dst.openbravo_id = src.c_bpartner_id,
    dst.bansuspender = if(src.isactive = 'Y', 'F', 'Y'),
    dst.codigo = src.value,
    dst.razonsocial = if(src.em_fet_razonsocial is null, src.name, src.em_fet_razonsocial),
    dst.notas = src.description,
    dst.fechaprimeraventa = CAST(src.firstsale AS DATE),
    dst.credito_limite = src.so_creditlimit,
    dst.creditoactivo = if(src.ptermisactive = 'Y', 'T', 'F'),
    dst.credito_plazo = src.ptermnetdays,
    dst.listanegra_esta = if(src.em_dmbp_listanegra = 'Y', 'T', 'F'),
    dst.clientedepiso = if(src.em_dmbp_ispiso = 'Y', 1, 0),
    dst.receptorusocfdi_id = (select uso.id from tusoscfdi as uso where uso.c_usocfdi = src.em_fet_uso_cfdi),
    dst.editado_en = UTC_TIMESTAMP(),
    dst.procesostatus = 9,
    src.stv_id = dst.id
WHERE src.iscustomer = 'Y' and src.rfc = dst.rfc and src.em_fet_razonsocial = dst.razonsocial
  and src.rfc in ('XAXA-010101-000', 'XAXX-010101-000', 'XXXX-XXXXXX-XXX');


# Paso 4: Se insertan los clientes pendientes donde el stv_id es nulo, ya que no fueron actualizador por alguna de los
#  pasos anteriores, y se usa el procesostatus = 10

INSERT INTO taob_empresaclientes (
                                  empresa_id, rfc, creado_en, creado_por, editado_por, nombrecorto,
                                  openbravo_id, bansuspender, codigo, razonsocial, notas,
                                  fechaprimeraventa, credito_limite, creditoactivo, credito_plazo,
                                  listanegra_esta, clientedepiso, receptorusocfdi_id, editado_en,
                                  procesostatus
                                  )
SELECT
       1,
       substr(src.rfc, 1, 15),
       UTC_TIMESTAMP(),
       1,
       1,
       substr(src.name, 1, 60),
    src.c_bpartner_id,
    if(src.isactive = 'Y', 'F', 'Y'),
    src.value,
    if(src.em_fet_razonsocial is null, src.name, src.em_fet_razonsocial),
    src.description,
    CAST(src.firstsale AS DATE),
    src.so_creditlimit,
    if(src.ptermisactive = 'Y', 'T', 'F'),
    src.ptermnetdays,
    if(src.em_dmbp_listanegra = 'Y', 'T', 'F'),
    if(src.em_dmbp_ispiso = 'Y', 1, 0),
    (select uso.id from tusoscfdi as uso where uso.c_usocfdi = src.em_fet_uso_cfdi),
    UTC_TIMESTAMP(),
    10  # procesostatus 10 indica insertados
FROM taob_clientes AS src
WHERE src.iscustomer = 'Y' and src.stv_id is null;


# Paso 5, se complementa el stv_id con los clientes insertados
UPDATE taob_empresaclientes AS dst, taob_clientes AS src
SET
    src.stv_id = dst.id
WHERE src.stv_id is null and src.c_bpartner_id = dst.openbravo_id and dst.procesostatus = 10;


# Paso 6, se insertan las direcciones
INSERT INTO tempresa_cliente_domicilios (
                                  empresa_cliente_id, emails, telefonos, pais_id, pais_estado_id, ciudad,
                                  colonia, calle, numexterior, numinterior,
                                  codigopostal, editado_en
                                  )
select
       src.stv_id,
       CASE
           WHEN src.em_dmbp_email is null and src.em_fet_email_rp is null THEN null
           WHEN src.em_dmbp_email is null THEN src.em_fet_email_rp
           WHEN src.em_fet_email_rp is null THEN src.em_dmbp_email
           WHEN src.em_dmbp_email = src.em_fet_email_rp THEN src.em_fet_email_rp
           ELSE CONCAT(src.em_dmbp_email, src.em_fet_email_rp)
           END,
       src.em_dmbp_phone,
       173,  # TPAISES.MEX,
       CASE
           WHEN src.regionname is null THEN null
           WHEN src.regionname = 'Distrito Federal' THEN 10
           ELSE (select tpe.id from tpais_estados as tpe where tpe.descripcion = src.regionname)
           END,
       src.em_dmbp_city,
       src.em_dmbp_address2,
       src.em_dmbp_address1,
       src.em_dmbp_tdirm_numex,
       src.em_dmbp_tdirm_numin,
       REGEXP_SUBSTR(src.em_dmbp_apostal, '[0-9]{5}'),
       UTC_TIMESTAMP()
from taob_clientes as src
where src.em_dmbp_address1 is not null or src.em_dmbp_phone is not null or src.em_dmbp_email is not null
   or src.em_fet_email_rp is not null
order by src.stv_id;


# Consultas adicionales:

# Se actualiza la tabla con las mismas columnas

-- Pendiente actualizar em_dmbp_tipocliente en un nuevo registro si es que no existe en tempresa_cliente_clasificaciones
-- Pendiente actualizar em_dmbp_address1 = calle en un nuevo registro si es que no existe en tempresa_cliente_domicilios
-- em_dmbp_city, em_dmbp_apostal, mexico, em_dmbp_address2 = colonia, em_dmbp_tdirm_numex, em_dmbp_tdirm_numin, em_dmbp_phone, regionname = municipio
-- em_dmbp_email = email, em_fet_email_rp

# Update en servidor entre tablas con las mismas columnas
# UPDATE tempresa_clientes AS dst, taob_empresaclientes AS src
# SET
#     dst.openbravo_id = src.openbravo_id,
#     dst.bansuspender = src.bansuspender,
#     dst.codigo = src.codigo,
#     dst.notas = src.notas,
#     dst.fechaprimeraventa = src.fechaprimeraventa,
#     dst.credito_limite = src.credito_limite,
#     dst.creditoactivo = src.creditoactivo,
#     dst.credito_plazo = src.credito_plazo,
#     dst.listanegra_esta = src.listanegra_esta,
#     dst.clientedepiso = src.clientedepiso,
#     src.procesostatus = 7  # usado para identificar los que ya fueron actualizados y usar el resto como insert
# WHERE src.id = dst.id
