# -*- coding: utf-8 -*-



def buscar_producto():
    """Forma buscar para productos
    """
    _dbTable = dbc01.tempresa_prodserv
    
    _s_action = request.args(0, None)
    
    _qry = (_dbTable.tipo == TEMPRESA_PRODSERV.E_TIPO.PRODUCTO)

    _L_GroupBy = [
        _dbTable.id,
        ]

    _O_cat = STV_FWK_FORM(
                dbReference = dbc01,
                dbReferenceSearch = dbc01(_qry),
                dbTable = _dbTable, 
                L_visibleButtons = request.stv_fwk_permissions.L_formSearchButtons, 
                s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
                L_fieldsToShow = [
                    _dbTable.id, 
                    _dbTable.codigo, 
                    _dbTable.tipo, 
                    _dbTable.descripcion, 
                    _dbTable.marca_id, 
                    _dbTable.unidad_id, 
                    _dbTable.prodserv_id,
                    dbc01.tempresa_prodserv_imagenes.imagen,
                    ], 
                L_fieldsToGroupBy = _L_GroupBy,
                L_fieldsToSearch = [
                    _dbTable.id, 
                    _dbTable.codigo,
                    _dbTable.descripcion,
                    ], 
                L_fieldsToSearchIfDigit = [
                    _dbTable.id, 
                    _dbTable.codigo
                    ],
                )
    
    if ((_s_action == request.stv_fwk_permissions.btn_none.code)):
        _O_cat.addRowSearchResultsLeftJoin(
            dbc01.tempresa_invmovto_productos.on(
                (dbc01.tempresa_inventariomovimientos.empresa_proveedor_id == 1 )
                &(dbc01.tempresa_invmovto_productos.empresa_inventariomovimiento_id == dbc01.tempresa_inventariomovimientos.id)
                &(dbc01.tempresa_invmovto_productos.empresa_producto_id == _dbTable.id)
                )
            )
    else:
        #No se hace este Join en caso de no ser botón menú
        pass
    
    _O_cat.addRowSearchResultsLeftJoin(
        dbc01.tempresa_prodserv_imagenes.on(
            (dbc01.tempresa_prodserv_imagenes.empresa_prodserv_id == _dbTable.id )
            )
        )     
    
    
    
    _D_returnVars = _O_cat.process()

    return (_D_returnVars)

def buscar_cfdis_recibidos_uploaddirect():
    
    D_returnJson = STV_RESPONSE_JSON_A01()
    
    _dbFile = request.vars.cfdi
    
    if isinstance(_dbFile, str):
        D_returnJson.add_error("No se identifico el archivo")
        D_returnJson.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
    else:
        _s_filename = _dbFile.filename
        _s_filetype = _dbFile.type
        _s_proveedor_id = request.vars.emisorproveedor_id
        
        # TODO checar permisos
        
        if (_s_filetype != "text/xml"):
            D_returnJson.add_error("Tipo de archivo no compatible, debe ser XML")
            D_returnJson.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
        else:
            
            # Primer paso es obtener la información en objectos del XML
            _stv_xml = STV_XML2CFDI()
            _D_result = _stv_xml.importar_string(_dbFile.file.read())
            if _D_result.E_return != stvfwk2_e_RETURN.OK:
                D_returnJson.add_error(_D_result.s_msgError)
                D_returnJson.E_return = _D_result.E_return
            elif not D_stvFwkCfg.cfg_cuenta_aplicacion_id:
                D_returnJson.add_error("Aplicación no reconocida, intente nuevamente")
                D_returnJson.E_return = stvfwk2_e_RETURN.NOK_CONDICIONES_INCORRECTAS            
            else:
                
                _dbRow_cuentaaplicacion = db.tcuenta_aplicaciones(D_stvFwkCfg.cfg_cuenta_aplicacion_id)
                
                _dbRow_empresa = dbc01.tempresas(_dbRow_cuentaaplicacion.empresa_id)
                
                _stv_importar_cfdi = STV_CFDI_INTEGRACION(
                    s_rfc = _dbRow_empresa.rfc,
                    s_empresa_id = _dbRow_empresa.id,
                    s_cuenta_id = _dbRow_cuentaaplicacion.cuenta_id,
                    )
                
                _D_result = _stv_importar_cfdi.importar(D_cfdi = _stv_xml.Comprobante)
                D_returnJson.add_error(_D_result.s_msgError)
                D_returnJson.E_return = _D_result.E_return
                D_returnJson.n_id = _D_result.n_cfdi_id
    
    return Storage(O_json = D_returnJson)
    
class BUSCAR_CFDIS():
    class FILTRO():
        TODOS = 0
        CON_CANTIDADES_PENDIENTES = 1
    
def _buscar_cfdis(n_rol, n_tipocomprobante_id, n_tiporelacion_id = None, dbQry = None, dbFiltro = BUSCAR_CFDIS.FILTRO.TODOS):
    
    _dbTCFDIs = dbc01.tcfdis
    _dbTCFDI_Conceptos = dbc01.tcfdi_conceptos
    _s_action = request.args(0, request.stv_fwk_permissions.btn_none.code)

    if (_s_action == request.stv_fwk_permissions.btn_view.code):
        
        redirect( URL( a = 'app_timbrado', c = '250cfdi', f = 'cfdisrecibidos.html', args = request.args, vars = request.vars) )

    else:
        # Variable que contendrá los campos a editar en la búsqueda avanzada    
        _L_advSearch = []
        
        # Se configura el uso del campo día para poder buscar por rango de fechas    
        _dbField_dia = _dbTCFDIs.dia.clone()
        _dbField_dia.widget = stv_widget_inputDateRange
        _L_advSearch.append(_dbField_dia)
        
        # Se agregan campos sin modificaciones
        _L_advSearch += [
            _dbTCFDIs.emisorproveedor_id.clone(),
            _dbTCFDIs.serie.clone(), 
            _dbTCFDIs.folio.clone(),
            Field('cfdi', type = 'upload',
              required = False, requires = IS_LENGTH(minsize=0, maxsize=1572864, error_message=T('El archivo es demasiado grande.')),
              notnull = False,
              uploadfield = True, 
              widget=lambda f, d, u=None: stv_widget_inputFile_directUpload(
                  f, 
                  d, 
                  URL(a='app_almacenes', c='600busquedas', f='buscar_cfdis_recibidos_uploaddirect.json')
                  ), 
              label = ('Importar CFDI'), comment = 'Si el CFDI no se encuentra, puede ser importado',
              writable = True, readable = True, authorize = lambda record: auth.is_logged_in(),
              autodelete = True, represent = lambda v,r: stv_represent_image(v,r,URL(a='app_empresas', c='120empresas', f='empresa_prodserv_imagenes')), 
              uploadfolder = os.path.join(request.folder, 'uploads', 'cfdis'),
              uploadseparate = False, uploadfs = None ),
            ]
        
        # Se configura la edición de los campos
        if request.vars.stv_search_filter:
            _L_search_filter = json.loads(request.vars.stv_search_filter)
        else:
            _L_search_filter = []
        
        for _dbField in _L_advSearch:
            
            for _D_filter in _L_search_filter:
                if _D_filter.get('s_reference_name') == str(_dbField):
                    _dbField.default = _D_filter.get('x_value')
                else:
                    pass
                
            # Tiene prioridad las request.var con respecto a search_filter
            if _dbField.name in request.vars:
                _dbField.default = request.vars[_dbField.name]
            else:
                pass
            
            if _dbField.name == "emisorproveedor_id":
                _s_emisorproveedor_id_buscar = _dbField.default
            else:
                pass
            _dbField.writable = True
                
        _dbTCFDIs.emisorproveedor_id.writable = False
        
        _s_alias_cantProductos = 'Cant_productos'
        _dbSUM_cantidad = _dbTCFDI_Conceptos.cantidad.coalesce_zero().sum().with_alias(_s_alias_cantProductos)

        _s_alias_cantRegistrados = 'Cant_registrados'
        _s_SUM_registrados = dbc01(
            (dbc01.tempresa_inventariomovimientos.cfdi_id == _dbTCFDIs.id)
            & (dbc01.tempresa_invmovto_productos.empresa_inventariomovimiento_id == dbc01.tempresa_inventariomovimientos.id)
            )._select(dbc01.tempresa_invmovto_productos.cantidad_entrada.coalesce_zero().sum()).replace('tcfdis, ', '')
        _s_SUM_registrados = 'COALESCE((' + _s_SUM_registrados.replace(';','') + '),0) AS ' + _s_alias_cantRegistrados
        
        _L_fields = [
                _dbTCFDIs.id,
                _dbTCFDIs.empresa_serie_id, 
                _dbTCFDIs.serie, 
                _dbTCFDIs.folio, 
                _dbTCFDIs.formapago_id, 
                _dbTCFDIs.fecha, 
                _dbTCFDIs.moneda_id, 
                _dbTCFDIs.subtotal, 
                _dbTCFDIs.descuento, 
                _dbTCFDIs.tipocambio, 
                _dbTCFDIs.total, 
                _dbTCFDIs.tipocomprobante_id, 
                _dbTCFDIs.metodopago_id, 
                _dbTCFDIs.emisorrfc, 
                _dbTCFDIs.emisorproveedor_id, 
                _dbTCFDIs.receptorrfc,
                _dbTCFDIs.receptorcliente_id,
                # _dbSUM_cantidad,
                #'(SELECT SUM(COALESCE(tempresa_invmovto_productos.cantidad_entrada,0)) FROM tempresa_invmovto_productos WHERE (tempresa_invmovto_productos.cfdi_concepto_id = tcfdi_conceptos.id)) AS Cant_registrados',
                # _s_SUM_registrados
                # TODO pendientes
            ]
        
        _L_fieldsGroupBy = [
                _dbTCFDIs.id,
                _dbTCFDIs.empresa_serie_id, 
                _dbTCFDIs.serie, 
                _dbTCFDIs.folio, 
                _dbTCFDIs.formapago_id, 
                _dbTCFDIs.fecha, 
                _dbTCFDIs.moneda_id, 
                _dbTCFDIs.subtotal, 
                _dbTCFDIs.descuento, 
                _dbTCFDIs.tipocambio, 
                _dbTCFDIs.total, 
                _dbTCFDIs.tipocomprobante_id, 
                _dbTCFDIs.metodopago_id, 
                _dbTCFDIs.emisorrfc, 
                _dbTCFDIs.emisorproveedor_id, 
                _dbTCFDIs.receptorrfc,
                _dbTCFDIs.receptorcliente_id,
            ]

        
        _qry = (_dbTCFDIs.rol == n_rol)
        _qry &= (_dbTCFDIs.tipocomprobante_id == n_tipocomprobante_id)
        _qry &= (_dbTCFDIs.tiporelacion_id == n_tiporelacion_id)
        if _s_emisorproveedor_id_buscar:
            _qry &= (_dbTCFDIs.emisorproveedor_id == _s_emisorproveedor_id_buscar)
        else:
            pass

        if dbQry:
            _qry &= (dbQry)
        else:
            pass

        _O_cat = STV_FWK_FORM(
            dbReference = dbc01,
            dbReferenceSearch = dbc01(_qry),
            dbTable = _dbTCFDIs, 
            L_visibleButtons = request.stv_fwk_permissions.L_formSearchButtons, 
            s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
            L_fieldsToShow = _L_fields,
            L_fieldsToGroupBy = _L_fieldsGroupBy,
            L_fieldsToSearch = [
                _dbTCFDIs.serie, 
                _dbTCFDIs.folio
                ], 
            L_fieldsToSearchIfDigit = [
                _dbTCFDIs.id, 
                _dbTCFDIs.tipocomprobante_id, 
                _dbTCFDIs.folio
                ]
            )
        
        _O_cat.addRowSearchResultsLeftJoin(
            _dbTCFDI_Conceptos.on(
                (_dbTCFDI_Conceptos.cfdi_id == _dbTCFDIs.id)
                ),
            )     
        
#         _O_cat.addRowSearchResultsLeftJoin(
#             dbc01.tempresa_inventariomovimientos.on(
#                 (dbc01.tempresa_inventariomovimientos.cfdi_id == _dbTable.id)
#                 ),
#             )     
#     
#         _O_cat.addRowSearchResultsLeftJoin(
#             dbc01.tempresa_invmovto_productos.on(
#                 (dbc01.tempresa_invmovto_productos.empresa_inventariomovimiento_id == dbc01.tempresa_inventariomovimientos.id)
#                 )
#             )
        
        if(dbFiltro):
            if dbFiltro == BUSCAR_CFDIS.FILTRO.CON_CANTIDADES_PENDIENTES:
                _O_cat.setRowSearchResultsHaving(
                    (
                        _s_alias_cantProductos + '!=' + _s_alias_cantRegistrados
                        )                    
                    )
            else:
                pass
        else:
            pass
        _O_cat.cfgRow_advSearch(
            L_dbFields_formFactory_advSearch = _L_advSearch,
            dbRow_formFactory_advSearch = None,
            D_hiddenFields_formFactory_advSearch = {},
            s_style = 'bootstrap',
            L_buttons = []
            )    

        _O_cat.addRowSearchResults_addColumnBegin(
            es_typeColumn = STV_FWK_FORM.ES_TYPECOLUMN.S_PLUS,
            s_colname = "plus",
            D_field = Storage(
                readable = True
                ),
            m_classHtml = lambda D_column, dbRow: "stv_rowresults_plus", 
            D_data = Storage(
                url = URL(f='buscar_cfdis_recibidos_plus', args=['master_'+str(request.function)])
                )
            )
        
        _D_returnVars = _O_cat.process()
        
        response.view = os.path.join('600busquedas','buscar_cfdis.html')
            
            
#SELECT  tcfdis.id, tcfdis.empresa_serie_id, tcfdis.serie, tcfdis.folio, tcfdis.formapago_id, tcfdis.fecha, tcfdis.moneda_id, tcfdis.subtotal, tcfdis.descuento, tcfdis.tipocambio, tcfdis.total, tcfdis.tipocomprobante_id, tcfdis.metodopago_id, tcfdis.emisorrfc, tcfdis.emisorproveedor_id, tcfdis.receptorrfc, tcfdis.receptorcliente_id, SUM(COALESCE(tcfdi_conceptos.cantidad,0)) AS Cant_productos FROM tcfdis LEFT JOIN tcfdi_conceptos ON (tcfdi_conceptos.cfdi_id = tcfdis.id) WHERE (((((tcfdis.rol = 2) AND (tcfdis.tipocomprobante_id = 1)) AND (tcfdis.tiporelacion_id IS NULL)) AND (tcfdis.emisorproveedor_id = 9)) AND ((((tcfdis.emisorproveedor_id = 9) AND 1) AND 1) OR ((tcfdis.id = 79717) OR 0))) GROUP BY tcfdis.id, tcfdis.empresa_serie_id, tcfdis.serie, tcfdis.folio, tcfdis.formapago_id, tcfdis.fecha, tcfdis.moneda_id, tcfdis.subtotal, tcfdis.descuento, tcfdis.tipocambio, tcfdis.total, tcfdis.tipocomprobante_id, tcfdis.metodopago_id, tcfdis.emisorrfc, tcfdis.emisorproveedor_id, tcfdis.receptorrfc, tcfdis.receptorcliente_id ORDER BY tcfdis.editado_en DESC LIMIT 1000 OFFSET 0            
            
    return (_D_returnVars)


def buscar_cfdis_recibidos():
    ###TODO: VOLVERÉ...
    
    _s_action = request.args(0, request.stv_fwk_permissions.btn_none.code)
    
    _s_folio = request.vars.folio
    _s_search = request.vars.stv_ed_find
    
    if (_s_action != request.stv_fwk_permissions.btn_findall.code) and not(_s_folio) and not(_s_search):
        _dbFiltro = BUSCAR_CFDIS.FILTRO.CON_CANTIDADES_PENDIENTES
    else:
        _dbFiltro = None
    
    _D_returnVars = _buscar_cfdis(TCFDIS.E_ROL.RECEPTOR, TTIPOSCOMPROBANTES.INGRESO, dbQry = None, dbFiltro = _dbFiltro)
            
    return (_D_returnVars)


def buscar_cfdis_emitidos():
    
    _D_returnVars = _buscar_cfdis(TCFDIS.E_ROL.EMISOR, TTIPOSCOMPROBANTES.INGRESO, dbQry = None)
            
    return (_D_returnVars)

def buscar_cfdis_egreso_recibidos():
    
    _D_returnVars = _buscar_cfdis(TCFDIS.E_ROL.RECEPTOR, TTIPOSCOMPROBANTES.EGRESO, TTIPOSRELACIONES.DEVOLUCION, dbQry = None)
    
    return (_D_returnVars)


def buscar_cfdis_recibidos_plus():
    """ Plus que mostrará los conceptos del CFDI
    
    Args:
        args0: texto en relación al maestro empresa
        args1: id del CFDI    
        [args2]: operación a realizar en el formulario
        [args3]: id en caso de existir relación a un registro en la operación
    """
    
    _dbTable = dbc01.tcfdi_conceptos
        
    _s_cfdi_id = request.args(1, None)
    
    _s_action = request.args(2, request.stv_fwk_permissions.btn_none.code)
    
    _dbField_cantidadMovtos = (
        dbc01.tempresa_invmovto_productos.cantidad_entrada.coalesce_zero().sum() 
        + dbc01.tempresa_invmovto_productos.cantidad_salida.coalesce_zero().sum()
        ).with_alias('registrados')
                
    
    _L_fields = [
            _dbTable.id,
            _dbTable.noidentificacion,
            _dbTable.descripcion,
            _dbTable.cantidad,
            _dbField_cantidadMovtos,
            _dbTable.valorunitario,
            _dbTable.importe,
            _dbTable.descuento
        ]
    
    _L_fieldsGroupBy = [
            _dbTable.id,
            _dbTable.noidentificacion,
            _dbTable.descripcion,
            _dbTable.cantidad,
            _dbTable.valorunitario,
            _dbTable.importe,
            _dbTable.descuento
        ]
                                
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tcfdis,
        xFieldLinkMaster = [_dbTable.cfdi_id],
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none, 
            request.stv_fwk_permissions.btn_findall
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab + "_" + str(_s_cfdi_id),
        L_fieldsToShow = _L_fields, 
        L_fieldsToGroupBy = _L_fieldsGroupBy,
        L_fieldsToSearch = [
            _dbTable.id,
            _dbTable.noidentificacion,
            _dbTable.descripcion
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )
    
    _O_cat.addRowSearchResultsLeftJoin(
        dbc01.tempresa_invmovto_productos.on(
            (dbc01.tempresa_invmovto_productos.cfdi_concepto_id == dbc01.tcfdi_conceptos.id)
            )
        )

    _D_returnVars = _O_cat.process()
    
    if (_s_action in (request.stv_fwk_permissions.btn_none.code,)):
    ###TODO: ¿Por qué el framework no reconoce que tiene que crear la vista? 
        _D_returnVars.D_useView.b_serverCreatesTab = True
        _D_returnVars.D_useView.b_cleanGrid = True
    else:
        #Si no es menú no crea las vistas, no pasa nada sigue su flujo.
        pass
    
    return (_D_returnVars)

