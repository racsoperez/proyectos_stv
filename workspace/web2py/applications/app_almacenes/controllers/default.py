# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
#########################################################################

def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    if '_flash' in request.vars:
        session.stv_flash = request.vars._flash # Se mantiene el mensaje despues de un logout
    else:
        pass
    
    
    _D_tabinfo = STV_FWK_FORM.TAB_INFORMATION(
        s_tabName = "Embarques",
        s_url = URL(f='empresa_almacen_embarques',  args=['master_'+str(request.function)]),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_almacen_embarques', s_masterIdentificator = str(request.function))
        )
    
    return dict(
        msg = 'Bienvenidos',
        dbFieldAlmacenes = STV_LIB_APP_ALMACENES.GET_DBFIELD_ALMACENES(
            n_default = request.vars.master_empresa_plaza_sucursal_almacen_id or 0,
            b_writable = True
            ),
        L_breadcrub = [
            STV_ALMACENES.BREADCRUB.BC_INDEX
            ],
        s_maintab_id = STV_CFG_APP_ALMACENES.ID,
        D_tabinfo = _D_tabinfo,
        L_inputs = [
            Storage(
                s_parent_id = STV_CFG_APP_ALMACENES.ID,
                s_selector = '#master_empresa_plaza_sucursal_almacen_id',
                s_tipo = "dinamico",
                s_valor = ""
                ),
            Storage(
                s_parent_id = "",
                s_selector = 'cfg_cuenta_aplicacion_id',
                s_tipo = "fijo",
                s_valor = str(D_stvFwkCfg.cfg_cuenta_aplicacion_id or "")
                ),            
            ]
        )

def index2():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    return dict(
        msg = 'Bienvenidos',
        )


def test_cfdi():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    
    _cfdi = STV_CFDI_INTEGRACION(
        s_rfc = "ALG-040428-RG4",
        s_empresa_id = 1,
        s_cuenta_id = 3,
        )
    
    _s_xml_cfdi_facturaCredito = '''<?xml version="1.0" encoding="UTF-8"?>
<cfdi:Comprobante xmlns:cfdi="http://www.sat.gob.mx/cfd/3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    Certificado="MIIGUTCCBDmgAwIBAgIUMDAwMDEwMDAwMDA0MDgxMjEyODYwDQYJKoZIhvcNAQELBQAwggGyMTgwNgYDVQQDDC9BLkMuIGRlbCBTZXJ2aWNpbyBkZSBB
    ZG1pbmlzdHJhY2nDs24gVHJpYnV0YXJpYTEvMC0GA1UECgwmU2VydmljaW8gZGUgQWRtaW5pc3RyYWNpw7NuIFRyaWJ1dGFyaWExODA2BgNVBAsML0FkbWluaXN0cmFja
    cOzbiBkZSBTZWd1cmlkYWQgZGUgbGEgSW5mb3JtYWNpw7NuMR8wHQYJKoZIhvcNAQkBFhBhY29kc0BzYXQuZ29iLm14MSYwJAYDVQQJDB1Bdi4gSGlkYWxnbyA3NywgQ2
    9sLiBHdWVycmVybzEOMAwGA1UEEQwFMDYzMDAxCzAJBgNVBAYTAk1YMRkwFwYDVQQIDBBEaXN0cml0byBGZWRlcmFsMRQwEgYDVQQHDAtDdWF1aHTDqW1vYzEVMBMGA1U
    ELRMMU0FUOTcwNzAxTk4zMV0wWwYJKoZIhvcNAQkCDE5SZXNwb25zYWJsZTogQWRtaW5pc3RyYWNpw7NuIENlbnRyYWwgZGUgU2VydmljaW9zIFRyaWJ1dGFyaW9zIGFs
    IENvbnRyaWJ1eWVudGUwHhcNMTcxMTE0MDAyMTI2WhcNMjExMTE0MDAyMTI2WjCB8TExMC8GA1UEAxMoQUNVTVVMQURPUkVTIFkgTExBTlRBUyBHQUxMQVJETyBTQSBER
    SBDVjExMC8GA1UEKRMoQUNVTVVMQURPUkVTIFkgTExBTlRBUyBHQUxMQVJETyBTQSBERSBDVjExMC8GA1UEChMoQUNVTVVMQURPUkVTIFkgTExBTlRBUyBHQUxMQVJETy
    BTQSBERSBDVjElMCMGA1UELRMcQUxHMDQwNDI4Ukc0IC8gR0FNSDU5MDcwNDNBMjEeMBwGA1UEBRMVIC8gR0FNSDU5MDcwNEhTTExSQzA3MQ8wDQYDVQQLEwZ1bmlkYWQ
    wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCe+9iSz8rD5V/+JJJhUmcYIbS+3F7TEXzHpqnTa07QeorJUu+seS1JWccfUTDaWky/RpAWA4dw/WSyxrToOnPF
    UjtO+DynAZh8ur35mm2BP/rUlIeDF+/J+Wu0HGZXfwdyu7YISzqniMGXBFQSLwLGw9hCt/btHOexDKdfeh69s5jZhtRmgLCKRPH3Zhp4IwMphQJLYjjdsxWMcr8EbRTQq
    o8+EOZfeZmWJt1DAIvhnCOmEDBIIJyraopRXvQwnwWOsRi2QXmtbo2haSXTrWK9DRcVzcQuMnE7y6jwhKmOfJD6eO+a9k9JYkabcHgaZeOQYYPniylWnZSVtNtnRiO3Ag
    MBAAGjHTAbMAwGA1UdEwEB/wQCMAAwCwYDVR0PBAQDAgbAMA0GCSqGSIb3DQEBCwUAA4ICAQCRnU6+haia2TYIX9mVLVYx931pbAL0QU7lL4r2wcNXU4GEa/TRCALx/Jp
    eSDS2K9ftf6zWCdM92GG2z5EAMuazywA2CeQfsAkZfbOU195Vou9YpNMHA+lQKlMAOFq9KaWOC+Kl9Kbeu2xCGd7X6O3IJKqtyqgerZuoDPm9c7m0n2h/28rCIKwVtUl2
    oHt0Jg8z+vuoey4zxuYHVr4jHJwqTpH5MO7gLE+OFDz03TV6L4hZ4ZpwwNOQCMZeQe3yjuAXvPnLj/V8w0UB8w/7DN13OWK4xetcLRfcEtyyndODs5csOTWUjErmEu61m
    E8USM7fkOW3+45ipxqwNfZ4kdQXZpj8J5kmqyvAuyR09jQ6vlLqX7iMTW+jAqTDXq2GZXJ6IUuaxC9RYhNF89DVOqgrGuF6RyPgzMm2wxIDsQKIKBGTlOwHYNDBqS8lPY
    DtWv61OoCJUe5nNFpnRJCKaruujkFltQ4ZAyiJU92RWrsAtMVtf3UQKX7hmjUGEhB0rnpEl4EFK9inEzQUoumZaMaDLAkurY68pO0hNyVPP5dRReB6faf7EfCe9+6noA7
    qeOfiIpLn9XcVTwIcSEVEWNSqE9iIYRbAR5oxkEAfaa/GFJ64NP7DxW+au6YqGCeZb/t+Btdappp21VdNm5P9T2C9tOx7isSYXjZdb2nNrHHzkg==" 
    Fecha="2019-12-02T10:28:18" Folio="1034193" FormaPago="99" LugarExpedicion="82134" MetodoPago="PPD" Moneda="MXN" 
    NoCertificado="00001000000408121286" 
    Sello="OUjqhEMA0TpcP0ZqGCwbsTMfcjdGRP/ROKD6HigOJAmmZWbj8hcxNIW+OV+BoHhC+IoG/IUpzmmiambOlns3e9VLRhRyi+j9Gmcj03raf73FjREHeTIkyatKgG
    1OvyAQvcCa1Z/j3vdDjSIs87Kt0yTB/XI+iDzaW0z57PqfUArHL0mn8P7fbKgRP6D+ksepu1zHyjwnKH+p17o7N2IKNDbDW61m5FLDwQcvZJ6TPbMVUC0Dd2/oFHGodo7
    zupXoXlOfiENQqG8laV/QPlBIbtAGVJIM3Ev5AwjODjMiw2WIRgF5N5+uP5AVj0p2KtjEj/kOZ0Et/5OkaBDw37DbPg==" Serie="C" SubTotal="726.00" 
    TipoCambio="1" TipoDeComprobante="I" Total="842.16" Version="3.3" 
    xsi:schemaLocation="http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd">
    <cfdi:Emisor 
    Nombre="PROVEEDOR A" RegimenFiscal="601" Rfc="PRA040428PRA" />
    <cfdi:Receptor 
    Nombre="ACUMULADORES Y LLANTAS GALLARDO SA DE CV" Rfc="ALG040428RG4" UsoCFDI="G01" />
    <cfdi:Conceptos>
        <cfdi:Concepto Cantidad="2.00" ClaveProdServ="11141608" ClaveUnidad="H87" Descripcion="GRUPO 1" Importe="440.00" 
        NoIdentificacion="GRUPO 1" Unidad="Piezas" ValorUnitario="220.00">
            <cfdi:Impuestos>
                <cfdi:Traslados>
                    <cfdi:Traslado Base="440.00" Importe="70.40" Impuesto="002" TasaOCuota="0.160000" TipoFactor="Tasa" />
                </cfdi:Traslados>
            </cfdi:Impuestos>
        </cfdi:Concepto>
        <cfdi:Concepto Cantidad="1.00" ClaveProdServ="11141608" ClaveUnidad="H87" Descripcion="GRUPO 2" Importe="286.00" 
        NoIdentificacion="GRUPO 2" Unidad="Piezas" ValorUnitario="286.00">
            <cfdi:Impuestos>
                <cfdi:Traslados>
                    <cfdi:Traslado Base="286.00" Importe="45.76" Impuesto="002" TasaOCuota="0.160000" TipoFactor="Tasa" />
                </cfdi:Traslados>
            </cfdi:Impuestos>
        </cfdi:Concepto>
    </cfdi:Conceptos>
    <cfdi:Impuestos TotalImpuestosTrasladados="116.16">
        <cfdi:Traslados>
            <cfdi:Traslado Importe="116.16" Impuesto="002" TasaOCuota="0.160000" TipoFactor="Tasa" />
        </cfdi:Traslados>
    </cfdi:Impuestos>
    <cfdi:Complemento>
        <tfd:TimbreFiscalDigital xmlns:tfd="http://www.sat.gob.mx/TimbreFiscalDigital" 
        xsi:schemaLocation="http://www.sat.gob.mx/TimbreFiscalDigital http://www.sat.gob.mx/sitio_internet/cfd/TimbreFiscalDigit
        al/TimbreFiscalDigitalv11.xsd" Version="1.1" UUID="FF930228-8C8D-4151-B133-2BDA96B8E0F3" 
        FechaTimbrado="2019-12-02T11:27:14" RfcProvCertif="DET080304395" SelloCFD="OUjqhEMA0TpcP0ZqGCwbsTMfcjdGRP/ROKD6HigOJAmm
        ZWbj8hcxNIW+OV+BoHhC+IoG/IUpzmmiambOlns3e9VLRhRyi+j9Gmcj03raf73FjREHeTIkyatKgG1OvyAQvcCa1Z/j3vdDjSIs87Kt0yTB/XI+iDzaW0z
        57PqfUArHL0mn8P7fbKgRP6D+ksepu1zHyjwnKH+p17o7N2IKNDbDW61m5FLDwQcvZJ6TPbMVUC0Dd2/oFHGodo7zupXoXlOfiENQqG8laV/QPlBIbtAGVJ
        IM3Ev5AwjODjMiw2WIRgF5N5+uP5AVj0p2KtjEj/kOZ0Et/5OkaBDw37DbPg==" NoCertificadoSAT="00001000000407371267" 
        SelloSAT="sl7BMv8XLf6rv+HsRKA/oSL98zmAmapyZ9/LMhNnvalGoiyUyVl8sjoNKocNzxg+oV/nyipb2qiC93zgzT3d98nWyKyTw8xOYlfxUg0TcG8pl
        i36FWcIHa37WVmoa/ppUBZlVCGHyOmXH9mpyYleLScm9K8GvB6VWEOpqNtt9vxxpGUaxz6iQGVYFGFqKUbSj0Dmzwp7w5/qjCNd1s80OHe/zt98n7wdI/5l
        OJo1MY3xaKcJ/zRbpwAQj75D3/s0NN0+6HQtl96WEyCEX/oy/ZKueb+PtnY+s3oR+7wJ2KkJQSC+lCx04seC/iPe3pUoc0ZsC02rkXGIP36Be9HAjg==" />
    </cfdi:Complemento>
</cfdi:Comprobante>
     
        '''
    
    _s_xml_cfdi_pago = '''<?xml version="1.0" encoding="UTF-8"?>
<cfdi:Comprobante
    xmlns:cfdi="http://www.sat.gob.mx/cfd/3"
    xmlns:pago10="http://www.sat.gob.mx/Pagos"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    Certificado="MIIGUTCCBDmgAwIBAgIUMDAwMDEwMDAwMDA0MDgxMjEyODYwDQYJKoZIhvcNAQELBQAwggGyMTgwNgYDVQQDDC9BLkMuIGRlbCBTZXJ2aWNpbyBkZSBBZG1pbmlzdHJhY2nDs24gVHJpYnV0YXJpYTEvMC0GA1UECgwmU2VydmljaW8gZGUgQWRtaW5pc3RyYWNpw7NuIFRyaWJ1dGFyaWExODA2BgNVBAsML0FkbWluaXN0cmFjacOzbiBkZSBTZWd1cmlkYWQgZGUgbGEgSW5mb3JtYWNpw7NuMR8wHQYJKoZIhvcNAQkBFhBhY29kc0BzYXQuZ29iLm14MSYwJAYDVQQJDB1Bdi4gSGlkYWxnbyA3NywgQ29sLiBHdWVycmVybzEOMAwGA1UEEQwFMDYzMDAxCzAJBgNVBAYTAk1YMRkwFwYDVQQIDBBEaXN0cml0byBGZWRlcmFsMRQwEgYDVQQHDAtDdWF1aHTDqW1vYzEVMBMGA1UELRMMU0FUOTcwNzAxTk4zMV0wWwYJKoZIhvcNAQkCDE5SZXNwb25zYWJsZTogQWRtaW5pc3RyYWNpw7NuIENlbnRyYWwgZGUgU2VydmljaW9zIFRyaWJ1dGFyaW9zIGFsIENvbnRyaWJ1eWVudGUwHhcNMTcxMTE0MDAyMTI2WhcNMjExMTE0MDAyMTI2WjCB8TExMC8GA1UEAxMoQUNVTVVMQURPUkVTIFkgTExBTlRBUyBHQUxMQVJETyBTQSBERSBDVjExMC8GA1UEKRMoQUNVTVVMQURPUkVTIFkgTExBTlRBUyBHQUxMQVJETyBTQSBERSBDVjExMC8GA1UEChMoQUNVTVVMQURPUkVTIFkgTExBTlRBUyBHQUxMQVJETyBTQSBERSBDVjElMCMGA1UELRMcQUxHMDQwNDI4Ukc0IC8gR0FNSDU5MDcwNDNBMjEeMBwGA1UEBRMVIC8gR0FNSDU5MDcwNEhTTExSQzA3MQ8wDQYDVQQLEwZ1bmlkYWQwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCe+9iSz8rD5V/+JJJhUmcYIbS+3F7TEXzHpqnTa07QeorJUu+seS1JWccfUTDaWky/RpAWA4dw/WSyxrToOnPFUjtO+DynAZh8ur35mm2BP/rUlIeDF+/J+Wu0HGZXfwdyu7YISzqniMGXBFQSLwLGw9hCt/btHOexDKdfeh69s5jZhtRmgLCKRPH3Zhp4IwMphQJLYjjdsxWMcr8EbRTQqo8+EOZfeZmWJt1DAIvhnCOmEDBIIJyraopRXvQwnwWOsRi2QXmtbo2haSXTrWK9DRcVzcQuMnE7y6jwhKmOfJD6eO+a9k9JYkabcHgaZeOQYYPniylWnZSVtNtnRiO3AgMBAAGjHTAbMAwGA1UdEwEB/wQCMAAwCwYDVR0PBAQDAgbAMA0GCSqGSIb3DQEBCwUAA4ICAQCRnU6+haia2TYIX9mVLVYx931pbAL0QU7lL4r2wcNXU4GEa/TRCALx/JpeSDS2K9ftf6zWCdM92GG2z5EAMuazywA2CeQfsAkZfbOU195Vou9YpNMHA+lQKlMAOFq9KaWOC+Kl9Kbeu2xCGd7X6O3IJKqtyqgerZuoDPm9c7m0n2h/28rCIKwVtUl2oHt0Jg8z+vuoey4zxuYHVr4jHJwqTpH5MO7gLE+OFDz03TV6L4hZ4ZpwwNOQCMZeQe3yjuAXvPnLj/V8w0UB8w/7DN13OWK4xetcLRfcEtyyndODs5csOTWUjErmEu61mE8USM7fkOW3+45ipxqwNfZ4kdQXZpj8J5kmqyvAuyR09jQ6vlLqX7iMTW+jAqTDXq2GZXJ6IUuaxC9RYhNF89DVOqgrGuF6RyPgzMm2wxIDsQKIKBGTlOwHYNDBqS8lPYDtWv61OoCJUe5nNFpnRJCKaruujkFltQ4ZAyiJU92RWrsAtMVtf3UQKX7hmjUGEhB0rnpEl4EFK9inEzQUoumZaMaDLAkurY68pO0hNyVPP5dRReB6faf7EfCe9+6noA7qeOfiIpLn9XcVTwIcSEVEWNSqE9iIYRbAR5oxkEAfaa/GFJ64NP7DxW+au6YqGCeZb/t+Btdappp21VdNm5P9T2C9tOx7isSYXjZdb2nNrHHzkg=="
    Fecha="2018-12-28T13:21:49" Folio="1132513" LugarExpedicion="82134"
    Moneda="XXX" NoCertificado="00001000000408121286"
    Sello="RqgfA/xbsarI77n4WBvj+5Tnl7o2BffTUCMXKQrpnpPz6s7BkK++asmpDWUUNURbs2Q+B/QmsnjCQFUb6h36yEvF2+5OCkqCBP5nM9ZTEIF39LzAIZgCf4ELAz9i0E9j3qlB4zwWAp6Tmaxjrlc5nQKzRjm8mb35D1AQU3M9lMOXtekJKXMls7sK5HsuN5Wsg1mbS4AyrwbboquJU3QFoo0ic4DmqC7VtQfkZaAO29Eoio1mGidBFnEg6s0uDyNkXcUtgyOgjdHnGQ/CY8Zt+zEzSxX9YGpqsZsAY8LEMl0Ox3C0vqSpILD2jb0HdIPpXjYy8PZ36hIa/FWIvR7VnA=="
    SubTotal="0" TipoDeComprobante="P" Total="0" Version="3.3"
    xsi:schemaLocation="http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd http://www.sat.gob.mx/Pagos http://www.sat.gob.mx/sitio_internet/cfd/Pagos/Pagos10.xsd ">
    <cfdi:Emisor
        Nombre="ACUMULADORES Y LLANTAS GALLARDO SA DE CV" RegimenFiscal="601"
        Rfc="ALG040428RG4" />
    <cfdi:Receptor Nombre="MENDEZ LIZARRAGA LUIS A"
        Rfc="XAXX010101000" UsoCFDI="P01" />
    <cfdi:Conceptos>
        <cfdi:Concepto Cantidad="1" ClaveProdServ="84111506"
            ClaveUnidad="ACT" Descripcion="Pago" Importe="0" ValorUnitario="0" />
    </cfdi:Conceptos>
    <cfdi:Complemento>
        <pago10:Pagos Version="1.0">
            <pago10:Pago FechaPago="2018-12-28T12:00:00"
                FormaDePagoP="01" MonedaP="MXN" Monto="3945.73">
                <pago10:DoctoRelacionado Folio="1022903"
                    IdDocumento="D30741A9-C340-4CD2-88C6-B5D91DC6643B"
                    ImpPagado="3945.73" ImpSaldoAnt="4283.25" ImpSaldoInsoluto="337.52"
                    MetodoDePagoDR="PPD" MonedaDR="MXN" NumParcialidad="1" Serie="C" />
            </pago10:Pago>
        </pago10:Pagos>
        <tfd:TimbreFiscalDigital
            xmlns:tfd="http://www.sat.gob.mx/TimbreFiscalDigital"
            FechaTimbrado="2018-12-28T13:21:49"
            NoCertificadoSAT="00001000000407371267" RfcProvCertif="DET080304395"
            SelloCFD="RqgfA/xbsarI77n4WBvj+5Tnl7o2BffTUCMXKQrpnpPz6s7BkK++asmpDWUUNURbs2Q+B/QmsnjCQFUb6h36yEvF2+5OCkqCBP5nM9ZTEIF39LzAIZgCf4ELAz9i0E9j3qlB4zwWAp6Tmaxjrlc5nQKzRjm8mb35D1AQU3M9lMOXtekJKXMls7sK5HsuN5Wsg1mbS4AyrwbboquJU3QFoo0ic4DmqC7VtQfkZaAO29Eoio1mGidBFnEg6s0uDyNkXcUtgyOgjdHnGQ/CY8Zt+zEzSxX9YGpqsZsAY8LEMl0Ox3C0vqSpILD2jb0HdIPpXjYy8PZ36hIa/FWIvR7VnA=="
            SelloSAT="ASHbc4TfAgspCl/va9tU7bgi2Y6Zml9QWFwzrPTyH8RwZzXLh8Ralygzg2CuQVK75GgCuqRZ0RXjEL95+91I+qJFgF9RQntVaYCOTIa7bkV4n//LiqUDCG+dzPuhVq+YOUScdSZJ/MyuLUiqzYEWcXk+1EwGvSdyIuONgzYiRtncziy6A9tqj23zydhXbYQ2G8D2aY4+Q/8LGdKsPX8YqungLUQv8STiWGBHfntTPVnvh6W5a/KswYCHIPq6tBIVAUMUVH3gYR5/233tGt+QkLqQmtjg97LG9ermAoEmXXRc2ip/aZVHrnTOVwkp33zseC0NkIv/84k2BNs1lcvkFA=="
            UUID="BC9D4FA6-F791-454A-A323-907B50C456F9" Version="1.1"
            xsi:schemaLocation="http://www.sat.gob.mx/TimbreFiscalDigital http://www.sat.gob.mx/sitio_internet/cfd/timbrefiscaldigital/TimbreFiscalDigitalv11.xsd" />
    </cfdi:Complemento>
</cfdi:Comprobante>
     
        '''
        
    _s_xml_cfdi_nc = '''
    <cfdi:Comprobante
    xmlns:cfdi="http://www.sat.gob.mx/cfd/3"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    Certificado="MIIGUTCCBDmgAwIBAgIUMDAwMDEwMDAwMDA0MDgxMjEyODYwDQYJKoZIhvcNAQELBQAwggGyMTgwNgYDVQQDDC9BLkMuIGRlbCBTZXJ2aWNpbyBkZSBBZG1pbmlzdHJhY2nDs24gVHJpYnV0YXJpYTEvMC0GA1UECgwmU2VydmljaW8gZGUgQWRtaW5pc3RyYWNpw7NuIFRyaWJ1dGFyaWExODA2BgNVBAsML0FkbWluaXN0cmFjacOzbiBkZSBTZWd1cmlkYWQgZGUgbGEgSW5mb3JtYWNpw7NuMR8wHQYJKoZIhvcNAQkBFhBhY29kc0BzYXQuZ29iLm14MSYwJAYDVQQJDB1Bdi4gSGlkYWxnbyA3NywgQ29sLiBHdWVycmVybzEOMAwGA1UEEQwFMDYzMDAxCzAJBgNVBAYTAk1YMRkwFwYDVQQIDBBEaXN0cml0byBGZWRlcmFsMRQwEgYDVQQHDAtDdWF1aHTDqW1vYzEVMBMGA1UELRMMU0FUOTcwNzAxTk4zMV0wWwYJKoZIhvcNAQkCDE5SZXNwb25zYWJsZTogQWRtaW5pc3RyYWNpw7NuIENlbnRyYWwgZGUgU2VydmljaW9zIFRyaWJ1dGFyaW9zIGFsIENvbnRyaWJ1eWVudGUwHhcNMTcxMTE0MDAyMTI2WhcNMjExMTE0MDAyMTI2WjCB8TExMC8GA1UEAxMoQUNVTVVMQURPUkVTIFkgTExBTlRBUyBHQUxMQVJETyBTQSBERSBDVjExMC8GA1UEKRMoQUNVTVVMQURPUkVTIFkgTExBTlRBUyBHQUxMQVJETyBTQSBERSBDVjExMC8GA1UEChMoQUNVTVVMQURPUkVTIFkgTExBTlRBUyBHQUxMQVJETyBTQSBERSBDVjElMCMGA1UELRMcQUxHMDQwNDI4Ukc0IC8gR0FNSDU5MDcwNDNBMjEeMBwGA1UEBRMVIC8gR0FNSDU5MDcwNEhTTExSQzA3MQ8wDQYDVQQLEwZ1bmlkYWQwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCe+9iSz8rD5V/+JJJhUmcYIbS+3F7TEXzHpqnTa07QeorJUu+seS1JWccfUTDaWky/RpAWA4dw/WSyxrToOnPFUjtO+DynAZh8ur35mm2BP/rUlIeDF+/J+Wu0HGZXfwdyu7YISzqniMGXBFQSLwLGw9hCt/btHOexDKdfeh69s5jZhtRmgLCKRPH3Zhp4IwMphQJLYjjdsxWMcr8EbRTQqo8+EOZfeZmWJt1DAIvhnCOmEDBIIJyraopRXvQwnwWOsRi2QXmtbo2haSXTrWK9DRcVzcQuMnE7y6jwhKmOfJD6eO+a9k9JYkabcHgaZeOQYYPniylWnZSVtNtnRiO3AgMBAAGjHTAbMAwGA1UdEwEB/wQCMAAwCwYDVR0PBAQDAgbAMA0GCSqGSIb3DQEBCwUAA4ICAQCRnU6+haia2TYIX9mVLVYx931pbAL0QU7lL4r2wcNXU4GEa/TRCALx/JpeSDS2K9ftf6zWCdM92GG2z5EAMuazywA2CeQfsAkZfbOU195Vou9YpNMHA+lQKlMAOFq9KaWOC+Kl9Kbeu2xCGd7X6O3IJKqtyqgerZuoDPm9c7m0n2h/28rCIKwVtUl2oHt0Jg8z+vuoey4zxuYHVr4jHJwqTpH5MO7gLE+OFDz03TV6L4hZ4ZpwwNOQCMZeQe3yjuAXvPnLj/V8w0UB8w/7DN13OWK4xetcLRfcEtyyndODs5csOTWUjErmEu61mE8USM7fkOW3+45ipxqwNfZ4kdQXZpj8J5kmqyvAuyR09jQ6vlLqX7iMTW+jAqTDXq2GZXJ6IUuaxC9RYhNF89DVOqgrGuF6RyPgzMm2wxIDsQKIKBGTlOwHYNDBqS8lPYDtWv61OoCJUe5nNFpnRJCKaruujkFltQ4ZAyiJU92RWrsAtMVtf3UQKX7hmjUGEhB0rnpEl4EFK9inEzQUoumZaMaDLAkurY68pO0hNyVPP5dRReB6faf7EfCe9+6noA7qeOfiIpLn9XcVTwIcSEVEWNSqE9iIYRbAR5oxkEAfaa/GFJ64NP7DxW+au6YqGCeZb/t+Btdappp21VdNm5P9T2C9tOx7isSYXjZdb2nNrHHzkg=="
    Fecha="2018-12-28T14:29:17" Folio="1008393" FormaPago="99"
    LugarExpedicion="82134" MetodoPago="PUE" Moneda="MXN"
    NoCertificado="00001000000408121286"
    Sello="CDVddBEFRyBDgPCyO+Q+WxebuKtIiXONHpOcYVF2yJC3hHrkQinUgJ3hfPAA3z8VZrQXf/LeI/bWgoH0YSf5pmXhPtnPT31TeGzoMhHNdnO2IfnXQtdWtAdsnG0koJZGsF33lacNQNGmAfHBm+KexpJ/VRl4ByyQavTvoySZIqLwzOgSigXl9UxOpH3LJwOAZza7a7tQh08GZcnvFZ4EooH3dAi0O3jSHiHPRiICBV77FyTyPxS359v7pJ34tbVqNBuSkbuEXPKdY01rWmFcnqzcV5Lt98z1DsF1oicBodklLK8kbcJat858vDbSXFuIoVs7l135WU4viDofsHV5CQ=="
    Serie="NC" SubTotal="290.96" TipoCambio="1" TipoDeComprobante="E"
    Total="337.51" Version="3.3"
    xsi:schemaLocation="http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd ">
    <cfdi:CfdiRelacionados TipoRelacion="01">
        <cfdi:CfdiRelacionado
            UUID="D30741A9-C340-4CD2-88C6-B5D91DC6643B" />
    </cfdi:CfdiRelacionados>
    <cfdi:Emisor
        Nombre="ACUMULADORES Y LLANTAS GALLARDO SA DE CV" RegimenFiscal="601"
        Rfc="ALG040428RG4" />
    <cfdi:Receptor Nombre="MENDEZ LIZARRAGA LUIS A"
        Rfc="XAXX010101000" UsoCFDI="G02" />
    <cfdi:Conceptos>
        <cfdi:Concepto Cantidad="1.00" ClaveProdServ="84111506"
            ClaveUnidad="ACT" Descripcion="53.-DESCUENTO VOLUMEN"
            Importe="290.96" NoIdentificacion="CVE53B" Unidad="Piezas"
            ValorUnitario="290.96">
            <cfdi:Impuestos>
                <cfdi:Traslados>
                    <cfdi:Traslado Base="290.96" Importe="46.55"
                        Impuesto="002" TasaOCuota="0.160000" TipoFactor="Tasa" />
                </cfdi:Traslados>
            </cfdi:Impuestos>
        </cfdi:Concepto>
    </cfdi:Conceptos>
    <cfdi:Impuestos TotalImpuestosTrasladados="46.55">
        <cfdi:Traslados>
            <cfdi:Traslado Importe="46.55" Impuesto="002"
                TasaOCuota="0.160000" TipoFactor="Tasa" />
        </cfdi:Traslados>
    </cfdi:Impuestos>
    <cfdi:Complemento>
        <tfd:TimbreFiscalDigital
            xmlns:tfd="http://www.sat.gob.mx/TimbreFiscalDigital"
            FechaTimbrado="2018-12-28T14:29:17"
            NoCertificadoSAT="00001000000407371267" RfcProvCertif="DET080304395"
            SelloCFD="CDVddBEFRyBDgPCyO+Q+WxebuKtIiXONHpOcYVF2yJC3hHrkQinUgJ3hfPAA3z8VZrQXf/LeI/bWgoH0YSf5pmXhPtnPT31TeGzoMhHNdnO2IfnXQtdWtAdsnG0koJZGsF33lacNQNGmAfHBm+KexpJ/VRl4ByyQavTvoySZIqLwzOgSigXl9UxOpH3LJwOAZza7a7tQh08GZcnvFZ4EooH3dAi0O3jSHiHPRiICBV77FyTyPxS359v7pJ34tbVqNBuSkbuEXPKdY01rWmFcnqzcV5Lt98z1DsF1oicBodklLK8kbcJat858vDbSXFuIoVs7l135WU4viDofsHV5CQ=="
            SelloSAT="IUzFnCgQ0BZCID6rDqvb2/0znpTzdu3OptUrpmkxedfExlFJDvoPjt273nPJBGt+0LsOWJLKlCA4yPzL2qPwEndcUT9kj9QuEP0EE8QY/uaro9Z/F8JnsBGmQs90QvP+NIUEfBXoDxYmIob6mb/Rfn197lK6U9pJvZSNEUTlJ7yPkhz5uZrGIIt5veuJDu/JmQH0UrzeCRaBHyImWShhs3/Jiy6TuBoGr1IpTPaVOt/W91j1HXJGwqdkDG7pQcbsq5sg8lPKGxE9Jfzd/yCHRJgCIvjQ0pBeTaak4iwqsWqjrZueXFjEtgh9UqA2TQJ8X1mSi+2T/Vg4lLCXVnVnaQ=="
            UUID="FA8E13F6-ABE6-4FB0-98AA-76C582292207" Version="1.1"
            xsi:schemaLocation="http://www.sat.gob.mx/TimbreFiscalDigital http://www.sat.gob.mx/sitio_internet/cfd/timbrefiscaldigital/TimbreFiscalDigitalv11.xsd" />
    </cfdi:Complemento>
</cfdi:Comprobante>
'''
        
    _cfdi.importar(_s_xml_cfdi_facturaCredito)
    
    
    return dict(
        msg = 'Bienvenidos',
        )

def empresa_almacen_embarques():
    '''Formulario para los embarques del almacén
        
        Args:
            arg0: master_[nombre función maestro]
            arg1: empresa_almacen_id
            [arg2]: codigo de funcionalidad
            [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    '''
    
    _dbTable = dbc01.tempresa_embarque_almacenes
    _s_empresa_almacen_id = request.args(1, None)
    _s_action = request.args(2, request.stv_fwk_permissions.btn_none.code)
    _s_id = request.args(3, None)

    _L_formButtons = [
        request.stv_fwk_permissions.btn_none,
        request.stv_fwk_permissions.btn_findall
        ]
    
    _L_OrderBy = [
        _dbTable.orden,
        ]
    
    _L_filtro = []
    
    _dbRowsEmpresa_id = dbc01(
        (_dbTable.empresa_plaza_sucursal_almacen_id == _s_empresa_almacen_id)
        &(dbc01.tempresa_embarques.id == _dbTable.empresa_embarque_id)
        &(dbc01.tempresas.id == dbc01.tempresa_embarques.empresa_id)
        ).select(
            dbc01.tempresas.id
            )
        
    if _dbRowsEmpresa_id:
        _s_empresa_id = _dbRowsEmpresa_id.last().id
    else:
        _s_empresa_id = None
        pass
    
    _dbRowsEmbarques = dbc01(
        (dbc01.tempresa_embarques.empresa_id == _s_empresa_id)
        &(dbc01.tempresa_embarques.estatus != TEMPRESA_EMBARQUES.ESTATUS.CERRADO)
        ).select(
            dbc01.tempresa_embarques.id
            )
    
    if _dbRowsEmbarques:
        for _dbRowEmbarque in _dbRowsEmbarques:
            
            _dbRowsMovimientos = dbc01(
                (_dbTable.empresa_embarque_id == _dbRowEmbarque.id)
                &(_dbTable.estatus != TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CERRADO)
                ).select(
                    _dbTable.ALL,
                    orderby = [
                        _dbTable.orden
                        ]
                    ).first()
            
            if _dbRowsMovimientos:
                if _dbRowsMovimientos.empresa_plaza_sucursal_almacen_id == int(_s_empresa_almacen_id):
                    _L_filtro.append(_dbRowsMovimientos.id)
                else:
                    pass
            else:
                pass
    else:
        pass
    
    # Filtro para los almacenes en espera.
    
    _qry = (
        (_dbTable.id.belongs(_L_filtro))
        )
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_plaza_sucursal_almacenes, 
        xFieldLinkMaster = [
            _dbTable.empresa_plaza_sucursal_almacen_id
            ],
        L_visibleButtons = _L_formButtons, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.empresa_embarque_id,
            _dbTable.empresa_plaza_sucursal_almacen_id,
            _dbTable.orden,
            _dbTable.estatus,
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )
    
    _O_cat.addRowSearchResultsProperty('orderBy', _L_OrderBy)

    _D_returnVars = _O_cat.process()  
         
    return _D_returnVars    