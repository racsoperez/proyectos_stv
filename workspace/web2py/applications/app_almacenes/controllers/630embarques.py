# -*- coding: utf-8 -*-

def empresa_embarques():
    '''Formulario para la creación de embarques
        
        Args:
            arg0: master_[nombre función maestro]
            arg1: empresa_id
            [arg2]: codigo de funcionalidad
            [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    '''
    
    _s_action = request.args(0, request.stv_fwk_permissions.btn_none.code)
    
    _dbTable = dbc01.tempresa_embarques
    
    _s_empresa_plaza_sucursal_almacen_id = request.vars.master_empresa_plaza_sucursal_almacen_id
    
    _s_cuenta_aplicacion_id = D_stvFwkCfg.cfg_cuenta_aplicacion_id
    
    _s_id = request.args(1, None)

    _s_empresa_inventariomovimiento_id = request.args(1, None)
    
    _dbTable.estatus.default = TEMPRESA_EMBARQUES.ESTATUS.ABIERTO
    _dbTable.estatus.writable = False
    
    if(_s_action in (request.stv_fwk_permissions.btn_edit.code, request.stv_fwk_permissions.btn_save.code,)):
        if(_s_id):
            _dbRow_embarque = dbc01.tempresa_embarques(_s_id)
            
            if(_dbRow_embarque.estatus == TEMPRESA_EMBARQUES.ESTATUS.CERRADO):
                _dbTable.fechainicio.writable = False
                _dbTable.tipoembarque.writable = False
                _dbTable.empresa_transporte_id.writable = False
                _dbTable.chofer.writable = False
                _dbTable.guiaembarque.writable = False
                _dbTable.estatus.writable = False
                _dbTable.descripcion.writable = False
            else:
                pass
        else:
            pass
        
    else:
        pass
    
    _D_result_empresa_id = TCUENTA_APLICACIONES.GET_EMPRESA_ID_CACHE(
        _s_cuenta_aplicacion_id
        )
        
    if _D_result_empresa_id.E_return != CLASS_e_RETURN.OK:
        #Si entra acá es que ocurrió un error durante la obtención del concepto_id
        _s_empresa_id = None
        stv_flashError("Empresa ID no encontrado.", _D_result_empresa_id.s_msgError)    
    elif _D_result_empresa_id.n_empresa_id:
        _s_empresa_id = _D_result_empresa_id.n_empresa_id
    else:
        #hubo bronca, no trajo error pero tampoco trajo la empresa_id
        _s_empresa_id = None
        stv_flashError("Empresa ID no encontrado.", "Empresa no definida para almacenes.")

    _dbTable.empresa_id.default = _s_empresa_id   
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(),
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = STV_CFG_APP_ALMACENES.ID,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.fechainicio,
            _dbTable.tipoembarque,
            _dbTable.empresa_transporte_id,
            _dbTable.chofer,
            _dbTable.guiaembarque,
            _dbTable.estatus
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
    )

    
    ###TODO: Faltan validaciones
    _O_cat.addRowContentEvent('onValidation', TEMPRESA_EMBARQUES.VALIDACION)
   
    _D_returnVars = _O_cat.process()    

    if (_s_action == request.stv_fwk_permissions.btn_save.code) and (_D_returnVars.rowContent_form.accepted):
        
        #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
        from gluon.storage import List
        request.args = List([request.stv_fwk_permissions.btn_view.code, _D_returnVars.rowContent_form.vars.id])
        _D_returnVars = empresa_embarques()
        
    else:
        _D_returnVars.update(
            dict(
                msg = 'Bienvenidos',
                dbFieldAlmacenes = STV_LIB_APP_ALMACENES.GET_DBFIELD_ALMACENES(
                    n_default = _s_empresa_plaza_sucursal_almacen_id or 0,
                    b_writable = False
                    ),
                L_breadcrub = [
                    STV_ALMACENES.BREADCRUB.BC_INDEX,
                    STV_ALMACENES.BREADCRUB.MOVTO.SALIDA.BC_EMBARQUES
                    ]
                )
            )

    _O_cat.addSubTab(
        s_tabName = "Ruta (Almacenes)",
        s_url = URL(f='empresa_embarque_almacenes',  args=['master_'+str(request.function),request.args(1)]),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_embarque_almacenes', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        s_tabName = "Movimientos",
        s_url = URL(f='empresa_embarque_movimientos',  args=['master_'+str(request.function),request.args(1)]),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_embarque_movimientos', s_masterIdentificator = str(request.function))
        )
    
    return _D_returnVars

def empresa_embarque_almacenes():
    '''Formulario para los almacenes a los que van los embarques
        
        Args:
            arg0: master_[nombre función maestro]
            arg1: empresa_embarque_id
            [arg2]: codigo de funcionalidad
            [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    '''
    
    _dbTable = dbc01.tempresa_embarque_almacenes
    _s_empresa_embarque_id = request.args(1, None)
    _s_action = request.args(2, request.stv_fwk_permissions.btn_none.code)
    _s_id = request.args(3, None)
    
    _dbTable.estatus.default = TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.EN_ESPERA
    _dbTable.estatus.writable = False
    
    if(
        (_s_action == request.stv_fwk_permissions.btn_edit.code)
        or (
            (_s_action == request.stv_fwk_permissions.btn_save.code)
            and (_s_id)
            )
        ):
        _s_empresa_embarque_almacen_id = request.args(3, None)
        
        _dbRow_empresa_embarque_almacen = dbc01.tempresa_embarque_almacenes(_s_empresa_embarque_almacen_id)
        #Si el almacén del embarque está cerrado no se permitirá editar.
        if(_dbRow_empresa_embarque_almacen.estatus == TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CERRADO):
            _dbTable.empresa_plaza_sucursal_almacen_id.writable = False
            _dbTable.orden.writable = False
#            _dbTable.estatus.writable = False
            _dbTable.fechasalida.writable = False
        else:
            pass
        
    else:
        pass
    
    _dbRow_maestro = dbc01.tempresa_embarques(_s_empresa_embarque_id)
    
    
    if(_dbRow_maestro.estatus != TEMPRESA_EMBARQUES.ESTATUS.CERRADO):
        #Si el embarque está abierto, en ruta permitrá hacer todo        
        _L_formButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_new,
            request.stv_fwk_permissions.btn_save,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_edit,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_remove,
            request.stv_fwk_permissions.btn_find,
            request.stv_fwk_permissions.btn_findall,
            ]
    else:
        # Pero si está cerrado, no permitirá hacer nada
        _L_formButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_findall
            ]
    
    _L_OrderBy = [
        _dbTable.orden,
        ]
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_embarques, 
        xFieldLinkMaster = [
            _dbTable.empresa_embarque_id
            ],
        L_visibleButtons = _L_formButtons, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.empresa_plaza_sucursal_almacen_id,
            _dbTable.orden,
            _dbTable.estatus,
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
    )
    
    _O_cat.addRowSearchResultsProperty('orderBy', _L_OrderBy)

    _O_cat.addRowContentEvent('onValidation', _empresa_embarque_almacenes_validation)
    
    if(_dbRow_maestro.estatus != TEMPRESA_EMBARQUES.ESTATUS.CERRADO):
        #Se crea el botón especial 1 para poder cerrar el almacén.
        _O_cat.addPermissionConfig(
            s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
            s_permissionLabel = "Cerrar almacén",
            s_permissionIcon = "fa fa-compass",
            s_type = "button",
            s_send = "find",
            L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
            D_actions = Storage(
                js = Storage(
                    sfn_onPress = 'stv_fwk_button_press',
                    sfn_onSuccess = 'stv_fwk_button_success',
                    sfn_onError = 'stv_fwk_button_error',
                    ),
                swap = {},
            confirm = Storage(
                s_title = "Favor de confirmar operación", 
                s_text = "¿Está seguro de querer cerrar este almacén?", 
                s_type = "warning", 
                s_btnconfirm = "Aceptar", 
                s_btncancel = "Cancelar", 
                )   
                ),
            L_options = []
            )
        
        #Se crea el botón especial 2 para poder cambiar el estatus del almacén a en_espera.
        _O_cat.addPermissionConfig(
            s_permissionCode = request.stv_fwk_permissions.special_permisson_02.code,
            s_permissionLabel = "Cambiar almacén de cerrado a 'En espera'",
            s_permissionIcon = "fa fa-compass",
            s_type = "button",
            s_send = "find",
            L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
            D_actions = Storage(
                js = Storage(
                    sfn_onPress = 'stv_fwk_button_press',
                    sfn_onSuccess = 'stv_fwk_button_success',
                    sfn_onError = 'stv_fwk_button_error',
                    ),
                swap = {},
            confirm = Storage(
                s_title = "Favor de confirmar operación", 
                s_text = "¿Está seguro de querer cambiar el estatus de este almacén?", 
                s_type = "warning", 
                s_btnconfirm = "Aceptar", 
                s_btncancel = "Cancelar", 
                )   
                ),
            L_options = []
            )
    else:
        pass
    _b_skipAction = False
    _D_overrideView = None
    if(_s_action == request.stv_fwk_permissions.special_permisson_01.code):
        _dbRow_empresa_embarque_almacen = dbc01.tempresa_embarque_almacenes(_s_id)
        #Si el almacén del embarque es diferente de cerrado se permitirá cerrar el almacén .
        if(_dbRow_empresa_embarque_almacen.estatus != TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CERRADO):
            
            _dbRows_empresa_embarque_almacenes = dbc01(
                (dbc01.tempresa_embarque_almacenes.empresa_embarque_id == _s_empresa_embarque_id)
                &(dbc01.tempresa_embarque_almacenes.estatus.belongs(
                    TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.EN_ESPERA,
                    TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CARGANDO,
                    TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.DESCARGANDO,
                    )
                  )
                ).select(
                    dbc01.tempresa_embarque_almacenes.ALL,
                    orderby = [
                        ~dbc01.tempresa_embarque_almacenes.orden
                        ]
                    ).last()
                    
            if((_dbRows_empresa_embarque_almacenes) ):
                if(
                    (_dbRows_empresa_embarque_almacenes.empresa_plaza_sucursal_almacen_id == _dbRow_empresa_embarque_almacen.empresa_plaza_sucursal_almacen_id)
                    and 
                    (_dbRows_empresa_embarque_almacenes.orden == _dbRow_empresa_embarque_almacen.orden)
                    ):
                    
                    dbc01(
                        (_dbTable.id == _dbRow_empresa_embarque_almacen.id)
                        ).update(
                            estatus = TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CERRADO
                            )
                        
                    stv_flashSuccess("Almacén de embarque", "Se cerró el almacén correctamente")                         
                    _D_overrideView = request.stv_fwk_permissions.view_refresh  
                    
                    #Row para saber si existe aún algún almacén abierto, en espera o descargando en este embarque. 
                    _dbRows = dbc01(
                        (dbc01.tempresa_embarque_almacenes.empresa_embarque_id == _s_empresa_embarque_id)
                        &(dbc01.tempresa_embarque_almacenes.estatus.belongs(
                            TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.EN_ESPERA,
                            TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CARGANDO,
                            TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.DESCARGANDO,
                            )
                          )
                        ).select(
                            dbc01.tempresa_embarque_almacenes.ALL,
                            )
                        
                    if not _dbRows:
                        #Si no tiene ningún almacén abierto ya entonces se procede a cerrar el embarque. 
                        dbc01(
                            (dbc01.tempresa_embarques.id == _s_empresa_embarque_id)
                            ).update(
                                estatus = TEMPRESA_EMBARQUES.ESTATUS.CERRADO
                                )
                        
                        stv_flashSuccess("Embarque", "Este es el último almacén del embarque, se procede a cerrar el embarque.")                         

                    else:
                        #no pasa nada todavía hay almacenes pendientes. 
                        pass

                else:
                    stv_flashError("Almacén de embarque.", "Se encontraron almacenes con estatus diferente de cerrado antes de este.")
                    _D_overrideView = request.stv_fwk_permissions.view_refresh  
            else:
                pass
        else:
            stv_flashError("Almacén de embarque.", "Este almacén ya se encuentra cerrado.")
            _D_overrideView = request.stv_fwk_permissions.view_refresh
    else:
        pass
    
    if(_s_action in (request.stv_fwk_permissions.special_permisson_02.code,)):
        
        _dbRow_embarque_almacen = dbc01.tempresa_embarque_almacenes(_s_id)
        
        #este row devolverá si existe un almacén Cargando, descargando, cerrado después del almacén que se quiere cambiar el estatus.
        _dbRows_embarque_almacenes = dbc01(
            (dbc01.tempresa_embarque_almacenes.empresa_embarque_id == _s_empresa_embarque_id)
            &(dbc01.tempresa_embarque_almacenes.orden > _dbRow_embarque_almacen.orden)
            &(dbc01.tempresa_embarque_almacenes.estatus  != TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.EN_ESPERA)
            ).select(
                dbc01.tempresa_embarque_almacenes.id,
                orderby = [
                    dbc01.tempresa_embarque_almacenes.orden
                    ]
                )
        
        if(_dbRows_embarque_almacenes):
            stv_flashError("Almacén de embarque.", "Este almacén no puede cambiar estatus ya que existe uno o más almacenes con movimientos adelante de él.")
            _D_overrideView = request.stv_fwk_permissions.view_refresh
        else:
            #No hay almacenes cerrados adelante de él, se procede a ver si no cuenta con nignún movimiento de traspaso asociado.
            
            _dbRow_empresa_invetariomovimientos = dbc01(
                (dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id == _s_id)
                &(dbc01.tempresa_inventariomovimientos.estatus_movimiento != TEMPRESA_INVENTARIOMOVIMIENTOS.ESTATUS_MOVIMIENTO.CARGANDO)
                ).select(
                    dbc01.tempresa_inventariomovimientos.ALL
                    )
                
            if(_dbRow_empresa_invetariomovimientos):
                stv_flashError("Almacén de embarque.", "Este almacén no puede cambiar estatus ya que existen movimientos asociados a este almacén del embarque.")
                _D_overrideView = request.stv_fwk_permissions.view_refresh
            else:
                #No hay movimientos asociados a este almacén, se procede a cambiar el estatus.
                dbc01(
                    (_dbTable.id == _s_id)
                    ).update(
                        estatus = TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.EN_ESPERA
                        )
                
                stv_flashSuccess("Almacén de embarque", "Se regresó el estatus a 'en espera' correctamente.")                         
                _D_overrideView = request.stv_fwk_permissions.view_refresh  
        
    else:
        pass
    
    if(_s_action in (request.stv_fwk_permissions.btn_edit.code, request.stv_fwk_permissions.btn_remove.code)):
        _dbRow_embarque_almacen = dbc01.tempresa_embarque_almacenes(_s_id)
        
        #este row devolverá si existe un almacén cerrado después del almacén que se quiere cerrar
        _dbRows_embarque_almacenes = dbc01(
            (dbc01.tempresa_embarque_almacenes.empresa_embarque_id == _s_empresa_embarque_id)
            &(dbc01.tempresa_embarque_almacenes.orden > _dbRow_embarque_almacen.orden)
            &(dbc01.tempresa_embarque_almacenes.estatus == TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CERRADO)
            ).select(
                dbc01.tempresa_embarque_almacenes.id,
                orderby = [
                    dbc01.tempresa_embarque_almacenes.orden
                    ]
                )
        
        if(_dbRows_embarque_almacenes):
            stv_flashError("Almacén de embarque.", "Este almacén no puede editarse ni eliminarse ya que existe uno o más almacenes cerrados adelante de él.")
            _D_overrideView = request.stv_fwk_permissions.view_refresh
            #Se usa el _b_skipAction para hacerle bypass al eliminar
            _b_skipAction = True
        else:
            #No hay almacenes cerrados adelante de él entonces puede cerrarse sin problemas. 
            pass
        
    else:
        pass

    if(_D_overrideView):
        _D_returnVars = _O_cat.process(D_overrideView = _D_overrideView, b_skipAction = _b_skipAction)
    else:
        _D_returnVars = _O_cat.process()   
         
    return _D_returnVars

### Cree la validación acá y no utilicé la de la clase TEMPRESA_EMBARQUE porque el request.args es diferente
###se tiene que pensar mejor esa parte. De momento se queda así.
def _empresa_embarque_almacenes_validation(O_form):
    '''Validación para los almacenes de embarque.
    '''
    _dbTable = dbc01.tempresa_embarque_almacenes
    
    _s_empresa_embarque_id = request.args(1, None)
    _s_empresa_embarque_almacen_id = request.args(3, None)
    
    _s_orden = O_form.vars.orden
    _s_empresa_plaza_sucursal_almacen_id = O_form.vars.empresa_plaza_sucursal_almacen_id
    _s_id = O_form.record_id
    
    #Se hace la condición porque _s_orden puede ir vacío
    if _s_orden:
        _dbRows = dbc01(
            (_dbTable.empresa_embarque_id == _s_empresa_embarque_id)
            &(_dbTable.orden == _s_orden)
            & (_dbTable.id != _s_id)  # Se ignora el registro que se esté modificando
            ).select(_dbTable.id)
        if _dbRows:
            O_form.errors.orden = "El número de orden no puede repetirse por embarque."
        else:
            
            _dbRows = dbc01(
                (_dbTable.empresa_embarque_id == _s_empresa_embarque_id)
                &(_dbTable.estatus == TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CERRADO)
                ).select(
                    _dbTable.ALL,
                    orderby = [
                        ~_dbTable.orden
                        ]
                    ).first()
                    
            if(_dbRows):
                if(_s_orden < (_dbRows.orden or 0)):
                    O_form.errors.orden = "El número de orden no puede ser menor a un número de orden ya cerrado."
                else:
                    pass
            else:
                #Puede no traer nada si no hay cerrados o es el primer registro.
                pass
            
    else:
        #O_form.errors.orden = "El número de orden no puede ir vacío."
        pass

#     _dbRows = dbc01(
#         (_dbTable.empresa_embarque_id == _s_empresa_embarque_id)
#         &(_dbTable.empresa_plaza_sucursal_almacen_id == _s_empresa_plaza_sucursal_almacen_id)
#         & (_dbTable.id != _s_id)  # Se ignora el registro que se esté modificando
#         ).select(_dbTable.id)
#     if _dbRows:
#         O_form.errors.empresa_plaza_sucursal_almacen_id = "El almacén no puede repetirse por embarque."
#     else:
#         pass
        
    return O_form 


def empresa_embarque_movimientos():
    ''' Formulario detalle de los movimientos de inventario
    
        Args:
            arg0: master_[nombre función maestro]
            arg1: empresa_embarque_id
            [arg2]: codigo de funcionalidad
            [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    '''
    
    _dbTable = dbc01.tempresa_inventariomovimientos

    _s_empresa_embarque_id = request.args(1, None)
    
    _s_cuenta_aplicacion_id = D_stvFwkCfg.cfg_cuenta_aplicacion_id
    
    _D_result_empresa_id = TCUENTA_APLICACIONES.GET_EMPRESA_ID_CACHE(
        _s_cuenta_aplicacion_id
        )
        
    if _D_result_empresa_id.E_return != CLASS_e_RETURN.OK:
        #Si entra acá es que ocurrió un error durante la obtención del concepto_id
        _s_empresa_id = None
        stv_flashError("Empresa ID no encontrado.", _D_result_empresa_id.s_msgError)    
    elif _D_result_empresa_id.n_empresa_id:
        _s_empresa_id = _D_result_empresa_id.n_empresa_id
    else:
        #hubo bronca, no trajo error pero tampoco trajo la empresa_id
        _s_empresa_id = None
        stv_flashError("Empresa ID no encontrado.", "Empresa no definida para almacenes.")
    
    _D_result_concepto_id = TEMPRESA_ALMACENESCONCEPTOS.GET_ID_CACHE(
        n_empresa_id = _s_empresa_id, 
        n_tipoconcepto = TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.SALIDA, 
        n_clasificacion = TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.TRASPASO_SALIDA
        )
    
    if _D_result_concepto_id.E_return != CLASS_e_RETURN.OK:
        #Si entra acá es que ocurrió un error durante la obtención del concepto_id
        _n_concepto_id = None
        stv_flashError("Salida por traspaso", _D_result_concepto_id.s_msgError)        
    elif _D_result_concepto_id.n_concepto_id:
        _n_concepto_id = _D_result_concepto_id.n_concepto_id
    
    _qry = (
        (_dbTable.empresa_embarque_id == _s_empresa_embarque_id)
        &(_dbTable.empresa_almacenesconcepto_id == _n_concepto_id)
        )
    
    _dbTable.ordencompra.label = 'Orden traspaso'
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        dbTableMaster = None, 
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none, 
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_findall,
            request.stv_fwk_permissions.btn_cancel,
            
            ], 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.empresa_plaza_sucursal_almacen_id,
            _dbTable.fecha_movimiento,
            _dbTable.folio,
            _dbTable.estatus_movimiento,
            _dbTable.empresa_embarque_almacen_id_relacionado,
            _dbTable.observaciones,
            ], 
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:2]
    )
    
    
    _D_returnVars = _O_cat.process()
    
    return (_D_returnVars)
