# -*- coding: utf-8 -*-

def movto_entrada_porcompra():
    '''Función para las entradas por compra
        Args:
            [arg0]: codigo de funcionalidad
            [arg1]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    '''
    
    _s_action = request.args(0, request.stv_fwk_permissions.btn_none.code)
        
    _dbTable = dbc01.tempresa_inventariomovimientos
    
    _s_empresa_plaza_sucursal_almacen_id = request.vars.master_empresa_plaza_sucursal_almacen_id
    
    _s_cuenta_aplicacion_id = D_stvFwkCfg.cfg_cuenta_aplicacion_id
    
    _s_empresa_inventariomovimiento_id = request.args(1, None)
    
    #_L_searchFilter se utiliza en la forma buscar para los CFDIs recibidos por proveedor.
    _L_searchFilter = [
        Storage(
            s_input_name = str(_dbTable.empresa_proveedor_id),
            s_reference_name = str(dbc01.tcfdis.emisorproveedor_id),
            b_mandatory = True
            )
        ]
    
    _dbTable.cfdi_id.widget = lambda f, v: stv_widget_db_search(
        f, 
        v, 
        s_display = '%(serie)s %(folio)s', 
        s_url = URL( a = 'app_almacenes', c = '600busquedas', f = 'buscar_cfdis_recibidos'),
        D_additionalAttributes = {
            'reference':dbc01.tcfdis.id,
            'L_searchFilter':_L_searchFilter,
            }
        )
    
    _D_result_empresa_id = TCUENTA_APLICACIONES.GET_EMPRESA_ID_CACHE(
        _s_cuenta_aplicacion_id
        )
    
    if _D_result_empresa_id.E_return != CLASS_e_RETURN.OK:
        #Si entra acá es que ocurrió un error durante la obtención del concepto_id
        _s_empresa_id = None
        stv_flashError("Empresa ID no encontrado.", _D_result_empresa_id.s_msgError)        
    elif _D_result_empresa_id.n_empresa_id:
        _s_empresa_id = _D_result_empresa_id.n_empresa_id
    else:
        #hubo bronca, no trajo error pero tampoco trajo la empresa_id
        _s_empresa_id = None
        stv_flashError("Empresa ID no encontrado.", "Empresa no definida para almacenes.")     
        
        
    _D_result_concepto_id = TEMPRESA_ALMACENESCONCEPTOS.GET_ID_CACHE(
        n_empresa_id = _s_empresa_id, 
        n_tipoconcepto = TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.ENTRADA, 
        n_clasificacion = TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.COMPRA
        )
    
    # Por default el query se pone en false para no regresar nada en las búsquedas
    _qry = False
    
    if _D_result_concepto_id.E_return != CLASS_e_RETURN.OK:
        #Si entra acá es que ocurrió un error durante la obtención del concepto_id
        _n_concepto_id = None
        stv_flashError("Entrada por compra", _D_result_concepto_id.s_msgError)        
    elif _D_result_concepto_id.n_concepto_id:
        _n_concepto_id = _D_result_concepto_id.n_concepto_id
        _dbTable.empresa_almacenesconcepto_id.default = _n_concepto_id
        _dbTable.empresa_almacenesconcepto_id.writable = False

        _qry = (
            (_dbTable.empresa_id == _s_empresa_id)
            &(_dbTable.empresa_almacenesconcepto_id == _n_concepto_id)
            &(_dbTable.empresa_plaza_sucursal_almacen_id == _s_empresa_plaza_sucursal_almacen_id)
            )
        
        #El Query tiene que mostrar las entradas que estén pendientes por asociar un CFDI. 
        if _s_action in (request.stv_fwk_permissions.btn_new.code, request.stv_fwk_permissions.btn_none.code):
            #Se agrega este query.
            _qry &= (_dbTable.cfdi_id == None)
        else:
            #Si es buscar todo el Query no aplica, ya que se puede filtrar todas las entradas
            pass
    
    else:
        #hubo bronca, no trajo error pero tampoco trajo concepto_id
        _n_concepto_id = None
        stv_flashError("Entrada por compra", "Concepto no defnido en Almacenes-Conceptos")        

    _dbTable.empresa_id.default = _s_empresa_id
    _dbTable.empresa_plaza_sucursal_almacen_id.default = _s_empresa_plaza_sucursal_almacen_id

    #Se asigna la fecha de registro con el día y hora actual. 
    _dbTable.fecha_registro.writable = False
    _dbTable.fecha_registro.default = request.browsernow

    _dbTable.empresa_proveedor_id.writable = True
    
    _L_dbFields = [
        _dbTable.id,
        _dbTable.empresa_proveedor_id,
        _dbTable.tipodocumento,
        _dbTable.remision,
        _dbTable.ordencompra,
        _dbTable.cfdi_id,
        _dbTable.fecha_registro,
        _dbTable.fecha_movimiento,
        ]
    
    if _n_concepto_id:
        # Si concepto esta definido, permitir hacer todo
        _L_formButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_new,
            request.stv_fwk_permissions.btn_save,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_edit,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_remove,
            request.stv_fwk_permissions.btn_find,
            request.stv_fwk_permissions.btn_findall,
            ]
    else:
        # Si concepto no esta definido, no permitir hacer nada
        _L_formButtons = [
            request.stv_fwk_permissions.btn_none,
            ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = _L_formButtons, 
        s_tabId = STV_CFG_APP_ALMACENES.ID,
        L_fieldsToShow = _L_dbFields,
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
    )

    _O_cat.addRowContentEvent('onValidation', TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.ENTRADA.PORCOMPRA)
        
    _O_cat.addRowContentEvent('onAccepted', TEMPRESA_INVENTARIOMOVIMIENTOS.ACEPTADO.ENTRADA.PORCOMPRA)

    _D_returnVars = _O_cat.process()

    if (_s_action == request.stv_fwk_permissions.btn_save.code) and (_D_returnVars.rowContent_form.accepted):
        
        #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
        from gluon.storage import List
        request.args = List([request.stv_fwk_permissions.btn_view.code, _D_returnVars.rowContent_form.vars.id])
        _D_returnVars = movto_entrada_porcompra()
        
    else:
        _D_returnVars.update(
            dict(
                msg = 'Bienvenidos',
                dbFieldAlmacenes = STV_LIB_APP_ALMACENES.GET_DBFIELD_ALMACENES(
                    n_default = _s_empresa_plaza_sucursal_almacen_id or 0,
                    b_writable = False
                    ),
                L_breadcrub = [
                    STV_ALMACENES.BREADCRUB.BC_INDEX,
                    STV_ALMACENES.BREADCRUB.MOVTO.ENTRADA.BC_PORCOMPRA
                    ]
                )
            )
        
    if _n_concepto_id:
        # Si concepto esta definido, permitir el detalle

        _O_cat.addSubTab(
            dbTableDetail = dbc01.tempresa_invmovto_productos, 
            s_url = URL(f='movto_entrada_porcompra_resumen', args=['master_'+str(request.function),request.args(1)]),
            s_idHtml = fn_createIDTabUnique(f = 'movto_entrada_porcompra_resumen', s_masterIdentificator = str(request.function))  
            )
    else:
        # de lo contrario, no mostrar el detalle
        pass
    
    
    if(_s_empresa_inventariomovimiento_id):
        _dbRow_movto = _dbTable(_s_empresa_inventariomovimiento_id)
        if(_dbRow_movto and _dbRow_movto.cfdi_id):
            _O_cat.addSubTab(
                s_tabName = "CFDI relacionado", 
                s_url = URL(a = 'app_timbrado', c= '250cfdi', f='cfdi_entradasalida_relacionado', args=['master_'+str(request.function), _dbRow_movto.cfdi_id]),
                s_idHtml = fn_createIDTabUnique(a = 'app_timbrado', c= '250cfdi', f = 'cfdi_entradasalida_relacionado', s_masterIdentificator = str(request.function))  
                )
        else:
            pass
    else:
        pass
    return _D_returnVars


def movto_entrada_porcompra_resumen():
    '''Función para las entradas por compra por remisión 
    
        Args:
            arg0: master_[nombre función maestro]
            arg1: empresa_inventariomovimiento_id
            [arg2]: codigo de funcionalidad
            [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    '''
    
    _dbTable = dbc01.tempresa_invmovto_productos
    
    _s_empresa_inventariomovimiento_id = request.args(1, None)

    _s_action = request.args(2, request.stv_fwk_permissions.btn_none.code)

    _s_empresa_inventariomovimientoresumen_id = request.args(3, None)

    _dbTable.descripcion.label = "Descripción CFDI"

    _dbTable.cfdi_concepto_id.readable = True
    _dbTable.cfdi_concepto_id.writable = True

    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.empresa_producto_id,
        _dbTable.cfdi_concepto_id,
        _dbTable.producto_externo,
        _dbTable.cantidad_entrada,
        _dbTable.preciounitario,
        _dbTable.costototal,
        _dbTable.tipo_inconsistencia,        
        ]
    
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_inventariomovimientos,
        xFieldLinkMaster = [
            _dbTable.empresa_inventariomovimiento_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
        )
    
    _dbRow_cfdi_concepto = None
        
    if(_s_empresa_inventariomovimientoresumen_id):
        # Si se encuentra viendo o editando o grabando y se identifica el resumen en el CFDI, se crea un registro para mostrar la inconsistencia en la vista.
        # Así mismo si coincide el producto_externo con el noidentificación se desabilita la edicion del produto_externo
        
        _dbRow_movimientoMaestro = dbc01.tempresa_inventariomovimientos(_s_empresa_inventariomovimiento_id)
        
        if(_dbRow_movimientoMaestro and _dbRow_movimientoMaestro.cfdi_id):
            
            _dbRow_movimientoResumen = _dbTable(_s_empresa_inventariomovimientoresumen_id)

            _dbRows_cfdi_conceptos = dbc01(
                (dbc01.tcfdi_conceptos.cfdi_id == _dbRow_movimientoMaestro.cfdi_id)
                &(dbc01.tcfdi_conceptos.noidentificacion == _dbRow_movimientoResumen.producto_externo)
                ).select(
                    dbc01.tcfdi_conceptos.id,
                    dbc01.tcfdi_conceptos.empresa_prodserv_id,
                    dbc01.tcfdi_conceptos.noidentificacion,
                    dbc01.tcfdi_conceptos.cantidad,
                    dbc01.tcfdi_conceptos.valorunitario,
                    dbc01.tcfdi_conceptos.descripcion,
                    dbc01.tcfdi_conceptos.claveunidad,
                    dbc01.tcfdi_conceptos.importe,
                    dbc01.tempresa_invmovto_productos.cantidad_entrada.coalesce_zero().sum().with_alias('registrados'),
                    left = [
                        dbc01.tempresa_invmovto_productos.on(
                            (dbc01.tempresa_invmovto_productos.cfdi_concepto_id == dbc01.tcfdi_conceptos.id)
                            & (dbc01.tempresa_invmovto_productos.id != _s_empresa_inventariomovimientoresumen_id)
                            ),
                        ],
                    groupby = [
                        dbc01.tcfdi_conceptos.id,
                        dbc01.tcfdi_conceptos.empresa_prodserv_id,
                        dbc01.tcfdi_conceptos.noidentificacion,
                        dbc01.tcfdi_conceptos.cantidad,
                        dbc01.tcfdi_conceptos.valorunitario,
                        dbc01.tcfdi_conceptos.descripcion,
                        dbc01.tcfdi_conceptos.claveunidad,
                        dbc01.tcfdi_conceptos.importe,
                        ]
                    
                    )
            
            #Si no trajo concepto es porque no existe registro con el producto externo que se escribió en el formulario, se revisa si por producto aparece el concepto.    
            if not (_dbRows_cfdi_conceptos) and (_dbRow_movimientoResumen.empresa_producto_id):
                
                #Query para saber si existe un concepto con el empresa_producto_id en ese CFDI.
                _dbRows_cfdi_conceptos = dbc01(
                    (dbc01.tcfdi_conceptos.cfdi_id == _dbRow_movimientoMaestro.cfdi_id)
                    &(dbc01.tcfdi_conceptos.empresa_prodserv_id == _dbRow_movimientoResumen.empresa_producto_id)
                    ).select(
                        dbc01.tcfdi_conceptos.id,
                        dbc01.tcfdi_conceptos.empresa_prodserv_id,
                        dbc01.tcfdi_conceptos.noidentificacion,
                        dbc01.tcfdi_conceptos.cantidad,
                        dbc01.tcfdi_conceptos.valorunitario,
                        dbc01.tcfdi_conceptos.descripcion,
                        dbc01.tcfdi_conceptos.claveunidad,
                        dbc01.tcfdi_conceptos.importe,
                        dbc01.tempresa_invmovto_productos.cantidad_entrada.coalesce_zero().sum().with_alias('registrados'),
                        left = [
                            dbc01.tempresa_inventariomovimientos.on(
                                (dbc01.tempresa_inventariomovimientos.cfdi_id == dbc01.tcfdi_conceptos.cfdi_id)
                                ),
                            dbc01.tempresa_invmovto_productos.on(
                                (dbc01.tempresa_invmovto_productos.empresa_inventariomovimiento_id == dbc01.tempresa_inventariomovimientos.id)
                                & (dbc01.tempresa_invmovto_productos.empresa_producto_id == dbc01.tcfdi_conceptos.empresa_prodserv_id)
                                & (dbc01.tempresa_invmovto_productos.id != _s_empresa_inventariomovimientoresumen_id)
                                ),
                            ],
                        groupby = [
                            dbc01.tcfdi_conceptos.id,
                            dbc01.tcfdi_conceptos.empresa_prodserv_id,
                            dbc01.tcfdi_conceptos.noidentificacion,
                            dbc01.tcfdi_conceptos.cantidad,
                            dbc01.tcfdi_conceptos.valorunitario,
                            dbc01.tcfdi_conceptos.descripcion,
                            dbc01.tcfdi_conceptos.claveunidad,
                            dbc01.tcfdi_conceptos.importe,
                            ]
                        
                        )
                
            else:
                pass
                
            if len(_dbRows_cfdi_conceptos) == 1:
                if(_dbRow_movimientoResumen.producto_externo == _dbRows_cfdi_conceptos.last().tcfdi_conceptos.noidentificacion):
                    _dbTable.producto_externo.writable = False
                else:
                    pass
                _dbRow_cfdi_concepto = _dbRows_cfdi_conceptos.last()                    
                
            else:
                pass
        else:
            pass
    else:
        pass


    _O_cat.addRowContentEvent('onValidation', TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.ENTRADA.PORCOMPRA_RESUMEN)
   
    _D_returnVars = _O_cat.process()

    if ((_s_action == request.stv_fwk_permissions.btn_save.code) and (_D_returnVars.rowContent_form.accepted)):
        
        _D_result = TEMPRESA_INVMOVTO_PRODUCTOS.VALIDAR_ROW_ENTRADA_COMPRA(
            _D_returnVars.rowContent_form.vars.id
            )
        
        if _D_result.E_return != CLASS_e_RETURN.OK:
            stv_flashWarning("Inconsistencias en producto", _D_result.s_msgError)  
        else:
            pass

    else:
        #Si es cualquier opción en vez de salvar no hace nada
        pass
    
    _D_returnVars.dbRow_cfdi_concepto = _dbRow_cfdi_concepto
    
    return _D_returnVars

def movto_entrada_pordevolucion():
    '''Función para las entradas por devolución
            
        Args:
            [arg0]: codigo de funcionalidad
            [arg1]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    '''
    
    _s_action = request.args(0, None)
    
    _dbTable = dbc01.tempresa_inventariomovimientos
    
    _s_empresa_id = db.tcuenta_aplicaciones(D_stvFwkCfg.cfg_cuenta_aplicacion_id).empresa_id
    _dbTable.empresa_id.default = _s_empresa_id
    
    _dbRows_conceptos = dbc01(
        (dbc01.tempresa_almacenesconceptos.empresa_id == _s_empresa_id)
        & (dbc01.tempresa_almacenesconceptos.tipoconcepto == TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.ENTRADA)
        & (dbc01.tempresa_almacenesconceptos.clasificacion == TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.DEVOLUCION_VENTA)
        ).select(
            dbc01.tempresa_almacenesconceptos.id,
            orderby = ~dbc01.tempresa_almacenesconceptos.id
            )
        
    if _dbRows_conceptos:
        _dbTable.empresa_almacenesconcepto_id.default = _dbRows_conceptos.first().id
        _dbTable.empresa_almacenesconcepto_id.writable = False
    else:
        #TODO  Mostrar error de que no existe un concepto de entrada por compra para la empresa
        pass
    
    #El Query tiene que mostrar las entradas que estén pendientes por asociar un CFDI. 
    _qry = (
        (_dbTable.empresa_id == _s_empresa_id)
        &(_dbTable.empresa_almacenesconcepto_id == _dbRows_conceptos.first().id)
        &(_dbTable.cfdi_id == None)
        )
    
    _L_dbFields = [
        _dbTable.id,
        _dbTable.empresa_proveedor_id,
        _dbTable.tipodocumento,
        _dbTable.serie,
        _dbTable.folio,
        _dbTable.remision,
        ]
    
    _L_formButtons = [
        request.stv_fwk_permissions.btn_none,
        request.stv_fwk_permissions.btn_new,
        request.stv_fwk_permissions.btn_save,
        request.stv_fwk_permissions.btn_cancel,
        request.stv_fwk_permissions.btn_edit,
        request.stv_fwk_permissions.btn_view,
        #request.stv_fwk_permissions.btn_print,
        #request.stv_fwk_permissions.btn_invalidate,
        #request.stv_fwk_permissions.btn_validate,
        #request.stv_fwk_permissions.btn_suspend,
        #request.stv_fwk_permissions.btn_activate,
        #request.stv_fwk_permissions.btn_remove,
        #request.stv_fwk_permissions.btn_restore,
        request.stv_fwk_permissions.btn_find,
        request.stv_fwk_permissions.btn_findall,
        #request.stv_fwk_permissions.btn_signature,
        ]
        
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = _L_formButtons, 
        s_tabId = STV_CFG_APP_ALMACENES.ID,
        L_fieldsToShow = _L_dbFields, 
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
    )
    
    
    
    if _s_action in (request.stv_fwk_permissions.btn_new.code, request.stv_fwk_permissions.btn_edit.code, request.stv_fwk_permissions.btn_save.code, request.stv_fwk_permissions.btn_view.code):
        # Acciones permitidas
        pass
    else:
        _s_action = request.stv_fwk_permissions.btn_new.code
    
    _D_returnVars = _O_cat.process(
        s_overrideCode = _s_action,  # Usar el action 
        b_skipAction = False,
        #L_formFactoryFields = _L_dbFields
        )

    if (_s_action == request.stv_fwk_permissions.btn_save.code) and (_D_returnVars.rowContent_form.accepted):
        #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
        from gluon.storage import List
        request.args = List([request.stv_fwk_permissions.btn_view.code, _D_returnVars.rowContent_form.vars.id])
        _D_returnVars = movto_entrada_porcompra()
         
    else:
        _D_returnVars.update(
            dict(
                msg = 'Bienvenidos',
                dbFieldAlmacenes = STV_LIB_APP_ALMACENES.GET_DBFIELD_ALMACENES(
                    n_default = request.vars.master_empresa_plaza_sucursal_almacen_id or 0,
                    b_writable = False
                    ),
                L_breadcrub = [
                    STV_ALMACENES.BREADCRUB.BC_INDEX,
                    STV_ALMACENES.BREADCRUB.MOVTO.ENTRADA.BC_PORDEVOLUCION
                    ]
                )
            )
        
#        _D_returnVars.D_useView.L_actionsCreateView += [request.stv_fwk_permissions.btn_new.code, request.stv_fwk_permissions.btn_edit.code]
        _D_returnVars.D_useView.E_buttonsBarPosition = request.stv_fwk_permissions.E_POSITION_DOWN

    if(request.vars.tipodocumento):
        if(int(request.vars.tipodocumento) == TEMPRESA_INVENTARIOMOVIMIENTOS.E_TIPODOCUMENTO.REMISION):
            _O_cat.addSubTab(
                dbTableDetail = dbc01.tempresa_invmovto_productos, 
                s_url = URL(f='movto_entrada_porcompra_remision', args=['master_'+str(request.function),request.args(1)]),
                s_idHtml = fn_createIDTabUnique(f = 'movto_entrada_porcompra_remision', s_masterIdentificator = str(request.function))  
                )
        else:
            #Ver que subtab abrirá directamente si es por factura
            pass
    else:
        pass
        
    return _D_returnVars

def _movto_entrada_pordevolucion_validation(O_form):
    '''Validación para las entradas por devolución
    '''
    
    if not O_form.vars.remision:
        O_form.errors.remision = "Es requerido"
    else:
        pass
    
    return O_form

def movto_entrada_portraspaso():
    '''Función para las entradas por traspaso de almacén
        Args:
            [arg0]: codigo de funcionalidad
            [arg1]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    '''
    
    _s_action = request.args(0, request.stv_fwk_permissions.btn_none.code)
        
    _dbTable = dbc01.tempresa_inventariomovimientos
    
    _s_empresa_plaza_sucursal_almacen_id = str(request.vars.master_empresa_plaza_sucursal_almacen_id)
    
    _s_cuenta_aplicacion_id = str(D_stvFwkCfg.cfg_cuenta_aplicacion_id)

    _s_empresa_inventariomovimiento_id = request.args(1, None)
    
    _D_result_empresa_id = TCUENTA_APLICACIONES.GET_EMPRESA_ID_CACHE(
        _s_cuenta_aplicacion_id
        )
    
    if _D_result_empresa_id.E_return != CLASS_e_RETURN.OK:
        #Si entra acá es que ocurrió un error durante la obtención del concepto_id
        _s_empresa_id = None
        stv_flashError("Empresa ID no encontrado.", _D_result_empresa_id.s_msgError)    
    elif _D_result_empresa_id.n_empresa_id:
        _s_empresa_id = _D_result_empresa_id.n_empresa_id
    else:
        #hubo bronca, no trajo error pero tampoco trajo la empresa_id
        _s_empresa_id = None
        stv_flashError("Empresa ID no encontrado.", "Empresa no definida para almacenes.")     
        
        
    _D_result_concepto_id = TEMPRESA_ALMACENESCONCEPTOS.GET_ID_CACHE(
        n_empresa_id = _s_empresa_id, 
        n_tipoconcepto = TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.ENTRADA, 
        n_clasificacion = TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.TRASPASO_ENTRADA
        )
    
    # Por default el query se pone en false para no regresar nada en las búsquedas
    _qry = False
    
    if _D_result_concepto_id.E_return != CLASS_e_RETURN.OK:
        #Si entra acá es que ocurrió un error durante la obtención del concepto_id
        _n_concepto_id = None
        stv_flashError("Entrada por traspaso", _D_result_concepto_id.s_msgError)        
    elif _D_result_concepto_id.n_concepto_id:
        _n_concepto_id = _D_result_concepto_id.n_concepto_id
        _dbTable.empresa_almacenesconcepto_id.default = _n_concepto_id
        _dbTable.empresa_almacenesconcepto_id.writable = False

        _qry = (
            (_dbTable.empresa_id == _s_empresa_id)
            &(_dbTable.empresa_almacenesconcepto_id == _n_concepto_id)
            &(_dbTable.empresa_plaza_sucursal_almacen_id == _s_empresa_plaza_sucursal_almacen_id)
            )
        
#         ###TODO: ¿en este caso cuales son los pendientes que se tienen que mostrar?
#         if _s_action in (request.stv_fwk_permissions.btn_new.code, request.stv_fwk_permissions.btn_none.code):
#             #Se agrega este query.
# #             _qry &= (None)
#             pass
#         else:
#             #Si es buscar todo el Query no aplica, ya que se puede filtrar todas las salidas
#             pass
    else:
        #hubo bronca, no trajo error pero tampoco trajo concepto_id
        _n_concepto_id = None
        stv_flashError("Entrada por traspaso", "Concepto no defnido en Almacenes-Conceptos")        

    _dbTable.empresa_id.default = _s_empresa_id
    _dbTable.empresa_plaza_sucursal_almacen_id.default = _s_empresa_plaza_sucursal_almacen_id

    #Se asigna la fecha de registro con el día y hora actual. 
    _dbTable.fecha_registro.writable = False
    _dbTable.fecha_registro.default = request.browsernow
    
    _dbTable.ordencompra.label = 'Orden de traspaso'
    
    _dbTable.empresa_embarque_almacen_id_relacionado.label = 'Almacén origen'
    
    _dbTable.empresa_embarque_almacen_id_relacionado.required = True

    _dbTable.empresa_embarque_almacen_id_relacionado.notnull = True

    _dbTable.empresa_embarque_id.required = True

    _dbTable.empresa_embarque_id.notnull = True
    
    _n_concepto_salida_traspaso_id = TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.GET_CONCEPTO_ID(
        n_empresa_id = _s_empresa_id, 
        n_tipoconcepto = TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.SALIDA,
        n_clasificacion = TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.TRASPASO_SALIDA
        )
    
    
    #Se acomoda el widget para empresa_embarque_almacen_id_relacionado para que busque por entradas
    _dbTable.empresa_embarque_almacen_id_relacionado.widget = (
        lambda field, 
        value: stv_widget_db_chosen_linked(
            field,
            value,
            1,
            'empresa_embarque_id',
            URL(a='app_almacenes', c='610entradas', f='embarque_almacenes_entradas_ver.json'),
            ) 
        )


    #Lista que tendrá todos los embarques a los que puede acceder este almacén
    _L_embarques_id = []
    if(_s_action == request.stv_fwk_permissions.btn_new.code):
        #Si _s_action es igual al botón de nuevo entonces verfica si existen embarques abiertos/en_ruta almacenes y que su primer
        #almacén con estatus != de cerrado asociado al embarque sea el almacén actual.
        
        #Se consiguen los embarques que están en_ruta y tengan asociada una salida con este almacén como destino. 
        _dbRows_salidasembarques = dbc01(
            (dbc01.tempresa_embarques.empresa_id == _s_empresa_id )
            &(dbc01.tempresa_embarques.estatus.belongs(
                TEMPRESA_EMBARQUES.ESTATUS.ABIERTO,
                TEMPRESA_EMBARQUES.ESTATUS.RUTA
                )
              )
            &(dbc01.tempresa_embarque_almacenes.empresa_embarque_id == dbc01.tempresa_embarques.id )
            &(dbc01.tempresa_embarque_almacenes.estatus.belongs(
                TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.EN_ESPERA,
                TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CARGANDO,
                TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.DESCARGANDO,
                )
              )
            ).select(
                dbc01.tempresa_embarques.id,
                dbc01.tempresa_embarque_almacenes.ALL,
                orderby = [
                    dbc01.tempresa_embarques.id, 
                    dbc01.tempresa_embarque_almacenes.orden
                    ]
                )

        if (_dbRows_salidasembarques):
            
            _s_embarque_id = None
            
            for _dbRow_salidaembarque in _dbRows_salidasembarques:
                _dbRows_embarque_almacenes = dbc01(
                    (dbc01.tempresa_embarque_almacenes.empresa_embarque_id == _dbRow_salidaembarque.tempresa_embarques.id)
                    ).select(
                        dbc01.tempresa_embarque_almacenes.ALL,
                        orderby = [
                            (
                                dbc01.tempresa_embarque_almacenes.orden
                            )
                            ]
                        ).first()
                        
                # Vamos a navegar por los resultados para detectar el proximo almacen abierto en el embarque.
                if not(_s_embarque_id) or (_s_embarque_id != _dbRow_salidaembarque.tempresa_embarques.id):
                    # Si es el primer registro del embarque...
                    _s_embarque_id = _dbRow_salidaembarque.tempresa_embarques.id
                    if _s_empresa_plaza_sucursal_almacen_id == str(_dbRow_salidaembarque.tempresa_embarque_almacenes.empresa_plaza_sucursal_almacen_id):
                        # ...si también corresponde al almacén actual, quiere decir que el almcén actual es donde se encuentra el embarque.
                        if(
                            (_dbRows_embarque_almacenes.empresa_plaza_sucursal_almacen_id == _dbRow_salidaembarque.tempresa_embarque_almacenes.empresa_plaza_sucursal_almacen_id)
                            and
                            (_dbRows_embarque_almacenes.orden == _dbRow_salidaembarque.tempresa_embarque_almacenes.orden)
                            ):
                            #Si el primer almacén del embarque coincide con este almacén y es el mismo orden entonces no se dejará generar entrada
                            pass
                        else:
                            _L_embarques_id += [_s_embarque_id]
                            
                    else:
                        pass
                else:
                    pass
            
            _dbTable.empresa_embarque_id.requires = IS_IN_DB(
                dbc01(
                    (dbc01.tempresa_embarques.id.belongs(_L_embarques_id))
                    ),
                'tempresa_embarques.id',
                dbc01.tempresa_embarques._format
                )
        else:
            pass
        
    else:
        #el _s_action es diferente de nuevo, sigue su función normal.
        pass 
        
    _L_dbFields = [
        _dbTable.id,
        _dbTable.ordencompra,
        _dbTable.empresa_embarque_id,
        _dbTable.fecha_registro,
        _dbTable.fecha_movimiento,
        ]
    
    if _n_concepto_id:
        # Si concepto esta definido, permitir hacer todo
        _L_formButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_new,
            request.stv_fwk_permissions.btn_save,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_edit,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_remove,
            request.stv_fwk_permissions.btn_find,
            request.stv_fwk_permissions.btn_findall,
            ]
    else:
        # Si concepto no esta definido, no permitir hacer nada
        _L_formButtons = [
            request.stv_fwk_permissions.btn_none,
            ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = _L_formButtons, 
        s_tabId = STV_CFG_APP_ALMACENES.ID,
        L_fieldsToShow = _L_dbFields,
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
    )

    _O_cat.addRowContentEvent('onValidation', TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.ENTRADA.PORTRASPASO)         

    _O_cat.addRowContentEvent('onAccepted', TEMPRESA_INVENTARIOMOVIMIENTOS.ACEPTADO.ENTRADA.PORTRASPASO)         

    #Se crea el botón especial 1 para poder cerrar el embarque_almacén.
    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionLabel = "Cerrar embarque",
        s_permissionIcon = "fa fa-compass",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
        confirm = Storage(
            s_title = "Favor de confirmar operación", 
            s_text = "¿Está seguro de querer cerrar este embarque?", 
            s_type = "warning", 
            s_btnconfirm = "Aceptar", 
            s_btncancel = "Cancelar", 
            )   
            ),
        L_options = []
        )

    #Se utiliza el _b_skipAction a este nivel para hacer un bypass a Eliminar si llega a dar error.
    _b_skipAction = False
    #Se utiliza el overrideView a este nivel porque como se va a eliminar el registro tiene que hacerse antes del _O_cat.process()
    _D_overrideView = None
    
    if(_s_action == request.stv_fwk_permissions.btn_remove.code):
        
        _dbRow_movimiento_entrada_traspaso = dbc01.tempresa_inventariomovimientos(_s_empresa_inventariomovimiento_id)
        
        #Sacará los movimientos que tengan para este almacén y que sean diferentes al ID actual. 
        _dbRows_movimientos = dbc01(
            (dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id == _dbRow_movimiento_entrada_traspaso.empresa_embarque_almacen_id)
            &(dbc01.tempresa_inventariomovimientos.id != _dbRow_movimiento_entrada_traspaso.id)
            ).select(
                dbc01.tempresa_inventariomovimientos.id
                )
        
        #Si no existe ningún movimiento con este embarque_almacén en todo entonces se procede a cambiar el estatus del almacén a "en espera"
        if not _dbRows_movimientos:
            
            _dbRow_empresa_embarque_almacen = dbc01.tempresa_embarque_almacenes(_dbRow_movimiento_entrada_traspaso.empresa_embarque_almacen_id)
                
            if(_dbRow_empresa_embarque_almacen.estatus != TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CERRADO):
            
                dbc01(
                    (dbc01.tempresa_embarque_almacenes.id == _dbRow_movimiento_entrada_traspaso.empresa_embarque_almacen_id)
                    ).update(
                        estatus = TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.EN_ESPERA
                        )
                
                #Su almacén relacionado se va a "Ruta".
                dbc01(
                    (dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id == _dbRow_movimiento_entrada_traspaso.empresa_embarque_almacen_id_relacionado)
                    &(dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id_relacionado == _dbRow_movimiento_entrada_traspaso.empresa_embarque_almacen_id)
                    ).update(
                        estatus_movimiento = TEMPRESA_INVENTARIOMOVIMIENTOS.ESTATUS_MOVIMIENTO.RUTA
                        )
                    
                #Si no hubo más movimientos a parte del que se quiere eliminar se elimina pero también el almacen_embarque cambia su estatus a "en_espera"
                stv_flashSuccess("Movimiento traspaso", "Se eliminó correctamente el movimiento")
                 
                _D_overrideView = request.stv_fwk_permissions.view_refresh
                
            else:
                #Si el embarque tiene un estatus == a cerrado entonces no podrá cambiarse su estatus a "En_espera"
                stv_flashError("Movimiento traspaso", "El movimiento no se pudo eliminar ya que el almacén al cual pertenece ya cuenta con estatus 'Cerrado'") 
                
                #Se usa el _b_skipAction para hacerle bypass al eliminar
                _b_skipAction = True                
                
                _D_overrideView = request.stv_fwk_permissions.view_refresh

        else:
            #Si sí hay se despliega un mensaje diciendo que se elimina este movimiento pero el embarque no cambia el estatus
            
            #Su almacén relacionado se va a "Ruta".
            dbc01(
                (dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id == _dbRow_movimiento_entrada_traspaso.empresa_embarque_almacen_id_relacionado)
                &(dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id_relacionado == _dbRow_movimiento_entrada_traspaso.empresa_embarque_almacen_id)
                ).update(
                    estatus_movimiento = TEMPRESA_INVENTARIOMOVIMIENTOS.ESTATUS_MOVIMIENTO.RUTA
                    )
                
            stv_flashSuccess("Movimiento traspaso", "Se eliminó correctamente movimiento") 
            
            _D_overrideView = request.stv_fwk_permissions.view_refresh
            
    else:
        pass

    if(_D_overrideView):
        _D_returnVars = _O_cat.process(D_overrideView = _D_overrideView, b_skipAction = _b_skipAction)
    else:
        _D_returnVars = _O_cat.process()
        
    if(_s_action == request.stv_fwk_permissions.btn_new.code):

        if not _L_embarques_id:
            #Si no regresó ningún embarque mandará un error y regresará la vista.
            stv_flashError("Embarques.", "No se encontraron embarques con salidas para este almacén.")
            #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
            from gluon.storage import List
            request.args = List([request.stv_fwk_permissions.btn_none.code,])
            _D_returnVars = movto_entrada_portraspaso()
        else:
            #No pasa nada encontró embarques a mostrar
            pass
    else:
        #el _s_action es diferente de nuevo, sigue su función normal.
        pass 


    if (_s_action == request.stv_fwk_permissions.btn_save.code) and (_D_returnVars.rowContent_form.accepted):

        ###TODO: Hacer el cambio de estatus solo cuando venga el guardado desde "nuevo". 
       
        #Se hace una consulta al registro que se acaba de guardar
        _dbRow_inventariomovimiento = dbc01.tempresa_inventariomovimientos(_D_returnVars.rowContent_form.vars.id)
        
        #Si se grabó y se aceptó se cambia el estatus del embarque_almacen a "cargando"
        dbc01(
            (dbc01.tempresa_embarque_almacenes.id == _dbRow_inventariomovimiento.empresa_embarque_almacen_id)
            ).update(
                estatus = TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.DESCARGANDO
                )
            
            
        #Su almacén relacionado se va a "Entregado".
        dbc01(
            (dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id == _dbRow_inventariomovimiento.empresa_embarque_almacen_id_relacionado)
            &(dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id_relacionado == _dbRow_inventariomovimiento.empresa_embarque_almacen_id)
            ).update(
                estatus_movimiento = TEMPRESA_INVENTARIOMOVIMIENTOS.ESTATUS_MOVIMIENTO.ENTREGADO
                )
        
        #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
        from gluon.storage import List
        request.args = List([request.stv_fwk_permissions.btn_view.code, _D_returnVars.rowContent_form.vars.id])
        _D_returnVars = movto_entrada_portraspaso()
        
    else:
        _D_returnVars.update(
            dict(
                msg = 'Bienvenidos',
                dbFieldAlmacenes = STV_LIB_APP_ALMACENES.GET_DBFIELD_ALMACENES(
                    n_default = _s_empresa_plaza_sucursal_almacen_id or 0,
                    b_writable = False
                    ),
                L_breadcrub = [
                    STV_ALMACENES.BREADCRUB.BC_INDEX,
                    STV_ALMACENES.BREADCRUB.MOVTO.ENTRADA.BC_PORTRASPASO
                    ]
                )
            )
        
    if(_s_action in (request.stv_fwk_permissions.special_permisson_01.code,)):
    #Si se le dio clic a "cerrar almacen embarque" hará la lógica para pasar el estatus de cargando a cerrado.
        _s_entrada_traspaso_id = request.args(1, None)
        
        _dbRow_entrada = _dbTable(_s_entrada_traspaso_id)
        
        #Query para verificar que se tengan movimientos a nivel detalle de este movimiento de almacén.
        _dbRow_entrada_movtos = dbc01(
            (dbc01.tempresa_invmovto_productos.empresa_inventariomovimiento_id == _s_entrada_traspaso_id)
            ).select(
                dbc01.tempresa_invmovto_productos.id
                )
        
        if(_dbRow_entrada_movtos):
            #Si tiene movimientos entonces puede hacer la lógica para cambiar el estatus.
            
            _dbRow_embarque_almacenes = dbc01(
                (dbc01.tempresa_inventariomovimientos.id == _s_entrada_traspaso_id)
                &(dbc01.tempresa_embarque_almacenes.empresa_embarque_id == dbc01.tempresa_inventariomovimientos.empresa_embarque_id)
                &(dbc01.tempresa_embarque_almacenes.empresa_plaza_sucursal_almacen_id == _s_empresa_plaza_sucursal_almacen_id)
                ).select(
                    dbc01.tempresa_embarque_almacenes.ALL
                    ).last()
            
            if(_dbRow_embarque_almacenes):
                if(_dbRow_embarque_almacenes.estatus in (TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CARGANDO,TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.DESCARGANDO, TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.EN_ESPERA)):
                    
                    _dbRow_embarques = dbc01.tempresa_embarques(_dbRow_embarque_almacenes.empresa_embarque_id)
                    
                    #Si el embarque aún está abierto se pasará a en ruta.
                    if(_dbRow_embarques.estatus == TEMPRESA_EMBARQUES.ESTATUS.ABIERTO):
                        dbc01(
                            (dbc01.tempresa_embarques.id == _dbRow_embarques.id)
                            ).update(
                                estatus = TEMPRESA_EMBARQUES.ESTATUS.RUTA
                                )
                    else:
                        pass
                    
                    _dbRows_movimientos_salidas = dbc01(
                        (dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id_relacionado == _dbRow_entrada.empresa_embarque_almacen_id)
                        &(dbc01.tempresa_inventariomovimientos.empresa_almacenesconcepto_id == _n_concepto_salida_traspaso_id)
                        &(dbc01.tempresa_inventariomovimientos.estatus_movimiento == TEMPRESA_INVENTARIOMOVIMIENTOS.ESTATUS_MOVIMIENTO.RUTA)
                        ).select(
                            dbc01.tempresa_inventariomovimientos.id,
                            orderby = [dbc01.tempresa_inventariomovimientos.id]
                            )
                        
                    if not _dbRows_movimientos_salidas:
                        #Si no hay movimientos de salidas para este embarque_almacen_id que estén en ruta entonces procede a hacerse toda la lógica para cerrar
                        
                        #Se obtine el último almacén por orden de los almacenes por embarque
                        _dbRows_embarques_almacenes = dbc01(
                            (dbc01.tempresa_embarque_almacenes.empresa_embarque_id == _dbRow_embarque_almacenes.empresa_embarque_id)
                            &(dbc01.tempresa_embarque_almacenes.estatus != TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CERRADO)
                            ).select(
                                dbc01.tempresa_embarque_almacenes.ALL,
                                orderby = [
                                    dbc01.tempresa_embarque_almacenes.orden
                                    ]
                                )
                        
                        if len(_dbRows_embarques_almacenes) == 1:
                            #Si solo queda un solo almacén pendiente entonces entrará aquí
                            _dbRow = _dbRows_embarques_almacenes.last()
                            if(_dbRow.empresa_plaza_sucursal_almacen_id == int(_s_empresa_plaza_sucursal_almacen_id)):
                                #Si el último almacén pendiente es este almacén entonces cambiará el estatus del embarque a Cerrado
                                dbc01(
                                    (dbc01.tempresa_embarques.id == _dbRow_embarques.id)
                                    ).update(
                                        estatus = TEMPRESA_EMBARQUES.ESTATUS.CERRADO
                                        )
                                stv_flashSuccess("Estatus embarque", "Este fue el ultimo almacén pendiente para este embarque. Se cerró embarque correctamente.") 
                            else:
                                pass
                        else:
                            #Si la longitud de los almacenes pendientes no es == 1 entonces no cerrará el embarque
                            pass
                        
                        #Si el embarque_almacen tiene un estatus de "Cargando" se pasa a cerrado. 
                        dbc01(
                            (dbc01.tempresa_embarque_almacenes.id == _dbRow_embarque_almacenes.id)
                            ).update(
                                estatus = TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CERRADO
                                )
                          
                        stv_flashSuccess("Estatus embarque almacen", "Se cerró el traspaso de almacén") 
                        #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
                        from gluon.storage import List
                        request.args = List([request.stv_fwk_permissions.btn_findall.code,])
                        _D_returnVars = movto_entrada_portraspaso()
                    else:
                        stv_flashError("Estatus embarque almacen", "Este almacén no puede cerrarse porque tiene uno o más movimientos pendientes de entregar.")
                        #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
                        from gluon.storage import List
                        request.args = List([request.stv_fwk_permissions.btn_findall.code,])
                        _D_returnVars = movto_entrada_portraspaso()
                        pass
                else:
                    stv_flashError("Estatus embarque almacen", "Este almacén no está en estatus de 'Cargando' en almacenes o ya fue cerrado.")
                    #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
                    from gluon.storage import List
                    request.args = List([request.stv_fwk_permissions.btn_findall.code,])
                    _D_returnVars = movto_entrada_portraspaso()
                    
            else:
                pass
        else:
            #En caso de no tener movimientos se mostrará el error.
            stv_flashError("Estatus embarque almacen", "El detalle de este movimiento no cuenta con productos, no se puede cambiar el estatus.")
            #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
            from gluon.storage import List
            request.args = List([request.stv_fwk_permissions.btn_findall.code,])
            _D_returnVars = movto_entrada_portraspaso()
            
    else:
        pass        

    if _n_concepto_id:
        # Si concepto esta definido, permitir el detalle
        _O_cat.addSubTab(
            dbTableDetail = dbc01.tempresa_invmovto_productos, 
            s_url = URL(f='movto_entrada_portraspaso_resumen', args=['master_'+str(request.function),request.args(1)]),
            s_idHtml = fn_createIDTabUnique(f = 'movto_entrada_portraspaso_resumen', s_masterIdentificator = str(request.function))  
            )
    else:
        # de lo contrario, no mostrar el detalle
        pass
    
    return _D_returnVars

def movto_entrada_portraspaso_resumen():
    '''Función para los resumenes de entrada por traspaso 
    
        Args:
            arg0: master_[nombre función maestro]
            arg1: empresa_inventariomovimiento_id
            [arg2]: codigo de funcionalidad
            [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    '''
    
    _dbTable = dbc01.tempresa_invmovto_productos
    
    _s_empresa_inventariomovimiento_id = request.args(1, None)
    
    _s_action = request.args(2, request.stv_fwk_permissions.btn_none.code)

    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.empresa_producto_id,
        _dbTable.cantidad_entrada,
        _dbTable.observaciones,
        _dbTable.tipo_inconsistencia
        ]

    _L_formButtons = [
        request.stv_fwk_permissions.btn_none,
        request.stv_fwk_permissions.btn_save,
        request.stv_fwk_permissions.btn_cancel,
        request.stv_fwk_permissions.btn_edit,
        request.stv_fwk_permissions.btn_view,
        request.stv_fwk_permissions.btn_remove,
        request.stv_fwk_permissions.btn_find,
        request.stv_fwk_permissions.btn_findall,
        ]    


    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(),
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_inventariomovimientos,
        xFieldLinkMaster = [
            _dbTable.empresa_inventariomovimiento_id
            ],
        L_visibleButtons = _L_formButtons, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
        )
   
    _D_returnVars = _O_cat.process()

    if ((_s_action == request.stv_fwk_permissions.btn_save.code) and (_D_returnVars.rowContent_form.accepted)):
        
        _dbRow_maestro = dbc01.tempresa_inventariomovimientos(_s_empresa_inventariomovimiento_id )
        
        _D_result = TEMPRESA_INVMOVTO_PRODUCTOS.VALIDAR_ROW_ENTRADA_PORTRASPASO(
            _D_returnVars.rowContent_form.vars.id,
            _dbRow_maestro.empresa_id, 
            _dbRow_maestro)
        
        if _D_result.E_return != CLASS_e_RETURN.OK:
            stv_flashWarning("Inconsistencias en producto", _D_result.s_msgError)  
        else:
            pass

    else:
        #Si es cualquier opción en vez de salvar no hace nada
        pass

    return _D_returnVars

def movto_entrada_otra():
    '''Función para los movimientos por otros tipo de entradas

        Args:
            [arg0]: codigo de funcionalidad
            [arg1]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    '''
    
    _s_action = request.args(0, request.stv_fwk_permissions.btn_none.code)
        
    _dbTable = dbc01.tempresa_inventariomovimientos
    
    _s_empresa_plaza_sucursal_almacen_id = request.vars.master_empresa_plaza_sucursal_almacen_id
    
    _s_cuenta_aplicacion_id = D_stvFwkCfg.cfg_cuenta_aplicacion_id

    _s_empresa_inventariomovimiento_id = request.args(1, None)
    
    _D_result_empresa_id = TCUENTA_APLICACIONES.GET_EMPRESA_ID_CACHE(
        _s_cuenta_aplicacion_id
        )
        
    if _D_result_empresa_id.E_return != CLASS_e_RETURN.OK:
        #Si entra acá es que ocurrió un error durante la obtención del concepto_id
        _s_empresa_id = None
        stv_flashError("Empresa ID no encontrado.", _D_result_empresa_id.s_msgError)    
    elif _D_result_empresa_id.n_empresa_id:
        _s_empresa_id = _D_result_empresa_id.n_empresa_id
    else:
        #hubo bronca, no trajo error pero tampoco trajo la empresa_id
        _s_empresa_id = None
        stv_flashError("Empresa ID no encontrado.", "Empresa no definida para almacenes.")     
        
        
    _D_result_concepto_id = TEMPRESA_ALMACENESCONCEPTOS.GET_ID_CACHE(
        n_empresa_id = _s_empresa_id, 
        n_tipoconcepto = TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.ENTRADA, 
        n_clasificacion = TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.OTRAS_ENTRADAS
        )
    
    # Por default el query se pone en false para no regresar nada en las búsquedas
    _qry = False
    
    if _D_result_concepto_id.E_return != CLASS_e_RETURN.OK:
        #Si entra acá es que ocurrió un error durante la obtención del concepto_id
        _n_concepto_id = None
        stv_flashError("Otras entradas", _D_result_concepto_id.s_msgError)        
    elif _D_result_concepto_id.n_concepto_id:
        _n_concepto_id = _D_result_concepto_id.n_concepto_id
        _dbTable.empresa_almacenesconcepto_id.default = _n_concepto_id
        _dbTable.empresa_almacenesconcepto_id.writable = False

        _qry = (
            (_dbTable.empresa_id == _s_empresa_id)
            &(_dbTable.empresa_almacenesconcepto_id == _n_concepto_id)
            &(_dbTable.empresa_plaza_sucursal_almacen_id == _s_empresa_plaza_sucursal_almacen_id)
            )
        
        ###TODO: Mostrar los movimientos de salidas por otros. 
#         if _s_action in (request.stv_fwk_permissions.btn_new.code, request.stv_fwk_permissions.btn_none.code):
#             #Se agrega este query.
# #             _qry &= (None)
#             pass
#         else:
#             #Si es buscar todo el Query no aplica, ya que se puede filtrar todas las salidas
#             pass
    else:
        #hubo bronca, no trajo error pero tampoco trajo concepto_id
        _n_concepto_id = None
        stv_flashError("Otras entradas", "Concepto no definido en Almacenes-Conceptos")  
            
            
    _dbTable.empresa_id.default = _s_empresa_id
    _dbTable.empresa_plaza_sucursal_almacen_id.default = _s_empresa_plaza_sucursal_almacen_id

    #Se asigna la fecha de registro con el día y hora actual. 
    _dbTable.fecha_registro.writable = False
    _dbTable.fecha_registro.default = request.browsernow  
    
    _L_dbFields = [
        _dbTable.id,
        _dbTable.fecha_movimiento,
        _dbTable.remision,
        _dbTable.descripcion,
        _dbTable.observaciones, 
        ]
    
    _L_formButtons = [
        request.stv_fwk_permissions.btn_none,
        request.stv_fwk_permissions.btn_new,
        request.stv_fwk_permissions.btn_save,
        request.stv_fwk_permissions.btn_cancel,
        request.stv_fwk_permissions.btn_edit,
        request.stv_fwk_permissions.btn_view,
        request.stv_fwk_permissions.btn_find,
        request.stv_fwk_permissions.btn_findall,
        ]
        
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = _L_formButtons, 
        s_tabId = STV_CFG_APP_ALMACENES.ID,
        L_fieldsToShow = _L_dbFields, 
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
    )
    
    _O_cat.addRowContentEvent('onValidation', TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.ENTRADA.OTRA)
    
    _D_returnVars = _O_cat.process()

    if (_s_action == request.stv_fwk_permissions.btn_save.code) and (_D_returnVars.rowContent_form.accepted):
        #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
        from gluon.storage import List
        request.args = List([request.stv_fwk_permissions.btn_view.code, _D_returnVars.rowContent_form.vars.id])
        _D_returnVars = movto_entrada_otra()
         
    else:
        _D_returnVars.update(
            dict(
                msg = 'Bienvenidos',
                dbFieldAlmacenes = STV_LIB_APP_ALMACENES.GET_DBFIELD_ALMACENES(
                    n_default = _s_empresa_plaza_sucursal_almacen_id or 0,
                    b_writable = False
                    ),
                L_breadcrub = [
                    STV_ALMACENES.BREADCRUB.BC_INDEX,
                    STV_ALMACENES.BREADCRUB.MOVTO.ENTRADA.BC_OTRA
                    ]
                )
            )
        
    if _n_concepto_id:
        # Si concepto esta definido, permitir el detalle

        _O_cat.addSubTab(
            dbTableDetail = dbc01.tempresa_invmovto_productos, 
            s_url = URL(f='movto_entrada_otra_resumen', args=['master_'+str(request.function),request.args(1)]),
            s_idHtml = fn_createIDTabUnique(f = 'movto_entrada_otra_resumen', s_masterIdentificator = str(request.function))  
            )
    else:
        # de lo contrario, no mostrar el detalle
        pass
        
    return _D_returnVars

def _movto_entrada_otra_validation(O_form):
    '''Validación para los movimientos por otros tipo de entrada
    '''
    
    if not O_form.vars.remision:
        O_form.errors.remision = "La remisión es requerida."
    else:
        pass
    
    if not O_form.vars.descripcion:
        O_form.errors.descripcion = "La descripción es requerida."
    else:
        pass
    
    return O_form

def movto_entrada_otra_resumen():
    '''Función para los resumenes de movimientos por otros tipo de entradas
    
        Args:
            arg0: master_[nombre función maestro]
            arg1: empresa_inventariomovimiento_id
            [arg2]: codigo de funcionalidad
            [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    '''
    
    _dbTable = dbc01.tempresa_invmovto_productos

    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.empresa_producto_id,
        _dbTable.cantidad_entrada,
        ]
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(),
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_inventariomovimientos,
        xFieldLinkMaster = [
            _dbTable.empresa_inventariomovimiento_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
        )

    _O_cat.addRowContentEvent('onValidation', _movto_entrada_otra_resumen_validation)
   
    _D_returnVars = _O_cat.process()

    return _D_returnVars

def _movto_entrada_otra_resumen_validation(O_form):
    '''Validación para los resumenes de movimientos por otros tipo de entradas
    '''

    _n_cantidad = int(O_form.vars.cantidad_entrada or 0)
    
    _s_empresa_inventariomovimiento_id = request.args(1, None)
        
    if (_n_cantidad <= 0):
        O_form.errors.cantidad_entrada = "La cantidad que entrada no puede ser menor o igual a cero"
    else:
        pass
    
    _dbRow = dbc01(
        (dbc01.tempresa_invmovto_productos.empresa_inventariomovimiento_id == _s_empresa_inventariomovimiento_id)
        &(dbc01.tempresa_invmovto_productos.empresa_producto_id == O_form.vars.empresa_producto_id)
        &(dbc01.tempresa_invmovto_productos.empresa_producto_id != O_form.vars.record_id)
        ).select(
            dbc01.tempresa_invmovto_productos.id
            )
    if(_dbRow):
        O_form.errors.empresa_producto_id = "El producto no puede repetirse."
    else:
        pass

    return O_form

def movto_entrada_inventarioinicial():
    '''Función para las entrada por inventario inicial
        Args:
            [arg0]: codigo de funcionalidad
            [arg1]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    '''
    _s_action = request.args(0, None)
    
    _dbTable = dbc01.tempresa_inventariomovimientos
    
    _s_empresa_plaza_sucursal_almacen_id = request.vars.master_empresa_plaza_sucursal_almacen_id
    
    _s_cuenta_aplicacion_id = D_stvFwkCfg.cfg_cuenta_aplicacion_id
    
    _D_result_empresa_id = TCUENTA_APLICACIONES.GET_EMPRESA_ID_CACHE(
        _s_cuenta_aplicacion_id
        )
    
    if _D_result_empresa_id.E_return != CLASS_e_RETURN.OK:
        #Si entra acá es que ocurrió un error durante la obtención del concepto_id
        _s_empresa_id = None
        stv_flashError("Empresa ID no encontrado.", _D_result_empresa_id.s_msgError)        
    elif _D_result_empresa_id.n_empresa_id:
        _s_empresa_id = _D_result_empresa_id.n_empresa_id
    else:
        #hubo bronca, no trajo error pero tampoco trajo la empresa_id
        _s_empresa_id = None
        stv_flashError("Empresa ID no encontrado.", "Empresa no definida para almacenes.")  
    
    _D_result = TEMPRESA_ALMACENESCONCEPTOS.GET_ID_CACHE(
        n_empresa_id = _s_empresa_id, 
        n_tipoconcepto = TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.ENTRADA, 
        n_clasificacion = TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.INVENTARIO_INICIAL
        )
    
    if _D_result.s_msgError:
        #Si entra acá es que ocurrió un error durante la obtención del concepto_id
        stv_flashError("Entrada por inventario inicial", _D_result_concepto_id.s_msgError)
        _s_concepto_inventarioinicial_id = None 
    elif _D_result.n_concepto_id:
        _s_concepto_inventarioinicial_id = _D_result.n_concepto_id
    else:
        _s_concepto_inventarioinicial_id = None
        pass   
    
    _dbTable.empresa_id.default = _s_empresa_id 
    
    # Obtener los movimientos de inventario inicial en el almacen
    _dbRows = dbc01(
        (_dbTable.empresa_almacenesconcepto_id == _s_concepto_inventarioinicial_id)
        & (_dbTable.empresa_plaza_sucursal_almacen_id == _s_empresa_plaza_sucursal_almacen_id)
        ).select(
            _dbTable.id,
            orderby = _dbTable.id
            )
        
    _s_movimiento_id = None  # Es el ID del movimiento de inventario inicial maestro
    
    # Se obtienen los productos que ya tienen movimientos con poliza generada
    _dbRows_prod_con_poliza = dbc01(
        (dbc01.tempresa_invmovto_producto_costeo.empresa_plaza_sucursal_almacen_id == _s_empresa_plaza_sucursal_almacen_id)
        & (dbc01.tempresa_invmovto_producto_costeo.estatus.belongs(
                TEMPRESA_INVMOVTO_PRODUCTO_COSTEO.ESTATUS.CON_POLIZA, 
                TEMPRESA_INVMOVTO_PRODUCTO_COSTEO.ESTATUS.CERRADO
                )
            )
        ).select(
            dbc01.tempresa_invmovto_producto_costeo.empresa_producto_id,
            orderby = dbc01.tempresa_invmovto_producto_costeo.empresa_producto_id,
            distinct = True
            )
    
    if (len(_dbRows) == 0) and (_s_concepto_inventarioinicial_id):
        # Crear registro inicial

        if _dbRows_prod_con_poliza:
            # Si existen no se puede generar entrada por inventario inicial
            stv_flashError("Movimiento de inventario inicial", "No se puede generar movimiento de inventario inicial, ya que existen movimientos con póliza generada en el almacen")
        else:
            # Se obtiene el primer movimiento en el almacen y se fija como fecha de registro una fecha anterior
            _dbRows_primerMovto = dbc01(
                (_dbTable.empresa_plaza_sucursal_almacen_id == _s_empresa_plaza_sucursal_almacen_id)
                ).select(
                    _dbTable.fecha_movimiento,
                    orderby = ~_dbTable.fecha_movimiento
                    )
                
            if _dbRows_primerMovto:
                # Si encuentra movimientos, toma la fecha con un día menos a la del primer movimiento
                _dt_fecha_movimiento = _dbRows_primerMovto.first().fecha_movimiento + datetime.timedelta(days=-1)
            else:
                # Si no hay movimientos, toma la fecha actual
                _dt_fecha_movimiento = request.browsernow
            
            _s_movimiento_id = dbc01.tempresa_inventariomovimientos.insert(
                empresa_id = _s_empresa_id,
                empresa_almacenesconcepto_id = _s_concepto_inventarioinicial_id,
                empresa_plaza_sucursal_almacen_id = _s_empresa_plaza_sucursal_almacen_id,
                fecha_movimiento = _dt_fecha_movimiento
                )
    elif _s_concepto_inventarioinicial_id:
        if len(_dbRows) > 1:
            # Crear warning y consultar con administrador
            stv_flashWarning("Movimiento de inventario inicial", "Se encontraron más de un registro de inventario inicial para el almacen, verificar con administrador")
        else:
            pass
        _s_movimiento_id = _dbRows.last().id
    else:
        _s_movimiento_id = None
    
    if _dbRows_prod_con_poliza:
        # Si tiene algún producto con poliza generada, no se puede editar la fecha de movimiento
        _dbTable.fecha_movimiento.writable = False
        #Si tiene algún producto con póliza generada no se podrá editar el registro maestro.
        _L_formButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_save,
            request.stv_fwk_permissions.btn_view,
            ]
    else:
        # Si no tiene productos con poliza generada, es posible editar la fecha de movimiento
        _dbTable.fecha_movimiento.writable = True
        #Si no tiene algún producto con póliza generada se podrá editar el registro maestro sin problema.   
        _L_formButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_save,
            request.stv_fwk_permissions.btn_edit,
            request.stv_fwk_permissions.btn_view,
            ]
        
    if not(_s_action in (request.stv_fwk_permissions.btn_edit.code, request.stv_fwk_permissions.btn_save.code)):
        # Cualquier action diferente a editar será ver
        from gluon.storage import List
        request.args = List([request.stv_fwk_permissions.btn_view.code, _s_movimiento_id])
    else:
        # Si esta editando procede
        pass
        
    # No es necesario definir dbFields ya que solo se maneja en las búsquedas, y en esta rutina solo se permite ver y editar
    _L_dbFields = [
        _dbTable.id,
        _dbTable.empresa_proveedor_id,
        _dbTable.tipodocumento,
        _dbTable.remision,
        ]
    
    # Se remueve de los posibles botones en la vista view el boton de cancel
    request.stv_fwk_permissions.view_viewRegister.L_permissionsDefault = [
        request.stv_fwk_permissions.btn_edit,
        request.stv_fwk_permissions.btn_print,
        ] 
    
        
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(),
        dbTable = _dbTable,
        L_visibleButtons = _L_formButtons, 
        s_tabId = STV_CFG_APP_ALMACENES.ID,
        L_fieldsToShow = _L_dbFields, 
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
    )
    
    _O_cat.addRowContentEvent('onValidation', TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.ENTRADA.INVENTARIOINICIAL)

    _D_returnVars = _O_cat.process()
    
    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_invmovto_productos, 
        s_url = URL(f='movto_entrada_inventarioinicial_movimientos', args=['master_'+str(request.function),request.args(1)]),
        s_idHtml = fn_createIDTabUnique(f = 'movto_entrada_inventarioinicial_movimientos', s_masterIdentificator = str(request.function))  
        )
 
    if (_s_action == request.stv_fwk_permissions.btn_save.code) and (_D_returnVars.rowContent_form.accepted):
   
        #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
        from gluon.storage import List
        request.args = List([request.stv_fwk_permissions.btn_view.code, _D_returnVars.rowContent_form.vars.id])
        _D_returnVars = movto_entrada_inventarioinicial()
        
    else:
        _D_returnVars.update(
            dict(
                msg = 'Bienvenidos',
                dbFieldAlmacenes = STV_LIB_APP_ALMACENES.GET_DBFIELD_ALMACENES(
                    n_default = request.vars.master_empresa_plaza_sucursal_almacen_id or 0,
                    b_writable = False
                    ),
                L_breadcrub = [
                    STV_ALMACENES.BREADCRUB.BC_INDEX,
                    STV_ALMACENES.BREADCRUB.MOVTO.ENTRADA.BC_INVENTARIOINICIAL
                    ]
                )
            )
        
        if not _s_action:
            #Se no existe s_action, se crea la vista
            _D_returnVars.D_useView.L_actionsCreateView += [request.stv_fwk_permissions.btn_edit.code, request.stv_fwk_permissions.btn_view.code]
        else:
            pass
      
    return _D_returnVars

def movto_entrada_inventarioinicial_movimientos():
    '''Función para las entrada por inventario inicial
        
        Args:
            arg0: maestro
            arg1: id del maestro
            [arg2]: codigo de funcionalidad
            [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    '''

    _s_action = request.args(2, None)
        
    _dbTable = dbc01.tempresa_invmovto_productos
    
    _s_empresa_inventariomovimiento_id = request.args(1, None)
        
    _s_empresa_plaza_sucursal_almacen_id = request.vars.master_empresa_plaza_sucursal_almacen_id
    
    #Query para ver si ya existen movimientos con póliza dentro de este almacén. Se ve a nivel inventariomovimiento_producto.
    _dbRows_prod_con_poliza = dbc01(
        (dbc01.tempresa_invmovto_producto_costeo.empresa_plaza_sucursal_almacen_id == _s_empresa_plaza_sucursal_almacen_id)
        & (dbc01.tempresa_invmovto_producto_costeo.estatus.belongs(
                TEMPRESA_INVMOVTO_PRODUCTO_COSTEO.ESTATUS.CON_POLIZA, 
                TEMPRESA_INVMOVTO_PRODUCTO_COSTEO.ESTATUS.CERRADO
                )
            )
        ).select(
            dbc01.tempresa_invmovto_producto_costeo.empresa_producto_id,
            orderby = dbc01.tempresa_invmovto_producto_costeo.empresa_producto_id
            )
    
    if _dbRows_prod_con_poliza:
        # Si tiene algún producto con poliza generada, no se pueden agregar o modificar registros
        _L_formButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_print,
            request.stv_fwk_permissions.btn_find,
            request.stv_fwk_permissions.btn_findall,
            request.stv_fwk_permissions.btn_signature,
            ]
    else:
        # Si no tiene productos, es posible completa funcionalidad
        _L_formButtons = request.stv_fwk_permissions.L_formButtonsAll
        
    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.empresa_producto_id,
        _dbTable.producto_externo,
        _dbTable.cantidad_entrada,
        _dbTable.preciounitario,
        ]
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_inventariomovimientos,
        xFieldLinkMaster = [
            _dbTable.empresa_inventariomovimiento_id
            ],
        L_visibleButtons = _L_formButtons, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
        )
    
    _O_cat.addRowContentEvent('onValidation', TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.ENTRADA.INVENTARIOINICIAL_RESUMEN)
   
    _D_returnVars = _O_cat.process()
        
    return _D_returnVars


def embarque_almacenes_entradas_ver():
    ''' Función para las salidas por traspaso de almacén
        Args:
            arg0: embarque id
    
    '''
    
    _dbTable = dbc01.tempresa_embarque_almacenes
    
    _s_almacen_id = request.vars.master_empresa_plaza_sucursal_almacen_id
    
    _LD_data = []
    
    if (len(request.args) > 0) :
        
        _s_embarque_id = request.args(0, None)
        
        _dbRows_embarque_almacenes_abiertos = dbc01(
            (dbc01.tempresa_embarque_almacenes.empresa_embarque_id == _s_embarque_id)
            & (
                dbc01.tempresa_embarque_almacenes.estatus.belongs(
                    TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.EN_ESPERA,
                    TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.CARGANDO,
                    TEMPRESA_EMBARQUE_ALMACENES.ESTATUS.DESCARGANDO,
                    )
                )
            ).select(
                dbc01.tempresa_embarque_almacenes.ALL,
                orderby = (
                    dbc01.tempresa_embarque_almacenes.orden
                    )
                )
            
        if len(_dbRows_embarque_almacenes_abiertos) == 0:
            pass
        else:
            _dbRow_embarque_almacenes_abierto_primero = _dbRows_embarque_almacenes_abiertos.first()
            if _dbRow_embarque_almacenes_abierto_primero.empresa_plaza_sucursal_almacen_id != int(_s_almacen_id):
                pass
            else:
                _s_empresa_embarque_almacen_actual = _dbRow_embarque_almacenes_abierto_primero.id
        
                _dbRow_embarque  = dbc01.tempresa_embarques(_s_embarque_id)
                
                _n_concepto_salida_id = TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.GET_CONCEPTO_ID(
                    n_empresa_id = _dbRow_embarque.empresa_id, 
                    n_tipoconcepto = TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.SALIDA,
                    n_clasificacion = TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.TRASPASO_SALIDA
                    )
                
                _n_concepto_entrada_id = TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.GET_CONCEPTO_ID(
                    n_empresa_id = _dbRow_embarque.empresa_id, 
                    n_tipoconcepto = TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.ENTRADA,
                    n_clasificacion = TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.TRASPASO_ENTRADA
                    )
                
                _rows_result = dbc01(
                    (dbc01.tempresa_inventariomovimientos.empresa_embarque_id == _s_embarque_id)
                    &(dbc01.tempresa_inventariomovimientos.empresa_almacenesconcepto_id == _n_concepto_salida_id)
                    &(dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id_relacionado == _s_empresa_embarque_almacen_actual)
                    &(_dbTable.id == dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id)
                    ).select(
                        _dbTable.ALL,
                        orderby = _dbTable.orden,
                        )
                    
        if(_rows_result):     
            for _row in _rows_result:
                
                #Consulta para saber si el almacen ya fue utilizado en alguna entrada.
                _dbRow = dbc01(
                    (dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id_relacionado == _row.id)
                    &(dbc01.tempresa_inventariomovimientos.empresa_almacenesconcepto_id == _n_concepto_entrada_id)
                    &(dbc01.tempresa_inventariomovimientos.empresa_embarque_almacen_id == _s_empresa_embarque_almacen_actual)
                    ).select(
                        dbc01.tempresa_inventariomovimientos.id
                        )
                
                if not (_dbRow):
                    #Si el almacén no ha sido utilizado en nignuna entrada entonces es posible asociarse aquí.
                    _LD_data.append({ 
                        'id':_row.id, 'optionname':(_dbTable._format(_row))})
                else:
                    #Si sí fue utilizado en alguna entrada entonces no se agrega a la lista de almacenes disponibles.
                    pass
                                
            return (response.json({'data': _LD_data}))
        else:
            return (response.json({'data': [], 'error': 'Argumentos inválidos'})) 
            pass
        
    else:
        return (response.json({'data': [], 'error': 'Argumentos inválidos'}))  