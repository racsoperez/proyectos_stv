# -*- coding: utf-8 -*-

class STV_LIB_APP_ALMACENES():

    @classmethod
    def WRITE_HTML(cls, str):
        return str
    
    @classmethod
    def GET_DBFIELD_ALMACENES(
            cls,
            n_default = 0,
            b_writable = True,
            ):

        _s_cuenta_aplicacion_id = D_stvFwkCfg.cfg_cuenta_aplicacion_id

        _D_almacenesResult = TEMPRESA_PLAZA_SUCURSAL_ALMACEN_ALMACENISTAS.GET_ALMACENES(
            n_empresa_id = db.tcuenta_aplicaciones(_s_cuenta_aplicacion_id).empresa_id,
            n_usuario_id = auth.user.id, 
            )
        
        _s_fieldname = 'master_empresa_plaza_sucursal_almacen_id'
        _D_row = Storage()
        _D_row[_s_fieldname] = n_default
        _dbField_almacenes = Field(_s_fieldname, 'integer', default = n_default,
          required = True, requires = IS_IN_SET(_D_almacenesResult.D_almacenes, zero = 0, error_message = 'Selecciona' ),
          notnull = True, unique = False,
          widget = lambda f, v: stv_widget_combobox(f, n_default), 
          writable = b_writable, readable = True,
          represent = lambda v, r: stv_represent_list(
              n_default, 
              _D_row, 
              None, 
              _D_almacenesResult.D_almacenes,
              D_additionalAttributes = Storage({
                  'bForce' : True,
                  '_id' : _s_fieldname,
                  '_name' : _s_fieldname,
                  #'_data-value' :  n_default
                  }) 
              )
          )
        
        return _dbField_almacenes
    
    @classmethod
    def CREAR_BREADCRUB(
            cls,
            s_texto,
            s_url,
            n_tipo):
        return Storage(
            s_texto = STV_LIB_APP_ALMACENES.WRITE_HTML(s_texto),
            s_url = s_url,
            n_tipo = n_tipo
            )
    

class STV_CFG_APP_ALMACENES():
    ID = "app_almacenes_id"
    
    class E_TIPO_LLAMADA():
        DOC = 0
        AJAX = 1
    
    
class STV_ALMACENES():
    
    class BREADCRUB():
    
        BC_INDEX = STV_LIB_APP_ALMACENES.CREAR_BREADCRUB(
            "Movimientos",
            URL(c='default', f='index'),
            STV_CFG_APP_ALMACENES.E_TIPO_LLAMADA.DOC
            )
        class MOVTO():
            class ENTRADA():
                BC_INVENTARIOINICIAL = STV_LIB_APP_ALMACENES.CREAR_BREADCRUB(
                    "Inventario inicial",
                    URL(c='610entradas', f='movto_entrada_inventarioinicial'),
                    STV_CFG_APP_ALMACENES.E_TIPO_LLAMADA.AJAX
                    )
                BC_PORCOMPRA = STV_LIB_APP_ALMACENES.CREAR_BREADCRUB(
                    "Entrada por compra",
                    URL(c='610entradas', f='movto_entrada_porcompra'),
                    STV_CFG_APP_ALMACENES.E_TIPO_LLAMADA.AJAX
                    )
                BC_PORDEVOLUCION = STV_LIB_APP_ALMACENES.CREAR_BREADCRUB(
                    "Entrada por devolución",
                    URL(c='610entradas', f='movto_entrada_pordevolucion'),
                    STV_CFG_APP_ALMACENES.E_TIPO_LLAMADA.AJAX
                    )
                BC_PORTRASPASO = STV_LIB_APP_ALMACENES.CREAR_BREADCRUB(
                    "Entrada por traspaso",
                    URL(c='610entradas', f='movto_entrada_portraspaso'),
                    STV_CFG_APP_ALMACENES.E_TIPO_LLAMADA.AJAX
                    )
                BC_OTRA = STV_LIB_APP_ALMACENES.CREAR_BREADCRUB(
                    "Otra entrada",
                    URL(c='610entradas', f='movto_entrada_otra'),
                    STV_CFG_APP_ALMACENES.E_TIPO_LLAMADA.AJAX
                    )
                class PORCOMPRA():
                    BC_REMISION = STV_LIB_APP_ALMACENES.CREAR_BREADCRUB(
                        "Usando remisión",
                        URL(c='610entradas', f='movto_entrada_porcompra_remision'),
                        STV_CFG_APP_ALMACENES.E_TIPO_LLAMADA.AJAX
                        )
                    
            class SALIDA():
                BC_PORVENTA = STV_LIB_APP_ALMACENES.CREAR_BREADCRUB(
                    "Salida por venta",
                    URL(c='620salidas', f='movto_salida_porventa'),
                    STV_CFG_APP_ALMACENES.E_TIPO_LLAMADA.AJAX
                    )
                BC_PORDEVOLUCION = STV_LIB_APP_ALMACENES.CREAR_BREADCRUB(
                    "Salida por devolucion de compra",
                    URL(c='620salidas', f='movto_salida_pordevolucion_compra'),
                    STV_CFG_APP_ALMACENES.E_TIPO_LLAMADA.AJAX
                    )
                BC_PORTRASPASO = STV_LIB_APP_ALMACENES.CREAR_BREADCRUB(
                    "Salida por traspaso",
                    URL(c='620salidas', f='movto_salida_portraspaso'),
                    STV_CFG_APP_ALMACENES.E_TIPO_LLAMADA.AJAX
                    )
                BC_EMBARQUES = STV_LIB_APP_ALMACENES.CREAR_BREADCRUB(
                    "Embarques",
                    URL(c='630embarques', f='empresa_embarques'),
                    STV_CFG_APP_ALMACENES.E_TIPO_LLAMADA.AJAX
                    )
                BC_OTRA = STV_LIB_APP_ALMACENES.CREAR_BREADCRUB(
                    "Otra salida",
                    URL(c='620salidas', f='movto_salida_otra'),
                    STV_CFG_APP_ALMACENES.E_TIPO_LLAMADA.AJAX
                    )

