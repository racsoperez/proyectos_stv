# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations
# __updated__ = '' 

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
#########################################################################

def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    response.flash = str(T("Hello World"))
    
    _db_tree_menu = db120_menus_generarMenu(D_stvFwkCfg.cfg_cuenta_aplicacion_id)


    return dict(
        message = T('Welcome to web2py!'),
        dbTree = _db_tree_menu,
        L_inputs = [
            Storage(
                s_parent_id = "",
                s_selector = 'cfg_cuenta_aplicacion_id',
                s_tipo = "fijo",
                s_valor = str(D_stvFwkCfg.cfg_cuenta_aplicacion_id or "")
                ),            
            ]
        )


def user():
    redirect(URL(a=D_stvFwkCfg.s_nameAccesoApp, c='default', f='user', vars=request.vars, args=request.args))

@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)
