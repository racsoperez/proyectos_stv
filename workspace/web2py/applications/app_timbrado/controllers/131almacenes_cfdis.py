# -*- coding: utf-8 -*-

def almacenes():
    """ Formulario de almacenes, optimizado en la búsqueda de expedientes de almacenes.
    
    @descripcion Almacenes
    
    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.
    
    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    #@permiso btn_signature
    @permiso special_permisson_01:[fas fa-money-check-alt] Emisión de CFDIs
    @permiso special_permisson_02:[<i class="fas fa-balance-scale-right] Recalcular saldos
    """

    _dbTable = dbc01.tempresa_plaza_sucursal_almacenes

    # Se casa la empresa_id con la que identifica a la aplicación
    _s_empresa_id = D_stvSiteHelper.dbconfigs.defaults.empresa_id
    _s_action = request.args(2, None)
    _s_almacen_id = request.args(3, None)

    _s_action_override = None
    # En caso de presionar el boton especial1; o si se esta trabajando en la subforma cliente_cfdis
    if _s_action == request.stv_fwk_permissions.special_permisson_01.code:
        request.args[2] = request.stv_fwk_permissions.btn_view.code
        _s_action_override = request.stv_fwk_permissions.btn_view.code

    elif _s_action == request.stv_fwk_permissions.special_permisson_02.code:
        # _D_results = STV_LIB_CFDI.ESTADOCUENTA(
        #     _s_empresa_id,
        #     _s_cliente_id,
        #     E_movto_generado_por = TCFDI_MOVIMIENTOS.E_GENERADO_POR.RECALCULO_CLIENTE
        #     ).procesar_cliente()

        # _D_insert_cliente_log = Storage(
        #     empresa_cliente_id = _s_cliente_id,
        #     fecha = request.browsernow,
        #     descripcion = "Cálculo de saldo manual",
        #     tipotransaccion = TEMPRESA_CLIENTE_LOG.TIPOTRANSACCION.CALCULODESALDO,
        #     errores = "\n".join(_D_results.L_msgError),
        #     warnings = "\n".join(_D_results.L_msgWarning),
        #     info = "\n".join(_D_results.L_msgInformativo),
        #     resultado = _D_results.E_return
        #     )
        #
        # dbc01.tempresa_cliente_log.insert(**_D_insert_cliente_log)
        _s_action_override = request.stv_fwk_permissions.btn_view.code

    else:
        pass

    request.stv_fwk_permissions.L_formButtonsAll += [
        # request.stv_fwk_permissions.special_permisson_01, Pendiende de ver su necesidad
        request.stv_fwk_permissions.special_permisson_02
        ]

    request.stv_fwk_permissions.special_permisson_01.s_type = "button"
    request.stv_fwk_permissions.special_permisson_01.s_actualizar = "stv_gIDTabMain"

    request.stv_fwk_permissions.agregarPermisoAVista(
        request.stv_fwk_permissions.special_permisson_01,
        [
            request.stv_fwk_permissions.view_selectedRow,
            request.stv_fwk_permissions.view_viewRegister,
            request.stv_fwk_permissions.view_editRegister
            ]
        )

    request.stv_fwk_permissions.special_permisson_02.s_type = "button"
    request.stv_fwk_permissions.special_permisson_02.s_actualizar = "stv_gIDTabMain"

    request.stv_fwk_permissions.agregarPermisoAVista(
        request.stv_fwk_permissions.special_permisson_02,
        [
            request.stv_fwk_permissions.view_selectedRow,
            request.stv_fwk_permissions.view_viewRegister,
            request.stv_fwk_permissions.view_editRegister
            ]
        )

    _dbQrySearch = (
        (dbc01.tempresa_plaza_sucursales.id == _dbTable.empresa_plaza_sucursal_id)
        & (dbc01.tempresa_plazas.id == dbc01.tempresa_plaza_sucursales.empresa_plaza_id)
        & (dbc01.tempresa_plazas.empresa_id == _s_empresa_id)
        )

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_dbQrySearch),
        dbTable = _dbTable,
        dbTableMaster = None,
        xFieldLinkMaster = [],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.empresa_plaza_sucursal_id,
            _dbTable.nombre, _dbTable.metodocosteo, _dbTable.uso,
            _dbTable.ciudad, _dbTable.telefonos, _dbTable.email,
            _dbTable.empresa_serie_traslado_id,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.id,
            _dbTable.nombre,
            _dbTable.ciudad,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            _dbTable.empresa_plaza_sucursal_id
            ],
        x_offsetInArgs = request.args[:2],
        )

    # _O_cat.addRowContentEvent(
    #     STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION,
    #     TEMPRESA_CLIENTES.AL_VALIDAR,
    #     s_empresa_id = _s_empresa_id
    #     )

    _D_returnVars = _O_cat.process(s_overrideCode = _s_action_override)

    _O_cat.addSubTab(
        s_tabName = 'Movtos. Pendientes',
        s_url = URL(
            a = 'app_inventarios',
            c = '610entradas',
            f = 'movtospendientes',
            args = request.args[:2] + [
                'master_' + str(request.function),
                request.args(3)
                ]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_inventarios',
            c = '610entradas',
            f = 'movtospendientes',
            s_masterIdentificator = str(request.function)
            )
        )

    _D_returnVars['htmlid_domicilio'] = _O_cat.addSubTab(
        dbTableDetail = None,
        s_url = False,
        s_tabName = "Domicilio",
        s_idHtml = None,
        s_type = "extended"
        )

    _O_cat.addSubTab(
        s_tabName = 'CFDIs Traslados',
        s_url = URL(
            a = 'app_timbrado',
            c = '131almacenes_cfdis',
            f = 'almacen_cfdis_traslados',
            args = request.args[:2] + ['master_' + str(request.function), request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado',
            c = '131almacenes_cfdis',
            f = 'almacen_cfdis_traslados',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_plaza_sucursal_almacen_almacenistas,
        s_url = URL(
            a = 'app_empresas',
            c = '125inventarios',
            f = 'empresa_plaza_sucursal_almacen_almacenistas',
            args = request.args[:2] + ['master_' + str(request.function), request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_empresas',
            c = '125inventarios',
            f = 'empresa_plaza_sucursal_almacen_almacenistas',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_plaza_sucursal_almacen_ejercicios,
        s_url = URL(
            a = 'app_empresas',
            c = '125inventarios',
            f = 'empresa_plaza_sucursal_almacen_ejercicios',
            args = request.args[:2] + ['master_' + str(request.function), request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_empresas',
            c = '125inventarios',
            f = 'empresa_plaza_sucursal_almacen_ejercicios',
            s_masterIdentificator = str(request.function)
            )
        )

    # _D_returnVars['htmlid_credito'] = _O_cat.addSubTab(
    #     dbTableDetail = None,
    #     s_url = False,
    #     s_tabName = "Crédito",
    #     s_idHtml = None,
    #     s_type = "extended"
    #     )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_almacen_log,
        s_url = URL(
            a = 'app_timbrado',
            c = '131almacenes_cfdis',
            f = 'empresa_almacen_log',
            args = request.args[:2] + [
                'master_' + str(request.function),
                request.args(3)
                ]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado',
            c = '131almacenes_cfdis',
            f = 'empresa_almacen_log',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        s_tabName = TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.GET_DICT()[
            TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.INVENTARIO_INICIAL
            ],
        s_url = URL(
            a = 'app_inventarios',
            c = '610entradas',
            f = 'inventarioinicial',
            args = request.args[:2] + [
                'master_' + str(request.function),
                request.args(3)
                ]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_inventarios',
            c = '610entradas',
            f = 'inventarioinicial',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        s_tabName = TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.GET_DICT()[
            TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.COMPRA],
        s_url = URL(
            a = 'app_inventarios',
            c = '610entradas',
            f = 'porcompra',
            args = request.args[:2] + [
                'master_' + str(request.function),
                request.args(3)
                ]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_inventarios',
            c = '610entradas',
            f = 'porcompra',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_almacen_ordenescompra,
        s_url = URL(
            a = 'app_timbrado',
            c = '131almacenes_cfdis',
            f = 'almacen_ordenescompra',
            args = request.args[:2] + [
                'master_' + str(request.function),
                request.args(3)
                ]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado',
            c = '131almacenes_cfdis',
            f = 'almacen_ordenescompra',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_almacen_productos,
        s_url = URL(
            a = 'app_timbrado',
            c = '131almacenes_cfdis',
            f = 'almacen_productos',
            args = request.args[:2] + [
                'master_' + str(request.function), request.args(3)
                ]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado',
            c = '131almacenes_cfdis',
            f = 'almacen_productos',
            s_masterIdentificator = str(request.function)
            )
        )

    return _D_returnVars


def almacen_cfdis_traslados():
    """ Formulario de CFDIs de traslados.

    @descripcion CFDIs de Traslados

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Almacen
    @keyword arg3: empresa_almacen_id.
    @keyword [arg4]: operación a realizar en el formulario.
    @keyword [arg5]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-file-code] Generar XML del CFDI
    @permiso special_permisson_02:[fas fa-stamp] Timbrar CFDI
    @permiso special_permisson_03:[fas fa-ban] Cancelar CFDI
    """

    # noinspection DuplicatedCode
    _D_return = Storage(
        E_return = CLASS_e_RETURN.OK,
        s_msgError = "",
        dbTable = dbc01.tcfdis,
        )

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 4,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_almacen_id = request.args(3, 0),
        s_cfdi_id = None,
        s_cartaporte_id = None
        )
    if _D_args.L_ids and _D_args.L_ids[0]:
        _D_args.s_cfdi_id = _D_args.L_ids[0][0]
        _D_args.s_cartaporte_id = _D_args.L_ids[0][1]
    else:
        pass

    _dbTable = dbc01.tcfdis
    _D_useView = None
    _CLS_TABLA = TCFDIS.TRASLADO

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):
        # Si esta editando o guardando una edición, se configuran los campos del CFDI
        _D_result = _CLS_TABLA.configuracioncampos_edicion(x_cfdi = _D_args.s_cfdi_id)

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_result)
            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            # Si todo va bien, se configuran los campos de la cartaporte que se usaran en la misma captura
            _D_result = TCFDI_COMPCARTAPORTE.configuracioncampos_edicion()
            _D_return.update(_D_result)

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _dbRow_almacen = dbc01.tempresa_plaza_sucursal_almacenes(_D_args.s_almacen_id)
        _D_result = _CLS_TABLA.configuracioncampos_nuevo(
            s_cfdi_id = _D_args.s_cfdi_id,
            empresa_id = _D_args.s_empresa_id,
            emp_pla_suc_almacen_id = _D_args.s_almacen_id
            )

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_result)

            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            # Si todo va bien, se configuran los campos de la cartaporte que se usaran en la misma captura
            _D_result = TCFDI_COMPCARTAPORTE.configuracioncampos_nuevo(
                s_cfdi_compCartaPorte_id = _D_args.s_cartaporte_id,
                x_cfdi = None
                )
            _D_return.update(_D_result)

            # Los valores por default se llenan en configuraCampos_nuevo

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.special_permisson_01.code,
            ):

        _D_result = STV_LIB_CFDI.REPRESENTAR_JSON(
            n_empresa_id = int(_D_args.s_empresa_id),
            n_cfdi_id = int(_D_args.s_cfdi_id)
            ).procesar(
                b_grabar = True
                )

        if _D_result.E_return == stvfwk2_e_RETURN.OK:
            stv_flashSuccess(
                'CFDI',
                "Creación de XML correcta",
                s_mode = 'stv_flash',
                D_addProperties = {}
                )

        else:
            stv_flashError(
                'CFDI',
                "Creación de XML incorrecta: " + _D_result.s_msgError,
                s_mode = 'stv_flash',
                D_addProperties = {}
                )

    elif _D_args.s_accion == request.stv_fwk_permissions.special_permisson_02.code:
        # Se intenta timbrar el CFDI

        _D_results = TCFDIS.TIMBRADO.TIMBRAR(x_cfdi = _D_args.s_cfdi_id)
        _D_return.update(_D_results)
        # _D_useView = request.stv_fwk_permissions.view_refresh

    elif _D_args.s_accion == request.stv_fwk_permissions.special_permisson_03.code:
        # Se intenta cancelar el CFDI

        cancelar()

        # _D_useView = request.stv_fwk_permissions.view_refresh
    else:
        pass

    # Principal filtro de CFDIs a mostrar
    _dbQrySearch = (
        (_dbTable.empresa_id == _D_args.s_empresa_id)
        & (_dbTable.rol == TCFDIS.E_ROL.EMISOR)
        & (_dbTable.tipocomprobante_id == TTIPOSCOMPROBANTES.TRASLADO)
        )

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_dbQrySearch),
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_plaza_sucursal_almacenes,
        xFieldLinkMaster = [_dbTable.emp_pla_suc_almacen_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01,
            request.stv_fwk_permissions.special_permisson_02,
            request.stv_fwk_permissions.special_permisson_03,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, dbc01.tcfdi_compcartaporte.id,
            _dbTable.generado_por, _dbTable.serie, _dbTable.folio, _dbTable.estado,
            _dbTable.fecha, _dbTable.fechatimbrado, _dbTable.sat_status,
            # _dbTable.metodopago,
            # _dbTable.formapago_id, _dbTable.credito_plazo, _dbTable.v_vencimiento,
            # _dbTable.v_estatuscxc, _dbTable.v_dvencido,
            # _dbTable.total, _dbTable.saldo, _dbTable.moneda, _dbTable.receptorrfc,
            # _dbTable.receptornombrerazonsocial,
            # _dbTable.emisorrfc,
            # _dbTable.emisornombrerazonsocial
            dbc01.tcfdi_compcartaporte.transpinternac, dbc01.tcfdi_compcartaporte.totaldistrec,
            dbc01.tcfdi_compcartaporte.merc_pesobrutototal, dbc01.tcfdi_compcartaporte.merc_unidadpeso_id,
            dbc01.tcfdi_compcartaporte.merc_pesonetototal, dbc01.tcfdi_compcartaporte.merc_numtotalmercancias,
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        L_fieldnameIds = [
            _dbTable.id, dbc01.tcfdi_compcartaporte.id,
            ],
        x_offsetInArgs = _D_args.n_offsetArgs - 2
        )

    _O_cat.addRowSearchResultsLeftJoin(
        dbc01.tcfdi_compcartaporte.on(
            (dbc01.tcfdi_compcartaporte.cfdi_id == _dbTable.id)
            )
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION,
        _CLS_TABLA.al_validar  # Se valida la info para el CFDI de traspaso
        )
    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION,
        TCFDI_COMPCARTAPORTE.al_validar,  # Se valida la info para la Carta Porte
        b_validarParaFormaPrincipal = True
        )
    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED,
        _CLS_TABLA.AL_ACEPTAR
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE,
        _CLS_TABLA.ANTES_ELIMINAR,
        dbRow = None  # Es necesario para indicar que se mande en dbRow que se intenta eliminar,
        #  llenado por la clase del FWK
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.AFTER_REMOVE,
        _CLS_TABLA.DESPUES_ELIMINAR,
        )

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_conceptos,
        s_url = URL(
            a = 'app_timbrado',
            c = '131almacenes_cfdis',
            f = 'cfdi_conceptos',
            args = request.args[:4] + ['master_' + str(request.function), request.args(5)]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado',
            c = '131almacenes_cfdis',
            f = 'cfdi_conceptos',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_relacionados,
        s_url = URL(
            a = 'app_timbrado',
            c = '131almacenes_cfdis',
            f = 'cfdi_relacionados',
            args = request.args[:4] + ['master_' + str(request.function), request.args(5)]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado',
            c = '131almacenes_cfdis',
            f = 'cfdi_relacionados',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc02.tcfdi_xmls,
        s_url = URL(
            a = 'app_timbrado',
            c = '250cfdi',
            f = 'cfdi_xml',
            args = ['master_' + str(request.function), _D_args.s_cfdi_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado',
            c = '250cfdi',
            f = 'cfdi_xml',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_compcartaporte_ubicaciones,
        s_url = URL(
            a = 'app_timbrado',
            c = '131almacenes_cfdis',
            f = 'almacen_cfdis_traslado_ubicaciones',
            args = request.args[:4] + ['master_' + str(request.function), request.args(5)]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado',
            c = '131almacenes_cfdis',
            f = 'almacen_cfdis_traslado_ubicaciones',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_compcartaporte_mercancias,
        s_url = URL(
            a = 'app_timbrado',
            c = '131almacenes_cfdis',
            f = 'almacen_cfdis_traslado_mercancias',
            args = request.args[:4] + ['master_' + str(request.function), request.args(5)]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado',
            c = '131almacenes_cfdis',
            f = 'almacen_cfdis_traslado_mercancias',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_compcartaporte_autotransporte,
        s_url = URL(
            a = 'app_timbrado',
            c = '131almacenes_cfdis',
            f = 'almacen_cfdis_traslado_autotransporte',
            args = request.args[:4] + ['master_' + str(request.function), request.args(5)]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado',
            c = '131almacenes_cfdis',
            f = 'almacen_cfdis_traslado_autotransporte',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_compcartaporte_figurastransporepersonas,
        s_url = URL(
            a = 'app_timbrado', c = '131almacenes_cfdis',
            f = 'almacen_cfdis_traslado_figuratranspersonas',
            args = request.args[:4] + ['master_' + str(request.function), request.args(5)]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado', c = '131almacenes_cfdis',
            f = 'almacen_cfdis_traslado_figuratranspersonas',
            s_masterIdentificator = str(request.function)
            )
        )

    # Manejo de CartaPorte
    if _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_view.code,
            ):

        if _D_args.s_cartaporte_id:
            pass

        else:
            _D_args.s_cfdi_id = _D_returnVars.rowContent_form.vars.id or _D_returnVars.rowContent_form.record_id or None
            if _D_args.s_cfdi_id:
                # Si tiene registro el CFDI

                _dbRows_cartaPorte = dbc01(
                    dbc01.tcfdi_compcartaporte.cfdi_id == _D_args.s_cfdi_id
                    ).select(
                        dbc01.tcfdi_compcartaporte.ALL
                        )
                if _dbRows_cartaPorte:
                    _dbRow_cartaPorte = _dbRows_cartaPorte.first()
                    _D_args.s_cartaporte_id = _dbRow_cartaPorte.id
                else:
                    pass
            else:
                pass

        dbc01.tcfdi_compcartaporte.cfdi_id.default = _D_args.s_cfdi_id

        if not _D_args.s_cartaporte_id and _D_args.s_accion in (
                request.stv_fwk_permissions.btn_new.code,
                request.stv_fwk_permissions.btn_edit.code,
                ):
            _s_accionCartaPorte = request.stv_fwk_permissions.btn_new.code
        else:
            _s_accionCartaPorte = _D_args.s_accion

        # Si existe un carta porte para el CFDI, asignar editar
        request.args[_D_args.n_offsetArgs] = _s_accionCartaPorte
        if _D_args.s_cartaporte_id:
            request.args.insert(_D_args.n_offsetArgs + 1, _D_args.s_cartaporte_id)
        else:
            pass

        _D_returnVars.s_formkey_cartaPorte = "_formkey_" + str(dbc01.tcfdi_compcartaporte)
        _D_returnVars.s_recordid_cartaPorte = "_recordid_" + str(dbc01.tcfdi_compcartaporte)
        if request.vars.get(_D_returnVars.s_formkey_cartaPorte, None):
            # Con esto se simula la captura de la forma de cartaporte
            request._post_vars._formkey = request.vars[_D_returnVars.s_formkey_cartaPorte]
            request._vars._formkey = request.vars[_D_returnVars.s_formkey_cartaPorte]
            request._post_vars.id = request.vars[_D_returnVars.s_recordid_cartaPorte]
            request._vars.id = request.vars[_D_returnVars.s_recordid_cartaPorte]
        else:
            pass

        _O_catCartaPorte = STV_FWK_FORM(
            dbReference = dbc01,
            dbReferenceSearch = dbc01,
            dbTable = dbc01.tcfdi_compcartaporte,
            L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
            s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
            L_fieldsToShow = [
                dbc01.tcfdi_compcartaporte.id,
                # dbc01.tcfdi_compcartaporte.transpinternac, dbc01.tcfdi_compcartaporte.totaldistrec,
                # dbc01.tcfdi_compcartaporte.merc_pesobrutototal, dbc01.tcfdi_compcartaporte.merc_unidadpeso_id,
                # dbc01.tcfdi_compcartaporte.merc_pesonetototal, dbc01.tcfdi_compcartaporte.merc_numtotalmercancias,
                ],
            L_fieldsToSearch = [_dbTable.id],
            L_fieldsToSearchIfDigit = [_dbTable.id],
            x_offsetInArgs = _D_args.n_offsetArgs
            )

        _O_catCartaPorte.addRowContentEvent(
            STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION,
            TCFDI_COMPCARTAPORTE.al_validar,
            )

        _O_catCartaPorte.addRowContentEvent(
            STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED,
            TCFDI_COMPCARTAPORTE.AL_ACEPTAR,
            )

        _D_returnVars.O_cartaPorte = _O_catCartaPorte.process()

        # Si la forma fue aceptada
        if (
                not _D_args.s_cartaporte_id
                and _D_returnVars.O_cartaPorte.rowContent_form
                and _D_returnVars.O_cartaPorte.rowContent_form.accepted
                ):
            dbc01.commit()

            # Se importa gluon.storage para poder crear un objeto especial de lista para request.args
            from gluon.storage import List
            request.args = List(
                _D_args.L_argsBasicos + [
                    request.stv_fwk_permissions.btn_edit.code,
                    str(_D_args.s_cfdi_id) + "-" + str(_D_returnVars.O_cartaPorte.rowContent_form.vars.id)
                    ]
                )
            request._post_vars = Storage()
            _D_returnVars = almacen_cfdis_traslados()
            return _D_returnVars
        else:
            pass

    else:
        _D_returnVars.O_cartaPorte = None

    generaMensajeFlash(_dbTable, _D_return)

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_type = "button",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_edit),
        D_actions = Storage(
            D_data = Storage(),
            modal = Storage(
                # s_url = URL(
                #     a = 'app_timbrado',
                #     c = '131almacenes_cfdis',
                #     f = 'importar_cfdi',
                #     args = _D_args.L_argsBasicos[:4],
                #     vars = Storage(
                #         master = request.function,
                #         s_almacen_id = _D_args.s_almacen_id,
                #         )
                #     ),
                # s_field = "",
                )
            )
        )
    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_02.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_02.code,
        s_type = "button",
        s_actualizar = "stv_gIDTabMain",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_edit),
        D_actions = Storage(
            D_data = Storage(),
            modal = Storage(
                # s_url = URL(
                #     a = 'app_timbrado',
                #     c = '131almacenes_cfdis',
                #     f = 'importar_cfdi',
                #     args = _D_args.L_argsBasicos[:4],
                #     vars = Storage(
                #         master = request.function,
                #         s_almacen_id = _D_args.s_almacen_id,
                #         )
                #     ),
                # s_field = "",
                )
            )
        )

    return _D_returnVars


def cfdi_conceptos():
    """ Formulario detalle de conceptos del cfdi

    @descripcion Conceptos

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Almacen
    @keyword arg3: empresa_almacen_id.
    @keyword arg4: CFDI Traslado
    @keyword arg5: cfdi_id y cartaporte_id.
    @keyword [arg6]: operación a realizar en el formulario.
    @keyword [arg7]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-file-code] Importar excel
    """

    # noinspection DuplicatedCode
    _D_return = Storage(
        E_return = CLASS_e_RETURN.OK,
        s_msgError = "",
        dbTable = dbc01.tcfdi_conceptos,
        )

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 6,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_almacen_id = request.args(3, 0),
        s_cfdi_id = request.args(5, 0).split("-")[0],
        s_cartaporte_id = request.args(5, 0).split("-")[1],
        )

    _dbTable = _D_return.dbTable
    _dbTable.empresa_prodserv_id.label = "Producto ID"
    _D_useView = None
    _CLS_TABLA = TCFDI_CONCEPTOS.TRASLADO

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):
        # Si esta editando o guardando una edición, se configuran los campos del CFDI
        _D_result = _CLS_TABLA.configuracioncampos_edicion()
        _D_return.update(_D_result)

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _D_result = _CLS_TABLA.configuracioncampos_nuevo(
            s_cfdi_concepto_id = None,
            x_cfdi = _D_args.s_cfdi_id
            )
        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_result)

            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            # Los valores por default se llenan en configuraCampos_nuevo
            pass

    else:
        pass

    # Principal filtro de CFDIs a mostrar
    _dbQrySearch = (_dbTable.cfdi_id == _D_args.s_cfdi_id)

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_dbQrySearch),
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.prodserv_id,
            _dbTable.noidentificacion,
            _dbTable.descripcion,
            _dbTable.cantidad,
            _dbTable.unidad_id,
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        x_offsetInArgs = _D_args.n_offsetArgs
        )

    _O_cat.addRowSearchResultsLeftJoin(
        dbc01.tempresa_prodserv.on(
            (dbc01.tempresa_prodserv.id == _dbTable.empresa_prodserv_id)
            )
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION,
        _CLS_TABLA.al_validar,
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED,
        _CLS_TABLA.AL_ACEPTAR
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE,
        _CLS_TABLA.ANTES_ELIMINAR,
        dbRow = None  # Es necesario para indicar que se mande en dbRow que se intenta eliminar,
        #  llenado por la clase del FWK
        )

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView
        )

    if _D_returnVars.rowContent_form and _D_returnVars.rowContent_form.accepted:
        _O_cat.agregar_comando(
            E_comando = STV_FWK_FORM.E_COMANDOS.REFRESCAR_TABS_HERMANOS,
            D_parametros = Storage(
                L_tabNombres = [dbc01.tcfdi_compcartaporte_mercancias._plural]
                )
            )
    else:
        pass

    generaMensajeFlash(_dbTable, _D_return)

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_type = "button",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_new),
        D_actions = Storage(
            D_data = Storage(),
            modal = Storage(
                s_url = URL(
                    a = 'app_timbrado',
                    c = '131almacenes_cfdis',
                    f = 'importar_excel',
                    args = _D_args.L_argsBasicos[:4],
                    vars = Storage(
                        master = request.function,
                        s_cfdi_id = _D_args.s_cfdi_id,
                        )
                    ),
                s_field = "",
                )
            )
        )

    return _D_returnVars


def cfdi_relacionados():
    """ Formulario detalle de documentos relacionados del cfdi

    @descripcion Relacionados

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Cliente
    @keyword arg3: empresa_cliente_id.
    @keyword arg4: CFDI Traslado
    @keyword arg5: cfdi_id y cartaporte_id.
    @keyword [arg6]: operación a realizar en el formulario.
    @keyword [arg7]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    #@permiso btn_signature
    """

    # noinspection PyShadowingNames
    def accion_searchdone(
            D_args,
            dbRow_cfdi,
            L_seleccionados
            ):
        # noinspection DuplicatedCode
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            D_useView = None
            )
        _D_result = _CLS_TABLA.configuracioncampos_nuevo(
            s_cfdi_relacion_id = D_args.n_id,
            x_cfdi = dbRow_cfdi,
            )
        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_result)

            _D_return.D_useView = request.stv_fwk_permissions.view_refresh

        else:

            for _L_id in L_seleccionados:
                _O_form = Storage(
                    record_id = None,  # Para simular inserción
                    vars = Storage(
                        cfdi_id = dbRow_cfdi.id,
                        cfdirelacionado_id = int(_L_id[0])
                        ),
                    errors = Storage(
                        )
                    )
                _CLS_TABLA.al_validar(O_form = _O_form)
                if _O_form.errors:
                    stv_flashError(_dbTable._plural, ".\n".join(_O_form.errors.values()))

                else:
                    _O_form.vars.id = _dbTable.insert(**_O_form.vars)
                    _CLS_TABLA.AL_ACEPTAR(O_form = _O_form)
                    stv_flashSuccess(
                        _dbTable._plural,
                        "CFDI %d relacionado correctamente" % _O_form.vars.cfdirelacionado_id
                        )

        return _D_return

    # noinspection DuplicatedCode
    _D_return = Storage(
        E_return = CLASS_e_RETURN.OK,
        s_msgError = "",
        dbTable = dbc01.tcfdi_relacionados,
        )

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 6,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_almacen_id = request.args(3, 0),
        s_cfdi_id = request.args(5, 0).split("-")[0],
        s_cartaporte_id = request.args(5, 0).split("-")[1],
        )

    _dbTable = _D_return.dbTable
    _D_useView = None
    _s_overrideCode = None
    _CLS_TABLA = TCFDI_RELACIONADOS.TRASLADO
    _dbRow_cfdi = dbc01.tcfdis(_D_args.s_cfdi_id)

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):
        # Si esta editando o guardando una edición, se configuran los campos del CFDI
        _D_result = _CLS_TABLA.configuracioncampos_edicion(x_cfdi = _dbRow_cfdi)
        _D_return.update(_D_result)

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _D_result = _CLS_TABLA.configuracioncampos_nuevo(
            s_cfdi_relacion_id = _D_args.n_id,
            x_cfdi = _dbRow_cfdi,
            )
        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_result)
            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            # Los valores por default se llenan en configuraCampos_nuevo
            pass

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.special_permisson_01.code,
            request.stv_fwk_permissions.special_permisson_02.code,
            ):

        stv_flashInfo(_dbTable._plural, "Traslado botones de timbrado, pendiente de desarrollo")
        _D_useView = request.stv_fwk_permissions.view_refresh

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_searchdone.code,
            ):

        if "L_seleccionados" not in request.vars:
            stv_flashWarning(_dbTable._plural, "No se seleccionaron registros para insertar")
        else:

            _D_result = accion_searchdone(
                D_args = _D_args,
                dbRow_cfdi = _dbRow_cfdi,
                L_seleccionados = simplejson.loads(request.vars.L_seleccionados)
                )

            if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.update(_D_result)

                _D_useView = _D_return.D_useView

            else:
                pass

        _s_overrideCode = request.stv_fwk_permissions.btn_findall.code

    else:
        pass

    # Principal filtro de CFDIs a mostrar
    _dbQrySearch = (_dbTable.cfdi_id == _D_args.s_cfdi_id)

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_dbQrySearch),
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01,
            request.stv_fwk_permissions.special_permisson_02,
            request.stv_fwk_permissions.special_permisson_03,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.cfdirelacionado_id,
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        x_offsetInArgs = _D_args.n_offsetArgs
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION,
        _CLS_TABLA.al_validar,
        x_cfdi = _dbRow_cfdi
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED,
        _CLS_TABLA.AL_ACEPTAR
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE,
        _CLS_TABLA.ANTES_ELIMINAR,
        dbRow = None  # Es necesario para indicar que se mande en dbRow que se intenta eliminar,
        #  llenado por la clase del FWK
        )

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView,
        s_overrideCode = _s_overrideCode
        )

    if _D_args.s_accion in (
            request.stv_fwk_permissions.btn_searchdone.code,
            ):
        _O_cat.agregar_comando(
            E_comando = STV_FWK_FORM.E_COMANDOS.REFRESCAR_TABS_HERMANOS,
            D_parametros = Storage(
                L_tabNombres = [dbc01.tcfdi_conceptos._plural, dbc01.tcfdi_compcartaporte_mercancias._plural]
                )
            )
    else:
        pass

    generaMensajeFlash(_dbTable, _D_return)

    s_confBusqueda_tipocomprobante_id = ""
    if _dbRow_cfdi.tiporelacion_id == TTIPOSRELACIONES.TRASLADOFACTPREV:
        s_confBusqueda_tipocomprobante_id = TTIPOSCOMPROBANTES.INGRESO

    elif _dbRow_cfdi.tiporelacion_id == TTIPOSRELACIONES.SUSTITUCION:
        s_confBusqueda_tipocomprobante_id = TTIPOSCOMPROBANTES.TRASLADO

    elif _dbRow_cfdi.tiporelacion_id == TTIPOSRELACIONES.DEVOLUCION:
        s_confBusqueda_tipocomprobante_id = TTIPOSCOMPROBANTES.EGRESO

    else:
        pass

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.btn_new.code,
        s_permissionCode = request.stv_fwk_permissions.btn_none.code,
        s_permissionLabel = 'Agregar relacionados',
        s_permissionIcon = '',
        s_type = 'button',
        s_send = 'find',
        L_enabledByView = None,
        D_actions = Storage(
            modal = Storage(
                s_url = URL(
                    a = 'app_timbrado', c = '250cfdi', f = 'cfdi_buscar_emitidos',
                    args = request.args[:2],
                    vars = Storage(
                        s_llamada = str(request.controller) + "/" + str(request.function),
                        s_cfdi_id = _D_args.s_cfdi_id,
                        E_tipo = STV_FWK_FORM.E_TIPO.MODAL,
                        s_filtro_cliente_id = _dbRow_cfdi.receptorcliente_id or "",
                        tipocomprobante_id = s_confBusqueda_tipocomprobante_id,
                        s_link_alterminarbusqueda = URL(args = _D_args.L_argsBasicos)
                        )
                    ),
                s_field = "",
                )
            ),
        )

    return _D_returnVars


def almacen_cfdis_traslado_ubicaciones():
    """ Formulario de ubicaciones de CFDIs de traslados.

    @descripcion Ubicaciones

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Almacen
    @keyword arg3: empresa_almacen_id.
    @keyword arg4: CFDI Traslado
    @keyword arg5: cfdi_id y cartaporte_id.
    @keyword [arg6]: operación a realizar en el formulario.
    @keyword [arg7]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-file-code] Agregar sucursal como destino
    @permiso special_permisson_02:[fas fa-stamp] Agregar cliente como destino
    """

    # noinspection DuplicatedCode
    _D_return = Storage(
        E_return = CLASS_e_RETURN.OK,
        s_msgError = "",
        dbTable = dbc01.tcfdi_compcartaporte_ubicaciones,
        )

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 6,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_almacen_id = request.args(3, 0),
        s_cfdi_id = request.args(5, 0).split("-")[0],
        s_cartaporte_id = request.args(5, 0).split("-")[1],
        )

    _dbTable = dbc01.tcfdi_compcartaporte_ubicaciones
    _D_useView = None
    _CLS_TABLA = TCFDI_COMPCARTAPORTE_UBICACIONES

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):
        # Si esta editando o guardando una edición, se configuran los campos del CFDI
        _D_result = _CLS_TABLA.configuracioncampos_edicion()
        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _D_result = _CLS_TABLA.configuracioncampos_nuevo(
            s_cfdi_compCartaPorte_ubicacion_id = None,
            x_cfdi_compCartaPorte = dbc01.tcfdi_compcartaporte(_D_args.s_cartaporte_id)
            )
        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.E_return = _D_result.E_return
            _D_return.s_msgError = _D_result.s_msgError

            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            # Los valores por default se llenan en configuraCampos_nuevo
            pass

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.special_permisson_01.code,
            request.stv_fwk_permissions.special_permisson_02.code,
            ):

        stv_flashInfo(_dbTable._plural, "Traslado botones de timbrado, pendiente de desarrollo")
        _D_useView = request.stv_fwk_permissions.view_refresh

    else:
        pass

    # Principal filtro de CFDIs a mostrar
    _dbQrySearch = (_dbTable.cfdi_compcartaporte_id == _D_args.s_cartaporte_id)

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_dbQrySearch),
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01,
            request.stv_fwk_permissions.special_permisson_02,
            request.stv_fwk_permissions.special_permisson_03,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, _dbTable.distanciarecorrida,
            _dbTable.dom_emp_pla_suc_almacen_id, _dbTable.dom_usardireccioncliente,
            _dbTable.origen_idorigen, _dbTable.origen_rfcremitente, _dbTable.origen_nombreremitente,
            _dbTable.origen_fechahorasalida,
            _dbTable.destino_iddestino, _dbTable.destino_rfcdestinatario, _dbTable.destino_nombredestinatario,
            _dbTable.destino_fechahoraprogllegada,
            _dbTable.dom_calle,
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        x_offsetInArgs = _D_args.n_offsetArgs
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION,
        _CLS_TABLA.al_validar,
        x_cfdi_compCartaPorte = _D_args.s_cartaporte_id
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED,
        _CLS_TABLA.AL_ACEPTAR
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE,
        _CLS_TABLA.ANTES_ELIMINAR,
        dbRow = None  # Es necesario para indicar que se mande en dbRow que se intenta eliminar,
        #  llenado por la clase del FWK
        )

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView
        )

    if _D_args.s_accion in (
            request.stv_fwk_permissions.btn_save.code,
            request.stv_fwk_permissions.btn_remove.code,
            ):
        # Si hubo cambios en el contenido se actualiza el campo nummercancias en el maestro
        _dbRow_cartaporte = dbc01.tcfdi_compcartaporte(_D_args.s_cartaporte_id)
        _D_actualizarCampos = Storage()
        _D_actualizarCampos[str(dbc01.tcfdi_compcartaporte.totaldistrec)] = Storage(
            x_valor = _dbRow_cartaporte.totaldistrec,
            s_display = _dbRow_cartaporte.totaldistrec
            )
        _O_cat.addRowContentProperty(
            s_property = STV_FWK_FORM.ROWCONTENT.E_PROPERTY.ACTUALIZAR_VALORES_MAESTRO,
            x_value = _D_actualizarCampos
            )
    else:
        pass

    if request.vars.get("esorigen", False):
        _b_esOrigenDefault = request.vars.get("esorigen", False)
    elif _D_returnVars.rowContent_form and _D_returnVars.rowContent_form.record:
        _b_esOrigenDefault = bool(_D_returnVars.rowContent_form.record.origen_idorigen)
    else:
        _b_esOrigenDefault = False

    _b_editarCheckboxes = _D_returnVars.rowContent_form and not _D_returnVars.rowContent_form.readonly
    _D_returnVars.campoEsOrigen = Field(
        'esorigen', 'boolean', default = _b_esOrigenDefault,
        required = False,
        notnull = False,
        widget = lambda f, v: stv_widget_inputCheckbox(f, v, D_additionalAttributes = Storage(b_unaLinea = True)),
        label = 'Es Ubicación Origen', comment = None,
        writable = _b_editarCheckboxes, readable = True,
        represent = stv_represent_boolean
        )

    if request.vars.get("esdestino", False):
        _b_esDestinoDefault = request.vars.get("esdestino", False)
    elif _D_returnVars.rowContent_form and _D_returnVars.rowContent_form.record:
        _b_esDestinoDefault = bool(_D_returnVars.rowContent_form.record.destino_iddestino)
    else:
        _b_esDestinoDefault = True
    _D_returnVars.campoEsDestino = Field(
        'esdestino', 'boolean', default = _b_esDestinoDefault,
        required = False,
        notnull = False,
        widget = lambda f, v: stv_widget_inputCheckbox(f, v, D_additionalAttributes = Storage(b_unaLinea = True)),
        label = 'Es Ubicación Destino', comment = None,
        writable = _b_editarCheckboxes, readable = True,
        represent = stv_represent_boolean
        )

    generaMensajeFlash(_dbTable, _D_return)

    return _D_returnVars


def almacen_cfdis_traslado_mercancias():
    """ Formulario de mercancías de CFDIs de traslados.

    @descripcion Mercancías

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Almacen
    @keyword arg3: empresa_almacen_id.
    @keyword arg4: CFDI Traslado
    @keyword arg5: cfdi_id y cartaporte_id.
    @keyword [arg6]: operación a realizar en el formulario.
    @keyword [arg7]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-file-code] Importar archivo con mercancías
    """

    # noinspection DuplicatedCode
    _D_return = Storage(
        E_return = CLASS_e_RETURN.OK,
        s_msgError = "",
        dbTable = dbc01.tcfdi_compcartaporte_mercancias,
        )

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 6,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_almacen_id = request.args(3, 0),
        s_cfdi_id = request.args(5, 0).split("-")[0],
        s_cartaporte_id = request.args(5, 0).split("-")[1],
        )

    _dbTable = _D_return.dbTable
    _D_useView = None
    _CLS_TABLA = TCFDI_COMPCARTAPORTE_MERCANCIAS

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):
        # Si esta editando o guardando una edición, se configuran los campos del CFDI
        _D_result = _CLS_TABLA.configuracioncampos_edicion()
        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _D_result = _CLS_TABLA.configuracioncampos_nuevo(
            s_cfdi_compCartaPorte_mercancia_id = None,
            x_cfdi_compCartaPorte = _D_args.s_cartaporte_id
            )
        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.E_return = _D_result.E_return
            _D_return.s_msgError = _D_result.s_msgError

            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            # Los valores por default se llenan en configuraCampos_nuevo
            pass

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.special_permisson_01.code,
            request.stv_fwk_permissions.special_permisson_02.code,
            ):

        stv_flashInfo(_dbTable._plural, "Traslado botones de timbrado, pendiente de desarrollo")
        _D_useView = request.stv_fwk_permissions.view_refresh

    else:
        pass

    # Principal filtro de CFDIs a mostrar
    _dbQrySearch = (_dbTable.cfdi_compcartaporte_id == _D_args.s_cartaporte_id)

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_dbQrySearch),
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01,
            request.stv_fwk_permissions.special_permisson_02,
            request.stv_fwk_permissions.special_permisson_03,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, _dbTable.prodservcp_id,
            _dbTable.descripcion, _dbTable.cantidad, _dbTable.unidad_id,
            _dbTable.dimensiones,
            _dbTable.materialpeligroso, _dbTable.cvematerialpeligroso_id, _dbTable.tipoembalaje_id,
            _dbTable.pesoenkg, _dbTable.valormercancia, _dbTable.detallemerc_unidadpeso_id,
            _dbTable.detallemerc_numpiezas,
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        x_offsetInArgs = _D_args.n_offsetArgs
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION,
        _CLS_TABLA.al_validar,
        x_cfdi_compCartaPorte = _D_args.s_cartaporte_id
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED,
        _CLS_TABLA.AL_ACEPTAR,
        x_cfdi_compCartaPorte = _D_args.s_cartaporte_id
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE,
        _CLS_TABLA.ANTES_ELIMINAR,
        dbRow = None  # Es necesario para indicar que se mande en dbRow que se intenta eliminar,
        #  llenado por la clase del FWK
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.AFTER_REMOVE,
        _CLS_TABLA.DESPUES_ELIMINAR,
        x_cfdi_compCartaPorte = _D_args.s_cartaporte_id
        )

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView
        )

    if _D_args.s_accion in (
            request.stv_fwk_permissions.btn_save.code,
            request.stv_fwk_permissions.btn_remove.code,
            ):
        # Si hubo cambios en el contenido se actualiza el campo nummercancias en el maestro
        _dbRow_cartaporte = dbc01.tcfdi_compcartaporte(_D_args.s_cartaporte_id)
        _D_actualizarCampos = Storage()
        _D_actualizarCampos[str(dbc01.tcfdi_compcartaporte.merc_numtotalmercancias)] = Storage(
            x_valor = _dbRow_cartaporte.merc_numtotalmercancias,
            s_display = _dbRow_cartaporte.merc_numtotalmercancias
            )
        _O_cat.addRowContentProperty(
            s_property = STV_FWK_FORM.ROWCONTENT.E_PROPERTY.ACTUALIZAR_VALORES_MAESTRO,
            x_value = _D_actualizarCampos
            )
    else:
        pass

    generaMensajeFlash(_dbTable, _D_return)

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_compcartaporte_mercancia_transporta,
        s_url = URL(
            a = 'app_timbrado', c = '131almacenes_cfdis',
            f = 'almacen_cfdis_traslado_merc_transporta',
            args = _D_args.L_argsBasicos + ['master_' + str(request.function), _D_args.n_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado', c = '131almacenes_cfdis',
            f = 'almacen_cfdis_traslado_merc_transporta',
            s_masterIdentificator = str(request.function)
            )
        )

    return _D_returnVars


def almacen_cfdis_traslado_merc_transporta():
    """ Formulario de transporta de mercancías de CFDIs de traslados.

    @descripcion Transporta

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Almacen
    @keyword arg3: empresa_almacen_id.
    @keyword arg4: CFDI Traslado
    @keyword arg5: cfdi_id y cartaporte_id.
    @keyword arg6: Mercancia
    @keyword arg7: mercancia_id.
    @keyword [arg8]: operación a realizar en el formulario.
    @keyword [arg9]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    """

    # noinspection DuplicatedCode
    _D_return = Storage(
        E_return = CLASS_e_RETURN.OK,
        s_msgError = "",
        dbTable = dbc01.tcfdi_compcartaporte_mercancia_transporta,
        )

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 8,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_almacen_id = request.args(3, 0),
        s_cfdi_id = request.args(5, 0).split("-")[0],
        s_cartaporte_id = request.args(5, 0).split("-")[1],
        s_mercancia_id = request.args(7, 0),
        )

    _dbTable = _D_return.dbTable
    _D_useView = None
    _CLS_TABLA = TCFDI_COMPCARTAPORTE_MERCANCIA_TRANSPORTA

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):
        # Si esta editando o guardando una edición, se configuran los campos del CFDI
        _D_result = _CLS_TABLA.configuracioncampos_edicion()
        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _D_result = _CLS_TABLA.configuracioncampos_nuevo(
            s_cfdi_compCartaPorte_mercancia_transporta_id = None,
            x_cfdi_compCartaPorte_mercancia = _D_args.s_mercancia_id
            )
        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.E_return = _D_result.E_return
            _D_return.s_msgError = _D_result.s_msgError

            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            # Los valores por default se llenan en configuraCampos_nuevo
            pass

    else:
        pass

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(),
        dbTable = _dbTable,
        dbTableMaster = dbc01.tcfdi_compcartaporte_mercancias,
        xFieldLinkMaster = [_dbTable.cfdi_compcartaporte_mercancia_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, _dbTable.cantidad,
            _dbTable.cfdi_compcartaporte_ubicacion_origen_id,
            _dbTable.cfdi_compcartaporte_ubicacion_destino_id,
            _dbTable.transporte_id,
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        x_offsetInArgs = _D_args.n_offsetArgs - 2
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION,
        _CLS_TABLA.al_validar,
        x_cfdi_compCartaPorte_mercancia = _D_args.s_mercancia_id
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED,
        _CLS_TABLA.AL_ACEPTAR,
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE,
        _CLS_TABLA.ANTES_ELIMINAR,
        dbRow = None  # Es necesario para indicar que se mande en dbRow que se intenta eliminar,
        #  llenado por la clase del FWK
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.AFTER_REMOVE,
        _CLS_TABLA.DESPUES_ELIMINAR,
        )

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView
        )

    generaMensajeFlash(_dbTable, _D_return)

    return _D_returnVars


def almacen_cfdis_traslado_autotransporte():
    """ Formulario de autotransportes de CFDIs de traslados.

    @descripcion Autotransportes

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Almacen
    @keyword arg3: empresa_almacen_id.
    @keyword arg4: CFDI Traslado
    @keyword arg5: cfdi_id y cartaporte_id.
    @keyword [arg6]: operación a realizar en el formulario.
    @keyword [arg7]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-file-code] Importar archivo con mercancías
    """

    # noinspection DuplicatedCode
    _D_return = Storage(
        E_return = CLASS_e_RETURN.OK,
        s_msgError = "",
        dbTable = dbc01.tcfdi_compcartaporte_autotransporte,
        )

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 6,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_almacen_id = request.args(3, 0),
        s_cfdi_id = request.args(5, 0).split("-")[0],
        s_cartaporte_id = request.args(5, 0).split("-")[1],
        )

    _dbTable = _D_return.dbTable
    _D_useView = None
    _CLS_TABLA = TCFDI_COMPCARTAPORTE_AUTOTRANSPORTE

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):
        # Si esta editando o guardando una edición, se configuran los campos del CFDI
        _D_result = _CLS_TABLA.configuracioncampos_edicion()
        _D_return.update(_D_result)

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _D_result = _CLS_TABLA.configuracioncampos_nuevo(
            s_cfdi_compCartaPorte_autotransporte_id = None,
            x_cfdi_compCartaPorte = _D_args.s_cartaporte_id
            )
        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_result)

            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            # Los valores por default se llenan en configuraCampos_nuevo
            pass

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.special_permisson_01.code,
            request.stv_fwk_permissions.special_permisson_02.code,
            ):

        stv_flashInfo(_dbTable._plural, "Traslado botones de timbrado, pendiente de desarrollo")
        _D_useView = request.stv_fwk_permissions.view_refresh

    else:
        pass

    # Principal filtro de CFDIs a mostrar
    _dbQrySearch = (_dbTable.cfdi_compcartaporte_id == _D_args.s_cartaporte_id)

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_dbQrySearch),
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01,
            request.stv_fwk_permissions.special_permisson_02,
            request.stv_fwk_permissions.special_permisson_03,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, _dbTable.emp_tra_permiso_id,
            _dbTable.numpermisosct, _dbTable.nombreaseg, _dbTable.numpolizaseguro,
            _dbTable.idvehic_auto_configautotransporte_id,
            _dbTable.idvehic_placavm, _dbTable.idvehic_aniomodelovm,
            _dbTable.remolque1_auto_configautotransporte_id, _dbTable.remolque1_placa,
            _dbTable.remolque2_auto_configautotransporte_id, _dbTable.remolque2_placa,
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        x_offsetInArgs = _D_args.n_offsetArgs
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION,
        _CLS_TABLA.al_validar,
        x_cfdi_compCartaPorte = _D_args.s_cartaporte_id
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED,
        _CLS_TABLA.AL_ACEPTAR,
        x_cfdi_compCartaPorte = _D_args.s_cartaporte_id
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE,
        _CLS_TABLA.ANTES_ELIMINAR,
        dbRow = None  # Es necesario para indicar que se mande en dbRow que se intenta eliminar,
        #  llenado por la clase del FWK
        )

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView
        )

    if _D_returnVars.rowContent_form and _D_returnVars.rowContent_form.accepted:
        _O_cat.agregar_comando(
            E_comando = STV_FWK_FORM.E_COMANDOS.REFRESCAR_TABS_HERMANOS,
            D_parametros = Storage(
                L_tabNombres = [dbc01.tcfdi_compcartaporte_figurastransporepersonas._plural]
                )
            )
    else:
        pass

    generaMensajeFlash(_dbTable, _D_return)

    return _D_returnVars


def almacen_cfdis_traslado_figuratranspersonas():
    """ Formulario de personas de CFDIs de traslados.

    @descripcion Fig. Auto. Personas

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Almacen
    @keyword arg3: empresa_almacen_id.
    @keyword arg4: CFDI Traslado
    @keyword arg5: cfdi_id y cartaporte_id.
    @keyword [arg6]: operación a realizar en el formulario.
    @keyword [arg7]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-file-code] Importar archivo con mercancías
    """

    # noinspection DuplicatedCode
    _D_return = Storage(
        E_return = CLASS_e_RETURN.OK,
        s_msgError = "",
        dbTable = dbc01.tcfdi_compcartaporte_figurastransporepersonas,
        )

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 6,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_almacen_id = request.args(3, 0),
        s_cfdi_id = request.args(5, 0).split("-")[0],
        s_cartaporte_id = request.args(5, 0).split("-")[1],
        )

    _dbTable = _D_return.dbTable
    _D_useView = None
    _CLS_TABLA = TCFDI_COMPCARTAPORTE_FIGURATRANPORTEPERSONAS

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):
        # Si esta editando o guardando una edición, se configuran los campos del CFDI
        _D_result = _CLS_TABLA.configuracioncampos_edicion()
        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _D_result = _CLS_TABLA.configuracioncampos_nuevo(
            s_cfdi_compCartaPorte_figtranspersona_id = None,
            x_cfdi_compCartaPorte = dbc01.tcfdi_compcartaporte(_D_args.s_cartaporte_id)
            )
        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.E_return = _D_result.E_return
            _D_return.s_msgError = _D_result.s_msgError

            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            # Los valores por default se llenan en configuraCampos_nuevo
            pass

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.special_permisson_01.code,
            request.stv_fwk_permissions.special_permisson_02.code,
            ):

        stv_flashInfo(_dbTable._plural, "Traslado botones de timbrado, pendiente de desarrollo")
        _D_useView = request.stv_fwk_permissions.view_refresh

    else:
        pass

    # Principal filtro de CFDIs a mostrar
    _dbQrySearch = (_dbTable.cfdi_compcartaporte_id == _D_args.s_cartaporte_id)

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_dbQrySearch),
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01,
            request.stv_fwk_permissions.special_permisson_02,
            request.stv_fwk_permissions.special_permisson_03,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, _dbTable.tipofiguratransporte,
            _dbTable.rfc, _dbTable.numlicencia, _dbTable.nombre,
            _dbTable.numregidtriboperador,
            _dbTable.residencia_pais_id, _dbTable.dom_calle, _dbTable.dom_numexterior, _dbTable.dom_numinterior,
            _dbTable.dom_colonia, _dbTable.dom_localidad, _dbTable.dom_referencia, _dbTable.dom_municipio,
            _dbTable.dom_pais_estado_id, _dbTable.dom_pais_id, _dbTable.dom_codigopostal,
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        x_offsetInArgs = _D_args.n_offsetArgs
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION,
        _CLS_TABLA.al_validar,
        x_cfdi_compCartaPorte = _D_args.s_cartaporte_id
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED,
        _CLS_TABLA.AL_ACEPTAR,
        x_cfdi_compCartaPorte = _D_args.s_cartaporte_id
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE,
        _CLS_TABLA.ANTES_ELIMINAR,
        dbRow = None  # Es necesario para indicar que se mande en dbRow que se intenta eliminar,
        #  llenado por la clase del FWK
        )

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView
        )

    generaMensajeFlash(_dbTable, _D_return)

    return _D_returnVars


class ALMACEN_CFDIS_TRASLADOS:

    def _configurar_detalles(
            self,
            O_cat
            ):

        if self._s_cfdi_id:

            O_cat.addSubTab(
                dbTableDetail = dbc01.tcfdi_conceptos,
                s_url = URL(
                    f = 'cfdi_conceptos',
                    args = request.args[:4] + ['master_' + str(request.function), self._s_cfdi_id]
                    ),
                s_idHtml = fn_createIDTabUnique(
                    f = 'cfdi_conceptos',
                    s_masterIdentificator = str(request.function)
                    )
                )

            O_cat.addSubTab(
                dbTableDetail = dbc01.tcfdi_relacionados,
                s_url = URL(
                    f = 'cfdi_relacionados',
                    args = request.args[:4] + ['master_' + str(request.function), self._s_cfdi_id]
                    ),
                s_idHtml = fn_createIDTabUnique(
                    f = 'cfdi_relacionados',
                    s_masterIdentificator = str(request.function)
                    )
                )

            O_cat.addSubTab(
                dbTableDetail = dbc01.tcfdi_manejosaldos,
                s_url = URL(
                    f = 'cfdi_manejosaldos',
                    args = request.args[:4] + ['master_' + str(request.function), self._s_cfdi_id]
                    ),
                s_idHtml = fn_createIDTabUnique(
                    f = 'cfdi_manejosaldos',
                    s_masterIdentificator = str(request.function)
                    )
                )

            #         O_cat.addSubTab(
            #             dbTableDetail = dbc01.tcfdi_complementopagos,
            #             s_url = URL(
            #                 a = 'app_timbrado',
            #                 c = '250cfdi',
            #                 f='cfdi_pagos',
            #                 args=['master_'+str(request.function), self._s_cfdi_id]
            #                 ),
            #             s_idHtml = fn_createIDTabUnique(
            #                 a = 'app_timbrado',
            #                 c = '250cfdi',
            #                 f = 'cfdi_pagos',
            #                 s_masterIdentificator = str(request.function)
            #                 )
            #             )

            O_cat.addSubTab(
                dbTableDetail = dbc02.tcfdi_xmls,
                s_url = URL(
                    a = 'app_timbrado',
                    c = '250cfdi',
                    f = 'cfdi_xml',
                    args = ['master_' + str(request.function), self._s_cfdi_id]
                    ),
                s_idHtml = fn_createIDTabUnique(
                    a = 'app_timbrado',
                    c = '250cfdi',
                    f = 'cfdi_xml',
                    s_masterIdentificator = str(request.function)
                    )
                )

            O_cat.addSubTab(
                dbTableDetail = dbc01.tcfdi_prepolizas,
                s_url = URL(
                    a = 'app_timbrado',
                    c = '255prepolizas_polizas',
                    f = 'prepoliza',
                    args = ['master_' + str(request.function), self._s_cfdi_id]
                    ),
                s_idHtml = fn_createIDTabUnique(
                    a = 'app_timbrado',
                    c = '255prepolizas_polizas',
                    f = 'prepoliza',
                    s_masterIdentificator = str(request.function)
                    )
                )

            O_cat.addSubTab(
                dbTableDetail = dbc01.tcfdi_movimientos,
                s_url = URL(
                    f = 'almacen_cfdi_movimientos',
                    args = request.args[:4] + ['master_' + str(request.function), self._s_cfdi_id]
                    ),
                s_idHtml = fn_createIDTabUnique(
                    f = 'almacen_cfdi_movimientos',
                    s_masterIdentificator = str(request.function)
                    )
                )
        else:
            pass

        # O_cat.addPermissionConfig_option(
        #     request.stv_fwk_permissions.btn_new.code,
        #     s_optionCode = request.stv_fwk_permissions.btn_new.code,
        #     s_optionLabel = "Crear CFDI de Ingreso",
        #     s_optionIcon = '',
        #     s_type = 'option',
        #     s_send = '',
        #     L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_new),
        # Copia las vistas en la que estara habilitada la opción como el caso de Print
        #     D_actions = Storage(
        #         D_data = None,
        #         )
        #     )

        return


def user():
    redirect(
        URL(
            a = D_stvFwkCfg.s_nameAccesoApp,
            c = 'default',
            f = 'user',
            vars = request.vars,
            args = request.args
            )
        )


def almacen_ordenescompra():
    """ Formulario de Órdenes de Compra para Almacén.

    @descripcion Ordenes Compra

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Almacen
    @keyword arg3: empresa_almacen_id.
    @keyword [arg4]: operación a realizar en el formulario.
    @keyword [arg5]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_invalidate
    @permiso btn_validate
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    """

    def validacion(O_form):
        return O_form

    # noinspection DuplicatedCode
    _D_return = Storage(
        E_return = CLASS_e_RETURN.OK,
        s_msgError = "",
        )

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 4,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_almacen_id = request.args(3, 0),
        )

    _dbTable = dbc01.tempresa_almacen_ordenescompra
    _s_overrideCode = None

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):
        # Si esta editando o guardando una edición
        _D_result = TEMPRESA_ALMACEN_ORDENESCOMPRA.CONFIGURA_CAMPOS_EDICION(
            x_empresa_ordencompra = _D_args.n_id,
            )

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            # Si ocurrió algun error
            _D_return.E_return = _D_result.E_return
            _D_return.s_msgError = _D_result.s_msgError

            _s_overrideCode = request.stv_fwk_permissions.btn_cancel.code

        else:
            pass

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _D_result = TEMPRESA_ALMACEN_ORDENESCOMPRA.CONFIGURA_CAMPOS_NUEVO(
            s_almacen_ordenescompra_id = _D_args.n_id
            )

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            # Si ocurrió algun error
            _D_return.E_return = _D_result.E_return
            _D_return.s_msgError = _D_result.s_msgError

            _s_overrideCode = request.stv_fwk_permissions.btn_cancel.code

        else:
            pass

    else:
        pass

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_plaza_sucursal_almacenes,
        xFieldLinkMaster = [_dbTable.empresa_plaza_sucursal_almacen_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.estatus,
            _dbTable.total,
            _dbTable.referencia,
            _dbTable.fecha,
            _dbTable.fecha_requerida,
            _dbTable.descripcion,
            _dbTable.inconsistencias,
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        x_offsetInArgs = _D_args.n_offsetArgs - 2
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION,
        validacion
        )

    _D_returnVars = _O_cat.process(
        s_overrideCode = _s_overrideCode
        )

    # _O_cat.addSubTab(
    #     dbTableDetail = dbc01.tempresa_invmovto_productos,
    #     s_url = URL(
    #         f = 'inventarioinicial_movimientos',
    #         args = ['master_' + str(request.function), request.args(1)]
    #         ),
    #     s_idHtml = fn_createIDTabUnique(
    #         f = 'inventarioinicial_movimientos',
    #         s_masterIdentificator = str(request.function)
    #         )
    #     )

    # if (_s_action == request.stv_fwk_permissions.btn_save.code) and (_D_returnVars.rowContent_form.accepted):
    #
    #     #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
    #     from gluon.storage import List
    #     request.args = List([request.stv_fwk_permissions.btn_view.code, _D_returnVars.rowContent_form.vars.id])
    #     _D_returnVars = movto_entrada_inventarioinicial()
    #
    # else:
    #     pass

    generaMensajeFlash(_dbTable, _D_return)

    return _D_returnVars


def almacen_productos():
    """ Formulario de Órdenes de Compra para Almacén.

    @descripcion Productos

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Almacen
    @keyword arg3: empresa_almacen_id.
    @keyword [arg4]: operación a realizar en el formulario.
    @keyword [arg5]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    #@permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_invalidate
    #@permiso btn_validate
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-file-code] Importar Min, Max, Info. Adicional
    """

    # noinspection DuplicatedCode
    _D_return = Storage(
        E_return = CLASS_e_RETURN.OK,
        s_msgError = "",
        )

    # noinspection DuplicatedCode
    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 4,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_almacen_id = request.args(3, 0),
        )

    _dbTable = dbc01.tempresa_prodserv
    _s_overrideCode = None
    _s_prodserv_id = None

    _qry = (_dbTable.empresa_id == _D_args.s_empresa_id)
    _D_results = TEMPRESA_ALMACEN_PRODUCTOS.BUSQUEDA_AVANZADA.GENERA_PREFILTRO(_D_args)
    _qry &= _D_results.qry

    # _dbRow_prodserv = None  # Usado en caso de editar/ver/guardar
    _dbRow_almacenProducto = None

    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.codigo,
        _dbTable.descripcion,
        dbc01.tempresa_almacen_productos.cantidad,
        dbc01.tempresa_almacen_productos.cantidad_maxima,
        dbc01.tempresa_almacen_productos.cantidad_minima,
        dbc01.tempresa_almacen_productos.infoadicional,
        _dbTable.tiposeguimiento,
        # _dbTable.categoria_id,
        dbc01.tempresa_prodcategorias.nombrecorto,
        dbc01.tempresa_almacen_productos.bansuspender,
        ]
    _L_fieldsToSearch = [
        _dbTable.codigo,
        _dbTable.descripcion,
        dbc01.tempresa_prodcategorias.nombrecorto,
        ]
    _L_fieldsToSearchIfDigit = [
        _dbTable.id,
        _dbTable.codigo,
        ]

    # TODOMejora crear una funcion para uso de la STV_FWK_FORM con ciertas instrucciones para la tabla de prod almacenes
    #  y otra para usar la tabla de prodserv a nivel empresa

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_suspend.code,
            request.stv_fwk_permissions.btn_activate.code,
            request.stv_fwk_permissions.btn_remove.code,
            request.stv_fwk_permissions.btn_view.code
            ):
        # Si presionó el boton de editar, esta editando el registro en el almacén
        # Al precionar editar, el id corresponde a empresa_prodserv, entonces se cambia la configuración del O_cat
        # _dbRow_prodserv = dbc01.tempresa_prodserv(_D_args.n_id)

        _s_prodserv_id = _D_args.n_id

        _dbRows_almacen_productos = dbc01(
            (dbc01.tempresa_almacen_productos.empresa_plaza_sucursal_almacen_id == _D_args.s_almacen_id)
            & (dbc01.tempresa_almacen_productos.empresa_prodserv_id == _s_prodserv_id)
            ).select(
            dbc01.tempresa_almacen_productos.ALL
            )

        if len(_dbRows_almacen_productos) == 0:
            _n_almacenProducto_id = dbc01.tempresa_almacen_productos.insert(
                empresa_plaza_sucursal_almacen_id = _D_args.s_almacen_id,
                empresa_prodserv_id = _s_prodserv_id,
                )
            dbc01.commit()

            _dbRow_almacenProducto = dbc01.tempresa_almacen_productos(_n_almacenProducto_id)

        elif len(_dbRows_almacen_productos) > 1:
            _D_return.E_return = CLASS_e_RETURN.OK_WARNING
            _D_return.s_msgError = (
                "Existen más de un registro en el almacén asociado a este producto, "
                + "llame al administrador"
                )
            # En este error, tiene que unirse todas las referencias al producto al que quede cómo único
            # Hacerlo con cuidado
            _dbRow_almacenProducto = _dbRows_almacen_productos.last()

        else:
            _dbRow_almacenProducto = _dbRows_almacen_productos.last()

        if _D_args.s_accion not in (
                request.stv_fwk_permissions.btn_view.code,
                ):
            # Si esta editando o guardando una edición
            _D_result = TEMPRESA_ALMACEN_PRODUCTOS.CONFIGURA_CAMPOS_EDICION(
                x_empresa_almacen_producto = _dbRow_almacenProducto,
                )

            if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
                # Si ocurrió algun error
                _D_return.E_return = _D_result.E_return
                _D_return.s_msgError = _D_result.s_msgError

                _s_overrideCode = request.stv_fwk_permissions.btn_cancel.code

            else:
                pass
        else:
            pass

        # Se configura la información para que trabaje sobre la tabla del almacén al editar
        _dbTable = dbc01.tempresa_almacen_productos
        request.args[_D_args.n_offsetArgs + 1] = _dbRow_almacenProducto.id
        _L_fieldsToShow = [_dbTable.id]
        _L_fieldsToSearch = []
        _L_fieldsToSearchIfDigit = []

    elif _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_save.code,
            ):
        # En caso de guardar, el id corresponde al almacen_productos, se obtiene el registro del producto para
        # actualizarlo en la vista y se cambia el dbTable para su uso en el O_cat

        _dbRow_almacenProducto = dbc01.tempresa_almacen_productos(_D_args.n_id)
        # _dbRow_prodserv = dbc01.tempresa_prodserv(_dbRow_almacenProducto.empresa_prodserv_id)

        # Si esta editando o guardando una edición
        _D_result = TEMPRESA_ALMACEN_PRODUCTOS.CONFIGURA_CAMPOS_EDICION(
            x_empresa_almacen_producto = _dbRow_almacenProducto,
            )

        # Se configura la información para que trabaje sobre la tabla del almacén al editar
        _dbTable = dbc01.tempresa_almacen_productos
        _L_fieldsToShow = [_dbTable.id]
        _L_fieldsToSearch = []
        _L_fieldsToSearchIfDigit = []

    # NO HAY OPCION DE NUEVO, SI SE DESEA UN NUEVO PRODUCTO DEBE SOLICITARSE A PRINCIPAL
    # elif _D_args.s_accion in (
    #         request.stv_fwk_permissions.btn_new.code,
    #         request.stv_fwk_permissions.btn_save.code,
    #         ):
    #
    #     _D_result = TEMPRESA_ALMACEN_ORDENESCOMPRA.CONFIGURA_CAMPOS_NUEVO(
    #         s_almacen_ordenescompra_id = _D_args.n_id
    #         )
    #
    #     if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
    #         # Si ocurrió algun error
    #         _D_return.E_return = _D_result.E_return
    #         _D_return.s_msgError = _D_result.s_msgError
    #
    #         _s_overrideCode = request.stv_fwk_permissions.btn_cancel.code
    #
    #     else:
    #         pass

    else:
        pass

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        # dbTableMaster = dbc01.tempresa_plaza_sucursal_almacenes,
        # xFieldLinkMaster = [_dbTable.empresa_plaza_sucursal_almacen_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = _L_fieldsToSearch,
        L_fieldsToSearchIfDigit = _L_fieldsToSearchIfDigit,
        x_offsetInArgs = _D_args.n_offsetArgs,
        L_fieldnameIds = [_dbTable.id]
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION,
        TEMPRESA_ALMACEN_PRODUCTOS.al_validar
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED,
        TEMPRESA_ALMACEN_PRODUCTOS.AL_ACEPTAR
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE,
        TEMPRESA_ALMACEN_PRODUCTOS.ANTES_ELIMINAR,
        dbRow = None
        )

    if _D_return.E_return == CLASS_e_RETURN.OK:
        _D_result = TEMPRESA_ALMACEN_PRODUCTOS.BUSQUEDA_AVANZADA.CONFIGURA(
            s_empresa_id = _D_args.s_empresa_id,
            O_cat = _O_cat
            )

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            # Si ocurrió algun error
            _D_return.E_return = _D_result.E_return
            _D_return.s_msgError = _D_result.s_msgError

            _s_overrideCode = request.stv_fwk_permissions.btn_cancel.code

        else:
            pass

    else:
        pass

    if _dbTable == dbc01.tempresa_prodserv:

        _O_cat.addRowSearchResultsLeftJoin(
            dbc01.tempresa_almacen_productos.on(
                (dbc01.tempresa_almacen_productos.empresa_plaza_sucursal_almacen_id == _D_args.s_almacen_id)
                & (dbc01.tempresa_almacen_productos.empresa_prodserv_id == _dbTable.id)
                )
            )

        _O_cat.addRowSearchResultsLeftJoin(
            dbc01.tempresa_prodcategorias.on(dbc01.tempresa_prodcategorias.id == _dbTable.categoria_id)
            )

        _O_cat.addRowSearchResultsConfig(
            STV_FWK_FORM.ROWSEARCHRESULTS.E_CONFIG.MULTISELECT, True
            )
    else:
        pass

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_type = "button",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_new),
        D_actions = Storage(
            D_data = Storage(),
            modal = Storage(
                s_url = URL(
                    f = 'importar_excel',
                    args = _D_args.L_argsBasicos,  # Only the first 4 args should be here
                    vars = Storage(master = request.function)
                    ),
                s_field = "",
                )
            )
        )

    _D_returnVars = _O_cat.process(
        s_overrideCode = _s_overrideCode
        )

    _D_returnVars.dbRow_prodserv = dbc01.tempresa_prodserv(_s_prodserv_id) if _s_prodserv_id else None

    # Se cambia la configuración del campo usado para supender y activar, usando el producto en el almacén
    _D_returnVars.D_permissionsConfig[request.stv_fwk_permissions.btn_suspend.code].D_actions.swap.s_field = str(
        dbc01.tempresa_almacen_productos.bansuspender
        )
    _D_returnVars.D_permissionsConfig[request.stv_fwk_permissions.btn_activate.code].D_actions.swap.s_field = str(
        dbc01.tempresa_almacen_productos.bansuspender
        )

    generaMensajeFlash(_dbTable, _D_return)

    # Se desabilita poder editar o borrar desde la vista de ver, para evitar conflictos con el uso de los ids
    #  de prodserv y almacen_prodserv
    _L_enabledByView_paraDesactivar = request.stv_fwk_permissions.getViewsByPermission(
        request.stv_fwk_permissions.btn_suspend
        )
    _D_returnVars.D_permissionsConfig[
        request.stv_fwk_permissions.btn_edit.code].L_enabledByView = _L_enabledByView_paraDesactivar
    _D_returnVars.D_permissionsConfig[
        request.stv_fwk_permissions.btn_remove.code].L_enabledByView = _L_enabledByView_paraDesactivar

    return _D_returnVars


def importar_excel():
    """ Modal para importar excel usando la clase especificada

    @descripcion Importar Excel

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Almacen
    @keyword arg3: empresa_almacen_id.
    @keyword [arg4]: operación a realizar en el formulario.
    @keyword [arg5]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    #@permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    #@permiso btn_view
    #@permiso btn_invalidate
    #@permiso btn_validate
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    #@permiso btn_find
    #@permiso btn_findall
    #@permiso btn_signature
    """

    def validacion(O_form, CLS_TABLA, D_vars_request):
        """ Validación
        """

        if isinstance(request.vars.archivo_excel, str):
            O_form.errors.archivo_excel = "Favor de agregar el archivo"
        else:
            from shutil import copyfile
            import tempfile
            _s_nombreArchivo = os.path.basename(request.vars.archivo_excel.filename.replace("\\", "/"))
            _s_pathArchivoTemporal = os.path.join(tempfile.gettempdir(), _s_nombreArchivo)
            copyfile(request.vars.archivo_excel.fp.name, _s_pathArchivoTemporal)

            if CLS_TABLA:

                _D_result = CLS_TABLA.IMPORTAR_EXCEL(_s_pathArchivoTemporal, request.vars.nombre, **D_vars_request)
            else:
                _D_result = Storage(
                    E_return = stvfwk2_e_RETURN.NOK_ERROR,
                    s_msgError = "Tabla requerida para importación no reconocida"
                    )

            if _D_result.E_return > stvfwk2_e_RETURN.OK_WARNING:
                O_form.errors.id = _D_result.s_msgError
            elif _D_result.s_msgError:
                stv_flashSuccess(
                    'Resultado Importación',
                    _D_result.s_msgError,
                    s_mode = 'stv_flash',
                    D_addProperties = {}
                    )
            else:
                pass

        return O_form

    # noinspection DuplicatedCode
    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 4,
        s_codigoDefault = request.stv_fwk_permissions.btn_new.code,
        s_empresa_id = request.args(1, 0),
        s_almacen_id = request.args(3, 0),
        )

    if _D_args.s_accion == request.stv_fwk_permissions.special_permisson_01.code:
        _s_action = request.stv_fwk_permissions.btn_none.code
    else:
        _s_action = _D_args.s_accion

    # noinspection DuplicatedCode
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbTable = None,
        L_visibleButtons = [request.stv_fwk_permissions.btn_none, request.stv_fwk_permissions.btn_save],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        b_applyPermissons = False,
        x_offsetInArgs = _D_args.n_offsetArgs
        )

    # Código para hace bypass de variables entre requests
    _D_vars_request = _O_cat.bypassVars(
        s_nombreVar = "vars_request",
        s_almacen_id = _D_args.s_almacen_id
        )

    if _D_vars_request.get('master', None) == 'almacen_productos':
        _CLS_TABLA = TEMPRESA_ALMACEN_PRODUCTOS
    elif _D_vars_request.get('master', None) == 'inventarioinicial_productos':
        _CLS_TABLA = TEMPRESA_INVMOVTO_PRODUCTOS
    elif _D_vars_request.get('master', None) == 'cfdi_conceptos':
        _CLS_TABLA = TCFDI_CONCEPTOS.TRASLADO
    else:
        _CLS_TABLA = None
    _s_mensaje = ""
    _n_rows = 2
    if _CLS_TABLA:
        _s_mensaje = _CLS_TABLA.IMPORTAR_EXCEL.__doc__.split("\n\n")[0]
        _L_mensaje = map(str.strip, (' '.join(_s_mensaje.split())).split('.'))
        _n_rows = len(_L_mensaje) + 2
        _s_mensaje = '.\n'.join(_L_mensaje)
    else:
        _s_mensaje = "ERROR: NO SE ENCONTRÓ CLASE PARA IMPORTAR. LLAME AL ADMINISTRADOR %d" % _D_vars_request.get(
            'master', None
            )

    _L_formFactoryFields = [
        Field(
            'comentarios', 'text', default = _s_mensaje,
            required = False,
            notnull = False, unique = False,
            widget = lambda v, r: stv_widget_text(v, r, D_additionalAttributes = {'n_rows': _n_rows}),
            label = 'Comentarios',
            comment = None,
            writable = False, readable = True,
            represent = stv_represent_text
            ),
        Field(
            'nombre', 'string', length = 100, default = None,
            required = False,
            notnull = False,
            widget = stv_widget_input, label = 'Nombre hoja excel', comment = '',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'archivo_excel', type = 'upload',
            required = True,
            requires = IS_LENGTH(minsize = 0, maxsize = 4194304, error_message = 'El archivo es demasiado grande.'),
            notnull = False,
            uploadfield = True, widget = lambda f, d, u = None: stv_widget_inputFile(
                f, d,
                s_url = None, x_linkName = None,
                D_additionalAttributes = Storage(notExternalLink = True, notDownload = True, notRemove = True)
                ),
            label = 'Archivo Excel', comment = None,
            writable = True, readable = True,
            autodelete = True,
            uploadfolder = os.path.join(request.folder, 'uploads', 'archivos_excel'),
            uploadseparate = False, uploadfs = None
            ),
        ]

    _O_cat.updateTabOption(
        "s_modalTitle",
        "Importar archivo"
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION,
        validacion,
        CLS_TABLA = _CLS_TABLA,
        D_vars_request = _D_vars_request
        )

    _D_returnVars = _O_cat.process(
        s_overrideTitle = "Importar archivo",
        s_overrideTitleSingular = "Importar archivo",
        s_overrideCode = _s_action,
        L_formFactoryFields = _L_formFactoryFields,
        b_skipAction = True,
        )

    if _D_returnVars.D_tabOptions.b_accepted:
        _D_useView = request.stv_fwk_permissions.view_refreshParent
        _D_returnVars.D_useView = _D_useView

    else:
        _D_returnVars.D_useView.b_serverCreatesTab = True
        _D_returnVars.D_useView.L_actionsCreateView = [request.stv_fwk_permissions.btn_none.code]

    return _D_returnVars


def importar_cfdi():
    """ Modal para importar cfdis

    @descripcion Importar CFDI Modal

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    #@permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    #@permiso btn_view
    #@permiso btn_invalidate
    #@permiso btn_validate
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    #@permiso btn_find
    #@permiso btn_findall
    #@permiso btn_signature
    """

    def validacion(O_form, CLS_TABLA, D_vars_request):
        """ Validación
        """

        _ = CLS_TABLA
        _ = D_vars_request
        if isinstance(request.vars.archivo_cfdi, str):
            O_form.errors.archivo_cfdi = "Favor de agregar el archivo"
        else:

            O_form.errors.id = "En proceso de desarrollo"

            # from shutil import copyfile
            # import tempfile
            # _s_nombreArchivo = os.path.basename(request.vars.archivo_cfdi.filename.replace("\\", "/"))
            # _s_pathArchivoTemporal = os.path.join(tempfile.gettempdir(), _s_nombreArchivo)
            # copyfile(request.vars.archivo_cfdi.fp.name, _s_pathArchivoTemporal)

            # if CLS_TABLA:
            #
            #     _D_result = CLS_TABLA.IMPORTAR_EXCEL(
            #         _s_pathArchivoTemporal,
            #         request.vars.nombre,
            #         **D_vars_request
            #         )
            # else:
            #     _D_result = Storage(
            #         E_return = stvfwk2_e_RETURN.NOK_ERROR,
            #         s_msgError = "Tabla requerida para importación no reconocida"
            #         )

            # if _D_result.E_return > stvfwk2_e_RETURN.OK_WARNING:
            #     O_form.errors.id = _D_result.s_msgError
            # elif _D_result.s_msgError:
            #     stv_flashSuccess(
            #         'Resultado Importación',
            #         _D_result.s_msgError,
            #         s_mode = 'stv_flash',
            #         D_addProperties = {}
            #         )
            # else:
            #     pass

        return O_form

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 4,
        s_codigoDefault = request.stv_fwk_permissions.btn_new.code,
        s_empresa_id = request.args(1, 0),
        s_almacen_id = request.args(3, 0),
        )

    if _D_args.s_accion == request.stv_fwk_permissions.special_permisson_01.code:
        _s_action = request.stv_fwk_permissions.btn_none.code
    else:
        _s_action = _D_args.s_accion

    # noinspection DuplicatedCode
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbTable = None,
        L_visibleButtons = [request.stv_fwk_permissions.btn_none, request.stv_fwk_permissions.btn_save],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        b_applyPermissons = False,
        x_offsetInArgs = _D_args.n_offsetArgs
        )

    # Código para hace bypass de variables entre requests
    _D_vars_request = _O_cat.bypassVars(
        s_nombreVar = "vars_request",
        s_almacen_id = _D_args.s_almacen_id
        )

    if _D_vars_request.get('master', None) == 'porcompra':
        _CLS_TABLA = TEMPRESA_INVENTARIOMOVIMIENTOS.PorCompra
    else:
        _CLS_TABLA = None

    _s_mensaje = ""
    _n_rows = 2
    if _CLS_TABLA:
        _s_mensaje = _CLS_TABLA.IMPORTAR.__doc__.split("\n\n")[0]
        _L_mensaje = map(str.strip, (' '.join(_s_mensaje.split())).split('.'))
        _n_rows = len(_L_mensaje) + 2
        _s_mensaje = '.\n'.join(_L_mensaje)
    else:
        _s_mensaje = "ERROR: NO SE ENCONTRÓ CLASE PARA IMPORTAR. LLAME AL ADMINISTRADOR %d" % _D_vars_request.get(
            'master', None
            )

    _L_formFactoryFields = [
        Field(
            'comentarios', 'text', default = _s_mensaje,
            required = False,
            notnull = False, unique = False,
            widget = lambda v, r: stv_widget_text(v, r, D_additionalAttributes = {'n_rows': _n_rows}),
            label = 'Comentarios',
            comment = None,
            writable = False, readable = True,
            represent = stv_represent_text
            ),
        Field(
            'nombre', 'string', length = 100, default = None,
            required = False,
            notnull = False,
            widget = stv_widget_input, label = 'Descripción', comment = '',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'archivo_cfdi', type = 'upload',
            required = True,
            requires = IS_LENGTH(minsize = 0, maxsize = 4194304, error_message = 'El archivo es demasiado grande.'),
            notnull = False,
            uploadfield = True, widget = lambda f, d, u = None: stv_widget_inputFile(
                f, d,
                s_url = None, x_linkName = None,
                D_additionalAttributes = Storage(notExternalLink = True, notDownload = True, notRemove = True)
                ),
            label = 'Archivo CFDI', comment = None,
            writable = True, readable = True,
            autodelete = True,
            uploadfolder = os.path.join(request.folder, 'uploads', 'archivos_cfdi'),
            uploadseparate = False, uploadfs = None
            ),
        ]

    _O_cat.updateTabOption(
        "s_modalTitle",
        "Importar archivo"
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION,
        validacion,
        CLS_TABLA = _CLS_TABLA,
        D_vars_request = _D_vars_request
        )

    _D_returnVars = _O_cat.process(
        s_overrideTitle = "Importar archivo",
        s_overrideTitleSingular = "Importar archivo",
        s_overrideCode = _s_action,
        L_formFactoryFields = _L_formFactoryFields,
        b_skipAction = True,
        )

    if _D_returnVars.D_tabOptions.b_accepted:
        _D_useView = request.stv_fwk_permissions.view_refreshParent
        _D_returnVars.D_useView = _D_useView

    else:
        _D_returnVars.D_useView.b_serverCreatesTab = True
        _D_returnVars.D_useView.L_actionsCreateView = [request.stv_fwk_permissions.btn_none.code]

    return _D_returnVars


def empresa_almacen_log():
    """ Formulario para el manejo de los logs de almacenes

    @descripcion Log

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Almacen
    @keyword arg3: empresa_almacen_id.
    @keyword [arg4]: operación a realizar en el formulario.
    @keyword [arg5]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    #@permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    #@permiso btn_signature

    """

    _dbTable = dbc01.tempresa_almacen_log

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_plaza_sucursal_almacenes,
        xFieldLinkMaster = [_dbTable.emp_pla_suc_almacen_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.fecha,
            _dbTable.descripcion,
            _dbTable.tipotransaccion,
            ],
        L_fieldsToSearch = [
            _dbTable.id,
            _dbTable.descripcion,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:2]
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def operadores():
    """ Formulario de operadores.

    @descripcion Operadores

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    """

    # noinspection DuplicatedCode
    _D_return = Storage(
        E_return = CLASS_e_RETURN.OK,
        s_msgError = "",
        dbTable = dbc01.tempresa_operadores,
        )

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 2,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        )

    _dbTable = _D_return.dbTable
    _D_useView = None
    _CLS_TABLA = TEMPRESA_OPERADORES

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):
        # Si esta editando o guardando una edición, se configuran los campos del CFDI
        _D_result = _CLS_TABLA.configuracioncampos_edicion()
        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _D_result = _CLS_TABLA.configuracioncampos_nuevo(
            s_operador_base_id = _D_args.n_id,
            s_empresa_id = _D_args.s_empresa_id
            )
        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.E_return = _D_result.E_return
            _D_return.s_msgError = _D_result.s_msgError

            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            # Los valores por default se llenan en configuraCampos_nuevo
            pass

    # elif _D_args.s_accion in (
    #         request.stv_fwk_permissions.special_permisson_01.code,
    #         request.stv_fwk_permissions.special_permisson_02.code,
    #         ):
    #
    #     stv_flashInfo(_dbTable._plural, "Traslado botones de timbrado, pendiente de desarrollo")
    #     _D_useView = request.stv_fwk_permissions.view_refresh

    else:
        pass

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas,
        xFieldLinkMaster = [_dbTable.empresa_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            # request.stv_fwk_permissions.special_permisson_01,
            # request.stv_fwk_permissions.special_permisson_02,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.nombrecorto, _dbTable.rfc, _dbTable.numlicencia,
            _dbTable.tipolicencia, _dbTable.fecha_finvigencia, _dbTable.nombre,
            _dbTable.residencia_pais_id, _dbTable.dom_calle, _dbTable.dom_colonia,
            _dbTable.dom_referencia, _dbTable.dom_municipio, _dbTable.dom_pais_estado_id,
            _dbTable.dom_pais_id, _dbTable.usuario_id,
            _dbTable.bansuspender,
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        x_offsetInArgs = _D_args.n_offsetArgs - 2
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION,
        _CLS_TABLA.al_validar
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED,
        _CLS_TABLA.AL_ACEPTAR
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE,
        _CLS_TABLA.ANTES_ELIMINAR,
        dbRow = None  # Es necesario para indicar que se mande en dbRow que se intenta eliminar,
        #  llenado por la clase del FWK
        )

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView
        )

    generaMensajeFlash(_dbTable, _D_return)

    # _O_cat.addPermissionConfig(
    #     s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
    #     s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
    #     s_type = "button",
    #     L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_edit),
    #     D_actions = Storage(
    #         D_data = Storage(),
    #         modal = Storage(
    #             s_url = URL(
    #                 a = 'app_timbrado',
    #                 c = '131almacenes_cfdis',
    #                 f = 'importar_excel',
    #                 args = _D_args.L_argsBasicos[:4],
    #                 vars = Storage(
    #                     master = request.function,
    #                     s_almacen_id = _D_args.s_almacen_id,
    #                     )
    #                 ),
    #             s_field = "",
    #             )
    #         )
    #     )

    return _D_returnVars


def transportes():
    """ Formulario de transportes.

    @descripcion Transportes

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    """

    # noinspection DuplicatedCode
    _D_return = Storage(
        E_return = CLASS_e_RETURN.OK,
        s_msgError = "",
        dbTable = dbc01.tempresa_transportes,
        )

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 2,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        )

    _dbTable = _D_return.dbTable
    _D_useView = None
    _CLS_TABLA = TEMPRESA_TRANSPORTES

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):
        # Si esta editando o guardando una edición, se configuran los campos del CFDI
        _D_result = _CLS_TABLA.configuracioncampos_edicion()
        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _D_result = _CLS_TABLA.configuracioncampos_nuevo(
            s_transporte_base_id = _D_args.n_id,
            s_empresa_id = _D_args.s_empresa_id
            )
        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.E_return = _D_result.E_return
            _D_return.s_msgError = _D_result.s_msgError

            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            # Los valores por default se llenan en configuraCampos_nuevo
            pass

    # elif _D_args.s_accion in (
    #         request.stv_fwk_permissions.special_permisson_01.code,
    #         request.stv_fwk_permissions.special_permisson_02.code,
    #         ):
    #
    #     stv_flashInfo(_dbTable._plural, "Traslado botones de timbrado, pendiente de desarrollo")
    #     _D_useView = request.stv_fwk_permissions.view_refresh

    else:
        pass

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas,
        xFieldLinkMaster = [_dbTable.empresa_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            # request.stv_fwk_permissions.special_permisson_01,
            # request.stv_fwk_permissions.special_permisson_02,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.nombrecorto, _dbTable.pertenencia, _dbTable.idvehic_auto_configautotransporte_id,
            _dbTable.idvehic_placavm, _dbTable.idvehic_aniomodelovm,
            _dbTable.numtarjetacirculacion, _dbTable.numtarjetacirculacion_vigencia,
            _dbTable.marca_id, _dbTable.empresa_proveedor_id,
            _dbTable.nombreaseg, _dbTable.numpolizaseguro, _dbTable.numpolizaseguro_vigencia,
            _dbTable.bansuspender,
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        x_offsetInArgs = _D_args.n_offsetArgs - 2
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION,
        _CLS_TABLA.al_validar
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED,
        _CLS_TABLA.AL_ACEPTAR
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE,
        _CLS_TABLA.ANTES_ELIMINAR,
        dbRow = None  # Es necesario para indicar que se mande en dbRow que se intenta eliminar,
        #  llenado por la clase del FWK
        )

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView
        )

    generaMensajeFlash(_dbTable, _D_return)

    _D_returnVars['htmlid_arrendador'] = _O_cat.addSubTab(
        dbTableDetail = None,
        s_url = False,
        s_tabName = "Arrendador",
        s_idHtml = None,
        s_type = "extended"
        )

    _D_returnVars['htmlid_propietario'] = _O_cat.addSubTab(
        dbTableDetail = None,
        s_url = False,
        s_tabName = "Propietario",
        s_idHtml = None,
        s_type = "extended"
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.temp_tra_permisos,
        s_url = URL(
            a = 'app_timbrado', c = '131almacenes_cfdis',
            f = 'transporte_permisos',
            args = request.args[:2] + ['master_' + str(request.function), request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado', c = '131almacenes_cfdis',
            f = 'transporte_permisos',
            s_masterIdentificator = str(request.function)
            )
        )

    return _D_returnVars


def transporte_permisos():
    """ Formulario de permisos de transportes.

    @descripcion Permiso

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Transporte
    @keyword arg3: transporte_id.
    @keyword [arg4]: operación a realizar en el formulario.
    @keyword [arg5]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    """

    # noinspection DuplicatedCode
    _D_return = Storage(
        E_return = CLASS_e_RETURN.OK,
        s_msgError = "",
        dbTable = dbc01.temp_tra_permisos,
        )

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 4,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_transporte_id = request.args(3, 0),
        )

    _dbTable = _D_return.dbTable
    _D_useView = None
    _CLS_TABLA = TEMP_TRA_PERMISOS

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):
        # Si esta editando o guardando una edición, se configuran los campos del CFDI
        _D_result = _CLS_TABLA.configuracioncampos_edicion()
        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _D_result = _CLS_TABLA.configuracioncampos_nuevo(
            s_permiso_base_id = _D_args.n_id,
            x_transporte = _D_args.s_transporte_id
            )
        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.E_return = _D_result.E_return
            _D_return.s_msgError = _D_result.s_msgError

            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            # Los valores por default se llenan en configuraCampos_nuevo
            pass

    else:
        pass

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_transportes,
        xFieldLinkMaster = [_dbTable.emp_transporte_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            # request.stv_fwk_permissions.special_permisson_01,
            # request.stv_fwk_permissions.special_permisson_02,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.descripcion, _dbTable.transporte_tipopermiso_id, _dbTable.numpermisosct,
            _dbTable.numpermisosct_vigencia,
            _dbTable.bansuspender,
            ],
        L_fieldsToSearch = [
            _dbTable.id,
            _dbTable.descripcion,
            _dbTable.numpermisosct
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            _dbTable.numpermisosct
            ],
        x_offsetInArgs = _D_args.n_offsetArgs - 2
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION,
        _CLS_TABLA.al_validar
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED,
        _CLS_TABLA.AL_ACEPTAR
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE,
        _CLS_TABLA.ANTES_ELIMINAR,
        dbRow = None  # El FWK agrega el row que esta por eliminar
        )

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView
        )

    generaMensajeFlash(_dbTable, _D_return)

    return _D_returnVars
