# -*- coding: utf-8 -*-

def poliza_porempresa_cerradas():
    """ Relación de CFDIs con pólizas cerradas por empresa
    
    Args:
        [arg0]: operación a realizar en el formulario
        [arg1]: id en caso de existir relación a un registro en la operación
    """

    _dbTable = dbc01.tempresas

    # Query para filtrar las empresas que tengan pólizas pendientes por exportar
    _qry = (
            (dbc01.tpolizas.estatus == TPOLIZAS.ESTATUS.SIN_EXPORTAR)
            & (_dbTable.id == dbc01.tpolizas.empresa_id)
    )
    # Se cuenta el número de pólizas que están sin exportar para mostrarse
    _dbField_calculado = dbc01.tpolizas.id.count()

    _L_fieldsToShow = [
        _dbTable.id,
        dbc01.tpolizas.empresa_id,
        _dbTable.razonsocial,
        _dbField_calculado.with_alias('Total_pólizas')
        ]

    # Se agrupa por el ID de la empresa.
    _L_GroupBy = [
        _dbTable.id,
        dbc01.tpolizas.empresa_id,
        _dbTable.razonsocial,
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_findall,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToGroupBy = _L_GroupBy,
        L_fieldsToSearch = [
            _dbTable.id,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        L_fieldnameIds = [
            _dbTable.id
            ]
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_plaza_sucursal_cajaschicas,
        s_url = URL(f = 'poliza_porcajachica_cerradas', args = ['master_' + str(request.function), request.args(1)]),
        s_idHtml = fn_createIDTabUnique(
            f = 'poliza_porcajachica_cerradas', s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_erroresexportacioncontpaqi,
        s_url = URL(
            f = 'poliza_erroresexportacioncontpaqi', args = ['master_' + str(request.function),
                                                             request.args(1)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'poliza_erroresexportacioncontpaqi', s_masterIdentificator = str(request.function)
            )
        )

    _D_returnVars = _O_cat.process()

    return (_D_returnVars)


def poliza_porcajachica_cerradas():
    """ Relación de CFDIs con pólizas cerradas por caja chica
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id    
        [arg2]: operación a realizar en el formulario
        [arg3]: id en caso de existir relación a un registro en la operación; el id se compone de 4 partes
            id de la caja chica
            año
            mes
            día
            
    """

    _dbTable = dbc01.tempresa_plaza_sucursal_cajaschicas

    _s_empresa_id = request.args(1, None)
    _s_action = request.args(2, request.stv_fwk_permissions.btn_none.code)
    _s_multi_id = request.args(3, '0-0-0-0')  # Se ponen 4 elementos separados '-' por default
    _L_multi_id = _s_multi_id.split('-')
    if _L_multi_id[0] == 'None':
        _s_cajachica_id = None
    else:
        _s_cajachica_id = _L_multi_id[0]

    _qry = (
            (dbc01.tpolizas.estatus == TPOLIZAS.ESTATUS.SIN_EXPORTAR)
            & (dbc01.tpolizas.empresa_id == _s_empresa_id)
    )

    _dbField_calculado = dbc01.tpolizas.id.count()

    # Se configura el campo como debe de presentarte sus subcampos calculados
    dbc01.tpolizas.dia.readable = True
    dbc01.tempresa_plaza_sucursal_cajaschicas.empresa_plaza_sucursal_id.readable = True

    _L_fieldsToShow = [
        dbc01.tempresa_plaza_sucursal_cajaschicas.id,
        dbc01.tpolizas.dia,
        dbc01.tempresa_plaza_sucursal_cajaschicas.empresa_plaza_sucursal_id,
        dbc01.tempresa_plaza_sucursal_cajaschicas.nombre,
        _dbField_calculado.with_alias('Total_pólizas')
        ]

    _L_GroupBy = [
        dbc01.tempresa_plaza_sucursal_cajaschicas.id,
        dbc01.tpolizas.dia,
        dbc01.tempresa_plaza_sucursal_cajaschicas.empresa_plaza_sucursal_id,
        dbc01.tempresa_plaza_sucursal_cajaschicas.nombre
        ]

    _L_OrderBy = [
        dbc01.tpolizas.dia,
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_findall,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToGroupBy = _L_GroupBy,
        L_fieldsToSearch = [
            _dbTable.id,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        L_fieldnameIds = [
            _dbTable.id, dbc01.tpolizas.dia
            ],
        x_offsetInArgs = request.args[:2]
        )

    _O_cat.addRowSearchResultsLeftJoin(
        _dbTable.on(
            (_dbTable.id == dbc01.tpolizas.empresa_plaza_sucursal_cajachica_id)
            )
        )

    _O_cat.addRowSearchResultsProperty('orderBy', _L_OrderBy)

    _O_cat.addSubTab(
        s_tabName = "Pólizas por exportar",
        s_url = URL(
            f = 'polizas_cerradas', args = request.args[:2] + ['master_' + str(request.function),
                                                               request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(f = 'polizas_cerradas', s_masterIdentificator = str(request.function))
        )

    if (_s_action in (request.stv_fwk_permissions.btn_view.code)):

        _dbField_fecha = Field(
            'fecha',
            'date',
            default = None,
            required = False,
            notnull = False,
            widget = stv_widget_inputDate,
            label = ('Fecha'),
            comment = '',
            writable = False,
            readable = True,
            represent = stv_represent_date
            )

        _dbRow = _dbTable(_s_cajachica_id)
        if _dbRow:
            pass
        else:
            _dbRow = Storage(
                id = None,
                nombre = "No definida"
                )
        _dbRow.fecha = datetime.datetime(int(_L_multi_id[1]), int(_L_multi_id[2]), int(_L_multi_id[3]))

        _D_returnVars = _O_cat.process(
            b_skipAction = True,
            L_formFactoryFields = [_dbTable.id, _dbTable.nombre, _dbField_fecha,
                                   _dbTable.empresa_plaza_sucursal_id],  # TODO se debe incluir el campo de fecha
            dbRow_formFactory = _dbRow  # TODO el row debe contener los datos en L_formFactoryFields
            )
    else:
        _D_returnVars = _O_cat.process()

    _D_returnVars

    return (_D_returnVars)


def poliza_porempresa_exportadas():
    """ Relación de pólizas exportadas por empresa
    
    Args:
        [arg0]: operación a realizar en el formulario
        [arg1]: id en caso de existir relación a un registro en la operación
    
    """

    _dbTable = dbc01.tempresas

    # Query para filtrar las empresas que tengan pólizas pendientes por exportar
    _qry = (
            (dbc01.tpolizas.estatus == TPOLIZAS.ESTATUS.EXPORTADO)
            & (
                (_dbTable.id == dbc01.tpolizas.empresa_id)
            )
    )
    # Se cuenta el número de pólizas que están sin exportar para mostrarse
    _dbField_calculado = dbc01.tpolizas.id.count()

    _L_fieldsToShow = [
        _dbTable.id,
        dbc01.tpolizas.empresa_id,
        _dbTable.razonsocial,
        _dbField_calculado.with_alias('Total_pólizas')
        ]

    # Se agrupa por el ID de la empresa.
    _L_GroupBy = [
        _dbTable.id,
        dbc01.tpolizas.empresa_id,
        _dbTable.razonsocial,
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_findall,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToGroupBy = _L_GroupBy,
        L_fieldsToSearch = [
            _dbTable.id,
            ],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        L_fieldnameIds = [_dbTable.id]
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_plaza_sucursal_cajaschicas,
        s_url = URL(f = 'poliza_porcajachica_exportadas', args = ['master_' + str(request.function), request.args(1)]),
        s_idHtml = fn_createIDTabUnique(
            f = 'poliza_porcajachica_exportadas', s_masterIdentificator = str(request.function)
            )
        )

    _D_returnVars = _O_cat.process()

    return (_D_returnVars)


def poliza_porcajachica_exportadas():
    """ Relación de pólizas exportadas por caja chica
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id    
        [arg2]: operación a realizar en el formulario
        [arg3]: id en caso de existir relación a un registro en la operación; el id se compone de 4 partes
            id de la caja chica
            año
            mes
            día
            
    """

    _dbTable = dbc01.tempresa_plaza_sucursal_cajaschicas

    _s_empresa_id = request.args(1, None)
    _s_action = request.args(2, request.stv_fwk_permissions.btn_none.code)
    _s_multi_id = request.args(3, '0-0-0-0')  # Se ponen 4 elementos separados '-' por default
    _L_multi_id = _s_multi_id.split('-')
    if _L_multi_id[0] == 'None':
        _s_cajachica_id = None
    else:
        _s_cajachica_id = _L_multi_id[0]

    _qry = (
            (dbc01.tpolizas.estatus == TPOLIZAS.ESTATUS.EXPORTADO)
            & (
                (dbc01.tempresas.id == _s_empresa_id)
            )
    )

    _dbField_calculado = dbc01.tpolizas.id.count()

    # Se configura el campo como debe de presentarte sus subcampos calculados
    dbc01.tpolizas.dia.readable = True
    dbc01.tempresa_plaza_sucursal_cajaschicas.empresa_plaza_sucursal_id.readable = True
    _L_fieldsToShow = [
        dbc01.tempresa_plaza_sucursal_cajaschicas.id,
        dbc01.tpolizas.dia,
        dbc01.tempresa_plaza_sucursal_cajaschicas.empresa_plaza_sucursal_id,
        dbc01.tempresa_plaza_sucursal_cajaschicas.nombre,
        _dbField_calculado.with_alias('Total_pólizas')
        ]

    _L_GroupBy = [
        dbc01.tempresa_plaza_sucursal_cajaschicas.id,
        dbc01.tpolizas.dia,
        dbc01.tempresa_plaza_sucursal_cajaschicas.empresa_plaza_sucursal_id,
        dbc01.tempresa_plaza_sucursal_cajaschicas.nombre
        ]

    _L_OrderBy = [
        dbc01.tpolizas.dia,
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_findall,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToGroupBy = _L_GroupBy,
        L_fieldsToSearch = [
            _dbTable.id,
            ],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        L_fieldnameIds = [_dbTable.id, dbc01.tpolizas.dia],
        x_offsetInArgs = request.args[:2]
        )

    _O_cat.addRowSearchResultsLeftJoin(
        _dbTable.on(
            (_dbTable.id == dbc01.tpolizas.empresa_plaza_sucursal_cajachica_id)
            )
        )

    _O_cat.addRowSearchResultsProperty('orderBy', _L_OrderBy)

    _O_cat.addSubTab(
        s_tabName = "Pólizas exportadas",
        s_url = URL(
            f = 'polizas_exportadas', args = request.args[:2] + ['master_' + str(request.function),
                                                                 request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(f = 'polizas_exportadas', s_masterIdentificator = str(request.function))
        )

    if (_s_action in (request.stv_fwk_permissions.btn_view.code)):

        _dbField_fecha = Field(
            'fecha',
            'date',
            default = None,
            required = False,
            notnull = False,
            widget = stv_widget_inputDate,
            label = ('Fecha'),
            comment = '',
            writable = False,
            readable = True,
            represent = stv_represent_date
            )

        _dbRow = _dbTable(_s_cajachica_id)
        if _dbRow:
            pass
        else:
            _dbRow = Storage(
                id = None,
                nombre = "No definida"
                )
        _dbRow.fecha = datetime.datetime(int(_L_multi_id[1]), int(_L_multi_id[2]), int(_L_multi_id[3]))

        _D_returnVars = _O_cat.process(
            b_skipAction = True,
            L_formFactoryFields = [_dbTable.id, _dbTable.nombre, _dbField_fecha,
                                   _dbTable.empresa_plaza_sucursal_id],  # TODO se debe incluir el campo de fecha
            dbRow_formFactory = _dbRow  # TODO el row debe contener los datos en L_formFactoryFields
            )
    else:
        _D_returnVars = _O_cat.process()

    _D_returnVars

    return (_D_returnVars)


def poliza_ingresos_abiertas():
    """ Relación de pólizas de tipo ingreso abiertas
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id    
        arg2: master_[nombre función maestro]
        arg3: id se compone de 4 partes
            id de la caja chica
            año
            mes
            día
        [arg4]: operación a realizar en el formulario
        [arg5]: id en caso de existir relación a un registro en la operación
    
    """

    _dbTable = dbc01.tpolizas

    _s_empresa_id = request.args(1, None)
    _s_multi_id = request.args(3, '0-0-0-0')  # Se ponen 4 elementos separados '-' por default
    _L_multi_id = _s_multi_id.split('-')
    if _L_multi_id[0] == 'None':
        _s_cajachica_id = None
    else:
        _s_cajachica_id = _L_multi_id[0]

    _s_action = request.args(4, request.stv_fwk_permissions.btn_none.code)

    # Botón especial 1
    if _s_action in (request.stv_fwk_permissions.special_permisson_01.code,):

        _s_poliza_id = request.args(5, None)

        _n_estatus = TPOLIZAS.ESTATUS.SIN_EXPORTAR

        _D_result = validarCfdisCancelados(_s_poliza_id)

        _dbRowsCargosAbonos = dbc01(
            (dbc01.tcfdi_prepoliza_asientoscontables.poliza_id == _s_poliza_id)
            ).select(
            dbc01.tcfdi_prepoliza_asientoscontables.poliza_id,
            dbc01.tcfdi_prepoliza_asientoscontables.importe_cargo.sum().with_alias('cargos'),
            dbc01.tcfdi_prepoliza_asientoscontables.importe_abono.sum().with_alias('abonos'),
            groupby = [
                dbc01.tcfdi_prepoliza_asientoscontables.poliza_id
                ]
            )

        if _dbRowsCargosAbonos:
            _n_cargos = _dbRowsCargosAbonos.last().cargos
            _n_abonos = _dbRowsCargosAbonos.last().abonos

            _n_resultado = _n_cargos - _n_abonos
            if _n_resultado == 0:

                _D_return = STV_FWK_POLIZA(_s_empresa_id).cerrar(_s_poliza_id)
                if _D_return.L_msgError:
                    stv_flashError("Error al cerrar la póliza: ", _D_return.L_msgError)
                    _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                else:
                    request.stv_flashInfo(
                        s_title = "Cambiar de estatus la póliza a 'cerrada'",
                        s_msg = "Se cambió el estado de la póliza correctamente",
                        s_mode = 'stv_flash',
                        D_addProperties = {}
                        )

                    if _D_result.n_numeroCfdisDevueltos > 0:
                        request.stv_flashInfo(
                            s_title = "Cambio de estatus",
                            s_msg = "Se cambió el estatus del siguiente número de pólizas a 'No contabilizar': "
                                    "%s" % _D_result.n_numeroCfdisDevueltos,
                            s_mode = 'stv_flash',
                            D_addProperties = {}
                            )
                    else:
                        pass

            else:
                stv_flashError("Error al cerrar la póliza: ", "La suma de cargos y abonos no concuerda.")

        else:
            pass

        _D_overrideView = request.stv_fwk_permissions.view_refresh

        # Botón especial 2
    elif _s_action in (request.stv_fwk_permissions.special_permisson_02.code,):

        _s_poliza_id = request.args(5, None)

        _D_return = _poliza_regresar_estado_poliza(_s_poliza_id)
        if _D_return.S_msgError:
            _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
        else:
            request.stv_flashInfo(
                s_title = "Se regresó la póliza completa",
                s_msg = "Se regresó el estado de la póliza correctamente",
                s_mode = 'stv_flash',
                D_addProperties = {}
                )
            # Todo se creo correctamente
            pass

        _D_overrideView = request.stv_fwk_permissions.view_refresh

    else:
        # No hacer nada y continuar con el procesio normal
        _D_overrideView = None
        pass

    _dt_fecha = datetime.datetime(int(_L_multi_id[1]), int(_L_multi_id[2]), int(_L_multi_id[3]))
    # Se generan las fechas en formato datetime para todo el rango del día
    _dt_fechaDesde = _dt_fecha.replace(hour = 0, minute = 0, second = 0, microsecond = 0)
    _dt_fechaHasta = _dt_fechaDesde + datetime.timedelta(days = 1)

    # Se crea el Query para identificar si el CFDI está como "sin contabilizar" y filstrando SOLO por CFDI que sea de
    # tipo ingreso
    _qry = (
            (_dbTable.empresa_id == _s_empresa_id)
            & (_dbTable.estatus == TPOLIZAS.ESTATUS.EN_PROCESO)
            & (_dbTable.empresa_plaza_sucursal_cajachica_id == _s_cajachica_id)
            # TODO: Crear una constante: "Tipo de poliza:  Ingresos"
            & (_dbTable.empresa_tipopoliza_id == 1)
            & (_dbTable.fecha >= _dt_fechaDesde)
            & (_dbTable.fecha < _dt_fechaHasta)
        )
    dbc01.tpolizas.dia.readable = True

    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.concepto,
        _dbTable.dia,
        ]

    _L_OrderBy = [
        _dbTable.fecha,
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_findall
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = [
            _dbTable.id,
            ],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        L_fieldnameIds = [_dbTable.id],
        x_offsetInArgs = request.args[:4]
        )

    _O_cat.addSubTab(
        s_tabName = "Poliza",
        s_url = URL(f = 'poliza', args = request.args[:4] + ['master_' + str(request.function), request.args(5)]),
        s_idHtml = fn_createIDTabUnique(f = 'poliza', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        s_tabName = "CFDIs",
        s_url = URL(
            c = '250cfdi', f = 'poliza_cfdis', args = request.args[:4] + ['master_' + str(request.function),
                                                                          request.args(5)]
            ),
        s_idHtml = fn_createIDTabUnique(
            c = '250cfdi', f = 'poliza_cfdis', s_masterIdentificator = str(request.function)
            )
        )

    # Se agrega el boton especial 01 que funciona para mandar la póliza a "Cerrada".
    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionLabel = "Cerrar póliza",
        s_permissionIcon = "fas fa-fast-forward",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
            confirm = Storage(
                s_title = "Favor de confirmar operación",
                s_text = "¿Está seguro de cerrar la póliza?",
                s_type = "warning",
                s_btnconfirm = "Aceptar",
                s_btncancel = "Cancelar",
                )
            ),
        L_options = []
        )

    # Se agrega el boton especial 02 que funciona para regresar/eliminar la póliza.
    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_02.code,
        s_permissionLabel = "Regresar póliza",
        s_permissionIcon = "fas fa-angle-double-left",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
            confirm = Storage(
                s_title = "Favor de confirmar operación",
                s_text = "¿Está seguro de querer eliminar la póliza?",
                s_type = "warning",
                s_btnconfirm = "Aceptar",
                s_btncancel = "Cancelar",
                )
            ),
        L_options = []
        )

    _O_cat.addRowSearchResultsProperty('orderBy', _L_OrderBy)

    _D_returnVars = _O_cat.process(D_overrideView = _D_overrideView)

    return (_D_returnVars)


def poliza_egresos_abiertas():
    """ Relación de pólizas de tipo egreso abiertas
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id    
        arg2: master_[nombre función maestro]
        arg3: id se compone de 4 partes
            id de la caja chica
            año
            mes
            día
        [arg4]: operación a realizar en el formulario
        [arg5]: id en caso de existir relación a un registro en la operación
        
    """

    _dbTable = dbc01.tpolizas

    _s_empresa_id = request.args(1, None)
    _s_multi_id = request.args(3, '0-0-0-0')  # Se ponen 4 elementos separados '-' por default
    _L_multi_id = _s_multi_id.split('-')
    if _L_multi_id[0] == 'None':
        _s_cajachica_id = None
    else:
        _s_cajachica_id = _L_multi_id[0]

    _s_action = request.args(4, request.stv_fwk_permissions.btn_none.code)

    # Botón especial 1
    if (_s_action in (request.stv_fwk_permissions.special_permisson_01.code,)):

        _s_poliza_id = request.args(5, None)

        _n_estatus = TPOLIZAS.ESTATUS.SIN_EXPORTAR

        _D_return = STV_FWK_POLIZA(_s_empresa_id).cerrar(_s_poliza_id)
        if _D_return.L_msgError:
            _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
        else:
            request.stv_flashInfo(
                s_title = "Cambiar de estatus la póliza a 'cerrada'",
                s_msg = "Se cambió el estado de la póliza correctamente",
                s_mode = 'stv_flash',
                D_addProperties = {}
                )
            # Todo se creo correctamente
            pass

        _D_overrideView = request.stv_fwk_permissions.view_refresh

        # Botón especial 2
    elif (_s_action in (request.stv_fwk_permissions.special_permisson_02.code,)):

        _s_poliza_id = request.args(5, None)

        _D_return = _poliza_regresar_estado_poliza(_s_poliza_id)
        if _D_return.S_msgError:
            _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
        else:
            request.stv_flashInfo(
                s_title = "Se regresó la póliza completa",
                s_msg = "Se regresó el estado de la póliza correctamente",
                s_mode = 'stv_flash',
                D_addProperties = {}
                )
            # Todo se creo correctamente
            pass

        _D_overrideView = request.stv_fwk_permissions.view_refresh

    else:
        # No hacer nada y continuar con el procesio normal
        _D_overrideView = None
        pass

    _dt_fecha = datetime.datetime(int(_L_multi_id[1]), int(_L_multi_id[2]), int(_L_multi_id[3]))
    # Se generan las fechas en formato datetime para todo el rango del día
    _dt_fechaDesde = _dt_fecha.replace(hour = 0, minute = 0, second = 0, microsecond = 0)
    _dt_fechaHasta = _dt_fechaDesde + datetime.timedelta(days = 1)

    # Se crea el Query para identificar si el CFDI está como "sin contabilizar" y filstrando SOLO por CFDI que sea de tipo Egreso
    _qry = (
            (_dbTable.empresa_id == _s_empresa_id)
            & (_dbTable.estatus == TPOLIZAS.ESTATUS.EN_PROCESO)
            & (_dbTable.empresa_plaza_sucursal_cajachica_id == _s_cajachica_id)
            # TODO: Crear una constante: "Tipo de poliza:  Ingresos"
            & (_dbTable.empresa_tipopoliza_id == 2)
            & (_dbTable.fecha >= _dt_fechaDesde)
            & (_dbTable.fecha < _dt_fechaHasta)
    )
    dbc01.tpolizas.dia.readable = True

    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.concepto,
        _dbTable.dia,
        ]

    _L_OrderBy = [
        _dbTable.fecha,
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_findall
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = [
            _dbTable.id,
            ],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        L_fieldnameIds = [_dbTable.id],
        x_offsetInArgs = request.args[:4]
        )

    _O_cat.addSubTab(
        s_tabName = "Poliza",
        s_url = URL(f = 'poliza', args = request.args[:4] + ['master_' + str(request.function), request.args(5)]),
        s_idHtml = fn_createIDTabUnique(f = 'poliza', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        s_tabName = "CFDIs",
        s_url = URL(
            c = '250cfdi', f = 'poliza_cfdis', args = request.args[:4] + ['master_' + str(request.function),
                                                                          request.args(5)]
            ),
        s_idHtml = fn_createIDTabUnique(
            c = '250cfdi', f = 'poliza_cfdis', s_masterIdentificator = str(request.function)
            )
        )

    # Se agrega el boton especial 01 que funciona para mandar la póliza a "Cerrada".
    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionLabel = "Cerrar póliza",
        s_permissionIcon = "fas fa-fast-forward",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
            confirm = Storage(
                s_title = "Favor de confirmar operación",
                s_text = "¿Está seguro de cerrar la póliza?",
                s_type = "warning",
                s_btnconfirm = "Aceptar",
                s_btncancel = "Cancelar",
                )
            ),
        L_options = []
        )

    # Se agrega el boton especial 02 que funciona para regresar/eliminar la póliza.
    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_02.code,
        s_permissionLabel = "Regresar póliza",
        s_permissionIcon = "fas fa-angle-double-left",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
            confirm = Storage(
                s_title = "Favor de confirmar operación",
                s_text = "¿Está seguro de querer eliminar la póliza?",
                s_type = "warning",
                s_btnconfirm = "Aceptar",
                s_btncancel = "Cancelar",
                )
            ),
        L_options = []
        )

    _O_cat.addRowSearchResultsProperty('orderBy', _L_OrderBy)

    _D_returnVars = _O_cat.process(D_overrideView = _D_overrideView)

    return (_D_returnVars)


# TODO: cambiar el nombre de la función a "pagos_sinexportar" 
def poliza_pagos_abiertas():
    """ Relación de pólizas de tipo pago abiertas
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id    
        arg2: master_[nombre función maestro]
        arg3: id se compone de 4 partes
            id de la caja chica
            año
            mes
            día
        [arg4]: operación a realizar en el formulario
        [arg5]: id en caso de existir relación a un registro en la operación
        
    """
    _dbTable = dbc01.tpolizas

    _s_empresa_id = request.args(1, None)
    _s_multi_id = request.args(3, '0-0-0-0')  # Se ponen 4 elementos separados '-' por default
    _L_multi_id = _s_multi_id.split('-')
    if _L_multi_id[0] == 'None':
        _s_cajachica_id = None
    else:
        _s_cajachica_id = _L_multi_id[0]

    _s_action = request.args(4, request.stv_fwk_permissions.btn_none.code)

    # Botón especial 1
    if (_s_action in (request.stv_fwk_permissions.special_permisson_01.code,)):

        _s_poliza_id = request.args(5, None)

        _n_estatus = TPOLIZAS.ESTATUS.SIN_EXPORTAR

        _D_return = STV_FWK_POLIZA(_s_empresa_id).cerrar(_s_poliza_id)
        if _D_return.L_msgError:
            _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
        else:
            request.stv_flashInfo(
                s_title = "Cambiar de estatus la póliza a 'cerrada'",
                s_msg = "Se cambió el estado de la póliza correctamente",
                s_mode = 'stv_flash',
                D_addProperties = {}
                )
            # Todo se creo correctamente
            pass

        _D_overrideView = request.stv_fwk_permissions.view_refresh

        # Botón especial 2
    elif (_s_action in (request.stv_fwk_permissions.special_permisson_02.code,)):

        _s_poliza_id = request.args(5, None)

        _D_return = _poliza_regresar_estado_poliza(_s_poliza_id)
        if _D_return.S_msgError:
            _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
        else:
            request.stv_flashInfo(
                s_title = "Se regresó la póliza completa",
                s_msg = "Se regresó el estado de la póliza correctamente",
                s_mode = 'stv_flash',
                D_addProperties = {}
                )
            # Todo se creo correctamente
            pass

        _D_overrideView = request.stv_fwk_permissions.view_refresh

    else:
        # No hacer nada y continuar con el procesio normal
        _D_overrideView = None
        pass

    _dt_fecha = datetime.datetime(int(_L_multi_id[1]), int(_L_multi_id[2]), int(_L_multi_id[3]))
    # Se generan las fechas en formato datetime para todo el rango del día
    _dt_fechaDesde = _dt_fecha.replace(hour = 0, minute = 0, second = 0, microsecond = 0)
    _dt_fechaHasta = _dt_fechaDesde + datetime.timedelta(days = 1)

    # Se crea el Query para identificar si el CFDI está como "sin contabilizar" y filstrando SOLO por CFDI que sea de tipo Egreso
    _qry = (
            (_dbTable.empresa_id == _s_empresa_id)
            & (_dbTable.estatus == TPOLIZAS.ESTATUS.EN_PROCESO)
            & (_dbTable.empresa_plaza_sucursal_cajachica_id == _s_cajachica_id)
            # TODO: Crear una constante: "Tipo de poliza:  Pagos"
            & (_dbTable.empresa_tipopoliza_id == 3)
            & (_dbTable.fecha >= _dt_fechaDesde)
            & (_dbTable.fecha < _dt_fechaHasta)
    )
    dbc01.tpolizas.dia.readable = True

    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.concepto,
        _dbTable.dia,
        ]

    _L_OrderBy = [
        _dbTable.fecha,
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_findall
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = [
            _dbTable.id,
            ],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        L_fieldnameIds = [_dbTable.id],
        x_offsetInArgs = request.args[:4]
        )

    _O_cat.addSubTab(
        s_tabName = "Poliza",
        s_url = URL(f = 'poliza', args = request.args[:4] + ['master_' + str(request.function), request.args(5)]),
        s_idHtml = fn_createIDTabUnique(f = 'poliza', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        s_tabName = "CFDIs",
        s_url = URL(
            c = '250cfdi', f = 'poliza_cfdis', args = request.args[:4] + ['master_' + str(request.function),
                                                                          request.args(5)]
            ),
        s_idHtml = fn_createIDTabUnique(
            c = '250cfdi', f = 'poliza_cfdis', s_masterIdentificator = str(request.function)
            )
        )

    # Se agrega el boton especial 01 que funciona para mandar la póliza a "Cerrada".
    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionLabel = "Cerrar póliza",
        s_permissionIcon = "fas fa-fast-forward",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
            confirm = Storage(
                s_title = "Favor de confirmar operación",
                s_text = "¿Está seguro de cerrar la póliza?",
                s_type = "warning",
                s_btnconfirm = "Aceptar",
                s_btncancel = "Cancelar",
                )
            ),
        L_options = []
        )

    # Se agrega el boton especial 02 que funciona para regresar/eliminar la póliza.
    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_02.code,
        s_permissionLabel = "Regresar póliza",
        s_permissionIcon = "fas fa-angle-double-left",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
            confirm = Storage(
                s_title = "Favor de confirmar operación",
                s_text = "¿Está seguro de querer eliminar la póliza?",
                s_type = "warning",
                s_btnconfirm = "Aceptar",
                s_btncancel = "Cancelar",
                )
            ),
        L_options = []
        )

    _O_cat.addRowSearchResultsProperty('orderBy', _L_OrderBy)

    _D_returnVars = _O_cat.process(D_overrideView = _D_overrideView)

    return (_D_returnVars)


def poliza_porempresa_abiertas():
    """ Relación de pólizas con estatus abiertas. 
    
    Args:
        [arg0]: operación a realizar en el formulario
        [arg1]: id en caso de existir relación a un registro en la operación
    """

    _dbTable = dbc01.tempresas

    # Query para filtrar las empresas que tengan pólizas pendientes por exportar
    _qry = (
            (dbc01.tpolizas.estatus == TPOLIZAS.ESTATUS.EN_PROCESO)
            & (
                (_dbTable.id == dbc01.tpolizas.empresa_id)
            )
    )
    # Se cuenta el número de pólizas que están sin exportar para mostrarse
    _dbField_calculado = dbc01.tpolizas.id.count()

    _L_fieldsToShow = [
        _dbTable.id,
        dbc01.tpolizas.empresa_id,
        _dbTable.razonsocial,
        _dbField_calculado.with_alias('Total_pólizas')
        ]

    # Se agrupa por el ID de la empresa.
    _L_GroupBy = [
        _dbTable.id,
        dbc01.tpolizas.empresa_id,
        _dbTable.razonsocial,
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_findall,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToGroupBy = _L_GroupBy,
        L_fieldsToSearch = [
            _dbTable.id,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        L_fieldnameIds = [
            _dbTable.id
            ]
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_plaza_sucursal_cajaschicas,
        s_url = URL(f = 'poliza_porcajachica_abiertas', args = ['master_' + str(request.function), request.args(1)]),
        s_idHtml = fn_createIDTabUnique(
            f = 'poliza_porcajachica_abiertas', s_masterIdentificator = str(request.function)
            )
        )

    _D_returnVars = _O_cat.process()

    return (_D_returnVars)


def poliza_porcajachica_abiertas():
    """ Relación de pólizas en proceso por caja chica
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id    
        [arg2]: operación a realizar en el formulario
        [arg3]: id en caso de existir relación a un registro en la operación; el id se compone de 4 partes
            id de la caja chica
            año
            mes
            día
    """

    _dbTable = dbc01.tempresa_plaza_sucursal_cajaschicas

    _s_empresa_id = request.args(1, None)
    _s_action = request.args(2, request.stv_fwk_permissions.btn_none.code)
    _s_multi_id = request.args(3, '0-0-0-0')  # Se ponen 4 elementos separados '-' por default
    _L_multi_id = _s_multi_id.split('-')
    if _L_multi_id[0] == 'None':
        _s_cajachica_id = None
    else:
        _s_cajachica_id = _L_multi_id[0]

    _qry = (
            (dbc01.tpolizas.estatus == TPOLIZAS.ESTATUS.EN_PROCESO)
            & (dbc01.tempresas.id == _s_empresa_id)
    )

    _dbField_calculado = dbc01.tpolizas.id.count()

    # Se configura el campo como debe de presentarte sus subcampos calculados
    dbc01.tpolizas.dia.readable = True
    dbc01.tempresa_plaza_sucursal_cajaschicas.empresa_plaza_sucursal_id.readable = True
    _L_fieldsToShow = [
        dbc01.tempresa_plaza_sucursal_cajaschicas.id,
        dbc01.tpolizas.dia,
        dbc01.tempresa_plaza_sucursal_cajaschicas.empresa_plaza_sucursal_id,
        dbc01.tempresa_plaza_sucursal_cajaschicas.nombre,
        _dbField_calculado.with_alias('Total_pólizas')
        ]

    _L_GroupBy = [
        dbc01.tempresa_plaza_sucursal_cajaschicas.id,
        dbc01.tpolizas.dia,
        dbc01.tempresa_plaza_sucursal_cajaschicas.empresa_plaza_sucursal_id,
        dbc01.tempresa_plaza_sucursal_cajaschicas.nombre
        ]

    _L_OrderBy = [
        dbc01.tpolizas.dia,
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_findall,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToGroupBy = _L_GroupBy,
        L_fieldsToSearch = [
            _dbTable.id,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        L_fieldnameIds = [
            _dbTable.id,
            dbc01.tpolizas.dia
            ],
        x_offsetInArgs = request.args[:2]
        )

    _O_cat.addRowSearchResultsLeftJoin(
        _dbTable.on(
            (_dbTable.id == dbc01.tpolizas.empresa_plaza_sucursal_cajachica_id)
            )
        )

    _O_cat.addRowSearchResultsProperty('orderBy', _L_OrderBy)

    _O_cat.addSubTab(
        s_tabName = "Ingresos",
        s_url = URL(
            f = 'poliza_ingresos_abiertas', args = request.args[:2] + ['master_' + str(request.function),
                                                                       request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(f = 'poliza_ingresos_abiertas', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        s_tabName = "Egresos",
        s_url = URL(
            f = 'poliza_egresos_abiertas', args = request.args[:2] + ['master_' + str(request.function),
                                                                      request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(f = 'poliza_egresos_abiertas', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        s_tabName = "Pagos",
        s_url = URL(
            f = 'poliza_pagos_abiertas', args = request.args[:2] + ['master_' + str(request.function),
                                                                    request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(f = 'poliza_pagos_abiertas', s_masterIdentificator = str(request.function))
        )

    if (_s_action in (request.stv_fwk_permissions.btn_view.code)):

        _dbField_fecha = Field(
            'fecha',
            'date',
            default = None,
            required = False,
            notnull = False,
            widget = stv_widget_inputDate,
            label = ('Fecha'),
            comment = '',
            writable = False,
            readable = True,
            represent = stv_represent_date
            )

        _dbRow = _dbTable(_s_cajachica_id)
        if _dbRow:
            pass
        else:
            _dbRow = Storage(
                id = None,
                nombre = "No definida"
                )
        _dbRow.fecha = datetime.datetime(int(_L_multi_id[1]), int(_L_multi_id[2]), int(_L_multi_id[3]))

        _D_returnVars = _O_cat.process(
            b_skipAction = True,
            L_formFactoryFields = [_dbTable.id, _dbTable.nombre, _dbField_fecha,
                                   _dbTable.empresa_plaza_sucursal_id],  # TODO se debe incluir el campo de fecha
            dbRow_formFactory = _dbRow  # TODO el row debe contener los datos en L_formFactoryFields
            )
    else:
        _D_returnVars = _O_cat.process()

    _D_returnVars

    return (_D_returnVars)


def polizas_cerradas():
    """ Relación de pólizas cerradas.
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id    
        arg2: master_[nombre función maestro]
        arg3: id compuesto por 4 partes
            id de la caja chica
            año
            mes
            día
        [arg4]: operación a realizar en el formulario
        [arg5]: id en caso de existir relación a un registro en la operación
        
    """
    _dbTable = dbc01.tpolizas

    _s_empresa_id = request.args(1, None)
    _s_multi_id = request.args(3, '0-0-0-0')  # Se ponen 4 elementos separados '-' por default
    _L_multi_id = _s_multi_id.split('-')
    if _L_multi_id[0] == 'None':
        _s_cajachica_id = None
    else:
        _s_cajachica_id = _L_multi_id[0]

    _s_action = request.args(4, request.stv_fwk_permissions.btn_none.code)

    # Botón especial 1 Exportar póliza contable
    if _s_action in (request.stv_fwk_permissions.special_permisson_01.code,):

        _s_poliza_id = request.args(5, None)

        _n_estatus = TPOLIZAS.ESTATUS.EXPORTADO

        _D_return = STV_FWK_POLIZA(_s_empresa_id).exportar(_s_poliza_id)
        if _D_return.L_msgError:
            request.stv_flashError(
                s_title = "No se puede exportar la póliza",
                s_msg = "La póliza no pudo ser exportada debido a que no tiene generado un TXT.",
                s_mode = 'stv_flash',
                D_addProperties = {}
                )
        else:
            request.stv_flashInfo(
                s_title = "Cambiar de estatus la póliza a 'Exportado'",
                s_msg = "Se cambió el estado de la póliza correctamente",
                s_mode = 'stv_flash',
                D_addProperties = {}
                )
            # Todo se creo correctamente
            pass

        _D_overrideView = request.stv_fwk_permissions.view_refresh

        # Botón especial 2 Generación del TXT
    elif _s_action in (request.stv_fwk_permissions.special_permisson_02.code,):

        _s_poliza_id = request.args(5, None)

        _D_return = STV_FWK_POLIZA(_s_empresa_id).generar_txt(_s_poliza_id)
        if _D_return.L_msgError:
            request.stv_flashInfo(
                s_title = "No se generó correctamente el TXT.",
                s_msg = " ".join(_D_return.L_msgError),
                s_mode = 'stv_flash',
                D_addProperties = {}
                )
        else:
            request.stv_flashInfo(
                s_title = "Generar TXT",
                s_msg = "Se generó el TXT correctamente",
                s_mode = 'stv_flash',
                D_addProperties = {}
                )
            # Todo se creo correctamente
            pass

        _D_overrideView = request.stv_fwk_permissions.view_refresh

        # Botón especial 3 Regresar estatus de la póliza
    elif _s_action in (request.stv_fwk_permissions.special_permisson_03.code,):

        _s_poliza_id = request.args(5, None)

        _dbRowsFolioPoliza = dbc01(
            _dbTable.id == _s_poliza_id
            ).select(
            _dbTable.folio
            )

        if _dbRowsFolioPoliza[0].folio != 0:
            request.stv_flashError(
                s_title = "Error regreso póliza",
                s_msg = "La póliza no puede abrirse de nuevo debido a que tiene un folio generado",
                s_mode = 'stv_flash',
                D_addProperties = {}
                )

        else:

            _D_result = validarCfdisCancelados(_s_poliza_id)
            if _D_result.n_numeroCfdisDevueltos > 0:
                request.stv_flashInfo(
                    s_title = "Cambio de estatus",
                    s_msg = "Se cambió el estatus del siguiente número de pólizas a 'No contabilizar': "
                            "%s" % _D_result.n_numeroCfdisDevueltos,
                    s_mode = 'stv_flash',
                    D_addProperties = {}
                    )
            else:
                pass

            _D_resultRegresoPolizaCerrada = validaRegresoPolizaCerrada(_s_poliza_id)

            if _D_resultRegresoPolizaCerrada.s_msgError:
                request.stv_flashError(
                    s_title = "Error al regresar la póliza a estatus Abierta",
                    s_msg = _D_resultRegresoPolizaCerrada.s_msgError,
                    s_mode = 'stv_flash',
                    D_addProperties = {}
                    )
            else:
                _n_estatus = TPOLIZAS.ESTATUS.EN_PROCESO
                _D_return = _poliza_actualizar_estado(_s_poliza_id, _n_estatus)
                if _D_return.S_msgError:
                    _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                else:
                    request.stv_flashInfo(
                        s_title = "Regresar la póliza a estatus 'Abierta'",
                        s_msg = "Se regresó correctamente el estatus",
                        s_mode = 'stv_flash',
                        D_addProperties = {}
                        )

        _D_overrideView = request.stv_fwk_permissions.view_refresh
        # Botón especial 4 Eliminar póliza completa
    elif _s_action in (request.stv_fwk_permissions.special_permisson_04.code,):

        _s_poliza_id = request.args(5, None)

        _dbRowsFolioPoliza = dbc01(
            _dbTable.id == _s_poliza_id
            ).select(
            _dbTable.folio
            )

        # Si la póliza ya tiene folio se hace un sweet alert para regresar la póliza completa
        if _dbRowsFolioPoliza[0].folio != 0:

            _s_poliza_id = request.args(5, None)
            # TODO: Aquí tiene que ir un sweet alert preguntando si está seguro
            _D_return = _poliza_regresar_estado_poliza(_s_poliza_id)
            if _D_return.S_msgError:
                _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
            else:
                request.stv_flashInfo(
                    s_title = "Eliminación de póliza",
                    s_msg = "Se eliminó correctamente la póliza",
                    s_mode = 'stv_flash',
                    D_addProperties = {}
                    )
                pass
        else:
            request.stv_flashError(
                s_title = "Error eliminación de póliza con folio",
                s_msg = "La póliza no contiene folio, aún se puede mandar la póliza a abierta.",
                s_mode = 'stv_flash',
                D_addProperties = {}
                )

        _D_overrideView = request.stv_fwk_permissions.view_refresh

    else:
        # No hacer nada y continuar con el procesio normal
        _D_overrideView = None
        pass

    _dt_fecha = datetime.datetime(int(_L_multi_id[1]), int(_L_multi_id[2]), int(_L_multi_id[3]))
    # Se generan las fechas en formato datetime para todo el rango del día
    _dt_fechaDesde = _dt_fecha.replace(hour = 0, minute = 0, second = 0, microsecond = 0)
    _dt_fechaHasta = _dt_fechaDesde + datetime.timedelta(days = 1)

    # Se crea el Query para identificar si el CFDI está como "sin contabilizar" y filstrando SOLO por CFDI que sea de tipo ingreso
    _qry = (
            (_dbTable.empresa_id == _s_empresa_id)
            & (_dbTable.estatus == TPOLIZAS.ESTATUS.SIN_EXPORTAR)
            # & (_dbTable.rol == TCFDIS.E_ROL.EMISOR)
            & (_dbTable.empresa_plaza_sucursal_cajachica_id == _s_cajachica_id)
            & (_dbTable.fecha >= _dt_fechaDesde)
            & (_dbTable.fecha < _dt_fechaHasta)
    )
    # Se pone visible el campo para poder mostrar la fecha en el grid
    dbc01.tpolizas.dia.readable = True

    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.empresa_tipopoliza_id,
        _dbTable.concepto,
        ]

    _L_OrderBy = [
        _dbTable.fecha,
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_findall
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = [
            _dbTable.id,
            ],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        L_fieldnameIds = [_dbTable.id],
        x_offsetInArgs = request.args[:4]
        )

    _O_cat.addSubTab(
        s_tabName = "Poliza",
        s_url = URL(f = 'poliza', args = request.args[:4] + ['master_' + str(request.function), request.args(5)]),
        s_idHtml = fn_createIDTabUnique(f = 'poliza', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        s_tabName = "CFDIs",
        s_url = URL(
            c = '250cfdi', f = 'poliza_cfdis', args = request.args[:4] + ['master_' + str(request.function),
                                                                          request.args(5)]
            ),
        s_idHtml = fn_createIDTabUnique(
            c = '250cfdi', f = 'poliza_cfdis', s_masterIdentificator = str(request.function)
            )
        )

    _s_poliza_id = request.args(5, None)

    _dbRowsTXTPoliza = dbc01(
        _dbTable.id == _s_poliza_id
        ).select(
        _dbTable.archivopoliza
        )

    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_02.code,
        s_permissionLabel = "Generar TXT",
        s_permissionIcon = "fas fa-copy",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
            confirm = Storage(
                s_title = "Favor de confirmar operación",
                s_text = "¿Está seguro de querer generar el TXT y el folio de la póliza? Si ya se generó con anterioridad un archivo TXT y folio para esta póliza se volverán a generar.",
                s_type = "warning",
                s_btnconfirm = "Aceptar",
                s_btncancel = "Cancelar",
                )
            ),
        L_options = []
        )

    # Se agrega el boton especial 01 que funciona para regresar la póliza de estatus cerrada a exportada.
    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionLabel = "Cambiar estatus a Exportado",
        s_permissionIcon = "fas fa-fast-forward",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
            confirm = Storage(
                s_title = "Favor de confirmar operación",
                s_text = ("¿Está seguro de querer mandar la póliza a 'Exportadas'?" +
                          "Esta acción solo podrá realizarse una vez que el TXT y folio se hayan generado."),
                s_type = "warning",
                s_btnconfirm = "Aceptar",
                s_btncancel = "Cancelar",
                )
            ),
        L_options = []
        )

    # Se agrega el boton especial 03 que funciona para regresar la póliza de estatus cerrada a abierta.
    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_03.code,
        s_permissionLabel = "Regresar estatus a En proceso",
        s_permissionIcon = "fas fa-angle-double-left",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
            confirm = Storage(
                s_title = "Favor de confirmar operación",
                s_text = "¿Está seguro de querer regresar la póliza a Abierta?",
                s_type = "warning",
                s_btnconfirm = "Aceptar",
                s_btncancel = "Cancelar",
                )
            ),
        L_options = []
        )

    # Se agrega el boton especial 04 que funciona para eliminar la póliza en caso de tener folio y no poderla regresar a abierta.
    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_04.code,
        s_permissionLabel = "Eliminar póliza con folio",
        s_permissionIcon = "fas fa-backspace",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
            confirm = Storage(
                s_title = "Favor de confirmar operación",
                s_text = ("¿Está seguro de querer eliminar esta póliza con folio?" +
                          "Una vez hecho no podrá recuperarse y se tendrá que generar de nuevo"),
                s_type = "warning",
                s_btnconfirm = "Aceptar",
                s_btncancel = "Cancelar",
                )
            ),
        L_options = []
        )

    _O_cat.addRowSearchResultsProperty('orderBy', _L_OrderBy)

    _D_returnVars = _O_cat.process(D_overrideView = _D_overrideView)

    return (_D_returnVars)


def polizas_exportadas():
    """ Relación de pólizas exportadas.
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id    
        arg2: master_[nombre función maestro]
        arg3: id se compone de 4 partes
            id de la caja chica
            año
            mes
            día
        [arg4]: operación a realizar en el formulario
        [arg5]: id en caso de existir relación a un registro en la operación
        
    """

    _dbTable = dbc01.tpolizas

    _s_empresa_id = request.args(1, None)
    _s_multi_id = request.args(3, '0-0-0-0')  # Se ponen 4 elementos separados '-' por default
    _L_multi_id = _s_multi_id.split('-')
    if _L_multi_id[0] == 'None':
        _s_cajachica_id = None
    else:
        _s_cajachica_id = _L_multi_id[0]

    _s_action = request.args(4, request.stv_fwk_permissions.btn_none.code)

    # Botón especial 1
    if (_s_action in (request.stv_fwk_permissions.special_permisson_01.code,)):

        _s_poliza_id = request.args(5, None)

        _n_estatus = TPOLIZAS.ESTATUS.SIN_EXPORTAR

        _D_return = _poliza_actualizar_estado(_s_poliza_id, _n_estatus)
        if _D_return.S_msgError:
            _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
        else:
            request.stv_flashInfo(
                s_title = "Regresar estatus la póliza a 'Cerrada'",
                s_msg = "Se regresó el estado de la póliza correctamente",
                s_mode = 'stv_flash',
                D_addProperties = {}
                )
            # Todo se creo correctamente
            pass

        _D_overrideView = request.stv_fwk_permissions.view_refresh

        # Botón especial 2
    elif (_s_action in (request.stv_fwk_permissions.special_permisson_02.code,)):

        _s_poliza_id = request.args(5, None)

        _dbRowsTXTPoliza = dbc01(
            _dbTable.id == _s_poliza_id
            ).select(
            _dbTable.folio
            )

        if (_dbRowsTXTPoliza):
            request.stv_flashInfo(
                s_title = "Generar TXT",
                s_msg = "El TXT de esta póliza ya ha sido generado.",
                s_mode = 'stv_flash',
                D_addProperties = {}
                )

        else:
            # Aquí debe de ser cerrar no exportar
            _D_return = STV_FWK_POLIZA(_s_empresa_id).generar_txt(_s_poliza_id)
            if _D_return.L_msgError:
                _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
            else:
                request.stv_flashInfo(
                    s_title = "Generar TXT",
                    s_msg = "Se generó el TXT correctamente",
                    s_mode = 'stv_flash',
                    D_addProperties = {}
                    )
                # Todo se creo correctamente
                pass

        _D_overrideView = request.stv_fwk_permissions.view_refresh

    else:
        # No hacer nada y continuar con el procesio normal
        _D_overrideView = None
        pass

    _dt_fecha = datetime.datetime(int(_L_multi_id[1]), int(_L_multi_id[2]), int(_L_multi_id[3]))
    # Se generan las fechas en formato datetime para todo el rango del día
    _dt_fechaDesde = _dt_fecha.replace(hour = 0, minute = 0, second = 0, microsecond = 0)
    _dt_fechaHasta = _dt_fechaDesde + datetime.timedelta(days = 1)

    # Se crea el Query para identificar si el CFDI está como "sin contabilizar" y filstrando SOLO por CFDI que sea de tipo ingreso
    _qry = (
            (_dbTable.empresa_id == _s_empresa_id)
            & (_dbTable.estatus == TPOLIZAS.ESTATUS.EXPORTADO)
            # & (_dbTable.rol == TCFDIS.E_ROL.EMISOR)
            & (_dbTable.empresa_plaza_sucursal_cajachica_id == _s_cajachica_id)
            & (_dbTable.fecha >= _dt_fechaDesde)
            & (_dbTable.fecha < _dt_fechaHasta)
    )
    # Se pone visible el campo para poder mostrar la fecha en el grid
    dbc01.tpolizas.dia.readable = True

    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.folio,
        _dbTable.empresa_tipopoliza_id,
        _dbTable.concepto,
        _dbTable.archivopoliza
        ]

    _L_OrderBy = [
        _dbTable.fecha,
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_findall
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = [
            _dbTable.id,
            ],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        L_fieldnameIds = [_dbTable.id],
        x_offsetInArgs = request.args[:4]
        )

    _O_cat.addSubTab(
        s_tabName = "Poliza",
        s_url = URL(f = 'poliza', args = request.args[:4] + ['master_' + str(request.function), request.args(5)]),
        s_idHtml = fn_createIDTabUnique(f = 'poliza', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        s_tabName = "CFDIs",
        s_url = URL(
            c = '250cfdi', f = 'poliza_cfdis', args = request.args[:4] + ['master_' + str(request.function),
                                                                          request.args(5)]
            ),
        s_idHtml = fn_createIDTabUnique(
            c = '250cfdi', f = 'poliza_cfdis', s_masterIdentificator = str(request.function)
            )
        )

    # Se agrega el boton especial 01 que funciona para regresar la póliza de estatus exportada a cerrada.
    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionLabel = "Regresar estatus a Sin exportar",
        s_permissionIcon = "fas fa-angle-double-left",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
            confirm = Storage(
                s_title = "Favor de confirmar operación",
                s_text = "¿Está seguro de querer regresar la póliza a Cerrada?",
                s_type = "warning",
                s_btnconfirm = "Aceptar",
                s_btncancel = "Cancelar",
                )
            ),
        L_options = []
        )

    _O_cat.addRowSearchResultsProperty('orderBy', _L_OrderBy)

    _D_returnVars = _O_cat.process(D_overrideView = _D_overrideView)

    return (_D_returnVars)


def poliza():
    """ Formulario detalle de póliza
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id    
        arg2: master_[nombre función maestro]
        arg3: id se compone de 4 partes
            id de la caja chica
            año
            mes
            día
        arg4: master_[nombre función maestro]
        arg5: poliza_id
        [arg6]: operación a realizar en el formulario
        [arg7]: id en caso de existir relación a un registro en la operación
        
    """

    _dbTable = dbc01.tpolizas

    _s_requestor = request.args(0)
    _s_poliza_id = request.args(5)

    _dbRowPoliza = dbc01.tpolizas(_s_poliza_id)

    _dbRows = dbc01(_dbTable.id == _s_poliza_id).select(_dbTable.folio)

    request.args.append(request.stv_fwk_permissions.btn_view.code)
    request.args.append(str(_s_poliza_id))

    request.stv_fwk_permissions.updateViewsFor_view_form()

    _L_fieldsToGroupBy = [
        _dbTable.id,
        _dbTable.empresa_tipopoliza_id,
        _dbTable.empresa_diario_id,
        _dbTable.concepto,
        _dbTable.archivopoliza
        ]

    _L_fieldsToShow = _L_fieldsToGroupBy + [
        dbc01.tcfdi_prepoliza_asientoscontables.importe_cargo.sum().with_alias('cargos'),
        dbc01.tcfdi_prepoliza_asientoscontables.importe_abono.sum().with_alias('abonos'),
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = None,
        xFieldLinkMaster = [],
        L_visibleButtons = [request.stv_fwk_permissions.btn_none, request.stv_fwk_permissions.btn_view],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        L_fieldsToGroupBy = _L_fieldsToGroupBy,
        x_offsetInArgs = request.args[:6]
        )

    _O_cat.addSubTab(
        s_tabName = "Asientos",
        s_url = URL(
            f = 'poliza_asientos', args = ['master_' + str(_s_requestor) + str(request.function),
                                           request.args(7)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'poliza_asientos', s_masterIdentificator = (
                    str(_s_requestor) + str(request.function))
            )
        )

    if (_dbRowPoliza.estatus == TPOLIZAS.ESTATUS.EN_PROCESO):
        _O_cat.addSubTab(
            s_tabName = "Depósitos bancarios",
            s_url = URL(
                f = 'poliza_depositosbancarios', args = request.args[:6] + [
                    'master_' + str(_s_requestor) + str(request.function), request.args(7)]
                ),
            s_idHtml = fn_createIDTabUnique(
                f = 'poliza_depositosbancarios', s_masterIdentificator = (
                        str(_s_requestor) + str(request.function))
                )
            )
    else:
        pass

    _D_returnVars = _O_cat.process()

    _D_returnVars.sumaCargos = 0
    _D_returnVars.sumaAbonos = 0

    if _D_returnVars.dbRecord:
        # Si hay un registro seleccionado, osea que es vero editar

        _dbRowsCargosAbonos = dbc01(
            (dbc01.tcfdi_prepoliza_asientoscontables.poliza_id == _s_poliza_id)
            ).select(
            dbc01.tcfdi_prepoliza_asientoscontables.poliza_id,
            dbc01.tcfdi_prepoliza_asientoscontables.importe_cargo.sum().with_alias('cargos'),
            dbc01.tcfdi_prepoliza_asientoscontables.importe_abono.sum().with_alias('abonos'),
            groupby = [
                dbc01.tcfdi_prepoliza_asientoscontables.poliza_id
                ]
            )

        if len(_dbRowsCargosAbonos) == 1:
            _dbRowCargosAbonos = _dbRowsCargosAbonos.last()
            _D_returnVars.sumaCargos = _dbRowCargosAbonos.cargos
            _D_returnVars.sumaAbonos = _dbRowCargosAbonos.abonos
        elif len(_dbRowsCargosAbonos) > 1:
            stv_flashError(
                s_title = "Error en poliza",
                s_msg = "Se encuentran varias polizas asociadas. Contacte al administrador.",
                s_mode = 'stv_flash',
                D_addProperties = {}
                )
        else:
            pass

    return (_D_returnVars)


def poliza_asientos():
    """ Formulario detalle de póliza
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: poliza_id    
        [arg2]: operación a realizar en el formulario
        [arg3]: id en caso de existir relación a un registro en la operación
        
    """

    _dbTable = dbc01.tcfdi_prepoliza_asientoscontables

    _s_poliza_id = request.args(1, None)
    _s_action = request.args(2, None)
    _qry = (
        (_dbTable.poliza_id == _s_poliza_id)
    )

    _L_OrderBy = [
        _dbTable.tipocreacion,
        _dbTable.referencia,
        _dbTable.asiento_contable,
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        dbTableMaster = dbc01.tpolizas,
        xFieldLinkMaster = [_dbTable.poliza_id],
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_findall,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.asiento_contable_poliza,
            _dbTable.cuenta_contable,
            _dbTable.importe_cargo,
            _dbTable.importe_abono,
            _dbTable.descripcion,
            _dbTable.referencia,
            # No es necesario mostrar los erroes a nivel póliza
            #            _dbTable.errores,
            _dbTable.empresa_diario_id,
            _dbTable.empresa_segmento_id,
            _dbTable.empresa_tipodocumentobancario_id,
            _dbTable.empresa_tipodocumentobancario_folio
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.poliza_id
            ],
        )

    _O_cat.addRowSearchResultsProperty('orderBy', _L_OrderBy)

    _D_returnVars = _O_cat.process()

    return (_D_returnVars)


def prepoliza():
    """ Formulario de la prepoliza de un CFDI
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: cfdi_id    
        [arg2]: operación a realizar en el formulario
        [arg3]: id en caso de existir relación a un registro en la operación
        
    """

    _dbTable = dbc01.tcfdi_prepolizas

    _s_cfdi_id = request.args(1)

    _dbRows = dbc01(
        (dbc01.tcfdi_prepolizas.cfdi_id == _s_cfdi_id)
        & (_dbTable.sat_status_cancelacion == TCFDI_PREPOLIZAS.SAT_STATUS.VIGENTE)
        ).select(dbc01.tcfdi_prepolizas.id)

    _qry = (
            (_dbTable.cfdi_id == _s_cfdi_id)
            & (_dbTable.sat_status_cancelacion == TCFDI_PREPOLIZAS.SAT_STATUS.VIGENTE)
    )

    if _dbRows:
        _dbRowPrepoliza = _dbRows.last()
        _n_prepoliza_id = _dbRowPrepoliza.id
    else:
        _n_prepoliza_id = 0

    request.stv_fwk_permissions.updateViewsFor_view_form()

    request.args.append(request.stv_fwk_permissions.btn_view.code)
    request.args.append(str(_n_prepoliza_id))

    _L_fieldsToGroupBy = [
        _dbTable.id,
        _dbTable.empresa_tipopoliza_id,
        _dbTable.empresa_diario_id,
        _dbTable.concepto
        ]

    _L_fieldsToShow = _L_fieldsToGroupBy + [
        dbc01.tcfdi_prepoliza_asientoscontables.importe_cargo.sum().with_alias('cargos'),
        dbc01.tcfdi_prepoliza_asientoscontables.importe_abono.sum().with_alias('abonos'),
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        dbTableMaster = dbc01.tcfdis,
        xFieldLinkMaster =
        [_dbTable.cfdi_id
         ],
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_view
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        L_fieldsToGroupBy = _L_fieldsToGroupBy,
        )

    _O_cat.addRowSearchResultsLeftJoin(
        dbc01.tcfdi_prepoliza_asientoscontables.on(
            dbc01.tcfdi_prepoliza_asientoscontables.cfdi_prepoliza_id == _dbTable.id
            )
        )

    if _n_prepoliza_id:

        _O_cat.addSubTab(
            dbTableDetail = dbc01.tcfdi_prepoliza_asientoscontables,
            s_url = URL(f = 'prepoliza_asientos', args = ['master_' + str(request.function), request.args(3)]),
            s_idHtml = fn_createIDTabUnique(f = 'prepoliza_asientos', s_masterIdentificator = str(request.function))
            )

    else:
        pass

    _D_returnVars = _O_cat.process()

    _D_returnVars.sumaCargos = 0
    _D_returnVars.sumaAbonos = 0

    if _D_returnVars.dbRecord:
        # Si hay un registro seleccionado, osea que es vero editar

        _dbRowsCargosAbonos = dbc01(
            (dbc01.tcfdi_prepoliza_asientoscontables.cfdi_prepoliza_id == _n_prepoliza_id)
            ).select(
            dbc01.tcfdi_prepoliza_asientoscontables.cfdi_prepoliza_id,
            dbc01.tcfdi_prepoliza_asientoscontables.importe_cargo.sum().with_alias('cargos'),
            dbc01.tcfdi_prepoliza_asientoscontables.importe_abono.sum().with_alias('abonos'),
            groupby = [
                dbc01.tcfdi_prepoliza_asientoscontables.cfdi_prepoliza_id
                ]
            )

        if len(_dbRowsCargosAbonos) == 1:
            _dbRowCargosAbonos = _dbRowsCargosAbonos.last()
            _D_returnVars.sumaCargos = _dbRowCargosAbonos.cargos
            _D_returnVars.sumaAbonos = _dbRowCargosAbonos.abonos
        elif len(_dbRowsCargosAbonos) > 1:
            stv_flashError(
                s_title = "Error en prepoliza",
                s_msg = "Se encuentran varias prepolizas asociadas. Contacte al administrador.",
                s_mode = 'stv_flash',
                D_addProperties = {}
                )
        else:
            pass

    else:
        pass

    return (_D_returnVars)


def prepoliza_asientos():
    """ Formulario detalle de los asientos contables de la prepoliza
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: cfdi_id    
        [arg2]: operación a realizar en el formulario
        [arg3]: id en caso de existir relación a un registro en la operación
    """

    _dbTable = dbc01.tcfdi_prepoliza_asientoscontables

    _L_OrderBy = [
        _dbTable.asiento_contable,
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tcfdi_prepolizas,
        xFieldLinkMaster = [_dbTable.cfdi_prepoliza_id],
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_findall,
            request.stv_fwk_permissions.btn_signature,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.asiento_contable,
            _dbTable.cuenta_contable,
            _dbTable.importe_cargo,
            _dbTable.importe_abono,
            _dbTable.descripcion,
            _dbTable.referencia,
            _dbTable.empresa_diario_id,
            _dbTable.empresa_segmento_id,
            _dbTable.empresa_tipodocumentobancario_id,
            _dbTable.errores,
            _dbTable.esingreso,
            _dbTable.escliente,
            _dbTable.esconcepto_porcobrar,
            _dbTable.esconcepto_cobrado,
            _dbTable.esconcepto_dctoporcobrar,
            _dbTable.esconcepto_dctocobrado,
            _dbTable.esimpuesto_trasladado,
            _dbTable.esimpuesto_portrasladar,
            _dbTable.esimpuesto_retenido,
            _dbTable.esimpuesto_porretener,
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.cfdi_prepoliza_id
            ]
        )

    _O_cat.addRowSearchResultsProperty('orderBy', _L_OrderBy)
    _D_returnVars = _O_cat.process()
    return _D_returnVars


def _poliza_actualizar_estado(s_poliza_id, n_estatus):
    '''Función para actualzar el estado actual de la póliza.
    
        Args:
            s_poliza_id = ID de la póliza
            n_estatus = Estatus al que se quiere cambiar la póliza
        
    '''
    _D_return = Storage(
        S_msgError = "",
        E_return = CLASS_e_RETURN.NOK_ERROR
        )
    if (s_poliza_id):
        dbc01.tpolizas(s_poliza_id).update_record(
            estatus = n_estatus
            )
        _D_return.E_return = CLASS_e_RETURN.OK
    else:
        _D_return.S_msgError = "No se pudo actualizar el estado"
        pass

    return (_D_return)


def _poliza_regresar_estado_poliza(s_poliza_id):
    """Función para regresar el estado de póliza

        Args:
            s_poliza_id: Id de la póliza que se quiere regresar.
    """
    _D_return = Storage(
        S_msgError = "",
        E_return = CLASS_e_RETURN.NOK_ERROR
        )
    # Constantes (luego preguntar a Jaime de como las crea él)
    _s_vacio = ' '

    try:

        _D_foliado = _poliza_get_estilopoliza(s_poliza_id)

        # Se tendrá que conservar el ID de la póliza y el registro dentro de la tabla póliza, solo se tendrá que
        # cambiar el estatus
        if _D_foliado.n_foliado.estilopoliza == TEMPRESA_PLAZA_SUCURSAL_CONTABILIDADES.ESTILOPOLIZA.POR_CFDI:
            # Se debe de dejar la prepoliza y asientos prepoliza igual con el ID de la póliza dentro.

            # Primero regresar el número de asiento en los asientos contables a vacío.
            _dbRowsAsientosPoliza = dbc01(
                dbc01.tcfdi_prepoliza_asientoscontables.poliza_id == s_poliza_id
                ).select(
                dbc01.tcfdi_prepoliza_asientoscontables.id
                )

            # Se barren uno por uno los asientos contables y se dejan vacíos
            for _dbRowAsiento_id in _dbRowsAsientosPoliza:
                
                dbc01(
                    dbc01.tcfdi_prepoliza_asientoscontables.id == _dbRowAsiento_id.id
                    ).update(
                    asiento_contable_poliza = None
                    )

            # Segundo, los CFDIS o CFDI que tenga anidada la póliza deben de regresarse a estatus "Sin contabilizar"
            _dbRowsPolizas_CFDIs = dbc01(
                dbc01.tcfdi_prepolizas.poliza_id == s_poliza_id
                ).select(
                dbc01.tcfdi_prepolizas.ALL
                )

            # Se barren los CFDIS que tenga anidado a la póliza para regresarles el estatus.
            for _dbRowPoliza_CFDI in _dbRowsPolizas_CFDIs:

                if _dbRowPoliza_CFDI.sat_status == TCFDIS.SAT_STATUS.CANCELADO:
                    dbc01.tcfdis(_dbRowPoliza_CFDI.cfdi_id).update_record(
                        etapa_cancelado = TCFDIS.E_ETAPA.SIN_CONTABILIZAR
                        )
                else:
                    dbc01.tcfdis(_dbRowPoliza_CFDI.cfdi_id).update_record(
                        etapa = TCFDIS.E_ETAPA.SIN_CONTABILIZAR
                        )
            # Si es por CFDI (1 a 1) se debe de regresar el estatus del CFDI a "No_definido"
            dbc01.tpolizas(s_poliza_id).update_record(
                estatus = TPOLIZAS.ESTATUS.NO_DEFINIDO
                )

        elif (
                _D_foliado.n_foliado.estilopoliza == TEMPRESA_PLAZA_SUCURSAL_CONTABILIDADES.ESTILOPOLIZA.ACUMULADA_DESGLOZE_POR_CFDI):
            # Si es por día se elimina ID de la póliza de las tablas prepolizas y asientos prepoliza

            # Primero regresar el número de asiento en los asientos contables a vacío.
            _dbRowsAsientosPoliza = dbc01(
                dbc01.tcfdi_prepoliza_asientoscontables.poliza_id == s_poliza_id
                ).select(
                dbc01.tcfdi_prepoliza_asientoscontables.id
                )

            # Se dejan vacíos.
            for _dbRowAsiento_id in _dbRowsAsientosPoliza:
                     
                dbc01.tcfdi_prepoliza_asientoscontables(_dbRowAsiento_id.id).update_record(
                    asiento_contable_poliza = None,
                    poliza_id = None
                    )

                # Segundo, los CFDIS o CFDI que tenga anidada la póliza deben de regresarse a estatus "Sin contabilizar"
            _dbRowsPolizas_CFDIs = dbc01(
                dbc01.tcfdi_prepolizas.poliza_id == s_poliza_id
                ).select(
                dbc01.tcfdi_prepolizas.ALL
                )

            # Se barren los CFDIS que tenga anidado a la póliza para regresarles el estatus.
            for _dbRowPoliza_CFDI in _dbRowsPolizas_CFDIs:
                if _dbRowPoliza_CFDI.sat_status_cancelacion == TCFDI_PREPOLIZAS.SAT_STATUS.CANCELADO:
                    dbc01.tcfdis(_dbRowPoliza_CFDI.cfdi_id).update_record(
                        etapa_cancelado = TCFDIS.E_ETAPA.SIN_CONTABILIZAR
                        )
                else:
                    dbc01.tcfdis(_dbRowPoliza_CFDI.cfdi_id).update_record(
                        etapa = TCFDIS.E_ETAPA.SIN_CONTABILIZAR
                        )

            dbc01.tpolizas(s_poliza_id).update_record(
                estatus = TPOLIZAS.ESTATUS.NO_DEFINIDO
                )

        else:
            _D_return.S_msgError = ["No se encontró un estilo de póliza para realizar este proceso"]
            # Si no se encontró el estilo de póliza que haga rollback a los cambios ya hechos. 
            dbc01.rollback()

    except:
        _D_return.S_msgError = ["Hubo un error durante el regreso de la póliza"]
        dbc01.rollback()

    finally:
        # _D_return.E_return = CLASS_e_RETURN.OK
        return (_D_return)

    return ()


def _poliza_get_estilopoliza(s_poliza_id):
    '''Función para conseguir el estilo de la póliza
          
            Args:
                s_poliza_id: ID de la póliza
            
    '''
    _D_return = Storage(
        s_msgError = "",
        n_foliado = 0,
        )

    # Se obtiene todos los datos de la póliza.
    _dbRowPoliza = dbc01(dbc01.tpolizas.id == s_poliza_id).select(dbc01.tpolizas.ALL)

    # Se consulta la sucursal para poder obtener el estilo de póliza que se está manejando.
    _dbRowSucursal_Poliza = dbc01(
        dbc01.tempresa_plaza_sucursal_cajaschicas.id == _dbRowPoliza[0].empresa_plaza_sucursal_cajachica_id
        ).select(
        dbc01.tempresa_plaza_sucursal_cajaschicas.empresa_plaza_sucursal_id
        )

    # El año se saca de la póliza que a su vez este se sacó del CFDI o los CFDIs relacionados.
    _s_anio = str(_dbRowPoliza[0].dia.year)

    # Se consulta el estilo de póliza que se está manejando.
    _dbTable = dbc01.tempresa_plaza_sucursal_contabilidades

    _dbQry = (_dbTable.empresa_plaza_sucursal_id == _dbRowSucursal_Poliza[0].empresa_plaza_sucursal_id)

    _D_return.n_foliado = _prepoliza_generar_getCamposPorEjercicio(
        s_aniocfdi = _s_anio,
        dbTable = _dbTable,
        dbQry = _dbQry,
        D_regresarCampos = Storage(
            estilopoliza = 'estilopoliza'
            )
        )

    return (_D_return)


def poliza_archivo():
    '''Función para la descarga del archivo TXT de la póliza
    '''
    _bDisplay = False if request.vars.get('display') else True
    return response.download(
        request, dbc01, attachment = _bDisplay, download_filename = request.vars.get('filename', None)
        )
    

def poliza_depositosbancarios():
    """Forma modal para el registro de los depósitos bancarios. 

    """

    _dbTable = dbc01.tcfdi_prepoliza_asientoscontables

    _s_empresa_id = request.args(1, None)

    _s_multi_id = request.args(3, '0-0-0-0')  # Se ponen 4 elementos separados '-' por default
    _L_multi_id = _s_multi_id.split('-')
    if _L_multi_id[0] == 'None':
        _s_cajachica_id = None
    else:
        _s_cajachica_id = _L_multi_id[0]

    _s_poliza_id = request.args(7, None)

    _s_action = request.args(8, None)

    _qry = (
            (_dbTable.poliza_id == _s_poliza_id)
            & (_dbTable.tipocreacion == TCFDI_PREPOLIZA_ASIENTOSCONTABLES.TIPOCREACION.MANUAL)
    )

    _L_OrderBy = [
        _dbTable.referencia,
        _dbTable.asiento_contable
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        dbTableMaster = dbc01.tpolizas,
        xFieldLinkMaster = [_dbTable.poliza_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.asiento_contable_poliza,
            _dbTable.cuenta_contable,
            _dbTable.importe_cargo,
            _dbTable.importe_abono,
            _dbTable.descripcion,
            _dbTable.referencia,
            _dbTable.empresa_diario_id,
            _dbTable.empresa_segmento_id,
            _dbTable.empresa_tipodocumentobancario_id,
            _dbTable.empresa_tipodocumentobancario_folio
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.poliza_id
            ],
        x_offsetInArgs = request.args[:6]
        )

    _L_fieldsForm = [
        Field(
            'descripcion', 'string', length = 100, default = None,
            required = True,
            notnull = True,
            widget = stv_widget_input, label = 'Descripción',
            comment = "Si se deja vacío será Depósito + fecha de depósito",
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'fechadeposito', 'date', default = None,
            required = True, requires = IS_NULL_OR(IS_DATE(format = (STV_FWK_APP.FORMAT.s_DATE))),
            notnull = False, unique = False,
            widget = stv_widget_inputDate, label = (
                'Fecha de depósito'), comment = 'Fecha en la que fue depositado el dinero al banco.',
            writable = True, readable = True,
            represent = stv_represent_date
            ),
        Field(
            'empresa_cuentabancaria_id', dbc01.tempresa_cuentasbancarias, default = None,
            required = True,
            requires = IS_IN_DB(dbc01, 'tempresa_cuentasbancarias.id', dbc01.tempresa_cuentasbancarias._format),
            ondelete = 'NO ACTION', notnull = True, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = ('Cuenta bancaria'), comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r)
            ),
        Field(
            'importe', 'decimal(14,4)', default = 0,
            required = True,
            notnull = True, unique = False,
            widget = stv_widget_inputMoney, label = 'Importe', comment = None,
            writable = True, readable = True,
            represent = stv_represent_money_ifnotzero
            ),
        Field(
            'referencia', 'string', length = 20, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = ('Referencia'), comment = (''),
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        ]

    _O_cat.addRowSearchResultsProperty('orderBy', _L_OrderBy)

    _O_cat.addRowContentEvent('onValidation', _poliza_depositosbancarios_validation)

    _D_returnVars = _O_cat.process(
        L_formFactoryFields = _L_fieldsForm,
        )

    if (_s_action == request.stv_fwk_permissions.btn_save.code) and (_D_returnVars.rowContent_form.accepted):
        insertarasiento_depositobancario(
            n_poliza_id = _s_poliza_id,
            n_cajachica_id = _s_cajachica_id,
            n_empresa_id = _s_empresa_id,
            s_descripcion = request.vars.descripcion,
            d_fechadeposito = request.vars.fechadeposito,
            n_cuentabancaria_id = request.vars.empresa_cuentabancaria_id,
            n_importe = request.vars.importe,
            s_referencia = request.vars.referencia,
            )
    else:
        pass

    return (_D_returnVars)


def _poliza_depositosbancarios_validation(O_form):
    """Validación para los depósitos bancarios.
    """

    _s_id = O_form.record_id

    if not O_form.vars.importe:
        O_form.errors.importe = 'El importe no puede estar vacío'
    else:
        pass

    return O_form


def insertarasiento_depositobancario(
        n_poliza_id, n_cajachica_id, n_empresa_id, s_descripcion, d_fechadeposito, n_cuentabancaria_id, n_importe,
        s_referencia
        ):
    """Función para la inserción del asiento para el depósito bancario. 
        
        Args:
            n_poliza_id: ID de la póliza.
            s_descripcion: Descripción del depósito a bando. 
            d_fechadeposito: Fecha del depósito. 
            n_cuentabancaria_id: ID de la cuenta bancaría a donde se depositó el dinero. 
            n_importe: Importe total depósitado al banco. 
            n_referencia: Número de referencia del depósito. 
    """

    _D_poliza_asiento_depositobancario_banco = Storage(
        poliza_id = None,
        descripcion = "",
        asiento_contable_poliza = "",
        cuenta_contable = None,
        referencia = "",
        empresa_diario_id = None,
        empresa_segmento_id = None,
        importe_cargo = 0,
        importe_abono = 0,
        cfdi_ingreso_id = None,
        esimpuesto_trasladado = True,
        errores = "",
        tipocreacion = TCFDI_PREPOLIZA_ASIENTOSCONTABLES.TIPOCREACION.MANUAL
        )

    _dbRows_cuentabancaria_guias = dbc01(
        (dbc01.tempresa_cuentabancaria_guias.empresa_cuentabancaria_id == n_cuentabancaria_id)
        ).select(
        dbc01.tempresa_cuentabancaria_guias.cuentacontable
        )

    if (_dbRows_cuentabancaria_guias):
        _D_poliza_asiento_depositobancario_banco.cuenta_contable = _dbRows_cuentabancaria_guias.last().cuentacontable
    else:
        pass

    if (s_descripcion):
        _D_poliza_asiento_depositobancario_banco.descripcion = s_descripcion
    else:
        pass

    _dbRows_empresa_contpaqi_bancosdiariosespeciales = dbc01(
        (dbc01.tempresa_bancosdiariosespeciales.empresa_id == n_empresa_id)
        ).select(
        dbc01.tempresa_bancosdiariosespeciales.bancos_tipo_documentos_venta_contado,
        dbc01.tempresa_bancosdiariosespeciales.bancos_venta_contado_diario_id
        )

    if (_dbRows_empresa_contpaqi_bancosdiariosespeciales):

        _D_poliza_asiento_depositobancario_banco.empresa_tipodocumentobancario_id = _dbRows_empresa_contpaqi_bancosdiariosespeciales.last().bancos_tipo_documentos_venta_contado
        _D_poliza_asiento_depositobancario_banco.empresa_diario_id = _dbRows_empresa_contpaqi_bancosdiariosespeciales.last().bancos_venta_contado_diario_id

    else:
        pass

    _D_poliza_asiento_depositobancario_banco.referencia = s_referencia  # TODO: Crear referencia para los depositos bancarios.

    _D_poliza_asiento_depositobancario_banco.importe_cargo = n_importe

    _D_poliza_asiento_depositobancario_banco.poliza_id = n_poliza_id

    dbc01.tcfdi_prepoliza_asientoscontables.insert(**_D_poliza_asiento_depositobancario_banco)

    _D_poliza_asiento_depositobancario_caja = Storage(
        poliza_id = None,
        descripcion = "",
        asiento_contable_poliza = "",
        cuenta_contable = None,
        referencia = "",
        empresa_diario_id = None,
        empresa_segmento_id = None,
        importe_cargo = 0,
        importe_abono = 0,
        cfdi_ingreso_id = None,
        esimpuesto_trasladado = True,
        errores = "",
        tipocreacion = TCFDI_PREPOLIZA_ASIENTOSCONTABLES.TIPOCREACION.MANUAL
        )

    _dbRows_cajachica_guiascontables = dbc01(
        (dbc01.tempresa_plaza_sucursal_cajachica_guias.empresa_plaza_sucursal_cajaschicas_id == n_cajachica_id)
        & (
                dbc01.tempresa_plaza_sucursal_cajachica_guias.monedacontpaqi_id == D_stvSiteHelper.dbconfigs.defaults.moneda_contpaqi_id)
        ).select(
        dbc01.tempresa_plaza_sucursal_cajachica_guias.cuenta_contable_cajachica
        )

    if (_dbRows_cajachica_guiascontables):
        _D_poliza_asiento_depositobancario_caja.cuenta_contable = _dbRows_cajachica_guiascontables.last().cuenta_contable_cajachica
    else:
        pass

    if (s_descripcion):
        _D_poliza_asiento_depositobancario_caja.descripcion = s_descripcion
    else:
        pass

    _D_poliza_asiento_depositobancario_caja.referencia = s_referencia  # TODO: Crear referencia para los depositos bancarios.

    _D_poliza_asiento_depositobancario_caja.importe_abono = n_importe

    _D_poliza_asiento_depositobancario_caja.poliza_id = n_poliza_id

    dbc01.tcfdi_prepoliza_asientoscontables.insert(**_D_poliza_asiento_depositobancario_caja)

    return


def prepoliza_canceladas():
    """ Prepoliza cancelada.

    @descripcion Prepoliza cancelada

    @keyword arg0: CFDI
    @keyword arg1: cfdi_id
    @keyword [arg2]: codigo de funcionalidad
    @keyword [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso btn_special_permisson_01

    """

    _dbTable = dbc01.tcfdi_prepolizas

    _s_cfdi_id = request.args(1)

    _dbRows = dbc01(
        (dbc01.tcfdi_prepolizas.cfdi_id == _s_cfdi_id)
        & (_dbTable.sat_status_cancelacion == TCFDI_PREPOLIZAS.SAT_STATUS.CANCELADO)
        ).select(dbc01.tcfdi_prepolizas.id)

    _qry = (
            (_dbTable.cfdi_id == _s_cfdi_id)
            & (_dbTable.sat_status_cancelacion == TCFDI_PREPOLIZAS.SAT_STATUS.CANCELADO)
    )

    if _dbRows:
        _dbRowPrepoliza = _dbRows.last()
        _n_prepoliza_id = _dbRowPrepoliza.id
    else:
        _n_prepoliza_id = 0

    request.stv_fwk_permissions.updateViewsFor_view_form()

    request.args.append(request.stv_fwk_permissions.btn_view.code)
    request.args.append(str(_n_prepoliza_id))

    _L_fieldsToGroupBy = [
        _dbTable.id,
        _dbTable.empresa_tipopoliza_id,
        _dbTable.empresa_diario_id,
        _dbTable.concepto
        ]

    _L_fieldsToShow = _L_fieldsToGroupBy + [
        dbc01.tcfdi_prepoliza_asientoscontables.importe_cargo.sum().with_alias('cargos'),
        dbc01.tcfdi_prepoliza_asientoscontables.importe_abono.sum().with_alias('abonos'),
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        dbTableMaster = dbc01.tcfdis,
        xFieldLinkMaster =
        [_dbTable.cfdi_id
         ],
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_view
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        L_fieldsToGroupBy = _L_fieldsToGroupBy,
        )

    _O_cat.addRowSearchResultsLeftJoin(
        dbc01.tcfdi_prepoliza_asientoscontables.on(
            dbc01.tcfdi_prepoliza_asientoscontables.cfdi_prepoliza_id == _dbTable.id
            )
        )

    if _n_prepoliza_id:

        _O_cat.addSubTab(
            dbTableDetail = dbc01.tcfdi_prepoliza_asientoscontables,
            s_url = URL(f = 'prepoliza_asientos', args = ['master_' + str(request.function), request.args(3)]),
            s_idHtml = fn_createIDTabUnique(f = 'prepoliza_asientos', s_masterIdentificator = str(request.function))
            )

    else:
        pass

    _D_returnVars = _O_cat.process()

    _D_returnVars.sumaCargos = 0
    _D_returnVars.sumaAbonos = 0

    if _D_returnVars.dbRecord:
        # Si hay un registro seleccionado, osea que es vero editar

        _dbRowsCargosAbonos = dbc01(
            (dbc01.tcfdi_prepoliza_asientoscontables.cfdi_prepoliza_id == _n_prepoliza_id)
            ).select(
            dbc01.tcfdi_prepoliza_asientoscontables.cfdi_prepoliza_id,
            dbc01.tcfdi_prepoliza_asientoscontables.importe_cargo.sum().with_alias('cargos'),
            dbc01.tcfdi_prepoliza_asientoscontables.importe_abono.sum().with_alias('abonos'),
            groupby = [
                dbc01.tcfdi_prepoliza_asientoscontables.cfdi_prepoliza_id
                ]
            )

        if len(_dbRowsCargosAbonos) == 1:
            _dbRowCargosAbonos = _dbRowsCargosAbonos.last()
            _D_returnVars.sumaCargos = _dbRowCargosAbonos.cargos
            _D_returnVars.sumaAbonos = _dbRowCargosAbonos.abonos
        elif len(_dbRowsCargosAbonos) > 1:
            stv_flashError(
                s_title = "Error en prepoliza",
                s_msg = "Se encuentran varias prepolizas asociadas. Contacte al administrador.",
                s_mode = 'stv_flash',
                D_addProperties = {}
                )
        else:
            pass

    else:
        pass

    return _D_returnVars


def validarCfdisCancelados(s_poliza_id):
    """ Función para verificar que los CFDIs que se vayan a contabilizar no hayan sido cancelados.
    """

    _D_return = Storage(
        s_msgError = "",
        E_return = CLASS_e_RETURN.NOK_ERROR,
        n_numeroCfidsDevueltos = 0
        )

    _dbRows_cfdisPoliza = dbc01(
        (dbc01.tcfdi_prepolizas.poliza_id == s_poliza_id)
        ).select(
            dbc01.tcfdi_prepolizas.cfdi_id
        )

    _n_contadorCfdis = 0

    # Validación generica para verificar que ninguno de los CFDIs de esta póliza se hayan cancelado en el mismo día.
    if _dbRows_cfdisPoliza:
        for _dbRow_cfdiPoliza in _dbRows_cfdisPoliza:
            _dbRow_cfdi = dbc01.tcfdis(_dbRow_cfdiPoliza.cfdi_id)

            if _dbRow_cfdi.sat_status == TCFDIS.SAT_STATUS.CANCELADO:
                if _dbRow_cfdi.etapa_cancelado == TCFDIS.E_ETAPA.SIN_CONTABILIZAR:
                    if _dbRow_cfdi.dia == _dbRow_cfdi.sat_fecha_cancelacion.date():
                        # Si la fecha de cancelación fue el mismo día entonces se manda a CFDI no contabilizable.

                        # Se cambia el estado al CFDI de ingreso por NO_CONTABILIZAR_CANCELADO
                        _dbRow_cfdi.etapa = TCFDIS.E_ETAPA.NO_CONTABILIZAR_CANCELADO
                        _dbRow_cfdi.update_record()

                        # Se borrar su prepoliza.
                        _dbRowCfidPrepoliza = dbc01(
                            (dbc01.tcfdi_prepolizas.cfdi_id == _dbRow_cfdi.id)
                            ).delete()

                        ++_n_contadorCfdis
                        _D_return = STV_FWK_POLIZA(_dbRow_cfdi.empresa_id).borrar_cfdi(_dbRow_cfdi.id)
                    else:
                        pass
                else:
                    pass
            else:
                pass
        _D_return.n_numeroCfdisDevueltos = _n_contadorCfdis
        _D_return.E_return = CLASS_e_RETURN.OK
    else:
        _D_return.s_msgError = "No se encontraron CFDIs."

    return _D_return


def validaRegresoPolizaCerrada(s_poliza_id):
    """ función para validar el regreso de la póliza cerrada a abierta.

    :return:
    """

    _D_return = Storage(
        s_msgError = "",
        E_return = CLASS_e_RETURN.NOK_ERROR,
        n_numeroCfidsDevueltos = 0
        )

    _dbRows_cfdisPoliza = dbc01(
        (dbc01.tcfdi_prepolizas.poliza_id == s_poliza_id)
        ).select(
            dbc01.tcfdi_prepolizas.cfdi_id
        )

    if _dbRows_cfdisPoliza:
        for _dbRow_cfdiPoliza in _dbRows_cfdisPoliza:
            _dbRow_cfdi = dbc01.tcfdis(_dbRow_cfdiPoliza.cfdi_id)

            if _dbRow_cfdi.sat_status == TCFDIS.SAT_STATUS.CANCELADO:
                if _dbRow_cfdi.etapa_cancelado == TCFDIS.E_ETAPA.CON_POLIZA:
                    _dbRowsPoliza = dbc01(
                        (dbc01.tcfdi_prepolizas.cfdi_id == _dbRow_cfdi.id)
                        & (dbc01.tcfdi_prepolizas.sat_status_cancelacion == TCFDI_PREPOLIZAS.SAT_STATUS.VIGENTE)
                        & (dbc01.tpolizas.estatus == TPOLIZAS.ESTATUS.EXPORTADO)
                        & (dbc01.tpolizas.id == dbc01.tcfdi_prepolizas.poliza_id)
                        ).select(
                            dbc01.tpolizas.ALL
                        )

                    _dbRowPoliza = _dbRowsPoliza.last()
                    if _dbRowPoliza:
                        if _dbRowPoliza.estatus in (TPOLIZAS.ESTATUS.SIN_EXPORTAR, TPOLIZAS.ESTATUS.EXPORTADO):
                            _D_return.s_msgError = "No se puede regresar la póliza porque la cancelación del CFDI %s " \
                                                   " ya fue contabilizado." % _dbRow_cfdi.id

                            break
                        else:
                            pass
                    else:
                        pass
                else:
                    pass
            else:
                pass
    else:
        pass

    return _D_return
