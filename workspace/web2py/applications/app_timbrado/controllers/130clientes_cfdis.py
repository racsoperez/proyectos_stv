# -*- coding: utf-8 -*-


def clientes():
    """ Formulario de clientes, optimizado en la búsqueda de expedientes de clientes.
    
    @descripcion Clientes
    
    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.
    
    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_print
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    #@permiso btn_findall
    #@permiso btn_signature
    @permiso special_permisson_01:[fas fa-money-check-alt] Emisión de CFDIs
    @permiso special_permisson_02:[<i class="fas fa-balance-scale-right] Recalcular saldos
    @permiso special_permisson_03:[<i class="fas fa-balance-scale-right] Recalcular saldos Optimizado
    @permiso special_permisson_04:[<i class="fas fa-balance-scale-right] Recalcular saldos de Todos los clientes
    """
    _D_return = FUNC_RETURN()

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 2,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_cliente_id = request.args(3, None),
        )

    _dbTabla = dbc01.tempresa_clientes
    _D_useView = None
    _s_overrideCode = None
    _CLS_TABLA = TEMPRESA_CLIENTES.PROC(empresa_id = _D_args.s_empresa_id)

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):
        # Si esta editando o guardando una edición, se configuran los campos
        _D_result = _CLS_TABLA.configuracampos_edicion(x_cliente = _D_args.s_cliente_id)
        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_result)
            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            pass

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _D_result = _CLS_TABLA.configuracampos_nuevo(s_cliente_id = _D_args.n_id)

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_result)
            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            pass

    elif _D_args.s_accion == request.stv_fwk_permissions.special_permisson_01.code:
        # En caso de presionar el boton especial1; se muestran solamente los detalles de CFDIs

        _s_overrideCode = request.stv_fwk_permissions.btn_view.code

    elif _D_args.s_accion == request.stv_fwk_permissions.special_permisson_02.code:
        _D_results = STV_LIB_CFDI.ESTADOCUENTA(
            _D_args.s_empresa_id,
            _D_args.s_cliente_id,
            E_movto_generado_por = TCFDI_MOVIMIENTOS.E_GENERADO_POR.RECALCULO_CLIENTE
            ).procesar_cliente()

        _D_insert_cliente_log = Storage(
            empresa_cliente_id = _D_args.s_cliente_id,
            fecha = request.browsernow,
            descripcion = "Cálculo de saldo manual",
            tipotransaccion = TEMPRESA_CLIENTE_LOG.TIPOTRANSACCION.CALCULODESALDO,
            errores = "\n".join(_D_results.L_msgError),
            warnings = "\n".join(_D_results.L_msgWarning),
            info = "\n".join(_D_results.L_msgInformativo),
            resultado = _D_results.E_return
            )

        dbc01.tempresa_cliente_log.insert(**_D_insert_cliente_log)
        _D_useView = request.stv_fwk_permissions.view_refresh

    elif _D_args.s_accion == request.stv_fwk_permissions.special_permisson_03.code:
        _D_results = TCFDIS.EDOCTA(
            _D_args.s_empresa_id,
            _D_args.s_cliente_id,
            E_movto_generado_por = TCFDI_MOVIMIENTOS.E_GENERADO_POR.RECALCULO_CLIENTE
            ).procesar_cliente()

        _D_insert_cliente_log = Storage(
            empresa_cliente_id = _D_args.s_cliente_id,
            fecha = request.browsernow,
            descripcion = "Cálculo de saldo manual 2",
            tipotransaccion = TEMPRESA_CLIENTE_LOG.TIPOTRANSACCION.CALCULODESALDO,
            errores = "\n".join(_D_results.L_msgError),
            warnings = "\n".join(_D_results.L_msgWarning),
            info = "\n".join(_D_results.L_msgInfo),
            resultado = _D_results.E_return
            )

        dbc01.tempresa_cliente_log.insert(**_D_insert_cliente_log)
        _D_useView = request.stv_fwk_permissions.view_refresh

    elif _D_args.s_accion == request.stv_fwk_permissions.special_permisson_04.code:

        _O_tarea = SCHD_CONTROL_TAREA(s_nombreFuncion = 'calcular_saldo_clientes')

        _D_results = _O_tarea.encolar_tarea(L_args = request.args, D_vars = request.vars, n_timeout = 60 * 60 * 2)
        _D_return.combinar(_D_results)
        # _dbRows_clientes = dbc01(
        #     (dbc01.tempresa_clientes.empresa_id == _D_args.s_empresa_id)
        #     ).select(dbc01.tempresa_clientes.id)
        #
        # for _dbRow in _dbRows_clientes:
        #
        #     _D_results = TCFDIS.EDOCTA(
        #         _D_args.s_empresa_id,
        #         _dbRow.id,
        #         E_movto_generado_por = TCFDI_MOVIMIENTOS.E_GENERADO_POR.RECALCULO_CLIENTE
        #         ).procesar_cliente()
        #
        #     _D_insert_cliente_log = Storage(
        #         empresa_cliente_id = _dbRow.id,
        #         fecha = request.browsernow,
        #         descripcion = "Cálculo de saldo manual 2",
        #         tipotransaccion = TEMPRESA_CLIENTE_LOG.TIPOTRANSACCION.CALCULODESALDO,
        #         errores = "\n".join(_D_results.L_msgError),
        #         warnings = "\n".join(_D_results.L_msgWarning),
        #         info = "\n".join(_D_results.L_msgInfo),
        #         resultado = _D_results.E_return
        #         )
        #
        #     dbc01.tempresa_cliente_log.insert(**_D_insert_cliente_log)
        #
        #     dbc01.commit()

        _D_useView = request.stv_fwk_permissions.view_refresh

    else:
        pass

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbTable = _dbTabla,
        dbTableMaster = dbc01.tempresas,
        xFieldLinkMaster = [_dbTabla.empresa_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01,
            request.stv_fwk_permissions.special_permisson_02,
            request.stv_fwk_permissions.special_permisson_03,
            request.stv_fwk_permissions.special_permisson_04,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTabla.id,
            _dbTabla.codigo, _dbTabla.cliente_codigo_contpaqi,
            _dbTabla.razonsocial, _dbTabla.rfc, _dbTabla.nombrecorto,
            _dbTabla.empresa_grupo_id,
            _dbTabla.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTabla.id, _dbTabla.codigo,
            _dbTabla.razonsocial, _dbTabla.rfc, _dbTabla.nombrecorto
            ],
        L_fieldsToSearchIfDigit = [
            _dbTabla.id, _dbTabla.codigo
            ],
        b_preventDoubleUpdate = False
        )

    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION, _CLS_TABLA.al_validar)
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED, _CLS_TABLA.AL_ACEPTAR)
    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE, _CLS_TABLA.ANTES_ELIMINAR,
        dbRow = None  # Llenado por el FWK
        )
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.AFTER_REMOVE, _CLS_TABLA.DESPUES_ELIMINAR)

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView,
        s_overrideCode = _s_overrideCode
        )

    if _D_args.s_accion == request.stv_fwk_permissions.special_permisson_01.code:
        _O_cat.addSubTab(
            s_tabName = 'CFDIs Ingresos',
            s_url = URL(
                a = 'app_timbrado', c = '130clientes_cfdis',
                f = 'cliente_cfdis_ingresos',
                args = _D_args.L_argsBasicos + ['master_' + str(request.function), _D_args.n_id]
                ),
            s_idHtml = fn_createIDTabUnique(
                a = 'app_timbrado', c = '130clientes_cfdis',
                f = 'cliente_cfdis_ingresos',
                s_masterIdentificator = str(request.function)
                )
            )

        # _O_cat.addSubTab(
        #     s_tabName = 'CFDIs Anticipos',
        #     s_url = URL(
        #         a = 'app_timbrado', c = '130clientes_cfdis',
        #         f = 'cliente_cfdis_anticipos',
        #         args = _D_args.L_argsBasicos + ['master_' + str(request.function), _D_args.n_id]
        #         ),
        #     s_idHtml = fn_createIDTabUnique(
        #         a = 'app_timbrado', c = '130clientes_cfdis',
        #         f = 'cliente_cfdis_anticipos',
        #         s_masterIdentificator = str(request.function)
        #         )
        #     )

        _O_cat.addSubTab(
            s_tabName = 'CFDIs Egresos',
            s_url = URL(
                a = 'app_timbrado', c = '130clientes_cfdis',
                f = 'cliente_cfdis_egresos',
                args = _D_args.L_argsBasicos + ['master_' + str(request.function), _D_args.n_id]
                ),
            s_idHtml = fn_createIDTabUnique(
                a = 'app_timbrado', c = '130clientes_cfdis',
                f = 'cliente_cfdis_egresos',
                s_masterIdentificator = str(request.function)
                )
            )

        _O_cat.addSubTab(
            s_tabName = 'CFDIs Pagos',
            s_url = URL(
                a = 'app_timbrado', c = '130clientes_cfdis',
                f = 'cliente_cfdis_pagos',
                args = _D_args.L_argsBasicos + ['master_' + str(request.function), _D_args.n_id]
                ),
            s_idHtml = fn_createIDTabUnique(
                a = 'app_timbrado', c = '130clientes_cfdis',
                f = 'cliente_cfdis_pagos',
                s_masterIdentificator = str(request.function)
                )
            )

    else:
        _O_cat.addSubTab(
            dbTableDetail = dbc01.tempresa_cliente_domicilios,
            s_url = URL(
                a = 'app_empresas', c = '130clientes',
                f = 'empresa_cliente_domicilios',
                args = _D_args.L_argsBasicos + ['master_' + str(request.function), _D_args.n_id]
                ),
            s_idHtml = fn_createIDTabUnique(
                a = 'app_empresas', c = '130clientes',
                f = 'empresa_cliente_domicilios',
                s_masterIdentificator = str(request.function)
                )
            )

        _O_cat.addSubTab(
            dbTableDetail = dbc01.tempresa_cliente_guiasclientes,
            s_url = URL(
                a = 'app_empresas', c = '130clientes',
                f = 'empresa_cliente_guiasclientes',
                args = _D_args.L_argsBasicos + ['master_' + str(request.function), _D_args.n_id]
                ),
            s_idHtml = fn_createIDTabUnique(
                a = 'app_empresas', c = '130clientes',
                f = 'empresa_cliente_guiasclientes',
                s_masterIdentificator = str(request.function)
                )
            )

        _O_cat.addSubTab(
            dbTableDetail = dbc01.tempresa_cliente_clasificaciones,
            s_url = URL(
                a = 'app_empresas', c = '130clientes',
                f = 'empresa_cliente_clasificaciones',
                args = _D_args.L_argsBasicos + ['master_' + str(request.function), _D_args.n_id]
                ),
            s_idHtml = fn_createIDTabUnique(
                a = 'app_empresas', c = '130clientes',
                f = 'empresa_cliente_clasificaciones',
                s_masterIdentificator = str(request.function)
                )
            )

    _D_returnVars['htmlid_credito'] = _O_cat.addSubTab(
        dbTableDetail = None,
        s_url = False,
        s_tabName = "Crédito",
        s_idHtml = None,
        s_type = "extended"
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_cliente_log,
        s_url = URL(
            a = 'app_empresas', c = '130clientes',
            f = 'empresa_cliente_log',
            args = _D_args.L_argsBasicos + ['master_' + str(request.function), _D_args.n_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_empresas', c = '130clientes',
            f = 'empresa_cliente_log',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_type = "button",
        s_actualizar = "stv_gIDTabMain",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_edit),
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_02.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_02.code,
        s_type = "button",
        s_actualizar = "stv_gIDTabMain",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_edit),
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_03.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_03.code,
        s_type = "button",
        s_actualizar = "stv_gIDTabMain",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_edit),
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_04.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_04.code,
        s_type = "button",
        s_actualizar = "stv_gIDTabMain",
        L_enabledByView = [request.stv_fwk_permissions.view_emptyForm],
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.btn_print.code,
        s_permissionCode = request.stv_fwk_permissions.btn_print.code + '_modal',
        s_type = 'button',
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_edit),
        D_actions = Storage(
            modal = Storage(
                s_url = URL(
                    f = 'imprimir_cliente',
                    args = _D_args.L_argsBasicos,
                    # vars = Storage()
                    ),
                s_field = "",
                )
            ),
        )

    generaMensajeFlash(_dbTabla, _D_return)
    return _D_returnVars


def cliente_cfdis_ingresos():
    """ Formulario de CFDIs de ingresos.

    @descripcion CFDIs de Ingresos

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Cliente
    @keyword arg3: empresa_cliente_id.
    @keyword [arg4]: operación a realizar en el formulario.
    @keyword [arg5]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_print
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-file-code] Generar XML del CFDI
    @permiso special_permisson_02:[fas fa-stamp] Timbrar CFDI
    @permiso special_permisson_03:[fas fa-ban] Cancelar CFDI de ingreso
    """
    _D_return = FUNC_RETURN()

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 4,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_cliente_id = request.args(3, 0),
        s_cfdi_id = request.args(5, None),
        )
    _dbTabla = dbc01.tcfdis
    _D_useView = None
    _CLS_TABLA = TCFDIS.INGRESO(empresa_id = _D_args.s_empresa_id, receptorcliente_id = _D_args.s_cliente_id)

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):
        # Si esta editando o guardando una edición, se configuran los campos del CFDI
        _D_result = _CLS_TABLA.configuracampos_edicion(x_cfdi = _D_args.s_cfdi_id)
        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_result)
            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            pass

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _D_result = _CLS_TABLA.configuracampos_nuevo(s_cfdi_id = _D_args.n_id)

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_result)
            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            pass

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.special_permisson_01.code,
            ):

        _D_result = STV_LIB_CFDI.REPRESENTAR_JSON(
            n_empresa_id = int(_D_args.s_empresa_id),
            n_cfdi_id = int(_D_args.s_cfdi_id)
            ).procesar(
                b_grabar = True
                )

        if _D_result.E_return == stvfwk2_e_RETURN.OK:
            stv_flashSuccess(
                'CFDI', "Creación de XML correcta",
                s_mode = 'stv_flash',
                D_addProperties = {}
                )

        else:
            stv_flashError(
                'CFDI', "Creación de XML incorrecta: " + _D_result.s_msgError,
                s_mode = 'stv_flash',
                D_addProperties = {}
                )

        _D_useView = request.stv_fwk_permissions.view_refresh

    elif _D_args.s_accion == request.stv_fwk_permissions.special_permisson_02.code:
        # Se intenta timbrar el CFDI

        _D_results = TCFDIS.TIMBRADO.TIMBRAR(x_cfdi = _D_args.s_cfdi_id)
        _D_return.update(_D_results)
        _D_useView = request.stv_fwk_permissions.view_refresh

    elif _D_args.s_accion == request.stv_fwk_permissions.special_permisson_03.code:
        # Se intenta cancelar el CFDI

        _D_results = TCFDIS.TIMBRADO.CANCELAR(x_cfdi = _D_args.s_cfdi_id)
        _D_return.update(_D_results)
        _D_useView = request.stv_fwk_permissions.view_refresh

    else:
        pass

    # Principal filtro de CFDIs a mostrar
    _dbQrySearch = (_dbTabla.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO)

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_dbQrySearch),
        dbTable = _dbTabla,
        dbTableMaster = dbc01.tempresa_clientes,
        xFieldLinkMaster = [_dbTabla.receptorcliente_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01,
            request.stv_fwk_permissions.special_permisson_02,
            request.stv_fwk_permissions.special_permisson_03,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTabla.id,
            _dbTabla.generado_por, _dbTabla.serie, _dbTabla.folio, _dbTabla.estado,
            _dbTabla.fecha, _dbTabla.fechatimbrado, _dbTabla.sat_status, _dbTabla.metodopago,
            _dbTabla.formapago_id, _dbTabla.credito_plazo, _dbTabla.v_vencimiento,
            _dbTabla.v_estatuscxc, _dbTabla.v_dvencido,
            _dbTabla.total, _dbTabla.saldo, _dbTabla.moneda,  # _dbTabla.receptorrfc,
            _dbTabla.emp_pla_suc_almacen_id,
            # _dbTabla.receptornombrerazonsocial,
            # _dbTabla.emisorrfc, _dbTabla.emisornombrerazonsocial
            ],
        L_fieldsToSearch = [
            _dbTabla.serie, _dbTabla.folio, _dbTabla.fecha, _dbTabla.fechatimbrado, _dbTabla.total,
            _dbTabla.lugarexpedicion, _dbTabla.receptorrfc, _dbTabla.uuid,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTabla.id, _dbTabla.tipocomprobante_id, _dbTabla.folio, _dbTabla.total,
            _dbTabla.lugarexpedicion,
            ],
        L_fieldnameIds = [_dbTabla.id],
        x_offsetInArgs = _D_args.n_offsetArgs - 2,
        b_preventDoubleUpdate = False  # Debido a que se puede autocalcular la info con los detalles
        )

    _O_cat.addRowSearchResultsProperty(
        STV_FWK_FORM.ROWSEARCHRESULTS.PROPERTIES.ORDERBY,
        [~_dbTabla.fecha]
        )

    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION, _CLS_TABLA.al_validar)
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED, _CLS_TABLA.AL_ACEPTAR)
    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE, _CLS_TABLA.ANTES_ELIMINAR,
        dbRow = None  # Llenado por el FWK
        )
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.AFTER_REMOVE, _CLS_TABLA.DESPUES_ELIMINAR)

    _O_cat.addRowSearchResults_addColumnBegin(
        es_typeColumn = STV_FWK_FORM.ES_TYPECOLUMN.S_PLUS,
        s_colname = "plus",
        D_field = Storage(
            readable = True
            ),
        m_classHtml = lambda D_column, dbRow: "stv_rowresults_plus",
        D_data = Storage(
            url = URL(
                f = 'cliente_cfdi_movimientos',
                args = ['master_' + str(request.function)]
                )
            )
        )

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView
        )

    # Si la forma fue aceptada
    if not _D_args.n_id and _D_returnVars.rowContent_form and _D_returnVars.rowContent_form.accepted:
        dbc01.commit()

        # Se importa gluon.storage para poder crear un objeto especial de lista para request.args
        from gluon.storage import List
        request.args = List(
            _D_args.L_argsBasicos + [
                request.stv_fwk_permissions.btn_edit.code,
                str(_D_returnVars.rowContent_form.vars.id)
                ]
            )
        request._post_vars = Storage()
        _D_returnVars = cliente_cfdis_ingresos()
        return _D_returnVars
    else:
        pass

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_conceptos,
        s_url = URL(
            a = 'app_timbrado', c = '130clientes_cfdis',
            f = 'cfdi_conceptos',
            args = _D_args.L_argsBasicos + ['master_' + str(request.function), _D_args.n_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado', c = '130clientes_cfdis',
            f = 'cfdi_conceptos',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_relacionados,
        s_url = URL(
            a = 'app_timbrado', c = '130clientes_cfdis',
            f = 'cfdi_relacionados',
            args = _D_args.L_argsBasicos + ['master_' + str(request.function), _D_args.n_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado', c = '130clientes_cfdis',
            f = 'cfdi_relacionados',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_manejosaldos,
        s_url = URL(
            a = 'app_timbrado', c = '130clientes_cfdis',
            f = 'cfdi_manejosaldos',
            args = _D_args.L_argsBasicos + ['master_' + str(request.function), _D_args.n_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado', c = '130clientes_cfdis',
            f = 'cfdi_manejosaldos',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc02.tcfdi_xmls,
        s_url = URL(
            a = 'app_timbrado', c = '250cfdi',
            f = 'cfdi_xml',
            args = ['master_' + str(request.function), _D_args.n_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado', c = '250cfdi',
            f = 'cfdi_xml',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_prepolizas,
        s_url = URL(
            a = 'app_timbrado', c = '255prepolizas_polizas',
            f = 'prepoliza',
            args = ['master_' + str(request.function), _D_args.n_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado', c = '255prepolizas_polizas',
            f = 'prepoliza',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_movimientos,
        s_url = URL(
            a = 'app_timbrado', c = '130clientes_cfdis',
            f = 'cliente_cfdi_movimientos',
            args = ['master_' + str(request.function), _D_args.n_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado', c = '130clientes_cfdis',
            f = 'cliente_cfdi_movimientos',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_type = "button",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_edit),
        )
    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_02.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_02.code,
        s_type = "button",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_edit),
        )
    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_03.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_03.code,
        s_type = "button",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_edit),
        )
    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.btn_print.code,
        s_permissionCode = request.stv_fwk_permissions.btn_print.code + '_modal',
        s_type = 'button',
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_edit),
        D_actions = Storage(
            modal = Storage(
                s_url = URL(
                    a = "app_timbrado", c = "250cfdi",
                    f = 'imprimir_cfdi',
                    args = [],
                    # vars = Storage()
                    ),
                s_field = "",
                )
            ),
        )

    if _D_args.s_accion in (
            request.stv_fwk_permissions.btn_save.code,
            request.stv_fwk_permissions.btn_remove.code,
            ):
        # Si hubo cambios en el contenido se actualiza el maestro

        _O_cat.agregar_comando(
            E_comando = STV_FWK_FORM.E_COMANDOS.REFRESCAR_CAMPOS_MAESTRO,
            D_parametros = Storage(
                L_camposActualizar = STV_LIB_DB.COMANDO_REFRESCAR(
                    _D_args.s_cliente_id,
                    dbc01.tempresa_clientes,
                    [
                        dbc01.tempresa_clientes.credito_saldo,
                        dbc01.tempresa_clientes.credito_saldovencido,
                        dbc01.tempresa_clientes.credito_saldopendtimbrar,
                        dbc01.tempresa_clientes.credito_saldotimbrado,
                        ]
                    )
                )
            )
    else:
        pass

    generaMensajeFlash(_dbTabla, _D_return)
    return _D_returnVars


def cliente_cfdi_movimientos():
    """ Formulario de movimientos de un CFDI de ingresos.

    @descripcion Movimientos

    @keyword arg0: CFDI
    @keyword arg1: cfdi_id.
    @keyword [arg6]: operación a realizar en el formulario.
    @keyword [arg7]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    #@permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    #@permiso btn_signature
    @permiso special_permisson_01:[fas fa-file-code] Agregar Pago
    @permiso special_permisson_02:[fas fa-stamp] Agregar Descuento
    @permiso special_permisson_03:[fas fa-ban] Agregar Devolución
    @permiso special_permisson_04:[fas fa-ban] Agregar Bonificación
    """

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 2,
        s_codigoDefault = request.stv_fwk_permissions.btn_new.code,
        s_cfdi_id = request.args(1, 0),
        s_movto_id = request.args(3, 0),
        )

    _dbTabla = dbc01.tcfdi_movimientos
    _dbRow_cfdi = dbc01.tcfdis(_D_args.s_cfdi_id)
    _E_tipoComprobante = None
    if _dbRow_cfdi:
        _E_tipoComprobante = _dbRow_cfdi.tipocomprobante_id
    else:
        pass

    _dbQry = None

    _dbTabla.fecha.represent = stv_represent_date
    dbc01.tcfdis.fechatimbrado.represent = stv_represent_date
    _dbTabla.cfdi_id.label = "Folio"
    _dbTabla.cfdi_ingreso_id.label = "Folio"
    _dbTabla.fechaingreso.represent = stv_represent_date
    if _E_tipoComprobante == TTIPOSCOMPROBANTES.INGRESO:
        # Va a buscar los elementos detalles de este campo como argumentno
        _dbField_enbusqueda = _dbTabla.cfdi_ingreso_id

        _dbQry = (dbc01.tcfdis.id == dbc01.tcfdi_movimientos.cfdi_id)  # Usado para asociar campos a desplegar

        _L_fieldsAMostrar = [
            _dbTabla.id, _dbTabla.orden, _dbTabla.fecha, dbc01.tcfdis.generado_por, _dbTabla.tipodocumento, _dbTabla.cfdi_id,
            dbc01.tcfdis.sat_status, dbc01.tcfdis.fechatimbrado,
            _dbTabla.accion, _dbTabla.cargo, _dbTabla.abono, _dbTabla.saldo,
            _dbTabla.monedacontpaqi_id, _dbTabla.importe_monedapago, _dbTabla.tipocambio,
            _dbTabla.nota,
            # dbc01.tcfdis.estado,
            ]

        # Se despliegan los botones si se esta consultando un cfdi de ingreso
        _L_botones = [
            request.stv_fwk_permissions.special_permisson_01,
            request.stv_fwk_permissions.special_permisson_02,
            request.stv_fwk_permissions.special_permisson_03,
            request.stv_fwk_permissions.special_permisson_04,
            ]
        _L_botones += request.stv_fwk_permissions.L_formButtonsAll

    else:
        _dbField_enbusqueda = _dbTabla.cfdi_id  # Va a buscar los elementos detalles de este campo como argumentno

        _dbQry = (dbc01.tcfdis.id == dbc01.tcfdi_movimientos.cfdi_ingreso_id)  # Usado para asociar campos a desplegar

        if _E_tipoComprobante == TTIPOSCOMPROBANTES.PAGO:
            _L_fieldsAMostrar = [
                _dbTabla.id, _dbTabla.fechaingreso, dbc01.tcfdis.generado_por,  # dbc01.tcfdis.tipodocumento,
                _dbTabla.cfdi_ingreso_id,
                dbc01.tcfdis.sat_status, dbc01.tcfdis.fechatimbrado,
                _dbTabla.accion, _dbTabla.cargo, _dbTabla.abono,  # _dbTabla.saldo,
                _dbTabla.nota,
                # dbc01.tcfdis.estado,
                ]

        elif _E_tipoComprobante == TTIPOSCOMPROBANTES.EGRESO:
            _L_fieldsAMostrar = [
                _dbTabla.id, _dbTabla.fechaingreso, dbc01.tcfdis.generado_por,  # dbc01.tcfdis.tipodocumento,
                _dbTabla.cfdi_ingreso_id,
                dbc01.tcfdis.sat_status, dbc01.tcfdis.fechatimbrado,
                _dbTabla.accion, _dbTabla.cargo, _dbTabla.abono,  # _dbTabla.saldo,
                _dbTabla.nota,
                # dbc01.tcfdis.estado,
                ]

        else:
            _L_fieldsAMostrar = [
                _dbTabla.id, _dbTabla.fechaingreso, dbc01.tcfdis.generado_por,  # dbc01.tcfdis.tipodocumento,
                _dbTabla.cfdi_ingreso_id,
                dbc01.tcfdis.sat_status, dbc01.tcfdis.fechatimbrado,
                _dbTabla.accion, _dbTabla.cargo, _dbTabla.abono,  # _dbTabla.saldo,
                _dbTabla.nota,
                # dbc01.tcfdis.estado,
                ]

        # No despliega boton de nuevo, si se estan consultando un cfdi de egreso o pago
        _L_botones = request.stv_fwk_permissions.L_formButtonsMinimal

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_dbQry),
        dbTable = _dbTabla,
        dbTableMaster = dbc01.tcfdis,
        xFieldLinkMaster = [_dbField_enbusqueda],
        L_visibleButtons = _L_botones,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsAMostrar,
        L_fieldsToSearch = [_dbTabla.descripcion, ],
        L_fieldsToSearchIfDigit = [_dbTabla.id, ],
        x_offsetInArgs = _D_args.n_offsetArgs - 2,
        # Es_behavior = STV_FWK_FORM.ES_BEHAVIOR.S_PLUS_FORM
        )

    _O_cat.addRowSearchResultsProperty(
        'orderBy',
        [_dbTabla.orden]
        )

    if _D_args.s_accion in (request.stv_fwk_permissions.btn_view.code,):

        # Para ver y llamar el visualizador de CFDI, se obtiene el movimiento de ingreso
        _dbRow_movimiento = dbc01.tcfdi_movimientos(_D_args.n_id)
        if str(_dbRow_movimiento.cfdi_id) == _D_args.s_cfdi_id:
            _s_cfdiAccion_id = _dbRow_movimiento.cfdi_ingreso_id
        else:
            _s_cfdiAccion_id = _dbRow_movimiento.cfdi_id

        redirect(
            URL(c = '250cfdi', f = 'cfdisemitidos', args = [_D_args.s_accion, _s_cfdiAccion_id], vars = request.vars)
            )

    else:

        _D_returnVars = _O_cat.process(
            D_overrideView = None
            )

        _D_returnVars.D_useView.b_serverCreatesTab = True

    if _E_tipoComprobante == TTIPOSCOMPROBANTES.INGRESO:
        _O_cat.addPermissionConfig(
            s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
            s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
            s_permissionLabel = '',
            s_permissionIcon = '',
            s_type = 'button',
            s_send = '',
            L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_new),
            D_actions = Storage(
                modal = Storage(
                    s_url = URL(
                        f = 'cliente_cfdi_movimiento_agregarpago',
                        args = _D_args.L_argsBasicos
                        ),
                    s_field = "", )
                ),
            )

        _O_cat.addPermissionConfig(
            s_initialCode = request.stv_fwk_permissions.special_permisson_02.code,
            s_permissionCode = request.stv_fwk_permissions.special_permisson_02.code,
            s_permissionLabel = '',
            s_permissionIcon = '',
            s_type = 'button',
            s_send = '',
            L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_new),
            D_actions = Storage(
                modal = Storage(
                    s_url = URL(
                        f = 'cliente_cfdi_movimiento_agregardescuento',
                        args = _D_args.L_argsBasicos
                        ),
                    s_field = "", )
                ),
            )

        _O_cat.addPermissionConfig(
            s_initialCode = request.stv_fwk_permissions.special_permisson_03.code,
            s_permissionCode = request.stv_fwk_permissions.special_permisson_03.code,
            s_permissionLabel = '',
            s_permissionIcon = '',
            s_type = 'button',
            s_send = '',
            L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_new),
            D_actions = Storage(
                modal = Storage(
                    s_url = URL(
                        f = 'cliente_cfdi_movimiento_agregarbonificacion',
                        args = _D_args.L_argsBasicos
                        ),
                    s_field = "", )
                ),
            )

        _O_cat.addPermissionConfig(
            s_initialCode = request.stv_fwk_permissions.special_permisson_04.code,
            s_permissionCode = request.stv_fwk_permissions.special_permisson_04.code,
            s_permissionLabel = '',
            s_permissionIcon = '',
            s_type = 'button',
            s_send = '',
            L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_new),
            D_actions = Storage(
                modal = Storage(
                    s_url = URL(
                        f = 'cliente_cfdi_movimiento_agregardevolucion',
                        args = _D_args.L_argsBasicos
                        ),
                    s_field = "", )
                ),
            )
    else:
        pass

    return _D_returnVars


def cliente_cfdi_movimiento_agregarpago():
    """ Formulario que agrega pago a un cfdi

    @descripcion Movimiento Agregar Pago

    @keyword arg0: CFDI
    @keyword arg1: cfdi_id.
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    #@permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    #@permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    #@permiso btn_find
    #@permiso btn_findall
    #@permiso btn_signature
    """

    def al_validar(O_form, D_args, O_cfdiPago):
        """ Validación
        """

        O_form = O_cfdiPago.al_validar(O_form)

        _O_cfdiPago_compPago = TCFDI_COMPLEMENTOPAGOS.PROC(cfdi_id = None)  # Ya que no se a creado el CFDI de pago
        O_form = _O_cfdiPago_compPago.al_validar(O_form, validacionDummy = True)

        _CLS_TABLA = TCFDI_COMPLEMENTOPAGO_DOCTOSRELACIONADOS.PROC(cfdi_complementopago_id = None)
        O_form.vars.cfdirelacionado_id = D_args.s_cfdi_id
        _O_form = _CLS_TABLA.al_validar(O_form, validacionDummy = True)

        return O_form

    def al_aceptar(O_form, D_args, s_cliente_id):
        _D_return = FUNC_RETURN()

        # Los campos de tcfdis ya estan configurados con los defaults

        _D_insert_cfdi = Storage(
            # empresa_id = _D_args.s_empresa_id, Ya tiene el valor desde el default
            fecha = O_form.vars.fecha,
            empresa_plaza_sucursal_cajachica_id = O_form.vars.empresa_plaza_sucursal_cajachica_id,
            receptorcliente_id = s_cliente_id,
            )

        _D_insert_cfdi.id = dbc01.tcfdis.insert(**_D_insert_cfdi)
        TCFDIS.PAGO.AL_ACEPTAR(O_form = Storage(vars = _D_insert_cfdi))

        _O_cfdiPago_compPago = TCFDI_COMPLEMENTOPAGOS.PROC(cfdi_id = _D_insert_cfdi.id)
        _D_result = _O_cfdiPago_compPago.configuracampos_nuevo(s_cfdi_comppago_id = None)
        _D_return.combinar(_D_result)

        _D_insert_cfdi_pago = Storage(
            # cfdi_id = _D_insert_cfdi.id,  # Se configura en los valores por default
            fechapago = O_form.vars.fechapago,
            formapago_id = O_form.vars.formapago_id,
            monedacontpaqi_id = O_form.vars.monedacontpaqi_id,
            tipocambio = O_form.vars.tipocambio,
            monto = O_form.vars.monto,
            )

        _D_insert_cfdi_pago.id = dbc01.tcfdi_complementopagos.insert(
            **_D_insert_cfdi_pago
            )
        TCFDI_COMPLEMENTOPAGOS.PROC.AL_ACEPTAR(O_form = Storage(vars = _D_insert_cfdi_pago))

        _O_cfdiPago_compPago_doctoRel = TCFDI_COMPLEMENTOPAGO_DOCTOSRELACIONADOS.PROC(
            cfdi_complementopago_id = _D_insert_cfdi_pago.id
            )
        _D_result = _O_cfdiPago_compPago_doctoRel.configuracampos_nuevo(s_cfdi_comppago_docrel_id = None)
        _D_return.combinar(_D_result)

        _D_insert_cfdi_pago_docRelacionado = Storage(
            # cfdi_complementopago_id = _D_insert_cfdi_pago.id,  # Se configura en los valores por default
            cfdirelacionado_id = D_args.s_cfdi_id,
            imppagado = O_form.vars.monto,
            )

        _D_insert_cfdi_pago_docRelacionado.id = dbc01.tcfdi_complementopago_doctosrelacionados.insert(
            **_D_insert_cfdi_pago_docRelacionado
            )
        TCFDI_COMPLEMENTOPAGO_DOCTOSRELACIONADOS.PROC.AL_ACEPTAR(
            O_form = Storage(vars = _D_insert_cfdi_pago_docRelacionado)
            )

        stv_flashSuccess(
            'Ingresos',
            "Pago generado",
            s_mode = 'stv_flash',
            D_addProperties = {}
            )

        generaMensajeFlash(dbc01.tcfdis, _D_return)
        return O_form

    _D_return = FUNC_RETURN()

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 2,
        s_codigoDefault = request.stv_fwk_permissions.btn_new.code,
        s_cfdi_id = request.args(1, 0),
        )

    _dbTabla = dbc01.tcfdis
    _dbRow_cfdi = _dbTabla(_D_args.s_cfdi_id)
    dbc01.tcfdi_complementopagos.fechapago.default = request.browsernow
    dbc01.tcfdi_complementopagos.monto.default = _dbRow_cfdi.saldo

    _O_cfdiPago = TCFDIS.PAGO(empresa_id = _dbRow_cfdi.empresa_id, receptorcliente_id = _dbRow_cfdi.receptorcliente_id)
    _D_result = _O_cfdiPago.configuracampos_nuevo(s_cfdi_id = None)
    _D_return.combinar(_D_result)

    _L_formFactoryFields = [
        dbc01.tcfdis.fecha,
        dbc01.tcfdis.empresa_plaza_sucursal_cajachica_id,
        dbc01.tcfdi_complementopagos.formapago_id,
        dbc01.tcfdi_complementopagos.monedacontpaqi_id,
        dbc01.tcfdi_complementopagos.tipocambio,
        dbc01.tcfdi_complementopagos.fechapago,
        dbc01.tcfdi_complementopagos.monto,
        ]

    # Se pone el writable de todos los campos del CFDI en False.
    for _dbField in _L_formFactoryFields:
        _dbField.writable = True

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbTable = None,
        L_visibleButtons = [request.stv_fwk_permissions.btn_none, request.stv_fwk_permissions.btn_save],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        b_applyPermissons = True,
        x_offsetInArgs = _D_args.n_offsetArgs
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION, al_validar, D_args = _D_args, O_cfdiPago = _O_cfdiPago
        )
    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED, al_aceptar, D_args = _D_args,
        s_cliente_id = _dbRow_cfdi.receptorcliente_id
        )

    _D_returnVars = _O_cat.process(
        s_overrideTitle = "Agregando Pago",
        b_skipAction = True,
        L_formFactoryFields = _L_formFactoryFields,
        dbRow_formFactory = None
        )

    if _D_returnVars.rowContent_form.accepted:

        _D_useView = request.stv_fwk_permissions.view_refreshParent
        _D_returnVars.D_useView = _D_useView
        _O_cat.agregar_comando(
            E_comando = STV_FWK_FORM.E_COMANDOS.CERRAR_MODAL,
            D_parametros = Storage()
            )

    else:
        pass

    if _dbRow_cfdi.sat_status != TCFDIS.SAT_STATUS.VIGENTE:
        _D_return.agrega_error("CFDI no se encuentra en un estatus que permita generar pagos")
        _O_cat.agregar_comando(
            E_comando = STV_FWK_FORM.E_COMANDOS.CERRAR_MODAL,
            D_parametros = Storage()
            )
    else:
        pass

    if _D_args.s_accion == request.stv_fwk_permissions.special_permisson_01.code:
        _D_returnVars.D_useView.b_serverCreatesTab = True
        _D_returnVars.D_useView.L_actionsCreateView = [request.stv_fwk_permissions.special_permisson_01.code]

    else:
        pass

    generaMensajeFlash(dbc01.tcfdis, _D_return)
    return _D_returnVars


def cliente_cfdi_movimiento_agregardescuento():
    """ Formulario que agrega descuento a un cfdi

    @descripcion Movimiento Agregar Descuento

    @keyword arg0: CFDI
    @keyword arg1: cfdi_id.
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    #@permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    #@permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    #@permiso btn_find
    #@permiso btn_findall
    #@permiso btn_signature
    """

    def al_validar(O_form, D_args, O_cfdiEgreso):
        """ Validación
        """
        _ = D_args
        O_form = O_cfdiEgreso.al_validar(O_form)

        # _O_cfdiPago_compPago = TCFDI_COMPLEMENTOPAGOS.PROC(cfdi_id = None)  # Ya que no se a creado el CFDI de pago
        # O_form = _O_cfdiPago_compPago.al_validar(O_form, validacionDummy = True)

        return O_form

    def al_aceptar(O_form, D_args, s_cliente_id):
        _D_return = FUNC_RETURN()

        # Los campos de tcfdis ya estan configurados con los defaults
        _D_insert_cfdi = Storage(
            # empresa_id = _D_args.s_empresa_id, Ya tiene el valor desde el default
            fecha = O_form.vars.fecha,
            empresa_plaza_sucursal_cajachica_id = O_form.vars.empresa_plaza_sucursal_cajachica_id,
            receptorcliente_id = s_cliente_id,
            tiporelacion_id = TTIPOSRELACIONES.NOTACREDITO,
            )

        _D_insert_cfdi.id = dbc01.tcfdis.insert(**_D_insert_cfdi)
        TCFDIS.EGRESO.AL_ACEPTAR(O_form = Storage(vars = _D_insert_cfdi))

        # Se configuran los campos del concepto y se inserta
        _O_cfdiEgreso_concepto = TCFDI_CONCEPTOS.EGRESO(cfdi_id = _D_insert_cfdi.id)
        _D_result = _O_cfdiEgreso_concepto.configuracampos_nuevo(s_cfdi_concepto_id = None)
        _D_return.combinar(_D_result)

        _D_insert_cfdi_concepto = Storage(
            # cfdi_id = _D_insert_cfdi.id,  # Se configura en los valores por default
            empresa_prodserv_id = O_form.vars.empresa_prodserv_id,
            valorunitario = O_form.vars.importe,
            valorunitario_capturado = O_form.vars.importe,
            cantidad = 1
            )

        _D_insert_cfdi_concepto.id = dbc01.tcfdi_conceptos.insert(
            **_D_insert_cfdi_concepto
            )
        TCFDI_CONCEPTOS.EGRESO.AL_ACEPTAR(O_form = Storage(vars = _D_insert_cfdi_concepto))

        _dbRow_concepto = dbc01.tcfdi_conceptos(_D_insert_cfdi_concepto.id)

        # Se configuran los campos del doc relacionado y se inserta
        _O_cfdiEgreso_relacionado = TCFDI_RELACIONADOS.PROC(cfdi_id = _D_insert_cfdi.id)
        _D_result = _O_cfdiEgreso_relacionado.configuracampos_nuevo(s_cfdi_relacion_id = None)
        _D_return.combinar(_D_result)

        _D_insert_cfdi_relacionado = Storage(
            # cfdi_id = _D_insert_cfdi.id,  # Se configura en los valores por default
            cfdirelacionado_id = D_args.s_cfdi_id,
            fijarimporte = True,
            importe = _dbRow_concepto.importe,
            )

        _D_insert_cfdi_relacionado.id = dbc01.tcfdi_relacionados.insert(
            **_D_insert_cfdi_relacionado
            )
        TCFDI_RELACIONADOS.PROC.AL_ACEPTAR(O_form = Storage(vars = _D_insert_cfdi_relacionado))

        stv_flashSuccess(
            'Ingresos',
            "Descuento generado",
            s_mode = 'stv_flash',
            D_addProperties = {}
            )

        generaMensajeFlash(dbc01.tcfdis, _D_return)
        return O_form

    _D_return = FUNC_RETURN()

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 2,
        s_codigoDefault = request.stv_fwk_permissions.btn_new.code,
        s_cfdi_id = request.args(1, 0),
        )

    _dbTabla = dbc01.tcfdis
    _dbRow_cfdi = _dbTabla(_D_args.s_cfdi_id)

    _O_cfdiEgreso = TCFDIS.EGRESO(
        empresa_id = _dbRow_cfdi.empresa_id, receptorcliente_id = _dbRow_cfdi.receptorcliente_id
        )
    _D_result = _O_cfdiEgreso.configuracampos_nuevo(s_cfdi_id = None)
    _D_return.combinar(_D_result)

    dbc01.tcfdi_conceptos.empresa_prodserv_id.requires = IS_IN_DB(
        dbc01(
            (dbc01.tempresa_prodserv.empresa_id == D_stvSiteHelper.dbconfigs.defaults.empresa_id)
            & (dbc01.tempresa_prodserv.tipo == TEMPRESA_PRODSERV.E_TIPO.BONIFICACION)
            ),
        'tempresa_prodserv.id',
        dbc01.tempresa_prodserv._format
        )
    dbc01.tcfdi_conceptos.empresa_prodserv_id.widget = lambda f, v: stv_widget_db_chosen(f, v, 1)

    _L_formFactoryFields = [
        dbc01.tcfdis.fecha,
        dbc01.tcfdis.empresa_plaza_sucursal_cajachica_id,
        dbc01.tcfdi_conceptos.empresa_prodserv_id,
        dbc01.tcfdi_relacionados.importe,
        ]

    # Se pone el writable de todos los campos del CFDI en False.
    for _dbField in _L_formFactoryFields:
        _dbField.writable = True

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbTable = None,
        L_visibleButtons = [request.stv_fwk_permissions.btn_none, request.stv_fwk_permissions.btn_save],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        b_applyPermissons = True,
        x_offsetInArgs = _D_args.n_offsetArgs
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION, al_validar, D_args = _D_args, O_cfdiEgreso = _O_cfdiEgreso
        )
    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED, al_aceptar, D_args = _D_args,
        s_cliente_id = _dbRow_cfdi.receptorcliente_id
        )

    _D_returnVars = _O_cat.process(
        s_overrideTitle = "Agregando Descuento",
        b_skipAction = True,
        L_formFactoryFields = _L_formFactoryFields,
        dbRow_formFactory = None
        )

    if _D_returnVars.rowContent_form.accepted:

        _D_useView = request.stv_fwk_permissions.view_refreshParent
        _D_returnVars.D_useView = _D_useView
        _O_cat.agregar_comando(
            E_comando = STV_FWK_FORM.E_COMANDOS.CERRAR_MODAL,
            D_parametros = Storage()
            )

    else:
        pass

    if _dbRow_cfdi.sat_status != TCFDIS.SAT_STATUS.VIGENTE:
        _D_return.agrega_error("CFDI no se encuentra en un estatus que permita generar descuentos")
        _O_cat.agregar_comando(
            E_comando = STV_FWK_FORM.E_COMANDOS.CERRAR_MODAL,
            D_parametros = Storage()
            )
    else:
        pass

    if _D_args.s_accion == request.stv_fwk_permissions.special_permisson_02.code:
        _D_returnVars.D_useView.b_serverCreatesTab = True
        _D_returnVars.D_useView.L_actionsCreateView = [request.stv_fwk_permissions.special_permisson_02.code]

    else:
        pass

    generaMensajeFlash(dbc01.tcfdis, _D_return)
    return _D_returnVars


def cfdi_conceptos():
    """ Formulario detalle de conceptos del cfdi

    @descripcion Conceptos

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Cliente
    @keyword arg3: empresa_cliente_id.
    @keyword arg4: CFDI
    @keyword arg5: cfdi_id.
    @keyword [arg6]: operación a realizar en el formulario.
    @keyword [arg7]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-file-code] Importar excel
    """
    _D_return = FUNC_RETURN()

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 6,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_cliente_id = request.args(3, 0),
        s_cfdi_id = request.args(5, 0),
        s_concepto_id = request.args(7, None)
        )

    _dbTabla = dbc01.tcfdi_conceptos
    _D_useView = None
    _dbRow_cfdi = dbc01.tcfdis(_D_args.s_cfdi_id)
    if _dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.EGRESO:
        _CLS_TABLA = TCFDI_CONCEPTOS.EGRESO(cfdi_id = _D_args.s_cfdi_id)
    elif _dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO:
        _CLS_TABLA = TCFDI_CONCEPTOS.INGRESO(cfdi_id = _D_args.s_cfdi_id)
    else:
        _CLS_TABLA = TCFDI_CONCEPTOS.PROC(cfdi_id = _D_args.s_cfdi_id)

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):
        # Si esta editando o guardando una edición, se configuran los campos del CFDI
        _D_result = _CLS_TABLA.configuracampos_edicion(_D_args.n_id)
        _D_return.combinar(_D_result)

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            pass

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _D_result = _CLS_TABLA.configuracampos_nuevo(s_cfdi_concepto_id = _D_args.n_id)
        _D_return.combinar(_D_result)

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            pass

    else:
        pass

    if _dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO:
        _L_fieldsToShow = [
            _dbTabla.id, _dbTabla.noidentificacion, _dbTabla.descripcion,
            _dbTabla.cantidad, _dbTabla.valorunitario, _dbTabla.importe, _dbTabla.descuento,
            # _dbTabla.aplicacasco_1a1,
            _dbTabla.prodserv_id,  # _dbTabla.empresa_prodserv_id,
            _dbTabla.unidad_id, _dbTabla.orden
            ]
        _L_permisos = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01,
            ]

    elif _dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.EGRESO:
        _L_fieldsToShow = [
            _dbTabla.id, _dbTabla.noidentificacion, _dbTabla.descripcion,
            _dbTabla.cantidad, _dbTabla.valorunitario, _dbTabla.importe,
            # _dbTabla.descuento,
            _dbTabla.aplicacasco_1a1,
            _dbTabla.prodserv_id,  # _dbTabla.empresa_prodserv_id,
            _dbTabla.unidad_id, _dbTabla.orden
            ]
        _L_permisos = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01,
            ]

    elif _dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.PAGO:
        _L_fieldsToShow = [
            _dbTabla.id, _dbTabla.noidentificacion, _dbTabla.descripcion,
            _dbTabla.cantidad, _dbTabla.valorunitario, _dbTabla.importe, _dbTabla.descuento,
            # _dbTabla.aplicacasco_1a1,
            _dbTabla.prodserv_id,  # _dbTabla.empresa_prodserv_id,
            _dbTabla.unidad_id, _dbTabla.orden
            ]
        _L_permisos = request.stv_fwk_permissions.L_formButtonsMinimal

    else:
        _L_fieldsToShow = [
            _dbTabla.id, _dbTabla.noidentificacion, _dbTabla.descripcion,
            _dbTabla.cantidad, _dbTabla.valorunitario, _dbTabla.importe, _dbTabla.descuento,
            # _dbTabla.aplicacasco_1a1,
            _dbTabla.prodserv_id,  # _dbTabla.empresa_prodserv_id,
            _dbTabla.unidad_id, _dbTabla.orden
            ]
        _L_permisos = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01,
            ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTabla,
        dbTableMaster = dbc01.tcfdis,
        xFieldLinkMaster = [_dbTabla.cfdi_id],
        L_visibleButtons = _L_permisos,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = [_dbTabla.claveprodserv, _dbTabla.noidentificacion],
        L_fieldsToSearchIfDigit = [_dbTabla.id, _dbTabla.noidentificacion],
        x_offsetInArgs = _D_args.n_offsetArgs - 2
        )

    _O_cat.addRowSearchResultsLeftJoin(
        dbc01.tempresa_prodserv.on(
            (dbc01.tempresa_prodserv.id == _dbTabla.empresa_prodserv_id)
            )
        )

    _O_cat.addRowSearchResultsProperty(
        STV_FWK_FORM.ROWSEARCHRESULTS.PROPERTIES.ORDERBY,
        [_dbTabla.orden, _dbTabla.id]
        )

    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION, _CLS_TABLA.al_validar)
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED, _CLS_TABLA.AL_ACEPTAR)
    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE, _CLS_TABLA.ANTES_ELIMINAR,
        dbRow = None  # Llenado por el FWK
        )
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.AFTER_REMOVE, _CLS_TABLA.DESPUES_ELIMINAR)

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_concepto_impuestostrasladados,
        s_url = URL(
            f = 'cfdi_concepto_impuestostrasladados',
            args = _D_args.L_argsBasicos + ['master_' + str(request.function), _D_args.n_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'cfdi_concepto_impuestostrasladados',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_concepto_impuestosretenidos,
        s_url = URL(
            f = 'cfdi_concepto_impuestosretenidos',
            args = _D_args.L_argsBasicos + ['master_' + str(request.function), _D_args.n_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'cfdi_concepto_impuestosretenidos',
            s_masterIdentificator = str(request.function)
            )
        )

    if _dbRow_cfdi.tipocomprobante_id in (TTIPOSCOMPROBANTES.INGRESO, TTIPOSCOMPROBANTES.EGRESO):

        _O_cat.addPermissionConfig(
            s_initialCode = request.stv_fwk_permissions.btn_new.code,
            s_permissionCode = request.stv_fwk_permissions.btn_new.code + '_modal',
            s_permissionLabel = 'Agregar conceptos',
            s_permissionIcon = '',
            s_type = 'button',
            s_send = 'find',
            L_enabledByView = None,
            D_actions = Storage(
                modal = Storage(
                    s_url = URL(
                        f = 'cliente_prodservs_buscar',
                        args = _D_args.L_argsBasicos
                        ),
                    s_field = "",
                    )
                ),
            )
    else:
        pass

    if _dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO:
        _O_cat.addSubTab(
            dbTableDetail = dbc01.tcfdi_concepto_descuentos,
            s_url = URL(
                f = 'cfdi_concepto_descuentos',
                args = _D_args.L_argsBasicos + ['master_' + str(request.function), _D_args.n_id]
                ),
            s_idHtml = fn_createIDTabUnique(
                f = 'cfdi_concepto_descuentos',
                s_masterIdentificator = str(request.function)
                )
            )
    else:
        pass

    if _D_args.s_accion in (
            request.stv_fwk_permissions.btn_save.code,
            request.stv_fwk_permissions.btn_remove.code,
            ):
        # Si hubo cambios en el contenido se actualiza el maestro

        _O_cat.agregar_comando(
            E_comando = STV_FWK_FORM.E_COMANDOS.REFRESCAR_CAMPOS_MAESTRO,
            D_parametros = Storage(L_camposActualizar = dbc01.tcfdis.comando_refrescar(_D_args.s_cfdi_id))
            )
    else:
        pass

    generaMensajeFlash(_dbTabla, _D_return)
    return _D_returnVars


def cliente_prodservs_buscar_capturaadicional():
    """ Formulario de captura adicional cuando se quiere agregar un concepto

        @descripcion Captura Adicional (No necesita permisos)

        @keyword arg0: Empresa
        @keyword arg1: empresa_id.
        @keyword arg2: Cliente
        @keyword arg3: empresa_cliente_id.
        @keyword arg4: CFDI
        @keyword arg5: cfdi_id.
        @keyword arg6: ProdServ
        @keyword arg7: empresa_prodserv_id.
        @keyword [arg8]: operación a realizar en el formulario.
        @keyword [arg9]: id en caso de existir relación a un registro en la operación.

        #@permiso btn_none
        #@permiso btn_new
        #@permiso btn_save
        #@permiso btn_cancel
        #@permiso btn_edit
        #@permiso btn_view
        #@permiso btn_suspend
        #@permiso btn_activate
        #@permiso btn_remove
        #@permiso btn_find
        #@permiso btn_findall
        #@permiso btn_signature
        """

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 8,
        s_codigoDefault = request.stv_fwk_permissions.btn_new.code,
        s_empresa_id = request.args(1, 0),
        s_empresa_cliente_id = request.args(3, 0),
        s_cfdi_id = request.args(5, 0),
        s_empresa_prodserv_id = request.args(7, 0),
        )

    _dbTabla = dbc01.tcfdi_conceptos
    _dbRow_cfdi = dbc01.tcfdis(_D_args.s_cfdi_id)

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbTable = None,
        L_visibleButtons = [request.stv_fwk_permissions.btn_none, request.stv_fwk_permissions.btn_save],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        b_applyPermissons = False,
        x_offsetInArgs = 8
        )

    _O_cat.updateTabOption(
        "s_modalTitle",
        "Configurar concepto"
        )

    _dbField_Cantidad = dbc01.tcfdi_conceptos.cantidad
    _dbField_Cantidad.default = 1
    _dbField_Cantidad.requires = IS_DECIMAL_IN_RANGE(1, 1000000)

    _D_prodserv_esCasco = False
    _dbField_ValorUnitario = dbc01.tcfdi_conceptos.valorunitario_capturado
    _dbField_ValorUnitario.default = 1

    _n_cantidad1a1_disponible = 0  # Se pone con zero por default, para su uso al aceptar la inserción
    _L_fieldsForm = [_dbField_Cantidad]

    if _dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO:
        _dbRows_afectaConteoCascos = dbc01(
            (dbc01.tempresa_prodserv.id == _D_args.s_empresa_prodserv_id)
            & (dbc01.tempresa_prodserv.empresa_prodserv_casco1a1_id > 0)
            ).select(
                dbc01.tempresa_prodserv.empresa_prodserv_casco1a1_id
                )

        if _dbRows_afectaConteoCascos:
            _dbField_Casco1a1 = dbc01.tempresa_prodserv.empresa_prodserv_casco1a1_id
            _dbField_Casco1a1.widget = lambda f, v: stv_widget_db_chosen(
                f, v, 1,
                D_additionalAttributes = Storage(
                    n_recomendado_id = _dbRows_afectaConteoCascos.first().empresa_prodserv_casco1a1_id,
                    vacio = 'No aplica'
                    )
                )
            _dbField_Casco1a1.label = "Grupo Casco(s) entregado(s)"
            TEMPRESA_PRODSERV.CONFIGURAR_CAMPO_CASCO1A1(dbField = _dbField_Casco1a1)
            _L_fieldsForm.append(_dbField_Casco1a1)
        else:
            pass

    elif _dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.EGRESO:
        _D_prodserv_esCasco = TEMPRESA_PRODSERV.ES_CASCO(
            s_empresa_prodserv_id = _D_args.s_empresa_prodserv_id
            ).b_resultado

        if not _D_prodserv_esCasco:
            # Si no es casco
            _L_fieldsForm.append(_dbField_ValorUnitario)
        else:
            pass

    else:
        pass

    _O_cat.addRowContentProperty(
        s_property = 'formHidden',
        x_value = {
            'empresa_prodserv_id': _D_args.s_empresa_prodserv_id
            }
        )

    _D_returnVars = _O_cat.process(
        s_overrideTitle = "Configurar concepto",
        s_overrideTitleSingular = "Concepto",
        s_overrideCode = _D_args.s_accion,
        L_formFactoryFields = _L_fieldsForm,
        b_skipAction = True
        )

    if _D_returnVars.D_tabOptions.b_accepted:
        # Validaciones aceptadas

        _D_returnVars.D_useView = request.stv_fwk_permissions.view_refreshParent

        if _dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO:

            _b_entregoCasco = bool(request.vars.empresa_prodserv_casco1a1_id)

            _D_fieldsInsertConcepto = Storage(
                cfdi_id = _D_args.s_cfdi_id,
                orden = 10,
                cantidad = request.vars.cantidad or 1,
                empresa_prodserv_id = _D_args.s_empresa_prodserv_id,
                )

            _D_fieldsInsertConcepto.id = dbc01.tcfdi_conceptos.insert(
                **_D_fieldsInsertConcepto
                )
            TCFDI_CONCEPTOS.INGRESO.AL_ACEPTAR(Storage(vars = _D_fieldsInsertConcepto))

            if _b_entregoCasco:
                _D_precioCasco1a1 = TEMPRESA_PRODSERV.GET_PRECIOVENTA(
                    request.vars.empresa_prodserv_casco1a1_id,
                    f_tipocambio = _dbRow_cfdi.tipocambio
                    )

                _D_fieldsInsertConceptoDescuento = Storage(
                    cfdi_concepto_id = _D_fieldsInsertConcepto.id,
                    tipo = TCFDI_CONCEPTO_DESCUENTOS.E_TIPO.CASCO_1A1,
                    empresa_prodserv_casco_id = request.vars.empresa_prodserv_casco1a1_id,
                    cantidad_cascos = _D_fieldsInsertConcepto.cantidad,
                    descuento = _D_precioCasco1a1.f_precioMinimo,
                    )
                _D_fieldsInsertConceptoDescuento.id = dbc01.tcfdi_concepto_descuentos.insert(
                    **_D_fieldsInsertConceptoDescuento
                    )
                TCFDI_CONCEPTO_DESCUENTOS.PROC.AL_ACEPTAR(Storage(vars = _D_fieldsInsertConceptoDescuento))

            else:
                # No hay descuentos
                pass

        elif _dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.EGRESO:

            _n_cantidadSolicitada = Decimal(request.vars.cantidad or 1)
            _f_valorUnitario = Decimal(request.vars.valorunitario_capturado or 1)

            if _D_prodserv_esCasco:
                _n_cantidad1a1_aceptada = 0
                _n_cantidadExcedente_aceptada = 0

                _D_result = TCFDIS.OBTENER_CASCOS1A1_DISPONIBLES_EN_EGRESO(
                    s_cfdi_id = _D_args.s_cfdi_id, s_ignorar_concepto_id = None
                    )
                _n_cantidad1a1_disponible = (
                    _D_result.n_cantidad_cascos1a1_posible
                    - _D_result.n_cantidad_cascos1a1_usada
                    # - _D_result.n_cantidad_cascos1a1_regresada  Por ahora no se considera regresada en cascos
                    )

                _n_cantidad1a1_aceptada = _n_cantidad1a1_disponible - _n_cantidadSolicitada \
                    if _n_cantidadSolicitada <= _n_cantidad1a1_disponible else 0
                _n_cantidadExcedente_aceptada = _n_cantidadSolicitada - _n_cantidad1a1_aceptada

                if _n_cantidad1a1_aceptada > 0:
                    # En este punto si queda cantidad disponible debe calcularse como 1a1
                    _D_precioCasco1a1 = TEMPRESA_PRODSERV.GET_PRECIOVENTA(
                        _D_args.s_empresa_prodserv_id,
                        f_tipocambio = _dbRow_cfdi.tipocambio
                        )

                    _D_fieldsInsertConcepto = Storage(
                        cfdi_id = _D_args.s_cfdi_id,
                        orden = 10,
                        cantidad = _n_cantidad1a1_aceptada,
                        valorunitario = _D_precioCasco1a1.f_precioMinimo_mxn,
                        valorunitario_capturado = _D_precioCasco1a1.f_precioMinimo_mxn,
                        empresa_prodserv_id = _D_args.s_empresa_prodserv_id,
                        aplicacasco_1a1 = True,
                        )

                    _D_fieldsInsertConcepto.id = dbc01.tcfdi_conceptos.insert(
                        **_D_fieldsInsertConcepto
                        )
                    TCFDI_CONCEPTOS.EGRESO.AL_ACEPTAR(Storage(vars = _D_fieldsInsertConcepto))

                else:
                    # No hay opción de conceptos en precio 1 a 1
                    pass

                if _n_cantidadExcedente_aceptada > 0:
                    _D_precioCascoExcedente = TEMPRESA_PRODSERV.GET_PRECIO_EXCEDENTE(
                        _D_args.s_empresa_prodserv_id,
                        f_tipocambio = _dbRow_cfdi.tipocambio
                        )

                    _D_fieldsInsertConcepto = Storage(
                        cfdi_id = _D_args.s_cfdi_id,
                        orden = 10,
                        cantidad = _n_cantidadExcedente_aceptada,
                        valorunitario = _D_precioCascoExcedente.f_precioMinimo_mxn,
                        valorunitario_capturado = _D_precioCascoExcedente.f_precioMinimo_mxn,
                        empresa_prodserv_id = _D_args.s_empresa_prodserv_id,
                        aplicacasco_1a1 = False,
                        )

                    _D_fieldsInsertConcepto.id = dbc01.tcfdi_conceptos.insert(
                        **_D_fieldsInsertConcepto
                        )
                    TCFDI_CONCEPTOS.EGRESO.AL_ACEPTAR(Storage(vars = _D_fieldsInsertConcepto))
                else:
                    # No hay opción de conceptos a precio excedente
                    pass

            else:

                _D_fieldsInsertConcepto = Storage(
                    cfdi_id = _D_args.s_cfdi_id,
                    orden = 10,
                    cantidad = request.vars.cantidad or 1,
                    valorunitario = _f_valorUnitario,
                    valorunitario_capturado = _f_valorUnitario,
                    empresa_prodserv_id = _D_args.s_empresa_prodserv_id,
                    )

                _D_fieldsInsertConcepto.id = dbc01.tcfdi_conceptos.insert(
                    **_D_fieldsInsertConcepto
                    )
                TCFDI_CONCEPTOS.EGRESO.AL_ACEPTAR(Storage(vars = _D_fieldsInsertConcepto))

        else:
            pass

        _O_cat.agregar_comando(
            E_comando = STV_FWK_FORM.E_COMANDOS.REFRESCAR_CAMPOS_MAESTRO_2,
            D_parametros = Storage(L_camposActualizar = dbc01.tcfdis.comando_refrescar(_D_args.s_cfdi_id))
            )

    else:
        _D_returnVars.D_useView.b_serverCreatesTab = True
        _D_returnVars.D_useView.L_actionsCreateView = [request.stv_fwk_permissions.btn_new.code]

    return _D_returnVars


def cliente_prodservs_buscar():
    """ Forma buscar para productos y servicios.

    Mostrará los más recientes productos comprados por el cliente.

    @descripcion ProdServ Buscar

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Cliente
    @keyword arg3: empresa_cliente_id.
    @keyword arg4: CFDI
    @keyword arg5: cfdi_id.
    @keyword [arg6]: operación a realizar en el formulario.
    @keyword [arg7]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    #@permiso btn_new
    #@permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    @permiso btn_find
    #@permiso btn_findall
    @permiso btn_signature
    @permiso btn_searchcancel
    @permiso btn_searchdone
    """
    _D_return = FUNC_RETURN()

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 6,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_cliente_id = request.args(3, 0),
        s_cfdi_id = request.args(5, 0),
        s_prodserv_id = request.args(7, None),
        )
    _dbTabla = dbc01.tempresa_prodserv
    _D_useView = None

    _qry = None
    _s_action_override = None

    _dbRow_cfdi = dbc01.tcfdis(_D_args.s_cfdi_id)

    _D_result = TCFDIS.PROC.PUEDE_EDITAR(_dbRow_cfdi)

    if _D_result.E_return != stvfwk2_e_RETURN.OK:
        _D_return.combinar(_D_result, "CFDI no se encuentra en un estado que permite modificarlo. %s")
        _s_action_override = request.stv_fwk_permissions.btn_searchcancel.code

    elif _D_args.s_accion == request.stv_fwk_permissions.btn_new.code + '_modal':
        _qry = (dbc01.tcfdis.receptorcliente_id == _D_args.s_cliente_id) \
            & (dbc01.tcfdi_conceptos.cfdi_id == dbc01.tcfdis.id) \
            & (_dbTabla.id == dbc01.tcfdi_conceptos.empresa_prodserv_id)

        _s_action_override = request.stv_fwk_permissions.btn_find.code

    else:
        _qry = (_dbTabla.empresa_id == _D_args.s_empresa_id)
        _s_action_override = None

    if _dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.EGRESO:

        if _dbRow_cfdi.tiporelacion_id == TTIPOSRELACIONES.DEVOLUCION:
            _qry &= _dbTabla.tipo.belongs(TEMPRESA_PRODSERV.E_TIPO.PRODUCTO, )

        elif _dbRow_cfdi.tiporelacion_id in (
                TTIPOSRELACIONES.NOTACREDITO, TTIPOSRELACIONES.NOTADEBITO
                ):
            _qry &= _dbTabla.tipo.belongs(TEMPRESA_PRODSERV.E_TIPO.SERVICIO, TEMPRESA_PRODSERV.E_TIPO.BONIFICACION)

        else:
            _D_return.agrega_error("Tipo relación no permite conceptos")
            _s_action_override = request.stv_fwk_permissions.btn_searchcancel.code

    else:
        pass

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTabla,
        L_visibleButtons = request.stv_fwk_permissions.L_formSearchButtons,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTabla.id, _dbTabla.codigo, _dbTabla.tipo, _dbTabla.descripcion, _dbTabla.marca_id,
            _dbTabla.unidad_id, _dbTabla.prodserv_id, dbc01.tempresa_prodserv_imagenes.imagen,
            ],
        L_fieldsToGroupBy = [],
        L_fieldsToSearch = [_dbTabla.id, _dbTabla.codigo, _dbTabla.descripcion],
        L_fieldsToSearchIfDigit = [_dbTabla.id, _dbTabla.codigo],
        x_offsetInArgs = _D_args.n_offsetArgs
        )

    _O_cat.addRowSearchResultsLeftJoin(
        dbc01.tempresa_prodserv_imagenes.on(dbc01.tempresa_prodserv_imagenes.empresa_prodserv_id == _dbTabla.id)
        )

    _O_cat.addRowSearchResultsProperty(
        STV_FWK_FORM.ROWSEARCHRESULTS.PROPERTIES.b_DISTINCT, True)

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.btn_searchdone.code,
        s_permissionCode = request.stv_fwk_permissions.btn_searchdone.code + '_modal',
        D_actions = Storage(
            D_data = Storage(),
            modal = Storage(
                s_url = URL(
                    f = 'cliente_prodservs_buscar_capturaadicional',
                    args = _D_args.L_argsBasicos,
                    # vars=request.vars
                    ),
                s_field = "",
                )
            )
        )

    _D_returnVars = _O_cat.process(
        s_overrideCode = _s_action_override,
        D_overrideView = _D_useView
        )

    _D_returnVars.D_useView.b_serverCreatesTab = True
    if _D_args.s_accion == request.stv_fwk_permissions.btn_new.code + '_modal':
        _D_returnVars.D_useView.L_actionsCreateView += [_s_action_override]
    else:
        pass

    generaMensajeFlash(_dbTabla, _D_return)
    return _D_returnVars


def user():
    redirect(
        URL(
            a = D_stvFwkCfg.s_nameAccesoApp,
            c = 'default',
            f = 'user',
            vars = request.vars,
            args = request.args
            )
        )


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http ://..../[app]/default/download/[filename]
    """
    return response.download(
        request,
        db
        )


def cfdi_concepto_impuestostrasladados():
    """ Formulario detalle de impuestos trasladados de los conceptos del cfdi

    @descripcion Impuestos Trasladados

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Cliente
    @keyword arg3: empresa_cliente_id.
    @keyword arg4: CFDI
    @keyword arg5: cfdi_id.
    @keyword arg6: Concepto
    @keyword arg7: cfdi_concepto_id.
    @keyword [arg8]: operación a realizar en el formulario.
    @keyword [arg9]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    #@permiso btn_searchcancel,
    #@permiso btn_searchdone,
    """
    _D_return = FUNC_RETURN()

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 8,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_cliente_id = request.args(3, 0),
        s_cfdi_id = request.args(5, 0),
        s_concepto_id = request.args(7, 0),
        s_impuesto_id = request.args(9, None),
        )

    _dbTabla = dbc01.tcfdi_concepto_impuestostrasladados
    _D_useView = None
    _CLS_TABLA = TCFDI_CONCEPTO_IMPUESTOSTRASLADADOS.PROC(cfdi_concepto_id = _D_args.s_concepto_id)

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):
        # Si esta editando o guardando una edición, se configuran los campos del CFDI
        _D_result = _CLS_TABLA.configuracampos_edicion(_D_args.n_id)
        _D_return.combinar(_D_result)

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_useView = request.stv_fwk_permissions.view_refresh
        else:
            pass

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _D_result = _CLS_TABLA.configuracampos_nuevo(
            s_cfdi_concepto_imptras_id = _D_args.n_id,
            )
        _D_return.combinar(_D_result)

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_useView = request.stv_fwk_permissions.view_refresh
        else:
            pass

    else:
        pass

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTabla,
        dbTableMaster = dbc01.tcfdi_conceptos,
        xFieldLinkMaster = [_dbTabla.cfdi_concepto_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTabla.id,  # _dbTabla.base,
            # _dbTabla.impuesto,
            _dbTabla.impuesto_id,  _dbTabla.tipofactor,
            _dbTabla.tasaocuota, _dbTabla.importe,
            _dbTabla.bansuspender
            ],
        L_fieldsToSearch = [_dbTabla.id],
        L_fieldsToSearchIfDigit = [_dbTabla.id],
        x_offsetInArgs = _D_args.n_offsetArgs - 2
        )

    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION, _CLS_TABLA.al_validar)
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED, _CLS_TABLA.AL_ACEPTAR)
    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE, _CLS_TABLA.ANTES_ELIMINAR,
        dbRow = None  # Llenado por el FWK
        )
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.AFTER_REMOVE, _CLS_TABLA.DESPUES_ELIMINAR)

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView
        )

    if _D_args.s_accion in (
            request.stv_fwk_permissions.btn_save.code,
            request.stv_fwk_permissions.btn_remove.code,
            ):
        # Si hubo cambios en el contenido se actualiza el maestro

        _O_cat.agregar_comando(
            E_comando = STV_FWK_FORM.E_COMANDOS.REFRESCAR_CAMPOS_MAESTRO,
            D_parametros = Storage(L_camposActualizar = dbc01.tcfdi_conceptos.comando_refrescar(_D_args.s_concepto_id))
            )
        _O_cat.agregar_comando(
            E_comando = STV_FWK_FORM.E_COMANDOS.REFRESCAR_CAMPOS_MAESTRO_2,
            D_parametros = Storage(L_camposActualizar = dbc01.tcfdis.comando_refrescar(_D_args.s_cfdi_id))
            )
    else:
        pass

    generaMensajeFlash(_dbTabla, _D_return)
    return _D_returnVars


def cfdi_concepto_impuestosretenidos():
    """ Formulario detalle de impuestos retenidos de los conceptos del cfdi

    @descripcion Impuestos Retenidos

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Cliente
    @keyword arg3: empresa_cliente_id.
    @keyword arg4: CFDI
    @keyword arg5: cfdi_id.
    @keyword arg6: Concepto
    @keyword arg7: cfdi_concepto_id.
    @keyword [arg8]: operación a realizar en el formulario.
    @keyword [arg9]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    #@permiso btn_searchcancel,
    #@permiso btn_searchdone,
    """
    _D_return = FUNC_RETURN()

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 8,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_cliente_id = request.args(3, 0),
        s_cfdi_id = request.args(5, 0),
        s_concepto_id = request.args(7, 0),
        s_impuesto_id = request.args(9, None),
        )

    _dbTabla = dbc01.tcfdi_concepto_impuestosretenidos
    _D_useView = None
    _CLS_TABLA = TCFDI_CONCEPTO_IMPUESTOSRETENIDOS.PROC(cfdi_concepto_id = _D_args.s_concepto_id)

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):
        # Si esta editando o guardando una edición, se configuran los campos del CFDI
        _D_result = _CLS_TABLA.configuracampos_edicion(_D_args.n_id)
        _D_return.combinar(_D_result)

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_useView = request.stv_fwk_permissions.view_refresh
        else:
            pass

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _D_result = _CLS_TABLA.configuracampos_nuevo(s_cfdi_concepto_imprete_id = _D_args.n_id)
        _D_return.combinar(_D_result)

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_useView = request.stv_fwk_permissions.view_refresh
        else:
            pass
    else:
        pass

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTabla,
        dbTableMaster = dbc01.tcfdi_conceptos,
        xFieldLinkMaster = [_dbTabla.cfdi_concepto_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTabla.id,  # _dbTabla.base,
            # _dbTabla.impuesto,
            _dbTabla.impuesto_id,  _dbTabla.tipofactor,
            _dbTabla.tasaocuota, _dbTabla.importe,
            _dbTabla.bansuspender
            ],
        L_fieldsToSearch = [_dbTabla.id],
        L_fieldsToSearchIfDigit = [_dbTabla.id],
        x_offsetInArgs = _D_args.n_offsetArgs - 2
        )

    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION, _CLS_TABLA.al_validar)
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED, _CLS_TABLA.AL_ACEPTAR)
    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE, _CLS_TABLA.ANTES_ELIMINAR,
        dbRow = None  # Llenado por el FWK
        )
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.AFTER_REMOVE, _CLS_TABLA.DESPUES_ELIMINAR)

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView
        )

    if _D_args.s_accion in (
            request.stv_fwk_permissions.btn_save.code,
            request.stv_fwk_permissions.btn_remove.code,
            ):
        # Si hubo cambios en el contenido se actualiza el maestro

        _O_cat.agregar_comando(
            E_comando = STV_FWK_FORM.E_COMANDOS.REFRESCAR_CAMPOS_MAESTRO,
            D_parametros = Storage(L_camposActualizar = dbc01.tcfdi_conceptos.comando_refrescar(_D_args.s_concepto_id))
            )
        _O_cat.agregar_comando(
            E_comando = STV_FWK_FORM.E_COMANDOS.REFRESCAR_CAMPOS_MAESTRO_2,
            D_parametros = Storage(L_camposActualizar = dbc01.tcfdis.comando_refrescar(_D_args.s_cfdi_id))
            )
    else:
        pass

    generaMensajeFlash(_dbTabla, _D_return)
    return _D_returnVars


def cfdi_concepto_descuentos():
    """ Formulario detalle de descuentos aplicados a los conceptos del cfdi

    @descripcion Descuentos

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Cliente
    @keyword arg3: empresa_cliente_id.
    @keyword arg4: CFDI
    @keyword arg5: cfdi_id.
    @keyword arg6: Concepto
    @keyword arg7: cfdi_concepto_id.
    @keyword [arg8]: operación a realizar en el formulario.
    @keyword [arg9]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    #@permiso btn_searchcancel,
    #@permiso btn_searchdone,
    """
    # noinspection DuplicatedCode
    _D_return = FUNC_RETURN()

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 8,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_cliente_id = request.args(3, 0),
        s_cfdi_id = request.args(5, 0),
        s_concepto_id = request.args(7, 0),
        s_descuento_id = request.args(9, None),
        )

    _dbTabla = dbc01.tcfdi_concepto_descuentos
    _D_useView = None
    _CLS_TABLA = TCFDI_CONCEPTO_DESCUENTOS.PROC(cfdi_concepto_id = _D_args.s_concepto_id)

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):
        # Si esta editando o guardando una edición, se configuran los campos del CFDI
        _D_result = _CLS_TABLA.configuracampos_edicion(_D_args.n_id)
        _D_return.combinar(_D_result)

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_useView = request.stv_fwk_permissions.view_refresh
        else:
            pass

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _D_result = _CLS_TABLA.configuracampos_nuevo(s_cfdi_concepto_descto_id = _D_args.n_id)
        _D_return.combinar(_D_result)

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_useView = request.stv_fwk_permissions.view_refresh
        else:
            pass
    else:
        pass

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTabla,
        dbTableMaster = dbc01.tcfdi_conceptos,
        xFieldLinkMaster = [_dbTabla.cfdi_concepto_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTabla.id,
            _dbTabla.tipo,
            _dbTabla.empresa_prodserv_casco_id,
            _dbTabla.cantidad_cascos,
            _dbTabla.valor_casco,
            _dbTabla.descuento,
            dbc01.tempresa_prodserv.codigo,
            _dbTabla.descripcion
            ],
        L_fieldsToSearch = [_dbTabla.id],
        L_fieldsToSearchIfDigit = [_dbTabla.id],
        x_offsetInArgs = _D_args.n_offsetArgs - 2
        )

    _O_cat.addRowSearchResultsLeftJoin(
        dbc01.tempresa_prodserv.on(
            (dbc01.tempresa_prodserv.id == _dbTabla.empresa_prodserv_casco_id)
            )
        )

    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION, _CLS_TABLA.al_validar)
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED, _CLS_TABLA.AL_ACEPTAR)
    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE, _CLS_TABLA.ANTES_ELIMINAR,
        dbRow = None  # Llenado por el FWK
        )
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.AFTER_REMOVE, _CLS_TABLA.DESPUES_ELIMINAR)

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView
        )

    if _D_args.s_accion in (
            request.stv_fwk_permissions.btn_save.code,
            request.stv_fwk_permissions.btn_remove.code,
            ):
        # Si hubo cambios en el contenido se actualiza el maestro

        _O_cat.agregar_comando(
            E_comando = STV_FWK_FORM.E_COMANDOS.REFRESCAR_CAMPOS_MAESTRO,
            D_parametros = Storage(L_camposActualizar = dbc01.tcfdi_conceptos.comando_refrescar(_D_args.s_concepto_id))
            )
        _O_cat.agregar_comando(
            E_comando = STV_FWK_FORM.E_COMANDOS.REFRESCAR_CAMPOS_MAESTRO_2,
            D_parametros = Storage(L_camposActualizar = dbc01.tcfdis.comando_refrescar(_D_args.s_cfdi_id))
            )
    else:
        pass

    generaMensajeFlash(_dbTabla, _D_return)
    return _D_returnVars


def cliente_cfdis_egresos():
    """ Formulario de CFDIs de egresos.

    @descripcion CFDIs de Egresos

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Cliente
    @keyword arg3: empresa_cliente_id.
    @keyword [arg4]: operación a realizar en el formulario.
    @keyword [arg5]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-file-code] Generar XML del CFDI
    @permiso special_permisson_02:[fas fa-file-signature] Firmar CFDI
    @permiso special_permisson_03:[fas fa-ban] Cancelar CFDI de egreso
    """
    _D_return = FUNC_RETURN()

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 4,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_cliente_id = request.args(3, 0),
        s_cfdi_id = request.args(5, 0),
        )

    _dbTabla = dbc01.tcfdis
    _D_useView = None
    _CLS_TABLA = TCFDIS.EGRESO(empresa_id = _D_args.s_empresa_id, receptorcliente_id = _D_args.s_cliente_id)

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):
        # Si esta editando o guardando una edición, se configuran los campos del CFDI
        _D_result = _CLS_TABLA.configuracampos_edicion(x_cfdi = _D_args.s_cfdi_id)
        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_result)
            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            pass

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _D_result = _CLS_TABLA.configuracampos_nuevo(s_cfdi_id = _D_args.s_cfdi_id)

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_result)
            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            pass

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.special_permisson_01.code,
            ):

        _D_result = STV_LIB_CFDI.REPRESENTAR_JSON(
            n_empresa_id = int(_D_args.s_empresa_id),
            n_cfdi_id = int(_D_args.s_cfdi_id)
            ).procesar(
                b_grabar = True
                )

        if _D_result.E_return == stvfwk2_e_RETURN.OK:
            stv_flashSuccess(
                'CFDI', "Creación de XML correcta",
                s_mode = 'stv_flash',
                D_addProperties = {}
                )

        else:
            stv_flashError(
                'CFDI', "Creación de XML incorrecta: " + _D_result.s_msgError,
                s_mode = 'stv_flash',
                D_addProperties = {}
                )

        _D_useView = request.stv_fwk_permissions.view_refresh

    elif _D_args.s_accion == request.stv_fwk_permissions.special_permisson_02.code:
        # Se intenta timbrar el CFDI

        _D_results = TCFDIS.TIMBRADO.TIMBRAR(x_cfdi = _D_args.s_cfdi_id)
        _D_return.update(_D_results)
        _D_useView = request.stv_fwk_permissions.view_refresh

    elif _D_args.s_accion == request.stv_fwk_permissions.special_permisson_03.code:
        # Se intenta cancelar el CFDI

        _D_results = TCFDIS.TIMBRADO.CANCELAR(x_cfdi = _D_args.s_cfdi_id)
        _D_return.update(_D_results)
        _D_useView = request.stv_fwk_permissions.view_refresh

    else:
        pass

    # Principal filtro de CFDIs a mostrar
    _dbQrySearch = (_dbTabla.tipocomprobante_id == TTIPOSCOMPROBANTES.EGRESO)

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_dbQrySearch),
        dbTable = _dbTabla,
        dbTableMaster = dbc01.tempresa_clientes,
        xFieldLinkMaster = [_dbTabla.receptorcliente_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01,
            request.stv_fwk_permissions.special_permisson_02,
            request.stv_fwk_permissions.special_permisson_03,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTabla.id,
            _dbTabla.generado_por, _dbTabla.serie, _dbTabla.folio, _dbTabla.estado,
            _dbTabla.fecha, _dbTabla.fechatimbrado, _dbTabla.sat_status,
            # _dbTabla.metodopago,
            # _dbTabla.formapago_id, _dbTabla.credito_plazo, _dbTabla.v_vencimiento,
            # _dbTabla.v_estatuscxc, _dbTabla.v_dvencido,
            # _dbTabla.total, _dbTabla.saldo, _dbTabla.moneda,
            # _dbTabla.emp_pla_suc_almacen_id,
            # _dbTabla.receptorrfc, _dbTabla.receptornombrerazonsocial,
            # _dbTabla.emisorrfc, _dbTabla.emisornombrerazonsocial,
            _dbTabla.total, _dbTabla.saldo,
            # _dbTabla.totalpago,
            ],
        L_fieldsToSearch = [
            _dbTabla.serie, _dbTabla.folio, _dbTabla.fecha, _dbTabla.fechatimbrado, _dbTabla.total,
            _dbTabla.lugarexpedicion, _dbTabla.receptorrfc, _dbTabla.uuid,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTabla.id, _dbTabla.tipocomprobante_id, _dbTabla.folio, _dbTabla.total,
            _dbTabla.lugarexpedicion,
            ],
        L_fieldnameIds = [_dbTabla.id],
        x_offsetInArgs = _D_args.n_offsetArgs - 2,
        b_preventDoubleUpdate = False  # Debido a que se puede autocalcular la info con los detalles
        )

    _O_cat.addRowSearchResultsProperty(
        STV_FWK_FORM.ROWSEARCHRESULTS.PROPERTIES.ORDERBY,
        [~_dbTabla.fecha]
        )

    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION, _CLS_TABLA.al_validar)
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED, _CLS_TABLA.AL_ACEPTAR)
    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE, _CLS_TABLA.ANTES_ELIMINAR,
        dbRow = None  # Llenado por el FWK
        )
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.AFTER_REMOVE, _CLS_TABLA.DESPUES_ELIMINAR)
    _dbRow_empresa = dbc01.tempresas(_D_args.s_empresa_id)

    _O_cat.addRowSearchResults_addColumnBegin(
        es_typeColumn = STV_FWK_FORM.ES_TYPECOLUMN.S_PLUS,
        s_colname = "plus",
        D_field = Storage(
            readable = True
            ),
        m_classHtml = lambda D_column, dbRow: "stv_rowresults_plus",
        D_data = Storage(
            url = URL(
                f = 'cliente_cfdi_movimientos',
                args = ['master_' + str(request.function)]
                )
            )
        )

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView
        )

    # Si la forma fue aceptada
    if not _D_args.n_id and _D_returnVars.rowContent_form and _D_returnVars.rowContent_form.accepted:
        dbc01.commit()

        # Se importa gluon.storage para poder crear un objeto especial de lista para request.args
        from gluon.storage import List
        request.args = List(
            _D_args.L_argsBasicos + [
                request.stv_fwk_permissions.btn_edit.code,
                str(_D_returnVars.rowContent_form.vars.id)
                ]
            )
        request._post_vars = Storage()
        _D_returnVars = cliente_cfdis_egresos()
        return _D_returnVars
    else:
        pass

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_conceptos,
        s_url = URL(
            a = 'app_timbrado', c = '130clientes_cfdis',
            f = 'cfdi_conceptos',
            args = _D_args.L_argsBasicos + ['master_' + str(request.function), _D_args.n_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado', c = '130clientes_cfdis',
            f = 'cfdi_conceptos',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_relacionados,
        s_url = URL(
            a = 'app_timbrado', c = '130clientes_cfdis',
            f = 'cfdi_relacionados',
            args = _D_args.L_argsBasicos + ['master_' + str(request.function), _D_args.n_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado', c = '130clientes_cfdis',
            f = 'cfdi_relacionados',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc02.tcfdi_xmls,
        s_url = URL(
            a = 'app_timbrado', c = '250cfdi',
            f = 'cfdi_xml',
            args = ['master_' + str(request.function), _D_args.n_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado', c = '250cfdi',
            f = 'cfdi_xml',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_prepolizas,
        s_url = URL(
            a = 'app_timbrado', c = '255prepolizas_polizas',
            f = 'prepoliza',
            args = ['master_' + str(request.function), _D_args.n_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado', c = '255prepolizas_polizas',
            f = 'prepoliza',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_movimientos,
        s_url = URL(
            a = 'app_timbrado', c = '130clientes_cfdis',
            f = 'cliente_cfdi_movimientos',
            args = ['master_' + str(request.function), _D_args.n_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado', c = '130clientes_cfdis',
            f = 'cliente_cfdi_movimientos',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_type = "button",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_edit),
        )
    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_02.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_02.code,
        s_type = "button",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_edit),
        )
    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_03.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_03.code,
        s_type = "button",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_edit),
        )

    if _D_args.s_accion in (
            request.stv_fwk_permissions.btn_save.code,
            request.stv_fwk_permissions.btn_remove.code,
            ):
        # Si hubo cambios en el contenido se actualiza el maestro

        _O_cat.agregar_comando(
            E_comando = STV_FWK_FORM.E_COMANDOS.REFRESCAR_CAMPOS_MAESTRO,
            D_parametros = Storage(
                L_camposActualizar = STV_LIB_DB.COMANDO_REFRESCAR(
                    _D_args.s_cliente_id,
                    dbc01.tempresa_clientes,
                    [
                        dbc01.tempresa_clientes.credito_saldo,
                        dbc01.tempresa_clientes.credito_saldovencido,
                        dbc01.tempresa_clientes.credito_saldopendtimbrar,
                        dbc01.tempresa_clientes.credito_saldotimbrado,
                        ]
                    )
                )
            )
    else:
        pass

    generaMensajeFlash(_dbTabla, _D_return)
    return _D_returnVars


def cliente_cfdis_pagos():
    """ Formulario de CFDIs de pagos.

    @descripcion CFDIs de Pagos

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Cliente
    @keyword arg3: empresa_cliente_id.
    @keyword [arg4]: operación a realizar en el formulario.
    @keyword [arg5]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_print
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-file-code] Generar XML del CFDI
    @permiso special_permisson_02:[fas fa-file-signature] Firmar CFDI
    @permiso special_permisson_03:[fas fa-ban] Cancelar CFDI de pago
    """
    # noinspection DuplicatedCode
    _D_return = FUNC_RETURN()

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 4,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_cliente_id = request.args(3, 0),
        s_cfdi_id = request.args(5, 0),
        )

    _dbTabla = dbc01.tcfdis
    _D_useView = None
    _CLS_TABLA = TCFDIS.PAGO(empresa_id = _D_args.s_empresa_id, receptorcliente_id = _D_args.s_cliente_id)

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):
        # Si esta editando o guardando una edición, se configuran los campos del CFDI
        _D_result = _CLS_TABLA.configuracampos_edicion(x_cfdi = _D_args.s_cfdi_id)
        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_result)
            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            pass

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _D_result = _CLS_TABLA.configuracampos_nuevo(s_cfdi_id = _D_args.s_cfdi_id)

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_result)
            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            pass

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.special_permisson_01.code,
            ):

        _D_result = STV_LIB_CFDI.REPRESENTAR_JSON(
            n_empresa_id = int(_D_args.s_empresa_id),
            n_cfdi_id = int(_D_args.s_cfdi_id)
            ).procesar(
                b_grabar = True
                )

        if _D_result.E_return == stvfwk2_e_RETURN.OK:
            stv_flashSuccess(
                'CFDI', "Creación de XML correcta",
                s_mode = 'stv_flash',
                D_addProperties = {}
                )

        else:
            stv_flashError(
                'CFDI', "Creación de XML incorrecta: " + _D_result.s_msgError,
                s_mode = 'stv_flash',
                D_addProperties = {}
                )

        _D_useView = request.stv_fwk_permissions.view_refresh

    elif _D_args.s_accion == request.stv_fwk_permissions.special_permisson_02.code:
        # Se intenta timbrar el CFDI

        _D_results = TCFDIS.TIMBRADO.TIMBRAR(x_cfdi = _D_args.s_cfdi_id)
        _D_return.update(_D_results)
        _D_useView = request.stv_fwk_permissions.view_refresh

    elif _D_args.s_accion == request.stv_fwk_permissions.special_permisson_03.code:
        # Se intenta cancelar el CFDI

        _D_results = TCFDIS.TIMBRADO.CANCELAR(x_cfdi = _D_args.s_cfdi_id)
        _D_return.update(_D_results)
        _D_useView = request.stv_fwk_permissions.view_refresh

    else:
        pass

    # Principal filtro de CFDIs a mostrar
    _dbQrySearch = (_dbTabla.tipocomprobante_id == TTIPOSCOMPROBANTES.PAGO)

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_dbQrySearch),
        dbTable = _dbTabla,
        dbTableMaster = dbc01.tempresa_clientes,
        xFieldLinkMaster = [_dbTabla.receptorcliente_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01,
            request.stv_fwk_permissions.special_permisson_02,
            request.stv_fwk_permissions.special_permisson_03,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTabla.id,
            _dbTabla.generado_por, _dbTabla.serie, _dbTabla.folio, _dbTabla.estado,
            _dbTabla.fecha, _dbTabla.fechatimbrado, _dbTabla.sat_status,
            # _dbTabla.metodopago,
            # _dbTabla.formapago_id, _dbTabla.credito_plazo, _dbTabla.v_vencimiento,
            # _dbTabla.v_estatuscxc, _dbTabla.v_dvencido,
            # _dbTabla.total, _dbTabla.saldo, _dbTabla.moneda,
            # _dbTabla.emp_pla_suc_almacen_id,
            # _dbTabla.receptorrfc, _dbTabla.receptornombrerazonsocial,
            # _dbTabla.emisorrfc, _dbTabla.emisornombrerazonsocial,
            _dbTabla.totalpago,  # _dbTabla.saldo,
            ],
        L_fieldsToSearch = [
            _dbTabla.serie, _dbTabla.folio, _dbTabla.fecha, _dbTabla.fechatimbrado, _dbTabla.total,
            _dbTabla.lugarexpedicion, _dbTabla.receptorrfc, _dbTabla.uuid,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTabla.id, _dbTabla.tipocomprobante_id, _dbTabla.folio, _dbTabla.total,
            _dbTabla.lugarexpedicion,
            ],
        L_fieldnameIds = [_dbTabla.id],
        x_offsetInArgs = _D_args.n_offsetArgs - 2,
        b_preventDoubleUpdate = False  # Debido a que se puede autocalcular la info con los detalles
        )

    _O_cat.addRowSearchResultsProperty(
        STV_FWK_FORM.ROWSEARCHRESULTS.PROPERTIES.ORDERBY,
        [~_dbTabla.fecha]
        )

    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION, _CLS_TABLA.al_validar)
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED, _CLS_TABLA.AL_ACEPTAR)
    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE, _CLS_TABLA.ANTES_ELIMINAR,
        dbRow = None  # Llenado por el FWK
        )
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.AFTER_REMOVE, _CLS_TABLA.DESPUES_ELIMINAR)
    _dbRow_empresa = dbc01.tempresas(_D_args.s_empresa_id)

    _O_cat.addRowSearchResults_addColumnBegin(
        es_typeColumn = STV_FWK_FORM.ES_TYPECOLUMN.S_PLUS,
        s_colname = "plus",
        D_field = Storage(
            readable = True
            ),
        m_classHtml = lambda D_column, dbRow: "stv_rowresults_plus",
        D_data = Storage(
            url = URL(
                f = 'cliente_cfdi_movimientos',
                args = ['master_' + str(request.function)]
                )
            )
        )

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView
        )

    # Si la forma fue aceptada
    if not _D_args.n_id and _D_returnVars.rowContent_form and _D_returnVars.rowContent_form.accepted:
        dbc01.commit()

        # Se importa gluon.storage para poder crear un objeto especial de lista para request.args
        from gluon.storage import List
        request.args = List(
            _D_args.L_argsBasicos + [
                request.stv_fwk_permissions.btn_edit.code,
                str(_D_returnVars.rowContent_form.vars.id)
                ]
            )
        request._post_vars = Storage()
        _D_returnVars = cliente_cfdis_pagos()
        return _D_returnVars
    else:
        pass

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_complementopagos,
        s_url = URL(
            a = 'app_timbrado', c = '130clientes_cfdis',
            f = 'cfdi_pagos',
            args = _D_args.L_argsBasicos + ['master_' + str(request.function), _D_args.n_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado', c = '130clientes_cfdis',
            f = 'cfdi_pagos',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_conceptos,
        s_url = URL(
            a = 'app_timbrado', c = '130clientes_cfdis',
            f = 'cfdi_conceptos',
            args = _D_args.L_argsBasicos + ['master_' + str(request.function), _D_args.n_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado', c = '130clientes_cfdis',
            f = 'cfdi_conceptos',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_relacionados,
        s_url = URL(
            a = 'app_timbrado', c = '130clientes_cfdis',
            f = 'cfdi_relacionados',
            args = _D_args.L_argsBasicos + ['master_' + str(request.function), _D_args.n_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado', c = '130clientes_cfdis',
            f = 'cfdi_relacionados',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc02.tcfdi_xmls,
        s_url = URL(
            a = 'app_timbrado', c = '250cfdi',
            f = 'cfdi_xml',
            args = ['master_' + str(request.function), _D_args.n_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado', c = '250cfdi',
            f = 'cfdi_xml',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_prepolizas,
        s_url = URL(
            a = 'app_timbrado', c = '255prepolizas_polizas',
            f = 'prepoliza',
            args = ['master_' + str(request.function), _D_args.n_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado', c = '255prepolizas_polizas',
            f = 'prepoliza',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_movimientos,
        s_url = URL(
            a = 'app_timbrado', c = '130clientes_cfdis',
            f = 'cliente_cfdi_movimientos',
            args = ['master_' + str(request.function), _D_args.n_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado', c = '130clientes_cfdis',
            f = 'cliente_cfdi_movimientos',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_type = "button",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_edit),
        )
    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_02.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_02.code,
        s_type = "button",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_edit),
        )
    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_03.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_03.code,
        s_type = "button",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_edit),
        )
    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.btn_print.code,
        s_permissionCode = request.stv_fwk_permissions.btn_print.code + '_modal',
        s_type = 'button',
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_edit),
        D_actions = Storage(
            modal = Storage(
                s_url = URL(
                    a = "app_timbrado", c = "250cfdi",
                    f = 'imprimir_cfdi',
                    args = [],
                    # vars = Storage()
                    ),
                s_field = "",
                )
            ),
        )

    if _D_args.s_accion in (
            request.stv_fwk_permissions.btn_save.code,
            request.stv_fwk_permissions.btn_remove.code,
            ):
        # Si hubo cambios en el contenido se actualiza el maestro

        _O_cat.agregar_comando(
            E_comando = STV_FWK_FORM.E_COMANDOS.REFRESCAR_CAMPOS_MAESTRO,
            D_parametros = Storage(
                L_camposActualizar = STV_LIB_DB.COMANDO_REFRESCAR(
                    _D_args.s_cliente_id,
                    dbc01.tempresa_clientes,
                    [
                        dbc01.tempresa_clientes.credito_saldo,
                        dbc01.tempresa_clientes.credito_saldovencido,
                        dbc01.tempresa_clientes.credito_saldopendtimbrar,
                        dbc01.tempresa_clientes.credito_saldotimbrado,
                        ]
                    )
                )
            )
    else:
        pass

    generaMensajeFlash(_dbTabla, _D_return)
    return _D_returnVars


def cfdi_pagos():
    """ Formulario detalle de pagos del cfdi

    @descripcion Complementos de Pago

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Cliente
    @keyword arg3: empresa_cliente_id.
    @keyword arg4: CFDI
    @keyword arg5: cfdi_id.
    @keyword [arg6]: operación a realizar en el formulario.
    @keyword [arg7]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    #@permiso btn_signature
    """
    # noinspection DuplicatedCode
    _D_return = FUNC_RETURN()

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 6,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_cliente_id = request.args(3, 0),
        s_cfdi_id = request.args(5, 0),
        s_comppago_id = request.args(7, None),
        )

    _dbTabla = dbc01.tcfdi_complementopagos
    _D_useView = None
    _CLS_TABLA = TCFDI_COMPLEMENTOPAGOS.PROC(cfdi_id = _D_args.s_cfdi_id)

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):
        # Si esta editando o guardando una edición, se configuran los campos del CFDI
        _D_result = _CLS_TABLA.configuracampos_edicion(_D_args.n_id)
        _D_return.combinar(_D_result)

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_useView = request.stv_fwk_permissions.view_refresh
        else:
            pass

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _D_result = _CLS_TABLA.configuracampos_nuevo(s_cfdi_comppago_id = _D_args.n_id)
        _D_return.combinar(_D_result)

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_useView = request.stv_fwk_permissions.view_refresh
        else:
            pass

    else:
        pass

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTabla,
        dbTableMaster = dbc01.tcfdis,
        xFieldLinkMaster = [_dbTabla.cfdi_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTabla.id, _dbTabla.versionpago, _dbTabla.fechapago, _dbTabla.formapago_id,
            _dbTabla.monedacontpaqi_id, _dbTabla.tipocambio, _dbTabla.monto,
            _dbTabla.sumarelacionados,
            ],
        L_fieldsToSearch = [_dbTabla.id],
        L_fieldsToSearchIfDigit = [_dbTabla.id],
        x_offsetInArgs = _D_args.n_offsetArgs - 2
        )

    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION, _CLS_TABLA.al_validar)
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED, _CLS_TABLA.AL_ACEPTAR)
    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE, _CLS_TABLA.ANTES_ELIMINAR,
        dbRow = None  # Llenado por el FWK
        )
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.AFTER_REMOVE, _CLS_TABLA.DESPUES_ELIMINAR)

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView
        )

    # Si esta insertando un registro
    if not _D_args.n_id and _D_returnVars.rowContent_form and _D_returnVars.rowContent_form.accepted:
        dbc01.commit()

        # Se importa gluon.storage para poder crear un objeto especial de lista para request.args
        from gluon.storage import List
        request.args = List(
            _D_args.L_argsBasicos + [
                request.stv_fwk_permissions.btn_edit.code,
                str(_D_returnVars.rowContent_form.vars.id)
                ]
            )
        request._post_vars = Storage()
        _D_returnVars = cfdi_pagos()
        return _D_returnVars

    else:
        _O_cat.addSubTab(
            dbTableDetail = dbc01.tcfdi_complementopago_doctosrelacionados,
            s_url = URL(
                f = 'cfdi_pago_relacionados',
                args = _D_args.L_argsBasicos + ['master_' + str(request.function), _D_args.n_id]
                ),
            s_idHtml = fn_createIDTabUnique(
                f = 'cfdi_pago_relacionados',
                s_masterIdentificator = str(request.function)
                )
            )

    if _D_args.s_accion in (
            request.stv_fwk_permissions.btn_save.code,
            request.stv_fwk_permissions.btn_remove.code,
            ):
        # Si hubo cambios en el contenido se actualiza el maestro

        _O_cat.agregar_comando(
            E_comando = STV_FWK_FORM.E_COMANDOS.REFRESCAR_CAMPOS_MAESTRO,
            D_parametros = Storage(L_camposActualizar = dbc01.tcfdis.comando_refrescar(_D_args.s_cfdi_id))
            )
    else:
        pass

    generaMensajeFlash(_dbTabla, _D_return)
    return _D_returnVars


def cfdi_pago_relacionados():
    """ Formulario detalle de documentos relacionados en pagos del cfdi

    @descripcion Doctos Relacionados

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Cliente
    @keyword arg3: empresa_cliente_id.
    @keyword arg4: CFDI
    @keyword arg5: cfdi_id.
    @keyword arg6: Complemento Pago
    @keyword arg7: cfdi_complementopago_id.
    @keyword [arg8]: operación a realizar en el formulario.
    @keyword [arg9]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    #@permiso btn_signature
    """
    _D_return = FUNC_RETURN()

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 8,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_cliente_id = request.args(3, 0),
        s_cfdi_id = request.args(5, 0),
        s_comppago_id = request.args(7, 0),
        s_docrel_id = request.args(9, None),
        )

    _dbTabla = dbc01.tcfdi_complementopago_doctosrelacionados
    _D_useView = None
    _CLS_TABLA = TCFDI_COMPLEMENTOPAGO_DOCTOSRELACIONADOS.PROC(cfdi_complementopago_id = _D_args.s_comppago_id)

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):
        # Si esta editando o guardando una edición, se configuran los campos del CFDI
        _D_result = _CLS_TABLA.configuracampos_edicion(_D_args.n_id)
        _D_return.combinar(_D_result)

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_useView = request.stv_fwk_permissions.view_refresh
        else:
            pass

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _D_result = _CLS_TABLA.configuracampos_nuevo(s_cfdi_comppago_docrel_id = _D_args.n_id)
        _D_return.combinar(_D_result)

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_useView = request.stv_fwk_permissions.view_refresh
        else:
            pass
    else:
        pass

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTabla,
        dbTableMaster = dbc01.tcfdi_complementopagos,
        xFieldLinkMaster = [_dbTabla.cfdi_complementopago_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTabla.id, _dbTabla.cfdirelacionado_id,  # _dbTabla.empresa_serie_id,
            # _dbTabla.folio,
            _dbTabla.monedacontpaqi_id, _dbTabla.tipocambio, _dbTabla.metodopago_id,
            _dbTabla.numparcialidad, _dbTabla.impsaldoant, _dbTabla.imppagado, _dbTabla.impsaldoinsoluto,
            _dbTabla.imppagado_monedapago
            ],
        L_fieldsToSearch = [_dbTabla.id],
        L_fieldsToSearchIfDigit = [_dbTabla.id],
        x_offsetInArgs = _D_args.n_offsetArgs - 2
        )

    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION, _CLS_TABLA.al_validar)
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED, _CLS_TABLA.AL_ACEPTAR)
    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE, _CLS_TABLA.ANTES_ELIMINAR,
        dbRow = None  # Llenado por el FWK
        )
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.AFTER_REMOVE, _CLS_TABLA.DESPUES_ELIMINAR)

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.btn_new.code,
        s_permissionCode = request.stv_fwk_permissions.btn_new.code + '_modal',
        s_permissionLabel = 'Agregar relacionados',
        s_permissionIcon = '',
        s_type = 'button',
        s_send = 'find',
        L_enabledByView = None,
        D_actions = Storage(
            modal = Storage(
                s_url = URL(
                    f = 'cliente_pago_relacionado_cfdis_consaldo_buscar',
                    args = _D_args.L_argsBasicos
                    ),
                s_field = "", )
            ),
        )

    if _D_args.s_accion in (
            request.stv_fwk_permissions.btn_save.code,
            request.stv_fwk_permissions.btn_remove.code,
            ):
        # Si hubo cambios en el contenido se actualiza el maestro
        _O_cat.agregar_comando(
            E_comando = STV_FWK_FORM.E_COMANDOS.REFRESCAR_CAMPOS_MAESTRO,
            D_parametros = Storage(
                L_camposActualizar = dbc01.tcfdi_complementopagos.comando_refrescar(_D_args.s_comppago_id)
                )
            )
        # _O_cat.agregar_comando(
        #     E_comando = STV_FWK_FORM.E_COMANDOS.REFRESCAR_CAMPOS_MAESTRO_2,
        #     D_parametros = Storage(L_camposActualizar = dbc01.tcfdis.comando_refrescar(_D_args.s_cfdi_id))
        #     )

    else:
        pass

    generaMensajeFlash(_dbTabla, _D_return)
    return _D_returnVars


def cliente_pago_relacionado_cfdis_consaldo_buscar():
    """ Forma buscar para cfdis de clientes que tienen saldo pendiente

    @descripcion Buscar CFDIs con Saldo

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Cliente
    @keyword arg3: empresa_cliente_id.
    @keyword arg4: CFDI
    @keyword arg5: cfdi_id.
    @keyword arg6: Complemento Pago
    @keyword arg7: cfdi_complementopago_id.
    @keyword [arg8]: operación a realizar en el formulario.
    @keyword [arg9]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    #@permiso btn_new
    #@permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    @permiso btn_find
    #@permiso btn_findall
    #@permiso btn_signature
    @permiso btn_searchcancel
    @permiso btn_searchdone
    """
    _D_return = FUNC_RETURN()

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 8,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_cliente_id = request.args(3, 0),
        s_cfdi_id = request.args(5, 0),
        s_cfdi_compPago_id = request.args(7, 0),
        s_cfdiARelacionar_id = request.args(9, None),
        )
    _dbTabla = dbc01.tcfdis
    _D_useView = None
    _s_action_override = None

    _qry = (_dbTabla.empresa_id == _D_args.s_empresa_id) \
        & (_dbTabla.rol == TCFDIS.E_ROL.EMISOR) \
        & (_dbTabla.receptorcliente_id == _D_args.s_cliente_id) \
        & (_dbTabla.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO) \
        & (_dbTabla.sat_status == TCFDIS.SAT_STATUS.VIGENTE)

    if _D_args.s_accion == request.stv_fwk_permissions.btn_find.code:
        # Si presiono buscar, que busque todos los CFDIs
        pass
    else:
        # De lo contrario, busca en los que tienen saldo solamente
        _qry &= (_dbTabla.saldo != 0)

    _dbRow_cfdi = dbc01.tcfdis(_D_args.s_cfdi_id)

    _D_result = TCFDIS.PROC.PUEDE_EDITAR(_dbRow_cfdi)

    if _D_result.E_return != stvfwk2_e_RETURN.OK:
        _D_return.combinar(_D_result, "CFDI no se encuentra en un estado que permite modificarlo. %s")
        _s_action_override = request.stv_fwk_permissions.btn_searchcancel.code

    elif _D_args.s_accion == request.stv_fwk_permissions.btn_new.code + '_modal':
        _s_action_override = request.stv_fwk_permissions.btn_find.code

    elif _D_args.s_accion == request.stv_fwk_permissions.btn_searchdone.code:

        # TODOMejora Implementar selección múltiple, inserta varios
        _D_insertar = Storage(
            cfdi_complementopago_id = _D_args.s_cfdi_compPago_id,
            cfdirelacionado_id = _D_args.s_cfdiARelacionar_id,
            imppagado = 0,  # Si es cero, se calcula al mapear el registro
            )

        _CLS_TABLA = TCFDI_COMPLEMENTOPAGO_DOCTOSRELACIONADOS.PROC(cfdi_complementopago_id = _D_args.s_cfdi_compPago_id)
        _O_form = _CLS_TABLA.al_validar(O_form = Storage(vars = _D_insertar, errors = Storage()))

        if _O_form.errors:
            _D_return.agrega_error("\n".join(_O_form.errors.values()))
            _s_action_override = None

        else:
            _D_insertar.id = dbc01.tcfdi_complementopago_doctosrelacionados.insert(
                **_D_insertar
                )
            TCFDI_COMPLEMENTOPAGO_DOCTOSRELACIONADOS.PROC.AL_ACEPTAR(O_form = Storage(vars = _D_insertar))

            _D_useView = request.stv_fwk_permissions.view_refreshParent
            _s_action_override = None

    else:
        _s_action_override = None

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTabla,
        L_visibleButtons = request.stv_fwk_permissions.L_formSearchButtons,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTabla.id,  # _dbTabla.tipocomprobante_id,
            _dbTabla.serie, _dbTabla.folio,
            _dbTabla.estado, _dbTabla.fecha,
            _dbTabla.fechatimbrado, _dbTabla.sat_status,
            _dbTabla.credito_plazo, _dbTabla.v_vencimiento,
            _dbTabla.v_estatuscxc, _dbTabla.v_dvencido,
            _dbTabla.total, _dbTabla.saldo,
            _dbTabla.monedacontpaqi_id,
            # _dbTabla.receptorrfc, _dbTabla.receptornombrerazonsocial,
            # _dbTabla.emisorrfc, _dbTabla.emisornombrerazonsocial
            ],
        L_fieldsToGroupBy = [],
        L_fieldsToSearch = [
            _dbTabla.id, _dbTabla.fecha, _dbTabla.total, _dbTabla.folio,
            _dbTabla.serie,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTabla.id, _dbTabla.folio, _dbTabla.serie,
            ],
        x_offsetInArgs = _D_args.n_offsetArgs
        )

    _D_returnVars = _O_cat.process(
        s_overrideCode = _s_action_override,
        D_overrideView = _D_useView
        )

    if _D_args.s_accion == request.stv_fwk_permissions.btn_searchdone.code:
        _O_cat.agregar_comando(
            E_comando = STV_FWK_FORM.E_COMANDOS.REFRESCAR_CAMPOS_MAESTRO_2,
            D_parametros = Storage(
                L_camposActualizar = dbc01.tcfdi_complementopagos.comando_refrescar(_D_args.s_cfdi_compPago_id)
                )
            )
    else:
        pass

    _D_returnVars.D_useView.b_serverCreatesTab = True
    if _D_args.s_accion == request.stv_fwk_permissions.btn_new.code + '_modal':
        _D_returnVars.D_useView.L_actionsCreateView += [_s_action_override]
    else:
        pass

    generaMensajeFlash(_dbTabla, _D_return)
    return _D_returnVars


def cfdi_relacionados():
    """ Formulario detalle de documentos relacionados del cfdi

    @descripcion Relacionados

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Cliente
    @keyword arg3: empresa_cliente_id.
    @keyword arg4: CFDI
    @keyword arg5: cfdi_id.
    @keyword [arg6]: operación a realizar en el formulario.
    @keyword [arg7]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    #@permiso btn_signature
    """
    _D_return = FUNC_RETURN()

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 6,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_cliente_id = request.args(3, 0),
        s_cfdi_id = request.args(5, 0),
        s_relacionado_id = request.args(7, None)
        )

    _dbTabla = dbc01.tcfdi_relacionados
    _D_useView = None
    _dbRow_cfdi = dbc01.tcfdis(_D_args.s_cfdi_id)
    _CLS_TABLA = TCFDI_RELACIONADOS.PROC(cfdi_id = _D_args.s_cfdi_id)

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):
        # Si esta editando o guardando una edición, se configuran los campos del CFDI
        _D_result = _CLS_TABLA.configuracampos_edicion(_D_args.n_id)
        _D_return.combinar(_D_result)

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            pass

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _D_result = _CLS_TABLA.configuracampos_nuevo(s_cfdi_relacion_id = _D_args.n_id)
        _D_return.combinar(_D_result)

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            pass

    else:
        pass

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTabla,
        dbTableMaster = dbc01.tcfdis,
        xFieldLinkMaster = [_dbTabla.cfdi_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTabla.id, _dbTabla.uuid, _dbTabla.cfdirelacionado_id, _dbTabla.fijarimporte,
            _dbTabla.importe,
            ],
        L_fieldsToSearch = [_dbTabla.uuid],
        L_fieldsToSearchIfDigit = [_dbTabla.id, _dbTabla.uuid],
        x_offsetInArgs = _D_args.n_offsetArgs - 2,
        )
    _O_cat.addRowSearchResultsProperty(
        STV_FWK_FORM.ROWSEARCHRESULTS.PROPERTIES.ORDERBY, [_dbTabla.id]
        )
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION, _CLS_TABLA.al_validar)
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED, _CLS_TABLA.AL_ACEPTAR)
    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE, _CLS_TABLA.ANTES_ELIMINAR,
        dbRow = None  # Llenado por el FWK
        )
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.AFTER_REMOVE, _CLS_TABLA.DESPUES_ELIMINAR)

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView
        )

    # TODO dependiendo del tipo de CFDI y el tipo relación, debe buscar relacionados
    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.btn_new.code,
        s_permissionCode = request.stv_fwk_permissions.btn_new.code + '_modal',
        s_permissionLabel = 'Agregar relacionados',
        s_permissionIcon = '',
        s_type = 'button',
        s_send = 'find',
        L_enabledByView = None,
        D_actions = Storage(
            modal = Storage(
                s_url = URL(
                    f = 'cliente_relacionado_cfdis_consaldo_buscar',
                    args = request.args[:6]
                    ),
                s_field = "",
                )
            ),
        )

    if _D_args.s_accion in (
            request.stv_fwk_permissions.btn_save.code,
            request.stv_fwk_permissions.btn_remove.code,
            ):
        # Si hubo cambios en el contenido se actualiza el maestro

        _O_cat.agregar_comando(
            E_comando = STV_FWK_FORM.E_COMANDOS.REFRESCAR_CAMPOS_MAESTRO,
            D_parametros = Storage(L_camposActualizar = dbc01.tcfdis.comando_refrescar(_D_args.s_cfdi_id))
            )
    else:
        pass

    generaMensajeFlash(_dbTabla, _D_return)
    return _D_returnVars


def cliente_relacionado_cfdis_consaldo_buscar():
    """ Forma buscar para cfdis de clientes que tienen saldo pendiente

    @descripcion Buscar CFDIs con Saldo

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Cliente
    @keyword arg3: empresa_cliente_id.
    @keyword arg4: CFDI
    @keyword arg5: cfdi_id.
    @keyword [arg6]: operación a realizar en el formulario.
    @keyword [arg7]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    #@permiso btn_new
    #@permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    @permiso btn_find
    #@permiso btn_findall
    #@permiso btn_signature
    @permiso btn_searchcancel
    @permiso btn_searchdone
    """
    _D_return = FUNC_RETURN()

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 6,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_cliente_id = request.args(3, 0),
        s_cfdi_id = request.args(5, 0),
        s_cfdiARelacionar_id = request.args(7, None),
        )
    _dbTabla = dbc01.tcfdis
    _D_useView = None

    _qry = True
    _s_action_override = None

    _dbRow_cfdi = dbc01.tcfdis(_D_args.s_cfdi_id)

    _D_result = TCFDIS.PROC.PUEDE_EDITAR(_dbRow_cfdi)

    if _D_result.E_return != stvfwk2_e_RETURN.OK:
        _D_return.combinar(_D_result, "CFDI no se encuentra en un estado que permite modificarlo. %s")
        _s_action_override = request.stv_fwk_permissions.btn_searchcancel.code

    elif _D_args.s_accion == request.stv_fwk_permissions.btn_new.code + '_modal':
        _s_action_override = request.stv_fwk_permissions.btn_find.code

    elif _D_args.s_accion == request.stv_fwk_permissions.btn_searchdone.code:

        _D_insertar = Storage(
            cfdi_id = _D_args.s_cfdi_id,
            cfdirelacionado_id = _D_args.s_cfdiARelacionar_id,
            )

        _D_insertar.id = dbc01.tcfdi_relacionados.insert(
            **_D_insertar
            )

        TCFDI_RELACIONADOS.PROC.AL_ACEPTAR(O_form = Storage(vars = _D_insertar))

        _D_useView = request.stv_fwk_permissions.view_refreshParent
        _s_action_override = None

    else:
        _qry = (_dbTabla.empresa_id == _D_args.s_empresa_id)
        _s_action_override = None

    if _dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO:

        if _dbRow_cfdi.tiporelacion_id == TTIPOSRELACIONES.SUSTITUCION:
            _qry &= \
                (_dbTabla.rol == TCFDIS.E_ROL.EMISOR) \
                & (_dbTabla.receptorcliente_id == _D_args.s_cliente_id) \
                & (_dbTabla.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO) \
                & (_dbTabla.sat_status == TCFDIS.SAT_STATUS.CANCELADO)

        elif _dbRow_cfdi.tiporelacion_id == TTIPOSRELACIONES.ANTICIPO:
            _qry &= \
                (_dbTabla.rol == TCFDIS.E_ROL.EMISOR) \
                & (_dbTabla.receptorcliente_id == _D_args.s_cliente_id) \
                & (_dbTabla.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO) \
                & (_dbTabla.sat_status == TCFDIS.SAT_STATUS.VIGENTE)
            # TODOMejora agregar mas reglas para identificar claramente el CFDI de anticipo
            #  checar https ://idconline.mx/fiscal-contable/2017/05/25/anticipos-en-el-cfdi-version-33

        elif _dbRow_cfdi.tiporelacion_id == TTIPOSRELACIONES.FACTPORTRASPREV:
            _qry &= \
                (_dbTabla.rol == TCFDIS.E_ROL.EMISOR) \
                & (_dbTabla.receptorcliente_id == _D_args.s_cliente_id) \
                & (_dbTabla.tipocomprobante_id == TTIPOSCOMPROBANTES.TRASLADO) \
                & (_dbTabla.sat_status == TCFDIS.SAT_STATUS.VIGENTE)
            # TODOMejora agregar mas reglas para identificar claramente el CFDI de anticipo
            #  checar https ://idconline.mx/fiscal-contable/2017/05/25/anticipos-en-el-cfdi-version-33

        else:
            _D_return.agrega_error("Tipo relación no soportada")
            _s_action_override = request.stv_fwk_permissions.btn_searchcancel.code

    elif _dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.EGRESO:

        if _dbRow_cfdi.tiporelacion_id == TTIPOSRELACIONES.SUSTITUCION:
            _qry &= \
                (_dbTabla.rol == TCFDIS.E_ROL.EMISOR) \
                & (_dbTabla.receptorcliente_id == _D_args.s_cliente_id) \
                & (_dbTabla.tipocomprobante_id == TTIPOSCOMPROBANTES.EGRESO) \
                & (_dbTabla.sat_status == TCFDIS.SAT_STATUS.CANCELADO)

        elif _dbRow_cfdi.tiporelacion_id == TTIPOSRELACIONES.ANTICIPO:
            _qry &= \
                (_dbTabla.rol == TCFDIS.E_ROL.EMISOR) \
                & (_dbTabla.receptorcliente_id == _D_args.s_cliente_id) \
                & (_dbTabla.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO) \
                & (_dbTabla.sat_status == TCFDIS.SAT_STATUS.VIGENTE)
            # TODOMejora agregar mas reglas para identificar claramente el CFDI de anticipo
            #  checar https ://idconline.mx/fiscal-contable/2017/05/25/anticipos-en-el-cfdi-version-33

        elif _dbRow_cfdi.tiporelacion_id in (TTIPOSRELACIONES.NOTACREDITO, TTIPOSRELACIONES.NOTADEBITO):
            _qry &= \
                (_dbTabla.rol == TCFDIS.E_ROL.EMISOR) \
                & (_dbTabla.receptorcliente_id == _D_args.s_cliente_id) \
                & (_dbTabla.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO) \
                & (_dbTabla.sat_status == TCFDIS.SAT_STATUS.VIGENTE)

        elif _dbRow_cfdi.tiporelacion_id == TTIPOSRELACIONES.DEVOLUCION:
            _qry &= \
                (_dbTabla.rol == TCFDIS.E_ROL.EMISOR) \
                & (_dbTabla.receptorcliente_id == _D_args.s_cliente_id) \
                & (_dbTabla.tipocomprobante_id.belongs(TTIPOSCOMPROBANTES.INGRESO, TTIPOSCOMPROBANTES.TRASLADO)) \
                & (_dbTabla.sat_status == TCFDIS.SAT_STATUS.VIGENTE)

        else:
            _D_return.agrega_error("Tipo relación no soportada")
            _s_action_override = request.stv_fwk_permissions.btn_searchcancel.code

    elif _dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.PAGO:

        if _dbRow_cfdi.tiporelacion_id == TTIPOSRELACIONES.SUSTITUCION:
            _qry &= \
                (_dbTabla.rol == TCFDIS.E_ROL.EMISOR) \
                & (_dbTabla.receptorcliente_id == _D_args.s_cliente_id) \
                & (_dbTabla.tipocomprobante_id == TTIPOSCOMPROBANTES.PAGO) \
                & (_dbTabla.sat_status == TCFDIS.SAT_STATUS.CANCELADO)

        else:
            _D_return.agrega_error("Tipo relación no soportada")
            _s_action_override = request.stv_fwk_permissions.btn_searchcancel.code

    elif _dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.TRASLADO:

        if _dbRow_cfdi.tiporelacion_id == TTIPOSRELACIONES.TRASLADOFACTPREV:
            _qry &= \
                (_dbTabla.rol == TCFDIS.E_ROL.EMISOR) \
                & (_dbTabla.receptorcliente_id == _D_args.s_cliente_id) \
                & (_dbTabla.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO) \
                & (_dbTabla.sat_status == TCFDIS.SAT_STATUS.VIGENTE)

        elif _dbRow_cfdi.tiporelacion_id == TTIPOSRELACIONES.DEVOLUCION:
            _qry &= \
                (_dbTabla.rol == TCFDIS.E_ROL.EMISOR) \
                & (_dbTabla.receptorcliente_id == _D_args.s_cliente_id) \
                & (_dbTabla.tipocomprobante_id.belongs(TTIPOSCOMPROBANTES.TRASLADO)) \
                & (_dbTabla.sat_status == TCFDIS.SAT_STATUS.VIGENTE)

        else:
            _D_return.agrega_error("Tipo relación no soportada")
            _s_action_override = request.stv_fwk_permissions.btn_searchcancel.code

    else:
        _D_return.agrega_error("Tipo comprobante no soportado")
        _s_action_override = request.stv_fwk_permissions.btn_searchcancel.code

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTabla,
        L_visibleButtons = request.stv_fwk_permissions.L_formSearchButtons,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTabla.id,  # _dbTabla.tipocomprobante_id,
            _dbTabla.serie, _dbTabla.folio,
            _dbTabla.estado, _dbTabla.fecha,
            _dbTabla.fechatimbrado, _dbTabla.sat_status,
            _dbTabla.credito_plazo, _dbTabla.v_vencimiento,
            _dbTabla.v_estatuscxc, _dbTabla.v_dvencido,
            _dbTabla.total, _dbTabla.saldo,
            # _dbTabla.moneda,
            # _dbTabla.receptorrfc, _dbTabla.receptornombrerazonsocial,
            # _dbTabla.emisorrfc, _dbTabla.emisornombrerazonsocial
            ],
        L_fieldsToGroupBy = [],
        L_fieldsToSearch = [
            _dbTabla.id, _dbTabla.fecha, _dbTabla.total, _dbTabla.folio,
            _dbTabla.serie,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTabla.id, _dbTabla.folio, _dbTabla.serie,
            ],
        x_offsetInArgs = _D_args.n_offsetArgs
        )

    _D_returnVars = _O_cat.process(
        s_overrideCode = _s_action_override,
        D_overrideView = _D_useView
        )

    if _D_args.s_accion == request.stv_fwk_permissions.btn_searchdone.code:
        _O_cat.agregar_comando(
            E_comando = STV_FWK_FORM.E_COMANDOS.REFRESCAR_CAMPOS_MAESTRO_2,
            D_parametros = Storage(L_camposActualizar = dbc01.tcfdis.comando_refrescar(_D_args.s_cfdi_id))
            )
    else:
        pass

    _D_returnVars.D_useView.b_serverCreatesTab = True
    if _D_args.s_accion == request.stv_fwk_permissions.btn_new.code + '_modal':
        _D_returnVars.D_useView.L_actionsCreateView += [_s_action_override]
    else:
        pass

    generaMensajeFlash(_dbTabla, _D_return)
    return _D_returnVars


def test():
    """ Formulario de CFDIs de egresos.

    @descripcion Test

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-file-code] Generar XML del CFDI
    @permiso special_permisson_02:[fas fa-file-signature] Firmar CFDI
    @permiso special_permisson_03:[fas fa-ban] Cancelar CFDI de egreso
    """
    return


def test_test():
    """ Formulario de CFDIs de egresos.
    
    @descripcion Subtest
    
    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Test
    @keyword arg3: empresa_test_id.
    @keyword [arg4]: operación a realizar en el formulario.
    @keyword [arg5]: id en caso de existir relación a un registro en la operación.    
        
    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-file-code] Generar XML del CFDI
    @permiso special_permisson_02:[fas fa-file-signature] Firmar CFDI
    @permiso special_permisson_03:[fas fa-ban] Cancelar CFDI de egreso
    """
    return


def cfdi_manejosaldos():
    """ Formulario detalle de manejo de saldo del CFDI, en caso de existir

    @descripcion Manejo Saldos

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Cliente
    @keyword arg3: empresa_cliente_id.
    @keyword arg4: CFDI
    @keyword arg5: cfdi_id.
    @keyword [arg6]: operación a realizar en el formulario.
    @keyword [arg7]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    @permiso btn_signature

    """
    _s_empresa_id = request.args(1, None)
    _s_cliente_id = request.args(3, None)
    _s_cfdi_id = request.args(5, None)

    _s_action = request.args(6, request.stv_fwk_permissions.btn_none.code)
    _s_cfdi_relacionado_id = request.args(7, 0)

    _dbTabla = dbc01.tcfdi_manejosaldos

    _dbRow_cfdi = dbc01.tcfdis(
        _s_cfdi_id
        )
    _s_overrideCode = None

    if (
            (
                _s_action in (
                    request.stv_fwk_permissions.btn_new.code,
                    )
                )
            and (
                (_dbRow_cfdi.sat_status != TCFDIS.SAT_STATUS.VIGENTE)
                or (_dbRow_cfdi.saldo >= 0)
                )
            ):
        stv_flashError(
            'Manejo Saldo',
            "CFDI no tiene saldo que se pueda transferir. ",
            s_mode = 'stv_flash',
            D_addProperties = {}
            )
        _s_overrideCode = request.stv_fwk_permissions.btn_cancel.code

    else:

        pass

    _dbTabla.proceso.default = TCFDI_MANEJOSALDOS.E_PROCESO.TRANSFERENCIA_A_CFDI
    _dbTabla.cfdirelacionado_id.writable = True
    if _dbRow_cfdi.saldo and (_dbRow_cfdi.saldo < 0):
        _dbTabla.importe.default = _dbRow_cfdi.saldo * (-1)
    else:
        pass

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTabla,
        dbTableMaster = dbc01.tcfdis,
        xFieldLinkMaster = [_dbTabla.cfdi_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [_dbTabla.id, _dbTabla.proceso, _dbTabla.cfdirelacionado_id, _dbTabla.descripcion,
                          _dbTabla.importe],
        L_fieldsToSearch = [_dbTabla.descripcion],
        L_fieldsToSearchIfDigit = [_dbTabla.id, _dbTabla.cfdirelacionado_id],
        x_offsetInArgs = request.args[:4]
        )

    _O_validar = STV_LIB_CFDI.VALIDAR(
        x_cfdi = _dbRow_cfdi
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION,
        _O_validar.form_validar_movtoEstadoCuenta,
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED,
        _O_validar.form_movtoEstadoCuenta,
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.AFTER_REMOVE,
        _O_validar.form_movtoEstadoCuenta,
        )

    _D_returnVars = _O_cat.process(
        s_overrideCode = _s_overrideCode
        )

    _D_returnVars.dbRow_cfdi = dbc01.tcfdis(
        _s_cfdi_id
        )

    return _D_returnVars


def cliente_manejosaldo_cfdis_consaldo_buscar():
    """ Forma buscar para cfdis de clientes que tienen saldo pendiente

    @descripcion Buscar CFDIs con Saldo para Manejo de Saldo

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Cliente
    @keyword arg3: empresa_cliente_id.
    @keyword arg4: CFDI
    @keyword arg5: cfdi_id.
    @keyword [arg6]: operación a realizar en el formulario.
    @keyword [arg7]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    #@permiso btn_new
    #@permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    #@permiso btn_signature
    @permiso btn_searchcancel
    @permiso btn_searchdone
    """

    _s_empresa_id = request.args(1, None)
    _s_cliente_id = request.args(3, None)
    _s_cfdi_id = request.args(5, None)
    _s_action = request.args(6, request.stv_fwk_permissions.btn_none.code)

    _s_action_override = None

    _dbTabla = dbc01.tcfdis

    _dbRow_cfdi = _dbTabla(
        _s_cfdi_id
        )

    _qry = (
        (_dbTabla.empresa_id == _s_empresa_id)
        & (_dbTabla.rol == TCFDIS.E_ROL.EMISOR)
        & (_dbTabla.receptorcliente_id == _s_cliente_id)
        & (_dbTabla.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO)
        & (_dbTabla.sat_status == TCFDIS.SAT_STATUS.VIGENTE)
        & (_dbTabla.id != _s_cfdi_id)
        )
    if _s_action == request.stv_fwk_permissions.btn_find.code:
        pass
    else:
        _qry &= (_dbTabla.saldo != 0)

    _L_orderBy = [~_dbTabla.fecha]
    _T_limitByLast = (0, 1000)
    _D_useView = None

    if (
            (_dbRow_cfdi.sat_status != TCFDIS.SAT_STATUS.VIGENTE)
            and (_dbRow_cfdi.saldo != 0)
            ):
        stv_flashError(
            'Manejo Saldo',
            "CFDI no se encuentra en un estado que permite modificarlo. " + _D_result.s_msgError,
            s_mode = 'stv_flash',
            D_addProperties = {}
            )
        _s_action_override = request.stv_fwk_permissions.btn_searchcancel.code
    else:

        if _s_action == request.stv_fwk_permissions.btn_new.code + '_modal':
            _s_action_override = request.stv_fwk_permissions.btn_findall.code

        elif _s_action == request.stv_fwk_permissions.btn_searchdone.code:

            _s_action_override = None
        else:
            _s_action_override = None

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(
            _qry
            ),
        dbTable = _dbTabla,
        L_visibleButtons = request.stv_fwk_permissions.L_formSearchButtons,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTabla.id,  # _dbTabla.tipocomprobante_id,
            _dbTabla.serie, _dbTabla.folio,
            _dbTabla.estado, _dbTabla.fecha,
            _dbTabla.fechatimbrado, _dbTabla.sat_status,
            _dbTabla.credito_plazo, _dbTabla.v_vencimiento,
            _dbTabla.v_estatuscxc, _dbTabla.v_dvencido,
            # Vencimiento
            # Estatus CxC
            # Días vencidos
            _dbTabla.total, _dbTabla.saldo,
            # _dbTabla.moneda,
            # _dbTabla.receptorrfc,
            # _dbTabla.receptornombrerazonsocial,
            # _dbTabla.emisorrfc,
            # _dbTabla.emisornombrerazonsocial
            ],
        L_fieldsToGroupBy = [],
        L_fieldsToSearch = [
            _dbTabla.id, _dbTabla.fecha, _dbTabla.total, _dbTabla.folio,
            _dbTabla.serie,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTabla.id, _dbTabla.folio, _dbTabla.serie,
            ],
        x_offsetInArgs = request.args[:6]
        )

    _O_cat.addRowSearchResultsProperty(
        'orderBy',
        _L_orderBy
        )
    _O_cat.addRowSearchResultsProperty(
        'limitByLast',
        _T_limitByLast
        )

    _D_returnVars = _O_cat.process(
        s_overrideCode = _s_action_override,
        D_overrideView = _D_useView
        )

    _D_returnVars.D_useView.b_serverCreatesTab = True
    if _s_action_override:
        _D_returnVars.D_useView.L_actionsCreateView += [_s_action_override]
    else:
        pass

    return _D_returnVars
