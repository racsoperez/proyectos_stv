# -*- coding: utf-8 -*-


def cfdisemitidos():
    """ Formulario de CFDIs, muestra CFDIs emitidos asociados a una empresa.

    @descripcion CFDIs Emitidos

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    #@permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_print
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    @permiso btn_find
    #@permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-file-code] Importar CFDI XML
    @permiso special_permisson_02:[fas fa-stamp] Timbrar CFDI

    """
    _D_return = FUNC_RETURN()

    if (len(request.args) >= 2) and (request.args[0] in ('empresa_id', 'master_empresas')):
        # Si tiene el argumento de empresa, se usa y define
        _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
            L_args = request.args,
            n_offsetArgs = 2,
            s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
            s_empresa_id = request.args(1, 0),
            s_cfdi_id = request.args(3, None),
            )

    else:
        # Si no tiene el argumento de empresa, se maneja global
        _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
            L_args = request.args,
            n_offsetArgs = 0,
            s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
            s_empresa_id = None,
            s_cfdi_id = request.args(1, None),
            )

    _dbTabla = dbc01.tcfdis
    _D_useView = None
    _CLS_TABLA = TCFDIS.PROC(empresa_id = _D_args.s_empresa_id)

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):
        # Si esta editando o guardando una edición, se configuran los campos del CFDI
        _D_result = _CLS_TABLA.configuracampos_edicion(x_cfdi = _D_args.s_cfdi_id)
        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_result)
            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            pass

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _D_result = _CLS_TABLA.configuracampos_nuevo(s_cfdi_id = _D_args.n_id)

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_result)
            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            pass

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.special_permisson_01.code,
            ):

        # TODO Importar XML

        _D_useView = request.stv_fwk_permissions.view_refresh

    elif _D_args.s_accion == request.stv_fwk_permissions.special_permisson_02.code:
        # Se intenta timbrar el CFDI

        _D_results = TCFDIS.TIMBRADO.TIMBRAR(x_cfdi = _D_args.s_cfdi_id)
        _D_return.update(_D_results)
        _D_useView = request.stv_fwk_permissions.view_refresh

    else:
        pass

    # Principal filtro de CFDIs a mostrar
    _dbQrySearch = dbc01.tcfdis.tipocomprobante_id.belongs(
        TTIPOSCOMPROBANTES.EGRESO,
        TTIPOSCOMPROBANTES.INGRESO,
        TTIPOSCOMPROBANTES.PAGO,
        TTIPOSCOMPROBANTES.TRASLADO,
        # TTIPOSCOMPROBANTES.NOMINA,  Tipo de comprobante nomina aún no soportado y contiene información confidencial
        )

    _D_results = TCFDIS.BUSQUEDA_AVANZADA.GENERA_PREFILTRO(_D_args)
    _dbQrySearch &= _D_results.qry

    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_new)

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_dbQrySearch),
        dbTable = _dbTabla,
        dbTableMaster = dbc01.tempresas if _D_args.s_empresa_id else None,
        xFieldLinkMaster = [_dbTabla.empresa_id] if _D_args.s_empresa_id else [],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01,
            request.stv_fwk_permissions.special_permisson_02,
            request.stv_fwk_permissions.special_permisson_03,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTabla.id,
            _dbTabla.generado_por, _dbTabla.serie, _dbTabla.folio, _dbTabla.estado,
            _dbTabla.fecha, _dbTabla.fechatimbrado, _dbTabla.sat_status, _dbTabla.metodopago,
            _dbTabla.formapago_id, _dbTabla.credito_plazo, _dbTabla.v_vencimiento,
            _dbTabla.v_estatuscxc, _dbTabla.v_dvencido,
            _dbTabla.total, _dbTabla.saldo, _dbTabla.moneda,  # _dbTabla.receptorrfc,
            _dbTabla.emp_pla_suc_almacen_id,
            # _dbTabla.receptornombrerazonsocial,
            # _dbTabla.emisorrfc, _dbTabla.emisornombrerazonsocial
            ],
        L_fieldsToSearch = [
            _dbTabla.serie, _dbTabla.folio, _dbTabla.fecha, _dbTabla.fechatimbrado, _dbTabla.total,
            _dbTabla.lugarexpedicion, _dbTabla.receptorrfc, _dbTabla.uuid,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTabla.id, _dbTabla.tipocomprobante_id, _dbTabla.folio, _dbTabla.total,
            _dbTabla.lugarexpedicion,
            dbc01.tcfdi_relacionados.cfdirelacionado_id,
            dbc01.tcfdi_complementopago_doctosrelacionados.cfdirelacionado_id,
            ],
        L_fieldnameIds = [_dbTabla.id],
        x_offsetInArgs = _D_args.n_offsetArgs - (2 if _D_args.s_empresa_id else 0),
        b_preventDoubleUpdate = False  # Debido a que se puede autocalcular la info con los detalles
        )

    # En caso de que el cfdi se encuentre con cfdi relacionados, se busca en los relacionados
    _O_cat.addRowSearchResultsLeftJoin(dbc01.tcfdi_relacionados.on((dbc01.tcfdi_relacionados.cfdi_id == _dbTabla.id)))

    # En caso de que el cfdi se encuentre con cfdis de pago, se busca en los pagos
    _O_cat.addRowSearchResultsLeftJoin(
        dbc01.tcfdi_complementopagos.on(dbc01.tcfdi_complementopagos.cfdi_id == _dbTabla.id)
        )
    _O_cat.addRowSearchResultsLeftJoin(
        dbc01.tcfdi_complementopago_doctosrelacionados.on(
            dbc01.tcfdi_complementopago_doctosrelacionados.cfdi_complementopago_id
            == dbc01.tcfdi_complementopagos.id
            )
        )

    _D_result = TCFDIS.BUSQUEDA_AVANZADA.CONFIGURA(
        O_cat = _O_cat,
        s_empresa_id = _D_args.s_empresa_id,
        )

    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION, _CLS_TABLA.al_validar)
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED, _CLS_TABLA.AL_ACEPTAR)
    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE, _CLS_TABLA.ANTES_ELIMINAR,
        dbRow = None  # Llenado por el FWK
        )
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.AFTER_REMOVE, _CLS_TABLA.DESPUES_ELIMINAR)

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView
        )

    # TODOMejora incluir el plus para ver el estado de cuenta del CFDI

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_conceptos,
        s_url = URL(
            a = 'app_timbrado', c = '250cfdi', f = 'cfdi_conceptos',
            args = ['master_' + str(request.function), _D_args.s_cfdi_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado', c = '250cfdi', f = 'cfdi_conceptos',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_relacionados,
        s_url = URL(
            a = 'app_timbrado', c = '250cfdi', f = 'cfdi_relacionados',
            args = ['master_' + str(request.function), _D_args.s_cfdi_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado', c = '250cfdi', f = 'cfdi_relacionados',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_complementopagos,
        s_url = URL(
            a = 'app_timbrado', c = '250cfdi', f = 'cfdi_pagos',
            args = ['master_' + str(request.function), _D_args.s_cfdi_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado', c = '250cfdi', f = 'cfdi_pagos',
            s_masterIdentificator = str(request.function)
            )
        )

    _D_returnVars['htmlid_emisor'] = _O_cat.addSubTab(s_tabName = "Emisor", s_type = "extended")

    _D_returnVars['htmlid_receptor'] = _O_cat.addSubTab(s_tabName = "Receptor", s_type = "extended")

    _D_returnVars['htmlid_vendedores'] = _O_cat.addSubTab(s_tabName = "Vendedores", s_type = "extended")

    _D_returnVars['htmlid_pac'] = _O_cat.addSubTab(s_tabName = "Timbrado", s_type = "extended")

    _O_cat.addSubTab(
        dbTableDetail = dbc02.tcfdi_xmls,
        s_url = URL(
            a = 'app_timbrado', c = '250cfdi', f = 'cfdi_xml',
            args = ['master_' + str(request.function), _D_args.s_cfdi_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado', c = '250cfdi', f = 'cfdi_xml',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_prepolizas,
        s_url = URL(
            a = 'app_timbrado', c = '255prepolizas_polizas', f = 'prepoliza',
            args = ['master_' + str(request.function), _D_args.s_cfdi_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado', c = '255prepolizas_polizas', f = 'prepoliza',
            s_masterIdentificator = str(request.function)
            )
        )

    if _D_args.s_cfdi_id and dbc01(
            (dbc01.tcfdi_prepolizas.cfdi_id == _D_args.s_cfdi_id)
            & (dbc01.tcfdi_prepolizas.sat_status_cancelacion == TCFDI_PREPOLIZAS.SAT_STATUS.CANCELADO)
            ).count():
        _O_cat.addSubTab(
            s_tabName = "Prepoliza cancelada",
            s_url = URL(
                c = '255prepolizas_polizas', f = 'prepoliza_canceladas',
                args = ['master_' + str(request.function), _D_args.s_cfdi_id]
                ),
            s_idHtml = fn_createIDTabUnique(
                c = '255prepolizas_polizas', f = 'prepoliza_canceladas',
                s_masterIdentificator = str(request.function)
                )
            )
    else:
        pass

    if _D_returnVars.dbRecord:
        # Si esta por ver/editar el cfdi, el FWK define dbRecord

        _O_cat.addSubTab(
            dbTableDetail = dbc01.tcfdi_movimientos,
            s_url = URL(
                a = 'app_timbrado', c = '130clientes_cfdis', f = 'cliente_cfdi_movimientos',
                args = ['master_' + str(request.function), _D_args.s_cfdi_id]
                ),
            s_idHtml = fn_createIDTabUnique(
                a = 'app_timbrado', c = '130clientes_cfdis', f = 'cliente_cfdi_movimientos',
                s_masterIdentificator = str(request.function)
                )
            )

    else:
        pass

    # Manejo de CFDI2
    if _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_view.code,
            ):

        if _D_args.s_cfdi2_id:
            pass

        else:
            _D_args.s_cfdi_id = _D_returnVars.rowContent_form.vars.id or _D_returnVars.rowContent_form.record_id or None
            if _D_args.s_cfdi_id:
                # Si tiene registro el CFDI

                _dbRows_cfdi2 = dbc01(
                    dbc01.tcfdis2.cfdi_id == _D_args.s_cfdi_id
                    ).select(
                        dbc01.tcfdis2.ALL
                        )
                if _dbRows_cfdi2:
                    _dbRow_cfdi2 = _dbRows_cfdi2.first()
                    _D_args.s_cfdi2_id = _dbRow_cfdi2.id
                else:
                    pass
            else:
                pass

        dbc01.tcfdis2.cfdi_id.default = _D_args.s_cfdi_id

        if not _D_args.s_cfdi2_id and _D_args.s_accion in (
                request.stv_fwk_permissions.btn_edit.code,
                request.stv_fwk_permissions.btn_view.code,
                ):
            _D_args.s_cfdi2_id = dbc01.tcfdis2.insert(cfdi_id = _D_args.s_cfdi_id)
        else:
            pass
        _s_accionCFDI2 = _D_args.s_accion

        # Si existe un cfdi2 para el CFDI, asignar editar
        request.args[_D_args.n_offsetArgs] = _s_accionCFDI2
        if _D_args.s_cfdi2_id:
            request.args.insert(_D_args.n_offsetArgs + 1, _D_args.s_cfdi2_id)
        else:
            pass

        _D_returnVars.s_formkey_cfdi2 = "_formkey_" + str(dbc01.tcfdis2)
        _D_returnVars.s_recordid_cfdi2 = "_recordid_" + str(dbc01.tcfdis2)
        if request.vars.get(_D_returnVars.s_formkey_cfdi2, None):
            # Con esto se simula la captura de la forma de cartaporte
            request._post_vars._formkey = request.vars[_D_returnVars.s_formkey_cfdi2]
            request._vars._formkey = request.vars[_D_returnVars.s_formkey_cfdi2]
            request._post_vars.id = request.vars[_D_returnVars.s_recordid_cfdi2]
            request._vars.id = request.vars[_D_returnVars.s_recordid_cfdi2]
        else:
            pass

        _O_catCFDI2 = STV_FWK_FORM(
            dbReference = dbc01,
            dbReferenceSearch = dbc01,
            dbTable = dbc01.tcfdis2,
            L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
            s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
            L_fieldsToShow = [
                dbc01.tcfdis2.id,
                ],
            L_fieldsToSearch = [dbc01.tcfdis2.id],
            L_fieldsToSearchIfDigit = [dbc01.tcfdis2.id],
            x_offsetInArgs = _D_args.n_offsetArgs
            )

        # TODOMejora agregar logica para validar capturas de tcfdis2
        # _O_catCFDI2.addRowContentEvent(
        #     STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION,
        #     TCFDI_COMPCARTAPORTE.al_validar,
        #     )
        #
        # _O_catCFDI2.addRowContentEvent(
        #     STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED,
        #     TCFDI_COMPCARTAPORTE.AL_ACEPTAR,
        #     )

        _D_returnVars.O_cfdi2 = _O_catCFDI2.process()

    else:
        _D_returnVars.O_cfdi2 = None

    if _D_args.s_empresa_id:
        # Solamente se puede importar XML file o timbrar si se tiene definida la empresa
        _O_cat.addPermissionConfig(
            s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
            s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
            s_type = "button",
            L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_find),
            D_actions = Storage(
                D_data = Storage(),
                modal = Storage(
                    s_url = URL(
                        a = 'app_timbrado', c = '250cfdi',
                        f = 'importar_cfdi_emitido',
                        args = [],
                        vars = Storage(
                            master = request.function,
                            s_almacen_id = _D_args.s_almacen_id,
                            )
                        ),
                    s_field = "",
                    )
                )
            )
        _O_cat.addPermissionConfig(
            s_initialCode = request.stv_fwk_permissions.special_permisson_02.code,
            s_permissionCode = request.stv_fwk_permissions.special_permisson_02.code,
            s_type = "button",
            L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_edit),
            )
    else:
        pass

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.btn_print.code,
        s_permissionCode = request.stv_fwk_permissions.btn_print.code + '_modal',
        s_type = 'button',
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_edit),
        D_actions = Storage(
            modal = Storage(
                s_url = URL(
                    f = 'imprimir_cfdi',
                    args = [],
                    # vars = Storage()
                    ),
                s_field = "",
                )
            ),
        )

    generaMensajeFlash(_dbTabla, _D_return)
    return _D_returnVars


def importar_cfdi_emitido():
    """ Modal para importar cfdis

    @descripcion Importar CFDI Emitido Modal

    @keyword [arg0]: operación a realizar en el formulario.
    @keyword [arg1]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    #@permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    #@permiso btn_view
    #@permiso btn_invalidate
    #@permiso btn_validate
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    #@permiso btn_find
    #@permiso btn_findall
    #@permiso btn_signature
    """

    def validacion(O_form, CLS_TABLA, D_vars_request):
        """ Validación
        """

        if isinstance(request.vars.archivo_cfdi, str):
            O_form.errors.archivo_cfdi = "Favor de agregar el archivo"
        else:

            if CLS_TABLA:

                _s_xml = str(request.vars.archivo_cfdi.file.read(), encoding='utf-8')
                _D_result = CLS_TABLA.IMPORTAR_CFDI(
                    _s_xml,
                    **D_vars_request
                    )

            else:
                _D_result = Storage(
                    E_return = stvfwk2_e_RETURN.NOK_ERROR,
                    s_msgError = "Tabla requerida para importación no reconocida"
                    )

            if _D_result.E_return > stvfwk2_e_RETURN.OK_WARNING:
                O_form.errors.id = _D_result.s_msgError
            else:
                O_form.cfdi_id = _D_result.n_cfdi_id
                stv_flashSuccess(
                    'Resultado Importación',
                    'CFDI %d importado correctamente' % (_D_result.n_cfdi_id or 0),
                    s_mode = 'stv_flash',
                    D_addProperties = {}
                    )

        return O_form

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 0,
        s_codigoDefault = request.stv_fwk_permissions.btn_new.code,
        s_cfdi_id = request.args(3, None),
        )

    if _D_args.s_accion == request.stv_fwk_permissions.special_permisson_01.code:
        _s_action = request.stv_fwk_permissions.btn_none.code
    else:
        _s_action = _D_args.s_accion

    # noinspection DuplicatedCode
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbTable = None,
        L_visibleButtons = [request.stv_fwk_permissions.btn_none, request.stv_fwk_permissions.btn_save],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        x_offsetInArgs = _D_args.n_offsetArgs
        )

    # Código para hace bypass de variables entre requests
    _D_vars_request = _O_cat.bypassVars(
        s_nombreVar = "vars_request",
        )

    if _D_vars_request.get('master', None) == 'cfdisemitidos':
        _CLS_TABLA = TCFDIS.PROC
    else:
        _CLS_TABLA = None

    _s_mensaje = ""
    _n_rows = 2
    if _CLS_TABLA:
        _s_mensaje = _CLS_TABLA.IMPORTAR_CFDI.__doc__.split("\n\n")[0]
        _L_mensaje = (' '.join(_s_mensaje.split())).split('.')
        _n_rows = len(_L_mensaje) + 2
        _s_mensaje = '.\n'.join(_L_mensaje)
    else:
        _s_mensaje = "ERROR: NO SE ENCONTRÓ CLASE PARA IMPORTAR. LLAME AL ADMINISTRADOR %d" % _D_vars_request.get(
            'master', None
            )

    _L_formFactoryFields = [
        Field(
            'comentarios', 'text', default = _s_mensaje,
            required = False,
            notnull = False, unique = False,
            widget = lambda v, r: stv_widget_text(v, r, D_additionalAttributes = {'n_rows': _n_rows}),
            label = 'Comentarios',
            comment = None,
            writable = False, readable = True,
            represent = stv_represent_text
            ),
        Field(
            'archivo_cfdi', type = 'upload',
            required = True,
            requires = IS_LENGTH(minsize = 0, maxsize = 4194304, error_message = 'El archivo es demasiado grande.'),
            notnull = False,
            uploadfield = True, widget = lambda f, d, u = None: stv_widget_inputFile(
                f, d,
                s_url = None, x_linkName = None,
                D_additionalAttributes = Storage(notExternalLink = True, notDownload = True, notRemove = True)
                ),
            label = 'Archivo CFDI', comment = None,
            writable = True, readable = True,
            autodelete = True,
            uploadfolder = os.path.join(request.folder, 'uploads', 'archivos_cfdi'),
            uploadseparate = False, uploadfs = None
            ),
        ]

    _O_cat.updateTabOption(
        "s_modalTitle",
        "Importar archivo XML"
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION,
        validacion,
        CLS_TABLA = _CLS_TABLA,
        D_vars_request = _D_vars_request
        )

    _D_returnVars = _O_cat.process(
        s_overrideTitle = "Importar archivo",
        s_overrideTitleSingular = "Importar archivo",
        s_overrideCode = _s_action,
        L_formFactoryFields = _L_formFactoryFields,
        b_skipAction = True,
        )

    if _D_returnVars.D_tabOptions.b_accepted:
        _O_cat.agregar_comando(
            E_comando = STV_FWK_FORM.E_COMANDOS.REFRESCAR_CAMPOS_BUSQUEDAAVANZADA,
            D_parametros = Storage(
                L_camposActualizar = [
                    Storage(
                        s_id = 'tcfdis_busquedaavanzada_folio',
                        x_valor = _D_returnVars.rowContent_form.cfdi_id or "No asociado",
                        s_display = _D_returnVars.rowContent_form.cfdi_id or "No asociado"
                        )
                    ]
                )
            )

        _D_useView = request.stv_fwk_permissions.view_refreshParent
        _D_returnVars.D_useView = _D_useView

    else:
        _D_returnVars.D_useView.b_serverCreatesTab = True
        _D_returnVars.D_useView.L_actionsCreateView = [request.stv_fwk_permissions.btn_none.code]

    return _D_returnVars


def reporte_cfdi():  # cmd_noEsForma
    """ Reporte de CFDI de ingreso

    @return:
    @rtype:
    """
    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 0,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_cfdi_id = request.args(1, 0),
        )

    _dbRow_cfdi = dbc01.tcfdis(_D_args.s_cfdi_id)

    _rpt = REPORTE_CFDI(
        _D_args.s_cfdi_id,
        E_tamanio = request.vars.pagina_tamanio,
        E_orientacion = request.vars.pagina_orientacion,
        )

    return _rpt.generar_pdf('archivo.pdf', "%s%s" % (_dbRow_cfdi.serie, _dbRow_cfdi.folio))


def imprimir_cfdi():  # cmd_noEsForma
    """ Modal que muestra pantalla de opciones para CFDIs emitidos.

    @descripcion Imprimir CFDI emitido

    @keyword arg0: CFDI
    @keyword arg1: cfdi_id.

    """

    _D_return = FUNC_RETURN()

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 2,
        s_codigoDefault = request.stv_fwk_permissions.btn_new.code,
        s_cfdi_id = request.args(1, 0),
        )
    _dbTabla = dbc01.tcfdis
    _D_useView = None
    _dbRow_cfdi = _dbTabla(_D_args.s_cfdi_id)

    if _dbRow_cfdi.sat_status == TCFDIS.SAT_STATUS.VIGENTE:
        _s_titulo = "CFDI VIGENTE"
    elif _dbRow_cfdi.sat_status == TCFDIS.SAT_STATUS.CANCELADO:
        _s_titulo = "CFDI CANCELADO"
    else:
        _s_titulo = "BORRADOR DE CFDI"

    if _dbRow_cfdi.tipocomprobante_id in (
            TTIPOSCOMPROBANTES.INGRESO, TTIPOSCOMPROBANTES.PAGO, TTIPOSCOMPROBANTES.EGRESO
            ):
        _s_reporte = "reporte_cfdi.pdf"
    elif _dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.TRASLADO:
        _s_reporte = "reporte_cfdi_traslado.pdf"
    else:
        _s_reporte = "reporte_cfdi.pdf"

    _D_returnVars = STV_REPORTE.CONFIGURACION_IMPRIMIR(
        dbTabla = _dbTabla,
        s_titulo = _s_titulo,
        s_accion = _D_args.s_accion,
        n_offsetArgs = _D_args.n_offsetArgs,
        s_urlReporte = URL(f = _s_reporte, args = ['cfdi_id', _D_args.s_cfdi_id])
        )

    return _D_returnVars


def cfdisrecibidos():
    """ Formulario de CFDIs recibidos.

    Args si args[0] = empresa_id:
        arg0: String empresa_id
        arg1: id de la empresa
        [arg2]: operación a realizar en el formulario
        [arg3]: id en caso de existir relación a un registro en la operación
    else:
        [arg0]: operación a realizar en el formulario
        [arg1]: id en caso de existir relación a un registro en la operación
    """

    _dbTable = dbc01.tcfdis

    if request.args(0, 'none') == 'empresa_id':
        _dbTableMaster = dbc01.tempresas
        _dbFieldLinkMaster = [_dbTable.empresa_id]
        _s_empresa_id = request.args(1, None)
        _s_action = request.args(2, request.stv_fwk_permissions.btn_none.code)
        _s_cfdi_id = request.args(3)

        _dbQrySearch = (
                (dbc01.tcfdis.empresa_id == _s_empresa_id)
                & (dbc01.tcfdis.rol == TCFDIS.E_ROL.RECEPTOR)
        )

    else:
        _dbTableMaster = None
        _dbFieldLinkMaster = []
        _s_action = request.args(0, request.stv_fwk_permissions.btn_none.code)
        _s_cfdi_id = request.args(1)
        _s_empresa_id = None

        _dbQrySearch = (
            (dbc01.tcfdis.rol == TCFDIS.E_ROL.RECEPTOR)
        )

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_dbQrySearch),
        dbTable = _dbTable,
        dbTableMaster = _dbTableMaster,
        xFieldLinkMaster = _dbFieldLinkMaster,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.tipocomprobante_id,
            _dbTable.serie,
            _dbTable.folio,
            _dbTable.fecha,
            _dbTable.fechatimbrado,
            _dbTable.metodopago_id,
            _dbTable.formapago_id,
            _dbTable.total,
            _dbTable.monedacontpaqi_id,
            _dbTable.emisorrfc,
            _dbTable.emisornombrerazonsocial,
            _dbTable.receptorrfc,
            _dbTable.receptornombrerazonsocial,
            ],
        L_fieldsToSearch = [
            _dbTable.serie,
            _dbTable.folio,
            _dbTable.fecha,
            _dbTable.fechatimbrado,
            _dbTable.total,
            _dbTable.lugarexpedicion,
            _dbTable.receptorrfc,
            _dbTable.uuid,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            _dbTable.tipocomprobante_id,
            _dbTable.folio,
            _dbTable.total,
            _dbTable.lugarexpedicion,
            ],
        )

    # Variable que contendrá los campos a editar en la búsqueda avanzada.
    _L_advSearch = []

    # Se configura el uso del campo día para poder buscar por rango de fechas.
    _dbField_dia = _dbTable.dia.clone()
    _dbField_dia.widget = stv_widget_inputDateRange
    _L_advSearch.append(_dbField_dia)

    if not (_s_empresa_id):
        # Si no esta definida la empresa, se agrega el seleccionar empresa.
        _dbField_empresa_id = _dbTable.empresa_id.clone()
        _dbField_empresa_id.notnull = False
        _L_advSearch.append(_dbField_empresa_id)

    # Se agregan campos sin modificaciones.
    _L_advSearch += [
        _dbTable.empresa_serie_id.clone(),
        _dbTable.folio.clone(),
        _dbTable.tipocomprobante_id.clone(),
        _dbTable.emisorproveedor_id.clone()
        ]

    # Se configura la edición de los campos.
    for _dbField in _L_advSearch:
        _dbField.writable = True

    _O_cat.cfgRow_advSearch(
        L_dbFields_formFactory_advSearch = _L_advSearch,
        dbRow_formFactory_advSearch = None,
        D_hiddenFields_formFactory_advSearch = {},
        s_style = 'bootstrap',
        L_buttons = []
        )

    _D_returnVars = _O_cat.process()

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_conceptos,
        s_url = URL(f = 'cfdi_conceptos', args = ['master_' + str(request.function), _s_cfdi_id]),
        s_idHtml = fn_createIDTabUnique(f = 'cfdi_conceptos', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_relacionados,
        s_url = URL(f = 'cfdi_relacionados', args = ['master_' + str(request.function), _s_cfdi_id]),
        s_idHtml = fn_createIDTabUnique(f = 'cfdi_relacionados', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_complementopagos,
        s_url = URL(f = 'cfdi_pagos', args = ['master_' + str(request.function), _s_cfdi_id]),
        s_idHtml = fn_createIDTabUnique(f = 'cfdi_pagos', s_masterIdentificator = str(request.function))
        )

    _D_returnVars['htmlid_emisor'] = _O_cat.addSubTab(
        dbTableDetail = None,
        s_url = False,
        s_tabName = "Emisor",
        s_idHtml = None,
        s_type = "extended"
        )

    _D_returnVars['htmlid_receptor'] = _O_cat.addSubTab(
        dbTableDetail = None,
        s_url = False,
        s_tabName = "Receptor",
        s_idHtml = None,
        s_type = "extended"
        )

    _D_returnVars['htmlid_vendedores'] = _O_cat.addSubTab(
        dbTableDetail = None,
        s_url = False,
        s_tabName = "Vendedores",
        s_idHtml = None,
        s_type = "extended"
        )

#     _dbRow_xml = dbc02(dbc02.tcfdi_xmls.cfdi_id == _s_cfdi_id).select(dbc02.tcfdi_xmls.xml_cfdi).first()
#
#     _D_returnVars.dbRow_xml = _dbRow_xml
#
#     _D_returnVars['htmlid_xml'] = _O_cat.addSubTab(
#         dbTableDetail = None,
#         s_url = False,
#         s_tabName = "XML",
#         s_idHtml = None,
#         s_type = "extended"
#         )

    _O_cat.addSubTab(
        dbTableDetail = dbc02.tcfdi_xmls,
        s_url = URL(
            a = 'app_timbrado',
            c = '250cfdi',
            f = 'cfdi_xml',
            args = ['master_' + str(request.function), _s_cfdi_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado',
            c = '250cfdi',
            f = 'cfdi_xml',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_prepolizas,
        s_url = URL(
            c = '255prepolizas_polizas', f = 'prepoliza', args = ['master_' + str(request.function),
                                                                  _s_cfdi_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            c = '255prepolizas_polizas', f = 'prepoliza', s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_contabilizacion,
        s_url = URL(f = 'cfdi_contabilizacion', args = ['master_' + str(request.function), _s_cfdi_id]),
        s_idHtml = fn_createIDTabUnique(f = 'cfdi_contabilizacion', s_masterIdentificator = str(request.function))
        )

    return _D_returnVars


def cfdi_xml():
    """ Formulario detalle de XML del cfdi

    @descripcion XML

    @keyword arg0: CFDI
    @keyword arg1: cfdi_id.

    @permiso btn_none
    #@permiso btn_new
    #@permiso btn_save
    #@permiso btn_cancel
    #@permiso btn_edit  << No debe permitirse editar >>
    #@permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    #@permiso btn_find
    #@permiso btn_findall
    #@permiso btn_signature

    """

    _s_cfdi_id = request.args(1, None)

    _dbTable = dbc02.tcfdi_xmls

    _dbRow_xml = dbc02(dbc02.tcfdi_xmls.cfdi_id == _s_cfdi_id).select(
        dbc02.tcfdi_xmls.xml_cfdi, dbc02.tcfdi_xmls.json_cfdi
        ).first()

    _O_cat = STV_FWK_FORM(
        dbReference = dbc02,
        dbReferenceSearch = dbc02,
        dbTable = None,
        dbTableMaster = dbc01.tcfdis,
        xFieldLinkMaster = [_dbTable.cfdi_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            ],
        L_fieldsToSearch = [
            _dbTable.id,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        )

    _D_returnVars = _O_cat.process()

    _D_returnVars.dbRow_xml = _dbRow_xml

    return _D_returnVars


def cfdi_conceptos():
    """ Formulario detalle de conceptos del cfdi

    Args:
        arg0: master_[nombre función maestro]
        arg1: cfdi_id
        [arg2]: operación a realizar en el formulario
        [arg3]: id en caso de existir relación a un registro en la operación
    """

    _dbTable = dbc01.tcfdi_conceptos

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tcfdis,
        xFieldLinkMaster = [_dbTable.cfdi_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsMinimal,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.noidentificacion,
            _dbTable.descripcion,
            _dbTable.cantidad,
            _dbTable.valorunitario,
            _dbTable.importe,
            _dbTable.descuento,
            #      _dbTable.claveprodserv,
            _dbTable.prodserv_id,
            #           _dbTable.empresa_prodserv_id,
            _dbTable.unidad_id,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.claveprodserv,
            _dbTable.noidentificacion
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            _dbTable.noidentificacion
            ],
        )

    _D_returnVars = _O_cat.process()

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_concepto_impuestostrasladados,
        s_url = URL(
            f = 'cfdi_concepto_impuestostrasladados', args = request.args[:2] + [
                'master_' + str(request.function), request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'cfdi_concepto_impuestostrasladados', s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_concepto_impuestosretenidos,
        s_url = URL(
            f = 'cfdi_concepto_impuestosretenidos', args = request.args[:2] + [
                'master_' + str(request.function), request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'cfdi_concepto_impuestosretenidos', s_masterIdentificator = str(request.function)
            )
        )

    return _D_returnVars


def cfdi_concepto_impuestostrasladados():
    """ Formulario detalle de impuestos trasladados de los conceptos del cfdi

    Args:
        arg0: master_[nombre función maestro]
        arg1: cfdi_id
        arg2: master_[nombre función maestro]
        arg3: cfdi_concepto_id
        [arg4]: operación a realizar en el formulario
        [arg5]: id en caso de existir relación a un registro en la operación
    """

    _dbTable = dbc01.tcfdi_concepto_impuestostrasladados

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tcfdi_conceptos,
        xFieldLinkMaster = [_dbTable.cfdi_concepto_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsMinimal,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.base,
            _dbTable.impuesto,
            _dbTable.impuesto_id,
            _dbTable.tipofactor,
            _dbTable.tasaocuota,
            _dbTable.importe,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [_dbTable.id],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        x_offsetInArgs = request.args[:2]
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def cfdi_concepto_impuestosretenidos():
    """ Formulario detalle de impuestos retenidos de los conceptos del cfdi

    Args:
        arg0: master_[nombre función maestro]
        arg1: cfdi_id
        arg2: master_[nombre función maestro]
        arg3: cfdi_concepto_id
        [arg4]: operación a realizar en el formulario
        [arg5]: id en caso de existir relación a un registro en la operación
    """

    _dbTable = dbc01.tcfdi_concepto_impuestosretenidos

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tcfdi_conceptos,
        xFieldLinkMaster = [_dbTable.cfdi_concepto_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsMinimal,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.base,
            _dbTable.impuesto,
            _dbTable.impuesto_id,
            _dbTable.tipofactor,
            _dbTable.tasaocuota,
            _dbTable.importe,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [_dbTable.id],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        x_offsetInArgs = request.args[:2]
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def cfdis_inconsistencias():

    _dbRows_NoDebenEstarTimbrados = dbc01(
        (dbc01.tcfdis.uuid == None)
        and (dbc01.tcfdis.sat_status != TCFDIS.SAT_STATUS.NO_DEFINIDO)
        ).select(
            dbc01.tcfdis.id,
            dbc01.tcfdis.uuid,
            dbc01.tcfdis.tipocomprobante_id,
            dbc01.tcfdis.receptorcliente_id,
            dbc01.tcfdis.fecha,
            dbc01.tcfdis.sat_status,
            )

    _dbRows_DebenEstarTimbrados = dbc01(
        (dbc01.tcfdis.uuid != None)
        and (dbc01.tcfdis.sat_status == TCFDIS.SAT_STATUS.NO_DEFINIDO)
        ).select(
            dbc01.tcfdis.id,
            dbc01.tcfdis.uuid,
            dbc01.tcfdis.tipocomprobante_id,
            dbc01.tcfdis.receptorcliente_id,
            dbc01.tcfdis.fecha,
            dbc01.tcfdis.sat_status,
            )

    # Busca los CFDIs que este referenciados con el tipo de sustitución
    # TODO, buscar a partir de cierta fecha
    # select tcfdi_relacionados.*, tcfdis.id, tcfdis.tipodecomprobante, tcfdis.receptorcliente_id, t2.id,
    # t2.sat_status from tcfdis, tcfdi_relacionados, tcfdis as t2
    # where tcfdis.tiporelacion = '04' and tcfdi_relacionados.cfdi_id = tcfdis.id
    # and t2.id = tcfdi_relacionados.cfdirelacionado_id and t2.sat_status != 2  AND
    # tcfdis.fecha >= '2020-01-01'
    # ORDER BY `tcfdis`.`tipodecomprobante`  ASC
    # Mostro 308 registros
    _tcfdisRef = dbc01.tcfdis.with_alias('tcfdisRef')
    _dbRows_DebenEstarCancelados = dbc01(
        (dbc01.tcfdis.tiporelacion_id == TTIPOSRELACIONES.SUSTITUCION)
        & (dbc01.tcfdi_relacionados.cfdi_id == dbc01.tcfdis.id)
        & (_tcfdisRef.id == dbc01.tcfdi_relacionados.cfdirelacionado_id)
        & (_tcfdisRef.sat_status != TCFDIS.SAT_STATUS.CANCELADO)
        ).select(
            dbc01.tcfdis.id,
            dbc01.tcfdis.uuid,
            dbc01.tcfdis.tipocomprobante_id,
            dbc01.tcfdis.receptorcliente_id,
            dbc01.tcfdis.fecha,
            dbc01.tcfdis.sat_status,
            dbc01.tcfdis.tiporelacion_id,
            dbc01.tcfdi_relacionados.ALL,
            _tcfdisRef.id,
            _tcfdisRef.uuid,
            _tcfdisRef.tipocomprobante_id,
            _tcfdisRef.receptorcliente_id,
            _tcfdisRef.fecha,
            _tcfdisRef.sat_status,
            _tcfdisRef.tiporelacion_id,
            )

    # Se verifica y actualiza el código para CFDIs por sustitución
    dbc01(
        (dbc01.tcfdis.tiporelacion == '04')
        & (dbc01.tcfdis.tiporelacion_id == None)
        ).update(tiporelacion_id = TTIPOSRELACIONES.SUSTITUCION)

    return


def cfdi_relacionados():
    """ Formulario detalle de documentos relacionados del cfdi

    @descripcion Relacionados

    @keyword arg0: CFDI
    @keyword arg1: cfdi_id.
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    #@permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-file-code] Buscar y asociar UUID con CFDI
    @permiso special_permisson_02:[fas fa-file-code] Ver CFDI relacionado
    """
    _D_return = FUNC_RETURN()

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 2,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_cfdi_id = request.args(1, 0),
        s_relacionado_id = request.args(3, None)
        )

    _dbTabla = dbc01.tcfdi_relacionados
    _D_useView = None
    _CLS_TABLA = TCFDI_RELACIONADOS.PROC(cfdi_id = _D_args.s_cfdi_id)

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):
        # Si esta editando o guardando una edición, se configuran los campos del CFDI
        _D_result = _CLS_TABLA.configuracampos_edicion(_D_args.n_id)
        _D_return.combinar(_D_result)

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            pass

    elif _D_args.s_accion == request.stv_fwk_permissions.special_permisson_01.code:
        TCFDI_RELACIONADOS.ASOCIAR_UUID(_D_args.n_id)  # TODO mostrar error si regresa error
        _D_useView = request.stv_fwk_permissions.view_refresh

    elif _D_args.s_accion == request.stv_fwk_permissions.special_permisson_02.code:
        # Para ver y llamar el visualizador de CFDI, se obtiene el movimiento de ingreso
        _s_cfdiAccion_id = _dbTabla(_D_args.n_id).cfdirelacionado_id

        redirect(
            URL(c = '250cfdi', f = 'cfdisemitidos', args = [request.stv_fwk_permissions.btn_view.code, _s_cfdiAccion_id], vars = request.vars)
            )

    else:
        pass

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTabla,
        dbTableMaster = dbc01.tcfdis,
        xFieldLinkMaster = [_dbTabla.cfdi_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01,
            request.stv_fwk_permissions.special_permisson_02,
            request.stv_fwk_permissions.special_permisson_03,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTabla.id, _dbTabla.uuid, _dbTabla.cfdirelacionado_id, _dbTabla.fijarimporte,
            _dbTabla.importe,
            ],
        L_fieldsToSearch = [_dbTabla.uuid],
        L_fieldsToSearchIfDigit = [_dbTabla.id, _dbTabla.uuid],
        x_offsetInArgs = _D_args.n_offsetArgs - 2,
        )
    _O_cat.addRowSearchResultsProperty(
        STV_FWK_FORM.ROWSEARCHRESULTS.PROPERTIES.ORDERBY, [_dbTabla.id]
        )
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION, _CLS_TABLA.al_validar)
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED, _CLS_TABLA.AL_ACEPTAR)
    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE, _CLS_TABLA.ANTES_ELIMINAR,
        dbRow = None  # Llenado por el FWK
        )
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.AFTER_REMOVE, _CLS_TABLA.DESPUES_ELIMINAR)

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_type = "button",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_edit),
        )
    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_02.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_02.code,
        s_type = "button",
        s_actualizar = "stv_gIDTabMain",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        )

    generaMensajeFlash(_dbTabla, _D_return)
    return _D_returnVars


def cfdi_pagos():
    """ Formulario detalle de pagos del cfdi

    Args:
        arg0: master_[nombre función maestro]
        arg1: cfdi_id
        [arg2]: operación a realizar en el formulario
        [arg3]: id en caso de existir relación a un registro en la operación
    """

    _dbTable = dbc01.tcfdi_complementopagos

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tcfdis,
        xFieldLinkMaster = [_dbTable.cfdi_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsMinimal,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.versionpago,
            _dbTable.fechapago,
            _dbTable.formapago_id,
            _dbTable.monedacontpaqi_id,
            _dbTable.tipocambio,
            _dbTable.monto,
            ],
        L_fieldsToSearch = [
            _dbTable.versionpago,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            _dbTable.versionpago
            ],
        )

    _D_returnVars = _O_cat.process()

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_complementopago_doctosrelacionados,
        s_url = URL(
            f = 'cfdi_pago_relacionados', args = request.args[:2] + ['master_' + str(request.function),
                                                                     request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(f = 'cfdi_pago_relacionados', s_masterIdentificator = str(request.function))
        )

    return (_D_returnVars)


def cfdi_pago_relacionados():
    """ Formulario detalle de documentos relacionados de los pagos del cfdi

    Args:
        arg0: master_[nombre función maestro]
        arg1: cfdi_id
        arg2: master_[nombre función maestro]
        arg3: cfdi_complementopago_id
        [arg4]: operación a realizar en el formulario
        [arg5]: id en caso de existir relación a un registro en la operación
    """

    _dbTable = dbc01.tcfdi_complementopago_doctosrelacionados

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tcfdi_complementopagos,
        xFieldLinkMaster = [_dbTable.cfdi_complementopago_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsMinimal,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.cfdirelacionado_id,
            _dbTable.empresa_serie_id,
            _dbTable.folio,
            _dbTable.monedacontpaqi_id,
            _dbTable.tipocambio,
            _dbTable.metodopago_id,
            _dbTable.numparcialidad,
            _dbTable.impsaldoant,
            _dbTable.imppagado,
            _dbTable.impsaldoinsoluto,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [_dbTable.id],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        x_offsetInArgs = request.args[:2]
        )

    _D_returnVars = _O_cat.process()

    return (_D_returnVars)


def cfdi_contabilizacion():
    """ Formulario para mostrar el estado contable del CFDI

    Args:
        arg0: master_[nombre función maestro]
        arg1: cfdi_id
        [arg2]: operación a realizar en el formulario
        [arg3]: id en caso de existir relación a un registro en la operación

    """

    _dbTable = dbc01.tcfdi_contabilizacion

    _s_cfdi_id = request.args(1)
    _s_action = request.args(2, request.stv_fwk_permissions.btn_none.code)

    if (_s_action in (request.stv_fwk_permissions.special_permisson_01.code,)):

        _D_result = _cfdi_contabilizacion(_s_cfdi_id)

        # TODO verificar el resultado y manejar los mensajes de error.

        _D_overrideView = request.stv_fwk_permissions.view_refresh

    else:
        # No hacer nada y continuar con el procesio normal.
        _D_overrideView = None
        pass

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tcfdis,
        xFieldLinkMaster = [_dbTable.cfdi_id],
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_findall
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.cfdi_relacionado_id,
            _dbTable.cfdi_concepto_id,
            _dbTable.cfdi_complementopago_doctorelacionado_id,
            _dbTable.impuesto_id,
            _dbTable.tasaocuota,
            _dbTable.nota,
            _dbTable.cobrado,
            _dbTable.por_cobrar,
            _dbTable.impuesto_trasladado,
            _dbTable.impuesto_portrasladar,
            _dbTable.impuesto_retenido,
            _dbTable.impuesto_porretener,
            ],
        L_fieldsToSearch = [_dbTable.id],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        )

    # Se agrega el boton especial 01 que funciona para re-asignar serie, caja chica y prepoliza en caso de ser necesario.
    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionLabel = "Re-contabilización",
        s_permissionIcon = "fa fa-compass",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_find),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
            ),
        L_options = []
        )

    _D_returnVars = _O_cat.process(D_overrideView = _D_overrideView)

    return (_D_returnVars)


def cfdi_sincontabilizar_porcajachica():
    """ Relación de CFDIs sin contabilizar por caja chica

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: operación a realizar en el formulario
        [arg3]: id en caso de existir relación a un registro en la operación; el id se compone de 4 partes
            id de la caja chica
            año
            mes
            día
    """

    _dbTable = dbc01.tcfdis_temp_sincontabilizar

    _s_empresa_id = request.args(1, None)
    _s_action = request.args(2, request.stv_fwk_permissions.btn_none.code)
    _s_multi_id = request.args(3, '0-0-0-0')  # Se ponen 4 elementos separados '-' por default.
    _L_multi_id = _s_multi_id.split('-')
    if _L_multi_id[0] == 'None':
        _s_cajachica_id = None
    else:
        _s_cajachica_id = _L_multi_id[0]

    # TODO si existen CFDIs con etapa = NO_DEFINIDO, manejar errores
    _dt_generado = request.browsernow.replace(microsecond = 0)

    if _s_action not in (request.stv_fwk_permissions.btn_view.code,):

        ''' Inicia sección
        Se obtienen las fechas que contienen registros de CFDIs sin contabilizar para ingresos y egresos,
        en caso de pagos, se usa la fecha del complemento de pago.
        Estas fechas se registran en una tabla temporal para su uso en la vista.
        Para mantener limpia la tabla, se limpian los registros creados hace dos días hacia atrás.
        '''

        # Se limpia la tabla temporal
        _d_hace2dias = datetime.date.today() - datetime.timedelta(days = 2)
        dbc01(_dbTable.generado_fechahora < _d_hace2dias).delete()

        _qry = (
                (dbc01.tcfdis.etapa == TCFDIS.E_ETAPA.SIN_CONTABILIZAR)
                & (dbc01.tcfdis.empresa_id == _s_empresa_id)
                & (dbc01.tempresas.id == dbc01.tcfdis.empresa_id)
                & (dbc01.tcfdis.fecha >= dbc01.tempresas.fecha_inicial)
            )

        _dbRows_fechas = dbc01(
            _qry
            ).select(
            dbc01.tcfdis.tipocomprobante_id,
            dbc01.tcfdis.empresa_plaza_sucursal_cajachica_id,
            dbc01.tcfdis.fecha,
            dbc01.tcfdi_complementopagos.fechapago,
            dbc01.tcfdis.sat_status,
            left = [
                dbc01.tcfdi_complementopagos.on(
                    (dbc01.tcfdis.tipocomprobante_id == TTIPOSCOMPROBANTES.PAGO)
                    & (dbc01.tcfdi_complementopagos.cfdi_id == dbc01.tcfdis.id)
                    )
                ],
            )

        _qry_cancelados = (
                (dbc01.tcfdis.etapa_cancelado == TCFDIS.E_ETAPA.SIN_CONTABILIZAR)
                & (dbc01.tcfdis.sat_status == TCFDIS.SAT_STATUS.CANCELADO)
                & (dbc01.tcfdis.empresa_id == _s_empresa_id)
                & (dbc01.tempresas.id == dbc01.tcfdis.empresa_id)
                & (dbc01.tcfdis.sat_fecha_cancelacion >= dbc01.tempresas.fecha_inicial)
            )

        _dbRows_fechas_cancelados = dbc01(
            _qry_cancelados
            ).select(
            dbc01.tcfdis.tipocomprobante_id,
            dbc01.tcfdis.empresa_plaza_sucursal_cajachica_id,
            dbc01.tcfdis.sat_fecha_cancelacion,
            dbc01.tcfdi_complementopagos.fechapago,
            dbc01.tcfdis.sat_status,
            left = [
                dbc01.tcfdi_complementopagos.on(
                    (dbc01.tcfdis.tipocomprobante_id == TTIPOSCOMPROBANTES.PAGO)
                    & (dbc01.tcfdi_complementopagos.cfdi_id == dbc01.tcfdis.id)
                    )
                ],
            )

        _D_sinContabilizar_porFecha = Storage()
        for _dbRow in _dbRows_fechas:
            if _dbRow.tcfdis.tipocomprobante_id in (TTIPOSCOMPROBANTES.INGRESO, TTIPOSCOMPROBANTES.EGRESO):
                _d_dia = _dbRow.tcfdis.fecha.date()
            elif _dbRow.tcfdis.tipocomprobante_id in (TTIPOSCOMPROBANTES.PAGO,):
                _d_dia = _dbRow.tcfdi_complementopagos.fechapago.date()
            else:
                _d_dia = _dbRow.tcfdis.fecha.date()

            _s_indice = str(_d_dia) + "_" + str(_dbRow.tcfdis.empresa_plaza_sucursal_cajachica_id)
            if _s_indice not in _D_sinContabilizar_porFecha:
                _D_sinContabilizar_porFecha[_s_indice] = Storage(
                    empresa_id = _s_empresa_id,
                    empresa_plaza_sucursal_cajachica_id = _dbRow.tcfdis.empresa_plaza_sucursal_cajachica_id,
                    dia = _d_dia,
                    generado_fechahora = _dt_generado,
                    ingresos = 0,
                    egresos = 0,
                    pagos = 0,
                    ingresos_cancelados = 0,
                    egresos_cancelados = 0,
                    pagos_cancelados = 0,
                    otros_cancelados = 0,
                    otros = 0,
                    )
            else:
                pass

            if TTIPOSCOMPROBANTES.INGRESO == _dbRow.tcfdis.tipocomprobante_id:
                _D_sinContabilizar_porFecha[_s_indice].ingresos += 1
            elif TTIPOSCOMPROBANTES.EGRESO == _dbRow.tcfdis.tipocomprobante_id:
                _D_sinContabilizar_porFecha[_s_indice].egresos += 1
            elif TTIPOSCOMPROBANTES.PAGO == _dbRow.tcfdis.tipocomprobante_id:
                _D_sinContabilizar_porFecha[_s_indice].pagos += 1
            else:
                _D_sinContabilizar_porFecha[_s_indice].otros += 1

        for _dbRow in _dbRows_fechas_cancelados:
            if _dbRow.tcfdis.tipocomprobante_id in (TTIPOSCOMPROBANTES.INGRESO, TTIPOSCOMPROBANTES.EGRESO):
                _d_dia = _dbRow.tcfdis.sat_fecha_cancelacion.date()
            elif _dbRow.tcfdis.tipocomprobante_id in (TTIPOSCOMPROBANTES.PAGO,):
                _d_dia = _dbRow.tcfdi_complementopagos.fechapago.date()
            else:
                _d_dia = _dbRow.tcfdis.sat_fecha_cancelacion.date()

            _s_indice = str(_d_dia) + "_" + str(_dbRow.tcfdis.empresa_plaza_sucursal_cajachica_id)
            if _s_indice not in _D_sinContabilizar_porFecha:
                _D_sinContabilizar_porFecha[_s_indice] = Storage(
                    empresa_id = _s_empresa_id,
                    empresa_plaza_sucursal_cajachica_id = _dbRow.tcfdis.empresa_plaza_sucursal_cajachica_id,
                    dia = _d_dia,
                    generado_fechahora = _dt_generado,
                    ingresos = 0,
                    egresos = 0,
                    pagos = 0,
                    ingresos_cancelados = 0,
                    egresos_cancelados = 0,
                    pagos_cancelados = 0,
                    otros_cancelados = 0,
                    otros = 0,
                    )
            else:
                pass

            if TTIPOSCOMPROBANTES.INGRESO == _dbRow.tcfdis.tipocomprobante_id:
                _D_sinContabilizar_porFecha[_s_indice].ingresos_cancelados += 1
            elif TTIPOSCOMPROBANTES.EGRESO == _dbRow.tcfdis.tipocomprobante_id:
                _D_sinContabilizar_porFecha[_s_indice].egresos_cancelados += 1
            elif TTIPOSCOMPROBANTES.PAGO == _dbRow.tcfdis.tipocomprobante_id:
                _D_sinContabilizar_porFecha[_s_indice].pagos_cancelados += 1
            else:
                _D_sinContabilizar_porFecha[_s_indice].otros_cancelados += 1

        _dbTable.bulk_insert(_D_sinContabilizar_porFecha.values())

        ''' Fin de sección '''
    else:
        pass

    if _s_action in (request.stv_fwk_permissions.special_permisson_01.code,):

        _qry = (
                (dbc01.tcfdis.etapa == TCFDIS.E_ETAPA.SIN_CONTABILIZAR)
                & (dbc01.tcfdis.empresa_id == _s_empresa_id)
                & (dbc01.tempresas.id == dbc01.tcfdis.empresa_id)
                & (dbc01.tcfdis.fecha >= dbc01.tempresas.fecha_inicial)
        )

        _dt_fecha = datetime.datetime(int(_L_multi_id[1]), int(_L_multi_id[2]), int(_L_multi_id[3]))
        # Se generan las fechas en formato datetime para el rango del día.
        _dt_fechaDesde = _dt_fecha.replace(hour = 0, minute = 0, second = 0, microsecond = 0)
        _dt_fechaHasta = _dt_fechaDesde + datetime.timedelta(days = 1)

        _qryFecha = (
                _qry
                & (dbc01.tcfdis.fecha >= _dt_fechaDesde)
                & (dbc01.tcfdis.fecha < _dt_fechaHasta)
        )

        if not _s_cajachica_id:

            _qryAsignarSerie = (_qryFecha & (dbc01.tcfdis.empresa_serie_id == None))

            _D_resultsAsignacionSerie = Storage(
                L_errores = [],
                L_correctos = []
                )

            _dbRows = dbc01(_qryAsignarSerie).select(dbc01.tcfdis.id, dbc01.tcfdis.serie)
            for _dbRow in _dbRows:
                _D_returnsData = TCFDIS.asociar_serie(_s_empresa_id, str(_dbRow.serie))
                if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                    _D_resultsAsignacionSerie.L_correctos.append(
                        Storage(
                            dbRow = _dbRow,
                            D_returnsData = _D_returnsData
                            )
                        )
                    dbc01(dbc01.tcfdis.id == _dbRow.id).update(empresa_serie_id = _D_returnsData.n_id)
                else:
                    _D_resultsAsignacionSerie.L_errores.append(
                        Storage(
                            dbRow = _dbRow,
                            D_returnsData = _D_returnsData
                            )

                        )

            _qryAsignarCajachica = (
                    _qryFecha
                    & (dbc01.tcfdis.empresa_serie_id != None)
                    & (dbc01.tcfdis.empresa_plaza_sucursal_cajachica_id == None)
                )

            _D_resultsAsignacionCajachica = Storage(
                L_errores = [],
                L_correctos = []
                )

            _dbRows = dbc01(_qryAsignarCajachica).select(
                dbc01.tcfdis.id, dbc01.tcfdis.empresa_serie_id, dbc01.tcfdis.lugarexpedicion
                )
            for _dbRow in _dbRows:
                _D_returnsData = TCFDIS.asociar_cajachica(
                    _s_empresa_id, str(_dbRow.empresa_serie_id), str(_dbRow.lugarexpedicion)
                    )
                if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                    _D_resultsAsignacionCajachica.L_correctos.append(
                        Storage(
                            dbRow = _dbRow,
                            D_returnsData = _D_returnsData
                            )
                        )
                    dbc01(dbc01.tcfdis.id == _dbRow.id).update(
                        empresa_plaza_sucursal_cajachica_id = _D_returnsData.n_id
                        )
                else:
                    _D_resultsAsignacionCajachica.L_errores.append(
                        Storage(
                            dbRow = _dbRow,
                            D_returnsData = _D_returnsData
                            )
                        )

            request.stv_flashInfo(
                s_title = "Asignación de Serie",
                s_msg = "Asignados %s de posibles %s" % (len(_D_resultsAsignacionSerie.L_correctos), (
                        len(_D_resultsAsignacionSerie.L_correctos) + len(_D_resultsAsignacionSerie.L_errores))),
                s_mode = 'stv_flash',
                D_addProperties = {}
                )

            request.stv_flashInfo(
                s_title = "Asignación de Caja Chica",
                s_msg = "Asignados %s de posibles %s" % (len(_D_resultsAsignacionCajachica.L_correctos), (
                        len(_D_resultsAsignacionCajachica.L_correctos) + len(_D_resultsAsignacionCajachica.L_errores))),
                s_mode = 'stv_flash',
                D_addProperties = {}
                )

        else:

            # Se actualiza el query para que genere la prepoliza de los CFDIs que su forma pago sea "por definir".
            _qryGenerarPrepoliza = (
                    _qryFecha
                    & (dbc01.tcfdis.empresa_plaza_sucursal_cajachica_id == _s_cajachica_id)
                    & (dbc01.tcfdis.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO)
                    & (dbc01.tcfdis.formapago_id == TFORMASPAGO.POR_DEFINIR)
            )

            _D_resultsPrepoliza = Storage(
                L_errores = [],
                L_correctos = []
                )

            _dbRows = dbc01(
                _qryGenerarPrepoliza
                ).select(
                dbc01.tcfdis.id,
                dbc01.tcfdis.formapago_id,
                dbc01.tcfdis.total
                )

            _D_registropago = Storage()

            for _dbRow in _dbRows:

                _dbRowsRegistrosPagos = dbc01(
                    dbc01.tcfdi_registropagos.cfdi_id == _dbRow.id
                    ).select(
                    dbc01.tcfdi_registropagos.id
                    )

                if _dbRowsRegistrosPagos:
                    # Si el CFDI actual tiene un registro de pago entonces no hace nada.
                    pass
                else:
                    # ... de lo contrario generará un registro de pago para poderlo contabilizar.

                    _D_registropago = Storage(
                        cfdi_id = _dbRow.id,
                        formapago_id = _dbRow.formapago_id,
                        monto = _dbRow.total
                        )

                    dbc01.tcfdi_registropagos.insert(**_D_registropago)

                _D_returnsData = STV_FWK_PREPOLIZA().generar(_dbRow.id)

                if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:

                    _D_resultsPrepoliza.L_correctos.append(
                        Storage(
                            dbRow = _dbRow,
                            D_returnsData = _D_returnsData
                            )
                        )

                else:

                    _D_resultsPrepoliza.L_errores.append(
                        Storage(
                            dbRow = _dbRow,
                            D_returnsData = _D_returnsData
                            )
                        )

            request.stv_flashInfo(
                s_title = "Generación de Prepoliza",
                s_msg = "Asignados %s de posibles %s" % (len(_D_resultsPrepoliza.L_correctos), (
                        len(_D_resultsPrepoliza.L_correctos) + len(_D_resultsPrepoliza.L_errores))),
                s_mode = 'stv_flash',
                D_addProperties = {}
                )

        _D_overrideView = request.stv_fwk_permissions.view_refresh

    else:
        # No hacer nada y continuar con el procesio normal.
        _D_overrideView = None
        pass

    # Se configura el campo como debe de presentarte sus subcampos calculados
    dbc01.tempresa_plaza_sucursal_cajaschicas.empresa_plaza_sucursal_id.readable = True
    _L_fieldsToShow = [
        _dbTable.id,
        dbc01.tempresa_plaza_sucursal_cajaschicas.id,
        _dbTable.dia,
        dbc01.tempresa_plaza_sucursal_cajaschicas.empresa_plaza_sucursal_id,
        dbc01.tempresa_plaza_sucursal_cajaschicas.nombre,
        _dbTable.ingresos,
        _dbTable.egresos,
        _dbTable.pagos,
        _dbTable.ingresos_cancelados,
        _dbTable.egresos_cancelados,
        _dbTable.pagos_cancelados,
        _dbTable.otros,
        _dbTable.otros_cancelados
        ]

    _L_OrderBy = [
        ~_dbTable.dia,
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(
            (_dbTable.generado_fechahora == _dt_generado)
            ),
        dbTable = _dbTable,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_findall,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToGroupBy = [],
        L_fieldsToSearch = [
            _dbTable.id,
            ],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        L_fieldnameIds = [dbc01.tempresa_plaza_sucursal_cajaschicas.id, _dbTable.dia, _dbTable.id],
        x_offsetInArgs = request.args[:2]
        )

    _O_cat.addRowSearchResultsLeftJoin(
        dbc01.tempresa_plaza_sucursal_cajaschicas.on(
            (dbc01.tempresa_plaza_sucursal_cajaschicas.id == _dbTable.empresa_plaza_sucursal_cajachica_id)
            )
        )

    _O_cat.addRowSearchResultsProperty('orderBy', _L_OrderBy)

    # Se agrega el boton especial 01 que funciona para re-asignar serie, caja chica y prepoliza en caso de ser
    # necesario.
    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionLabel = "Asignación automática de Serie, Caja Chica y Prepoliza para los CFDIs de crédito",
        s_permissionIcon = "fa fa-compass",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
            confirm = Storage(
                s_title = "Favor de confirmar operación",
                s_text = "Esta seguro de que desea modificar todos los CFDIs asociando la serie, caja chica y generando la prepoliza en casos posibles",
                s_type = "warning",
                s_btnconfirm = "Confirmar",
                s_btncancel = "Cancelar",
                )
            ),
        L_options = []
        )

    _O_cat.addSubTab(
        s_tabName = "Ingresos",
        s_url = URL(f = 'cfdi_sincontabilizar_ingresos',
            args = request.args[:2] + ['master_' + str(request.function), request.args(3)]),
        s_idHtml = fn_createIDTabUnique(f = 'cfdi_sincontabilizar_ingresos', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        s_tabName = "Egresos",
        s_url = URL(f = 'cfdi_sincontabilizar_egresos',
            args = request.args[:2] + ['master_' + str(request.function), request.args(3)]),
        s_idHtml = fn_createIDTabUnique(f = 'cfdi_sincontabilizar_egresos', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        s_tabName = "Pagos",
        s_url = URL(f = 'cfdi_sincontabilizar_pagos',
            args = request.args[:2] + ['master_' + str(request.function), request.args(3)]),
        s_idHtml = fn_createIDTabUnique(f = 'cfdi_sincontabilizar_pagos', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        s_tabName = "Ingresos cancelados",
        s_url = URL(f = 'cfdi_sincontabilizar_ingresoscancelados',
            args = request.args[:2] + ['master_' + str(request.function), request.args(3)]),
        s_idHtml = fn_createIDTabUnique(f = 'cfdi_sincontabilizar_ingresoscancelados', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        s_tabName = "Egresos cancelados",
        s_url = URL(f = 'cfdi_sincontabilizar_egresoscancelados',
            args = request.args[:2] + ['master_' + str(request.function), request.args(3)]),
        s_idHtml = fn_createIDTabUnique(f = 'cfdi_sincontabilizar_egresoscancelados', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        s_tabName = "Pagos cancelados",
        s_url = URL(f = 'cfdi_sincontabilizar_pagoscancelados',
            args = request.args[:2] + ['master_' + str(request.function), request.args(3)]),
        s_idHtml = fn_createIDTabUnique(f = 'cfdi_sincontabilizar_pagoscancelados', s_masterIdentificator = str(request.function))
        )

    if _s_action in (request.stv_fwk_permissions.btn_view.code,):

        _dbField_fecha = Field(
            'fecha',
            'date',
            default = None,
            required = False,
            notnull = False,
            widget = stv_widget_inputDate,
            label = ('Fecha'),
            comment = '',
            writable = False,
            readable = True,
            represent = stv_represent_date
            )

        _dbRow = dbc01.tempresa_plaza_sucursal_cajaschicas(_s_cajachica_id)
        if _dbRow:
            pass
        else:
            _dbRow = Storage(
                id = None,
                nombre = "No definida"
                )
        _dbRow.fecha = datetime.datetime(int(_L_multi_id[1]), int(_L_multi_id[2]), int(_L_multi_id[3]))

        _D_returnVars = _O_cat.process(
            b_skipAction = True,
            L_formFactoryFields = [dbc01.tempresa_plaza_sucursal_cajaschicas.id,
                                   _dbField_fecha],  # TODO se debe incluir el campo de fecha
            dbRow_formFactory = _dbRow  # TODO el row 1debe contener los datos en L_formFactoryFields
            )
    else:
        _D_returnVars = _O_cat.process(D_overrideView = _D_overrideView)

    _D_returnVars

    return (_D_returnVars)


def cfdi_errorespolizas():
    """ Relación de CFDIs sin contabilizar por caja chica

    @descripcion Errores en polizas

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.

        [arg2]: operación a realizar en el formulario
        [arg3]: id en caso de existir relación a un registro en la operación; el id se compone de 4 partes
            id de la caja chica
            año
            mes
            día

    @permiso btn_none
    #@permiso btn_new
    #@permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    #@permiso btn_signature


    """

    _dbTable = dbc01.tcfdi_prepoliza_asientoscontables

    _s_empresa_id = request.args(1, None)
    _s_action = request.args(2, request.stv_fwk_permissions.btn_none.code)
    _s_multi_id = request.args(3, '0-0-0-0')  # Se ponen 4 elementos separados '-' por default.
    _L_multi_id = _s_multi_id.split('-')
    if _L_multi_id[0] == 'None':
        _s_cajachica_id = None
    else:
        _s_cajachica_id = _L_multi_id[0]

    _qry = (
            (dbc01.tcfdis.etapa.belongs(TCFDIS.E_ETAPA.SIN_CONTABILIZAR, TCFDIS.E_ETAPA.NO_DEFINIDO))
            & (
                    (dbc01.tcfdis.empresa_id == _s_empresa_id)
                    & (dbc01.tcfdi_prepolizas.cfdi_id == dbc01.tcfdis.id)
                    & (dbc01.tempresas.id == dbc01.tcfdis.empresa_id)
                    & (dbc01.tempresas.fecha_inicial <= dbc01.tcfdis.fecha)
                    & (_dbTable.cfdi_prepoliza_id == dbc01.tcfdi_prepolizas.id)
                    & (_dbTable.errores != '')
                    & (_dbTable.errores != None)
            )
    )

    dbc01.tcfdis.dia.readable = True

    # Se configura el campo como debe de presentarte sus subcampos calculados
    dbc01.tempresa_plaza_sucursal_cajaschicas.empresa_plaza_sucursal_id.readable = True
    _L_fieldsToShow = [
        _dbTable.id,
        dbc01.tcfdis.dia,
        _dbTable.errores
        ]

    _L_GroupBy = [
        _dbTable.errores,
        ]

    _L_OrderBy = [
        dbc01.tcfdis.dia,
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_findall,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToGroupBy = _L_GroupBy,
        L_fieldsToSearch = [
            _dbTable.id,
            ],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        L_fieldnameIds = [_dbTable.id, dbc01.tcfdis.dia],
        x_offsetInArgs = request.args[:2]
        )

    _O_cat.addRowSearchResultsProperty('orderBy', _L_OrderBy)

    _D_returnVars = _O_cat.process()

    return (_D_returnVars)


def cfdi_sincontabilizar_porempresa():
    """ Relación de CFDIs sin contabilizar por empresa

    Args:
        [arg0]: operación a realizar en el formulario
        [arg1]: id en caso de existir relación a un registro en la operación
    """

    _dbTable = dbc01.tempresas

    # Query para filtrar las empresas que tengan CFDIs sin contabilizar.
    _qry = (
            (dbc01.tcfdis.etapa.belongs(TCFDIS.E_ETAPA.SIN_CONTABILIZAR, TCFDIS.E_ETAPA.NO_DEFINIDO))
            & (
                    (_dbTable.id == dbc01.tcfdis.empresa_id)
                    & (dbc01.tempresas.fecha_inicial <= dbc01.tcfdis.fecha)
            )
    )
    # Se cuenta el número de CFDIs que están sin contabilizar para mostrarse.
    _dbField_calculado = dbc01.tcfdis.id.count()

    _L_fieldsToShow = [
        _dbTable.id,
        dbc01.tcfdis.empresa_id,
        _dbTable.razonsocial,
        _dbField_calculado.with_alias('Total_CFDIs')
        ]

    # Se agrupa por el ID de la empresa.
    _L_GroupBy = [
        _dbTable.id,
        dbc01.tcfdis.empresa_id,
        _dbTable.razonsocial,
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_findall,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToGroupBy = _L_GroupBy,
        L_fieldsToSearch = [
            _dbTable.id,
            ],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        L_fieldnameIds = [_dbTable.id]
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_plaza_sucursal_cajaschicas,
        s_url = URL(
            f = 'cfdi_sincontabilizar_porcajachica', args = ['master_' + str(request.function),
                                                             request.args(1)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'cfdi_sincontabilizar_porcajachica', s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        s_tabName = 'Errores póliza',
        s_url = URL(f = 'cfdi_errorespolizas', args = ['master_' + str(request.function), request.args(1)]),
        s_idHtml = fn_createIDTabUnique(f = 'cfdi_errorespolizas', s_masterIdentificator = str(request.function))
        )
    _D_returnVars = _O_cat.process()

    return (_D_returnVars)



def cfdi_sincontabilizar_pagos_plus_plus():
    """ Especifica la información necesaria para contabilizar el CFDI de pago.

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: id compuesto por 4 partes
            id de la caja chica
            año
            mes
            día
        arg4: master_[nombre función maestro]
        arg5: cfdi_id
        arg6: master_[nombre función maestro]
        arg7: cfdi_complementopago_id
        [arg8]: operación a realizar en el formulario
        [arg9]: id en caso de existir relación a un registro en la operación
    """

    _dbTable = dbc01.tcfdi_registropagos
    _s_empresa_id = request.args(1, None)
    _s_multi_id = request.args(3, '0-0-0-0')  # Se ponen 4 elementos separados '-' por default.
    _L_multi_id = _s_multi_id.split('-')
    if _L_multi_id[0] == 'None':
        _s_cajachica_id = None
    else:
        _s_cajachica_id = _L_multi_id[0]

    _s_cfdi_id = request.args(5, None)
    _s_cfdi_complementopago_id = request.args(7, None)
    _s_action = request.args(8, request.stv_fwk_permissions.btn_none.code)

    _dbRow_cfdi = dbc01.tcfdis(_s_cfdi_id)

    # Si no se encuentra el id del registro y no esta guardando.
    if (_s_action in (request.stv_fwk_permissions.btn_none.code,)):

        _dbRows = dbc01(
            (_dbTable.cfdi_id == _s_cfdi_id)
            & (_dbTable.cfdi_complementopago_id == _s_cfdi_complementopago_id)
            ).select(
            _dbTable.ALL,
            orderby = _dbTable.id,
            )

        # Si solamente se encuentra un registro...
        if (len(_dbRows) == 1):
            # ...se procede a configurar los parametros para editar.
            request.args = request.args[:8] + [request.stv_fwk_permissions.btn_edit.code, _dbRows.first().id]
        # Si no se ecuentran registros...
        elif (len(_dbRows) == 0):
            # ...se procede a configurar parámetros para nuevo.
            request.args = request.args[:8] + [request.stv_fwk_permissions.btn_new.code]
        # Si se ecuentran más de un registro...
        else:
            # ... TODO hay un error, sin embargo se procede a editar el último registro.
            request.args = request.args[:8] + [request.stv_fwk_permissions.btn_edit.code, _dbRows.last().id]
    elif (_s_action in (request.stv_fwk_permissions.btn_save.code)):
        _dbTable.cfdi_id.default = _s_cfdi_id
        if request.vars.empresa_plaza_sucursal_cajachica_id != _dbRow_cfdi.empresa_plaza_sucursal_cajachica_id:
            _dbRow_cfdi.empresa_plaza_sucursal_cajachica_id = request.vars.empresa_plaza_sucursal_cajachica_id
            _dbRow_cfdi.update_record()

            # Se agrega commit para siempre hacer update a la caja chica sin importar que se genere o no la prepoliza.
            dbc01.commit()
        else:
            # Mantener mismo row.
            pass
    else:
        pass

    _dbRow_cfdi_complementopago = dbc01.tcfdi_complementopagos(_s_cfdi_complementopago_id)

    _dbRowRegistroPago = dbc01(
        (dbc01.tcfdi_registropagos.cfdi_complementopago_id == _s_cfdi_complementopago_id)
        & (dbc01.tcfdi_registropagos.cfdi_id == _s_cfdi_id)
        ).select(
        dbc01.tcfdi_registropagos.ALL
        ).first()  # Se selecciona el primer registro porque un complemento de pago no puede tener más de 2 registro de pago
    if _dbRowRegistroPago:
        if not _dbRowRegistroPago.formapago_id:
            _dbRowRegistroPago.formapago_id = _dbRow_cfdi_complementopago.formapago_id
            _dbRowRegistroPago.update_record()
        else:
            # ya contiene una forma de pago, no es necesario actualizar el row.
            pass
    else:
        pass

    _dbTable.formapago_id.default = _dbRow_cfdi_complementopago.formapago_id

    _dbTable.bancocontpaqi_id_cliente.requires = IS_IN_DB(
        dbc01(
            dbc01.tempresa_bancoscontpaqi.empresa_id == _s_empresa_id
            ),
        'tempresa_bancoscontpaqi.id',
        dbc01.tempresa_bancoscontpaqi._format
        )

    if (_dbRow_cfdi.empresa_plaza_sucursal_cajachica_id):
        _dbRow_cajachica = dbc01.tempresa_plaza_sucursal_cajaschicas(_dbRow_cfdi.empresa_plaza_sucursal_cajachica_id)
        if (_dbRow_cfdi_complementopago.formapago_id in (TFORMASPAGO.CHEQUE,)):
            _dbTable.empresa_cuentabancaria_id.required = True
            _dbTable.empresa_cuentabancaria_id.notnull = True
            _dbTable.empresa_cuentabancaria_id.default = _dbRow_cajachica.empresa_cuentabancaria_id_cheques
            _dbTable.bancocontpaqi_id_cliente.required = True
            _dbTable.bancocontpaqi_id_cliente.notnull = True
            _dbTable.cheque.required = True
            _dbTable.cheque.requires = IS_NOT_EMPTY()
            _dbTable.cuenta_cliente.required = True
            _dbTable.cuenta_cliente.requires = IS_NOT_EMPTY()
            _dbTable.fechadeposito.required = True
            _dbTable.fechadeposito.notnull = True
            _dbTable.fechadeposito.default = _dbRow_cfdi.dia

        elif (_dbRow_cfdi_complementopago.formapago_id in (TFORMASPAGO.EFECTIVO,)):
            _dbTable.monto.required = True
            _dbTable.monto.notnull = True

        elif (_dbRow_cfdi_complementopago.formapago_id in (TFORMASPAGO.TDD, TFORMASPAGO.TDC)):
            _dbTable.empresa_cuentabancaria_terminal_id.required = True
            _dbTable.empresa_cuentabancaria_terminal_id.notnull = True
            _dbTable.empresa_cuentabancaria_terminal_id.default = _dbRow_cajachica.empresa_cuentabancaria_terminal_id
            _dbTable.empresa_cuentabancaria_terminal_id.requires = IS_IN_DB(
                dbc01(
                    dbc01.tempresa_cuentabancaria_terminales.empresa_plaza_sucursal_id == _dbRow_cajachica.empresa_plaza_sucursal_id
                    ), 'tempresa_cuentabancaria_terminales.id', dbc01.tempresa_cuentabancaria_terminales._format
                )
            _dbTable.bancocontpaqi_id_cliente.required = True
            _dbTable.bancocontpaqi_id_cliente.notnull = True
            _dbTable.tarjeta.required = True
            _dbTable.tarjeta.requires = IS_NOT_EMPTY()
            _dbTable.autorizacion.required = True
            _dbTable.autorizacion.requires = IS_NOT_EMPTY()
            _dbTable.operacion.required = True
            _dbTable.operacion.requires = IS_NOT_EMPTY()
            _dbTable.fechadeposito.required = True
            _dbTable.fechadeposito.notnull = True
            _dbTable.fechadeposito.default = _dbRow_cfdi.dia

        elif (_dbRow_cfdi_complementopago.formapago_id in (TFORMASPAGO.TRANSFERENCIA,)):
            _dbTable.empresa_cuentabancaria_id.required = True
            _dbTable.empresa_cuentabancaria_id.notnull = True
            _dbTable.empresa_cuentabancaria_id.default = _dbRow_cajachica.empresa_cuentabancaria_id_transferencia
            _dbTable.bancocontpaqi_id_cliente.required = True
            _dbTable.bancocontpaqi_id_cliente.notnull = True
            _dbTable.cuenta_cliente.required = True
            _dbTable.cuenta_cliente.requires = IS_NOT_EMPTY()
            _dbTable.referencia.required = True
            _dbTable.referencia.requires = IS_NOT_EMPTY()
            _dbTable.claverastero.required = True
            _dbTable.claverastero.requires = IS_NOT_EMPTY()
            _dbTable.fechadeposito.required = True
            _dbTable.fechadeposito.notnull = True
            _dbTable.fechadeposito.default = _dbRow_cfdi.dia
        else:
            pass

    _dbTable.empresa_cuentabancaria_id.requires = IS_IN_DB(
        dbc01(
            (dbc01.tempresa_cuentasbancarias.empresa_id == request.args[1])
            ),
        'tempresa_cuentasbancarias.id',
        dbc01.tempresa_cuentasbancarias._format
        )

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tcfdi_complementopagos,
        xFieldLinkMaster = [_dbTable.cfdi_complementopago_id],
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_new,
            request.stv_fwk_permissions.btn_save,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_edit
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab + "_" + str(_s_cfdi_complementopago_id),
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.cfdi_id,
            _dbTable.empresa_cuentabancaria_terminal_id,
            _dbTable.empresa_cuentabancaria_id,
            ],
        L_fieldsToSearch = [_dbTable.id],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        x_offsetInArgs = request.args[:6],
        Es_behavior = STV_FWK_FORM.ES_BEHAVIOR.S_PLUS_FORM
        )

    _O_cat.addRowContentProperty(
        'formHidden', {
            'cfdi_id'     : str(_s_cfdi_id),
            'formapago_id': str(_dbRow_cfdi_complementopago.formapago_id)
            }
        )

    _O_cat.addRowContentEvent('onValidation', _cfdi_sincontabilizar_pagos_plus_plus_validation)

    _D_returnVars = _O_cat.process()

    _D_returnVars.n_formapago_id = _dbRow_cfdi_complementopago.formapago_id

    if (_D_returnVars.D_tabOptions.b_accepted):
        # Este commit se hace para evitar que se haga rollback al registro de pago.
        dbc01.commit()

        if _dbRow_cfdi.etapa in (TCFDIS.E_ETAPA.SIN_CONTABILIZAR, TCFDIS.E_ETAPA.CON_PREPOLIZA):
            _D_returnGenerarPrepoliza = STV_FWK_PREPOLIZA().generar(_s_cfdi_id)
            if _D_returnGenerarPrepoliza.L_msgError:
                for _s_error in _D_returnGenerarPrepoliza.L_msgError:
                    stv_flashError("Generación de prepoliza", _s_error)

            else:
                stv_flashSuccess("Generación de prepoliza", "Prepoliza generada correctamente")
        else:
            stv_flashError(
                "Generación de prepoliza",
                "La prepoliza no puede generarse de nuevo debido a que ésta ya tiene una generada"
                )

    _D_returnVars.dbRow_cfdiformapago = _dbRow_cfdi_complementopago
    _D_returnVars.cfdiForm = _O_cat.createFactoryForm(
        L_formFactoryFields = [
            dbc01.tcfdis.empresa_plaza_sucursal_cajachica_id
            ],
        dbRow_formFactory = Storage(
            empresa_plaza_sucursal_cajachica_id = _dbRow_cfdi.empresa_plaza_sucursal_cajachica_id
            ),
        s_factoryTableName = False,
        D_inputsHidden = {}
        )

    return (_D_returnVars)


def _cfdi_sincontabilizar_pagos_plus_plus_validation(O_form):
    """Validación para el formulario ingresos_plus_plus.
    """

    _s_cfdi_complementopago_id = request.args(7, None)

    _dbRow_cfdi_complementopago = dbc01.tcfdi_complementopagos(_s_cfdi_complementopago_id)

    if _dbRow_cfdi_complementopago.formapago_id == TFORMASPAGO.EFECTIVO:

        if int(O_form.vars.tratoefectivo) == TCFDI_REGISTROPAGOS.E_TRATOEFECTIVO.RETENIDOCAJA:
            pass
        else:
            if not O_form.vars.empresa_cuentabancaria_id:
                O_form.errors.empresa_cuentabancaria_id = "Si se seleccionó Depósito a bancos la cuenta bancaria no puede ir vacía."
            else:
                pass

            if not O_form.vars.referencia:
                O_form.errors.referencia = "Si se seleccionó Depósito a bancos la referencia no puede ir vacía."
            else:
                pass

            if not O_form.vars.fechadeposito:
                O_form.errors.fechadeposito = "Si se seleccionó Depósito a bancos la fecha del depósito no puede ir vacía."
            else:
                pass

    else:
        pass

    #     if O_form.vars.monto < 0:
    #         O_form.errors.monto = "El monto no puede ser negativo."
    #     else:
    #         pass

    return O_form


def cfdi_sincontabilizar_pagos_plus():
    """ Formulario para mostrar los complementos de pago y poder registrar el pago

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: id compuesto por 4 partes
            id de la caja chica
            año
            mes
            día
        arg4: master_[nombre función maestro]
        arg5: cfdi_id
        [arg6]: operación a realizar en el formulario
        [arg7]: id en caso de existir relación a un registro en la operación
    """

    _dbTable = dbc01.tcfdi_complementopagos
    _s_empresa_id = request.args(1, None)
    _s_multi_id = request.args(3, '0-0-0-0')  # Se ponen 4 elementos separados '-' por default.
    _L_multi_id = _s_multi_id.split('-')
    if _L_multi_id[0] == 'None':
        _s_cajachica_id = None
    else:
        _s_cajachica_id = _L_multi_id[0]

    _s_cfdi_id = request.args(5, None)
    _s_action = request.args(6, request.stv_fwk_permissions.btn_none.code)

    _dbRow_cfdi = dbc01.tcfdis(_s_cfdi_id)

    _dbRowsPagos = dbc01(
        (dbc01.tcfdi_complementopagos.cfdi_id == _s_cfdi_id)
        ).select(
        dbc01.tcfdi_complementopagos.ALL,
        )

    for _dbRowPago in _dbRowsPagos:

        _dbRowsDoctosRelacionados = dbc01(
            dbc01.tcfdi_complementopago_doctosrelacionados.cfdi_complementopago_id == _dbRowPago.id
            ).select(
            dbc01.tcfdi_complementopago_doctosrelacionados.cfdirelacionado_id
            )

        for _dbRowDoctoRelacionado in _dbRowsDoctosRelacionados:

            if _dbRowDoctoRelacionado.cfdirelacionado_id:

                _dbRowsRegistrospago = dbc01(
                    (dbc01.tcfdi_registropagos.cfdi_id == _dbRowDoctoRelacionado.cfdirelacionado_id)
                    ).select(
                    dbc01.tcfdi_registropagos.id
                    )

                if not _dbRowsRegistrospago:
                    # Si el CFDI no tiene registros de pago se le genera para poderse contabilizar...

                    _dbRowCfdiRelacionado = dbc01.tcfdis(_dbRowDoctoRelacionado.cfdirelacionado_id)

                    _D_registropago = Storage(
                        cfdi_id = _dbRowCfdiRelacionado.id,
                        formapago_id = _dbRowCfdiRelacionado.formapago_id,
                        monto = _dbRowCfdiRelacionado.total
                        )

                    dbc01.tcfdi_registropagos.insert(**_D_registropago)
                else:
                    # ... de lo contrario no hace nada.
                    pass
            else:
                pass

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tcfdis,
        xFieldLinkMaster = [_dbTable.cfdi_id],
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_findall
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab + "_" + str(_s_cfdi_id),
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.fechapago,
            _dbTable.formapago_id,
            _dbTable.monedacontpaqi_id,
            _dbTable.monto,
            ],
        L_fieldsToSearch = [_dbTable.id],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        x_offsetInArgs = request.args[:4],
        )

    # Se crea una nueva columna en el grid. Se agrega un "+" y se especifica la función entorno a donde funcionará el botón "+".
    _O_cat.addRowSearchResults_addColumnBegin(
        es_typeColumn = STV_FWK_FORM.ES_TYPECOLUMN.S_PLUS,
        s_colname = "plus",
        D_field = Storage(
            readable = True
            ),
        m_classHtml = lambda D_column, dbRow: "stv_rowresults_plus",
        D_data = Storage(
            url = URL(
                f = 'cfdi_sincontabilizar_pagos_plus_plus', args = request.args[:6] + [
                    'master_' + str(request.function)]
                )
            )
        )

    _D_returnVars = _O_cat.process()

    if (_dbRow_cfdi.etapa in (TCFDIS.E_ETAPA.CON_PREPOLIZA, TCFDIS.E_ETAPA.CON_POLIZA)) and (
            _s_action == request.stv_fwk_permissions.btn_findall.code):
        _D_returnVars.refrescarmaestro = True
    else:
        _D_returnVars.refrescarmaestro = False

    _D_returnVars.D_useView.b_serverCreatesTab = True
    _D_returnVars.D_useView.b_cleanGrid = True

    return (_D_returnVars)



def cfdi_sincontabilizar_ingresos():
    """ Relación de CFDIs sin contabilizar para una caja chica por concepto de ingresos

    @descripcion Sin Contabilizar Ingresos

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Caja chica y Fecha
    @keyword arg3: empresa_plaza_sucursal_cajachica_id - Año - Mes - Día.
    @keyword [arg4]: operación a realizar en el formulario.
    @keyword [arg5]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    #@permiso btn_new
    #@permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    @permiso btn_view
    #@permiso btn_invalidate
    #@permiso btn_validate
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    #@permiso btn_signature
    @permiso special_permisson_01:[fas fa-file-code] Ignorar movimiento
    """

    _dbTable = dbc01.tcfdis

    _s_empresa_id = request.args(1, None)
    _s_multi_id = request.args(3, '0-0-0-0')  # Se ponen 4 elementos separados '-' por default.
    _L_multi_id = _s_multi_id.split('-')
    if _L_multi_id[0] == 'None':
        _s_cajachica_id = None
    else:
        _s_cajachica_id = _L_multi_id[0]

    _s_action = request.args(4, request.stv_fwk_permissions.btn_none.code)

    _dt_fecha = datetime.datetime(int(_L_multi_id[1]), int(_L_multi_id[2]), int(_L_multi_id[3]))
    # Se generan las fechas en formato datetime para todo el rango del día
    _dt_fechaDesde = _dt_fecha.replace(hour = 0, minute = 0, second = 0, microsecond = 0)
    _dt_fechaHasta = _dt_fechaDesde + datetime.timedelta(days = 1)

    # Se crea el Query para identificar si el CFDI está como "sin contabilizar" y filstrando SOLO por CFDI que sea de tipo ingreso.
    _qry = (
            (_dbTable.empresa_id == _s_empresa_id)
            & (dbc01.tempresas.id == _dbTable.empresa_id)
            & (_dbTable.fecha >= dbc01.tempresas.fecha_inicial)
            & (_dbTable.etapa == TCFDIS.E_ETAPA.SIN_CONTABILIZAR)
            & (_dbTable.rol == TCFDIS.E_ROL.EMISOR)
            & (_dbTable.empresa_plaza_sucursal_cajachica_id == _s_cajachica_id)
            & (_dbTable.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO)
            & (_dbTable.fecha >= _dt_fechaDesde)
            & (_dbTable.fecha < _dt_fechaHasta)
    )

    _L_fieldsToShow = [
        _dbTable.id,
        #   _dbTable.rol,
        _dbTable.empresa_serie_id,
        _dbTable.folio,
        _dbTable.fecha,
        _dbTable.metodopago_id,
        _dbTable.formapago_id,
        _dbTable.total,
        #         dbc01.tcfdi_registropagos.empresa_cuentabancaria_terminal_id,
        #         dbc01.tcfdi_registropagos.empresa_cuentabancaria_id,
        _dbTable.receptorrfc,
        _dbTable.receptornombrerazonsocial
        ]

    _L_OrderBy = [
        _dbTable.fecha,
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_findall
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = [
            _dbTable.id,
            ],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        L_fieldnameIds = [_dbTable.id],
        x_offsetInArgs = request.args[:4]
        )

    #     _O_cat.addRowSearchResultsLeftJoin(
    #         dbc01.tcfdi_registropagos.on(
    #             (dbc01.tcfdi_registropagos.cfdi_id == _dbTable.id)
    #             )
    #         )

    _O_cat.addRowSearchResultsProperty('orderBy', _L_OrderBy)

    # Se agrega el boton especial 01 que funciona para mandar el CFDI a CFDI no contabilizable.
    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionLabel = "No contabilizar CFDI",
        s_permissionIcon = "fas fa-window-close",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
            confirm = Storage(
                s_title = "Favor de confirmar operación",
                s_text = "¿Estás seguro de querer marcar este CFDI como 'No contabilizar'?",
                s_type = "warning",
                s_btnconfirm = "Confirmar",
                s_btncancel = "Cancelar",
                )
            ),
        L_options = []
        )

    # Se crea una nueva columna en el grid, se agrega un "+" y se especifica la función entorno a donde funcionará el botón "+".
    _O_cat.addRowSearchResults_addColumnBegin(
        es_typeColumn = STV_FWK_FORM.ES_TYPECOLUMN.S_PLUS,
        s_colname = "plus",
        D_field = Storage(
            readable = True
            ),
        m_classHtml = lambda D_column, dbRow: "stv_rowresults_plus",
        D_data = Storage(
            url = URL(
                f = 'cfdi_sincontabilizar_ingresos_plus', args = request.args[:4] + [
                    'master_' + str(request.function)]
                )
            )
        )

    # Código para poder jalar la vista de CFDIS y utilizarlas en esta función.
    if _s_action in (request.stv_fwk_permissions.btn_view.code,):
        from gluon.storage import List

        _s_cfdi_id = str(request.args(5, 0))  # Se guarda el argumento que se quiere utilizar dentro de una variable.
        request.args = List()
        request.args.append(_s_action)  # Se mete _s_action dentro de la lista con la función append.
        request.args.append(_s_cfdi_id)

        _D_returnVars = cfdisemitidos()
        # Usar el os.path.join para evitar conflicto por la diagonal (/) en sistemas operativos.
        response.view = os.path.join('250cfdi', 'cfdisemitidos.html')

    elif _s_action in (request.stv_fwk_permissions.special_permisson_01.code,):

        _s_cfdi_id = str(request.args(5, 0))  # Se guarda el argumento que se quiere utilizar dentro de una variable

        _dbRowCfdi = dbc01.tcfdis(_s_cfdi_id)

        if _dbRowCfdi.etapa != TCFDIS.E_ETAPA.SIN_CONTABILIZAR:
            stv_flashError("Cambio de estatus del CFDI",
                "El CFDI no se puede cambiar de estado ya que se encuentra con un estatus diferente a 'Sin contabilizar'.")
        else:
            _dbRowCfdi.etapa = TCFDIS.E_ETAPA.NO_CONTABILIZAR
            _dbRowCfdi.update_record()
            stv_flashSuccess("Cambio de estatus del CFDI", "Cambio hecho correctamente.")

            _D_overrideView = request.stv_fwk_permissions.view_refresh

            _D_returnVars = _O_cat.process(D_overrideView = _D_overrideView)
    else:
        _D_returnVars = _O_cat.process()

    return _D_returnVars


def cfdi_sincontabilizar_ingresos_plus():
    """ Formulario para mostrar los distintios registros de pago por CFDI de tipo ingreso.

    Args:
        arg0: texto en relación al maestro empresa
        arg1: empresa_id
        arg2: texto en relación al maestro cajachica y fecha
        arg3: id compuesto por 4 partes
            id de la caja chica
            año
            mes
            día
        arg4: master_[nombre función maestro]
        arg5: cfdi_id
        [arg6]: operación a realizar en el formulario
        [arg7]: id en caso de existir relación a un registro en la operación
    """

    _dbTable = dbc01.tcfdi_registropagos

    _s_empresa_id = request.args(1, None)

    _s_multi_id = request.args(3, '0-0-0-0')  # Se ponen 4 elementos separados '-' por default.

    _L_multi_id = _s_multi_id.split('-')

    if _L_multi_id[0] == 'None':
        _s_cajachica_id = None
    else:
        _s_cajachica_id = _L_multi_id[0]

    _s_cfdi_id = request.args(5, None)

    _s_action = request.args(6, request.stv_fwk_permissions.btn_none.code)

    _dbRow_cfdi = dbc01.tcfdis(_s_cfdi_id)

    _dbTable.cfdi_id.default = _s_cfdi_id

    _dbRowsRegistrospago = dbc01(
        _dbTable.cfdi_id == _s_cfdi_id
        ).select(
        _dbTable.ALL
        )

    if not _dbRowsRegistrospago:
        # Si no tiene ningún registro de pago asociado este CFDI entonces se crea uno, se le hereda su formapago_id y el total del CFDI al monto.

        _D_registropago = Storage(
            cfdi_id = _s_cfdi_id,
            formapago_id = _dbRow_cfdi.formapago_id,
            monto = _dbRow_cfdi.total,
            tipo_cfdi = TCFDI_REGISTROPAGOS.TIPO_CFDI.VIGENTE
            )

        _dbTable.insert(**_D_registropago)

    else:
        pass

    _dbTable.formapago_id.notnull = True
    _dbTable.formapago_id.required = True

    if (_s_action == request.stv_fwk_permissions.btn_new.code):
        # Si el _s_action es btn_nuevo se recorrerán los registros de pago para calcular su total de montos.

        _n_registrospago_totalmontos = 0
        for _dbRowRegistropago in _dbRowsRegistrospago:
            _n_registrospago_totalmontos += _dbRowRegistropago.monto

        _n_resultadomonto = _dbRow_cfdi.total - _n_registrospago_totalmontos

        if _n_resultadomonto > 0:
            _dbTable.monto.default = _n_resultadomonto
        else:
            pass

    else:
        pass

    _qry = (
            (_dbTable.cfdi_id == _s_cfdi_id)
            & (_dbTable.tipo_cfdi == TCFDI_REGISTROPAGOS.TIPO_CFDI.VIGENTE)
    )

    _dbTable.cfdi_id.readable = True

    _dbTable.formapago_id.requires = IS_IN_DB(
        db01(
            db01.tformaspago.id.belongs(
                TFORMASPAGO.EFECTIVO,
                TFORMASPAGO.TDD,
                TFORMASPAGO.TDC,
                TFORMASPAGO.CHEQUE,
                TFORMASPAGO.TRANSFERENCIA,
                )
            ),
        'tformaspago.id',
        db01.tformaspago._format
        )

    _dbTable.empresa_cuentabancaria_id.requires = IS_IN_DB(
        dbc01(
            (dbc01.tempresa_cuentasbancarias.empresa_id == request.args[1])
            ),
        'tempresa_cuentasbancarias.id',
        dbc01.tempresa_cuentasbancarias._format
        )

    _dbRow_cajachica = dbc01.tempresa_plaza_sucursal_cajaschicas(_dbRow_cfdi.empresa_plaza_sucursal_cajachica_id)

    _dbTable.empresa_cuentabancaria_terminal_id.requires = IS_IN_DB(
        dbc01(
            (dbc01.tempresa_cuentabancaria_terminales.empresa_plaza_sucursal_id == _dbRow_cajachica.empresa_plaza_sucursal_id)
            ),
        'tempresa_cuentabancaria_terminales.id',
        dbc01.tempresa_cuentabancaria_terminales._format
        )

    if _dbRow_cfdi.formapago_id == TFORMASPAGO.POR_DEFINIR:
        _L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_findall
            ]
    else:
        _L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_new,
            request.stv_fwk_permissions.btn_save,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_remove,
            request.stv_fwk_permissions.btn_edit,
            request.stv_fwk_permissions.btn_findall
            ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        dbTableMaster = dbc01.tcfdis,
        xFieldLinkMaster = [_dbTable.cfdi_id],
        L_visibleButtons = _L_visibleButtons,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab + "_" + str(_s_cfdi_id),
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.cfdi_id,
            _dbTable.formapago_id,
            _dbTable.monto,
            _dbTable.empresa_cuentabancaria_terminal_id,
            _dbTable.empresa_cuentabancaria_id,
            ],
        L_fieldsToSearch = [_dbTable.id],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        x_offsetInArgs = request.args[:4],
        )

    # Se crea una nueva columna en el grid. Se agrega un "+" y se especifica la función entorno a donde funcionará el botón "+".
    _O_cat.addRowSearchResults_addColumnBegin(
        es_typeColumn = STV_FWK_FORM.ES_TYPECOLUMN.S_PLUS,
        s_colname = "plus",
        D_field = Storage(
            readable = True
            ),
        m_classHtml = lambda D_column, dbRow: "stv_rowresults_plus",
        D_data = Storage(
            url = URL(
                f = 'cfdi_sincontabilizar_ingresos_plus_plus', args = request.args[:6] + [
                    'master_' + str(request.function)]
                )
            )
        )

    _O_cat.addRowContentEvent('onValidation', _cfdi_sincontabilizar_ingresos_plus_validation)

    _D_returnVars = _O_cat.process()

    if (_dbRow_cfdi.etapa in (TCFDIS.E_ETAPA.CON_PREPOLIZA, TCFDIS.E_ETAPA.CON_POLIZA)) and (
            _s_action == request.stv_fwk_permissions.btn_findall.code):
        _D_returnVars.refrescarmaestro = True
    else:
        _D_returnVars.refrescarmaestro = False

    if (_s_action == request.stv_fwk_permissions.btn_save.code) and _D_returnVars.rowContent_form.accepted:
        # Este commit se hace para evitar que se haga rollback al registro de pago.
        dbc01.commit()
        if _dbRow_cfdi.etapa in (TCFDIS.E_ETAPA.SIN_CONTABILIZAR, TCFDIS.E_ETAPA.CON_PREPOLIZA):
            _D_returnGenerarPrepoliza = STV_FWK_PREPOLIZA().generar(_s_cfdi_id)
            if _D_returnGenerarPrepoliza.L_msgError:
                for _s_error in _D_returnGenerarPrepoliza.L_msgError:
                    stv_flashError("Generación de prepoliza", _s_error)

            else:
                stv_flashSuccess("Generación de prepoliza", "Prepoliza generada correctamente")
        else:
            stv_flashError(
                "Generación de prepoliza",
                "La prepoliza no puede generarse de nuevo debido a que ésta ya tiene una generada"
                )

    else:
        pass

    _D_returnVars.D_useView.b_serverCreatesTab = True

    _D_returnVars.D_useView.b_cleanGrid = True

    return (_D_returnVars)


def _cfdi_sincontabilizar_ingresos_plus_validation(O_form):
    """ Validación para el formulario de ingresos_plus.
    """

    _s_id = O_form.record_id

    _s_empresa_id = request.args(1, None)

    _s_diferencia = O_form.vars.diferencia

    if _s_diferencia != 'on':

        if O_form.vars.formapago_id:

            if int(O_form.vars.formapago_id) == TFORMASPAGO.EFECTIVO:

                if not O_form.vars.tratoefectivo or (O_form.vars.tratoefectivo == "null"):
                    O_form.errors.tratoefectivo = "Si se seleccionó la forma de pago efectivo se debe de seleccionar un trato del efectivo."
                else:
                    if int(O_form.vars.tratoefectivo) == TCFDI_REGISTROPAGOS.E_TRATOEFECTIVO.RETENIDOCAJA:
                        pass
                    else:
                        if not O_form.vars.empresa_cuentabancaria_id:
                            O_form.errors.empresa_cuentabancaria_id = "Si se seleccionó Depósito a bancos la cuenta bancaria no puede ir vacía."
                        else:
                            pass

                        if not O_form.vars.referencia:
                            O_form.errors.referencia = "Si se seleccionó Depósito a bancos la referencia no puede ir vacía."
                        else:
                            pass

                        if not O_form.vars.fechadeposito:
                            O_form.errors.fechadeposito = "Si se seleccionó Depósito a bancos la fecha del depósito no puede ir vacía."
                        else:
                            pass

                    if not O_form.vars.monto:
                        O_form.errors.monto = "El monto tiene que ser capturado."
                    else:
                        pass

            elif int(O_form.vars.formapago_id) in (TFORMASPAGO.TDD, TFORMASPAGO.TDC,):

                if not O_form.vars.empresa_cuentabancaria_terminal_id:
                    O_form.errors.empresa_cuentabancaria_terminal_id = "La terminal tiene que ser capturado."
                else:
                    pass

                if not O_form.vars.bancocontpaqi_id_cliente:
                    O_form.errors.bancocontpaqi_id_cliente = "El banco del cliente tiene que ser capturado."
                else:
                    pass

                if not O_form.vars.tarjeta:
                    O_form.errors.tarjeta = "La tarjeta tiene que ser capturada."
                else:
                    pass

                if not O_form.vars.autorizacion:
                    O_form.errors.autorizacion = "La autorización tiene que ser capturada."
                else:
                    pass

                if not O_form.vars.operacion:
                    O_form.errors.operacion = "La operación tiene que ser capturada."
                else:
                    pass

                if not O_form.vars.fechadeposito:
                    O_form.errors.fechadeposito = "La fecha del deposito tiene que ser capturada."
                else:
                    pass

                if not O_form.vars.monto:
                    O_form.errors.monto = "El monto tiene que ser capturado."
                else:
                    pass

            elif int(O_form.vars.formapago_id) == TFORMASPAGO.CHEQUE:

                if not O_form.vars.empresa_cuentabancaria_id:
                    O_form.errors.empresa_cuentabancaria_id = "La cuenta bancaria tiene que ser capturada."
                else:
                    pass

                if not O_form.vars.bancocontpaqi_id_cliente:
                    O_form.errors.bancocontpaqi_id_cliente = "La banco del cliente tiene que ser capturado."
                else:
                    pass

                if not O_form.vars.cheque:
                    O_form.errors.cheque = "El cheque tiene que ser capturado."
                else:
                    pass

                if not O_form.vars.cuenta_cliente:
                    O_form.errors.cuenta_cliente = "La cuenta del cliente tiene que ser capturada."
                else:
                    pass

                if not O_form.vars.fechadeposito:
                    O_form.errors.fechadeposito = "La fecha del deposito tiene que ser capturada."
                else:
                    pass

                if not O_form.vars.monto:
                    O_form.errors.monto = "El monto tiene que ser capturado."
                else:
                    pass
            else:
                # Si no es ninguna de las anteriores es Transferencia.

                if not O_form.vars.empresa_cuentabancaria_id:
                    O_form.errors.empresa_cuentabancaria_id = "La cuenta bancaria tiene que ser capturada."
                else:
                    pass

                if not O_form.vars.bancocontpaqi_id_cliente:
                    O_form.errors.bancocontpaqi_id_cliente = "La banco del cliente tiene que ser capturado."
                else:
                    pass

                if not O_form.vars.cuenta_cliente:
                    O_form.errors.cuenta_cliente = "La cuenta del cliente tiene que ser capturada."
                else:
                    pass

                if not O_form.vars.referencia:
                    O_form.errors.referencia = "La referencia tiene que ser capturada."
                else:
                    pass

                if not O_form.vars.claverastero:
                    O_form.errors.claverastero = "La clave de rastreo tiene que ser capturada."
                else:
                    pass

                if not O_form.vars.fechadeposito:
                    O_form.errors.fechadeposito = "La fecha del deposito tiene que ser capturada."
                else:
                    pass

                if not O_form.vars.monto:
                    O_form.errors.monto = "El monto tiene que ser capturado."
                else:
                    pass
        else:
            O_form.errors.formapago_id = "Se tiene que capturar una forma de pago para poder generar un registro de pago."

        if O_form.vars.monto < 0:
            O_form.errors.monto = "El monto no puede ser negativo."
        else:
            pass
    else:
        #  Si la diferencia es 'on' (por alguna razón la base de datos antes de guardarlo lo hace ON antes de TRUE el valor.) entonces se verifica si la forma de pago es
        # confusión y si el valor absoluto es menor al establecido en la empresa.

        if int(O_form.vars.formapago_id) == TFORMASPAGO.CONFUSION:
            _dbRowEmpresa = dbc01.tempresas(_s_empresa_id)

            if abs(O_form.vars.monto) > _dbRowEmpresa.maximosobrantefaltante:
                O_form.errors.monto = "El monto de diferencia o sobrante no puede ser mayor al que se parametrizó en la empresa. Favor de hablar con un supervisor."
            else:
                pass
        else:
            O_form.errors.formapago_id = "Si se seleccionó el checkbox de diferencias la forma de pago no puede ser diferente a la de confusión."

    return O_form


def cfdi_sincontabilizar_ingresos_plus_plus():
    """ Formulario para especificar la información necesaria para contabilizar el ingreso

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: id compuesto por 4 partes
            id de la caja chica
            año
            mes
            día
        arg4: master_[nombre función maestro]
        arg5: cfdi_id
        arg6: master_[nombre función maestro]
        arg7: cfdi_registropago_id
        [arg8]: operación a realizar en el formulario
        [arg9]: id en caso de existir relación a un registro en la operación
    """

    _dbTable = dbc01.tcfdi_registropagos
    _s_empresa_id = request.args(1, None)
    _s_multi_id = request.args(3, '0-0-0-0')  # Se ponen 4 elementos separados '-' por default.
    _L_multi_id = _s_multi_id.split('-')
    if _L_multi_id[0] == 'None':
        _s_cajachica_id = None
    else:
        _s_cajachica_id = _L_multi_id[0]
    _s_cfdi_id = request.args(5, None)
    _s_registropago_id = request.args(7, None)
    _s_action = request.args(8, request.stv_fwk_permissions.btn_none.code)

    _dbRow_cfdi = dbc01.tcfdis(_s_cfdi_id)

    _dbRow_registropago = dbc01.tcfdi_registropagos(_s_registropago_id)

    ### TODO: Esto se puede hacer en ingresos_plus? En caso de que tenga un solo registro de pago y no tenga formapago_id que la actualice.
    if not ((_dbRow_registropago.formapago_id) or 0) and (_s_action != request.stv_fwk_permissions.btn_new.code):
        # Si el registro de pago no tiene la forma de pago dada de alta y no sea nuevo se le pondrá la del CFDI.
        _dbRow_registropago.formapago_id = _dbRow_cfdi.formapago_id
        _dbRow_registropago.update_record()
    else:
        # De lo contrario ya está en la variable y se guardará automaticamente.
        pass

    # Si no se encuentra el id del registro y no esta guardando.
    if (_s_action in (request.stv_fwk_permissions.btn_none.code)):

        _dbRows = dbc01(
            (_dbTable.id == _s_registropago_id)
            & (_dbTable.cfdi_id == _s_cfdi_id)
            ).select(
            _dbTable.ALL,
            orderby = _dbTable.id,
            )

        # Si solamente se encuentra un registro...
        if (len(_dbRows) == 1):
            # ...se procede a configurar los parametros para editar.
            request.args = request.args[:8] + [request.stv_fwk_permissions.btn_edit.code, _dbRows.first().id]

            # En caso de tener un solo registro de pago se procede a dejar como default el total de la factura.

        # Si no se ecuentran registros...
        elif (len(_dbRows) == 0):
            ### TODO: Ver si se queda esta lógica, porque si no hay registro de pago se crea en el nivel ingresos_plus.
            # ...se procede a configurar parámetros para nuevo
            request.args = request.args[:8] + [request.stv_fwk_permissions.btn_new.code]
        # Si se ecuentran más de un registro...
        else:
            # ticket P01-429 Debido a los cambios de arquitectura hechos se permite más de un registro de pago en las facturas de ingreso.

            # ... TODO hay un error, sin embargo se procede a editar el último registro.
            # request.args = request.args[:8] + [request.stv_fwk_permissions.btn_edit.code, str(_dbRows.last().id)]
            pass

    elif (_s_action in (request.stv_fwk_permissions.btn_save.code)):
        if request.vars.empresa_plaza_sucursal_cajachica_id != _dbRow_cfdi.empresa_plaza_sucursal_cajachica_id:
            _dbRow_cfdi.empresa_plaza_sucursal_cajachica_id = request.vars.empresa_plaza_sucursal_cajachica_id
            _dbRow_cfdi.update_record()

            dbc01.commit()
        else:
            # Mantener mismo row.
            pass
    else:
        pass

    _dbTable.bancocontpaqi_id_cliente.requires = IS_IN_DB(
        dbc01(
            dbc01.tempresa_bancoscontpaqi.empresa_id == _s_empresa_id
            ),
        'tempresa_bancoscontpaqi.id',
        dbc01.tempresa_bancoscontpaqi._format
        )

    if (_dbRow_cfdi.empresa_plaza_sucursal_cajachica_id):
        _dbRow_cajachica = dbc01.tempresa_plaza_sucursal_cajaschicas(_dbRow_cfdi.empresa_plaza_sucursal_cajachica_id)
        if (_dbRow_registropago.formapago_id in (TFORMASPAGO.CHEQUE,)):
            _dbTable.empresa_cuentabancaria_id.required = True
            _dbTable.empresa_cuentabancaria_id.notnull = True
            _dbTable.empresa_cuentabancaria_id.default = _dbRow_cajachica.empresa_cuentabancaria_id_cheques
            _dbTable.bancocontpaqi_id_cliente.required = True
            _dbTable.bancocontpaqi_id_cliente.notnull = True
            _dbTable.cheque.required = True
            _dbTable.cheque.requires = IS_NOT_EMPTY()
            _dbTable.cuenta_cliente.required = True
            _dbTable.cuenta_cliente.requires = IS_NOT_EMPTY()
            _dbTable.fechadeposito.required = True
            _dbTable.fechadeposito.notnull = True
            _dbTable.fechadeposito.default = _dbRow_cfdi.dia
            _dbTable.monto.required = True
            _dbTable.monto.notnull = True
        elif (_dbRow_registropago.formapago_id in (TFORMASPAGO.EFECTIVO,)):
            _dbTable.monto.required = True
            _dbTable.monto.notnull = True
            _dbTable.monto.comment = "En caso de haber más de 1 pago en esta factura especificar el monto de la primer forma de pago."

        elif (_dbRow_registropago.formapago_id in (TFORMASPAGO.TDD, TFORMASPAGO.TDC)):
            _dbTable.empresa_cuentabancaria_terminal_id.required = True
            _dbTable.empresa_cuentabancaria_terminal_id.notnull = True
            _dbTable.empresa_cuentabancaria_terminal_id.default = _dbRow_cajachica.empresa_cuentabancaria_terminal_id
            _dbTable.empresa_cuentabancaria_terminal_id.requires = IS_IN_DB(
                dbc01(
                    dbc01.tempresa_cuentabancaria_terminales.empresa_plaza_sucursal_id == _dbRow_cajachica.empresa_plaza_sucursal_id
                    ), 'tempresa_cuentabancaria_terminales.id', dbc01.tempresa_cuentabancaria_terminales._format
                )
            _dbTable.bancocontpaqi_id_cliente.required = True
            _dbTable.bancocontpaqi_id_cliente.notnull = True
            _dbTable.tarjeta.required = True
            _dbTable.tarjeta.requires = IS_NOT_EMPTY()
            _dbTable.autorizacion.required = True
            _dbTable.autorizacion.requires = IS_NOT_EMPTY()
            _dbTable.operacion.required = True
            _dbTable.operacion.requires = IS_NOT_EMPTY()
            _dbTable.fechadeposito.required = True
            _dbTable.fechadeposito.notnull = True
            _dbTable.fechadeposito.default = _dbRow_cfdi.dia
            _dbTable.monto.required = True
            _dbTable.monto.notnull = True
        elif (_dbRow_registropago.formapago_id in (TFORMASPAGO.TRANSFERENCIA,)):
            _dbTable.empresa_cuentabancaria_id.required = True
            _dbTable.empresa_cuentabancaria_id.notnull = True
            _dbTable.empresa_cuentabancaria_id.default = _dbRow_cajachica.empresa_cuentabancaria_id_transferencia
            _dbTable.bancocontpaqi_id_cliente.required = True
            _dbTable.bancocontpaqi_id_cliente.notnull = True
            _dbTable.cuenta_cliente.required = True
            _dbTable.cuenta_cliente.requires = IS_NOT_EMPTY()
            _dbTable.referencia.required = True
            _dbTable.referencia.requires = IS_NOT_EMPTY()
            _dbTable.claverastero.required = True
            _dbTable.claverastero.requires = IS_NOT_EMPTY()
            _dbTable.fechadeposito.required = True
            _dbTable.fechadeposito.notnull = True
            _dbTable.fechadeposito.default = _dbRow_cfdi.dia
            _dbTable.monto.required = True
            _dbTable.monto.notnull = True
        else:
            pass

    _dbTable.empresa_cuentabancaria_id.requires = IS_IN_DB(
        dbc01(
            (dbc01.tempresa_cuentasbancarias.empresa_id == request.args[1])
            ),
        'tempresa_cuentasbancarias.id',
        dbc01.tempresa_cuentasbancarias._format
        )

    ### TODO: Arreglar el manejo de los argumentos un nivel antes, para poder manejar la edición.
    _dbTable.cfdi_id.default = _s_cfdi_id

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tcfdis,
        xFieldLinkMaster = [_dbTable.id],
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_new,
            request.stv_fwk_permissions.btn_save,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_edit
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab + "_" + str(_s_registropago_id),
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.cfdi_id,
            _dbTable.monto,
            _dbTable.empresa_cuentabancaria_terminal_id,
            _dbTable.empresa_cuentabancaria_id,
            ],
        L_fieldsToSearch = [_dbTable.id],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        x_offsetInArgs = request.args[:6],
        Es_behavior = STV_FWK_FORM.ES_BEHAVIOR.S_PLUS_FORM
        )

    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionLabel = "Permiso",
        s_permissionIcon = "fa fa-compass",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
            ),
        L_options = []
        )


    _O_cat.addRowContentProperty(
        'formHidden', {
            'cfdi_id'              : str(_s_cfdi_id),
            'formapago_id'         : str(_dbRow_registropago.formapago_id),
            'verificacion_aceptada': str(0)
            }
        )

    _O_cat.addRowContentEvent('onValidation', _cfdi_sincontabilizar_ingresos_plus_plus_validation)

    _D_returnVars = _O_cat.process()

    _D_returnVars.n_formapago_id = _dbRow_registropago.formapago_id

    if _D_returnVars.D_tabOptions.b_accepted:
        ### TODO: ¿Preguntar a Jaime si está bien el commit después del accepted?
        # Este commit se hace para evitar que se haga rollback al registro de pago.
        dbc01.commit()

        # Regresa un script para cerrar el plus y actualizar el registro específico

        if _dbRow_cfdi.etapa in (TCFDIS.E_ETAPA.SIN_CONTABILIZAR, TCFDIS.E_ETAPA.CON_PREPOLIZA):
            _D_returnGenerarPrepoliza = STV_FWK_PREPOLIZA().generar(_s_cfdi_id)
            if _D_returnGenerarPrepoliza.L_msgError:
                for _s_error in _D_returnGenerarPrepoliza.L_msgError:
                    stv_flashError("Generación de prepoliza", _s_error)

            else:
                stv_flashSuccess("Generación de prepoliza", "Prepoliza generada correctamente")
        else:
            stv_flashError(
                "Generación de prepoliza",
                "La prepoliza no puede generarse de nuevo debido a que ésta ya tiene una generada"
                )

        _D_returnVars.cfdiForm = None
    else:
        _D_returnVars.cfdiForm = _O_cat.createFactoryForm(
            L_formFactoryFields = [
                dbc01.tcfdis.empresa_plaza_sucursal_cajachica_id
                ],
            dbRow_formFactory = Storage(
                empresa_plaza_sucursal_cajachica_id = _dbRow_cfdi.empresa_plaza_sucursal_cajachica_id
                ),
            s_factoryTableName = False,
            D_inputsHidden = {}
            )

    _D_returnVars.dbRow_cfdi = _dbRow_registropago

    return _D_returnVars


def _cfdi_sincontabilizar_ingresos_plus_plus_validation(O_form):
    """Validación para el formulario ingresos_plus_plus.
    """
    _s_empresa_id = request.args(1, None)
    _s_cfdi_registropago_id = request.args(7, None)

    _dbRowEmpresa = dbc01.tempresas(_s_empresa_id)

    _dbRowRegistroPago = dbc01.tcfdi_registropagos(_s_cfdi_registropago_id)

    if _dbRowRegistroPago.formapago_id == TFORMASPAGO.CONFUSION:

        if abs(O_form.vars.monto) > _dbRowEmpresa.maximosobrantefaltante:
            O_form.errors.monto = "El monto de diferencia o sobrante no puede ser mayor al que se parametrizó en la empresa. Favor de hablar con un supervisor."
        else:
            pass

    elif _dbRowRegistroPago.formapago_id == TFORMASPAGO.EFECTIVO:

        if int(O_form.vars.tratoefectivo) == TCFDI_REGISTROPAGOS.E_TRATOEFECTIVO.RETENIDOCAJA:
            pass
        else:
            if not O_form.vars.empresa_cuentabancaria_id:
                O_form.errors.empresa_cuentabancaria_id = "Si se seleccionó Depósito a bancos la cuenta bancaria no puede ir vacía."
            else:
                pass

            if not O_form.vars.referencia:
                O_form.errors.referencia = "Si se seleccionó Depósito a bancos la referencia no puede ir vacía."
            else:
                pass

            if not O_form.vars.fechadeposito:
                O_form.errors.fechadeposito = "Si se seleccionó Depósito a bancos la fecha del depósito no puede ir vacía."
            else:
                pass

    else:
        pass

    #     if O_form.vars.monto < 0:
    #         O_form.errors.monto = "El monto no puede ser negativo."
    #     else:
    #         pass

    return O_form



def cfdi_sincontabilizar_egresos():
    """ Relación de CFDIs sin contabilizar para una caja chica por concepto de egresos

    @descripcion Sin Contabilizar Egresos

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Caja chica y Fecha
    @keyword arg3: empresa_plaza_sucursal_cajachica_id - Año - Mes - Día.
    @keyword [arg4]: operación a realizar en el formulario.
    @keyword [arg5]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    #@permiso btn_new
    #@permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    @permiso btn_view
    #@permiso btn_invalidate
    #@permiso btn_validate
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    #@permiso btn_signature
    @permiso special_permisson_01:[fas fa-file-code] Ignorar movimiento
    """
    _dbTable = dbc01.tcfdis

    _s_empresa_id = request.args(1, None)
    _s_multi_id = request.args(3, '0-0-0-0')  # Se ponen 4 elementos separados '-' por default.
    _L_multi_id = _s_multi_id.split('-')
    if _L_multi_id[0] == 'None':
        _s_cajachica_id = None
    else:
        _s_cajachica_id = _L_multi_id[0]
    _dt_fecha = datetime.datetime(int(_L_multi_id[1]), int(_L_multi_id[2]), int(_L_multi_id[3]))

    _s_action = request.args(4, request.stv_fwk_permissions.btn_none.code)

    # Se generan las fechas en formato datetime para el rango del día.
    _dt_fechaDesde = _dt_fecha.replace(hour = 0, minute = 0, second = 0, microsecond = 0)
    _dt_fechaHasta = _dt_fechaDesde + datetime.timedelta(days = 1)

    # Se crea el Query para identificar si el CFDI está como "sin contabilizar" y filstrando SOLO por CFDI que sea de tipo ingreso.
    _qry = (
            (_dbTable.empresa_id == _s_empresa_id)
            & (dbc01.tempresas.id == _dbTable.empresa_id)
            & (_dbTable.fecha >= dbc01.tempresas.fecha_inicial)
            & (_dbTable.etapa == TCFDIS.E_ETAPA.SIN_CONTABILIZAR)
            & (_dbTable.rol == TCFDIS.E_ROL.EMISOR)
            & (_dbTable.empresa_plaza_sucursal_cajachica_id == _s_cajachica_id)
            & (_dbTable.tipocomprobante_id == TTIPOSCOMPROBANTES.EGRESO)
            & (_dbTable.fecha >= _dt_fechaDesde)
            & (_dbTable.fecha < _dt_fechaHasta)
    )

    _L_fieldsToShow = [
        _dbTable.id,
        #   _dbTable.rol,
        _dbTable.empresa_serie_id,
        _dbTable.folio,

        _dbTable.fecha,
        _dbTable.metodopago_id,
        _dbTable.formapago_id,
        _dbTable.total,
        dbc01.tcfdi_registropagos.empresa_cuentabancaria_terminal_id,
        dbc01.tcfdi_registropagos.empresa_cuentabancaria_id,
        _dbTable.receptorrfc,
        _dbTable.receptornombrerazonsocial
        ]

    _L_OrderBy = [
        _dbTable.fecha,
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_findall
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = [
            _dbTable.id,
            ],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        L_fieldnameIds = [_dbTable.id],
        x_offsetInArgs = request.args[:4]
        )

    # Se agrega el boton especial 01 que funciona para mandar el CFDI a CFDI no contabilizable.
    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionLabel = "No contabilizar CFDI",
        s_permissionIcon = "fas fa-window-close",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
            confirm = Storage(
                s_title = "Favor de confirmar operación",
                s_text = "¿Estás seguro de querer marcar este CFDI como 'No contabilizar'?",
                s_type = "warning",
                s_btnconfirm = "Confirmar",
                s_btncancel = "Cancelar",
                )
            ),
        L_options = []
        )

    _O_cat.addRowSearchResultsLeftJoin(
        dbc01.tcfdi_registropagos.on(
            (dbc01.tcfdi_registropagos.cfdi_id == _dbTable.id)
            )
        )

    _O_cat.addRowSearchResultsProperty('orderBy', _L_OrderBy)

    _O_cat.addRowSearchResults_addColumnBegin(
        es_typeColumn = STV_FWK_FORM.ES_TYPECOLUMN.S_PLUS,
        s_colname = "plus",
        D_field = Storage(
            readable = True
            ),
        m_classHtml = lambda D_column, dbRow: "stv_rowresults_plus",
        D_data = Storage(
            url = URL(
                f = 'cfdi_sincontabilizar_egresos_plus', args = request.args[:4] + [
                    'master_' + str(request.function)]
                )
            )
        )

    # Código para poder jalar la vista de CFDIS y utilizarlas en esta función.
    if _s_action in (request.stv_fwk_permissions.btn_view.code,):
        from gluon.storage import List

        _s_cfdi_id = str(request.args(5, 0))  # Se guarda el argumento que se quiere utilizar dentro de una variable.
        request.args = List()
        request.args.append(_s_action)  # Se mete _s_action dentro de la lista con la función append.
        request.args.append(_s_cfdi_id)

        _D_returnVars = cfdisemitidos()
        # Usar el os.path.join para evitar conflicto por la diagonal (/) en sistemas operativos.
        response.view = os.path.join('250cfdi', 'cfdisemitidos.html')

    elif _s_action in (request.stv_fwk_permissions.special_permisson_01.code,):

        _s_cfdi_id = str(request.args(5, 0))  # Se guarda el argumento que se quiere utilizar dentro de una variable

        _dbRowCfdi = dbc01.tcfdis(_s_cfdi_id)

        if _dbRowCfdi.etapa != TCFDIS.E_ETAPA.SIN_CONTABILIZAR:
            stv_flashError("Cambio de estatus del CFDI",
                "El CFDI no se puede cambiar de estado ya que se encuentra con un estatus diferente a 'Sin contabilizar'.")
        else:
            _dbRowCfdi.etapa = TCFDIS.E_ETAPA.NO_CONTABILIZAR
            _dbRowCfdi.update_record()
            stv_flashSuccess("Cambio de estatus del CFDI", "Cambio hecho correctamente.")

            _D_overrideView = request.stv_fwk_permissions.view_refresh

            _D_returnVars = _O_cat.process(D_overrideView = _D_overrideView)

    else:
        _D_returnVars = _O_cat.process()

    return (_D_returnVars)



def cfdi_sincontabilizar_egresos_plus():
    """ Especifica la información necesaria para contabilizar el egreso

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: id compuesto por 4 partes
            id de la caja chica
            año
            mes
            día
        arg4: master_[nombre función maestro]
        arg5: cfdi_id
        [arg6]: operación a realizar en el formulario
        [arg7]: id en caso de existir relación a un registro en la operación
    """

    _dbTable = dbc01.tcfdi_registropagos
    _s_empresa_id = request.args(1, None)
    _s_multi_id = request.args(3, '0-0-0-0')  # Se ponen 4 elementos separados '-' por default.
    _L_multi_id = _s_multi_id.split('-')
    if _L_multi_id[0] == 'None':
        _s_cajachica_id = None
    else:
        _s_cajachica_id = _L_multi_id[0]

    _s_cfdi_id = request.args(5, None)
    _s_action = request.args(6, request.stv_fwk_permissions.btn_none.code)

    _dbRow_cfdi = dbc01.tcfdis(_s_cfdi_id)

    _dbTable.formapago_id.default = _dbRow_cfdi.formapago_id

    # Si no se encuentra el id del registro y no esta guardando.
    if (_s_action in (request.stv_fwk_permissions.btn_none.code,)):

        _dbRows = dbc01(
            (_dbTable.cfdi_id == _s_cfdi_id)
            ).select(
            _dbTable.ALL,
            orderby = _dbTable.id,
            )

        # Si solamente se encuentra un registro...
        if (len(_dbRows) == 1):
            # ...se procede a configurar los parametros para editar.
            request.args = request.args[:6] + [request.stv_fwk_permissions.btn_edit.code, _dbRows.first().id]
        # Si no se ecuentran registros...
        elif (len(_dbRows) == 0):
            # ...se procede a configurar parámetros para nuevo.
            request.args = request.args[:6] + [request.stv_fwk_permissions.btn_new.code]
        # Si se ecuentran más de un registro...
        else:
            # ... TODO hay un error, sin embargo se procede a editar el último registro.
            request.args = request.args[:6] + [request.stv_fwk_permissions.btn_edit.code, _dbRows.last().id]
    elif (_s_action in (request.stv_fwk_permissions.btn_save.code)):
        _dbTable.cfdi_id.default = _s_cfdi_id
        if request.vars.empresa_plaza_sucursal_cajachica_id != _dbRow_cfdi.empresa_plaza_sucursal_cajachica_id:
            _dbRow_cfdi.empresa_plaza_sucursal_cajachica_id = request.vars.empresa_plaza_sucursal_cajachica_id
            _dbRow_cfdi.update_record()

            # Se agrega commit para siempre hacer update a la caja chica sin importar que se genere o no la prepoliza.
            dbc01.commit()
        else:
            # Mantener mismo row.
            pass
    else:
        pass

    _dbTable.bancocontpaqi_id_cliente.requires = IS_IN_DB(
        dbc01(
            dbc01.tempresa_bancoscontpaqi.empresa_id == _s_empresa_id
            ),
        'tempresa_bancoscontpaqi.id',
        dbc01.tempresa_bancoscontpaqi._format
        )

    if (_dbRow_cfdi.empresa_plaza_sucursal_cajachica_id):
        _dbRow_cajachica = dbc01.tempresa_plaza_sucursal_cajaschicas(_dbRow_cfdi.empresa_plaza_sucursal_cajachica_id)
        if (_dbRow_cfdi.formapago_id in (TFORMASPAGO.CHEQUE,)):
            _dbTable.empresa_cuentabancaria_id.required = True
            _dbTable.empresa_cuentabancaria_id.notnull = True
            _dbTable.empresa_cuentabancaria_id.default = _dbRow_cajachica.empresa_cuentabancaria_id_cheques
            _dbTable.bancocontpaqi_id_cliente.required = True
            _dbTable.bancocontpaqi_id_cliente.notnull = True
            _dbTable.cheque.required = True
            _dbTable.cheque.requires = IS_NOT_EMPTY()
            _dbTable.cuenta_cliente.required = True
            _dbTable.cuenta_cliente.requires = IS_NOT_EMPTY()
            _dbTable.fechadeposito.required = True
            _dbTable.fechadeposito.notnull = True
            _dbTable.fechadeposito.default = _dbRow_cfdi.dia

        elif (_dbRow_cfdi.formapago_id in (TFORMASPAGO.TDD, TFORMASPAGO.TDC)):
            _dbTable.empresa_cuentabancaria_terminal_id.required = True
            _dbTable.empresa_cuentabancaria_terminal_id.notnull = True
            _dbTable.empresa_cuentabancaria_terminal_id.default = _dbRow_cajachica.empresa_cuentabancaria_terminal_id
            _dbTable.empresa_cuentabancaria_terminal_id.requires = IS_IN_DB(
                dbc01(
                    dbc01.tempresa_cuentabancaria_terminales.empresa_plaza_sucursal_id == _dbRow_cajachica.empresa_plaza_sucursal_id
                    ), 'tempresa_cuentabancaria_terminales.id', dbc01.tempresa_cuentabancaria_terminales._format
                )
            _dbTable.bancocontpaqi_id_cliente.required = True
            _dbTable.bancocontpaqi_id_cliente.notnull = True
            _dbTable.tarjeta.required = True
            _dbTable.tarjeta.requires = IS_NOT_EMPTY()
            _dbTable.autorizacion.required = True
            _dbTable.autorizacion.requires = IS_NOT_EMPTY()
            _dbTable.operacion.required = True
            _dbTable.operacion.requires = IS_NOT_EMPTY()
            _dbTable.fechadeposito.required = True
            _dbTable.fechadeposito.notnull = True
            _dbTable.fechadeposito.default = _dbRow_cfdi.dia

        elif (_dbRow_cfdi.formapago_id in (TFORMASPAGO.TRANSFERENCIA,)):
            _dbTable.empresa_cuentabancaria_id.required = True
            _dbTable.empresa_cuentabancaria_id.notnull = True
            _dbTable.empresa_cuentabancaria_id.default = _dbRow_cajachica.empresa_cuentabancaria_id_transferencia
            _dbTable.bancocontpaqi_id_cliente.required = True
            _dbTable.bancocontpaqi_id_cliente.notnull = True
            _dbTable.cuenta_cliente.required = True
            _dbTable.cuenta_cliente.requires = IS_NOT_EMPTY()
            _dbTable.referencia.required = True
            _dbTable.referencia.requires = IS_NOT_EMPTY()
            _dbTable.claverastero.required = True
            _dbTable.claverastero.requires = IS_NOT_EMPTY()
            _dbTable.fechadeposito.required = True
            _dbTable.fechadeposito.notnull = True
            _dbTable.fechadeposito.default = _dbRow_cfdi.dia
        else:
            pass

    _dbTable.empresa_cuentabancaria_id.requires = IS_IN_DB(
        dbc01(
            (dbc01.tempresa_cuentasbancarias.empresa_id == request.args[1])
            ),
        'tempresa_cuentasbancarias.id',
        dbc01.tempresa_cuentasbancarias._format
        )

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tcfdis,
        xFieldLinkMaster = [_dbTable.cfdi_id],
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_new,
            request.stv_fwk_permissions.btn_save,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_edit
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.cfdi_id,
            _dbTable.empresa_cuentabancaria_terminal_id,
            _dbTable.empresa_cuentabancaria_id,
            ],
        L_fieldsToSearch = [_dbTable.id],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        x_offsetInArgs = request.args[:4],
        Es_behavior = STV_FWK_FORM.ES_BEHAVIOR.S_PLUS_FORM
        )

    _O_cat.addRowContentProperty('formHidden', {'cfdi_id': str(_s_cfdi_id)})

    _D_returnVars = _O_cat.process()

    if _D_returnVars.D_tabOptions.b_accepted:
        # Este commit se hace para evitar que se haga rollback al registro de pago.
        dbc01.commit()

        if _dbRow_cfdi.etapa in (TCFDIS.E_ETAPA.SIN_CONTABILIZAR, TCFDIS.E_ETAPA.CON_PREPOLIZA):
            _D_returnGenerarPrepoliza = STV_FWK_PREPOLIZA().generar(_s_cfdi_id)
            if _D_returnGenerarPrepoliza.L_msgError:
                for _s_error in _D_returnGenerarPrepoliza.L_msgError:
                    stv_flashError("Generación de prepoliza", _s_error)

            else:
                stv_flashSuccess("Generación de prepoliza", "Prepoliza generada correctamente")
        else:
            stv_flashError(
                "Generación de prepoliza",
                "La prepoliza no puede generarse de nuevo debido a que ésta ya tiene una generada"
                )

    _D_returnVars.dbRow_cfdi = _dbRow_cfdi
    _D_returnVars.cfdiForm = _O_cat.createFactoryForm(
        L_formFactoryFields = [
            dbc01.tcfdis.empresa_plaza_sucursal_cajachica_id
            ],
        dbRow_formFactory = Storage(
            empresa_plaza_sucursal_cajachica_id = _dbRow_cfdi.empresa_plaza_sucursal_cajachica_id
            ),
        s_factoryTableName = False,
        D_inputsHidden = {}
        )

    return (_D_returnVars)


def cfdi_sincontabilizar_pagos():
    """ Relación de CFDIs sin contabilizar para una caja chica por concepto de pagos.

    @descripcion Sin Contabilizar Pagos

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Caja chica y Fecha
    @keyword arg3: empresa_plaza_sucursal_cajachica_id - Año - Mes - Día.
    @keyword [arg4]: operación a realizar en el formulario.
    @keyword [arg5]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    #@permiso btn_new
    #@permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    @permiso btn_view
    #@permiso btn_invalidate
    #@permiso btn_validate
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    #@permiso btn_signature
    @permiso special_permisson_01:[fas fa-file-code] Ignorar movimiento
    """
    _dbTable = dbc01.tcfdis

    _s_empresa_id = request.args(1, None)
    _s_multi_id = request.args(3, '0-0-0-0')  # Se ponen 4 elementos separados '-' por default
    _L_multi_id = _s_multi_id.split('-')
    if _L_multi_id[0] == 'None':
        _s_cajachica_id = None
    else:
        _s_cajachica_id = _L_multi_id[0]

    _s_action = request.args(4, request.stv_fwk_permissions.btn_none.code)

    _dt_fecha = datetime.datetime(int(_L_multi_id[1]), int(_L_multi_id[2]), int(_L_multi_id[3]))

    # Se generan las fechas en formato datetime para el rango del día
    _dt_fechaDesde = _dt_fecha.replace(hour = 0, minute = 0, second = 0, microsecond = 0)
    _dt_fechaHasta = _dt_fechaDesde + datetime.timedelta(days = 1)

    # Se crea el Query para identificar si el CFDI está como "sin contabilizar" y filtrando SOLO por CFDI que sea de tipo pagos.
    _qry = (
            (_dbTable.empresa_id == _s_empresa_id)
            & (dbc01.tempresas.id == _dbTable.empresa_id)
            & (_dbTable.fecha >= dbc01.tempresas.fecha_inicial)
            & (_dbTable.id == dbc01.tcfdi_complementopagos.cfdi_id)
            & (_dbTable.etapa == TCFDIS.E_ETAPA.SIN_CONTABILIZAR)
            & (_dbTable.rol == TCFDIS.E_ROL.EMISOR)
            & (_dbTable.empresa_plaza_sucursal_cajachica_id == _s_cajachica_id)
            & (_dbTable.tipocomprobante_id == TTIPOSCOMPROBANTES.PAGO)
            # Debido a que la fecha que se usa como parametro corresponde a la fecha del cfdi involucrado, aunque la fecha de pago sea diferente, se usa para el filtro la fecha del cfdi
            # & (_dbTable.fecha >= _dt_fechaDesde)
            # & (_dbTable.fecha < _dt_fechaHasta)
            & (dbc01.tcfdi_complementopagos.fechapago >= _dt_fechaDesde)
            & (dbc01.tcfdi_complementopagos.fechapago < _dt_fechaHasta)
    )

    dbc01.tcfdi_complementopagos.fechapago.readable = True
    _dbTable.fecha.readable = True

    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.empresa_serie_id,
        _dbTable.folio,
        _dbTable.fecha,
        dbc01.tcfdi_complementopagos.fechapago,
        _dbTable.receptorrfc,
        _dbTable.receptornombrerazonsocial
        ]

    _L_OrderBy = [
        dbc01.tcfdis.fecha,
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_findall
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = [
            _dbTable.id,
            ],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        L_fieldnameIds = [_dbTable.id],
        x_offsetInArgs = request.args[:4]
        )

    _O_cat.addRowSearchResultsProperty('orderBy', _L_OrderBy)

    # Se crea una nueva columna en el grid, se agrega un "+" y se especifica la función entorno a donde funcionará el botón "+".
    _O_cat.addRowSearchResults_addColumnBegin(
        es_typeColumn = STV_FWK_FORM.ES_TYPECOLUMN.S_PLUS,
        s_colname = "plus",
        D_field = Storage(
            readable = True
            ),
        m_classHtml = lambda D_column, dbRow: "stv_rowresults_plus",
        D_data = Storage(
            url = URL(
                f = 'cfdi_sincontabilizar_pagos_plus', args = request.args[:4] + [
                    'master_' + str(request.function)]
                )
            )
        )

    # Código para poder jalar la vista de CFDIS y utilizarlas en esta función.
    if _s_action in (request.stv_fwk_permissions.btn_view.code,):
        from gluon.storage import List

        _s_cfdi_id = str(request.args(5, 0))  # Se guarda el argumento que se quiere utilizar dentro de una variable.
        request.args = List()
        request.args.append(_s_action)  # Se mete _s_action dentro de la lista con la función append.
        request.args.append(_s_cfdi_id)

        _D_returnVars = cfdisemitidos()
        # Usar el os.path.join para evitar conflicto por la diagonal (/) en sistemas operativos.
        response.view = os.path.join('250cfdi', 'cfdisemitidos.html')

    else:
        _D_returnVars = _O_cat.process()

    return _D_returnVars


def cfdi_sincontabilizar_ingresoscancelados():
    """ Relación de CFDIs sin contabilizar cancelados para una caja chica por concepto de ingresos

    @descripcion Sin Contabilizar Ingresos Cancelados

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Caja chica y Fecha
    @keyword arg3: empresa_plaza_sucursal_cajachica_id - Año - Mes - Día.
    @keyword [arg4]: operación a realizar en el formulario.
    @keyword [arg5]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    #@permiso btn_new
    #@permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    @permiso btn_view
    #@permiso btn_invalidate
    #@permiso btn_validate
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    #@permiso btn_signature
    @permiso special_permisson_01:[fas fa-file-code] Ignorar movimiento
    """
    _dbTable = dbc01.tcfdis

    _s_empresa_id = request.args(1, None)
    _s_multi_id = request.args(3, '0-0-0-0')  # Se ponen 4 elementos separados '-' por default.
    _L_multi_id = _s_multi_id.split('-')
    if _L_multi_id[0] == 'None':
        _s_cajachica_id = None
    else:
        _s_cajachica_id = _L_multi_id[0]

    _s_action = request.args(4, request.stv_fwk_permissions.btn_none.code)

    _dt_fecha = datetime.datetime(int(_L_multi_id[1]), int(_L_multi_id[2]), int(_L_multi_id[3]))
    # Se generan las fechas en formato datetime para todo el rango del día
    _dt_fechaDesde = _dt_fecha.replace(hour = 0, minute = 0, second = 0, microsecond = 0)
    _dt_fechaHasta = _dt_fechaDesde + datetime.timedelta(days = 1)

    # Se crea el Query para identificar si el CFDI está como "sin contabilizar" y filstrando SOLO por CFDI que sea de
    # tipo ingreso.
    _qry = (
            (_dbTable.empresa_id == _s_empresa_id)
            & (dbc01.tempresas.id == _dbTable.empresa_id)
            & (_dbTable.fecha >= dbc01.tempresas.fecha_inicial)
            & (_dbTable.etapa_cancelado == TCFDIS.E_ETAPA.SIN_CONTABILIZAR)
            & (_dbTable.sat_status == TCFDIS.SAT_STATUS.CANCELADO)
            & (_dbTable.rol == TCFDIS.E_ROL.EMISOR)
            & (_dbTable.empresa_plaza_sucursal_cajachica_id == _s_cajachica_id)
            & (_dbTable.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO)
            & (_dbTable.sat_fecha_cancelacion >= _dt_fechaDesde)
            & (_dbTable.sat_fecha_cancelacion < _dt_fechaHasta)
    )

    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.empresa_serie_id,
        _dbTable.folio,
        _dbTable.fecha,
        _dbTable.metodopago_id,
        _dbTable.formapago_id,
        _dbTable.total,
        _dbTable.receptorrfc,
        _dbTable.receptornombrerazonsocial,
        _dbTable.sat_status
        ]

    _L_OrderBy = [
        _dbTable.fecha,
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_findall
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = [
            _dbTable.id,
            ],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        L_fieldnameIds = [_dbTable.id],
        x_offsetInArgs = request.args[:4]
        )

    _O_cat.addRowSearchResultsProperty('orderBy', _L_OrderBy)

    # Se agrega el boton especial 01 que funciona para mandar el CFDI a CFDI no contabilizable.
    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionLabel = "No contabilizar CFDI",
        s_permissionIcon = "fas fa-window-close",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
            confirm = Storage(
                s_title = "Favor de confirmar operación",
                s_text = "¿Estás seguro de querer marcar este CFDI como 'No contabilizar'?",
                s_type = "warning",
                s_btnconfirm = "Confirmar",
                s_btncancel = "Cancelar",
                )
            ),
        L_options = []
        )

    # Se crea una nueva columna en el grid, se agrega un "+" y se especifica la función entorno a donde funcionará el
    # botón "+".
    _O_cat.addRowSearchResults_addColumnBegin(
        es_typeColumn = STV_FWK_FORM.ES_TYPECOLUMN.S_PLUS,
        s_colname = "plus",
        D_field = Storage(
            readable = True
            ),
        m_classHtml = lambda D_column, dbRow: "stv_rowresults_plus",
        D_data = Storage(
            url = URL(f = 'cfdi_sincontabilizar_ingresoscancelados_plus', args = request.args[:4] + [
                'master_' + str(request.function)])
            )
        )

    # Código para poder jalar la vista de CFDIS y utilizarlas en esta función.
    if _s_action in (request.stv_fwk_permissions.btn_view.code,):
        from gluon.storage import List

        _s_cfdi_id = str(request.args(5, 0))  # Se guarda el argumento que se quiere utilizar dentro de una variable.
        request.args = List()
        request.args.append(_s_action)  # Se mete _s_action dentro de la lista con la función append.
        request.args.append(_s_cfdi_id)

        _D_returnVars = cfdisemitidos()
        # Usar el os.path.join para evitar conflicto por la diagonal (/) en sistemas operativos.
        response.view = os.path.join('250cfdi', 'cfdisemitidos.html')

    elif _s_action in (request.stv_fwk_permissions.special_permisson_01.code,):

        _s_cfdi_id = str(request.args(5, 0))  # Se guarda el argumento que se quiere utilizar dentro de una variable

        _dbRowCfdi = dbc01.tcfdis(_s_cfdi_id)

        if _dbRowCfdi.etapa != TCFDIS.E_ETAPA.SIN_CONTABILIZAR:
            stv_flashError("Cambio de estatus del CFDI",
                "El CFDI no se puede cambiar de estado ya que se encuentra con un estatus diferente a 'Sin contabilizar'.")
        else:
            _dbRowCfdi.etapa = TCFDIS.E_ETAPA.NO_CONTABILIZAR
            _dbRowCfdi.update_record()
            stv_flashSuccess("Cambio de estatus del CFDI", "Cambio hecho correctamente.")

            _D_overrideView = request.stv_fwk_permissions.view_refresh

            _D_returnVars = _O_cat.process(D_overrideView = _D_overrideView)
    else:
        _D_returnVars = _O_cat.process()

    return (_D_returnVars)


def cfdi_sincontabilizar_ingresoscancelados_plus():
    """ Formulario para mostrar los distintios registros de pago por CFDI cancelados de tipo ingreso.

    @descripcion  CFDIs sin contabilizar ingresos cancelados captura

    @keyword arg0: Empresa
    @keyword arg1: empresa_id
    @keyword arg2: Caja chica y fecha
    @keyword arg3: id compuesto por 4 partes
            id de la caja chica
            año
            mes
            día
    @keyword arg5: CFDI
    @keyword arg6: empresa_id
    @keyword [arg7]: codigo de funcionalidad
    @keyword [arg8]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature

    """

    _dbTable = dbc01.tcfdi_registropagos

    _s_empresa_id = request.args(1, None)

    _s_multi_id = request.args(3, '0-0-0-0')  # Se ponen 4 elementos separados '-' por default.

    _L_multi_id = _s_multi_id.split('-')

    if _L_multi_id[0] == 'None':
        _s_cajachica_id = None
    else:
        _s_cajachica_id = _L_multi_id[0]

    _s_cfdi_id = request.args(5, None)

    _s_action = request.args(6, request.stv_fwk_permissions.btn_none.code)

    _dbRow_cfdi = dbc01.tcfdis(_s_cfdi_id)

    _dbTable.cfdi_id.default = _s_cfdi_id

    _dbTable.formapago_id.notnull = True
    _dbTable.formapago_id.required = True

    _dbTable.tipo_cfdi.default = TCFDI_REGISTROPAGOS.TIPO_CFDI.CANCELADO

    _dbTable.tratoefectivo.default = TCFDI_REGISTROPAGOS.E_TRATOEFECTIVO.RETENIDOCAJA

    _dbRowsRegistrospago = dbc01(
        (_dbTable.cfdi_id == _s_cfdi_id)
        & (_dbTable.tipo_cfdi == TCFDI_REGISTROPAGOS.TIPO_CFDI.CANCELADO)
        ).select(
        _dbTable.ALL
        )

    _dbRowsRegistrospago = dbc01(
        (_dbTable.cfdi_id == _s_cfdi_id)
        & (_dbTable.tipo_cfdi == TCFDI_REGISTROPAGOS.TIPO_CFDI.CANCELADO)
        ).select(
        _dbTable.ALL
        )

    _dbRow_cfdi = dbc01.tcfdis(_s_cfdi_id)

    if not _dbRowsRegistrospago and (_dbRow_cfdi.formapago_id == TFORMASPAGO.POR_DEFINIR):

        _D_registropago = Storage(
            cfdi_id = _s_cfdi_id,
            formapago_id = _dbRow_cfdi.formapago_id,
            monto = _dbRow_cfdi.total,
            tipo_cfdi = TCFDI_REGISTROPAGOS.TIPO_CFDI.CANCELADO
            )

        _dbTable.insert(**_D_registropago)

    else:
        pass

    if _s_action == request.stv_fwk_permissions.btn_new.code:
        # Si el _s_action es btn_nuevo se recorrerán los registros de pago para calcular su total de montos.

        _n_registrospago_totalmontos = 0
        for _dbRowRegistropago in _dbRowsRegistrospago:
            _n_registrospago_totalmontos += _dbRowRegistropago.monto

        _n_resultadomonto = _dbRow_cfdi.total - _n_registrospago_totalmontos

        if _n_resultadomonto > 0:
            _dbTable.monto.default = _n_resultadomonto
        else:
            pass

    else:
        pass

    _qry = (
            (_dbTable.cfdi_id == _s_cfdi_id)
            & (_dbTable.tipo_cfdi == TCFDI_REGISTROPAGOS.TIPO_CFDI.CANCELADO)
    )

    _dbTable.cfdi_id.readable = True

    _dbTable.formapago_id.requires = IS_IN_DB(
        db01(
            db01.tformaspago.id.belongs(
                TFORMASPAGO.EFECTIVO,
                TFORMASPAGO.CHEQUE,
                TFORMASPAGO.TRANSFERENCIA,
                )
            ),
        'tformaspago.id',
        db01.tformaspago._format
        )

    _dbRow_cajachica = dbc01.tempresa_plaza_sucursal_cajaschicas(_dbRow_cfdi.empresa_plaza_sucursal_cajachica_id)

    if _dbRow_cfdi.formapago_id == TFORMASPAGO.POR_DEFINIR:
        _L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_findall
            ]
    else:
        _L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_new,
            request.stv_fwk_permissions.btn_save,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_remove,
            request.stv_fwk_permissions.btn_edit,
            request.stv_fwk_permissions.btn_findall
            ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        dbTableMaster = dbc01.tcfdis,
        xFieldLinkMaster = [_dbTable.cfdi_id],
        L_visibleButtons = _L_visibleButtons,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab + "_" + str(_s_cfdi_id),
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.cfdi_id,
            _dbTable.formapago_id,
            _dbTable.monto,
            _dbTable.empresa_cuentabancaria_terminal_id,
            _dbTable.empresa_cuentabancaria_id,
            ],
        L_fieldsToSearch = [_dbTable.id],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        x_offsetInArgs = request.args[:4],
        )

    # Se agrega un "+" y se especifica la función entorno a donde funcionará el botón "+".
    _O_cat.addRowSearchResults_addColumnBegin(
        es_typeColumn = STV_FWK_FORM.ES_TYPECOLUMN.S_PLUS,
        s_colname = "plus",
        D_field = Storage(
            readable = True
            ),
        m_classHtml = lambda D_column, dbRow: "stv_rowresults_plus",
        D_data = Storage(
            url = URL(f = 'cfdi_sincontabilizar_ingresoscancelados_plus_plus',
                args = request.args[:6] + ['master_' + str(request.function)])
            )
        )

    _D_returnVars = _O_cat.process()

    if (_dbRow_cfdi.etapa in (TCFDIS.E_ETAPA.CON_PREPOLIZA, TCFDIS.E_ETAPA.CON_POLIZA)) and (
            _s_action == request.stv_fwk_permissions.btn_findall.code):
        _D_returnVars.refrescarmaestro = True
    else:
        _D_returnVars.refrescarmaestro = False

    if (_s_action == request.stv_fwk_permissions.btn_save.code) and _D_returnVars.rowContent_form.accepted:
        # Este commit se hace para evitar que se haga rollback al registro de pago.
        dbc01.commit()
        if _dbRow_cfdi.etapa in (TCFDIS.E_ETAPA.CON_POLIZA,):

            _D_returnGenerarPrepoliza = STV_FWK_PREPOLIZA().generar(_s_cfdi_id, True, True, True)

            if _D_returnGenerarPrepoliza.L_msgError:
                for _s_error in _D_returnGenerarPrepoliza.L_msgError:
                    stv_flashError("Contabilización cancelado", _s_error)
            else:
                stv_flashSuccess("Contabilización cancelado", "Movimiento cancelado generado exitosamente.")

        else:
            if _dbRow_cfdi.dia == _dbRow_cfdi.sat_fecha_cancelacion.date():
                # Se cambia el estado al CFDI de ingreso por NO_CONTABILIZAR_CANCELADO
                dbc01(dbc01.tcfdis.id == _dbRow_cfdi.id).update(etapa = TCFDIS.E_ETAPA.NO_CONTABILIZAR_CANCELADO)
                dbc01(dbc01.tcfdis.id == _dbRow_cfdi.id).update(etapa_cancelado = TCFDIS.E_ETAPA.NO_CONTABILIZAR_CANCELADO)

                # Se borrar su prepoliza.
                _dbRowCfidPrepoliza = dbc01(
                    (dbc01.tcfdi_prepolizas.cfdi_id == _dbRow_cfdi.id)
                    ).delete()

                stv_flashError(
                    "Contabilización cancelado",
                    "La factura no puede contabilizarse debido a que la cancelación de este fue el mismo día que su "
                    "emisión. Se cambia el estatus a 'No contabilizable por cancelación'."
                    )
            else:
                stv_flashError(
                    "Contabilización cancelado",
                    "La factura no puede contabilizarse debido a que el ingreso aún no ha sido contabilizado."
                    )

    else:
        pass

    _D_returnVars.D_useView.b_serverCreatesTab = True

    _D_returnVars.D_useView.b_cleanGrid = True

    return _D_returnVars


def _cfdi_sincontabilizar_ingresoscancelados_plus_validation(O_form):
    """ Validación para el formulario de ingresoscancelados_plus.
    """

    _s_id = O_form.record_id

    _s_empresa_id = request.args(1, None)

    _s_diferencia = O_form.vars.diferencia

    if O_form.vars.formapago_id:

        if int(O_form.vars.formapago_id) == TFORMASPAGO.EFECTIVO:

            if not O_form.vars.monto:
                O_form.errors.monto = "El monto tiene que ser capturado."
            else:
                pass

        elif int(O_form.vars.formapago_id) == TFORMASPAGO.CHEQUE:

            if not O_form.vars.empresa_cuentabancaria_id:
                O_form.errors.empresa_cuentabancaria_id = "La cuenta bancaria tiene que ser capturada."
            else:
                pass

            if not O_form.vars.cheque:
                O_form.errors.cheque = "El cheque tiene que ser capturado."
            else:
                pass

            if not O_form.vars.cuenta_cliente:
                O_form.errors.cuenta_cliente = "La cuenta del cliente tiene que ser capturada."
            else:
                pass

            if not O_form.vars.fechadeposito:
                O_form.errors.fechadeposito = "La fecha del deposito tiene que ser capturada."
            else:
                pass

            if not O_form.vars.monto:
                O_form.errors.monto = "El monto tiene que ser capturado."
            else:
                pass
        else:
            # Si no es ninguna de las anteriores es Transferencia.

            if not O_form.vars.empresa_cuentabancaria_id:
                O_form.errors.empresa_cuentabancaria_id = "La cuenta bancaria tiene que ser capturada."
            else:
                pass

            if not O_form.vars.bancocontpaqi_id_cliente:
                O_form.errors.bancocontpaqi_id_cliente = "La banco del cliente tiene que ser capturado."
            else:
                pass

            if not O_form.vars.cuenta_cliente:
                O_form.errors.cuenta_cliente = "La cuenta del cliente tiene que ser capturada."
            else:
                pass

            if not O_form.vars.referencia:
                O_form.errors.referencia = "La referencia tiene que ser capturada."
            else:
                pass

            if not O_form.vars.claverastero:
                O_form.errors.claverastero = "La clave de rastreo tiene que ser capturada."
            else:
                pass

            if not O_form.vars.fechadeposito:
                O_form.errors.fechadeposito = "La fecha del deposito tiene que ser capturada."
            else:
                pass

            if not O_form.vars.monto:
                O_form.errors.monto = "El monto tiene que ser capturado."
            else:
                pass
    else:
        O_form.errors.formapago_id = "Se tiene que capturar una forma de pago para poder generar un registro de pago."

    if O_form.vars.monto < 0:
        O_form.errors.monto = "El monto no puede ser negativo."
    else:
        pass

    return O_form


def cfdi_sincontabilizar_ingresoscancelados_plus_plus():
    """ Formulario para capturar el registro de pago por CFDI cancelados de tipo ingreso.

    @descripcion  CFDIs sin contabilizar ingresos cancelados captura

    @keyword arg0: Empresa
    @keyword arg1: empresa_id
    @keyword arg2: Caja chica y fecha
    @keyword arg3: id compuesto por 4 partes
            id de la caja chica
            año
            mes
            día
    @keyword arg5: CFDI
    @keyword arg6: empresa_id
    @keyword [arg7]: codigo de funcionalidad
    @keyword [arg8]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature

    """

    _dbTable = dbc01.tcfdi_registropagos
    _s_empresa_id = request.args(1, None)
    _s_multi_id = request.args(3, '0-0-0-0')  # Se ponen 4 elementos separados '-' por default.
    _L_multi_id = _s_multi_id.split('-')
    if _L_multi_id[0] == 'None':
        _s_cajachica_id = None
    else:
        _s_cajachica_id = _L_multi_id[0]
    _s_cfdi_id = request.args(5, None)
    _s_registropago_id = request.args(7, None)
    _s_action = request.args(8, request.stv_fwk_permissions.btn_none.code)

    _dbRow_cfdi = dbc01.tcfdis(_s_cfdi_id)

    _dbRow_registropago = dbc01.tcfdi_registropagos(_s_registropago_id)

    if not (_dbRow_registropago.formapago_id or 0) and (_s_action != request.stv_fwk_permissions.btn_new.code):
        # Si el registro de pago no tiene la forma de pago dada de alta y no sea nuevo se le pondrá la del CFDI.
        _dbRow_registropago.formapago_id = _dbRow_cfdi.formapago_id
        _dbRow_registropago.update_record()
    else:
        # De lo contrario ya está en la variable y se guardará automaticamente.
        pass

    # Si no se encuentra el id del registro y no esta guardando.
    if _s_action in (request.stv_fwk_permissions.btn_none.code,):

        _dbRows = dbc01(
            (_dbTable.id == _s_registropago_id)
            & (_dbTable.cfdi_id == _s_cfdi_id)
            ).select(
            _dbTable.ALL,
            orderby = _dbTable.id,
            )

        # Si solamente se encuentra un registro...
        if len(_dbRows) == 1:
            # ...se procede a configurar los parametros para editar.
            request.args = request.args[:8] + [request.stv_fwk_permissions.btn_edit.code, _dbRows.first().id]

            # En caso de tener un solo registro de pago se procede a dejar como default el total de la factura.

        # Si no se ecuentran registros...
        elif len(_dbRows) == 0:
            ### TODO: Ver si se queda esta lógica, porque si no hay registro de pago se crea en el nivel ingresos_plus.
            # ...se procede a configurar parámetros para nuevo
            request.args = request.args[:8] + [request.stv_fwk_permissions.btn_new.code]
        # Si se ecuentran más de un registro...
        else:
            # ticket P01-429 Debido a los cambios de arquitectura hechos se permite más de un registro de pago en las
            # facturas de ingreso.

            # request.args = request.args[:8] + [request.stv_fwk_permissions.btn_edit.code, str(_dbRows.last().id)]
            pass

    elif _s_action in (request.stv_fwk_permissions.btn_save.code,):
        if request.vars.empresa_plaza_sucursal_cajachica_id != _dbRow_cfdi.empresa_plaza_sucursal_cajachica_id:
            _dbRow_cfdi.empresa_plaza_sucursal_cajachica_id = request.vars.empresa_plaza_sucursal_cajachica_id
            _dbRow_cfdi.update_record()

            dbc01.commit()
        else:
            # Mantener mismo row.
            pass
    else:
        pass

    _dbTable.bancocontpaqi_id_cliente.requires = IS_IN_DB(
        dbc01(
            dbc01.tempresa_bancoscontpaqi.empresa_id == _s_empresa_id
            ),
        'tempresa_bancoscontpaqi.id',
        dbc01.tempresa_bancoscontpaqi._format
        )

    _dbTable.empresa_cuentabancaria_id.requires = IS_IN_DB(
        dbc01(
            dbc01.tempresa_bancoscontpaqi.empresa_id == _s_empresa_id
            ),
        'tempresa_bancoscontpaqi.id',
        dbc01.tempresa_bancoscontpaqi._format
        )


    if _dbRow_cfdi.empresa_plaza_sucursal_cajachica_id:
        _dbRow_cajachica = dbc01.tempresa_plaza_sucursal_cajaschicas(_dbRow_cfdi.empresa_plaza_sucursal_cajachica_id)
        if _dbRow_registropago.formapago_id in (TFORMASPAGO.CHEQUE,):
            _dbTable.empresa_cuentabancaria_id.required = True
            _dbTable.empresa_cuentabancaria_id.notnull = True
            _dbTable.empresa_cuentabancaria_id.default = _dbRow_cajachica.empresa_cuentabancaria_id_cheques
            _dbTable.cheque.required = True
            _dbTable.cheque.requires = IS_NOT_EMPTY()
            _dbTable.fechadeposito.required = True
            _dbTable.fechadeposito.notnull = True
            _dbTable.fechadeposito.default = _dbRow_cfdi.dia
            _dbTable.monto.required = True
            _dbTable.monto.notnull = True
        elif _dbRow_registropago.formapago_id in (TFORMASPAGO.EFECTIVO,):
            _dbTable.monto.required = True
            _dbTable.monto.notnull = True
            _dbTable.monto.comment = "En caso de haber más de 1 pago en esta factura especificar el monto de la primer forma de pago."
        elif _dbRow_registropago.formapago_id in (TFORMASPAGO.TRANSFERENCIA,):
            _dbTable.empresa_cuentabancaria_id.required = True
            _dbTable.empresa_cuentabancaria_id.notnull = True
            _dbTable.empresa_cuentabancaria_id.default = _dbRow_cajachica.empresa_cuentabancaria_id_transferencia
            _dbTable.cuenta_cliente.required = True
            _dbTable.cuenta_cliente.requires = IS_NOT_EMPTY()
            _dbTable.referencia.required = True
            _dbTable.referencia.requires = IS_NOT_EMPTY()
            _dbTable.claverastero.required = True
            _dbTable.claverastero.requires = IS_NOT_EMPTY()
            _dbTable.fechadeposito.required = True
            _dbTable.fechadeposito.notnull = True
            _dbTable.fechadeposito.default = _dbRow_cfdi.dia
            _dbTable.monto.required = True
            _dbTable.monto.notnull = True
        else:
            pass

    _dbTable.empresa_cuentabancaria_id.requires = IS_IN_DB(
        dbc01(
            (dbc01.tempresa_cuentasbancarias.empresa_id == request.args[1])
            ),
         'tempresa_cuentasbancarias.id',
        dbc01.tempresa_cuentasbancarias._format
        )

    _dbTable.cfdi_id.default = _s_cfdi_id

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tcfdis,
        xFieldLinkMaster = [_dbTable.id],
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_new,
            request.stv_fwk_permissions.btn_save,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_edit
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab + "_" + str(_s_registropago_id),
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.cfdi_id,
            _dbTable.monto,
            _dbTable.empresa_cuentabancaria_terminal_id,
            _dbTable.empresa_cuentabancaria_id,
            ],
        L_fieldsToSearch = [_dbTable.id],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        x_offsetInArgs = request.args[:6],
        Es_behavior = STV_FWK_FORM.ES_BEHAVIOR.S_PLUS_FORM
        )

    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionLabel = "Permiso",
        s_permissionIcon = "fa fa-compass",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
            ),
        L_options = []
        )

    _O_cat.addRowContentProperty(
        'formHidden', {
            'cfdi_id': str(_s_cfdi_id),
            'formapago_id': str(_dbRow_registropago.formapago_id),
            'verificacion_aceptada': str(0)
            }
        )

    _D_returnVars = _O_cat.process()

    _D_returnVars.n_formapago_id = _dbRow_registropago.formapago_id

    if _D_returnVars.D_tabOptions.b_accepted:
        # Este commit se hace para evitar que se haga rollback al registro de pago.
        dbc01.commit()

        if _dbRow_cfdi.etapa in (TCFDIS.E_ETAPA.CON_POLIZA,):

            _D_returnGenerarPrepoliza = STV_FWK_PREPOLIZA().generar(_s_cfdi_id, True, True, True)

            if _D_returnGenerarPrepoliza.L_msgError:
                for _s_error in _D_returnGenerarPrepoliza.L_msgError:
                    stv_flashError("Contabilización cancelado", _s_error)
            else:
                stv_flashSuccess("Contabilización cancelado", "Movimiento cancelado generado exitosamente.")

        else:
            if _dbRow_cfdi.dia == _dbRow_cfdi.sat_fecha_cancelacion.date():
                # Se cambia el estado al CFDI de ingreso por NO_CONTABILIZAR_CANCELADO
                dbc01(dbc01.tcfdis.id == _dbRow_cfdi.id).update(etapa = TCFDIS.E_ETAPA.NO_CONTABILIZAR_CANCELADO)
                dbc01(dbc01.tcfdis.id == _dbRow_cfdi.id).update(etapa_cancelado = TCFDIS.E_ETAPA.NO_CONTABILIZAR_CANCELADO)

                # Se borrar su prepoliza.
                _dbRowCfidPrepoliza = dbc01(
                    (dbc01.tcfdi_prepolizas.cfdi_id == _dbRow_cfdi.id)
                    ).delete()

                stv_flashError(
                    "Contabilización cancelado",
                    "La factura no puede contabilizarse debido a que la cancelación de este fue el mismo día que su "
                    "emisión. Se cambia el estatus a 'No contabilizable por cancelación'."
                    )
            else:
                stv_flashError(
                    "Contabilización cancelado",
                    "La factura no puede contabilizarse debido a que el ingreso aún no ha sido contabilizado."
                    )

    else:
        _D_returnVars.cfdiForm = _O_cat.createFactoryForm(
            L_formFactoryFields = [
                dbc01.tcfdis.empresa_plaza_sucursal_cajachica_id
                ],
            dbRow_formFactory = Storage(
                empresa_plaza_sucursal_cajachica_id = _dbRow_cfdi.empresa_plaza_sucursal_cajachica_id
                ),
            s_factoryTableName = False,
            D_inputsHidden = {}
            )
        pass

    _D_returnVars.dbRow_cfdi = _dbRow_registropago

    return _D_returnVars


def cfdi_sincontabilizar_egresoscancelados():
    """ Relación de CFDIs sin contabilizar cancelados para una caja chica por concepto de egreso

    @descripcion Sin Contabilizar Egresos Cancelados

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Caja chica y Fecha
    @keyword arg3: empresa_plaza_sucursal_cajachica_id - Año - Mes - Día.
    @keyword [arg4]: operación a realizar en el formulario.
    @keyword [arg5]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    #@permiso btn_new
    #@permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    @permiso btn_view
    #@permiso btn_invalidate
    #@permiso btn_validate
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    #@permiso btn_signature
    @permiso special_permisson_01:[fas fa-file-code] Ignorar movimiento
    """
    global _D_returnVars
    _dbTable = dbc01.tcfdis

    _s_empresa_id = request.args(1, None)
    _s_multi_id = request.args(3, '0-0-0-0')  # Se ponen 4 elementos separados '-' por default.
    _L_multi_id = _s_multi_id.split('-')
    if _L_multi_id[0] == 'None':
        _s_cajachica_id = None
    else:
        _s_cajachica_id = _L_multi_id[0]

    _s_action = request.args(4, request.stv_fwk_permissions.btn_none.code)

    _dt_fecha = datetime.datetime(int(_L_multi_id[1]), int(_L_multi_id[2]), int(_L_multi_id[3]))
    # Se generan las fechas en formato datetime para el rango del día
    _dt_fechaDesde = _dt_fecha.replace(hour = 0, minute = 0, second = 0, microsecond = 0)
    _dt_fechaHasta = _dt_fechaDesde + datetime.timedelta(days = 1)

    # Se crea el Query para identificar si el CFDI está como "sin contabilizar" y filstrando SOLO por CFDI que sea de tipo ingreso.
    _qry = (
            (_dbTable.empresa_id == _s_empresa_id)
            & (dbc01.tempresas.id == _dbTable.empresa_id)
            & (_dbTable.fecha >= dbc01.tempresas.fecha_inicial)
            & (_dbTable.etapa_cancelado == TCFDIS.E_ETAPA.SIN_CONTABILIZAR)
            & (_dbTable.sat_status == TCFDIS.SAT_STATUS.CANCELADO)
            & (_dbTable.rol == TCFDIS.E_ROL.EMISOR)
            & (_dbTable.empresa_plaza_sucursal_cajachica_id == _s_cajachica_id)
            & (_dbTable.tipocomprobante_id == TTIPOSCOMPROBANTES.EGRESO)
            & (_dbTable.sat_fecha_cancelacion >= _dt_fechaDesde)
            & (_dbTable.sat_fecha_cancelacion < _dt_fechaHasta)
    )

    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.empresa_serie_id,
        _dbTable.folio,
        _dbTable.fecha,
        _dbTable.metodopago_id,
        _dbTable.formapago_id,
        _dbTable.total,
        _dbTable.receptorrfc,
        _dbTable.receptornombrerazonsocial
        ]

    _L_OrderBy = [
        _dbTable.fecha,
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_findall
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = [
            _dbTable.id,
            ],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        L_fieldnameIds = [_dbTable.id],
        x_offsetInArgs = request.args[:4]
        )

    _O_cat.addRowSearchResultsProperty('orderBy', _L_OrderBy)

    # Se agrega el boton especial 01 que funciona para mandar el CFDI a CFDI no contabilizable.
    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionLabel = "No contabilizar CFDI",
        s_permissionIcon = "fas fa-window-close",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
            confirm = Storage(
                s_title = "Favor de confirmar operación",
                s_text = "¿Estás seguro de querer marcar este CFDI como 'No contabilizar'?",
                s_type = "warning",
                s_btnconfirm = "Confirmar",
                s_btncancel = "Cancelar",
                )
            ),
        L_options = []
        )

    # Se crea una nueva columna en el grid, se agrega un "+" y se especifica la función entorno a donde funcionará el botón "+".
    _O_cat.addRowSearchResults_addColumnBegin(
        es_typeColumn = STV_FWK_FORM.ES_TYPECOLUMN.S_PLUS,
        s_colname = "plus",
        D_field = Storage(
            readable = True
            ),
        m_classHtml = lambda D_column, dbRow: "stv_rowresults_plus",
        D_data = Storage(
            url = URL(f = 'cfdi_sincontabilizar_ingresos_plus', args = request.args[:4] + [
                'master_' + str(request.function)])
            )
        )

    # Código para poder jalar la vista de CFDIS y utilizarlas en esta función.
    if _s_action in (request.stv_fwk_permissions.btn_view.code,):
        from gluon.storage import List

        _s_cfdi_id = str(request.args(5, 0))  # Se guarda el argumento que se quiere utilizar dentro de una variable.
        request.args = List()
        request.args.append(_s_action)  # Se mete _s_action dentro de la lista con la función append.
        request.args.append(_s_cfdi_id)

        _D_returnVars = cfdisemitidos()
        # Usar el os.path.join para evitar conflicto por la diagonal (/) en sistemas operativos.
        response.view = os.path.join('250cfdi', 'cfdisemitidos.html')

    elif _s_action in (request.stv_fwk_permissions.special_permisson_01.code,):

        _s_cfdi_id = str(request.args(5, 0))  # Se guarda el argumento que se quiere utilizar dentro de una variable

        _dbRowCfdi = dbc01.tcfdis(_s_cfdi_id)

        if _dbRowCfdi.etapa != TCFDIS.E_ETAPA.SIN_CONTABILIZAR:
            stv_flashError("Cambio de estatus del CFDI",
                "El CFDI no se puede cambiar de estado ya que se encuentra con un estatus diferente a 'Sin contabilizar'.")
        else:
            _dbRowCfdi.etapa = TCFDIS.E_ETAPA.NO_CONTABILIZAR
            _dbRowCfdi.update_record()
            stv_flashSuccess("Cambio de estatus del CFDI", "Cambio hecho correctamente.")

            _D_overrideView = request.stv_fwk_permissions.view_refresh

            _D_returnVars = _O_cat.process(D_overrideView = _D_overrideView)
    else:
        _D_returnVars = _O_cat.process()

    return (_D_returnVars)


def cfdi_sincontabilizar_pagoscancelados():
    """ Relación de CFDIs sin contabilizar cancelados para una caja chica por concepto de pago

    @descripcion Sin Contabilizar Pagos Cancelados

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Caja chica y Fecha
    @keyword arg3: empresa_plaza_sucursal_cajachica_id - Año - Mes - Día.
    @keyword [arg4]: operación a realizar en el formulario.
    @keyword [arg5]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    #@permiso btn_new
    #@permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    @permiso btn_view
    #@permiso btn_invalidate
    #@permiso btn_validate
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    #@permiso btn_signature
    @permiso special_permisson_01:[fas fa-file-code] Ignorar movimiento
    """
    _dbTable = dbc01.tcfdis

    _s_empresa_id = request.args(1, None)
    _s_multi_id = request.args(3, '0-0-0-0')  # Se ponen 4 elementos separados '-' por default.
    _L_multi_id = _s_multi_id.split('-')
    if _L_multi_id[0] == 'None':
        _s_cajachica_id = None
    else:
        _s_cajachica_id = _L_multi_id[0]

    _s_action = request.args(4, request.stv_fwk_permissions.btn_none.code)

    _dt_fecha = datetime.datetime(int(_L_multi_id[1]), int(_L_multi_id[2]), int(_L_multi_id[3]))
    # Se generan las fechas en formato datetime para todo el rango del día
    _dt_fechaDesde = _dt_fecha.replace(hour = 0, minute = 0, second = 0, microsecond = 0)
    _dt_fechaHasta = _dt_fechaDesde + datetime.timedelta(days = 1)

    # Se crea el Query para identificar si el CFDI está como "sin contabilizar" y filstrando SOLO por CFDI que sea de
    # tipo ingreso.
    _qry = (
        (_dbTable.empresa_id == _s_empresa_id)
        & (dbc01.tempresas.id == _dbTable.empresa_id)
        & (_dbTable.fecha >= dbc01.tempresas.fecha_inicial)
        & (_dbTable.etapa_cancelado == TCFDIS.E_ETAPA.SIN_CONTABILIZAR)
        & (_dbTable.sat_status == TCFDIS.SAT_STATUS.CANCELADO)
        & (_dbTable.rol == TCFDIS.E_ROL.EMISOR)
        & (_dbTable.empresa_plaza_sucursal_cajachica_id == _s_cajachica_id)
        & (_dbTable.tipocomprobante_id == TTIPOSCOMPROBANTES.PAGO)
        & (_dbTable.sat_fecha_cancelacion >= _dt_fechaDesde)
        & (_dbTable.sat_fecha_cancelacion < _dt_fechaHasta)
    )

    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.empresa_serie_id,
        _dbTable.folio,
        _dbTable.fecha,
        _dbTable.metodopago_id,
        _dbTable.formapago_id,
        _dbTable.total,
        _dbTable.receptorrfc,
        _dbTable.receptornombrerazonsocial,
        _dbTable.sat_status
        ]

    _L_OrderBy = [
        _dbTable.fecha,
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_findall
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = [
            _dbTable.id,
            ],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        L_fieldnameIds = [_dbTable.id],
        x_offsetInArgs = request.args[:4]
        )

    _O_cat.addRowSearchResultsProperty('orderBy', _L_OrderBy)

    # Se agrega el boton especial 01 que funciona para mandar el CFDI a CFDI no contabilizable.
    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionLabel = "No contabilizar CFDI",
        s_permissionIcon = "fas fa-window-close",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
            confirm = Storage(
                s_title = "Favor de confirmar operación",
                s_text = "¿Estás seguro de querer marcar este CFDI como 'No contabilizar'?",
                s_type = "warning",
                s_btnconfirm = "Confirmar",
                s_btncancel = "Cancelar",
                )
            ),
        L_options = []
        )

    # Se crea una nueva columna en el grid, se agrega un "+" y se especifica la función entorno a donde funcionará el
    # botón "+".
    _O_cat.addRowSearchResults_addColumnBegin(
        es_typeColumn = STV_FWK_FORM.ES_TYPECOLUMN.S_PLUS,
        s_colname = "plus",
        D_field = Storage(
            readable = True
            ),
        m_classHtml = lambda D_column, dbRow: "stv_rowresults_plus",
        D_data = Storage(
            url = URL(f = 'cfdi_sincontabilizar_pagoscancelados_plus', args = request.args[:4] + [
                'master_' + str(request.function)])
            )
        )

    # Código para poder jalar la vista de CFDIS y utilizarlas en esta función.
    if _s_action in (request.stv_fwk_permissions.btn_view.code,):
        from gluon.storage import List

        _s_cfdi_id = str(request.args(5, 0))  # Se guarda el argumento que se quiere utilizar dentro de una variable.
        request.args = List()
        request.args.append(_s_action)  # Se mete _s_action dentro de la lista con la función append.
        request.args.append(_s_cfdi_id)

        _D_returnVars = cfdisemitidos()
        # Usar el os.path.join para evitar conflicto por la diagonal (/) en sistemas operativos.
        response.view = os.path.join('250cfdi', 'cfdisemitidos.html')

    elif _s_action in (request.stv_fwk_permissions.special_permisson_01.code,):

        _s_cfdi_id = str(request.args(5, 0))  # Se guarda el argumento que se quiere utilizar dentro de una variable

        _dbRowCfdi = dbc01.tcfdis(_s_cfdi_id)

        if _dbRowCfdi.etapa != TCFDIS.E_ETAPA.SIN_CONTABILIZAR:
            stv_flashError("Cambio de estatus del CFDI",
                "El CFDI no se puede cambiar de estado ya que se encuentra con un estatus diferente a 'Sin contabilizar'.")
        else:
            _dbRowCfdi.etapa = TCFDIS.E_ETAPA.NO_CONTABILIZAR
            _dbRowCfdi.update_record()
            stv_flashSuccess("Cambio de estatus del CFDI", "Cambio hecho correctamente.")

            _D_overrideView = request.stv_fwk_permissions.view_refresh

            _D_returnVars = _O_cat.process(D_overrideView = _D_overrideView)
    else:
        _D_returnVars = _O_cat.process()

    return (_D_returnVars)

def cfdi_sincontabilizar_pagoscancelados_plus():
    """ Formulario para mostrar los distintios registros de pago por CFDI cancelados de tipo pago.

    @descripcion  CFDIs sin contabilizar ingresos cancelados captura

    @keyword arg0: Empresa
    @keyword arg1: empresa_id
    @keyword arg2: Caja chica y fecha
    @keyword arg3: id compuesto por 4 partes
            id de la caja chica
            año
            mes
            día
    @keyword arg5: CFDI
    @keyword arg6: empresa_id
    @keyword [arg7]: codigo de funcionalidad
    @keyword [arg8]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature

    """

    _dbTable = dbc01.tcfdi_registropagos

    _s_empresa_id = request.args(1, None)

    _s_multi_id = request.args(3, '0-0-0-0')  # Se ponen 4 elementos separados '-' por default.

    _L_multi_id = _s_multi_id.split('-')

    if _L_multi_id[0] == 'None':
        _s_cajachica_id = None
    else:
        _s_cajachica_id = _L_multi_id[0]

    _s_cfdi_id = request.args(5, None)

    _s_action = request.args(6, request.stv_fwk_permissions.btn_none.code)

    _dbRow_cfdi = dbc01.tcfdis(_s_cfdi_id)

    _dbTable.cfdi_id.default = _s_cfdi_id

    _dbTable.formapago_id.notnull = True
    _dbTable.formapago_id.required = True

    _dbTable.tipo_cfdi.default = TCFDI_REGISTROPAGOS.TIPO_CFDI.CANCELADO

    _dbTable.tratoefectivo.default = TCFDI_REGISTROPAGOS.E_TRATOEFECTIVO.RETENIDOCAJA

    _dbRowsRegistrospago = dbc01(
        (_dbTable.cfdi_id == _s_cfdi_id)
        & (_dbTable.tipo_cfdi == TCFDI_REGISTROPAGOS.TIPO_CFDI.CANCELADO)
        ).select(
        _dbTable.ALL
        )

    _dbRowsRegistrospago = dbc01(
        (_dbTable.cfdi_id == _s_cfdi_id)
        & (_dbTable.tipo_cfdi == TCFDI_REGISTROPAGOS.TIPO_CFDI.CANCELADO)
        ).select(
        _dbTable.ALL
        )

    _dbRow_cfdi = dbc01.tcfdis(_s_cfdi_id)

    if not _dbRowsRegistrospago and (_dbRow_cfdi.formapago_id == TFORMASPAGO.POR_DEFINIR):

        _D_registropago = Storage(
            cfdi_id = _s_cfdi_id,
            formapago_id = _dbRow_cfdi.formapago_id,
            monto = _dbRow_cfdi.total,
            tipo_cfdi = TCFDI_REGISTROPAGOS.TIPO_CFDI.CANCELADO
            )

        _dbTable.insert(**_D_registropago)

    else:
        pass

    if _s_action == request.stv_fwk_permissions.btn_new.code:
        # Si el _s_action es btn_nuevo se recorrerán los registros de pago para calcular su total de montos.

        _n_registrospago_totalmontos = 0
        for _dbRowRegistropago in _dbRowsRegistrospago:
            _n_registrospago_totalmontos += _dbRowRegistropago.monto

        _n_resultadomonto = _dbRow_cfdi.total - _n_registrospago_totalmontos

        if _n_resultadomonto > 0:
            _dbTable.monto.default = _n_resultadomonto
        else:
            pass

    else:
        pass

    _qry = (
            (_dbTable.cfdi_id == _s_cfdi_id)
            & (_dbTable.tipo_cfdi == TCFDI_REGISTROPAGOS.TIPO_CFDI.CANCELADO)
    )

    _dbTable.cfdi_id.readable = True

    _dbTable.formapago_id.requires = IS_IN_DB(
        db01(
            db01.tformaspago.id.belongs(
                TFORMASPAGO.EFECTIVO,
                TFORMASPAGO.CHEQUE,
                TFORMASPAGO.TRANSFERENCIA,
                )
            ),
        'tformaspago.id',
        db01.tformaspago._format
        )

    _dbRow_cajachica = dbc01.tempresa_plaza_sucursal_cajaschicas(_dbRow_cfdi.empresa_plaza_sucursal_cajachica_id)

    if _dbRow_cfdi.formapago_id == TFORMASPAGO.POR_DEFINIR:
        _L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_findall
            ]
    else:
        _L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_new,
            request.stv_fwk_permissions.btn_save,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_remove,
            request.stv_fwk_permissions.btn_edit,
            request.stv_fwk_permissions.btn_findall
            ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        dbTableMaster = dbc01.tcfdis,
        xFieldLinkMaster = [_dbTable.cfdi_id],
        L_visibleButtons = _L_visibleButtons,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab + "_" + str(_s_cfdi_id),
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.cfdi_id,
            _dbTable.formapago_id,
            _dbTable.monto,
            _dbTable.empresa_cuentabancaria_terminal_id,
            _dbTable.empresa_cuentabancaria_id,
            ],
        L_fieldsToSearch = [_dbTable.id],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        x_offsetInArgs = request.args[:4],
        )

    # Se agrega un "+" y se especifica la función entorno a donde funcionará el botón "+".
    _O_cat.addRowSearchResults_addColumnBegin(
        es_typeColumn = STV_FWK_FORM.ES_TYPECOLUMN.S_PLUS,
        s_colname = "plus",
        D_field = Storage(
            readable = True
            ),
        m_classHtml = lambda D_column, dbRow: "stv_rowresults_plus",
        D_data = Storage(
            url = URL(f = 'cfdi_sincontabilizar_pagoscancelados_plus_plus',
                args = request.args[:6] + ['master_' + str(request.function)])
            )
        )

    _D_returnVars = _O_cat.process()

    if (_dbRow_cfdi.etapa in (TCFDIS.E_ETAPA.CON_PREPOLIZA, TCFDIS.E_ETAPA.CON_POLIZA)) and (
            _s_action == request.stv_fwk_permissions.btn_findall.code):
        _D_returnVars.refrescarmaestro = True
    else:
        _D_returnVars.refrescarmaestro = False

    if (_s_action == request.stv_fwk_permissions.btn_save.code) and _D_returnVars.rowContent_form.accepted:
        # Este commit se hace para evitar que se haga rollback al registro de pago.
        dbc01.commit()

        _D_returnGenerarPrepoliza = STV_FWK_PREPOLIZA().generar(_s_cfdi_id, True, True, True)

        if _D_returnGenerarPrepoliza.L_msgError:
            for _s_error in _D_returnGenerarPrepoliza.L_msgError:
                stv_flashError("Contabilización cancelado", _s_error)
        else:
            stv_flashSuccess("Contabilización cancelado", "Movimiento cancelado generado exitosamente.")

    else:
        pass

    _D_returnVars.D_useView.b_serverCreatesTab = True

    _D_returnVars.D_useView.b_cleanGrid = True

    return _D_returnVars


def _cfdi_sincontabilizar_pagoscancelados_plus_validation(O_form):
    """ Validación para el formulario de ingresoscancelados_plus.
    """

    _s_id = O_form.record_id

    _s_empresa_id = request.args(1, None)

    _s_diferencia = O_form.vars.diferencia

    if O_form.vars.formapago_id:

        if int(O_form.vars.formapago_id) == TFORMASPAGO.EFECTIVO:

            if not O_form.vars.monto:
                O_form.errors.monto = "El monto tiene que ser capturado."
            else:
                pass

        elif int(O_form.vars.formapago_id) == TFORMASPAGO.CHEQUE:

            if not O_form.vars.empresa_cuentabancaria_id:
                O_form.errors.empresa_cuentabancaria_id = "La cuenta bancaria tiene que ser capturada."
            else:
                pass

            if not O_form.vars.cheque:
                O_form.errors.cheque = "El cheque tiene que ser capturado."
            else:
                pass

            if not O_form.vars.cuenta_cliente:
                O_form.errors.cuenta_cliente = "La cuenta del cliente tiene que ser capturada."
            else:
                pass

            if not O_form.vars.fechadeposito:
                O_form.errors.fechadeposito = "La fecha del deposito tiene que ser capturada."
            else:
                pass

            if not O_form.vars.monto:
                O_form.errors.monto = "El monto tiene que ser capturado."
            else:
                pass
        else:
            # Si no es ninguna de las anteriores es Transferencia.

            if not O_form.vars.empresa_cuentabancaria_id:
                O_form.errors.empresa_cuentabancaria_id = "La cuenta bancaria tiene que ser capturada."
            else:
                pass

            if not O_form.vars.bancocontpaqi_id_cliente:
                O_form.errors.bancocontpaqi_id_cliente = "La banco del cliente tiene que ser capturado."
            else:
                pass

            if not O_form.vars.cuenta_cliente:
                O_form.errors.cuenta_cliente = "La cuenta del cliente tiene que ser capturada."
            else:
                pass

            if not O_form.vars.referencia:
                O_form.errors.referencia = "La referencia tiene que ser capturada."
            else:
                pass

            if not O_form.vars.claverastero:
                O_form.errors.claverastero = "La clave de rastreo tiene que ser capturada."
            else:
                pass

            if not O_form.vars.fechadeposito:
                O_form.errors.fechadeposito = "La fecha del deposito tiene que ser capturada."
            else:
                pass

            if not O_form.vars.monto:
                O_form.errors.monto = "El monto tiene que ser capturado."
            else:
                pass
    else:
        O_form.errors.formapago_id = "Se tiene que capturar una forma de pago para poder generar un registro de pago."

    if O_form.vars.monto < 0:
        O_form.errors.monto = "El monto no puede ser negativo."
    else:
        pass

    return O_form


def cfdi_sincontabilizar_pagoscancelados_plus_plus():
    """ Formulario para capturar el registro de pago por CFDI cancelados de tipo ingreso.

    @descripcion  CFDIs sin contabilizar ingresos cancelados captura

    @keyword arg0: Empresa
    @keyword arg1: empresa_id
    @keyword arg2: Caja chica y fecha
    @keyword arg3: id compuesto por 4 partes
            id de la caja chica
            año
            mes
            día
    @keyword arg5: CFDI
    @keyword arg6: empresa_id
    @keyword [arg7]: codigo de funcionalidad
    @keyword [arg8]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature

    """

    _dbTable = dbc01.tcfdi_registropagos
    _s_empresa_id = request.args(1, None)
    _s_multi_id = request.args(3, '0-0-0-0')  # Se ponen 4 elementos separados '-' por default.
    _L_multi_id = _s_multi_id.split('-')
    if _L_multi_id[0] == 'None':
        _s_cajachica_id = None
    else:
        _s_cajachica_id = _L_multi_id[0]
    _s_cfdi_id = request.args(5, None)
    _s_registropago_id = request.args(7, None)
    _s_action = request.args(8, request.stv_fwk_permissions.btn_none.code)

    _dbRow_cfdi = dbc01.tcfdis(_s_cfdi_id)

    _dbRow_registropago = dbc01.tcfdi_registropagos(_s_registropago_id)

    if not (_dbRow_registropago.formapago_id or 0) and (_s_action != request.stv_fwk_permissions.btn_new.code):
        # Si el registro de pago no tiene la forma de pago dada de alta y no sea nuevo se le pondrá la del CFDI.
        _dbRow_registropago.formapago_id = _dbRow_cfdi.formapago_id
        _dbRow_registropago.update_record()
    else:
        # De lo contrario ya está en la variable y se guardará automaticamente.
        pass

    # Si no se encuentra el id del registro y no esta guardando.
    if _s_action in (request.stv_fwk_permissions.btn_none.code,):

        _dbRows = dbc01(
            (_dbTable.id == _s_registropago_id)
            & (_dbTable.cfdi_id == _s_cfdi_id)
            ).select(
            _dbTable.ALL,
            orderby = _dbTable.id,
            )

        # Si solamente se encuentra un registro...
        if len(_dbRows) == 1:
            # ...se procede a configurar los parametros para editar.
            request.args = request.args[:8] + [request.stv_fwk_permissions.btn_edit.code, _dbRows.first().id]

            # En caso de tener un solo registro de pago se procede a dejar como default el total de la factura.

        # Si no se ecuentran registros...
        elif len(_dbRows) == 0:
            ### TODO: Ver si se queda esta lógica, porque si no hay registro de pago se crea en el nivel ingresos_plus.
            # ...se procede a configurar parámetros para nuevo
            request.args = request.args[:8] + [request.stv_fwk_permissions.btn_new.code]
        # Si se ecuentran más de un registro...
        else:
            # ticket P01-429 Debido a los cambios de arquitectura hechos se permite más de un registro de pago en las
            # facturas de ingreso.

            # request.args = request.args[:8] + [request.stv_fwk_permissions.btn_edit.code, str(_dbRows.last().id)]
            pass

    elif _s_action in (request.stv_fwk_permissions.btn_save.code,):
        if request.vars.empresa_plaza_sucursal_cajachica_id != _dbRow_cfdi.empresa_plaza_sucursal_cajachica_id:
            _dbRow_cfdi.empresa_plaza_sucursal_cajachica_id = request.vars.empresa_plaza_sucursal_cajachica_id
            _dbRow_cfdi.update_record()

            dbc01.commit()
        else:
            # Mantener mismo row.
            pass
    else:
        pass

    _dbTable.bancocontpaqi_id_cliente.requires = IS_IN_DB(
        dbc01(
            dbc01.tempresa_bancoscontpaqi.empresa_id == _s_empresa_id
            ),
        'tempresa_bancoscontpaqi.id',
        dbc01.tempresa_bancoscontpaqi._format
        )

    _dbTable.empresa_cuentabancaria_id.requires = IS_IN_DB(
        dbc01(
            dbc01.tempresa_bancoscontpaqi.empresa_id == _s_empresa_id
            ),
        'tempresa_bancoscontpaqi.id',
        dbc01.tempresa_bancoscontpaqi._format
        )


    if _dbRow_cfdi.empresa_plaza_sucursal_cajachica_id:
        _dbRow_cajachica = dbc01.tempresa_plaza_sucursal_cajaschicas(_dbRow_cfdi.empresa_plaza_sucursal_cajachica_id)
        if _dbRow_registropago.formapago_id in (TFORMASPAGO.CHEQUE,):
            _dbTable.empresa_cuentabancaria_id.required = True
            _dbTable.empresa_cuentabancaria_id.notnull = True
            _dbTable.empresa_cuentabancaria_id.default = _dbRow_cajachica.empresa_cuentabancaria_id_cheques
            _dbTable.cheque.required = True
            _dbTable.cheque.requires = IS_NOT_EMPTY()
            _dbTable.fechadeposito.required = True
            _dbTable.fechadeposito.notnull = True
            _dbTable.fechadeposito.default = _dbRow_cfdi.dia
            _dbTable.monto.required = True
            _dbTable.monto.notnull = True
        elif _dbRow_registropago.formapago_id in (TFORMASPAGO.EFECTIVO,):
            _dbTable.monto.required = True
            _dbTable.monto.notnull = True
            _dbTable.monto.comment = "En caso de haber más de 1 pago en esta factura especificar el monto de la primer forma de pago."
        elif _dbRow_registropago.formapago_id in (TFORMASPAGO.TRANSFERENCIA,):
            _dbTable.empresa_cuentabancaria_id.required = True
            _dbTable.empresa_cuentabancaria_id.notnull = True
            _dbTable.empresa_cuentabancaria_id.default = _dbRow_cajachica.empresa_cuentabancaria_id_transferencia
            _dbTable.cuenta_cliente.required = True
            _dbTable.cuenta_cliente.requires = IS_NOT_EMPTY()
            _dbTable.referencia.required = True
            _dbTable.referencia.requires = IS_NOT_EMPTY()
            _dbTable.claverastero.required = True
            _dbTable.claverastero.requires = IS_NOT_EMPTY()
            _dbTable.fechadeposito.required = True
            _dbTable.fechadeposito.notnull = True
            _dbTable.fechadeposito.default = _dbRow_cfdi.dia
            _dbTable.monto.required = True
            _dbTable.monto.notnull = True
        else:
            pass

    _dbTable.empresa_cuentabancaria_id.requires = IS_IN_DB(
        dbc01(
            (dbc01.tempresa_cuentasbancarias.empresa_id == request.args[1])
            ),
         'tempresa_cuentasbancarias.id',
        dbc01.tempresa_cuentasbancarias._format
        )

    _dbTable.cfdi_id.default = _s_cfdi_id

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tcfdis,
        xFieldLinkMaster = [_dbTable.id],
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_new,
            request.stv_fwk_permissions.btn_save,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_edit
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab + "_" + str(_s_registropago_id),
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.cfdi_id,
            _dbTable.monto,
            _dbTable.empresa_cuentabancaria_terminal_id,
            _dbTable.empresa_cuentabancaria_id,
            ],
        L_fieldsToSearch = [_dbTable.id],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        x_offsetInArgs = request.args[:6],
        Es_behavior = STV_FWK_FORM.ES_BEHAVIOR.S_PLUS_FORM
        )

    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionLabel = "Permiso",
        s_permissionIcon = "fa fa-compass",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
            ),
        L_options = []
        )

    _O_cat.addRowContentProperty(
        'formHidden', {
            'cfdi_id': str(_s_cfdi_id),
            'formapago_id': str(_dbRow_registropago.formapago_id),
            'verificacion_aceptada': str(0)
            }
        )

    _D_returnVars = _O_cat.process()

    _D_returnVars.n_formapago_id = _dbRow_registropago.formapago_id

    if _D_returnVars.D_tabOptions.b_accepted:
        # Este commit se hace para evitar que se haga rollback al registro de pago.
        dbc01.commit()

        if _dbRow_cfdi.etapa in (TCFDIS.E_ETAPA.CON_POLIZA,):

            _D_returnGenerarPrepoliza = STV_FWK_PREPOLIZA().generar(_s_cfdi_id, True, True, True)

            if _D_returnGenerarPrepoliza.L_msgError:
                for _s_error in _D_returnGenerarPrepoliza.L_msgError:
                    stv_flashError("Contabilización cancelado", _s_error)
            else:
                stv_flashSuccess("Contabilización cancelado", "Movimiento cancelado generado exitosamente.")

        else:
            if _dbRow_cfdi.dia == _dbRow_cfdi.sat_fecha_cancelacion.date():
                # Se cambia el estado al CFDI de ingreso por NO_CONTABILIZAR_CANCELADO
                dbc01(dbc01.tcfdis.id == _dbRow_cfdi.id).update(etapa = TCFDIS.E_ETAPA.NO_CONTABILIZAR_CANCELADO)
                dbc01(dbc01.tcfdis.id == _dbRow_cfdi.id).update(etapa_cancelado = TCFDIS.E_ETAPA.NO_CONTABILIZAR_CANCELADO)

                # Se borrar su prepoliza.
                _dbRowCfidPrepoliza = dbc01(
                    (dbc01.tcfdi_prepolizas.cfdi_id == _dbRow_cfdi.id)
                    ).delete()

                stv_flashError(
                    "Contabilización cancelado",
                    "La factura no puede contabilizarse debido a que la cancelación de este fue el mismo día que su "
                    "emisión. Se cambia el estatus a 'No contabilizable por cancelación'."
                    )
            else:
                stv_flashError(
                    "Contabilización cancelado",
                    "La factura no puede contabilizarse debido a que el ingreso aún no ha sido contabilizado."
                    )

    else:
        _D_returnVars.cfdiForm = _O_cat.createFactoryForm(
            L_formFactoryFields = [
                dbc01.tcfdis.empresa_plaza_sucursal_cajachica_id
                ],
            dbRow_formFactory = Storage(
                empresa_plaza_sucursal_cajachica_id = _dbRow_cfdi.empresa_plaza_sucursal_cajachica_id
                ),
            s_factoryTableName = False,
            D_inputsHidden = {}
            )
        pass

    _D_returnVars.dbRow_cfdi = _dbRow_registropago

    return _D_returnVars

def cfdi_buscar_emitidos():
    """ Forma genérica para buscar CFDIs
    Se intenta usar esta forma genérica para la búsqueda de CFDIs, remplazando la otras pantallas de búsqueda

    @descripcion CFDIs Buscar Emitidos

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg2]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    #@permiso btn_new
    #@permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso btn_searchcancel
    @permiso btn_searchdone
    @permiso special_permisson_01:[fas fa-file-code] Importar CFDI
    @permiso special_permisson_02:[fas fa-file-code] Mandar por email
    @permiso special_permisson_03:[fas fa-file-code] Re-mapear y calcular CFDI
    @permiso special_permisson_04:[fas fa-file-code] Recalcular saldo del Cliente

    """

    # noinspection DuplicatedCode
    _D_return = Storage(
        E_return = CLASS_e_RETURN.OK,
        s_msgError = "",
        dbTable = dbc01.tcfdis,
        )

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 2,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        )

    _dbTable = _D_return.dbTable
    _D_useView = None
    _s_overrideCode = None
    _CLS_TABLA = TCFDIS
    _CLS_TABLA_REFERENCIA = None  # Clase de la tabla con la que existe la referencia de la búsqueda
    _qry = True

    # s_llamada y otros vars solo se reciben la primera vez, luego se encapsulan en vars_request
    _s_llamada = request.vars.get("s_llamada", None)

    if _s_llamada:
        # Significa que existen configuraciones de pre-filtrado

        if _s_llamada == "131almacenes_cfdis/cfdi_relacionados":
            _CLS_TABLA_REFERENCIA = TCFDI_RELACIONADOS.TRASLADO
            _s_cfdi_id_referencia = request.vars.get("s_cfdi_id", None)
            if not _s_cfdi_id_referencia:
                stv_flashError("Buscar CFDI Relacionados", "No se definió el CFDI id que se está editando")
                _s_overrideCode = request.stv_fwk_permissions.btn_searchcancel.code
            else:
                _D_results = _CLS_TABLA_REFERENCIA.puede_crear(x_cfdi = _s_cfdi_id_referencia)
                if _D_results.E_return > CLASS_e_RETURN.OK:
                    stv_flashError("Buscar CFDI Relacionados", _D_results.s_msgError)
                    _s_overrideCode = request.stv_fwk_permissions.btn_searchcancel.code
                else:
                    pass
        else:
            pass

        # Se configuran los filtros fijos
        _s_filtro_cliente_id = request.vars.get("s_filtro_cliente_id", None)

        if _s_filtro_cliente_id:
            _qry &= dbc01.tcfdis.receptorcliente_id == _s_filtro_cliente_id
        else:
            pass

    else:
        pass

    _D_results = _CLS_TABLA.BUSQUEDA_AVANZADA.GENERA_PREFILTRO(_D_args)
    _qry &= _D_results.qry

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        dbTableMaster = None,
        xFieldLinkMaster = [],
        L_visibleButtons = request.stv_fwk_permissions.L_formSearchButtons + [
            request.stv_fwk_permissions.special_permisson_01,
            request.stv_fwk_permissions.special_permisson_02,
            request.stv_fwk_permissions.special_permisson_03,
            request.stv_fwk_permissions.special_permisson_04,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.tipocomprobante_id,
            _dbTable.serie, _dbTable.folio, _dbTable.fecha,
            _dbTable.fechatimbrado,
            _dbTable.metodopago_id, _dbTable.formapago_id, _dbTable.total, _dbTable.monedacontpaqi_id,
            _dbTable.receptorrfc, _dbTable.receptornombrerazonsocial,
            _dbTable.sat_status
            ],
        L_fieldsToSearch = [
            _dbTable.fecha,
            _dbTable.fechatimbrado,
            _dbTable.receptorrfc,
            _dbTable.uuid,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            _dbTable.folio,
            dbc01.tcfdi_relacionados.cfdirelacionado_id,
            dbc01.tcfdi_complementopago_doctosrelacionados.cfdirelacionado_id,
            ],
        x_offsetInArgs = _D_args.n_offsetArgs
        )

    _D_vars_request = _O_cat.bypassVars(
        s_nombreVar = "vars_request",
        )

    _O_cat.addRowContentProperty(
        'formHidden',
        x_value = Storage(
            s_filtro_cliente_id = request.vars.s_filtro_cliente_id or "",
            )
        )

    _O_cat.addRowSearchResultsLeftJoin(
        dbc01.tcfdi_relacionados.on(dbc01.tcfdi_relacionados.cfdi_id == _dbTable.id)
        )

    _O_cat.addRowSearchResultsLeftJoin(
        dbc01.tcfdi_complementopagos.on(dbc01.tcfdi_complementopagos.cfdi_id == _dbTable.id)
        )

    _O_cat.addRowSearchResultsLeftJoin(
        dbc01.tcfdi_complementopago_doctosrelacionados.on(
            dbc01.tcfdi_complementopago_doctosrelacionados.cfdi_complementopago_id == dbc01.tcfdi_complementopagos.id
            )
        )

    _D_result = _CLS_TABLA.BUSQUEDA_AVANZADA.CONFIGURA(
        O_cat = _O_cat,
        s_empresa_id = _D_args.s_empresa_id,
        tipocomprobante_id = request.vars.tipocomprobante_id or None
        )

    # Se agrega información para el boton de finalizar la búsqueda
    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.btn_searchdone.code,
        D_actions = Storage(
            D_data = Storage(
                vars_request = simplejson.dumps(_D_vars_request, cls = STV_FWK_LIB.DecimalEncodere)
                )
            ),
        L_options = []
        )

    # Se agrega configuración para botones especiales
    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionLabel = "Importar CFDI",
        s_permissionIcon = "fa fa-compass",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_find),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
            ),
        L_options = []
        )
    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_02.code,
        s_permissionLabel = "Mandar por email",
        s_permissionIcon = "fa fa-compass",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
            ),
        L_options = []
        )
    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_03.code,
        s_permissionLabel = "Remapear y recalcular CFDI",
        s_permissionIcon = "fa fa-compass",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
            D_data = Storage()
            ),
        L_options = []
        )
    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_04.code,
        s_permissionLabel = "Recalcular saldo cliente",
        s_permissionIcon = "fa fa-compass",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
            ),
        L_options = []
        )

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView,
        s_overrideCode = _s_overrideCode
        )

    if (
            (_D_vars_request.get("E_tipo", None) == str(STV_FWK_FORM.E_TIPO.MODAL))
            and (_D_args.s_accion == request.stv_fwk_permissions.btn_none.code)
            ):
        _D_returnVars.D_useView.b_serverCreatesTab = True
        _D_returnVars.D_useView.L_actionsCreateView += [_D_args.s_accion]

    elif _D_args.s_accion == request.stv_fwk_permissions.btn_searchdone.code:

        if "s_link_alterminarbusqueda" in _D_vars_request:
            _D_returnVars.D_tabOptions.D_redirect = Storage(
                s_url = (
                    _D_vars_request.s_link_alterminarbusqueda
                    + "/" + request.stv_fwk_permissions.btn_searchdone.code
                    ),
                x_config = Storage(
                    x_data = Storage(
                        L_seleccionados = _D_args.L_ids
                        ),
                    ),
                s_type = 'form',
                E_row = STV_FWK_FORM.E_ROWS.PADRE_RESULTADOS
                )
        else:
            pass

    else:
        pass

    return _D_returnVars


def _cfdi_contabilizacion(s_cfdi_id):
    """ Proceso para generar la contabilización del cfdi

    """

    _dbTable = dbc01.tcfdi_contabilizacion

    _D_return = Storage(
        L_msgError = [],
        E_return = CLASS_e_RETURN.NOK_ERROR,
        n_prepoliza_id = 0
        )

    # Variable utilizada para definir el proceso en el que se encuentra la generación.
    _s_proceso = ""

    try:

        # Si ya existe la pre-poliza se elimina.
        dbc01(dbc01.tcfdi_contabilizacion.cfdi_id == s_cfdi_id).delete()
        dbc01.commit()

        # Query para obtener las variables necesarias para las condiciones.
        #         _dbRows = dbc01(
        #             (dbc01.tcfdis.id == s_cfdi_id)
        #             ).select(
        #                 dbc01.tcfdis.ALL,
        #                 dbc01.tcfdi_conceptos.ALL,
        #                 dbc01.tcfdi_concepto_impuestostrasladados.ALL,
        #                 dbc01.tcfdi_concepto_impuestosretenidos.ALL,
        #                 dbc01.tcfdi_complementopago_doctosrelacionados.ALL,
        #                 dbc01.tcfdi_complementopagos.ALL,
        #                 dbc01.tcfdi_relacionados.ALL,
        #                 _dbTable_cfdiReferenciado.ALL,
        #                 _dbTable_cfdiReferenciadoPago.ALL,
        #                 orderby = [
        #                     dbc01.tcfdi_conceptos.id,
        #                     _dbTable_cfdiReferenciado.fecha,
        #                     _dbTable_cfdiReferenciadoPago.fecha,
        #                     ],
        #                 left = [
        #                     dbc01.tcfdi_conceptos.on(dbc01.tcfdi_conceptos.cfdi_id == dbc01.tcfdis.id),
        #                     dbc01.tcfdi_concepto_impuestostrasladados.on(dbc01.tcfdi_concepto_impuestostrasladados.cfdi_concepto_id == dbc01.tcfdi_conceptos.id),
        #                     dbc01.tcfdi_concepto_impuestosretenidos.on(dbc01.tcfdi_concepto_impuestosretenidos.cfdi_concepto_id == dbc01.tcfdi_conceptos.id),
        #                     dbc01.tcfdi_complementopago_doctosrelacionados.on(dbc01.tcfdi_complementopago_doctosrelacionados.cfdirelacionado_id == dbc01.tcfdis.id),
        #                     dbc01.tcfdi_complementopagos.on(dbc01.tcfdi_complementopagos.id == dbc01.tcfdi_complementopago_doctosrelacionados.cfdi_complementopago_id),
        #                     _dbTable_cfdiReferenciadoPago.on(_dbTable_cfdiReferenciadoPago.id == dbc01.tcfdi_complementopagos.cfdi_id),
        #                     dbc01.tcfdi_relacionados.on(dbc01.tcfdi_relacionados.cfdirelacionado_id == dbc01.tcfdis.id),
        #                     _dbTable_cfdiReferenciado.on(_dbTable_cfdiReferenciado.id == dbc01.tcfdi_relacionados.cfdi_id),
        #                     ]
        #                 )

        _s_proceso = "Se obtiene información del CFDI y conceptos"
        _D_result = _cfdi_contabilizacion_conceptos(s_cfdi_id)

        if _D_result.E_return != CLASS_e_RETURN.OK:

            _D_return.L_msgError += _D_result.L_msgError

        else:
            _D_result = _cfdi_contabilizacion_pagos(
                s_cfdi_id, _D_result.L_conceptos, _D_result.D_impuestosTrasladados, _D_result.D_impuestosRetenidos
                )
            _D_return.L_msgError += _D_result.L_msgError

        if _D_return.L_msgError:
            dbc01.rollback()
        else:
            _D_return.E_return = stvfwk2_e_RETURN.OK
            # se creo correctamente
            dbc01.commit()
            pass

    except Exception as _O_excepcion:
        _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
        _D_return.L_msgError.append('Proceso %s; Excepcion: %s' % (_s_proceso, str(_O_excepcion)))
        _D_return.n_prepoliza_id = 0

        # Se regresa cualquier inserción a la base de datos dbc01.
        dbc01.rollback()

    return _D_return


def _cfdi_contabilizacion_conceptos(s_cfdi_id):
    """ Proceso para generar la contabilización de los conceptos del cfdi

    """

    _dbTable = dbc01.tcfdi_contabilizacion

    _D_return = Storage(
        L_msgError = [],
        E_return = CLASS_e_RETURN.NOK_ERROR,
        L_conceptos = [],  # Cada elemento es un diccionario con dos datos: cfdi_concepto_id, importe, peso.
        D_impuestosTrasladados = Storage(),
        # Cada elemento es un diccionario con tres datos: el impuesto_id, tasaocuota y el importe; donde el id es impuesto_id+"_"+tasaocuota, peso.
        D_impuestosRetenidos = Storage(),
        # Cada elemento es un diccionario con tres datos: el impuesto_id, tasaocuota y el importe; donde el id es impuesto_id+"_"+tasaocuota, peso.
        )

    _dbRows = dbc01(
        (dbc01.tcfdis.id == s_cfdi_id)
        ).select(
        dbc01.tcfdis.ALL,
        dbc01.tcfdi_conceptos.ALL,
        dbc01.tcfdi_concepto_impuestostrasladados.ALL,
        dbc01.tcfdi_concepto_impuestosretenidos.ALL,
        orderby = [
            dbc01.tcfdi_conceptos.id,
            ],
        left = [
            dbc01.tcfdi_conceptos.on(dbc01.tcfdi_conceptos.cfdi_id == dbc01.tcfdis.id),
            dbc01.tcfdi_concepto_impuestostrasladados.on(
                dbc01.tcfdi_concepto_impuestostrasladados.cfdi_concepto_id == dbc01.tcfdi_conceptos.id
                ),
            dbc01.tcfdi_concepto_impuestosretenidos.on(
                dbc01.tcfdi_concepto_impuestosretenidos.cfdi_concepto_id == dbc01.tcfdi_conceptos.id
                ),
            ]
        )

    if not _dbRows:
        _D_return.L_msgError.append("CFDI no existe")
    else:

        _D_insertFields = Storage(
            cfdi_id = s_cfdi_id,
            cfdi_concepto_id = None,
            )

        for _dbRow in _dbRows:
            if _dbRow.tcfdis.tipocomprobante_id != TTIPOSCOMPROBANTES.INGRESO:
                _D_return.L_msgError.append("CFDI no es de ingreso")
                break
            elif _D_insertFields.cfdi_concepto_id != _dbRow.tcfdi_conceptos.id:
                # Si no se tiene dato en la variable, significa que se inserta el primer registro de contabilizacion.
                _D_insertFields.cfdi_concepto_id = _dbRow.tcfdi_conceptos.id

                _n_importe = _dbRow.tcfdi_conceptos.importe - _dbRow.tcfdi_conceptos.descuento
                _D_concepto = Storage(
                    cfdi_concepto_id = _D_insertFields.cfdi_concepto_id,
                    importe = _n_importe,
                    peso = _n_importe / (_dbRow.tcfdis.subtotal - _dbRow.tcfdis.descuento)
                    )

                if (_dbRow.tcfdis.metodopago_id == TMETODOSPAGO.UNAEXHIBICION):
                    _D_insertFields.cobrado = _D_concepto.importe
                elif (_dbRow.tcfdis.metodopago_id == TMETODOSPAGO.PARCIALIDADES):
                    _D_insertFields.por_cobrar = _D_concepto.importe
                else:
                    _D_return.L_msgError.append("Método de pago desconocido")
                    break

                _dbTable.insert(**_D_insertFields)

                _D_insertFields.cobrado = 0
                _D_insertFields.por_cobrar = 0
                _D_return.L_conceptos += [_D_concepto]

            else:
                pass

            if (
                    _dbRow.tcfdi_concepto_impuestostrasladados.id
            ):

                _s_impuestoIndex = str(_dbRow.tcfdi_concepto_impuestostrasladados.impuesto_id) + "_" + str(
                    _dbRow.tcfdi_concepto_impuestostrasladados.tasaocuota
                    )
                if _s_impuestoIndex in _D_return.D_impuestosTrasladados:
                    _D_return.D_impuestosTrasladados[
                        _s_impuestoIndex].importe += _dbRow.tcfdi_concepto_impuestostrasladados.importe
                else:
                    _D_return.D_impuestosTrasladados[_s_impuestoIndex] = Storage(
                        impuesto_id = _dbRow.tcfdi_concepto_impuestostrasladados.impuesto_id,
                        tasaocuota = _dbRow.tcfdi_concepto_impuestostrasladados.tasaocuota,
                        importe = _dbRow.tcfdi_concepto_impuestostrasladados.importe,
                        )

            else:
                pass
            pass

            if (
                    _dbRow.tcfdi_concepto_impuestosretenidos.id
            ):

                _s_impuestoIndex = str(_dbRow.tcfdi_concepto_impuestosretenidos.impuesto_id) + "_" + str(
                    _dbRow.tcfdi_concepto_impuestosretenidos.tasaocuota
                    )
                if _s_impuestoIndex in _D_return.D_impuestosRetenidos:
                    _D_return.D_impuestosRetenidos[
                        _s_impuestoIndex].importe += _dbRow.tcfdi_concepto_impuestosretenidos.importe
                else:
                    _D_return.D_impuestosRetenidos[_s_impuestoIndex] = Storage(
                        impuesto_id = _dbRow.tcfdi_concepto_impuestosretenidos.impuesto_id,
                        tasaocuota = _dbRow.tcfdi_concepto_impuestosretenidos.tasaocuota,
                        importe = _dbRow.tcfdi_concepto_impuestosretenidos.importe
                        )
            else:
                pass
            pass

        if not _D_return.L_msgError:

            _D_insertFields.cfdi_concepto_id = None

            for _s_impTrasladado_index in _D_return.D_impuestosTrasladados:
                _D_return.D_impuestosTrasladados[_s_impTrasladado_index].peso = _D_return.D_impuestosTrasladados[
                                                                                    _s_impTrasladado_index].importe / \
                                                                                _dbRows[
                                                                                    0].tcfdis.totalimpuestostrasladados
                _D_insertFields.impuesto_id = _D_return.D_impuestosTrasladados[_s_impTrasladado_index].impuesto_id
                _D_insertFields.tasaocuota = _D_return.D_impuestosTrasladados[_s_impTrasladado_index].tasaocuota
                if (_dbRows[0].tcfdis.metodopago_id == TMETODOSPAGO.UNAEXHIBICION):
                    _D_insertFields.impuesto_trasladado = _D_return.D_impuestosTrasladados[
                        _s_impTrasladado_index].importe
                elif (_dbRows[0].tcfdis.metodopago_id == TMETODOSPAGO.PARCIALIDADES):
                    _D_insertFields.impuesto_portrasladar = _D_return.D_impuestosTrasladados[
                        _s_impTrasladado_index].importe
                else:
                    _D_return.L_msgError.append("Método de pago desconocido")
                    break

                _dbTable.insert(**_D_insertFields)
                _D_insertFields.impuesto_trasladado = 0
                _D_insertFields.impuesto_portrasladar = 0

            for _s_impRetenido_index in _D_return.D_impuestosRetenidos:
                _D_return.D_impuestosRetenidos[_s_impRetenido_index].peso = _D_return.D_impuestosRetenidos[
                                                                                _s_impRetenido_index].importe / _dbRows[
                                                                                0].tcfdis.totalimpuestosretenidos
                _D_insertFields.impuesto_id = _D_return.D_impuestosRetenidos[_s_impRetenido_index].impuesto_id
                _D_insertFields.tasaocuota = _D_return.D_impuestosRetenidos[_s_impRetenido_index].tasaocuota
                if (_dbRows[0].tcfdis.metodopago_id == TMETODOSPAGO.UNAEXHIBICION):
                    _D_insertFields.impuesto_retenido = _D_return.D_impuestosRetenidos[_s_impRetenido_index].importe
                elif (_dbRows[0].tcfdis.metodopago_id == TMETODOSPAGO.PARCIALIDADES):
                    _D_insertFields.impuesto_porretener = _D_return.D_impuestosRetenidos[_s_impRetenido_index].importe
                else:
                    _D_return.L_msgError.append("Método de pago desconocido")
                    break

                _dbTable.insert(**_D_insertFields)
                _D_insertFields.impuesto_retenido = 0
                _D_insertFields.impuesto_porretener = 0
        else:
            # Hay errores.
            pass

        if not _D_return.L_msgError:
            _D_return.E_return = stvfwk2_e_RETURN.OK
            # se creo correctamente.
        else:
            # Mantener el error en el retorno.
            pass

    return (_D_return)


def _cfdi_contabilizacion_pagos(s_cfdi_id, L_conceptos, D_impuestosTrasladados, D_impuestosRetenidos):
    """ Se agrega la contabilización de los pagos

    """

    _dbTable = dbc01.tcfdi_contabilizacion

    _dbTable_cfdiReferenciadoPago = dbc01.tcfdis.with_alias('cfdipago')

    _D_return = Storage(
        L_msgError = [],
        E_return = CLASS_e_RETURN.NOK_ERROR,
        )

    # Se procede a obtener la información de los pagos.
    _dbRowsPagos = dbc01(
        (dbc01.tcfdis.id == s_cfdi_id)
        & (dbc01.tcfdi_complementopago_doctosrelacionados.cfdirelacionado_id == dbc01.tcfdis.id)
        ).select(
        dbc01.tcfdis.ALL,
        dbc01.tcfdi_complementopago_doctosrelacionados.ALL,
        dbc01.tcfdi_complementopagos.ALL,
        _dbTable_cfdiReferenciadoPago.ALL,
        orderby = [
            _dbTable_cfdiReferenciadoPago.fecha,
            ],
        left = [
            dbc01.tcfdi_complementopagos.on(
                dbc01.tcfdi_complementopagos.id == dbc01.tcfdi_complementopago_doctosrelacionados.cfdi_complementopago_id
                ),
            _dbTable_cfdiReferenciadoPago.on(_dbTable_cfdiReferenciadoPago.id == dbc01.tcfdi_complementopagos.cfdi_id),
            ]
        )

    if (
            not (_dbRowsPagos)
    ):
        _D_return.E_return = CLASS_e_RETURN.OK
    else:

        # Se define el peso que se abonará a los conceptos y el peso que se abonará a los impuestos.
        _n_peso_conceptos = (_dbRowsPagos[0].tcfdis.subtotal - _dbRowsPagos[0].tcfdis.descuento) / (
            _dbRowsPagos[0].tcfdis.total)
        _n_peso_imp_trasladados = (_dbRowsPagos[0].tcfdis.totalimpuestostrasladados) / (_dbRowsPagos[0].tcfdis.total)
        _n_peso_imp_retenidos = (_dbRowsPagos[0].tcfdis.totalimpuestosretenidos) / (_dbRowsPagos[0].tcfdis.total)

        # TODO aplicar control de decimales para completar el 100% sumando los pesos.

        _D_insertFields = Storage(
            cfdi_id = s_cfdi_id,
            cfdi_relacionado_id = None,
            cfdi_concepto_id = None,
            cfdi_complementopago_doctorelacionado_id = None,
            )

        for _dbRowPago in _dbRowsPagos:
            _D_insertFields.cfdi_complementopago_doctorelacionado_id = _dbRowPago.tcfdi_complementopago_doctosrelacionados.id

            _n_importePago_conceptos = _n_peso_conceptos * _dbRowPago.tcfdi_complementopago_doctosrelacionados.imppagado

            for _D_concepto in L_conceptos:
                _D_insertFields.cfdi_concepto_id = _D_concepto.cfdi_concepto_id
                _D_insertFields.cobrado = _n_importePago_conceptos * _D_concepto.peso
                _D_insertFields.por_cobrar = (-1 * _D_insertFields.cobrado)

                _dbTable.insert(**_D_insertFields)

            _n_importePago_impTrasladados = _n_peso_imp_trasladados * _dbRowPago.tcfdi_complementopago_doctosrelacionados.imppagado

            _D_insertFields.cfdi_concepto_id = None
            _D_insertFields.cobrado = 0
            _D_insertFields.por_cobrar = 0
            for _s_impTrasladado_index in D_impuestosTrasladados:

                _D_insertFields.impuesto_id = D_impuestosTrasladados[_s_impTrasladado_index].impuesto_id
                _D_insertFields.tasaocuota = D_impuestosTrasladados[_s_impTrasladado_index].tasaocuota
                _D_insertFields.impuesto_trasladado = _n_importePago_impTrasladados * D_impuestosTrasladados[
                    _s_impTrasladado_index].peso
                _D_insertFields.impuesto_portrasladar = (-1) * _D_insertFields.impuesto_trasladado

                _dbTable.insert(**_D_insertFields)

            _n_importePago_impRetenidos = _n_peso_imp_retenidos * _dbRowPago.tcfdi_complementopago_doctosrelacionados.imppagado

            _D_insertFields.impuesto_id = None
            _D_insertFields.tasaocuota = 0
            _D_insertFields.impuesto_trasladado = 0
            _D_insertFields.impuesto_portrasladar = 0
            for _s_impRetenido_index in D_impuestosRetenidos:

                _D_insertFields.impuesto_id = D_impuestosRetenidos[_s_impRetenido_index].impuesto_id
                _D_insertFields.tasaocuota = D_impuestosRetenidos[_s_impRetenido_index].tasaocuota
                _D_insertFields.impuesto_retenido = _n_importePago_impRetenidos * D_impuestosRetenidos[
                    _s_impRetenido_index].peso
                _D_insertFields.impuesto_porretener = (-1) * _D_insertFields.impuesto_retenido

                _dbTable.insert(**_D_insertFields)

        _D_return.E_return = CLASS_e_RETURN.OK

    return (_D_return)



def poliza_cfdis():
    """ Formulario detalle de póliza

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: id compuesto por 4 partes
            id de la caja chica
            año
            mes
            día
        arg4: master_[nombre función maestro]
        arg5: poliza_id
        [arg6]: operación a realizar en el formulario
        [arg7]: id en caso de existir relación a un registro en la operación
    """

    _dbTable = dbc01.tcfdis

    _s_empresa_id = request.args(1, None)

    _s_multi_id = request.args(3, '0-0-0-0')  # Se ponen 4 elementos separados '-' por default.
    _L_multi_id = _s_multi_id.split('-')
    if _L_multi_id[0] == 'None':
        _s_cajachica_id = None
    else:
        _s_cajachica_id = _L_multi_id[0]

    _s_poliza_id = request.args(5, None)

    _s_action = request.args(6, request.stv_fwk_permissions.btn_none.code)

    # Se hace un select de todas las prepolizas que tengan ese folio.
    _qry = (
        (dbc01.tcfdi_prepolizas.poliza_id == _s_poliza_id)
    )

    # Boleano que dirá si se tiene que usar el refrescar vista o no (Platicarlo con Jaime)
    _b_refrescarvista = True

    # Se crea el Query para identificar los cfdis que son parte del folio.
    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.empresa_serie_id,
        _dbTable.folio,
        _dbTable.fecha,
        _dbTable.receptorrfc,
        _dbTable.receptornombrerazonsocial
        ]

    _L_OrderBy = [
        dbc01.tcfdis.fecha,
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        dbTableMaster = None,
        xFieldLinkMaster = [],
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_findall
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = [
            _dbTable.id,
            ],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        L_fieldnameIds = [_dbTable.id],
        x_offsetInArgs = request.args[:6]
        )

    _O_cat.addRowSearchResultsLeftJoin(
        _dbTable.on(
            (_dbTable.id == dbc01.tcfdi_prepolizas.cfdi_id)
            )
        )

    _O_cat.addRowSearchResultsProperty('orderBy', _L_OrderBy)

    _D_foliado = _poliza_get_estilopoliza(_s_poliza_id)

    # Código para poder jalar la vista de CFDIS y utilizarlas en esta función.
    if (_s_action in (request.stv_fwk_permissions.btn_view.code,)):
        from gluon.storage import List

        _s_cfdi_id = str(request.args(7, 0))  # Se guarda el argumento que se quiere utilizar dentro de una variable.
        request.args = List()
        request.args.append(_s_action)  # Se mete _s_action dentro de la lista con la función append.
        request.args.append(_s_cfdi_id)

        _D_returnVars = cfdisemitidos()
        # Usar el os.path.join para evitar conflicto por la diagonal (/) en sistemas operativos.
        response.view = os.path.join('250cfdi', 'cfdisemitidos.html')
        _b_refrescarvista = False

    elif _s_action in (request.stv_fwk_permissions.special_permisson_01.code,):

        _s_poliza_id = request.args(5, None)
        _s_cfdi_id = str(request.args(7, 0))  # Se guarda el argumento que se quiere utilizar dentro de una variable.

        _D_result_prepolizaCancelaciones = obtenerPrepoliza(
            _s_cfdi_id, TCFDI_PREPOLIZAS.SAT_STATUS.CANCELADO
            )

        if not _D_result_prepolizaCancelaciones.s_msgError:
            # Si encontró prepoliza de cancelación se empiezan las validaciones...
            _D_result_prepolizaIngreso = obtenerPrepoliza(
                _s_cfdi_id, TCFDI_PREPOLIZAS.SAT_STATUS.VIGENTE
                )
            if not _D_result_prepolizaIngreso.s_msgError:
                if _D_result_prepolizaIngreso['dbRowCfdiPrepoliza'][0].tpolizas.estatus != TPOLIZAS.ESTATUS.EXPORTADO:
                    _D_return = STV_FWK_POLIZA(_s_empresa_id).borrar_cfdi(_s_cfdi_id)
                    if _D_return.L_msgError:
                       _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                    else:
                       request.stv_flashInfo(
                           s_title = "Regresar CFDI",
                           s_msg = "Se regresó correctamente el CFDI",
                           s_mode = 'stv_flash',
                           D_addProperties = {}
                           )
                else:
                    stv_flashError(
                        "Regresar CFDI",
                        "El cambio del CFDI no puede ser realizado debido a que su CFDI de ingreso "
                        "ya se encuentra contabilizado."
                        )
            else:
                pass

        else:
            # Si no econtró prepoliza de cancelación se elimina el CFDI de manera normal
            _D_return = STV_FWK_POLIZA(_s_empresa_id).borrar_cfdi(_s_cfdi_id)

            if _D_return.L_msgError:
                _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
            else:
                request.stv_flashInfo(
                    s_title = "Regresar CFDI",
                    s_msg = "Se regresó correctamente el CFDI",
                    s_mode = 'stv_flash',
                    D_addProperties = {}
                    )

        _D_overrideView = request.stv_fwk_permissions.view_refresh

    else:
        # No hacer nada y continuar con el procesio normal.
        _D_overrideView = None
        pass

    if (
            _D_foliado.n_foliado.estilopoliza == TEMPRESA_PLAZA_SUCURSAL_CONTABILIDADES.ESTILOPOLIZA.ACUMULADA_DESGLOZE_POR_CFDI):
        # Se agrega el boton especial 01 que funciona para regresar al CFDI.
        _O_cat.addPermissionConfig(
            s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
            s_permissionLabel = "Regresar CFDI",
            s_permissionIcon = "fas fa-angle-double-left",
            s_type = "button",
            s_send = "find",
            L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
            D_actions = Storage(
                js = Storage(
                    sfn_onPress = 'stv_fwk_button_press',
                    sfn_onSuccess = 'stv_fwk_button_success',
                    sfn_onError = 'stv_fwk_button_error',
                    ),
                swap = {},
                confirm = Storage(
                    s_title = "Favor de confirmar operación",
                    s_text = "¿Está seguro de querer regresar el CFDI? Una vez regresado tendrá que volverlo a contabilizar.",
                    s_type = "warning",
                    s_btnconfirm = "Aceptar",
                    s_btncancel = "Cancelar",
                    )
                ),
            L_options = []
            )
    else:
        # No es necesario mostrar este botón ya que es póliza por CFDI y no por día.
        pass

    if (_b_refrescarvista == True):
        _D_returnVars = _O_cat.process(D_overrideView = _D_overrideView)
    else:
        pass

    return (_D_returnVars)


def cfdi_entradasalida_relacionado():
    """ Formulario detalle de entradas y salidas para el CFDI

    Args:
        arg0: master_[nombre función maestro]
        arg1: cfdi_id
        [arg2]: operación a realizar en el formulario
        [arg3]: id en caso de existir relación a un registro en la operación

    """

    _dbTable = dbc01.tcfdis

    _s_action = request.stv_fwk_permissions.btn_view.code

    _s_cfdi_id = request.args(1, None)

    _L_formButtons = [
        request.stv_fwk_permissions.btn_none,
        request.stv_fwk_permissions.btn_view,
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(),
        dbTable = _dbTable,
        L_visibleButtons = _L_formButtons,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id
            ],
        L_fieldsToSearch = [
            _dbTable.id,
            ],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        L_fieldnameIds = [_dbTable.id],
        )

    from gluon.storage import List
    # _s_action = request.stv_fwk_permissions.btn_none.code.
    request.args = List([_s_action, _s_cfdi_id])

    _D_returnVars = vercfdis()
    # Usar el os.path.join para evitar conflicto por la diagonal (/) en sistemas operativos.
    response.view = os.path.join('250cfdi', 'vercfdis.html')

    _D_returnVars.D_useView.L_actionsCreateView += [request.stv_fwk_permissions.btn_view.code]

    return (_D_returnVars)



def vercfdis():
    """ Formulario genérico para ver los CFDIs.

    Args si args[0] = empresa_id:
        arg0: String empresa_id
        arg1: id de la empresa
        [arg2]: operación a realizar en el formulario
        [arg3]: id en caso de existir relación a un registro en la operación
    else:
        [arg0]: operación a realizar en el formulario
        [arg1]: id en caso de existir relación a un registro en la operación
    """

    _dbTable = dbc01.tcfdis

    ### TODO: Ver si es necesario este IF o solo mandar directamente el CFDI ID.
    if request.args(0, 'none') == 'empresa_id':
        _dbTableMaster = dbc01.tempresas
        _dbFieldLinkMaster = [_dbTable.empresa_id]
        _s_empresa_id = request.args(1, None)
        _s_action = request.args(2, request.stv_fwk_permissions.btn_none.code)
        _s_cfdi_id = request.args(3)

    else:
        _dbTableMaster = None
        _dbFieldLinkMaster = []
        _s_action = request.args(0, request.stv_fwk_permissions.btn_none.code)
        _s_cfdi_id = request.args(1)
        _s_empresa_id = None

    _L_formButtons = [
        request.stv_fwk_permissions.btn_none,
        request.stv_fwk_permissions.btn_view,
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(),
        dbTable = _dbTable,
        dbTableMaster = _dbTableMaster,
        xFieldLinkMaster = _dbFieldLinkMaster,
        L_visibleButtons = _L_formButtons,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [],
        L_fieldsToSearch = [],
        L_fieldsToSearchIfDigit = [],
        )

    _D_returnVars = _O_cat.process()

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_conceptos,
        s_url = URL(f = 'vercfdi_conceptos', args = ['master_' + str(request.function), _s_cfdi_id]),
        s_idHtml = fn_createIDTabUnique(f = 'vercfdi_conceptos', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_relacionados,
        s_url = URL(f = 'vercfdi_relacionados', args = ['master_' + str(request.function), _s_cfdi_id]),
        s_idHtml = fn_createIDTabUnique(f = 'vercfdi_relacionados', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_complementopagos,
        s_url = URL(f = 'vercfdi_pagos', args = ['master_' + str(request.function), _s_cfdi_id]),
        s_idHtml = fn_createIDTabUnique(f = 'vercfdi_pagos', s_masterIdentificator = str(request.function))
        )

    _D_returnVars['htmlid_emisor'] = _O_cat.addSubTab(
        dbTableDetail = None,
        s_url = False,
        s_tabName = "Emisor",
        s_idHtml = None,
        s_type = "extended"
        )

    _D_returnVars['htmlid_receptor'] = _O_cat.addSubTab(
        dbTableDetail = None,
        s_url = False,
        s_tabName = "Receptor",
        s_idHtml = None,
        s_type = "extended"
        )

    _D_returnVars['htmlid_vendedores'] = _O_cat.addSubTab(
        dbTableDetail = None,
        s_url = False,
        s_tabName = "Vendedores",
        s_idHtml = None,
        s_type = "extended"
        )

    #     _dbRow_xml = dbc02(dbc02.tcfdi_xmls.cfdi_id == _s_cfdi_id).select(dbc02.tcfdi_xmls.xml_cfdi).first()
    #
    #     _D_returnVars.dbRow_xml = _dbRow_xml
    #
    #     _D_returnVars['htmlid_xml'] = _O_cat.addSubTab(
    #         dbTableDetail = None,
    #         s_url = False,
    #         s_tabName = "XML",
    #         s_idHtml = None,
    #         s_type = "extended"
    #         )

    _O_cat.addSubTab(
        dbTableDetail = dbc02.tcfdi_xmls,
        s_url = URL(
            a = 'app_timbrado',
            c = '250cfdi',
            f = 'cfdi_xml',
            args = ['master_' + str(request.function), _s_cfdi_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            a = 'app_timbrado',
            c = '250cfdi',
            f = 'cfdi_xml',
            s_masterIdentificator = str(request.function)
            )
        )

    return (_D_returnVars)


def vercfdi_conceptos():
    """ Formulario detalle genérico para ver los conceptos del cfdi

    Args:
        arg0: master_[nombre función maestro]
        arg1: cfdi_id
        [arg2]: operación a realizar en el formulario
        [args]: id en caso de existir relación a un registro en la operación
    """

    _dbTable = dbc01.tcfdi_conceptos

    # En estos formularios para solo ver sólo se necesitará estas acciones.
    _L_formButtons = [
        request.stv_fwk_permissions.btn_none,
        request.stv_fwk_permissions.btn_view,
        request.stv_fwk_permissions.btn_findall,
        request.stv_fwk_permissions.btn_cancel
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tcfdis,
        xFieldLinkMaster = [_dbTable.cfdi_id],
        L_visibleButtons = _L_formButtons,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.noidentificacion,
            _dbTable.descripcion,
            _dbTable.cantidad,
            _dbTable.valorunitario,
            _dbTable.importe,
            _dbTable.descuento,
            _dbTable.prodserv_id,
            _dbTable.unidad_id,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.claveprodserv,
            _dbTable.noidentificacion
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            _dbTable.noidentificacion
            ],
        )

    _D_returnVars = _O_cat.process()

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_concepto_impuestostrasladados,
        s_url = URL(
            f = 'vercfdi_concepto_impuestostrasladados', args = request.args[:2] + [
                'master_' + str(request.function), request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'vercfdi_concepto_impuestostrasladados', s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_concepto_impuestosretenidos,
        s_url = URL(
            f = 'vercfdi_concepto_impuestosretenidos', args = request.args[:2] + [
                'master_' + str(request.function), request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'vercfdi_concepto_impuestosretenidos', s_masterIdentificator = str(request.function)
            )
        )

    return (_D_returnVars)


def vercfdi_concepto_impuestostrasladados():
    """ Formulario detalle genérico para ver los impuestos trasladados de los conceptos del cfdi.

    Args:
        arg0: master_[nombre función maestro]
        arg1: cfdi_id
        arg2: master_[nombre función maestro]
        arg3: cfdi_concepto_id
        [arg4]: operación a realizar en el formulario
        [arg5]: id en caso de existir relación a un registro en la operación
    """

    _dbTable = dbc01.tcfdi_concepto_impuestostrasladados

    # En estos formularios para solo ver sólo se necesitará estas acciones.
    _L_formButtons = [
        request.stv_fwk_permissions.btn_none,
        request.stv_fwk_permissions.btn_view,
        request.stv_fwk_permissions.btn_findall,
        request.stv_fwk_permissions.btn_cancel
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tcfdi_conceptos,
        xFieldLinkMaster = [_dbTable.cfdi_concepto_id],
        L_visibleButtons = _L_formButtons,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.base,
            _dbTable.impuesto,
            _dbTable.impuesto_id,
            _dbTable.tipofactor,
            _dbTable.tasaocuota,
            _dbTable.importe,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [_dbTable.id],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        x_offsetInArgs = request.args[:2]
        )

    _D_returnVars = _O_cat.process()

    return (_D_returnVars)



def vercfdi_concepto_impuestosretenidos():
    """ Formulario detalle genérico para ver los impuestos retenidos de los conceptos del cfdi.

    Args:
        arg0: master_[nombre función maestro]
        arg1: cfdi_id
        arg2: master_[nombre función maestro]
        arg3: cfdi_concepto_id
        [arg4]: operación a realizar en el formulario
        [arg5]: id en caso de existir relación a un registro en la operación
    """

    _dbTable = dbc01.tcfdi_concepto_impuestosretenidos

    # En estos formularios para solo ver sólo se necesitará estas acciones.
    _L_formButtons = [
        request.stv_fwk_permissions.btn_none,
        request.stv_fwk_permissions.btn_view,
        request.stv_fwk_permissions.btn_findall,
        request.stv_fwk_permissions.btn_cancel
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tcfdi_conceptos,
        xFieldLinkMaster = [_dbTable.cfdi_concepto_id],
        L_visibleButtons = _L_formButtons,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.base,
            _dbTable.impuesto,
            _dbTable.impuesto_id,
            _dbTable.tipofactor,
            _dbTable.tasaocuota,
            _dbTable.importe,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [_dbTable.id],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        x_offsetInArgs = request.args[:2]
        )

    _D_returnVars = _O_cat.process()

    return (_D_returnVars)



def vercfdi_relacionados():
    """ Formulario detalle genérico para ver los documentos relacionados del cfdi

    Args:
        arg0: master_[nombre función maestro]
        arg1: cfdi_id
        [arg2]: operación a realizar en el formulario
        [arg3]: id en caso de existir relación a un registro en la operación
    """

    _dbTable = dbc01.tcfdi_relacionados

    # En estos formularios para solo ver sólo se necesitará estas acciones.
    _L_formButtons = [
        request.stv_fwk_permissions.btn_none,
        request.stv_fwk_permissions.btn_view,
        request.stv_fwk_permissions.btn_findall,
        request.stv_fwk_permissions.btn_cancel
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tcfdis,
        xFieldLinkMaster = [_dbTable.cfdi_id],
        L_visibleButtons = _L_formButtons,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.uuid,
            _dbTable.cfdirelacionado_id,
            ],
        L_fieldsToSearch = [
            _dbTable.uuid
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            _dbTable.uuid
            ],
        )

    _D_returnVars = _O_cat.process()

    return (_D_returnVars)



def vercfdi_pagos():
    """ Formulario detalle genérico para ver los pagos del cfdi

    Args:
        arg0: master_[nombre función maestro]
        arg1: cfdi_id
        [arg2]: operación a realizar en el formulario
        [arg3]: id en caso de existir relación a un registro en la operación
    """

    _dbTable = dbc01.tcfdi_complementopagos

    # En estos formularios para solo ver sólo se necesitará estas acciones.
    _L_formButtons = [
        request.stv_fwk_permissions.btn_none,
        request.stv_fwk_permissions.btn_view,
        request.stv_fwk_permissions.btn_findall,
        request.stv_fwk_permissions.btn_cancel
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tcfdis,
        xFieldLinkMaster = [_dbTable.cfdi_id],
        L_visibleButtons = _L_formButtons,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.versionpago,
            _dbTable.fechapago,
            _dbTable.formapago_id,
            _dbTable.monedacontpaqi_id,
            _dbTable.tipocambio,
            _dbTable.monto,
            ],
        L_fieldsToSearch = [
            _dbTable.versionpago,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            _dbTable.versionpago
            ],
        )

    _D_returnVars = _O_cat.process()

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tcfdi_complementopago_doctosrelacionados,
        s_url = URL(
            f = 'vercfdi_pago_relacionados', args = request.args[:2] + ['master_' + str(request.function),
                                                                        request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(f = 'vercfdi_pago_relacionados', s_masterIdentificator = str(request.function))
        )

    return (_D_returnVars)


def vercfdi_pago_relacionados():
    """ Formulario detalle genérico para ver los documentos relacionados de los pagos del cfdi

    Args:
        arg0: master_[nombre función maestro]
        arg1: cfdi_id
        arg2: master_[nombre función maestro]
        arg3: cfdi_complementopago_id
        [arg4]: operación a realizar en el formulario
        [arg5]: id en caso de existir relación a un registro en la operación
    """

    _dbTable = dbc01.tcfdi_complementopago_doctosrelacionados

    # En estos formularios para solo ver sólo se necesitará estas acciones.
    _L_formButtons = [
        request.stv_fwk_permissions.btn_none,
        request.stv_fwk_permissions.btn_view,
        request.stv_fwk_permissions.btn_findall,
        request.stv_fwk_permissions.btn_cancel
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tcfdi_complementopagos,
        xFieldLinkMaster = [_dbTable.cfdi_complementopago_id],
        L_visibleButtons = _L_formButtons,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.cfdirelacionado_id,
            _dbTable.empresa_serie_id,
            _dbTable.folio,
            _dbTable.monedacontpaqi_id,
            _dbTable.tipocambio,
            _dbTable.metodopago_id,
            _dbTable.numparcialidad,
            _dbTable.impsaldoant,
            _dbTable.imppagado,
            _dbTable.impsaldoinsoluto,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [_dbTable.id],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        x_offsetInArgs = request.args[:2]
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


# TODO: Cambiar el nombre de este método.
def cfdi_nocontabilizar_cajaschicas():
    """ Relación de CFDIs sin contabilizar por caja chica

    @descripcion Estados de Cuenta

    @keyword arg0: Empresa
    @keyword arg1: empresa_id
    @keyword [arg2]: codigo de funcionalidad
    @keyword [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso btn_special_permisson_01

    """

    _s_empresa_id = request.args(1, None)
    _s_action = request.args(2, request.stv_fwk_permissions.btn_none.code)
    _s_multi_id = request.args(3, '0-0-0-0')  # Se ponen 4 elementos separados '-' por default.
    _L_multi_id = _s_multi_id.split('-')
    if _L_multi_id[0] == 'None':
        _s_cajachica_id = None
    else:
        _s_cajachica_id = _L_multi_id[0]

    _dbTable = dbc01.tcfdis

    _s_empresa_id = request.args(1, None)
    _s_action = request.args(2, request.stv_fwk_permissions.btn_none.code)

    _D_overrideView = None

    _qry = (
            (dbc01.tcfdis.etapa.belongs(TCFDIS.E_ETAPA.NO_CONTABILIZAR, TCFDIS.E_ETAPA.NO_CONTABILIZAR_CANCELADO))
            & (dbc01.tcfdis.empresa_id == _s_empresa_id)
        )

    _dbField_calculado = dbc01.tcfdis.id.count()
    dbc01.tempresa_plaza_sucursal_cajaschicas.empresa_plaza_sucursal_id.readable = True
    dbc01.tcfdis.dia.readable = True

    _L_fieldsToShow = [
        _dbTable.id,
        dbc01.tempresa_plaza_sucursal_cajaschicas.id,
        dbc01.tcfdis.dia,
        dbc01.tempresa_plaza_sucursal_cajaschicas.empresa_plaza_sucursal_id,
        dbc01.tempresa_plaza_sucursal_cajaschicas.nombre,
        _dbField_calculado.with_alias('Total_cancelados')
        ]

    _L_GroupBy = [
        _dbTable.id,
        dbc01.tempresa_plaza_sucursal_cajaschicas.id,
        _dbTable.dia,
        dbc01.tempresa_plaza_sucursal_cajaschicas.empresa_plaza_sucursal_id,
        dbc01.tempresa_plaza_sucursal_cajaschicas.nombre
        ]

    _L_OrderBy = [
        dbc01.tcfdis.dia,
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_findall,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToGroupBy = _L_GroupBy,
        L_fieldsToSearch = [
            _dbTable.id,
            ],
        L_fieldsToSearchIfDigit = [_dbTable.id, dbc01.tcfdis.dia],
        L_fieldnameIds = [dbc01.tempresa_plaza_sucursal_cajaschicas.id, _dbTable.dia, _dbTable.id],
        x_offsetInArgs = request.args[:2]
        )

    _O_cat.addRowSearchResultsLeftJoin(
        _dbTable.on(
            (_dbTable.empresa_plaza_sucursal_cajachica_id == dbc01.tempresa_plaza_sucursal_cajaschicas.id)
            )
        )

    _O_cat.addRowSearchResultsProperty('orderBy', _L_OrderBy)

    _O_cat.addSubTab(
        s_tabName = "Ingresos no contabilizados",
        s_url = URL(f = 'cfdi_nocontabilizar_ingresos',
            args = request.args[:2] + ['master_' + str(request.function), request.args(3)]),
        s_idHtml = fn_createIDTabUnique(f = 'cfdi_nocontabilizar_ingresos', s_masterIdentificator = str(request.function))
        )

    if _s_action in (request.stv_fwk_permissions.btn_view.code,):

        _dbField_fecha = Field(
            'fecha',
            'date',
            default = None,
            required = False,
            notnull = False,
            widget = stv_widget_inputDate,
            label = ('Fecha'),
            comment = '',
            writable = False,
            readable = True,
            represent = stv_represent_date
            )

        _dbRow = dbc01.tempresa_plaza_sucursal_cajaschicas(_s_cajachica_id)
        if _dbRow:
            pass
        else:
            _dbRow = Storage(
                id = None,
                nombre = "No definida"
                )

        _dbRow.fecha = datetime.datetime(int(_L_multi_id[1]), int(_L_multi_id[2]), int(_L_multi_id[3]))

        _D_returnVars = _O_cat.process(
            b_skipAction = True,
            L_formFactoryFields = [
                dbc01.tempresa_plaza_sucursal_cajaschicas.id,
                dbc01.tempresa_plaza_sucursal_cajaschicas.nombre,
                _dbField_fecha
                ],
            # TODO se debe incluir el campo de fecha
            dbRow_formFactory = _dbRow  # TODO el row 1debe contener los datos en L_formFactoryFields
            )
    else:
        _D_returnVars = _O_cat.process(D_overrideView = None)

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def cfdi_nocontabilizar_ingresos():
    """ Relación de CFDIs que no se quieren contabilizar para una caja chica por concepto de ingresos

    @descripcion No Contabilizar Ingresos

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Caja chica y Fecha
    @keyword arg3: empresa_plaza_sucursal_cajachica_id - Año - Mes - Día.
    @keyword [arg4]: operación a realizar en el formulario.
    @keyword [arg5]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    #@permiso btn_new
    #@permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    @permiso btn_view
    #@permiso btn_invalidate
    #@permiso btn_validate
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    #@permiso btn_signature
    @permiso special_permisson_01:[fas fa-file-code] Ignorar movimiento
    """

    _dbTable = dbc01.tcfdis

    _s_empresa_id = request.args(1, None)
    _s_multi_id = request.args(3, '0-0-0-0')  # Se ponen 4 elementos separados '-' por default.
    _L_multi_id = _s_multi_id.split('-')
    if _L_multi_id[0] == 'None':
        _s_cajachica_id = None
    else:
        _s_cajachica_id = _L_multi_id[0]

    _s_action = request.args(4, request.stv_fwk_permissions.btn_none.code)

    _dt_fecha = datetime.datetime(int(_L_multi_id[1]), int(_L_multi_id[2]), int(_L_multi_id[3]))
    # Se generan las fechas en formato datetime para todo el rango del día
    _dt_fechaDesde = _dt_fecha.replace(hour = 0, minute = 0, second = 0, microsecond = 0)
    _dt_fechaHasta = _dt_fechaDesde + datetime.timedelta(days = 1)

    # Se crea el Query para identificar si el CFDI está como "sin contabilizar" y filstrando SOLO por CFDI que sea de tipo ingreso.
    _qry = (
            (_dbTable.empresa_id == _s_empresa_id)
            & (dbc01.tempresas.id == _dbTable.empresa_id)
            & (_dbTable.fecha >= dbc01.tempresas.fecha_inicial)
            & (_dbTable.etapa.belongs(TCFDIS.E_ETAPA.NO_CONTABILIZAR, TCFDIS.E_ETAPA.NO_CONTABILIZAR_CANCELADO))
            & (_dbTable.rol == TCFDIS.E_ROL.EMISOR)
            & (_dbTable.empresa_plaza_sucursal_cajachica_id == _s_cajachica_id)
            & (_dbTable.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO)
            & (_dbTable.fecha >= _dt_fechaDesde)
            & (_dbTable.fecha < _dt_fechaHasta)
        )

    _L_fieldsToShow = [
        _dbTable.id,
        #   _dbTable.rol,
        _dbTable.empresa_serie_id,
        _dbTable.folio,
        _dbTable.fecha,
        _dbTable.metodopago_id,
        _dbTable.formapago_id,
        _dbTable.total,
        _dbTable.receptorrfc,
        _dbTable.receptornombrerazonsocial
        ]

    _L_OrderBy = [
        _dbTable.fecha,
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_findall
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = [
            _dbTable.id,
            ],
        L_fieldsToSearchIfDigit = [_dbTable.id],
        L_fieldnameIds = [_dbTable.id],
        x_offsetInArgs = request.args[:4]
        )

    #     _O_cat.addRowSearchResultsLeftJoin(
    #         dbc01.tcfdi_registropagos.on(
    #             (dbc01.tcfdi_registropagos.cfdi_id == _dbTable.id)
    #             )
    #         )

    _O_cat.addRowSearchResultsProperty('orderBy', _L_OrderBy)

    # Se agrega el boton especial 01 que funciona para mandar el CFDI a CFDI no contabilizable.
    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionLabel = "No contabilizar CFDI",
        s_permissionIcon = "fas fa-window-close",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
            confirm = Storage(
                s_title = "Favor de confirmar operación",
                s_text = "¿Estás seguro de querer marcar este CFDI como 'No contabilizar'?",
                s_type = "warning",
                s_btnconfirm = "Confirmar",
                s_btncancel = "Cancelar",
                )
            ),
        L_options = []
        )


    # Código para poder jalar la vista de CFDIS y utilizarlas en esta función.
    if _s_action in (request.stv_fwk_permissions.btn_view.code,):
        from gluon.storage import List

        _s_cfdi_id = str(request.args(5, 0))  # Se guarda el argumento que se quiere utilizar dentro de una variable.
        request.args = List()
        request.args.append(_s_action)  # Se mete _s_action dentro de la lista con la función append.
        request.args.append(_s_cfdi_id)

        _D_returnVars = cfdisemitidos()
        # Usar el os.path.join para evitar conflicto por la diagonal (/) en sistemas operativos.
        response.view = os.path.join('250cfdi', 'cfdisemitidos.html')

    elif _s_action in (request.stv_fwk_permissions.special_permisson_01.code,):

        _s_cfdi_id = str(request.args(5, 0))  # Se guarda el argumento que se quiere utilizar dentro de una variable

        _dbRowCfdi = dbc01.tcfdis(_s_cfdi_id)

        if _dbRowCfdi.etapa != TCFDIS.E_ETAPA.NO_CONTABILIZAR_CANCELADO:

            _dbRowCfdi.etapa = TCFDIS.E_ETAPA.SIN_CONTABILIZAR
            _dbRowCfdi.update_record()
            stv_flashSuccess(
                "Cambio de estatus del CFDI",
                "Cambio hecho correctamente."
                )

            _D_overrideView = request.stv_fwk_permissions.view_refresh

            _D_returnVars = _O_cat.process(D_overrideView = _D_overrideView)

        else:
            stv_flashError(
                "Cambio de estatus del CFDI",
                "El cambio del CFDI no puede ser realizado debido a que tiene etapa: 'No contabilizar cancelado,"
                " favor de hablar con un administrador.'"
                )
    else:
        _D_returnVars = _O_cat.process(D_overrideView = None)

    return _D_returnVars


def obtenerPrepoliza(s_cfdi_id, sat_status_cancelacion):
    """ Función para obtener la prepoliza cancelada o de ingreso

            Params:
                s_cfdi_id: Id del CFDI.
                stat_status_cancelacion: Estatus de la cancelación para la busqueda de la prepoliza.
    """

    _D_return = Storage(
        s_msgError = "",
        E_return = CLASS_e_RETURN.NOK_ERROR,
        dbRowCfdiPrepoliza = None
        )

    _dbRowCfdiPrepoliza = dbc01(
        (dbc01.tcfdi_prepolizas.cfdi_id == s_cfdi_id)
        & (dbc01.tcfdi_prepolizas.sat_status_cancelacion == sat_status_cancelacion)
        & (dbc01.tcfdis.id == dbc01.tcfdi_prepolizas.cfdi_id)
        & (dbc01.tpolizas.id == dbc01.tcfdi_prepolizas.poliza_id)
        ).select(
            dbc01.tcfdi_prepolizas.ALL,
            dbc01.tcfdis.ALL,
            dbc01.tpolizas.ALL
            )

    if _dbRowCfdiPrepoliza:
        _D_return.dbRowCfdiPrepoliza = _dbRowCfdiPrepoliza
        _D_return.E_return = CLASS_e_RETURN.OK
    else:
        _D_return.s_msgError = "No se encontró información de la prepoliza."

    return _D_return
