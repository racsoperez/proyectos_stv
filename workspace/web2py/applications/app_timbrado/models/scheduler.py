# -*- coding: utf-8 -*-

def calcular_saldo_clientes(*pargs, **pvars):
    _D_results = FUNC_RETURN()
    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = pargs,
        n_offsetArgs = 2,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = pargs[1] if len(pargs) > 1 else 0,
        s_cliente_id = pargs[3] if len(pargs) > 3 else None,
        )

    pvars = Storage(pvars)
    _cfg_cuenta_aplicacion_id = int(pvars.cfg_cuenta_aplicacion_id) \
        if str(pvars.cfg_cuenta_aplicacion_id).isdigit() else None
    _cfg_cuenta_id = int(pvars.cfg_cuenta_id) if str(pvars.cfg_cuenta_id).isdigit() else None

    if _cfg_cuenta_aplicacion_id or _cfg_cuenta_id:
        importar_dbs_cuenta(_cfg_cuenta_aplicacion_id, _cfg_cuenta_id)

    else:
        raise RuntimeError("No se pudo determinar la cuenta a utilizar")

    _dbTabla = dbc01.tempresa_clientes

    if _D_args.s_accion == request.stv_fwk_permissions.special_permisson_03.code:
        _D_results = TCFDIS.EDOCTA(
            _D_args.s_empresa_id,
            _D_args.s_cliente_id,
            E_movto_generado_por = TCFDI_MOVIMIENTOS.E_GENERADO_POR.RECALCULO_CLIENTE
            ).procesar_cliente()

        _D_insert_cliente_log = Storage(
            empresa_cliente_id = _D_args.s_cliente_id,
            fecha = request.browsernow,
            descripcion = "Cálculo de saldo manual 2",
            tipotransaccion = TEMPRESA_CLIENTE_LOG.TIPOTRANSACCION.CALCULODESALDO,
            errores = "\n".join(_D_results.L_msgError),
            warnings = "\n".join(_D_results.L_msgWarning),
            info = "\n".join(_D_results.L_msgInfo),
            resultado = _D_results.E_return
            )

        dbc01.tempresa_cliente_log.insert(**_D_insert_cliente_log)

    elif _D_args.s_accion == request.stv_fwk_permissions.special_permisson_04.code:

        _dbRows_clientes = dbc01(
            (_dbTabla.empresa_id == _D_args.s_empresa_id)
            ).select(_dbTabla.id)

        _O_tarea = SCHD_CONTROL_TAREA(s_nombreFuncion = 'calcular_saldo_clientes')

        # Se graba en p6 la cantidad de registros a procesar
        _O_tarea.actualizar_datos_ultimaTarea(n_param6 = len(_dbRows_clientes))
        _n_index = 0

        for _dbRow in _dbRows_clientes:

            _n_index += 1

            _D_results = TCFDIS.EDOCTA(
                _D_args.s_empresa_id,
                _dbRow.id,
                E_movto_generado_por = TCFDI_MOVIMIENTOS.E_GENERADO_POR.RECALCULO_CLIENTE
                ).procesar_cliente()

            if _D_results.E_return > CLASS_e_RETURN.OK:
                # Se graba en n_param7 el número de cliente
                _O_tarea.actualizar_datos_ultimaTarea(
                    n_param7 = _n_index,
                    t_param8 = "Cliente %d: %s \n" % (_dbRow.id, "; ".join(_D_results.L_msgError))
                    )

            else:
                # Se graba en n_param7 el número de cliente
                _O_tarea.actualizar_datos_ultimaTarea(n_param7 = _n_index)

            _D_insert_cliente_log = Storage(
                empresa_cliente_id = _dbRow.id,
                fecha = request.browsernow,
                descripcion = "Cálculo de saldo clientes global",
                tipotransaccion = TEMPRESA_CLIENTE_LOG.TIPOTRANSACCION.CALCULODESALDO,
                errores = "\n".join(_D_results.L_msgError),
                warnings = "\n".join(_D_results.L_msgWarning),
                info = "\n".join(_D_results.L_msgInfo),
                resultado = _D_results.E_return
                )

            dbc01.tempresa_cliente_log.insert(**_D_insert_cliente_log)

            dbc01.commit()

    else:
        pass

    return _D_results

