# -*- coding: utf-8 -*-

""" Este archivo contiene las funciones para las conexiones a las bases de datos.


"""

def importar_dbs(n_empresa_id):
    """Función para importar las base de datos por cuenta utilizando la empresa_id. 
        
        Params:
            n_empresa_id: ID de la empresa.
    """
    
    _dbRows_cuentas = db(
        db.tcuenta_empresas.empresa_id == n_empresa_id
        ).select(
            db.tcuenta_empresas.ALL
            )
    
    if(_dbRows_cuentas):
        _dbRow_cuenta = _dbRows_cuentas.first()
        importar_dbs_cuenta(s_remplaza_cuenta_id = _dbRow_cuenta.cuenta_id)
    else:
        pass
    
    return
