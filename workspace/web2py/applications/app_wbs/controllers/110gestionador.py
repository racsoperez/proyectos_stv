# -*- coding: utf-8 -*-
# noinspection SpellCheckingInspection

class GestionadorInstrucciones:
    """ Gestión y uso común de instrucciones en el servidor

    Ejemplos de uso:
        https://p01.stverticales.mx/app_wbs/110gestionador/call/soap?WSDL
        http://127.0.0.1:8000/app_wbs/110gestionador/call/soap?WSDL

    """

    # Define si es posible o no generar el log
    CFG_GENERAR_REGISTRO_CONEXION = False

    # Define si debe actualizar el estatus de conexión del servidor, si esta o no conectado a un cliente
    CFG_ACTUALIZAR_ESTATUS_CONEXION = False

    # Código que definirá la acción a realizar por cada registro
    class E_TIPOINSTRUCCION(TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION):
        pass
    
    def leer_servidor_id(self):
        return self._n_servidor_id

    def __init__(
            self,
            dt_request_utc,
            n_usuario_id,
            ):
        """ Inicializa la clase para determinar los parámetros a utilizar en las instrucciones
        
        @param dt_request_utc: corresponde a la fecha/hora del request en formato utc.
        @param n_usuario_id: define el id del usuario a utilizar en la clase.

        """
        
        self._n_usuario_id = n_usuario_id
        self._dt_request_utc = dt_request_utc
        self._L_instrucciones = []
        self._n_servidor_id = None
        self._dbRow_servidor = None
        self._dt_request_browser = TZONASHORARIAS.CONVERTIR_FECHAHORA_DEUTC_AZONAH(
            STV_FWK_APP.n_zonahoraria_id, self._dt_request_utc
            )
        
        return

    def identificar_servidor(
            self,
            s_codigo = "",
            s_instruccion_id = None,
            b_configurarHoraServidor = False,
            ):
        """ Identifica el server_id usando diferentes condiciones

        @param s_codigo: en caso de que servidor_id no este especificado, se utiliza este parámetro
            para localizar el servidor_id.
        @type s_codigo:
        @param s_instruccion_id: en caso de que ni servidor_id o instruccion_id esten especificados,
            se utilizar este parámetro para localizar el servidor_id.
        @type s_instruccion_id:
        @param b_configurarHoraServidor: Configura la fecha/hora especificada en el servidor para
            consultas e inserciones. Normalmente se utiliza en caso de llamadas desde web services.
        @type b_configurarHoraServidor:
        @return:
        @rtype:
        """

        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        if s_codigo:
            _dbQry_servidor = (
                (db.tservidores.codigo == s_codigo)
                & (db.tservidores.serveruser_id == self._n_usuario_id)
                )

        elif s_instruccion_id:
            _dbQry_servidor = (
                (db.tservidor_instrucciones.id == s_instruccion_id)
                & (db.tservidores.id == db.tservidor_instrucciones.servidor_id)
                & (db.tservidores.serveruser_id == self._n_usuario_id)
                )

        else:
            _dbQry_servidor = None

        if _dbQry_servidor:
            _dbRows_servidor = db(
                _dbQry_servidor
                & (db.tservidores.gestionador == True)
                & (db.tservidores.bansuspender == False)
                ).select(
                    db.tservidores.ALL,
                    orderby = ~db.tservidores.id
                    )

            if len(_dbRows_servidor) >= 1:
                self._dbRow_servidor = _dbRows_servidor.first()
                self._n_servidor_id = self._dbRow_servidor.id

                if b_configurarHoraServidor:
                    # Se actualiza la zona horaria de acuerdo al servidor
                    STV_FWK_APP.DEFINIR_ZONAHORARIA(
                        self._dbRow_servidor.zonahoraria_id, STV_FWK_APP.E_ZONAHORARIA_DEFINIDOR.SERVIDOR
                        )
                    # Como se va a usar la zona horaria del servidor, se actualiza la hora esperada en el browser
                    request.browsernow = TZONASHORARIAS.CONVERTIR_FECHAHORA_DEUTC_AZONAH(
                        STV_FWK_APP.n_zonahoraria_id, request.utcnow
                        )
                    # Se actualiza la fecha hora del request a la hora en el browser
                    self._dt_request_browser = TZONASHORARIAS.CONVERTIR_FECHAHORA_DEUTC_AZONAH(
                        STV_FWK_APP.n_zonahoraria_id, self._dt_request_utc
                        )
                else:
                    pass

            else:
                _D_return.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
                _D_return.s_msgError = u"El servidor no pudo localizarse con los parámetros especificados"
        else:
            _D_return.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
            _D_return.s_msgError = u"El servidor no pudo localizarse"

        return _D_return

    def conectar(
            self,
            s_webservice,
            s_notas = ""
            ):
        """ Crea los registros de conexión y actualiza el status del servidor

        @param s_webservice:
        @type s_webservice:
        @param s_notas:
        @type s_notas:
        @return:
        @rtype:
        """
        if GestionadorInstrucciones.CFG_GENERAR_REGISTRO_CONEXION:
            db.tservidor_conexiones.insert(
                servidor_id = self._n_servidor_id,
                aplicacion_w2p = request.application,
                controlador_w2p = request.controller,
                funcion_w2p = s_webservice,
                argumentos = str(request.args),
                fecha = TZONASHORARIAS.CONVERTIR_FECHAHORA_DEUTC_AZONAH(STV_FWK_APP.n_zonahoraria_id, request.utcnow),
                # Fix para poder registrar la fecha utc en la base de datos
                creado_por = auth.user_id,
                notas = s_notas
                )
        else:
            pass

        if GestionadorInstrucciones.CFG_ACTUALIZAR_ESTATUS_CONEXION:
            db(
                db.tservidores.id == self._n_servidor_id
                ).update(
                    sesionesactivas = db.tservidores.sesionesactivas + 1
                    )
        else:
            pass
        return

    def desconectar(
            self,
            s_webservice,
            s_notas = ""
            ):
        if GestionadorInstrucciones.CFG_GENERAR_REGISTRO_CONEXION:
            db.tservidor_conexiones.insert(
                servidor_id = self._n_servidor_id,
                aplicacion_w2p = request.application,
                controlador_w2p = request.controller,
                funcion_w2p = s_webservice,
                argumentos = str(request.args),
                fecha = TZONASHORARIAS.CONVERTIR_FECHAHORA_DEUTC_AZONAH(STV_FWK_APP.n_zonahoraria_id, request.utcnow),
                # Fix para poder registrar la fecha utc en la base de datos
                creado_por = auth.user_id,
                notas = s_notas
                )
        else:
            pass

        if GestionadorInstrucciones.CFG_ACTUALIZAR_ESTATUS_CONEXION:
            db(
                (db.tservidores.serveruser_id == self._n_usuario_id)
                & (db.tservidores.sesionesactivas >= 1)
                ).update(
                    sesionesactivas = db.tservidores.sesionesactivas - 1
                    )
        else:
            pass
        return

    def obtener_instrucciones(self):
        
        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            L_instrucciones = []
            )
        
        for _D_instruccion in self._L_instrucciones:
            _D_return.L_instrucciones.append(
                Storage(
                    s_id = _D_instruccion.s_id,
                    E_tipoinstruccion = _D_instruccion.E_tipoinstruccion,
                    s_param0 = _D_instruccion.s_param0,
                    s_param1 = _D_instruccion.s_param1,
                    s_param2 = _D_instruccion.s_param2,
                    s_param3 = _D_instruccion.s_param3,
                    dt_param4 = _D_instruccion.dt_param4.isoformat() if _D_instruccion.dt_param4 else None,
                    dt_param5 = _D_instruccion.dt_param5.isoformat() if _D_instruccion.dt_param4 else None,
                    s_param6 = _D_instruccion.s_param6,
                    s_param7 = _D_instruccion.s_param7,
                    )
                )
        
        return _D_return  

    def grabar_instrucciones(self):
        
        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        if not self._n_usuario_id:
            _D_return.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
            _D_return.s_msgError = u"El usuario debe estar definido para obtener las instrucciones del servidor"

        elif not self._n_servidor_id:
            _D_return.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
            _D_return.s_msgError = u"El servidor no esta definido o su código no esta especificado."
        
        else:
            for _D_instruccion in self._L_instrucciones:
                if _D_instruccion.dbRow:
                    
                    # En cualquier instruccion actualizar el dato de fecha final
                    _D_instruccion.dbRow.dt_param5 = _D_instruccion.dt_param5
                    _D_instruccion.dbRow.estado = TSERVIDOR_INSTRUCCIONES.E_ESTADO.EN_PROCESO
                    if not _D_instruccion.s_rastro:
                        _D_instruccion.s_rastro = "Se reproceso la instrucción"
                    else:
                        pass
                    _D_instruccion.dbRow.rastro += (
                        self._dt_request_utc.isoformat() + "UTC: " + _D_instruccion.s_rastro + "\n"
                        )
                    del _D_instruccion.dbRow.editado_en
                    del _D_instruccion.dbRow.editado_por  # Se borra para poder actualizar el campo editado_en
                    _D_instruccion.dbRow.update_record()

                else:
                    
                    _D_instruccionRow = Storage(
                        servidor_id = self._n_servidor_id,
                        cuenta_id = _D_instruccion.dbRow_cuenta.id,
                        empresa_id = _D_instruccion.dbRow_empresa.id,
                        user_id = self._n_usuario_id,
                        fecha = self._dt_request_browser,
                        tipoinstruccion = _D_instruccion.E_tipoinstruccion,
                        servicio = TSERVIDOR_INSTRUCCIONES.E_SERVICIO.GESTIONADOR,
                        estado = TSERVIDOR_INSTRUCCIONES.E_ESTADO.EN_PROCESO,
                        s_param0 = _D_instruccion.s_param0,
                        s_param1 = _D_instruccion.s_param1,
                        s_param2 = _D_instruccion.s_param2,
                        s_param3 = _D_instruccion.s_param3,
                        dt_param4 = _D_instruccion.dt_param4,
                        dt_param5 = _D_instruccion.dt_param5,
                        s_param6 = _D_instruccion.s_param6,
                        s_param7 = _D_instruccion.s_param7,
                        rastro = self._dt_request_utc.isoformat() + "UTC: " + _D_instruccion.s_rastro + "\n",
                        ultimo_resultado = None,
                        ultimo_mensaje = "",
                        contador_errores = 0,
                        es_autoactualizable = _D_instruccion.es_autoactualizable,
                        creado_por = auth.user_id,
                        editado_por = auth.user_id
                        )
                    _D_instruccion.s_id = db.tservidor_instrucciones.insert(**_D_instruccionRow)
        
        return _D_return  

    def preparar_instrucciones(
            self,
            b_actualizarFechaHasta = True
            ):
        """ Prepara las instrucciones al servidor.
        
        @param b_actualizarFechaHasta: Especifica si se debe actualizar la fecha final de las instrucciones continuas.
         Toma en cuenta si toca actualizar la instrucción.
        """

        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )        

        if not self._n_usuario_id:
            _D_return.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
            _D_return.s_msgError = u"El usuario debe estar definido para obtener las instrucciones del servidor"
        elif not self._dbRow_servidor:
            _D_return.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
            _D_return.s_msgError = u"El servidor no esta definido, esta suspendido o su código no esta especificado."
        else:
                
            # Se obtienen las instrucciones activas y se guardan en un arreglo local
            self._obtener_inst_pendientes()
            
            if b_actualizarFechaHasta:
                
                # Obtener información en _dbRows_cuentas_empresas_ids del servidor, las cuenta y sus empresas
                #  relacionadas que conectan con el mismo servidor s_codigo y el usuario registrado.
                _dbRows_cuentas_empresas_ids = db(
                    (db.tcuentas.servidorgestionador_id == self._n_servidor_id)
                    & (db.tcuentas.bansuspender == False)
                    ).select(
                        db.tcuentas.id,
                        db.tcuentas.nombrecorto,
                        db.tcuentas.idintegrador,
                        db.tcuentas.rfcintegrador,
                        db.tcuenta_empresas.empresa_id,
                        db.tcuenta_empresas.id,
                        db.tcuentas.frecuenciagestionador_cfdis,
                        db.tcuentas.frecuenciagestionador_cancelados,
                        db.tcuentas.horagestionador_cfdis,
                        db.tcuentas.horagestionador_cancelados,
                        left = [
                            db.tcuenta_empresas.on(
                                (db.tcuenta_empresas.cuenta_id == db.tcuentas.id)
                                & (db.tcuenta_empresas.bansuspender == False)
                                )
                            ],
                        orderby = [
                            db.tcuentas.id,
                            db.tcuenta_empresas.empresa_id
                            ]
                        )
                    
                if not _dbRows_cuentas_empresas_ids:
                    _D_return.E_return = stvfwk2_e_RETURN.NOK_CONDICIONES_INCORRECTAS
                    _D_return.s_msgError = u"No se encontró el servidor relacionado con el usuario."
                
                else:
                    
                    # Crea un tree para facilidad en el manejo de la información
                    _O_tree = DB_Tree(_dbRows_cuentas_empresas_ids)
                    _O_tree.addLevel([db.tcuentas])
                    _O_tree.addLevel([db.tcuenta_empresas])
                    _L_tree = _O_tree.process()
                    
                    for _dbRowParent in _L_tree:
                        # Por cada cuenta se buscarán las empresas
                        
                        if (
                                not _dbRowParent.data.tcuentas.frecuenciagestionador_cfdis
                                and not _dbRowParent.data.tcuentas.frecuenciagestionador_cancelados
                                and not _dbRowParent.data.tcuentas.horagestionador_cfdis
                                and not _dbRowParent.data.tcuentas.horagestionador_cancelados
                                ):
                            # El proceso de instrucciones automáticas esta desabilitado
                            pass
                        
                        else:
                        
                            _L_empresas_ids = []
                            for _dbRowChild in _dbRowParent.subrows:
                                if _dbRowChild.data.tcuenta_empresas.empresa_id:
                                    _L_empresas_ids.append(_dbRowChild.data.tcuenta_empresas.empresa_id)
                                    
                                else:
                                    pass
                                
                            if not _L_empresas_ids:
                                # _D_return.E_return = stvfwk2_e_RETURN.NOK_CONDICIONES_INCORRECTAS
                                # Se permite la opción de no generar instrucciones automaticas si las empresas estan
                                #  suspendidas, y solo se pueden generar de forma manual
                                _D_return.s_msgError = (
                                    u"La cuenta %s[%d] no tiene empresas asociadas para bajar instrucciones "
                                    + u"automaticas."
                                    ) % (_dbRowParent.data.tcuentas.nombrecorto, _dbRowParent.data.tcuentas.id)
                                # TODO: Evaluar manejo de errores para permitir generar las instrucciones de
                                #  otras cuentas. Los errores deben estar drigidos para las cuentas con problemas,
                                #  no suspender todas las cuentas debido al error de una
                            else:
                                
                                # Se preparan las instrucciones por cuenta y sus empresas
                                _D_results = self._preparar_instrucciones_cuenta(
                                    _dbRowParent.data.tcuentas, _L_empresas_ids
                                    )
                                if _D_results.E_return != stvfwk2_e_RETURN.OK:
                                    _D_return.E_return = _D_results.E_return
                                    _D_return.s_msgError = _D_results.s_msgError
                                    # TODO: Mismo TODO anterior
                                    break
                                
                                else:
                                    pass
            else:
                # No se desea actualizar las instrucciones automáticas
                pass
                        
        return _D_return
    
    def agregar_actualizar_cfdis(
            self,
            dbRow_cuenta,
            dbRow_empresa,
            dt_fechaDesde,
            dt_fechaHasta,
            E_tipoInstruccion,
            s_rastro = "",
            ):
        """ Se crea la instruccion en la lista del objeto
        """
        
        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )        

        _D_instruccion = Storage(
            s_id = None,
            E_tipoinstruccion = E_tipoInstruccion,
            s_param0 = dbRow_cuenta.id,
            s_param1 = dbRow_empresa.id,
            s_param2 = dbRow_empresa.rfc,
            s_param3 = dbRow_empresa.claveciec,
            dt_param4 = dt_fechaDesde,
            dt_param5 = dt_fechaHasta,
            s_param6 = dbRow_cuenta.idintegrador,
            s_param7 = dbRow_cuenta.rfcintegrador,
            dbRow = None,
            dbRow_cuenta = dbRow_cuenta,
            dbRow_empresa = dbRow_empresa,
            s_rastro = s_rastro,
            es_autoactualizable = True
            )
        self._L_instrucciones.append(_D_instruccion)
            
        return _D_return  
    
    def _validar_instruccion_actualizar_cfdis(
            self,
            dbRow_cuenta,
            dbRow_empresa,
            E_tipoinstruccion,
            n_frecuencia,
            t_horaEspecifica
            ):
        """ Función que verifica si es momento o no de crear una instrucción nueva de forma automática
        
        """
        
        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            b_crearInstruccion = False
            )
        
        # Se obtienen la instrucción con el último fecha actualizado del tipo definido en el parámetro para saber
        #  cuando corresponde la siguiente
        _dbRows = db(
            (db.tservidor_instrucciones.servidor_id == self._n_servidor_id)
            & (db.tservidor_instrucciones.cuenta_id == dbRow_cuenta.id)
            & (db.tservidor_instrucciones.empresa_id == dbRow_empresa.id)
            & (db.tservidor_instrucciones.servicio == TSERVIDOR_INSTRUCCIONES.E_SERVICIO.GESTIONADOR)
            & (db.tservidor_instrucciones.tipoinstruccion == E_tipoinstruccion)
            & (db.tservidor_instrucciones.estado.belongs(TSERVIDOR_INSTRUCCIONES.E_ESTADO.L_PERMITEN_PROGRAMAR_NUEVA))
            ).select(
                db.tservidor_instrucciones.ALL,
                orderby = [
                    ~db.tservidor_instrucciones.ultimo_fecha
                    ],
                limitby = (0, 1)
                )

        _dt_frecuenciaHoras = n_frecuencia or 0
        _t_horaEjecutar = t_horaEspecifica

        if _dbRows:
            # Si existe esa instrucción, se realiza el cálculo para saber si toca la siguiente instrucción
            
            _dbRow = _dbRows.first()            
            if _t_horaEjecutar:
                if _dbRow.ultimo_fecha < (self._dt_request_browser - datetime.timedelta(days = 1)):
                    _D_return.b_crearInstruccion = True

                elif (
                        (_dbRow.ultimo_fecha.date() + datetime.timedelta(days = 1))
                        == self._dt_request_browser.date()
                        ) \
                        and (self._dt_request_browser.time() > _t_horaEjecutar):
                    _D_return.b_crearInstruccion = True
                
                else:
                    pass
            else:
                pass
                        
            if not _D_return.b_crearInstruccion:
                if _dt_frecuenciaHoras > 0:
                    _dt_siguienteActualizacion = (
                        _dbRow.ultimo_fecha 
                        + datetime.timedelta(hours = _dt_frecuenciaHoras)
                        ) 
                    if _dt_siguienteActualizacion <= _dt_siguienteActualizacion:
                        # OK
                        _D_return.b_crearInstruccion = True
                        
                    else:
                        _D_return.E_return = stvfwk2_e_RETURN.NOK_CONDICIONES_INCORRECTAS
                        _D_return.s_msgError = "No es momento de generar instrucción"
                else:
                    _D_return.E_return = stvfwk2_e_RETURN.NOK_CONDICIONES_INCORRECTAS
                    _D_return.s_msgError = "No es momento de generar instrucción"
            else:
                pass
             
        else:

            if _t_horaEjecutar or (_dt_frecuenciaHoras > 0):
                # Si no se encontraron ninguna instrucción anterior y tiene abilitado algún sistema de programación,
                #  se acepta la creación de la instrucción
                # OK 
                _D_return.b_crearInstruccion = True

            else:
                _D_return.E_return = stvfwk2_e_RETURN.NOK_CONDICIONES_INCORRECTAS
                _D_return.s_msgError = "Generación automática de instrucción desabilitada"
        
        return _D_return

    def _preparar_instrucciones_cuenta(
            self,
            dbRow_cuenta,
            L_empresas_ids = None
            ):
        """ Se obtienen las instrucciones de una cuenta y sus empresas especificadas
        """

        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )
        
        # Importar las dbs correspondientes a la cuenta
        importar_dbs_cuenta(s_remplaza_cuenta_id = dbRow_cuenta.id)
        
        # Fecha fin es la fecha actual y fecha inicio es el principio del mes anterior para cubrir el brinco de mes
        _dt_fechaFin = self._dt_request_browser
        _dt_fechaInicio = (
            _dt_fechaFin - datetime.timedelta(days = (request.browsernow.day + 1))
            ).replace(day = 1, hour = 0, minute = 0, second = 1)
        
        for _s_empresa_id in L_empresas_ids or []:
            
            _dbRow_empresa = dbc01.tempresas(_s_empresa_id)
            _dt_primerDia2000 = datetime.datetime(2000, 1, 1)
            
            if not dbRow_cuenta.frecuenciagestionador_cfdis and not dbRow_cuenta.horagestionador_cfdis:
                # La instrucción para actualizar CFDIs se encuentra desabilitada
                pass 
            else:
                
                # Buscar y actualizar registro de ACTUALIZAR_CFDIS
                # noinspection DuplicatedCode
                _E_tipoInstruccion = TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_CFDIS

                # Se busca la instrucción que pudiera modificarse    
                _n_index_seleccionado = None
                for _n_index, _D_instruccion in enumerate(self._L_instrucciones):
                    if (
                            (_D_instruccion.E_tipoinstruccion == _E_tipoInstruccion)
                            and _D_instruccion.dbRow
                            and (_D_instruccion.dbRow.es_autoactualizable == True)
                            and (_D_instruccion.dbRow.cuenta_id == dbRow_cuenta.id)
                            and (_D_instruccion.dbRow.empresa_id == _s_empresa_id)
                            and (_D_instruccion.dbRow.servicio == TSERVIDOR_INSTRUCCIONES.E_SERVICIO.GESTIONADOR)
                            ):
                        _n_index_seleccionado = _n_index
                    else:
                        pass
                                        
                if _n_index_seleccionado is None:
                    # Si no lo encontro, se procede a crear una instrucción
                    
                    # Se obtienen el último CFDI actualizado en el proceso de timbrado
                    _dbRows_cfdis = dbc01(
                        (dbc01.tcfdis.empresa_id == _s_empresa_id) 
                        & (dbc01.tcfdis.fechatimbrado >= _dt_fechaInicio)
                        & (dbc01.tcfdis.sat_status != TCFDIS.SAT_STATUS.NO_DEFINIDO)
                        # & (dbc01.tcfdis.fechatimbrado <= _dt_fechaFin)
                        ).select(
                            dbc01.tcfdis.fechatimbrado,
                            orderby = [ 
                                ~dbc01.tcfdis.fechatimbrado, 
                                ],
                            limitby = (0, 1),
                            orderby_on_limitby = False
                            )

                    if _dbRows_cfdis:
                        # TODO, verificar si la última instrucción regreso un error, usar la fecha inicial de la
                        #  última instrucción
                        _dt_fechaInicioInstruccion = _dbRows_cfdis.first().fechatimbrado - datetime.timedelta(days = 4)
                    else:
                        _dt_fechaInicioInstruccion = _dt_fechaInicio
                    
                    # Se verifica si es momento o no de crear una instrucción
                    _D_results = self._validar_instruccion_actualizar_cfdis(
                        dbRow_cuenta,
                        _dbRow_empresa,
                        _E_tipoInstruccion,
                        dbRow_cuenta.frecuenciagestionador_cfdis,
                        dbRow_cuenta.horagestionador_cfdis
                        )
                    
                    if _D_results.b_crearInstruccion:                    
                    
                        self.agregar_actualizar_cfdis(
                            dbRow_cuenta = dbRow_cuenta,
                            dbRow_empresa = _dbRow_empresa,
                            dt_fechaDesde = _dt_fechaInicioInstruccion,
                            dt_fechaHasta = _dt_fechaFin,
                            E_tipoInstruccion = _E_tipoInstruccion,
                            s_rastro = "Se inserta la instrucción auto",
                            )
                        
                    else:
                        pass
                    
                else:
                    # Si lo encontró, se actualiza la fecha fin a la fecha actual
                    self._L_instrucciones[_n_index_seleccionado].dt_param5 = _dt_fechaFin
                    self._L_instrucciones[_n_index_seleccionado].s_rastro = "Se actualiza la fecha fin"
                    
            if not dbRow_cuenta.frecuenciagestionador_cancelados and not dbRow_cuenta.horagestionador_cancelados:
                # La instruccion para actualizar CFDIs se encuentra desabilitada
                pass 
            else:
                
                # Buscar y actualizar registro de ACTUALIZAR_CFDIS_CANCELADOS
                # noinspection DuplicatedCode
                _E_tipoInstruccion = TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_CFDIS_CANCELADOS

                # Se busca la instrucción que pudiera modificarse    
                _n_index_seleccionado = None
                for _n_index, _D_instruccion in enumerate(self._L_instrucciones):
                    if (
                            (_D_instruccion.E_tipoinstruccion == _E_tipoInstruccion)
                            and _D_instruccion.dbRow
                            and (_D_instruccion.dbRow.es_autoactualizable == True)
                            and (_D_instruccion.dbRow.cuenta_id == dbRow_cuenta.id)
                            and (_D_instruccion.dbRow.empresa_id == _s_empresa_id)
                            and (_D_instruccion.dbRow.servicio == TSERVIDOR_INSTRUCCIONES.E_SERVICIO.GESTIONADOR)
                            ):
                        _n_index_seleccionado = _n_index
                    else:
                        pass
                                        
                if _n_index_seleccionado is None:
                    # Si no lo encontro, se procede a crear una instrucción
                    
                    # Se obtienen el último CFDI actualizado en el proceso de timbrado
                    _dbRows_cfdis_cancelados = dbc01(
                        (dbc01.tcfdis.empresa_id == _s_empresa_id) 
                        & (dbc01.tcfdis.sat_status == TCFDIS.SAT_STATUS.CANCELADO) 
                        & (dbc01.tcfdis.sat_fecha_cancelacion >= _dt_fechaInicio)
                        # & (dbc01.tcfdis.sat_fecha_cancelacion <= _dt_fechaFin)
                        ).select(
                            dbc01.tcfdis.sat_fecha_cancelacion,
                            orderby = [ 
                                ~dbc01.tcfdis.sat_fecha_cancelacion, 
                                ],
                            limitby = (0, 1),
                            orderby_on_limitby = False
                            )

                    if _dbRows_cfdis_cancelados:
                        _dt_fechaInicioInstruccion = _dbRows_cfdis_cancelados.first().sat_fecha_cancelacion
                    else:
                        _dt_fechaInicioInstruccion = _dt_fechaInicio
                    
                    # Se verifica si es momento o no de crear una instrucción
                    _D_results = self._validar_instruccion_actualizar_cfdis(
                        dbRow_cuenta,
                        _dbRow_empresa,
                        _E_tipoInstruccion,
                        dbRow_cuenta.frecuenciagestionador_cancelados,
                        dbRow_cuenta.horagestionador_cancelados
                        )
                    
                    if _D_results.b_crearInstruccion:                    
                    
                        self.agregar_actualizar_cfdis(
                            dbRow_cuenta = dbRow_cuenta,
                            dbRow_empresa = _dbRow_empresa,
                            dt_fechaDesde = _dt_fechaInicioInstruccion,
                            dt_fechaHasta = _dt_fechaFin,
                            E_tipoInstruccion = _E_tipoInstruccion,
                            s_rastro = "Se inserta la instrucción auto",
                            )
                        
                    else:
                        pass
                    
                else:
                    # Si lo encontró, se actualiza la fecha fin a la fecha actual
                    self._L_instrucciones[_n_index_seleccionado].dt_param5 = _dt_fechaFin
                    self._L_instrucciones[_n_index_seleccionado].s_rastro = "Se actualiza la fecha fin"       
                    
        return _D_return  

    def _obtener_inst_pendientes(
            self,
            ):
        
        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )
        
        # Se obtienen instrucciones de Gestionador
        _dbRows = db(
            (db.tservidor_instrucciones.servidor_id == self._n_servidor_id)
            & (db.tservidor_instrucciones.servicio == TSERVIDOR_INSTRUCCIONES.E_SERVICIO.GESTIONADOR)
            & (db.tservidor_instrucciones.estado.belongs(TSERVIDOR_INSTRUCCIONES.E_ESTADO.L_ACTIVOS))
            ).select(
                db.tservidor_instrucciones.ALL,
                orderby = [
                    db.tservidor_instrucciones.fecha
                    ]
                )

        for _dbRow in _dbRows:
            _D_instruccion = Storage(
                s_id = _dbRow.id,
                E_tipoinstruccion = _dbRow.tipoinstruccion,
                s_param0 = _dbRow.s_param0,
                s_param1 = _dbRow.s_param1,
                s_param2 = _dbRow.s_param2,
                s_param3 = _dbRow.s_param3,
                dt_param4 = _dbRow.dt_param4,
                dt_param5 = _dbRow.dt_param5,
                s_param6 = _dbRow.s_param6,
                s_param7 = _dbRow.s_param7,
                dbRow = _dbRow,
                es_autoactualizable = _dbRow.es_autoactualizable,
                )
            
            self._L_instrucciones.append(_D_instruccion)
                        
        return _D_return

    def actualizar_instruccion(
            self,
            s_instruccion_id, 
            E_ultimoReturn, 
            s_ultimoMensaje = "", 
            s_rastroAgregar = "",
            s_jsonparams = ""
            ):
        """ Graba las instrucciones en la tabla de instrucciones
        """
        
        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        _dbRow = db.tservidor_instrucciones(s_instruccion_id)
        
        if not _dbRow:
            _D_return.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
            _D_return.s_msgError = u"Instrucción no encontrada"

        elif _dbRow.estado not in TSERVIDOR_INSTRUCCIONES.E_ESTADO.L_ACTIVOS:
            _D_return.E_return = stvfwk2_e_RETURN.NOK_INFORMACION_ACTUALIZADA
            _D_return.s_msgError = u"La instrucción se encuentra cerrada y no se puede actualizar"

        elif _dbRow.servicio != TSERVIDOR_INSTRUCCIONES.E_SERVICIO.GESTIONADOR:
            _D_return.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
            _D_return.s_msgError = u"La instrucción no corresponde al servicio actual"

        else:
            if _dbRow.contador_errores is None:
                _dbRow.contador_errores = 0

            else:
                pass
            
            _n_ultimoReturn = int(E_ultimoReturn)
            if _n_ultimoReturn > CLASS_e_RETURN.MAX:
                _dbRow.ultimo_funcionalidad = E_ultimoReturn[:-2]
                _n_ultimoReturn %= 100  # Utilizado para regresar los últimos dos números del error

            else:
                _dbRow.ultimo_funcionalidad = TSERVIDOR_INSTRUCCIONES.E_FUNCIONALIDAD.NADA
            
            if _n_ultimoReturn in (CLASS_e_RETURN.OK, CLASS_e_RETURN.OK_WARNING):
                # _dbRow.contador_errores = 0  No se reinician los errores ya que se cierra la instrucción
                _dbRow.estado = TSERVIDOR_INSTRUCCIONES.E_ESTADO.TERMINADA

            elif _n_ultimoReturn in (CLASS_e_RETURN.NOK_LOGIN, CLASS_e_RETURN.NOK_PERMISOS):
                _dbRow.estado = TSERVIDOR_INSTRUCCIONES.E_ESTADO.DENEGADA
                _dbRow.contador_errores += 1

            elif _n_ultimoReturn in (
                    CLASS_e_RETURN.NOK_DATOS_INVALIDOS,
                    CLASS_e_RETURN.NOK_CONDICIONES_INCORRECTAS,
                    CLASS_e_RETURN.NOK_SECUENCIA,
                    CLASS_e_RETURN.NOK_INFORMACION_ACTUALIZADA,
                    CLASS_e_RETURN.NOK_ERROR
                    ):
                _dbRow.estado = TSERVIDOR_INSTRUCCIONES.E_ESTADO.ERROR
                _dbRow.contador_errores += 1

            else:
                
                # Si es otro estado, se intenta nuevamente
                _dbRow.contador_errores += 1
                
            _dbRow.ultimo_resultado = _n_ultimoReturn
            _dbRow.ultimo_fecha = request.browsernow
            _dbRow.ultimo_mensaje = s_ultimoMensaje or ""
            _dbRow.ultimo_jsonparams = s_jsonparams or ""
            _dbRow.rastro += (
                self._dt_request_utc.isoformat() + "UTC:" + str(E_ultimoReturn) + ":" + str(s_ultimoMensaje)
                + ":" + str(s_rastroAgregar) + "\n"
                )
            if auth.user_id:
                _dbRow.editado_por = auth.user_id
            else:
                _D_return.s_msgError = u"Auth esta vacio en actualizar instruccion"

            del _dbRow.editado_en  # Se borra para poder actualizar el campo editado_en
            del _dbRow.editado_por
            _dbRow.update_record()
                
        return _D_return
    
    def darseguimiento_instruccion(
            self,
            s_instruccion_id,
            n_incrementar_numoperaciones,
            s_rastro = ""
            ):
        """ Graba las instrucciones en la tabla de instrucciones
        """

        _ = self._n_servidor_id

        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        _dbRow = db.tservidor_instrucciones(s_instruccion_id)
        
        if not _dbRow:
            _D_return.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
            _D_return.s_msgError = u"Instrucción no encontrada"

        elif _dbRow.servicio not in (TSERVIDOR_INSTRUCCIONES.E_SERVICIO.GESTIONADOR,):
            _D_return.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
            _D_return.s_msgError = u"La instrucción no corresponde al servicio actual"

        else:
            if n_incrementar_numoperaciones:
                if _dbRow.numoperaciones:
                    _dbRow.numoperaciones += 1
                else:
                    _dbRow.numoperaciones = 1
                
                if s_rastro:
                    _dbRow.rastro += s_rastro + "\n"
                else:
                    pass
                del _dbRow.editado_en  # Se borra para poder actualizar el campo editado_en
                del _dbRow.editado_por
                _dbRow.update_record()

            else:
                pass
                                
        return _D_return        


def _validaciones(
        b_verificarSession = False,
        D_verificarPermiso = None,
        ):
    _D_return = Storage(
        E_return = stvfwk2_e_RETURN.OK,
        s_msgError = "",
        )    
    
    if (
            b_verificarSession
            and not(auth.is_logged_in())
            ):
        _D_return.E_return = stvfwk2_e_RETURN.NOK_LOGIN
        _D_return.s_msgError = u"Usuario no tiene sesión activa"
        
    elif (
            D_verificarPermiso
            and not(
                request.stv_fwk_permissions.hasPermission(
                    x_code = D_verificarPermiso.x_code,
                    ps_function = D_verificarPermiso.s_function
                    )
                )
            ):
        _D_return.E_return = stvfwk2_e_RETURN.NOK_PERMISOS
        _D_return.s_msgError = u"No cuenta con permisos"

    else:   
        pass
        
    return _D_return


@service.soap(
    'login', 
    returns = {
        'E_return'  : int,
        's_msgError': str,
        'D_auth'    : wbsT_D_AUTHv2,
        'json_data' : str
        }, 
    args = {
        's_email'    : str,
        's_password' : str,
        's_codigo'   : str,
        'json_params': str
        }
    )
def login(s_email, s_password, s_codigo, json_params):
    """
    
    @descripcion Gestionador > Login
    
    @permiso btn_none
    
    Ejemplo:
        from gluon.contrib.pysimplesoap.client import SoapClient
        client = SoapClient(wsdl="http://127.0.0.1:8000/app_wbs/110gestionador/call/soap?WSDL")
        client.Login(s_email="jherrero@stverticales.mx",s_password="pass",s_codigo="test", json_params="")
    """

    """
    <Envelope >
        <Body>
            <login xmlns="https://stverticales.mx/">
                <s_codigo>[string]</s_codigo>
                <json_params>[string]</json_params>
                <s_email>test@stverticales.mx</s_email>
                <s_password>3{Q}Z.L3INs{</s_password>
            </login>
        </Body>
    </Envelope>    
    """
    _ = json_params
    _s_webservice = "login"
    
    _D_return = Storage(
        E_return = stvfwk2_e_RETURN.OK,
        s_msgError = "",
        D_auth = Storage(wbsT_D_AUTHv2),
        json_data = ""
        )    
    
    _D_return.D_auth.s_ver = "2.0"
    _D_return.D_auth.json_key = ""
    _D_return.D_auth.s_token = ""
    _D_return.D_auth.s_signature = ""
    _D_return.D_auth.n_empresa_id = 0
        
    _dt_request_utc = request.utcnow.replace(microsecond = 0)
    
    _D_json = Storage()
    
    try:
        
        if not auth.login_bare(username=str(s_email), password=str(s_password)):
            _D_return.E_return = stvfwk2_e_RETURN.NOK_LOGIN
            _D_return.s_msgError = u"Contraseña incorrecta, favor de intentar nuevamente"
            
        else:

            _O_gestionador = GestionadorInstrucciones(
                dt_request_utc = _dt_request_utc,
                n_usuario_id = auth.user_id,
                )

            _D_results = _O_gestionador.identificar_servidor(
                s_codigo = s_codigo,
                b_configurarHoraServidor = True
                )

            if _D_results.E_return != stvfwk2_e_RETURN.OK:
                _D_return.E_return = _D_results.E_return
                _D_return.s_msgError = _D_results.s_msgError

            else:

                _O_gestionador.conectar(
                    s_webservice = _s_webservice,
                    s_notas = ""
                    )

                _D_results = _validaciones(
                    b_verificarSession = True,
                    D_verificarPermiso = Storage(
                        x_code = request.stv_fwk_permissions.btn_none,
                        s_function = _s_webservice
                        ),
                    )

                _L_instrucciones = []
                if _D_results.E_return != stvfwk2_e_RETURN.OK:
                    _D_return.E_return = _D_results.E_return
                    _D_return.s_msgError = _D_results.s_msgError

                else:

                    # En el servidor parece que no crea el session_id en la base de datos, por lo que
                    #  se crea manualmente
                    _D_sessionData = Storage(
                        locked = False,
                        client_ip = response.session_client,
                        modified_datetime = request.now,
                        unique_key = session.auth.hmac_key
                        )

                    _n_record_id = response.session_db_table.insert(**_D_sessionData)
                    response.session_id = '%s:%s' % (_n_record_id, session.auth.hmac_key)
                    response.session_db_unique_key = session.auth.hmac_key
                    response.session_db_record_id = _n_record_id
                    response.session_new = False  # Para no cambiar la variable de sesion

                    # Se prepara el manejo de instrucciones, con la opción de generar las instrucciones automáticas
                    _D_results = _O_gestionador.preparar_instrucciones(True)

                    if _D_results.E_return == stvfwk2_e_RETURN.OK:
                        _D_results = _O_gestionador.grabar_instrucciones()
                        if _D_results.E_return == stvfwk2_e_RETURN.OK:
                            _D_results = _O_gestionador.obtener_instrucciones()
                            if _D_results.E_return == stvfwk2_e_RETURN.OK:
                                _L_instrucciones = _D_results.L_instrucciones
                            else:
                                _D_return.E_return = _D_results.E_return
                                _D_return.s_msgError = _D_results.s_msgError
                        else:
                            _D_return.E_return = _D_results.E_return
                            _D_return.s_msgError = _D_results.s_msgError

                    else:
                        _D_return.E_return = _D_results.E_return
                        _D_return.s_msgError = _D_results.s_msgError

                _D_json = Storage(
                    L_instrucciones = _L_instrucciones,
                    # debug = [str(auth.user_id)]
                    )

            _D_return.D_auth.s_ver = "2.0"
            _D_return.D_auth.json_key = ""
            _D_return.D_auth.s_token = str(response.session_id)
            _D_return.D_auth.s_signature = s_codigo
            _D_return.D_auth.n_empresa_id = 0

            _D_return.json_data = str(simplejson.dumps(_D_json))
                         
    except Exception as _O_excepcion:
        _D_return.E_return = stvfwk2_e_RETURN.NOK_LOGIN
        _D_return.s_msgError = u'Proceso: %s, Excepcion: %s %s %s' % (
            "login", str(_O_excepcion.__doc__), str(_O_excepcion.args), str(auth.user_id)
            )
        db.rollback()
        
    return _D_return


@service.soap(
    'logout',
    returns = {
        'E_return'  : int,
        's_msgError': str,
        'json_data' : str
        },
    args = {
        'D_auth'     : wbsT_D_AUTHv2,
        'json_params': str
        }
    )
def logout(D_auth, json_params):
    """
    @descripcion Gestionador > Logout
    
    @permiso btn_none

    """
    _ = D_auth
    _ = json_params
    _s_webservice = 'logout'
    
    _D_return = Storage(
        E_return = stvfwk2_e_RETURN.OK,
        s_msgError = "",
        json_data = ""
        )

    try:

        _O_gestionador = GestionadorInstrucciones(
            dt_request_utc = request.utcnow.replace(microsecond = 0),
            n_usuario_id = auth.user_id,
            )
        _O_gestionador.desconectar(
            s_webservice = _s_webservice,
            s_notas = ""
            )
        
        if auth.is_logged_in():
            auth.settings.logout_next = None
            auth.logout()
            _D_return.s_msgError = u"Usuario estaba registrado"
            _D_return.E_return = stvfwk2_e_RETURN.OK

        else:
            _D_return.s_msgError = u"Sesion no estaba activa"
            _D_return.E_return = stvfwk2_e_RETURN.OK

    except Exception as _O_excepcion:
        _D_return.E_return = stvfwk2_e_RETURN.NOK_LOGIN
        _D_return.s_msgError = u'Proceso: %s, Excepcion: %s %s' % (
            "logout", str(_O_excepcion.__doc__), str(_O_excepcion.args)
            )

    return _D_return


@service.soap(
    'subir_cfdi', 
    returns={
        'E_return'  : int,
        's_msgError': str,
        'json_data' : str,
        }, 
    args= {
        'D_auth'      : wbsT_D_AUTHv2,
        's_id'        : str,
        's_rfc'       : str,
        's_empresa_id': str,
        'xml_cfdi'    : None,
        'json_params' : str
        }
    )
def subir_cfdi(D_auth, s_id, s_rfc, s_empresa_id, xml_cfdi, json_params):
    """ Valida la informacion e inserta el CFDI

    Las validaciones son:
    - Si empresa corresponde a rfc
    - Si empresa esta en cuenta
    - Si los pagos estan en pie. Si no estan en pie, registra el CFDI pero no lo relaciona
    - Si el UUID para la empresa no ha sido capturado, else verifica que contenga la misma informacion
    -   Si la informacion es la misma regresa un 0, con el texto de que ya se encontraba registrado
    -   Si la informacion es diferente y no se ha registrado la poliza, se actualiza la informacion
    -   Si ya se registro la poliza, regresa un error, y registra el intento en el sistema
    - Registra la informacion del CFDI e inicia la asociacion con los catalogos relacionados

    Ejemplo:

    @descripcion Gestionador > SubirCFDI
    
    @permiso btn_none

    """
    _ = D_auth
    _ = json_params
    _s_webservice = "subir_cfdi"

    # Se establece el valor por default de las variables
    _D_return = Storage(
        E_return = stvfwk2_e_RETURN.NOK_ERROR,
        s_msgError = "",
        json_data = "",
        )
    _L_msgError = []

    _s_rfc = STV_LIB_CFDI.FORMATEAR_RFC_AGREGARGUIONES(s_rfc)
    _xml_cfdi = None

    # if True:  # Para pruebas
    try:

        # D_auth = Storage(D_auth)

        # noinspection DuplicatedCode
        _O_gestionador = GestionadorInstrucciones(
            dt_request_utc = request.utcnow.replace(microsecond = 0),
            n_usuario_id = auth.user_id,
            )

        _D_results = _O_gestionador.identificar_servidor(
            s_codigo = "",
            s_instruccion_id = s_id,
            b_configurarHoraServidor = True
            )

        if _D_results.E_return != stvfwk2_e_RETURN.OK:
            _D_return.E_return = _D_results.E_return
            _D_return.s_msgError = _D_results.s_msgError

        else:

            _O_gestionador.conectar(
                s_webservice = _s_webservice,
                s_notas = ""
                )

            _dbRow_instruccion = db.tservidor_instrucciones(s_id)

            _D_results = _validaciones(
                b_verificarSession = True,
                D_verificarPermiso = Storage(
                    x_code = request.stv_fwk_permissions.btn_none,
                    s_function = _s_webservice
                    ),
                )

            if _D_results.E_return != stvfwk2_e_RETURN.OK:
                _D_return.E_return = _D_results.E_return
                _L_msgError.append(_D_results.s_msgError)

            elif not xml_cfdi._element.childNodes:
                _D_return.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
                _L_msgError.append(u"xml_cfdi debe definirse")

            else:

                importar_definiciones_aplicacion()
                importar_dbs_genericos()
                importar_dbs_cuenta(s_remplaza_cuenta_id = _dbRow_instruccion.cuenta_id)

                _stv_importar_cfdi = STV_CFDI_INTEGRACION(
                    s_rfc = _s_rfc,
                    s_empresa_id = s_empresa_id,
                    s_cuenta_id = _dbRow_instruccion.cuenta_id,
                    s_procesoIniciador = "wbs::subir_cfdi"
                    )

                _xml_cfdi = str(xml_cfdi._element.childNodes[0].data.encode('utf-8').strip(), encoding='utf-8')

                _dbTabla_cfdi = None
                _CLS_CFDI = None
                if _dbRow_instruccion.tipoinstruccion in (
                        TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_CFDIS,
                        TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_CFDIS_CANCELADOS
                        ):
                    _dbTabla_cfdi = dbc01.tcfdis
                    _CLS_CFDI = STV_LIB_CFDI
                    _D_results = _stv_importar_cfdi.importar(
                        xml_cfdi = _xml_cfdi,
                        E_tipoImportacion = STV_CFDI_INTEGRACION.E_TIPOIMPORTACION.CFDI_EMITIDO
                        )

                elif _dbRow_instruccion.tipoinstruccion in (
                        TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_CFDISRX,
                        TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_CFDISRX_CANCELADOS
                        ):
                    _dbTabla_cfdi = dbc01.tcfdisrx
                    _CLS_CFDI = None
                    _D_results = _stv_importar_cfdi.importar(
                        xml_cfdi = _xml_cfdi,
                        E_tipoImportacion = STV_CFDI_INTEGRACION.E_TIPOIMPORTACION.CFDI_RECIBIDO
                        )
                elif _dbRow_instruccion.tipoinstruccion in (
                        TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.TIMBRAR_CFDI,
                        TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.CANCELAR_CFDI
                        ):
                    _dbTabla_cfdi = dbc01.tcfdis
                    _CLS_CFDI = STV_LIB_CFDI
                    _D_results = _stv_importar_cfdi.verificar(
                        s_cfdi_id = _dbRow_instruccion.s_param3,  # param3 es el parametro donde se guarda el cfdi_id
                        xml_cfdi = _xml_cfdi,
                        E_tipoImportacion = STV_CFDI_INTEGRACION.E_TIPOIMPORTACION.CFDI_EMITIDO
                        )

                else:
                    _D_results.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
                    _D_results.s_msgError = u"Tipo de instrucción inválido para esta operación"
                    _D_results.n_cfdi_id = None

                _D_return.E_return = _D_results.E_return
                _L_msgError.append(_D_results.s_msgError)
                _s_cfdi_id = _D_results.n_cfdi_id
                _D_results = _O_gestionador.darseguimiento_instruccion(
                    s_instruccion_id = s_id,
                    n_incrementar_numoperaciones = 1,
                    s_rastro = "Subir_cfdi %s %s%s"
                               % (str(_D_results.n_cfdi_id), str(_D_results.D_cfdi.serie), str(_D_results.D_cfdi.folio))
                    )
                # Se ignora el resultado por ahora

                _s_ultimalinea = _xml_cfdi.split('\n')[-1]

                """
                <!-- {"uuid":"00A4B2A2-75EA-4C55-BCDE-B5224493BB0F","status":"1",
                "fecha_cancelacion":"0001-01-01T00:00:00","status_cancelacion":"2","proceso_cancelacion":"",
                "pac_rfc":"DET080304395","pac_nombre_comercial":"DETECNO",
                "pac_razon_social":"DETECNO, S.A de C.V."} -->            
                <!--{"uuid":"E00B09ED-D5E2-4639-B782-3331CB0ECA0E","status":"Cancelado",
                "fecha_cancelacion":"2021-02-16T18:41:24","status_cancelacion":"Cancelable sin aceptaci\xc3\xb3n",
                "proceso_cancelacion":"Sin aceptaci\xc3\xb3n"}-->
                """

                _O_match = re.compile('.*\<\!\-\-\\s*{\"(.*)\"\}\s*\-\-\>.*').match(_s_ultimalinea)

                if _O_match and _O_match.group(1):
                    _L_datos_encrudo = _O_match.group(1).split('","')
                    _D_datos = Storage()
                    for _s_dato in _L_datos_encrudo:
                        _L_dato = _s_dato.split('":"')
                        _D_datos[_L_dato[0]] = _L_dato[1]

                    _dbRow_CFDI = _dbTabla_cfdi(_s_cfdi_id)
                    if not _dbRow_CFDI:
                        _L_msgError.append(u'No se identificó el registro del CFDI')
                        _D_return.E_return = stvfwk2_e_RETURN.NOK_REPETIR
                    elif _dbRow_CFDI.uuid.upper() == _D_datos.uuid.upper():
                        _dbRow_CFDI.sat_status = int(_D_datos.status)
                        if _dbRow_CFDI.sat_status == TCFDIS.SAT_STATUS.VIGENTE:
                            _dbRow_CFDI.estado = TCFDIS.E_ESTADO.VIGENTE
                        elif _dbRow_CFDI.sat_status == TCFDIS.SAT_STATUS.CANCELADO:
                            _dbRow_CFDI.estado = TCFDIS.E_ESTADO.CANCELADO
                        else:
                            # Mantiene le estado local del CFDI
                            pass
                        _dt_fechaCancelacion = datetime.datetime.strptime(
                            _D_datos.fecha_cancelacion, '%Y-%m-%dT%H:%M:%S'
                            )
                        if _dt_fechaCancelacion.year > 1900:
                            _dbRow_CFDI.sat_fecha_cancelacion = _dt_fechaCancelacion
                        else:
                            _dbRow_CFDI.sat_fecha_cancelacion = None
                        _dbRow_CFDI.sat_status_cancelacion = _D_datos.status_cancelacion
                        _dbRow_CFDI.sat_status_proceso_cancelacion = _D_datos.proceso_cancelacion
                        _dbRow_CFDI.pac_rfc = _D_datos.pac_rfc
                        _dbRow_CFDI.pac_razon_social = _D_datos.pac_razon_social
                        _dbRow_CFDI.pac_nombre_comercial = _D_datos.pac_nombre_comercial
                        del _dbRow_CFDI.editado_en  # Se borra para poder actualizar el campo editado_en
                        del _dbRow_CFDI.editado_por
                        _dbRow_CFDI.update_record()
                        _L_msgError.append(u'Se actualizó información adicional del CFDI')

                        if (_D_return.E_return == stvfwk2_e_RETURN.OK) and _CLS_CFDI:
                            # Si se agrego el CFDI regresó un OK y si existe la clase procede a la
                            #  generación del estado de cuenta, bajo el entendido de que si regresa warning,
                            #  ya fue subido anteriormente
                            _D_results = TCFDIS.EDOCTA(
                                n_empresa_id = _dbRow_CFDI.empresa_id,
                                n_cliente_id = _dbRow_CFDI.receptorcliente_id,
                                E_movto_generado_por = TCFDI_MOVIMIENTOS.E_GENERADO_POR.IMPORTACION_CFDI,
                                ).procesar_cfdi(_dbRow_CFDI)

                            if _D_results.E_return > stvfwk2_e_RETURN.OK_WARNING:
                                _L_msgError.append(
                                    u'Error al generar Edo Cta. %s ' % _D_results.s_msgError
                                    )
                                _D_return.E_return = stvfwk2_e_RETURN.NOK_REPETIR
                            else:
                                pass

                        else:
                            pass

                    else:
                        _L_msgError.append(u'UUID del CFDI no coincide con UUID de información adicional del CFDI')
                        _D_return.E_return = stvfwk2_e_RETURN.NOK_REPETIR
                else:
                    if _D_return.E_return <= stvfwk2_e_RETURN.OK_WARNING:
                        _D_return.E_return = stvfwk2_e_RETURN.NOK_REPETIR
                    else:
                        # Mandener el nivel del error
                        pass
                    _L_msgError.append(u'No se identificó información adicional del CFDI')

    except Exception as _O_excepcion:
        _D_return.E_return = stvfwk2_e_RETURN.NOK_LOGIN
        _L_msgError.append(u'Proceso: %s, Excepción: %s %s' % (
            "subir_cfdi", str(_O_excepcion.__doc__), str(_O_excepcion.args)
            ))
        
    _D_return.s_msgError = ". ".join(_L_msgError)

    return _D_return


@service.soap(
    'responder_instruccion', 
    returns = {
        'E_return'  : int,
        's_msgError': str,
        'json_data' : str
        },
    args = {
        'D_auth'                : wbsT_D_AUTHv2,
        's_id'                  : str,
        'instruccion_E_return'  : str,
        'instruccion_s_msgError': str,
        'json_params'           : str
        }
    )
def responder_instruccion(D_auth, s_id, instruccion_E_return, instruccion_s_msgError, json_params):
    """
    @descripcion Gestionador > Responder Instruccion
    
    @permiso btn_none

    """
    
    """ Actualiza el status de una instruccion
        
    """
    
    _s_webservice = "responder_instruccion"
    
    # Se establece el valor por default de las variables
    _D_return = Storage(
        E_return = stvfwk2_e_RETURN.NOK_ERROR,
        s_msgError = "",
        json_data = "",
        )

    # if True:
    try:

        _ = Storage(D_auth)
        
        _O_gestionador = GestionadorInstrucciones(
            dt_request_utc = request.utcnow.replace(microsecond = 0),
            n_usuario_id = auth.user_id,
            )

        _D_results = _O_gestionador.identificar_servidor(
            s_codigo = "",
            s_instruccion_id = s_id,
            b_configurarHoraServidor = True
            )

        if _D_results.E_return != stvfwk2_e_RETURN.OK:
            _D_return.E_return = _D_results.E_return
            _D_return.s_msgError = _D_results.s_msgError

        else:

            _O_gestionador.conectar(
                s_webservice = _s_webservice,
                s_notas = ""
                )

            _D_results = _validaciones(
                b_verificarSession = True,
                D_verificarPermiso = Storage(
                    x_code = request.stv_fwk_permissions.btn_none,
                    s_function = _s_webservice
                    ),
                )

            if _D_results.E_return != stvfwk2_e_RETURN.OK:
                _D_return.E_return = _D_results.E_return
                _D_return.s_msgError = _D_results.s_msgError

            elif not instruccion_E_return:
                _D_return.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
                _D_return.s_msgError = u"El código E_return es obligado para terminar la instrucción"

            else:

                _dbRow_instruccion = db.tservidor_instrucciones(s_id)

                if _dbRow_instruccion.cuenta_id:
                    importar_definiciones_aplicacion()
                    importar_dbs_genericos()
                    importar_dbs_cuenta(s_remplaza_cuenta_id = _dbRow_instruccion.cuenta_id)
                else:
                    pass

                if instruccion_s_msgError:
                    _s_instruccion_msgError = str(instruccion_s_msgError.encode("utf8"))
                else:
                    _s_instruccion_msgError = ""

                _D_results = _O_gestionador.actualizar_instruccion(
                    s_instruccion_id = s_id,
                    E_ultimoReturn = instruccion_E_return,
                    s_ultimoMensaje = _s_instruccion_msgError,
                    s_rastroAgregar = "WBS responder_instruccion",
                    s_jsonparams = json_params,
                    )

                _D_return.E_return = _D_results.E_return
                _D_return.s_msgError = _D_results.s_msgError

                if _dbRow_instruccion.tipoinstruccion == TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.TIMBRAR_CFDI:
                    _dbRow_cfdi = dbc01.tcfdis(_dbRow_instruccion.s_param3)

                    # No importa si el CFDI esta en proceso o no de cancelacón, su estado debe actualizarse
                    _O_jsonData = simplejson.loads(json_params)

                    if _O_jsonData and ('O_resultado' in _O_jsonData) and _O_jsonData['O_resultado']:

                        _O_resultado = Storage(_O_jsonData['O_resultado'])

                        if _O_resultado.error_proceso:
                            _D_error_proceso = Storage(_O_resultado['error_proceso'])
                            _D_informacion_timbre = Storage(_O_resultado['informacion_timbre'])

                            if _D_error_proceso.E_proveedorerror in (
                                    TCFDIS.E_PROVEEDOR_ERROR.NO_DEFINIDO,
                                    TCFDIS.E_PROVEEDOR_ERROR.OK
                                    ):
                                _dbRow_cfdi.estado = TCFDIS.E_ESTADO.VIGENTE
                                _dbRow_cfdi.uuid = _D_informacion_timbre.s_uuid
                                _dbRow_cfdi.fechatimbrado_str = _D_informacion_timbre.s_fechatimbrado
                                _dbRow_cfdi.fechatimbrado = TZONASHORARIAS.CONVERTIR_FECHAHORA_DEUTC_AZONAH(STV_FWK_APP.n_zonahoraria_id,
                                    datetime.datetime.strptime(_dbRow_cfdi.fechatimbrado_str, '%Y-%m-%dT%H:%M:%S')
                                    )
                                _dbRow_cfdi.sat_status = TCFDIS.SAT_STATUS.VIGENTE
                                _dbRow_cfdi.pac_rfc = _D_informacion_timbre.s_rfcproveedorcertificacion

                                _D_results = TCFDIS.EDOCTA(
                                    n_empresa_id = _dbRow_cfdi.empresa_id,
                                    n_cliente_id = _dbRow_cfdi.receptorcliente_id,
                                    E_movto_generado_por = TCFDI_MOVIMIENTOS.E_GENERADO_POR.TIMBRADO,
                                    ).procesar_cfdi(_dbRow_cfdi)

                                if _D_results.E_return > stvfwk2_e_RETURN.OK_WARNING:
                                    _D_return.s_msgError = u'Error al generar Edo Cta. %s ' % _D_results.s_msgError
                                    _D_return.E_return = stvfwk2_e_RETURN.NOK_REPETIR
                                else:
                                    pass

                            elif (
                                    (_D_error_proceso.E_proveedorerror == TCFDIS.E_PROVEEDOR_ERROR.ERROR_CON_PAC)
                                    and (_D_error_proceso.n_numero == TCFDIS.E_ERRORES_PAC.HASH_DUPLICADO)
                                    ):
                                # Este caso se presenta cuando se trata de firmar el mismo CFDI
                                _dbRow_cfdi.estado = TCFDIS.E_ESTADO.VIGENTE

                                _D_results = TCFDIS.EDOCTA(
                                    n_empresa_id = _dbRow_cfdi.empresa_id,
                                    n_cliente_id = _dbRow_cfdi.receptorcliente_id,
                                    E_movto_generado_por = TCFDI_MOVIMIENTOS.E_GENERADO_POR.TIMBRADO,
                                    ).procesar_cfdi(_dbRow_cfdi)

                                if _D_results.E_return > stvfwk2_e_RETURN.OK_WARNING:
                                    _D_return.s_msgError = u'Error al generar Edo Cta. %s ' % _D_results.s_msgError
                                    _D_return.E_return = stvfwk2_e_RETURN.NOK_REPETIR
                                else:
                                    pass

                            else:
                                _dbRow_cfdi.errores = _D_error_proceso.s_descripcion
                                _dbRow_cfdi.estado = TCFDIS.E_ESTADO.TIMBRE_RECHAZADO

                        elif int(instruccion_E_return) >= stvfwk2_e_RETURN.OK_WARNING:

                            _dbRow_cfdi.errores = _s_instruccion_msgError
                            _dbRow_cfdi.estado = TCFDIS.E_ESTADO.TIMBRE_RECHAZADO

                        else:
                            pass

                        del _dbRow_cfdi.editado_en  # Se borra para poder actualizar el campo editado_en
                        del _dbRow_cfdi.editado_por
                        _dbRow_cfdi.update_record()

                    else:

                        _D_return.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
                        _D_return.s_msgError = "Parametro JSON no tiene formato correcto para esta instrucción"

                elif _dbRow_instruccion.tipoinstruccion == TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.CANCELAR_CFDI:
                    _dbRow_cfdi = dbc01.tcfdis(_dbRow_instruccion.s_param3)

                    # No importa si el CFDI esta en proceso o no de cancelación, su estado debe actualizarse
                    _O_jsonData = simplejson.loads(json_params)

                    if _O_jsonData and ('O_resultado' in _O_jsonData) and _O_jsonData['O_resultado']:

                        _D_error_proceso = Storage(_O_jsonData['O_resultado']['error_proceso'])
                        # _D_informacion_timbre = Storage(_O_jsonData['O_resultado']['informacion_timbre']) No requerido

                        _E_nuevoEstadoCFDI = TCFDIS.E_ESTADO.VIGENTE
                        _s_errorCFDI = ""

                        if _D_error_proceso.E_proveedorerror in (
                                TCFDIS.E_PROVEEDOR_ERROR.NO_DEFINIDO,
                                TCFDIS.E_PROVEEDOR_ERROR.OK
                                ):
                            _E_nuevoEstadoCFDI = TCFDIS.E_ESTADO.CANCELADO

                        else:
                            _s_errorCFDI = _D_error_proceso.s_descripcion
                            _E_nuevoEstadoCFDI = TCFDIS.E_ESTADO.CANCELACION_RECHAZADO

                        _n_rowsActualizados = dbc01(
                            dbc01.tcfdis.id == _dbRow_instruccion.s_param3
                            ).update(
                                estado = _E_nuevoEstadoCFDI,
                                errores = _s_errorCFDI
                                )
                        if _n_rowsActualizados != 1:
                            _D_return.s_msgError += (
                                u"Se actualizó el estado a un número inesperado de CFDIs %d. "
                                % _n_rowsActualizados
                                )
                        else:
                            pass
                    else:

                        _D_return.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
                        _D_return.s_msgError = "Parametro JSON no tiene formato correcto para esta instrucción"

                    _D_results = TCFDIS.EDOCTA(
                        n_empresa_id = _dbRow_cfdi.empresa_id,
                        n_cliente_id = _dbRow_cfdi.receptorcliente_id,
                        E_movto_generado_por = TCFDI_MOVIMIENTOS.E_GENERADO_POR.CANCELACION,
                        ).procesar_cfdi(_dbRow_cfdi)
                    # TODO Incluir si regresa error en el mensaje de retorno

                else:
                    pass

    except Exception as _O_excepcion:
        _D_return.E_return = stvfwk2_e_RETURN.NOK_LOGIN
        _D_return.s_msgError = u'Proceso: %s, Excepción: %s %s' % (
            "responder_instruccion", str(_O_excepcion.__doc__), str(_O_excepcion.args)
            )

    return _D_return


@service.soap(
    'verify',
    returns = {
        'E_return'  : int,
        's_msgError': str,
        'json_data' : str
        },
    args = {
        'D_auth'     : wbsT_D_AUTHv2,
        'json_params': str
        }
    )
def verify(D_auth, json_params):
    """
    @descripcion Gestionador > Verificacion de sesion
    
    @permiso btn_none

    """
    
    """
    
    Ejemplo:
        client.Verify(D_auth = {'s_token': u'129:d038384f-3a50-4f7a-917d-ac0a2690d846', 's_signature': u'Signature', 
        's_key': u'Llave publica'})
    """

    _ = D_auth
    _ = json_params

    _D_return = Storage(
        E_return = stvfwk2_e_RETURN.OK,
        s_msgError = "",
        json_data = ""
        )

    _s_test = _s_body
    if auth.is_logged_in():
        _D_return.E_return = stvfwk2_e_RETURN.OK
        _D_return.s_msgError = "Sesion activa"
    else:
        _D_return.E_return = stvfwk2_e_RETURN.NOK_LOGIN
        _D_return.s_msgError = "No login"

    return _D_return


@service.soap(
    'coronavirus',
    returns = {
        'result': int
        },
    args = {
        'param_a': int,
        'param_b': int
        }
    )
def coronavirus(param_a, param_b):
    return param_a + param_b


def call():
    # session.forget()
    response.namespace = 'https://stverticales.mx/'
    return service()
