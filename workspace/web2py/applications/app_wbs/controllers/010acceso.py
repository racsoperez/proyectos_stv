# -*- coding: utf-8 -*-

# https://p01.stverticales.mx/app_wbs/010acceso/call/soap?WSDL
# http://127.0.0.1:8000/app_wbs/010acceso/call/soap?WSDL

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
#########################################################################


@service.soap(
    'login', 
    returns={
        'n_return':int,
        's_msgError':str,
        'D_auth':wbsT_D_AUTHv2,
        'json_data':str
        }, 
    args={
        's_email':str, 
        's_password':str, 
        's_codigo':str, 
        'json_params':str
        }
    )
def login(s_email, s_password, s_codigo, json_params):
    """
    
    Ejemplo:
        from gluon.contrib.pysimplesoap.client import SoapClient
        client = SoapClient(wsdl="http://127.0.0.1:8000/app_wbs/010acceso/call/soap?WSDL")
        client.Login(s_email="jherrero@stverticales.mx",s_password="pass",s_codigo="test",json_params="")
    """
    _D_auth = Storage(wbsT_D_AUTHv2)
    
    """
<Envelope >
    <Body>
        <login xmlns="https://stverticales.mx/">
            <s_codigo>[string]</s_codigo>
            <json_params>[string]</json_params>
            <s_email>test@stverticales.mx</s_email>
            <s_password>3{Q}Z.L3INs{</s_password>
        </login>
    </Body>
</Envelope>    
    """

    _json_t_cuentas = ""
    _json_data = ""
    if not auth.login_bare(username=str(s_email), password=str(s_password)):
        _s_msgError = u"Contraseña incorrecta, favor de intentar nuevamente"
        _n_return = stvfwk2_e_RETURN.NOK_LOGIN
    elif not request.stv_fwk_permissions.hasPermission(request.stv_fwk_permissions.btn_none, ps_function = "login"):
        _s_msgError = u"No cuenta con permisos"
        _n_return = stvfwk2_e_RETURN.NOK_PERMISOS
    elif not s_codigo:
        _s_msgError = u"El parámetro código debe existir"
        _n_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS        
    else:
        
        # En el servidor parece que no crea el session_id en la base de datos, por lo que se crea manualmente
        _D_sessionData = dict(
            locked=False,
            client_ip = response.session_client,
            modified_datetime = request.now,
            unique_key = session.auth.hmac_key
            )
 
        _n_record_id = response.session_db_table.insert(**_D_sessionData)
        response.session_id = '%s:%s' % (_n_record_id, session.auth.hmac_key)
        response.session_db_unique_key = session.auth.hmac_key
        response.session_db_record_id = _n_record_id        
        response.session_new = False  # Para no cambiar la variable de sesion
        
        _s_msgError = u""
        _n_return = stvfwk2_e_RETURN.OK

        _D_auth.s_ver = "2.0"
        _D_auth.json_key = ""
        _D_auth.s_token = str(response.session_id)
        _D_auth.s_signature = s_codigo
        _D_auth.n_empresa_id = 0
        
        _D_json_vacio = {}
        _json_data = str(
            (simplejson.dumps(_D_json_vacio))
            ) 

        # Consulta para desplegar los privilegios a las diferentes aplicaciones
        #_db_rows = db((db.tcuenta_aplicacion_usuarios.user_id == auth.user_id)
        #              & (db.tcuenta_aplicaciones.id == db.tcuenta_aplicacion_usuarios.cuenta_aplicacion_id)
        #              & (db.tcuentas.id == db.tcuenta_aplicaciones.cuenta_id)
        #              ).select(db.tcuenta_aplicacion_usuarios.cuenta_aplicacion_id, 
        #                       db.tcuenta_aplicaciones.aplicacion_w2p,
        #                       db.tcuenta_aplicaciones.nombrecorto,
        #                       db.tcuentas.nombrecorto,
        #                       db.tcuentas.id)

        # Condulta para mostrar solo las cuentas que tiene el usuario
#         _db_rows = db(
#             (db.tcuenta_aplicacion_usuarios.user_id == auth.user_id)
#             & (db.tcuenta_aplicaciones.id == db.tcuenta_aplicacion_usuarios.cuenta_aplicacion_id)
#             & (db.tcuentas.id == db.tcuenta_aplicaciones.cuenta_id)
#             ).select(
#                 db.tcuentas.id,
#                 db.tcuentas.nombrecorto,
#                 db.tcuenta_aplicaciones.id,
#                 db.tcuenta_aplicaciones.nombrecorto,
#                 distinct=True
#                 )
# 
#         # TODO prevenir seleccion de suspendidos o borrados lógicos
#         _json_t_cuentas = str(simplejson.dumps({
#             'atributos':"",
#             'datos':_db_rows.as_list()
#             }))
        
    

    return {'n_return':_n_return, 
            's_msgError':_s_msgError, 
            'D_auth':_D_auth,
            'json_data':_json_data}


@service.soap(
    'logout', 
    returns = {
        'n_return':int,
        's_msgError':str,
        'json_data':str
        }, 
    args={
        'D_auth':wbsT_D_AUTHv2, 
        'json_params':str
        }
    )
def logout(D_auth):
    
    if auth.is_logged_in():
        auth.settings.logout_next = None
        auth.logout()
        _s_msgError = u"Usuario estaba registrado"
        _n_return = stvfwk2_e_RETURN.OK
    else:
        _s_msgError = u"Usuario no estaba registrado"
        _n_return = stvfwk2_e_RETURN.OK

    return {'n_return':_n_return,
            's_msgError':_s_msgError,
            'json_data':""}


@service.soap('verify', 
              returns={
                  'n_return':int,
                  's_msgError':str,
                  'json_data':str
                  }, 
              args={
                  'D_auth':wbsT_D_AUTHv2, 
                  'json_params':str
                  })
def verify(D_auth, json_params):
    """
    
    Ejemplo:
        client.Verify(D_auth = {'s_token': u'129:d038384f-3a50-4f7a-917d-ac0a2690d846', 's_signature': u'Signature', 's_key': u'Llave publica'})
    """
    
    _s_test = _s_body
    if auth.is_logged_in():
        _n_return = stvfwk2_e_RETURN.OK
    else:
        _n_return = stvfwk2_e_RETURN.NOK_LOGIN

    return {
            'n_return' : _n_return,
            's_msgError' : "",
            'json_data' : "",            
            }


@service.soap(
    'coronavirus', 
    returns={
        'result':int
        }, 
    args={
        'param_a':int, 
        'param_b':int
        }
    )
def coronavirus(param_a, param_b):
    return param_a + param_b


def call():
    #session.forget()
    response.namespace = 'https://stverticales.mx/'
    return service()

