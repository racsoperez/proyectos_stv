# -*- coding: utf-8 -*-

#########################################################################
# # This is a sample controller
# # - index is the default action of any application
# # - user is required for authentication and authorization
# # - download is for downloading files uploaded in the db (does streaming)
#########################################################################

        
# #
# # Funciones usadas para manejar cache con la información que es recurrente
# #
def _definir_camposrelacion_cfdi():
    """ Se define la relación entre el nombre del elemento de json y un diccionario con información 
    necesaria para la insersión.
    
    Args:
    
    Returns:
        Storage donde el key es el string que identifica el campo en json,
        y el valor es otro Storage con la siguiente info:
            - dbField: referencia al campo donde se asigna la información
            - fnValidation: si es texto es validación directa, si es función de llama y se espera un boleano
            - [sMsg]: es un mensaje que se regresa en caso de error en la validación    
    """

    _D_campos_relacion = {
        'version' :                 Storage(dbField = dbc01.tcfdis.versioncfdi, fnValidation = None),
        'serie' :                   Storage(dbField = dbc01.tcfdis.serie, fnValidation = None),
        'folio' :                   Storage(dbField = dbc01.tcfdis.folio, fnValidation = None),
        'fecha' :                   Storage(dbField = dbc01.tcfdis.fecha_str, fnValidation = None),
        'formapago' :               Storage(dbField = dbc01.tcfdis.formapago, fnValidation = None),
        'condicionesdepago' :       Storage(dbField = dbc01.tcfdis.condicionesdepago, fnValidation = None),
        'subtotal' :                Storage(dbField = dbc01.tcfdis.subtotal, fnValidation = None),
        'descuento' :               Storage(dbField = dbc01.tcfdis.descuento, fnValidation = None),
        'moneda' :                  Storage(dbField = dbc01.tcfdis.moneda, fnValidation = None),
        'tipocambio' :              Storage(dbField = dbc01.tcfdis.tipocambio, fnValidation = None),
        'total' :                   Storage(dbField = dbc01.tcfdis.total, fnValidation = None),
        'tipodecomprobante' :       Storage(dbField = dbc01.tcfdis.tipodecomprobante, fnValidation = None),
        'metodopago' :              Storage(dbField = dbc01.tcfdis.metodopago, fnValidation = None),
        'lugarexpedicion' :         Storage(dbField = dbc01.tcfdis.lugarexpedicion, fnValidation = None),
        'totalimpuestosretenidos' : Storage(dbField = dbc01.tcfdis.totalimpuestosretenidos, fnValidation = None),
        'totalimpuestostrasladados' : 
                                    Storage(dbField = dbc01.tcfdis.totalimpuestostrasladados, fnValidation = None),
        'cfdirelacionados.tiporelacion' : 
                                    Storage(dbField = dbc01.tcfdis.tiporelacion, fnValidation = None),

        #'confirmacion' :            Storage(dbField = dbc01.tcfdis.confirmacion, fnValidation = None),
        #'ordencompra' :             Storage(dbField = dbc01.tcfdis.ordencompra, fnValidation = None),
        #'foliointernopedido' :      Storage(dbField = dbc01.tcfdis.foliointernopedido, fnValidation = None),
        #'agentevendedor' :          Storage(dbField = dbc01.tcfdis.agentevendedor, fnValidation = None),
        #'montobaseprodiva0' :       Storage(dbField = dbc01.tcfdis.montobaseprodiva0, fnValidation = None),
        #'montobaseprodiva' :        Storage(dbField = dbc01.tcfdis.montobaseprodiva, fnValidation = None),

        'emisor.rfc' :              Storage(dbField = dbc01.tcfdis.emisorrfc, fnValidation = None),
        'emisor.nombre' :           Storage(dbField = dbc01.tcfdis.emisornombrerazonsocial, fnValidation = None),
        'emisor.regimenfiscal' :    Storage(dbField = dbc01.tcfdis.emisorregimenfiscal, fnValidation = None),
        'emisor.calle' :            Storage(dbField = dbc01.tcfdis.emisorcalle, fnValidation = None),
        'emisor.noexterior' :       Storage(dbField = dbc01.tcfdis.emisornumexterior, fnValidation = None),
        'emisor.nointerior' :       Storage(dbField = dbc01.tcfdis.emisornuminterior, fnValidation = None),
        'emisor.colonia' :          Storage(dbField = dbc01.tcfdis.emisorcolonia, fnValidation = None),
        'emisor.localidad' :        Storage(dbField = dbc01.tcfdis.emisorlocalidad, fnValidation = None),
        'emisor.municipio' :        Storage(dbField = dbc01.tcfdis.emisormunicipio, fnValidation = None),
        'emisor.estado' :           Storage(dbField = dbc01.tcfdis.emisorestado, fnValidation = None),
        'emisor.pais' :             Storage(dbField = dbc01.tcfdis.emisorpais, fnValidation = None),
        'emisor.cp' :               Storage(dbField = dbc01.tcfdis.emisorcodigopostal, fnValidation = None),
        'receptor.rfc' :            Storage(dbField = dbc01.tcfdis.receptorrfc, fnValidation = None),
        'receptor.nombre' :         Storage(dbField = dbc01.tcfdis.receptornombrerazonsocial, fnValidation = None),
        'receptor.usocfdi' :        Storage(dbField = dbc01.tcfdis.receptorusocfdi, fnValidation = None),
        'receptor.calle' :          Storage(dbField = dbc01.tcfdis.receptorcalle, fnValidation = None),
        'receptor.noexterior' :     Storage(dbField = dbc01.tcfdis.receptornumexterior, fnValidation = None),
        'receptor.nointerior' :     Storage(dbField = dbc01.tcfdis.receptornuminterior, fnValidation = None),
        'receptor.colonia' :        Storage(dbField = dbc01.tcfdis.receptorcolonia, fnValidation = None),
        'receptor.localidad' :      Storage(dbField = dbc01.tcfdis.receptorlocalidad, fnValidation = None),
        'receptor.municipio' :      Storage(dbField = dbc01.tcfdis.receptormunicipio, fnValidation = None),
        'receptor.estado' :         Storage(dbField = dbc01.tcfdis.receptorestado, fnValidation = None),
        'receptor.pais' :           Storage(dbField = dbc01.tcfdis.receptorpais, fnValidation = None),
        'receptor.cp' :             Storage(dbField = dbc01.tcfdis.receptorcodigopostal, fnValidation = None),
        
        'timbrefiscaldigital.uuid' : 
                                    Storage(dbField = dbc01.tcfdis.uuid, fnValidation = None),
        'timbrefiscaldigital.fechatimbrado' : 
                                    Storage(dbField = dbc01.tcfdis.fechatimbrado_str, fnValidation = None),        
        }

    return _D_campos_relacion

def _asociar_tabla(dbTabla, s_nombreCampo, s_datoBuscar, s_msgError = "", qry_maestro = None, s_proceso = ""):
    """ Asocia el codigo de la serie y regresa su id
    
    Args:
        n_empresa_id: id de la empresa
        dbTabla: tabla detalle de la empresa de la que se busca el ID
        s_nombreCampo: campo de la tabla por el cual se va a buscar s_datoBuscar
        s_datoBuscar: información a buscar
        s_msgError: en caso de existir es el mensaje que será retornado, puede incluir los siguientes códigos
            %(tabla)s: para imprimir el singular de la tabla
            %(tablaSingular)s: para imprimir el singular de la tabla
            %(nombreCampo)s: para imprimir el nombre del campo
            %(datoBuscar)s: para imprimir el dato a buscar
        L_referencia: es una lista con aplicacion, controlador, funcion, argumentos
        
    Returns:
        D_NOTIFICACIONMENSAJE
    """
    _D_return = D_NOTIFICACIONMENSAJE(s_proceso = s_proceso)
    _D_return.mensaje_al_asociar(str(dbTabla), s_nombreCampo, s_datoBuscar)

    if qry_maestro:
        _dbRows = dbTabla._db(
            qry_maestro
            & (dbTabla[s_nombreCampo] == s_datoBuscar)
            & (dbTabla.bansuspender == False)
            ).select(
                dbTabla.id,
                )
    else:
        _dbRows = dbTabla._db(
            (dbTabla[s_nombreCampo] == s_datoBuscar)
            & (dbTabla.bansuspender == False)
            ).select(
                dbTabla.id,
                )

    if (len(_dbRows) != 1):

        _D_print = {
            'tabla'        : str(dbTabla),
            'tablaSingular': dbTabla._singular,
            'nombreCampo'  : s_nombreCampo,
            'datoBuscar'   : str(s_datoBuscar)
            }
        
        _D_return.n_len_dbRows = len(_dbRows)
        
        if s_msgError:
            _D_return.s_msgError = s_msgError % _D_print
        elif _D_return.n_len_dbRows == 0:
            _D_return.s_msgError = ('No se pudo encontro referencia a %s') % dbTabla._singular
        else:
            _D_return.s_msgError = ('Se encontraron 2 o más referencias a %s') % dbTabla._singular
        _D_return.s_msgExcepcion = ('%(tabla)s : %(nombreCampo)s = %(datoBuscar)s') % _D_print
        _D_return.s_msgLastSQL = dbTabla._db._lastsql
        _D_return.n_return = stvfwk2_e_RETURN.NOK_ERROR
        
    else:
        _D_return.n_id = _dbRows.first().id
        _D_return.n_return = stvfwk2_e_RETURN.OK

    return _D_return


def _insertar_json(O_json_data, D_campos_relacion, dbTabla, D_fields_defaults, s_proceso = ""):
    """ Importa y asocia el contenido de un json con las tablas del cliente.
    
    Args:
        O_json_data: objeto json con el contenido de datos.
        D_campos_relacion: es lo que regresa la función _definir_camposrelacion_cfdi()
        dbTabla: tabla donde se insertarán los datos 
        D_fields_defaults: campos prellenados con valores por default

    Returns:
        D_NOTIFICACIONMENSAJE
    """
    _D_return = D_NOTIFICACIONMENSAJE(s_proceso = s_proceso)
    _D_return.mensaje_al_insertar(str(dbTabla))

    # Se agregan los campos standard con sus valores
    _D_fieldsInsert = D_fields_defaults

    # Se agregan los valores adicionales importados del json
    _s_jsonData = None
    for _s_jsonField in D_campos_relacion:
        _L_elementosxml = _s_jsonField.split('.')
        if len(_L_elementosxml) == 1:
            if _L_elementosxml[0] in O_json_data:
                _s_jsonData = O_json_data[_L_elementosxml[0]]
            else:
                _s_jsonData = None
        elif len(_L_elementosxml) == 2:
            if (
                (_L_elementosxml[0] in O_json_data)
                and (_L_elementosxml[1] in O_json_data[_L_elementosxml[0]])
                ):
                _s_jsonData = O_json_data[_L_elementosxml[0]][_L_elementosxml[1]]
            else:
                _s_jsonData = None
        else:
            if (
                (
                    (_L_elementosxml[0] in O_json_data)
                    and (_L_elementosxml[1] in O_json_data[_L_elementosxml[0]])
                    )
                and (_L_elementosxml[2] in (O_json_data[_L_elementosxml[0]][_L_elementosxml[1]]))
                ):
                _s_jsonData = O_json_data[_L_elementosxml[0]][_L_elementosxml[1]][_L_elementosxml[2]]
            else:
                _s_jsonData = None

        _D_fieldsInsert[str(D_campos_relacion[_s_jsonField].dbField.name)] = _s_jsonData

    try:
        _D_return.n_id = dbTabla.insert(**_D_fieldsInsert)
        _D_return.n_return = stvfwk2_e_RETURN.OK
        _D_return.s_msgError = ('Inserción satisfactoria en ' + dbTabla._singular)        
    except Exception as _O_excepcion:
        _D_return.s_msgError = ('No se pudo insertar el json de ' + dbTabla._singular)
        _D_return.s_msgExcepcion = str(dbTabla) + (':%s' % str(_O_excepcion))
        _D_return.s_msgLastSQL = dbTabla._db._lastsql
        _D_return.n_return = stvfwk2_e_RETURN.NOK_ERROR

    return _D_return

def _insertar_clienteproveedor(s_empresa_id, s_elementoXml, dbTabla, D_cfdi, s_proceso = ""):
    """ Inserta un cliente o proveedor.
    
    Args:
        s_empresa_id: id de la empresa.
        s_elementoXml: es el elemento del json que encapsula los datos del cliente o del proveedor 
        dbTabla: puede ser la tabla de tempresa_clientes o tempresa_proveedores
        D_cfdi: json del cfdi

    Returns:
        D_RETURN_ERRORES_PRIORIDAD
    """
    _D_returnErroresPrioridad = D_RETURN_ERRORES_PRIORIDAD()

    _D_fieldsInsertClienteProveedor = Storage(
        empresa_id = s_empresa_id,
        nombrecorto = D_cfdi[s_elementoXml]['nombre']
        )
    
    if 'pais' in D_cfdi[s_elementoXml]:
        # Se asocia el pais con la tabla
        _s_proceso = "CFDI.35/45.010: Asociar país"
        _s_cache_id = 'cfdi_asociar_pais' + unicode(D_cfdi[s_elementoXml]['pais'])
        _D_returnsData = cache.ram(
            _s_cache_id, 
            lambda:_asociar_tabla(
                db01.tpaises, 
                'c_pais', 
                D_cfdi[s_elementoXml]['pais']
                ), 
            time_expire = 86400
            )
        if _D_returnsData.n_return == stvfwk2_e_RETURN.OK:
            _D_fieldsInsertClienteProveedor.pais_id = _D_returnsData.n_id
            _D_returnErroresPrioridad.informacion(_D_returnsData, _s_proceso)
        else:
            cache.ram(_s_cache_id, None)
            _D_fieldsInsertClienteProveedor.pais_id = 0
            _D_returnErroresPrioridad.baja(_D_returnsData, _s_proceso)
    
        if 'estado' in D_cfdi[s_elementoXml]:
            # Se asocia el estado con la tabla, el campo c_estado es único por lo que no es necesario aplicar otra condición de pais
            _s_proceso = "CFDI.35/45.020: Asociar estado"
            _s_cache_id = 'cfdi_asociar_estado' + unicode(D_cfdi[s_elementoXml]['estado'])
            _D_returnsData = cache.ram(
                _s_cache_id, 
                lambda:_asociar_tabla(
                    db01.tpais_estados, 
                    'c_estado', 
                    D_cfdi[s_elementoXml]['estado'],
                    ), 
                time_expire = 86400
                )
            if _D_returnsData.n_return == stvfwk2_e_RETURN.OK:
                _D_fieldsInsertClienteProveedor.pais_estado_id = _D_returnsData.n_id
                _D_returnErroresPrioridad.informacion(_D_returnsData, _s_proceso)
            else:
                cache.ram(_s_cache_id, None)
                _D_fieldsInsertClienteProveedor.pais_estado_id = 0
                _D_returnErroresPrioridad.baja(_D_returnsData, _s_proceso)
        
            if _D_fieldsInsertCliente.pais_estado_id:
                # Se asocia el municipio con la tabla, el campo c_municipio NO es único por lo que es necesario aplicar otra condición de pais_estado_id
                _s_proceso = "CFDI.35/45.030: Asociar municipio"
                _s_cache_id = 'cfdi_asociar_municipio' + unicode(D_cfdi[s_elementoXml]['municipio'])
                _D_returnsData = cache.ram(
                    _s_cache_id, 
                    lambda:_asociar_tabla(
                        db01.tpais_estado_municipios, 
                        'c_municipio', 
                        D_cfdi[s_elementoXml]['municipio'], 
                        qry_maestro = (db01.tpais_estado_municipios.pais_estado_id == _D_fieldsInsertCliente.pais_estado_id),
                        ), 
                    time_expire = 86400)
                if _D_returnsData.n_return == stvfwk2_e_RETURN.OK:
                    _D_fieldsInsertClienteProveedor.pais_estado_municipio_id = _D_returnsData.n_id
                    _D_returnErroresPrioridad.informacion(_D_returnsData, _s_proceso)
                else:
                    cache.ram(_s_cache_id, None)
                    _D_fieldsInsertClienteProveedor.pais_estado_municipio_id = 0
                    _D_returnErroresPrioridad.baja(_D_returnsData, _s_proceso)
            else:
                _D_fieldsInsertCliente.pais_estado_municipio_id = 0
        else:
            _D_fieldsInsertClienteProveedor.pais_estado_id = 0
    else:
        #_D_fieldsInsertClienteProveedor.pais_id = 0
        pass

    _D_campos_relacion = {
        s_elementoXml + '.rfc' :              Storage(dbField = dbTabla.rfc, fnValidation = None),
        s_elementoXml + '.nombre' :           Storage(dbField = dbTabla.razonsocial, fnValidation = None),
        #s_elementoXml + '.regimenfiscal' :    Storage(dbField = dbTabla.regimenfiscal, fnValidation = None),
        #s_elementoXml + '.calle' :            Storage(dbField = dbTabla.calle, fnValidation = None),
        #s_elementoXml + '.noexterior' :       Storage(dbField = dbTabla.numexterior, fnValidation = None),
        #s_elementoXml + '.nointerior' :       Storage(dbField = dbTabla.numinterior, fnValidation = None),
        #s_elementoXml + '.colonia' :          Storage(dbField = dbTabla.colonia, fnValidation = None),
        #s_elementoXml + '.localidad' :        Storage(dbField = dbTabla.ciudad, fnValidation = None),
        #s_elementoXml + '.cp' :               Storage(dbField = dbTabla.codigopostal, fnValidation = None),
        }
    _D_returnsData = _insertar_json(
        D_cfdi, 
        _D_campos_relacion, 
        dbTabla, 
        _D_fieldsInsertClienteProveedor, 
        )
    
    if _D_returnsData.n_return == stvfwk2_e_RETURN.OK:
        _D_returnErroresPrioridad.ultimo_id = _D_returnsData.n_id
        _D_returnErroresPrioridad.informacion(_D_returnsData, s_proceso)
    else:
        _D_returnErroresPrioridad.ultimo_id = None
        _D_returnErroresPrioridad.critica(_D_returnsData, s_proceso)

    return _D_returnErroresPrioridad

def _insertar_prodserv(s_empresa_id, D_cfdi_concepto, D_fieldsInsertConcepto, s_proceso = ""):
    """ Inserta un cliente o proveedor.
    
    Args:
        s_empresa_id: id de la empresa.
        s_elementoXml: es el elemento del json que encapsula los datos del cliente o del proveedor 
        dbTabla: puede ser la tabla de tempresa_clientes o tempresa_proveedores
        D_cfdi: json del cfdi

    Returns:
        D_NOTIFICACIONMENSAJE
    """    
    _dbTabla = dbc01.tempresa_prodserv

    _D_return = D_NOTIFICACIONMENSAJE(s_proceso = s_proceso)
    _D_return.mensaje_al_insertar(str(_dbTabla), 'codigo', D_cfdi_concepto['noidentificacion'])

    _D_fieldsInsertProdServ = Storage(
        empresa_id = s_empresa_id,
        unidad_id = D_fieldsInsertConcepto.unidad_id,
        prodserv_id = D_fieldsInsertConcepto.prodserv_id,
        )

    _D_campos_relacion = {
        'noidentificacion' :        Storage(dbField = _dbTabla.codigo, fnValidation = None),
        'descripcion' :             Storage(dbField = _dbTabla.descripcion, fnValidation = None),
        }
    _D_return = _insertar_json(
        D_cfdi_concepto, 
        _D_campos_relacion, 
        _dbTabla, 
        _D_fieldsInsertProdServ, 
        s_proceso = s_proceso)

    return _D_return

def _importar_cfdi_prevalidaciones(s_rfc, s_empresa_id, s_cuenta_id, json_cfdi):
    
    _D_return = Storage(
        n_return = stvfwk2_e_RETURN.NOK_ERROR,
        s_msgError = "",
        n_cfdi_id = 0,
        b_pagosaldia = False,
        D_cfdi = None,
        n_cuenta_empresa_id = -0,
        dbRow_empresa = None
        )
    
    if not auth.is_logged_in():
        _D_return.n_return = stvfwk2_e_RETURN.NOK_LOGIN
#     elif not request.stv_fwk_permissions.hasPermission(request.stv_fwk_permissions.btn_none, "importar_cfdi"):
#         _D_return.n_return = stvfwk2_e_RETURN.NOK_PERMISOS
    else:

        # Se valida si la empresa corresponde al RFC
        _dbRows = dbc01(
            (dbc01.tempresas.rfc == s_rfc)
            & (dbc01.tempresas.id == s_empresa_id)
            ).select(
                dbc01.tempresas.id,
                dbc01.tempresas.rfc_publico,
                dbc01.tempresas.rfc_publico_extranjeros                              
                )

        if not (_dbRows):
            _D_return.n_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
            _D_return.s_msgError = "Empresa no encontrada"
        else:
            
            _D_return.dbRow_empresa = _dbRows.first()
            
            # Se valida si la empresa esta relacionada con la cuenta
            _dbRows = db(
                (db.tcuenta_empresas.cuenta_id == s_cuenta_id)
                & (db.tcuenta_empresas.empresa_id == s_empresa_id)
                ).select(
                    db.tcuenta_empresas.id)

            if not _dbRows:
                _D_return.n_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
                _D_return.s_msgError = "Empresa no asociada a cuenta"
            else:

                _D_return.n_cuenta_empresa_id = _dbRows.first().id
                
                # Se hace verificación de pagos
                _n_mes = int(datetime.datetime.now().strftime("%m"))
                _dbRows = db(
                    (db.tcuenta_empresa_pagos.cuenta_empresa_id == _D_return.n_cuenta_empresa_id)
                    & (db.tcuenta_empresa_pagos.mes == _n_mes)
                    ).select(
                        db.tcuenta_empresa_pagos.id)

                _D_return.b_pagosaldia = bool(_dbRows)  # Se registra si los pagos estan al día o no

                _D_return.D_cfdi = json.loads(json_cfdi)
                
                _s_rfc_formateado = (
                    _D_return.D_cfdi['emisor']['rfc'][:len(_D_return.D_cfdi['emisor']['rfc'])-9] 
                    + "-" + _D_return.D_cfdi['emisor']['rfc'][-9:-3] 
                    + "-" + _D_return.D_cfdi['emisor']['rfc'][-3:]
                    )
                _D_return.D_cfdi['emisor']['rfc'] = _s_rfc_formateado
                 
                _s_rfc_formateado = (
                    _D_return.D_cfdi['receptor']['rfc'][:len(_D_return.D_cfdi['receptor']['rfc'])-9] 
                    + "-" + _D_return.D_cfdi['receptor']['rfc'][-9:-3] 
                    + "-" + _D_return.D_cfdi['receptor']['rfc'][-3:]
                    )
                _D_return.D_cfdi['receptor']['rfc'] = _s_rfc_formateado                

                # Se verifica que para la empresa el uuid no exista ya
                _dbRows = dbc01(
                    (dbc01.tcfdis.empresa_id == s_empresa_id)
                    & (dbc01.tcfdis.uuid == _D_return.D_cfdi['timbrefiscaldigital']['uuid'])
                    ).select(
                        dbc01.tcfdis.id)
                    
                if _dbRows:
                    _D_return.n_return = stvfwk2_e_RETURN.NOK_INFORMACION_ACTUALIZADA
                    _D_return.s_msgError = "UUID ya fue ingresado para la empresa"                    
                else:
                    # No existe registro
                    
                    # Verificar que el RFC de la empresa este referenciado en el CFDI
                    if (
                        (str(_D_return.D_cfdi['emisor']['rfc']) != s_rfc)
                        and (str(_D_return.D_cfdi['receptor']['rfc']) != s_rfc)
                        ):
                        _D_return.n_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
                        _D_return.s_msgError = "RFC de la empresa no coincide con emisor o receptor"
                    else: 
                        _D_return.n_return = stvfwk2_e_RETURN.OK
                    
    return _D_return


@service.soap('importar_cfdi',
              returns = {
                  'n_return':int,
                  's_msgError':str,
                  'n_cfdi_id':int },
              args = {
                  'D_auth':wbsT_D_AUTH,
                  's_rfc':str,
                  's_empresa_id':str,
                  'json_cfdi':str,
                  'xml_cfdi' :str,
                  'D_attr':{}})
def importar_cfdi(D_auth, s_rfc, s_empresa_id, json_cfdi, xml_cfdi, D_attr):
    """ Valida la informacion e inserta el CFDI
    
    Las validaciones son:
    - Si empresa corresponde a rfc
    - Si empresa esta en cuenta
    - Si los pagos estan en pie. Si no estan en pie, registra el CFDI pero no lo relaciona
    - Si el UUID para la empresa no ha sido capturado, else verifica que contenga la misma informacion
    -   Si la informacion es la misma regresa un 0, con el texto de que ya se encontraba registrado
    -   Si la informacion es diferente y no se ha registrado la poliza, se actualiza la informacion
    -   Si ya se registro la poliza, regresa un error, y registra el intento en el sistema
    - Registra la informacion del CFDI e inicia la asociacion con los catalogos relacionados
    
    Ejemplo:
        
    """
    
    #TODO Si s_rfc no tiene guiones hay que ponerlos.

    # Se establece el valor por default de las variables
    _D_return = Storage(
        n_return = stvfwk2_e_RETURN.NOK_ERROR,
        s_msgError = "",
        n_cfdi_id = 0
        )
    _n_cfdi_id = 0
    
    
    
    try:
#    if True:
        _s_proceso = "CFDI.00.000: Validaciones para iniciar importación"
        _D_returnValidaciones = _importar_cfdi_prevalidaciones(s_rfc, s_empresa_id, D_stvFwkCfg.cfg_cuenta_id, json_cfdi)    
    
        if stvfwk2_e_RETURN.OK != _D_returnValidaciones.n_return:
            _D_return.n_return = _D_returnValidaciones.n_return
            _D_return.s_msgError = _D_returnValidaciones.s_msgError
            _D_return.n_cfdi_id = _D_returnValidaciones.n_cfdi_id
        else:
    
            _D_cfdi = _D_returnValidaciones.D_cfdi
            _b_pagosaldia = _D_returnValidaciones.b_pagosaldia
            _n_cuenta_empresa_id = _D_returnValidaciones.n_cuenta_empresa_id
            _dbRow_empresa = _D_returnValidaciones.dbRow_empresa
    
            # ##
            # ## Se inserta el maestro de CFDI
            # ##
    
            # Se define el storage donde se van a almacenar los errores durante el proceso en forma de lista de acuerdo a prioridad
            _D_erroresPrioridad = D_RETURN_ERRORES_PRIORIDAD()
    
            # Se agrega el campo que identifica a la empresa como valor por default en la inserción
            _D_fieldsInsert = Storage(
                empresa_id = s_empresa_id
                )

            # Se asocia el tipo de comprobante con la tabla
            _s_proceso = "CFDI.10.020: Asociar tipo de comprobante"
            _s_cache_id = 'cfdi_asociar_tipocomprobante' + str(_D_cfdi['tipodecomprobante'])
            _D_returnsData = cache.ram(
                _s_cache_id, 
                lambda:_asociar_tabla(
                    db01.ttiposcomprobantes, 
                    'c_tipodecomprobante', 
                    _D_cfdi['tipodecomprobante'],
                    ), 
                time_expire = 86400
                )
            if _D_returnsData.n_return == stvfwk2_e_RETURN.OK:
                _D_fieldsInsert.tipocomprobante_id = _D_returnsData.n_id
                _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
            else:
                cache.ram(_s_cache_id, None)
                _D_fieldsInsert.tipocomprobante_id = None
                _D_erroresPrioridad.critica(_D_returnsData, _s_proceso)
    
            # Solamente si es emisor
            if str(_D_cfdi['emisor']['rfc']) == s_rfc:    
                # Se asocia la serie con la tabla
                _s_proceso = "CFDI.10.010: Asociar serie"
                _s_cache_id = 'cfdi_asociar_empresa' + str(s_empresa_id) + 'serie' + str(_D_cfdi['serie'])
                _D_returnsData = cache.ram(
                    _s_cache_id, 
                    lambda:_asociar_tabla(
                        dbc01.tempresa_series, 
                        'serie', 
                        _D_cfdi['serie'],
                        qry_maestro = (dbc01.tempresa_series.empresa_id == s_empresa_id),
                        ), 
                    time_expire = 86400
                    )
                if _D_returnsData.n_return == stvfwk2_e_RETURN.OK:
                    _D_fieldsInsert.empresa_serie_id = _D_returnsData.n_id
                    _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
                    
                    # Se asocia la caja chica con serie de ingreso
                    _s_proceso = "CFDI.10.015: Asociar caja chica"
                    
                    if _D_fieldsInsert.tipocomprobante_id in (TTIPOSCOMPROBANTES.INGRESO,): 
                    
                        _s_cache_id = 'cfdi_asociar_cajachica_empresaingreso' + str(s_empresa_id) + 'serie' + str(_D_fieldsInsert.empresa_serie_id)
                        _D_returnsData = cache.ram(
                            _s_cache_id, 
                            lambda:_asociar_tabla(
                                dbc01.tempresa_plaza_sucursal_cajaschicas, 
                                'empresa_serie_ingreso_id', 
                                _D_fieldsInsert.empresa_serie_id
                                ), 
                            time_expire = 86400
                            )
                            
                    elif _D_fieldsInsert.tipocomprobante_id in (TTIPOSCOMPROBANTES.EGRESO,): 
                    
                        _s_cache_id = 'cfdi_asociar_cajachica_empresaegreso' + str(s_empresa_id) + 'serie' + str(_D_fieldsInsert.empresa_serie_id)
                        _D_returnsData = cache.ram(
                            _s_cache_id, 
                            lambda:_asociar_tabla(
                                dbc01.tempresa_plaza_sucursal_cajaschicas, 
                                'empresa_serie_egreso_id', 
                                _D_fieldsInsert.empresa_serie_id
                                ), 
                            time_expire = 86400
                            )
                    
                    elif _D_fieldsInsert.tipocomprobante_id in (TTIPOSCOMPROBANTES.PAGO,): 
                    
                        _s_cache_id = 'cfdi_asociar_cajachica_empresapago' + str(s_empresa_id) + 'serie' + str(_D_fieldsInsert.empresa_serie_id)
                        _D_returnsData = cache.ram(
                            _s_cache_id, 
                            lambda:_asociar_tabla(
                                dbc01.tempresa_plaza_sucursal_cajaschicas, 
                                'empresa_serie_pago_id', 
                                _D_fieldsInsert.empresa_serie_id
                                ), 
                            time_expire = 86400
                            )
                            
                    else:
                        pass
                    
                    
                    if _D_returnsData.n_return == stvfwk2_e_RETURN.OK:
                        _D_fieldsInsert.empresa_plaza_sucursal_cajachica_id = _D_returnsData.n_id
                        _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
                    else:
                        cache.ram(_s_cache_id, None)
                        
                        # Se asocia la caja chica por código postal
                        _s_proceso = "CFDI.10.015: Asociar caja chica por lugar de expedición"
                        _s_cache_id = 'cfdi_asociar_cajachica_empresa' + str(s_empresa_id) + 'lugarexpedicion' + str(_D_cfdi['lugarexpedicion'])
                        _D_returnsData = cache.ram(
                            _s_cache_id, 
                            lambda:_asociar_tabla(
                                dbc01.tempresa_plaza_sucursal_cajaschicas, 
                                'bansuspender', 
                                False,
                                qry_maestro = (
                                    (dbc01.tempresa_plazas.empresa_id == s_empresa_id)
                                    & (dbc01.tempresa_plaza_sucursales.codigopostal == str(_D_cfdi['lugarexpedicion']))
                                    & (dbc01.tempresa_plaza_sucursales.empresa_plaza_id == dbc01.tempresa_plazas.id)
                                    & (dbc01.tempresa_plaza_sucursal_cajaschicas.empresa_plaza_sucursal_id == dbc01.tempresa_plaza_sucursales.id)
                                    ),
                                ), 
                            time_expire = 86400
                            )
                        
                        if _D_returnsData.n_return == stvfwk2_e_RETURN.OK:
                            _D_fieldsInsert.empresa_plaza_sucursal_cajachica_id = _D_returnsData.n_id
                            _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
                        else:
                            cache.ram(_s_cache_id, None)
                            _D_fieldsInsert.empresa_plaza_sucursal_cajachica_id = None
                            _D_erroresPrioridad.alta(_D_returnsData, _s_proceso)
                    
                else:
                    cache.ram(_s_cache_id, None)
                    _D_fieldsInsert.empresa_serie_id = None
                    _D_erroresPrioridad.alta(_D_returnsData, _s_proceso)
            else:
                # Si no es emisor, no se puede asociar la serie
                
                pass
            

            # Se asocia la forma de pago con la tabla
            _s_proceso = "CFDI.10.030: Asociar forma de pago"
            _s_cache_id = 'cfdi_asociar_formapago' + str(_D_cfdi['formapago'])
            _D_returnsData = cache.ram(
                _s_cache_id, 
                lambda:_asociar_tabla(
                    db01.tformaspago, 
                    'c_formapago', 
                    _D_cfdi['formapago'],
                    ), 
                time_expire = 86400
                )
            if _D_returnsData.n_return == stvfwk2_e_RETURN.OK:
                _D_fieldsInsert.formapago_id = _D_returnsData.n_id
                _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
            else:
                cache.ram(_s_cache_id, None)
                _D_fieldsInsert.formapago_id = None
                # Si tipo de comprobante es Ingreso o Egreso, la forma de pago y el metodo de pago es obligatoria
                if _D_fieldsInsert.tipocomprobante_id in (TTIPOSCOMPROBANTES.EGRESO, TTIPOSCOMPROBANTES.INGRESO): 
                    _D_erroresPrioridad.alta(_D_returnsData, _s_proceso)
                else:
                    _D_erroresPrioridad.baja(_D_returnsData, _s_proceso)
    
            # Se asocia el método de pago con la tabla
            _s_proceso = "CFDI.10.040: Asociar método de pago"
            _s_cache_id = 'cfdi_asociar_metodopago' + str(_D_cfdi['metodopago'])
            _D_returnsData = cache.ram(
                _s_cache_id, 
                lambda:_asociar_tabla(
                    db01.tmetodospago, 
                    'c_metodopago', 
                    _D_cfdi['metodopago'],
                    ), 
                time_expire = 86400
                )
            if _D_returnsData.n_return == stvfwk2_e_RETURN.OK:
                _D_fieldsInsert.metodopago_id = _D_returnsData.n_id
                _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
            else:
                cache.ram(_s_cache_id, None)
                _D_fieldsInsert.metodopago_id = None
                # Si tipo de comprobante es Ingreso o Egreso, la forma de pago y el metodo de pago es obligatoria
                if _D_fieldsInsert.tipocomprobante_id in (TTIPOSCOMPROBANTES.EGRESO, TTIPOSCOMPROBANTES.INGRESO): 
                    _D_erroresPrioridad.alta(_D_returnsData, _s_proceso)
                else:
                    _D_erroresPrioridad.baja(_D_returnsData, _s_proceso)
    
            # Se asocia la moneda con la tabla
            _s_proceso = "CFDI.10.050: Asociar moneda"
            _s_cache_id = 'cfdi_asociar_moneda' + str(_D_cfdi['moneda'])
            _D_returnsData = cache.ram(
                _s_cache_id, 
                lambda:_asociar_tabla(
                    db01.tmonedas, 
                    'c_moneda', 
                    _D_cfdi['moneda'],
                    ), 
                time_expire = 86400
                )
            if _D_returnsData.n_return == stvfwk2_e_RETURN.OK:
                _D_fieldsInsert.moneda_id = _D_returnsData.n_id
                _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
                
                _s_cache_id = 'cfdi_asociar_moneda' + str(s_empresa_id) + 'contpaqi' + str(_D_fieldsInsert.moneda_id)
                _D_returnsData = cache.ram(
                    _s_cache_id, 
                    lambda:_asociar_tabla(
                        dbc01.tempresa_monedascontpaqi, 
                        'moneda_id',
                        _D_fieldsInsert.moneda_id,
                        qry_maestro = (dbc01.tempresa_monedascontpaqi.empresa_id == s_empresa_id),
                        ), 
                    time_expire = 86400
                    )        
                
                if _D_returnsData.n_return == stvfwk2_e_RETURN.OK:
                    _D_fieldsInsert.monedacontpaqi_id = _D_returnsData.n_id
                    _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
                else:
                    cache.ram(_s_cache_id, None)
                    _D_fieldsInsert.monedacontpaqi_id = None
                    _D_erroresPrioridad.alta(_D_returnsData, _s_proceso)                
                
            else:
                cache.ram(_s_cache_id, None)
                _D_fieldsInsert.moneda_id = None
                # Si tipo de comprobante es Ingreso o Egreso, la forma de pago y el metodo de pago es obligatoria
                if _D_fieldsInsert.tipocomprobante_id in (TTIPOSCOMPROBANTES.EGRESO, TTIPOSCOMPROBANTES.INGRESO): 
                    _D_erroresPrioridad.alta(_D_returnsData, _s_proceso)
                else:
                    _D_erroresPrioridad.baja(_D_returnsData, _s_proceso)
    
            # Se asocia el lugar de expedición con la tabla
            _s_proceso = "CFDI.10.060: Asociar lugar de expedición"
            _s_cache_id = 'cfdi_asociar_lugarexpedicion' + str(_D_cfdi['lugarexpedicion'])
            _D_returnsData = cache.ram(
                _s_cache_id, 
                lambda:_asociar_tabla(
                    db01.tcodigospostales, 
                    'c_codigopostal', 
                    _D_cfdi['lugarexpedicion'],
                    ), 
                time_expire = 86400
                )
            if _D_returnsData.n_return == stvfwk2_e_RETURN.OK:
                _D_fieldsInsert.lugarexpedicion_id = _D_returnsData.n_id
                _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
            else:
                cache.ram(_s_cache_id, None)
                _D_fieldsInsert.lugarexpedicion_id = None
                _D_erroresPrioridad.alta(_D_returnsData, _s_proceso)
    
            if _D_fieldsInsert.tipocomprobante_id in (TTIPOSCOMPROBANTES.EGRESO,): 
    
                # Se asocia el tipo relación con la tabla
                _s_proceso = "CFDI.10.070: Asociar tipo de relación, ya que es egreso"
                _s_cache_id = 'cfdi_asociar_tiporelacion' + str(_D_cfdi['cfdirelacionados']['tiporelacion'])
                _D_returnsData = cache.ram(
                    _s_cache_id, 
                    lambda:_asociar_tabla(
                        db01.ttiposrelaciones, 
                        'c_tiporelacion', 
                        _D_cfdi['cfdirelacionados']['tiporelacion'],
                        ), 
                    time_expire = 86400
                    )
                if _D_returnsData.n_return == stvfwk2_e_RETURN.OK:
                    _D_fieldsInsert.tiporelacion_id = _D_returnsData.n_id
                    _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
                else:
                    cache.ram(_s_cache_id, None)
                    _D_fieldsInsert.tiporelacion_id = None
                    # Si tipo de comprobante es Ingreso o Egreso, la forma de pago y el metodo de pago es obligatoria
                    _D_erroresPrioridad.alta(_D_returnsData, _s_proceso)
                        
            else:
                # Se ignora el tiporelacion
                pass

            # Se asocia el uso cfdi con la tabla
            _s_proceso = "CFDI.10.080: Asociar receptor uso CFDI"
            _s_cache_id = 'cfdi_asociar_receptorusocfdi' + str(_D_cfdi['receptor']['usocfdi'])
            _D_returnsData = cache.ram(
                _s_cache_id, 
                lambda:_asociar_tabla(
                    db01.tusoscfdi, 
                    'c_usocfdi', 
                    _D_cfdi['receptor']['usocfdi'],
                    ), 
                time_expire = 86400
                )
            if _D_returnsData.n_return == stvfwk2_e_RETURN.OK:
                _D_fieldsInsert.receptorusocfdi_id = _D_returnsData.n_id
                _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
            else:
                cache.ram(_s_cache_id, None)
                _D_fieldsInsert.receptorusocfdi_id = None
                _D_erroresPrioridad.alta(_D_returnsData, _s_proceso)    
    
            _s_proceso = "CFDI.20.010: Identificar fecha"
            if _D_cfdi['fecha']:
                try:
                    _D_fieldsInsert.fecha = datetime.datetime.strptime(_D_cfdi['fecha'], '%Y-%m-%dT%H:%M:%S')
                    _D_fieldsInsert.dia = _D_fieldsInsert.fecha.date()
                    _D_fieldsInsert.hora = _D_fieldsInsert.fecha.time()
                except Exception as _O_excepcion:
                    _D_returnsData = D_NOTIFICACIONMENSAJE(
                        n_id = 0,
                        s_msgError = ('No se pudo asociar la fecha %s' % _D_cfdi['fecha']),
                        s_msgExcepcion = ('Fecha:%s' % str(_O_excepcion)),
                        s_msgLastSQL = '',
                        n_return = stvfwk2_e_RETURN.NOK_ERROR,
                        s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.VALIDAR % (str(dbc01.tcfdis), 'fecha', _D_cfdi['fecha'], "", "", ""),
                        )
                    _D_erroresPrioridad.alta(_D_returnsData, _s_proceso)
            else:
                _D_erroresPrioridad.alta(D_NOTIFICACIONMENSAJE(
                    n_id = 0,
                    s_msgError = "Fecha no esta definida en el cfdi",
                    s_msgExcepcion = "",
                    s_msgLastSQL = "",
                    n_return = stvfwk2_e_RETURN.NOK_ERROR,
                    s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.VALIDAR % (str(dbc01.tcfdis), 'fecha', _D_cfdi['fecha_str'], "", "", ""),
                    s_proceso = _s_proceso
                    )
                )
            
            _s_proceso = "CFDI.20.020: Identificar fecha de timbrado"
            if _D_cfdi['timbrefiscaldigital']['fechatimbrado']:
                try:
                    _D_fieldsInsert.fechatimbrado = datetime.datetime.strptime(_D_cfdi['timbrefiscaldigital']['fechatimbrado'], '%Y-%m-%dT%H:%M:%S')
                except Exception as _O_excepcion:
                    _D_returnsData = D_NOTIFICACIONMENSAJE(
                        n_id = 0,
                        s_msgError = ('No se pudo asociar la fecha de timbrado %s' % _D_cfdi['timbrefiscaldigital']['fechatimbrado']),
                        s_msgExcepcion = ('Fecha Timbrado:%s' % str(_O_excepcion)),
                        s_msgLastSQL = '',
                        n_return = stvfwk2_e_RETURN.NOK_ERROR,
                        s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.VALIDAR % (str(dbc01.tcfdis), 'fechatimbrado', _D_cfdi['timbrefiscaldigital']['fechatimbrado'], "", "", ""),
                        )
                    _D_erroresPrioridad.alta(_D_returnsData, _s_proceso)
            else:
                _D_erroresPrioridad.alta(D_NOTIFICACIONMENSAJE(
                    n_id = 0,
                    s_msgError = "Fecha no esta definida en el cfdi",
                    s_msgExcepcion = "",
                    s_msgLastSQL = "",
                    n_return = stvfwk2_e_RETURN.NOK_ERROR,
                    s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.VALIDAR % (str(dbc01.tcfdis), 'fechatimbrado', _D_cfdi['timbrefiscaldigital']['fechatimbrado'], "", "", ""),
                    s_proceso = _s_proceso
                    )
                )
    
    
            if str(_D_cfdi['emisor']['rfc']) == s_rfc:
                # Si el RFC de la empresa corresponde al emisor, se asocia el RFC del receptor a clientes
                _D_fieldsInsert.rol = TCFDIS.ROL.EMISOR
                _D_fieldsInsert.emisorproveedor_id = None
                
                _s_proceso = "CFDI.30.010: Asociar receptor con clientes, ya que la empresa es el emisor"
                _s_cache_id = 'cfdi_asociar_receptorrfc_' + str(s_empresa_id) + '_' + str(_D_cfdi['receptor']['rfc'])
                _D_returnsData = cache.ram(
                    _s_cache_id, 
                    lambda:_asociar_tabla(
                        dbc01.tempresa_clientes, 
                        'rfc', 
                        _D_cfdi['receptor']['rfc'],
                        qry_maestro = (dbc01.tempresa_clientes.empresa_id == s_empresa_id),
                        ), 
                    time_expire = 86400
                    )
                
                # Si se encontraron más de un registro con el RFC, se asocia por razon social también
                if ( (_D_returnsData.n_len_dbRows > 1) 
                     or (_dbRow_empresa.rfc_publico == _D_cfdi['receptor']['rfc'])
                     or (_dbRow_empresa.rfc_publico_extranjeros == _D_cfdi['receptor']['rfc'])
                     ):
                    cache.ram(_s_cache_id, None)
                    _D_erroresPrioridad.media(_D_returnsData, _s_proceso + ". Se procede a buscar por rfc")
                    
                    _s_proceso = "CFDI.30.015: RFC en clientes esta repetido, se intenta asociar por razon social"
                    _D_returnsData = _asociar_tabla(
                        dbc01.tempresa_clientes, 
                        'razonsocial', 
                        _D_cfdi['receptor']['nombre'],
                        qry_maestro = (
                            (dbc01.tempresa_clientes.empresa_id == s_empresa_id)
                            & (dbc01.tempresa_clientes.rfc == _D_cfdi['receptor']['rfc'])
                            ),
                        )

                if _D_returnsData.n_return == stvfwk2_e_RETURN.OK:
                    _D_fieldsInsert.receptorcliente_id = _D_returnsData.n_id
                    _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
                else:
                    cache.ram(_s_cache_id, None)
                    _D_erroresPrioridad.alta(_D_returnsData, _s_proceso + ". Se procede a insertarlo")
                    
                    _s_proceso = "CFDI.30.020: Cliente no identificado, se procede a insertarlo"
                    _D_returnsData = _insertar_clienteproveedor(
                        s_empresa_id, 
                        'receptor', 
                        dbc01.tempresa_clientes, 
                        _D_cfdi,
                        )
                    _D_fieldsInsert.receptorcliente_id = _D_returnsData.ultimo_id
                    _D_erroresPrioridad.agregar(_D_returnsData)
                                
            elif str(_D_cfdi['receptor']['rfc']) == s_rfc:
                # Si el RFC de la empresa corresponde al receptor, Se asocia el emisor rfc con la tabla de proveedores
                _D_fieldsInsert.rol = TCFDIS.ROL.RECEPTOR
                _D_fieldsInsert.receptorcliente_id = None
                
                _s_proceso = "CFDI.40.010: Asociar emisor con proveedores, ya que la empresa es el receptor"
                _s_cache_id = 'cfdi_asociar_emisorrfc_' + str(s_empresa_id) + '_' + str(_D_cfdi['emisor']['rfc'])
                _D_returnsData = cache.ram(
                    _s_cache_id, 
                    lambda:_asociar_tabla(
                        dbc01.tempresa_proveedores, 
                        'rfc', 
                        _D_cfdi['emisor']['rfc'],
                        qry_maestro = (dbc01.tempresa_proveedores.empresa_id == s_empresa_id),
                        ), 
                    time_expire = 86400
                    )
                if _D_returnsData.n_return == stvfwk2_e_RETURN.OK:
                    _D_fieldsInsert.emisorproveedor_id = _D_returnsData.n_id
                    _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
                else:
                    cache.ram(_s_cache_id, None)
                    _D_erroresPrioridad.alta(_D_returnsData, _s_proceso)
                    
                    _s_proceso = "CFDI.40.020: Proveedor no identificado, se procede a insertarlo"
                    _D_returnsData = _insertar_clienteproveedor(
                        s_empresa_id, 
                        'emisor', 
                        dbc01.tempresa_proveedores, 
                        _D_cfdi,
                        )
                    _D_fieldsInsert.emisorproveedor_id = _D_returnsData.ultimo_id
                    _D_erroresPrioridad.agregar(_D_returnsData)
    
            else:
    
                # Si no coincide el RFC es un grave error porque no se sabe como contabilizar el CFDI, pero existe un validación previa
                _D_fieldsInsert.rol = TCFDIS.ROL.NO_DEFINIDO
                _D_fieldsInsert.emisorproveedor_id = None
                _D_fieldsInsert.receptorcliente_id = None 
                1/0
                
    
            # Se define el objeto que relaciona los campos del json con el cfdi
            _D_campos_relacion_cfdi = cache.ram('cfdi_camposrelacion_cfdi', lambda:_definir_camposrelacion_cfdi(), time_expire = 86400)

            _s_proceso = "CFDI.50.000: Se inserta la información maestro del CFDI"    
            _D_returnsData = _insertar_json(
                _D_cfdi, 
                _D_campos_relacion_cfdi, 
                dbc01.tcfdis, 
                _D_fieldsInsert, 
                )
            
            if _D_returnsData.n_return != stvfwk2_e_RETURN.OK:
                # Si existen errores en la creación del cfdi
                _D_erroresPrioridad.critica(_D_returnsData, _s_proceso)
                _n_cfdi_id = 0
            else:
                
                # Si no hay errores en la creación del CFDI continua la inserción de conceptos, pagos y relacionados
                _D_erroresPrioridad.baja(_D_returnsData, _s_proceso)
                _n_cfdi_id = _D_returnsData.n_id

                _s_proceso = "CFDI.60.000: Se asocia CFDI"    
                
                _D_returnsData = TCFDIS.asociar_uuid(_n_cfdi_id)
                #TODO capturar errores en la sociación y generar notificaciones
                                
                _D_erroresPrioridad.agregar(_importar_cfdi_xml(_n_cfdi_id, xml_cfdi, json_cfdi, _D_fieldsInsert));                
                
                _D_erroresPrioridad.agregar(_importar_cfdi_conceptos(_n_cfdi_id, _D_cfdi, s_empresa_id, _D_fieldsInsert));
                        
                _D_erroresPrioridad.agregar(_importar_cfdi_relacionados(_n_cfdi_id, _D_cfdi, _D_fieldsInsert));
    
                _D_erroresPrioridad.agregar(_importar_cfdi_pagos(_n_cfdi_id, _D_cfdi, s_empresa_id, _D_fieldsInsert));
    
    
            _D_notificacionMensajes = _D_erroresPrioridad.getTodos()
    
            # Si se existen errores para crear notificaciones
            if (_D_notificacionMensajes):
            
                _s_titutlo = ""
                
                if _D_erroresPrioridad.CRITICA:
                    # Se regresa cualquier inserción a la base de datos dbc01 antes de guardar las notificaciones                        
                    dbc01.rollback()
                    _s_titutlo = u"Al menos un error CRÍTICO no permitió la inserción del CFDI %(serie)s %(folio)s" % _D_cfdi

                elif _D_erroresPrioridad.ALTA:
                    _s_titutlo = u"Al menos un error ALTA necesita resolverse sobre el CFDI %(serie)s %(folio)s" % _D_cfdi
                elif _D_erroresPrioridad.MEDIA:
                    _s_titutlo = u"Al menos un error MEDIA necesita ser revisados sobre el CFDI %(serie)s %(folio)s" % _D_cfdi
                elif _D_erroresPrioridad.BAJA:
                    _s_titutlo = u"Al menos un error BAJA se generó durante la importación del CFDI %(serie)s %(folio)s" % _D_cfdi
                elif _D_erroresPrioridad.INFORMACION:
                    _s_titutlo = u"Se generó INFORMACIÓN durante la inserción del CFDI %(serie)s %(folio)s" % _D_cfdi
                else:
                    _s_titutlo = u"Se generaron notificaciones sin prioridad sobre el CFDI %(serie)s %(folio)s" % _D_cfdi
            
                # Se procede a insertar la notificación
                _D_fieldsInsert = Storage(
                    empresa_id = s_empresa_id,
                    titulo = _s_titutlo,
                    descripcion = '',
                    aplicacion_w2p = request.application,
                    controlador_w2p = request.controller,
                    funcion_w2p = request.function,
                    argumentos = '/'.join(request.args),
                    referencia = json_cfdi,
                    referencia2 = '',
                    ultimosql = dbc01._lastsql,
                    estatus = TNOTIFICACIONES.ESTATUS.NUEVO,
                    proceso_id = TNOTIFICACIONES.PROCESO_ID.IMPORTAR_CFDI,
                    reprocesar_aplicacion_w2p = request.application,
                    reprocesar_controlador_w2p = request.controller,
                    reprocesar_funcion_w2p = request.function,
                    reprocesar_argumentos = '/'.join(request.args),
                    user_id_asignado = None,
                    historia = 'Creado por %s' % D_stvFwkCfg.cfg_cuenta_id,           
                    )
                _n_notificacion_id = dbc02.tnotificaciones.insert(**_D_fieldsInsert)
                
                # Si se inserto la notificación correctamente
                if _n_notificacion_id:
    
                    for _O_mensaje in _D_notificacionMensajes:
    
                        # Se procede a insertar el mensaje de la notificación
                        _D_fieldsInsertMensaje = Storage(
                            notificacion_id = _n_notificacion_id,
                            codigovalidacion = _O_mensaje.s_codigovalidacion,
                            descripcion = _O_mensaje.s_msgError,
                            excepcion = _O_mensaje.s_msgExcepcion,
                            ultimosql = _O_mensaje.msgLastSQL,
                            referencia_id = _O_mensaje.n_referencia_id or _O_mensaje.n_id,
                            codigoerror = _O_mensaje.n_return,
                            prioridad = _O_mensaje.n_prioridad,
                            proceso = _O_mensaje.s_proceso                               
                            )
                        _n_notificacionmensaje_id = dbc02.tnotificacion_mensajes.insert(**_D_fieldsInsertMensaje)
                     
                    if _D_erroresPrioridad.CRITICA:
                        
                        # Si existen errores criticos
                        _D_return.n_return = stvfwk2_e_RETURN.NOK_CONDICIONES_INCORRECTAS
                        _D_return.s_msgError = _s_titutlo
                        _D_return.n_cfdi_id = 0

                    else:
                        # Todo pasó correctamente y no se generaron notificaciones
                        _D_return.n_return = stvfwk2_e_RETURN.OK
                        _D_return.s_msgError = u"Importación sin notificaciones, excelente!"
                        _D_return.n_cfdi_id = _n_cfdi_id
                       
                else:
                    
                    # Si no puedo ser insertada la notificación
                    
                    # Se regresa cualquier inserción a la base de datos dbc01
                    dbc01.rollback()
                    
                    _D_return.n_return = stvfwk2_e_RETURN.NOK_ERROR
                    _D_return.s_msgError = u"No se pudo generar la notificación en el sistema. Se cancelo la operacion."
                    _D_return.n_cfdi_id = 0
            else:
                # Todo pasó correctamente y no se generaron notificaciones
                
                _D_return.n_return = stvfwk2_e_RETURN.OK
                _D_return.s_msgError = u"Importación sin notificaciones, excelente!"
                _D_return.n_cfdi_id = _n_cfdi_id
                pass
    except Exception as _O_excepcion:
        _D_return.n_return = stvfwk2_e_RETURN.NOK_ERROR
        _D_return.s_msgError = 'Proceso: %s, Excepcion: %s' % (_s_proceso, str(_O_excepcion))
        _D_return.n_cfdi_id = 0
         
        # Se regresa cualquier inserción a la base de datos dbc01
        dbc01.rollback()
        cache.ram.clear(regex='^cfdi.*')
        
    
    return _D_return

def _importar_cfdi_conceptos(n_cfdi_id, D_cfdi, s_empresa_id, D_fieldsInsertCfdi):
    """ Valida la informacion e inserta los conceptos de un CFDI
    
    Return:
        D_RETURN_ERRORES_PRIORIDAD
        
    """    
    _D_erroresPrioridad = D_RETURN_ERRORES_PRIORIDAD()

    # Se relacionan los nombres en el json con los campos en la tabla tcfdi_conceptos
    _D_campos_relacion_conceptos = {
        'cantidad' :            Storage(dbField = dbc01.tcfdi_conceptos.cantidad, fnValidation = None),
        'claveprodserv' :       Storage(dbField = dbc01.tcfdi_conceptos.claveprodserv, fnValidation = None),
        'claveunidad' :         Storage(dbField = dbc01.tcfdi_conceptos.claveunidad, fnValidation = None),
        'descripcion' :         Storage(dbField = dbc01.tcfdi_conceptos.descripcion, fnValidation = None),
        'descuento' :           Storage(dbField = dbc01.tcfdi_conceptos.descuento, fnValidation = None),
        'importe' :             Storage(dbField = dbc01.tcfdi_conceptos.importe, fnValidation = None),
        'noidentificacion' :    Storage(dbField = dbc01.tcfdi_conceptos.noidentificacion, fnValidation = None),
        'unidad' :              Storage(dbField = dbc01.tcfdi_conceptos.descuento, fnValidation = None),
        'valorunitario' :       Storage(dbField = dbc01.tcfdi_conceptos.valorunitario, fnValidation = None),
        #'InfoAduaneraNoPedimento' : 
        #                        Storage(dbField = dbc01.tcfdi_conceptos.infoaduaneranopedimento, fnValidation = None),
        #'CuentaPredialNo' :     Storage(dbField = dbc01.tcfdi_conceptos.cuentapredialno, fnValidation = None),
        #'CantidadReal' :        Storage(dbField = dbc01.tcfdi_conceptos.cantidadreal, fnValidation = None),
        }
    
    _n_indice = 0
    _s_proceso = ""
    for _D_cfdi_concepto in D_cfdi['conceptos']:

        _n_indice += 1
        
        # Se asocia el noidentificacion con la tabla de productos y servicios de la empresa
        _s_proceso = "CFDI-Concepto%s.00.000: Identificar concepto en base a tipo de comprobante" % str(_n_indice) 
        if (D_fieldsInsertCfdi.tipocomprobante_id in (TTIPOSCOMPROBANTES.EGRESO, TTIPOSCOMPROBANTES.INGRESO, TTIPOSCOMPROBANTES.PAGO)):

            _s_proceso = "CFDI-Concepto%s.10.010: Identificar clave de producto o servicio" % str(_n_indice) 
            if _D_cfdi_concepto['claveprodserv']:

                # Se agregan los campos standard con sus valores
                _D_fieldsInsertConcepto = Storage(
                    cfdi_id = n_cfdi_id
                    )
        
                # Se asocia el producto o servicio con la tabla
                _s_proceso = "CFDI-Concepto%s.10.020: Se asocia la clave de producto o servicio" % str(_n_indice) 
                _D_returnsData = _asociar_tabla(
                    db01.tprodservs, 
                    'c_claveprodserv', 
                    _D_cfdi_concepto['claveprodserv'],
                    )
                if _D_returnsData.n_return == stvfwk2_e_RETURN.OK:
                    _D_fieldsInsertConcepto.prodserv_id = _D_returnsData.n_id
                    _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
                else:
                    _D_fieldsInsertConcepto.prodserv_id = None
                    _D_erroresPrioridad.alta(_D_returnsData, _s_proceso)
        
                # Se asocia la unidad con la tabla
                _s_proceso = "CFDI-Concepto%s.10.030: Se asocia la clave de unidad" % str(_n_indice) 
                _s_cache_id = 'cfdi_asociar_unidad' + str(_D_cfdi_concepto['claveunidad'])
                _D_returnsData = cache.ram(
                    _s_cache_id, 
                    lambda:_asociar_tabla(
                        db01.tunidades, 
                        'c_claveunidad', 
                        _D_cfdi_concepto['claveunidad'],
                        ), 
                    time_expire = 86400
                    )
                if _D_returnsData.n_return == stvfwk2_e_RETURN.OK:
                    _D_fieldsInsertConcepto.unidad_id = _D_returnsData.n_id
                    _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
                else:
                    cache.ram(_s_cache_id, None)
                    _D_fieldsInsertConcepto.unidad_id = None
                    _D_erroresPrioridad.alta(_D_returnsData, _s_proceso)
                
                _s_proceso = "CFDI-Concepto%s.10.040: Se asocia no de identificación con el código del producto" % str(_n_indice) 
                _D_returnsData = _asociar_tabla(
                    dbc01.tempresa_prodserv, 
                    'codigo', 
                    _D_cfdi_concepto['noidentificacion'],
                    qry_maestro = (dbc01.tempresa_prodserv.empresa_id == s_empresa_id),
                    )
                    
                # Si se asocio correctamente:
                if _D_returnsData.n_return == stvfwk2_e_RETURN.OK:
                    _D_fieldsInsertConcepto.empresa_prodserv_id = _D_returnsData.n_id
                else:
                    # de lo contrario se trata de insertar
                    _D_erroresPrioridad.alta(_D_returnsData, _s_proceso + ". Se procede a insertarlo")
                    _s_proceso = "CFDI-Concepto%s.10.050: No se encontró el producto, por lo que se procede a insertarlo" % str(_n_indice) 
                    _D_returnsData = _insertar_prodserv(
                        s_empresa_id, 
                        _D_cfdi_concepto, 
                        _D_fieldsInsertConcepto,
                        )
                    
                # Si no se aoscio correctamente o no se logro insertar
                if _D_returnsData.n_return != stvfwk2_e_RETURN.OK:
                    _D_fieldsInsertConcepto.empresa_prodserv_id = None
                    _D_erroresPrioridad.critica(_D_returnsData, _s_proceso)
                else:
                    # si existe asociación exitosa...
                    _D_fieldsInsertConcepto.empresa_prodserv_id = _D_returnsData.n_id
                    _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
                
                    if D_fieldsInsertCfdi.tipocomprobante_id in (TTIPOSCOMPROBANTES.INGRESO,):
                        if D_fieldsInsertCfdi.metodopago_id in (TMETODOSPAGO.PARCIALIDADES,):
                            _D_fieldsInsertConcepto.saldo_cobrado = 0
                            _D_fieldsInsertConcepto.saldo_nocobrado = _D_cfdi_concepto['importe']
                        elif D_fieldsInsertCfdi.metodopago_id in (TMETODOSPAGO.UNAEXHIBICION,):
                            _D_fieldsInsertConcepto.saldo_cobrado = _D_cfdi_concepto['importe']  
                            _D_fieldsInsertConcepto.saldo_nocobrado = 0                  
                        else:
                            _D_fieldsInsertConcepto.saldo_cobrado = 0
                            _D_fieldsInsertConcepto.saldo_nocobrado = 0
                    else:
                        # No existe logica para otros tipos de comprobantes
                        pass
            
                    _s_proceso = "CFDI-Concepto%s.20.000: Se inserta el concepto del CFDI" % str(_n_indice) 
                    _D_returnsData = _insertar_json(
                        _D_cfdi_concepto, 
                        _D_campos_relacion_conceptos, 
                        dbc01.tcfdi_conceptos, 
                        _D_fieldsInsertConcepto,
                        )
                    
                    if _D_returnsData.n_return != stvfwk2_e_RETURN.OK:
                        # Si existen errores en la creación del cfdi
                        _D_erroresPrioridad.critica(_D_returnsData, _s_proceso)
                        _n_cfdi_concepto_id = None
                    else:
                        # Si no hay errores en la creación del CFDI continua la inserción de conceptos, pagos y relacionados
                        _n_cfdi_concepto_id = _D_returnsData.n_id
                        _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
            
                        ###
                        ### Se asocian los impuestos
                        ###
                        
                        _D_erroresPrioridad.agregar(
                            _importar_cfdi_concepto_impuestos(
                                _n_cfdi_concepto_id, 
                                _D_cfdi_concepto, 
                                dbc01.tcfdi_concepto_impuestostrasladados, 
                                'conceptoimpuestostraslados', 
                                ('%s-ImpTrasladados' % str(_n_indice))
                                )
                            )
                        _D_erroresPrioridad.agregar(
                            _importar_cfdi_concepto_impuestos(
                                _n_cfdi_concepto_id, 
                                _D_cfdi_concepto, 
                                dbc01.tcfdi_concepto_impuestosretenidos, 
                                'conceptoimpuestosretenciones', 
                                ('%s-ImpRetenidos' % str(_n_indice))
                                )
                            )
                        
            else:
                _D_returnsData = D_NOTIFICACIONMENSAJE(
                    n_id = 0, 
                    n_return = stvfwk2_e_RETURN.OK, 
                    s_msgError = "Producto o servicio asociado es nulo", 
                    s_msgExcepcion = "tipocomprobante %s" % str(D_fieldsInsertCfdi.tipocomprobante_id), 
                    s_msgLastSQL = "",
                    s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.VALIDAR % (str(dbc01.tcfdi_conceptos), "claveprodserv", _D_cfdi_concepto['claveprodserv'], "", "", ""),
                    )    
                _D_erroresPrioridad.media(_D_returnsData, _s_proceso)

        else:
            _D_returnsData = D_NOTIFICACIONMENSAJE(
                n_id = 0, 
                n_return = stvfwk2_e_RETURN.OK, 
                s_msgError = "No hay error, tipo de comprobante no debe tener conceptos asociados", 
                s_msgExcepcion = "tipocomprobante %s" % str(D_fieldsInsertCfdi.tipocomprobante_id), 
                s_msgLastSQL = "",
                s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.VALIDAR % (str(dbc01.tcfdi_conceptos), "tipocomprobante", D_cfdi['tipodecomprobante'], "", "", ""),
                )
            _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
            
    return _D_erroresPrioridad

def _importar_cfdi_concepto_impuestos(n_cfdi_concepto_id, D_cfdi_concepto, dbTabla, s_cfdiIndex, s_procesoSeccion = 'Imp'):
    """ Valida la informacion e inserta los impuestos de los conceptos de un CFDI

    Return:
        D_RETURN_ERRORES_PRIORIDAD
        
    """    
    _D_erroresPrioridad = D_RETURN_ERRORES_PRIORIDAD() 
    
        # Se relacionan los nombres en el json con los campos en la tabla tcfdi_conceptos
    _D_campos_relacion_impuestos = {
        'base' :            Storage(dbField = dbTabla.base, fnValidation = None),
        'impuesto' :        Storage(dbField = dbTabla.impuesto, fnValidation = None),
        'tipofactor' :      Storage(dbField = dbTabla.tipofactor, fnValidation = None),
        'tasaocuota' :      Storage(dbField = dbTabla.tasaocuota, fnValidation = None),
        'importe' :         Storage(dbField = dbTabla.importe, fnValidation = None),
        }
    
    if (s_cfdiIndex in D_cfdi_concepto) and D_cfdi_concepto[s_cfdiIndex]:
        
        _n_indice = 0
        _s_proceso = ""
        
        for _D_cfdi_impuesto in D_cfdi_concepto[s_cfdiIndex]:
    
            _n_indice += 1
            
            # Se agregan los campos standard con sus valores
            _D_fieldsInsertImpuesto = Storage(
                cfdi_concepto_id = n_cfdi_concepto_id
                )
    
            # Se asocia el impuesto con la tabla
            _s_proceso = "CFDI-Concepto%s%s.10.010: Se asocia el impuesto" % (str(s_procesoSeccion),str(_n_indice)) 
            _s_cache_id = 'cfdi_asociar_impuesto' + str(_D_cfdi_impuesto['impuesto'])
            _D_returnsData = cache.ram(
                _s_cache_id, 
                lambda:_asociar_tabla(
                    db01.timpuestos, 
                    'c_impuesto', 
                    _D_cfdi_impuesto['impuesto'],
                    ), 
                time_expire = 86400
                )
            if _D_returnsData.n_return == stvfwk2_e_RETURN.OK:
                _D_fieldsInsertImpuesto.impuesto_id = _D_returnsData.n_id
                _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
            else:
                cache.ram(_s_cache_id, None)
                _D_fieldsInsertImpuesto.impuesto_id = None
                _D_erroresPrioridad.alta(_D_returnsData, _s_proceso)
                
            _s_proceso = "CFDI-Concepto%s%s.20.000: Se inserta el impuesto en el CFDI" % (str(s_procesoSeccion),str(_n_indice)) 
            _D_returnsData = _insertar_json(
                _D_cfdi_impuesto, 
                _D_campos_relacion_impuestos, 
                dbTabla, 
                _D_fieldsInsertImpuesto,
                )
                    
            if _D_returnsData.n_return != stvfwk2_e_RETURN.OK:
                # Si existen errores en la creación del cfdi
                _D_erroresPrioridad.critica(_D_returnsData, _s_proceso)
            else:
                # Si no hay errores en la creación del CFDI continua la inserción de conceptos, pagos y relacionados
                _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
                pass
    else:
        # No insertar nada
        pass
    
    return _D_erroresPrioridad

def _importar_cfdi_xml(n_cfdi_id, s_xml, s_json, D_fieldsInsertCfdi):
    """ Inserta el XML
    
    Return:
        D_RETURN_ERRORES_PRIORIDAD
        
    """    
    _D_erroresPrioridad = D_RETURN_ERRORES_PRIORIDAD()
    
    if s_xml:
        
        # Se agregan los campos standard con sus valores
        _D_fieldsInsertXML = Storage(
            cfdi_id = n_cfdi_id,
            xml_cfdi = s_xml,
            json_cfdi = s_json,
            )

        # Se inserta el XML
        _s_proceso = "CFDI-XML-%s.20.000: Se inserta el XML del CFDI" 
        _D_returnsData = _insertar_json(
            {}, 
            {}, 
            dbc02.tcfdi_xmls, 
            _D_fieldsInsertXML,
            s_proceso = _s_proceso
            )
        
        if _D_returnsData.n_return != stvfwk2_e_RETURN.OK:
            # Si existen errores en la creación del cfdi
            _D_erroresPrioridad.critica(_D_returnsData, _s_proceso)
        else:
            # Si no hay errores
            _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
            pass
    else:
        # No se inserta nada
        pass
                
    return _D_erroresPrioridad

def _importar_cfdi_relacionados(n_cfdi_id, D_cfdi, D_fieldsInsertCfdi):
    """ Valida la informacion e inserta los relacionados de un CFDI
    
    Return:
        D_RETURN_ERRORES_PRIORIDAD
        
    """    
    _D_erroresPrioridad = D_RETURN_ERRORES_PRIORIDAD()

    # Se relacionan los nombres en el json con los campos en la tabla tcfdi_conceptos
    _D_campos_relacion_relacionados = {
        'uuid' :                Storage(dbField = dbc01.tcfdi_relacionados.uuid, fnValidation = None),
        }
    
    if D_cfdi['cfdirelacionados']['relacionado']:
        
        _n_indice = 0
        _s_proceso = ""

        for _D_cfdi_relacionado in D_cfdi['cfdirelacionados']['relacionado']:
    
            _n_indice += 1

            # Se agregan los campos standard con sus valores
            _D_fieldsInsertRelacionado = Storage(
                cfdi_id = n_cfdi_id
                )
    
            # Se asocia el producto o servicio con la tabla
            _s_proceso = "CFDI-Relacionados-%s.10.010: Se asocia el CFDI relacionado" % (str(_n_indice)) 
            _D_returnsData = _asociar_tabla(
                dbc01.tcfdis, 
                'uuid', 
                _D_cfdi_relacionado['uuid'],
                )
            if _D_returnsData.n_return == stvfwk2_e_RETURN.OK:
                _D_fieldsInsertRelacionado.cfdirelacionado_id = _D_returnsData.n_id
                _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
            else:
                _D_fieldsInsertRelacionado.cfdirelacionado_id = None
                _D_erroresPrioridad.alta(_D_returnsData, _s_proceso)
        
            _s_proceso = "CFDI-Relacionados-%s.20.000: Se inserta el relacionado en el CFDI" % (str(_n_indice)) 
            _D_returnsData = _insertar_json(
                _D_cfdi_relacionado, 
                _D_campos_relacion_relacionados, 
                dbc01.tcfdi_relacionados, 
                _D_fieldsInsertRelacionado,
                s_proceso = _s_proceso
                )
            
            if _D_returnsData.n_return != stvfwk2_e_RETURN.OK:
                # Si existen errores en la creación del cfdi
                _D_erroresPrioridad.critica(_D_returnsData, _s_proceso)
            else:
                # Si no hay errores
                _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
                pass
    else:
        # No se inserta nada
        pass
                
    return _D_erroresPrioridad

def _importar_cfdi_pagos(n_cfdi_id, D_cfdi, s_empresa_id, D_fieldsInsertCfdi):
    """ Valida la informacion e inserta los pagos de un CFDI
    
    Return:
        D_RETURN_ERRORES_PRIORIDAD
        
    """    
    _D_erroresPrioridad = D_RETURN_ERRORES_PRIORIDAD()
    
    _s_proceso = "CFDI-Pagos.00.000: Identificar pagos"  

    if (('pago' in D_cfdi)
        and (D_cfdi['pago']) 
        and ('formapagop' in D_cfdi['pago'])
        and (D_cfdi['pago']['formapagop'])):

        if (D_fieldsInsertCfdi.tipocomprobante_id in (TTIPOSCOMPROBANTES.EGRESO,TTIPOSCOMPROBANTES.INGRESO)):
            _D_erroresPrioridad.media(D_NOTIFICACIONMENSAJE(
                n_id = 0,
                s_msgError = "Existen pagos para tipos de comprobantes diferentes a Pago",
                s_msgExcepcion = "",
                s_msgLastSQL = "",
                n_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS,
                s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.VALIDAR % (str(dbc01.tcfdi_complementopagos), 'pago:formapago', D_cfdi['pago']['formapagop'], "", "", ""),
                s_proceso = _s_proceso
                )
            )
        else:
            # No hay problema, es un coprobante de pago, debe incluir pagos
            pass

        # Se relacionan los nombres en el json con los campos en la tabla tcfdi_conceptos
        _D_campos_relacion_pagos = {
            'version' :         Storage(dbField = dbc01.tcfdi_complementopagos.versionpago, fnValidation = None),
            'fechapago' :       Storage(dbField = dbc01.tcfdi_complementopagos.fechapago_str, fnValidation = None),
            'formapagop' :       Storage(dbField = dbc01.tcfdi_complementopagos.formapago, fnValidation = None),
            'monedap' :          Storage(dbField = dbc01.tcfdi_complementopagos.moneda, fnValidation = None),
            'tipocambiop' :      Storage(dbField = dbc01.tcfdi_complementopagos.tipocambio, fnValidation = None),
            'monto' :           Storage(dbField = dbc01.tcfdi_complementopagos.monto, fnValidation = None),
            }
        
        # Se agregan los campos standard con sus valores
        _D_fieldsInsertPago = Storage(
            cfdi_id = n_cfdi_id
            )

        _s_proceso = "CFDI-Pagos.10.010: Identificar fecha de pago"  
        if D_cfdi['pago']['fechapago']:
            try:
                _D_fieldsInsertPago.fechapago = datetime.datetime.strptime(D_cfdi['pago']['fechapago'], '%Y-%m-%dT%H:%M:%S')
            except Exception as _O_excepcion:
                _D_erroresPrioridad.alta(D_NOTIFICACIONMENSAJE(
                    n_id = 0,
                    s_msgError = ('No se pudo asociar la fecha de timbrado %s' % D_cfdi['pago']['fechapago']),
                    s_msgExcepcion = ('Fecha Pago:%s' % str(_O_excepcion)),
                    s_msgLastSQL = '',
                    n_return = stvfwk2_e_RETURN.NOK_ERROR,
                    s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.VALIDAR % (str(dbc01.tcfdi_complementopagos), 'fechapago', D_cfdi['pago']['fechapago'], "", "", ""),
                    s_proceso = _s_proceso
                    )
                )
        else:
            _D_erroresPrioridad.alta(D_NOTIFICACIONMENSAJE(
                n_id = 0,
                s_msgError = "Fecha de pago no esta definida en el cfdi",
                s_msgExcepcion = "",
                s_msgLastSQL = "",
                n_return = stvfwk2_e_RETURN.NOK_ERROR,
                s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.VALIDAR % (str(dbc01.tcfdi_complementopagos), 'fechapago', D_cfdi['pago']['fechapago'], "", "", ""),
                s_proceso = _s_proceso
                )
            )

        # Se asocia la forma de pago con la tabla
        _s_proceso = "CFDI-Pagos.20.010: Asociar forma de pago" 
        _s_cache_id = 'cfdi_asociar_formapago' + str(D_cfdi['pago']['formapagop'])
        _D_returnsData = cache.ram(
            _s_cache_id, 
            lambda:_asociar_tabla(
                db01.tformaspago, 
                'c_formapago', 
                D_cfdi['pago']['formapagop'],
                ), 
            time_expire = 86400
            )
        if _D_returnsData.n_return == stvfwk2_e_RETURN.OK:
            _D_fieldsInsertPago.formapago_id = _D_returnsData.n_id
            _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
        else:
            cache.ram(_s_cache_id, None)
            _D_fieldsInsertPago.formapago_id = None
            _D_erroresPrioridad.alta(_D_returnsData, _s_proceso)
            
        # Se asocia la moneda con la tabla
        _s_proceso = "CFDI-Pagos.20.020: Asociar moneda" 
        _s_cache_id = 'cfdi_asociar_moneda' + str(D_cfdi['pago']['monedap'])
        _D_returnsData = cache.ram(
            _s_cache_id, 
            lambda:_asociar_tabla(
                db01.tmonedas, 
                'c_moneda', 
                D_cfdi['pago']['monedap'],
                ), 
            time_expire = 86400
            )
        if _D_returnsData.n_return == stvfwk2_e_RETURN.OK:
            _D_fieldsInsertPago.moneda_id = _D_returnsData.n_id
            _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
            
            _s_cache_id = 'cfdi_asociar_moneda' + str(s_empresa_id) + 'contpaqi' + str(_D_fieldsInsertPago.moneda_id)
            _D_returnsData = cache.ram(
                _s_cache_id, 
                lambda:_asociar_tabla(
                    dbc01.tempresa_monedascontpaqi, 
                    'moneda_id',
                    _D_fieldsInsertPago.moneda_id,
                    qry_maestro = (dbc01.tempresa_monedascontpaqi.empresa_id == s_empresa_id),
                    ), 
                time_expire = 86400
                )        
            
            if _D_returnsData.n_return == stvfwk2_e_RETURN.OK:
                _D_fieldsInsertPago.monedacontpaqi_id = _D_returnsData.n_id
                _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
            else:
                cache.ram(_s_cache_id, None)
                _D_fieldsInsertPago.monedacontpaqi_id = None
                _D_erroresPrioridad.alta(_D_returnsData, _s_proceso)
                                
        else:
            cache.ram(_s_cache_id, None)
            _D_fieldsInsertPago.moneda_id = None
            _D_erroresPrioridad.alta(_D_returnsData, _s_proceso)
    
        _s_proceso = "CFDI-Pagos.30.000: Insertar pago maestro de CFDI" 
        _D_returnsData = _insertar_json(
            D_cfdi['pago'], 
            _D_campos_relacion_pagos, 
            dbc01.tcfdi_complementopagos, 
            _D_fieldsInsertPago,
            )
        
        if _D_returnsData.n_return != stvfwk2_e_RETURN.OK:
            # Si existen errores en la creación del cfdi
            _D_erroresPrioridad.critica(_D_returnsData, _s_proceso)
            _cfdi_complementopago_id = None
        elif D_cfdi['pago']['doctorelacionado']:
            # Si no hay errores se continua con el registro de pagos
            _cfdi_complementopago_id = _D_returnsData.n_id
            _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
            
            # Se relacionan los nombres en el json con los campos en la tabla tcfdi_conceptos
            _D_campos_relacion_pago_doctosrelacionados = {
                'iddocumento' :     Storage(dbField = dbc01.tcfdi_complementopago_doctosrelacionados.iddocumento, fnValidation = None),
                'serie' :           Storage(dbField = dbc01.tcfdi_complementopago_doctosrelacionados.serie, fnValidation = None),
                'folio' :           Storage(dbField = dbc01.tcfdi_complementopago_doctosrelacionados.folio, fnValidation = None),
                'monedadr' :          Storage(dbField = dbc01.tcfdi_complementopago_doctosrelacionados.moneda, fnValidation = None),
                'tipocambiodr' :      Storage(dbField = dbc01.tcfdi_complementopago_doctosrelacionados.tipocambio, fnValidation = None),
                'metodopagodr' :      Storage(dbField = dbc01.tcfdi_complementopago_doctosrelacionados.metodopago, fnValidation = None),
                'numparcialidad' :  Storage(dbField = dbc01.tcfdi_complementopago_doctosrelacionados.numparcialidad, fnValidation = None),
                'impsaldoant' :     Storage(dbField = dbc01.tcfdi_complementopago_doctosrelacionados.impsaldoant, fnValidation = None),
                'imppagado' :       Storage(dbField = dbc01.tcfdi_complementopago_doctosrelacionados.imppagado, fnValidation = None),
                'impsaldoinsoluto' : Storage(dbField = dbc01.tcfdi_complementopago_doctosrelacionados.impsaldoinsoluto, fnValidation = None),
                }
            
            _n_indice = 0
            
            for _D_cfdi_pago_doctorelacionado in D_cfdi['pago']['doctorelacionado']:
            
                _n_indice += 1
                
                # Se agregan los campos standard con sus valores
                _D_fieldsInsertPagoDoctoRelacionado = Storage(
                    cfdi_complementopago_id = _cfdi_complementopago_id
                    )

                # Se asocia el producto o servicio con la tabla
                _s_proceso = "CFDI-Pagos-Pago%s.10.010: Asociar documento relacionado del pago" % str(_n_indice) 
                _D_returnsData = _asociar_tabla(
                    dbc01.tcfdis, 
                    'uuid', 
                    _D_cfdi_pago_doctorelacionado['iddocumento'],
                    )
                if _D_returnsData.n_return == stvfwk2_e_RETURN.OK:
                    _D_fieldsInsertPagoDoctoRelacionado.cfdirelacionado_id = _D_returnsData.n_id
                    _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
                else:
                    _D_fieldsInsertPagoDoctoRelacionado.cfdirelacionado_id = None
                    _D_erroresPrioridad.alta(_D_returnsData, _s_proceso)
        
                # Se asocia la serie con la tabla
                _s_proceso = "CFDI-Pagos-Pago%s.10.020: Asociar serie del pago" % str(_n_indice) 
                _s_cache_id = 'cfdi_asociar_empresa' + str(s_empresa_id) + 'serie' + str(_D_cfdi_pago_doctorelacionado['serie'])
                _D_returnsData = cache.ram(
                    _s_cache_id, 
                    lambda:_asociar_tabla(
                        dbc01.tempresa_series, 
                        'serie', 
                        _D_cfdi_pago_doctorelacionado['serie'],
                        qry_maestro = (dbc01.tempresa_series.empresa_id == s_empresa_id),
                        ), 
                    time_expire = 86400
                    )
                if _D_returnsData.n_return == stvfwk2_e_RETURN.OK:
                    _D_fieldsInsertPagoDoctoRelacionado.empresa_serie_id = _D_returnsData.n_id
                    _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
                else:
                    cache.ram(_s_cache_id, None)
                    _D_fieldsInsertPagoDoctoRelacionado.empresa_serie_id = None
                    _D_erroresPrioridad.alta(_D_returnsData, _s_proceso)

                # Se asocia la moneda con la tabla
                _s_proceso = "CFDI-Pagos-Pago%s.10.030: Asociar moneda del pago" % str(_n_indice) 
                _s_cache_id = 'cfdi_asociar_moneda' + str(_D_cfdi_pago_doctorelacionado['monedadr'])
                _D_returnsData = cache.ram(
                    _s_cache_id, 
                    lambda:_asociar_tabla(
                        db01.tmonedas, 
                        'c_moneda', 
                        _D_cfdi_pago_doctorelacionado['monedadr'],
                        ), 
                    time_expire = 86400
                    )
                if _D_returnsData.n_return == stvfwk2_e_RETURN.OK:
                    _D_fieldsInsertPagoDoctoRelacionado.moneda_id = _D_returnsData.n_id
                    _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
                    
                    _s_cache_id = 'cfdi_asociar_moneda' + str(s_empresa_id) + 'contpaqi' + str(_D_fieldsInsertPagoDoctoRelacionado.moneda_id)
                    _D_returnsData = cache.ram(
                        _s_cache_id, 
                        lambda:_asociar_tabla(
                            dbc01.tempresa_monedascontpaqi, 
                            'moneda_id',
                            _D_fieldsInsertPagoDoctoRelacionado.moneda_id,
                            qry_maestro = (dbc01.tempresa_monedascontpaqi.empresa_id == s_empresa_id),
                            ), 
                        time_expire = 86400
                        )        
                    
                    if _D_returnsData.n_return == stvfwk2_e_RETURN.OK:
                        _D_fieldsInsertPagoDoctoRelacionado.monedacontpaqi_id = _D_returnsData.n_id
                        _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
                    else:
                        cache.ram(_s_cache_id, None)
                        _D_fieldsInsertPagoDoctoRelacionado.monedacontpaqi_id = None
                        _D_erroresPrioridad.alta(_D_returnsData, _s_proceso)                
                    
                else:
                    cache.ram(_s_cache_id, None)
                    _D_fieldsInsertPagoDoctoRelacionado.moneda_id = None
                    _D_erroresPrioridad.alta(_D_returnsData, _s_proceso)                
                
                if _D_returnsData.n_return == stvfwk2_e_RETURN.OK:
                    _D_fieldsInsertPagoDoctoRelacionado.moneda_id = _D_returnsData.n_id
                    _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
                else:
                    cache.ram(_s_cache_id, None)
                    _D_fieldsInsertPagoDoctoRelacionado.moneda_id = None
                    _D_erroresPrioridad.alta(_D_returnsData, _s_proceso)

                # Se asocia el método de pago con la tabla
                _s_proceso = "CFDI-Pagos-Pago%s.10.040: Asociar método de pago del pago" % str(_n_indice) 
                _s_cache_id = 'cfdi_asociar_metodopago' + str(_D_cfdi_pago_doctorelacionado['metodopagodr'])
                _D_returnsData = cache.ram(
                    _s_cache_id, 
                    lambda:_asociar_tabla(
                        db01.tmetodospago, 
                        'c_metodopago', 
                        _D_cfdi_pago_doctorelacionado['metodopagodr'],
                        ), 
                    time_expire = 86400
                    )
                if _D_returnsData.n_return == stvfwk2_e_RETURN.OK:
                    _D_fieldsInsertPagoDoctoRelacionado.metodopago_id = _D_returnsData.n_id
                    _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
                else:
                    cache.ram(_s_cache_id, None)
                    _D_fieldsInsertPagoDoctoRelacionado.metodopago_id = None
                    _D_erroresPrioridad.alta(_D_returnsData, _s_proceso)
                                
                _s_proceso = "CFDI-Pagos-Pago%s.20.000: Insertar información del pago en el CFDI" % str(_n_indice) 
                _D_returnsData = _insertar_json(
                    _D_cfdi_pago_doctorelacionado, 
                    _D_campos_relacion_pago_doctosrelacionados, 
                    dbc01.tcfdi_complementopago_doctosrelacionados, 
                    _D_fieldsInsertPagoDoctoRelacionado,
                    )
                
                if _D_returnsData.n_return != stvfwk2_e_RETURN.OK:
                    # Si existen errores en la creación del cfdi
                    _D_erroresPrioridad.critica(_D_returnsData, _s_proceso)
                else:
                    # Si no hay errores continuar
                    _D_erroresPrioridad.informacion(_D_returnsData, _s_proceso)
                    pass
                
        else:
            # No existieron documentos relacionados a los pagos
            _D_erroresPrioridad.alta(D_NOTIFICACIONMENSAJE(
                n_id = 0,
                s_msgError = "El pago no genero documentos relacionados",
                s_msgExcepcion = "",
                s_msgLastSQL = "",
                n_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS,
                s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.VALIDAR % (str(dbc01.tcfdi_complementopagos), 'pago:doctorelacionado', D_cfdi['pago']['doctorelacionado'], "", "", ""),
                s_proceso = _s_proceso
                )
            )
    else:
        # Si no existe pago
        if (D_fieldsInsertCfdi.tipocomprobante_id in (TTIPOSCOMPROBANTES.PAGO,)):
            _D_erroresPrioridad.alta(D_NOTIFICACIONMENSAJE(
                n_id = 0,
                s_msgError = "Información sobre el comprobante de pago no existe",
                s_msgExcepcion = "",
                s_msgLastSQL = "",
                n_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS,
                s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.VALIDAR % (str(dbc01.tcfdi_complementopagos), 'pago:formapago', D_cfdi['pago']['formapagop'], "", "", ""),
                s_proceso = _s_proceso
                )
            )
        else:
            # No hay razon para que existe un pago
            pass

                
    return _D_erroresPrioridad



@service.soap('MyAdd', returns = {'result':int}, args = {'a':int, 'b':int, })
def add(a, b):
    return a + b


def call():
    # session.forget()
    response.namespace = 'https://stverticales.mx/'
    return service()

