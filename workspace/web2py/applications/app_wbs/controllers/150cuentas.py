# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

# https://p01.stverticales.mx/app_wbs/150cuentas/call/soap?WSDL
# http://127.0.0.1:8000/app_wbs/150cuentas/call/soap?WSDL

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
#########################################################################

@service.soap(
    'getrfcs', 
    returns = {
        'n_return':int, 
        's_msgError':str, 
        'json_t_rfcs':str
        },
    args = {
        'D_auth':wbsT_D_AUTH, 
        'D_attr':{}
        }
    )
def getrfcs(D_auth, D_attr): 
    """ Regresa una tabla con los RFCs y las cuenta asignada
    
    Ejemplo:
        
    """
    
    _json_t_rfcs = ""
    if not auth.is_logged_in():
        _n_return = stvfwk2_e_RETURN.NOK_LOGIN
        _s_msgError = ("Usuario no registrado")
    else:
        # Busca el formulario que corresponde a la aplicacion, controlador y funcion del request.
        _dbRows = db(
            (dbF.aplicacion_w2p == request.application )
            & (dbF.controlador_w2p == request.controller )
            & (dbF.funcion_w2p == getrfcs.__name__ )
            & (db.tformulario_permisos.formulario_id == dbF.id)
            & ( 
                (dbGP.record_id == db.tformulario_permisos.id) 
                & (dbGP.group_id.belongs(auth.user_groups.keys()))
                )
            ).select(
                # Selecciona los campos requeridos
                dbF.id, 
                dbF.descripcion, 
                #dbF.nombrebotones,
                db.tformulario_permisos.id, 
                db.tformulario_permisos.descripcion, 
                db.tformulario_permisos.permiso_id, 
                db.tformulario_permisos.icono,
                db.tformulario_permisos.id
                )
        if not _dbRows:
            _n_return = stvfwk2_e_RETURN.NOK_PERMISOS
            _s_msgError = ("Usuario no tiene permisos")
        else:
            
            # Filtrar también si la cuenta esta suspendida
            ###TODO: Agregar filtro por cuenta_id
            _dbRows = db(
                (db.tcuenta_empresas.bansuspender == 0)
                ).select(
                    db.tcuenta_empresas.id,
                    db.tcuenta_empresas.cuenta_id,
                    db.tcuenta_empresas.empresa_id,
                    )
             
            # Se ajusta el retorno y el mensaje de error al principio, ya que en el for se actualiza el error. Simplifica el codigo
            _n_return = stvfwk2_e_RETURN.OK
            _s_msgError = ("")
            
            _L_rfcs = []
             
            for _dbRow in _dbRows:
                                     
                 
                # Regresar estado de cuenta con los meses pagados por RFC
                # TODO, solo regresar los ultimos dos años de pagos
                _dbRows_pagos = db(db.tcuenta_empresa_pagos.id == _dbRow.id).select(db.tcuenta_empresa_pagos.id, db.tcuenta_empresa_pagos.anio, db.tcuenta_empresa_pagos.mes, orderby=db.tcuenta_empresa_pagos.anio)
                _x_anio = None
                _D_pagos = {}
                for _dbRow_pago in _dbRows_pagos:
                    if (_x_anio != _dbRow_pago.anio):
                        _x_anio = _dbRow_pago.anio
                        _D_pagos[str(_x_anio)] = [str(_dbRow_pago.mes)]
                    else:
                        _D_pagos[str(_x_anio)] += [str(_dbRow_pago.mes)]
                
                # Regresar ultima FechaTimbrado por RFC
                _dbRows_ultimotimbre = dbc01(
                    dbc01.tcfdis.id == _dbRow.empresa_id
                    ).select(
                        dbc01.tcfdis.id, 
                        dbc01.tcfdis.fechatimbrado_str, 
                        dbc01.tcfdis.fechatimbrado, 
                        orderby=~dbc01.tcfdis.fechatimbrado,
                        limitby=(0,1)
                        )
                
                if _dbRows_ultimotimbre:
                    _s_lastfechatimbrado = str(_dbRows_ultimotimbre.first().fechatimbrado_str)
                else:
                    _s_lastfechatimbrado = None
                
                # TODO Regresar si hay request especiales
                
                _dbRows_empresaEnCuenta = dbc01(
                    dbc01.tempresas.id == _dbRow.empresa_id
                    ).select(
                        dbc01.tempresas.rfc, 
                        dbc01.tempresas.razonsocial, 
                        dbc01.tempresas.claveciec
                        )
                if len(_dbRows_empresaEnCuenta) == 1:
                    
                    _s_rfc = _dbRows_empresaEnCuenta.first().rfc
                    _s_claveciec = _dbRows_empresaEnCuenta.first().claveciec
                    
                    _L_rfcs.append(
                        Storage(
                            empresa_id = _dbRow.empresa_id, 
                            rfc = _s_rfc, 
                            razonsocial = _dbRows_empresaEnCuenta.first().razonsocial, 
                            claveciec = _s_claveciec, 
                            pagos = _D_pagos, 
                            lastfechatimbrado = _s_lastfechatimbrado
                            )
                        )                    
                    
                elif len(_dbRows_empresaEnCuenta) > 1:     
                    
                    _s_msgError = 'Error >1 cuenta '+str(_dbRow.cuenta_id)
                    _n_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
                
                else:
                    
                    _s_msgError = 'Error 0 cuenta '+str(_dbRow.cuenta_id)
                    _n_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
 
            _json_t_rfcs = str(
                simplejson.dumps(
                    {
                        'atributos':"",
                        'datos':_L_rfcs
                        }
                    )
                )

    return {
        'n_return' : _n_return, 
        's_msgError' : _s_msgError, 
        'json_t_rfcs' : _json_t_rfcs, 
        }


# Crear servicio de request especiales, donde se indique:
#   - Operación:
#        - Verificar si el registro en el sistema es correcto, subiendo todos los cfdis en el periodo indicado
#        - Checa por cancelaciones
#   - Periodo: del dia, mes y anio a dia mes y anio
#   - Filtrar: Opcionalmente lista de uuid y totales que se deben filtrar de la operación
#   - Seleccionar: Opcionalmente lista de uuid que se desea verificar

# Crear servicio de response especial, donde se suban los resultados del request especial, uno por uno
#   - Regresa un OK, condiciones incorrectas, timeout, etc.

# Crear servicio de stop especial, donde se finalize el request especial
#   - Opciones de stop son:
#       * No se pudo realizar request, intentarlo despues
#       * Request finalizado satisfactoriamente
#       * Parcialmente terminado, intentar después

# Crear app_wbs


@service.soap('MyAdd', returns={'result':int}, args={'a':int, 'b':int,})
def add(a, b):
    return a + b


def call():
    #session.forget()
    response.namespace = 'https://stverticales.mx/'
    return service()

