# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

# https://p01.stverticales.mx/app_wbs/100acceso/call/soap?WSDL
# http://127.0.0.1:8000/app_wbs/100acceso/call/soap?WSDL

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
#########################################################################


@service.soap('login', 
              returns={
                  'n_return':int, 
                  's_msgError':str, 
                  'D_auth':wbsT_D_AUTH,
                  'json_t_cuentas':str}, 
              args={
                  's_email':str, 
                  's_password':str, 
                  'D_attr':{}})
def login(s_email, s_password, D_attr):
    """
    
    Ejemplo:
        from gluon.contrib.pysimplesoap.client import SoapClient
        client = SoapClient(wsdl="http://127.0.0.1:8000/acceso/default/call/soap?WSDL")
        client.Login(s_email="jherrero@stverticales.mx",s_password="pass",D_attr={})
    """

    _json_t_cuentas = ""    
    if not auth.login_bare(username=str(s_email), password=str(s_password)):
        _s_msgError = u"Contraseña incorrecta, favor de intentar nuevamente"
        _n_return = stvfwk2_e_RETURN.NOK_LOGIN
        _D_auth = wbsT_D_AUTH
    else:        
        _D_sessionData = dict(locked=False,
                              client_ip = response.session_client,
                              modified_datetime = request.now,
                              unique_key = session.auth.hmac_key)

        _n_record_id = response.session_db_table.insert(**_D_sessionData)
        response.session_id = '%s:%s' % (_n_record_id, session.auth.hmac_key)
        response.session_db_unique_key = session.auth.hmac_key
        response.session_db_record_id = _n_record_id        
        response.session_new = False  # Para no cambiar la variable de sesion        
        
        _s_msgError = u""
        _n_return = stvfwk2_e_RETURN.OK

        _D_auth = Storage(wbsT_D_AUTH)
        _D_auth.s_token = str(response.session_id)
        _D_auth.s_signature = 'Signature'
        _D_auth.s_key = 'Llave publica'
        _D_auth.cuenta_aplicacion_id = 0
        _D_auth.cuenta_id = 0

        # Consulta para desplegar los privilegios a las diferentes aplicaciones
        #_db_rows = db((db.tcuenta_aplicacion_usuarios.user_id == auth.user_id)
        #              & (db.tcuenta_aplicaciones.id == db.tcuenta_aplicacion_usuarios.cuenta_aplicacion_id)
        #              & (db.tcuentas.id == db.tcuenta_aplicaciones.cuenta_id)
        #              ).select(db.tcuenta_aplicacion_usuarios.cuenta_aplicacion_id, 
        #                       db.tcuenta_aplicaciones.aplicacion_w2p,
        #                       db.tcuenta_aplicaciones.nombrecorto,
        #                       db.tcuentas.nombrecorto,
        #                       db.tcuentas.id)

        # Condulta para mostrar solo las cuentas que tiene el usuario
        _db_rows = db(
            (db.tcuenta_aplicacion_usuarios.user_id == auth.user_id)
            & (db.tcuenta_aplicaciones.id == db.tcuenta_aplicacion_usuarios.cuenta_aplicacion_id)
            & (db.tcuentas.id == db.tcuenta_aplicaciones.cuenta_id)
            & (db.taplicaciones.id == db.tcuenta_aplicaciones.aplicacion_id)
            & (db.taplicaciones.aplicacion_w2p == "app_wbs")
            ).select(
                db.tcuentas.id,
                db.tcuentas.nombrecorto,
                db.tcuenta_aplicaciones.id,
                db.tcuenta_aplicaciones.nombrecorto,
                distinct=True
                )

        # TODO prevenir seleccion de suspendidos o borrados lógicos
        _json_t_cuentas = str(simplejson.dumps({
            'atributos':"",
            'datos':_db_rows.as_list()
            }))
        


    return {'n_return':_n_return, 
            's_msgError':_s_msgError, 
            'D_auth':_D_auth,
            'json_t_cuentas':_json_t_cuentas}


@service.soap('logout', 
              returns={
                  'n_return':int, 
                  's_msgError':str}, 
              args={
                  'D_auth':wbsT_D_AUTH})
def logout(D_auth):
    
    if auth.is_logged_in():
        auth.settings.logout_next = None
        auth.logout()
        _s_msgError = u"Usuario estaba registrado"
        _n_return = stvfwk2_e_RETURN.OK
    else:
        _s_msgError = u"Usuario no estaba registrado"
        _n_return = stvfwk2_e_RETURN.OK

    return {'n_return':_n_return,
            's_msgError':_s_msgError}


@service.soap('verify', 
              returns={
                  'n_return':int,
                  's_token':str,
                  's_app':str}, 
              args={
                  'D_auth':wbsT_D_AUTH})
def verify(D_auth):
    """
    
    Ejemplo:
        client.Verify(D_auth = {'s_token': u'129:d038384f-3a50-4f7a-917d-ac0a2690d846', 's_signature': u'Signature', 's_key': u'Llave publica'})
    """
    
    _s_test = _s_body
    if auth.is_logged_in():
        _n_return = stvfwk2_e_RETURN.OK
    else:
        _n_return = stvfwk2_e_RETURN.NOK_LOGIN

    return {
            'n_return' : _n_return,
            's_token' : _testToken,
            's_app' : str(D_stvFwkCfg.s_nameAccesoApp),            
            }


@service.soap('MyAdd', returns={'result':int}, args={'a':int, 'b':int,})
def add(a, b):
    return a + b


def call():
    #session.forget()
    response.namespace = 'https://stverticales.mx/'
    return service()

