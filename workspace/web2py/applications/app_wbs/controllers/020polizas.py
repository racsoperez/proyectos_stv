# -*- coding: utf-8 -*-

# https://p01test.stverticales.mx/app_wbs/010acceso/call/soap?WSDL
# https://p01.stverticales.mx/app_wbs/010acceso/call/soap?WSDL
# http://127.0.0.1:8000/app_wbs/010acceso/call/soap?WSDL

# https://p01.stverticales.mx/app_wbs/020polizas/call/soap?WSDL


@service.soap(
    'polizas_pendientes',
    returns = {
        'n_return'  : int,
        's_msgError': str,
        'json_data' : str
        },
    args = {
        'D_auth'     : wbsT_D_AUTHv2,
        'json_params': str
        }
    )
def polizas_pendientes(D_auth, json_params):
    """
    
    Ejemplo:
        from gluon.contrib.pysimplesoap.client import SoapClient
        client = SoapClient(wsdl="http://127.0.0.1:8000/app_wbs/010acceso/call/soap?WSDL")
        client.Login(s_email="jherrero@stverticales.mx",s_password="pass",s_codigo="test",json_params="")
    """

    def validarDatoTabla(D_paramsTabla, dbTabla, s_preTexto = "El codigo ", D_codigosMensajes = None):
        _D_return = FUNC_RETURN()

        _D_codigosMensajes = {
            1: "",  # valor "" significa no hay mensaje ni error
            None: 'no fue encontrado'  # indice None significa cualquier otro valor
            }
        _D_codigosMensajes.update(D_codigosMensajes or {})

        for _n_index, _n_indicadorError in enumerate(D_paramsTabla['_L_msgsError']):

            if int(_n_indicadorError) in _D_codigosMensajes:
                if _D_codigosMensajes[_n_indicadorError]:
                    _D_return.agrega_error(
                        '{preTexto} {codigocontpaqi}{tablaSingular} {mensaje}. '.format(
                            preTexto = s_preTexto,
                            codigocontpaqi = str(D_paramsTabla['codigoscontpaqi'][_n_index]),
                            tablaSingular = (" de %s " % dbTabla._singular) if dbTabla else "",
                            mensaje = _D_codigosMensajes[_n_indicadorError]
                            )
                        )
                else:
                    # No hay mensaje de error, por lo que no hay error
                    pass
            else:
                # Si no encuentra el código, significa error
                _D_return.agrega_error(
                    'El codigo {codigocontpaqi} de {tablaSingular} {mensaje}'.format(
                        codigocontpaqi = str(D_paramsTabla['codigoscontpaqi'][_n_index]),
                        tablaSingular = dbTabla._singular,
                        mensaje = _D_codigosMensajes[None]
                        )
                    )

        return _D_return

    _D_return = FUNC_RETURN()

    _D_auth = Storage(D_auth)

    _s_msgError = ""
    _json_data = ""
    if not auth.is_logged_in():
        _s_msgError = u"No se identifica sesion activa"
        _n_return = stvfwk2_e_RETURN.NOK_LOGIN
    # elif not request.stv_fwk_permissions.hasPermission(
    #         request.stv_fwk_permissions.btn_none, ps_function = "polizas_pendientes"
    #         ):
    #     _s_msgError = u"No cuenta con permisos"
    #     _n_return = stvfwk2_e_RETURN.NOK_PERMISOS
    else:
        _n_return = stvfwk2_e_RETURN.OK
        
        #  D_auth.s_signature para determinar el nombre del servidor y obtener ejercicios, empresa_id, empresas,
        #   nombre base de datos
        #  - 2019,  1, ACULLAGSA, ctEMPRESA_SA_DE_CV_2019
        #  - 2020,  1, ACULLAGSA, ctEMPRESA_SA_DE_CV_2020
        #
        #  Usar el usuario para determinar si tiene acceso a aplicaciones y así a la
        #
        # _D_jsonparams = json.loads(json_params)

        _D_json = simplejson.loads(json_params)
        
        _D_params = Storage(_D_json)

        # Se configura la base de datos usada para la cuenta, despues de esta consulta dbc01 existe
        importar_dbs_cuenta(s_remplaza_cuenta_id = _D_params['t_stv_cuentas']['cuenta_id'])

        # Se revisan todos los registros que traigan las tablas y se verifica que se hayan encontrado todos.
        # _L_msgsError puede traer 1 o 0, 1 para decir que el código fue encontrado y
        #  0 para decir que no fue encontrado.
        _D_result = validarDatoTabla(_D_params['validacionescontpaqi']['t_tipospoliza'], dbc01.tempresa_tipospoliza)
        _D_return.combinar(_D_result)

        _D_result = validarDatoTabla(
            _D_params['validacionescontpaqi']['t_tipodocumentosbancarios'], dbc01.tempresa_tipodocumentosbancarios
            )
        _D_return.combinar(_D_result)

        _D_result = validarDatoTabla(_D_params['validacionescontpaqi']['t_diariosespeciales'], dbc01.tempresa_diarios)
        _D_return.combinar(_D_result)

        _D_result = validarDatoTabla(
            _D_params['validacionescontpaqi']['t_cuentasbancarias'], dbc01.tempresa_cuentasbancarias
            )
        _D_return.combinar(_D_result)

        _D_result = validarDatoTabla(_D_params['validacionescontpaqi']['t_segmentosnegocio'], dbc01.tempresa_segmentos)
        _D_return.combinar(_D_result)

        _D_result = validarDatoTabla(_D_params['validacionescontpaqi']['t_bancos'], dbc01.tempresa_bancoscontpaqi)
        _D_return.combinar(_D_result)

        _D_result = validarDatoTabla(_D_params['validacionescontpaqi']['t_monedas'], dbc01.tempresa_monedascontpaqi)
        _D_return.combinar(_D_result)

        _D_result = validarDatoTabla(
            _D_params['validacionescontpaqi']['t_cuentascontables'], None, s_preTexto = 'La cuenta contable ',
            D_codigosMensajes = {2: 'no es afectable'}
            )
        _D_return.combinar(_D_result)

        if _D_return.E_return <= CLASS_e_RETURN.OK_WARNING:
            # Si no se llenó el string de error entonces procede a hacer la lógica para obtener las
            _L_basesdedatos = []
            _L_empresas = []
            _L_polizas = []
            _s_empresa_id = None
            _s_empresacontpaqi = None

            _qry = (
                (dbc01.tempresa_contpaqiejercicios.cuenta_servidorcontpaqi_id
                 == _D_params['t_stv_cuentas']['servidor_id'])
                & (dbc01.tpolizas.empresa_id == dbc01.tempresa_contpaqiejercicios.empresa_id)
                & (dbc01.tpolizas.estatus == TPOLIZAS.ESTATUS.SIN_EXPORTAR)
                )

            _dbRows_polizas = dbc01(
                _qry
                & (dbc01.tpolizas.dia.year() == dbc01.tempresa_contpaqiejercicios.ejercicio)
                ).select(
                    dbc01.tempresa_contpaqiejercicios.ALL,
                    dbc01.tpolizas.ALL,
                    )
                
            for _dbRow_poliza in _dbRows_polizas:
                if _dbRow_poliza.tempresa_contpaqiejercicios.empresacontpaqi not in _L_basesdedatos:
                    # si la base de datos no se encuentra dentro de la lista
                    _L_basesdedatos.append(_dbRow_poliza.tempresa_contpaqiejercicios.empresacontpaqi)
                else:
                    # De lo contrario no hará nada
                    pass
            
            for _s_basededatos in _L_basesdedatos:
                _L_polizas = []
                _s_empresa_id = None
                _dbRow_rfc = ""
                for _dbRow_poliza in _dbRows_polizas:
                    
                    if _dbRow_poliza.tempresa_contpaqiejercicios.empresacontpaqi == _s_basededatos:
                        _L_polizas.append(_dbRow_poliza.tpolizas.id)
                        # TODO: Consolidar validaciones de Segmentos de negocio, tipos de póliza, diarios especiales,
                        #  monedas, tipo documentos bancarios y bancos.
                        if not _s_empresa_id:
                            _s_empresa_id = _dbRow_poliza.tempresa_contpaqiejercicios.empresa_id
                            _dbRow_rfc = dbc01.tempresas(_s_empresa_id).rfc
                        else:
                            pass
                    else:
                        pass
                
                _L_empresas.append(
                    Storage(
                        empresa_id = _s_empresa_id, empresacontpaqi = _s_basededatos,
                        polizas_cerradas_ids = _L_polizas, rfc = _dbRow_rfc,
                       )
                    )

            if _L_empresas:
                _json_data = str(
                    simplejson.dumps(Storage(datos = _L_empresas, s_error = ""), cls = STV_FWK_LIB.DecimalEncoder)
                    ) 
            else:
                _s_msgError = u"No se encontraron polizas pendientes."
        else:
            _s_msgError = u"Se econtraron inconsistencias en la verificación de los catalogos. " + _D_return.s_msgError
            _json_data = str(
                simplejson.dumps(Storage(datos = [], s_error = _s_msgError), cls = STV_FWK_LIB.DecimalEncoder)
                ) 
            _n_return = _D_return.E_return

    return {
        'n_return'  : _n_return,
        's_msgError': _s_msgError,
        'json_data' : _json_data
        }


@service.soap(
    'poliza_obtener',
    returns = {
        'n_return'  : int,
        's_msgError': str,
        'json_data' : str
        },
    args = {
        'D_auth'     : wbsT_D_AUTHv2,
        'json_params': str,
        'n_poliza_id': int
        }
    )
def poliza_obtener(D_auth, json_params, n_poliza_id):
    u""" Función para obtener la póliza 
    
    Ejemplo:
        from gluon.contrib.pysimplesoap.client import SoapClient
        client = SoapClient(wsdl="http://127.0.0.1:8000/app_wbs/010acceso/call/soap?WSDL")
        client.Login(s_email="jherrero@stverticales.mx",s_password="pass",s_codigo="test",json_params="")
    """
    _ = json_params
    _D_auth = Storage(D_auth)
    _D_poliza = None
    _s_msgError = ""
    _json_data = ""
    if not auth.is_logged_in():
        _s_msgError = u"Contraseña incorrecta, favor de intentar nuevamente"
        _n_return = stvfwk2_e_RETURN.NOK_LOGIN
    elif not request.stv_fwk_permissions.hasPermission(
            request.stv_fwk_permissions.btn_none, ps_function = "poliza_obtener"
            ):
        _s_msgError = u"No cuenta con permisos"
        _n_return = stvfwk2_e_RETURN.NOK_PERMISOS
    else:
        # TODO: Verificar el signature que sea compatible con la empresa y usuario.
        
        _n_return = stvfwk2_e_RETURN.OK
        
        # TODO, este es un error, si otra cuenta tiene el mismo id en la empresa, va a tronar
        importar_dbs(_D_auth.n_empresa_id)
        
        _dbTableDiarioAsiento = dbc01.tempresa_diarios.with_alias('tempresa_diarios_porasiento')
                                          
        _s_msgError = u"Consulta de información"
        
        # Variables para la suma de los cargos y abonos
        _n_cargos = 0
        _n_abonos = 0   
         
        # Se consiguen todas las prepolizas anidadas a la póliza ordenadas por su referencia.
        _dbRowsPoliza = dbc01(
            (dbc01.tcfdi_prepoliza_asientoscontables.poliza_id == n_poliza_id)
            & (dbc01.tpolizas.id == n_poliza_id)
            & (dbc01.tempresa_tipospoliza.id == dbc01.tpolizas.empresa_tipopoliza_id)
            & (dbc01.tempresa_diarios.id == dbc01.tpolizas.empresa_diario_id)
            ).select(
                dbc01.tpolizas.ALL,
                dbc01.tcfdi_prepoliza_asientoscontables.ALL,
                dbc01.tempresa_tipospoliza.id,
                dbc01.tempresa_tipospoliza.codigo,
                dbc01.tempresa_diarios.codigo,
                dbc01.tempresa_diarios.verificacioncontpaqi,
                dbc01.tcfdis.ALL,
                dbc01.tcfdi_registropagos.ALL,
                _dbTableDiarioAsiento.codigo,
                _dbTableDiarioAsiento.verificacioncontpaqi,
                dbc01.tempresa_segmentos.codigo,
                dbc01.tempresa_segmentos.verificacioncontpaqi,
                dbc01.tempresa_bancosdiariosespeciales.bancos_tipo_documentos_venta_contado,
                dbc01.tempresa_clientes.cliente_codigo_contpaqi,
                dbc01.tcfdi_complementopagos.ALL,
                dbc01.tempresa_tipopoliza_ejercicios.clasepoliza,
                dbc01.tempresa_tipopoliza_ejercicios.impresionpoliza,
                dbc01.tempresa_bancoscontpaqi.codigo_contpaqi,
                dbc01.tempresa_bancoscontpaqi.banco_id,
                dbc01.tempresa_monedascontpaqi.moneda_contpaqi,
                orderby= [
                    dbc01.tcfdi_prepoliza_asientoscontables.tipocreacion,
                    dbc01.tcfdi_prepoliza_asientoscontables.referencia,
                    dbc01.tcfdi_prepoliza_asientoscontables.id
                    ],
                left = [
                    dbc01.tcfdi_prepolizas.on(
                        dbc01.tcfdi_prepolizas.id == dbc01.tcfdi_prepoliza_asientoscontables.cfdi_prepoliza_id
                        ),
                    dbc01.tcfdis.on(
                        dbc01.tcfdis.id == dbc01.tcfdi_prepolizas.cfdi_id
                        ),                    
                    _dbTableDiarioAsiento.on(
                        _dbTableDiarioAsiento.id == dbc01.tcfdi_prepoliza_asientoscontables.empresa_diario_id
                        ),
                    dbc01.tempresa_segmentos.on(
                        dbc01.tempresa_segmentos.id == dbc01.tcfdi_prepoliza_asientoscontables.empresa_segmento_id
                        ),
                    dbc01.tcfdi_registropagos.on(
                        dbc01.tcfdi_registropagos.cfdi_id == dbc01.tcfdis.id
                        ),
                    # Este tabla solo tiene un registro por empresa ya que es de configuración.
                    dbc01.tempresa_bancosdiariosespeciales.on(
                        dbc01.tempresa_bancosdiariosespeciales.empresa_id == dbc01.tcfdis.empresa_id
                        ),
                    dbc01.tempresa_clientes.on(
                        dbc01.tempresa_clientes.id == dbc01.tcfdis.receptorcliente_id
                        ),
                    dbc01.tcfdi_complementopagos.on(
                        dbc01.tcfdi_complementopagos.cfdi_id == dbc01.tcfdis.id
                        ),
                    dbc01.tempresa_tipopoliza_ejercicios.on(
                        (dbc01.tempresa_tipopoliza_ejercicios.empresa_tipopoliza_id == dbc01.tempresa_tipospoliza.id)
                        & (dbc01.tempresa_tipopoliza_ejercicios.ejercicio == dbc01.tpolizas.dia.year())
                        ),
                    dbc01.tempresa_bancoscontpaqi.on(
                        (dbc01.tempresa_bancoscontpaqi.id == dbc01.tcfdi_registropagos.bancocontpaqi_id_cliente)
                        ),
                    dbc01.tempresa_monedascontpaqi.on(
                        (dbc01.tempresa_monedascontpaqi.empresa_id == dbc01.tcfdis.empresa_id)
                        & (dbc01.tempresa_monedascontpaqi.moneda_id == dbc01.tcfdis.moneda_id)
                        )
                    ],
                )

        # Manera de ejecutar una sentencia SQL
        # _dbRows_prueba = dbc01.executesql(_dbRowsPoliza)

        _D_poliza = Storage()
        # TODO: Saber distinción si es de cliente o proveedor la póliza
        if not _dbRowsPoliza:
            _s_msgError = u"Referencia a poliza y/o tipo poliza no existe"
            _n_return = stvfwk2_e_RETURN.NOK_ERROR
            _D_poliza = None
        else:
            _s_msgError = u"POLIZA"
             
            _n_folio = None  # El folio se pone como Nulo ya que en el SDK se dará el folio consecutivo.
            _L_listaDeUUIDs = []
            _n_ultimoCFDIusado = None
            _n_ultimoAsientousado = None

            _D_validacionescontpaqi = Storage(
                t_clientes = Storage(
                    razonsocial = [],
                    rfcs = [], 
                    codigoscontpaqi = []
                    )
                )

            # TODO: Aún que esté vacío mandar un array de vacíos junto con su registro de póliza
            _D_poliza_encabezado = Storage(
                # Encabezado de la póliza
                poliza_id = None,
                poliza = None,
                fecha = None,
                tipopoliza = None,
                folio = None,
                clase = None,
                diarioespecial = None,
                concepto = None,
                origen = None,
                poliza_impresa = None,
                ajuste = None,
                guid = None,
                periodo = None, 
                ejercicio = None
                )
            
            _D_poliza_cuerpo = Storage(
                # Cuerpo de la póliza
                movimientos = [],
                cuentascontables = [],
                referencias = [],
                cargoabonos = [],
                importes_cargos_abonos = [],
                diariosespeciales = [],
                importesmonedasextranjeras = [],
                descripciones = [],
                segmentosnegocio = [], 
                guids = [],
                uuids = []
                
                )
            
            _D_poliza_uuids = Storage(
                # ADD de la póliza
                polizauuids = [],
                )
            
            _D_poliza_documentosbancarios = Storage(
                # DocumentoBancario (IN) de la póliza.
                ingresos = [],
                idsdocumentosde = [],
                tipodocumentos = [],
                folios = [],
                fechas_documentosbancarios = [],
                fechasaplicacion = [],
                codigospersonas = [],
                beneficiariospagadores = [],
                idscuentascheques = [],
                codigosmonedas = [],
                totales = [],
                referencias = [],
                documentosdeorigen = [],
                bancosorigen = [],
                cuentasorigen = [],
                otrosmetodospago = [],
                guids = [],
                rfcs = [],
                bancosextranjeros = [],
                tiposcambios = [],
                numeroscheques = [],
                uuidsrep = [],
                nodospagos = [],
                codigosmonedas_tipocambio = [],
                # Variable con el ID del documento bancario para las ventas de contado
                bancos_tiposdocumentos_venta_contado = [],
                asientos_id = [],
                )
 
            # Se barren una por una las prepolizas
            for _dbRowPoliza in _dbRowsPoliza:
                
                if _n_ultimoAsientousado != _dbRowPoliza.tcfdi_prepoliza_asientoscontables.id:
                    _n_ultimoAsientousado = _dbRowPoliza.tcfdi_prepoliza_asientoscontables.id

                    if not _n_ultimoCFDIusado:
                        # Siempre que sea el primer asiento contable se agregará el encabezado                   
                        
                        # Póliza (Siempre va una P)
                        _s_msgError = u"POLIZA"
                       
                        _D_poliza_encabezado.poliza = u'P' 
        
                        # Fecha 8 char
                        _s_msgError = u"Fecha inválida"
                        
                        _D_poliza_encabezado.fecha = (_dbRowPoliza.tpolizas.fecha.strftime("%Y%m%d"))
                        
                        # Tipo de póliza 4 char
                        _s_msgError = u"Tipo de póliza inválida"
                        _D_poliza_encabezado.tipopoliza = str(_dbRowPoliza.tempresa_tipospoliza.codigo)
                        
                        # Folio poliza 9 char
                        _s_msgError = u"Folio de la póliza inválido"
                        _D_poliza_encabezado.folio = (str(_n_folio))
        
                        # Clase 1 char (Siempre lleva un 1 la clase)
                        _s_msgError = u"Clase inválida"
                        _D_poliza_encabezado.clase = (
                            u' ' + (str(_dbRowPoliza.tempresa_tipopoliza_ejercicios.clasepoliza))
                            )
                
                        # Diario especial 11 chars
                        _s_msgError = u"Diario especial inválido"
                        _D_poliza_encabezado.diarioespecial = (str(_dbRowPoliza.tempresa_diarios.codigo))

                        # Concepto 100 chars
                        _s_msgError = u"Concepto inválido"
    
                        _D_poliza_encabezado.concepto = (
                            str(_dbRowPoliza.tpolizas.concepto).replace('Ñ', 'N').replace('Á', 'A')
                            .replace('É', 'E').replace('Í', 'I').replace('Ó', 'O').replace('Ú', 'U')
                            .replace('ñ', 'n').replace('á', 'a').replace('é', 'e').replace('í', 'i')
                            .replace('ó', 'o').replace('ú', 'u')
                            )
                        
                        # Origen del sistema 2 chars (Siempre tiene que ser 11)
                        _D_poliza_encabezado.origen = u'11'
                        
                        # Impresa o no impresa (Siempre lleva 0)
                        _D_poliza_encabezado.poliza_impresa = (
                            str(_dbRowPoliza.tempresa_tipopoliza_ejercicios.impresionpoliza)
                            )
                        
                        # Poliza Ajuste 1 chars (siempre lleva 0)
                        _D_poliza_encabezado.ajuste = u'0'
                        
                        # GUID 36 chars (Identificador global, NO es el UUID)
                        _s_msgError = u"GUID inválido encabezado"
                        _D_poliza_encabezado.guid = u' '

                        _D_poliza_encabezado.poliza_id = n_poliza_id
                       
                        _D_poliza_encabezado.periodo = _dbRowPoliza.tpolizas.fecha.month
                       
                        _D_poliza_encabezado.ejercicio = _dbRowPoliza.tpolizas.fecha.year
                       
                        pass
                    else:
                        # Si no es el primer registro, ya no imprimas el encabezado
                        pass
                    
                    if _dbRowPoliza.tcfdis.id and (_dbRowPoliza.tcfdis.id != _n_ultimoCFDIusado):
                        _n_ultimoCFDIusado = _dbRowPoliza.tcfdis.id
                        _L_listaDeUUIDs.append(str(_dbRowPoliza.tcfdis.uuid))
                        
                    else:
                        # No hacer nada, hasta que cambie el cfdi id
                        pass
    
                    # Movimiento (Siempre lleva 'M1')
                    _D_poliza_cuerpo.movimientos.append(u'M1')
        
                    # Cuenta contable 30 chars
                    _s_msgError = u"Cuenta contable inválida"
                    _D_poliza_cuerpo.cuentascontables.append(
                        str(_dbRowPoliza.tcfdi_prepoliza_asientoscontables.cuenta_contable).replace('-', '')
                        )
        
                    # Referencia 20 chars
                    _s_msgError = u"Referencia inválida"
                    _D_poliza_cuerpo.referencias.append(str(_dbRowPoliza.tcfdi_prepoliza_asientoscontables.referencia))
                    
                    if _dbRowPoliza.tcfdi_prepoliza_asientoscontables.importe_cargo != 0:
                        _n_cargo_abono = 0
                        _s_importe_cargo_abono = str(
                            float(_dbRowPoliza.tcfdi_prepoliza_asientoscontables.importe_cargo)
                            )
                        _n_abonos += _dbRowPoliza.tcfdi_prepoliza_asientoscontables.importe_cargo
                        
                    elif _dbRowPoliza.tcfdi_prepoliza_asientoscontables.importe_abono != 0:
                        _n_cargo_abono = 1
                        _s_importe_cargo_abono = str(
                            float(_dbRowPoliza.tcfdi_prepoliza_asientoscontables.importe_abono)
                            )
                        _n_cargos += _dbRowPoliza.tcfdi_prepoliza_asientoscontables.importe_abono
    
                    else:  # Este else se hace en caso de que haya un cargo en la cuenta complemento.
                        _n_cargo_abono = 0
                        _s_importe_cargo_abono = "0"
                        
                    # Cargo/Abono Cargo = 0 y Abono = 1
                    _D_poliza_cuerpo.cargoabonos.append(str(_n_cargo_abono))
                    
                    # Importe cargo X chars
                    _s_msgError = u"Importe cargo/abono inválido"
                    _D_poliza_cuerpo.importes_cargos_abonos.append(_s_importe_cargo_abono)
        
                    # Diario especial X chars
                    _s_msgError = u"Diario especial inválido"
                    # TODO cual es el error aqui, ver ejemplos
                    if _dbRowPoliza.tcfdi_prepoliza_asientoscontables.empresa_diario_id == None:
                        _s_diario_especial = u'0'
                       
                    else: 
                        _s_diario_especial = str(_dbRowPoliza.tempresa_diarios_porasiento.codigo)
                    
                    _D_poliza_cuerpo.diariosespeciales.append(_s_diario_especial)
                   
                    # Importe moneda extranjera
                    _s_msgError = u"Importe moneda extranjera inválido"
                    _D_poliza_cuerpo.importesmonedasextranjeras.append(
                        str(float(_dbRowPoliza.tcfdi_prepoliza_asientoscontables.importe_moneda_extranjera))
                        )
        
                    # Descripción
                    _s_msgError = u"Descripción inválido"
                    _D_poliza_cuerpo.descripciones.append(
                        _dbRowPoliza.tcfdi_prepoliza_asientoscontables.descripcion.replace('Ñ', 'N')
                        .replace('Á', 'A').replace('É', 'E').replace('Í', 'I').replace('Ó', 'O')
                        .replace('Ú', 'U').replace('ñ', 'n').replace('á', 'a').replace('é', 'e')
                        .replace('í', 'i').replace('ó', 'o').replace('ú', 'u')
                        )
                    
                    # Segmento de negocio 1 chars
                    _s_msgError = u"Segmento de negocio inválido"
                    if _dbRowPoliza.tempresa_segmentos.codigo == None:
                        _s_segmento_negocio = ' '
                    else: 
                        _s_segmento_negocio = str(_dbRowPoliza.tempresa_segmentos.codigo)
                        
                    _D_poliza_cuerpo.segmentosnegocio.append(_s_segmento_negocio)
                   
                    _D_poliza_cuerpo.uuids.append(str(_dbRowPoliza.tcfdis.uuid))
                    
                    # GUID 100 chars (identificador global NO es el UUID)
                    _s_msgError = u"GUID inválido cuerpo"
                    _D_poliza_cuerpo.guids.append(' ')
                else:
                    pass
            
            _D_poliza_uuids.polizauuids = _L_listaDeUUIDs
            # Generación del asiento para documentos bancarios.
        
            _n_ultimoCFDIusado = None
            
            _n_utlimoRegistroPagoUsado = []

            # Se genera la ultima columna de pagos.
            for _dbRowPoliza in _dbRowsPoliza:
                
                if _dbRowPoliza.tcfdis.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO:
                    
                    _dbRowsDocumentoPago = dbc01(
                        (dbc01.tempresa_tipodocumentosbancarios.id
                         == _dbRowPoliza.tempresa_bancosdiariosespeciales.bancos_tipo_documentos_venta_contado)
                        ).select(
                            dbc01.tempresa_tipodocumentosbancarios.codigo_contpaqi
                            )
                      
                    if _dbRowsDocumentoPago:
                        _dbRowDocumentoPago = _dbRowsDocumentoPago.last().codigo_contpaqi
                    else:
                        _dbRowDocumentoPago = ''
                  
                    if (_n_ultimoCFDIusado != _dbRowPoliza.tcfdis.id) \
                            or (_dbRowPoliza.tcfdi_registropagos.id not in _n_utlimoRegistroPagoUsado):
                        
                        _s_informacionTXT = ''
                         
                        # Se asigna a la varible el ultimo cfdi utilizado
                        _n_ultimoCFDIusado = _dbRowPoliza.tcfdis.id
                      
                        # Se asigna a la variable el ultimo id de registro de pago usado. 
                        _n_utlimoRegistroPagoUsado.append(_dbRowPoliza.tcfdi_registropagos.id)

                        # Los siguientes datos está basado en el layout otorgado por Contpaqi
                        # Si la forma de pago es Por definir, Transeferencia, TDC, TDD entonces se usa el encabezado IN
                        if _dbRowPoliza.tcfdi_registropagos.formapago_id in (
                                TFORMASPAGO.TRANSFERENCIA,
                                TFORMASPAGO.TDC,
                                TFORMASPAGO.TDD,
                                TFORMASPAGO.CHEQUE,
                                ) \
                                or (
                                    (_dbRowPoliza.tcfdi_registropagos.formapago_id == TFORMASPAGO.EFECTIVO)
                                    and
                                    (_dbRowPoliza.tcfdi_registropagos.tratoefectivo
                                     == TCFDI_REGISTROPAGOS.E_TRATOEFECTIVO.DEPOSITADOBANCO)
                                    ):
                            
                            _n_folioPagos = None  # El folio del pago se conseguirá en el sdk
                            
                            # Ingreso char de 2
                            _D_poliza_documentosbancarios.ingresos.append(u'IN')
                            
                            # IdDocumentoDe char 5
                            # TODO, ver si se cambia
                            _s_msgError = u"IdDocumentoDe inválido."
                            _D_poliza_documentosbancarios.idsdocumentosde.append('04020')
                            
                            # TipoDocumento char 30
                            _s_msgError = u"TipoDocumento inválido."
                            # TODO: sacar el tipo de documento bancario
                            _D_poliza_documentosbancarios.tipodocumentos.append(str(_dbRowDocumentoPago))

                            # Folio char 20
                            _s_msgError = u"Folio inválido."
                            _D_poliza_documentosbancarios.folios.append(str(_n_folioPagos))
                             
                            # Fecha char 8
                            _s_msgError = u"Fecha inválida."
                            _D_poliza_documentosbancarios.fechas_documentosbancarios.append(
                                _dbRowPoliza.tpolizas.fecha.strftime("%Y%m%d")
                                )

                            # FechaAplicación char 8
                            # TODO: Ver si esta fecha está correcta aquí
                            _s_msgError = u"FechaAplicación inválida."
                            _D_poliza_documentosbancarios.fechasaplicacion.append(
                                _dbRowPoliza.tpolizas.fecha.strftime("%Y%m%d")
                                )
                             
                            # Código Persona char 6
                            _s_msgError = u"Código Persona inválido."
                            if _dbRowPoliza.tempresa_clientes.cliente_codigo_contpaqi == None:
                                _D_poliza_documentosbancarios.codigospersonas.append('')
                            else:
                                _D_poliza_documentosbancarios.codigospersonas.append(
                                    str(_dbRowPoliza.tempresa_clientes.cliente_codigo_contpaqi)
                                    )
                          
                            # Beneficiario pagador char 200
                            # TODO: ¿Este es el receptor o el emisor? De momento dejo el receptor
                            _s_msgError = u"Beneficiario pagador inválido."
                            _D_poliza_documentosbancarios.beneficiariospagadores.append(
                                str(_dbRowPoliza.tcfdis.receptornombrerazonsocial)
                                )
                          
                            if str(_dbRowPoliza.tcfdis.receptornombrerazonsocial) \
                                    not in _D_validacionescontpaqi.t_clientes.razonsocial:
                                _D_validacionescontpaqi.t_clientes.razonsocial.append(
                                    str(_dbRowPoliza.tcfdis.receptornombrerazonsocial)
                                    )
                                _D_validacionescontpaqi.t_clientes.rfcs.append(
                                    str(_dbRowPoliza.tcfdis.receptorrfc).replace('-', '')
                                    )
                            else:
                                pass
                        
                            # IdCuentaCheques char 20 (Cuenta bancaria a la que es mandada el dinero)
                            _s_msgError = u"IdCuentaCheques inválida."
                            if _dbRowPoliza.tcfdi_registropagos.formapago_id in (TFORMASPAGO.TDC, TFORMASPAGO.TDD,):
                                _dbRows_empresa_cuentasbancarias = dbc01(
                                    (dbc01.tempresa_cuentabancaria_terminales.id
                                     == _dbRowPoliza.tcfdi_registropagos.empresa_cuentabancaria_terminal_id)
                                    & (dbc01.tempresa_cuentasbancarias.id
                                       == dbc01.tempresa_cuentabancaria_terminales.empresa_cuentabancaria_id)
                                    ).select(
                                        dbc01.tempresa_cuentasbancarias.codigo_contpaqi
                                        ) 
                            else: 
                                _dbRows_empresa_cuentasbancarias = dbc01(
                                    (dbc01.tempresa_cuentasbancarias.id
                                     == _dbRowPoliza.tcfdi_registropagos.empresa_cuentabancaria_id)
                                    ).select(
                                        dbc01.tempresa_cuentasbancarias.codigo_contpaqi
                                        )
                        
                            if not _dbRows_empresa_cuentasbancarias:
                                _D_poliza_documentosbancarios.idscuentascheques.append('2')
                            else:
                                _D_poliza_documentosbancarios.idscuentascheques.append(
                                    str(_dbRows_empresa_cuentasbancarias.last().codigo_contpaqi)
                                    )

                            # Código Moneda char 4
                            # TODO: Hacer el código de contpaq para la moneda
                            _s_msgError = u"Código Moneda inválida."
                            _D_poliza_documentosbancarios.codigosmonedas.append(
                                str(_dbRowPoliza.tempresa_monedascontpaqi.moneda_contpaqi)
                                )
                                
                            # Total char 20
                            _s_msgError = u"Total inválido."
                            _D_poliza_documentosbancarios.totales.append(
                                str(float(_dbRowPoliza.tcfdi_registropagos.monto))
                                )
                                                         
                            # Referencia char 20
                            # Si la referencia está vacía se pone el folio
                            # TODO: Crear un folio auto incremental
                            _s_msgError = u"Referencia inválida."
                            _D_poliza_documentosbancarios.referencias.append('')
                            
                            # Origen char 5
                            # TODO: Origen del pago
                            _s_msgError = u"IdDocumentoDe Origen."
                            _D_poliza_documentosbancarios.documentosdeorigen.append('11')
                                                         
                            # Banco Origen char 30 
                            _s_msgError = u"Banco origen inválido."
                            if not _dbRowPoliza.tempresa_bancoscontpaqi.banco_id:
                                _D_poliza_documentosbancarios.bancosorigen.append(0)  # 0 es "ninguno" en CONTPAQi
                            else:
                                _dbRowBancoOrigen = db01.tbancos(_dbRowPoliza.tempresa_bancoscontpaqi.banco_id)
                                _D_poliza_documentosbancarios.bancosorigen.append(str(_dbRowBancoOrigen.c_banco))
     
                            # Cuenta Origen char 30
                            _s_msgError = u"Cuenta origen inválida."
                            if _dbRowPoliza.tcfdi_registropagos.cuenta_cliente == None:
                                _D_poliza_documentosbancarios.cuentasorigen.append('')
                            else:
                                _D_poliza_documentosbancarios.cuentasorigen.append(
                                    str(_dbRowPoliza.tcfdi_registropagos.cuenta_cliente)
                                    )

                            # Otro método de pago char 5
                            _s_msgError = u"Forma de pago inválida."
                          
                            _dbRow_formapago = db01.tformaspago(_dbRowPoliza.tcfdi_registropagos.formapago_id)
                            
                            if _dbRow_formapago:
                                _D_poliza_documentosbancarios.otrosmetodospago.append(
                                    str(_dbRow_formapago.codigo_contpaqi)
                                    )
                            else:
                                pass
                          
                            # GUID char 36 (identificador global NO es el UUID)
                            _s_msgError = u"GUID documento pago inválido." 
                            _D_poliza_documentosbancarios.guids.append('')  # No se hace append porque siempre es vacío
                                                         
                            # RFC char 13
                            # TODO: En el TXT aquí siempre viene vacío
                            # TODO: Mandar RFC
                            _s_msgError = u"RFC inválido."
                            _D_poliza_documentosbancarios.rfcs.append(str(_dbRowPoliza.tcfdis.receptorrfc))
                                                         
                            # BancoExtranjero char 60 
                            _s_msgError = u"BancoExtranjero inválido."
                            _D_poliza_documentosbancarios.bancosextranjeros.append('')
                                                         
                            # TipoCambio char 20
                            _s_msgError = u"TipoCambio inválido."
                            _D_poliza_documentosbancarios.tiposcambios.append(
                                str(float(_dbRowPoliza.tcfdis.tipocambio))
                                )
                                                         
                            # NúmeroCheque char 100
                            _s_msgError = u"NúmeroCheque inválido."
                            if _dbRowPoliza.tcfdi_registropagos.cheque == None:
                                _D_poliza_documentosbancarios.numeroscheques.append('')
                            else:
                                _D_poliza_documentosbancarios.numeroscheques.append(
                                    str(_dbRowPoliza.tcfdi_registropagos.cheque)
                                    )
 
                            # UUIDRep char 36
                            _s_msgError = u"UUIDRep inválido."
                            _D_poliza_documentosbancarios.uuidsrep.append('')
                                                         
                            # NodoPago char 3 (parece que aquí siempre va este dato)
                            _s_msgError = u"NodoPago inválido."
                            _D_poliza_documentosbancarios.nodospagos.append('0')
                                                         
                            # Código Moneda Tipo Cambio char 4 (parece que aquí siempre va este dato)
                            _s_msgError = u"Código Moneda Tipo Cambio inválido."
                            _D_poliza_documentosbancarios.codigosmonedas_tipocambio.append('2')
                          
                            # ID del documento bancario para las ventas de contado
                            # Este ID es importante ya que con él se podrá actualizar el folio del documento de pago.
                            _s_msgError = u"ID del documentobancario"
                            _D_poliza_documentosbancarios.bancos_tiposdocumentos_venta_contado.append(
                                _dbRowPoliza.tempresa_bancosdiariosespeciales.bancos_tipo_documentos_venta_contado
                                )

                            # Aquí se van a meter los ID de los asientos que se les va a actualizar con el folio
                            #  del documento de pago.
                            _D_poliza_documentosbancarios.asientos_id.append(
                                _dbRowPoliza.tcfdi_prepoliza_asientoscontables.id
                                )

                        else:
                            # Si no se identifica la forma de pago, no se inserta nada, y se continua
                            pass
                    else:
                        # Si el row aounta al mismo cfdi que ya fue impreso, se continua con el siguiente
                        pass
                    
                elif _dbRowPoliza.tcfdis.tipocomprobante_id == TTIPOSCOMPROBANTES.PAGO:
 
                    if (_n_ultimoCFDIusado != _dbRowPoliza.tcfdis.id) \
                            or (_dbRowPoliza.tcfdi_registropagos.id not in _n_utlimoRegistroPagoUsado):
                        if (
                                _dbRowPoliza.tcfdi_registropagos.formapago_id in (
                                    TFORMASPAGO.TRANSFERENCIA,
                                    TFORMASPAGO.TDC, TFORMASPAGO.TDD,
                                    TFORMASPAGO.CHEQUE,
                                    )
                                ) \
                                or (
                                    (_dbRowPoliza.tcfdi_registropagos.formapago_id == TFORMASPAGO.EFECTIVO)
                                    and
                                    (_dbRowPoliza.tcfdi_registropagos.tratoefectivo
                                     == TCFDI_REGISTROPAGOS.E_TRATOEFECTIVO.DEPOSITADOBANCO)
                                    ):
                       
                            _dbRowsDocumentoPago = dbc01(
                                (dbc01.tempresa_tipodocumentosbancarios.id
                                 == _dbRowPoliza.tempresa_bancosdiariosespeciales.bancos_tipo_documentos_venta_contado)
                                ).select(
                                    dbc01.tempresa_tipodocumentosbancarios.codigo_contpaqi
                                    )
                                  
                            if _dbRowsDocumentoPago:
                                _dbRowDocumentoPago = _dbRowsDocumentoPago.last().codigo_contpaqi
                            else:
                                _dbRowDocumentoPago = ''
                            
                            _dbRowsRegistroPagoComplemento = dbc01(
                                dbc01.tcfdi_registropagos.cfdi_complementopago_id
                                == _dbRowPoliza.tcfdi_complementopagos.id
                                ).select(dbc01.tcfdi_registropagos.ALL).last()
                                                     
                            # Se asigna a la varible el ultimo cfdi utilizado
                            _n_ultimoCFDIusado = _dbRowPoliza.tcfdis.id
                           
                            # Se asigna a la variable el ultimo id de registro de pago usado. 
                            _n_utlimoRegistroPagoUsado.append(_dbRowPoliza.tcfdi_registropagos.id)

                            _n_folioPagos = None  # El folio se sacará del SDK.
                           
                            # Ingreso char de 2
                            _D_poliza_documentosbancarios.ingresos.append(u'IN')
                                 
                            # IdDocumentoDe char 5
                            _s_msgError = u"IdDocumentoDe inválido."
                            _D_poliza_documentosbancarios.idsdocumentosde.append(u'04020')
                                 
                            # TipoDocumento char 30
                            _s_msgError = u"TipoDocumento inválido."
                            _D_poliza_documentosbancarios.tipodocumentos.append(str(_dbRowDocumentoPago))
                                 
                            # Folio char 20
                            _s_msgError = u"Folio inválido."
                            _D_poliza_documentosbancarios.folios.append(str(_n_folioPagos))
                                 
                            # Fecha char 8
                            _s_msgError = u"Fecha inválida."
                            _D_poliza_documentosbancarios.fechas_documentosbancarios.append(
                                _dbRowPoliza.tpolizas.fecha.strftime("%Y%m%d")
                                )
                                 
                            # FechaAplicación char 8
                            # TODO: Ver si esta fecha está correcta aquí
                            _s_msgError = u"FechaAplicación inválida."
                            _D_poliza_documentosbancarios.fechasaplicacion.append(
                                _dbRowPoliza.tpolizas.fecha.strftime("%Y%m%d")
                                )
                                 
                            # Código Persona char 6
                            _s_msgError = u"Código Persona inválido."
                            _D_poliza_documentosbancarios.codigospersonas.append(
                                str(_dbRowPoliza.tempresa_clientes.cliente_codigo_contpaqi)
                                )
                           
                            # Beneficiario pagador char 200
                            # TODO: ¿Este es el receptor o el emisor? De momento dejo el receptor
                            _s_msgError = u"Beneficiario pagador inválido."
                            _D_poliza_documentosbancarios.beneficiariospagadores.append(
                                str(_dbRowPoliza.tcfdis.receptornombrerazonsocial)
                                )
                                 
                            if str(_dbRowPoliza.tcfdis.receptornombrerazonsocial) \
                                    not in _D_validacionescontpaqi.t_clientes.razonsocial:
                                _D_validacionescontpaqi.t_clientes.razonsocial.append(
                                    str(_dbRowPoliza.tcfdis.receptornombrerazonsocial)
                                    )
                                _D_validacionescontpaqi.t_clientes.rfcs.append(
                                    str(_dbRowPoliza.tcfdis.receptorrfc).replace('-', '')
                                    )
                            else:
                                pass                       
                           
                            # IdCuentaCheques char 20 (Cuenta bancaria a la que es mandada el dinero)
                            _s_msgError = u"IdCuentaCheques inválida."
                            if _dbRowPoliza.tcfdi_registropagos.formapago_id in (TFORMASPAGO.TDC, TFORMASPAGO.TDD,):
                                _dbRows_empresa_cuentasbancarias = dbc01(
                                    (dbc01.tempresa_cuentabancaria_terminales.id
                                     == _dbRowPoliza.tcfdi_registropagos.empresa_cuentabancaria_terminal_id)
                                    & (dbc01.tempresa_cuentasbancarias.id
                                       == dbc01.tempresa_cuentabancaria_terminales.empresa_cuentabancaria_id)
                                    ).select(
                                        dbc01.tempresa_cuentasbancarias.codigo_contpaqi
                                        )
                            else: 
                                _dbRows_empresa_cuentasbancarias = dbc01(
                                    (dbc01.tempresa_cuentasbancarias.id
                                     == _dbRowsRegistroPagoComplemento.empresa_cuentabancaria_id)
                                    ).select(
                                        dbc01.tempresa_cuentasbancarias.codigo_contpaqi
                                        )
                        
                            if not _dbRows_empresa_cuentasbancarias:
                                _D_poliza_documentosbancarios.idscuentascheques.append('2')
                            else:
                                _D_poliza_documentosbancarios.idscuentascheques.append(
                                    str(_dbRows_empresa_cuentasbancarias.last().codigo_contpaqi)
                                    )

                            # Código Moneda char 4
                            # TODO: Hacer el código de contpaq para la moneda
                            _s_msgError = u"Código Moneda inválida."
                            # Hago el query a este nivel porque tratando de hacerlo en el query maestro
                            #  tiraba error en el left.
                            _dbRows_moneda = dbc01(
                                (dbc01.tempresa_monedascontpaqi.empresa_id == _dbRowPoliza.tcfdis.empresa_id)
                                & (dbc01.tempresa_monedascontpaqi.moneda_id
                                   == _dbRowPoliza.tcfdi_complementopagos.moneda_id)
                                ).select(
                                    dbc01.tempresa_monedascontpaqi.moneda_contpaqi
                                    )
                            if _dbRows_moneda:
                                _dbRow_moneda = _dbRows_moneda.last()
                                _s_moneda_contpaqi = _dbRow_moneda.moneda_contpaqi
                            else:
                                _s_moneda_contpaqi = ""
                            
                            _D_poliza_documentosbancarios.codigosmonedas.append(str(_s_moneda_contpaqi))
                                                            
                            # Total char 20
                            _s_msgError = u"Total inválido."
                            _D_poliza_documentosbancarios.totales.append(
                                str(float(_dbRowPoliza.tcfdi_complementopagos.monto))
                                )
                                                             
                            # Referencia char 20
                            # Si la referencia está vacía se pone el folio
                            # TODO: Crear un folio auto incremental
                            _s_msgError = u"Referencia inválida."
                            _D_poliza_documentosbancarios.referencias.append('')
                                
                            # Origen char 5 # TODO: Origen del pago
                            _s_msgError = u"IdDocumentoDe Origen."
                            _D_poliza_documentosbancarios.documentosdeorigen.append('11')
                                                             
                            # Banco Origen char 30 
                            _s_msgError = u"Banco origen inválido."
                            _dbRowBancoOrigen_id = dbc01.tempresa_bancoscontpaqi(
                                _dbRowsRegistroPagoComplemento.bancocontpaqi_id_cliente
                                )
                            
                            if not _dbRowPoliza.tempresa_bancoscontpaqi.banco_id:
                                _D_poliza_documentosbancarios.bancosorigen.append(0)  # 0 es "ninguno" en CONTPAQi
                            else:
                                _dbRowBancoOrigen = db01.tbancos(_dbRowBancoOrigen_id.banco_id)
                                _D_poliza_documentosbancarios.bancosorigen.append((str(_dbRowBancoOrigen.c_banco)))
     
                            # Cuenta Origen char 30
                            _s_msgError = u"Cuenta origen inválida."
                            if (_dbRowsRegistroPagoComplemento.cuenta_cliente == None) \
                                    or (_dbRowsRegistroPagoComplemento.cuenta_cliente == ' '):
                                _D_poliza_documentosbancarios.cuentasorigen.append('')
                            else:
                                _D_poliza_documentosbancarios.cuentasorigen.append(
                                    str(_dbRowsRegistroPagoComplemento.cuenta_cliente)
                                    )
                                                             
                            # Otro método de pago char 5
                            _s_msgError = u"Forma de pago inválida."
                            _D_poliza_documentosbancarios.otrosmetodospago.append(
                                str(_dbRowPoliza.tcfdi_complementopagos.formapago)
                                )
                                                             
                            # GUID char 36 (identificador global NO es el UUID)
                            _s_msgError = u"GUID inválido documento bancario." 
                            _D_poliza_documentosbancarios.guids.append('')
                                                             
                            # RFC char 13 TODO: En el TXT aquí siempre viene vacío
                            _s_msgError = u"RFC inválido."
                            _D_poliza_documentosbancarios.rfcs.append(str(_dbRowPoliza.tcfdis.receptorrfc)) 
                                                             
                            # BancoExtranjero char 60 
                            _s_msgError = u"BancoExtranjero inválido."
                            # No se hace append porque siempre es vacío
                            _D_poliza_documentosbancarios.bancosextranjeros.append('')
                                      
                            # TipoCambio char 20
                            _s_msgError = u"TipoCambio inválido."
                            _D_poliza_documentosbancarios.tiposcambios.append('1.0')
                                                             
                            # NúmeroCheque char 100
                            _s_msgError = u"NúmeroCheque inválido."
                            if _dbRowsRegistroPagoComplemento.cheque == None:
                                _D_poliza_documentosbancarios.numeroscheques.append('')
                            else:
                                _D_poliza_documentosbancarios.numeroscheques.append(
                                    str(_dbRowsRegistroPagoComplemento.cheque)
                                    )
     
                            # UUIDRep char 36
                            _s_msgError = u"UUIDRep inválido."
                            _D_poliza_documentosbancarios.uuidsrep.append('')
                                                         
                            # NodoPago char 3 (parece que aquí siempre va este dato)
                            _s_msgError = u"NodoPago inválido."
                            _D_poliza_documentosbancarios.nodospagos.append('0')
                                                         
                            # Código Moneda Tipo Cambio char 4 (parece que aquí siempre va este dato)
                            _s_msgError = u"Código Moneda Tipo Cambio inválido."
                            _D_poliza_documentosbancarios.codigosmonedas_tipocambio.append('2')
                           
                            # ID del documento bancario para las ventas de contado
                            # Este ID es importante ya que con él se podrá actualizar el folio del documento de pago.
                            _s_msgError = u"ID del documentobancario"
                            _D_poliza_documentosbancarios.bancos_tiposdocumentos_venta_contado.append(
                                _dbRowPoliza.tempresa_bancosdiariosespeciales.bancos_tipo_documentos_venta_contado
                                )
    
                            # Aquí se van a meter los ID de los asientos que se les va a actualizar con el folio
                            #  del documento de pago.
                            _D_poliza_documentosbancarios.asientos_id.append(
                                _dbRowPoliza.tcfdi_prepoliza_asientoscontables.id
                                )
                        else:
                            pass
                    else:
                        pass
                else:
                    pass
               
            _D_poliza = Storage(
                encabezado = _D_poliza_encabezado,
                t_movimientos = _D_poliza_cuerpo,
                t_uuids = _D_poliza_uuids,
                t_documentosbancarios = _D_poliza_documentosbancarios,
                validacionescontpaqi = _D_validacionescontpaqi
                )

    _json_data = str(
        (simplejson.dumps(_D_poliza, cls = STV_FWK_LIB.DecimalEncoder))
        )

    return {
        'n_return'  : _n_return,
        's_msgError': _s_msgError,
        'json_data' : _json_data
        }


@service.soap(
    'poliza_cerrar',
    returns = {
        'n_return'  : int,
        's_msgError': str,
        'json_data' : str
        },
    args = {
        'D_auth'        : wbsT_D_AUTHv2,
        'json_params'   : str,
        's_error'       : str,
        'n_poliza_id'   : int,
        'n_folio'       : int,
        'n_basedatos_id': int,
        'n_servidor_id' : int,
        'n_result'      : int,
        }
    )
def poliza_cerrar(
        D_auth, 
        json_params, 
        s_error, 
        n_poliza_id, 
        n_folio,
        n_basedatos_id,
        n_servidor_id,
        n_result,
        ):
    u"""
        Params:
            s_error: Error con el que viene la póliza al intentar insertarse en CONTPAQi.
            n_poliza_id: ID de la póliza.
            n_folio: Folio de la póliza que manda el SDK.
            n_basedatos_id: ID de la base de datos donde se exportará la póliza.
            n_servidor_id: ID del servidor donde se exportará la póliza.
            n_result: Manejo de código de errores.

            json_params: 
                t_documentosbancarios:
                    folios_documentopago: Lista con los folios del documento de pago a actualizar traidos desde el SDK 
                    asientos_id: lista de IDs de los asientos contables que se tendrán que actualizar con su
                     folio_documentopago 1 a 1.
                    tipodocumentobancario_ventacontado_id: ID del docuemento bancario donde se actualizará el
                     folio del documento de pago.
                t_clientes:
                    rfcs: Lista con los RFC de los clientes a actualizar.
                    razonessociales: lista con la razón social de los clientes a actualizar.
                    codigoscontpaqi: Lista de códigos de clientes a actualizar.
                
    Ejemplo:
        from gluon.contrib.pysimplesoap.client import SoapClient
        client = SoapClient(wsdl="http://127.0.0.1:8000/app_wbs/010acceso/call/soap?WSDL")
        client.Login(s_email="jherrero@stverticales.mx",s_password="pass",s_codigo="test",json_params="")
    """
    _ = n_result
    _D_auth = Storage(D_auth)

    _s_msgError = ""
    _json_data = ""
    _s_wbs = "poliza_cerrar"
    if not auth.is_logged_in():
        _s_msgError = u"Contraseña incorrecta, favor de intentar nuevamente"
        _n_return = stvfwk2_e_RETURN.NOK_LOGIN
    elif not request.stv_fwk_permissions.hasPermission(
            request.stv_fwk_permissions.btn_none, ps_function = "poliza_cerrar"
            ):
        _s_msgError = u"No cuenta con permisos"
        _n_return = stvfwk2_e_RETURN.NOK_PERMISOS
    else:
        _n_return = stvfwk2_e_RETURN.OK     
        
        _D_json = json.loads(json_params)
        
        _D_params = Storage(_D_json)
        
        importar_dbs(_D_auth.n_empresa_id)
        
        if _D_params.t_clientes['codigoscontpaqi']:
            # Si t_clientes trae algo se procede a actualizar el código de los clientes.
        
            _D_result_actualizar_codigocliente = actualizar_codigocliente(_D_auth.n_empresa_id, _D_params.t_clientes)
            
            if _D_result_actualizar_codigocliente.E_return == stvfwk2_e_RETURN.NOK_ERROR:
                s_error = _D_result_actualizar_codigocliente.s_msgError
            else:
                pass
            
        else:
            pass   
            
        # Se hace una consulta a la póliza en este nivel porque en cualquier casó adelante se tendrá que hacer
        #  un update a esta
        _dbRow_poliza = dbc01.tpolizas(n_poliza_id)
        
        if _dbRow_poliza:
            if not s_error:
                # Si la póliza no tuvo ningún problema entonces se procede a hacer el update de su estatus
            
                if _D_params.t_documentosbancarios['folios_documentopago']:
                                                     
                    _n_anio = _dbRow_poliza.fecha.year
                                
                    _n_mes = _dbRow_poliza.fecha.month
                    
                    _dbRows_tipofoliado = dbc01(  
                        (dbc01.tempresa_tipopoliza_ejercicios.empresa_tipopoliza_id
                         == _dbRow_poliza.empresa_tipopoliza_id)
                        & (dbc01.tempresa_tipopoliza_ejercicios.ejercicio == _n_anio)
                        ).select(
                            dbc01.tempresa_tipopoliza_ejercicios.ALL
                            )
                    
                    if _dbRows_tipofoliado:
                        # Si encontró tipo de foliado para este ejercicio entonces buscar el último folio utilizado
                         
                        _dbRow_tipofoliado = _dbRows_tipofoliado.last()
                         
                        if _dbRow_tipofoliado.foliado == TEMPRESA_TIPOPOLIZA_EJERCICIOS.FOLIADO.POR_EJERCICIO:
                            # Si es por ejercicio entonces se buscará en el año el folio que trajo del SDK.
                            _dbRows_ultimofolio = dbc01(
                                (dbc01.tpolizas.empresa_id == _D_auth.n_empresa_id)
                                & (dbc01.tpolizas.empresa_tipopoliza_id == _dbRow_poliza.empresa_tipopoliza_id)
                                & (dbc01.tpolizas.dia.year() == _n_anio)
                                & (dbc01.tpolizas.folio == n_folio)
                                ).select(
                                    dbc01.tpolizas.id
                                    )
                                
                            _dbField_folio = 'folioejercicio'
                            
                        else:
                            # Si no es por ejercicio se buscará por el periodo del ejercicio.
                            
                            _dbRows_ultimofolio = dbc01(
                                (dbc01.tpolizas.empresa_id == _D_auth.n_empresa_id)
                                & (dbc01.tpolizas.empresa_tipopoliza_id == _dbRow_poliza.empresa_tipopoliza_id)
                                & (dbc01.tpolizas.dia.year() == _n_anio)
                                & (dbc01.tpolizas.dia.month() == _n_mes)
                                & (dbc01.tpolizas.folio == n_folio)
                                ).select(
                                    dbc01.tpolizas.id
                                    )
                                
                            # Se consigue el campo a actualizar para el folio.
                            if _dbRow_poliza.dia.month == TGENERICAS.MES.ENERO:
                                # TODO: Ver si mandar campo como expresión o como string.
                                _dbField_folio = 'folioperiodo01'
                            elif _dbRow_poliza.dia.month == TGENERICAS.MES.FEBRERO:
                                _dbField_folio = 'folioperiodo02'
                            elif _dbRow_poliza.dia.month == TGENERICAS.MES.MARZO:
                                _dbField_folio = 'folioperiodo03'
                            elif _dbRow_poliza.dia.month == TGENERICAS.MES.ABRIL:
                                _dbField_folio = 'folioperiodo04'
                            elif _dbRow_poliza.dia.month == TGENERICAS.MES.MAYO:
                                _dbField_folio = 'folioperiodo05'
                            elif _dbRow_poliza.dia.month == TGENERICAS.MES.JUNIO:
                                _dbField_folio = 'folioperiodo06'
                            elif _dbRow_poliza.dia.month == TGENERICAS.MES.JULIO:
                                _dbField_folio = 'folioperiodo07'
                            elif _dbRow_poliza.dia.month == TGENERICAS.MES.AGOSTO:
                                _dbField_folio = 'folioperiodo08'
                            elif _dbRow_poliza.dia.month == TGENERICAS.MES.SEPTIEMBRE:
                                _dbField_folio = 'folioperiodo09'
                            elif _dbRow_poliza.dia.month == TGENERICAS.MES.OCTUBRE:
                                _dbField_folio = 'folioperiodo10'
                            elif _dbRow_poliza.dia.month == TGENERICAS.MES.NOVIEMBRE:
                                _dbField_folio = 'folioperiodo11'
                            elif _dbRow_poliza.dia.month == TGENERICAS.MES.DICIEMBRE:
                                _dbField_folio = 'folioperiodo12'
                            else:
                                _dbField_folio = "folioejercicio"

                        if not _dbRows_ultimofolio:
                            # Si no encontró coincidencias entonces hacer el update de los campos necesarios.
                            
                            # TODO ¿Qué pasa si el número de folio que se quiere guardar es menor al
                            #  último folio guardado?
                            dbc01(
                                (dbc01.tempresa_tipopoliza_ejercicios.empresa_tipopoliza_id
                                 == _dbRow_poliza.empresa_tipopoliza_id)
                                & (dbc01.tempresa_tipopoliza_ejercicios.ejercicio == _n_anio)
                                ).update(
                                    **{_dbField_folio : n_folio}
                                    )
                        
                            _dbRow_poliza.exaut_estatus = TPOLIZAS.EXAUT_ESTATUS.ENVIADA
                            _s_log = u"La póliza fue insertada correctamente"
                            
                        else:
                            # Si encontró una coincidencia con el folio es que dentro de la base de datos ya se
                            #  ingresó un folio igual.
                            _dbRow_poliza.exaut_estatus = TPOLIZAS.EXAUT_ESTATUS.ERROR
                            s_error = u"La póliza se intentó insertar con un folio que ya se ha agregado " \
                                      + u"anteriormente a un registro en la tabla."
                            
                    else:
                        # Si no trajo el tipo de foliado mostrar errores
                        s_error = u"No se encontró el tipo de foliado"
                        pass

                    # Folio documentopago
                    if not s_error:
                        # Si no trajo error en la inserción del folio entonces procede a hacer este
                    
                        # Se crea el diccionario con el json_params
                        _D_params = json.loads(json_params)
                        
                        _D_param = Storage(_D_params)
                        
                        _L_tipodocumentobancario_ventacontado_id = \
                            _D_param.t_documentosbancarios['tipodocumentobancario_ventacontado_id']
                        
                        _n_contador_asiento_id = 0
                        _n_folio_documentopagoUltimo = 0
                        _s_log = ""
                        
                        for _n_folio_documentopago in _D_param.t_documentosbancarios['folios_documentopago']:
                            # Verificar si el folio que se quiere insertar no ha sido utilizado ya en la base de datos.
                            _dbRows_foliopago = dbc01(
                                (dbc01.tpolizas.empresa_id == _D_auth.n_empresa_id)
                                & (dbc01.tpolizas.dia.year == _dbRow_poliza.dia.year)
                                & (dbc01.tcfdi_prepoliza_asientoscontables.poliza_id == dbc01.tpolizas.id)
                                & (dbc01.tcfdi_prepoliza_asientoscontables.empresa_tipodocumentobancario_id
                                   == _L_tipodocumentobancario_ventacontado_id[0])
                                & (dbc01.tcfdi_prepoliza_asientoscontables.empresa_tipodocumentobancario_folio
                                   == _n_folio_documentopago)
                                ).select(
                                    dbc01.tcfdi_prepoliza_asientoscontables.id
                                    )
                                
                            if not _dbRows_foliopago:
                                # Si no trajo coinicidencia entonces se va a pasar a actualizar el folio del documento
                                # de pago y tipodocumentobancario a nivel asiento
                            
                                # Se hacen update a los dos campos en el mismo registro.
                                dbc01(
                                    (dbc01.tcfdi_prepoliza_asientoscontables.id
                                     == _D_param.t_documentosbancarios['asientos_id'][_n_contador_asiento_id])
                                    ).update(
                                        empresa_tipodocumentobancario_id = _L_tipodocumentobancario_ventacontado_id[0],
                                        empresa_tipodocumentobancario_folio = _n_folio_documentopago
                                        )
                                    
                                _n_contador_asiento_id += 1
    
                            else:
                                # Si trajo alguna coinicdencia del folio se truena el for y se manda error en el
                                #  estatus de la póliza y un log explicando el problema.
                                # Se hace rollback a este nivel porque se va a estar actualizando información del los
                                #  asientos contables para insertar folio y tipodocumentobancario_ventacontado_id
                                dbc01.rollback()
                                _dbRow_poliza.exaut_estatus = TPOLIZAS.EXAUT_ESTATUS.ERROR
                                _s_log = u"Un documento de pago de la póliza se intentó guardar con un folio " \
                                         + u"que ya se registró anteriormente en la tabla."
                                s_error = u"Un documento de pago de la póliza se intentó guardar con un " \
                                          + u"folio que ya se registró anteriormente en la tabla "
                            _n_folio_documentopagoUltimo = _n_folio_documentopago

                        # TODO ¿Qué pasa si el número de folio que se quiere guardar es menor al último folio guardado?
                        # Hacer el update a tempresa_tipopoliza_ejercicios con el número de folio.
                        dbc01(
                            (dbc01.tempresa_tipodocumentobancario_ejercicios.empresa_tipodocumentobancario_id
                             == _L_tipodocumentobancario_ventacontado_id[0])
                            ).update(
                                # TODO: ¿Se puede reutilziar la variable que se utiliza en el ciclo para utilizarse
                                #  cómo el último folio insertado?
                                folio = _n_folio_documentopagoUltimo
                                )
                            
                        if _dbRow_poliza.exaut_estatus != TPOLIZAS.EXAUT_ESTATUS.ERROR:
                            # Si no trajo ningún error en el estatus de la exportación automática entonces se
                            # procede a poner el folio y cambiar el estatus a cerrada.
                            _n_return = stvfwk2_e_RETURN.OK
                            _dbRow_poliza.estatus = TPOLIZAS.ESTATUS.EXPORTADO
                            _dbRow_poliza.folio = n_folio
                            # TODO: ¿Guardar con hora del server o browser?
                            _dbRow_poliza.exaut_fecha = request.browsernow
                            _dbRow_poliza.cuenta_servidorcontpaqi_id = n_servidor_id
                            _dbRow_poliza.empresa_contpaqiejercicio_id = n_basedatos_id
                        else:
                            # TODO: ¿Guardar con hora del server o browser?
                            _dbRow_poliza.exaut_fecha = request.browsernow
                            _dbRow_poliza.log = _s_log
                        
                        _dbRow_poliza.update_record()
                    else:
                        # De lo contrarío no se guardará el folio y se le hará un update al exaut_estatus definiendo
                        #  el error que puede traer.
                        _s_msgError = s_error
                        _dbRow_poliza.exaut_estatus = TPOLIZAS.EXAUT_ESTATUS.ERROR
                        _dbRow_poliza.exaut_fecha = request.browsernow  # TODO: ¿Guardar con hora del server o browser?
                        # Pedir que me manden un error para ponerlo en el log o que se utilicen los stvfwk2_e_RETURN
                        # para esto.
                        _dbRow_poliza.exaut_log = s_error
                        _dbRow_poliza.update_record()   
                        _n_return = stvfwk2_e_RETURN.NOK_ERROR
                        
                else:
                    # Si no trajo ningún folio para documentos bancarios la póliza ya puede psar a exportada.
                    _n_return = stvfwk2_e_RETURN.OK
                    _dbRow_poliza.estatus = TPOLIZAS.ESTATUS.EXPORTADO
                    _dbRow_poliza.folio = n_folio
                    _dbRow_poliza.exaut_fecha = request.browsernow  # TODO: ¿Guardar con hora del server o browser?
                    _dbRow_poliza.cuenta_servidorcontpaqi_id = n_servidor_id
                    _dbRow_poliza.empresa_contpaqiejercicio_id = n_basedatos_id
                    
                    _dbRow_poliza.update_record()
            else:
                # De lo contrarío no se guardará el folio y se le hará un update al exaut_estatus definiendo
                #  el error que puede traer.
                _s_msgError = s_error
                _dbRow_poliza.exaut_estatus = TPOLIZAS.EXAUT_ESTATUS.ERROR
                _dbRow_poliza.exaut_fecha = request.browsernow  # TODO: ¿Guardar con hora del server o browser?
                # Pedir que me manden un error para ponerlo en el log o que se utilicen los stvfwk2_e_RETURN para esto.
                _dbRow_poliza.log = s_error
                _dbRow_poliza.cuenta_servidorcontpaqi_id = n_servidor_id
                _dbRow_poliza.empresa_contpaqiejercicio_id = n_basedatos_id
                
                _dbRow_poliza.update_record()   
                _n_return = stvfwk2_e_RETURN.NOK_ERROR
            
            if s_error:
                # Se hace la condición en este punto porque pudo generarse error en el cerrado de la póliza o
                #  puede traerlo desde el llamado del wbs.
                # Para evitar duplicación de código se compara en este punto.
            
                # Si se generó un error se crea el log 
                _D_result_crearerrores = crear_errores(
                    _D_auth.n_empresa_id, _dbRow_poliza.fecha.year, n_basedatos_id, s_error, _s_wbs
                    )
                
                _n_return = _D_result_crearerrores.E_return
                _s_msgError = _D_result_crearerrores.s_msgError
                
            else:
                pass
    
        else:
            _s_msgError = "Poliza no encontrada"
            _n_return = stvfwk2_e_RETURN.NOK_ERROR

        _json_data = None

    return {
        'n_return'  : _n_return,
        's_msgError': _s_msgError,
        'json_data' : _json_data
        }    


@service.soap(
    'proveedor_obtener',
    returns = {
        'n_return'  : int,
        's_msgError': str,
        'json_data' : str
        },
    args = {
        'D_auth'         : wbsT_D_AUTHv2,
        'json_params'    : str,
        's_proveedor_rfc': str
        }
    )
def proveedor_obtener(D_auth, json_params, s_proveedor_rfc):
    """
    
    Ejemplo:
        from gluon.contrib.pysimplesoap.client import SoapClient
        client = SoapClient(wsdl="http://127.0.0.1:8000/app_wbs/010acceso/call/soap?WSDL")
        client.Login(s_email="jherrero@stverticales.mx",s_password="pass",s_codigo="test",json_params="")
    """
    _ = json_params
    _D_auth = Storage(D_auth)

    _s_msgError = ""
    _json_data = ""
    if not auth.is_logged_in():
        _s_msgError = u"Contraseña incorrecta, favor de intentar nuevamente"
        _n_return = stvfwk2_e_RETURN.NOK_LOGIN
    elif not request.stv_fwk_permissions.hasPermission(
            request.stv_fwk_permissions.btn_none, ps_function = "proveedor_obtener"
            ):
        _s_msgError = u"No cuenta con permisos"
        _n_return = stvfwk2_e_RETURN.NOK_PERMISOS
    else:
        _n_return = stvfwk2_e_RETURN.OK
        
        importar_dbs(_D_auth.n_empresa_id)
        
        _dbRows_proveedor = dbc01(
            (dbc01.tempresa_proveedores.empresa_id == _D_auth.n_empresa_id)
            & (dbc01.tempresa_proveedores.rfc == s_proveedor_rfc)
            ).select(
                dbc01.tempresa_clientes.ALL
                )
        
        if _dbRows_proveedor:
            # Si se encontró un cliente con ese RFC entonces mandarlo por _json_data
            _dbRow_proveedor = _dbRows_proveedor.last()
            
            _D_proveedor = Storage(
                cliente_id = _dbRow_proveedor.id,
                codigo_interno = _dbRow_proveedor.codigo,
                razonsocial = _dbRow_proveedor.razonsocial,
                empresa_grupo_id = _dbRow_proveedor.empresa_grupo_id
                )
            
        else:
            # Si no se encontró un cliente con ese RFC mandar error.
            _D_proveedor = None
            _s_msgError = "El RFC que quiere buscar no pertenece a ningún proveedor para esta empresa."
            pass
        
        _json_data = str(
            simplejson.dumps(_D_proveedor, cls = STV_FWK_LIB.DecimalEncoder)
            )

    return {
        'n_return'  : _n_return,
        's_msgError': _s_msgError,
        'json_data' : _json_data
        }


@service.soap(
    'cliente_obtener',
    returns = {
        'n_return'  : int,
        's_msgError': str,
        'json_data' : str
        },
    args = {
        'D_auth'       : wbsT_D_AUTHv2,
        'json_params'  : str,
        's_cliente_rfc': str
        }
    )
def cliente_obtener(D_auth, json_params, s_cliente_rfc):
    """
    
    Ejemplo:
        from gluon.contrib.pysimplesoap.client import SoapClient
        client = SoapClient(wsdl="http://127.0.0.1:8000/app_wbs/010acceso/call/soap?WSDL")
        client.Login(s_email="jherrero@stverticales.mx",s_password="pass",s_codigo="test",json_params="")
    """
    _ = json_params
    _D_auth = Storage(D_auth)

    _s_msgError = ""
    _json_data = ""
    if not auth.is_logged_in():
        _s_msgError = u"Contraseña incorrecta, favor de intentar nuevamente"
        _n_return = stvfwk2_e_RETURN.NOK_LOGIN
    elif not request.stv_fwk_permissions.hasPermission(
            request.stv_fwk_permissions.btn_none, ps_function = "cliente_obtener"
            ):
        _s_msgError = u"No cuenta con permisos"
        _n_return = stvfwk2_e_RETURN.NOK_PERMISOS
    else:
        _n_return = stvfwk2_e_RETURN.OK
        
        importar_dbs(_D_auth.n_empresa_id)
        
        _dbRows_cliente = dbc01(
            (dbc01.tempresa_clientes.empresa_id == _D_auth.n_empresa_id)
            & (dbc01.tempresa_clientes.rfc == s_cliente_rfc)
            ).select(
                dbc01.tempresa_clientes.ALL
                )
        
        if _dbRows_cliente:
            # Si se encontró un cliente con ese RFC entonces mandarlo por _json_data
            _dbRow_cliente = _dbRows_cliente.last()
            
            _D_cliente = Storage(
                cliente_id = _dbRow_cliente.id,
                codigo_interno = _dbRow_cliente.codigo,
                razonsocial = _dbRow_cliente.razonsocial,
                cliente_codigo_contpaqi = _dbRow_cliente.cliente_codigo_contpaqi,
                empresa_grupo_id = _dbRow_cliente.empresa_grupo_id
                )
            
        else:
            # Si no se encontró un cliente con ese RFC mandar error.
            _D_cliente = None
            _s_msgError = "El RFC que quiere buscar no pertenece a ningún cliente para esta empresa."
            pass
        
        _json_data = str(
            simplejson.dumps(_D_cliente, cls = STV_FWK_LIB.DecimalEncoder)
            )

    return {
        'n_return'  : _n_return,
        's_msgError': _s_msgError,
        'json_data' : _json_data
        }
    
    
def actualizar_codigocliente(n_empresa_id, L_clientes):
    u"""
        Params:

            L_clientes: 
                rfcs: Lista con los rfcs de los clientes a actualizar. 
                clientes_razonsocial: lista de las razones sociales de los clientes a actualizar.
                codigos: lista con los códigos de los clientes a actualizar.
    """

    _D_result = Storage(
        E_return = stvfwk2_e_RETURN.NOK_ERROR,
        s_msgError = "",
        )
    
    _D_param = Storage(L_clientes)
    
    _n_posicion = 0
    
    try:
        for _s_rfc in _D_param.rfcs:
            
            _s_rfc_guiones = (
                _s_rfc[:len(_s_rfc)-9] 
                + "-" + _s_rfc[-9:-3] 
                + "-" + _s_rfc[-3:]
                )
            
            # Update a la tabla de clientes donde el RFC y razon social coincida con las traidas en el json.
            dbc01(
                (dbc01.tempresa_clientes.empresa_id == n_empresa_id)
                & (dbc01.tempresa_clientes.rfc == _s_rfc_guiones)
                & (dbc01.tempresa_clientes.razonsocial == _D_param.razonessociales[_n_posicion])
                ).update(
                    cliente_codigo_contpaqi = _D_param.codigoscontpaqi[_n_posicion],
                    )
                
            _n_posicion += 1
        
        _D_result.E_return = stvfwk2_e_RETURN.OK
         
    except:
        _D_result.E_return = stvfwk2_e_RETURN.NOK_ERROR
        _D_result.s_msgError = "Error en la actualización de clientes"
         
    return _D_result


@service.soap(
    'validaciones_contpaqi',
    returns = {
        'n_return'  : int,
        's_msgError': str,
        'json_data' : str
        },
    args = {
        'D_auth'     : wbsT_D_AUTHv2,
        'json_params': str
        }
    )
def validaciones_contpaqi(D_auth, json_params):
    """
    
    Ejemplo:
        from gluon.contrib.pysimplesoap.client import SoapClient
        client = SoapClient(wsdl="http://127.0.0.1:8000/app_wbs/010acceso/call/soap?WSDL")
        client.Login(s_email="jherrero@stverticales.mx",s_password="pass",s_codigo="test",json_params="")
    """
    _ = json_params
    _D_auth = Storage(D_auth)

    _s_msgError = ""
    _json_data = ""
    if not auth.is_logged_in():
        _s_msgError = u"Contraseña incorrecta, favor de intentar nuevamente"
        _n_return = stvfwk2_e_RETURN.NOK_LOGIN
    elif not request.stv_fwk_permissions.hasPermission(
            request.stv_fwk_permissions.btn_none, ps_function = "validaciones_contpaqi"
            ):
        _s_msgError = u"No cuenta con permisos"
        _n_return = stvfwk2_e_RETURN.NOK_PERMISOS
    else:
        _n_return = stvfwk2_e_RETURN.OK
        
        #  D_auth.s_signature para determinar el nombre del servidor y obtener ejercicios, empresa_id, empresas,
        #   nombre base de datos
        #  - 2019,  1, ACULLAGSA, ctEMPRESA_SA_DE_CV_2019
        #  - 2020,  1, ACULLAGSA, ctEMPRESA_SA_DE_CV_2020
        #
        #  Usar el usuario para determinar si tiene acceso a aplicaciones y así a la
        #
        # _D_jsonparams = json.loads(json_params)
        # TODO: Incluir solamente en la busqueda las cuentas a las que pertenece el usuario logeado, ahorita regresa
        #  todas las cuentas asociadas al servidor CONTPAQi
        _dbRows_servidorescontpaqi = db(
            (db.tcuenta_servidorescontpaqi.numeroserie == _D_auth.s_signature)
            ).select(
                db.tcuenta_servidorescontpaqi.ALL,
                distinct = True
                )
        
        _L_basesdedatos = []
        _L_empresas = []
        _L_polizas = []
        _s_empresacontpaqi = None
        if _dbRows_servidorescontpaqi:
            for _dbRows_servidor in _dbRows_servidorescontpaqi:
                # Se configura la base de datos usada para la cuenta, despues de esta consulta dbc01 existe
                importar_dbs_cuenta(s_remplaza_cuenta_id = _dbRows_servidor.cuenta_id)
                
                _qry = (
                    (dbc01.tempresa_contpaqiejercicios.cuenta_servidorcontpaqi_id == _dbRows_servidor.id)
                    & (dbc01.tpolizas.estatus == TPOLIZAS.ESTATUS.SIN_EXPORTAR)
                    )

                _dbRows_polizas = dbc01(
                    _qry
                    & (dbc01.tpolizas.empresa_id == dbc01.tempresa_contpaqiejercicios.empresa_id)
                    & (dbc01.tpolizas.dia.year() == dbc01.tempresa_contpaqiejercicios.ejercicio)
                    ).select(
                        dbc01.tempresa_contpaqiejercicios.ALL,
                        dbc01.tpolizas.ALL,
                        )
                    
                for _dbRow_poliza in _dbRows_polizas:
                    if _dbRow_poliza.tempresa_contpaqiejercicios.empresacontpaqi not in _L_basesdedatos:
                        # si la base de datos no se encuentra dentro de la lista
                        _L_basesdedatos.append(_dbRow_poliza.tempresa_contpaqiejercicios.empresacontpaqi)
                    else:
                        # De lo contrario no hará nada
                        pass
                    
                _L_polizas = []
                
                for _s_basededatos in _L_basesdedatos:
                                        
                    # Validación para tipos de poliza
                    _dbRows_val_tipospolizas = dbc01(
                        _qry 
                        & (dbc01.tempresa_contpaqiejercicios.empresacontpaqi == _s_basededatos)
                        & (dbc01.tpolizas.empresa_id == dbc01.tempresa_contpaqiejercicios.empresa_id)
                        & (dbc01.tempresa_tipospoliza.empresa_id == dbc01.tpolizas.empresa_id)
                        & (dbc01.tempresa_tipospoliza.id == dbc01.tpolizas.empresa_tipopoliza_id)
                        ).select(
                            dbc01.tempresa_tipospoliza.codigo,
                            groupby = [
                                dbc01.tempresa_tipospoliza.codigo
                                ]
                            )
                    
                    _L_tipospoliza = []
                    for _dbRow_val_tipopoliza in _dbRows_val_tipospolizas:
                        _L_tipospoliza.append(int(_dbRow_val_tipopoliza.codigo))

                    # Validación para diarios especiales poliza.
                                        
                    _dbRows_val_diariosespeciales = dbc01(
                        _qry 
                        & (dbc01.tempresa_contpaqiejercicios.empresacontpaqi == _s_basededatos)
                        & (dbc01.tpolizas.empresa_id == dbc01.tempresa_contpaqiejercicios.empresa_id)
                        & (dbc01.tempresa_diarios.empresa_id == dbc01.tpolizas.empresa_id)
                        & (dbc01.tempresa_diarios.id == dbc01.tpolizas.empresa_diario_id)
                        ).select(
                            dbc01.tempresa_diarios.codigo,
                            groupby = [
                                dbc01.tempresa_diarios.codigo
                                ]
                            )
                        
                    _L_diariosespeciales = []
                    for _dbRow_val_diarioespecial in _dbRows_val_diariosespeciales:
                        _L_diariosespeciales.append(_dbRow_val_diarioespecial.codigo)
                        
                    # Validación para diarios especiales asientos polizas.
                    _dbRows_val_diariosespeciales_asientos = dbc01(
                        _qry 
                        & (dbc01.tempresa_contpaqiejercicios.empresacontpaqi == _s_basededatos)
                        & (dbc01.tpolizas.empresa_id == dbc01.tempresa_contpaqiejercicios.empresa_id)
                        & (dbc01.tcfdi_prepolizas.poliza_id == dbc01.tpolizas.id)
                        & (dbc01.tcfdi_prepoliza_asientoscontables.cfdi_prepoliza_id == dbc01.tcfdi_prepolizas.id)
                        & (dbc01.tempresa_diarios.empresa_id == dbc01.tpolizas.empresa_id)
                        & (dbc01.tempresa_diarios.id == dbc01.tcfdi_prepoliza_asientoscontables.empresa_diario_id)
                        ).select(
                            dbc01.tempresa_diarios.codigo,
                            groupby = [
                                dbc01.tempresa_diarios.codigo
                                ]
                            )
                    for _dbRow_val_diarioespecial_asiento in _dbRows_val_diariosespeciales_asientos:
                        _L_diariosespeciales.append(_dbRow_val_diarioespecial_asiento.codigo)
    
                    # Validación para los segmentos de negocio.
                    _dbRows_val_segmentosnegocio = dbc01(
                        _qry 
                        & (dbc01.tempresa_contpaqiejercicios.empresacontpaqi == _s_basededatos)
                        & (dbc01.tpolizas.empresa_id == dbc01.tempresa_contpaqiejercicios.empresa_id)
                        & (dbc01.tcfdi_prepolizas.poliza_id == dbc01.tpolizas.id)
                        & (dbc01.tcfdi_prepoliza_asientoscontables.cfdi_prepoliza_id == dbc01.tcfdi_prepolizas.id)
                        & (dbc01.tempresa_segmentos.empresa_id == dbc01.tpolizas.empresa_id)
                        & (dbc01.tempresa_segmentos.id == dbc01.tcfdi_prepoliza_asientoscontables.empresa_segmento_id)
                        ).select(
                            dbc01.tempresa_segmentos.codigo,
                            groupby = [
                                dbc01.tempresa_segmentos.codigo
                                ]
                            )
                        
                    _L_segmentonegocio = []
                    for _dbRow_val_segmentonegocio in _dbRows_val_segmentosnegocio:
                        _L_segmentonegocio.append(_dbRow_val_segmentonegocio.codigo)
                    
                    # Validación bancos diarios especiales conocido tipo documento bancario.
                    _dbRows_val_tipodocumentobancario = dbc01(
                        _qry 
                        & (dbc01.tempresa_contpaqiejercicios.empresacontpaqi == _s_basededatos)
                        & (dbc01.tpolizas.empresa_id == dbc01.tempresa_contpaqiejercicios.empresa_id)
                        & (dbc01.tcfdi_prepolizas.poliza_id == dbc01.tpolizas.id)
                        & (dbc01.tcfdis.id == dbc01.tcfdi_prepolizas.cfdi_id)
                        & (dbc01.tempresa_bancosdiariosespeciales.empresa_id == dbc01.tcfdis.empresa_id)
                        & (dbc01.tempresa_tipodocumentosbancarios.empresa_id == dbc01.tpolizas.empresa_id)
                        & (
                            dbc01.tempresa_tipodocumentosbancarios.id.belongs(
                                dbc01.tempresa_bancosdiariosespeciales.bancos_tipo_documentos_venta_contado, 
                                dbc01.tempresa_bancosdiariosespeciales.bancos_tipo_documentos_cobranza
                                )
                            )
                        ).select(
                            dbc01.tempresa_tipodocumentosbancarios.codigo_contpaqi,
                            groupby = [
                                dbc01.tempresa_tipodocumentosbancarios.codigo_contpaqi
                                ]
                            )
                        
                    _L_tiposdocumentosbancarios = []
                    for _dbRow_val_tipodocumentobancario in _dbRows_val_tipodocumentobancario:
                        _L_tiposdocumentosbancarios.append(_dbRow_val_tipodocumentobancario.codigo_contpaqi)
    
                    # Validación bancos
                    #  Poliza -> Prepoliza -> CFDI -> Registro pagos -> bancos.codigocontpaqi
                    _dbRows_val_bancos = dbc01(
                        _qry 
                        & (dbc01.tempresa_contpaqiejercicios.empresacontpaqi == _s_basededatos)
                        & (dbc01.tpolizas.empresa_id == dbc01.tempresa_contpaqiejercicios.empresa_id)
                        & (dbc01.tcfdi_prepolizas.poliza_id == dbc01.tpolizas.id)
                        & (dbc01.tcfdis.id == dbc01.tcfdi_prepolizas.cfdi_id)
                        & (dbc01.tcfdi_registropagos.cfdi_id == dbc01.tcfdis.id)
                        & (dbc01.tempresa_bancoscontpaqi.empresa_id == dbc01.tpolizas.empresa_id)
                        & (dbc01.tempresa_bancoscontpaqi.id == dbc01.tcfdi_registropagos.bancocontpaqi_id_cliente)
                        ).select(
                            dbc01.tempresa_bancoscontpaqi.codigo_contpaqi,
                            groupby = [
                                dbc01.tempresa_bancoscontpaqi.codigo_contpaqi
                                ]
                            )
                        
                    _L_bancos = []
                    for _dbRow_val_banco in _dbRows_val_bancos:
                        _L_bancos.append(_dbRow_val_banco.codigo_contpaqi)
                    
                    # Monedas
                    # Poliza -> Prepoliza -> CFDI -> Monedas -> Monedas_CONTPAQi
                    _dbRows_val_monedas_ingreso = dbc01(
                        _qry 
                        & (dbc01.tempresa_contpaqiejercicios.empresacontpaqi == _s_basededatos)
                        & (dbc01.tpolizas.empresa_id == dbc01.tempresa_contpaqiejercicios.empresa_id)
                        & (dbc01.tcfdi_prepolizas.poliza_id == dbc01.tpolizas.id)
                        & (dbc01.tcfdis.id == dbc01.tcfdi_prepolizas.cfdi_id)
                        & (dbc01.tempresa_monedascontpaqi.empresa_id == dbc01.tpolizas.empresa_id)
                        & (dbc01.tempresa_monedascontpaqi.moneda_id == dbc01.tcfdis.moneda_id)
                        ).select(
                            dbc01.tempresa_monedascontpaqi.moneda_contpaqi,
                            groupby = [
                                dbc01.tempresa_monedascontpaqi.moneda_contpaqi
                                ]
                            )
                        
                    _L_monedas = []
                    for _dbRow_val_moneda_ingreso in _dbRows_val_monedas_ingreso:
                        _L_monedas.append(int(_dbRow_val_moneda_ingreso.moneda_contpaqi))
                        
                    # Poliza -> Prepoliza -> CFDI -> Pagos -> Monedas -> Monedas_CONTPAQi
                    _dbRows_val_monedas_pagos = dbc01(
                        _qry 
                        & (dbc01.tempresa_contpaqiejercicios.empresacontpaqi == _s_basededatos)
                        & (dbc01.tpolizas.empresa_id == dbc01.tempresa_contpaqiejercicios.empresa_id)
                        & (dbc01.tcfdi_prepolizas.poliza_id == dbc01.tpolizas.id)
                        & (dbc01.tcfdis.id == dbc01.tcfdi_prepolizas.cfdi_id)
                        & (dbc01.tcfdi_complementopagos.cfdi_id == dbc01.tcfdis.id)
                        & (dbc01.tempresa_monedascontpaqi.empresa_id == dbc01.tpolizas.empresa_id)
                        & (dbc01.tempresa_monedascontpaqi.moneda_id == dbc01.tcfdi_complementopagos.moneda_id)
                        ).select(
                            dbc01.tempresa_monedascontpaqi.moneda_contpaqi,
                            groupby = [
                                dbc01.tempresa_monedascontpaqi.moneda_contpaqi
                                ]
                            )
                        
                    # Cuentas bancarias
                    _dbRows_val_cuentasbancarias = dbc01(
                        _qry 
                        & (dbc01.tempresa_contpaqiejercicios.empresacontpaqi == _s_basededatos)
                        & (dbc01.tpolizas.empresa_id == dbc01.tempresa_contpaqiejercicios.empresa_id)
                        & (dbc01.tcfdi_prepolizas.poliza_id == dbc01.tpolizas.id)
                        & (dbc01.tcfdis.id == dbc01.tcfdi_prepolizas.cfdi_id)
                        & (dbc01.tcfdi_registropagos.cfdi_id == dbc01.tcfdis.id)
                        & (dbc01.tempresa_cuentabancaria_terminales.id
                           == dbc01.tcfdi_registropagos.empresa_cuentabancaria_terminal_id)
                        & (dbc01.tempresa_cuentasbancarias.empresa_id == dbc01.tpolizas.empresa_id)
                        & (dbc01.tempresa_cuentasbancarias.id
                           == dbc01.tempresa_cuentabancaria_terminales.empresa_cuentabancaria_id)
                        ).select(
                            dbc01.tempresa_cuentasbancarias.codigo_contpaqi,
                            groupby = [
                                dbc01.tempresa_cuentasbancarias.codigo_contpaqi
                                ]
                            )
                        
                    _L_cuentasbancarias = []
                    for _dbRow_val_cuentabancaria in _dbRows_val_cuentasbancarias:
                        _L_cuentasbancarias.append(_dbRow_val_cuentabancaria.codigo_contpaqi)

                    for _dbRow_val_moneda_pagos in _dbRows_val_monedas_pagos:
                        _L_monedas.append(int(_dbRow_val_moneda_pagos.moneda_contpaqi))
                        
                    # Validación para diarios especiales asientos polizas.
                    _dbRows_val_cuentascontables = dbc01(
                        _qry 
                        & (dbc01.tempresa_contpaqiejercicios.empresacontpaqi == _s_basededatos)
                        & (dbc01.tpolizas.empresa_id == dbc01.tempresa_contpaqiejercicios.empresa_id)
                        & (dbc01.tcfdi_prepolizas.poliza_id == dbc01.tpolizas.id)
                        & (dbc01.tcfdi_prepoliza_asientoscontables.cfdi_prepoliza_id == dbc01.tcfdi_prepolizas.id)
                        ).select(
                            dbc01.tcfdi_prepoliza_asientoscontables.cuenta_contable,
                            groupby = [
                                dbc01.tcfdi_prepoliza_asientoscontables.cuenta_contable
                                ]
                            )
                    
                    _L_cuentascontables = []
                    for _dbRow_val_cuentacontable in _dbRows_val_cuentascontables:
                        _L_cuentascontables.append(str(_dbRow_val_cuentacontable.cuenta_contable).replace('-', ''))

                    # Validación para tipos de poliza
                    _dbRows_empresas = dbc01(
                        _qry 
                        & (dbc01.tempresa_contpaqiejercicios.empresacontpaqi == _s_basededatos)
                        & (dbc01.tempresas.id == dbc01.tempresa_contpaqiejercicios.empresa_id)
                        ).select(
                            dbc01.tempresas.id,
                            )
                        
                    if _dbRows_empresas:
                        _dbRow_empresa = _dbRows_empresas.last()
                    else:
                        _dbRow_empresa = None  # Esta opción va a hacer tronar el wbs

                    _L_empresas.append(
                        Storage(
                            empresa_id = _dbRow_empresa.id,
                            empresacontpaqi = _s_basededatos,
                            empresacontpaqi_id = _dbRows_polizas.last().tempresa_contpaqiejercicios.id,
                            ejercicio = _dbRows_polizas.last().tempresa_contpaqiejercicios.ejercicio,
                            validacionescontpaqi = Storage(
                                t_tipospoliza = Storage(
                                    codigoscontpaqi = _L_tipospoliza,
                                    _L_msgsError = []
                                    ),
                                t_diariosespeciales = Storage(
                                    codigoscontpaqi = _L_diariosespeciales,
                                    _L_msgsError = []
                                    ),
                                t_segmentosnegocio = Storage(
                                    codigoscontpaqi = _L_segmentonegocio,
                                    _L_msgsError = []
                                    ),
                                t_tipodocumentosbancarios = Storage(
                                    codigoscontpaqi = _L_tiposdocumentosbancarios,
                                    _L_msgsError = []
                                    ),
                                t_bancos = Storage(
                                    codigoscontpaqi = _L_bancos,
                                    _L_msgsError = []
                                    ),
                                t_monedas = Storage(
                                    codigoscontpaqi = _L_monedas,
                                    _L_msgsError = []
                                    ),
                                t_cuentasbancarias = Storage(
                                    codigoscontpaqi = _L_cuentasbancarias,
                                    _L_msgsError = []
                                    ),
                                t_cuentascontables = Storage(
                                    codigoscontpaqi = _L_cuentascontables,
                                    _L_msgsError = []
                                    ),
                                ),
                            # Campo para evitar hacer consulta un nivel adelante (polizas_pendientes)
                            t_stv_cuentas = Storage(
                                cuenta_id = _dbRows_servidor.cuenta_id,
                                servidor_id = _dbRows_servidor.id
                                )
                            )
                        )

            if _L_empresas:
                _json_data = str(
                    simplejson.dumps(
                        Storage(
                            atributos = "",
                            datos = _L_empresas, 
                            ), cls = STV_FWK_LIB.DecimalEncoder
                        )
                    ) 
            else:
                _s_msgError = u"No se encontraron polizas pendientes."
        else:
            _s_msgError = u"No se encuentra el servidor asociado a alguna cuenta."

    return {
        'n_return'  : _n_return,
        's_msgError': _s_msgError,
        'json_data' : _json_data
        }


@service.soap(
    'poliza_errores',
    returns = {
        'n_return'  : int,
        's_msgError': str,
        'json_data' : str
        },
    args = {
        'D_auth'        : wbsT_D_AUTHv2,
        's_error'       : str,
        'n_ejercicio'   : int,
        'n_basedatos_id': int,
        'json_params'   : str
        }
    )
def poliza_errores(D_auth, s_error, n_ejercicio, n_basedatos_id, json_params):
    """
    
    Ejemplo:
        from gluon.contrib.pysimplesoap.client import SoapClient
        client = SoapClient(wsdl="http://127.0.0.1:8000/app_wbs/010acceso/call/soap?WSDL")
        client.Login(s_email="jherrero@stverticales.mx",s_password="pass",s_codigo="test",json_params="")
    """
    _ = json_params
    _D_auth = Storage(D_auth)

    _s_msgError = ""
    _json_data = ""
    _s_wbs = 'poliza_errores'
    if not auth.is_logged_in():
        _s_msgError = u"Contraseña incorrecta, favor de intentar nuevamente"
        _n_return = stvfwk2_e_RETURN.NOK_LOGIN
    elif not request.stv_fwk_permissions.hasPermission(
            request.stv_fwk_permissions.btn_none, ps_function = "poliza_errores"
            ):
        _s_msgError = u"No cuenta con permisos"
        _n_return = stvfwk2_e_RETURN.NOK_PERMISOS
    else:
        _n_return = stvfwk2_e_RETURN.OK
        
    importar_dbs(_D_auth.n_empresa_id)
    
    _D_result_crearerrores = crear_errores(_D_auth.n_empresa_id, n_ejercicio, n_basedatos_id, s_error, _s_wbs)
    
    _n_return = _D_result_crearerrores.E_return
    _s_msgError = _D_result_crearerrores.s_msgError

    return {
        'n_return'  : _n_return,
        's_msgError': _s_msgError,
        'json_data' : _json_data
        }


def crear_errores(n_empresa_id, n_ejercicio, n_basedatos_id, s_error, s_wbs):
    u"""Función para crear el log de errores.
        
        Params:

            L_clientes: 
                rfcs: Lista con los rfcs de los clientes a actualizar. 
                clientes_razonsocial: lista de las razones sociales de los clientes a actualizar.
                codigos: lista con los códigos de los clientes a actualizar.
    """

    _D_result = Storage(
        E_return = stvfwk2_e_RETURN.NOK_ERROR,
        s_msgError = "",
        )
    
    _n_posicion = 0
    
    try:
                        
        _D_insert_Helper = Storage(
            empresa_id = n_empresa_id,
            ejercicio = n_ejercicio,
            fecha = request.browsernow,
            dia = datetime.date.today(),
            empresa_contpaqiejercicio_id = n_basedatos_id,
            descripcion = s_error,
            webservice = s_wbs
            )
        
        dbc01.tempresa_erroresexportacioncontpaqi.insert(**_D_insert_Helper)
        
        _D_result.E_return = stvfwk2_e_RETURN.OK
        
    except:
        _D_result.E_return = stvfwk2_e_RETURN.NOK_ERROR
        _D_result.s_msgError = "Hubo un error en la funcion para la creacion de errores"
        
    return _D_result


def call():
    # session.forget()
    response.namespace = 'https://stverticales.mx/'
    return service()
