# -*- coding: utf-8 -*-


def localidades():
    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = None,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [],
        L_fieldsToSearch = [],
        L_fieldsToSearchIfDigit = [],
        )

    _D_returnVars = _O_cat.process(D_overrideView = request.stv_fwk_permissions.view_viewRegister)

    _O_cat.addSubTab(
        dbTableDetail = db.auth_group,
        s_url = URL(f = 'localidad_paises', args = []),
        s_idHtml = fn_createIDTabUnique(f = 'localidad_paises', s_masterIdentificator = None)
        )

    return dict(_D_returnVars)


def localidad_paises():
    _dbTable = db01.tpaises

    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_find)

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        dbTableMaster = None,
        xFieldLinkMaster = [],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.descripcion,
            _dbTable.c_pais,
            _dbTable.lada,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.descripcion,
            _dbTable.c_pais,
            _dbTable.lada
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _D_returnVars = _O_cat.process()

    _O_cat.addRowSearchResults_addColumnBegin(
        es_typeColumn = STV_FWK_FORM.ES_TYPECOLUMN.S_LINKTAB,
        s_colname = "linktab",
        D_field = Storage(
            readable = True,
            represent = lambda value, dbRow: '<i class="fa fa-external-link-square" aria-hidden="true"></i>',
            ),
        m_classHtml = lambda D_column, dbRow: "stv_rowresults_linktab",
        D_data = Storage(
            url = URL(f = 'localidad_pais_estados', args = ['master_' + str(request.function)]),
            tablinkedtitle = db01.tpais_estados._plural,
            tabcurrentfield = str(_dbTable.descripcion),
            idhtml = fn_createIDTabUnique(
                f = 'localidad_pais_estados',
                s_masterIdentificator = str(request.function)
                )
            )
        )

    return _D_returnVars


def localidad_pais_estados():
    _dbTable = db01.tpais_estados

    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_find)

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        dbTableMaster = db01.tpaises,
        xFieldLinkMaster = [
            _dbTable.pais_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.descripcion,
            _dbTable.c_estado,
            _dbTable.lada,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.descripcion,
            _dbTable.c_estado,
            _dbTable.lada
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            _dbTable.lada
            ],
        )

    _D_returnVars = _O_cat.process()

    _O_cat.addRowSearchResults_addColumnBegin(
        es_typeColumn = STV_FWK_FORM.ES_TYPECOLUMN.S_LINKTAB,
        s_colname = "linktab_mun",
        D_field = Storage(
            readable = True,
            represent = lambda value, dbRow: '<i class="fa fa-external-link-square" aria-hidden="true"></i>',
            ),
        m_classHtml = lambda D_column, dbRow: "stv_rowresults_linktab",
        D_data = Storage(
            url = URL(f = 'localidad_pais_estado_mun', args = request.args[:2] + ['master_' + str(request.function)]),
            tablinkedtitle = db01.tpais_estado_municipios._plural,
            tabcurrentfield = str(_dbTable.descripcion),
            idhtml = fn_createIDTabUnique(
                f = 'localidad_pais_estado_mun',
                s_masterIdentificator = str(request.function)
                )
            )
        )

    _O_cat.addRowSearchResults_addColumnBegin(
        es_typeColumn = STV_FWK_FORM.ES_TYPECOLUMN.S_LINKTAB,
        s_colname = "linktab_loc",
        D_field = Storage(
            readable = True,
            represent = lambda value, dbRow: '<i class="fa fa-external-link-square" aria-hidden="true"></i>',
            ),
        m_classHtml = lambda D_column, dbRow: "stv_rowresults_linktab",
        D_data = Storage(
            url = URL(f = 'localidad_pais_estado_loc', args = request.args[:2] + ['master_' + str(request.function)]),
            tablinkedtitle = db01.tpais_estado_localidades._plural,
            tabcurrentfield = str(_dbTable.descripcion),
            idhtml = fn_createIDTabUnique(
                f = 'localidad_pais_estado_loc', s_masterIdentificator = str(request.function)
                ),
            )
        )

    return _D_returnVars


def localidad_pais_estados_ver():
    if len(request.args) > 0:
        _dbTable = db01.tpais_estados
        _rows_result = db01(_dbTable.pais_id == request.args(0)).select(
            _dbTable.id, _dbTable.descripcion, _dbTable.c_estado
            )
        _LD_data = []
        for _row in _rows_result:
            _LD_data.append({'id': _row.id, 'optionname': (_dbTable._format % _row.as_dict())})
        return response.json({'data': _LD_data})
    else:
        return response.json({'data': [], 'error': 'Argumentos inválidos'})


def localidad_pais_estado_mun():
    _dbTable = db01.tpais_estado_municipios

    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_find)

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        dbTableMaster = db01.tpais_estados,
        xFieldLinkMaster = [
            _dbTable.pais_estado_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.descripcion,
            _dbTable.c_municipio,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.descripcion,
            _dbTable.c_municipio
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[0:2]
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def localidad_pais_estado_mun_ver():
    if len(request.args) > 0:
        _dbTable = db01.tpais_estado_municipios
        _rows_result = db01(_dbTable.pais_estado_id == request.args(0)).select(
            _dbTable.id, _dbTable.descripcion, _dbTable.c_municipio
            )
        _LD_data = []
        for _row in _rows_result:
            _LD_data.append({'id': _row.id, 'optionname': (_dbTable._format % _row.as_dict())})
        return response.json({'data': _LD_data})
    else:
        return response.json({'data': [], 'error': 'Argumentos inválidos'})


def localidad_pais_estado_loc():
    _dbTable = db01.tpais_estado_localidades

    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_find)

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        dbTableMaster = db01.tpais_estados,
        xFieldLinkMaster = [
            _dbTable.pais_estado_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.descripcion,
            _dbTable.c_localidad,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.descripcion,
            _dbTable.c_localidad
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[0:2]
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def aduanas():
    _dbTable = db01.taduanas

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        dbTableMaster = None,
        xFieldLinkMaster = [],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_aduana,
            _dbTable.descripcion,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.descripcion,
            _dbTable.c_aduana
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def prodservs():
    _dbTable = db01.tprodservs

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        dbTableMaster = None,
        xFieldLinkMaster = [],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_claveprodserv,
            _dbTable.descripcion,
            _dbTable.incluiriva,
            _dbTable.incluirieps,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.descripcion,
            _dbTable.c_claveprodserv
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            _dbTable.c_claveprodserv
            ],
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def prodservs_buscar():
    """

        @descripcion Buscar prodservs

        @permiso btn_none
        #@permiso btn_new
        #@permiso btn_save
        @permiso btn_cancel
        #@permiso btn_edit
        @permiso btn_view
        #@permiso btn_suspend
        #@permiso btn_activate
        #@permiso btn_remove
        @permiso btn_find
        @permiso btn_findall
        @permiso btn_signature
        @permiso btn_searchcancel
        @permiso btn_searchdone

    """

    _dbTable = db01.tprodservs

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formSearchButtons,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_claveprodserv,
            _dbTable.descripcion,
            _dbTable.incluiriva,
            _dbTable.incluirieps,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.descripcion,
            _dbTable.c_claveprodserv
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            _dbTable.c_claveprodserv
            ],
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def unidades():
    _dbTable = db01.tunidades

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        dbTableMaster = None,
        xFieldLinkMaster = [],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_claveunidad,
            _dbTable.nombre,
            _dbTable.simbolo,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.descripcion,
            _dbTable.c_claveunidad,
            _dbTable.nombre,
            _dbTable.simbolo
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def unidades_buscar():
    _dbTable = db01.tunidades

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formSearchButtons,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_claveunidad,
            _dbTable.nombre,
            _dbTable.simbolo,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.descripcion,
            _dbTable.c_claveunidad,
            _dbTable.nombre,
            _dbTable.simbolo
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def codigospostales():
    _dbTable = db01.tcodigospostales

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbReferenceSearch = db01,
        dbTable = _dbTable,
        dbTableMaster = None,
        xFieldLinkMaster = [],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_codigopostal,
            _dbTable.pais_estado_id,
            _dbTable.pais_estado_municipio_id,
            _dbTable.pais_estado_localidad_id,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.c_codigopostal,
            db01.tpais_estados.descripcion,
            db01.tpais_estados.c_estado,
            db01.tpais_estado_municipios.descripcion,
            db01.tpais_estado_municipios.c_municipio,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            _dbTable.c_codigopostal
            ],
        )

    _O_cat.addRowSearchResultsLeftJoin(
        db01.tpais_estado_municipios.on(
            db01.tpais_estado_municipios.id == _dbTable.pais_estado_municipio_id
            )
        )

    _O_cat.addRowSearchResultsLeftJoin(
        db01.tpais_estados.on(
            db01.tpais_estados.id == _dbTable.pais_estado_id
            )
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def codigospostales_buscar():
    _dbTable = db01.tcodigospostales

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbReferenceSearch = db01(
            (_dbTable.pais_estado_id == db01.tpais_estados.id) & (
                        _dbTable.pais_estado_municipio_id == db01.tpais_estado_municipios.id) & (
                        _dbTable.pais_estado_localidad_id == db01.tpais_estado_localidades.id)
            ),
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formSearchButtons,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_codigopostal,
            _dbTable.pais_estado_id,
            _dbTable.pais_estado_municipio_id,
            _dbTable.pais_estado_localidad_id,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.c_codigopostal,
            db01.tpais_estados.descripcion,
            db01.tpais_estados.c_estado,
            db01.tpais_estado_municipios.descripcion,
            db01.tpais_estado_municipios.c_municipio,
            db01.tpais_estado_localidades.descripcion,
            db01.tpais_estado_localidades.c_localidad
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            _dbTable.c_codigopostal
            ],
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def impuestos():
    _dbTable = db01.timpuestos

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        dbTableMaster = None,
        xFieldLinkMaster = [],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_impuesto,
            _dbTable.descripcion,
            _dbTable.retencion,
            _dbTable.traslado,
            _dbTable.federal,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.descripcion,
            _dbTable.c_impuesto
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _D_returnVars = _O_cat.process()

    _O_cat.addSubTab(
        dbTableDetail = db01.timpuesto_estados,
        s_url = URL(f = 'impuesto_estados', args = ['master_' + str(request.function), request.args(1)]),
        s_idHtml = fn_createIDTabUnique(f = 'impuesto_estados', s_masterIdentificator = str(request.function))
        )

    return _D_returnVars


def impuesto_estados():
    _dbTable = db01.timpuesto_estados

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        dbTableMaster = db01.timpuestos,
        xFieldLinkMaster = [
            _dbTable.impuesto_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.pais_estado_id,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.pais_estado_id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def metodospago():
    _dbTable = db01.tmetodospago

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        dbTableMaster = None,
        xFieldLinkMaster = [],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_metodopago,
            _dbTable.descripcion,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.descripcion,
            _dbTable.c_metodopago
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def formaspago():
    _dbTable = db01.tformaspago

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        dbTableMaster = None,
        xFieldLinkMaster = [],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_formapago,
            _dbTable.codigo_contpaqi,
            _dbTable.descripcion,
            _dbTable.bancarizado,
            _dbTable.numerooperacion,
            _dbTable.ctaordenante,
            _dbTable.ctabeneficiario,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.descripcion,
            _dbTable.c_formapago
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def monedas():
    _dbTable = db01.tmonedas

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        dbTableMaster = None,
        xFieldLinkMaster = [],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_moneda,
            _dbTable.descripcion,
            _dbTable.decimales,
            _dbTable.variacion,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.descripcion,
            _dbTable.c_moneda
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def patentesaduanales():
    _dbTable = db01.tpatentesaduanales

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        dbTableMaster = None,
        xFieldLinkMaster = [],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_patenteaduanal,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.c_patenteaduanal
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def tiposrelaciones():
    """

    @return:
    @rtype:
    """

    _dbTable = TTIPOSRELACIONES.DBTABLA()

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        dbTableMaster = None,
        xFieldLinkMaster = [],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_tiporelacion,
            _dbTable.descripcion,
            _dbTable.aplicaingreso, _dbTable.aplicaegreso, _dbTable.aplicapago,
            _dbTable.aplicatraslado, _dbTable.aplicanomina,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.c_tiporelacion,
            _dbTable.descripcion
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def usoscfdi():
    _dbTable = db01.tusoscfdi

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        dbTableMaster = None,
        xFieldLinkMaster = [],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_usocfdi,
            _dbTable.descripcion,
            _dbTable.aplicaperfisica,
            _dbTable.aplicapermoral,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.c_usocfdi,
            _dbTable.descripcion
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def tiposcomprobantes():
    _dbTable = db01.ttiposcomprobantes

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        dbTableMaster = None,
        xFieldLinkMaster = [],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_tipodecomprobante,
            _dbTable.descripcion,
            _dbTable.valmaximo,
            _dbTable.valmaximo_ns,
            _dbTable.valmaximo_nds,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.c_tipodecomprobante,
            _dbTable.descripcion
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def regimenesfiscales():
    _dbTable = db01.tregimenesfiscales

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        dbTableMaster = None,
        xFieldLinkMaster = [],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_regimenfiscal,
            _dbTable.descripcion,
            _dbTable.aplicaperfisica,
            _dbTable.aplicapermoral,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.c_regimenfiscal,
            _dbTable.descripcion
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def timbrados():
    _dbTable = db01.ttimbrados

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        dbTableMaster = None,
        xFieldLinkMaster = [],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.versiontimbrado,
            _dbTable.descripcion,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.versiontimbrado,
            _dbTable.descripcion
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def tablasisr():
    _dbTable = db01.ttablasisr

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        dbTableMaster = None,
        xFieldLinkMaster = [],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.ejercicio,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.ejercicio
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            _dbTable.ejercicio
            ]
        )

    _D_returnVars = _O_cat.process()

    _O_cat.addSubTab(
        dbTableDetail = db01.ttablaisr_mensual,
        s_url = URL(f = 'tablaisr_mensual', args = ['master_' + str(request.function), request.args(1)]),
        s_idHtml = fn_createIDTabUnique(f = 'tablaisr_mensual', s_masterIdentificator = str(request.function))
        )
    _O_cat.addSubTab(
        dbTableDetail = db01.ttablaisr_subsidiomensual,
        s_url = URL(f = 'tablaisr_subsidiomensual', args = ['master_' + str(request.function), request.args(1)]),
        s_idHtml = fn_createIDTabUnique(f = 'tablaisr_subsidiomensual', s_masterIdentificator = str(request.function))
        )
    _O_cat.addSubTab(
        dbTableDetail = db01.ttablaisr_anual,
        s_url = URL(f = 'tablaisr_anual', args = ['master_' + str(request.function), request.args(1)]),
        s_idHtml = fn_createIDTabUnique(f = 'tablaisr_anual', s_masterIdentificator = str(request.function))
        )
    _O_cat.addSubTab(
        dbTableDetail = db01.ttablaisr_subsidioanual,
        s_url = URL(f = 'tablaisr_subsidioanual', args = ['master_' + str(request.function), request.args(1)]),
        s_idHtml = fn_createIDTabUnique(f = 'tablaisr_subsidioanual', s_masterIdentificator = str(request.function))
        )

    return _D_returnVars


''' Detalle isr mensual, correspondiente a Tabla Isr y Subsidio '''


def tablaisr_mensual():
    _dbTable = db01.ttablaisr_mensual

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbReferenceSearch = db01,
        dbTable = _dbTable,
        dbTableMaster = db01.ttablasisr,
        xFieldLinkMaster = [
            _dbTable.tablaisr_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.limiteinferior,
            _dbTable.cuotafija,
            _dbTable.excedente,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.id,
            _dbTable.limiteinferior,
            _dbTable.cuotafija,
            _dbTable.excedente
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _O_cat.addRowSearchResultsProperty('orderBy', _dbTable.limiteinferior)

    _D_returnVars = _O_cat.process()

    return _D_returnVars


''' Detalle isr anual, correspondiente a Tabla Isr y Subsidio '''


def tablaisr_anual():
    _dbTable = db01.ttablaisr_anual

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbReferenceSearch = db01,
        dbTable = _dbTable,
        dbTableMaster = db01.ttablasisr,
        xFieldLinkMaster = [_dbTable.tablaisr_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.limiteinferior,
            _dbTable.cuotafija,
            _dbTable.excedente,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.id,
            _dbTable.limiteinferior,
            _dbTable.cuotafija,
            _dbTable.excedente
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _O_cat.addRowSearchResultsProperty('orderBy', _dbTable.limiteinferior)

    _D_returnVars = _O_cat.process()

    return _D_returnVars


''' Detalle subsidio mensual, correspondiente a Tabla Isr y Subsidio '''


def tablaisr_subsidiomensual():
    _dbTable = db01.ttablaisr_subsidiomensual

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbReferenceSearch = db01,
        dbTable = _dbTable,
        dbTableMaster = db01.ttablasisr,
        xFieldLinkMaster = [
            _dbTable.tablaisr_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.limiteinferior,
            _dbTable.cuotafija,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.id,
            _dbTable.limiteinferior,
            _dbTable.cuotafija],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _O_cat.addRowSearchResultsProperty('orderBy', _dbTable.limiteinferior)

    _D_returnVars = _O_cat.process()

    return _D_returnVars


''' Detalle subsidio anual, correspondiente a Tabla Isr y Subsidio '''


def tablaisr_subsidioanual():
    _dbTable = db01.ttablaisr_subsidioanual

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbReferenceSearch = db01,
        dbTable = _dbTable,
        dbTableMaster = db01.ttablasisr,
        xFieldLinkMaster = [
            _dbTable.tablaisr_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.limiteinferior,
            _dbTable.cuotafija,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.id,
            _dbTable.limiteinferior,
            _dbTable.cuotafija
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _O_cat.addRowSearchResultsProperty('orderBy', _dbTable.limiteinferior)

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def unidadesmedicinafamiliar():
    """ Se definen los Tipos de movimientos para su uso en los estados de cuenta

    @descripcion Unidades Medicina Familiar

    @keyword [arg0]: operación a realizar en el formulario.
    @keyword [arg1]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature

    """

    _dbTable = db01.tunidadesmedicinafamiliar

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.codigo,
            _dbTable.unidadmedica,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.codigo,
            _dbTable.unidadmedica
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def _conceptossat_validation(O_form):
    """ Se valida que el concepto y tipo no se repitan.

    """
    _s_tipo = O_form.vars.tipo
    _s_c_concepto = O_form.vars.c_concepto
    _s_id = O_form.record_id

    if db01(
            (db01.tconceptossat.tipo == _s_tipo)
            & (db01.tconceptossat.c_concepto == _s_c_concepto)
            & (db01.tconceptossat.id != _s_id)
            ).count() > 0:

        O_form.errors.tipo = 'Relación Tipo y Concepto SAT ya existe'
    return O_form


'''Catalogo de catconceptossat '''


def conceptossat():
    _dbTable = db01.tconceptossat

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.tipo,
            _dbTable.c_concepto,
            _dbTable.descripcion,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.tipo,
            _dbTable.c_concepto,
            _dbTable.descripcion
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _O_cat.addRowContentEvent('onValidation', _conceptossat_validation)

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def bancos():
    _dbTable = db01.tbancos

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.nombre,
            _dbTable.c_banco,
            _dbTable.razonsocial,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.id,
            _dbTable.nombre
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def bancostiposmovimientos():
    """ Se definen los Tipos de movimientos para su uso en los estados de cuenta

    @descripcion Tipos de movimientos

    @keyword [arg0]: operación a realizar en el formulario.
    @keyword [arg1]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature

    """

    _dbTable = db01.tbancostiposmovimientos

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.descripcion,
            _dbTable.formapago_id
            ],
        L_fieldsToSearch = [
            _dbTable.id,
            _dbTable.descripcion,
            _dbTable.formapago_id,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _O_cat.addSubTab(
        dbTableDetail = db01.tbancostipomovimiento_codigos,
        s_url = URL(f = 'bancostipomovimiento_codigos', args = ['master_' + str(request.function), request.args(1)]),
        s_idHtml = fn_createIDTabUnique(
            f = 'bancostipomovimiento_codigos', s_masterIdentificator = str(request.function)
            )
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def bancostipomovimiento_codigos():
    """ Se identifican el código para relacionar el tipo de pago

    @descripcion Códigos bancarios

    @keyword arg0: Tipos de Movimiento
    @keyword arg1: bancostipomovimiento_id.
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature

    """

    _dbTable = db01.tbancostipomovimiento_codigos

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbReferenceSearch = db01,
        dbTable = _dbTable,
        dbTableMaster = db01.tbancostiposmovimientos,
        xFieldLinkMaster = [
            _dbTable.bancostipomovimiento_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.banco_id,
            _dbTable.codigomovimiento,
            _dbTable.descripcion
            ],
        L_fieldsToSearch = [
            _dbTable.id,
            _dbTable.banco_id,
            _dbTable.codigomovimiento,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def zonashorarias():
    """ Catalogo zonas horarias

    @keyword [arg0]: codigo de funcionalidad
    @keyword [arg1]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature

    """

    _dbTable = db01.tzonashorarias

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.utc,
            _dbTable.zonahoraria,
            _dbTable.pais_id
            ],
        L_fieldsToSearch = [
            _dbTable.id,
            _dbTable.utc,
            _dbTable.zonahoraria,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def transportes():
    """ Se definen las Claves Transporte para su uso en los estados de cuenta

    @descripcion Transportes

    @keyword [arg0]: operación a realizar en el formulario.
    @keyword [arg1]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-money-check-alt] Importar de excel

    """

    _dbTable = db01.ttransportes

    request.stv_fwk_permissions.special_permisson_01.s_type = "button"
    request.stv_fwk_permissions.special_permisson_01.s_actualizar = "stv_gIDTabMain"

    request.stv_fwk_permissions.agregarPermisoAVista(
        request.stv_fwk_permissions.special_permisson_01,
        [
            request.stv_fwk_permissions.view_emptyForm,
            request.stv_fwk_permissions.view_selectedRow
            ]
        )

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_cvetransporte,
            _dbTable.descripcion,
            _dbTable.fecha_inicovigencia,
            _dbTable.fecha_finvigencia,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.c_cvetransporte,
            _dbTable.descripcion
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        D_actions = Storage(
            D_data = Storage(
                importar = str(_dbTable)
                ),
            modal = Storage(
                s_url = URL(
                    f = 'importar_excel',
                    args = ['master_' + str(request.function), 0]
                    # vars=request.vars
                    ),
                s_field = "",
                )
            )
        )

    _D_returnVars = _O_cat.process()

    _O_cat.addSubTab(
        dbTableDetail = db01.ttransporte_tiposestaciones,
        s_url = URL(
            f = 'transporte_tiposestaciones',
            args = ['master_' + str(
                request.function
                ), request.args(1, None)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'transporte_tiposestaciones',
            s_masterIdentificator = str(
                request.function
                )
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = db01.ttransporte_estaciones,
        s_url = URL(
            f = 'transporte_estaciones',
            args = ['master_' + str(
                request.function
                ), request.args(1, None)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'transporte_estaciones',
            s_masterIdentificator = str(
                request.function
                )
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = db01.ttransporte_tipospermisos,
        s_url = URL(
            f = 'transporte_tipospermisos',
            args = ['master_' + str(
                request.function
                ), request.args(1, None)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'transporte_tipospermisos',
            s_masterIdentificator = str(
                request.function
                )
            )
        )

    return _D_returnVars


def importar_excel():
    """ Formulario de captura adicional cuando se quiere importar de un excel

        @descripcion Captura Adicional (No necesita permisos)

        @keyword [arg0]: operación a realizar en el formulario.
        @keyword [arg1]: id en caso de existir relación a un registro en la operación.

        #@permiso btn_none
        #@permiso btn_new
        #@permiso btn_save
        #@permiso btn_cancel
        #@permiso btn_edit
        #@permiso btn_view
        #@permiso btn_suspend
        #@permiso btn_activate
        #@permiso btn_remove
        #@permiso btn_find
        #@permiso btn_findall
        #@permiso btn_signature
        """

    def validacion(O_form):
        """ Validación
        """

        if isinstance(request.vars.archivo_excel, str):
            O_form.errors.archivo_excel = "Favor de agregar el archivo"
        else:
            from shutil import copyfile
            import tempfile
            _s_nombreArchivo = os.path.basename(request.vars.archivo_excel.filename.replace("\\", "/"))
            _s_pathArchivoTemporal = os.path.join(tempfile.gettempdir(), _s_nombreArchivo)
            copyfile(request.vars.archivo_excel.fp.name, _s_pathArchivoTemporal)
            _CLS_TABLA = None
            if request.args(0, None) == "master_transportes":
                _CLS_TABLA = TTRANSPORTES
            elif request.args(0, None) == "master_transporte_tiposestaciones":
                _CLS_TABLA = TTRANSPORTE_TIPOSESTACIONES
            elif request.args(0, None) == "master_transporte_estaciones":
                _CLS_TABLA = TTRANSPORTE_ESTACIONES
            elif request.args(0, None) == "master_transporte_tipospermisos":
                _CLS_TABLA = TTRANSPORTE_TIPOSPERMISOS
            elif request.args(0, None) == "master_unidadespeso":
                _CLS_TABLA = TUNIDADESPESO
            elif request.args(0, None) == "master_tiposembalajes":
                _CLS_TABLA = TTIPOSEMBALAJES
            elif request.args(0, None) == "master_prodservcp":
                _CLS_TABLA = TPRODSERVCP
            elif request.args(0, None) == "master_materialespeligrosos":
                _CLS_TABLA = TMATERIALESPELIGROSOS
            elif request.args(0, None) == "master_colonias":
                _CLS_TABLA = TCOLONIAS
            elif request.args(0, None) == "master_auto_configautotransportes":
                _CLS_TABLA = TAUTO_CONFIGAUTOTRANSPORTES
            elif request.args(0, None) == "master_auto_tiposremolques":
                _CLS_TABLA = TAUTO_TIPOSREMOLQUES
            elif request.args(0, None) == "master_mar_configmaritimas":
                _CLS_TABLA = TMAR_CONFIGMARITIMAS
            elif request.args(0, None) == "master_mar_tiposcargas":
                _CLS_TABLA = TMAR_TIPOSCARGAS
            elif request.args(0, None) == "master_mar_contenedoresmaritimos":
                _CLS_TABLA = TMAR_CONTENEDORESMARITIMOS
            elif request.args(0, None) == "master_mar_autorizacionesnavieras":
                _CLS_TABLA = TMAR_AUTORIZACIONESNAVIERAS
            elif request.args(0, None) == "master_aero_transportesaereos":
                _CLS_TABLA = TAERO_TRANSPORTESAEREOS
            elif request.args(0, None) == "master_ferr_prodservstcc":
                _CLS_TABLA = TFERR_PRODSERVSTCC
            elif request.args(0, None) == "master_ferr_tiposservicios":
                _CLS_TABLA = TFERR_TIPOSSERVICIOS
            elif request.args(0, None) == "master_ferr_derechospaso":
                _CLS_TABLA = TFERR_DERECHOSPASO
            elif request.args(0, None) == "master_ferr_tiposcarros":
                _CLS_TABLA = TFERR_TIPOSCARROS
            elif request.args(0, None) == "master_ferr_tiposcontenedores":
                _CLS_TABLA = TFERR_TIPOSCONTENEDORES
            else:
                pass

            if _CLS_TABLA:
                if 'vars_request' in request.vars:
                    _D_vars_request = Storage(simplejson.loads(request.vars.vars_request))
                else:
                    _D_vars_request = None

                _D_result = _CLS_TABLA.IMPORTAR_EXCEL(
                    _s_pathArchivoTemporal,
                    request.vars.nombre,
                    **_D_vars_request
                    )
            else:
                _D_result = Storage(
                    E_return = stvfwk2_e_RETURN.NOK_ERROR,
                    s_msgError = "Tabla requerida para importación no reconocida"
                    )

            if _D_result.E_return > stvfwk2_e_RETURN.OK_WARNING:
                O_form.errors.nombre = _D_result.s_msgError
            else:
                pass

        return O_form

    _s_action = request.args(2, request.stv_fwk_permissions.btn_none.code)

    if _s_action == request.stv_fwk_permissions.special_permisson_01.code:
        _s_action = request.stv_fwk_permissions.btn_none.code
    else:
        pass

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbTable = None,
        L_visibleButtons = [request.stv_fwk_permissions.btn_none, request.stv_fwk_permissions.btn_save],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        b_applyPermissons = False,
        x_offsetInArgs = 2
        )

    if 'vars_request' in request.vars:
        _json_vars_request = request.vars.vars_request
    else:
        _json_vars_request = simplejson.dumps(request.vars, cls = STV_FWK_LIB.DecimalEncoder)

    _O_cat.addRowContentProperty(
        'formHidden',
        x_value = Storage(
            vars_request = _json_vars_request
            )
        )

    _L_formFactoryFields = [
        Field(
            'nombre', 'string', length = 100, default = None,
            required = False,
            notnull = False,
            widget = stv_widget_input, label = 'Nombre hoja excel', comment = '',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'archivo_excel', type = 'upload',
            required = True,
            requires = IS_LENGTH(minsize = 0, maxsize = 4194304, error_message = 'El archivo es demasiado grande.'),
            notnull = False,
            uploadfield = True, widget = lambda f, d, u = None: stv_widget_inputFile(
                f, d,
                s_url = None, x_linkName = None,
                D_additionalAttributes = Storage(notExternalLink = True, notDownload = True, notRemove = True)
                ),
            label = 'Archivo Excel', comment = None,
            writable = True, readable = True,
            autodelete = True,
            uploadfolder = os.path.join(request.folder, 'uploads', 'archivos_excel'),
            uploadseparate = False, uploadfs = None
            ),
        ]

    _O_cat.updateTabOption(
        "s_modalTitle",
        "Importar archivo"
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION,
        validacion
        )

    _D_returnVars = _O_cat.process(
        s_overrideTitle = "Importar archivo",
        s_overrideTitleSingular = "Importar archivo",
        s_overrideCode = _s_action,
        L_formFactoryFields = _L_formFactoryFields,
        b_skipAction = True,
        )

    if _D_returnVars.D_tabOptions.b_accepted:
        _D_useView = request.stv_fwk_permissions.view_refreshParent
        _D_returnVars.D_useView = _D_useView

    else:
        _D_returnVars.D_useView.b_serverCreatesTab = True
        _D_returnVars.D_useView.L_actionsCreateView = [request.stv_fwk_permissions.btn_none.code]

    return _D_returnVars


def transporte_tiposestaciones():
    """ Catálogo de tipo de estación

    @descripcion Tipos Estaciones

    @keyword arg0: Transporte
    @keyword arg1: transporte_id.
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-money-check-alt] Importar de excel

    """

    _dbTable = db01.ttransporte_tiposestaciones
    _s_transporte_id = request.args(1, 0)

    request.stv_fwk_permissions.special_permisson_01.s_type = "button"
    request.stv_fwk_permissions.special_permisson_01.s_actualizar = "stv_gIDTabMain"

    request.stv_fwk_permissions.agregarPermisoAVista(
        request.stv_fwk_permissions.special_permisson_01,
        [
            request.stv_fwk_permissions.view_emptyForm,
            request.stv_fwk_permissions.view_selectedRow
            ]
        )

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        dbTableMaster = db01.ttransportes,
        xFieldLinkMaster = [_dbTable.transporte_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_tipoestacion,
            _dbTable.descripcion,
            _dbTable.fecha_inicovigencia,
            _dbTable.fecha_finvigencia,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.c_tipoestacion,
            _dbTable.descripcion
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        D_actions = Storage(
            D_data = Storage(
                importar = str(_dbTable)
                ),
            modal = Storage(
                s_url = URL(
                    f = 'importar_excel',
                    args = ['master_' + str(request.function), 0],
                    vars = Storage(
                        transporte_id = _s_transporte_id
                        )
                    ),
                s_field = "",
                )
            )
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def transporte_estaciones():
    """ Catálogo de puertos marítimos, estaciones aeroportuarias y estaciones férreas

    @descripcion Estaciones

    @keyword arg0: Transporte
    @keyword arg1: transporte_id.
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-money-check-alt] Importar de excel

    """

    _dbTable = db01.ttransporte_estaciones
    _s_transporte_id = request.args(1, 0)

    request.stv_fwk_permissions.special_permisson_01.s_type = "button"
    request.stv_fwk_permissions.special_permisson_01.s_actualizar = "stv_gIDTabMain"

    request.stv_fwk_permissions.agregarPermisoAVista(
        request.stv_fwk_permissions.special_permisson_01,
        [
            request.stv_fwk_permissions.view_emptyForm,
            request.stv_fwk_permissions.view_selectedRow
            ]
        )

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        dbTableMaster = db01.ttransportes,
        xFieldLinkMaster = [_dbTable.transporte_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_estacion,
            _dbTable.descripcion,
            _dbTable.nacionalidad,
            _dbTable.pais_id,
            _dbTable.designadoriata,
            _dbTable.lineaferrea,
            _dbTable.fecha_inicovigencia,
            _dbTable.fecha_finvigencia,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.c_estacion,
            _dbTable.descripcion
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        D_actions = Storage(
            D_data = Storage(
                importar = str(_dbTable)
                ),
            modal = Storage(
                s_url = URL(
                    f = 'importar_excel',
                    args = ['master_' + str(request.function), 0],
                    vars = Storage(
                        transporte_id = _s_transporte_id
                        )
                    ),
                s_field = "",
                )
            )
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def transporte_tipospermisos():
    """ Catálogo tipos permisos

    @descripcion Tipos Permisos

    @keyword arg0: Transporte
    @keyword arg1: transporte_id.
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-money-check-alt] Importar de excel

    """

    _dbTable = db01.ttransporte_tipospermisos
    _s_transporte_id = request.args(1, 0)

    request.stv_fwk_permissions.special_permisson_01.s_type = "button"
    request.stv_fwk_permissions.special_permisson_01.s_actualizar = "stv_gIDTabMain"

    request.stv_fwk_permissions.agregarPermisoAVista(
        request.stv_fwk_permissions.special_permisson_01,
        [
            request.stv_fwk_permissions.view_emptyForm,
            request.stv_fwk_permissions.view_selectedRow
            ]
        )

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        dbTableMaster = db01.ttransportes,
        xFieldLinkMaster = [_dbTable.transporte_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_tipopermiso,
            _dbTable.descripcion,
            _dbTable.fecha_inicovigencia,
            _dbTable.fecha_finvigencia,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.c_tipopermiso,
            _dbTable.descripcion
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        D_actions = Storage(
            D_data = Storage(
                importar = str(_dbTable)
                ),
            modal = Storage(
                s_url = URL(
                    f = 'importar_excel',
                    args = ['master_' + str(request.function), 0],
                    vars = Storage(
                        transporte_id = _s_transporte_id
                        )
                    ),
                s_field = "",
                )
            )
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def unidadespeso():
    """ Catálogo de unidades de medida y embalaje

    @descripcion Unidades Peso

    @keyword [arg0]: operación a realizar en el formulario.
    @keyword [arg1]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-money-check-alt] Importar de excel

    """

    _dbTable = db01.tunidadespeso

    request.stv_fwk_permissions.special_permisson_01.s_type = "button"
    request.stv_fwk_permissions.special_permisson_01.s_actualizar = "stv_gIDTabMain"

    request.stv_fwk_permissions.agregarPermisoAVista(
        request.stv_fwk_permissions.special_permisson_01,
        [
            request.stv_fwk_permissions.view_emptyForm,
            request.stv_fwk_permissions.view_selectedRow
            ]
        )

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_claveunidadpeso,
            _dbTable.nombre,
            _dbTable.descripcion,
            _dbTable.fecha_inicovigencia,
            _dbTable.fecha_finvigencia,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.c_claveunidadpeso,
            _dbTable.nombre,
            _dbTable.descripcion
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        D_actions = Storage(
            D_data = Storage(
                importar = str(_dbTable)
                ),
            modal = Storage(
                s_url = URL(
                    f = 'importar_excel',
                    args = ['master_' + str(request.function), 0]
                    # vars=request.vars
                    ),
                s_field = "",
                )
            )
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def tiposembalajes():
    """ Catálogo de tipo de embalaje

    @descripcion Tipos Embalajes

    @keyword [arg0]: operación a realizar en el formulario.
    @keyword [arg1]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-money-check-alt] Importar de excel

    """

    _dbTable = db01.ttiposembalajes

    request.stv_fwk_permissions.special_permisson_01.s_type = "button"
    request.stv_fwk_permissions.special_permisson_01.s_actualizar = "stv_gIDTabMain"

    request.stv_fwk_permissions.agregarPermisoAVista(
        request.stv_fwk_permissions.special_permisson_01,
        [
            request.stv_fwk_permissions.view_emptyForm,
            request.stv_fwk_permissions.view_selectedRow
            ]
        )

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_tipoembalaje,
            # _dbTable.nombre,
            _dbTable.descripcion,
            _dbTable.fecha_inicovigencia,
            _dbTable.fecha_finvigencia,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.c_tipoembalaje,
            # _dbTable.nombre,
            _dbTable.descripcion
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        D_actions = Storage(
            D_data = Storage(
                importar = str(_dbTable)
                ),
            modal = Storage(
                s_url = URL(
                    f = 'importar_excel',
                    args = ['master_' + str(request.function), 0]
                    # vars=request.vars
                    ),
                s_field = "",
                )
            )
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def prodservcp():
    """ Catálogo de productos y servicios carta porte

    @descripcion Productos o Servicios de Carta Porte

    @keyword [arg0]: operación a realizar en el formulario.
    @keyword [arg1]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-money-check-alt] Importar de excel

    """

    _dbTable = db01.tprodservcp

    request.stv_fwk_permissions.special_permisson_01.s_type = "button"
    request.stv_fwk_permissions.special_permisson_01.s_actualizar = "stv_gIDTabMain"

    request.stv_fwk_permissions.agregarPermisoAVista(
        request.stv_fwk_permissions.special_permisson_01,
        [
            request.stv_fwk_permissions.view_emptyForm,
            request.stv_fwk_permissions.view_selectedRow
            ]
        )

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_claveprodservcp,
            # _dbTable.nombre,
            _dbTable.descripcion,
            _dbTable.palabrassimilares,
            _dbTable.materialpeligroso,
            _dbTable.fecha_inicovigencia,
            _dbTable.fecha_finvigencia,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.c_claveprodservcp,
            # _dbTable.nombre,
            _dbTable.descripcion,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        D_actions = Storage(
            D_data = Storage(
                importar = str(_dbTable)
                ),
            modal = Storage(
                s_url = URL(
                    f = 'importar_excel',
                    args = ['master_' + str(request.function), 0]
                    # vars=request.vars
                    ),
                s_field = "",
                )
            )
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def prodservscp_buscar():
    """ Forma buscar productos y servicios de carta porte

    @descripcion Productos o Servicios de Carta Porte > Buscar

    @keyword [arg0]: operación a realizar en el formulario.
    @keyword [arg1]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    #@permiso btn_new
    #@permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    @permiso btn_find
    #@permiso btn_findall
    #@permiso btn_signature
    @permiso btn_searchcancel
    @permiso btn_searchdone
    """

    _dbTable = db01.tprodservcp

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbReferenceSearch = db01,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formSearchButtons,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_claveprodservcp, _dbTable.descripcion,
            _dbTable.palabrassimilares, _dbTable.materialpeligroso,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.c_claveprodservcp, _dbTable.descripcion,
            _dbTable.palabrassimilares, _dbTable.materialpeligroso,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            _dbTable.c_claveprodservcp
            ],
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def materialespeligrosos():
    """ Catálogo de materiales peligrosos

    @descripcion Materiales Peligrosos

    @keyword [arg0]: operación a realizar en el formulario.
    @keyword [arg1]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-money-check-alt] Importar de excel

    """

    _dbTable = db01.tmaterialespeligrosos

    request.stv_fwk_permissions.special_permisson_01.s_type = "button"
    request.stv_fwk_permissions.special_permisson_01.s_actualizar = "stv_gIDTabMain"

    request.stv_fwk_permissions.agregarPermisoAVista(
        request.stv_fwk_permissions.special_permisson_01,
        [
            request.stv_fwk_permissions.view_emptyForm,
            request.stv_fwk_permissions.view_selectedRow
            ]
        )

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_materialpeligroso,
            # _dbTable.nombre,
            _dbTable.descripcion, _dbTable.claseodiv, _dbTable.peligrosecundario,
            _dbTable.grupoonu, _dbTable.dispocisionespecial,
            _dbTable.cantlimitadas_1, _dbTable.cantlimitadas_2,
            _dbTable.embalajerig_instrucciones, _dbTable.embalajerig_disposicion,
            _dbTable.cisternascontenedores_instrucciones,
            _dbTable.cisternascontenedores_disposicion,
            _dbTable.fecha_inicovigencia, _dbTable.fecha_finvigencia,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.c_materialpeligroso,
            # _dbTable.nombre,
            _dbTable.descripcion, _dbTable.claseodiv,
            _dbTable.peligrosecundario,
            _dbTable.grupoonu, _dbTable.dispocisionespecial,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        D_actions = Storage(
            D_data = Storage(
                importar = str(_dbTable)
                ),
            modal = Storage(
                s_url = URL(
                    f = 'importar_excel',
                    args = ['master_' + str(request.function), 0]
                    # vars=request.vars
                    ),
                s_field = "",
                )
            )
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def materialespeligrosos_buscar():
    """ Forma buscar materiales peligrosos

    @descripcion Materiales peligrosos > Buscar

    @keyword [arg0]: operación a realizar en el formulario.
    @keyword [arg1]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    #@permiso btn_new
    #@permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    @permiso btn_find
    #@permiso btn_findall
    #@permiso btn_signature
    @permiso btn_searchcancel
    @permiso btn_searchdone
    """

    _dbTable = db01.tmaterialespeligrosos

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbReferenceSearch = db01,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formSearchButtons,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_materialpeligroso, _dbTable.descripcion,
            _dbTable.grupoonu, _dbTable.dispocisionespecial,
            _dbTable.cantlimitadas_1, _dbTable.cantlimitadas_2,
            _dbTable.embalajerig_instrucciones, _dbTable.embalajerig_disposicion,
            _dbTable.cisternascontenedores_instrucciones, _dbTable.cisternascontenedores_disposicion,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.c_materialpeligroso, _dbTable.descripcion,
            _dbTable.grupoonu, _dbTable.dispocisionespecial,
            _dbTable.cantlimitadas_1, _dbTable.cantlimitadas_2,
            _dbTable.embalajerig_instrucciones, _dbTable.embalajerig_disposicion,
            _dbTable.cisternascontenedores_instrucciones, _dbTable.cisternascontenedores_disposicion,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def colonias():
    """ Catálogo de colonias.

    @descripcion Colonias

    @keyword [arg0]: operación a realizar en el formulario.
    @keyword [arg1]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-money-check-alt] Importar de excel

    """

    _dbTable = db01.tcolonias

    request.stv_fwk_permissions.special_permisson_01.s_type = "button"
    request.stv_fwk_permissions.special_permisson_01.s_actualizar = "stv_gIDTabMain"

    request.stv_fwk_permissions.agregarPermisoAVista(
        request.stv_fwk_permissions.special_permisson_01,
        [
            request.stv_fwk_permissions.view_emptyForm,
            request.stv_fwk_permissions.view_selectedRow
            ]
        )

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_colonia,
            _dbTable.nombre,
            _dbTable.codigopostal_id,
            _dbTable.c_codigopostal,
            _dbTable.fecha_inicovigencia,
            _dbTable.fecha_finvigencia,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.nombre,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            _dbTable.c_colonia,
            _dbTable.codigopostal_id,
            _dbTable.c_codigopostal,
            ],
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        D_actions = Storage(
            D_data = Storage(
                importar = str(_dbTable)
                ),
            modal = Storage(
                s_url = URL(
                    f = 'importar_excel',
                    args = ['master_' + str(request.function), 0]
                    # vars=request.vars
                    ),
                s_field = "",
                )
            )
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def auto_configautotransportes():
    """ Catálogo de configuración autotransporte federal.

    @descripcion Carta Porte: Autotransporte: Configuración Autotransporte

    @keyword [arg0]: operación a realizar en el formulario.
    @keyword [arg1]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-money-check-alt] Importar de excel

    """

    _dbTable = db01.tauto_configautotransportes

    request.stv_fwk_permissions.special_permisson_01.s_type = "button"
    request.stv_fwk_permissions.special_permisson_01.s_actualizar = "stv_gIDTabMain"

    request.stv_fwk_permissions.agregarPermisoAVista(
        request.stv_fwk_permissions.special_permisson_01,
        [
            request.stv_fwk_permissions.view_emptyForm,
            request.stv_fwk_permissions.view_selectedRow
            ]
        )

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_configautotransporte,
            _dbTable.descripcion,
            _dbTable.numeroejes,
            _dbTable.numerollantas,
            _dbTable.fecha_inicovigencia,
            _dbTable.fecha_finvigencia,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.c_configautotransporte,
            _dbTable.descripcion,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        D_actions = Storage(
            D_data = Storage(
                importar = str(_dbTable)
                ),
            modal = Storage(
                s_url = URL(
                    f = 'importar_excel',
                    args = ['master_' + str(request.function), 0]
                    # vars=request.vars
                    ),
                s_field = "",
                )
            )
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def auto_tiposremolques():
    """ Catálogo de tipo de remolque.

    @descripcion Carta Porte: Autotransporte: Tipo Remolques

    @keyword [arg0]: operación a realizar en el formulario.
    @keyword [arg1]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-money-check-alt] Importar de excel

    """

    _dbTable = db01.tauto_tiposremolques

    request.stv_fwk_permissions.special_permisson_01.s_type = "button"
    request.stv_fwk_permissions.special_permisson_01.s_actualizar = "stv_gIDTabMain"

    request.stv_fwk_permissions.agregarPermisoAVista(
        request.stv_fwk_permissions.special_permisson_01,
        [
            request.stv_fwk_permissions.view_emptyForm,
            request.stv_fwk_permissions.view_selectedRow
            ]
        )

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_subtiporem,
            _dbTable.descripcion,
            _dbTable.fecha_inicovigencia,
            _dbTable.fecha_finvigencia,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.c_subtiporem,
            _dbTable.descripcion,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        D_actions = Storage(
            D_data = Storage(
                importar = str(_dbTable)
                ),
            modal = Storage(
                s_url = URL(
                    f = 'importar_excel',
                    args = ['master_' + str(request.function), 0]
                    # vars=request.vars
                    ),
                s_field = "",
                )
            )
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def mar_configmaritimas():
    """ Catálogo de configuración marítima.

    @descripcion Carta Porte: Marítimo: Configuraciones Marítimas

    @keyword [arg0]: operación a realizar en el formulario.
    @keyword [arg1]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-money-check-alt] Importar de excel

    """

    _dbTable = db01.tmar_configmaritimas

    request.stv_fwk_permissions.special_permisson_01.s_type = "button"
    request.stv_fwk_permissions.special_permisson_01.s_actualizar = "stv_gIDTabMain"

    request.stv_fwk_permissions.agregarPermisoAVista(
        request.stv_fwk_permissions.special_permisson_01,
        [
            request.stv_fwk_permissions.view_emptyForm,
            request.stv_fwk_permissions.view_selectedRow
            ]
        )

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_configmaritima,
            _dbTable.descripcion,
            _dbTable.fecha_inicovigencia,
            _dbTable.fecha_finvigencia,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.c_configmaritima,
            _dbTable.descripcion,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        D_actions = Storage(
            D_data = Storage(
                importar = str(_dbTable)
                ),
            modal = Storage(
                s_url = URL(
                    f = 'importar_excel',
                    args = ['master_' + str(request.function), 0]
                    # vars=request.vars
                    ),
                s_field = "",
                )
            )
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def mar_tiposcargas():
    """ Catálogo del tipo de carga.

    @descripcion Carta Porte: Marítimo: Tipos Cargas

    @keyword [arg0]: operación a realizar en el formulario.
    @keyword [arg1]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-money-check-alt] Importar de excel

    """

    _dbTable = db01.tmar_tiposcargas

    request.stv_fwk_permissions.special_permisson_01.s_type = "button"
    request.stv_fwk_permissions.special_permisson_01.s_actualizar = "stv_gIDTabMain"

    request.stv_fwk_permissions.agregarPermisoAVista(
        request.stv_fwk_permissions.special_permisson_01,
        [
            request.stv_fwk_permissions.view_emptyForm,
            request.stv_fwk_permissions.view_selectedRow
            ]
        )

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_clavetipocarga,
            _dbTable.descripcion,
            _dbTable.fecha_inicovigencia,
            _dbTable.fecha_finvigencia,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.c_clavetipocarga,
            _dbTable.descripcion,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        D_actions = Storage(
            D_data = Storage(
                importar = str(_dbTable)
                ),
            modal = Storage(
                s_url = URL(
                    f = 'importar_excel',
                    args = ['master_' + str(request.function), 0]
                    # vars=request.vars
                    ),
                s_field = "",
                )
            )
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def mar_contenedoresmaritimos():
    """ Catálogo de contenedores marítimos.

    @descripcion Carta Porte: Marítimo: Contenedores Marítimos

    @keyword [arg0]: operación a realizar en el formulario.
    @keyword [arg1]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-money-check-alt] Importar de excel

    """

    _dbTable = db01.tmar_contenedoresmaritimos

    request.stv_fwk_permissions.special_permisson_01.s_type = "button"
    request.stv_fwk_permissions.special_permisson_01.s_actualizar = "stv_gIDTabMain"

    request.stv_fwk_permissions.agregarPermisoAVista(
        request.stv_fwk_permissions.special_permisson_01,
        [
            request.stv_fwk_permissions.view_emptyForm,
            request.stv_fwk_permissions.view_selectedRow
            ]
        )

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_contenedormaritimo,
            _dbTable.descripcion,
            _dbTable.fecha_inicovigencia,
            _dbTable.fecha_finvigencia,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.c_contenedormaritimo,
            _dbTable.descripcion,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        D_actions = Storage(
            D_data = Storage(
                importar = str(_dbTable)
                ),
            modal = Storage(
                s_url = URL(
                    f = 'importar_excel',
                    args = ['master_' + str(request.function), 0]
                    # vars=request.vars
                    ),
                s_field = "",
                )
            )
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def mar_autorizacionesnavieras():
    """ Catálogo de número autorización agente naviero consignatario.

    @descripcion Carta Porte: Marítimo: Autorizaciones Navieras

    @keyword [arg0]: operación a realizar en el formulario.
    @keyword [arg1]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-money-check-alt] Importar de excel

    """

    _dbTable = db01.tmar_autorizacionesnavieras

    request.stv_fwk_permissions.special_permisson_01.s_type = "button"
    request.stv_fwk_permissions.special_permisson_01.s_actualizar = "stv_gIDTabMain"

    request.stv_fwk_permissions.agregarPermisoAVista(
        request.stv_fwk_permissions.special_permisson_01,
        [
            request.stv_fwk_permissions.view_emptyForm,
            request.stv_fwk_permissions.view_selectedRow
            ]
        )

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_numautorizacionnaviero,
            # _dbTable.descripcion,
            _dbTable.fecha_inicovigencia,
            _dbTable.fecha_finvigencia,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.c_numautorizacionnaviero,
            # _dbTable.descripcion,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        D_actions = Storage(
            D_data = Storage(
                importar = str(_dbTable)
                ),
            modal = Storage(
                s_url = URL(
                    f = 'importar_excel',
                    args = ['master_' + str(request.function), 0]
                    # vars=request.vars
                    ),
                s_field = "",
                )
            )
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def aero_transportesaereos():
    """ Catálogo código transporte aéreo.

    @descripcion Carta Porte: Aéreo: Transportes Aéreos (Aerolineas)

    @keyword [arg0]: operación a realizar en el formulario.
    @keyword [arg1]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-money-check-alt] Importar de excel

    """

    _dbTable = db01.taero_transportesaereos

    request.stv_fwk_permissions.special_permisson_01.s_type = "button"
    request.stv_fwk_permissions.special_permisson_01.s_actualizar = "stv_gIDTabMain"

    request.stv_fwk_permissions.agregarPermisoAVista(
        request.stv_fwk_permissions.special_permisson_01,
        [
            request.stv_fwk_permissions.view_emptyForm,
            request.stv_fwk_permissions.view_selectedRow
            ]
        )

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_codigotransporteaereo,
            _dbTable.nacionalidad,
            _dbTable.nombreaerolinia,
            _dbTable.designadoroaci,
            _dbTable.fecha_inicovigencia,
            _dbTable.fecha_finvigencia,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.c_codigotransporteaereo,
            _dbTable.nacionalidad,
            _dbTable.nombreaerolinia,
            _dbTable.designadoroaci,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        D_actions = Storage(
            D_data = Storage(
                importar = str(_dbTable)
                ),
            modal = Storage(
                s_url = URL(
                    f = 'importar_excel',
                    args = ['master_' + str(request.function), 0]
                    # vars=request.vars
                    ),
                s_field = "",
                )
            )
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def ferr_prodservstcc():
    """ Catálogo de claves de productos y servicios STCC.

    @descripcion Carta Porte: Ferreo: Productos y Servicios STCC

    @keyword [arg0]: operación a realizar en el formulario.
    @keyword [arg1]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-money-check-alt] Importar de excel

    """

    _dbTable = db01.tferr_prodservstcc

    request.stv_fwk_permissions.special_permisson_01.s_type = "button"
    request.stv_fwk_permissions.special_permisson_01.s_actualizar = "stv_gIDTabMain"

    request.stv_fwk_permissions.agregarPermisoAVista(
        request.stv_fwk_permissions.special_permisson_01,
        [
            request.stv_fwk_permissions.view_emptyForm,
            request.stv_fwk_permissions.view_selectedRow
            ]
        )

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_claveprodstcc,
            _dbTable.descripcion,
            _dbTable.fecha_inicovigencia,
            _dbTable.fecha_finvigencia,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.c_claveprodstcc,
            _dbTable.descripcion,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            _dbTable.c_claveprodstcc,
            ],
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        D_actions = Storage(
            D_data = Storage(
                importar = str(_dbTable)
                ),
            modal = Storage(
                s_url = URL(
                    f = 'importar_excel',
                    args = ['master_' + str(request.function), 0]
                    # vars=request.vars
                    ),
                s_field = "",
                )
            )
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def ferr_tiposservicios():
    """ Catálogo de tipo servicio.

    @descripcion Carta Porte: Ferreo: Tipo de Servicios

    @keyword [arg0]: operación a realizar en el formulario.
    @keyword [arg1]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-money-check-alt] Importar de excel

    """

    _dbTable = db01.tferr_tiposservicios

    request.stv_fwk_permissions.special_permisson_01.s_type = "button"
    request.stv_fwk_permissions.special_permisson_01.s_actualizar = "stv_gIDTabMain"

    request.stv_fwk_permissions.agregarPermisoAVista(
        request.stv_fwk_permissions.special_permisson_01,
        [
            request.stv_fwk_permissions.view_emptyForm,
            request.stv_fwk_permissions.view_selectedRow
            ]
        )

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_tipodeservicio,
            _dbTable.descripcion,
            _dbTable.fecha_inicovigencia,
            _dbTable.fecha_finvigencia,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.c_tipodeservicio,
            _dbTable.descripcion,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        D_actions = Storage(
            D_data = Storage(
                importar = str(_dbTable)
                ),
            modal = Storage(
                s_url = URL(
                    f = 'importar_excel',
                    args = ['master_' + str(request.function), 0]
                    # vars=request.vars
                    ),
                s_field = "",
                )
            )
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def ferr_derechospaso():
    """ Catálogo derechos de paso.

    @descripcion Carta Porte: Ferreo: Derechos de Paso

    @keyword [arg0]: operación a realizar en el formulario.
    @keyword [arg1]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-money-check-alt] Importar de excel

    """

    _dbTable = db01.tferr_derechospaso

    request.stv_fwk_permissions.special_permisson_01.s_type = "button"
    request.stv_fwk_permissions.special_permisson_01.s_actualizar = "stv_gIDTabMain"

    request.stv_fwk_permissions.agregarPermisoAVista(
        request.stv_fwk_permissions.special_permisson_01,
        [
            request.stv_fwk_permissions.view_emptyForm,
            request.stv_fwk_permissions.view_selectedRow
            ]
        )

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_derechosdepaso,
            _dbTable.derechodepaso,
            _dbTable.entre,
            _dbTable.hasta,
            _dbTable.otorgarecibe,
            _dbTable.concesionario,
            _dbTable.fecha_inicovigencia,
            _dbTable.fecha_finvigencia,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.c_derechosdepaso,
            _dbTable.derechodepaso,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        D_actions = Storage(
            D_data = Storage(
                importar = str(_dbTable)
                ),
            modal = Storage(
                s_url = URL(
                    f = 'importar_excel',
                    args = ['master_' + str(request.function), 0]
                    # vars=request.vars
                    ),
                s_field = "",
                )
            )
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def ferr_tiposcarros():
    """ Catálogo tipo de carro.

    @descripcion Carta Porte: Ferreo: Tipos de Carros

    @keyword [arg0]: operación a realizar en el formulario.
    @keyword [arg1]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-money-check-alt] Importar de excel

    """

    _dbTable = db01.tferr_tiposcarros

    request.stv_fwk_permissions.special_permisson_01.s_type = "button"
    request.stv_fwk_permissions.special_permisson_01.s_actualizar = "stv_gIDTabMain"

    request.stv_fwk_permissions.agregarPermisoAVista(
        request.stv_fwk_permissions.special_permisson_01,
        [
            request.stv_fwk_permissions.view_emptyForm,
            request.stv_fwk_permissions.view_selectedRow
            ]
        )

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_tipocarro,
            _dbTable.nombre,
            _dbTable.descripcion,
            _dbTable.fecha_inicovigencia,
            _dbTable.fecha_finvigencia,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.c_tipocarro,
            _dbTable.nombre,
            _dbTable.descripcion,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        D_actions = Storage(
            D_data = Storage(
                importar = str(_dbTable)
                ),
            modal = Storage(
                s_url = URL(
                    f = 'importar_excel',
                    args = ['master_' + str(request.function), 0]
                    # vars=request.vars
                    ),
                s_field = "",
                )
            )
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def ferr_tiposcontenedores():
    """ Catálogo de tipo contenedor.

    @descripcion Carta Porte: Ferreo: Tipos de Contenedores

    @keyword [arg0]: operación a realizar en el formulario.
    @keyword [arg1]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    @permiso special_permisson_01:[fas fa-money-check-alt] Importar de excel

    """

    _dbTable = db01.tferr_tiposcontenedores

    request.stv_fwk_permissions.special_permisson_01.s_type = "button"
    request.stv_fwk_permissions.special_permisson_01.s_actualizar = "stv_gIDTabMain"

    request.stv_fwk_permissions.agregarPermisoAVista(
        request.stv_fwk_permissions.special_permisson_01,
        [
            request.stv_fwk_permissions.view_emptyForm,
            request.stv_fwk_permissions.view_selectedRow
            ]
        )

    _O_cat = STV_FWK_FORM(
        dbReference = db01,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll + [
            request.stv_fwk_permissions.special_permisson_01],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.c_tipocontenedor,
            _dbTable.tipocontenedor,
            _dbTable.descripcion,
            _dbTable.fecha_inicovigencia,
            _dbTable.fecha_finvigencia,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.c_tipocontenedor,
            _dbTable.tipocontenedor,
            _dbTable.descripcion,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        )

    _O_cat.addPermissionConfig(
        s_initialCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        D_actions = Storage(
            D_data = Storage(
                importar = str(_dbTable)
                ),
            modal = Storage(
                s_url = URL(
                    f = 'importar_excel',
                    args = ['master_' + str(request.function), 0]
                    # vars=request.vars
                    ),
                s_field = "",
                )
            )
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars
