/*
 *
 *   Callbacks del framewrok para uso por la aplicación cliente
 *   version 1.0
 *
 */

function stv_app_assignFunctionality_tab( oTab ) {
    /* Asigna objetos de diferentes clases relacionadas con date y sus propiedades */
    var config = {
        '.datepicker_date_after': {
            format:'yyyy/mm/dd',
            todayHighlight: true,
            startDate:'0d',
            timepicker:false,
            autoclose: true
            },
        '.datepicker_datetime_before': {
            format:'yyyy/mm/dd',
            todayHighlight: true,
            endDate: '0d',
            timepicker:true,
            sideBySide: true,
            autoclose: true
            }
    };
    for (var selector in config) {
    	oTab.find(selector).datepicker(config[selector]);
    }

}

function stvCallback_formMenuButtonClick_before(oButton, oTab, sLink){

    var _result = true;
    return _result;
    
}