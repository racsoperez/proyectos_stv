# -*- coding: utf-8 -*-


@auth.requires_login()
def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    
    _s_action = request.args(0, None)
    
    _qry = \
        (  # Se obtiene la información del usuario, para evitar si esta suspendido
            (db.auth_user.id == auth.user_id) 
            & (db.auth_user.bansuspender == False)
            ) \
        & (  # Se obtiene la información de sus membrecías, que determinan las cuentas
            (db.auth_membership.user_id == db.auth_user.id) 
            & (db.auth_membership.bansuspender == False)
            ) \
        & (  # De las memebresías se obtiene la información de las cuentas
            (db.tcuentas.id == db.auth_membership.cuenta_id) 
            & (db.tcuentas.bansuspender == False)
            ) \
        & (  # De las cuentas se obtiene la información de las aplicaciones disponibles en las cuentas
            (db.tcuenta_aplicaciones.cuenta_id == db.tcuentas.id)
            & (db.tcuenta_aplicaciones.bansuspender == False)
            & (db.tcuenta_aplicaciones.mostrar == True)
            )
        
    # Se obtienen las cuentas y las aplicaciones a las que el usuario tiene acceso
    _db_rows = db(_qry).select(
        db.tcuentas.id, 
        db.tcuentas.nombrecorto,
        db.tcuenta_aplicaciones.id, 
        db.tcuenta_aplicaciones.nombrecorto, 
        db.taplicaciones.aplicacion_w2p, 
        db.tcuenta_aplicaciones.empresa_id,
        left = [
            db.taplicaciones.on(
                (db.taplicaciones.id == db.tcuenta_aplicaciones.aplicacion_id)
                & (db.taplicaciones.bansuspender == False)  # TODO manejar opciones suspendidas con otro color
                )
            ],
        orderby = [
            db.tcuentas.nombrecorto, 
            db.tcuenta_aplicaciones.nombrecorto
            ]
        )

    if not D_stvFwkCfg.b_isSupportUser:

        # ...si no se encuentran resultados...
        if not _db_rows:
            # ...se redirecciona al inicio.
            response.flash = 'Sin permisos en la aplicación o cuenta suspendida, error en el inicio de sesión'
        else:
            pass

        if len(_db_rows) == 1:
            if not ('avoidredirect' in request.vars):
                redirect(
                    URL(
                        a = _db_rows.first().taplicaciones.aplicacion_w2p, c = 'default', f = 'index',
                        vars = {
                            'cfg_cuenta_aplicacion_id': _db_rows.first().tcuenta_aplicaciones.id,
                            'cfg_cuenta_id'           : _db_rows.first().tcuentas.id
                            }
                        )
                    )
            else:
                del request.vars.avoidredirect
                request.vars.update(mensaje = "No existen cuentas o aplicaciones relacionadas al usuario")
                redirect(URL(f = 'user', args = ['logout'], vars = request.vars))
        else:
            pass
    else:
        pass
        
    if _s_action != "crear":
    
        _db_tree = DB_Tree(_db_rows)
        _db_tree.addLevel(['tcuentas'])
        _db_tree.addLevel(['tcuenta_aplicaciones', 'taplicaciones'])
        
        _db_tree_applications = _db_tree.process()
    
        for _db_treeCuentas in _db_tree_applications:
        
            importar_definiciones_aplicacion()
            importar_dbs_cuenta(s_remplaza_cuenta_id = _db_treeCuentas.data.tcuentas.id)
        
            for _db_treeAplicaciones in _db_treeCuentas.subrows:
                
                if _db_treeAplicaciones.data.tcuenta_aplicaciones.empresa_id:
                    
                    _dbRows_empresas = dbc01(
                        (dbc01.tempresas.id == _db_treeAplicaciones.data.tcuenta_aplicaciones.empresa_id)
                        ).select(dbc01.tempresas.nombrecorto)
                    
                    if _dbRows_empresas:
                        _s_nombrecorto = _dbRows_empresas.first().nombrecorto
                    else:
                        _s_nombrecorto = "No definida"
                    
                    _db_treeAplicaciones.data.tE = Storage(
                        nombrecorto = _s_nombrecorto
                        )
                else:
                    pass
                    
    else:
        _db_tree_applications = None
        pass
    
    # Si existe un vas con el nombre de _flash...
    if request.vars.get('_flash', None):
        # ...remplaza lo que contenga response.flasg.
        response.flash = request.vars.get('_flash', None)
    else:
        pass

    return Storage(
        dbTreeMenuApps = _db_tree_applications,
        msg = 'Seleccione la aplicación a la que desea acceder',
        displayBackoffice = D_stvFwkCfg.b_isSupportUser
        )


def user():
    """
    exposes:
    http ://..../[app]/default/user/login
    http ://..../[app]/default/user/logout
    http ://..../[app]/default/user/register
    http ://..../[app]/default/user/profile
    http ://..../[app]/default/user/retrieve_password
    http ://..../[app]/default/user/change_password
    http ://..../[app]/default/user/manage_users (requires membership in
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    
    _s_email = request.vars.get('email', '') or request.vars.get('emailuser', '') 
    if request.vars.get('_formname'):
        request.get_vars['emailuser'] = _s_email
        # Si cambia el timezone del usuario, cuando se vuelva a cargar la pagina, ya se va a actualizar
        # # El timezoneoffset viene en minutos, y se multiplica por 60
        # if request.vars.s_timezoneoffset:
        #     _n_zonahoraria_id_posible = TZONASHORARIAS.BUSCAR_ZONAHORARIA(int(request.vars.s_timezoneoffset or 0) * 60)
        #     session.stv_browser_zonahoraria_detectada = STV_FWK_APP.DEFINIR_ZONAHORARIA(
        #         _n_zonahoraria_id_posible, STV_FWK_APP.E_ZONAHORARIA_DEFINIDOR.REQUEST_VAR
        #         )
        # else:
        #     pass
    else:
        pass
        
    if len(request.args) > 0:
        if request.args[0] == 'profile':
            current.response.stv_E_presentacion = STV_FWK_FORM.E_PRESENTACION.FORMA
            db.auth_user.email.writable = False
            db.auth_user.notes.readable = False
            db.auth_user.notes.writable = False
        else:
            pass
    else:
        pass
    
    if '_flash' in request.vars:
        session.stv_flash = request.vars._flash  # Se mantiene el mensaje despues de un logout
        
    _O_form = auth()
    _O_form.custom.dspval.email = _s_email
    _s_msg = ""
    
    if len(request.args) > 0:
        if request.args[0] == 'login':
            response.stv_navbar = Storage()
            _s_msg = 'Accede al sistema para empezar la acción!'
        elif request.args[0] == 'logout':
            _s_msg = 'Gracias por venir'
        elif request.args[0] == 'register':
            _s_msg = 'Error, esta opción esta prohibida'
        elif request.args[0] == 'profile':
            _s_msg = 'Actualiza tu información en el sistema'
        elif request.args[0] == 'retrieve_password':
            _s_msg = 'Error, esta opción esta prohibida'
        elif request.args[0] == 'change_password':
            _s_msg = 'Captura tus passwords para hacer el cambio'
        elif request.args[0] == 'request_reset_password':
            _s_msg = 'Para resetear el password es necesario que indiques el email de tu cuenta'
        elif request.args[0] == 'reset_password':
            _s_msg = 'Capture las nuevas contraseñas'
        elif request.args[0] == 'manage_users':
            _s_msg = 'Error, esta opción esta prohibida'
        else:
            _s_msg = 'Error, operación no soportada'
    
    _L_newFlash = []
    if response.flash:
        _L_newFlash.append(
            dict(       
                type = 'warning',
                title = '',
                msg = response.flash
                )
            )

    if 'stv_flash' in session:
        _L_newFlash.append(
            dict(       
                type = 'warning',
                title = '',
                msg = session.stv_flash
                )
            )
                
        del session.stv_flash

    response.flash = _L_newFlash

    return dict(
                form = _O_form,
                msg = _s_msg
                )


def sessionverify():
    
    _n_delayFor_s = 1

    _s_cookieSession = request.cookies.get('session_id_acceso').value
    (_n_sessionId, _s_uniqueKey) = re.compile('(?P<session_id>.*):(?P<uniqueKey>.*)').search(_s_cookieSession).groups()

    if auth.is_logged_in():
        response.cookies['stv_session'] = auth.user_id
        # El timezoneoffset viene en minutos, y se multiplica por 60
        if request.vars.s_timezoneoffset:
            _n_zonahoraria_id_posible = TZONASHORARIAS.BUSCAR_ZONAHORARIA(int(request.vars.s_timezoneoffset or 0) * 60)
            session.stv_browser_zonahoraria_detectada = STV_FWK_APP.DEFINIR_ZONAHORARIA(
                _n_zonahoraria_id_posible, STV_FWK_APP.E_ZONAHORARIA_DEFINIDOR.REQUEST_VAR
                )
        else:
            pass

        _s_status = 'signed'
        
        _dbRow = db.web2py_session_acceso(_n_sessionId)
        if _dbRow:
            _dt_dateTimeLastUpdate = _dbRow.modified_datetime
            _n_delayFor_s = auth.settings.expiration - (request.now - _dt_dateTimeLastUpdate).seconds
            if _n_delayFor_s < 0:
                _n_delayFor_s = 0
            else:
                pass
        else:
            pass
    
    else:
        _s_status = 'unsigned'
    
    if ("b_renewSessionTime" in request.vars) and (request.vars.b_renewSessionTime != "false"):
        pass
    else:
        # Para evitar que se actualice la información de la sesion
        session.forget(response)

    if ("s_application" in request.vars) and request.vars.s_application:
        _s_application = request.vars.s_application
    else:
        _s_application = False
            
    _s_fileLastUpdated = stv_ultimafechadearchivomodificado(_s_application)
    
    # datetime.datetime.fromtimestamp(os.path.getctime(_s_fileLastUpdated)).strftime('%Y-%m-%d %H:%M:%S')
    #  Ejemplo de como mostrar el tiempo

    return response.json(
        {
            's_status': _s_status, 'n_delayfor_s': _n_delayFor_s, 'n_session': _n_sessionId,
            's_uniqueKey': _s_uniqueKey, 'n_lastUpdate': _s_fileLastUpdated
            }
        )


def usermain():
    return dict()


def userajax():

    _dbRow = db.auth_user(request.cookies['stv_session'].value) if request.cookies.get('stv_session') else None

    _sEmail = ""
    if _dbRow:
        _sEmail = _dbRow.email
    else:
        redirect(
            URL(a = D_stvFwkCfg.s_nameAccesoApp, c = 'default', f = 'usermain', args = ['login'], vars = request.vars)
            )

        # TODO: Trabajar en la vista para que muestre la pantalla de log in de forma chida

    _sMsgError = None
    _oForm = SQLFORM.factory(
        db.auth_user.password,
        table_name = 't_login_ajax',
        buttons = [],
        _id = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab + "_form",
        )

    if auth.is_logged_in():
        _oForm = None

    elif 'edpwd' in request.vars:
        if not auth.login_bare(username = _sEmail, password = request.vars['edpwd']):
            _sMsgError = "Contraseña incorrecta, favor de intentar nuevamente"
        else:
            if request.vars.s_timezoneoffset:
                _n_zonahoraria_id_posible = TZONASHORARIAS.BUSCAR_ZONAHORARIA(
                    int(request.vars.s_timezoneoffset or 0) * 60
                    )
                session.stv_browser_zonahoraria_detectada = STV_FWK_APP.DEFINIR_ZONAHORARIA(
                    _n_zonahoraria_id_posible, STV_FWK_APP.E_ZONAHORARIA_DEFINIDOR.REQUEST_VAR
                    )
            else:
                pass
            _oForm = None
    else:
        pass

    return dict(
        O_form = _oForm,
        s_cuenta_aplicacion_id = request.vars.s_cuenta_aplicacion_id,
        s_msgError = _sMsgError,
        s_userName = _dbRow.first_name + " " + _dbRow.last_name,
        s_urlPicture = URL(
            a = D_stvFwkCfg.s_nameBackofficeApp, c = 'accesos', f = 'fotomin'
            ) + '/%(fotominiatura)s?filename=%(fotonombrearchivo)s' % _dbRow,
        s_msg = "La sesión a expirado, favor de registrarse nuevamente."
        )


def servidor_instruccion_cfdi():
    
    # if auth.is_logged_in() and request.vars.s_cuenta_id:
    if request.vars.s_cuenta_id and request.vars.s_instruccion_id:
                
        _dbRow = db.tservidor_instrucciones(request.vars.s_instruccion_id)
        # if _dbRow.fecha < (request.browsernow - datetime.timedelta(days = 1)): No hay timeout para firma de CFDIs
        if _dbRow.estado not in TSERVIDOR_INSTRUCCIONES.E_ESTADO.L_ACTIVOS:
            return "Operación cerrada, necesita generar instrucción nuevamente"
        else:
            # Define una lista de los archivos a importar desde la configuración del sitio.
            importar_definiciones_aplicacion(
                s_filenameDefinitions = [
                    'cfga010g_Definitions.py', 
                    'cfga010s_Definitions.py', 
                    ]
                )
            importar_dbs_genericos()
            importar_dbs_cuenta(s_remplaza_cuenta_id = request.vars.s_cuenta_id)

            _dbRow_json = dbc02(
                dbc02.tcfdi_xmls.cfdi_id == request.vars.s_cfdi_id
                ).select(
                    dbc02.tcfdi_xmls.json_cfdi
                    ).first()
            
            return response.json(simplejson.loads(_dbRow_json.json_cfdi))
        
    else:
        return "Cuenta o instruccion no identificada"


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http ://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


@service.soap('MyAdd', returns = {'result': int}, args = {'a': int, 'b': int})
def add(a, b):
    return a + b


def call():
    # session.forget()
    return service()


def cache_limpiar():
    cache.ram.clear()
    return
