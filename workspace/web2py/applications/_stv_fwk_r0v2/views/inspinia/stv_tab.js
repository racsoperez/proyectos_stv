{{# Cuando la forma esta lista...}}
$(document).ready(function() {

	//var $tab = $("#{{=D_tabOptions.s_tabId}}");
    var $tab = $("#{{=D_tabOptions.s_tabId}}_reference").closest('.stv_tab');;
    
    {{#IDs a nivel de ventana  }}
    $tab.data("stv_gLinkFunction", "{{=(s_cleanUrl or D_stvFwkCfg.D_uniqueHtmlIds.s_link)}}");
    $tab.data("stv_gIDTabButtonBar", "{{=D_tabOptions.s_tabId_buttonsBar}}");
    $tab.data("stv_gIDTabMain", "{{=D_tabOptions.s_tabId_main}}");
    $tab.data("stv_gIDTabDetails", "{{=D_tabOptions.s_tabId_details}}");
    $tab.data("stv_gIDTabResults", "{{=D_tabOptions.s_tabId_results}}");
    $tab.data("stv_gIDTabGrid", "{{=D_tabOptions.s_tabId_grid}}");
    $tab.data("stv_gIDTabModal", "{{=D_tabOptions.s_tabId_modal}}");
    $tab.data("stv_gIDTabMainForm", "{{=D_tabOptions.s_tabId_form}}");
    $tab.data("stv_O_tabOptions", '{{=XML(simplejson.dumps(D_tabOptions))}}');
    $tab.data("stv_E_rowSearchResults_type", '{{=D_rowSearchResults.D_properties.s_type}}');

    {{# Se crea elemento para referenciar a los ids de la tabla }}
    $tab.data("stv_lNameFieldIds", '{{=XML(simplejson.dumps(L_fieldnameIds))}}');
    
    {{#Variable que se actualizará al hacer un request y contendrá el último valor de stv_s_currentViewHtmlClass  }}
    $tab.data("stv_s_previousViewHtmlClass", "{{=XML(D_useView.s_htmlClass)}}");
    $tab.data("stv_L_previousRowsToShow", '{{=XML(simplejson.dumps(D_useView.L_rowsToShow))}}');
    $tab.data("stv_s_previousAction", "{{=XML(s_actionRequested)}}");
	
});


/* Esta función tiene que quedar en esta parte para tener de forma global  */

