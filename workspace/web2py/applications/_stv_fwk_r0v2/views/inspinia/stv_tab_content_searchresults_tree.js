
    var _$_tree = $('#{{=D_tabOptions.s_tabId_tree}}');

    _$_tree.on("select_node.jstree", function (e, O_data) {
            var _$this = $(this);
            var _$tab = _$this.closest('.stv_tab');
            var _$tabResults = $('#' + _$tab.data('stv_gIDTabResults'));
            
            var _$this_jstree = _$this.jstree();
            var _$nodeSelected = _$this_jstree.get_node(_$this_jstree.get_selected(), true);
            if ( _$nodeSelected.hasClass('stv_selected') ) {
                _$nodeSelected.removeClass('stv_selected');
                _$this_jstree.deselect_node(_$nodeSelected);
            } else {
                _$tabResults.find('.stv_selected').removeClass('stv_selected');
                _$nodeSelected.addClass('stv_selected');
            }
            
            stv_fwk_resultsGrid_onClickRow(_$this, _$tab, _$tab.data("stv_gIDTabResults"));
        })
        .jstree({
            'core' : {
                'check_callback' : true
            },
            'plugins' : [ 'types', 'dnd', 'changed' ],
            'types' : {
                'default' : {
                    'icon' : 'fa fa-folder'
                },
                'html' : {
                    'icon' : 'fa fa-file-code-o'
                },
                'svg' : {
                    'icon' : 'fa fa-file-picture-o'
                },
                'css' : {
                    'icon' : 'fa fa-file-code-o'
                },
                'img' : {
                    'icon' : 'fa fa-file-image-o'
                },
                'js' : {
                    'icon' : 'fa fa-file-text-o'
                }
            }
        });

    
