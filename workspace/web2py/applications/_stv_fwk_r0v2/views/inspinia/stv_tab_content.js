{{# Cuando la forma esta lista...}}
$(document).ready(function() {

	{{# Referencia al tab que contiene todas las referencia y variables generales de la forma. }}
	var $tab = $("#{{=D_tabOptions.s_tabId}}_reference").closest('.stv_tab');

    {{# Variable con la clase que corresponde a la vista principal de despliegue. }}
    $tab.data("stv_s_currentViewHtmlClass", "{{=XML(D_useView.s_htmlClass)}}");
    {{# Variable con los rows que se despliegan. }}
    $tab.data("stv_L_currentRowsToShow", '{{=XML(simplejson.dumps(D_useView.L_rowsToShow))}}');
    $tab.data("stv_s_currentAction", "{{=XML(s_actionRequested)}}");
    
    {{# Define el comportamiento de las acciones y elementos de la forma}}
    $tab.data("stv_s_currentBehavior", "{{=XML(D_tabOptions.Es_behavior)}}");

    $tab.data("stv_L_errorsAction", '{{=XML(simplejson.dumps(L_errorsAction))}}');

    
{{  if response.flash:  }}
        stv_fwk_flashMessage({{=XML(simplejson.dumps(response.flash))}});                
{{  pass
    # Si contiene información en la variable response.stv_flash...
    if response.stv_flash:  }}
		stv_fwk_flashMessage({{=XML(simplejson.dumps(response.stv_flash))}});                
{{  pass  }}


{{ if (D_useView.s_htmlClass != 'stv_en_refreshResults'):
	
	
		'''
		Solamente si tiene definido el elemento gridData, ejecuta se agrega el siguiente código a la respuesta.
		'''
		if ("stv_rowSearchResults" in D_useView.L_rowsToShow):  }}
    	{

{{          if D_rowSearchResults.D_properties.s_type == STV_FWK_FORM.ROWSEARCHRESULTS.PROPERTIES.TYPE.S_GRID:  }}    

                {{include os.path.join(D_stvFwkCfg.s_dirFwkView, 'stv_tab_content_searchresults_grid.js')}}

{{          elif D_rowSearchResults.D_properties.s_type == STV_FWK_FORM.ROWSEARCHRESULTS.PROPERTIES.TYPE.S_TREE:  }}    

                {{include os.path.join(D_stvFwkCfg.s_dirFwkView, 'stv_tab_content_searchresults_tree.js')}}

{{          else:  }}    

{{          pass  }}
    	    
    	    
	    }
{{  	elif ( (("stv_rowContent" in D_useView.L_rowsToShow) or ("stv_rowContentDetail" in D_useView.L_rowsToShow))
				and ((rowContent_form) or (L_details)) ):  }}

		    var $detail = $tab.find('.stv_rowContentDetail').first();
		    $detail.find('.detailTabList').empty(); 
		    $detail.find('.detailTabContent').empty();

{{ 		 	if L_details:
				for _D_detail in L_details:  }}
{{		  			# Si el detail es de tipo detail, significa que es detalle y usara el url para actualizar el contenido del tab...
					if (_D_detail.s_type == "detail"):
						if (D_useView.s_htmlClass != 'stv_en_newregister'):  }}
							stv_fwk_tabAdd($detail, "{{=_D_detail.s_tabName}}", "{{=_D_detail.s_idHtml}}", ".detailTabList", ".detailTabContent", {hasClose: false, url_ifEmpty:"{{=_D_detail.s_url}}"});
							/* Haciendo llamada AJAX y actualizando el contenido del tab */
							//stv_fwk_ajax("{{=_D_detail.s_url}}", $tab, "{{=_D_detail.s_idHtml}}", "none", {b_hideAllTabRows: true});
{{    	   				pass
					# ...de lo contrario, si es extended, es una extención de información del main en tabs...
					elif _D_detail.s_type == "extended":  }}
						stv_fwk_tabAdd($detail, "{{=_D_detail.s_tabName}}", "{{=_D_detail.s_idHtml}}", ".detailTabList", ".detailTabContent", {hasClose: false, addClass: "stv_form_partOfMain"});
						$("#{{=_D_detail.s_idDivToUse}}").appendTo("#{{=_D_detail.s_idHtml}}");
{{					# ... en cualquier otro caso, solamente crear el tab y asociar el div.
					else:  }}			
						stv_fwk_tabAdd($detail, "{{=_D_detail.s_tabName}}", "{{=_D_detail.s_idHtml}}", ".detailTabList", ".detailTabContent", {hasClose: false});
						$("#{{=_D_detail.s_idDivToUse}}").appendTo("#{{=_D_detail.s_idHtml}}");
{{					pass
	    		pass  }}
	    		$detail.find('.detailTabList').find('a:first').tab('show');	    		
{{      	pass
    	elif ( D_useView.s_htmlClass == 'stv_en_cierraBusqueda' ):  }}
			$('#{{=D_tabOptions.s_search_edinput_id}}').val("{{=D_tabOptions.s_selectedIds}}").attr('value', "{{=D_tabOptions.s_selectedIds}}");
			$('#search_{{=D_tabOptions.s_search_edinput_id}}').val("{{=D_tabOptions.s_display}}").attr('value', "{{=D_tabOptions.s_display}}");
{{		pass
	
		# Si se esta deslegando una row de edición, se asigna funcionalidad al rowContent y en el detalle a las extenciones stv_form_partOfMain
		if ("stv_rowContent" in D_useView.L_rowsToShow): }}
			stv_fwk_assignFunctionality_form($tab, $tab.find('.stv_rowContent').first());
			stv_fwk_assignFunctionality_form($tab, $tab.find('.stv_rowContentDetail').find('.stv_form_partOfMain'));
{{      pass  }}			
{{      if rowContent_formAdvSearch and _b_createRowsTab:}}
            stv_fwk_assignFunctionality_form($tab, $tab.find('.stv_rowContent_formAdvSearch').first());
{{      pass
	pass  }}


{{  if (request.vars.get('edbuscar', None) != None):}}
        $tab.find("[name='edbuscar']").val("{{=request.vars.get('edbuscar', None)}}");
{{  pass}}

    
	stv_fwk_assignFunctionality_tab($tab);

{{ if (D_tabOptions.sfn_afterLoad): }}
        try {
            var _fn_call = eval( "{{=D_tabOptions.sfn_afterLoad}}" );
            var _x_return = _fn_call($tab);
        }
        catch(err) {
            
        }
{{  pass}}

{{ # Se identifica si existe un redirect desde el browser }}
let _s_idTarget = false;
{{ if D_tabOptions.D_redirect: }}
	{{ if D_tabOptions.D_redirect.E_row: }}
		{{ if D_tabOptions.D_redirect.E_row == STV_FWK_FORM.E_ROWS.RESULTADOS: }}
			_s_idTarget = $tab.data("stv_gIDTabResults");
		{{ elif D_tabOptions.D_redirect.E_row == STV_FWK_FORM.E_ROWS.PADRE_RESULTADOS: }}
			_s_idTarget = $tab.parent().closest(".stv_tab").data("stv_gIDTabResults");
			{{ else: pass }}
		{{ else: pass  # D_tabOptions.D_redirect.E_row }}
	stv_fwk_ajax("{{=D_tabOptions.D_redirect.s_url}}", $tab, _s_idTarget,
		"{{=D_tabOptions.D_redirect.s_type}}",
		{{=XML(simplejson.dumps(D_tabOptions.D_redirect.x_config))}}
		);
	{{ else: pass  # D_tabOptions.D_redirect }}

{{ # Se existen configuraciónes del RowContent }}
{{ if D_rowContent: }}
	{{ _D_propiedades = D_rowContent.D_properties; }}
    {{ # Se identifica si existen campos a actualizar en el maestro }}
	{{ if _D_propiedades.D_actualizarValoresMaestro: }}
		{{ _D_actualizarValoresMaestro = _D_propiedades.D_actualizarValoresMaestro; }}
		{{ # Se búsca el maestro }}
		_$tabMaestro = $tab.parent().closest(".stv_tab");
		_$tabMaestro_rowsContenido = _$tabMaestro.find(".stv_tab_content:first").find(".stv_rowContent:first, .stv_rowContentDetail:first.stv_form_partOfMain");
		{{ for _s_nombreEditor in _D_actualizarValoresMaestro: }}
			$inputs = _$tabMaestro_rowsContenido.find("[field='{{=_s_nombreEditor}}']");
			$inputs.data('value', '{{=_D_actualizarValoresMaestro[_s_nombreEditor].x_valor}}');
			$inputs.val('{{=_D_actualizarValoresMaestro[_s_nombreEditor].s_display}}');
			{{ pass }}

	{{ else: pass  # D_rowContent.D_properties.D_actualizarValoresMaestro }}

{{ else: pass  # _D_rowContent }}

{{ # Se existen comandos a ejecutar }}
{{ if L_comandos: }}
	{{ for _D_comando in L_comandos: }}
        {{ if _D_comando.E_comando == STV_FWK_FORM.E_COMANDOS.REFRESCAR_TABS_HERMANOS: }}
            {{ for _s_tabNombre in _D_comando.D_parametros.L_tabNombres: }}
                _$tabHermano = $tab.siblings(".stv_tab[nombretab='{{=_s_tabNombre}}']");
                _$tabHermano.html(stv_fwk_createLoading());
                _$tabHermano.data("stv_empty", true);
                {{ pass }}

        {{ elif _D_comando.E_comando == STV_FWK_FORM.E_COMANDOS.REFRESCAR_CAMPOS_MAESTRO: }}
            _$tabMaestro = $tab.parent().closest(".stv_tab");
            _$tabMaestro_rowsContenido = _$tabMaestro.find(".stv_tab_content:first").find(".stv_rowContent:first, .stv_rowContentDetail:first.stv_form_partOfMain");
            {{ for _D_campo in _D_comando.D_parametros.L_camposActualizar: }}
                $inputs = _$tabMaestro_rowsContenido.find("[field='{{=str(_D_campo.dbCampo)}}']");
                $inputs.data('value', '{{=_D_campo.x_valor}}');
                $inputs.val('{{=_D_campo.s_display or _D_campo.x_valor}}');
                {{ pass }}

        {{ elif _D_comando.E_comando == STV_FWK_FORM.E_COMANDOS.REFRESCAR_CAMPOS_MAESTRO_2: }}
            _$tabMaestro = $tab.parent().closest(".stv_tab").parent().closest(".stv_tab");
            _$tabMaestro_rowsContenido = _$tabMaestro.find(".stv_tab_content:first").find(".stv_rowContent:first, .stv_rowContentDetail:first.stv_form_partOfMain");
            {{ for _D_campo in _D_comando.D_parametros.L_camposActualizar: }}
                $inputs = _$tabMaestro_rowsContenido.find("[field='{{=str(_D_campo.dbCampo)}}']");
                $inputs.data('value', '{{=_D_campo.x_valor}}');
                $inputs.val('{{=_D_campo.s_display or _D_campo.x_valor}}');
                {{ pass }}

        {{ elif _D_comando.E_comando == STV_FWK_FORM.E_COMANDOS.CERRAR_MODAL: }}
            _$tabMaestro = $tab.parent().closest(".stv_tab").parent().closest(".stv_tab");
            _$tabMaestro_rowsContenido = _$tabMaestro.find(".stv_tab_content:first").find(".stv_rowContent:first, .stv_rowContentDetail:first.stv_form_partOfMain");
            var _$modal = _$tabMaestro.find(".stv_modal");
            if (_$modal.length > 0) {
                _$modal.modal("hide");
            } else {}

        {{ elif _D_comando.E_comando == STV_FWK_FORM.E_COMANDOS.REFRESCAR_CAMPOS_BUSQUEDAAVANZADA: }}
            _$tabMaestro = $tab.parent().closest(".stv_tab");
            _$tabMaestro_rowsBusquedaAvanzada = _$tabMaestro.find(".stv_rowContent_formAdvSearch:first");
            {{ for _D_campo in _D_comando.D_parametros.L_camposActualizar: }}
                $inputs = _$tabMaestro_rowsBusquedaAvanzada.find("[id='{{=str(_D_campo.s_id)}}']");
                $inputs.data('value', '{{=_D_campo.x_valor}}');
                $inputs.val('{{=_D_campo.s_display or _D_campo.x_valor}}');
                {{ pass }}

        {{ else: pass }}

        {{ pass  # For cada comando }}
{{ else: pass  # L_comandos }}


});


/* Esta función tiene que quedar en esta parte para tener de forma global  */
