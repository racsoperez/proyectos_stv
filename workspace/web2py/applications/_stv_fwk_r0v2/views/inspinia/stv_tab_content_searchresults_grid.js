/* Se configura la tabla con los resultados. */
/* http://legacy.datatables.net/index */

if ( (! $.fn.DataTable.isDataTable('#{{=D_tabOptions.s_tabId_grid}}')) ) {
    let L_columnas = [];
    let D_columna;

    {{ for _D_column in _L_columns: }}

        {{ s_colVisible = "true" if getattr(_D_column.D_field, 'readable', True) else "false" }}

         D_columna = {
             data: "{{=_D_column.s_colname}}",
             name: "{{=_D_column.s_colname}}",
             render: fwk2_grid_interpretarValor,
             visible: {{=s_colVisible}}
             };

        {{ if _D_column.s_type in (
                STV_FWK_FORM.ES_TYPECOLUMN.S_FIELD,
                STV_FWK_FORM.ES_TYPECOLUMN.S_CALCULATED,
                STV_FWK_FORM.ES_TYPECOLUMN.S_CUSTOM,
                STV_FWK_FORM.ES_TYPECOLUMN.S_FIELDVIRTUAL,
                ):
            _s_type = str(_D_column.D_field.type) }}

            {{ if (_s_type == "boolean"): }}
                D_columna.orderDataType = "dom-boolean";
            {{ elif (_s_type == "string"): }}
                D_columna.orderDataType = "dom-text";
            {{ elif (_s_type == "date"): }}
                D_columna.orderDataType = "dom-text-date";
            {{ elif (_s_type == "datetime"): }}
                D_columna.orderDataType = "dom-text-datetime";
            {{ elif (_s_type == "time"): }}
                D_columna.orderDataType = "dom-text-time";
            {{ elif (_s_type in ("integer", "id")): }}
                D_columna.orderDataType = "dom-text-numeric";
            {{ elif ("decimal" in _s_type): }}
                D_columna.orderDataType = "dom-text-numeric";
            {{ else: pass  }}
        {{ else: }}
             D_columna.orderable = false;
             D_columna.defaultContent = '';
            {{ pass }}
        L_columnas.push(D_columna);
    {{ pass }}

    let confDataTable = {
            dom: '<"html5buttons"B>lfitp',
            columns: L_columnas,
            select: {items: 'row'},
            //order:  [],
            //ordering: true,
            colReorder: true,
            //select: true,
            //rowId: "{{=XML(globals().get('nameFieldId', ''))}}",
            paging:  true,
            info: true,
            stripeClasses: ['stv_row_odd', 'stv_row_even'],
            //stateSave: true,  // Checar como se graba la información del grid antes de usar esta opción
            lengthMenu: [[10, 50, 100, 200, 500, 1000, -1], [10, 50, 100, 200, 500, 1000, "Todo"]],
            language: {
                emptyTable:     "No hay información disponible",
                info:           "Registros _START_ a _END_ de _TOTAL_",
                infoEmpty:      "Sin registro",
                infoFiltered:   "Filtrado de _MAX_",
                infoPostFix:    "",
                thousands:      ",",
                lengthMenu:     "Limitado a _MENU_ registros",
                loadingRecords: "Cargando...",
                processing:     "Procesando...",
                search:         "Buscar:",
                zeroRecords:    "No se encontraron registros",
                paginate: {
                    first:      "Primera",
                    last:       "Última",
                    next:       "Siguiente",
                    previous:   "Anterior"
                    },
                aria: {
                    sortAscending:  ": activate to sort column ascending",
                    sortDescending: ": activate to sort column descending"
                    },
                buttons: {
                    copyTitle: 'Tabla copiada',
                    copySuccess: {1:"Un renglon copiado", _:"Copiados %d renglones"}
                    },
                select: {
                    rows: {
                        _: "%d registros seleccionados",
                        0: "",
                        1: "Un registro seleccionado"
                        },
                    },
                },
            } ;

    {{ if not(D_useView.b_cleanGrid): }}
        let s_nombreArchivo = "{{=globals().get('s_title', 'TablaExportada')}}";
        {{ if globals().get('dbTable', False): }}
            s_nombreArchivo += " {{=dbTable._plural}}";
        {{ else: pass }}

        confDataTable.buttons = [
            {extend: 'copy', text: 'Copiar'},
            /*{extend: 'csv'},*/
            {
                extend: 'excel', title: s_nombreArchivo,
                exportOptions: { orthogonal: 'export' }
            },
            {extend: 'pdf', title: s_nombreArchivo},
            /*{extend: 'print',
                customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                    }
                }*/
            ]

        confDataTable.scrollY = '80vh';
        confDataTable.scrollCollapse = true;
        confDataTable.scrollX = true;
    {{ else: }}

        //confDataTable.scrollY = '50vh';
        //confDataTable.scrollCollapse = false;
        //confDataTable.scrollX = true;

    {{ pass }}

    var $tablaResultados = $('#{{=D_tabOptions.s_tabId_grid}}');
    var $gridElement = $tablaResultados.DataTable(confDataTable);

    {{if D_rowSearchResults.D_properties.limitBySearch:}}
    $gridElement.page.len({{=D_rowSearchResults.D_properties.limitBySearch[1]}});
    {{else:}}
    $gridElement.page.len(-1);
    {{pass}}

    {{if D_rowSearchResults.D_properties.L_ordenGrid:}}
    $gridElement.order({{=XML(D_rowSearchResults.D_properties.L_ordenGrid)}}).draw();
    {{else:}}
    $gridElement.order([[0,"desc"]]).draw();
    {{pass}}
    $gridElement.draw('page');

    $gridElement.on( 'select', function ( e, dt, type, indexes ) {
        let _$target = $(e.target);
        let _$this = _$target.closest("tr");
        let _$tab = _$target.closest('.stv_tab');
        //debugger
        stv_fwk_resultsGrid_onClickRow(_$this, _$tab, _$tab.data("stv_gIDTabResults"));
        });

    $gridElement.on( 'deselect', function ( e, dt, type, indexes ) {
        let _$target = $(e.target);
        let _$this = _$target.closest("tr");
        let _$tab = _$target.closest('.stv_tab');
        //debugger
        stv_fwk_resultsGrid_onClickRow(_$this, _$tab, _$tab.data("stv_gIDTabResults"));
        });


} else {};
    
