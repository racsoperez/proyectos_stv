/*
 *
 *   Soluciones Tecnológicas Verticales con INSPINA
 *   version 1.0
 *
 */
var STV_FWK2_LIB = {
        VERSION: 2,
        E_RETURN : {
            OK: 0,
            OK_WARNING: 1,
            NOK_LOGIN: 10,
            NOK_PERMISOS: 11,
            NOK_DATOS_INVALIDOS: 12,
            NOK_CONDICIONES_INCORRECTAS: 13,
            NOK_SECUENCIA: 14,
            NOK_REPETIR: 15,
            NOK_INFORMACION_ACTUALIZADA: 16,
            NOK_ERROR: 17,
        },
    };

STV_FWK2_LIB.APP = {
        s_CFG_ID: "stv_configuracion_app",
        TIPO_DINAMICO: "dinamico",
        TIPO_FIJO: "fijo",
        fn_get_inputs: function()
        {
            var _$cfgApp = $('#'+STV_FWK2_LIB.APP.s_CFG_ID);
            var _D_inputsValues = {};
            if ( (_$cfgApp) && (_$cfgApp.data('stv_L_inputs')))
            {
                var _L_inputs = $.parseJSON(_$cfgApp.data('stv_L_inputs'));
                $.each(_L_inputs, function (n_index, s_elemento) {
                        if (s_elemento.s_tipo == STV_FWK2_LIB.APP.TIPO_DINAMICO)
                        {
                            if (s_elemento.s_parent_id)
                            {
                                var _$parent = $('#' + s_elemento.s_parent_id);
                            }
                            else
                            {
                                var _$parent = $('body');
                            }
                            
                            var _$elementos = _$parent.find(s_elemento.s_selector);
                            
                            _$elementos.each(function() {
                                    if ($(this).hasClass('stv_represent'))
                                    {
                                        _D_inputsValues[$(this).attr('name')] = $(this).data('value');
                                    }
                                    else
                                    {
                                        _D_inputsValues[$(this).attr('name')] = $(this).val();
                                    }
                                });
                                    
                        }
                        else if (s_elemento.s_tipo == STV_FWK2_LIB.APP.TIPO_FIJO)
                        {
                            _D_inputsValues[s_elemento.s_selector] = s_elemento.s_valor;
                        }
                        else
                        {
                            alert("Tipo de input no reconocido, contacte al administrador");
                        }
                    });
                 
            }
            else
            {
                /* Nada */
            }
            return (_D_inputsValues);
        },
        
        fn_definicion: function(s_nombreDefinicion)
        {
            var _$cfgApp = $('#'+STV_FWK2_LIB.APP.s_CFG_ID);
            return (_$cfgApp.data(s_nombreDefinicion));
        }        
    };


STV_FWK2_LIB.LINK = {
        TIPO_AJAX: "ajax",
        TIPO_DOC: "doc",
        fn_click: function(e)
        {
            var _$objectClick = $(e.currentTarget);
            var _s_url = _$objectClick.data('stv_url');
            var _s_tipo = _$objectClick.data('stv_tipo');
            
            if (_s_tipo == STV_FWK2_LIB.LINK.TIPO_AJAX)
            {
                alert('ajax detectado');
            }
            else if (_s_tipo == STV_FWK2_LIB.LINK.TIPO_DOC)
            {
                var _L_inputs = STV_FWK2_LIB.APP.fn_get_inputs();
                var _O_form = '<form action="' + _s_url + '" method="post">';
                $.each(_L_inputs, function(s_index, s_value) {
                    _O_form += '<input type="text" name="'+s_index+'" value="'+s_value+'" />';
                    });
                _O_form += '</form>';
                var _$form = $(_O_form);
                $('body').append(_$form);
                _$form.submit();
            }
            else
            {
                /* No hacer nada con el link */
            }
        }
    };

/* Variable utilizada para identificar si el mouse fue movido en la pantalla o no */
$(document).ready(function() {

    /**
     * Number.prototype.format(n, x, s, c)
     * 
     * @param integer lenDecimal: length of decimal
     * @param integer lenThousands: length of whole part
     * @param mixed   txtThousands: sections delimiter
     * @param mixed   txtDecimals: decimal delimiter
     * @param mixed   prefix: string before
     * @param mixed   postfix: string after
     */
    jQuery.fn.extend({
        stv_formatNumber: function(options) {
            return this.each(function() {
                var $this = $(this);

                var re = '\\d(?=(\\d{' + (options.lenThousands || 3) + '})+' + (options.lenDecimal > 0 ? '\\D' : '$') + ')';

                var _n_value = 0.0;
                if ($this.is('input')) {
                    _n_value = $this.val();

                }
                else {
                    _n_value = $this.data('value');
                }

                if ((_n_value) || (typeof(_n_value) == "number"))
                {

	                _n_value = Number(parseFloat(_n_value)).toFixed(Math.max(0, ~~options.lenDecimal));

                    _s_value = (options.prefix || '')
                        + (options.txtDecimals ? _n_value.replace('.', options.txtDecimals) : _n_value).replace(new RegExp(re, 'g'), '$&' + (options.txtThousands || ','))
                        + (options.postfix || '');
	
	                if ($this.is('input')) {
	                    $this.val(_s_value);

	                }                
	                else {
	                    $this.text( _s_value );
	                    $this.parent('td').css('text-align','right');
	                }

                }
                else {
                	$this.text( "" );
                }

            });
        }
    });
    
	/* Se agrega a las funciones de los elementos jQuery la funcion disabled para usar una clase para disable / enable */
	jQuery.fn.extend({
	    disable: function(state) {
	        return this.each(function() {
	        	var $this = $(this);
	        	$this.toggleClass('disabled', state);
	        });
	    }
	});
	
	/* Configuración para los flashMessage */
	toastr.options = {
          closeButton: true,
          debug: false,
          progressBar: true,
          positionClass: "toast-top-right",
          onclick: null,
          showDuration: 400,
          hideDuration: 1000,
          timeOut: 2000,
          extendedTimeOut: 1000,          
          showEasing: "swing",
          hideEasing: "linear",
          showMethod: "fadeIn",
          hideMethod: "fadeOut",
          tapToDismiss: false
      };

    toastr.optionsInfo = jQuery.extend(true, {}, toastr.options);

    toastr.optionsWarning = jQuery.extend(true, {}, toastr.options);

    toastr.optionsError = jQuery.extend(true, {}, toastr.options);
    toastr.optionsError.timeOut = 10000;
    toastr.optionsError.extendedTimeOut = 0;
    toastr.optionsError.closeMethod = 'fadeOut';
    toastr.optionsError.closeDuration = 300;
    toastr.optionsError.closeEasing = 'swing';   

    toastr.optionsSuccess = jQuery.extend(true, {}, toastr.options);

	/* Create an array with the values of all the input boxes in a column */
	$.fn.dataTable.ext.order['dom-text'] = function  ( settings, col )
	{
	    return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
	        return $(td).data("value");
	    } );
	}

	/* Create an array with the values of all the input boxes in a column, parsed as numbers */
	$.fn.dataTable.ext.order['dom-text-numeric'] = function  ( settings, col )
	{
	    return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
	        var _value = $(td).data("value");
	        if (_value == "None") {
	            _value = 0;
	        } else {}
	        return (_value * 1);
	    } );
	}

    /* Create an array with the values of all the input boxes in a column, parsed as numbers */
    $.fn.dataTable.ext.order['dom-text-datetime'] = function  ( settings, col )
    {
        return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
            var _value = $(td).data("value");
            if (_value == "None") {
                _value = 0;
            } else {
                /* Normalmente la fecha hora viene en el formato "2021-02-01 07:01:06", por lo que si se agrega una T se puede convertir en Date de JS directo */
                _value = new Date(_value.replace(" ","T"));
            }
            return (_value);
        } );
    }

    /* Create an array with the values of all the input boxes in a column, parsed as numbers */
    $.fn.dataTable.ext.order['dom-text-date'] = function  ( settings, col )
    {
        return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
            var _value = $(td).data("value");
            if (_value == "None") {
                _value = 0;
            } else {
                /* Normalmente la fecha hora viene en el formato "2021-02-01", por lo que si se agrega una T se puede convertir en Date de JS directo */
                _value = new Date(_value);
            }
            return (_value);
        } );
    }

    /* Create an array with the values of all the input boxes in a column, parsed as numbers */
    $.fn.dataTable.ext.order['dom-text-time'] = function  ( settings, col )
    {
        return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
            var _value = $(td).data("value");
            if (_value == "None") {
                _value = 0;
            } else {
                _L_time = _value.split(":");
                _value = _L_time[0] * 60 * 60;
                _value += _L_time[1] * 60;
                if (_L_time.length >= 3) {
                    _value += _L_time[0] * 1;
                } else {}   
            }
            return (_value * 1);
        } );
    }
	 
	/* Create an array with the values of all the checkboxes in a column */
	$.fn.dataTable.ext.order['dom-boolean'] = function  ( settings, col )
	{
	    return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
	    	return $(td).data("value") == "True"? 1 : 0;
	    } );
	}
	
});

function stv_applyInspinia($parent) {
    // Collapse ibox function
    $parent.find('.collapse-link').click(function () {
        var ibox = $(this).closest('div.ibox');
        var button = $(this).find('i');
        var content = ibox.find('div.ibox-content');
        content.slideToggle(200);
        button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        ibox.toggleClass('').toggleClass('border-bottom');
        setTimeout(function () {
            ibox.resize();
            ibox.find('[id^=map-]').resize();
        }, 50);
    });

    // Close ibox function
    $parent.find('.close-link').click(function () {
        var content = $(this).closest('div.ibox');
        content.remove();
    });    
}

/** Función que regresa el símbolo de loading
 * 
 * @returns Código HTML como string que contiene el símbolo de loading.
 */
function stv_fwk_createLoading() {
    /* Se agregan las clases para su uso en tabs creados dinámicamente */
    return (
        '<div class="stv_msg_loading" tabindex="-1" style="">'
        + '<div align="center">'
        + '<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="spinner" role="img"' 
        + ' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-spinner fa-w-16 fa-spin fa-lg"' 
        + ' style="height: 50px;">'
        +       '<path fill="currentColor" d="M304 48c0 26.51-21.49 48-48 48s-48-21.49-48-48 21.49-48 48-48 48 21.49 48 '
        +       '48zm-48 368c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48-21.49-48-48-48zm208-208c-26.51 0-48 '
        +       '21.49-48 48s21.49 48 48 48 48-21.49 48-48-21.49-48-48-48zM96 256c0-26.51-21.49-48-48-48S0 229.49 0 '
        +       '256s21.49 48 48 48 48-21.49 48-48zm12.922 99.078c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 '
        +       '48-48c0-26.509-21.491-48-48-48zm294.156 0c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 '
        +       '48-48c0-26.509-21.49-48-48-48zM108.922 60.922c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 '
        +       '48-48-21.491-48-48-48z" class="">'
        +       '</path>'
        + '</svg>'
        + '</div>'
        + '</div>'
        )
  }

/** Asigna la funcionalidad a los inputs de la forma
 * 
 * @param $tab: Objeto jQuery al tab
 * @param $parent: Objeto jQuery a partir del cual se van a asignar las funcionalidades
 * @returns Nada
 */
function stv_fwk_assignFunctionality_form($tab, $parent) {

	/* En elementos con la clase .stv_linked se asocian eventos de onChange para actualizar el contenido */
	$parent.find(".stv_form-control.stv_linked").each( function( ) {
            var _$mainField = $(this);
            var _$tab = _$mainField.closest('.stv_tab_content')
            var _n_linkedfieldname_len = Number(_$mainField.data('linkedfieldname_len'));
            for (var _i=0; _i < _n_linkedfieldname_len; _i++) 
            {
                var _$linkedField = _$tab.find('#' + _$mainField.data('linkedfieldname' + _i.toString()));
                if (_$linkedField.length == 0)
                {
                    alert("Error en link, no se encuentra en ningún elemento");
                }
                else if (_$linkedField.length != 1)
                {
                    alert("Error en link, se encuentra en más de un elemento");
                }
                else 
                {
                    var _n_updatefieldname_len = Number(_$linkedField.data("updatefieldname_len"));
                    var _b_encontrado = false;
                    if (_n_updatefieldname_len > 0)
                    {
                        for (var _k=0; _k < _n_updatefieldname_len; _k++)
                        {
                            if (_$linkedField.data("updatefieldname" + _k.toString()) == _$mainField.attr('id'))
                            {
                                _b_encontrado = true;
                                break;
                            }
                            else
                            {
                                /* Checar el siguiente */
                            }
                        }
                        if (!_b_encontrado)
                        {
                            _$linkedField.data("updatefieldname" + _n_updatefieldname_len, _$mainField.attr('id'));
                            _n_updatefieldname_len += 1;
                        }
                        else
                        {
                            /* nada */
                        }
                        _$linkedField.data("updatefieldname_len", _n_updatefieldname_len);
                    }
                    else
                    {
                        _$linkedField.data("updatefieldname0", _$mainField.attr('id'));
                        _$linkedField.data("updatefieldname_len", 1);
                    }
                    _$linkedField.change(fn_stv_formLinkedFieldChange);
                    _$linkedField.change();
                }                   
            }
            
        });
    
    /* En caso de contenido en las pestañas de detalle con error, se pone en negritas el nombre de la pestaña */
	$parent.find(".detailTabContent").find(".error").closest(".stv_tab").each( function( ) {
            $('#' + $(this).data('idlink')).addClass('stv_errorInTab');
        });
    
    /* Asigna un diferenciador a los elementos stv_fieldRequired */
	$parent.find('.stv_fieldRequired').closest('.form-group').find('label').prepend('*');

    
    /* Asigna objetos de la clase datepicker como editores de fecha */
	$parent.find('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        todayHighlight: true,
        autoclose: true
    });
	
	/* Datepicker especial para solo utulizar mes y año. */
	$parent.find('.datepickermonthyear').datepicker({
        format: 'mm/yyyy',
        todayHighlight: true,
        autoclose: true,
        viewMode: "months", 
        minViewMode: "months"
	});

    $parent.find('.datepickeryear').datepicker({
        format: 'yyyy',
        todayHighlight: true,
        autoclose: true,
        viewMode: "years",
        minViewMode: "years"
	});

	$parent.find('.stv_remove_file').on( 'click', function () {
    	var _$this = $(this);
    	_$this.closest('.input-group').find('.stv_file_uploaded').attr('value', "remove");
    	_$this.closest('.input-group-addon').remove();
    } );
	
	$parent.find('.datetimepicker').datetimepicker({
        format: 'dd/mm/yyyy hh:ii',
        language:  'es',
        weekStart: 0,
        todayBtn:  true,
		autoclose: true,
		todayHighlight: true,
		startView: 2,
		forceParse: false,
        showMeridian: true
    }).datetimepicker('update');

    /* Asigna objetos de diferentes clases relacionadas con chosen, sus propiedades */
    var config = {
        '.chosen-select'           : {width:"100%", no_results_text:'Vacío!', search_contains:true},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Ups! No se encontró nada'},
        '.chosen-select-width'     : {width:"100%"}
    };
    for (var selector in config) {
    	$parent.find(selector).chosen(config[selector]);
    }
    $parent.find('.chosen-select[multiple="multiple"]').each(
    		function() {
                var $this = $(this);
                var _s_value = $this.attr('value');
                if (_s_value)
                {
                	var _L_values = _s_value.split(",");
                	$this.val(_L_values);
                } else {/* Nada que actualizar */}
            	$this.trigger('chosen:updated');
            }
    		);

    //$parent.find('.chosen-select').change();

    /* Asigna objetos de diferentes clases relacionadas con inputmask, sus propiedades */
    var config = {
        '.stv_represent_atgrid.stv_represent_money': 
            {lenDecimal : 2, lenThousands : 3, txtThousands : ',', txtDecimals : '.', prefix : '$ ', postfix : false},
        '.stv_represent_atgrid.stv_represent_percentage': 
            {lenDecimal : 2, lenThousands : 3, txtThousands : ',', txtDecimals : '.', prefix : false, postfix : '%'},
        'input.stv_represent_money': 
            {lenDecimal : 2, lenThousands : 3, txtThousands : ',', txtDecimals : '.', prefix : false, postfix : false},
        'input.stv_represent_percentage': 
            {lenDecimal : 2, lenThousands : 3, txtThousands : ',', txtDecimals : '.', prefix : false, postfix : false},
        'input.stv_represent_number': 
            {lenDecimal : 0, lenThousands : 3, txtThousands : ',', txtDecimals : '.', prefix : false, postfix : false},
    };
    for (var selector in config) {
        $parent.find(selector).stv_formatNumber(config[selector]);
    }

    $parent.find('input.stv_checkbox').iCheck({
    	labelHover: false,
    	cursor: true,
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',    	
	});
    
    $parent.find('textarea.stv_represent_xml').each(
            function() {
                var _O_xmlEditor = CodeMirror.fromTextArea(
                    this,
                    {
                        lineNumbers: true,
                        matchBrackets: true,
                        styleActiveLine: true,
                        lineWrapping: true,
                        readOnly: false,
                        mode: "text/html",
                        }
                    );
                $(this).data("CodeMirror", _O_xmlEditor);
                }
            );

    $parent.find('textarea.stv_represent_json').each(
            function() {
                var _O_xmlEditor = CodeMirror.fromTextArea(
                    this,
                    {
                        lineNumbers: true,
                        matchBrackets: true,
                        styleActiveLine: true,
                        lineWrapping: true,
                        readOnly: false,
                        mode: "json",
                        }
                    );
                $(this).data("CodeMirror", _O_xmlEditor);
                }
            );
                
    $parent.find('.stv_directupload').not(".stv_functionality_done").addClass('stv_functionality_done').on('change', function () {
        var _$this = $(this);
        stv_fwk_ajax(
            _$this.data('uploadurl'), 
            null, 
            "", 
            "form", 
            {
                x_data: $parent,
                x_params: {
                    parent: $parent,  // Se le pasa como data a la función el parent y el tab
                    tab: $tab
                },
                fn_returnAjax: stv_directupload_success, 
            });
        
    } );
    
}

function stv_directupload_success(s_url, O_tab, s_idTarget, s_type, x_config, x_msg) {

    if (String(x_msg.E_return) != String(STV_FWK2_LIB.E_RETURN.OK))
    {
        for (var n_index in x_msg.L_errores)
        {
            stv_fwk_flashMessage({
                type : 'error',
                title : 'Importando CFDI',
                msg : x_msg.L_errores[n_index],
                mode : 'swal'
            });
        }
    } 
    else 
    {        
        if (x_msg.L_errores[0])
        {
            stv_fwk_flashMessage({
                type : 'success',
                title : 'Importando CFDI',
                msg : x_msg.L_errores[0],
                mode : 'swal'
            });
        }
        else
        {
            stv_fwk_flashMessage({
                type : 'success',
                title : 'Importando CFDI',
                msg : 'Sin mensajes',
                mode : 'swal'
            });            
        }
    }
    
    if (x_msg.n_id)
    {
        x_config.x_params.tab.find('[name="stv_ed_find"]').val(x_msg.n_id);
        stv_fwk_repeatLastSearch(x_config.x_params.tab);
    }
    else
    {
        /* Nada */
    }
    
}

/** Acción a ejecutar al seleccionar un tab
 * 
 * @param $tab
 * @param $parent
 * @returns
 */
function stv_fwk_tab_onSelect($tab, $parent) {
    
    /* Se asigna el CodeMirror en caso de represent XML, ya que se tienen problemas asignando 
     * la funcionalidad al bajar el contenido del formulario */
    $parent.find('textarea.stv_represent_xml, textarea.stv_represent_json').each(
        function() {
            var $this = $(this).data("CodeMirror");
            setTimeout(
                function() {
                    $this.refresh();
                    },
                1
                );
            }
        );
        
    if ($parent.data("stv_empty") == true)
    {
        var _s_url = $parent.data("stv_s_url_ifEmpty");
        if (_s_url) {
            stv_fwk_ajax(_s_url, $tab, $parent.attr('id'), "none", {b_hideAllTabRows: false});
        } else {}
        $parent.data("stv_empty", false);
    }
    else
    {
    
    }

}

/**
 * 
 * @param $tab
 * @param $object
 */
function stv_fwk_linktab($tab, $object, s_idRow) {
	var _$this = $object;
	var _$header = _$this.closest('.stv_grid').find(".stv_column[field='"+_$this.attr('field')+"']");
	var _s_tabLinkedName = _$header.data("tablinkedtitle")
	var _s_idHtml = _$header.data("idhtml")
	var _s_tabCurrentField = _$header.data("tabcurrentfield")
	var _s_fieldText = _$this.closest("tr").find("td[field='"+_s_tabCurrentField+"']").data("value");
	var _stv_gIDTabDetails = $tab.parent().closest(".stv_tab").data('stv_gIDTabDetails');
	var _$tabLink = $('#' + _$this.closest(".stv_tab").data("idlink"));
	
	/* Siempre que de click a una opción, debe limpiar las otras pestañas. */
    while ( _$tabLink.closest('li').next().length > 0 )
    {
    	stv_fwk_tabDelete(_$tabLink.closest('li').next());
    }
    
	stv_fwk_tabAdd($('#'+_stv_gIDTabDetails), _s_tabLinkedName, _s_idHtml, ".detailTabList", ".detailTabContent", {hasClose: false}).tab('show');

	_$tabLink.find(".stv_tabtitle").remove();
	_$tabLink.append( "<span class='stv_tabtitle'> [<b>"+_s_fieldText+"</b>]</span>" );
	
	/* Haciendo llamada AJAX y actualizando el contenido del tab */
	stv_fwk_ajax(_$header.data("url") + '/' + s_idRow, $tab, _s_idHtml, "none", {b_hideAllTabRows: false});
}

/**
 * 
 * @param $tab
 * @param $object
 */
function stv_fwk_updateTabName($tab, s_newName) {
	var _s_idLink = $tab.data("idlink");
	$('#' + _s_idLink).text(s_newName);
}


/* Asigna la funcionalidad a los botones de la forma */
function stv_fwk_formatContent($tab) {
    
}

/* Asigna la funcionalidad a los botones de la forma */
function stv_fwk_assignFunctionality_tab($tab) {
	/* Se crea una variable local que apunta al objeto de la forma  */
	var _$gButtonBar = $('#' + $tab.data('stv_gIDTabButtonBar'));
	var _$gTabResults = $('#' + $tab.data('stv_gIDTabResults'));
	var _s_currentAction = $tab.data("stv_s_currentAction");
    
	if (jQuery.isEmptyObject($tab))
	{
	    alert('Error en la referencia al tab');
	}
	else 
	{
	    
        $tab.find('.nav-tabs a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
            });
        
	    /* STV_LINK asignación de funcionalidad */
	    $tab.find('a.stv_link').unbind("click").click(STV_FWK2_LIB.LINK.fn_click);
	    
        if ($tab.data('stv_s_currentViewHtmlClass') == "stv_en_refreshResults") 
    	{
    	    
            if ($tab.data('stv_L_errorsAction')) {
                var _L_errorsAction = $.parseJSON($tab.data('stv_L_errorsAction'));
                if (_L_errorsAction.length > 0)
                {
                    var _s_msgs = "";
                    for (var _idx = 0; _idx < _L_errorsAction.length; _idx++)
                    {
                        _s_msgs += "* " + _L_errorsAction[_idx].msg + "\n";
                    }
                    if (_s_currentAction == stv_gButtons.btn_remove) {
                        swal("Uups!", "El registro no pudo ser eliminado debido a: \n" + _s_msgs, "error");
                    } else {
                        swal("Uups!", "Se encontraron los siguientes errores: \n" + _s_msgs, "error");
                    }
                } else
                {
                    if (_s_currentAction == stv_gButtons.btn_remove) {
                        swal("Eliminado!", "El registro fue eliminado.", "success");
                    } else {
                        /* Nada */
                    }
                }
            } else { /* Nada */ }
            
    		/* Se habilitan todos los botones para poder ejecutar la accuón de búsqueda */
    		_$gButtonBar.find(".stv_visible").disable(false);
    		stv_fwk_repeatLastSearch($tab);
    	}
        else if ($tab.data('stv_s_currentViewHtmlClass') == "stv_en_refreshResultsParent") 
        {
            var _$tabParent = $tab.parent().closest(".stv_tab");
            if (_$tabParent) {
                var _$gButtonBarParent = $('#' + _$tabParent.data('stv_gIDTabButtonBar'));
                
                /* Se habilitan todos los botones para poder ejecutar la accuón de búsqueda */
                _$gButtonBarParent.find(".stv_visible").disable(false);
                stv_fwk_repeatLastSearch(_$tabParent);
            } else {}
        }
    	else 
    	{ 	
    	
    		/* Asigna los tooltip */
        	    $tab.find(".stv_hint").tooltip({
                container: $tab,
            });
            
        	    /* Tiene que ser hasta el final para poder obtener el texto para realizar la búsqueda */
        	    stv_fwk_configureView($tab, $tab.data('stv_s_currentViewHtmlClass'), $tab.data("stv_L_currentRowsToShow"));
    
            /* Asignar las funciones de presionado de los botones */
            _$gButtonBar.find(".btn-primary").unbind( "click" ).click( stv_fwk_buttonBarClick );
            _$gButtonBar.find(".dropdown-menu > li").unbind( "click" ).click( stv_fwk_buttonBarClick );
            /* Se eliminan los eventos de keypress y se agrega un evento de keypress */
            $tab.find(".stv_buttonsBarFind").find("[name=stv_ed_find]").unbind( "keypress" ).keypress(function (e) {
                var key = e.which;
                /* Si es la tecla enter... */
                if (key == 13)  // the enter key code
                {
                    /* ...se simula que se preciona el primer boton dentro del elemento stv_buttonsBarFind. */
                	    $(this).closest(".stv_buttonsBarFind").find("button.btn-primary, button.btn-info").first().click();
                    return false;  
                } 
                else 
                { /* ...en caso de presionar otra tecla, no hacer nada */ }
            });
    
            /* Funcionalidad de plus y minus para abrir renglon debajo del row del grid y mostrar mas información.
             * Esta funcionalidad es solamente para rowSearchResults de tipo grid */
            _$gTabResults.find('.stv_grid').off('click', 'td.stv_rowresults_plus').on( 'click', 'td.stv_rowresults_plus', function (e) {

                var _$tr = $($(this).closest('tr'));
                let O_dataTable = $('#' + $tab.data('stv_gIDTabResults')).find('.stv_grid').DataTable();
                var _O_row = O_dataTable.row( _$tr );
                if ( _O_row.child.isShown() )
                {
                    _O_row.child.hide();
                	_$tr.removeClass('shown');
                }
                else
                {
                    if (
                        (!_$tr.hasClass('stv_selected'))
                        && (!_$tr.hasClass('selected'))
                        )
                    {
                        _$tr.click();
                        O_dataTable.rows().deselect();
                        _O_row.select();
            	    }
                    else
                    {
                    }

                    var _s_idRow = stv_fwk_getSelectedIds($tab, _$gTabResults, false);
                    var _s_idDiv = _$gTabResults.attr('id') + "_" + _s_idRow;
                    _O_row.child('<div id="'+_s_idDiv+'" class=""></div>', "stv_row_plus").show();
                    var _s_url = $(this).closest('.stv_grid').find(".stv_column[field='"+$(this).attr('field')+"']");
                    if (_s_url) {
                        _s_url = _s_url.data("url");
                    } else {}

                    stv_fwk_ajax(_s_url + "/" + _s_idRow, $tab, _s_idDiv, "find", { b_hideAllTabRows : false, b_ignorarCamposBuscar : true });
                    _$tr.addClass('shown');
                }
                e.stopPropagation();
            } );

            /* Funcionalidad de link para abrir ejecutar una funcion javascript.
             * Esta funcionalidad es solamente para rowSearchResults de tipo grid */
            _$gTabResults.find('.stv_grid').off('click', 'td.stv_rowresults_linktab').on( 'click', 'td.stv_rowresults_linktab', function (e) {
                var _$tr = $($(this).closest('tr'));
                let O_dataTable = $('#' + $tab.data('stv_gIDTabResults')).find('.stv_grid').DataTable();
                var _O_row = O_dataTable.row( _$tr );
        	        if (
                        (!_$tr.hasClass('stv_selected'))
                        && (!_$tr.hasClass('selected'))
                        ) {
                        _$tr.click();
                        O_dataTable.rows().deselect();
                        _O_row.select();
        	        } else {}
                var _s_idRow = stv_fwk_getSelectedIds($tab, _$gTabResults, false);
                try {
                    var _b_continue = stv_fwk_linktab($tab, $(this), _s_idRow);
                }
                catch(err) {

                }
    	        e.stopPropagation();
            } );

            _$gTabResults.find('.stv_grid').off('click', 'td.stv_rowresults_link').on( 'click', 'td.stv_rowresults_link', function (e) {
                var _$tr = $($(this).closest('tr'));
                let O_dataTable = $('#' + $tab.data('stv_gIDTabResults')).find('.stv_grid').DataTable();
                var _O_row = O_dataTable.row( _$tr );
                    if (
                        (!_$tr.hasClass('stv_selected'))
                        && (!_$tr.hasClass('selected'))
                        ) {
                        _$tr.click();
                        O_dataTable.rows().deselect();
                        _O_row.select();
                    } else {}
                var _s_idRow = stv_fwk_getSelectedIds($tab, _$gTabResults, false);
                if ($(this).data("jsfunc")) {
                    try {
                        var _fn_call = eval($(this).data("jsfunc"));
                        var _b_continue = _fn_call($tab, $(this), _s_idRow);
                    }
                    catch(err) {

                    }
                } else {
                    stv_fwk_ajax($(this).data("url") + "/" + _s_idRow, $tab, ":eval", "find", { b_hideAllTabRows : false });
                }
                e.stopPropagation();
            } );
            
            /* Funcionalidad de grid para remover los registros seleccionados en caso de cambiar de pagina.
             * Esta funcionalidad es solamente para rowSearchResults de tipo grid */        
            _$gTabResults.find('.stv_grid').off('page.dt').on( 'page.dt', function (e) {
                    var _$gTabResults = $(e.target).closest(".stv_rowSearchResults");
                    var _$tab = _$gTabResults.closest('.stv_tab');
                    // Probablement ya no se aplica
                    //_$gTabResults.find('tbody:first>tr.stv_selected').removeClass('stv_selected');
                    stv_fwk_configureView(_$tab, false, false);
            });
            // _$gTabResults.find('.stv_grid').off('order.dt').on( 'order.dt', function (e) {
            //         var _$gTabResults = $(e.target).closest(".stv_rowSearchResults");
            //         var _$tab = _$gTabResults.closest('.stv_tab');
            //         // Probablement ya no se aplica
            //         //_$gTabResults.find('tbody:first>tr.stv_selected').removeClass('stv_selected');
            //         stv_fwk_configureView(_$tab, false, false);
            // });
            _$gTabResults.find('.stv_grid').off('search.dt').on( 'search.dt', function (e) {
                    var _$gTabResults = $(e.target).closest(".stv_rowSearchResults");
                    var _$tab = _$gTabResults.closest('.stv_tab');
            	    // Probablement ya no se aplica
                    //_$gTabResults.find('tbody:first>tr.stv_selected').removeClass('stv_selected');
            	    stv_fwk_configureView(_$tab, false, false);
            });
    
            	_$gButtonBar.find(".stv_act_conditional").each(function() {
            		var _o_this = $(this);
            		var _s_btnLinked = _o_this.data("cond-buttoncodelinked");
            		var _o_btnLinked = _$gButtonBar.find('[data-action="' + _s_btnLinked + '"]');
            		
            		if (_o_btnLinked) {
            			var _s_field = _o_this.data("cond-field");
            	        var _b_isActive = (
                            ($(_$gResultsGrid.find('tbody:first>tr.stv_selected')[0]).children("[field='"+_s_field+"']").data('value') == 'True')
                            || ($(_$gResultsGrid.find('tbody:first>tr.selected')[0]).children("[field='"+_s_field+"']").data('value') == 'True')
                            );
            			if (_b_isActive) {
            				_o_btnLinked.show();
            				_o_this.hide();
            			} else {
            				_o_btnLinked.hide();
            				_o_this.show();
            			}
            		} else {
            			/* Nada */
            		}
        		});
        	
                            
            /* Asignar la funcionalidad de hacer request de búsqueda */
            $tab.find(".stv_dbsearch").unbind( "click" ).click( fn_stv_fwk_formDBSearchButtonClick );

            $tab.find(".stv_input_limpia").unbind("click").click(fn_stv_fwk_inputLimpiaButtonClick);
    
            /* Asigna objetos de diferentes clases relacionadas con inputmask, sus propiedades */
            /* Se esta procesando en otra funcion
            var config = {
                '.stv_represent_atgrid.stv_represent_money': 
                    {lenDecimal : 2, lenThousands : 3, txtThousands : ',', txtDecimals : '.', prefix : '$ ', postfix : false},
                '.stv_represent_atgrid.stv_represent_percentage': 
                    {lenDecimal : 2, lenThousands : 3, txtThousands : ',', txtDecimals : '.', prefix : false, postfix : '%'},
                'input.stv_represent_money': 
                    {lenDecimal : 2, lenThousands : 3, txtThousands : ',', txtDecimals : '.', prefix : false, postfix : false},
                'input.stv_represent_percentage': 
                    {lenDecimal : 2, lenThousands : 3, txtThousands : ',', txtDecimals : '.', prefix : false, postfix : false},
                'input.stv_represent_number': 
                    {lenDecimal : 0, lenThousands : 3, txtThousands : ',', txtDecimals : '.', prefix : false, postfix : false},
            };
            for (var selector in config) {
                $tab.find(selector).stv_formatNumber(config[selector]);
            }
            $tab.find('.stv_represent_atgrid.stv_represent_boolean').parent('td').css('text-align','center');
            */
    
            var config = {
                '.stv_represent_atgrid.stv_represent_money': 
                    {lenDecimal : 2, lenThousands : 3, txtThousands : ',', txtDecimals : '.', prefix : '$ ', postfix : false},
                '.stv_represent_atgrid.stv_represent_percentage': 
                    {lenDecimal : 2, lenThousands : 3, txtThousands : ',', txtDecimals : '.', prefix : false, postfix : '%'},
                '.stv_represent_atgrid.stv_represent_number': 
                    {lenThousands : 3, txtThousands : ',', txtDecimals : '.', prefix : false, postfix : false},
            };
            for (var selector in config) {
                _$gTabResults.find(selector).stv_formatNumber(config[selector]);
            }
            _$gTabResults.find('.stv_represent_atgrid.stv_represent_boolean').parent('td').css('text-align','center');
            
            /* Asignar la funcionalidad a botones adicionales a la forma */
            $tab.find(".stv_button_additional").unbind( "click" ).click( stv_fwk_buttonBarClick );
            
            /* En caso de que exista algo específico en la página del cliente, se usa el siguiente callback para formatear la forma */
            if (typeof stv_app_assignFunctionality_tab == 'function') { 
            	stv_app_assignFunctionality_tab($tab); 
        	} else { /* Nada */ }
            
            try {
                var _fn_app = eval('stv_app_'+$tab.attr('id')+'_assignFunctionality_tab_after');
                var _b_continue = _fn_app($tab);
            }
            catch(err) {
                
            }
                    
    	}
	}
    
}


function fn_stv_updateList(s_url, O_tab, s_idTarget, s_type, x_config, x_msg) {

    var _$select = x_config.x_params.O_elemento;
    var _s_nuevoValor = x_config.x_params.s_valor;
    
    /* Se seleccionan los option, excepto por el de ninguno */
    _$select.data('linkedvalue', _s_nuevoValor);
    
    _$select.children(':not([value=""])').remove();
    $.each(x_msg['data'], (
        function(index, optionData) {
            var _$option = $("<option></option>").attr("value", optionData['id']).text(optionData['optionname']);
            
            if ( 
                (_$select.attr("value")) 
                && (_$select.attr("value").toString() == optionData['id'].toString()) 
                ) {
                _$option.attr("selected", "selected");
            } else {
                /* Si es la seleccionada, no se remueve */
                if (optionData['disabled']) {
                    _$option.attr("disabled", "disabled");
                } else { }
                
            }
            
            _$select.append(_$option);
            }
        ));
    _$select.trigger("chosen:updated");
    _$select.change();
    
}

function fn_stv_formLinkedFieldChange(e)
{
    var _$linkedField_changed = $(this);
    var _n_updatefieldname_len = Number(_$linkedField_changed.data('updatefieldname_len'));
    var _$tab = _$linkedField_changed.closest('.stv_tab_content');

    for (var _i = 0; _i < _n_updatefieldname_len; _i++)
    {
        var _$mainField = _$tab.find('#' + _$linkedField_changed.data('updatefieldname' + _i.toString()));
        
        var _n_linkedfieldname_len = Number(_$mainField.data('linkedfieldname_len'));
        var _L_values = [];
        for (var _k=0; _k < _n_linkedfieldname_len; _k++) 
        {
            var _$linkedField = _$tab.find('#' + _$mainField.data('linkedfieldname' + _k.toString()));
            if (_$linkedField.length == 0)
            {
                alert("Error en link, el elemento de referencia no se encuentra");
            }
            else if (_$linkedField.length != 1)
            {
                alert("Error en link, se encuentra en más de un elemento");
            }
            else 
            {
                if (_$linkedField.val())
                {
                    _L_values[_k] = _$linkedField.val().toString();
                }
                else
                {
                    _L_values[_k] = 0
                }
            }                   
        }
        var _s_linkedFieldsValue =  _L_values.join("/");
        var _s_lastlinkedValue = _$mainField.data('linkedvalue');
        var _linkedUrl = _$mainField.data('linkedurl');
        if (_s_linkedFieldsValue != _s_lastlinkedValue)
        {
            stv_fwk_ajax(
                _linkedUrl + '/' + _s_linkedFieldsValue, 
                null, 
                "", 
                null, 
                {
                    x_params: {
                        O_elemento: _$mainField,
                        s_valor: _s_linkedFieldsValue
                    },
                    fn_returnAjax: fn_stv_updateList, 
                });     
            _$mainField.data('linkedvalue', _s_linkedFieldsValue);      
        } else { /* No actualizar el select */ }        
    }    
    
}


function fn_stv_fwk_inputLimpiaButtonClick(e)
{
    let _$objectClick = $(e.currentTarget);
    let _$tab = _$objectClick.closest(".input-group");
    // let _s_nombreInput = _$objectClick.attr('nombreinput'); # TODOMejora uso futuro
    // let _$input = _$tab.find("input[name='"+_s_nombreInput+"']");
    let _$inputs = _$tab.find("input");
    _$inputs.val("");
    return false;
}

function fn_stv_fwk_formDBSearchButtonClick(e)
{
    var _$objectClick = $(e.currentTarget);
    var _$tab = $(_$objectClick.closest(".stv_tab"));
    var _s_stv_gIDTabModal = _$tab.data("stv_gIDTabModal");
	var _s_link = _$objectClick.data('action-modal-url') + 
			"/" + _$objectClick.data('action') + 
			"/" + $('#'+_$objectClick.data('action_search_edinput_id')).val();

    $('#'+_s_stv_gIDTabModal).find('.modal-content').html("");
	$('#'+_s_stv_gIDTabModal).modal("show");
	
	var _O_stvSearchFilter = _$objectClick.data('stv_search_filter');
	var _b_searchFilterError = false;
	if (_O_stvSearchFilter)
	{
	    $.each( _O_stvSearchFilter, function( n_key, O_filter ) {
	            _s_element_id = "#" + O_filter.s_input_name.replace('.','_');
	            _$_element = _$tab.find(_s_element_id);
	            if ( (O_filter.b_mandatory) && !(_$_element) )
                {
	                alert(_s_element_id + ' es mandatorio para la búsqueda y no fue encontrado');
	                _b_searchFilterError = true;
                }
	            else
                {
	                _O_stvSearchFilter[n_key].x_value = _$_element.val();
                }
	        });
	} else {}

	var _x_data = {
	        stv_search_edinput_id:_$objectClick.data('action_search_edinput_id'),
	        stv_search_edinput_format:_$objectClick.data('action_search_edinput_format'),
	        stv_search_filter:_O_stvSearchFilter
	        };
	
	stv_fwk_ajax(
	    _s_link, 
	    _$tab, 
	    _s_stv_gIDTabModal + "_content", 
	    "none", 
	    { 
	        x_data : _x_data, 
	        fn_returnAjax: false, 
	        fn_returnError: false,  
	        b_hideAllTabRows: false,
	        b_dontSaveAction: true
	        }
	    );

}

/* Asigna la funcionalidad del layout principal */
function stv_fwk_assignFunctionality_layout()
{

	
	
}


function stv_fwk_buttonEvent($tab, o_button, o_option)
{
	var _s_actualView = $tab.data("stv_actualView");
    var _s_link = $tab.data('stv_gLinkFunction') + "/" + o_button.data('action');
    var _b_continue = true;
    
    /* Si se presionó la opcion, se agrega al código. */
    if (o_option) {
    	_s_link += "_" + o_option.data('code')
	} else {}
	
    /* Si existe definida la función before en el boton, se manda llamar. */
	if ($tab.data("action-js-before")) {
		var _fn_actionBefore = eval($tab.data("action-js-before"));
		_b_continue = _fn_callback($tab, o_button, _s_link); 
	} else {}
	
	if ((true == _b_continue) && ($tab.data("action-js-after"))) {
		var _fn_actionBefore = eval($tab.data("action-js-after"));
		_b_continue = _fn_callback($tab, o_button, _s_link); 
	} else {}
	
	if (true == _b_continue) {
		/* Manetener referencia del boton previo en caso de error en los resultados de ajax */
		$tab.data('stv_button_beforelast', $tab.data('stv_button_last'));
		/* Mantener la referencia del boton presionado */
		$tab.data('stv_button_last', o_button.data('action'));
	} else { /* Mantener previo boton presionado */ }

}

function stv_fwk_buttonBarClick(e) {
    $('.datetimepicker.dropdown-menu').hide(); /* Workaround para ocultar los datetimepickers */
    var $button = $(e.currentTarget);
    var _s_jsPress = $button.data('action-js-fnonpress');
    
    /* Si no esta definida la función a ejecutar en el boton... */
    if (!_s_jsPress) {
        /* ...se define la función por default. */
        _s_jsPress = 'stv_fwk_button_press';
	} else {}

    /* Si el boton no esta desabilitado, no tiene submenu, y tiene una función definida... */
    if ( (!$button.hasClass('disabled')) 
        && (!$button.hasClass('dropdown-toggle'))
        && (_s_jsPress)) {
    	    /* ...ejecuta la función definida. */
        var _fn_jsPress = eval(_s_jsPress);
        _fn_jsPress(e);
    } else {
    	    /* ...no se hace nada. */
    }
}

/**
 * Configura la vista, habilitando o desabilitando los botones, mostrando los TabRows especificados
 * @param $tab
 * @param s_viewHtmlClass: clase de la vista con la que se seleccionan los botones a habilitar
 * @param x_rowsToShow: lista con los TabRows a mostrar
 */
function stv_fwk_configureView($tab, s_viewHtmlClass, x_rowsToShow) {
	
	var _$gButtonBar = $('#' + $tab.data('stv_gIDTabButtonBar')); /* Referencia a la barra de botones TabRow */
	var _$gIDTabResults = $('#' + $tab.data('stv_gIDTabResults')); /* Referencia a los resultados TabRow  */
	var _$gIDTabMain = $('#' + $tab.data('stv_gIDTabMain')); /* Referencia a los resultados TabRow  */
	var _L_rowsToShow = []; /* Lista con los TabRows a mostrar */
	var _s_viewHtmlClass = "None"; /* Clase actual de la vista que se esta desplegando o que se va a desplegar */
	var _n_rowsSelected = 0; /* Contendrá el número de registros seleccionados */
    var _L_rowsSelected = []; /* Referencias a los registros seleccionados */
	var _b_resultsAreVisible = false; /* Define si la vista de resultados esta visible o no */

	/* Si esta definido rowsToShow... */
	if (x_rowsToShow) {
		/* Si es de tipo string, interpreta que es JSON y lo convierte a lista, esperando que el JSON sea un []... */
	    if (typeof(x_rowsToShow) === 'string'){
	    	_L_rowsToShow = $.parseJSON(x_rowsToShow);
	    /* ...de lo contrario, lo interpreta como si fuera una lista. */
	    } else {
			_L_rowsToShow = x_rowsToShow;
	    }
	/* ...si no esta especificado, toma los actuales definidos en el tab. */
	} else {
		_s_json = $tab.data('stv_L_currentRowsToShow');
		if (_s_json)		
		{
			_L_rowsToShow = $.parseJSON(_s_json);			
		}
		else
		{
			_L_rowsToShow = [];
		}
	}
	
	/* Para cada TabRow definido... */
	$tab.find(".stv_tab_content:first>div.stv_tabRow").each(function() {
		var _$this = $(this);
		var _b_found = false;
		/* Se busca que este en la lista de los tabRow visibles */
		for (var _n_index in _L_rowsToShow) {
			if (_$this.hasClass(_L_rowsToShow[_n_index])) {
				_b_found = true;
				break;
			} else {}
		}
		if (_b_found) {
		    /* En caso de ser encontrado y el id corresponde a los resultados de búsqueda... */
			if (_$this.attr('id') == _$gIDTabResults.attr('id')) {
			    /* ...se ajustan las columnas, previniendo a bug en la vista de las columnas que se distorcionan */
				_b_resultsAreVisible = true;
				_$this.show("fast", function () {
					$('#' + $tab.data('stv_gIDTabResults')).find('.stv_grid').DataTable().columns.adjust(); /* Fix que ajusta las columnas, despues de mostrar el tabRow de resultados */
				});
			} else {
			    /* ...de lo contrario, se hace scroll a la vista */
				_$this.show("fast", function () { 
					$("body").scrollTop($tab.offset().top);
				});
			}
		} else {
			if (_$this.hasClass('stv_msg_loading')) {
			    /* Si el tabRow es stv_msg_loading, no se oculta, sino que hasta el complete de la llamada ajax es ejecutada */
			} else {
			    /* Si es otro tipo de tabRow que no se require estar visible, se oculta */
				_$this.hide("fast");
			}
		}
	});

	/* Si esta definida la clase, se usa, de lo contrario, se obtiene la clase definida en el Tab. */
	if (s_viewHtmlClass) {
		_s_viewHtmlClass = s_viewHtmlClass;
	} else {
		_s_viewHtmlClass = $tab.data('stv_s_currentViewHtmlClass');
	}

	_$gButtonBar.find(".stv_visible").disable(true);
	
	if (_s_viewHtmlClass == 'stv_msg_loading') {
	    /* Si se intenta visualizar el loading, se muestra directamente */
		$tab.find(".stv_tabRow." + _s_viewHtmlClass).show();
	} else {
		
		/* Si se esta desplegando el TabRow de resultados de búsqueda... */
		if (_b_resultsAreVisible) {
            if ($tab.data("stv_E_rowSearchResults_type") == STV_FWK2_LIB.APP.fn_definicion("stv_E_ROWSEARCHRESULTS_TYPE_GRID")) {
                //let O_datatable = $tab.find("table.stv_grid[id$='grid']:first").DataTable();
                    //$('#' + $tab.data('stv_gIDTabGrid')).DataTable();
                _L_rowsSelected = _$gIDTabResults.find('tbody:first>tr.selected');
                //_L_rowsSelected = _$gIDTabResults.find('tbody:first>tr.selected');
    			_n_rowsSelected = _L_rowsSelected.length; /* Se obtiene el número de registros seleccionados */

            } else if ($tab.data("stv_E_rowSearchResults_type") == STV_FWK2_LIB.APP.fn_definicion("stv_E_ROWSEARCHRESULTS_TYPE_TREE")) {
                _L_rowsSelected = _$gIDTabResults.find('div.stv_tree .stv_selected');
                _n_rowsSelected = _L_rowsSelected.length; /* Se obtiene el número de registros seleccionados */

            } else {
                _L_rowsSelected = _$gIDTabResults.find('tbody:first>tr.stv_selected')
                _n_rowsSelected = _L_rowsSelected.length; /* Se obtiene el número de registros seleccionados */
                
            } 
			/* Si los seleccionados son uno, la clase de la vista es selectedrow, si son más seleccionados son seletcedrows,
			 * de lo contrario se queda la clase anteriormente definida. */
			if (_n_rowsSelected == 1) {
				_s_viewHtmlClass = "stv_en_selectedrow";
			} else if (_n_rowsSelected > 1) {
				_s_viewHtmlClass = "stv_en_selectedrows";		
			} else {
				/* Si no hay registros seleccionados, se busca stv_ed_selectedIds, si se encuentra, se selecciona el registro definido */
				/* Temporalmente no hace nada, definir que hacer cuando tiene una seleccion en el tab */
				//var _s_ids = _$gButtonBar.find("input[name=stv_ed_selectedIds]").val();
				/* TODO, actualmente funciona con una seleccion simple, pero aqui debe buscarse si el valor empieza con IDS_, descomponer los ids y seleccionar los registros */
				//_$gIDTabResults.find('td[data-type=id][data-value='+_s_ids+']').click();
			}
		} else { /* Mantener clase definida */ }
		
		/* Se habilitan o desabilitan los botones como sea necesario */
		_$gButtonBar.find("."+_s_viewHtmlClass).disable(false);
	
		/* Si existen configuraciones tipo swap, para cada una... */
		_$gButtonBar.find(".stv_visible.stv_btnFunc_swap").each( function () {
			var _$this = $(this);
			var _$divButton = _$this.closest( "div.btn-group" ); 
			var _s_fieldValue = "False";
			
			/* Si se estan mostrando los resultados... */
			if (_b_resultsAreVisible) {
				/* Si no se tiene ninguno seleccionado, se usa el que tenga default definido como true para mostrarlo. */
				if (_n_rowsSelected == 0) {
					if ((_$this.data('action-swap-default') == true) || (_$this.data('action-swap-default') == "True")) {
						_$divButton.show();
					} else {
						_$divButton.hide();
					}
				/* ..., si tiene uno seleccionados, se busca el campo y dependiendo del valor se muestra o no el boton swap. */
				} else if (_n_rowsSelected >= 1) {
					_s_fieldValue = stv_fwk_getSelectedRow_field($tab, _$this.data('action-swap-field'), 'value');
                    if (_s_fieldValue == "None") {
                        _s_fieldValue = "False";
                    } else {}
					if (_s_fieldValue == String(_$this.data('action-swap-value'))) {
						_$divButton.show();
					} else {
						_$divButton.hide();
					}
				/* ..., se desabilita el boton */
				} else {
					
					_$this.disable(true);
				}
			/* ..., si se esta mostrando la form, se busca el campo en el form y dependiendo del valor se muestra o no el boton swap. */
			} else {
				var _O_fieldValue = _$gIDTabMain.find("input[name="+_$this.data('action-swap-field').replace(".", "_")+"]");
				if (_O_fieldValue && ($(_O_fieldValue).val())) {
					if ($(_O_fieldValue).val() == String(_$this.data('action-swap-value'))) {
						_$divButton.show();
					} else {
						_$divButton.hide();
					}
				} else {
					if ((_$this.data('action-swap-default') == true) || (_$this.data('action-swap-default') == "True")) {
						_$divButton.show();
					} else {
						_$divButton.hide();
					}
					_$this.disable(true);
				}
			}
		} );
		
		_$gButtonBar.find(".stv_visible.stv_btnFunc_onConfigureView").each( function () {
			var _$this = $(this);
		    /* Si existe definida la función before en el boton, se manda llamar. */
			try
			{
        			if (_$this.data("action-js-fnonconfigureview")) {
        				var _fn_actionOnConfigureView = eval(_$this.data("action-js-fnonconfigureview"));
        				_fn_actionOnConfigureView($tab, _$this, _b_resultsAreVisible, _n_rowsSelected, _L_rowsSelected); 
        			} else {}
			}
			catch(err)
			    {
			    
			    }
		});
		
	}

}

function stv_fwk_getSelectedIds($tab, x_IDTabResults, b_acceptMultiple)
{
	var _L_rowsSelected = [];
	var _s_result = "";
	
	if (jQuery.type( x_IDTabResults ) === "string") {
		var _$IDTabResults = $('#'+x_IDTabResults);
	} else {
		var _$IDTabResults = x_IDTabResults;
	}
	
    if ($tab.data("stv_E_rowSearchResults_type") == STV_FWK2_LIB.APP.fn_definicion("stv_E_ROWSEARCHRESULTS_TYPE_GRID")) {
        _L_rowsSeleccionados = _$IDTabResults.find('tbody:first>tr.selected');

    } else if ($tab.data("stv_E_rowSearchResults_type") == STV_FWK2_LIB.APP.fn_definicion("stv_E_ROWSEARCHRESULTS_TYPE_TREE")) {
        _L_rowsSeleccionados = _$IDTabResults.find('div.stv_tree .stv_selected');

    } else {
        _L_rowsSeleccionados = _$IDTabResults.find('tbody:first>tr.stv_selected');
        
    } 
    /* En caso de grid */
	_L_rowsSeleccionados.each( function( ) {
		var _L_idsSelectedRow = [];
		var $this = $(this);
		_L_rowsSelected.push(stv_fwk_getRowIds($tab, $this));
    });
    /* En caso de tree */
    

	if (_L_rowsSelected.length == 1) {
		_s_result = _L_rowsSelected[0]
	} else if (_L_rowsSelected.length > 1) {
		_s_result = "IDS_" + _L_rowsSelected.join("_");
	} else {
	    _s_result = "";
	}
	
	if ((_L_rowsSelected.length > 1) && (!b_acceptMultiple)) {
		_s_result = "";
		stv_fwk_flashMessage({
			type : 'error',
			title : 'stv_fwk_getSelectedIds',
			msg : 'Multiple seleccion no permitida para operación',
			mode : 'swal'
		});
	} else { /* Manten resultado obtenido */ }
	return(_s_result);
}

function stv_fwk_getRowIds($tab, $row)
{
    var _L_nameFieldIDs = $.parseJSON($tab.data('stv_lNameFieldIds'));
    var _L_idsRow = []
        
    for (var _n_index in _L_nameFieldIDs) {
        _L_idsRow.push($row.find("[field='" + _L_nameFieldIDs[_n_index] + "']").data('value'));
    }

    return(_L_idsRow.join("-"));
}

/* Busca id con el contenido en x_data y regresa los objetos */
function stv_fwk_getRowObjectById($tab, x_data)
{
    var _L_nameFieldIDs = $.parseJSON($tab.data('stv_lNameFieldIds'));
    var _L_idsRow = []
        
    
    
    for (var _n_index in _L_nameFieldIDs) {
        _L_idsRow.push($row.find("[field='" + _L_nameFieldIDs[_n_index] + "']").data('value'));
    }

    return(_L_idsRow.join("-"));
}

function stv_fwk_updateRowContent(s_url, $tab, s_idTarget, s_type, x_config, x_msg) {
    
    /* Si esta deslogeado... */
    var _L_nameFieldIDs = $.parseJSON($tab.data('stv_lNameFieldIds'));
    var _D_data = x_msg;
    var _$dataTable = $('#' + $tab.data('stv_gIDTabResults')).find('.stv_grid').DataTable();
    var _L_dbRows = _D_data.L_actualizar;
    var _s_time = (new Date).getTime().toString();
    
    $('#' + $tab.data('stv_gIDTabGrid')).find('tbody > tr').removeClass('stv_row_actualizado');
    
    for (var _n_index_dbRow in _L_dbRows) {
        var _a_selectorRow = [];
        for (var _n_index in _L_nameFieldIDs) {
            _a_selectorRow.push("[field='" + _L_nameFieldIDs[_n_index] + "'][data-value='" + _L_dbRows[_n_index_dbRow][_L_nameFieldIDs[_n_index]].value + "']");
        }
        
        var _$rowToUpdate = $('#' + $tab.data('stv_gIDTabResults')).find('.stv_grid').find(_a_selectorRow.join(",")).parent('tr');
        _$rowToUpdate.addClass('stv_row_actualizado');
        
        if (_$rowToUpdate) {
            for (var _s_index_data in _L_dbRows[_n_index_dbRow]) {
                var _$cell = $(_$rowToUpdate.find("[field='" + _s_index_data + "']"));
                
                if (_$cell) {
                    var _dbRow = _L_dbRows[_n_index_dbRow][_s_index_data];
                    _$cell.data("value", _dbRow.value);
                    if (_$cell.is("td")) {
                        _$cell.html(_dbRow.represent)
                    }
                    else {
                        _$cell.attr("value", _dbRow.represent);
                    }
                } else {}
            }            
        } else {}
    }
    
    if (_D_data.b_removersinoactualizado) {
        $('#' + $tab.data('stv_gIDTabGrid')).find('tbody > tr').not('.stv_row_actualizado').remove();
    } else {}
    
    
    stv_fwk_assignFunctionality_tab($tab);    
};

/**
 * Presenta un mensaje en pantalla
 * @param x_msgs: es un objeto con las siguientes propiedades:
 * 		- type: especifica el tipo de mensaje puede ser 'info', 'success', 'warning' ó 'error'
 * 		- title: especifica el título a desplegar en el mensaje
 * 		- msg: es el mensaje a desplegar
 * 		- mode: especifica si el mensaje se despliega toastr 'stv_flash' ó 'swal', otro caso se usa alert
 */
function stv_fwk_flashMessage(x_msgs) {
	// toastr puede tener 'info', 'success', 'warning' ó 'error'
	// mode puede ser 'stv_flash' ó 'swal'
	var _a_msgs = new Array();
	if (jQuery.type( x_msgs ) === "array") {
		_a_msgs = x_msgs;
	} else if (jQuery.type( x_msgs ) === "object")
	{
		_a_msgs.push(x_msgs);		
	}
	else
	{
		_a_msgs.push({
				type : 'info',
				title : 'Mensaje',
				msg : x_msgs,
				mode : 'stv_flash'
			});
	}
	
	$.each( _a_msgs, function( key, value ) {
		var _s_type = 'type' in value ? value['type'] : 'info'; 
		var _s_title = 'title' in value ? value['title'] : 'Sin título'; 
		var _s_msg = 'msg' in value ? value['msg'] : 'Sin mensaje'; 
		var _s_mode = 'mode' in value ? value['mode'] : 'stv_flash';
		if (_s_mode === 'stv_flash') {
		    var _D_options = toastr.options;
		    if (_s_type == 'info') {
		        _D_options = toastr.optionsInfo;
            } else if (_s_type == 'warning') {
                _D_options = toastr.optionsWarning;
            } else if (_s_type == 'error') {
                _D_options = toastr.optionsError;
            } else if (_s_type == 'success') {
                _D_options = toastr.optionsSuccess;
            }
		    toastr[_s_type](_s_msg, _s_title, _D_options);
		} else if (_s_mode === 'swal') {
			swal(_s_title, _s_msg, _s_type);
		} else {
			alert( _s_msg );
		}
	});
    return;
}

/**
 * 
 * @param e
 */
function stv_fwk_tabDeleteButtonClick(e) {
    /* Se obtiene la referencia al link (pestaña del tab) */
    var $_close = $(this).closest( "li" );
    stv_fwk_tabDelete($_close);
}

/* Elimina un tab de la pantalla con todo y su pestaña 
 * > sTabLinkId es el id único de la pestaña o el objeto */
function stv_fwk_tabDelete(x_idTabLink) {
    var _$_idTabLink;
    
    /* Si el parámetro es de tipo string... */
    if (jQuery.type( x_idTabLink ) === "string") {
        /* ... se usa el parámetro como nombre del tab */
    	_$_idTabLink = $('#'+x_idTabLink);
    }
    else if (jQuery.type( x_idTabLink ) === "object") {
        /* ... si el parámetro es un objeto, se usa el objeto de referencia al link de la pestaña en el tab */
    	_$_idTabLink = x_idTabLink;
    }
    else {
        /* ... en caso de no identificarse el tipo del dato se despliega el error */
        alert('Error identificando parametro código STV_JS_tDB001');
    }
    
	/* Por standard se sabe que el id del tab esta en el atributo data-idtab del <li> que contiene el link */
	var _s_idTab = _$_idTabLink.attr('data-idtab');

	/* TODO verificar que no este en nuevo o modificar antes de cerrar tab */
	
	/* Quitar el objeto del link (pestaña del tab) */
	_$_idTabLink.remove();
	/* Quitar el objeto del tab */
	$("#" + _s_idTab).remove();
	
    return;
}


/* Agrega un tab con todo y su pestaña 
 * Se debe especificar un array de tres elementos aName:
 *  [0] es el nombre de la aplicación
 *  [1] es el nombre del controlador
 *  [2] es el nombre de la rutina 
 * sIdTabsList es el selector jQuery donde se agregan los tabs
 * sIdTabContent es el selector jQuery donde se agrega el contenido del tab
 * Regresa el objeto jQuery del link <a> para su uso posteror */
function stv_fwk_tabAdd(O_parent, x_nameTab, x_idTab, s_classTabsList, s_classTabsContent, x_config) {
	/* Definición de las variables locales utilizadas en la función */
	var _s_idTab, _s_nameTab, _s_idTabLink;
	
	if (jQuery.type( x_config ) === "object") {
	    var _x_config = x_config;
	}
	else 
	{
        var _x_config = {};	    
	}
	
	if (_x_config.hasOwnProperty("hasClose")) {
		var _b_hasClose = _x_config.hasClose;
	}
	else 
	{ 
	    var _b_hasClose = false;
	}

	if (_x_config.hasOwnProperty("addClass")) {
		var _s_addClass = _x_config.addClass;
	}
	else 
	{ 
	    var _s_addClass = "";
	}
		
    if (_x_config.hasOwnProperty("url_ifEmpty")) {
        var _s_url_ifEmpty = _x_config.url_ifEmpty;
    }
    else 
    { 
        var _s_url_ifEmpty = "";
    }
	
    /* Si el parámetro aPrefixUniqueName es un array... */
    if (jQuery.type( x_idTab ) === "array")
    {
        /* ...se hará un join con los diferentes datos del array */
        /* TODO Se hace la suposición que todos los datos del array son string */
        _s_idTab = x_idTab.join('_');
    }
    else if (jQuery.type( x_idTab ) === "string")
    {
        /* ... si es string, se hace uso del parámetro directamente */
        _s_idTab = x_idTab;
    }
    else
    {
        /* ... en caso de no identificarse el tipo del dato se despliega el error */
        alert('Error identificando parametro código STV_JS_tA002');
        _s_idTab = 'errorEnNombreLink';
    }	
    
    /* Creando el tab vacío */
    {
        var _html_div_tab = $('<div></div>', {
            id: _s_idTab,
        }).addClass("stv_tab tab-pane " + _s_addClass);
        
        /* En caso de existir la clase con "partOfMain" significa que es parte del tab principal 
         * y no require individual loading */
        if (_s_addClass.indexOf("partOfMain") >= 0) {
            /* No agregar loading ya que es parte del main */
        } else {
            /* Agregar loading */
            _html_div_tab.html(stv_fwk_createLoading());
        }
        
        _html_div_tab.data("stv_empty", true);
        _html_div_tab.data("stv_s_url_ifEmpty", _s_url_ifEmpty);
    }
    
	
	/* Definiendo los nombres */
	if (s_classTabsList)
	{
		/* Si el parámetro es de tipo string... */
		if (jQuery.type( x_nameTab ) === "string")
		{
			/* ... se usa el parámetro como nombre del tab */
			_s_nameTab = x_nameTab;
		}
		else if (jQuery.type( x_nameTab ) === "object")
		{
			/* ... si el parámetro es un objeto, se usa el objeto para generar el nombre del tab */
			/* TODO Se hace la suposición de que el objeto es de tipo HTML */
			_s_nameTab = x_nameTab.closest('ul').siblings('a').find('.nav-label').text() + ' : ' + x_nameTab.text();
		}
		else
		{
			/* ... en caso de no identificarse el tipo del dato se despliega el error */
			alert('Error identificando parametro código STV_JS_tA001');
			_s_nameTab = 'errorEnNombreTab';
		}

        _html_div_tab.attr("nombretab", _s_nameTab);

        _s_idTabLink = 'link' + _s_idTab;
	
    	if ($("#"+_s_idTabLink).length) {
    		var _html_a = $("#"+_s_idTabLink);
    	} else {
    	
    		/* Creando la pestaña */
    		{
    			/* Se crea el link de selección del tab que contendra el texto a mostrar en el tab */
    			var _html_a = $('<a></a>', {
    				id: _s_idTabLink,
    			    href: '#' + _s_idTab,
    			    text: _s_nameTab,
    			}).attr("data-toggle", "tab").addClass('stv_tabLink').on('shown.bs.tab', function(event){
    				var _$tab = $('#'+_s_idTab); 
    				$('#' + _$tab.data('stv_gIDTabResults')).find('.stv_grid').DataTable().columns.adjust();
    			    stv_fwk_tab_onSelect(O_parent, $('#' + _s_idTab));
    			});
    		
    			/* Solamente si tiene configurado el boton de close... */
    			var _html_a_close;
    			if (_b_hasClose)
    			{
    				/* ... se crea la X para cerrar el tab */
    				_html_a_close = $('<a></a>', {}).addClass("stv_tab_close").append(
    					$('<i></i>', {}).addClass("fa fa-times")
    				).click( stv_fwk_tabDeleteButtonClick );
    			} else { _html_a_close = null; };
    		
    			/* Se agregan ambos elementos a un elemento de tipo span */
    			var _html_span = $('<span></span>', {}).append(_html_a).append(_html_a_close);
    		
    			/* Se agrega el elemento de tipo span al elemento de tipo li */
    			var _html_li = $('<li></li>', {}).attr("data-idtab", _s_idTab).append( _html_span );
    		
    			/* Agregando la pestaña al componente que maneja las pestañas */
    			O_parent.find(s_classTabsList + ":first").append(_html_li);
    		}
    	}
    	
    	_html_div_tab.attr("data-idlink", _s_idTabLink);

	}
	else
    {
	    _html_a = _html_div_tab;
    }
    	
	/* Definiendo los nombres */
    if (s_classTabsContent)
	{
		
		/* Agregando el tab al componente que maneja los tabs */
		O_parent.find(s_classTabsContent + ":first").append(_html_div_tab);
	}
    else
    {
        O_parent.append(_html_div_tab);
        stv_fwk_tab_onSelect(O_parent, $('#' + _s_idTab));    
    }
	
	return (_html_a);
}

/* Función que regresa una lista con los elementos que conforman el url
 * oMenu es el objeto jQuery del elemento del menú que se presionó de clase "mainbtn" o "stv_btnCustomerApp"
 * sDftApplication es el valor por default, en caso de que el elemento oMenu no tenga dato "a"
 * sDftController es el valor por default, en caso de que el elemento oMenu no tenga dato "c"
 * sDftRoutine es el valor por default, en caso de que el elemento oMenu no tenga dato "r" */
function stv_fwk_getArrayFromMenuOption(O_menu, s_applicationDft, s_controllerDft, s_routineDft)
{
    var _a_name = [];
    if (O_menu.data('a')) {
    	_a_name.push( O_menu.data('a') );
    } else {
    	_a_name.push( s_applicationDft );
    }
    if (O_menu.data('c')) {
    	_a_name.push( O_menu.data('c') );
    } else {
    	_a_name.push( s_controllerDft );
    }
    if (O_menu.data('r')) {
    	_a_name.push( O_menu.data('r') );
    } else {
    	_a_name.push( s_routineDft );
    }
    return (_a_name);
}

/* Función que crea un tab y pone el contenido de la rutina en él 
 * oMenu es el objeto jQuery del elemento del menú que se presionó de clase "mainbtn" 
 * sDftApplication es el valor por default, en caso de que el elemento oMenu no tenga dato "a"
 * sDftController es el valor por default, en caso de que el elemento oMenu no tenga dato "c"
 * sDftRoutine es el valor por default, en caso de que el elemento oMenu no tenga dato "r" */
function stv_fwk_menuOptionClick(O_menu, s_applicationDft, s_controllerDft, s_routineDft) {

	var _a_name = stv_fwk_getArrayFromMenuOption(O_menu, s_applicationDft, s_controllerDft, s_routineDft);
	var _s_idTab = _a_name.join('_');
	
	/* Si existe el tab */
	if ($("#" + _s_idTab).length > 0)
	{
	    /* Mostrando el tab */
	    $("#link" + _s_idTab).tab('show');
	}
	else
	{
		stv_fwk_tabAdd($('body'), O_menu, _s_idTab, ".mainTabList", ".mainTabContent", {hasClose: true}).tab('show');
		
		/* Si el path name esta definido y no se esta partiendo de solamente el nombre del dominio, se elimina del link para evitar duplicación */
		//window.history.pushState({},"", window.location.origin); Lo comento para poder refrescar las página rapidamente
		
	    /* Haciendo llamada AJAX y actualizando el contenido del tab */
		var _D_menuData = O_menu.data()
		if (
		    (_D_menuData.manejoargumentos == 1) /* Si solamente utilizará argumentos por default con la empresa id */
		    && (_D_menuData.empresa_id)
		    )
	    {
	        _a_name.push('empresa_id',_D_menuData.empresa_id)
	    }
		else if (
		    (_D_menuData.manejoargumentos == 2) /* Si solamente utilizará argumentos definidos por argumentos */
		    && (_D_menuData.argumentos)
		    )
		{
		    _a_name.push(_D_menuData.argumentos)
		}
		else if (
            (_D_menuData.manejoargumentos == 3) /* Si utilizara la empresa y los argumentos */
            && (_D_menuData.empresa_id)
            && (_D_menuData.argumentos)
		    )
		{
		    _a_name.push('empresa_id',_D_menuData.empresa_id, _D_menuData.argumentos)
		}
		else
	    {
		    /* No tiene manejo de parámetros */
	    }
				
	    stv_fwk_ajax('/'+_a_name.join('/'), null, _s_idTab, null, { x_data: false, fn_returnAjax: false, fn_returnError: false });	    
	    
	}
}


/* Función que abre un tab para links a otras applicaciones
 */
function stv_fwk_appOptionClick($menu, s_applicationDft, s_controllerDft, s_routineDft) {

	var _a_name = stv_fwk_getArrayFromMenuOption($menu, s_applicationDft, s_controllerDft, s_routineDft);
	window.open('/'+_a_name.join('/')+"?cfg_cuenta_aplicacion_id=" + $menu.data('id'));
}


/**
 * Funcion llamada cuando se presiona algún boton de alguna forma 
 * @param e: objeto boton o opción que fue presionado
 */
function stv_fwk_button_press(e)
{
    var $button = $(e.currentTarget);
    var $tab = $button.closest( ".stv_tab" ); 
    var _s_action = $button.data('action');
    var _s_link = $tab.data('stv_gLinkFunction') + "/" + _s_action;
    var _s_idsForLink = "";
    var _s_send = $button.data('send');
    var _s_actualRow = "";
    
    var _stv_gIDTabMain = $tab.data('stv_gIDTabMain');
    var _stv_gIDTabButtonBar = $tab.data('stv_gIDTabButtonBar');
    var _stv_gIDTabResults = $tab.data('stv_gIDTabResults');
    var _stv_gIDTabDetails = $tab.data('stv_gIDTabDetails');
    var _stv_gIDTabModal = $tab.data('stv_gIDTabModal');

    var _stv_gEs_behavior = $tab.data('stv_s_currentBehavior');
    
    if ($tab.find("#"+_stv_gIDTabMain).is(':visible')) {
    	    _s_idsForLink = "/" + $("#"+_stv_gIDTabMain).find("input[name=stv_id]").val();
    	    _s_actualRow = _stv_gIDTabMain;
    } else if ($tab.find("#"+_stv_gIDTabResults).is(':visible')) {
    	    _s_idsForLink = "/" + stv_fwk_getSelectedIds($tab, _stv_gIDTabResults, true);
    	    _s_actualRow = _stv_gIDTabResults;
    } else {
    	    /* No se agrega nada al link */
    }
    
    var _s_jsBeforepressaction = $button.data('action-js-fnbeforepressaction');

    var _x_data = {};
    var _b_continue = true;
    if (_s_jsBeforepressaction) {
        var _fn_jsPress = eval(_s_jsBeforepressaction);
        _D_resultado = _fn_jsPress($button, $tab, _s_action, _s_send, _s_actualRow, _s_link, _s_idsForLink);
        _b_continue = _D_resultado.b_continuar
        if (_D_resultado.hasOwnProperty('D_vars')) {
            _x_data = {
                ..._x_data,
                ..._D_resultado.D_vars
            };
        } else {}
	} else {}
    
    if ($button.data('action-jsondata')) {
        // TODO Verificar si el merge al mismo objeto funciona
        _x_data = {
            ..._x_data,
            ...$button.data('action-jsondata')
        };
    } else {
        // No hacer nada
    }
    
    if (_b_continue) {
	    if ($button.data('action') == stv_gButtons.btn_find) {
	    		stv_fwk_ajax(_s_link + _s_idsForLink, $tab, _stv_gIDTabResults, _s_send, { x_data: _x_data, fn_returnAjax: false, fn_returnError: false });
	    } else if ($button.data('action') == stv_gButtons.btn_findall) {
	    		stv_fwk_ajax(_s_link + _s_idsForLink, $tab, _stv_gIDTabResults, _s_send, { x_data: _x_data, fn_returnAjax: false, fn_returnError: false });
	    } else if ($button.data('action') == stv_gButtons.btn_new) {
	    		stv_fwk_ajax(_s_link + _s_idsForLink, $tab, _stv_gIDTabMain, _s_send, { x_data: _x_data, fn_returnAjax: false, fn_returnError: false });
	    } else if ($button.data('action') == stv_gButtons.btn_save) {
	    		stv_fwk_ajax(_s_link + _s_idsForLink, $tab, _stv_gIDTabMain, _s_send, { x_data: _x_data, fn_returnAjax: false, fn_returnError: false });
	    } else if ($button.data('action') == stv_gButtons.btn_cancel) {
	        /* Si se define el comportamiento/behavior como plus_form... */
	        if (_stv_gEs_behavior == "plus_form")
	        {
	            /* ...se cierra el plus */
	            $tab.closest("tr.stv_row_plus").prev("tr.shown").find("td.stv_rowresults_plus").click()
	        }
	        else if (_stv_gEs_behavior == "tab_onlyform")
            {
	            stv_fwk_ajax(_s_link + _s_idsForLink, $tab, _stv_gIDTabMain, _s_send, { x_data: _x_data, fn_returnAjax: false, fn_returnError: false });
            }
	        else if (_stv_gEs_behavior == "modal")
            {
	            stv_fwk_modal_press_close(e);
            }
	        else
	        {
		    	if (
                    ($tab.data("stv_s_currentAction") == stv_gButtons.btn_save)
                    || ($tab.data("stv_s_previousAction") == stv_gButtons.btn_new)
                    ) {
		    		/* Se habilitan todos los botones para poder ejecutar la accuón de búsqueda */
		    		$("#"+_stv_gIDTabButtonBar).find(".stv_visible").disable(false);
		    		stv_fwk_repeatLastSearch($tab);	    		
		    	} else {
			    	stv_fwk_configureView($tab, $tab.data('stv_s_previousViewHtmlClass'), $tab.data('stv_L_previousRowsToShow'));
                    $tab.data("stv_s_currentViewHtmlClass", $tab.data("stv_s_previousViewHtmlClass"));
                    $tab.data("stv_L_currentRowsToShow", $tab.data("stv_L_previousRowsToShow"));
                    $tab.data("stv_s_currentAction", $tab.data("stv_s_previousAction"));
                    $("body").scrollTop($tab.offset().top);
		    	}
	        }
	    } else if ($button.data('action') == stv_gButtons.btn_edit) {
	    		stv_fwk_ajax(_s_link + _s_idsForLink, $tab, _stv_gIDTabMain, _s_send, { x_data: _x_data, fn_returnAjax: false, fn_returnError: false });
	    } else if ($button.data('action') == stv_gButtons.btn_view) {
	    		stv_fwk_ajax(_s_link + _s_idsForLink, $tab, _stv_gIDTabMain, _s_send, { x_data: _x_data, fn_returnAjax: false, fn_returnError: false });
	    } else if ($button.data('action') == stv_gButtons.btn_remove) {
	        swal({
	            title: "Estas seguro?",
	            text: "El registro será eliminado",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#DD6B55",
	            confirmButtonText: "Si, eliminalo!",
	            cancelButtonText: "No, dejame checarlo!",
	            closeOnConfirm: false,
	            closeOnCancel: false },
	        function (isConfirm) {
	            if (isConfirm) {
	                /* Si se preciona algún boton relacionado con flags en el registro, se manda información del registro a modificar y se actualiza la búsqueda de resultados */
	            	stv_fwk_ajax(_s_link + _s_idsForLink, $tab, _stv_gIDTabMain, _s_send, { x_data: _x_data, fn_returnAjax: false, fn_returnError: false });
	            } else {
	                swal("Cancelado", "El registro se mantiene", "error");
	            }
	        });         
	    } else if ($button.data('action') == stv_gButtons.btn_signature) {
	    	var _sTextCreatedOn, _sTextCreatedBy, _sTextUpdatedOn, _sTextUpdatedBy;
	    	/* Si estan los resultados visibles */
	    	if (_s_actualRow == _stv_gIDTabResults) {
	    		_sTextCreatedOn = stv_fwk_getSelectedRow_field($tab, 'creado_en', 'value');
	    		_sTextCreatedBy = stv_fwk_getSelectedRow_field($tab, 'creado_por', 'value');
	    		_sTextUpdatedOn = stv_fwk_getSelectedRow_field($tab, 'editado_en', 'value');
	    		_sTextUpdatedBy = stv_fwk_getSelectedRow_field($tab, 'editado_por', 'value');
			} else {
				var _$IdTabMain = $("#"+_stv_gIDTabMain);
	    		_sTextCreatedOn = _$IdTabMain.find("[name=" +  'creado_en_represent' + "]").val();
	    		_sTextCreatedBy = _$IdTabMain.find("[name=" +  'creado_por_represent' + "]").val();
	    		_sTextUpdatedOn = _$IdTabMain.find("[name=" +  'editado_en_represent' + "]").val();
	    		_sTextUpdatedBy = _$IdTabMain.find("[name=" +  'editado_por_represent' + "]").val();
			}
	    	swal({
	    		  title: "<i class='fa fa-user-secret fa-3' style='font-size: 4em;'></i> <br/>Huella",
	    		  text: "<div class='feed-element'>" +
	    			  	"<div class='row'><label class='col-sm-4' align='right'>Creado el:</label><div class='col-sm-8' align='left'>"+_sTextCreatedOn+"</div></div>" +
	    		  		"<div class='row'><label class='col-sm-4' align='right'>Creado por:</label><div class='col-sm-8' align='left'>"+_sTextCreatedBy+"</div></div>" +
	    		  		"<div class='row'><label class='col-sm-4' align='right'>Modificado el:</label><div class='col-sm-8' align='left'>"+_sTextUpdatedOn+"</div></div>" +
	    		  		"<div class='row'><label class='col-sm-4' align='right'>Modificado por:</label><div class='col-sm-8' align='left'>"+_sTextUpdatedBy+"</div></div>" +
	    		  		"</div>",
	    		  html: true
	    		});
	    } else if ( ($button.data('action') == stv_gButtons.btn_searchcancel)
	    			|| ($button.data('action') == stv_gButtons.btn_print_cancel) ) {
	    		stv_fwk_modal_press_close(e);
	    } else if ($button.data('action') == stv_gButtons.btn_searchdone) {
	    		stv_fwk_ajax(_s_link + _s_idsForLink, $tab, _s_actualRow, _s_send, { x_data: { ...{actualRow:_s_actualRow}, ..._x_data }, fn_returnAjax: false, fn_returnError: false });
	    		stv_fwk_modal_press_close(e);
	    } else {
		    	if ($button.hasClass("stv_btnFunc_modal")) {
                    var _$this = $(e.currentTarget);
                    var _$modal = _$this.closest(".stv_modal");
                    if (_$modal.length > 0) {
                        /* No puede existir un modal dentro de otro modal, por lo que se remplaza el actual */
                        _stv_gIDTabModal = _$modal.attr("id");
                        $tab = _$modal.closest( ".stv_tab" );
                    } else {
                        $('#' + _stv_gIDTabModal).find('.modal-content').html("");
                        $('#' + _stv_gIDTabModal).modal("show");
                    }
                    var _L_link = $button.data('action-modal-url').split("?")
                    _L_link[0] += "/" + _s_action
		    		if ($button.data('action-modal-field')) {
		    			_L_link[0] += stv_fwk_getSelectedRow_field($tab, $button.data('action-modal-field'), "value");
		    		} else {
		    			_L_link[0] += _s_idsForLink;    			
		    		}
                    if (_L_link.length > 1) {
                        _s_link =  _L_link.join("?");
                    } else {
                        _s_link =  _L_link[0];
                    }
		    		stv_fwk_ajax(
		    		    _s_link,
                        $tab,
                        _stv_gIDTabModal + "_content",
                        _s_send,
                        {
                            x_data: {
                                ...{actualRow:_s_actualRow},
                                ..._x_data
                                },
                            fn_returnAjax: false,
                            fn_returnError: false,
                            b_hideAllTabRows: false,
                            b_ignorarCamposBuscar: true
                            }
                        );
		    	} else if ($button.hasClass("stv_btnFunc_confirm")) {
		            swal({
		                title: $button.data('action-confirm-title'),
		                text: $button.data('action-confirm-text'),
		                type: $button.data('action-confirm-type'),
		                showCancelButton: true,
		                confirmButtonColor: "#DD6B55",
		                confirmButtonText: $button.data('action-confirm-btnconfirm'),
		                cancelButtonText: $button.data('action-confirm-btncancel'),
		                closeOnConfirm: true,
		                closeOnCancel: true },
		            function (isConfirm) {
		                if (isConfirm) {
		                    stv_fwk_ajax(_s_link + _s_idsForLink, $tab, _s_actualRow, _s_send, { x_data: { ...{actualRow:_s_actualRow}, ..._x_data }, fn_returnAjax: false, fn_returnError: false });
		                } else {
		                    // Nada
		                }
		            });		    	    
		    	} else {
                    
                    _s_actualizar = $button.data('actualizar');
                    if (_s_actualizar) {
                        _s_actualizarRow_id = $tab.data(_s_actualizar);
                        if (_s_actualizarRow_id) {
                            /* El ID existe */
                        } else {
                            _s_actualizarRow_id = _s_actualRow;
                        }
                    } else {
                        _s_actualizarRow_id = _s_actualRow;
                    }
                    
			    	stv_fwk_ajax(_s_link + _s_idsForLink, $tab, _s_actualizarRow_id, _s_send, { x_data: { ...{actualRow:_s_actualRow}, ..._x_data }, fn_returnAjax: false, fn_returnError: false });
		    	}
	    }
    } else { /* No ejecutar acción alguna */ }

    var _s_jsAfterpressaction = $button.data('action-js-fnafterpressaction');

    if (_s_jsAfterpressaction) {
    	    var _fn_jsPress = eval(_s_jsAfterpressaction);
    	    _b_continue = _fn_jsPress($button, $tab, _s_action, _s_send, _s_actualRow, _s_link, _s_idsForLink, _b_continue);
	} else {}

    return;
}

/**
 * Normaliza la manera de enviar la información al servidor
 * @param $Input: objeto jquery al input
 * @param s_value: valor por default en caso de error
 * @returns valor normalizado
 */
function stv_fwk_formControlNormalize($Input, s_value)
{
    var _L_classForNumber = [
                        'stv_money',
                        'stv_percentage',
                        'stv_integer',
                        'stv_float',
                    ];
    var _s_valueReturn = $Input.val();
    try
    {
        if ($Input.attr('type') === "checkbox")
  	    {
  	    	/* Borrar las commas */
        	_s_valueReturn = $Input.prop('checked') ? $Input.val() : '';
     	}
        else
        {
        	for (var _nIndexClass in _L_classForNumber) 
        	{
        		if ($Input.hasClass(_L_classForNumber[_nIndexClass]))
        		{
        			/* Borrar las commas */
        			_s_valueReturn = $Input.val().replace(/\,/g,'');
        			break;
        		}
        		else {}
        	}
        }
    }
    catch (err)
    {
    	_s_valueReturn = s_value;
    }
    return _s_valueReturn;
}

function stv_fwk_downloadFile(filePath){
    var link=document.createElement('a');
    link.href = filePath;
    link.download = filePath.substr(filePath.lastIndexOf('/') + 1);
    link.click();
}

/**
 * Llamada ajax genérica
 * @param s_url: url a llamar, cuya respuesta se almacenará en s_idTarget
 * @param $tab: especifica el objeto ajax del tab
 * @param s_idTarget: el id del objeto donde se actualizará la información
 * @param s_type: tipo de llamada ajax
 * 		"form": utiliza $tab para encontrar el form en rowContent y busca todos los editores, tambien busca en rowContentDetail
 * 				la clase stv_form_partOfMain para el envío. Adicionalmente agrega el contenido en x_data al envío
 * 		"formWithFind": lo mismo que "form" agregando el contenido en buttonsBarFind
 * 		"find": sólo manda lo que se especifique en buttonsBarFind y en x_data
 * 		"modal": sólo manda lo que se especifique en buttonsBarFind y en x_data
 * 		"none": sólo ejecuta la llamada ajax
 * @param x_config: es un objeto con las siguientes propiedades
 * 			x_data: si es string, especifica el id del elemento a partir del cual se va a usar con metodo post el envio;
 * 					si es objeto, se manda el nombre de la propiedad y su valor; else usar false
 * 
 * 			fn_returnAjax: funcion que se llamará despues del regreso ajax, si se especifica como false, se hace procedimiento standar
 * 			fn_returnError: funcion que se llamará despues de un error en ajax, si se especifica como false, se hace procedimiento standar
 * 			fn_returnComplete: funcion que se llamará despues de un regreso o error en ajax, si se especifica como false, se hace procedimiento standar
 * 			b_hideAllTabRows: Oculta todos los tabRows al hacer la llamada AJAX, default true
 *          b_openNewWindowPDF: Indica que abrirá una nueva ventana si tiene texto definido, el texto se usará para el título y el tipo de dato va a ser pdf
 *          b_downloadPDF: Indica que creará un archivo PDF con el contenido que regrese la llamada ajax y creará el proceso para bajarlo
 *          b_dontSaveAction: Indica si la acción debe ser guardada para utilizarla con el cancel 
 * 			x_params: Parametros adicionales a mandar a las funcioner fn_return*
 */
function stv_fwk_ajax(s_url, $tab, s_idTarget, s_type, x_config)
{
	var _O_formData = new FormData(); /* Objeto que contendra la información a mandar al servidor */
    var _x_contentType = false; /* Content type debe ser false para poder enviar FormData al servidor */
    var _b_processData = false; /* Process Data debe ser false para no procesar los datos cuando es de tipo FormData */
    
    var _x_data = false;
    var _fn_returnAjax = false;
    var _fn_returnError = false;
    var _fn_returnComplete = false;
    var _b_hideAllTabRows = true;
    var _b_openNewWindowPDF = "";
    var _b_downloadPDF = false;
    var _b_dontSaveAction = false;
    var _b_ignorarCamposBuscar = false;
    
    if ((x_config) && (jQuery.type( x_config ) === "object")) {

        if (x_config.hasOwnProperty("x_data")) {
        	_x_data = x_config.x_data;
        } else {}

        if (x_config.hasOwnProperty("fn_returnAjax")) {
        	_fn_returnAjax = x_config.fn_returnAjax;
        } else {}
        
        if (x_config.hasOwnProperty("fn_returnError")) {
        	_fn_returnError = x_config.fn_returnError;
        } else {}

        if (x_config.hasOwnProperty("fn_returnComplete")) {
            _fn_returnComplete = x_config.fn_returnComplete;
        } else {}

        if (x_config.hasOwnProperty("b_hideAllTabRows")) {
        	_b_hideAllTabRows = x_config.b_hideAllTabRows;
        } else {}

        if (x_config.hasOwnProperty("b_openNewWindowPDF")) {
            _b_openNewWindowPDF = x_config.b_openNewWindowPDF;
        } else {}

        if (x_config.hasOwnProperty("b_downloadPDF")) {
            _b_downloadPDF = x_config.b_downloadPDF;
        } else {}

        if (x_config.hasOwnProperty("b_dontSaveAction")) {
            _b_dontSaveAction = x_config.b_dontSaveAction;
        } else {}

        if (x_config.hasOwnProperty("b_ignorarCamposBuscar")) {
            _b_ignorarCamposBuscar = x_config.b_ignorarCamposBuscar;
        } else {}

    } else {}
    
    function addFormData(A_inputs) {
    	$.each(A_inputs, function(n_key,O_input) {
        	var _$input = $(O_input);
        	if (_$input.hasClass("upload")) {
        		if (O_input.files.length > 0) {
                	_O_formData.append(O_input.name, O_input.files[0], O_input.value);
                } else {
                	_O_formData.append(O_input.name, $(O_input).attr("value"));
                }
        	} else {
        	    if (O_input.name) {
        	        _O_formData.append(O_input.name, stv_fwk_formControlNormalize(_$input, O_input.value));
        	    } else {}
        	}
        });
    }
    
    if (_x_data) {
        /* Si define datos, se importan para mandarse como datos tipo post */
    	if(jQuery.type( _x_data ) === "string") {
    	    /* Si los se especifico en forma de string, se buscan los inputs */
            var _A_inputsToCheck = $("#"+_x_data).find(".stv_form-control, input:hidden, textarea");
            addFormData(_A_inputsToCheck);
		} else if (jQuery.type( _x_data ) === "object") {
		    
		    if (_x_data.hasOwnProperty('prevObject')) {
		        /* Quiere decir que el objecto es DOM de jquery */
	            var _A_inputsToCheck = _x_data.find(".stv_form-control, input:hidden, textarea");
	            addFormData(_A_inputsToCheck);
		    }
		    else
	        {
                /* Si los se especifico en forma de objecto, se agregan como si fuera un diccionario */
        		for (var _s_property in _x_data) {
        		    if (_x_data.hasOwnProperty(_s_property)) {
        		        if (
    		                (jQuery.type( _x_data[_s_property] ) === "object")
    		                || (jQuery.type( _x_data[_s_property] ) === "array")
    		                )
    		            {
        		            _O_formData.append(_s_property, JSON.stringify(_x_data[_s_property]));
    		            }
        		        else
    		            {
        		            _O_formData.append(_s_property, _x_data[_s_property]);
    		            }
        		    } else {}
        		}
	        }
    	} else { /* ...si no es válido, ignora obtener cualquier dato */ }
	} else { /* No agregar datos */ }
    
    if ($tab) {
	    var _stv_gIDTabMain = $tab.data('stv_gIDTabMain');
	    var _stv_gIDTabButtonBar = $tab.data('stv_gIDTabButtonBar');
	    var _stv_gIDTabDetails = $tab.data('stv_gIDTabDetails');
	    var _stv_gIDTabModal = $tab.data('stv_gIDTabModal');

        let _O_resBusq_datatable = $tab.find('.stv_tab_content:first > .stv_rowSearchResults').find('.dataTables_scrollBody > .stv_grid').DataTable();
        let _n_paginaRows = 0;
        let _s_ordenColumnas = [];
        if (!_b_ignorarCamposBuscar && _O_resBusq_datatable) {
            _n_paginaRows = _O_resBusq_datatable.page.len();
            if (_n_paginaRows) {
                $tab.data('stv_paginarows', _n_paginaRows);
            } else if ($tab.data('stv_paginarows')) {
                _n_paginaRows = $tab.data('stv_paginarows');
            } else {
                _n_paginaRows = 10;
            }

            _s_ordenColumnas = JSON.stringify(_O_resBusq_datatable.order());
            if (_s_ordenColumnas) {
                $tab.data('stv_ordencolumnas', _s_ordenColumnas);
            } else if ($tab.data('stv_ordencolumnas')) {
                _s_ordenColumnas = $tab.data('stv_ordencolumnas');
            } else {
                _s_ordenColumnas = JSON.stringify([]);
            }

        } else {}

	    if ((s_type == "form") || (s_type == "formWithFind"))
		{
    	    var _A_inputsToCheck = $("#"+_stv_gIDTabMain).find(".stv_form-control, input:hidden, textarea");
    	    addFormData(_A_inputsToCheck);
	        var _A_subTabsToCheck = $("#"+_stv_gIDTabDetails).find(".stv_form_partOfMain");
	        $.each(_A_subTabsToCheck, function(n_key, O_subTab) {
	            var _$subTab = $(O_subTab);
	        	    var _A_inputsToCheck = _$subTab.find(".stv_form-control, input:hidden");
	        	    addFormData(_A_inputsToCheck);
	        });
	        
	        if (s_type == "formWithFind") {
	        	var _A_inputsToCheck = $("#"+_stv_gIDTabButtonBar).find(".stv_buttonsBarFind > .stv_form-control, input:hidden");
	        	addFormData(_A_inputsToCheck);
	            _A_inputsToCheck = $("#"+_stv_gIDTabButtonBar).find(".stv_rowContent_formAdvSearch").find(".stv_form-control, input:hidden, textarea");
	            addFormData(_A_inputsToCheck);
	    	} else {}
		} else if (s_type == "find") {
            var _A_inputsToCheck = $("#"+_stv_gIDTabButtonBar).find(".stv_buttonsBarFind > .stv_form-control, input:hidden");
            addFormData(_A_inputsToCheck);
            _A_inputsToCheck = $("#"+_stv_gIDTabButtonBar).find(".stv_rowContent_formAdvSearch").find(".stv_form-control, input:hidden, textarea");
            addFormData(_A_inputsToCheck);
            _O_formData.append('stv_paginarows', _n_paginaRows);
            _O_formData.append('stv_ordencolumnas', _s_ordenColumnas);
		} else if (s_type == "modal") {
			var _A_inputsToCheck = $("#"+_stv_gIDTabModal).find(".stv_form-control, input:hidden, textarea");
	    	addFormData(_A_inputsToCheck);
		} else {}

        /* Vistas seguras, donde no se esta operando un registro o forma */
        let _L_vistasSinOperacion = [
            'stv_en_emptyform',
            'stv_en_selectedrow',
            'stv_en_selectedrows'
            ]
        /* Vistas donde se esta operando sobre un registro */
        let _L_vistasConOperacion = [
            'stv_en_viewregister',
            'stv_en_editregister',
            'stv_en_newregister'
            ]

	    if (
            $tab.data("stv_s_currentViewHtmlClass")
	        && (!($.inArray($tab.data("stv_s_currentViewHtmlClass"), _L_vistasConOperacion) >= 0))
	        && (!_b_dontSaveAction)
	        ) {
	    	    $tab.data("stv_s_previousAction", $tab.data("stv_s_currentAction"));
	    	    $tab.data("stv_s_previousViewHtmlClass", $tab.data("stv_s_currentViewHtmlClass"));
	    	    $tab.data("stv_s_previousRowIdToShow", $tab.data("stv_s_rowIdToShow"));
		} else {}
	    
	    if (($tab.data("stv_s_currentAction") == stv_gButtons.btn_findall)
	    	    || ($tab.data("stv_s_currentAction") == stv_gButtons.btn_find)
	    	    || ($tab.data("stv_s_currentAction") == stv_gButtons.btn_view)) {
	    	    $tab.data("stv_s_previousSearchAction", $tab.data("stv_s_currentAction"));
	    } else {}
	    
	    // Agregar el id del tab a los datos a enviar
	    if ($tab.hasClass('stv_tabModal')) {
	    	    var _s_id = $tab.attr('id')
	    	    if (_s_id) {
	    	        _O_formData.append('stv_tabid', _s_id);
	    	    } else {}
		} else {}
	    
		if (_b_hideAllTabRows) {
		    /* Si se ocultan todos los Rows, se agrega la vista del loading a través de la clase stv_msg_loading */
			stv_fwk_configureView($tab, 'stv_msg_loading', ['ninguno']);
		} else {}
    } else {
    	/* Sólo hacer llamada ajax */
    }
    
    var _D_cfgApp_inputs = STV_FWK2_LIB.APP.fn_get_inputs();
    $.each(_D_cfgApp_inputs, function (s_index, s_valor) {
            _O_formData.append(s_index, s_valor);
        });
    
    /* Haz la llamada ajax */
    $.ajax({
        type: "POST",
		url: s_url,
		data: _O_formData,
		cache: false,
		contentType: _x_contentType,
		processData: _b_processData,
		success: function(x_msg) {
			var _b_continue = true;
			if (_fn_returnAjax) {
				_b_continue = _fn_returnAjax(s_url, $tab, s_idTarget, s_type, x_config, x_msg);
			} else {
				_b_continue = true;
			} 
			if (_b_continue) {
				if (s_idTarget) {
					/* Si target es válido... */
					/* ...y si su contenido es :eval... */
					if(s_idTarget == ':eval') {
						/* ...evalua el mensaje como script. */
						eval(x_msg);
					/* TODO en ves de usar el idTarget, se puede usar un selector a partir de $tab */
					} else if(jQuery.type(s_idTarget) === "string") {
						/* ...de lo conrario, si es string, sustituye lo que se regresa en el elemento con id = target. */
					    if ($tab) {
					        _$destination = $tab.find("#" + s_idTarget);
					        if (_$destination.length == 0) {
                               _$destination = $("#" + s_idTarget); 
					        } else {
					        }
					    } else {
					        _$destination = $("#" + s_idTarget); 
					    }
					    _$destination.html(x_msg);
					} else {
						/* ...de lo contrario, utiliza target como función y de parametro mandar lo que regresa la llamada ajax */
						s_idTarget(x_msg);
				    }
				} else if (_b_openNewWindowPDF) {
					var _file = new Blob(convertToByteArray(x_msg), {type: "application/pdf"});
					var _win = window.open(URL.createObjectURL(_file), "PDF");
					stv_fwk_configureView($tab, false, false);
				} else if (_b_downloadPDF) {
                    var _file = new Blob([x_msg], {type: "application/pdf"});
                    stv_fwk_downloadFile(URL.createObjectURL(_file));
                    stv_fwk_configureView($tab, false, false);				    
				} else {
					stv_fwk_flashMessage({
						type : 'error',
						title : 'JS AJAX Error',
						msg : 'No se tiene identificado el idTarget en la última llamada AJAX',
						mode : 'stv_flash'
					});
				}
			} else { /* No continuar, la acción ajax terminó */ };
		},
		error: function(x_ajaxresponse, textStatus) {
			if (_fn_returnError) {
				_fn_returnError(s_url, $tab, s_idTarget, s_type, x_config, x_ajaxresponse, textStatus);
			} else {
				stv_fwk_flashMessage({
					type : 'error',
					title : 'JS AJAX Error',
					msg : 'La última llamada AJAX no terminó satisfactoriamente',
					mode : 'stv_flash'
				});
				if ($tab) {
					stv_fwk_configureView($tab, false, false);
				} else {
					/* No se puede configurar la vista, ya que $tab no esta definido */
				}
			}
		},
		complete: function (x_ajaxresponse, textStatus) {
		    if ($tab) {
		        /* Si existe un tab, se oculta cualquier tabRow.loading activo en el tab */
		        $tab.find(".stv_msg_loading.stv_tabRow").hide();
		    } else {}
		    
			if (_fn_returnComplete) {
				_fn_returnComplete(s_url, $tab, s_idTarget, s_type, x_config, x_ajaxresponse, textStatus);
			} else {}
		}
    });
}

function convertToByteArray(input) {
  var sliceSize = 512;
  var bytes = [];

  for (var offset = 0; offset < input.length; offset += sliceSize) {
    var slice = input.slice(offset, offset + sliceSize);

    var byteNumbers = new Array(slice.length);

    for (var i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }

    const byteArray = new Uint8Array(byteNumbers);

    bytes.push(byteArray);
  }

  return bytes;
}

function stv_fwk_resultsGrid_onClickRow($row, $tab, x_IDTabResults) {
    
	if (jQuery.type( x_IDTabResults ) === "string") {
		var _$IDTabResults = $('#'+x_IDTabResults);
	} else {
		var _$IDTabResults = x_IDTabResults;
	}
	stv_fwk_configureView($tab, false, false);

}

/**
 * Regresa el contenido de algún campo del registro seleccionado
 * @param $tab
 * @param s_fieldName
 * @param s_attr
 * @returns {String}
 */
function stv_fwk_getSelectedRow_field($tab, s_fieldName, s_attr)
{    
	var _s_result = '';
    /* Si el array de field ids no tiene elementos... */
    if (!s_fieldName) {
        /* ...hay un error */
        /* TODO: Encapsular error solo para tiempo de desarrollo, en producción manejar otro tipo de error y grabarlo en log */
        alert('Campo no definido');
    } else {
	    var _stv_gIDTabResults = $tab.data('stv_gIDTabResults');
        if ($tab.data("stv_E_rowSearchResults_type") == STV_FWK2_LIB.APP.fn_definicion("stv_E_ROWSEARCHRESULTS_TYPE_GRID")) {
            _s_selector = 'tbody:first>tr.selected';
            
        } else if ($tab.data("stv_E_rowSearchResults_type") == STV_FWK2_LIB.APP.fn_definicion("stv_E_ROWSEARCHRESULTS_TYPE_TREE")) {
            _s_selector = 'div.stv_tree .stv_selected';
            
        } else {
            _s_selector = 'tbody:first>tr.stv_selected';
            
        } 
        
        var _$dbRowsSelected = $("#"+_stv_gIDTabResults).find(_s_selector);
        //if (_$dbRowsSelected.lenght > 1) {
        //	alert('Más de un registro seleccionado, no puede retornar un valor en el campo');
    	    //} else if (_$dbRowsSelected.length == 1) {
    		var _O_cellSelected = $(_$dbRowsSelected[0]).find("[field='" + s_fieldName + "']");
    		if (_O_cellSelected.data(s_attr)) {
    			_s_result = _O_cellSelected.data(s_attr);
    		} else {
    			_s_result = _O_cellSelected.attr(s_attr);
    		}
    	//} else {
    		/* Manetener valor por default ya que no se tiene registro seleccionado */
    	//}
    }
    return _s_result;
}


/**
 * Regresa un array con los registros seleccionados de la tabla de resultados.
 * @param $tab
 * @param D_params
 * @returns Objeto DOM con los attributos
 */
function stv_fwk_obtenerCampo_enRowSeleccionado($tab, s_nombreCampo, D_params) {
    var _O_tablaResultados = $tab.find('.stv_tab_content:first > .stv_rowSearchResults').find('.stv_grid').DataTable();
    /* TODO checar si existe mas de un row seleccionado y no regresar nada, checar si la condicion field esta en mas de una columna y no regresar nada */
    var _$_celda = $(_O_tablaResultados.cell(_O_tablaResultados.rows(".selected"), "[field$='"+s_nombreCampo+"']").node());
    return _$_celda;
}

function stv_fwk_repeatLastSearch($tab)
{
    var _stv_gEs_behavior = $tab.data('stv_s_currentBehavior');

    /* Si se define el comportamiento/behavior como plus_form... */
    if (_stv_gEs_behavior == "plus_form")
    {
        /* Se obtiene el registro que se va a actualizar */
        var $row_dataToUpdate = $tab.closest("tr.stv_row_plus").prev("tr.shown");
        /* Se actualiza el tab con al que corresponde la lista */
        var $tab = $row_dataToUpdate.closest("div.stv_tab");
        
        /* El link va a incluir findall pero solo va a buscar el id requerido */
        var _s_link = $tab.data('stv_gLinkFunction') + "/" + stv_gButtons.btn_findall;
        var _a_link = _s_link.split("/");
        _a_link[3] += ".json";
        _s_link = _a_link.join("/");
        
        var _s_id = stv_fwk_getRowIds($tab, $row_dataToUpdate);
        
        stv_fwk_ajax(_s_link + "/" + _s_id, $tab, false, "none", { x_data: false, fn_returnAjax: stv_fwk_updateRowContent, fn_returnError: false, b_hideAllTabRows:false });
        
        /* Se cierra el plus */
        $row_dataToUpdate.find("td.stv_rowresults_plus").click();
    }
    /* ...se obtiene la ultima información de búsqueda y se realiza nuevamente para actualizar la información */
    else
    {
    	var s_actionSearch = $tab.data("stv_s_previousSearchAction");
    	if (s_actionSearch == stv_gButtons.btn_findall)
    	{
    		$tab.find("[data-action="+stv_gButtons.btn_findall+"]").click();		
    	}
        else
        {
            $tab.find(".stv_buttonsBarFind").find("button.btn-primary, button.btn-info").first().click();
        }
    }
}

function stv_fwk_reloadMaster($tab)
{
    var _$tabMaster = $tab.parent().closest(".stv_tab");
    if (_$tabMaster)
    {
        var _$gButtonBar = $('#' + _$tabMaster.data('stv_gIDTabButtonBar'));
        var _s_action = _$tabMaster.data("stv_s_currentAction");
        var _$btnAction = _$gButtonBar.find('button[data-action="'+_s_action+'"]');
        var _$btnActionCancel = _$gButtonBar.find('button[data-action="'+stv_gButtons.btn_cancel+'"]');
        if (_$btnAction)
        {
            $(_$btnActionCancel).click();            
            $(_$btnAction).click();
        }
        else
        {
            alert("Action not found");
        }
    }
    else
    {
        
    }
        
}

function stv_fwk_modal_press_close(e) {
	var _$this = $(e.currentTarget);
	var _$modal = _$this.closest(".stv_modal");
	if (_$modal.length > 0) {
		_$modal.modal("hide");
	} else {}
}


function stv_fwk_sessionControl(s_urlSessionVerification, s_urlSessionLogin, s_application, s_lastUpdate) {
	this.b_renewSessionTime = false;  
    this.s_urlSessionVerification = s_urlSessionVerification;
    this.s_urlSessionLogin = s_urlSessionLogin;
    this.n_cyclicVerificationTime_s = 60*5;
    this.s_application = s_application;
    this.s_lastUpdate = s_lastUpdate;
    this.fn_sessionVerification_success = function(s_url, $tab, s_idTarget, s_type, x_config, x_msg) {
        /* Si esta deslogeado... */
        if (x_msg.s_status == 'unsigned') {
            /* ...llama el contenido del lockscreen. */
            stv_fwk_ajax(
                    this.s_urlSessionLogin, 
                    false, 
                    "stv_lockscreen", 
                    "none", 
                    {
                        x_data: {s_application: this.s_application}
                    }
                    );
        } else {
            /* ...de lo contrario, checa si hay actualización requerida del sistema. */
        	if ( ($.isNumeric(this.s_lastUpdate)) && ($.isNumeric(x_msg.n_lastUpdate)) ) {
        		var _n_lastUpdatePage = parseInt(this.s_lastUpdate);
        		var _n_lastUpdateServer = parseInt(x_msg.n_lastUpdate);
        		if (_n_lastUpdateServer > _n_lastUpdatePage) {
        			$(".stv_navbar_message").html("Se detectó actualización en el sistema, favor de refrescar pantalla.");
        		} else {
        			/* Nada */
        		}
        	} else {
        		/* No hay control de aviso de actualización del framework */
        	}
        }
    };
    this.fn_sessionVerification_complete = function(s_url, $tab, s_idTarget, s_type, x_config, x_ajaxresponse, textStatus) {
        /* De cualquier modo, si fue o no satisfactoria la llamada, ejecutar la función 5 segundos después */
    	if (x_ajaxresponse.status == 200) /* Response OK */ {
    	    var _n_delayFor = x_ajaxresponse.responseJSON.n_delayfor_s;
    	    if (($.isNumeric(_n_delayFor)) && (_n_delayFor > 1)) {
    	        if (_n_delayFor < this.n_cyclicVerificationTime_s) {
    	        	_n_delayFor = _n_delayFor-1;
	    	    } else {
	    	    	_n_delayFor = this.n_cyclicVerificationTime_s;
	    	    }
    	    } else {
    	    	_n_delayFor = 1;
    	    }
    	    setTimeout(this.fn_sessionVerification.bind(this), _n_delayFor * 1000);
    	} else { /* Error en la llamada AJAX */ }
    };
    this.fn_sessionVerification = function () {
        /* Si el lockscreen esta visible... */
        if ($("#stv_lockscreen").hasClass("stv_active"))
        {
            /* ...solamente llama la función cada 5 segundos. */
            setTimeout(this.fn_sessionVerification.bind(this), 5000);
        }
        else
        {
            /* ...de lo contrario, haz una llamada ajax para determinar el estado de la sesion. */
            stv_fwk_ajax(
            	this.s_urlSessionVerification, 
        		false, 
        		"stv_lockscreen", 
        		"none", 
        		{
        			x_data: {b_renewSessionTime: this.b_renewSessionTime, s_application: this.s_application, s_timezoneoffset: new Date($.now()).getTimezoneOffset()}, 
        			fn_returnAjax: this.fn_sessionVerification_success.bind(this), 
        			fn_returnComplete: this.fn_sessionVerification_complete.bind(this),
        		});
            this.b_renewSessionTime = false;
        }
    };
}



function stv_fwk_formGetFieldValue($tab, $input) {
    if ($input.data('value')) {
        _s_value = $input.data('value');
    } else {
        _s_value = $input.val();
    }
    return (_s_value);
}

function stv_fwk_formGetInput_byFieldName($parent, s_tablename, s_fieldname) {
	var _$input = $($parent.find("[name="+s_fieldname+"]"));
    if (_$input.length == 0) {
    	_$input = $($parent.find("[field='"+s_tablename+"."+s_fieldname+"']"));
    } else {}
    return (_$input);
}

function stv_fwk2_setValue_byFieldName($parent, s_tablename, s_fieldname, s_newValue) {
    var _$input = $($parent.find("[name="+s_fieldname+"]"));
    if (_$input.length == 0) {
        _$input = $($parent.find("[field='"+s_tablename+"."+s_fieldname+"']"));
    } else {}
    /* TODO dependiendo del tipo de campo, es como debe actualizar su contenido así como formatearlo */
    if (_$input) {
        _$input.val(s_newValue);
    } else {}
    return (_$input);
}

function fwk2_grid_interpretarValor(data, type, row) {
    let x_valor;
    if (type === 'export') {
        if (data.indexOf("data-value") >= 0) {
            x_valor = $(data).data("value");
            if (x_valor === 'None') {
                x_valor = "";
            } else {}
        } else {
            x_valor = data;
        }

    } else if (type === 'display') {
        x_valor = data;

    } else if (type === 'filter') {
        x_valor = data;

    } else {
        x_valor = data;
    }
    return x_valor;
}

