# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
#########################################################################

# Siempre redireccionar a la aplicación acceso, ya que esta aplicación es solamente para manejor genérico del framework
redirect(URL(a = 'acceso', c = 'default', f = 'user', args = ['login'], vars = {}))
