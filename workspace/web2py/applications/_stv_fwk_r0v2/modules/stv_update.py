# -*- coding: utf-8 -*-

''' Librerías genéricas para manejo de copia de archivos o importación de archivos
'''

import glob
import os
import shutil
import filecmp
import stat
try:
    from gluon.shell import execfile
except ImportError:
    pass


def genericAppFileUpdate(sAppSrc, sNameDir, sWildchar, lsAppDst, lsFilesToUpdate):
    ''' Función genérica que copia archivos de una aplicación a otra
    :param str sAppSrc: string que define la aplicación origen
    :param str sNameDir: nombre del subdirectorio dentro de la aplicación donde se hará la búsqueda
    :param str sWildchar: caracteres especiales que filtran los resultados de los archivos a comprar, por ejemplo *, [cd]*, etc.
    :param list_of_string lsAppDst: lista que define las aplicaciones destino
    :param list_of_string lsFilesToUpdate: lista con los nombres de los archivos a actualizar
    '''
    
    # Buscamos cualquier archivo que comience con el wildchar en la aplicacion sAppSrc y el subdirectorio sNameDir.
    _sPathSrc = os.path.join('applications', sAppSrc, sNameDir, sWildchar)

    # Para cada nombre de archivo...
    for _sFileSrc in glob.iglob(_sPathSrc):
        # ...si el nombre del archivo esta en la lista de archivos a actualizar...
        if os.path.basename(_sFileSrc) in lsFilesToUpdate:
            # ...para cada aplicación destino en la lista de aplicaciones destino...
            for _sAppDst in lsAppDst:
                _sFileDst = os.path.join('applications', _sAppDst, sNameDir, os.path.basename(_sFileSrc))
                # ...si en archivo destino no existe o tiene diferencias con el archivo fuente...
                if (not os.path.exists(_sFileDst)) or (not filecmp.cmp(_sFileSrc, _sFileDst)):
                    # ...si el archivo existe...
                    if os.path.exists(_sFileDst):
                        # ...borralo.
                        os.remove(_sFileDst)
                    # ...copia el archivo fuente al archivo destino.
                    shutil.copy(_sFileSrc, _sFileDst)
                    # Obten los parametros actuales del archivo.
                    _mode = os.stat(_sFileDst)[stat.ST_MODE]
                    # Configurale el permiso de solo escritura.
                    os.chmod(_sFileDst, _mode & ~stat.S_IWUSR & ~stat.S_IWGRP & ~stat.S_IWOTH)
                    # Imprime en el log el archivo actualizado
                    #print "Updating "+_sAppDst+"/"+sNameDir+": "+_sFileDst


        
def genericAppImport(sAppSrc, sNameDir, lsFilesToImport, globalsReference):
    ''' Función genérica que importa archivos de una aplicación a otra
    :param str sAppSrc: string que define la aplicación origen
    :param str sNameDir: nombre del subdirectorio dentro de la aplicación donde se hará la búsqueda
    :parsm list_of_string lsFilesToImport: lista con los archivos a importar
    '''

    _sPathSrc = os.path.join('applications', sAppSrc, sNameDir)
    
    # Para cada nombre de archivo en la lista...
    for _sFileSrc in lsFilesToImport:
        _sFilePathSrc = os.path.join(_sPathSrc, _sFileSrc)
        # ...si el archivo no existe...
        if (not os.path.exists(_sFilePathSrc)):
            # ...muestra error en la consola.
            #print "Error importando: " + _sFilePathSrc
            pass
        else:
            # ...executa el archivo
            execfile(_sFilePathSrc, globalsReference)
        