# -*- coding: utf-8 -*-

from applications._stv_fwk_r0v2.modules.cfg030_Class import *
import gluon.contrib.simplejson as simplejson


#from gluon.dal import DAL, Field
#from gluon.sqlhtml import SQLFORM
#from gluon.storage import Storage
#from gluon.validators import IS_NOT_EMPTY, IS_EMAIL, IS_LENGTH

''' Widgets para editores '''

# Parece que no se usan los dias y meses
# gListDays = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado']
# gListDaysMin = [ele[:2] for ele in gListDays]
# gListDaysShort = [ele[:3] for ele in gListDays]
#
# gListMonths = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
# gListMonthsShort = [ele[:3] for ele in gListMonths]


class STV_WIDGETS_GENERALES:

    @staticmethod
    def GENERAR_ID(dbCampo, D_atributos):
        if hasattr(dbCampo, '_tablename'):
            _sId = "%s_%s" % (dbCampo._tablename, dbCampo.name)
        else:
            _sId = "%s" % dbCampo.name

        if D_atributos:
            if 'multiple' in D_atributos:
                _sId += "_%s" % D_atributos['multiple']
            else:
                pass
        else:
            pass

        return _sId

    @staticmethod
    def GENERAR_OPCIONES(dbCampo, x_valor, D_atributos):
        _s_leyendaNada = "Nada"
        _s_valorNada = ""
        if D_atributos:
            if 'leyendaNada' in D_atributos:
                _s_leyendaNada = D_atributos['leyendaNada']
                del D_atributos['leyendaNada']
            else:
                pass

            if 'valorNada' in D_atributos:
                _s_valorNada = D_atributos['valorNada']
                del D_atributos['valorNada']
            else:
                pass
        else:
            pass

        _L_opciones = []
        if dbCampo.requires:
            _optionNada = OPTION(_value = _s_valorNada)
            _optionNada.components.extend(_s_leyendaNada)

            if isinstance(dbCampo.requires, IS_EMPTY_OR):
                _L_opciones.append(_optionNada)
                _ref_require = dbCampo.requires.other
            else:
                if (dbCampo.notnull == False) and (dbCampo.required == False):
                    _L_opciones.append(_optionNada)
                else:
                    pass
                _ref_require = dbCampo.requires

            if _ref_require.labels:
                _L_etiquetas = _ref_require.labels
                _L_valores = _ref_require.theset
            else:
                _L_etiquetas = _ref_require.theset
                _L_valores = _ref_require.theset

            for _n_index, _s_etiqueta in enumerate(_L_etiquetas):
                _option = OPTION(
                    _value = _L_valores[_n_index] if (len(_L_valores) > _n_index) else _s_etiqueta,
                    )
                if str(_L_valores[_n_index]) == str(x_valor):
                    _option['_selected'] = 'selected'
                else:
                    pass
                _option.components.extend(_s_etiqueta)
                _L_opciones.append(_option)
        else:
            _L_opciones.append(
                "Sin opciones"
                )
        return _L_opciones

    @staticmethod
    def GENERAR_OPCIONES_DB(dbCampo, x_valor, D_atributos):

        _s_vacio_leyenda = D_atributos.pop('s_vacio_leyenda') if 's_vacio_leyenda' in D_atributos else "Nada"
        _s_vacio_valor = D_atributos.pop('_s_vacio_valor') if '_s_vacio_valor' in D_atributos else ""
        _n_recomendado_id = D_atributos.pop('n_recomendado_id') \
            if 'n_recomendado_id' in D_atributos else "Nada"
        _L_addFieldsInfo = D_atributos.pop('L_addFieldsInfo') \
            if 'L_addFieldsInfo' in D_atributos else []

        _L_opciones = []
        if dbCampo.requires:
            _optionNada = OPTION(_value = _s_vacio_valor)
            _optionNada.components.extend(_s_vacio_leyenda)

            if isinstance(dbCampo.requires, IS_EMPTY_OR):
                _L_opciones.append(_optionNada)
                _ref_require = dbCampo.requires.other
            else:
                if not dbCampo.notnull and not dbCampo.required:
                    _L_opciones.append(_optionNada)
                else:
                    pass
                _ref_require = dbCampo.requires

            if not _ref_require:
                _optionError = OPTION(_value = _s_valorNada)
                _optionError.components.extend("No se encontró referenecia")
                _L_opciones.append(_optionError)

            else:
                _ref_require.build_set()

                # En caso de estar configurados agrega información adicional a la opción
                _dbRowsInfoAdicional = None
                if _L_addFieldsInfo:
                    _dbTablaRef = _ref_require.dbset.db[_ref_require.ktable]
                    _L_addFieldsInfoTodosNecesarios = []
                    # Se obtienen la lista de todos los campos requeridos en la relación
                    for _s_field in _ref_require.fieldnames:
                        _L_addFieldsInfoTodosNecesarios.append(_dbTablaRef[_s_field])
                    # Se crea la consulta para obtener la información adicional.
                    #  Es importante hacer notar que la condición incluye el query de la consulta
                    #  en el mismo orden que lo hace el el build_set
                    _dbRowsInfoAdicional = _ref_require.dbset.db(
                        _ref_require.dbset.query
                        & (_dbTablaRef[_ref_require.kfield] != None)
                        ).select(
                            *(_L_addFieldsInfoTodosNecesarios + _L_addFieldsInfo),
                            orderby = _L_addFieldsInfoTodosNecesarios
                            )
                else:
                    pass

                # Se procede a crear las opciones
                for _n_index, _s_label in enumerate(_ref_require.labels):
                    _option = OPTION(
                        _value = _ref_require.theset[_n_index] if len(_ref_require.theset) > _n_index else _s_label,
                        )

                    if _dbRowsInfoAdicional:
                        for _dbField_info in _L_addFieldsInfo:
                            _option['_field_' + _dbField_info.name] = str(_dbRowsInfoAdicional[_n_index][_dbField_info])
                    else:
                        pass

                    if str(_ref_require.theset[_n_index]) == str(x_valor):
                        _option['_selected'] = 'selected'
                    else:
                        pass

                    if _n_recomendado_id \
                            and (str(_n_recomendado_id) == str(_ref_require.theset[_n_index])):
                        _s_label += "*"
                    else:
                        pass

                    _option.components.extend(_s_label)
                    _L_opciones.append(_option)

        else:
            _L_opciones.append(
                "Sin opciones"
                )

        return _L_opciones

    pass


def stv_safe_cast(val, to_type, default=None):
    try:
        return to_type(val)
    except ValueError:
        return default


def stv_obtener_referencias(dbField):
    """ Regresa la relación a la base de datos, la tabla, y el nombre del campo
    """
    
    D_return = Storage(
        db = None,
        dbT = None,
        s_nombrecampo = None
        )

    if dbField.referent:
        D_return.db = dbField.db
        D_return.dbT = dbField.referent._table
        D_return.s_nombrecampo = dbField.referent.name

    else:
        # Siempre la primer condición debe tener un IS_IN_DB
        _dbField_requires = dbField.requires if not isinstance(dbField.requires, list) \
            else dbField.requires[0]

        _dbField_requires = _dbField_requires.other if isinstance(_dbField_requires, IS_EMPTY_OR) \
            else _dbField_requires

        if isinstance(_dbField_requires, IS_IN_DB):
            D_return.db = _dbField_requires.dbset.db
            D_return.dbT = D_return.db[_dbField_requires.ktable]
            D_return.s_nombrecampo = _dbField_requires.kfield

        else:
            pass
        
    return D_return
    
    
def stv_widget_input_common(
        field, 
        value, 
        sInputType, 
        bAddTooltip, 
        L_addOnAfter = None, 
        sAddOnBefore = None, 
        sAddClass = None, 
        sPlaceHolder = None, 
        D_additionalAttributes = dict()
        ):
    """ Widget usado para mostrar los inputs con un boton de tip en caso de definir comentarios en los campos

    @param field: referencia al campo de la tabla
    @type field:
    @param value: valor del campo
    @type value:
    @param sInputType: tipo del elemento INPUT
    @type sInputType:
    @param bAddTooltip: especifica si agrega o no el boton de tooltip al input, solo en caso de que el campo
     tenga comentarios
    @type bAddTooltip:
    @param L_addOnAfter: texto que se deplegará en el editor al final, como %, $ o None en caso de no requerirse
    @type L_addOnAfter:
    @param sAddOnBefore:
    @type sAddOnBefore:
    @param sAddClass: clase o clases adicionales a agregar al input
    @type sAddClass:
    @param sPlaceHolder:
    @type sPlaceHolder:
    @param D_additionalAttributes:
    @type D_additionalAttributes:
    @return:
    @rtype:
    """
    _b_unaLinea = False

    if 'multiple' in D_additionalAttributes:
        _sId = "%s_%s_%s" % (field._tablename, field.name, str(D_additionalAttributes['multiple']))
        del D_additionalAttributes['multiple']
    else:
        if hasattr(field, '_tablename'):
            _sId = "%s_%s" % (field._tablename, field.name)
        else:
            _sId = "%s" % field.name
    
    if 'stv_name' in D_additionalAttributes: 
        _s_name = D_additionalAttributes['stv_name']
        del D_additionalAttributes['stv_name']
    else:
        _s_name = field.name

    if 'b_unaLinea' in D_additionalAttributes:
        _b_unaLinea = D_additionalAttributes['b_unaLinea']
        del D_additionalAttributes['b_unaLinea']
    else:
        pass

    _input = INPUT(
        _name = _s_name,
        _id = _sId,
        _value = str(value) if value else "",
        _type = sInputType,
        _placeholder = sPlaceHolder if sPlaceHolder else None,
        requires = field.requires,
        )
    _lClasses = [str(field.type), (sAddClass or ""), (" stv_fieldRequired " if field.required else "")]
    if sInputType == 'checkbox':
        if value:
            if 'checked' in D_additionalAttributes:
                if D_additionalAttributes['checked']:
                    _input['_checked'] = 'checked'
                else:
                    pass
            else:
                _input['_checked'] = 'checked'
    else:
        _lClasses += ["form-control"]
    _lClasses += ["stv_form-control"]
    if field.length:
        _input['_maxlength'] = field.length
    for _ele in D_additionalAttributes:
        _input[_ele] = D_additionalAttributes[_ele]
    _input['_class'] = " ".join(_lClasses)
    
    _spanAddOnBefore = TAG.DIV(
        sAddOnBefore, 
        _class = "input-group-addon"
        ) if sAddOnBefore else None
    _spanAddOnAfter = TAG.DIV(
        *L_addOnAfter, 
        _class = "input-group-addon"
        ) if L_addOnAfter else None
    if bAddTooltip and field.comment:
        _spanHint = TAG.DIV(
            _class = "input-group-btn"
            )
        _button = TAG.BUTTON(
            _type = "button",
            _class = "btn btn-primary stv_hint stv_hintsmall",
            _title = field.comment,
            _hint = field.comment,
            )
        _button['_data-toggle'] = "tooltip"
        _button['_data-placement'] = "bottom"
        _i = I(
            _class = "fa fa-question"
            )
        _button.components.extend([_i])
        _spanHint.components.extend([_button])
    else:
        _spanHint = TAG.DIV(
            _class = "input-group-btn"
            )

    if _b_unaLinea:

        return _input
    else:
        _divGroup = TAG.DIV(
            _class = "input-group",
            )
        if _spanAddOnBefore or _spanAddOnAfter:
            _divGroup.components.extend([_spanAddOnBefore]) if _spanAddOnBefore else ''
            _divGroup.components.extend([_input])
            _divGroup.components.extend([_spanAddOnAfter]) if _spanAddOnAfter else ''
            _divGroup.components.extend([_spanHint]) if _spanHint else ''

            return _divGroup
        else:
            _divGroup.components.extend([_input])
            _divGroup.components.extend([_spanHint]) if _spanHint else ''

            return _divGroup


def stv_widget_input_link(dbField, x_value, sLink, D_additionalAttributes = dict()):
    return stv_widget_input_common(field = dbField, 
                                    value = x_value, 
                                    sInputType = 'text', 
                                    bAddTooltip = True, 
                                    L_addOnAfter = [A(I(_class = "fa fa-external-link"), _href=sLink, _target="_blank")],
                                    sAddClass = None,
                                    sPlaceHolder = None, 
                                    D_additionalAttributes = D_additionalAttributes)

def stv_widget_input(field, value, D_additionalAttributes = dict()):
    return stv_widget_input_common(field = field, 
                                    value = value, 
                                    sInputType = 'text', 
                                    bAddTooltip = True, 
                                    sAddClass = None,
                                    sPlaceHolder = None, 
                                    D_additionalAttributes = D_additionalAttributes)


def stv_widget_inputFile(
        dbField,
        x_value,
        s_url = None,
        x_linkName = None,
        D_additionalAttributes = dict()
        ):
    """ Represent usado para la representación de campos tipo upload

    @param dbField:
    @type dbField:
    @param x_value: valor del campo
    @type x_value:
    @param s_url: url usado para bajar el archivo
    @type s_url:
    @param x_linkName: link que define el texto del link, si es un campo usara el valor de ese campo en odbRow
    @type x_linkName:
    @param D_additionalAttributes:
    @type D_additionalAttributes:
    @return:
    @rtype:
    """

    if x_value:

        if 'notExternalLink' in D_additionalAttributes: 
            _b_notExternalLink = D_additionalAttributes['notExternalLink']
            del D_additionalAttributes['notExternalLink']
        else:
            _b_notExternalLink = False

        if 'notDownload' in D_additionalAttributes: 
            _b_notDownload = D_additionalAttributes['notDownload']
            del D_additionalAttributes['notDownload']
        else:
            _b_notDownload = False

        if 'notRemove' in D_additionalAttributes: 
            _b_notRemove = D_additionalAttributes['notRemove']
            del D_additionalAttributes['notRemove']
        else:
            _b_notRemove = False
                    
        _sFileName = "file"
        _odbRowRef = current.request.stv_fwk_recordForWidgets if hasattr(current.request, 'stv_fwk_recordForWidgets') else None
        if isinstance(x_linkName, str):
            _sLinkName = x_linkName
        elif _odbRowRef and isinstance(x_linkName, Field):
            if (x_linkName.name in _odbRowRef):
                _sLinkName = _odbRowRef[x_linkName.name]
                _sFileName = _sLinkName
            elif (x_linkName.tablename in _odbRowRef):
                if (x_linkName.name in _odbRowRef[x_linkName.tablename]):
                    _sLinkName = _odbRowRef[x_linkName.tablename][x_linkName.name]
                    _sFileName = _sLinkName
                else:
                    _sLinkName = str(x_linkName)
            else:
                _sLinkName = str(x_linkName)
        else:
            _sLinkName = str(x_linkName)
        
        if s_url:
            _L_url = (s_url).split("?")
        else:
            _L_url = ["download"]
        
        if len(_L_url) == 1:
            _s_url = (_L_url[0] + '/' + x_value + '?filename=' + str(_sFileName))
        else:
            _s_url = (_L_url[0] + '/' + x_value + '?filename=' + str(_sFileName) + '&' + _L_url[1])          
        
        _L_addOnAfterLinks = []
        if not _b_notExternalLink:
            _L_addOnAfterLinks += [A(I(_class = "fa fa-external-link"), _href=_s_url + "&display=yes", _target="_blank")]
        if not _b_notDownload: 
            _L_addOnAfterLinks += [A(I(_class = "fa fa-download"), _href=_s_url, _target="_blank")]
        if not _b_notRemove:
            _L_addOnAfterLinks += [A(I(_class = "fa fa-times"), _href="#", _class="stv_remove_file")] 
            
        return stv_widget_input_common(
            field = dbField,
            value = x_value,
            sInputType = 'file',
            bAddTooltip = True,
            L_addOnAfter = _L_addOnAfterLinks,
            sAddOnBefore = None,
            sAddClass = "stv_file_uploaded",
            sPlaceHolder = None,
            D_additionalAttributes = D_additionalAttributes
            )
    else:
        return stv_widget_input_common(
            field = dbField,
            value = x_value,
            sInputType = 'file',
            bAddTooltip = True,
            sAddClass = None,
            sPlaceHolder = None,
            D_additionalAttributes = D_additionalAttributes
            )


def stv_widget_inputFile_directUpload(dbField, x_value, s_uploadUrl = None, D_additionalAttributes = dict()):
    """ Represent usado para la representación de campos tipo upload
    :param x_value: valor del campo
    :param odbRow: registro de la tabla
    :param s_uploadUrl: url usado para subir el archivo al ser seleccionado
    :param D_additionalAttributes: Atributos adicionales
    """
    
    _D_additionalAttributes = D_additionalAttributes
    _D_additionalAttributes['_data-uploadurl'] = s_uploadUrl
    
    return stv_widget_input_common(field = dbField, 
                                value = x_value, 
                                sInputType = 'file', 
                                bAddTooltip = True, 
                                sAddClass = "stv_directupload",
                                sPlaceHolder = None, 
                                D_additionalAttributes = _D_additionalAttributes) 

def stv_widget_inputEmail(field, value, D_additionalAttributes = dict()):
    return stv_widget_input_common(field = field, 
                                    value = value, 
                                    sInputType = 'email', 
                                    bAddTooltip = True, 
                                    sAddClass = None,
                                    sPlaceHolder = None, 
                                    D_additionalAttributes = D_additionalAttributes)

def stv_widget_inputPassword(field, value, D_additionalAttributes = dict()):
    return stv_widget_input_common(field = field, 
                                    value = value, 
                                    sInputType = 'text', 
                                    bAddTooltip = True, 
                                    sAddClass = 'stv_password',
                                    sPlaceHolder = None, 
                                    D_additionalAttributes = D_additionalAttributes)
 
def stv_widget_inputInteger(field, value, D_additionalAttributes = dict()):
    _D_additionalAttributes = D_additionalAttributes
    _min = None
    _max = None
    if hasattr(field, 'requires') and field.requires:
        if ("IS_EMPTY_OR" in str(field.requires)):
            if isinstance(field.requires, list):
                _fieldrequires = field.requires[0].other
            else:
                _fieldrequires = [field.requires.other]
        else:
            _fieldrequires = field.requires
        if not isinstance(_fieldrequires, (list, tuple)):
            _lRequires = [_fieldrequires]
        elif isinstance(_fieldrequires, tuple):
            _lRrequires = list(_fieldrequires)
        else:
            _lRequires = []
        _lRequires.reverse()
        for _item in _lRequires:
            if ("IS_INT_IN_RANGE" in str(_item)):
                #html5 wants yyyy-mm-ddThh:mm:ssZ 
                #or yyyy-mm-ddThh:mm:ss-TimeZoneOffset
                if _item.minimum != None:
                    _min = int(_item.minimum)
                if _item.maximum != None:
                    _max = int(_item.maximum)
    _D_additionalAttributes['_data-inputmask-max'] = _max
    _D_additionalAttributes['_data-inputmask-min'] = _min
    _D_additionalAttributes['_data-inputmask-alias'] = 'numeric'
    _D_additionalAttributes['_data-inputmask-digits'] = '' # Para tener falso
    _D_additionalAttributes['_data-inputmask-placeholder'] = '0'
    _D_additionalAttributes['_data-inputmask-autoGroup'] = '' # Para tener falso
    
    return stv_widget_input_common(field = field, 
                                    value = value, 
                                    sInputType = 'text', 
                                    bAddTooltip = True, 
                                    L_addOnAfter = None,
                                    sAddClass = 'stv_integer',
                                    sPlaceHolder = None,
                                    D_additionalAttributes = _D_additionalAttributes)
    
def stv_widget_inputPercentage(field, value, D_additionalAttributes = dict()):
    
    fn_match = re.match(r"(?P<function>\w+)\s?\((?P<digits>\w+),(?P<decimal>\w+)\)", str(field.type))
    _D_additionalAttributes = D_additionalAttributes
    if (fn_match):
        _d_match = fn_match.groupdict()
        if (_d_match['function'] == 'decimal'):
            _min = None
            _max = None
            if hasattr(field, 'requires') and field.requires:
                if ("IS_EMPTY_OR" in str(field.requires)):
                    if isinstance(field.requires, list):
                        _fieldrequires = field.requires[0].other
                    else:
                        _fieldrequires = [field.requires.other]
                else:
                    _fieldrequires = field.requires
                if not isinstance(_fieldrequires, (list, tuple)):
                    _lRequires = [_fieldrequires]
                elif isinstance(_fieldrequires, tuple):
                    _lRrequires = list(_fieldrequires)
                else:
                    _lRequires = []
                _lRequires.reverse()
                for _item in _lRequires:
                    if ("IS_DECIMAL_IN_RANGE" in str(_item)):
                        #html5 wants yyyy-mm-ddThh:mm:ssZ 
                        #or yyyy-mm-ddThh:mm:ss-TimeZoneOffset
                        if _item.minimum != None:
                            _min = int(_item.minimum)
                        if _item.maximum != None:
                            _max = int(_item.maximum)
            _D_additionalAttributes['_data-inputmask-max'] = _max
            _D_additionalAttributes['_data-inputmask-min'] = _min
            _D_additionalAttributes['_data-inputmask-alias'] = 'numeric'
            _D_additionalAttributes['_data-inputmask-digits'] = int(_d_match.get('decimal', 2))
            _D_additionalAttributes['_data-inputmask-radixPoint'] = '.'
            _D_additionalAttributes['_data-inputmask-placeholder'] = '0'
            _D_additionalAttributes['_data-inputmask-autoGroup'] = '' # Para tener falso
            
    return stv_widget_input_common(field = field, 
                                    value = value, 
                                    sInputType = 'text', 
                                    bAddTooltip = True, 
                                    L_addOnAfter = ['%'],
                                    sAddClass = 'stv_percentage',
                                    sPlaceHolder = None,
                                    D_additionalAttributes = _D_additionalAttributes)

def stv_widget_inputMoney(field, value, D_additionalAttributes = dict()):
    fn_match = re.match(r"(?P<function>\w+)\s?\((?P<digits>\w+),(?P<decimal>\w+)\)", str(field.type))
    _D_additionalAttributes = D_additionalAttributes
    if (fn_match):
        _d_match = fn_match.groupdict()
        if (_d_match['function'] == 'decimal'):
            _min = None
            _max = None
            if hasattr(field, 'requires') and field.requires:
                if ("IS_EMPTY_OR" in str(field.requires)):
                    if isinstance(field.requires, list):
                        _fieldrequires = field.requires[0].other
                    else:
                        _fieldrequires = [field.requires.other]
                else:
                    _fieldrequires = field.requires
                if not isinstance(_fieldrequires, (list, tuple)):
                    _lRequires = [_fieldrequires]
                elif isinstance(_fieldrequires, tuple):
                    _lRrequires = list(_fieldrequires)
                else:
                    _lRequires = []
                _lRequires.reverse()
                for _item in _lRequires:
                    if ("IS_DECIMAL_IN_RANGE" in str(_item)):
                        #html5 wants yyyy-mm-ddThh:mm:ssZ 
                        #or yyyy-mm-ddThh:mm:ss-TimeZoneOffset
                        if _item.minimum != None:
                            _min = int(_item.minimum)
                        if _item.maximum != None:
                            _max = int(_item.maximum)
            _D_additionalAttributes['_data-inputmask-max'] = _max
            _D_additionalAttributes['_data-inputmask-min'] = _min
            _D_additionalAttributes['_data-inputmask-alias'] = 'numeric'
            _D_additionalAttributes['_data-inputmask-digits'] = int(_d_match.get('decimal', 2))
            _D_additionalAttributes['_data-inputmask-digitsOptional'] = '' # Para tener falso
            _D_additionalAttributes['_data-inputmask-groupSeparator'] = ','
            _D_additionalAttributes['_data-inputmask-placeholder'] = '0'
            _D_additionalAttributes['_data-inputmask-autoGroup'] = '1'

    return stv_widget_input_common(
        field = field,
        value = value,
        sInputType = 'text',
        bAddTooltip = True,
        sAddOnBefore = '$',  # TODO agregar el addOnBefore si es que esta definido
        sAddClass = 'stv_money',
        sPlaceHolder = None,
        D_additionalAttributes = _D_additionalAttributes
        )


def stv_widget_inputMoneyRange(field, value, D_additionalAttributes = dict()):
    if 's_from' in D_additionalAttributes:
        _s_from = D_additionalAttributes['s_from']
        del D_additionalAttributes['s_from']
    else:
        _s_from = "De:"

    if 's_to' in D_additionalAttributes:
        _s_to = D_additionalAttributes['s_to']
        del D_additionalAttributes['s_to']
    else:
        _s_to = " A:"

    if value:
        _x_valores = value
    elif hasattr(field, "adv_default"):
        _x_valores = field.adv_default
    else:
        _x_valores = None

    if isinstance(_x_valores, list):
        _value1 = _x_valores[0]
        _value2 = _x_valores[1]
    else:
        _value1 = _x_valores
        _value2 = _x_valores


    _divGroup = TAG.DIV(
        _class = "",
        )

    _divGroup_1 = TAG.DIV(
        _class = "stv_widget_inputmoneyrange1",
        )
    D_additionalAttributes.update(sAddOnBefore = _s_from)
    _divGroup_1.components.extend(
        [
            stv_widget_inputMoney(
                field, _value1, D_additionalAttributes
                )
            ]
        )

    _divGroup_2 = TAG.DIV(
        _class = "stv_widget_inputmoneyrange2",
        )
    D_additionalAttributes.update(sAddOnBefore = _s_to)
    _divGroup_2.components.extend(
        [
            stv_widget_inputMoney(
                field, _value2, D_additionalAttributes
                )
            ]
        )

    _divGroup.components.extend(
        [
            _divGroup_1,
            _divGroup_2
            ]
        )

    return _divGroup


def stv_widget_inputFloat(field, value, D_additionalAttributes = dict()):
    fn_match = re.match(r"(?P<function>\w+)\s?\((?P<digits>\w+),(?P<decimal>\w+)\)", str(field.type))
    _D_additionalAttributes = D_additionalAttributes
    if (fn_match):
        _d_match = fn_match.groupdict()
        if (_d_match['function'] == 'decimal'):
            _min = None
            _max = None
            if hasattr(field, 'requires') and field.requires:
                if ("IS_EMPTY_OR" in str(field.requires)):
                    if isinstance(field.requires, list):
                        _fieldrequires = field.requires[0].other
                    else:
                        _fieldrequires = [field.requires.other]
                else:
                    _fieldrequires = field.requires
                if not isinstance(_fieldrequires, (list, tuple)):
                    _lRequires = [_fieldrequires]
                elif isinstance(_fieldrequires, tuple):
                    _lRrequires = list(_fieldrequires)
                else:
                    _lRequires = []
                _lRequires.reverse()
                for _item in _lRequires:
                    if ("IS_DECIMAL_IN_RANGE" in str(_item)):
                        #html5 wants yyyy-mm-ddThh:mm:ssZ 
                        #or yyyy-mm-ddThh:mm:ss-TimeZoneOffset
                        if _item.minimum != None:
                            _min = int(_item.minimum)
                        if _item.maximum != None:
                            _max = int(_item.maximum)
            _D_additionalAttributes['_data-inputmask-max'] = _max
            _D_additionalAttributes['_data-inputmask-min'] = _min
            _D_additionalAttributes['_data-inputmask-alias'] = 'numeric'
            _D_additionalAttributes['_data-inputmask-digits'] = int(_d_match.get('decimal', 2))
            _D_additionalAttributes['_data-inputmask-digitsOptional'] = '' # Para tener falso
            _D_additionalAttributes['_data-inputmask-groupSeparator'] = ','
            _D_additionalAttributes['_data-inputmask-placeholder'] = '0'
            _D_additionalAttributes['_data-inputmask-autoGroup'] = '1'
            
            
    return stv_widget_input_common(field = field, 
                                    value = value, 
                                    sInputType = 'text', 
                                    bAddTooltip = True, 
                                    sAddOnBefore = None,
                                    sAddClass = 'stv_float',
                                    sPlaceHolder = None,
                                    D_additionalAttributes = _D_additionalAttributes)


def stv_widget_inputTemperature(field, value, D_additionalAttributes = dict()):
    return stv_widget_input_common(field = field, 
                                    value = value, 
                                    sInputType = 'text', 
                                    bAddTooltip = True, 
                                    L_addOnAfter = ['°C'],
                                    sAddClass = None,
                                    sPlaceHolder = None, 
                                    D_additionalAttributes = D_additionalAttributes)
 
# http://bootstrap-datepicker.readthedocs.org/en/stable/
def stv_widget_inputTime(field, value, D_additionalAttributes = dict()):
    return stv_widget_input_common(field = field, 
                                    value = value, 
                                    sInputType = 'text', 
                                    bAddTooltip = True, 
                                    sAddClass = 'timepicker',
                                    sPlaceHolder = None, 
                                    D_additionalAttributes = D_additionalAttributes)


def stv_widget_inputDate(field, value, D_additionalAttributes = dict()):
    return stv_widget_input_common(field = field, 
                                    value = value, 
                                    sInputType = 'text', 
                                    bAddTooltip = True, 
                                    sAddClass = 'datepicker',
                                    sPlaceHolder = None, 
                                    D_additionalAttributes = D_additionalAttributes)


def stv_widget_inputDateRange(field, value, D_additionalAttributes = dict()):
    
    if 's_from' in D_additionalAttributes: 
        _s_from = D_additionalAttributes['s_from']
        del D_additionalAttributes['s_from']
    else:
        _s_from = "De:"

    if 's_to' in D_additionalAttributes: 
        _s_to = D_additionalAttributes['s_to']
        del D_additionalAttributes['s_to']
    else:
        _s_to = " A:"
    
    
    _divGroup = TAG.DIV(
        _class = "",
        )
    
    _divGroup_date1 = TAG.DIV(
        _class = "stv_widget_inputdaterange1",
        )

    if value:
        _x_valores = value
    elif hasattr(field, "adv_default"):
        _x_valores = field.adv_default
    else:
        _x_valores = None

    if isinstance(_x_valores, list):
        _value1 = _x_valores[0]
        _value2 = _x_valores[1]
    else:
        _value1 = _x_valores
        _value2 = _x_valores

    _divGroup_date1.components.extend(
        [
            stv_widget_input_common(
                field = field, 
                value = _value1,
                sInputType = 'text', 
                bAddTooltip = True, 
                sAddClass = 'datepicker',
                sPlaceHolder = None, 
                sAddOnBefore = _s_from,
                D_additionalAttributes = D_additionalAttributes
                )
            ]
        )
    
    _divGroup_date2 = TAG.DIV(
        _class = "stv_widget_inputdaterange2",
        )
    _divGroup_date2.components.extend(
        [
            stv_widget_input_common(
                field = field, 
                value = _value2,
                sInputType = 'text', 
                bAddTooltip = True, 
                sAddClass = 'datepicker',
                sPlaceHolder = None, 
                sAddOnBefore = _s_to,
                D_additionalAttributes = D_additionalAttributes
                )
            ]
        )

    _divGroup.components.extend(
        [
            _divGroup_date1,
            _divGroup_date2
            ]
        )

    return _divGroup
    
    
    return stv_widget_input_common(field = field, 
                                    value = value, 
                                    sInputType = 'text', 
                                    bAddTooltip = True, 
                                    sAddClass = 'datepicker',
                                    sPlaceHolder = None, 
                                    D_additionalAttributes = D_additionalAttributes)


def stv_widget_inputDateTimeRange(field, value, D_additionalAttributes = dict()):
    
    if 's_from' in D_additionalAttributes: 
        _s_from = D_additionalAttributes['s_from']
        del D_additionalAttributes['s_from']
    else:
        _s_from = "De:"

    if 's_to' in D_additionalAttributes: 
        _s_to = D_additionalAttributes['s_to']
        del D_additionalAttributes['s_to']
    else:
        _s_to = " A:"
    
    
    _divGroup = TAG.DIV(
        _class = "",
        )

    if value:
        _x_valores = value
    elif hasattr(field, "adv_default"):
        _x_valores = field.adv_default
    else:
        _x_valores = None

    if isinstance(_x_valores, list):
        _value1 = _x_valores[0]
        _value2 = _x_valores[1]
    else:
        _value1 = _x_valores
        _value2 = _x_valores

    _divGroup_date1 = TAG.DIV(
        _class = "stv_widget_inputdatetimerange1",
        )
    _divGroup_date1.components.extend(
        [
            stv_widget_input_common(
                field = field, 
                value = _value1,
                sInputType = 'text', 
                bAddTooltip = True, 
                sAddClass = 'datetimepicker',
                sPlaceHolder = None, 
                sAddOnBefore = _s_from,
                D_additionalAttributes = D_additionalAttributes
                )
            ]
        )
    
    _divGroup_date2 = TAG.DIV(
        _class = "stv_widget_inputdatetimerange2",
        )
    _divGroup_date2.components.extend(
        [
            stv_widget_input_common(
                field = field, 
                value = _value2,
                sInputType = 'text', 
                bAddTooltip = True, 
                sAddClass = 'datetimepicker',
                sPlaceHolder = None, 
                sAddOnBefore = _s_to,
                D_additionalAttributes = D_additionalAttributes
                )
            ]
        )

    _divGroup.components.extend(
        [
            _divGroup_date1,
            _divGroup_date2
            ]
        )

    return _divGroup
    
    
    return stv_widget_input_common(field = field, 
                                    value = value, 
                                    sInputType = 'text', 
                                    bAddTooltip = True, 
                                    sAddClass = 'datepicker',
                                    sPlaceHolder = None, 
                                    D_additionalAttributes = D_additionalAttributes)


def stv_widget_inputDateTime(field, value, D_additionalAttributes = dict()):
    return stv_widget_input_common(
        field = field,
        value = value,
        sInputType = 'text',
        bAddTooltip = True,
        sAddClass = 'datetimepicker',
        sPlaceHolder = None,
        D_additionalAttributes = D_additionalAttributes
        )


def stv_widget_inputCheckbox(
        field,
        value,
        D_additionalAttributes = None
        ):
    return stv_widget_input_common(
        field = field,
        value = value,
        sInputType = 'checkbox',
        bAddTooltip = True,
        sAddClass = 'stv_checkbox',
        sPlaceHolder = None,
        D_additionalAttributes = D_additionalAttributes or dict()
        )


def stv_widget_text(field, value, D_additionalAttributes = dict()):
    
    if ('multiple' in D_additionalAttributes):
        _sId = "%s_%s_%s" % (field._tablename, field.name, str(D_additionalAttributes['multiple']))
    else:
        _sId = "%s_%s" % (field._tablename, field.name)

    if 'n_cols' in D_additionalAttributes: 
        _n_cols = D_additionalAttributes['n_cols']
        del D_additionalAttributes['n_cols']
    else:
        _n_cols = 40

    if 'n_rows' in D_additionalAttributes: 
        _n_rows = D_additionalAttributes['n_rows']
        del D_additionalAttributes['n_rows']
    else:
        _n_rows = 10
    
    _input = TEXTAREA(
        _name = field.name,
        _id = _sId,
        _value = str(value) if value else "",
        value = str(value) if value else "",
        _cols = _n_cols,
        _rows = _n_rows,
        requires = field.requires,
        )
    _lClasses = [str(field.type), "form-control", (" stv_fieldRequired " if field.required else "")]
    _lClasses += ["stv_form-control"]
    for _ele in D_additionalAttributes:
        _input[_ele] = D_additionalAttributes[_ele]
    _input['_class'] = " ".join(_lClasses)
    
    if (field.comment):
        _spanHint = TAG.DIV(
            _class = "input-group-btn"
            )
        _button = TAG.BUTTON(
            _type = "button",
            _class = "btn btn-primary stv_hint stv_hintsmall",
            _title = field.comment,
            _hint = field.comment,
            )
        _button['_data-toggle'] = "tooltip"
        _button['_data-placement'] = "bottom"
        _i = I(
            _class = "fa fa-question"
            )
        _button.components.extend([_i])
        _spanHint.components.extend([_button])
    else:
        _spanHint = None

    _divGroup = TAG.DIV(
        _class = "input-group",
        )
    if _spanHint:
        _divGroup.components.extend([_input])
        _divGroup.components.extend([_spanHint]) if _spanHint else ''
        
        return _divGroup
    else:
        _divGroup.components.extend([_input])
        return _divGroup


def stv_widget_combobox(
        field,
        value,
        D_additionalAttributes = None
        ):
    """ Widget usado para mostrar un combo box al estilo de la página con un boton de tip en caso de
    definir comentarios en los campos
    """

    _s_id = STV_WIDGETS_GENERALES.GENERAR_ID(field, D_additionalAttributes)

    _select = SELECT(
        _name = field.name,
        _id = _s_id,
        _class = "form-control stv_form-control " + ("stv_fieldRequired " if field.required else ""),
        _value = value,
        )

    if D_additionalAttributes:
        for _ele in D_additionalAttributes:
            _select[_ele] = D_additionalAttributes[_ele]
    else:
        pass

    _options = STV_WIDGETS_GENERALES.GENERAR_OPCIONES(field, value, D_additionalAttributes)

    _select.components.extend(_options)
    
    _divGroup = TAG.DIV(
        _class = "input-group",
        )
    _span = TAG.DIV(
        _class = "input-group-btn"
        )

    if field.comment:
        _button = TAG.BUTTON(
            _type = "button",
            _class = "btn btn-primary stv_hint stv_hintsmall",
            _title = field.comment,
            _hint = field.comment,
            )
        _button['_data-toggle'] = "tooltip"
        _button['_data-placement'] = "bottom"
        _i = I(
            _class = "fa fa-question"
            )
        _button.components.extend([_i])
        _span.components.extend([_button])
        _divGroup.components.extend([_select, _span])
    else:
        _divGroup.components.extend([_select, _span])

    return _divGroup


' Widget usado para mostrar un combo box al estilo de la página con un boton de tip en caso de definir comentarios en los campos ' 
def stv_widget_db_combobox(field, value, D_additionalAttributes = dict()):
    
    if ('multiple' in D_additionalAttributes):
        _sId = "%s_%s_%s" % (field._tablename, field.name, D_additionalAttributes['multiple'])
    else:
        _sId = "%s_%s" % (field._tablename, field.name)

    _select = SELECT(
        _name = field.name,
        _id = _sId,
        _class = "form-control stv_form-control"  + (" stv_fieldRequired " if field.required else ""),
        _value = value,
    )
    for _ele in D_additionalAttributes:
        _select[_ele] = D_additionalAttributes[_ele]
    _options = []
    _refRequire = []
    if isinstance( field.requires, ( IS_EMPTY_OR ) ):
        _option = OPTION(
            _value = '',
        )
        _options.append(_option)
        field.requires.other.build_set()
        _refRequire = field.requires.other
    else:
        if field.notnull == False:
            _option = OPTION(
                _value = '',
            ) 
            _options.append(_option)
        try:
            field.requires.build_set()
            _refRequire = field.requires
        except:
            field.requires = None
            _refRequire = None
            
    if _refRequire:    
        for _index, _label in enumerate(_refRequire.labels):
            _option = OPTION(
                _value = _refRequire.theset[_index] if (len(_refRequire.theset) > _index) else _label,
            )
            if str(_refRequire.theset[_index]) == str(value):
                _option['_selected'] = 'selected'
            _option.components.extend(_label) 
            _options.append(_option)
    else:
        # No hay datos
        pass
    _select.components.extend(_options)
    
    if (field.comment):
        _divGroup = TAG.DIV(
            _class = "input-group",
            )
        _span = TAG.DIV(
            _class = "input-group-btn"
            )
        _button = TAG.BUTTON(
            _type = "button",
            _class = "btn btn-primary stv_hint stv_hintsmall",
            _title = field.comment,
            _hint = field.comment,
            )
        _button['_data-toggle'] = "tooltip"
        _button['_data-placement'] = "bottom"
        _i = I(
            _class = "fa fa-question"
            )
        _button.components.extend([_i])
        _span.components.extend([_button])
        _divGroup.components.extend([_select, _span])
        return _divGroup
    else:
        return _select


def stv_widget_chosen(field, value, maxselect, D_additionalAttributes = dict()):

    if 'multiple' in D_additionalAttributes:
        _sId = "%s_%s_%s" % (field._tablename, field.name, D_additionalAttributes['multiple'])
        del D_additionalAttributes['multiple']
    else:
        _sId = "%s_%s" % (field._tablename, field.name)
        
    if 'stv_name' in D_additionalAttributes: 
        _s_name = D_additionalAttributes['stv_name']
        del D_additionalAttributes['stv_name']
    else:
        _s_name = field.name    

    _select = SELECT(
        _name = _s_name,
        _id = _sId,
        _class = "form-control stv_form-control chosen-select" + (" stv_fieldRequired " if field.required else ""),
        _value = value,
    )
    for _ele in D_additionalAttributes:
        _select[_ele] = D_additionalAttributes[_ele]

    if maxselect > 1:
        _select['_multiple'] = 'multiple'
        _select['_data-placeholder'] = 'Selecciona opciones'
        _option = None
    else:
        _option = OPTION(
            _value = '',
        )
        _option.components.extend('Nada')

    _options = STV_WIDGETS_GENERALES.GENERAR_OPCIONES(field, value, D_additionalAttributes)

    _select.components.extend(_options)

    _divGroup = TAG.DIV(
        _class = "input-group stv_chosen_flex",
        )
    if field.comment:
        _span = TAG.DIV(
            _class = "input-group-btn"
            )
        _button = TAG.BUTTON(
            _type = "button",
            _class = "btn btn-primary stv_hint stv_hintsmall",
            _title = field.comment,
            _hint = field.comment,
            )
        _button['_data-toggle'] = "tooltip"
        _button['_data-placement'] = "bottom"
        _i = I(
            _class = "fa fa-question"
            )
        _button.components.extend([_i])
        _span.components.extend([_button])
        _divGroup.components.extend([_select, _span])
    else:
        _divGroup.components.extend([_select])

    return _divGroup


def stv_widget_db_chosen(field, value, maxselect = 1, D_additionalAttributes = None):
    """ Widget usado para mostrar un combo box al estilo chosen con info de DBs

    http://harvesthq.github.io/chosen/ con un boton de tip en caso de definir comentarios en los campos

    """
    D_additionalAttributes = D_additionalAttributes if D_additionalAttributes else Storage()

    _sId = ("%s_%s" % (field._tablename, field.name)) if hasattr(field, '_tablename') else ("%s" % field.name)
    _sId += ("%s" % D_additionalAttributes.pop('multiple')) if 'multiple' in D_additionalAttributes else ""

    _x_valor = value if value else field.adv_default if hasattr(field, "adv_default") else None

    _s_class_requerido = " stv_fieldRequired " if field.required or field.notnull else ""

    _select = SELECT(
        _name = field.name,
        _id = _sId,
        _class = "form-control stv_form-control chosen-select" + _s_class_requerido,
        _value = _x_valor,
        )

    for _ele in D_additionalAttributes:
        _select[_ele] = D_additionalAttributes[_ele]

    if maxselect > 1:
        _select['_multiple'] = 'multiple'
        _select['_data-placeholder'] = 'Selecciona opciones'
    else:
        pass

    _L_options = STV_WIDGETS_GENERALES.GENERAR_OPCIONES_DB(field, value, D_additionalAttributes)

    _select.components.extend(_L_options)

    _divGroup = TAG.DIV(_class = "input-group stv_chosen_flex")

    if field.comment:
        _span = TAG.DIV(_class = "input-group-btn")
        _button = TAG.BUTTON(
            _type = "button",
            _class = "btn btn-primary stv_hint stv_hintsmall",
            _title = field.comment,
            _hint = field.comment,
            )
        _button['_data-toggle'] = "tooltip"
        _button['_data-placement'] = "bottom"
        _i = I(_class = "fa fa-question")
        _button.components.extend([_i])
        _span.components.extend([_button])
        _divGroup.components.extend([_select, _span])
    else:
        _divGroup.components.extend([_select])

    return _divGroup

' Widget usado para mostrar un combo box al estilo chosen http://harvesthq.github.io/chosen/ con un boton de tip en caso de definir comentarios en los campos ' 
def stv_widget_db_chosen_tree(field, value, maxselect, D_additionalAttributes = dict()):
    
    if ('multiple' in D_additionalAttributes):
        _sId = "%s_%s_%s" % (field._tablename, field.name, D_additionalAttributes['multiple'])
    else:
        _sId = "%s_%s" % (field._tablename, field.name)

    if ('class_dbTree' in D_additionalAttributes):
        _class_dbTree = D_additionalAttributes['class_dbTree']
        del D_additionalAttributes['class_dbTree']
    else:
        _class_dbTree = None

    if ('dbRows_tree' in D_additionalAttributes):
        _dbRows_tree = D_additionalAttributes['dbRows_tree']
        del D_additionalAttributes['dbRows_tree']
    else:
        _dbRows_tree = None

    if ('dbTable_tree' in D_additionalAttributes):
        _dbTable_tree = D_additionalAttributes['dbTable_tree']
        del D_additionalAttributes['dbTable_tree']
    else:
        _dbTable_tree = None

    if ('dbRows_tree_filter' in D_additionalAttributes):
        _D_tree_filter = D_additionalAttributes['dbRows_tree_filter'].as_dict()
        del D_additionalAttributes['dbRows_tree_filter']
    else:
        _D_tree_filter = {}

    if ('dbField_order' in D_additionalAttributes):
        _dbField_order = D_additionalAttributes['dbField_order']
        del D_additionalAttributes['dbField_order']
    else:
        _dbField_order = None

    if ('dbField_tree_groupby' in D_additionalAttributes):
        _dbField_tree_groupby = D_additionalAttributes['dbField_tree_groupby']
        del D_additionalAttributes['dbField_tree_groupby']
    else:
        _dbField_tree_groupby = None

    if ('b_onlyLastNodes' in D_additionalAttributes):
        _b_onlyLastNodes = D_additionalAttributes['b_onlyLastNodes']
        del D_additionalAttributes['b_onlyLastNodes']
    else:
        _b_onlyLastNodes = None

    if ('b_includeId' in D_additionalAttributes):
        b_includeId = D_additionalAttributes['b_includeId']
        del D_additionalAttributes['b_includeId']
    else:
        b_includeId = None

    if ('L_addFieldsInfo' in D_additionalAttributes):
        _L_addFieldsInfo = D_additionalAttributes['L_addFieldsInfo']
        del D_additionalAttributes['L_addFieldsInfo']
    else:
        _L_addFieldsInfo = []

    if ('D_tree' in D_additionalAttributes):
        _D_tree = D_additionalAttributes['D_tree']
        del D_additionalAttributes['D_tree']
    else:
        if not field.requires:
            _refRequire = None
        elif isinstance(field.requires, (IS_EMPTY_OR)):
            _refRequire = field.requires.other
        elif isinstance(field.requires, (IS_IN_DB)):
            _refRequire = field.requires
        else:
            _refRequire = None

        if _refRequire.dbset:
            _dbRows = _refRequire.dbset.select(_dbTable_tree.ALL, orderby = _dbField_order)
        elif _dbTable_tree:
            _dbRows = _dbTable_tree._db(_dbTable_tree.id > 0).select(_dbTable_tree.ALL, orderby = _dbField_order)
        elif _dbRows_tree:
            _dbRows = _dbRows_tree
        else:
            _dbRows = None
        if _dbRows and _dbField_tree_groupby and _class_dbTree:
            _dbTree = _class_dbTree(_dbRows)
            _D_tree  = _dbTree.process_groupby(str(_dbField_tree_groupby))
        else:
            _D_tree = None
        
    if ('n_filterFromId' in D_additionalAttributes):
        _n_filterFromId = D_additionalAttributes['n_filterFromId']
        del D_additionalAttributes['n_filterFromId']
    else:
        _n_filterFromId = None
        

    _select = SELECT(
        _name = field.name,
        _id = _sId,
        _class = "form-control stv_form-control chosen-select"  + (" stv_fieldRequired " if field.required else ""),
        _value = value,
    )
    for _ele in D_additionalAttributes:
        _select[_ele] = D_additionalAttributes[_ele]

    if maxselect > 1:
        _select['_multiple'] = 'multiple'
        _select['_data-placeholder'] = ('Selecciona opciones')
    _options = []
    if isinstance( field.requires, ( IS_EMPTY_OR ) ):
        _option = OPTION(
            _value = '',
        )
        _option.components.extend(('Nada')) 
        _options.append(_option)
    else:
        if (field.notnull == False) or (field.required == False):
            _option = OPTION(
                _value = '',
            ) 
            _option.components.extend(('Nada')) 
            _options.append(_option)

    _L_namesParents = []
    _L_dbRow = [Storage(n_index = 0, L_dbRows = _D_tree.raiz)] # Se define lista que agrupara los rows que se van trabajando.
    _n_timeout = 10000
    # Se recorren todos los elementos del row.
    while _L_dbRow and (_n_timeout > 0):
        _L_rowActual = _L_dbRow.pop()
        # Si ya se termino la rama
        if (len(_L_rowActual.L_dbRows) <= _L_rowActual.n_index):
            if _L_namesParents:
                _L_namesParents.pop()
            pass
        else:
            _dbRowActual = _L_rowActual.L_dbRows[_L_rowActual.n_index]
            # Si esta definido el id desde el cual filtrar.
            if (
                    (_n_filterFromId)
                    and (str(_dbRowActual.id) == str(_n_filterFromId))
                    ):
                if _L_namesParents:
                    _L_namesParents.pop()
                _L_rowActual.n_index += 1
                _L_dbRow.append(_L_rowActual)                
            else:
                _L_namesParents.append(_dbRowActual.nombrecorto)
                _option = OPTION(
                        _value = str(_dbRowActual.id),
                    )
                for _dbField in _L_addFieldsInfo:
                    if _dbRowActual[_dbField]:
                        _option['_field_'+str(_dbField.name)] = str(_dbRowActual[_dbField])
                    else:
                        # Si esta vacío no lo imprime para evitar trafico no necesario
                        pass
                
                if str(_dbRowActual.id) == str(value):
                    _option['_selected'] = 'selected'
                _option.components.extend(' > '.join(_L_namesParents)) 
                
                # Si el id del row actual existe en la lista de rows, quiere decir que tiene subelementos.
                if str(_dbRowActual.id) in _D_tree:
                    _L_rowActual.n_index += 1
                    _L_dbRow.append(_L_rowActual)
                    _L_dbRow.append(Storage(n_index = 0, L_dbRows = _D_tree[str(_dbRowActual.id)]))
                    if (b_includeId):
                        _option.components.insert(0, str(_dbRowActual.id) + " : ") 
                    if (not _b_onlyLastNodes):
                        if (_dbRowActual.id in _D_tree_filter):
                            # Opción se encuentra filtrada
                            pass
                        else:                            
                            _options.append(_option)
                    
                # Si no existen hijos, vamos por el siguiente hermano.
                else:
                    if _L_namesParents:
                        _L_namesParents.pop()
                    _L_rowActual.n_index += 1
                    _L_dbRow.append(_L_rowActual)
                    if (b_includeId):
                        _option.components.insert(0, str(_dbRowActual.id) + " : ")
                    if (_dbRowActual.id in _D_tree_filter):
                        # Opción se encuentra filtrada
                        pass
                    else:                                              
                        _options.append(_option)
        _n_timeout -= 1
        
    _select.components.extend(_options)
    
    if (field.comment):
        _divGroup = TAG.DIV(
            _class = "input-group stv_chosen_flex",
            )
        _span = TAG.DIV(
            _class = "input-group-btn"
            )
        _button = TAG.BUTTON(
            _type = "button",
            _class = "btn btn-primary stv_hint stv_hintsmall",
            _title = field.comment,
            _hint = field.comment,
            )
        _button['_data-toggle'] = "tooltip"
        _button['_data-placement'] = "bottom"
        _i = I(
            _class = "fa fa-question"
            )
        _button.components.extend([_i])
        _span.components.extend([_button])
        _divGroup.components.extend([_select, _span])
        return _divGroup
    else:
        return _select
    
def stv_widget_db_chosen_linked(field, value, maxselect, filterFieldName, linkedUrl, D_additionalAttributes = dict()):
    """ Widget usado para mostrar un combo box al estilo chosen http://harvesthq.github.io/chosen/ con un boton
    de tip en caso de definir comentarios en los campos
    """

    if filterFieldName:
        if isinstance(filterFieldName, str):
            _L_currentTableFieldNames = [filterFieldName]
        elif isinstance(filterFieldName, list):
            _L_currentTableFieldNames = filterFieldName           
        else:
            _L_currentTableFieldNames = ["error_campo_no_definido"]
    else:
        _L_currentTableFieldNames = ["error_campo_no_definido"]

    if 'linkedTableFieldName' in D_additionalAttributes:
        if isinstance(D_additionalAttributes['linkedTableFieldName'], str):
            _L_linkedTableFieldNames = [D_additionalAttributes['linkedTableFieldName']]
        elif isinstance(D_additionalAttributes['linkedTableFieldName'], list):
            _L_linkedTableFieldNames = D_additionalAttributes['linkedTableFieldName']            
        else:
            _L_linkedTableFieldNames = _L_currentTableFieldNames
        
        if len(_L_currentTableFieldNames) != len(_L_linkedTableFieldNames):
            _L_linkedTableFieldNames = list(_L_currentTableFieldNames)
        else:
            pass

        del D_additionalAttributes['linkedTableFieldName']
    else:
        _L_linkedTableFieldNames = _L_currentTableFieldNames

    if ('multiple' in D_additionalAttributes):
        _sId = "%s_%s_%s" % (field._tablename, field.name, D_additionalAttributes['multiple'])
    else:
        _sId = "%s_%s" % (field._tablename, field.name)

    
    _select = SELECT(
        _name = field.name,
        _id = _sId,
        _class = "form-control stv_form-control chosen-select stv_linked"  + (" stv_fieldRequired " if field.required else ""),
        _value = value,
    )
    for _ele in D_additionalAttributes:
        _select[_ele] = D_additionalAttributes[_ele]

    if maxselect > 1:
        _select['_multiple'] = 'multiple'
        _select['_data-placeholder'] = 'Selecciona opciones'
    
    for _n_index, _s_currentTableFieldName in enumerate(_L_currentTableFieldNames):
        _s_linkedFieldName = str(field._table[_s_currentTableFieldName])
        _select['_data-linkedfieldname' + str(_n_index)] = _s_linkedFieldName.replace(".", "_")   
    
    _select['_data-linkedurl'] = linkedUrl
    _select['_data-linkedfieldname_len'] = len(_L_currentTableFieldNames)
    _options = []

    if isinstance( field.requires, ( IS_EMPTY_OR ) ):
        _option = OPTION(
            _value = '',
        )
        _option.components.extend(('Nada')) 
        _options.append(_option)
    else:
        if (field.notnull == False) or (field.required == False):
            _option = OPTION(
                _value = '',
            )
            _option.components.extend(('Nada')) 
            _options.append(_option)

    _refRow = current.request.stv_fwk_recordForWidgets if hasattr(current.request, 'stv_fwk_recordForWidgets') else None
    _refOptions = []
    # Encontrar los elementos relacionados si el tipo de dato referencía a la tabla directamente en su tipo o si la referencía a través de IS_IN_DB
    if (field.referent):
        _refDBTable = field.referent._table
        _refDB = field.db
    elif isinstance( field.requires, ( IS_IN_DB ) ):
        _refDBTable = field.requires.dbset.db[field.requires.ktable]
        _refDB = field.requires.dbset.db
    else:
        _refDBTable = None
        _refDB = None
    
    if (_refDBTable and _refDB):
        if _refRow:
            _L_values = []
            _qry = True
            for _n_index, _s_linkedFieldName in enumerate(_L_linkedTableFieldNames):
                _qry &= (_refDBTable[_s_linkedFieldName] == _refRow[_L_currentTableFieldNames[_n_index]])
                _L_values.append(str(_refRow[_L_currentTableFieldNames[_n_index]]))
            _select['_data-linkedvalue'] = "_".join(_L_values)
            _refOptions = _refDB(_qry).select(_refDBTable.ALL)
                
        else:
             _select['_data-linkedvalue'] = ""
        for _row in _refOptions:
            _option = OPTION(
                _value = _row.id,
            )
            if str(_row.id) == str(value):
                _option['_selected'] = 'selected'
                        # Si es una función lambda...
            if (hasattr(_refDBTable._format, '__call__')):
                # ...ejecuta la función y regresa el resultado.
                _s_refRequire = _refDBTable._format(_row)
            else:
                # ...de lo contrario haz el formato.
                _s_refRequire = _refDBTable._format % _row.as_dict()
    
            _option.components.extend(_s_refRequire) 
            _options.append(_option)
    else:
        _select['_data-linkedvalue'] = "" # Se pone vacío para que se oblige la actualización por AJAX
        if value:
            _option = OPTION(
                    _value = value,
                    _selected = 'selected'
                )
            _option.components.extend(str(value))
            _options.append(_option)

    _select.components.extend(_options)

    if (field.comment):
        _divGroup = TAG.DIV(
            _class = "input-group",
            )
        _span = TAG.DIV(
            _class = "input-group-btn"
            )
        _button = TAG.BUTTON(
            _type = "button",
            _class = "btn btn-primary stv_hint stv_hintsmall",
            _title = field.comment,
            _hint = field.comment,
            )
        _button['_data-toggle'] = "tooltip"
        _button['_data-placement'] = "bottom"
        _i = I(
            _class = "fa fa-question"
            )
        _button.components.extend([_i])
        _span.components.extend([_button])
        _divGroup.components.extend([_select, _span])
        return _divGroup
    else:
        return _select


def stv_widget_db_search(
        field,
        value,
        s_display = None,
        s_url = None,
        s_fieldId = None,
        D_additionalAttributes = dict()
        ):
    """

    @param field:
    @type field:
    @param value:
    @type value:
    @param s_display:
    @type s_display:
    @param s_url:
    @type s_url:
    @param s_fieldId:
    @type s_fieldId:
    @param D_additionalAttributes:
    @type D_additionalAttributes:
    @return:
    @rtype:
    """
    if s_url:
        _s_value = str(value) if value else ""
        
        if 'multiple' in D_additionalAttributes: 
            _s_addAttr_multiple = D_additionalAttributes['multiple']
            del D_additionalAttributes['multiple']
        else:
            _s_addAttr_multiple = False

        if 'search_quantity' in D_additionalAttributes: 
            _n_addAttr_search_quantity = D_additionalAttributes['search_quantity']
            del D_additionalAttributes['search_quantity']
        else:
            _n_addAttr_search_quantity = 1

        if 'reference' in D_additionalAttributes:
            if D_additionalAttributes['reference'].referent:
                # Si se esta tomando reference como ejemplo para el redireccionamiento
                _dbField_addAttr_referenceTable = D_additionalAttributes['reference'].referent.table
            else:
                # De lo contrario si es solamente un campo, su tabla se toma como la tabla de referencia
                _dbField_addAttr_referenceTable = D_additionalAttributes['reference'].table
            del D_additionalAttributes['reference']
        else:
            _dbField_addAttr_referenceTable = field.referent.table

        if 'L_searchFilter' in D_additionalAttributes:
            _s_searchFilter = simplejson.dumps(D_additionalAttributes['L_searchFilter'])
            del D_additionalAttributes['L_searchFilter']
        else:
            _s_searchFilter = ""

        if 's_display' in D_additionalAttributes: 
            _s_display = D_additionalAttributes['s_display']
            del D_additionalAttributes['s_display']
        else:
            _s_display = "{id} : {value}"
            
        if _s_addAttr_multiple:
            _sId = "%s_%s_%s" % (field._tablename, field.name, _s_addAttr_multiple)
        else:
            _sId = "%s_%s" % (field._tablename, field.name)

        _s_edinput_id = _sId
        _O_hidden = INPUT(
            _name = field.name,
            _id = _s_edinput_id,
            _type = 'hidden',
            _value = _s_value,
            requires = field.requires
            )
        
        _O_hidden['_data-stv_search_quantity'] = str(_n_addAttr_search_quantity)

        for _ele in D_additionalAttributes:
            _O_hidden[_ele] = D_additionalAttributes[_ele]

        _dbRow_reference = _dbField_addAttr_referenceTable(value) 
        if _dbRow_reference:
            # Si es una función lambda...
            if hasattr(_dbField_addAttr_referenceTable._format, '__call__'):
                # ...ejecuta la función y regresa el resultado.
                _s_refRequire = _dbField_addAttr_referenceTable._format(_dbRow_reference)
                _O_hidden['_data-stv_search_fieldname_desc'] = _s_refRequire
            else:
                # ...de lo contrario haz el formato.
                _s_refRequire = _dbField_addAttr_referenceTable._format % _dbRow_reference.as_dict()
                _O_hidden['_data-stv_search_fieldname_desc'] = s_display or _dbField_addAttr_referenceTable._format
        else:
            _s_refRequire = ''
            _O_hidden['_data-stv_search_fieldname_desc'] = s_display

        _s_nombreInput = "search_"+field.name
        _O_input = INPUT(
            _name = _s_nombreInput,
            _id = "search_%s" % _s_edinput_id,
            _class = str(field.type) + " form-control " + (" stv_fieldRequired " if field.required else ""),
            _value = _s_display.format(id = _s_value, value = _s_refRequire),
            _format_display = _s_display,
            _type = 'text',
            _readonly = "readonly") 

        _divGroup = TAG.DIV(
            _class = "input-group",
            )
        _span = TAG.DIV(
            _class = "input-group-btn"
            )
        _button = TAG.BUTTON(
            _type = "button",
            _class = "btn btn-primary stv_dbsearch",
            )
        _button['_data-action-modal-url'] = s_url
        _button['_data-action'] = current.request.stv_fwk_permissions.form_search.code
        _button['_data-action_search_id'] = s_fieldId # Uso futuro no definido por ahora
        # No usado _button['_data-stv_fieldname'] = field.name
        _button['_data-action_search_edinput_id'] = _s_edinput_id
        _button['_data-action_search_edinput_format'] = _s_display
        _button['_data-toggle'] = "modal"
        _button['_data-stv_search_filter'] = _s_searchFilter

        _i = I(
            _class = "fa fa-search"
            )
        _button.components.extend([_i])
        _span.components.extend([_button])

        _L_addOnDespues = []
        if isinstance(field.requires, IS_EMPTY_OR):
            _btn_limpia = TAG.BUTTON(
                I(_class = "fa fa-times"),
                _type = "button", _class = "btn btn-primary stv_input_limpia",
                _nombreinput = _s_nombreInput
                )
            _L_addOnDespues += [_btn_limpia]
        else:
            pass
        _span.components.extend(_L_addOnDespues)

        _divGroup.components.extend([_O_input, _O_hidden, _span])
        return _divGroup
    else:
        return stv_widget_db_combobox(field, value)


def _stv_represent_common_decorate(fnFunc):
    def _stv_represent_commonwrapper(x_value, odbRow, sFieldType, **dArgs):
        _oRepresent = None
        # Si se esta viendo el registro o modificando...
        _E_presentacion = current.response.get('stv_E_presentacion', STV_FWK_FORM.E_PRESENTACION.NO_DEFINIDO)

        if _E_presentacion == STV_FWK_FORM.E_PRESENTACION.NO_DEFINIDO:
            # Si no se tiene definida la presentación pero se trata de identificar de otra manera
            if current.request.get('stv_fwk_recordForWidgets', None):
                # Si esta definido es forma
                _E_presentacion = STV_FWK_FORM.E_PRESENTACION.FORMA
            elif dArgs.get('D_additionalAttributes', {}).get('bForce', False):
                # Si esta el parametro bForce en true, se forza la forma
                _E_presentacion = STV_FWK_FORM.E_PRESENTACION.FORMA
            else:
                _E_presentacion = STV_FWK_FORM.E_PRESENTACION.TABLA
        else:
            # Mantiene el valor de E_presentacion
            pass

        if _E_presentacion == STV_FWK_FORM.E_PRESENTACION.FORMA:
            _oRepresent = fnFunc(x_value, odbRow, sFieldType, **dArgs)
        else:
            _oRepresent = SPAN(
                _class = (dArgs.get('sAddClass', '') or '') + ' stv_represent_atgrid',
                )
            if dArgs.get('D_additionalAttributes', {}).get('_data-value'):
                _oRepresent['_data-value'] = dArgs.get('D_additionalAttributes', {}).get('_data-value')
            else:
                _oRepresent['_data-value'] = x_value
            if sFieldType == 'file':
                _oRepresent.components.extend(dArgs['L_addOnAfter'])
            elif x_value is None:
                # _oRepresent.components.extend([])
                pass
            else:
                _oRepresent.components.extend([x_value])
        return _oRepresent
    return _stv_represent_commonwrapper


@_stv_represent_common_decorate
def _stv_represent_input_common(
        x_value, 
        odbRow, 
        sFieldType = 'string', 
        L_addOnAfter = None, 
        sAddOnBefore = None, 
        sAddClass = None, 
        sPlaceHolder = None,
        dbField = None, 
        D_additionalAttributes = dict()
        ):
    """ Represent usado para representar cualquier input manteniendo generalidades
    :param x_value: valor del campo
    :param odbRow: registro
    :param sInputType: tipo del campo a hacer represent
    :param L_addOnAfter: texto que se deplegará en el editor al final, como %, $ o None en caso de no requerirse 
    :param sAddOnBefore: texto que se deplegará en el editor al inicio, como %, $ o None en caso de no requerirse 
    :param sAddClass: clase o clases adicionales a agregar al input
    :param sPlaceHolder: texto a desplegar en caso de no tener valor
    :param D_additionalAttributes: diccionario con atributos adicionales a agregar al input
    """
    _oRepresent = None
    _oInput = INPUT(
        _class = "stv_represent form-control stv_represent_atform " + (sAddClass or ""),
        _value = x_value,
        _type = 'input',
        _placeholder = sPlaceHolder,
        _disabled = 'disabled',
        _fieldType = sFieldType,
        )
    
    if dbField:
        _oInput['_field'] = str(dbField)
    else:
        pass
    
    for _sIndex in D_additionalAttributes:
        _oInput[_sIndex] = D_additionalAttributes[_sIndex]
    
    if sAddOnBefore or L_addOnAfter:    
        _oDivGroup = TAG.DIV(
            _class = "input-group",
            )
        if sAddOnBefore: 
            _oSpanAddOnBefore = TAG.DIV(
                sAddOnBefore, 
                _class = "input-group-addon"
                )
            _oDivGroup.components.extend([_oSpanAddOnBefore])
        else:
            pass
        _oDivGroup.components.extend([_oInput])
        if L_addOnAfter:
            _oSpanAddOnAfter = TAG.DIV(
                *L_addOnAfter, 
                _class = "input-group-addon"
                ) if L_addOnAfter else None
            _oDivGroup.components.extend([_oSpanAddOnAfter])
        else:
            pass
        _oRepresent = _oDivGroup
    else:
        _oRepresent = _oInput
    return _oRepresent    



''' Codigo para mejorar la presentación de campos tipo boolean  '''
def stv_represent_boolean(x_value, odbRow):
    _oRepresent = None
    _sClassRepresent = 'stv_represent_atform' if odbRow and odbRow.get('update_record', None) else 'stv_represent_atgrid' 
    _oCheck = I(_class='fa fa-check-circle-o stv_represent_boolean ' + _sClassRepresent) if x_value else I(_class='fa fa-circle-o stv_represent_boolean ' + _sClassRepresent)
    _oCheck['_data-value'] = str(x_value)
    _oRepresent = P(_oCheck) if odbRow and odbRow.get('update_record', None) else _oCheck
    return _oRepresent

''' Codigo para mejorar la presentación de campos tipo text  '''
   
def stv_represent_text(value, row, D_additionalAttributes = {}):
    if 'n_cols' in D_additionalAttributes: 
        _n_cols = D_additionalAttributes['n_cols']
        del D_additionalAttributes['n_cols']
    else:
        _n_cols = None

    if 'n_rows' in D_additionalAttributes: 
        _n_rows = D_additionalAttributes['n_rows']
        del D_additionalAttributes['n_rows']
    else:
        _n_rows = None
        
    _input = TEXTAREA(
        _value = str(value) if value else "",
        value = str(value) if value else "",
        _cols = _n_cols,
        _rows = _n_rows,
        _disabled = 'disabled'
        )
    _lClasses = ["stv_represent form-control"]
    for _ele in D_additionalAttributes:
        _input[_ele] = D_additionalAttributes[_ele]
    _input['_class'] = " ".join(_lClasses)

    return _input

def stv_represent_xml(value, row, D_additionalAttributes = {}):
    
    if 'n_cols' in D_additionalAttributes: 
        _n_cols = D_additionalAttributes['n_cols']
        del D_additionalAttributes['n_cols']
    else:
        _n_cols = None

    if 'n_rows' in D_additionalAttributes: 
        _n_rows = D_additionalAttributes['n_rows']
        del D_additionalAttributes['n_rows']
    else:
        _n_rows = None
        
    _input = TEXTAREA(
        _value = str(value) if value else "",
        value = str(value) if value else "",
        _cols = _n_cols,
        _rows = _n_rows,
        )
    _lClasses = ["stv_represent_xml stv_represent form-control"]
    for _ele in D_additionalAttributes:
        _input[_ele] = D_additionalAttributes[_ele]
    _input['_class'] = " ".join(_lClasses)

    return _input

def stv_represent_json(value, row, D_additionalAttributes = {}):
    
    if 'n_cols' in D_additionalAttributes: 
        _n_cols = D_additionalAttributes['n_cols']
        del D_additionalAttributes['n_cols']
    else:
        _n_cols = None

    if 'n_rows' in D_additionalAttributes: 
        _n_rows = D_additionalAttributes['n_rows']
        del D_additionalAttributes['n_rows']
    else:
        _n_rows = None
        
    _input = TEXTAREA(
        _value = str(value) if value else "",
        value = str(value) if value else "",
        _cols = _n_cols,
        _rows = _n_rows,
        )
    _lClasses = ["stv_represent_json stv_represent form-control"]
    for _ele in D_additionalAttributes:
        _input[_ele] = D_additionalAttributes[_ele]
    _input['_class'] = " ".join(_lClasses)

    return _input


def stv_represent_image(x_value, odbRow, s_url = None):
    if x_value:
        _L_url = s_url.split("?") if s_url else ["download"]
        _L_url[0] += "/%s" % x_value

        _s_url = "?".join(_L_url)
        return IMG(_src = _s_url, _class="stv_img_uploaded")
    else:
        return "Nada"


stv_represent_file_showOption_download = 0
stv_represent_file_showOption_open = 2
stv_represent_file_showOption_opendownload = 3

def stv_represent_file(
        x_value, 
        odbRow, 
        s_url = None, 
        x_linkName = None, 
        dbField = None,
        n_showOption = stv_represent_file_showOption_download):
    """ Represent usado para la representación de campos tipo upload
    :param x_value: valor del campo
    :param odbRow: registro de la tabla
    :param s_url: url usado para bajar el archivo
    :param x_linkName: link que define el texto del link, si es un campo usara el valor de ese campo en odbRow
    :param sFieldFileName: nombre del campo que contendrá el nombre del archivo
    :param n_showOption: define si se va a mostrar el link de descargar "1", ver "2" o ver y descargar "3"
    """
    if x_value:
        _sFileName = "file"
        if isinstance(x_linkName, str):
            _sLinkName = x_linkName
        elif isinstance(x_linkName, Field):
            if (x_linkName.name in odbRow):
                _sLinkName = odbRow[x_linkName.name]
                _sFileName = _sLinkName
            elif (x_linkName.tablename in odbRow):
                if (x_linkName.name in odbRow[x_linkName.tablename]):
                    _sLinkName = odbRow[x_linkName.tablename][x_linkName.name]
                    _sFileName = _sLinkName
                else:
                    _sLinkName = str(x_linkName)
            else:
                _sLinkName = str(x_linkName)
        else:
            _sLinkName = str(x_linkName)
        if not _sFileName:
            _sFileName = "file"
            
        if s_url:
            _L_url = (s_url).split("?")
        else:
            _L_url = ["download"]
        
        if len(_L_url) == 1:
            _s_url = (_L_url[0] + '/' + x_value + '?filename=' + str(_sFileName))
        else:
            _s_url = (_L_url[0] + '/' + x_value + '?filename=' + str(_sFileName) + '&' + _L_url[1])          
                    
        if (n_showOption == 2): #Link de ver
            _L_addOnAfter = [A(I(_class = "fa fa-external-link"), _href=_s_url + "&display=yes", _target="_blank")]
        elif (n_showOption ==3): #Link de descarga y ver
            _L_addOnAfter = [A(I(_class = "fa fa-external-link"), _href=_s_url + "&display=yes", _target="_blank"), A(I(_class = "fa fa-download"), _href=_s_url, _target="_blank")]
        else: #Default: Link de descarga
            _L_addOnAfter = [A(I(_class = "fa fa-download"), _href=_s_url, _target="_blank")]
        
        return _stv_represent_input_common(
                            _sLinkName, odbRow, 
                            sFieldType = 'file', 
                            L_addOnAfter = _L_addOnAfter, 
                            sAddOnBefore = None, 
                            sAddClass = "stv_file_uploaded", 
                            sPlaceHolder = None, 
                            dbField = dbField, 
                            D_additionalAttributes = dict())
    else:
        return _stv_represent_input_common(('Nada'), odbRow, 'string', 
                                       L_addOnAfter = None, sAddOnBefore = None, 
                                       sAddClass = 'stv_represent_string', sPlaceHolder = None, 
                                       dbField = dbField,
                                       D_additionalAttributes = dict())

def stv_represent_advanced(value, row, field, sFormat, sClass):
    _representSpan = SPAN(
        _class = 'stv_represent ' + sClass,
        )
    if (str(field.type) == 'datetime') and value:
        _sContent = value.strftime(sFormat)
    else:
        _sContent = sFormat % value
    _representSpan.components.extend([_sContent])

    return _representSpan

def stv_represent_referencefield(x_value, odbRow, dbField = None, D_additionalAttributes = None):
    """ Representa un campo referenciado.
    
    x_value: valor del id que se busará en la referencia, si el campo fue definido con el reference, si tiene la referencia a la tabla relacionada
    odbRow: renglon de la tabla con todos los valores
    dbField: en caso de no ser el campo de tipo Reference, utiliza este campo auxiliar para determinar la referencia
    D_additionalAttributes: configuraciones adicionales
        - campo_si_nulo: campo que se utilizara en caso de x_value sea None
    
    """
    _s_value = ""
    
    if not D_additionalAttributes:
        _D_additionalAttributes = {}
    else:
        _D_additionalAttributes = D_additionalAttributes

    if 'campo_si_nulo' in _D_additionalAttributes:
        _dbField_siNulo = _D_additionalAttributes['campo_si_nulo']
        del _D_additionalAttributes['campo_si_nulo']
    else:
        _dbField_siNulo = None

    # Se define de donde se obtiene el valor
    if x_value:
        _x_value = x_value
    elif _dbField_siNulo:
        if hasattr(odbRow, str(_dbField_siNulo)):
            _x_value = odbRow[_dbField_siNulo]
            # Se ejecuta el represent del campo relacionado
            _s_value = _dbField_siNulo.represent(_x_value, odbRow)
            return _s_value
        else:
            _x_value = x_value
            _s_value = ('Campo no en Row')
    else:
        _x_value = x_value
        

    _D_fieldRef = Storage(
        db = None,
        dbT = None,
        s_nombrecampo = None
        )

    # Se define de donde se obtiene la referencia
    if dbField:
        _D_fieldRef = stv_obtener_referencias(dbField)
    elif ('Reference' in str(type(_x_value))) and _x_value._table:
        _D_fieldRef.db = _x_value._table._db
        _D_fieldRef.dbT = _x_value._table
        _D_fieldRef.s_nombrecampo = _x_value._table._id.name
    else:
        _s_value = "Nada"

    if not('_field' in _D_additionalAttributes):
        if ('multiple' in _D_additionalAttributes):
            _D_additionalAttributes['_field'] = "%s.%s_%s" % (str(_D_fieldRef.dbT), _D_fieldRef.s_nombrecampo, str(_D_additionalAttributes['multiple']))
        else:
            _D_additionalAttributes['_field'] = "%s.%s" % (str(_D_fieldRef.dbT), _D_fieldRef.s_nombrecampo)
    else:
        # Usa el campo _field como campo
        pass    

    if not('_data-value' in _D_additionalAttributes):
        _D_additionalAttributes['_data-value'] = str(_x_value)
    else:
        pass

    if not _D_fieldRef.s_nombrecampo:
        _s_value = ""

    elif not(_s_value) and _x_value:

        _dbRows = _D_fieldRef.db(_D_fieldRef.dbT[_D_fieldRef.s_nombrecampo] == _x_value).select(_D_fieldRef.dbT.ALL) 
        if _dbRows:
            # Si es una función lambda...
            if (hasattr(_D_fieldRef.dbT._format, '__call__')):
                # ...ejecuta la función y regresa el resultado.
                _s_value = _D_fieldRef.dbT._format(_dbRows.first())
            else:
                # ...de lo contrario haz el formato.
                _s_value = _D_fieldRef.dbT._format % _dbRows.first().as_dict()
        else:
            pass
    elif _s_value == "Nada":
        _s_value = ""
    else:
        pass
        
    return _stv_represent_input_common(_s_value, odbRow, 'string', 
                                       L_addOnAfter = None, sAddOnBefore = None, 
                                       sAddClass = 'stv_represent_string', sPlaceHolder = None, 
                                       dbField = dbField,
                                       D_additionalAttributes = _D_additionalAttributes)


def stv_represent_referencefield_tree(x_value, odbRow, dbFieldTree, dbField = None):
    """ Crea una referencia de padres, hasta el elemento raiz

    @param x_value: valor a imprimir en la referencia
    @type x_value:
    @param odbRow: renglon con el contenido del registro
    @type odbRow:
    @param dbFieldTree: campo usado para los padres del registro actual
    @type dbFieldTree:
    @param dbField: en caso de que el campo padre no es el mismo que el campo actual, se debe especificar
    @type dbField:
    @return:
    @rtype:
    """
    _D_additionalAttributes = {}

    # Se define el campo
    dbField = dbField if dbField else dbFieldTree

    # Se identifica la tabla referenciada
    if dbField.referent:
        _refDB = dbField.db
        _refDBTable = dbField.referent.table
        _s_refFieldName = dbField.referent.name
    elif isinstance(dbField.requires, IS_EMPTY_OR):
        _refDB = dbField.requires.other.dbset.db
        _refDBTable = _refDB[dbField.requires.other.ktable]
        _s_refFieldName = dbField.requires.other.kfield
    elif isinstance(dbField.requires, IS_IN_DB):
        _refDB = dbField.requires.dbset.db
        _refDBTable = _refDB[dbField.requires.ktable]
        _s_refFieldName = dbField.requires.kfield
    else:
        _refDB = None
        _refDBTable = None
        _s_refFieldName = None

    _x_value = x_value
    _L_references = []

    while _x_value:

        _L_value = []
        _dbRows = _refDB(_refDBTable[_s_refFieldName] == _x_value).select(_refDBTable.ALL)
        if _dbRows:
            for _dbRow in _dbRows:
                if hasattr(_refDBTable._format, '__call__'):
                    # ...ejecuta la función y regresa el resultado.
                    _L_value.append(_refDBTable._format(_dbRow))
                else:
                    # ...de lo contrario haz el formato.
                    _L_value.append(_refDBTable._format % _dbRow.as_dict())
            _x_value = _dbRows.first()[dbFieldTree]
        else:
            _x_value = None
            _L_value.append("No encontrado")

        _L_references.insert(0, " | ".join(_L_value))

    return _stv_represent_input_common(
        " > ".join(_L_references), odbRow, 'string', L_addOnAfter = None, sAddOnBefore = None,
        sAddClass = 'stv_represent_string', sPlaceHolder = None,
        dbField = dbField, D_additionalAttributes = _D_additionalAttributes
        )


def stv_represent_string(x_value, odbRow, fnLambda = None, dbField = None, D_additionalAttributes = dict()):
    _xValue = fnLambda(x_value, odbRow) if fnLambda else x_value
    return _stv_represent_input_common(_xValue, odbRow, 'string', 
                                       L_addOnAfter = None, sAddOnBefore = None, 
                                       sAddClass = 'stv_represent_string', sPlaceHolder = None,
                                       dbField = dbField, 
                                       D_additionalAttributes = D_additionalAttributes)

def stv_represent_date(x_value, odbRow, fnLambda = None, dbField = None, D_additionalAttributes = dict()):
    if x_value:
        # TODO, checar en los require del campo para tomar el formato de la fecha
        if isinstance(x_value, (datetime.date, datetime.datetime)):
            _xValue = x_value.strftime(STV_FWK_APP.FORMAT.s_DATE)
        else:
            _xValue = fnLambda(x_value, odbRow) if fnLambda else x_value
    else:
        _xValue = ('Nada')
    return _stv_represent_input_common(_xValue, odbRow, 'date', 
                                       L_addOnAfter = None, sAddOnBefore = None, 
                                       sAddClass = 'stv_represent_date', sPlaceHolder = None, 
                                       dbField = dbField,
                                       D_additionalAttributes = D_additionalAttributes)


def stv_represent_datetime(x_value, odbRow, fnLambda = None, dbField = None, D_additionalAttributes = dict()):
    _s_value = ""
    if not D_additionalAttributes:
        _D_additionalAttributes = {}
    else:
        _D_additionalAttributes = D_additionalAttributes

    if 'campo_si_nulo' in _D_additionalAttributes:
        _dbField_siNulo = _D_additionalAttributes['campo_si_nulo']
        del _D_additionalAttributes['campo_si_nulo']
    else:
        _dbField_siNulo = None
    
    # Se define de donde se obtiene el valor
    if x_value:
        _x_value = x_value

    elif _dbField_siNulo:
        if hasattr(odbRow, str(_dbField_siNulo)):
            _x_value = odbRow[_dbField_siNulo]
            # Se ejecuta el represent del campo relacionado
            _s_value = _dbField_siNulo.represent(_x_value, odbRow)
            return _s_value

        else:
            _x_value = x_value
            _s_value = 'Campo no en Row'

    else:
        _x_value = x_value
        _s_value = ''
            
    if not _s_value:
        
        # TODO, checar en los require del campo para tomar el formato de la fecha
        if isinstance(_x_value, (datetime.time, datetime.datetime)):
            _x_value = _x_value.strftime(STV_FWK_APP.FORMAT.s_DATETIME)
        else:
            _x_value = fnLambda(_x_value, odbRow) if fnLambda else _x_value
    else:
        _x_value = _s_value

    return _stv_represent_input_common(
        _x_value, odbRow, 'datetime',
        L_addOnAfter = None, sAddOnBefore = None,
        sAddClass = 'stv_represent_string', sPlaceHolder = None,
        dbField = dbField,
        D_additionalAttributes = D_additionalAttributes
        )


def stv_represent_time(x_value, odbRow, fnLambda = None, dbField = None, D_additionalAttributes = dict()):
    if x_value:
        # TODO, checar en los require del campo para tomar el formato de la fecha
        if isinstance(x_value, (datetime.time, datetime.datetime)):
            _xValue = x_value.strftime(STV_FWK_APP.FORMAT.s_TIME)
        else:
            _xValue = fnLambda(x_value, odbRow) if fnLambda else x_value
    else:
        _xValue = ('Nada')
    return _stv_represent_input_common(_xValue, odbRow, 'time', 
                                       L_addOnAfter = None, sAddOnBefore = None, 
                                       sAddClass = 'stv_represent_string', sPlaceHolder = None, 
                                       dbField = dbField,
                                       D_additionalAttributes = D_additionalAttributes)


def stv_represent_number(x_value, odbRow, fnLambda = None, dbField = None, D_additionalAttributes = dict()):
    _xValue = fnLambda(x_value, odbRow) if fnLambda else x_value
    return _stv_represent_input_common(
        _xValue, odbRow, 'integer',
        L_addOnAfter = None, sAddOnBefore = None,
        sAddClass = 'stv_represent_number', sPlaceHolder = None,
        dbField = dbField,
        D_additionalAttributes = D_additionalAttributes
        )

def stv_represent_number_ifnotzero(x_value, odbRow, fnLambda = None, dbField = None, D_additionalAttributes = dict()):
    _s_value = ""
    if not D_additionalAttributes:
        _D_additionalAttributes = {}
    else:
        _D_additionalAttributes = D_additionalAttributes

    if 'campo_si_nulo' in _D_additionalAttributes:
        _dbField_siNulo = _D_additionalAttributes['campo_si_nulo']
        del _D_additionalAttributes['campo_si_nulo']
    else:
        _dbField_siNulo = None
    
    # Se define de donde se obtiene el valor
    if x_value:
        _x_value = x_value
    elif _dbField_siNulo:
        if hasattr(odbRow, str(_dbField_siNulo)):
            _x_value = odbRow[_dbField_siNulo]
            # Se ejecuta el represent del campo relacionado
            _s_value = _dbField_siNulo.represent(_x_value, odbRow)
            return _s_value
        else:
            _x_value = x_value
            _s_value = ('Campo no en Row')
    else:
        _x_value = x_value

    if (_x_value != 0):
        return stv_represent_number(_x_value, odbRow, fnLambda, _D_additionalAttributes)
    else:
        return stv_represent_number("", odbRow, fnLambda, _D_additionalAttributes)

def stv_represent_percentage(x_value, odbRow, fnLambda = None, dbField = None, D_additionalAttributes = dict()):
    _xValue = fnLambda(x_value, odbRow) if fnLambda else x_value
    return _stv_represent_input_common(_xValue, odbRow, 'float', 
                                       L_addOnAfter = ['%'], sAddOnBefore = None, 
                                       sAddClass = 'stv_represent_percentage', sPlaceHolder = None, 
                                       dbField = dbField,
                                       D_additionalAttributes = D_additionalAttributes)

def stv_represent_money_ifnotzero(
        x_value, 
        odbRow, 
        fnLambda = None, 
        dbField = None, 
        D_additionalAttributes = dict()
        ):
    _s_value = ""
    if not D_additionalAttributes:
        _D_additionalAttributes = {}
    else:
        _D_additionalAttributes = D_additionalAttributes

    if 'campo_si_nulo' in _D_additionalAttributes:
        _dbField_siNulo = _D_additionalAttributes['campo_si_nulo']
        del _D_additionalAttributes['campo_si_nulo']
    else:
        _dbField_siNulo = None
    
    # Se define de donde se obtiene el valor
    if x_value:
        _x_value = x_value
    elif _dbField_siNulo:
        if hasattr(odbRow, str(_dbField_siNulo)):
            _x_value = odbRow[_dbField_siNulo]
            # Se ejecuta el represent del campo relacionado
            _s_value = _dbField_siNulo.represent(_x_value, odbRow)
            return _s_value
        else:
            _x_value = x_value
            _s_value = ('Campo no en Row')
    else:
        _x_value = x_value

    if (_x_value != 0):
        return stv_represent_money(_x_value, odbRow, fnLambda, dbField = dbField, D_additionalAttributes = _D_additionalAttributes)
    else:
        return stv_represent_number("", odbRow, fnLambda, dbField = dbField, D_additionalAttributes = _D_additionalAttributes)

def stv_represent_money(x_value, odbRow, fnLambda = None, dbField = None, D_additionalAttributes = dict()):
    _xValue = fnLambda(x_value, odbRow) if fnLambda else x_value
    return _stv_represent_input_common(_xValue, odbRow, 'float', 
                                       L_addOnAfter = None, sAddOnBefore = '$', 
                                       sAddClass = 'stv_represent_money', sPlaceHolder = None, 
                                       dbField = dbField,
                                       D_additionalAttributes = D_additionalAttributes)

def stv_represent_float(x_value, odbRow, fnLambda = None, dbField = None, D_additionalAttributes = dict()):
    _xValue = fnLambda(x_value, odbRow) if fnLambda else x_value
    return _stv_represent_input_common(_xValue, odbRow, 'float', 
                                       L_addOnAfter = None, sAddOnBefore = None, 
                                       sAddClass = 'stv_represent_float', sPlaceHolder = None, 
                                       dbField = dbField,
                                       D_additionalAttributes = D_additionalAttributes)

def stv_represent_icon(x_value, odbRow, fnLambda = None, dbField = None, D_additionalAttributes = dict()):
    _xValue = fnLambda(x_value, odbRow) if fnLambda else x_value
    return _stv_represent_input_common(_xValue, odbRow, 'string', 
                                       L_addOnAfter = None,
                                       sAddOnBefore = XML(x_value),  
                                       sAddClass = 'stv_represent_icon', sPlaceHolder = None, 
                                       dbField = dbField,
                                       D_additionalAttributes = D_additionalAttributes)


def stv_represent_list(
        x_value,
        odbRow,
        fnLambda = None,
        D_data = None,
        dbField = None,
        D_additionalAttributes = dict()
        ):

    if x_value:
        _L_ids = (str(x_value)).split(',')
        _L_values = []
        for _s_id in _L_ids:
            if _s_id in D_data:
                _L_values.append(D_data[_s_id])
            else:
                _L_values.append(D_data[int(_s_id)])
                
        _s_value = ", ".join(_L_values)

    elif x_value in D_data:
        _s_value = D_data[x_value]

    else:
        _s_value = ""

    if D_additionalAttributes.get('_data-value'):
        # Mantener el valor definido para data-value
        pass

    else:
        D_additionalAttributes['_data-value'] = str(x_value)

    _xValue = fnLambda(_s_value, odbRow) if fnLambda else _s_value

    return _stv_represent_input_common(_xValue, odbRow, 'string', 
                                       L_addOnAfter = None,
                                       sAddOnBefore = None,  
                                       sAddClass = 'stv_represent_icon', sPlaceHolder = None, 
                                       dbField = dbField,
                                       D_additionalAttributes = D_additionalAttributes)
