# -*- coding: utf-8 -*-

import datetime
import re

from gluon import *
from gluon.storage import Storage
from gluon.html import xmlescape
from gluon.dal import SQLCustomType
from gluon.dal import Expression
from pydal.objects import FieldVirtual, FieldMethod
from decimal import Decimal
import json
import gluon.contrib.simplejson as simplejson


class STV_FWK_LIB:
    DIAS_SEMANA = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado']
    MESES = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre',
        'Noviembre', 'Diciembre']

    # gListDaysMin = [ele[:2] for ele in gListDays]
    # gListDaysShort = [ele[:3] for ele in gListDays]
    #
    # gListMonths =
    # gListMonthsShort = [ele[:3] for ele in gListMonths]

    @staticmethod
    def DECODIFICAR_ARGS(
            L_args,
            n_offsetArgs,
            s_codigoDefault = None,
            **D_otros
            ):
        """ Decodifica los argumentos en diccionario de argumentos, acción y id

        @param L_args: lista con los argumentos a decodificar
        @type L_args:
        @param n_offsetArgs: número que indica el offset en los argumentos anteriores a identificar la acción y el id.
            Debe ser un múltiplo de 2, de lo contrario se ignora el último elementos
        @type n_offsetArgs:
        @param s_codigoDefault: código por default usado en caso de accion no definida, si no existe se pone None
        @type s_codigoDefault:
        @param D_otros: parámetros adicionales que se agregan al diccionario de retorno
        @type D_otros:
        @return: diccionario con los elementos de L_args, s_accion, n_id, L_ids en caso de varios ids,
        @rtype:
        """
        _D_return = Storage(
            s_accion = "",
            n_id = "",  # En caso de un número único se llena este campo, de lo contrario se pone lo que se recibió
            L_ids = [],  # En caso de IDs separados por
            L_argsAdicionales = [],
            n_offsetArgs = n_offsetArgs,
            L_argsBasicos = [],  # Son los argumentos que pueden usarse para las llamadas a subtabs
            **D_otros
            )

        _D_return.L_argsBasicos = L_args[:n_offsetArgs]

        if n_offsetArgs > len(L_args):
            _D_return.accion = "ErrorArgs_y_Offset"

        else:
            _s_argumento = ""
            for _n_index, _s_arg in enumerate(L_args):
                if _n_index < n_offsetArgs:
                    if not _n_index.__mod__(2):
                        # Si es un argumento par
                        _s_argumento = _s_arg
                    else:
                        _D_return[_s_argumento] = _s_arg

                elif not _D_return.s_accion:
                    _D_return.s_accion = _s_arg

                elif not _D_return.n_id:
                    _D_return.n_id = _s_arg
                    _D_return.L_ids = STV_FWK_LIB.OBTENER_IDS(_D_return.n_id)

                else:
                    _D_return.L_argsAdicionales.append(_s_arg)

        _D_return.s_accion = _D_return.s_accion or s_codigoDefault or current.request.stv_fwk_permissions.btn_none.code

        return _D_return

    @staticmethod
    def DEFINIR_INTEGRIDADREFERENCIAL(
            dbTabla_referenciada,
            dbCampo_haceReferencia,
            b_actualizarTipo = None,
            fn_widget = None,
            dbQry = None
            ):

        dbTabla_referenciada._referenced_by.append(dbCampo_haceReferencia)
        dbTabla_referenciada._references.append(dbCampo_haceReferencia)

        if b_actualizarTipo:
            _s_nombreTabla = dbTabla_referenciada._tablename
            dbCampo_haceReferencia.type = "reference %s" % _s_nombreTabla
            dbCampo_haceReferencia.ondelete = 'NO ACTION'

            if not dbCampo_haceReferencia.notnull:
                dbCampo_haceReferencia.requires = IS_NULL_OR(
                    IS_IN_DB(dbTabla_referenciada._db(dbQry), dbTabla_referenciada.id, dbTabla_referenciada._format)
                    )
            else:
                dbCampo_haceReferencia.requires = IS_IN_DB(
                    dbTabla_referenciada._db(dbQry), dbTabla_referenciada.id, dbTabla_referenciada._format
                    )

        else:
            pass

        if fn_widget:
            dbCampo_haceReferencia.widget = fn_widget
        else:
            pass

        return None

    @staticmethod
    @current.cache("STV_FWK_LIB.AGREGAR_INDICES", time_expire = 86400, cache_model = current.cache.ram)
    def AGREGAR_INDICES(dbReferencia, L_dbFields):
        """ Agrega los indices a las tablas relacionadas para los campos que no son definidos como referencias
        a otros campos en la misma base de datos.

        Esta función se ejecuta una sola vez cada día.
        """

        # Se obtiene el nombre de la base de datos
        _O_regex = re.compile('(?P<protocolo>\w+)\:\/\/(?P<usuario>\w+)\:.*\@(?P<servidor>\w+)\/(?P<basededatos>\w+)')
        _O_match = _O_regex.match(L_dbFields[0].db._uri)
        if _O_match and _O_match.group('basededatos'):
            _s_basededatos = _O_match.group('basededatos')

            for _dbField in L_dbFields:
                _s_instruccion_sql = (
                    "SELECT COUNT(1) FROM INFORMATION_SCHEMA.STATISTICS "
                    "WHERE table_schema='%s' AND table_name='%s' AND index_name='%s__idx'"
                     ) % (_s_basededatos, _dbField.tablename, _dbField.name)
                _dbRows = dbReferencia.executesql(_s_instruccion_sql)

                if (
                        _dbRows
                        and (_dbRows[0][0] == 0)
                        ):
                    _s_instruccion_sql = (
                        "CREATE INDEX %s__idx ON %s (%s);"
                         ) % (_dbField.name, _dbField.tablename, _dbField.name)
                    dbReferencia.executesql(_s_instruccion_sql)

                else:
                    pass
        else:
            pass

        return None

    @staticmethod
    def OBTENER_IDS(s_id):
        _L_ids = []
        if "IDS_" in str(s_id):
            _L_ids = s_id.split("_")[1:]
        else:
            _L_ids = [s_id]

        for _n_index, _x_id in enumerate(_L_ids):
            if "-" in str(_x_id):
                _L_ids[_n_index] = _x_id.split("-")

            elif str(_x_id):
                _L_ids[_n_index] = [_L_ids[_n_index]]

            else:
                # No tiene subcampos el id, es solo un campo
                _L_ids[_n_index] = []

        return _L_ids

    @staticmethod
    def OBTENER_CAMPOSACHECAR_PARA_VALIDAR(dbTabla, dbRow = None, O_form = None):
        """ Función que regresa un diccionario con el valor a usar en el registro.

        Se basa en la funcionalidad del FWK, utiliza en primera instacnia el valor en el
        request.vars, si existe en row usa el row, de lo contrario el valor por default.

        @param O_form:
        @type O_form:
        @param dbTabla:
        @type dbTabla:
        @param dbRow:
        @type dbRow:
        @return:
        @rtype:
        """

        def convierte(dbCampo, s_valor):
            _x_valor = None
            if isinstance(dbCampo.type, str) \
                    and (dbCampo.type != 'upload') \
                    and s_valor:
                if (dbCampo.type == 'integer')\
                        or ('reference' in dbCampo.type):
                    _x_valor = int(s_valor)
                elif 'decimal' in dbCampo.type:
                    _x_valor = Decimal(s_valor)
                else:
                    _x_valor = s_valor
            else:
                # Campo esta vacío
                _x_valor = s_valor
            return _x_valor

        _D_camposAChecar = Storage()
        for _s_nombreCampo in dbTabla.fields:
            if O_form and (_s_nombreCampo in O_form.vars):
                _D_camposAChecar[_s_nombreCampo] = O_form.vars[_s_nombreCampo]

            elif (_s_nombreCampo in current.request.vars) and dbTabla[_s_nombreCampo].writable:
                _D_camposAChecar[_s_nombreCampo] = convierte(
                    dbTabla[_s_nombreCampo], current.request.vars[_s_nombreCampo]
                    )
            elif dbRow:
                _D_camposAChecar[_s_nombreCampo] = dbRow[_s_nombreCampo]
            else:
                _D_camposAChecar[_s_nombreCampo] = dbTabla[_s_nombreCampo].default

        return _D_camposAChecar

    @staticmethod
    def HTML_REPRESENTAR_CAMPO(
            dbCampo,
            dbRow
            ):
        return XML(
            "<label>%s</label><br/>%s" % (
            dbCampo.label, dbCampo.represent(dbRow[dbCampo], dbRow) if dbCampo.represent else dbRow[dbCampo]
                )
            )

    @staticmethod
    def CONVIERTE_A_LETRAS(f_numero):

        def convierte_tresCifras(d_numero, n_selector_unidad):
            _L_centena = [
                "", ("CIEN", "CIENTO"), "DOSCIENTOS", "TRESCIENTOS", "CUATROCIENTOS", "QUINIENTOS",
                "SEISCIENTOS", "SETECIENTOS", "OCHOCIENTOS", "NOVECIENTOS"
                ]
            _L_decena = [
                "",
                ("DIEZ", "ONCE", "DOCE", "TRECE", "CATORCE", "QUINCE", "DIECISEIS", "DIECISIETE", "DIECIOCHO",
                "DIECINUEVE"),
                ("VEINTE", "VEINTI"),
                ("TREINTA", "TREINTA Y "),
                ("CUARENTA", "CUARENTA Y "),
                ("CINCUENTA", "CINCUENTA Y "),
                ("SESENTA", "SESENTA Y "),
                ("SETENTA", "SETENTA Y "),
                ("OCHENTA", "OCHENTA Y "),
                ("NOVENTA", "NOVENTA Y ")
                ]
            _L_unidad = [
                "", ("UN", "UNO"), "DOS", "TRES", "CUATRO", "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE"
                ]
            _d_centena = int(d_numero / 100)
            _d_decena = int((d_numero - (_d_centena * 100)) / 10)
            _d_unidad = int(d_numero - (_d_centena * 100 + _d_decena * 10))
            # print "_d_centena: ",_d_centena, "_d_decena: ",_d_decena,'_d_unidad: ',_d_unidad

            _s_centena = ""
            _s_decena = ""
            _s_unidad = ""

            # Validad las centenas
            _s_centena = _L_centena[_d_centena]
            if _d_centena == 1:
                _s_centena = _s_centena[1] if (_d_decena + _d_unidad) != 0 else _s_centena[0]
            else:
                pass

            # Valida las decenas
            _s_decena = _L_decena[_d_decena]
            if _d_decena == 1:
                _s_decena = _s_decena[_d_unidad]
            elif _d_decena > 1:
                _s_decena = _s_decena[1] if _d_unidad != 0 else _s_decena[0]
            else:
                pass

            # Validar las unidades
            # print "_s_unidad: ",_s_unidad
            if _d_decena != 1:
                _s_unidad = _L_unidad[_d_unidad]
                if _d_unidad == 1:
                    _s_unidad = _s_unidad[n_selector_unidad]
                else:
                    pass
            else:
                pass

            return "%s %s %s" % (_s_centena, _s_decena, _s_unidad)

        _L_indicador = [  # Indicador por cada tres cifras en singular 1 y plural
            ("", ""),
            ("MIL", "MIL"),
            ("MILLON", "MILLONES"),
            ("MIL", "MIL"),
            ("BILLON", "BILLONES")
            ]
        _d_entero = int(f_numero)
        _d_decimal = int(round((f_numero - _d_entero) * 100))
        # print '_d_decimal : ',_d_decimal
        _n_contador_miles = 0
        _s_conversionALetras = ""
        while _d_entero > 0:
            _d_numeroATresCifras = _d_entero % 1000
            _s_letrasATresCifras = convierte_tresCifras(_d_numeroATresCifras, 1).strip() \
                if _n_contador_miles == 0 else convierte_tresCifras(_d_numeroATresCifras, 0).strip()

            if _d_numeroATresCifras == 0:
                _s_conversionALetras = _s_letrasATresCifras + " " + _s_conversionALetras

            elif _d_numeroATresCifras == 1:
                if _n_contador_miles in (1, 3):
                    _s_conversionALetras = _L_indicador[_n_contador_miles][0] + " " + _s_conversionALetras
                else:
                    _s_conversionALetras = _s_letrasATresCifras + " " + _L_indicador[_n_contador_miles][0] + " " \
                                           + _s_conversionALetras

            else:
                _s_conversionALetras = _s_letrasATresCifras + " " + _L_indicador[_n_contador_miles][1] + " " \
                                       + _s_conversionALetras

            _s_conversionALetras = _s_conversionALetras.strip()
            _n_contador_miles = _n_contador_miles + 1
            _d_entero = int(_d_entero / 1000)
        _s_conversionALetras = _s_conversionALetras + " con " + str(_d_decimal) + "/100"

        return _s_conversionALetras

    class DecimalEncoder(json.JSONEncoder):
        def default(self, o):
            if isinstance(o, Decimal):
                return str(o)
            else:
                pass
            return super(DecimalEncoder, self).default(o)

    pass  # STV


class STV_FWK_PERMISSIONS:
    """ Clase que define los permisos/botones/vistas.

    Attributes:
        _D_permissions: Lista de permisos/botones con el código string como key.
        _D_views: Lista de vistas con el código string como key.
        _D_granted: Lista de permisos otorgados una vez procesado el objeto con los permisos en la base de datos.

    """
        
    E_POSITION_UP = 0
    E_POSITION_DOWN = 1
    
    '''
    :var _D_permissions: Diccionario interna para el control de permisos, se utiliza lista para evitar repetir IDs 
    '''

    def __init__(self):
        self._D_permissions = Storage()
        self._D_views = Storage()
        self._D_granted = Storage() # Almacena los permisos por codigo, una vez aprovados
        self._s_application = ""
        self._s_controller = ""
        self._s_function = ""
                
        # Los IDs no deben modificarse ya que estan siendo utilizados en la base de datos
        self.btn_none = self._addPermission(("Menu"), 'menu', 'btn_none', 'fa-file-o', 0, "form", "")
        self.btn_new = self._addPermission(("Nuevo"), 'nuevo', 'btn_new', 'fa-file-o', 1, "button", "")
        self.btn_save = self._addPermission(("Grabar"), 'grabar', 'btn_save', 'fa-save', 2, "button", "formWithFind")
        self.btn_cancel = self._addPermission(("Cancelar"), 'cancelar', 'btn_cancel', 'fa-undo', 3, "button", "")
        self.btn_edit = self._addPermission(("Editar"), 'editar', 'btn_edit', 'fa-pencil-square-o', 4, "button", "")
        self.btn_view = self._addPermission(("Ver"), 'ver', 'btn_view', 'fa-eye', 10, "button", "")
        self.btn_print = self._addPermission(("Imprimir"), 'imprimir', 'btn_print', 'fa-print', 5, "button", "")
        self.btn_invalidate = self._addPermission(("Invalidar"), 'invalidar', 'btn_invalidate', 'fa-ban', 6, "button", "")
        self.btn_validate = self._addPermission(("Validar"), 'validar', 'btn_validate', 'fa-star-o', 7, "button", "")
        self.btn_suspend = self._addPermission(("Desactivar"), 'desactivar', 'btn_suspend', 'fa-thumbs-o-down', 8, "button", "")
        self.btn_activate = self._addPermission(("Activar"), 'activar', 'btn_activate', 'fa-thumbs-o-up', 16, "button", "")
        self.btn_remove = self._addPermission(("Borrar"), 'borrar', 'btn_remove', 'fa-times', 9, "button", "")
        # No soportado por ahora self.btn_restore = self._addPermission(("Recuperar"), 'recuperar', 'btn_restore', 'fa-plus', 17, "button")
        self.btn_find = self._addPermission(("Búsqueda filtrada"), 'buscar', 'btn_find', 'fa-search', 11, "buttonFind", "find")
        self.btn_findall = self._addPermission(("Buscar todo"), 'buscartodo', 'btn_findall', 'fa-asterisk', 12, "buttonFind", "find")
        self.btn_signature = self._addPermission(("Rastro"), 'rastro', 'btn_signature', 'fa-user-secret', 23, "button", "")

        self.btn_print_print = self._addPermission(("Imprimir"), 'imprimir', 'btn_print_print', 'fa-print', 25, "button", "form")
        self.btn_print_cancel = self._addPermission(("Cancelar Imprimir"), 'impcancelar', 'btn_print_cancel', 'fa-undo', 26, "button", "")
        self.btn_print_save = self._addPermission(("Guardar archivo"), 'impguardar', 'btn_print_save', 'fa-save', 27, "button", "form")
        self.btn_print_send = self._addPermission(("Enviar archivo"), 'impenviar', 'btn_print_send', 'fa-paper-plane', 28, "button", "form")
        self.btn_print_preview = self._addPermission(("Preview"), 'imppreview', 'btn_print_preview', 'fa-print', 29, "button", "form")
        self.form_print = self._addPermission(("Forma imprimir"), 'formaimprimir', 'form_print', 'fa-print', 30, "form", "")

        self.btn_searchcancel = self._addPermission(("Cancelar búsqueda"), 'busquedacancelar', 'btn_searchcancel', 'fa-eraser', 13, "button", "find")
        self.btn_searchdone = self._addPermission(("Terminar búsqueda"), 'busquedaterminar', 'btn_searchdone', 'fa-check', 14, "button", "find")
        #self.btn_searchselect = self._addPermission(("Seleccionar"), 'busquedaseleccionar', 'btn_searchselect', 'fa-magnet', 15, "button", "")
        self.form_search = self._addPermission(("Forma buscar"), 'formabuscar', 'form_search', 'fa-search', 24, "form", "")

        self.btn_findadvanced = self._addPermission(
            s_labelDefault = ("Búsqueda avanzada"), 
            s_code = "busquedaavanzada", 
            s_nameVar = 'btn_findadvanced', 
            s_icon = 'fa-search', 
            n_id = 330, 
            s_type = "buttonFind", 
            s_send = "find"
            )

        # Define los permisos de los botones
        self.L_formButtonsAll = [
            self.btn_none,
            self.btn_new,
            self.btn_save,
            self.btn_cancel,
            self.btn_edit,
            self.btn_view,
            self.btn_print,
            #self.btn_invalidate,
            #self.btn_validate,
            self.btn_suspend,
            self.btn_activate,
            self.btn_remove,
            #self.btn_restore,
            self.btn_find,
            self.btn_findall,
            self.btn_signature,
            ]
        
        self.L_formPrintButtons = [
            self.btn_none,
            self.btn_print_print,
            self.btn_print_cancel,
            self.btn_print_save,
            self.btn_print_send,
            ]
        
        self.L_formSearchButtons = [
            self.btn_none,
            self.btn_cancel,
            self.btn_view,
            self.btn_searchcancel,
            self.btn_searchdone,
            self.btn_find,
            # self.btn_findall,
            self.btn_signature,
            ]
        
        self.L_formButtonsMinimal = [
            self.btn_none,
            self.btn_cancel,
            self.btn_view,
            self.btn_find,
            self.btn_findall,
            self.btn_signature,
            ]

        self.L_formButtonsMinimalUpdate = [
            self.btn_none,
            self.btn_save,
            self.btn_cancel,
            self.btn_edit,
            self.btn_view,
            self.btn_find,
            self.btn_findall,
            self.btn_signature,
            ]
        
        # Los siguientes permisos pueden ser utilizados de forma única por cada forma, por lo que su uso debe
        # ser definido en la función que depliega la forma
        self.special_permisson_01 = self._addPermission(("Especial") + ' 1',  'especial1', 'special_permisson_01', 'fa-file-o', 18, "permission", "")
        self.special_permisson_02 = self._addPermission(("Especial") + ' 2',  'especial2', 'special_permisson_02', 'fa-file-o', 19, "permission", "")
        self.special_permisson_03 = self._addPermission(("Especial") + ' 3',  'especial3', 'special_permisson_03', 'fa-file-o', 20, "permission", "")
        self.special_permisson_04 = self._addPermission(("Especial") + ' 4',  'especial4', 'special_permisson_04', 'fa-file-o', 21, "permission", "")
        self.special_permisson_05 = self._addPermission(("Especial") + ' 5',  'especial5', 'special_permisson_05', 'fa-file-o', 22, "permission", "")
        
        self.view_emptyForm = self._addView('vacio', 'stv_en_emptyform', 'Forma vacía', 0,
                [
                    self.btn_new,
                    self.btn_print,
                    self.btn_find,
                    self.btn_findall,
                    self.btn_searchcancel,
                ], ["stv_rowSearchResults"])
        self.view_selectedRow = self._addView('selreg', 'stv_en_selectedrow', 'Forma de selección de un registro', 1,
                [
                    self.btn_new,
                    self.btn_edit,
                    self.btn_view,
                    self.btn_print,
                    self.btn_invalidate,
                    self.btn_validate,
                    self.btn_suspend,
                    self.btn_activate,
                    self.btn_remove,
                    #self.btn_restore,
                    self.btn_find,
                    self.btn_findall,
                    self.btn_signature,
                    self.btn_searchcancel,
                    self.btn_searchdone,
                ], ["stv_rowSearchResults"])
        self.view_selectedRows = self._addView('selregs', 'stv_en_selectedrows', 'Forma de selección de dos o mas registros', 2,
                [
                    self.btn_new,
                    self.btn_print,
                    self.btn_invalidate,
                    self.btn_validate,
                    self.btn_suspend,
                    self.btn_activate,
                    self.btn_remove,
                    #self.btn_restore,
                    self.btn_find,
                    self.btn_findall,
                    self.btn_searchcancel,
                    self.btn_searchdone,
                ], ["stv_rowSearchResults"])
        self.view_viewRegister = self._addView('ver', 'stv_en_viewregister', 'Forma Ver registro', 3,
                [
                    self.btn_cancel,
                    self.btn_edit,
                    self.btn_print,
                    self.btn_invalidate,
                    self.btn_validate,
                    self.btn_suspend,
                    self.btn_activate,
                    self.btn_remove,
                    #self.btn_restore,
                    self.btn_signature,
                    self.btn_searchcancel,
                    self.btn_searchdone,
                ], ["stv_rowContent", "stv_rowContentDetail"])
        self.view_editRegister = self._addView('editar', 'stv_en_editregister', 'Forma Editar registro', 4,
                [
                    self.btn_save,
                    self.btn_cancel,
                    self.btn_signature,
                ], ["stv_rowContent", "stv_rowContentDetail"])
        self.view_newRegister = self._addView('nuevo', 'stv_en_newregister', 'Forma Nuevo registro', 5,
                [
                    self.btn_save,
                    self.btn_cancel,
                ], ["stv_rowContent", "stv_rowContentDetail"])
        self.view_refresh = self._addView('refresca', 'stv_en_refreshResults', 'Forma Refresca la forma actual', 6,
                [], ["stv_rowSearchResults"])
        self.view_refreshParent = self._addView('refrescapadre', 'stv_en_refreshResultsParent', 'Forma Refresca la forma actual', 12,
                [], ["stv_rowSearchResults"])
        self.view_closeSearch = self._addView('cierrabus', 'stv_en_cierraBusqueda', 'Cierra la ventana de búsqueda', 10,
                [], [])
        self.view_print = self._addView(
            s_code = 'imprimir', 
            s_htmlClass = 'stv_en_print', 
            s_description = 'Forma para imprimir', 
            n_id = 11,
            L_permissionsDefault = [
                self.btn_print_print,
                self.btn_print_cancel,
                self.btn_print_save,
                self.btn_print_send,
                ], 
            L_rowsToShow = ["stv_rowContent", "stv_rowContentDetail"],
            E_buttonsBarPosition = self.E_POSITION_DOWN
            )
        self.view_customEvent1 = self._addView('custom1', 'stv_en_customEvent1', 'Forma custom 1', 7, [], [])
        self.view_customEvent2 = self._addView('custom2', 'stv_en_customEvent2', 'Forma custom 2', 8, [], [])
        self.view_customEvent3 = self._addView('custom3', 'stv_en_customEvent3', 'Forma custom 3', 9, [], [])
            
    def assignTables(self, dbReference):

        self._dbReference = dbReference
        
        self._dbF = self._dbReference.tformularios.with_alias('tF')
        self._dbFP = self._dbReference.tformulario_permisos.with_alias('tFP')
        self._dbM = self._dbReference.tmenus.with_alias('tM')
        self._dbMS = self._dbReference.tmenu_submenus.with_alias('tMS')
        self._dbU = self._dbReference.auth_user.with_alias('tU')
        self._dbG = self._dbReference.auth_group.with_alias('tG')
        self._dbGP = self._dbReference.auth_permission.with_alias('tGP')
        self._dbUG = self._dbReference.auth_membership.with_alias('tUG')

        
    def _addPermission(
            self, 
            s_labelDefault, 
            s_code, 
            s_nameVar, 
            s_icon, 
            n_id, 
            s_type, 
            s_send = ""
            ):
        ''' Crea un permiso y regresa un Storage con los datos de forma genérica
        :param str sLabelDefault: es el label por default a utilizar para el boton
        :param str sCode: es el código a utilizar en las llamdas al url, no debe contener caracteres especiales, ni espacios
        :param str sNameVar: es el nombre de la variable que se esta utilizando
        :return: regresa el diccionario con la construcción del permiso
        :rtype: int
        '''
        # TODO validar que code no tenga ni espacios ni caracteres especiales
        if n_id in self._D_permissions:
            100/0 # Crear un error para no repetir IDs
        
        _D_permisson = Storage(
            id = n_id, 
            label = s_labelDefault, 
            code = s_code, 
            icon = s_icon, 
            haspermisson = False, 
            description = s_labelDefault,
            varName = s_nameVar,
            s_type = s_type,
            s_send = s_send
            )
        self._D_permissions[n_id] = _D_permisson
        return (_D_permisson)

    def _addView(
            self, 
            s_code, 
            s_htmlClass, 
            s_description, 
            n_id, 
            L_permissionsDefault, 
            L_rowsToShow = None,
            E_buttonsBarPosition = None,
            L_actionsCreateView = None
            ):
        """ Crea una vista y le asocia sus parámetros.
        
        Args:
            s_code: Código string que se usa para asociar la vista.
            s_htmlClass: Clase html que se usa para agregar a los botones e identificar los que se habilitan o no en determinada vista.
            s_description: Descripción de la vista, no mayor uso.
            n_id: Id único para identificar a la vista
            L_permissionsDefault: Lista de permisos/botones por default en la vista. Pueden ser modificados a nivel STV_FWK_FORM.
            L_rowsToShow: Lista con los rows en la forma que pueden ser desplegados en la vista.
            E_buttonsBarPosition: Define si la barra de botones va en la parte superior o inferior.
            L_actionsCreateView: Lista con los códigos de los permisos/botones que crean la vista, los demás solamente actualizan su contenido.
            b_serverCreatesTab: Indica si el servidor crea el Div del tab o se crea por el cliente (browser)
            
        Returns:
            Diccionario con la información de la vista.
            
        """
        # TODO validar que code no tenga ni espacios ni caracteres especiales, y que el n_id no este duplicado
        if not E_buttonsBarPosition:
            E_buttonsBarPosition = self.E_POSITION_UP
        if not L_actionsCreateView:
            L_actionsCreateView = [self.btn_none.code]
        
        self._D_views[s_code] = Storage(
            n_id = n_id, 
            s_code = s_code, 
            s_htmlClass = s_htmlClass, 
            L_permissionsDefault = L_permissionsDefault,
            L_rowsToShow = L_rowsToShow,
            E_buttonsBarPosition = E_buttonsBarPosition,
            L_actionsCreateView = L_actionsCreateView,
            b_serverCreatesTab = False
            )
        return (self._D_views[s_code])

    def agregarPermisoAVista(
            self,
            O_permiso,
            O_vista
            ):
        if isinstance(O_vista, list) :
            _L_vistas = O_vista
        else:
            _L_vistas = [O_vista]
            
        for _x_vista in _L_vistas:
            if isinstance( _x_vista, ( str ) ) :
                _s_vista_codigo = _x_vista
            else:
                _s_vista_codigo = _x_vista.s_code
                
            self._D_views[_s_vista_codigo].L_permissionsDefault += [O_permiso]
        
        return

    def updateViewsFor_plus_form(
            self, 
            ):
        """ Modifica las vistas para su uso como AjaxForm.
        
        La idea de un AjaxForm es mostrar una forma de captura, con los botones abajo, donde las acciones
        Nuevo y Editar crean el tab con todos sus componentes y muestra inicialmente la forma de captura 
        con dos botones Cancelar y Grabar.
        
        Args:
            
        Returns:
            
        """
        for _s_viewcode in (self.view_newRegister.s_code, self.view_editRegister.s_code):
            self._D_views[_s_viewcode].E_buttonsBarPosition = self.E_POSITION_DOWN
            self._D_views[_s_viewcode].L_actionsCreateView = [
                self.btn_new.code,
                self.btn_edit.code
                ]
            self._D_views[_s_viewcode].b_serverCreatesTab = True
            
        self.btn_new.s_type = "form"
        self.btn_edit.s_type = "form"

        return ()
    
    def updateViewsFor_view_form(
            self, 
            ):
        """ Modifica las vistas para empezar la forma como view en vez de query.
                
        Args:
            
        Returns:
            
        """
        for _s_viewcode in (self.view_viewRegister.s_code,):
            self._D_views[_s_viewcode].L_actionsCreateView = [
                self.btn_view.code,
                ]

        return ()
        
    def getHtmlClassForViews(self, L_enabledByView):
        _s_htmlClassForViews = ""
        for _x_viewCode in L_enabledByView:
            if isinstance( _x_viewCode, ( str ) ) :
                if _x_viewCode in self._D_views:
                    _s_htmlClassForViews += self._D_view[s_viewCode].s_htmlClass + " "
                else:
                    _s_htmlClassForViews += _x_viewCode + "NOTFOUND "
            else:
                # Se supone que el elemento es el diccionario vista
                _s_htmlClassForViews += _x_viewCode.s_htmlClass + " "
        return _s_htmlClassForViews

    def getViewsByPermission(self, D_permission):
        _L_views = []
        for _s_key, _D_view in self._D_views.items():
            if D_permission in _D_view.L_permissionsDefault:
                _L_views.append(_D_view)
        return _L_views
    
    def getList(self):
        ''' Regresa la lista de permisos
        :return: lista de listas con dos elementos, el índice y el label
        :rtype: list of lists
        '''
        _L_return = []
        for _n_id, _D_permisson in self._D_permissions.items():
            _L_return.append([ _n_id, _D_permisson.label ])
        return _L_return

    def getDict(self):
        _D_return = {}
        for _n_id, _D_permisson in self._D_permissions.items():
            _D_return[_D_permisson.varName] = _D_permisson.code 
        return (_D_return)

    def getDict_ids(self):
        return (self._D_permissions)

    def getDict_varName(self):
        _D_return = {}
        for _n_id, _D_permisson in self._D_permissions.items():
            _D_return[_D_permisson.varName] = _D_permisson 
        return (_D_return)

    def getListFiltered(self, n_formulario_id, n_permiso_id):
        ''' Regresa la lista de permisos filtrados por id del formulario y agregando el id actual
        :param int formulario_id: id del formulario para filtrar resultados de permisos que no se hallan utilizado
        :param int n_permiso_id: id del permiso que se esté editando, para ser incluido en los resultados
        :return: lista de listas con dos elementos, el índice y el label
        :rtype: list of lists
        '''
        _L_dbRows = self._dbReference( (self._dbFP.formulario_id == n_formulario_id) & (self._dbFP.permiso_id != n_permiso_id) ).select(self._dbFP.permiso_id).as_list()
        _L_filter = []
        for _D_dbRow in _L_dbRows:
            _L_filter.append(_D_dbRow['permiso_id'])
        _L_permissonsValid = []
        for _id, _D_permisson in self._D_permissions.items():
            if (not _id in _L_filter):
                _L_permissonsValid.append([ _id, _D_permisson.label ])
            
        return _L_permissonsValid

    def getLabel(self, n_id):
        ''' Regresa el label asociado al indice especificado
        :param int index: indice a regresar su label asociado
        :return: regresa el label asociado al indice
        :rtype: str
        '''
        if isinstance( n_id, ( int, ) ) :
            return self._D_permissions[n_id].label
        else:
            print ("Error de permisos con indice ")+n_id
            return ('Nada')

    def _setPermisson(self, n_id, bPermiso, sDescription, s_icon):
        ''' Define el permiso para el indice especifico
        :param boolean bPermiso: define si tiene o no permiso para el index especificado
        '''
        if isinstance( n_id, ( int, ) ) and (n_id in self._D_permissions) :
            self._D_permissions[n_id].haspermisson = bPermiso
            self._D_permissions[n_id].description = sDescription
            # Si tiene un icono definido, remplaza el icono por default
            if s_icon:
                self._D_permissions[n_id].icon = s_icon
            self._D_granted[self._D_permissions[n_id].code] = self._D_permissions[n_id]
        else:
            print ("Error de permisos con indice ")+str(n_id)

    def getCodesWithPermisson(self):
        ''' Regresa una lista de los código que tiene permiso en True
        :return: regresa la lista
        :rtype: list 
        '''
        _L_codes = []
        if not self._D_granted:
            # No se tienen permisos
            pass
        else:
            for _s_key, _D_granted in self._D_granted.items():
                _L_codes.append(_s_key)
        return _L_codes

    def getListWithPermisson(self, b_ignorePermission = True, L_filterList = None):
        ''' Regresa una lista de los objectos (botones/especial) que tiene permiso en True
        :return: regresa la lista
        :rtype: list 
        '''
        _L_objects = []
        self._applyPermissons(
            b_ignorePermission, 
            L_filterList,
            ps_application = current.request.application,
            ps_controller = current.request.controller, 
            ps_function = current.request.function
            )
        for _D_filterPermission in L_filterList:
            if (True == b_ignorePermission) or (_D_filterPermission.code in self._D_granted):
                _L_objects.append(_D_filterPermission)
        return _L_objects
    
    def hasPermission(
            self, 
            x_code, 
            ps_application = None,
            ps_controller = None, 
            ps_function = None
            ):
        """Method that verifies if there is permission or not for actual app/controller/function.

        Args:
            x_code: Código usado para el permiso, puede ser string o dict

        Returns:
            True if has permission, False otherwise.

        """
        
        self._applyPermissons(
            False, 
            [], 
            ps_application = ps_application,
            ps_controller = ps_controller, 
            ps_function = ps_function
            )
        if isinstance( x_code, ( str ) ) :
            _D_permision = self._D_granted[x_code]
        else:
            # Se supone que el elemento es el diccionario
            _D_permision = self._D_granted[x_code.code]

        if _D_permision:
            _b_return = _D_permision.haspermisson
        else:
            _b_return = False
        return _b_return
        
        
    
    def _applyPermissons(
            self, 
            b_ignorePermission = True, 
            L_filterList = [],
            ps_application = '',
            ps_controller = '', 
            ps_function = ''
            ):
        ''' Busca en la base de datos por los permisos para el request actual '''
        # Si no a obtenido los permisos aún...
        if not(ps_application):
            ps_application = current.request.application
        else:
            pass
        if not(ps_controller):
            ps_controller = current.request.controller
        else:
            pass
        if not(ps_function):
            ps_function = current.request.function
        else:
            pass
        
        if (
            (self._s_application != ps_application)
            or (self._s_controller != ps_controller)
            or (self._s_function != ps_function)
            ):
            # ...obtener los permisos.
            if b_ignorePermission:
                for _D_filterElement in L_filterList:
                    # Aqui debe buscarse el permiso para agregar la descripcion y el icono definidos en la tabla y no usar los valores por default
                    self._setPermisson(_D_filterElement.id, True, _D_filterElement.label, False)
            else:
                                
                _dbRows_formulario = self._dbReference( 
                        # Busca el formulario que corresponde a la aplicacion, controlador y funcion del request.
                        ( self._dbF.aplicacion_w2p == ps_application )
                        & ( self._dbF.controlador_w2p == ps_controller )
                        & ( self._dbF.funcion_w2p == ps_function )
                        & (self._dbFP.formulario_id == self._dbF.id)
                        & ((self._dbGP.record_id == self._dbFP.id) & (self._dbGP.group_id.belongs(current.request.auth.user_groups.keys())))
                        ).select(
                                 # Selecciona los campos requeridos
                                 self._dbF.id, self._dbF.descripcion, #self._dbF.nombrebotones,
                                 self._dbFP.id, self._dbFP.descripcion, self._dbFP.permiso_id, self._dbFP.icono,
                                 self._dbGP.id, 
                                 
                                 )
                # Para cada row en los rows del formulario...
                for _dbRow in _dbRows_formulario:
                    # ...si tiene valores válidos para definir el permiso...
                    if _dbRow and _dbRow.get('tFP', None):
                        # ...graba true en el permiso si alguna de las memebresías tienen el permiso asociado.
                       self._setPermisson(_dbRow['tFP']['permiso_id'], (True if _dbRow['tGP']['id'] else False), _dbRow['tFP']['descripcion'], _dbRow['tFP']['icono'])
                       
                self._s_application = ps_application
                self._s_controller = ps_controller
                self._s_function = ps_function
        return


class STV_FWK_FORM:
    ''' Clase para manejar los catálogos '''

    class E_TIPO:
        NORMAL = 0  # Indica que la form se esta ejecutando de una manera nórmal, uso común
        MODAL = 1  # Inidica que la forma se esta ejecutando como un modal

    class E_ROWS:
        CONTENIDO = 0
        CONTENIDO_DETALLE = 1
        RESULTADOS = 2
        MODAL = 3
        PADRE_CONTENIDO = 4
        PADRE_CONTENIDO_DETALLE = 5
        PADRE_RESULTADOS = 6
        PADRE_MODAL = 7

    class ROWCONTENT:
        class E_EVENT:
            ON_VALIDATION = 'onValidation'
            ON_ACCEPTED = 'onAccepted'
            BEFORE_REMOVE = 'beforeRemove'
            AFTER_REMOVE = 'afterRemove'

        class E_PROPERTY:
            FORM_UPLOAD = 'formUpload'
            FORM_HIDDEN = 'formHidden'
            FORM_CONTROLUPLOAD = 'formControlUpload'
            MESSAGE = 's_message'
            ERROR = 's_error'
            ACTUALIZAR_VALORES_MAESTRO = 'D_actualizarValoresMaestro'

    class ROWSEARCHRESULTS:
        class PROPERTIES:
            LIMITBYSEARCH = 'limitBySearch'
            LIMITBYLAST = 'limitByLast'
            ORDERBY = 'orderBy'
            S_MESSAGE = 's_message'
            S_ERROR = 's_error'
            L_SELECTIDS = 'L_selectIDs'
            FN_ORDERBY = 'fn_orderby'
            L_LEFTJOINS = 'L_leftJoins'
            S_TYPE = 's_type'
            ODBQRY_HAVING = 'odbQry_having'
            D_HIDDEN = "D_hidden"
            b_DISTINCT = "b_distinct"

            class TYPE:
                S_GRID = "grid"
                S_TREE = "tree"


        class E_CONFIG:
            MULTISELECT = "multiselect"
            COLUMNS_BEGIN = "L_columnsBegin"
            COLUMNS_FIELDS = "L_columnsFields"
            COLUMNS_END = "L_columnsEnd"

    class E_COMANDOS:
        DUMMY = 0
        REFRESCAR_TABS_HERMANOS = 1  # Normalmente un detalle puede refrescar los tabs hermanos
        REFRESCAR_CAMPOS_MAESTRO = 2  # Refresca los campos de el form maestro. Lo mismo que la propiedad ACTUALIZAR_VALORES_MAESTRO
        REFRESCAR_CAMPOS_MAESTRO_2 = 3  # Refresca los campos de el form maestro 2nd nivel arriba.
        CERRAR_MODAL = 4  # Cierra ventana de modal.
        REFRESCAR_CAMPOS_BUSQUEDAAVANZADA = 5  # Refresca los campos de el form maestro 2nd nivel arriba.

    class ES_BEHAVIOR:
        S_TAB = "tab"  # Es el comportamiento normal de un tab del framework
        S_PLUS_FORM = "plus_form"  # Es el comportamiento de una forma ajax, para capturar datos con la funcionalidad de nuevo y edicion, grabar y cancelar
        S_MODAL = "modal"  # Es el comportamiento de una forma ajax modal, donde calcel cierra el modal
        S_TAB_ONLYFORM = "tab_onlyform"  # Es el comportamiento de una forma ajax, para capturar datos con la duncionalidad de nuevo y edicion, grabar y cancelar

    class ES_TYPECOLUMN:
        S_CHECKBOX = "checkbox"  # Despliega un check box en la columna
        S_LINK = "link"  # despliega un link en la columna
        S_LINKTAB = "linktab"  # despliega un link en la columna
        S_PLUS = "plus"  # despliega un +/- en la columna
        S_CUSTOM = "custom"  # columna totalmente configurable, puede usarse para hidden element en el grid
        S_CALCULATED = "calculated"
        S_FIELD = "field"
        S_FIELDVIRTUAL = "fieldvirtual"   

    class E_PRESENTACION:
        NO_DEFINIDO = 0
        FORMA = 1
        TABLA = 2

    @classmethod
    def TAB_INFORMATION(
        cls,
        s_url = "", 
        s_idHtml = None, 
        s_tabName = None, 
        s_type = "none"
        ):
        ''' Se genera la información necesaria para crear un tabpane
        :param s_url: especifica el url que se debe de executar para obtener los datos del tabpane, si esta vacío solamente agrega el tabpane
        :param s_idHtml: es un id unico para asignar al tabpane que contendrá la forma detalle
        :param s_tabName: es el nombre que describira al tabpane
        :param s_type: especifica el tipo de sub tab en la forma
            - detail: es tabla detalle, es necesario especificar el link a ejecutar
            - extended: es solo extención del maestro, su contenido se enviara con operacion save, la función regresara el id del div a utilizar para posicionar el contenido
            - none: es solo un sub tab
            
        :return regresa un diccionario con los datos para la creación del tabpane
        '''
        if (s_type == "detail"):
            _s_idDivToUse = s_idHtml
        else:
            _s_idDivToUse = s_idHtml + "_div"        
        _D_tabpane = Storage(
            s_url = s_url,
            s_idHtml = s_idHtml,
            s_tabName = s_tabName or ("Sin nombre"), 
            s_type = s_type,
            s_idDivToUse = _s_idDivToUse
            )
        return _D_tabpane        
    
    def __init__(
        self, 
        dbReference = None, 
        dbReferenceSearch = None,
        dbTable = None, 
        dbTableMaster = None, 
        xFieldLinkMaster = None, 
        s_tabId = "", 
        s_errorFunctionCode = 'Err',
        L_visibleButtons = None,
        L_fieldsToShow = None, 
        L_fieldsToHide = None,
        L_fieldsToGroupBy = None, 
        L_fieldsToSearch = None, 
        L_fieldsToSearchIfDigit = None,
        L_fieldsToAdvancedSearch = None,                  
        x_offsetInArgs = None, 
        b_preventDoubleUpdate = True, 
        b_applyPermissons = True,
        L_fieldnameIds = None, 
        Es_behavior = "tab"
        ):
        ''' Inicialización de la clase para manejo de Catálogos
        :param dbTable: referencia a la tabla principal donde se almacenarán los datos
        :param s_idHtmlForm: id de la forma que contendrá los datos, tiene que ser un id único
        :param s_errorFunctionCode: código que identifica a la función y a la forma en caso de presentarse errores, utilizado para el log
        :param L_fieldsToShow: lista con los campos a mostrar en el caso de búsquedas
        :param L_fieldsToHide: lista de campos a mostrar en forma oculta
        :param L_fieldsToGroupBy: opcional lista con los campos a para agrupar en caso de requerir agrupar registros en la búsqueda
        :param L_fieldsToSearch: lista de campos usados en las búsquedas
        :param L_fieldsToSearchIfDigit: lista de campos usados en las búsquedas en caso de búsquedas de números/digitos
        :param dbReference: referencia a la base de datos
        :param n_offsetInArgs: offset de argumentos sobre el que se buscara el id principal y el id maestro en caso de especificar tabla maestro
        :param b_preventDoubleUpdate: crea la lógica para prevenir actualizar un registro si este ya fue modificado, usando la ultima fecha/hora en que se modifico el registro y la fecha/hora en el se creo la forma
        '''
        self._db_ = dbReference
        self._dbReferenceSearch = dbReferenceSearch
        self._dbTable = dbTable
        #from stv_fwk_r0v2.cfg020_Widgets import stv_represent_number
        #self._dbTable.id.represent = stv_represent_number
        
        # Código usado para errores
        self._s_errorFunctionCode = s_errorFunctionCode
        # Se determinan los campos a mostrar en la búsqueda
        if not L_visibleButtons:
            L_visibleButtons = [] 
        self._L_fieldsToShow = L_fieldsToShow if L_fieldsToShow else [] 
        self._L_fieldsToHide = L_fieldsToHide if L_fieldsToHide else [] 
        self._L_fieldsToGroupBy = L_fieldsToGroupBy if L_fieldsToGroupBy else []
        self._L_fieldsToSearch = L_fieldsToSearch if L_fieldsToSearch else []
        self._L_fieldsToSearchIfDigit = L_fieldsToSearchIfDigit if L_fieldsToSearchIfDigit else []
        self._b_preventDoubleUpdate = b_preventDoubleUpdate
        self._b_applyPermissons = b_applyPermissons

        self._LD_referencialIntegrity = []  # Se usa para almacenar condiciones de integredad referencial cuando se requere en diferentes bases de datos
        self.D_permissionsConfig = Storage(L_order = []) # Variable accesible con la configuración de los botones
        if L_fieldnameIds:
            self._L_fieldnameIds = L_fieldnameIds
        elif self._dbTable:
            self._L_fieldnameIds = [self._dbTable.id]
        else:
            self._L_fieldnameIds = []

        self._dbTableMaster = None
        self._L_fieldMasterLink = [] # Lista con los nombres de los campos en la tabla que se usan como liga al maestro
        self._L_details = [] # Lista con la información de las tablas detalle de la forma
        self._L_errorsAction = [] # Lista de errores adicionales a mostrar

        self._D_tabOptions = Storage(
            b_tabModal = False,  # Genera la pantalla para búsquedas, con encabezado
            s_modalTitle = "",  # Título en caso de tabModal = True
            s_tabId = s_tabId,
            s_selectedIds = current.request.vars.get('stv_ed_selectedIds', False),
            s_search_edinput_id = current.request.vars.get('stv_search_edinput_id', ''),
            s_search_edinput_format = current.request.vars.get('stv_search_edinput_format', '{id} : {value}'),
            s_display = '',
            b_accepted = False, # Resultado de procesar la forma
            sfn_afterLoad = '', # Función JS a executar después de cargar la página
            Es_behavior = Es_behavior
            )
        
        # Si el behavior requerido es como forma ajax...
        if Es_behavior == self.ES_BEHAVIOR.S_PLUS_FORM:
            # ...se configuran los botones como forma ajax.
            current.request.stv_fwk_permissions.updateViewsFor_plus_form()
        else:
            # ...se mantienen los botones como se encuentren configurados
            pass

        # Variable que contendrá una lista de comandos y parámetros adicionales a realizar en el browser
        self._L_comandos = []

        self._D_rowContent = Storage()
        
        # TODO: en los eventos incluir parámetros adicionales, usar diccionario en ves de lista
        self._D_rowContent.D_events = Storage(
                                                    onValidation = [], # Funciones llamadas para validar un form antes de grabar 
                                                    onAccepted = [], # Funciones llamada una vez que se grabo correctamente el form
                                                    beforeRemove = [],
                                                    afterRemove = [],
                                                    )
        self._D_rowContent.D_properties = Storage(
                                                    formUpload = [], # Corresponde al parametro upload del SQLForm
                                                    formHidden = {}, # Corresponde al parameter hidden del SQLForm, es una manera de agregar elementos hidden en la form
                                                    formControlUpload = [], # Corresponde al control para manejo de uploads, contendra diccionario con dos elementos: [fieldUpload, fieldUploadFilename]
                                                    s_message = "",
                                                    s_error = "",
                                                    D_actualizarValoresMaestro = {},
                                                    )

        self._D_rowSearchResults = Storage()
        self._D_rowSearchResults.D_config = Storage(
            multiselect = None, # Funcionalidad que permite la selección múltiple de registros
            L_columnsBegin = [],
            L_columnsFields = [],
            L_columnsEnd = []
            )

        self._D_rowSearchResults.D_properties = Storage(
            limitBySearch = None,  # Propiedad usada en el select del search, primer numero indica la página y segundo la cantidad de resultados
            limitByLast = (0, 10),  # Propiedad usada en el select cuando regresa los ultimos registros modificados, primer numero indica la página y segundo la cantidad de resultados
            orderBy = None,  # Campo de la tabla por el que se hace el orden
            L_ordenGrid = [],  # Variable que contendra el orden requerido para el grid
            s_message = "",
            s_error = "",
            L_selectIDs = [], # Lista con los ids a seleccionar en la búsqueda, si es un storage debe tener field y value
            fn_orderby = "", # Función en JS para realizar ordenamientos complicados
            L_leftJoins = [], # Lista de consultas de tipo left join para la búsqueda
            s_type = STV_FWK_FORM.ROWSEARCHRESULTS.PROPERTIES.TYPE.S_GRID,
            odbQry_having = None,
            D_hidden = {},
            b_distinct = False
            )

        self._O_permissions = current.request.stv_fwk_permissions
            
        # Se calcula el inicio de los argumentos que identifican el manejo de la forma
        if x_offsetInArgs:
            self._n_offsetInArgs = x_offsetInArgs if isinstance( x_offsetInArgs, ( int, ) ) else len(x_offsetInArgs)
        else:
            self._n_offsetInArgs = 0
        
        if xFieldLinkMaster and dbTableMaster:
            self._dbTableMaster = dbTableMaster
            if isinstance(xFieldLinkMaster, (list, tuple)):
                _L_fieldMasterLink = xFieldLinkMaster
            else:
                _L_fieldMasterLink = [xFieldLinkMaster]
                
            for _x_fieldMaster in _L_fieldMasterLink:
                if isinstance( _x_fieldMaster, ( str ) ) :
                    self._L_fieldMasterLink += [_x_fieldMaster]
                else:
                    self._L_fieldMasterLink += [_x_fieldMaster.name]

        # Crear datos para uso de las posiciones
        self._D_args = self._helper_getArguments();
        
        if self._D_args.s_action in [self._O_permissions.form_search.code]:
            if not (self._O_permissions.btn_searchcancel in L_visibleButtons):                
                L_visibleButtons.append(self._O_permissions.btn_searchcancel)
            if not (self._O_permissions.btn_searchdone in L_visibleButtons):                
                L_visibleButtons.append(self._O_permissions.btn_searchdone)
            
        self._L_codesVisible = self._O_permissions.getListWithPermisson(not self._b_applyPermissons, L_visibleButtons)
        self._L_s_codesWithPermission = self._O_permissions.getCodesWithPermisson()

        # Si la acción corresponde a la forma de búsqueda
        
        if ((self._D_args.s_action not in [self._O_permissions.btn_none.code])
            and (current.request.vars.get('stv_tabid', None))):
            self._D_tabOptions.s_tabId = current.request.vars['stv_tabid']
        
        # Si la acción contiene alguna forma, definir la información para el modal
        if (self._D_args.s_action in [self._O_permissions.form_search.code]):
            self._D_tabOptions.b_tabModal = True
            self._D_tabOptions.s_tabId += "_search"
            self._D_tabOptions.s_selectedIds = self._D_args.s_rawIds
            self._D_tabOptions.s_modalTitle = "Buscar " + self._dbTable._singular
        elif (self._D_args.s_action in [self._O_permissions.form_print.code]):
            self._D_tabOptions.b_tabModal = True
            self._D_tabOptions.s_tabId += "_print"
            self._D_tabOptions.s_selectedIds = self._D_args.s_rawIds
        elif current.request.vars.get('stv_ed_selectedIds', None):
            self._D_tabOptions.s_selectedIds = current.request.vars['stv_ed_selectedIds']
            

        self._D_tabOptions.s_tabId_buttonsBar = self._D_tabOptions.s_tabId + "_btnsBar"
        self._D_tabOptions.s_tabId_main = self._D_tabOptions.s_tabId + "_main"

        # Para poder usar el id único en cada forma, en cada request, es necesario rescatar el nombre anterior si existe
        if ('_formname' in current.request.vars) \
                and (self._D_args.s_action == self._O_permissions.btn_save.code):
            self._D_tabOptions.s_tabId_form = current.request.vars['_formname']
        else:
            self._D_tabOptions.s_tabId_form = self._D_tabOptions.s_tabId + "_form"
        self._D_tabOptions.s_tabId_details = self._D_tabOptions.s_tabId + "_details"
        self._D_tabOptions.s_tabId_results = self._D_tabOptions.s_tabId + "_results"
        self._D_tabOptions.s_tabId_grid = self._D_tabOptions.s_tabId + "_grid"
        self._D_tabOptions.s_tabId_tree = self._D_tabOptions.s_tabId + "_tree"
        self._D_tabOptions.s_tabId_modal = self._D_tabOptions.s_tabId + "_modal"

        # Se aplican las configuraciones estandar a los botones para que puedan modificarse propiedades individuales de los botones
        self._configurePermissions()
        
        self.rowContent_formAdvSearch = None
        self._L_dbFields_formFactory_advSearch = []

    def agregar_comando(
            self,
            E_comando,
            D_parametros = None
            ):

        self._L_comandos.append(
            Storage(
                E_comando = E_comando,
                D_parametros = D_parametros
                )
            )
        return

    def addRowSearchResults_addColumnBegin(
            self, 
            es_typeColumn, 
            s_colname,
            s_tablename = "", 
            s_fieldname = "", 
            D_field = {}, 
            m_value = None, 
            m_classHtml = None,
            D_data = {},
            ):
        """
        Args:
            es_typeColumn: Es el tipo de columna, para su uso especial por el framework
            s_colname: Nombre que definirá la columna y el atribute field en los renglones, normalmente es tabla.campo
            s_tablename: Nombre de la tabla
            s_fieldname: Nombre del campo
            D_field: Debe incluir por lo menos
                type: string que define el tipo de campo
                name: nombre del campo
                represent: lambda de presentación del campo se manda el valor y el row
                comment: comentarios del campo
                label: nombre a desplegar por el campo, nombre de la columna
                readable: true si se quiere desplegar, false si se agrega como hidden
            m_value: Método a llamar para obtener el valor, se manda con la información de la columna, y el dbRow del grid
            m_classHtml: Método que se usa para determinar dinámicamente la clase de la columna y/o celda, se manda la columna y el dbRow. dbRow esta vación en caso del header
            D_data: Datos adicionales a agregar a los registros
        """
        D_field = Storage(D_field)
        _D_field = Storage(
            type = D_field.type or "string",
            name = D_field.name or s_fieldname,
            represent = D_field.represent or None, # Si es None, pone el valor tal como es
            comment = D_field.comment or "",
            label = D_field.label or "",
            readable = D_field.readable or False
            )
        
        self._D_rowSearchResults.D_config.L_columnsBegin.append(
            Storage(
                s_type = es_typeColumn,
                s_colname = s_colname or (s_tablename + "." + s_fieldname), 
                s_tablename = s_tablename, 
                s_fieldname = s_fieldname,
                D_field = _D_field,
                m_value = m_value,
                m_classHtml = m_classHtml,
                D_data = D_data
                )
            )
        return
    
    def addRowSearchResults_addColumnEnd(
            self, 
            es_typeColumn, 
            s_colname, 
            s_tablename, 
            s_fieldname, 
            D_field, 
            m_value = None, 
            m_classHtml = None,
            D_data = {},
            ):
        """
        Args:
            es_typeColumn: Es el tipo de columna, para su uso especial por el framework
            s_colname: Nombre que definirá la columna y el atribute field en los renglones, normalmente es tabla.campo
            s_tablename: Nombre de la tabla
            s_fieldname: Nombre del campo
            D_field: Debe incluir por lo menos
                type: string que define el tipo de campo
                name: nombre del campo
                represent: lambda de presentación del campo se manda el valor y el row
                comment: comentarios del campo
                label: nombre a desplegar por el campo, nombre de la columna
                readable: true si se quiere desplegar, false si se agrega como hidden
            m_value: Método a llamar para obtener el valor, se manda con la información de la columna, y el dbRow del grid
            m_classHtml: Método que se usa para determinar dinámicamente la clase de la columna y/o celda, se manda la columna y el dbRow. dbRow esta vación en caso del header
            D_data: Datos adicionales a agregar a los registros
        """
        _D_field = Storage(
            type = D_field.type or "string",
            name = D_field.name or s_fieldname,
            represent = D_field.represent or None, # Si es None, pone el valor tal como es
            comment = D_field.comment or "",
            label = D_field.label or "",
            readable = D_field.readable or False
            )
        
        self._D_rowSearchResults.D_config.L_columnsEnd.append(
            Storage(
                s_type = es_typeColumn,
                s_colname = s_colname or (s_tablename + "." + s_fieldname), 
                s_tablename = s_tablename, 
                s_fieldname = s_fieldname,
                D_field = _D_field,
                m_value = m_value,
                m_classHtml = m_classHtml,
                D_data = D_data
                )
            )
        return
    
    def _addRowSearchResults_addColumnFields(
            self,
            rowSearchResults_dbRows
            ):
        """
        Args:
            rowSearchResults_dbRows: Resultados de la búsqueda que definiran las columnas
        """
        for _dbFieldToShow in (self._L_fieldsToShow + self._L_fieldsToHide):
            _s_field = str(_dbFieldToShow)
            
            if isinstance(_dbFieldToShow, Field):
                _s_fieldType = self.ES_TYPECOLUMN.S_FIELD
                
            elif isinstance(_dbFieldToShow, Expression):
                
                _s_fieldType = self.ES_TYPECOLUMN.S_CALCULATED
                
                if not hasattr(_dbFieldToShow, 'label'):
                    if hasattr(_dbFieldToShow, 'second') and isinstance(_dbFieldToShow.second, str):
                        _dbFieldToShow.label = _dbFieldToShow.second.replace('_', ' ')
                    elif hasattr(_dbFieldToShow, 'first') and hasattr(_dbFieldToShow.first, 'label'):
                        _dbFieldToShow.label = _dbFieldToShow.first.label
                    else:
                        pass
                else:
                    pass
                # _s_field = _s_field.replace(" ", "_")
                # Probablemente _s_field debe ser el nombre despues de AS
                                
            elif isinstance(_dbFieldToShow, FieldVirtual):

                _s_fieldType = self.ES_TYPECOLUMN.S_FIELDVIRTUAL
                
            else:
                _s_fieldType = self.ES_TYPECOLUMN.S_FIELD

            self._D_rowSearchResults.D_config.L_columnsFields.append(
                Storage(
                    s_type = _s_fieldType,
                    s_colname = _s_field,
                    s_tablename = getattr(_dbFieldToShow, 'tablename', 'nodefinida'), 
                    s_fieldname = getattr(_dbFieldToShow, 'name', _s_field),
                    m_value = getattr(_dbFieldToShow, 'm_value', None),
                    D_field = _dbFieldToShow,
                    m_classHtml = getattr(_dbFieldToShow, 'm_classHtml', None),
                    D_data =  getattr(_dbFieldToShow, 'D_data', None),
                    )
                )
        
        return

    def addRowContentEvent(self, s_event, fn_callback, **D_adicionales):
        ''' Se definen callback en eventos específicos
        :param str sEvent: evento a asociar con el callback
        :param str fnCallback: funcion a mandar llamar al momento de que el evento ocurra
        '''
        if s_event in self._D_rowContent.D_events:
            self._D_rowContent.D_events[s_event].append(
                Storage(
                    fn_callback = fn_callback,
                    D_adicionales = D_adicionales
                    )
                )
        else:
            1/0 # Error, el evento no esta definido

    def bypassVars(self, s_nombreVar = "vars_request", **D_adicionales):
        """ Usa una variable en el request para agrupar en un json todas las variables del request inicial.

        @param s_nombreVar: nombre de la variable en el request
        @type s_nombreVar: string
        @return:
        @rtype:
        """
        if s_nombreVar in current.request.vars:
            _json_vars_request = current.request.vars[s_nombreVar]
        else:
            _D_variables = current.request.vars
            _D_variables.update(D_adicionales)
            _json_vars_request = simplejson.dumps(_D_variables, cls = STV_FWK_LIB.DecimalEncoder)

        self.addRowContentProperty(
            'formHidden',
            x_value = Storage({
                s_nombreVar: _json_vars_request
                })
            )

        self.addRowSearchResultsProperty(
            STV_FWK_FORM.ROWSEARCHRESULTS.PROPERTIES.D_HIDDEN,
            x_value = Storage({
                s_nombreVar: _json_vars_request
                })
            )

        return Storage(simplejson.loads(_json_vars_request))

    def addRowContentProperty(self, s_property, x_value):
        """ Se definen propiedades para definiciones en la clase
        @param sProperty: propiedad a especificar
        @param x_value: valor a definir a la propiedad
        """
        if s_property in self._D_rowContent.D_properties:
            if isinstance(self._D_rowContent.D_properties[s_property], dict):
                self._D_rowContent.D_properties[s_property].update(x_value)
            elif isinstance(self._D_rowContent.D_properties[s_property], list):
                if isinstance(x_value, list):
                    self._D_rowContent.D_properties[s_property] += x_value
                else:
                    self._D_rowContent.D_properties[s_property].append(x_value)
            else:
                self._D_rowContent.D_properties[s_property] = x_value
        else:
            1/0 # Error, la propiedad no esta definida
        return

    def updateTabOption(self, s_option, x_value):
        ''' Se actualiza el contenido de alguna opción del tab
        :param str s_option: propiedad a especificar
        :param str x_value: valor a definir a la propiedad
        '''
        if s_option in self._D_tabOptions:
            self._D_tabOptions[s_option] = x_value
        else:
            1/0 # Error, la propiedad no esta definida

    def addErrorAction(self,
                       s_msg,
                       s_errortype = "error",
                       s_fieldname = ""):
        self._L_errorsAction.append({'msg':s_msg, 'errortype': s_errortype, 'fieldname':s_fieldname})

    def addRowSearchResultsConfig(self, 
                                  s_feature, 
                                  b_enabled, 
                                  s_url = "", 
                                  m_classHTML = None, 
                                  s_jsFunc = "", 
                                  s_title="", 
                                  s_content = "", 
                                  s_hint = "", 
                                  dbField_reference = None,
                                  D_additionalData = Storage()):
        """ Agrega configuraciones usadas en los grid para uso funcional de cada renglón.
        
        Args:
            
            s_feature: Tipo de funcionalidad. Las opciones son:
                plus
                multiselect
                checkbox
                link
                link2
            b_enabled: Habilitada o no
            s_url: Url a mandar llamar, se agregará al url el id del registro seleccionado
            m_classHTML: Es una funcion lambda que se analiza en cada registro para saber si muestra la funcionalidad en el registro o no. Se manda el row y el feature
            s_jsFunc: Nombre de la función en javascript llamada cuando se da click en el boton de la funcionalidad
            s_title: 
            s_hint:
            dbField_reference: Referencia
            D_additionalData:
        """
        if (s_feature in self._D_rowSearchResults.D_config):
            self._D_rowSearchResults.D_config[s_feature] = Storage(b_enabled = b_enabled, 
                                                                s_url = s_url, 
                                                                m_classHTML = m_classHTML, 
                                                                s_jsFunc = s_jsFunc,
                                                                s_title = s_title,
                                                                s_content = s_content,
                                                                s_hint = s_hint,
                                                                dbField_reference = dbField_reference,
                                                                D_additionalData = D_additionalData)

    def addRowSearchResultsProperty(self, s_property, x_value):
        """ Se definen propiedades para definiciones en la clase
        :param str s_property: propiedad a especificar
        :param x_value: valor a definir a la propiedad
        """
        if s_property in self._D_rowSearchResults.D_properties:
            if isinstance(self._D_rowSearchResults.D_properties[s_property], dict):
                self._D_rowSearchResults.D_properties[s_property].update(x_value)
            elif isinstance(self._D_rowSearchResults.D_properties[s_property], list):
                if isinstance(x_value, list):
                    self._D_rowSearchResults.D_properties[s_property] += x_value
                else:
                    self._D_rowSearchResults.D_properties[s_property].append(x_value)
            else:
                self._D_rowSearchResults.D_properties[s_property] = x_value
        else:
            1/0 # Error, la propiedad no esta definida
        return

    def addRowSearchResultsLeftJoin(self, odbQryLeftJoin):
        ''' Se definen SQL left joins 
        :param str odbQryLeftJoin: query con el objeto join del tipo db.thing.on(...). Ejemplo: db.thing.on(db.person.id == db.thing.owner_id)
        '''
        if odbQryLeftJoin:
            self._D_rowSearchResults.D_properties.L_leftJoins.append(odbQryLeftJoin)
        else:
            1/0 # Error, left join no esta definido

    def setRowSearchResultsHaving(self, odbQry_having):
        ''' Se definen SQL left joins 
        :param str odbQryLeftJoin: query con el objeto join del tipo db.thing.on(...). Ejemplo: db.thing.on(db.person.id == db.thing.owner_id)
        '''
        if odbQry_having:
            self._D_rowSearchResults.D_properties.odbQry_having = odbQry_having
        else:
            1/0 # Error, left join no esta definido

    def _configurePermissions(self):
        
        for _D_codeVisible in self._L_codesVisible:
            
            _D_actionSwap = Storage()
            
            _L_viewsForPermission = self._O_permissions.getViewsByPermission(_D_codeVisible)
            
            # Si es el boton de suspender y; no esta definida la tabla o no tiene el campo de bansuspender...
            if ( ( (_D_codeVisible.code == self._O_permissions.btn_suspend.code)
                    or (_D_codeVisible.code == self._O_permissions.btn_activate.code) ) 
                and ( (not self._dbTable) 
                    or (not self._dbTable.get('bansuspender', False))) ):
                # ... no se agrega el boton y se va al siguiente permiso
                continue
            
            # Si se tiene la opción de visualizar el boton de firma (signature)...
            if (_D_codeVisible.code == self._O_permissions.btn_signature.code):
                if (not('creado_en' in self._dbTable.fields)
                    or not('creado_por' in self._dbTable.fields)
                    or not('editado_en' in self._dbTable.fields)
                    or not('editado_por' in self._dbTable.fields) ):
                    continue
             
            if (_D_codeVisible.code == self._O_permissions.btn_suspend.code):
                _D_actionSwap.s_field = str(self._dbTable['bansuspender'])
                _D_actionSwap.s_value = 'False'
                _D_actionSwap.s_default = 'True'
            elif (_D_codeVisible.code == self._O_permissions.btn_activate.code):
                _D_actionSwap.s_field = str(self._dbTable['bansuspender'])
                _D_actionSwap.s_value = 'True'
                _D_actionSwap.s_default = 'False'
            
            # Actualmente no existe la función de eliminado lógico, solamente eliminado físico
            if False:
                # Si es el boton de eliminar y; no esta definida la tabla o no tiene el campo de baneliminar...
                if ( ( (_D_codeVisible.code == self._O_permissions.btn_remove.code) )
                        #or (_D_codeVisible.code == self._O_permissions.btn_restore.code) )
                         
                    and ( (not self._dbTable)
                        or (not self._dbTable.get('baneliminar', False))) ):
                    # ... no se agrega el boton y se va al siguiente permiso
                    continue 
    
                if (_D_codeVisible.code == self._O_permissions.btn_suspend.code):
                    _D_actionSwap.s_field = 'baneliminar'
                    _D_actionSwap.s_value = 'true'
    
                if (_D_codeVisible.code == self._O_permissions.btn_activate.code):
                    _D_actionSwap.s_field = 'baneliminar'
                    _D_actionSwap.s_value = 'false'

                
            
            # Si el codigo visible es de un boton predefinido...
            if (_D_codeVisible in self._O_permissions.L_formButtonsAll):
                
                # ...agregar y configurar el boton.
                self.addPermissionConfig(
                    s_permissionCode = _D_codeVisible.code,
                    s_permissionLabel = _D_codeVisible.description,
                    s_permissionIcon = _D_codeVisible.icon,
                    s_type = _D_codeVisible.s_type,
                    s_send = _D_codeVisible.s_send,
                    s_actualizar = _D_codeVisible.s_actualizar,
                    L_enabledByView = _L_viewsForPermission,
                    D_actions = Storage(
                        js = Storage(
                            sfn_onPress = 'stv_fwk_button_press',
                            sfn_onSuccess = 'stv_fwk_button_success',
                            sfn_onError = 'stv_fwk_button_error',
                            ),
                        swap = _D_actionSwap,
                        ),
                    L_options = []
                    )
            # ...de lo contrario, si no es un boton (quiere decir que si es solo un permiso)...
            else:
                self.addPermissionConfig(
                    s_permissionCode = _D_codeVisible.code,
                    s_permissionLabel = _D_codeVisible.description,
                    s_permissionIcon = _D_codeVisible.icon,
                    s_type = _D_codeVisible.s_type,
                    s_send = _D_codeVisible.s_send,
                    L_enabledByView = _L_viewsForPermission,
                    D_actions = Storage( # Agregar un Storage vacío en action, para invalidarlo como boton
                        ),
                    L_options = []
                    )
        return      

    def addPermissionConfig(
            self,
            s_permissionCode,
            s_permissionLabel = "",
            s_permissionIcon = "",
            s_type = "",
            s_send = "",
            L_enabledByView = [],
            D_actions = {},
            L_options = [],
            s_actualizar = "",
            s_initialCode = ""
            ):
        """ Configura los botones en el tab
        @param s_initialCode: Es el código que determina los permisos. 
            En caso de ser nulo, utiliza s_permissionCode.
        """        
        if not s_initialCode:
            s_initialCode = s_permissionCode
        else:
            pass
        if s_initialCode in self._L_s_codesWithPermission:
            if self.D_permissionsConfig[s_initialCode]:
                _D_temp = Storage(self.D_permissionsConfig[s_initialCode].copy())
                
                self.D_permissionsConfig[s_initialCode] = Storage(
                    s_permissionCode = s_permissionCode or _D_temp.s_permissionCode,
                    s_permissionLabel = s_permissionLabel or _D_temp.s_permissionLabel,
                    s_permissionIcon = s_permissionIcon or _D_temp.s_permissionIcon,
                    s_type = s_type or _D_temp.s_type,
                    s_send = s_send or _D_temp.s_send,
                    s_actualizar = s_actualizar or _D_temp.s_actualizar,
                    L_enabledByView = L_enabledByView or _D_temp.L_enabledByView,
                    D_actions = D_actions or _D_temp.D_actions,
                    L_options = L_options or _D_temp.L_options
                    )
            else:
                self.D_permissionsConfig[s_initialCode] = Storage(
                    s_permissionCode = s_permissionCode,
                    s_permissionLabel = s_permissionLabel,
                    s_permissionIcon = s_permissionIcon,
                    s_type = s_type,
                    s_send = s_send,
                    s_actualizar = s_actualizar,
                    L_enabledByView = L_enabledByView,
                    D_actions = D_actions,
                    L_options = L_options
                    )
            
            if not s_initialCode in self.D_permissionsConfig.L_order:
                self.D_permissionsConfig.L_order.append(s_initialCode)
            else:
                pass 
   
    def addPermissionConfig_option(self, 
                s_permissionCode = "", 
                s_optionCode = "",   
                s_optionLabel = "",  
                s_optionIcon = "",   
                s_type = "",
                s_send = "",
                L_enabledByView = [],
                D_actions = Storage(),      
        ):
        if s_permissionCode in self._L_s_codesWithPermission:
            self.D_permissionsConfig[s_permissionCode].L_options.append( Storage(
                s_optionCode = s_optionCode,
                s_optionLabel = s_optionLabel,
                s_optionIcon = s_optionIcon,
                s_type = s_type,
                s_send = s_send,
                L_enabledByView = L_enabledByView,
                D_actions = D_actions,
                ) )
        
    def addSubTab(
            self,
            dbTableDetail = None,
            s_url = "",
            s_idHtml = None,
            s_tabName = None,
            s_type = "detail",
            ):
        """ Aqui se especifican todas los detalles que contiene la forma a través de un diccionario de dos datos

        @param s_class: class adicional para el elemento stv_tab
        @type s_class:
        @param dbTableDetail: referencia a la tabla de detalle
        @type dbTableDetail:
        @param s_url: especifica el url que se debe de executar para obtener los datos del detalle, si esta vacío
         solamente agrega el tab
        @type s_url:
        @param s_idHtml: es un id unico para asignar al tab detalle que contendrá la forma detalle
        @type s_idHtml:
        @param s_tabName:
        @type s_tabName:
        @param s_type: especifica el tipo de sub tab en la forma
            - detail: es tabla detalle, es necesario especificar el link a ejecutar
            - extended: es solo extención del maestro, su contenido se enviara con operacion save, la función regresara
             el id del div a utilizar para posicionar el contenido
            - none: es solo un sub tab, la función regresara el id del div a utilizar para posicionar el contenido
        @type s_type:
        @return:
        @rtype:
        """
        _s_idHtml = s_idHtml if s_idHtml else (self._D_tabOptions.s_tabId_form + str(len(self._L_details)))
        _D_tabInformation = self.TAB_INFORMATION(
            s_url = s_url,
            s_idHtml = _s_idHtml,
            s_tabName = s_tabName or dbTableDetail._plural or "Sin nombre",
            s_type = s_type,
            )
        self._L_details.append(_D_tabInformation)
        return _D_tabInformation.s_idDivToUse
        
    def addReferencialIntegrity_differentDatabases(self, dbFieldLinked):
        self._LD_referencialIntegrity.append( Storage(fieldLinked = dbFieldLinked) )
         
    def _btnRemove_helper_verifyRecord(self, fieldsReferenced, x_id, sErrorMsgTemplate):
        _L_errors = []            
        for _field in fieldsReferenced:
            if _field.ondelete == 'NO ACTION':
                _rows_referenced = _field.db( _field == x_id).select(_field._table.id)
                for _row_referenced in _rows_referenced:
                    if len(_L_errors) < 3:
                        _sErrorMsg = sErrorMsgTemplate + " %s [%s] " % (_field._table._singular, _row_referenced.id)
                        _L_errors.append({'msg':_sErrorMsg})
                    else:
                       _L_errors.append({'msg':'...'})
                       break
            elif _field.ondelete == 'CASCADE':
                _rows_referenced = _field.db( _field == x_id).select(_field._table.id)
                if _rows_referenced and _field._table._referenced_by:
                    for _row_referenced in _rows_referenced:
                        if len(_L_errors) < 3:
                            _sErrorDetail = sErrorMsgTemplate + " a detalle %s [%s] > " % (_field._table._singular, _row_referenced.id) + " con referencia"
                            _LerrorTemp = self._btnRemove_helper_verifyRecord(_field._table._referenced_by, _row_referenced.id, _sErrorDetail)
                            if _LerrorTemp:
                                _L_errors += _LerrorTemp
                        else:
                           _L_errors.append({'msg':'...'})
                           break
        if (len(self._LD_referencialIntegrity) > 0):
            for _D_referencial in self._LD_referencialIntegrity:
                _rows_referenced =  _D_referencial.fieldLinked.db( _D_referencial.fieldLinked == x_id ).select(_D_referencial.fieldLinked._table.id)
                for _row_referenced in _rows_referenced:
                    if len(_L_errors) < 3:
                        _sErrorMsg = sErrorMsgTemplate + " %s [%s] " % (_D_referencial.fieldLinked._table._singular, _row_referenced.id)
                        _L_errors.append({'msg':_sErrorMsg})
                    else:
                       _L_errors.append({'msg':'...'})
                       break
                
        return _L_errors
    
    def _btnRemove_helper_recordsInCascade(self, fieldsReferenced, x_id):
        for _field in fieldsReferenced:
            if _field.ondelete == 'CASCADE':
                _rows_referenced = _field.db( _field == x_id).select(_field._table.id)
                if _rows_referenced and _field._table._referenced_by:
                    for _row_referenced in _rows_referenced:
                        self._btnRemove_helper_recordsInCascade(_field._table._referenced_by, _row_referenced.id)
                        _row_referenced.delete_record()
        return
    
    def _btnSave_helper_callValidations(self, O_form):
        if self._D_rowContent.D_properties.formControlUpload:
            import os
            for _D_formControlUpload in self._D_rowContent.D_properties.formControlUpload:
                if not isinstance( O_form.vars[_D_formControlUpload.fieldUpload.name], ( str ) ) :
                    _s_filename = O_form.vars[_D_formControlUpload.fieldUpload.name].filename
                    O_form.vars[_D_formControlUpload.fieldUploadFilename.name] = os.path.basename(_s_filename.replace("\\", "/"))
                elif O_form.vars[_D_formControlUpload.fieldUpload.name] == "remove":
                    O_form.vars[_D_formControlUpload.fieldUploadFilename.name] = ""
                else:
                    # Si no tiene nada de texto, no se modifica nada
                    _D_formControlUpload.fieldUploadFilename.writable = False
                    _D_formControlUpload.fieldUpload.writable = False
        for _D_event in self._D_rowContent.D_events.onValidation:
            _D_event.fn_callback(O_form, **_D_event.D_adicionales)
            
        if not(O_form.errors):
            # Si esta por guardar el registro y tiene campos como writeable en false y no estan en los request,
            # se cambia readable para evitar que se intente de escribir el dato al momento de guardar la forma
            if (self._D_args.s_action in [self._O_permissions.btn_save.code,]):
                # Se previene ejecutar la protección si no se tiene tabla definida, previniendo problemas cuando
                # se usa una factory form
                if self._dbTable:
                    for _dbField in self._dbTable:
                        if (
                            (_dbField.name in O_form.vars)
                            and not(_dbField.writable)
                            and not(_dbField.name in current.request.vars)
                            and (_dbField.readable)
                            ):
                            del O_form.vars[_dbField.name]
                        else:
                            pass
                else:
                    pass
            else:
                pass
        else:
            pass

        return
    
    def _btnSave_helper_callAccepted(self, O_form):
        if self._D_rowContent.D_properties.formControlUpload:
            for _D_formControlUpload in self._D_rowContent.D_properties.formControlUpload:
                if "" == O_form.vars[_D_formControlUpload.fieldUploadFilename.name]:
                    _dbRow = self._dbTable(O_form.vars.id)
                    _dbRow[_D_formControlUpload.fieldUpload.name] = None
                    # TODO Borrar uploaded files
                    _dbRow.update_record()
        for _D_event in self._D_rowContent.D_events.onAccepted:
            _D_event.fn_callback(O_form, **_D_event.D_adicionales)
        return
    
    def _btnSave_helper_formValidation(self, O_form):
        if self._b_preventDoubleUpdate and (current.request.vars.get('stv_presented_on', None)) and O_form.record_id:
            _datetimePresentedOn = datetime.datetime.strptime(current.request.vars.get('stv_presented_on', None), "%Y-%m-%d %H:%M:%S.%f")
            if 'editado_en' in self._dbTable:
                _odbRecord = self._dbTable[O_form.record_id]
                if _datetimePresentedOn < _odbRecord.editado_en:
                    current.request.stv_flashError(
                        'Prevención de operación', 'El registro que intenta modificar pudo haber sido actualizado', 'swal')
                    O_form.errors.id = 'El registro que intenta modificar ya tiene cambios'

    def _helper_getArguments(self):
        ''' Tiene que ser llamada, despues de definir maetros y detalles
        '''
        _D_positionArgs = Storage(
            n_action = self._n_offsetInArgs if not self._dbTableMaster else (self._n_offsetInArgs + 2),
            n_id = (self._n_offsetInArgs + 1) if not self._dbTableMaster else (self._n_offsetInArgs + 3),
            n_actionMaster = self._n_offsetInArgs if self._dbTableMaster else None,
            n_idMaster = (self._n_offsetInArgs + 1) if self._dbTableMaster else None,
            )

        _n_minimum = _D_positionArgs.n_action
        _n_lenArgs = len(current.request.args)
        
        _D_args = Storage(s_action = self._O_permissions.btn_none,
                          s_actionMaster = None,
                          L_idMaster = [],
                          L_ids = [],
                          s_rawIds = "")
        if (_n_lenArgs >= _n_minimum):
            if (_n_lenArgs > _n_minimum):
                _D_args.s_action = current.request.args[_D_positionArgs.n_action]
                if (_n_lenArgs > _D_positionArgs.n_id):
                    _D_args.s_rawIds = current.request.args[_D_positionArgs.n_id]
                    _D_args.L_ids = STV_FWK_LIB.OBTENER_IDS(_D_args.s_rawIds)
                    # Se verifica si la cantidad de elementos en el id coincide con self._L_fieldnameIds
                    for _n_index, _x_id in enumerate(_D_args.L_ids):
                        # Si los ids necesarios son mayores que los ids en los argumentos, y hay ids en los argumentos
                        if (len(self._L_fieldnameIds) > (len(_D_args.L_ids[_n_index]))) and (len(_D_args.L_ids[_n_index]) > 0):
                            current.request.stv_flashError( self._dbTable._singular, ('No coinciden la cantidad de campos con los ids recibidos') )
                            _D_args = None
                            break
                        for _n_index2, _s_fieldIdValue in enumerate(_D_args.L_ids[_n_index]):
                            try:
                                _D_args.L_ids[_n_index][_n_index2] = int(_s_fieldIdValue)
                            except Exception:
                                _D_args.L_ids[_n_index][_n_index2] = None
                                
            else:
                _D_args.s_action = self._O_permissions.btn_none.code
                
            if self._dbTableMaster:
                _D_args.s_actionMaster = current.request.args[_D_positionArgs.n_actionMaster]
                if (len(self._L_fieldMasterLink) == 0):
                    current.request.stv_flashError( self._dbTable._singular, ('Existe relación de tabla maestro, pero campo relacionado no se ha especificado') )
                    _D_args = None
                elif (len(self._L_fieldMasterLink) > 1) and (not("-" in current.request.args[_D_positionArgs.n_idMaster])):
                    current.request.stv_flashError( self._dbTable._singular, ('Existe relación de tabla maestro, pero campos relacionados no coinciden con cantidad de Ids en argumentos') )
                    _D_args = None
                else:
                    if ("-" in current.request.args[_D_positionArgs.n_idMaster]):
                        _D_args.L_idMaster = current.request.args[_D_positionArgs.n_idMaster].split("-")
                    else:
                        _D_args.L_idMaster = [current.request.args[_D_positionArgs.n_idMaster]]
                    
                    if len(self._L_fieldMasterLink) != len(_D_args.L_idMaster):
                        current.request.stv_flashError( self._dbTable._singular, ('Cantidad incorrecta de campos ids con maestro y argumentos') )
                        _D_args = None
                
        else:
            # Hay un error en la cantidad de argumentos
            raise "Error en offset para argumentos"
            current.request.stv_flashError( self._dbTable._singular, ('Cantidad incorrecta de argumentos') )
            _D_args = None
        
        return (_D_args)
    
    @property
    def D_args(self):
        return self._D_args
    
    def getIDs(self):
        return self._D_args.L_ids

    def _process_helper_getMasterQry(self):
        # Se supone que las validaciones de idMaster en este punto ya quedaron
        _qryMaster = (True) # Se crea una condición siempre True para usar el for
        for _n_index, _s_fieldId in enumerate(self._L_fieldMasterLink):
             _qryMaster &= (self._dbTable[_s_fieldId] == self._D_args.L_idMaster[_n_index])
        return _qryMaster
    
    def _helper_getQueryId(self, x_id):
        _qry = (True) # Se crea una condición siempre True para usar el for
        for _n_index, _dbField in enumerate(self._L_fieldnameIds):
            if _dbField.table == self._dbTable:
                _qry &= (_dbField == x_id[_n_index])
            else:
                # Ignorar el campo si no existe en la tabla
                pass
        return _qry
    
    def _process_helper_getRow(self, x_id):
        _dbRow = None
        # ...es necesario dos argumentos adicionales, uno que diga la acción y otro que diga el id a Editar o Ver
        if (not x_id) :
            # ...se regresa un error.
            current.request.stv_flashError( self._dbTable._singular, ('Id de registro no especificado') )
            
        # ...de lo contrario si se cuenta con id...
        else:

            _qryRow = self._helper_getQueryId(x_id)
            
            if _qryRow:
            
                _qryMaster = self._process_helper_getMasterQry() # Si no hay maestro, siempre regresa true
                    
                _dbRows = self._db_(_qryRow & _qryMaster).select()
    
                if not _dbRows:
                    current.request.stv_flashError( self._dbTable._singular, ('Registro no encontrado para editar o ver') )
                elif (len(_dbRows) > 1):
                    current.request.stv_flashError( self._dbTable._singular, ('Se encontró más de un registro coincidente') )
                else:
                    _dbRow = _dbRows.last()
            else:
                _dbRow = None
                
        return _dbRow
    
    def createFactoryForm(
        self,
        L_formFactoryFields = [],
        dbRow_formFactory = None,
        s_factoryTableName = False,
        D_inputsHidden = []
        ):
        _O_rowContentForm = SQLFORM.factory(
                *L_formFactoryFields,
                record = dbRow_formFactory,
                table_name = s_factoryTableName or str(self._dbTable),
                buttons = [], 
                _id = self._D_tabOptions.s_tabId_form + "_" + (s_factoryTableName or str(self._dbTable)),
                readonly = (self._D_args.s_action == self._O_permissions.btn_view.code), 
                formstyle = 'bootstrap',
                upload = self._D_rowContent.D_properties.formUpload or None, 
                hidden = D_inputsHidden
                )
        return _O_rowContentForm
        
    def cfgRow_advSearch(
        self,
        L_dbFields_formFactory_advSearch = [],
        dbRow_formFactory_advSearch = None,
        D_hiddenFields_formFactory_advSearch = {},
        s_style = 'bootstrap',
        L_buttons = []
        ):
        
        self._L_dbFields_formFactory_advSearch = L_dbFields_formFactory_advSearch

        if True: #self._D_args.s_action in self._O_permissions.view_emptyForm.L_actionsCreateView:
            # Si la acción actual existe en las acciones para crear la vista
            if L_dbFields_formFactory_advSearch:
                # Si existen campos para las búsquedas avanzadas
                self.rowContent_formAdvSearch = SQLFORM.factory(
                    *L_dbFields_formFactory_advSearch,
                    record = dbRow_formFactory_advSearch,
                    table_name = str(self._dbTable) + "_" + self._O_permissions.btn_findadvanced.code,
                    buttons = L_buttons, 
                    _id = self._D_tabOptions.s_tabId_form + "_" + self._O_permissions.btn_findadvanced.code,
                    readonly = False, 
                    formstyle = s_style,
                    hidden = D_hiddenFields_formFactory_advSearch
                    )
            else:
                pass
        else:
            pass
        
        return

    def _configurar_orderBy(self):
        """ Se configura el ordenamiento para la consulta

        @return:
        @rtype:
        """

        _L_orderBy = []

        if current.request.vars.stv_ordencolumnas:
            # Si existe el ordenamiento desde el browser
            # Si se encuentra definido el orden en la variable stv_ordencolumnas, se usa
            # El orden llega como [[numCol, "asc"|"desc"], ...]
            _L_ordenesColumnas = simplejson.loads(current.request.vars.stv_ordencolumnas)
            for _L_ordenColumna in _L_ordenesColumnas:
                _n_columnasBegin = len(self._D_rowSearchResults.D_config.L_columnsBegin)
                if _n_columnasBegin <= _L_ordenColumna[0]:
                    _n_columnaAOrdenar = _L_ordenColumna[0] - _n_columnasBegin
                else:
                    _n_columnaAOrdenar = _n_columnasBegin
                if _L_ordenColumna[1] == "desc":
                    _L_orderBy.append(~self._L_fieldsToShow[_n_columnaAOrdenar])
                else:
                    _L_orderBy.append(self._L_fieldsToShow[_n_columnaAOrdenar])

        elif self._D_rowSearchResults.D_properties.orderBy:
            # Si tiene algo definido el orderBy, se usa eso
            if not isinstance(self._D_rowSearchResults.D_properties.orderBy, list):
                _L_orderBy.append(self._D_rowSearchResults.D_properties.orderBy)
            else:
                _L_orderBy += self._D_rowSearchResults.D_properties.orderBy

        elif self._dbTable.get('editado_en', None):
            # Si tiene el campo editado_en la tabla
            _L_orderBy.append(~self._dbTable.editado_en)

        else:
            # Se usa el id de la tabla
            _L_orderBy.append(~self._dbTable.id)

        _D_camposOrdenar = Storage()
        for _dbField_orden in _L_orderBy:
            if bool(_dbField_orden.op):
                # if _dbField_orden.op.im_func.func_name == "INVERT": # Python 2.6
                if _dbField_orden.op == _dbField_orden._dialect.invert:
                    # Indica que el orden es invertido
                    _D_camposOrdenar[str(_dbField_orden.first)] = 'desc'
                else:
                    _D_camposOrdenar[str(_dbField_orden)] = 'asc'
            else:
                _D_camposOrdenar[str(_dbField_orden)] = 'asc'

        _L_ordenParaGrid = []
        _n_columnasBegin = len(self._D_rowSearchResults.D_config.L_columnsBegin)
        for _n_index, _dbField in enumerate(self._L_fieldsToShow):
            if str(_dbField) in _D_camposOrdenar:
                _L_ordenParaGrid.append([_n_index + _n_columnasBegin, _D_camposOrdenar[str(_dbField)]])
            else:
                pass

        # Se configura el orden que verá el grid
        self._D_rowSearchResults.D_properties.L_ordenGrid = _L_ordenParaGrid

        if self._D_args.s_action == self._O_permissions.btn_findall.code:
            # En este caso el orden para el grid, debe ser diferente al orden de búsqueda,
            # en la búsqueda se localizan los últimos modificados, en el grid se respeta el filtro en el browser.
            _L_orderBy = []
            if self._dbTable.get('editado_en', None):
                # Si tiene el campo editado_en la tabla
                _L_orderBy.append(~self._dbTable.editado_en)

            else:
                # Se usa el id de la tabla
                _L_orderBy.append(~self._dbTable.id)

        else:
            pass

        return _L_orderBy

    def _configurar_limitBy(self):

        def paginaRows_enBrowser(n_limiteMinimoResultados):
            _s_paginaRows = current.request.vars.stv_paginarows
            if _s_paginaRows.isdigit():
                if int(_s_paginaRows) > n_limiteMinimoResultados:
                    _T_limitBy = (0, int(_s_paginaRows))
                else:
                    _T_limitBy = (0, n_limiteMinimoResultados)
            else:
                _T_limitBy = None
            return _T_limitBy

        _T_limitBy = (0, 10)

        if self._D_args.s_action == self._O_permissions.btn_findall.code:
            if current.request.vars.stv_paginarows:
                _T_limitBy = paginaRows_enBrowser(n_limiteMinimoResultados = 200)
            else:
                _T_limitBy = (0, 200)

        elif current.request.vars.stv_paginarows and (current.request.vars.stv_paginarows != '0'):
            _T_limitBy = paginaRows_enBrowser(n_limiteMinimoResultados = 0)

        elif self._D_rowSearchResults.D_properties.limitBySearch:
            _T_limitBy = self._D_rowSearchResults.D_properties.limitBySearch

        elif self._D_args.s_action == self._O_permissions.btn_none.code:

            if self._O_permissions.btn_find.code in self.D_permissionsConfig:
                pass

            elif self._O_permissions.btn_findall.code in self.D_permissionsConfig:
                _T_limitBy = None

            else:
                pass

        else:
            pass

        self._D_rowSearchResults.D_properties.limitBySearch = _T_limitBy

        return _T_limitBy

    def process(
            self,
            s_overrideCode = None,
            s_overrideTitle = None,
            s_overrideTitleSingular = None,
            D_overrideView = None,
            x_overrideQuery = None,
            b_skipAction = False,
            L_formFactoryFields = [],
            dbRow_formFactory = None,
            s_factoryTableName = False,
            ):
        
        _s_title = ""
        _s_overrideTitleSingular = ""
        if s_overrideTitleSingular:
            _s_overrideTitleSingular = s_overrideTitleSingular
        elif self._dbTable:
            _s_overrideTitleSingular = self._dbTable._singular
        else:
            _s_overrideTitleSingular = "registro"
        
        # Componente objeto forma SQLForm
        _O_rowContentForm = None
        # Componente objeto grid, resultado de una búsqueda en DAL
        _dbRows_rowSearchResults = None
        
        # Mensajes adicionales en caso de errores en la acción a realizar
        _L_errorsAction = self._L_errorsAction
        
        # This view is going to be updated by the code selected
        _D_useView = self._O_permissions.view_emptyForm
        
        # Si quiere sobreescribir la acción
        if self._D_args and s_overrideCode:
            self._D_args.s_action = s_overrideCode
        elif (self._D_args.s_action in [self._O_permissions.form_search.code, self._O_permissions.form_print.code]):
            self._D_args.s_action = self._O_permissions.btn_none.code
        elif current.request.extension == 'json':
            # Si se esta pidiendo un json, se sobrescribe la vista a find
            self._D_args.s_action = self._O_permissions.btn_findall.code
        else:
            # Usar la action generada por _processArguments
            pass
        
        # Objeto que contendra el registro a editar o ver            
        _dbRow = None
        
        # Si se tiene la opción de visualizar el boton de firma (signature)...
        if (self._O_permissions.btn_signature.code in self.D_permissionsConfig):
            # ...mostrar los campos necesarios como hidden si estan en la tabla.
            if ('creado_en' in self._dbTable.fields):
                self._L_fieldsToHide.append(self._dbTable.creado_en)
            if ('creado_por' in self._dbTable.fields):
                self._L_fieldsToHide.append(self._dbTable.creado_por)
            if ('editado_en' in self._dbTable.fields):
                self._L_fieldsToHide.append(self._dbTable.editado_en)
            if ('editado_por' in self._dbTable.fields):
                self._L_fieldsToHide.append(self._dbTable.editado_por)
            
        # Si puede ver el boton de suspender o activar, tiene derecho a ver la columna de suspender
        if (self._O_permissions.btn_suspend.code in self.D_permissionsConfig) or (self._O_permissions.btn_activate.code in self.D_permissionsConfig):
            self._dbTable.bansuspender.readable = True

        if self._b_preventDoubleUpdate:
            self.addRowContentEvent('onValidation', self._btnSave_helper_formValidation)
        
        # Si no se tienen los mínimos argumentos para el detalle...    
        if (not self._D_args) :
            # ...se regresa un error
            current.request.stv_flashError( _s_overrideTitleSingular, ('Error en argumentos') )
        
        # ...si no se tienen botones posibles...    
        elif (len(self.D_permissionsConfig) == 0):
            # ...se regresa un error
            current.request.stv_flashError( _s_overrideTitleSingular, ('No se cuenta con permisos suficientes para acceder a la forma: '+current.request.function) )
            
        # ...de lo contrario, si el argumento esta fuera de los botones visibles, regresa un error
        elif not(self._D_args.s_action in self.D_permissionsConfig) and self._b_applyPermissons and (not b_skipAction):
            # ...se regresa un error
            current.request.stv_flashError( _s_overrideTitleSingular, ('No se cuenta con permisos suficientes para la acción indicada: '+self._D_args.s_action+' en la forma '+current.request.function) + '; o offset esta mal configurado. ' )
        
        # ...de lo contratio si el argumento para el detalle es alguno de los de la lista...
        elif ( (self._D_args.s_action in [self._O_permissions.btn_new.code, 
                                     self._O_permissions.btn_save.code, 
                                     self._O_permissions.btn_edit.code, 
                                     self._O_permissions.btn_view.code])
                or (L_formFactoryFields and b_skipAction) ) :
            # ...realiza la acción correspondiente que utiliza la forma.

            # Si el argumento es Nuevo...  
            if self._D_args.s_action == self._O_permissions.btn_new.code:
                # ...el registro debe quedar limpio
                pass
            
            # ...de lo contrario, si el argumento es Editar, Ver o Salvar debe de tener un id el registro...
            elif (self._D_args.s_action in [self._O_permissions.btn_edit.code, 
                                         self._O_permissions.btn_view.code, 
                                         self._O_permissions.btn_save.code]):
                
                if (not b_skipAction):
                    if (len(self._D_args.L_ids) > 1):
                        current.request.stv_flashError( _s_overrideTitleSingular, ('Se esta tratando de editar, ver o guardar mas de un registro') )
                    elif (self._D_args.L_ids):
                        _dbRow = self._process_helper_getRow(self._D_args.L_ids[0])
                        
                    # Se actualiza la información para los widgets que requieran referencia hacia el _dbRow
                    current.request.stv_fwk_recordForWidgets = _dbRow
                                
            for _n_index, _s_fieldId in enumerate(self._L_fieldMasterLink):
                # Si existe un valor válido en los argumentos, se actualizan los defaults de la tabla para fijar el padre
                if self._D_args.L_idMaster[_n_index]:
                    self._dbTable[_s_fieldId].default = self._D_args.L_idMaster[_n_index]
                else:
                    pass
            
            _D_inputsHidden = self._D_rowContent.D_properties.formHidden if self._D_rowContent.D_properties.formHidden else {}
            # No usar el form hidden, ya que el framework en stv_tab_content los despliega
            #if self._L_fieldsToHide and _dbRow:
            #    for _dbField in self._L_fieldsToHide:
            #        _D_inputsHidden.update({_dbField.name: _dbRow[_dbField.name]})
            #        _s_represent = _dbField.represent(_dbRow[_dbField.name], _dbRow)
            #        _D_inputsHidden.update({_dbField.name + "_represent": xmlescape(_s_represent)})
                                
            if self._b_preventDoubleUpdate:
                if (
                    current.request.vars.get('stv_presented_on', None)
                    and (self._D_args.s_action == self._O_permissions.btn_save.code)
                    ):
                    _D_inputsHidden.update({'stv_presented_on': current.request.vars.get('stv_presented_on', None)})
                else:
                    _D_inputsHidden.update({'stv_presented_on': current.request.browsernow})
            
            # Al momento solo se permite editar ver y guardar un registro con un id
            if self._D_args.L_ids and (self._D_args.s_action not in [self._O_permissions.btn_new.code]):
                _D_inputsHidden.update({'stv_id': self._D_args.s_rawIds})
            else:
                _D_inputsHidden.update({'stv_id': ''})

            current.response.stv_E_presentacion = STV_FWK_FORM.E_PRESENTACION.FORMA
            if ('_formname' in current.request.vars) \
                    and (self._D_args.s_action == self._O_permissions.btn_save.code):
                self._D_tabOptions.s_tabId_form = current.request.vars['_formname']
            else:
                pass

            if L_formFactoryFields:
                _O_rowContentForm = SQLFORM.factory(
                        *L_formFactoryFields,
                        record = dbRow_formFactory,
                        table_name = s_factoryTableName or str(self._dbTable),
                        buttons = [], 
                        _id = self._D_tabOptions.s_tabId_form,
                        readonly = (self._D_args.s_action == self._O_permissions.btn_view.code), 
                        formstyle = 'bootstrap',
                        upload = self._D_rowContent.D_properties.formUpload or None, 
                        hidden = _D_inputsHidden
                        )
            else:
                _L_camposADesplegar = []
                for _s_campo in self._dbTable.fields:
                    if self._dbTable[_s_campo].writable \
                            or self._dbTable[_s_campo].readable:
                        _L_camposADesplegar.append(_s_campo)
                    else:
                        pass

                # Se crea la forma o se da seguimiento a la última acción, haciendo uso de la estructura de Web2Py
                _O_rowContentForm = SQLFORM(
                        self._dbTable, 
                        _dbRow,
                        fields = _L_camposADesplegar,
                        buttons = [], 
                        _id = self._D_tabOptions.s_tabId_form, 
                        readonly = (self._D_args.s_action == self._O_permissions.btn_view.code), 
                        formstyle = 'bootstrap',
                        upload = self._D_rowContent.D_properties.formUpload or None, 
                        hidden = _D_inputsHidden,
                        )
            
            # Si el proceso de la forma es aceptado por Web2Py... 
            if _O_rowContentForm.process(
                        formname = self._D_tabOptions.s_tabId_form, 
                        onvalidation = self._btnSave_helper_callValidations,
                        dbio=not b_skipAction,
                        ).accepted:
                # ...significa que se grabo correctamente, y se regresa a la pantalla de búsqueda con el grid de resultados.
                
                self._btnSave_helper_callAccepted(_O_rowContentForm)
                
                current.request.stv_flashSuccess(_s_overrideTitleSingular, 'Aceptado')
                
                # Se asigna la vista despues de aceptar la operación para actualizar la búsqueda
                _D_useView = self._O_permissions.view_refresh
                
                self._D_tabOptions.b_accepted = True
                
            # ...de lo contrario, si hay errores...
            elif _O_rowContentForm.errors:
                # ...se genera la misma forma con descripción de los errores, usando la estructura de web2py para la presentación de los errores
                current.request.stv_flashError(_s_overrideTitleSingular, 'Se encontraron errores durante la operación')

                # Si el campo no es writable o no existe para el error, se agrega el error al id para mostrarlo
                _L_errores = []
                for _s_campoConError in _O_rowContentForm.errors:
                    if (
                            self._dbTable
                            and (_s_campoConError in self._dbTable)
                            and self._dbTable[_s_campoConError].writable
                            ):
                        pass
                    else:
                        _L_errores.append(_O_rowContentForm.errors[_s_campoConError])

                if _L_errores:
                    _O_rowContentForm.errors.id = "\n".join(_L_errores)
                else:
                    pass

                # Se asigna la vista despues de aceptar la operación para actualizar la búsqueda
                if self._D_args.L_ids:
                    _D_useView = self._O_permissions.view_editRegister
                else:
                    _D_useView = self._O_permissions.view_newRegister
            
            # ...de lo contrario...
            else:
                # Se asigna la vista despues de aceptar la operación para actualizar la búsqueda
                if self._D_args.L_ids:
                    _D_useView = self._O_permissions.view_editRegister
                else:
                    _D_useView = self._O_permissions.view_newRegister

            if self._D_args.s_action == self._O_permissions.btn_edit.code:
                _s_title = P(('Editando ') + _s_overrideTitleSingular)
                _D_useView = self._O_permissions.view_editRegister
            elif self._D_args.s_action == self._O_permissions.btn_new.code:
                _s_title = P(('Agregando ') + _s_overrideTitleSingular)
                _D_useView = self._O_permissions.view_newRegister
            elif self._D_args.s_action == self._O_permissions.btn_view.code:
                _s_title = P(('Viendo ') + _s_overrideTitleSingular)
                _D_useView = self._O_permissions.view_viewRegister
            elif self._D_args.s_action == self._O_permissions.btn_save.code:
                _s_title = P(('Guardando ') + _s_overrideTitleSingular)
                # En el caso de save ya se definió la vista 
            
        # ...de lo contrario si el argumento es Cancelar
        elif (self._D_args.s_action == self._O_permissions.btn_cancel.code):
            # ...regresa a la pantalla de búsqueda con el grid de resultados.
            _D_useView = self._O_permissions.view_emptyForm
            pass
        
        # ...de lo contrario si el argumento es Reporte
        elif (self._D_args.s_action == self._O_permissions.btn_print.code):
            # ...por definir
            pass
                
        # ...de lo contratio si el argumento para el detalle es alguno de Eliminar o Suspender
        # que también pudiera ser Restaurar o Reactivar en su defecto
        elif (self._D_args.s_action in [
                        self._O_permissions.btn_suspend.code, 
                        self._O_permissions.btn_remove.code, 
                        self._O_permissions.btn_activate.code, 
                        #self._O_permissions.btn_restore.code,
                        ]):
            # ...es necesario dos argumentos adicionales, uno que diga la acción y otro que diga el id a Suspender o Eliminar...
            
            if not b_skipAction:
                _L_errorsAction = []
                for _x_id in self._D_args.L_ids:
                    _dbRow = self._process_helper_getRow(_x_id)

                    _L_errorsAction_row = []
                    if _dbRow:
                    
                        # Control para hacer la eliminación del registro
                        if self._D_args.s_action == self._O_permissions.btn_remove.code:
                            
                            for _D_event in self._D_rowContent.D_events.beforeRemove:
                                if "dbRow" in _D_event.D_adicionales:
                                    _D_event.D_adicionales['dbRow'] = _dbRow
                                else:
                                    pass
                                _L_errorsAction_row = _D_event.fn_callback(_L_errorsAction_row, **_D_event.D_adicionales)
                                
                            if not _L_errorsAction_row:
                            
                                # Se hace el proceso manual de integridad referencial
                                _L_errorsAction_row += self._btnRemove_helper_verifyRecord(self._dbTable._referenced_by, _dbRow.id, ("Existe referencia"))
                                if not _L_errorsAction_row:
                                    try:
                                        self._btnRemove_helper_recordsInCascade(self._dbTable._referenced_by, _dbRow.id)
                                        _dbRow.delete_record()
                                        for _D_event in self._D_rowContent.D_events.afterRemove:
                                            _L_errorsAction_row = _D_event.fn_callback(_L_errorsAction_row, **_D_event.D_adicionales)
                                        if 'stv_fwk_dblog' in current.request:
                                            current.request.stv_fwk_dblog.tlogs.insert(notas = str(_dbRow.as_json()) + '; sql=' + str(self._dbTable._db._lastsql))
                                    except self._dbTable._db._adapter.driver.IntegrityError as error:
                                        if 'stv_fwk_dblog' in current.request:
                                            current.request.stv_fwk_dblog.tlogs.insert(notas = str(_dbRow.as_json()) + '; sql=' + str(error))
                                        _L_errorsAction_row.append({'msg':xmlescape(str(error))})
                                        current.request.stv_flashError(_s_overrideTitleSingular, ('Problemas al intentar borrar'), s_mode = "swal" )
                                    else:
                                        current.request.stv_flashError(_s_overrideTitleSingular, ('Borrado satisfactoriamente'), s_mode = "swal" )
                                        pass
                            else:
                                # Existen errores en los callbacks del usuario por lo que no se puede borrar
                                pass
                        else:
                            # Control para hacer toggle en caso de suspender o activar
                            if self._D_args.s_action == self._O_permissions.btn_suspend.code:
                                _dbRow.bansuspender = True 
                            elif self._D_args.s_action == self._O_permissions.btn_activate.code:
                                _dbRow.bansuspender = False
                            #elif self._D_args.s_action == self._O_permissions.btn_restore.code:
                            #    _dbRow.baneliminar = False 
                            else:
                                current.request.stv_flashError(_s_overrideTitleSingular, ('Comando no especificado') )
                                pass
                            _dbRow.update_record()
                    else:
                        _L_errorsAction_row.append({'msg': 'No se encontró el registro %s'%str(_x_id)})
                        pass

                    _L_errorsAction += _L_errorsAction_row
            else:
                #current.request.stv_flashError(_s_overrideTitleSingular, ('Borrado satisfactoriamente'), s_mode = "swal" )
                # No hacer nada si se salta la acción
                pass
            _D_useView = self._O_permissions.view_refresh
        
        # ...en caso contrario si se requiere hacer una búsqueda, o no se manda nada en la acción...
        elif (
                (self._D_args.s_action in [
                        self._O_permissions.btn_find.code, 
                        self._O_permissions.btn_findall.code, 
                        self._O_permissions.btn_none.code] 
                    and self._dbTable) ):
            # ...se realiza la búsqueda y se regresan los resultados.

            # Para permitir la modificación de los botones a mostrar normalmente
            _s_title = ("Resultados de búsqueda")
            
            if (x_overrideQuery):
                
                _dbRows_rowSearchResults = x_overrideQuery
                
            else:

                _L_busqLimites = (0, 10)
                if current.request.vars.stv_paginarows:
                    _s_paginaRows = current.request.vars.stv_paginarows
                    if _s_paginaRows.isdigit():
                        _L_busqLimites = (0, int(_s_paginaRows))
                    else:
                        _L_busqLimites = None
                else:
                    pass

                #_T_limitby = self._D_rowSearchResults.D_properties.limitBySearch

                _subqry_search = (False)
                _subqry_ids = (False)
                # Se cambia la referencia para la búsqueda
                _dbReference = self._dbReferenceSearch or self._db_
                                    
                # Si no tiene argumentos, o el argumento es nada...            
                if (
                        not self._L_dbFields_formFactory_advSearch
                        and (self._D_args.s_action in [self._O_permissions.btn_none.code])
                        ):
                
                    # ...y si se tiene opción de ver el campo de búsqueda...
                    if (self._O_permissions.btn_find.code in self.D_permissionsConfig):
                        # ...se deben regresar cero registros en la búsqueda.
                        _subqry_search = (True)
                        #_T_limitby = self._D_rowSearchResults.D_properties.limitByLast or (0,10)
                    # ...de lo contrario si tiene permiso de buscar todo
                    elif (self._O_permissions.btn_findall.code in self.D_permissionsConfig):
                        _subqry_search = (True)
                    else:
                        pass
                # ...de lo contrario, si presiono algun boton buscarTodo...
                elif (self._D_args.s_action in [self._O_permissions.btn_findall.code]):
                    _subqry_search = (True)
                # ...de lo contrario si presion buscar o cualquier otra cosa...
                else:
                    # ...realizar la búsqueda.

                    _subqry_advSearch = True
                    if self._L_dbFields_formFactory_advSearch:

                        for _dbField in self._L_dbFields_formFactory_advSearch:

                            if _dbField.table \
                                    and (self._O_permissions.btn_findadvanced.code in _dbField.table._tablename):
                                # Si esta definida la tabla, y se es el código de búsqueda avanzada,
                                # no se usa en esta parte del query
                                pass

                            elif not _dbField.table and (_dbField.name not in self._dbTable) :
                                # Si es un campo sin tabla definida y que no existe en la tabla actual,
                                # no se usa en esta parte del query
                                pass

                            elif current.request.vars.get(_dbField.name, None) is not None:
                                _x_valorCampo = current.request.vars.get(_dbField.name)
                                _dbField_mapeado = self._dbTable[_dbField.name]

                                if str(_dbField_mapeado.type) in ('date', 'datetime'):
                                    if isinstance(_x_valorCampo, list):
                                        _L_fechas = _x_valorCampo
                                        # Se obtiene el formato a usar para decodificar la fecha, definido en el requires
                                        if isinstance(_dbField_mapeado.requires, (IS_DATE, IS_DATETIME)):
                                            _s_format = _dbField_mapeado.requires.format

                                        elif isinstance(_dbField_mapeado.requires.other, (IS_DATE, IS_DATETIME)):
                                            _s_format = _dbField_mapeado.requires.other.format

                                        else:
                                            _s_format = "%d/%m/%Y"
                                        
                                        if (len(_L_fechas) == 2) and _L_fechas[0] and _L_fechas[1]:
                                            _subqry_advSearch &= \
                                                (_dbField_mapeado
                                                    >= datetime.datetime.strptime(_L_fechas[0], _s_format)) \
                                                & (_dbField_mapeado
                                                    <= datetime.datetime.strptime(_L_fechas[1], _s_format))

                                        else:
                                            _L_qry_fechas = []
                                            for _fecha in _L_fechas:
                                                if _fecha:
                                                    _L_qry_fechas.append(
                                                        _dbField_mapeado
                                                        == datetime.datetime.strptime(_fecha, _s_format)
                                                        )
                                                else:
                                                    pass

                                            if _L_qry_fechas:
                                                _subqryfechas = False
                                                for _qryFecha in _L_qry_fechas:
                                                    _subqryfechas |= _qryFecha
                                                _subqry_advSearch &= _subqryfechas
                                                
                                    else:
                                        _subqry_advSearch &= \
                                            _dbField_mapeado \
                                            == datetime.datetime.strptime(
                                                current.request.vars.get(_dbField.name),
                                                _dbField_mapeado.requires.format
                                                )

                                elif _x_valorCampo not in ("null", ""):
                                    _s_tipoBaseDelCampo = str(_dbField_mapeado.type).split('(')[0]
                                    if _s_tipoBaseDelCampo in ('integer', 'double', 'decimal'):
                                        if isinstance(_x_valorCampo, str) and ("," in _x_valorCampo):
                                            # Si el tipo del campo es integer, y se esta evaluando un str con comas,
                                            #  significan muchas opciones seleccionadas
                                            _subqry_multiple = False
                                            for _s_opcion in _x_valorCampo.split(","):
                                                _subqry_multiple |= _dbField_mapeado == _s_opcion

                                        elif isinstance(_x_valorCampo, list) and (len(_x_valorCampo) == 2):
                                            _subqry_multiple = True

                                            if _x_valorCampo[0] not in ('', None):
                                                _subqry_multiple &= _dbField_mapeado >= _x_valorCampo[0]
                                            else:
                                                pass

                                            if _x_valorCampo[1] not in ('', None):
                                                _subqry_multiple &= _dbField_mapeado <= _x_valorCampo[1]
                                            else:
                                                pass

                                        else:
                                            # No se identifico un caso especial
                                            _subqry_multiple = (_dbField_mapeado == _x_valorCampo)

                                    else:
                                        # No se identifico un caso especial
                                        _subqry_multiple = (_dbField_mapeado == _x_valorCampo)

                                    _subqry_advSearch &= _subqry_multiple
                                else:
                                    pass
                            else:
                                pass
                    else:
                        pass
                        
                    # Si no existe edbuscar o esta vacio...
                    if not current.request.vars.get('stv_ed_find', None):
                        # ...regresa los ultimos 10 renglones modificados
                        _subqry_search = (True)
                        
                    # ...en caso contrario...
                    else:
                        # ...utiliza el filtro edbuscar en los campos definidos.
                        # Si el argumento de búsqueda es un número...
                        if current.request.vars.get('stv_ed_find', 'ABC').isdigit():
                            # ...se busca en campos numéricos.
                            _L_fieldsToFilterBy = self._L_fieldsToSearchIfDigit
                        else:
                            # ...de lo contrario, se busca en cualquier otro campo.
                            _L_fieldsToFilterBy = self._L_fieldsToSearch
                            
                        # Se agrega al string the búsqueda un caracter comodín
                        _s_strToFind = current.request.vars.get('stv_ed_find', '') + '%'
                        
                        # Se genera la query con los datos a buscar                
                        for _dbField in _L_fieldsToFilterBy:
                            _subqry_search |= (_dbField.like(_s_strToFind))
                    
                    _subqry_search = (_subqry_search) & (_subqry_advSearch)
                                                            
                # Si es una petición json y existen ids
                if (current.request.extension == "json") and (len(self._D_args.L_ids) > 0):
                    for s_id in self._D_args.L_ids:
                        _subqry_ids |= self._helper_getQueryId(s_id)
                elif (self._D_tabOptions.s_selectedIds):
                    if ("IDS_" in self._D_tabOptions.s_selectedIds):
                        _L_searchIds = self._D_tabOptions.s_selectedIds.split("_")[1:]
                    else:
                        _L_searchIds = [self._D_tabOptions.s_selectedIds]
                    
                    for _s_id in _L_searchIds:
                        _subqry_ids |= self._dbTable.id == _s_id
                
                _subqry = (_subqry_search) | _subqry_ids
                
                # Si tiene derecho al boton restore, se permite ver registros borrados lógicamente
                #if (self._O_permissions.btn_restore.code not in self.D_permissionsConfig) and self._dbTable.get('baneliminar', None):
                #    _subqry = (_subqry) & (self._dbTable.baneliminar == False)

                # Si no esta modificando el detalle...
                if (not self._dbTableMaster) or (not self._L_fieldMasterLink):
                    # ...el query es tal cual el subqry
                    _qry = _subqry
                else:
                    # ...en caso contrario, el qry incluye el filtro del id de la tabla maestro
                    _qryMaster = self._process_helper_getMasterQry()
                    _qry = (_qryMaster) & (_subqry)

                _L_fieldsToShow = self._L_fieldsToShow + self._L_fieldsToHide
                _L_dbFieldsToShow = []
                for _dbField in _L_fieldsToShow:
                    if isinstance(_dbField, (Field, Expression)):
                        _L_dbFieldsToShow += [_dbField]
                    else:
                        pass

                if not self._L_fieldsToGroupBy:
                    # Se realizar la búsqueda
                    _dbRows_rowSearchResults = _dbReference(
                        _qry,
                        ignore_common_filters = True
                        ).select(
                            *_L_dbFieldsToShow,
                            orderby = self._configurar_orderBy(),
                            limitby = self._configurar_limitBy(),
                            left = self._D_rowSearchResults.D_properties.L_leftJoins,
                            distinct = self._D_rowSearchResults.D_properties.b_distinct
                            )
                else:
                    _L_fieldsToGroupBy = self._L_fieldsToGroupBy + self._L_fieldsToHide
                    _dbRows_rowSearchResults = _dbReference(
                        _qry,
                        ignore_common_filters = True
                        ).select(
                            *_L_dbFieldsToShow,
                            orderby = self._configurar_orderBy(),
                            limitby = self._configurar_limitBy(),
                            groupby = _L_fieldsToGroupBy,
                            left = self._D_rowSearchResults.D_properties.L_leftJoins,
                            having = self._D_rowSearchResults.D_properties.odbQry_having
                            )

                current.response.stv_E_presentacion = STV_FWK_FORM.E_PRESENTACION.TABLA
                # Se agragan las columnas para despliegue de resultados
                self._addRowSearchResults_addColumnFields(_dbRows_rowSearchResults)
            
            # if (x_overrideQuery): else:
            
        elif (self._D_args.s_action in [
                        self._O_permissions.btn_searchdone.code, 
                        ]):

            # Se prueba el poder seleccionar varios registros a la vez
            # if (len(self._D_args.L_ids) > 1):
            #    current.request.stv_flashError( _s_overrideTitleSingular, ('Solamente puede seleccionarse un registro') )
            # el
            if (self._D_args.L_ids):
                
                self._D_tabOptions.s_selectedIds = str(self._D_args.L_ids[0][0])
                if (hasattr(self._dbTable._format, '__call__')):                
                    # ...ejecuta la función y regresa el resultado.
                    _s_refRequire = self._dbTable._format(self._dbTable(self._D_tabOptions.s_selectedIds))
                else:
                    # ...de lo contrario haz el formato.
                    _s_refRequire = self._dbTable._format % self._dbTable(self._D_tabOptions.s_selectedIds).as_dict()
                    
                self._D_tabOptions.s_display = self._D_tabOptions.s_search_edinput_format.format(id = self._D_tabOptions.s_selectedIds, value = _s_refRequire)
            
                _D_useView = self._O_permissions.view_closeSearch
        
        else:
            pass
                                    
        if D_overrideView:
            _D_useView = D_overrideView
        else:
            pass
            
        # Definir el título en la ventana.
        if s_overrideTitle:
            _s_title = s_overrideTitle
        else:
            pass

        current.response.flash = ""
        
        self._L_fieldnamesIds_str = []
        for _dbField in self._L_fieldnameIds:
            if isinstance(_dbField, Field):
                self._L_fieldnamesIds_str += [str(_dbField)]
            elif isinstance(_dbField, Expression):
                self._L_fieldnamesIds_str += [str(_dbField.second)]
            else:
                self._L_fieldnamesIds_str += [str(_dbField)]
        
        # Regresa las variables necesarias para generar la vista
        return Storage(
                    # Título de la forma
                    s_title = _s_title,
                    # Objeto tipo SQLFrom con la forma a desplegar en caso de insertar, modificar o ver
                    rowContent_form = _O_rowContentForm,
                    # Resultado de búsqueda con los registros a mostrar
                    rowSearchResults_dbRows = _dbRows_rowSearchResults,
                    # Campos a ocultar en la grid o en la form, y no mostrarlos en el grid
                    L_fieldsToHide = self._L_fieldsToHide,
                    # Diccionario con los permisos utilizados en la form
                    D_permissionsConfig = self.D_permissionsConfig,
                    # Diccionario con el contenido de información de la vista, incluyendo los botones a mostrar
                    D_useView = _D_useView,
                    # String con un URL limpio para re-utilizarlo en las acciones de la form
                    s_cleanUrl = URL( a = str(current.request.application), c = str(current.request.controller), f = str(current.request.function), args= current.request.args[:(self._n_offsetInArgs if not self._dbTableMaster else (self._n_offsetInArgs + 2))]),
                    # Lista de diccionarios con información de las form detalle para subllamadas
                    L_details = self._L_details,
                    # String con la última acción requerida en la form y a la cual se esta respondiendo
                    s_actionRequested = self._D_args.s_action,
                    # Lista con descripción de los errores obtenidos en la última acción
                    L_errorsAction = _L_errorsAction,
                    # Referencia a la tabla principal usada en la form
                    dbTable = self._dbTable,
                    # Registro de la tabla que se esta visualizando o editando
                    dbRecord = _dbRow,
                    # Diccionario con la configuración y eventos de las funcionalidades en la form
                    D_rowContent = self._D_rowContent,
                    # Diccionario con la configuración y eventos de las funcionalidades del grid
                    D_rowSearchResults = self._D_rowSearchResults,
                    # Lista de nombres de campos usados para IDs
                    L_fieldnameIds = self._L_fieldnamesIds_str,
                    # Storage con las opciones de la forma pls
                    D_tabOptions = self._D_tabOptions,
                    # Resultado para búsquedas avanzadas
                    rowContent_formAdvSearch = self.rowContent_formAdvSearch,
                    L_comandos = self._L_comandos
                    )
        
''' Clase para crear Tree en base a una consulta SQL '''
class DB_Tree:
    def __init__(self, dbRows):
        self._db_rows = dbRows
        self._Ls_levelTables = []
        self._Ls_levelFields = []

    def addLevel(self, L_dbTables):
        _L_s_dbTables = []
        for _dbTable in L_dbTables:
            _L_s_dbTables.append(str(_dbTable))
        self._Ls_levelTables.append(_L_s_dbTables)

    def _generateTree(self, dbRows, LsTablesForLevel):
        _n_groupById = 0
        _db_subRows = []
        _D_dataTree = Storage()
        _L_result = []
        
        # Si el argumento tiene niveles válidos...
        if ((len(LsTablesForLevel) > 0) and (len(LsTablesForLevel[0]) > 0)):
            # ...comienza el proces de generación del Tree
            
            # Se almacena la referencia a la lista con las tablas en el nivel actual
            _L_tableLevel = LsTablesForLevel[0]
            
            # Si tiene dos niveles adicionales significa que tiene un nivel más para el tree y el resto son 
            # los últimos registros; y si tiene también al menos una tabla definida para el siguiente nivel...
            if ((len(LsTablesForLevel) > 1) and (len(LsTablesForLevel[1]) > 0)):
                _L_tableNextLevel = LsTablesForLevel[1]
            else:
                _L_tableNextLevel = None
            
            # Para cada row en los resultados del select...
            for _db_row in dbRows:
                # ...si el ultimo valor de agrupamiento es diferente al contenido del campo id de la primer tabla...
                if (_n_groupById != _db_row[_L_tableLevel[0]]['id']):
                    
                    # ...si ya tiene datos para el tree...
                    if _D_dataTree:
                        # ...guarda los datos en el resultado
                        
                        # Si existen más niveles...
                        if _L_tableNextLevel:
                            # ...llama recursivamente a esta función para generar el tree al siguiente nivel.
                            _db_subRows = self._generateTree(_db_subRows, LsTablesForLevel[1:])
                        
                        # Guarda los resultados
                        _L_result.append(
                            Storage(
                                data = _D_dataTree,
                                subrows = _db_subRows
                                )
                            )
                    
                    # Limpia los subRows
                    _db_subRows = []
                    
                    # Guarda el ID utilizado como referencia para los registros del nivel
                    _n_groupById = _db_row[_L_tableLevel[0]]['id']
                    
                    # Guarda el contenido de los datos del nivel del tree
                    _D_dataTree = Storage()
                    # Para cada nombre de table en la lista con las tablas para el nivel del tree...
                    for _s_table in _L_tableLevel:
                        # ...almacena el contenido de forma directa del row en el dataTree
                        _D_dataTree[_s_table] = _db_row[_s_table]
                    
                else:
                    # ...de lo contrario si coincide el id no hacer nada, se supone que la información en las tablas
                    # del nivel actual del tree es la misma y ya esta almacenada en dataTree
                    pass
                
                # Si existen más niveles...
                if _L_tableNextLevel:
                    # ...agrega el row a subRows para la generación del siguiente nivel del tree después.
                    _db_subRows.append(_db_row)
                else :
                    # ...de lo contrario, guarda la información con el contenido de las tablas en el último nivel
                    #_D_subDataTemp = {}
                    #for _s_table in LsTablesForLevel[1] :
                    #    _D_subDataTemp[_s_table] = _db_row[_s_table]
                    #_db_subRows.append(_D_subDataTemp)
                    pass
                    
                
                 
            # En caso de ser el último registro del nivel y ya tener definido el contenido de grupo en dataTree...
            if _D_dataTree:
                # ...guardar los datos en el resultado
                
                # Si existen más niveles...
                if _L_tableNextLevel:
                    # ...llama recursivamente a esta función para generar el tree al siguiente nivel.
                    _db_subRows = self._generateTree(_db_subRows, LsTablesForLevel[1:])
                
                # Guarda los resultados
                _L_result.append(
                    Storage(
                        data = _D_dataTree,
                        subrows = _db_subRows
                        )
                    )
        
        return _L_result
        
    def process(self):
        return self._generateTree(self._db_rows, self._Ls_levelTables)
            
    def process_groupby(self, s_fieldPadre):
        '''Regresa un diccionario de diccionarios con los elementos de la tabla
        
        Agrupa elementos de la tabla en base al campo definido como padre, y el id
        '''
        
        #Primero se almacena la información en un diccionario
        _D_dbRows_byId = Storage()
        _D_dbRows_byId['raiz'] = []
        for _dbRow in self._db_rows:
            if _dbRow[s_fieldPadre]:
                _s_idPadre = str(_dbRow[s_fieldPadre])
            else:
                _s_idPadre = 'raiz'
                
            if _s_idPadre in _D_dbRows_byId:
                _D_dbRows_byId[_s_idPadre].append(_dbRow)
            else:
                _D_dbRows_byId[_s_idPadre] = [_dbRow]
            
        
        return _D_dbRows_byId


class STV_FWK_CRYPTO:
    
    # 
    # Common utility functions and constants for cryptography use.
    # 
    # Copyright (c) 2018 Project Nayuki. (MIT License)
    # https://www.nayuki.io/page/cryptographic-primitives-in-plain-python
    # 
    # Permission is hereby granted, free of charge, to any person obtaining a copy of
    # this software and associated documentation files (the "Software"), to deal in
    # the Software without restriction, including without limitation the rights to
    # use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
    # the Software, and to permit persons to whom the Software is furnished to do so,
    # subject to the following conditions:
    # - The above copyright notice and this permission notice shall be included in
    #   all copies or substantial portions of the Software.
    # - The Software is provided "as is", without warranty of any kind, express or
    #   implied, including but not limited to the warranties of merchantability,
    #   fitness for a particular purpose and noninfringement. In no event shall the
    #   authors or copyright holders be liable for any claim, damages or other
    #   liability, whether in an action of contract, tort or otherwise, arising from,
    #   out of or in connection with the Software or the use or other dealings in the
    #   Software.
    # 
    
    UINT32_MASK = (1 << 32) - 1  # 0xFFFF FFFF
    UINT64_MASK = (1 << 64) - 1  # 0xFFFF FFFF FFFF FFFF
    KEY0 = 0
    KEY1 = 1
    KEY2 = 2
    KEY3 = 3
    KEY4 = 4
    KEY5 = 5
    KEY6 = 6
    KEY7 = 7
    KEY8 = 8
    KEY9 = 9
    
    
    def _init(self):
        
        pass

    
    
    # 'value' must be uint32, 'amount' must be in the range [0, 32), and the result is uint32.
    def rotate_left_uint32(self, n_value, n_amount):
        if not(0 <= n_value <= UINT32_MASK):
            n_value = 0
        if not(0 <= n_amount < 32):
            n_amount = 0
        return ((n_value << n_amount) | (n_value >> (32 - n_amount))) & UINT32_MASK
    
    
    # 'value' must be uint32, 'amount' must be in the range [0, 32), and the result is uint32.
    def rotate_right_uint32(self, n_value, n_amount):
        if not(0 <= n_value <= UINT32_MASK):
            n_value = 0
        if not(0 <= n_amount < 32):
            n_amount = 0
        return ((n_value << (32 - n_amount)) | (n_value >> n_amount)) & UINT32_MASK
    
    
    # 'value' must be uint64, 'amount' must be in the range [0, 64), and the result is uint64.
    def rotate_left_uint64(self, n_value, n_amount):
        if not(0 <= n_value <= UINT64_MASK):
            n_value = 0
        if not(0 <= n_amount < 64):
            n_amount = 0
        return ((n_value << n_amount) | (n_value >> (64 - n_amount))) & UINT64_MASK
    
    
    # 'value' must be uint64, 'amount' must be in the range [0, 64), and the result is uint64.
    def rotate_right_uint64(self, n_value, n_amount):
        if not(0 <= n_value <= UINT64_MASK):
            n_value = 0
        if not(0 <= n_amount < 64):
            n_amount = 0
        return ((n_value << (64 - n_amount)) | (n_value >> n_amount)) & UINT64_MASK

    # For example: asciistr_to_bytelist("0Az") -> [48, 65, 122].
    def string_to_bytelist(self, s_data):
        return list(map(ord, s_data))

    # For example: bytelist_to_string([48, 65, 122]) -> "0Az".
    def bytelist_to_string(self, L_byte):
        return "".join("{}".format(_n_element) for _n_element in L_byte)

    # For example: hexstr_to_bytelist("FF00C0") -> [255, 0, 192].
    def hexstr_to_bytelist(self, s_hexstr):
        _L_bytes = []
        if not(len(s_hexstr) % 2 == 0):
            s_hexstr += '0'
        for i in range(0, len(s_hexstr), 2):
            _L_bytes.append(int(s_hexstr[i : i + 2], 16))
        return _L_bytes
    
    # For example: bytelist_to_hexstr([255, 0, 192]) -> "FF00C0".
    def bytelist_to_hexstr(self, L_byte):
        return "".join("{:02X}".format(_n_element) for _n_element in L_byte)
    
    def __encrypt_decrypt_data(self, s_salt, n_key, s_data):
        import math
        
        __s_keys = [
            "xwasFycBUhIUzggf", # 0
            "n1rCkXNeajz7OekX", # 1
            "4o4HZ3VCEtzFIr5B", # 2
            "3l4ShAl9Qw3Lmar1", # 3
            "ES5Q6IJXAK6IQOq9", # 4
            "Qdbn3BHLg9fv3DXq", # 5
            "JVvZLJJS7W9x5WG2", # 6
            "fwhLB0xQKarp1KXW", # 7
            "eXduUAz8Cfr3F2Kq", # 8
            "fJVbohtnTG6Lpc9z", # 9
            ]

        # Obtrener una lista de códigos unicode de los strings
        __L_bytesKey = self.string_to_bytelist(__s_keys[n_key])
        __L_bytesSalt = self.string_to_bytelist(s_salt)
        __L_bytesData = self.string_to_bytelist(s_data)
        
        # Se duplican la salt y la key para tener mínimo el mismo tamaño que los datos
        __L_bytesKey *= 1 + int(math.ceil(len(__L_bytesData) / len(__L_bytesKey)))
        __L_bytesSalt *= 1 + int(math.ceil(len(__L_bytesData) / len(__L_bytesSalt)))
        
        # Se encriptan los datos
        __L_encryptedData = []
        for i in range(0, len(__L_bytesData), 1):
            __L_encryptedData.append( __L_bytesKey[i] ^ __L_bytesSalt[i] ^ __L_bytesData[i] )
        
        return __L_encryptedData
    
    def encrypt(self, s_salt, n_key, s_data):
        """ Encripta información en base a una clave, un valor inicial y los datos
        
        Example:
            _O_crypto = STV_FWK_CRYPTO()
            _secure_claveciec = _O_crypto.encrypt(_s_salt, STV_FWK_CRYPTO.KEY1, _s_data)
        
        
        """
        
        return self.bytelist_to_hexstr(self.__encrypt_decrypt_data(s_salt, n_key, s_data))
    
    def decrypt(self, s_salt, n_key, s_encrypteddata):
        """ Des encripta información
        
        Example:
            _s_decrypt_claveciec = _O_crypto.decrypt(_s_salt, STV_FWK_CRYPTO.KEY1, _s_secure_data)

        """

        return "".join(chr(_n_element) for _n_element in self.__encrypt_decrypt_data(s_salt, n_key, hexstr_to_bytelist(s_encrypteddata)))
        
    def test(self):
        message = "3243F6A8885A308D313198A2E0370734"
        key = "2B7E151628AED2A6ABF7158809CF4F3C"
        
        plaintextbytelist = cryptocommon.hexstr_to_bytelist(message)
        keybytelist = cryptocommon.hexstr_to_bytelist(key)
        ciphertextbytelist = aescipher.encrypt(plaintextbytelist, keybytelist, printdebug=True)
        ciphertexthexstr = cryptocommon.bytelist_to_hexstr(ciphertextbytelist)
        assert aescipher.decrypt(ciphertextbytelist, keybytelist) == plaintextbytelist
        
        print('Plaintext  (hex): {}'.format(message))
        print('Ciphertext (hex): {}'.format(ciphertexthexstr))
        print('Key        (hex): {}'.format(key))
        print("Plaintext  (bytelist): {}".format(plaintextbytelist))
        print("Ciphertext (bytelist): {}".format(ciphertextbytelist))
        print("Key        (bytelist): {}".format(keybytelist))        
    
    
class STV_FWK_CRYPTO_ELLIPTIC_CURVE():
    """ Define el uso de Elliptic Curve Diffie Hellman para generación de llaves
    
    Esta clase utiliza la formula: y^2 = x^3 + 2x + 2 (mod 17)
    que corresponde a y^2 = x^3 + ax + b (mod p)
    
    G = (5,1)
    a = 2
    b = 2
    
    https://www.youtube.com/watch?v=F3zzNa42-tQ
    
    """
    k_ALGO_NAIVE = 0
    k_ALGO_M_PRIME = 1  # Por default
    
    
    def __init__(
        self,
        n_p = 17, # 16785407 Numero primo en el rango de millones
        n_x_G = 5, 
        n_y_G = 1, 
        n_a = 2,  # 810108
        n_b = 2,  # 12734743
        n_n = 20,
        k_algo = k_ALGO_M_PRIME): # 2000
        self._n_p = n_p
        self._n_x_G = n_x_G
        self._n_y_G = n_y_G
        self._n_a = n_a
        self._n_b = n_b
        self._n_n = n_n   # ord(G)
        self._k_algo = k_algo
        
        self._L_G = []

    def compute_Gpoints(self):
        self._n_s = self.compute_s(self._n_x_G, self._n_y_G)
        
        self._L_G = [(self._n_x_G, self._n_y_G)]
        _T_point = self.compute_2P(self._n_x_G, self._n_y_G)
        self._L_G.append(_T_point)
        for _i in range(1, self._n_n):
            _T_point = self.compute_nextPoint(self._n_x_G, self._n_y_G, _T_point[0], _T_point[1])
            if _T_point:
                self._L_G.append(_T_point)
            else:
                break
        self._n_h = len(self._L_G) / float(self._n_n)
        return self._n_h
        
    def compute_s(self, n_x, n_y):
        """ Calcula el valor reusable para determinar 2P
        
        s = (3x^2+a)/(2y) donde x,y son el punto P; a es el coeficiente de x en la formula
        """
        
        _n_numerador = ((3 * pow(n_x, 2, self._n_p)) + self._n_a) % self._n_p
        _n_denominador = (2 * n_y) % self._n_p
        
        if self._k_algo == STV_FWK_CRYPTO_ELLIPTIC_CURVE.k_ALGO_NAIVE:
            _n_denominador_mod = self.modInverse_naive(_n_denominador, self._n_p)        
        else:
            _n_denominador_mod = self.modInverse_m_prime(_n_denominador, self._n_p)
        
        _n_result = (_n_numerador * _n_denominador_mod) % self._n_p
        
        return _n_result
    
    def compute_2P(self, n_x_P, n_y_P):
        """ Calcula 2P en la curva eliptica
        
        x_2G = s^2 -2x_G
        y_2G = s(x_G - x_2G) - y_G
        """
        _n_s = self._n_s
        
        if _n_s:
            
            _n_x_2P = ( pow(_n_s, 2, self._n_p) - ((2 * n_x_P)) ) % self._n_p
            _n_y_2P = ( ( ( _n_s * (n_x_P - _n_x_2P) )) - n_y_P ) % self._n_p
            
        else:
            _n_x_2P = None
            _n_y_2P = None
        
        return (_n_x_2P, _n_y_2P)
    
    def compute_nextPoint(self, n_x_P, n_y_P, n_x_2P, n_y_2P):
        if n_x_2P == n_x_P:
            return None
        else:
            if self._k_algo == STV_FWK_CRYPTO_ELLIPTIC_CURVE.k_ALGO_NAIVE:
                _n_lambda = ((n_y_2P - n_y_P) * self.modInverse_naive((n_x_2P - n_x_P), self._n_p)) % self._n_p
            else:
                _n_lambda = ((n_y_2P - n_y_P) * self.modInverse_m_prime((n_x_2P - n_x_P), self._n_p)) % self._n_p
            _n_x_next = (pow(_n_lambda, 2, self._n_p) - n_x_P - n_x_2P) % self._n_p
            _n_y_next = ( (_n_lambda*(n_x_P - _n_x_next)) - n_y_P ) % self._n_p
        return (_n_x_next, _n_y_next)
    
    def compute_nextpoint2(self, n_x, n_y):
        """ Calcula el componente 2G
        
        x_2G = s^2 -2x_G
        y_2G = s(x_G - x_2G) - y_G
        """
        _n_s = self.compute_s(n_x, n_y)
        
        if _n_s:
            
            _n_x_2G = ( (pow(_n_s, 2, self._n_p)) - ((2 * n_x)) ) % self._n_p
            _n_y_2G = ( ( ( _n_s * (n_x - _n_x_2G) ) ) - n_y ) % self._n_p
            
        else:
            _n_x_2G = None
            _n_y_2G = None
        
        return (_n_x_2G, _n_y_2G)    
        
    def compute_formula(self, n_x, n_y):
        return (pow(n_x,3,self._n_p) + (self._n_a*n_x) + self._n_b) % self._n_p
        
    def modInverse_naive(self, n_num, n_mod):
        """ Calcula el modulo multiplicativo inverso de un numero
        
        De forma que (E_return * n_num) % n_mod = 1
        
        """
        
        _n_num = n_num % n_mod; 
        for _n_try in range(1, n_mod):
            if ((_n_num * _n_try) % n_mod == 1):
                return _n_try
        return 1
    
    
    def modInverse_m_prime(self, n_num, n_mod):
        """ Calcula el modulo inverso cuando el modulo m es primo
        
        """
        _n_result = None

        _n_mcd = self.gcd(n_num, n_mod); 
        if (_n_mcd != 1):
            _n_result = None
            print ("Error, modulo no es primo")
        else:
            _n_result = pow(n_num, n_mod-2, n_mod)
        return _n_result

    def pow_wModule(self, n_num, n_potencia, n_modulo):
        E_return = None
        if (n_potencia == 0):
            E_return = 1
        else:
            _n_potencia = self.pow_wModule(n_num, int(n_potencia/2), n_modulo) % n_modulo; 
            _n_potencia = (_n_potencia * _n_potencia) % n_modulo; 
            if ((n_potencia%2) == 0):
                E_return = _n_potencia
            else:
                E_return = (n_num * _n_potencia) % n_modulo
        return E_return

    def gcd(self, n_numa, n_numb):
        _n_return = None
        if (n_numa == 0):
            _n_return = abs(n_numb)
        else:
            _n_return = self.gcd(n_numb%n_numa, n_numa)
        return _n_return


class STV_FWK_PROTECT:
    
    def generateChallenge(
            self
            ):

        return


class STV_FWK_APP:
    s_APP_ID = ""
    
    n_zonahoraria_id = None
    _E_zonahoraria_definidor = 0
    
    class E_ZONAHORARIA_DEFINIDOR:
        """ Se definen las fuentes para determinar la zona horaria,
        el número indica la prioridad, más alto = más alta prioridad
        """
        NO_IDENTIFICADO = 0
        SESION = 10  # Indica que la variable de session determinó la zona horaria
        REQUEST_VAR = 20  # Indica que una variable get/port determinó la zona horaria
        USUARIO = 30  # Indica que el usuario determinó la zona horaria
        SERVIDOR = 40  # Indica que el servidor determinó la zona horaria
    
    class FORMAT:
        
        s_DATE = '%d/%m/%Y'
        s_DATETIME = '%d/%m/%Y %H:%M'
        s_DATETIMEISO = '%Y-%m-%dT%H:%M:%S.'    
        s_TIME = '%H:%M'

    @classmethod
    def DEFINIR_ZONAHORARIA(cls, n_zonahoraria_id, E_zonahoraria_definidor = 0):
        try:
            # Se identifican las prioridades
            if E_zonahoraria_definidor >= cls._E_zonahoraria_definidor:
                cls.n_zonahoraria_id = n_zonahoraria_id
                cls._E_zonahoraria_definidor = E_zonahoraria_definidor
            else:
                pass
        except Exception:
            pass
        return cls.n_zonahoraria_id

    pass  # STV_FWK_APP
