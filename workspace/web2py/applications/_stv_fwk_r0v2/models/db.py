# -*- coding: utf-8 -*-

#########################################################################
# This scaffolding model makes your app work on Google App Engine too
# File is released under public domain and you can use without limitations
#########################################################################

# if SSL/HTTPS is properly configured and you want all HTTP requests to
# be redirected to HTTPS, uncomment the line below:
# request.requires_https()

# app configuration made easy. Look inside private/appconfig.ini
from gluon.contrib.appconfig import AppConfig
# once in production, remove reload=True to gain full speed

D_stvFwkCfg.conf = AppConfig(os.path.join(D_stvFwkCfg.s_dirFwk, 'private', 'appconfig.ini'), reload = True)

request.stv_fwk_permissions = STV_FWK_PERMISSIONS()

D_stvFwkCfg.b_esServidorPruebas = bool(
    D_stvFwkCfg.conf.take('web2py_path_para_pruebas').get(request.env['web2py_path'], False)
    )
D_stvFwkCfg.b_esServidorProduccion = not D_stvFwkCfg.b_esServidorPruebas and bool(
    D_stvFwkCfg.conf.take('web2py_path_para_produccion').get(request.env['web2py_path'], False)
    )
D_stvFwkCfg.b_esServidorDesarrollo = not D_stvFwkCfg.b_esServidorPruebas \
                                     and not D_stvFwkCfg.b_esServidorProduccion \
                                     and (request.is_local or ('127.0.0.1' in request.env['http_host']))
D_stvFwkCfg.b_esServidorScheduler = request.is_scheduler
D_stvFwkCfg.b_esReconstruirDefinicionesDAL = False

if D_stvFwkCfg.b_esServidorPruebas:
    _D_serverConf = D_stvFwkCfg.conf.take(
        D_stvFwkCfg.conf.take('web2py_path_para_pruebas').get(request.env['web2py_path'], "error")
        )

    if not _D_serverConf or (_D_serverConf['tipo_servidor'] != "pruebas"):
        raise RuntimeError("Servidor de pruebas o path no estan correctamente configurados")

    else:
        db = DAL(
            _D_serverConf['uri'], pool_size = int(_D_serverConf['pool_size']), check_reserved = ['all'],
            fake_migrate = D_stvFwkCfg.b_esReconstruirDefinicionesDAL
            )

elif D_stvFwkCfg.b_esServidorProduccion:
    _D_serverConf = D_stvFwkCfg.conf.take(
        D_stvFwkCfg.conf.take('web2py_path_para_produccion').get(request.env['web2py_path'], "error")
        )

    if not _D_serverConf or (_D_serverConf['tipo_servidor'] != "produccion"):
        raise RuntimeError("Servidor de producción o path no estan correctamente configurados")

    else:
        db = DAL(
            _D_serverConf['uri'], pool_size = int(_D_serverConf['pool_size']), check_reserved = ['all'],
            fake_migrate = D_stvFwkCfg.b_esReconstruirDefinicionesDAL
            )

elif D_stvFwkCfg.b_esServidorDesarrollo:
    _D_serverConf = D_stvFwkCfg.conf.take('localhost_b')
    if not _D_serverConf or (_D_serverConf['tipo_servidor'] != "desarrollo"):
        raise RuntimeError("Servidor de desarrollo o path no estan correctamente configurados")

    else:
        db = DAL(
            _D_serverConf['uri'], pool_size = int(_D_serverConf['pool_size']), check_reserved = ['all'],
            fake_migrate = D_stvFwkCfg.b_esReconstruirDefinicionesDAL
            )

else:
    raise RuntimeError("No se pudo identificar el origen de execución, agregar path al archivo ini")


# Se configura el uso de la base de datos para grabar los datos de la session
D_stvFwkCfg.cfg_cuenta_aplicacion_id = 0
D_stvFwkCfg.cfg_cuenta_id = 0
D_stvFwkCfg.b_iswebservice = False

if request.url and ('call/soap' in request.url):
    # Remove the line breaks
    D_stvFwkCfg.b_iswebservice = True
    if hasattr(request.body, "file"):
        _s_wbsData = str(request.body.file.raw.readall())
    else:
        _s_wbsData = ""

    if _s_wbsData and ("D_auth" in _s_wbsData):
        _s_D_auth = re.compile(r'<D_auth[^>]*>([^|]*)</D_auth>').search(_s_wbsData).group(1)\
            .replace('\\n', '').strip()

        request.cookies['session_id_%s' % D_stvFwkCfg.s_nameAccesoApp.lower()] = \
            re.compile('<.*s_token.*>([\s\S]*?)</.*s_token>').search(_s_D_auth).group(1)

        # Se registra la cookie para recuperar la session abierta
        if 'cuenta_aplicacion_id' in _s_D_auth:
            D_stvFwkCfg.cfg_cuenta_aplicacion_id = \
                re.compile('<.*cuenta_aplicacion_id.*>([\s\S]*?)</.*cuenta_aplicacion_id>').search(_s_D_auth).group(1)
            D_stvFwkCfg.cfg_cuenta_aplicacion_id = int(D_stvFwkCfg.cfg_cuenta_aplicacion_id) \
                if D_stvFwkCfg.cfg_cuenta_aplicacion_id.isdigit() else 0

        else:
            D_stvFwkCfg.cfg_cuenta_aplicacion_id = 0

        if 'cuenta_id' in _s_D_auth:
            D_stvFwkCfg.cfg_cuenta_id = \
                re.compile('<.*cuenta_id.*>([\s\S]*?)</.*cuenta_id>').search(_s_D_auth).group(1)
            D_stvFwkCfg.cfg_cuenta_id = int(D_stvFwkCfg.cfg_cuenta_aplicacion_id) \
                if D_stvFwkCfg.cfg_cuenta_id.isdigit() else 0

        else:
            D_stvFwkCfg.cfg_cuenta_id = 0

    else:
        pass

elif request.is_scheduler:
    pass
    
elif request.vars.s_token:
    _s_sessionNombre = 'session_id_%s' % D_stvFwkCfg.s_nameAccesoApp.lower()
    # TODO respaldar la session, para regresarla despues
    request.cookies[_s_sessionNombre] = str(request.vars.s_token)
else:
    D_stvFwkCfg.cfg_cuenta_aplicacion_id = int(request.vars.cfg_cuenta_aplicacion_id) if str(
        request.vars.cfg_cuenta_aplicacion_id
        ).isdigit() else 0
    D_stvFwkCfg.cfg_cuenta_id = int(request.vars.cfg_cuenta_id) if str(request.vars.cfg_cuenta_id).isdigit() else 0  

if D_stvFwkCfg.b_requestIsMaster:
    session.connect(request, response, db)
else:
    session.connect(request, response, db, masterapp = D_stvFwkCfg.s_nameAccesoApp)

if (
        not session.auth
        or not(str(D_stvFwkCfg.cfg_cuenta_aplicacion_id).isdigit())
        or not(str(D_stvFwkCfg.cfg_cuenta_id).isdigit())
        ):
    # Si no esta autorizada la session, se limpian por seguridad la aplicación
    D_stvFwkCfg.cfg_cuenta_aplicacion_id = 0
    D_stvFwkCfg.cfg_cuenta_id = 0   
else:
    pass

D_stvFwkCfg.D_cfg_cuenta_para_links = Storage(
    cfg_cuenta_aplicacion_id = D_stvFwkCfg.cfg_cuenta_aplicacion_id, cfg_cuenta_id = D_stvFwkCfg.cfg_cuenta_id
    )
    
# by default give a view/generic.extension to all actions from localhost
# none otherwise. a pattern can be 'controller/function.extension'
response.generic_patterns = ['*'] if request.is_local else []
# choose a style for forms
response.formstyle = D_stvFwkCfg.conf.take('forms.formstyle')  # or 'bootstrap3_stacked' or 'bootstrap2' or other
response.form_label_separator = D_stvFwkCfg.conf.take('forms.separator')


# (optional) optimize handling of static files
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'
# (optional) static assets folder versioning
# response.static_version = '0.0.0'
#########################################################################
# Here is sample code if you need for
# - email capabilities
# - authentication (registration, login, logout, ... )
# - authorization (role based authorization)
# - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
# - old style crud actions
# (more options discussed in gluon/tools.py)
#########################################################################

auth = Auth(db)


class AUTH_USER:

    @staticmethod
    def OBTENER_FECHAHORA_USUARIO(n_usuario_id, dt_fecha_utc = request.utcnow):
        _dbRow = db.auth_user(n_usuario_id)
        _n_segundos = TZONASHORARIAS.OBTENER_DIFERENCIA_SEG_ZONAHORARIA(_dbRow.zonahoraria_id, dt_fecha_utc) \
            if _dbRow.zonahoraria_id else 0
        return dt_fecha_utc + datetime.timedelta(seconds = _n_segundos)

    pass  # AUTH_USER


auth.settings.extra_fields['auth_user'] = [
    Field(
        'address', 'string', length = None, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Domicilio', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'phones', 'string', length = None, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Teléfono', comment = None,
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'foto', type = 'upload',
        required = False,
        requires = [IS_NULL_OR(
            IS_LENGTH(minsize = 0, maxsize = 1572864, error_message = T('File is too huge.')),
            IS_UPLOAD_FILENAME(extension = 'gif|jpg|png', error_message = T('Image file required'))
            )],
        notnull = False,
        uploadfield = True, widget = lambda f, d, u = None: stv_widget_inputFile(
            f, d, URL(a = D_stvFwkCfg.s_nameBackofficeApp, c = 'accesos', f = 'foto'), db.auth_user.fotonombrearchivo
            ),
        label = 'Foto', comment = 'Foto del usuario',
        writable = True, readable = True, authorize = lambda record: auth.is_logged_in(),
        autodelete = False, represent = lambda v, r: stv_represent_file(
            v, r, URL(a = D_stvFwkCfg.s_nameBackofficeApp, c = 'accesos', f = 'foto'), db.auth_user.fotonombrearchivo,
            stv_represent_file_showOption_opendownload
            ),
        uploadfolder = os.path.join(
            request.folder, '..', D_stvFwkCfg.s_nameBackofficeApp, 'uploads', 'accesos', 'foto'
            ),
        uploadseparate = False, uploadfs = None
        ),
    Field(
        'fotominiatura', type = 'upload',
        required = False, requires = [IS_NULL_OR(
            IS_LENGTH(minsize = 0, maxsize = 1572864, error_message = T('File is too huge.')),
            IS_UPLOAD_FILENAME(extension = 'gif|jpg|png', error_message = T('Image file required'))
            )],
        notnull = False,
        uploadfield = True, widget = lambda f, d, u = None: stv_widget_inputFile(
            f, d, URL(a = D_stvFwkCfg.s_nameBackofficeApp, c = 'accesos', f = 'fotomin'), db.auth_user.fotonombrearchivo
            ),
        label = 'Foto Miniatura', comment = 'Foto del usuario en miniatura',
        writable = False, readable = False, authorize = lambda record: auth.is_logged_in(),
        autodelete = False, represent = lambda v, r: stv_represent_file(
            v, r, URL(a = D_stvFwkCfg.s_nameBackofficeApp, c = 'accesos', f = 'fotomin'),
            db.auth_user.fotonombrearchivo, stv_represent_file_showOption_opendownload
            ),
        uploadfolder = os.path.join(
            request.folder, '..', D_stvFwkCfg.s_nameBackofficeApp, 'uploads', 'accesos', 'fotomin'
            ),
        uploadseparate = False, uploadfs = None
        ),
    Field(
        'fotonombrearchivo', 'string', length = 100, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = T('Photo Filename'), comment = None,
        writable = False, readable = False,
        represent = stv_represent_string
        ),
    Field(
        'zonahoraria_id', 'integer', default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Zona Horaria', comment = None,
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r, db.auth_user.zonahoraria_id)
        ),
    Field(
        'notes', 'text', default = None,
        required = False,
        notnull = False,
        widget = stv_widget_text, label = 'Notas', comment = None,
        writable = True, readable = True,
        represent = stv_represent_text
        ),
    Field(
        'bansuspender', 'boolean', default = 0,
        required = True,
        notnull = True,
        label = 'Suspender', comment = None,
        writable = False, readable = True,
        represent = stv_represent_boolean
        ),
    Field(
        'baneliminar', 'boolean', default = 0,
        required = True,
        notnull = True,
        label = T('Deleted'), comment = 'Bandera que identifica el registro como eliminado',
        writable = False, readable = False,
        represent = stv_represent_boolean
        ),
  ]


class AUTH_GROUP:
    pass


auth.settings.extra_fields['auth_group'] = [
    Field(
        'notes', 'text', default = None,
        required = False,
        notnull = False,
        widget = stv_widget_text, label = 'Notas', comment = None,
        writable = True, readable = True,
        represent = stv_represent_text
        ),
    Field(
        'bansuspender', 'boolean', default = 0,
        required = True,
        notnull = True,
        label = 'Suspender', comment = None,
        writable = False, readable = True,
        represent = stv_represent_boolean
        ),
    Field(
        'baneliminar', 'boolean', default = 0,
        required = True,
        notnull = True,
        label = T('Deleted'), comment = 'Bandera que identifica el registro como eliminado',
        writable = False, readable = False,
        represent = stv_represent_boolean
        ),
  ]


class AUTH_PERMISSION:
    # El campo record_id de la tabla auth_permission corresponde a tformulario_permisos.id
    pass


auth.settings.extra_fields['auth_permission'] = [
    Field(
        'bansuspender', 'boolean', default = 0,
        required = True,
        notnull = True,
        label = 'Suspender', comment = None,
        writable = False, readable = True,
        represent = stv_represent_boolean
        ),
    Field(
        'baneliminar', 'boolean', default = 0,
        required = True,
        notnull = True,
        label = T('Deleted'), comment = 'Bandera que identifica el registro como eliminado',
        writable = False, readable = False,
        represent = stv_represent_boolean
        ),
  ]


class AUTH_MEMBERSHIP:
    pass


auth.settings.extra_fields['auth_membership'] = [
    Field(
        'cuenta_id', 'integer', default=None, 
        required=True,
        ondelete='NO ACTION', notnull=True, unique=False,
        widget = stv_widget_db_combobox, label=T('Account'), comment=None,
        writable=True, readable=True,
        represent=stv_represent_referencefield
        ),
    Field(
        'bansuspender', 'boolean', default = 0,
        required = True,
        notnull = True,
        label = 'Suspender', comment = None,
        writable = False, readable = True,
        represent = stv_represent_boolean
        ),
    Field(
        'baneliminar', 'boolean', default = 0,
        required = True,
        notnull = True,
        label = T('Deleted'), comment = 'Bandera que identifica el registro como eliminado',
        writable = False, readable = False,
        represent = stv_represent_boolean
        ),
  ]


# Configure auth policy
auth.settings.actions_disabled.append('register')

auth.settings.formstyle = "bootstrap"
auth.settings.registration_requires_verification = (not request.is_local)
auth.settings.registration_requires_approval = True
auth.settings.reset_password_requires_verification = True
auth.settings.create_user_groups = False

auth.settings.keep_session_onlogout = True


def onlogout_clearsession(user):
    _ = user
    _stv_flash = None
    if 'stv_flash' in session:
        _stv_flash = session.stv_flash
    else:
        pass
    session.renew(True)
    if _stv_flash:
        session.stv_flash = _stv_flash
    else:
        pass


auth.settings.logout_onlogout = onlogout_clearsession


auth.settings.login_url = URL(a=D_stvFwkCfg.s_nameAccesoApp, c='default', f='user', args=['login'], vars=request.vars)
auth.settings.logged_url = URL(
    a = D_stvFwkCfg.s_nameAccesoApp, c = 'default', f = 'user', args = ['profile'], vars = request.vars
    )

auth.settings.login_next = URL(a=D_stvFwkCfg.s_nameAccesoApp, c='default', f='index', vars=request.vars)
auth.settings.logout_next = URL(a=D_stvFwkCfg.s_nameAccesoApp, c='default', f='index')
auth.settings.profile_next = URL(a=D_stvFwkCfg.s_nameAccesoApp, c='default', f='index', vars=request.vars)
auth.settings.register_next = URL(a=D_stvFwkCfg.s_nameAccesoApp, c='default', f='index')
auth.settings.retrieve_username_next = URL(a=D_stvFwkCfg.s_nameAccesoApp, c='default', f='index')
auth.settings.retrieve_password_next = URL(a=D_stvFwkCfg.s_nameAccesoApp, c='default', f='index')
auth.settings.change_password_next = URL(a=D_stvFwkCfg.s_nameAccesoApp, c='default', f='index')
auth.settings.request_reset_password_next = auth.settings.login_url
auth.settings.reset_password_next = auth.settings.login_url
auth.settings.verify_email_next = auth.settings.login_url

auth.settings.on_failed_authorization = URL(
    a = D_stvFwkCfg.s_nameAccesoApp, c = 'default', f = 'user', args = ['on_failed_authorization']
    )

auth.settings.login_onvalidation = []
auth.settings.register_onvalidation = []
auth.settings.profile_onvalidation = []
auth.settings.retrieve_password_onvalidation = []
auth.settings.reset_password_onvalidation = []
auth.settings.login_onaccept = []
auth.settings.register_onaccept = []
auth.settings.profile_onaccept = []
auth.settings.verify_email_onaccept = []

auth.settings.captcha = None
auth.settings.login_captcha = None
auth.settings.register_captcha = None
auth.settings.retrieve_username_captcha = None
auth.settings.retrieve_password_captcha = None

auth.settings.expiration = 1500  # seconds
# auth.settings.expiration = 30  # seconds

auth.settings.long_expiration = 3600*24*30  # one month
auth.settings.remember_me_form = True

# create all tables needed by auth if not custom tables
auth.define_tables(username = False, signature = False, migrate = D_stvFwkCfg.b_requestIsMaster)

# Se actualizar los widgets usados para las tablas de usuario
db.auth_group.role.widget = stv_widget_input
db.auth_group.description.widget = stv_widget_input
db.auth_membership.group_id.widget = stv_widget_db_combobox
db.auth_membership.group_id.notnull = True
db.auth_membership.group_id.required = True
db.auth_membership.user_id.writable = False
db.auth_membership.user_id.readable = False
# Incluir integridad referencial con auth
db.auth_membership.group_id.ondelete = 'NO ACTION'
db.auth_permission.group_id.widget = stv_widget_db_combobox
db.auth_permission.name.widget = stv_widget_input
db.auth_permission.record_id.required = True
db.auth_permission.record_id.notnull = True
db.auth_permission.record_id.default = None
db.auth_permission.record_id.widget = stv_widget_db_combobox

db.auth_user.first_name.widget = stv_widget_input
db.auth_user.last_name.widget = stv_widget_input
db.auth_user.email.widget = stv_widget_inputEmail
db.auth_user.password.widget = stv_widget_inputPassword

db.auth_user.first_name.represent = stv_represent_string
db.auth_user.last_name.represent = stv_represent_string
db.auth_user.email.represent = stv_represent_string
db.auth_user.password.represent = stv_represent_string

db.auth_permission.group_id.represent = stv_represent_referencefield
db.auth_permission.record_id.represent = stv_represent_referencefield
db.auth_membership.group_id.represent = stv_represent_referencefield
db.auth_membership.user_id.represent = stv_represent_referencefield

# Formato para despliegue de los registros de usuarios
db.auth_user._format = '%(first_name)s %(last_name)s [%(email)s]'

# Configure email
mail = auth.settings.mailer
# mail.settings.server = 'logging' if request.is_local else D_stvFwkCfg.conf.take('smtp.server')
# mail.settings.server = D_stvFwkCfg.conf.take('smtp.server')
# mail.settings.sender = D_stvFwkCfg.conf.take('smtp.sender')
# mail.settings.login = D_stvFwkCfg.conf.take('smtp.login')
# mail.settings.ssl = True


mail.settings.server = "162.214.116.38:465"
mail.settings.sender = "contacto@stverticales.mx"
mail.settings.login = "contacto@stverticales.mx:L1vgGw6w"
mail.settings.tls = False
mail.settings.ssl = True

''' Definición de alias para las tablas '''
dbU = db.auth_user.with_alias('tU')
dbG = db.auth_group.with_alias('tG')
dbGP = db.auth_permission.with_alias('tGP')
dbUG = db.auth_membership.with_alias('tUG')

D_stvFwkCfg.b_isSupportUser = any(
    word in auth.user.email for word in ('@stverticales.mx', '@stverticales.com')
    ) if auth.user else None

if auth.user:
    # Si existe el usuario, este define la zonahoraria
    STV_FWK_APP.DEFINIR_ZONAHORARIA(auth.user.zonahoraria_id, STV_FWK_APP.E_ZONAHORARIA_DEFINIDOR.USUARIO)

else:
    pass

if session.stv_browser_zonahoraria_detectada:
    # Si existe una zonahoraria detectada en la sesion, se intenta definir, si tiene menor prioridad se actualizara
    session.stv_browser_zonahoraria_detectada = STV_FWK_APP.DEFINIR_ZONAHORARIA(
        session.stv_browser_zonahoraria_detectada, STV_FWK_APP.E_ZONAHORARIA_DEFINIDOR.SESION
        )
else:
    session.stv_browser_zonahoraria_detectada = STV_FWK_APP.n_zonahoraria_id

# Mientras se define la tabla de TZONASHORARIOS, se usa el utc como fechahora del browser
request.browsernow = request.utcnow


def stv_fotoUsuarioThumbnail(nUsuarioId, tTamanio=(150, 150)):
    try:
        _dbRow = dbU(nUsuarioId)
        import os
        import uuid
        from PIL import Image

    except Exception as _O_excepcion:
        return

    im = Image.open(os.path.join(D_stvFwkCfg.s_dirAcceso, 'uploads', _dbRow.mainfile))
    im.thumbnail(tTamanio, Image.ANTIALIAS)
    thumbName = 'uploads.thumb.%s.jpg' % (uuid.uuid4())
    im.save(os.path.join(D_stvFwkCfg.s_dirAcceso, 'uploads', thumbName), 'jpeg')
    _dbRow.update_record(thumb=thumbName)
    return

# Se agrega el uso de scheduler para tareas que tomen mucho tiempo
import gluon.scheduler as schd_tasks
scheduler = schd_tasks.Scheduler(db, heartbeat = 10, utc_time = True, migrate = D_stvFwkCfg.b_requestIsMaster)

db.define_table(
    'scheduler_datos',
    Field('task_id', 'reference scheduler_task'),
    Field(
        's_param0', 'string', length = 512, default = '',
        widget = stv_widget_input, label = 'Param 0',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        's_param1', 'string', length = 512, default = '',
        widget = stv_widget_input, label = 'Param 1',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        's_param2', 'string', length = 512, default = '',
        widget = stv_widget_input, label = 'Param 2',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'n_param3', 'decimal(16,4)', default = 0,
        widget = stv_widget_inputMoney, label = 'Param 3',
        writable = False, readable = True,
        represent = stv_represent_money_ifnotzero
        ),
    Field(
        'dt_param4', type = TDATETIME_GRABADIRECTO_LEERDIRECTO, default = None,
        requires = IS_NULL_OR(IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME)),
        widget = stv_widget_inputDateTime, label = 'Param 4',
        writable = False, readable = True,
        represent = stv_represent_datetime
        ),
    Field(
        'dt_param5', type = TDATETIME_GRABADIRECTO_LEERDIRECTO, default = None,
        requires = IS_NULL_OR(IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME)),
        widget = stv_widget_inputDateTime, label = 'Param 5',
        writable = False, readable = True,
        represent = stv_represent_datetime
        ),
    Field(
        'n_param6', 'integer', default = 0,
        widget = stv_widget_inputInteger, label = 'Param 6',
        writable = False, readable = True,
        represent = stv_represent_number
        ),
    Field(
        'n_param7', 'integer', default = 0,
        widget = stv_widget_inputInteger, label = 'Param 7',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        't_param8', 'text',
        widget = stv_widget_text, label = 'Param 8',
        writable = False, readable = True,
        represent = stv_represent_text
        ),
    migrate = D_stvFwkCfg.b_requestIsMaster
    )

class SCHD_CONTROL_TAREA:

    class E_ESTADO:
        EN_COLA = 0
        EJECUTANDO = 1
        COMPLETADO = 2
        FALLO = 3
        TIMEOUT = 4
        DETENIDA = 5
        EXPIRO = 6
        OTRO = 7
        NO_HAY_TAREA = 8

        D_TODOS = {
            EN_COLA     : "En Cola",
            EJECUTANDO  : "Ejecutando",
            COMPLETADO  : "Completada",
            FALLO       : "Falló",
            TIMEOUT     : "Timeout",
            DETENIDA    : "Detenida",
            EXPIRO      : "Expiró",
            OTRO        : "Otra",
            NO_HAY_TAREA: " No hay tarea"
            }

        D_WRAP_SCHEDULER = {
            schd_tasks.QUEUED   : EN_COLA,
            schd_tasks.RUNNING  : EJECUTANDO,
            schd_tasks.COMPLETED: COMPLETADO,
            schd_tasks.FAILED   : FALLO,
            schd_tasks.TIMEOUT  : TIMEOUT,
            schd_tasks.STOPPED  : DETENIDA,
            schd_tasks.EXPIRED  : EXPIRO,
            }
        T_CERRADA = (COMPLETADO, FALLO, TIMEOUT, DETENIDA, EXPIRO, OTRO, NO_HAY_TAREA)

        pass  # E_ESTADO

    def __init__(self, s_nombreFuncion):

        self.s_nombreFuncion = s_nombreFuncion

        _dbRows_tasks = db(
            (db.scheduler_task.function_name == self.s_nombreFuncion)
            ).select(
            db.scheduler_task.ALL,
            orderby = [~db.scheduler_task.id],
            limitby = [0, 1]
            )

        if _dbRows_tasks:
            self.dbRow_ultimaTarea = _dbRows_tasks.first()
            self.dbRow_ultimaTarea.E_estatus = self.E_ESTADO.D_WRAP_SCHEDULER[self.dbRow_ultimaTarea.status]
        else:
            self.dbRow_ultimaTarea = None

        _dbRows_workers = db(
            (db.scheduler_worker.status == schd_tasks.ACTIVE)
            ).select(
            db.scheduler_worker.ALL,
            orderby = [~db.scheduler_worker.id],
            )

        self.dbRow_workerVivo = None

        if _dbRows_workers:
            for _dbRow in _dbRows_workers:
                if _dbRow.last_heartbeat > (request.utcnow - datetime.timedelta(seconds = 30)):
                    # Si el último heartbeat del worker fue hace menos de 30 segundos, sigue vivo
                    self.dbRow_workerVivo = _dbRow
                    break
                else:
                    # De lo contrario, no esta activo
                    pass

        else:
            pass

        return

    def encolar_tarea(self, L_args, D_vars, n_timeout = 60 * 60 * 2):
        _D_return = self.obtener_estatus_ultimaTarea()

        if not self.dbRow_ultimaTarea or (self.dbRow_ultimaTarea.E_estatus in self.E_ESTADO.T_CERRADA):
            # Si no existe tarea o esta en un estado de cierre, se crea

            _dbRow_task = scheduler.queue_task(
                self.s_nombreFuncion,
                pargs = L_args,
                pvars = D_vars,
                timeout = n_timeout
                )
            if _dbRow_task:
                self.dbRow_ultimaTarea = db.scheduler_task(_dbRow_task.id)
                self.dbRow_ultimaTarea.E_estatus = self.E_ESTADO.D_WRAP_SCHEDULER[self.dbRow_ultimaTarea.status]
            else:
                pass

        else:
            _D_return.agrega_advertencia("Ya existe una tarea en proceso")

        return _D_return

    def obtener_estatus_ultimaTarea(self):
        _D_return = FUNC_RETURN(
            E_estatus = self.dbRow_ultimaTarea.E_estatus if self.dbRow_ultimaTarea else self.E_ESTADO.NO_HAY_TAREA,
            b_workersActivo = True if self.dbRow_workerVivo else False
            )

        if not _D_return.b_workersActivo:
            _D_return.agrega_error("No existe un scheduler activo que pueda correr la tarea")
        else:
            pass

        return _D_return

    def actualizar_datos_ultimaTarea(self, **D_params):

        _D_return = self.obtener_estatus_ultimaTarea()

        if self.dbRow_ultimaTarea:
            D_params = Storage(D_params) if D_params else Storage()
            D_params['task_id'] = self.dbRow_ultimaTarea.id
            db.scheduler_datos.update_or_insert(
                db.scheduler_datos.task_id == self.dbRow_ultimaTarea.id,
                **D_params
                )
            db.commit()
        else:
            _D_return.agrega_error("Tarea no identificada")

        return

    def leer_datos_ultimaTarea(self):

        _D_return = self.obtener_estatus_ultimaTarea()

        if self.dbRow_ultimaTarea:
            _dbRows = db(db.scheduler_datos.task_id == self.dbRow_ultimaTarea.id).select(db.scheduler_datos.ALL)
            if _dbRows:
                _D_return.combinar(Storage(_dbRows.last().as_dict()))
            else:
                _D_return.agrega_advertencia("No se encontro registro de datos")
        else:
            _D_return.agrega_error("Tarea no identificada")

        return _D_return

    pass  # SCHD_CONTROL_TAREA
