# -*- coding: utf-8 -*-
"""
Plantilla que define la forma en que se deben de crear las tablas y sys campos
"""

'''
Definir un campo con todas las propiedades
Field(name, 'string', length = None, default = None,
      required = False, requires = '<default>',
      ondelete = 'CASCADE', notnull = False, unique = False,
      uploadfield = True, widget = None, label = None, comment = None,
      writable = True, readable = True, update = None, authorize = None,
      autodelete = False, represent = None, compute = None,
      uploadfolder = None,
      uploadseparate = None,uploadfs = None)

Un campo de tipo Link:
Field(name, db.table, default = None,
      required = False,
      ondelete = 'CASCADE', notnull = False, unique = False,
      label = None, comment = None,
      writable = True, readable = True,
      represent = None),
      
Un campo de tipo String:
Field(name, 'string', length = None, default = None,
      required = False,
      notnull = False, unique = False,
      label = None, comment = None,
      writable = True, readable = True,
      represent = None),

Un campo de tipo Text:
Field(name, 'text', default = None,
      required = False,
      notnull = False,
      label = None, comment = None,
      writable = True, readable = True),
      
Un campo de tipo Boolean:
Field(name, 'boolean', default = None,
      required = False, requires = '<default>',
      notnull = False,
      label = None, comment = None,
      writable = True, readable = True,
      represent = None),

Un campo de tipo Integer:
Field(name, 'integer', default = None,
      required = False, requires = '<default>',
      notnull = False,
      label = None, comment = None,
      writable = True, readable = True,
      represent = None),
      
Un campo virtual
Field.Virtual(
    name, f = none, ftype = 'string' label = None)

Un campo que suba un archivo
Field(fieldname, type = 'upload',
      required = False, requires = IS_LENGTH(minsize=0, maxsize=1572864, error_message=T('File is too huge.')),
      ondelete = 'CASCADE', notnull = False,
      uploadfield = True, widget = None, label = None, comment = None,
      writable = True, readable = True, authorize = lambda record: auth.is_logged_in(),
      autodelete = False, represent = None, 
      uploadfolder = os.path.join(request.folder, 'uploads', 'table'),
      uploadseparate = None, uploadfs = None)
'''