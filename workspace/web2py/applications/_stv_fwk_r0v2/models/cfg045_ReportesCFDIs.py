# -*- coding: utf-8 -*-

# noinspection PyAttributeOutsideInit
class REPORTE_CFDI(STV_FWK_REPORT_PDF):
    """ Generación de reportes para STV """

    def __init__(
            self, x_cfdi,
            **D_paramsReporte
            ):
        _D_return = FUNC_RETURN()
        STV_FWK_REPORT_PDF.__init__(self, **D_paramsReporte)

        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_cfdi, dbc01.tcfdis)
        self._dbRow_cfdi = _D_results.dbRow
        _dbRows_cfdis2 = self._dbRow_cfdi.tcfdis2.select()
        self._dbRow_empresa = dbc01.tempresas(self._dbRow_cfdi.empresa_id)

        if self._dbRow_cfdi.emisornombrerazonsocial and (self._dbRow_cfdi.sat_status is not None) \
                and _dbRows_cfdis2:
            # Pareciera que fue importado correctamente
            pass

        else:
            # Si no tiene definida la razon social del emisor, no fue importado correctamente
            #  se procede a reimportar del XML

            # Primero se verifica que el XML exista
            _dbRow_xmls = dbc02(dbc02.tcfdi_xmls.cfdi_id == self._dbRow_cfdi.id).select(
                dbc02.tcfdi_xmls.id, dbc02.tcfdi_xmls.xml_cfdi
                )

            if _dbRow_xmls and D_stvFwkCfg.cfg_cuenta_aplicacion_id:
                _dbRow_xml = _dbRow_xmls.first()
                _stv_importar_cfdi = STV_CFDI_INTEGRACION(
                    s_rfc = self._dbRow_empresa.rfc,
                    s_empresa_id = self._dbRow_empresa.id,
                    s_cuenta_id = db.tcuenta_aplicaciones(D_stvFwkCfg.cfg_cuenta_aplicacion_id).cuenta_id,
                    )
                _D_results = _stv_importar_cfdi.verificar(
                    s_cfdi_id = self._dbRow_cfdi.id,
                    xml_cfdi = _dbRow_xml.xml_cfdi,
                    E_tipoImportacion = STV_CFDI_INTEGRACION.E_TIPOIMPORTACION.CFDI_EMITIDO
                    )
                _D_return.combinar(_D_results, "Re-importación de CFDI")

                self._dbRow_cfdi = dbc01.tcfdis(self._dbRow_cfdi.id)

                _dbRows_cfdis2 = self._dbRow_cfdi.tcfdis2.select()

            else:
                pass

        if _dbRows_cfdis2:
            self._dbRow_cfdi2 = _dbRows_cfdis2.first()

            if not self._dbRow_cfdi2.emisorcalle and self._dbRow_cfdi.empresa_plaza_sucursal_cajachica_id:
                # Se usa el domicilio de la sucursal para grabar la información del emisor
                _n_emp_plaz_suc_id = dbc01.tempresa_plaza_sucursal_cajaschicas(
                    self._dbRow_cfdi.empresa_plaza_sucursal_cajachica_id
                    ).empresa_plaza_sucursal_id
                _dbRow_sucursal = dbc01.tempresa_plaza_sucursales(_n_emp_plaz_suc_id)
                self._dbRow_cfdi2.emisorcalle = _dbRow_sucursal.calle
                self._dbRow_cfdi2.emisornumexterior = _dbRow_sucursal.numexterior
                self._dbRow_cfdi2.emisornuminterior = _dbRow_sucursal.numinterior
                self._dbRow_cfdi2.emisorcolonia = _dbRow_sucursal.colonia
                self._dbRow_cfdi2.emisorlocalidad = _dbRow_sucursal.ciudad
                self._dbRow_cfdi2.emisormunicipio = db01.tpais_estado_municipios(
                    _dbRow_sucursal.pais_estado_municipio_id
                    ).descripcion \
                    if _dbRow_sucursal.pais_estado_municipio_id else ""
                self._dbRow_cfdi2.emisorestado = db01.tpais_estados(_dbRow_sucursal.pais_estado_id).c_estado \
                    if _dbRow_sucursal.pais_estado_id else ""
                self._dbRow_cfdi2.emisorpais = db01.tpaises(_dbRow_sucursal.pais_id).c_pais \
                    if _dbRow_sucursal.pais_id else ""
                self._dbRow_cfdi2.emisorcodigopostal = _dbRow_sucursal.codigopostal
                self._dbRow_cfdi2.update_record()
            else:
                pass

        else:
            self._dbRow_cfdi2 = None

        self.L_msgError = []
        self.L_encabezado = []
        self.L_emisor = []
        self.L_receptor = []
        self.L_conceptosEncabezado = []
        self.L_conceptos = []
        self._D_conceptoImpuestos = Storage()  # Tendrá la lista de impuestos por concepto
        self.L_conceptoImpuestos = []
        self.L_conceptosResumen = []
        self.L_conceptosImpuestos = []
        self.L_relacionados = []
        self.L_pagare = []
        self.L_certificado = []
        self.L_piePagina = []

        self.n_msgError_offset = 0
        self.n_paginas = 0  # Número que se genera para saber el número de páginas en la primer impresión del reporte

        # Nombres normalizados de configuración en textos

        self.letra_ = self.LETRA.E_TIPO.TIMES_ROMAN
        self.letraB = self.LETRA.E_TIPO.TIMES_BOLD
        self.letra2_ = self.LETRA.E_TIPO.HELVETICA
        self.letra2B = self.LETRA.E_TIPO.HELVETICA_BOLD

        self.colorN = self.LETRA.COLOR.NEGRO
        self.colorV = self.LETRA.COLOR.STV_VERDE
        self.colorG = self.LETRA.COLOR.STV_GRIS
        self.colorVT = self.LETRA.COLOR.STV_VERDE_T
        self.colorGT = self.LETRA.COLOR.STV_GRIS_T
        self.colorR = self.LETRA.COLOR.ROJO
        self.colorE = self.LETRA.COLOR.VERDE
        self.colorA = self.LETRA.COLOR.AZUL
        self.colorT = self.LETRA.COLOR.TRANSPARENTE

        self.aIzq = self.E_ALINEACION.IZQUIERDA
        self.aDer = self.E_ALINEACION.DERECHA
        self.aCen = self.E_ALINEACION.CENTRO

        # generaMensajeFlash(dbc01.tcfdis, _D_return)
        if _D_return.E_return > CLASS_e_RETURN.OK:

            _O_textoMsgError = self._configurar_objeto_texto(
                s_texto = _D_return.s_msgError,
                D_posicion = STV_FWK_REPORT_PDF.POSICION(0, 0),
                E_alineacion = self.aCen,
                E_tipoletra = self.letra_,
                D_color = self.colorR,
                n_letratamanio = 15,
                n_ancho = None
                )

            self.L_msgError = _O_textoMsgError.L_elementos
            self.n_msgError_offset = _O_textoMsgError.n_offsetAlto
        else:
            pass

        return

    def define_encabezado(
            self,
            n_offset_vertical = 0,
            s_leyendaSuperior1 = "",
            s_leyendaSuperior2 = ""
            ):
        _D_return = FUNC_RETURN(
            n_offset_vertical = 0
            )

        # Se obtiene el path a una imagen
        if self._dbRow_empresa.imagen:
            (_s_nombreArchivo, _s_imgPath) = dbc01.tempresas.imagen.retrieve(self._dbRow_empresa.imagen, nameonly=True)
            _s_imgPath = _s_imgPath.replace('app_timbrado', 'app_empresas')
        else:
            _s_imgPath = None

        # Se agrega un encabezado
        _D_encabezado = self.helper_crearEncabezado(
            D_linea1 = Storage(s_texto = s_leyendaSuperior1 or "", E_tipoletra = self.letra2B, n_letratamanio = 15),
            D_linea2 = Storage(s_texto = s_leyendaSuperior2 or "", E_tipoletra = self.letra2B, n_letratamanio = 15),
            s_rutaImagen = _s_imgPath,
            D_fecha = Storage(
                s_texto = datetime.datetime.strftime(request.browsernow, STV_FWK_APP.FORMAT.s_DATE),
                E_alineacion = STV_FWK_REPORT_PDF.E_ALINEACION.DERECHA,
                # D_marco = self.HELPER_MARCO(True, True, True, True, 1, self.colorN)
                ),
            D_textoControl = Storage(),
            b_linea = False,
            n_offsetAlto = n_offset_vertical,
            )

        _D_return.n_offset_vertical = _D_encabezado.n_offsetAlto
        self.L_encabezado = _D_encabezado.L_elementos

        return _D_return

    def define_emisor(self, n_offset_vertical = 0):
        _D_return = FUNC_RETURN(
            n_offset_vertical = n_offset_vertical
            )

        _dbRow_regimenFiscal = db01.tregimenesfiscales(self._dbRow_empresa.regimenfiscal_id)
        _b_marco = False

        _D_tbl_emisor1 = self.helper_crearTabla(
            LL_datos = [
                [
                    self.HELPER_CELDA(
                        self.letraB, self.aIzq, self.colorN, 10, _b_marco,
                        self._dbRow_cfdi.emisornombrerazonsocial, None
                        )
                    ],
                [
                    self.HELPER_CELDA(
                        self.letraB, self.aIzq, self.colorN, 10, _b_marco,
                        self._dbRow_cfdi.emisorrfc, None
                        )
                    ],
                [
                    self.HELPER_CELDA(
                        self.letraB, self.aIzq, self.colorN, 10, _b_marco,
                        "{c_regimenfiscal} {descripcion}".format(
                            c_regimenfiscal = _dbRow_regimenFiscal.c_regimenfiscal,
                            descripcion = _dbRow_regimenFiscal.descripcion
                            ), None
                        )
                    ],
                [
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        "{calle} {numexterior} {numinterior}".format(
                            calle = self._dbRow_empresa.calle or "",
                            numexterior = self._dbRow_empresa.numexterior or "",
                            numinterior = self._dbRow_empresa.numinterior or ""
                            ),
                        None
                        )
                    ],
                [
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        "{colonia} {codigopostal} {ciudad}".format(
                            colonia = self._dbRow_empresa.colonia or "",
                            codigopostal = self._dbRow_empresa.codigopostal or "",
                            ciudad = self._dbRow_empresa.ciudad or ""
                            ),
                        None
                        )
                    ],
                [
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        "{estado} {pais}".format(
                            estado = db01.tpais_estados(self._dbRow_empresa.pais_estado_id).descripcion
                            if self._dbRow_empresa.pais_estado_id else "",
                            pais = db01.tpaises(self._dbRow_empresa.pais_id).descripcion
                            if self._dbRow_empresa.pais_id else ""
                            ),
                        None
                        )
                    ],
                ],
            LD_infoColumnas = [
                self.HELPER_COLUMNA(0.5, self.letra_, self.aIzq, self.colorN, 10, _b_marco),
                ],
            n_offsetAlto = _D_return.n_offset_vertical + 5
            )

        # Obtiene el offset horizontal para la segunda tabla
        _LD_offsetColumna = self.helper_offsetParaColumnas([Storage(n_ancho_pje = 0.5)])

        if self._dbRow_cfdi2:
            _D_tbl_emisor2 = self.helper_crearTabla(
                LL_datos = [
                    [self.HELPER_CELDA(
                        self.letraB, self.aIzq, self.colorN, 14, _b_marco,
                        "%s %s%s" % (
                            db01.ttiposcomprobantes(self._dbRow_cfdi.tipocomprobante_id).descripcion,
                            self._dbRow_cfdi.empresa_serie_id.serie, self._dbRow_cfdi.folio),
                        None
                        )],
                    [self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        "Certificado Digital {nocertificado}".format(
                            nocertificado = self._dbRow_cfdi2.nocertificado
                            ),
                        None
                        )],
                    [self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        "Folio Fiscal %s" % self._dbRow_cfdi.uuid,
                        None
                        )],
                    [self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        "Expedida en {emisorcalle} No. {emisornumexterior} {emisornuminterior}".format(
                            emisorcalle = self._dbRow_cfdi2.emisorcalle or "",
                            emisornumexterior = self._dbRow_cfdi2.emisornumexterior or "",
                            emisornuminterior = self._dbRow_cfdi2.emisornuminterior or ""
                            ),
                        None
                        )],
                    [self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        "{emisorcolonia} C.P. {emisorcodigopostal} {emisormunicipio}".format(
                            emisorcolonia = self._dbRow_cfdi2.emisorcolonia or "",
                            emisorcodigopostal = self._dbRow_cfdi2.emisorcodigopostal or "",
                            emisormunicipio = self._dbRow_cfdi2.emisormunicipio or ""
                            ),
                        None
                        )],
                    [self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        "{emisorestado}, {emisorpais}".format(
                            emisorestado = self._dbRow_cfdi2.emisorestado or "",
                            emisorpais = self._dbRow_cfdi2.emisorpais or ""
                            ),
                        None
                        )],
                    ],
                LD_infoColumnas = [
                    self.HELPER_COLUMNA(1, self.letra_, self.aIzq, self.colorN, 10, _b_marco),
                    ],
                n_offsetAlto = _D_return.n_offset_vertical,
                n_offsetDerecha = _LD_offsetColumna[0].n_ancho,
                # D_marco = self.HELPER_MARCO(True, True, True, True, 1, self.colorA)
                )
        else:
            _D_tbl_emisor2 = self.helper_crearTabla(
                LL_datos = [
                    [self.HELPER_CELDA(
                        self.letraB, self.aIzq, self.colorN, 14, _b_marco,
                        "%s %s%s" % (
                            db01.ttiposcomprobantes(self._dbRow_cfdi.tipocomprobante_id).descripcion,
                            self._dbRow_cfdi.empresa_serie_id.serie, self._dbRow_cfdi.folio),
                        None
                        )],
                    [self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        "Registro en CFDI2 no identificado",
                        None
                        )],
                    ],
                LD_infoColumnas = [
                    self.HELPER_COLUMNA(1, self.letra_, self.aIzq, self.colorN, 10, _b_marco),
                    ],
                n_offsetAlto = _D_return.n_offset_vertical,
                n_offsetDerecha = _LD_offsetColumna[0].n_ancho,
                # D_marco = self.HELPER_MARCO(True, True, True, True, 1, self.colorA)
                )

        _D_return.n_offset_vertical = _D_tbl_emisor2.n_offsetAlto \
            if _D_tbl_emisor2.n_offsetAlto > _D_tbl_emisor1.n_offsetAlto else _D_tbl_emisor1.n_offsetAlto
        self.L_emisor = _D_tbl_emisor1.L_elementos + _D_tbl_emisor2.L_elementos

        return _D_return

    def define_receptor(self, n_offset_vertical = 0):
        # noinspection DuplicatedCode
        _D_return = FUNC_RETURN(
            n_offset_vertical = n_offset_vertical
            )

        _dbRow_cliente = dbc01.tempresa_clientes(self._dbRow_cfdi.receptorcliente_id) \
            if self._dbRow_cfdi.receptorcliente_id else None

        _b_marco = False

        _D_tbl_receptor1 = self.helper_crearTabla(
            LL_datos = [
                [self.HELPER_CELDA(
                    self.letraB, self.aIzq, self.colorN, 11, _b_marco,
                    "Cliente", None,
                    ),
                self.HELPER_CELDA(
                    self.letra_, self.aIzq, self.colorN, 11, _b_marco,
                    str(self._dbRow_cfdi.receptorcliente_id), None,
                    )],
                ],
            LD_infoColumnas = [
                self.HELPER_COLUMNA(0.1, self.letra_, self.aIzq, self.colorN, 10, _b_marco),
                self.HELPER_COLUMNA(0.9, self.letra_, self.aIzq, self.colorN, 10, _b_marco),
                ],
            n_offsetAlto = _D_return.n_offset_vertical,
            D_marco = self.HELPER_MARCO(True, True, True, True, 1, self.colorN),
             D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
            )

        _D_return.n_offset_vertical = _D_tbl_receptor1.n_offsetAlto

        # noinspection DuplicatedCode
        _D_tbl_receptor2 = self.helper_crearTabla(
            LL_datos = [
                # Fecha de emisión y Vencimiento
                [
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        "Fecha de Emisión:", None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        self._dbRow_cfdi.fecha, None,
                        D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
                        x_formato = self.FORMATEA_FECHA
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        "Fecha de Vencimiento: ", None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        self._dbRow_cfdi.fecha + datetime.timedelta(days = self._dbRow_cfdi.credito_plazo or 0), None,
                        D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
                        x_formato = self.FORMATEA_FECHA
                        ),
                    ],
                # Nombre y RFC
                [
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        dbc01.tcfdis.receptornombrerazonsocial.label, None,
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        self._dbRow_cfdi.receptornombrerazonsocial, None,
                        D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        dbc01.tcfdis.receptorrfc.label, None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        self._dbRow_cfdi.receptorrfc, None,
                        D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
                        ),
                    ],
                # Uso CFDI y NIF
                [
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        dbc01.tcfdis.receptorusocfdi_id.label, None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        (
                            "%(c_usocfdi)s %(descripcion)s" %
                            db01.tusoscfdi(self._dbRow_cfdi.receptorusocfdi_id).as_dict()
                            ) if self._dbRow_cfdi.receptorusocfdi_id else "",
                        None,
                        D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                         "Receptor N.I.F.", None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        " ", None,
                        D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
                        ),
                    ],
                # Calle
                [
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        dbc01.tcfdis2.receptorcalle.label, None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        "{receptorcalle} {receptornumexterior} {receptornuminterior}".format(
                            receptorcalle = self._dbRow_cfdi2.receptorcalle or "",
                            receptornumexterior = self._dbRow_cfdi2.receptornumexterior or "",
                            receptornuminterior = self._dbRow_cfdi2.receptornuminterior or "",
                            ) if self._dbRow_cfdi2 else "",
                        None,
                        D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        "Número", None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        " ", None,
                        D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        " ", None,
                        ),
                    ],
                # Referencia
                [
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        dbc01.tcfdis2.receptorreferencia.label, None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        self._dbRow_cfdi2.receptorreferencia if self._dbRow_cfdi2 else "", None,
                        D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        " ", None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        " ", None,
                        D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
                        ),
                    ],
                # Código postal y colonia
                [
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        dbc01.tcfdis2.receptorcodigopostal.label, None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        self._dbRow_cfdi2.receptorcodigopostal if self._dbRow_cfdi2 else "", None,
                        D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        dbc01.tcfdis2.receptorcolonia.label, None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        self._dbRow_cfdi2.receptorcolonia if self._dbRow_cfdi2 else "", None,
                        D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
                        ),
                    ],
                # Ciudad y Municipio
                [
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        dbc01.tcfdis2.receptormunicipio.label, None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        self._dbRow_cfdi2.receptormunicipio if self._dbRow_cfdi2 else "", None,
                        D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        dbc01.tcfdis2.receptorlocalidad.label, None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        self._dbRow_cfdi2.receptorlocalidad if self._dbRow_cfdi2 else "", None,
                        D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
                        ),
                    ],
                # Estado y Pais
                [
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        dbc01.tcfdis2.receptorestado.label, None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        self._dbRow_cfdi2.receptorestado if self._dbRow_cfdi2 else "", None,
                        D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        dbc01.tcfdis2.receptorpais.label, None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        self._dbRow_cfdi2.receptorpais if self._dbRow_cfdi2 else "", None,
                        D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
                        ),
                    ],
                # Telefonos e emails
                [
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        dbc01.tcfdis2.receptortelefonos.label, None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        self._dbRow_cfdi2.receptortelefonos if self._dbRow_cfdi2 else "",
                        None,
                        D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        dbc01.tcfdis2.receptoremails.label, None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        self._dbRow_cfdi2.receptoremails if self._dbRow_cfdi2 else "", None,
                        D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
                        ),
                    ],
                # No. Orden y Forma Pago
                [
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        dbc01.tcfdis2.ordencompra.label, None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        self._dbRow_cfdi2.ordencompra if self._dbRow_cfdi2 else "",
                        None,
                        D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        dbc01.tcfdis.formapago_id.label, None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        "{c_formapago} {descripcion}".format(
                            **db01.tformaspago(self._dbRow_cfdi.formapago_id).as_dict()
                            ) if self._dbRow_cfdi.formapago_id else "",
                        D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
                        ),
                    ],
                # Método pago y condiciones
                [
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        dbc01.tcfdis.metodopago_id.label, None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        "{c_metodopago} {descripcion}".format(
                            **db01.tmetodospago(self._dbRow_cfdi.metodopago_id).as_dict()
                            ) if self._dbRow_cfdi.metodopago_id else "",
                        D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        dbc01.tcfdis.condicionesdepago.label, None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        self._dbRow_cfdi.condicionesdepago,
                        None,
                        D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
                        ),
                    ],
                # Agente de Ventas
                [
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        dbc01.tcfdis2.agentevendedor.label, None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        self._dbRow_cfdi2.agentevendedor if self._dbRow_cfdi2 else "",
                        D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        " ", None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        " ", None,
                        D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
                        ),
                    ],
                # Moneda y Tipo de Cambio
                [
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        dbc01.tcfdis.moneda.label, None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        self._dbRow_cfdi.moneda,
                        D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        dbc01.tcfdis.tipocambio.label, None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        self._dbRow_cfdi.tipocambio, None, x_formato = self.FORMATEA_DINERO,
                        D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
                        ),
                    ],
                ],
            LD_infoColumnas = [
                self.HELPER_COLUMNA(0.2, self.letra_, self.aIzq, self.colorN, 10, _b_marco),
                self.HELPER_COLUMNA(0.3, self.letra_, self.aIzq, self.colorN, 10, _b_marco, D_marco = self.HELPER_MARCO(False, False, False, True, .5, self.colorN)),
                self.HELPER_COLUMNA(0.2, self.letra_, self.aIzq, self.colorN, 10, _b_marco),
                self.HELPER_COLUMNA(0.3, self.letra_, self.aIzq, self.colorN, 10, _b_marco),
                ],
            n_offsetAlto = _D_return.n_offset_vertical,
            D_marco = self.HELPER_MARCO(True, True, True, True, 1, self.colorN),
            # D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
            )

        _D_return.n_offset_vertical = _D_tbl_receptor2.n_offsetAlto
        self.L_receptor = _D_tbl_receptor1.L_elementos + _D_tbl_receptor2.L_elementos

        return _D_return

    def define_conceptos(self, n_offset_vertical = 0):

        def formatea_impuestoConcepto(valor, row = None):
            if not row['id']:
                _s_impuesto = "ID del registro del concepto se encuentra vacío"

            elif not row['id'] in self._D_conceptoImpuestos:
                _s_impuesto = "Sin impuestos"

            else:
                _L_impuestos = []
                for _n_index in self._D_conceptoImpuestos[row['id']]:
                    _L_impuestos.append(
                        "Aplica {impuesto}-{impuesto_desc} {tipofactor} {tasaocuota:.2f}".format(
                            **self._D_conceptoImpuestos[row['id']][_n_index]
                            )
                        )
                    _s_impuesto = "\n".join(_L_impuestos)

            return _s_impuesto

        _D_return = FUNC_RETURN(
            n_offset_vertical = n_offset_vertical
            )

        _dbTabla = dbc01.tcfdi_conceptos
        _b_marco = False

        _tbl_conceptoColumnas = [
            self.HELPER_COLUMNA(0.1, self.letra_, self.aCen, self.colorN, 10, _b_marco),
            self.HELPER_COLUMNA(0.1, self.letra_, self.aCen, self.colorN, 10, _b_marco),
            self.HELPER_COLUMNA(0.1, self.letra_, self.aDer, self.colorN, 10, _b_marco),
            self.HELPER_COLUMNA(0.3, self.letra_, self.aIzq, self.colorN, 9, _b_marco),
            self.HELPER_COLUMNA(0.1, self.letra_, self.aDer, self.colorN, 10, _b_marco),
            self.HELPER_COLUMNA(0.1, self.letra_, self.aDer, self.colorN, 10, _b_marco),
            self.HELPER_COLUMNA(0.1, self.letra_, self.aDer, self.colorN, 10, _b_marco),
            self.HELPER_COLUMNA(0.1, self.letra_, self.aDer, self.colorN, 10, _b_marco),
            ]

        _tbl_conceptosEncabezado = self.helper_crearTabla(
            LL_datos = [
                [
                    self.HELPER_CELDA(
                        self.letra_, self.aCen, self.colorN, 10, _b_marco,
                        _dbTabla.claveprodserv.label, None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aCen, self.colorN, 10, _b_marco,
                        _dbTabla.noidentificacion.label, None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aCen, self.colorN, 10, _b_marco,
                        _dbTabla.cantidad.label, None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aIzq, self.colorN, 10, _b_marco,
                        _dbTabla.descripcion.label, None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aCen, self.colorN, 10, _b_marco,
                        _dbTabla.unidad.label, None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aCen, self.colorN, 10, _b_marco,
                        _dbTabla.valorunitario.label, None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aCen, self.colorN, 10, _b_marco,
                        _dbTabla.descuento.label, None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aCen, self.colorN, 10, _b_marco,
                        _dbTabla.importe.label, None
                        ),
                    ],
                ],
            LD_infoColumnas = _tbl_conceptoColumnas,
            n_offsetAlto = _D_return.n_offset_vertical,
            D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
            D_marco = self.HELPER_MARCO(True, True, True, True, 1, self.colorN)
            )

        _tbl_conceptos = self.helper_crearTabla(
            LL_datos = [
                [
                    Storage(s_campo = _dbTabla.claveprodserv.name),
                    Storage(s_campo = _dbTabla.noidentificacion.name, E_tipoletra = self.letraB),
                    Storage(s_campo = _dbTabla.cantidad.name, x_formato = self.FORMATEA_DINERO),
                    Storage(s_campo = _dbTabla.descripcion.name, E_tipoletra = self.letraB, b_ajustarTexto = False),
                    Storage(s_campo = _dbTabla.unidad.name),
                    Storage(s_campo = _dbTabla.valorunitario.name, x_formato = self.FORMATEA_DINERO),
                    Storage(s_campo = _dbTabla.descuento.name, x_formato = self.FORMATEA_DINERO),
                    Storage(s_campo = _dbTabla.importe.name, x_formato = self.FORMATEA_DINERO),
                    ],
                ],
            LD_infoColumnas = _tbl_conceptoColumnas,
            n_offsetAlto = 0,
            D_fondo = self.HELPER_FONDO(0, [self.colorGT, self.colorT], self.colorN),
            D_marco = self.HELPER_MARCO(False, False, True, True, 1, self.colorN)
            )

        _tbl_conceptoImpuestos = self.helper_crearTabla(
            LL_datos = [
                [
                    self.HELPER_CELDA(
                        self.letra_, self.aDer, self.colorN, 10, _b_marco,
                        None, _dbTabla.id.name, x_formato = formatea_impuestoConcepto
                        ),
                    ],
                ],
            LD_infoColumnas = [
                Storage(
                    n_ancho_pje = 1,
                    # E_alineacion = self.aDer,
                    D_marco = None
                    )
                ],
            n_offsetAlto = 0,
            D_fondo = self.HELPER_FONDO(0, [self.colorGT, self.colorT], self.colorN),
            D_marco = self.HELPER_MARCO(False, False, True, True, 1, self.colorN)
            )

        # Obtiene el offset horizontal para la segunda tabla
        _LD_offsetColumna = self.helper_offsetParaColumnas([Storage(n_ancho_pje = 0.75)])

        _tbl_conceptosResumen = self.helper_crearTabla(
            LL_datos = [
                [
                    self.HELPER_CELDA(
                        self.letra_, self.aDer, self.colorN, 10, _b_marco,
                        "Descuentos:", None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aDer, self.colorN, 10, _b_marco,
                        self._dbRow_cfdi.descuento, None,
                        x_formato = self.FORMATEA_DINERO
                        ),
                    ],
                [
                    self.HELPER_CELDA(
                        self.letra_, self.aDer, self.colorN, 10, _b_marco,
                        "Subtotal:", None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aDer, self.colorN, 10, _b_marco,
                        self._dbRow_cfdi.subtotal, None,
                        x_formato = self.FORMATEA_DINERO
                        ),
                    ],
                [
                    self.HELPER_CELDA(
                        self.letra_, self.aDer, self.colorN, 10, _b_marco,
                        "Impuestos:", None
                        ),
                    self.HELPER_CELDA(
                        self.letra_, self.aDer, self.colorN, 10, _b_marco,
                        (self._dbRow_cfdi.totalimpuestostrasladados or 0)
                        + (self._dbRow_cfdi.totalimpuestosretenidos or 0),
                        None, x_formato = self.FORMATEA_DINERO
                        ),
                    ],
                [
                    self.HELPER_CELDA(
                        self.letraB, self.aDer, self.colorN, 10, _b_marco,
                        "Total:", None
                        ),
                    self.HELPER_CELDA(
                        self.letraB, self.aDer, self.colorN, 10, _b_marco,
                        self._dbRow_cfdi.total, None,
                        D_marco = self.HELPER_MARCO(True, False, False, False, 1, self.colorN),
                        x_formato = self.FORMATEA_DINERO
                        ),
                    ],
                ],
            LD_infoColumnas = [
                Storage(
                    n_ancho_pje = 0.5,
                    # E_alineacion = self.aDer,
                    D_marco = None
                    ),
                Storage(
                    n_ancho_pje = 0.5,
                    # E_alineacion = self.aDer,
                    D_marco = None
                    )
                ],
            n_offsetAlto = 0,
            n_offsetDerecha = _LD_offsetColumna[0].n_ancho,
            D_marco = self.HELPER_MARCO(True, False, False, False, 1, self.colorN)
            )

        _D_impuestos = Storage()
        _dbRows_impuestos_tras = dbc01(
            (dbc01.tcfdi_conceptos.cfdi_id == self._dbRow_cfdi.id)
            & (dbc01.tcfdi_concepto_impuestostrasladados.cfdi_concepto_id == dbc01.tcfdi_conceptos.id)
            ).select(
                dbc01.tcfdi_concepto_impuestostrasladados.ALL
                )
        for _dbRow_impuesto in _dbRows_impuestos_tras:
            _s_index = str(_dbRow_impuesto.impuesto_id) + "_" + str(_dbRow_impuesto.tasaocuota)
            if _s_index in _D_impuestos:
                _D_impuestos[_s_index].importe_acumulado += _dbRow_impuesto.importe or 0
                _D_impuestos[_s_index].base_acumulado += _dbRow_impuesto.base or 0
            else:
                _D_impuestos[_s_index] = Storage(
                    impuesto_id = _dbRow_impuesto.impuesto_id,
                    impuesto = _dbRow_impuesto.impuesto,
                    impuesto_desc = db01.timpuestos(_dbRow_impuesto.impuesto_id).descripcion,
                    tasaocuota = _dbRow_impuesto.tasaocuota or 0,
                    tipofactor = _dbRow_impuesto.tipofactor,
                    importe_acumulado = _dbRow_impuesto.importe or 0,
                    base_acumulado = _dbRow_impuesto.base or 0
                    )
            # Se organiza la información de los impuestos en base al id del concepto, con el objetivo
            #  de imprimir el impuesto después de cada concepto
            if _dbRow_impuesto.cfdi_concepto_id in self._D_conceptoImpuestos:
                self._D_conceptoImpuestos[_dbRow_impuesto.cfdi_concepto_id][_s_index] = _D_impuestos[_s_index]
            else:
                self._D_conceptoImpuestos[_dbRow_impuesto.cfdi_concepto_id] = {_s_index: _D_impuestos[_s_index]}

        _dbRows_impuestos_ret = dbc01(
            (dbc01.tcfdi_conceptos.cfdi_id == self._dbRow_cfdi.id)
            & (dbc01.tcfdi_concepto_impuestosretenidos.cfdi_concepto_id == dbc01.tcfdi_conceptos.id)
            ).select(
                dbc01.tcfdi_concepto_impuestosretenidos.ALL
                )
        for _dbRow_impuesto in _dbRows_impuestos_ret:
            _s_index = str(_dbRow_impuesto.impuesto_id) + "_" + str(_dbRow_impuesto.tasaocuota)
            if _s_index in _D_impuestos:
                _D_impuestos[_s_index].importe_acumulado += _dbRow_impuesto.importe or 0
                _D_impuestos[_s_index].base_acumulado += _dbRow_impuesto.base or 0
            else:
                _D_impuestos[_s_index] = Storage(
                    impuesto_id = _dbRow_impuesto.impuesto_id,
                    impuesto = _dbRow_impuesto.impuesto,
                    impuesto_desc = db01.timpuestos(_dbRow_impuesto.impuesto_id).descripcion,
                    tasaocuota = _dbRow_impuesto.tasaocuota or 0,
                    tipofactor = _dbRow_impuesto.tipofactor,
                    importe_acumulado = _dbRow_impuesto.importe or 0,
                    base_acumulado = _dbRow_impuesto.base or 0
                    )
            # Se organiza la información de los impuestos en base al id del concepto, con el objetivo
            #  de imprimir el impuesto después de cada concepto
            if _dbRow_impuesto.cfdi_concepto_id in self._D_conceptoImpuestos:
                self._D_conceptoImpuestos[_dbRow_impuesto.cfdi_concepto_id][_s_index] = _D_impuestos[_s_index]
            else:
                self._D_conceptoImpuestos[_dbRow_impuesto.cfdi_concepto_id] = {_s_index: _D_impuestos[_s_index]}

        _LL_datosImpuestos = []
        for _s_index in _D_impuestos:
            _LL_datosImpuestos.append(
                [
                    self.HELPER_CELDA(
                        self.letraB, self.aCen, self.colorN, 10, _b_marco,
                        "Impuesto: %(impuesto)s %(impuesto_desc)s Base: %(base_acumulado).2f TipoFactor :%(tipofactor)s TasaOCuota: %(tasaocuota).3f" %
                        _D_impuestos[_s_index],
                        None
                        ),
                 
                    ]
                )
            
            _LL_datosImpuestos.append(
                [
                    self.HELPER_CELDA(
                        self.letraB, self.aIzq, self.colorN, 10, _b_marco,
                        "IMPORTE CON LETRA: %s %s" %
                        (STV_FWK_LIB.CONVIERTE_A_LETRAS(self._dbRow_cfdi.total).upper(),
                        self._dbRow_cfdi.moneda),
                        None,
                        ),
                    ],
                )
    
            _tbl_conceptosImpuestos = self.helper_crearTabla(
                LL_datos = _LL_datosImpuestos,
                LD_infoColumnas = [
                    Storage(
                        n_ancho_pje = 1,
                        # E_alineacion = self.aCen,
                        D_marco = None
                        ),
                    ],
                n_offsetAlto = 0,
                n_offsetDerecha = 0,
                n_ancho = _LD_offsetColumna[0].n_ancho,
                D_marco = self.HELPER_MARCO(True, False, False, False, 1, self.colorN)
                )

        _D_return.n_offset_vertical = _tbl_conceptosImpuestos.n_offsetAlto \
            if _tbl_conceptosImpuestos.n_offsetAlto > _tbl_conceptosResumen.n_offsetAlto \
            else _tbl_conceptosResumen.n_offsetAlto
        self.L_conceptosImpuestos = _tbl_conceptosImpuestos.L_elementos
        self.L_conceptosEncabezado = _tbl_conceptosEncabezado.L_elementos
        self.L_conceptos = _tbl_conceptos.L_elementos
        self.L_conceptosResumen = _tbl_conceptosResumen.L_elementos
        self.L_conceptoImpuestos = _tbl_conceptoImpuestos.L_elementos

        return _D_return

    def define_relacionados(self, n_offset_vertical = 0):
        # noinspection DuplicatedCode
        _D_return = FUNC_RETURN(
            n_offset_vertical = n_offset_vertical
            )

        _dbTabla = dbc01.tcfdi_relacionados
        _b_marco = False

        _dbRows_relacionados = self._dbRow_cfdi.tcfdi_relacionados.select()

        if _dbRows_relacionados:
            _D_texto = Storage(
                s_texto = "CFDI Relacionados por %s-%s" % (
                    self._dbRow_cfdi.tiporelacion,
                    db01.ttiposrelaciones(self._dbRow_cfdi.tiporelacion_id).descripcion
                    if self._dbRow_cfdi.tiporelacion_id else ""
                    ),
                D_posicion = STV_FWK_REPORT_PDF.POSICION(0, n_offset_vertical),
                E_alineacion = self.aCen,
                E_tipoletra = self.letraB,
                n_letratamanio = 12,
                n_ancho = None  # Para ancho máximo
                )

            _O_texto = self._configurar_objeto_texto(**_D_texto)
            _D_return.n_offset_vertical = _O_texto.n_tamanioAlto

            _LL_datosRelacionados = []
            for _dbRow_relacionado in _dbRows_relacionados:
                _LL_datosRelacionados.append(
                    [
                        self.HELPER_CELDA(
                            self.letra_, self.aCen, self.colorN, 10, _b_marco,
                            _dbRow_relacionado.uuid, None
                            ),
                        ]
                    )

            _tbl_relacionados = self.helper_crearTabla(
                LL_datos = _LL_datosRelacionados,
                LD_infoColumnas = [
                    Storage(
                        n_ancho_pje = 1,
                        # E_alineacion = self.aCen,
                        D_marco = None
                        ),
                    ],
                n_offsetAlto = _D_return.n_offset_vertical,
                n_offsetDerecha = 0,
                n_ancho = None,
                # D_marco = self.HELPER_MARCO(True, True, True, True, 1, self.colorN)
                )

            _D_return.n_offset_vertical = _tbl_relacionados.n_offsetAlto
            self.L_relacionados = _O_texto.L_elementos + _tbl_relacionados.L_elementos

        else:
            # Si no hay CFDI relacionados
            _D_texto = Storage(
                s_texto = "No hay CFDI relacionados",
                D_posicion = STV_FWK_REPORT_PDF.POSICION(0, n_offset_vertical),
                E_alineacion = self.aCen,
                E_tipoletra = self.letraB,
                n_letratamanio = 12,
                n_ancho = None  # Para ancho máximo
                )

            _O_texto = self._configurar_objeto_texto(**_D_texto)
            _D_return.n_offset_vertical = _O_texto.n_offsetAlto
            self.L_relacionados = _O_texto.L_elementos

        return _D_return

    def define_pagare(self, n_offset_vertical = 0):
        # noinspection DuplicatedCode
        _D_return = FUNC_RETURN(
            n_offset_vertical = n_offset_vertical
            )

        _b_marco = False
        _D_texto2 = Storage(
            s_texto = ("BUENO POR $%.2f") % (self._dbRow_cfdi.total),
            D_posicion = STV_FWK_REPORT_PDF.POSICION(0, _D_return.n_offset_vertical),
            E_alineacion = self.aDer,
            E_tipoletra = self.letraB,
            n_letratamanio = 10,
            n_ancho = None,  # Para ancho máximo
            #D_marco = self.HELPER_MARCO(True, True, True, True, 1, self.colorG),
            #D_fondo = self.HELPER_FONDO(0, self.colorGT, self.colorN),
            )
   
        _O_texto2 = self._configurar_objeto_texto(** _D_texto2) 
           
        # Se imprime el pagaré
        _dt_fechaVencimiento = self._dbRow_cfdi.fecha + datetime.timedelta(days = self._dbRow_cfdi.credito_plazo or 0)

        _D_texto = Storage(
            s_texto = ("EN MAZATLAN A %d DE %s DEL %d \n" 
                       "DEBE(MOS) Y PAGARE(MOS) INCONDICIONALMENTE POR ESTE PAGARE A LA ORDEN DE ACUMULADORES "
                       "Y LLANTAS GALLARDO SA DE CV EN MAZATLAN O EN CUALQUIER OTRA PLAZA A ELECCION DE NUESTRO "
                       "ACREEDOR POR EL IMPORTE DE LAS MERCANCIAS Y/O POR LOS SERVICIOS RECIBIDOS A ENTERA "
                       "SATISFACCION.\n\n"
                       "SI ESTE PAGARE NO FUERA CUBIERTO A SU VENCIMIENTO CAUSARA INTERESES MORATORIOS AL %.0f%% "
                       "MENSUAL SIN QUE POR ESTO SE ENTIENDA PRORROGADO EL PLAZO.\n \n"
                       "EL DIA %d DE %s DEL %d LA CANTIDAD DE: %s %s.\n \n"
                       "ACEPTAMOS ______________________________________.") % (
                request.browsernow.day,
                STV_FWK_LIB.MESES[request.browsernow.month - 1].upper(),
                request.browsernow.year,
                self._dbRow_empresa.interesmoratoriopagare or 5,
                _dt_fechaVencimiento.day,
                STV_FWK_LIB.MESES[_dt_fechaVencimiento.month - 1].upper(),
                _dt_fechaVencimiento.year,
                STV_FWK_LIB.CONVIERTE_A_LETRAS(self._dbRow_cfdi.total).upper(),
                self._dbRow_cfdi.moneda
                )
            ,
            D_posicion = STV_FWK_REPORT_PDF.POSICION(0, _D_return.n_offset_vertical),
            E_alineacion = self.aIzq,
            E_tipoletra = self.letra_,
            n_letratamanio = 9,
            n_ancho = None,  # Para ancho máximo
            D_marco = self.HELPER_MARCO(True, True, True, True, 1, self.colorG)
            )
    
        _O_texto = self._configurar_objeto_texto(** _D_texto)
        _D_return.n_offset_vertical = _O_texto.n_offsetAlto
        self.L_pagare = _O_texto.L_elementos + _O_texto2.L_elementos

        return _D_return

    def crear_cadenaoriginal(self):
        if self._dbRow_cfdi2:
            _s_cadenaOriginal = ("||%(versiontimbre)s|%(uuid)s|%(fechatimbrado_str)s|%(rfcprovcertif)s" \
                                "|%(sellocfd)s|%(nocertificadosat)s") \
                                % Storage(
                                    uuid = self._dbRow_cfdi.uuid,
                                    fechatimbrado_str = self._dbRow_cfdi.fechatimbrado_str,
                                    **self._dbRow_cfdi2.as_dict()
                                    )
        else:
            _s_cadenaOriginal = "Información de timbre no encontrada"
        return _s_cadenaOriginal

    def define_certificado(self, n_offset_vertical = 0):
        # noinspection DuplicatedCode
        _D_return = FUNC_RETURN(
            n_offset_vertical = n_offset_vertical
            )

        _b_marco = False

        if (self._dbRow_cfdi.sat_status != TCFDIS.SAT_STATUS.NO_DEFINIDO) and self._dbRow_cfdi2:

            # Obtiene el offset horizontal para las tabla
            _LD_offsetColumna = self.helper_offsetParaColumnas([Storage(n_ancho_pje = 0.2), Storage(n_ancho_pje = 0.8)])

            _s_url = ("https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx?id={uuid}&" +
                      "re={emisorrfc}&rr={receptorrfc}&tt={total}&fe={sello}").format(
                uuid = self._dbRow_cfdi.uuid,
                emisorrfc = self._dbRow_cfdi.emisorrfc.replace("-", ""),
                receptorrfc = self._dbRow_cfdi.receptorrfc.replace("-", ""),
                total = ("%.6f" % self._dbRow_cfdi.total).zfill(25),
                sello = self._dbRow_cfdi2.sellocfd[-8:] if self._dbRow_cfdi2 else "",
                )

            import qrcode
            _O_img = qrcode.make(_s_url)

            _D_tbl_certificadoQR = self.helper_crearTabla(
                LL_datos = [
                    [
                        self.HELPER_CELDA(
                            self.letraB, self.aCen, self.colorN, 10, _b_marco,
                            None, None, x_imagen = _O_img, # D_fondo = self.colorV
                            )
                        ],
                    ],
                LD_infoColumnas = [
                    self.HELPER_COLUMNA(1, self.letra_, self.aIzq, self.colorN, 10, _b_marco),
                    ],
                n_ancho = _LD_offsetColumna[0].n_ancho,
                n_offsetDerecha = 0,
                n_offsetAlto = _D_return.n_offset_vertical,
                # D_marco = self.HELPER_MARCO(True, True, True, True, 1, self.colorA)
                )

            _D_tbl_certificado1 = self.helper_crearTabla(
                LL_datos = [
                    [
                        self.HELPER_CELDA(
                            self.letraB, self.aCen, self.colorN, 10, _b_marco,
                            'Lugar Expedición', None,
                            D_marco = self.HELPER_MARCO(False, True, False, False, 1, self.colorN)
                            ),
                        self.HELPER_CELDA(
                            self.letraB, self.aCen, self.colorN, 10, _b_marco,
                            'Certificado Digital SAT', None,
                            D_marco = self.HELPER_MARCO(False, True, False, False, 1, self.colorN)
                            ),
                        self.HELPER_CELDA(
                            self.letraB, self.aCen, self.colorN, 10, _b_marco,
                            'Fecha de Certificado', None,
                            D_marco = self.HELPER_MARCO(False, True, False, False, 1, self.colorN)
                            )
                        ],
                    [
                        self.HELPER_CELDA(
                            self.letra_, self.aCen, self.colorN, 10, _b_marco,
                            self._dbRow_cfdi.lugarexpedicion, None
                            ),
                        self.HELPER_CELDA(
                            self.letra_, self.aCen, self.colorN, 10, _b_marco,
                            self._dbRow_cfdi2.nocertificadosat, None
                            ),
                        self.HELPER_CELDA(
                            self.letra_, self.aCen, self.colorN, 10, _b_marco,
                            self._dbRow_cfdi.fechatimbrado_str, None
                            )
                        ],
                    [
                        self.HELPER_CELDA(
                            self.letraB, self.aCen, self.colorN, 10, _b_marco,
                            'Leyenda', None,
                            D_marco = self.HELPER_MARCO(False, True, False, False, 1, self.colorN)
                            ),
                        self.HELPER_CELDA(
                            self.letraB, self.aCen, self.colorN, 10, _b_marco,
                            'RFC de Provedor Certificado', None,
                            D_marco = self.HELPER_MARCO(False, True, False, False, 1, self.colorN)
                            ),
                        self.HELPER_CELDA(
                            self.letraB, self.aCen, self.colorN, 10, _b_marco,
                            'Version TFD', None,
                            D_marco = self.HELPER_MARCO(False, True, False, False, 1, self.colorN)
                            )
                        ],
                    [
                        self.HELPER_CELDA(
                            self.letra_, self.aCen, self.colorN, 10, _b_marco,
                            '', None
                            ),
                        self.HELPER_CELDA(
                            self.letra_, self.aCen, self.colorN, 10, _b_marco,
                            self._dbRow_cfdi2.rfcprovcertif, None
                            ),
                        self.HELPER_CELDA(
                            self.letra_, self.aCen, self.colorN, 10, _b_marco,
                            self._dbRow_cfdi2.versiontimbre, None
                            )
                        ],
                    ],
                LD_infoColumnas = [
                    self.HELPER_COLUMNA(0.3, self.letra_, self.aCen, self.colorN, 10, _b_marco),
                    self.HELPER_COLUMNA(0.4, self.letra_, self.aCen, self.colorN, 10, _b_marco),
                    self.HELPER_COLUMNA(0.3, self.letra_, self.aCen, self.colorN, 10, _b_marco),
                    ],
                n_ancho = _LD_offsetColumna[1].n_ancho,
                n_offsetDerecha = _LD_offsetColumna[1].n_offsetDerecha,
                n_offsetAlto = _D_return.n_offset_vertical
                )

            _D_tbl_certificado2 = self.helper_crearTabla(
                LL_datos = [
                       [
                        self.HELPER_CELDA(
                            self.letraB, self.aIzq, self.colorN, 10, _b_marco,
                            D_marco = self.HELPER_MARCO(False, True, False, False, 1, self.colorN)
                            )
                        ],
                    
                    [
                        self.HELPER_CELDA(
                            self.letraB, self.aIzq, self.colorN, 8, _b_marco,
                            'Cadena Original del Timbre', None,
                            D_marco = self.HELPER_MARCO(False, True, False, False, 1, self.colorN),
                            D_fondo = self.HELPER_FONDO(0, self.colorGT, self.colorN),
                            )
                        ],
                    [
                        self.HELPER_CELDA(
                            self.letra_, self.aIzq, self.colorN, 7, _b_marco,
                            self.crear_cadenaoriginal(), None, b_ajustarTexto = False,
                            D_marco = self.HELPER_MARCO(False, True, False, False, 1, self.colorN),
                            D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
                            )
                        ],
                    [
                        self.HELPER_CELDA(
                            self.letraB, self.aIzq, self.colorN, 8, _b_marco,
                            'Sello Digital del Emisor', None,
                            D_marco = self.HELPER_MARCO(False, True, False, False, 1, self.colorN),
                            D_fondo = self.HELPER_FONDO(0, self.colorGT, self.colorN),
                            )
                        ],
                    [
                        self.HELPER_CELDA(
                            self.letra_, self.aIzq, self.colorN, 7, _b_marco,
                            self._dbRow_cfdi2.sellocfd, None, b_ajustarTexto = False,
                            D_marco = self.HELPER_MARCO(False, True, False, False, 1, self.colorN),
                            D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
                            )
                        ],
                    [
                        self.HELPER_CELDA(
                            self.letraB, self.aIzq, self.colorN, 8, _b_marco,
                            'Sello Digital del SAT', None,
                            D_marco = self.HELPER_MARCO(False, True, False, False, 1, self.colorN),
                            D_fondo = self.HELPER_FONDO(0, self.colorGT, self.colorN),
                            )
                        ],
                    [
                        self.HELPER_CELDA(
                            self.letra_, self.aIzq, self.colorN, 7, _b_marco,
                            self._dbRow_cfdi2.sellosat, None, b_ajustarTexto = False,
                            D_fondo = self.HELPER_FONDO(0, self.colorVT, self.colorN),
                            )
                        ],
                    ],
                LD_infoColumnas = [
                    self.HELPER_COLUMNA(1, self.letra_, self.aIzq, self.colorN, 10, _b_marco),
                    ],
                n_ancho = _LD_offsetColumna[1].n_ancho,
                n_offsetDerecha = _LD_offsetColumna[1].n_offsetDerecha,
                n_offsetAlto = _D_tbl_certificado1.n_offsetAlto
                # D_marco = self.HELPER_MARCO(True, True, True, True, 1, self.colorA)
                )

            _D_return.n_offset_vertical = _D_tbl_certificado2.n_offsetAlto
            self.L_certificado = _D_tbl_certificado1.L_elementos + _D_tbl_certificado2.L_elementos \
                                 + _D_tbl_certificadoQR.L_elementos

        else:
            pass

        return _D_return

    def piepagina_paginacion(self, row):
        if self.n_paginas < self._O_reporte.pagenumber:
            self.n_paginas = self._O_reporte.pagenumber
        else:
            pass
        return "Página %d de %d" % (self._O_reporte.pagenumber, self.n_paginas)

    def define_piepagina(self, n_offset_vertical = 0):
        # noinspection DuplicatedCode
        _D_return = FUNC_RETURN(
            n_offset_vertical = n_offset_vertical
            )

        _b_marco = False

        # Obtiene el offset horizontal para las tabla
        _LD_offsetColumna = self.helper_offsetParaColumnas([Storage(n_ancho_pje = 0.3), Storage(n_ancho_pje = 0.7)])

        _O_textoPagina = self._configurar_objeto_texto(
            s_texto = "",
            x_getvalue = self.piepagina_paginacion,
            D_posicion = STV_FWK_REPORT_PDF.POSICION(_LD_offsetColumna[0].n_offsetDerecha, _D_return.n_offset_vertical),
            E_alineacion = self.aCen,
            E_tipoletra = self.letra_,
            n_letratamanio = 10,
            n_ancho = _LD_offsetColumna[0].n_ancho
            )

        _O_texto = self._configurar_objeto_texto(
            s_texto = "Este documento es una representación impresa de un CFDI versión 3.3",
            D_posicion = STV_FWK_REPORT_PDF.POSICION(_LD_offsetColumna[1].n_offsetDerecha, _D_return.n_offset_vertical),
            E_alineacion = self.aCen,
            E_tipoletra = self.letra_,
            n_letratamanio = 10,
            n_ancho = _LD_offsetColumna[1].n_ancho
            )

        _D_return.n_offset_vertical = _O_texto.n_tamanioAlto

        self.L_piePagina = _O_textoPagina.L_elementos + _O_texto.L_elementos

        return _D_return

    def generar_pdf(self, s_nombreArchivo = None, s_tituloBrowser = None, **D_otros):
        _D_return = FUNC_RETURN()

        _s_tituloDocumento = "FACTURA SIN ESTATUS VÁLIDO"
        if self._dbRow_cfdi.sat_status == TCFDIS.SAT_STATUS.NO_DEFINIDO:
            _s_tituloDocumento = "FACTURA SIN TIMBRAR"
        elif self._dbRow_cfdi.sat_status == TCFDIS.SAT_STATUS.VIGENTE:
            _s_tituloDocumento = "FACTURA"
        elif self._dbRow_cfdi.sat_status == TCFDIS.SAT_STATUS.CANCELADO:
            _s_tituloDocumento = "FACTURA CANCELADA"
        else:
            pass

        if self._dbRow_cfdi.receptorrfc == "AAA-010101-AAA":
            _s_tituloDocumento += " [Timbrado de prueba]"
        else:
            pass

        _D_results = self.define_encabezado(self.n_msgError_offset, _s_tituloDocumento)
        self.definir_paginaEncabezado(self.L_msgError + self.L_encabezado)

        _D_results = self.define_emisor()
        _D_results = self.define_receptor(_D_results.n_offset_vertical + 5)
        self.definir_reporteEncabezado(self.L_emisor + self.L_receptor)

        if self._dbRow_cfdi.tipocomprobante_id in (
                TTIPOSCOMPROBANTES.INGRESO,
                TTIPOSCOMPROBANTES.EGRESO,
                TTIPOSCOMPROBANTES.TRASLADO,
                ):

            _D_results = self.define_conceptos(5)
            self.agrupar(
                L_elementosEncabezado = self.L_conceptosEncabezado,
                L_elementosPie = None,
                s_porCampo = "cfdi_id"
                )
            self.agrupar(
                L_elementosEncabezado = None,
                L_elementosPie = self.L_conceptoImpuestos,
                s_porCampo = "id"
                )
            self.agrupar(
                L_elementosEncabezado = None,
                L_elementosPie = self.L_conceptosImpuestos + self.L_conceptosResumen,
                s_porCampo = "cfdi_id"
                )
            self.definir_reporteDetalle(self.L_conceptos)

        elif self._dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.PAGO:

            # TODO imprimir información de pagos
            pass

        else:
            pass

        _D_results = self.define_relacionados()
        _D_results = self.define_pagare(_D_results.n_offset_vertical + 5)
        _D_results = self.define_certificado(_D_results.n_offset_vertical + 5)
        self.definir_reportePie(
            self.L_relacionados + self.L_pagare + self.L_certificado
            )

        _D_results = self.define_piepagina()
        self.definir_paginaPie(
            self.L_piePagina
            )

        _dbRows_conceptos = self._dbRow_cfdi.tcfdi_conceptos.select()

        # Se ejecuta una vez para contar las páginas
        _s_nombreArchivo = super(REPORTE_CFDI, self).generar_pdf(
            s_nombreArchivo or 'archivo.pdf',
            s_tituloBrowser or 'CFDI',
            _dbRows_conceptos
            )

        super(REPORTE_CFDI, self).limpia_reporte()

        _s_nombreArchivo = super(REPORTE_CFDI, self).generar_pdf(
            s_nombreArchivo or 'archivo.pdf',
            s_tituloBrowser or 'CFDI',
            _dbRows_conceptos
            )

        response.stream(_s_nombreArchivo, request = request)

        _D_return.combinar(_D_results)

        return _D_return
