# -*- coding: utf-8 -*-

class STV_FWK2_VALIDACIONES:
    
    class APLICACION_W2P:
        
        @classmethod
        def OPCIONES(cls):

            _L_opciones = []

            # Aplicaciones que se dejan fuera de las opciones
            _L_filterApps = ['backoffice', 'admin', 'acceso']
            
            for _s_dirName in os.listdir(os.path.join('applications')):
                if (
                    not (os.path.isfile(os.path.join('applications', _s_dirName)))
                    and not(_s_dirName.startswith("_"))
                    and not(_s_dirName.startswith("."))
                    and (_s_dirName not in _L_filterApps) 
                    ):
                    _L_opciones.append(_s_dirName)
                else:
                    pass
            
            return _L_opciones
    pass

    class CONTROLADOR_W2P:
        
        @classmethod
        def OPCIONES(cls, s_aplicacion_w2p):
            
            # Buscamos cualquier archivo que comience con el wildchar en la aplicacion sAppSrc y el subdirectorio sNameDir.
            _s_pathSrc = os.path.join('applications', str(s_aplicacion_w2p), 'controllers')
        
            _L_opciones = []
        
            _L_filterControllers = ['appadmin.py']
        
            for _s_fullFileName in os.listdir(_s_pathSrc):
                _s_fileName = os.path.basename(_s_fullFileName)
                if ( 
                    (os.path.isfile(os.path.join(_s_pathSrc, _s_fileName)))
                    and (_s_fileName not in _L_filterControllers) 
                    ):
                    _s_fileName = os.path.splitext(_s_fileName)[0]
                    _L_opciones.append(_s_fileName)
                else:
                    pass
        
            return _L_opciones

    class RUTINA_W2P:
        
        @classmethod
        def OPCIONES(cls, s_aplicacion_w2p, s_controlador_w2p):
            
            # Buscamos cualquier archivo que comience con el wildchar en la aplicacion sAppSrc
            #  y el subdirectorio sNameDir.
            _s_pathSrc = os.path.join(
                'applications', str(s_aplicacion_w2p), 'controllers', str(s_controlador_w2p) + '.py'
                )
            _L_opciones = []
        
            if os.path.isfile(_s_pathSrc):

                _re_expresion = re.compile(
                    '^def\s(?P<funcion>[^_][\w\W]*?)\(\s*(?P<parameters>.*)\s*(?P<final>\)\:)(\s*#\s*)?(?P<comando>.*)'
                    )
                _re_expresionWBS = re.compile('^\@service\.soap\(')

                _b_esWebService = False

                _O_archivo = open(_s_pathSrc, encoding="utf-8")
                for _s_linea in _O_archivo:
                    _re_grupo = _re_expresion.search(_s_linea)
                    _re_grupoWBS = _re_expresionWBS.search(_s_linea)
                    _b_esWebService = True if _re_grupoWBS else _re_grupoWBS  # Cuando es True, mantener el True

                    if _re_grupo and _re_grupo.group('funcion'):
                        if _re_grupo.group('parameters'):
                            if _b_esWebService:
                                _L_opciones.append(_re_grupo.group('funcion'))
                            else:
                                pass
                        else:
                            _L_comandos = _re_grupo.group('comando').split(",") if _re_grupo.group('comando') else []
                            if "cmd_noEsForma" in _L_comandos:
                                pass
                            else:
                                _L_opciones.append(_re_grupo.group('funcion'))
                    else:
                        pass
            else:
                pass
        
            return _L_opciones

        @classmethod
        def INFORMACION(cls, s_aplicacion_w2p, s_controlador_w2p, s_rutina_w2p, s_permiso = None):
            """ Busca en la rutina los permiso y los regresa como lista
            
            @param s_aplicacion_w2p: 
            @param s_controlador_w2p: 
            @param s_rutina_w2p: 
            @param s_permiso: Opcionalmente búsca únicamente el permiso especificado
            """
            
            _D_return = FUNC_RETURN(
                s_descripcion = "",
                L_keywords = [],
                L_permisos = []
                )
            
            # Buscamos cualquier archivo que comience con el wildchar en la aplicacion sAppSrc
            #  y el subdirectorio sNameDir.
            _s_pathSrc = os.path.join(
                'applications', str(s_aplicacion_w2p), 'controllers', str(s_controlador_w2p) + '.py'
                )
                
            if os.path.isfile(_s_pathSrc):

                _re_expresionFuncion = re.compile('^def\s(?P<funcion>[^_][\w\W]*?)\(')
                _re_expresionPermiso = re.compile(
                    '^\s*\@permiso\s(?P<permiso>\w*)(\:(\[(?P<icono>.*)\])?\s*(?P<descripcion>.*))?\s*$'
                    )
                _re_expresionKeyboard = re.compile(
                    '^\s*\@keyword\s(?P<keyword>\w*)(\:(\[(?P<icono>.*)\])?\s*(?P<descripcion>.*))?\s*$'
                    )
                _re_expresionDescripcion = re.compile('^\s*(\@descripcion|\@description)\:?\s(?P<descripcion>.*)\s*$')
                _re_expresionFuncionFin = re.compile(
                    '^def\s(?P<funcion>[\w\W]*?)\('
                    )  # Se busca el final de una función utilizando el def de la siguiente

                _b_funcionEncontrada = False

                _D_permisos_datos = request.stv_fwk_permissions.getDict_varName()

                _O_archivo = open(_s_pathSrc, encoding = 'utf-8')
                for _s_linea in _O_archivo:
                    if not _b_funcionEncontrada:
                        _re_grupo = _re_expresionFuncion.search(_s_linea)
                        if _re_grupo and _re_grupo.group('funcion') and (_re_grupo.group('funcion') == s_rutina_w2p):
                            _b_funcionEncontrada = True
                        else:
                            pass
                    else:
                        _re_grupo = _re_expresionFuncionFin.search(_s_linea)
                        if _re_grupo and _re_grupo.group('funcion'):
                            # Encontro otra funcion que ya no es importante para esta búsqueda
                            break
                        else:
                            _re_grupo = _re_expresionPermiso.search(_s_linea)
                            if _re_grupo and _re_grupo.group('permiso'):
                                if not s_permiso or (s_permiso == _re_grupo.group('permiso')):
                                    if not(_re_grupo.group('permiso') in _D_permisos_datos):
                                        _D_permiso_datos = Storage(
                                            icon = "",
                                            description = _re_grupo.group('permiso'),
                                            id = 0
                                            ) 
                                    else:
                                        _D_permiso_datos = _D_permisos_datos[_re_grupo.group('permiso')]
                                    _D_return.L_permisos.append(
                                        Storage(
                                            permiso = _re_grupo.group('permiso'),
                                            icono = _re_grupo.group('icono') or _D_permiso_datos.icon,
                                            descripcion = _re_grupo.group('descripcion') or _D_permiso_datos.description,
                                            permiso_id = _D_permiso_datos.id
                                            )
                                        )
                                else:
                                    pass
                            else:
                                pass

                            _re_grupo = _re_expresionKeyboard.search(_s_linea)
                            if _re_grupo and _re_grupo.group('keyword'):
                                _D_return.L_keywords.append(
                                    Storage(
                                        keyboard = _re_grupo.group('keyword'),
                                        icono = _re_grupo.group('icono') or "",
                                        descripcion = _re_grupo.group('descripcion') or _re_grupo.group('keyword'),
                                        )
                                    )
                            else:
                                pass

                            _re_grupo = _re_expresionDescripcion.search(_s_linea)
                            if _re_grupo and _re_grupo.group('descripcion'):
                                _D_return.s_descripcion += _re_grupo.group('descripcion') or ""
                            else:
                                pass
                    
            else:
                _D_return.E_return = CLASS_e_RETURN.NOK_CONDICIONES_INCORRECTAS
                _D_return.s_msgError = "Archivo no encontrado"
        
            return _D_return

    pass

class IMPORTAR_EXCEL:

    class E_FORMATO:
        NO_CAMBIAR = 0
        TEXTO = 1
        NUMERICO = 2
        FLOAT = 3
        DECIMAL = 4
        FECHA = 5
        FECHAHORA = 6

    class E_RESULTADO_VALIDACION:
        OK = 0 # Continua con el proceso default de la clase, definido por sus opciones
        INSERTARLO = 1 # Forza la inserción de un registro en dbTabla con todos los datos convertidos
        MODIFICARLO = 2 # Modifica solamente campos_columnas_importar
        IGNORARLO = 3 # Ignora utilizar el registro para algo

    def __init__(
            self,
            dbTabla = None,
            b_insertar_noRelacionados = False,
            b_ignorar_noRelacionados = False,
            b_insertarTodos = False,
            fnIdentificarHeader = None,
            alValidar = None,
            alAceptar = None
            ):
        """

        @param dbTabla: opcional tabla donde se grabarán los datos
        @type dbTabla: Table
        @param b_insertar_noRelacionados: indica, en caso de definir la tabla si los datos no relacionados se usen para
        insertar nuevos registros.
        @type b_insertar_noRelacionados: bool
        @param b_insertarTodos: indica, en caso de definir la tabla si se insertan todos los registros.
        @type b_insertarTodos: bool
        @param fnIdentificarHeader: determina si el renglon actual es el header de la tabla
        @type fnIdentificarHeader: funcion(D_rowExcel) > D_result {E_return, s_msgError, b_headersTabla}
        @param alValidar: manda información del registro actual con los datos convertidos para tomar una decisión.
        @type alValidar: function(D_registroExcel, b_fueIdentificadoAntes, b_hayVariasCoincidenciasEnTabla) > {E_RESULTADO_VALIDACION)
        @param alAceptar: manda información del registro
        @type alAceptar: function(D_registroExcel, D_result)

        """
        self._D_campos_columnas_relacionar = Storage()
        self._D_campos_columnas_importar = Storage()
        self._D_columnas_valores_ignorar = Storage()
        self._D_opciones = Storage(
            b_insertar_noRelacionados = b_insertar_noRelacionados,
            b_insertarTodos = b_insertarTodos,
            b_ignorar_noRelacionados = b_ignorar_noRelacionados,
            dbTabla = dbTabla,
            fnIdentificarHeader = fnIdentificarHeader,
            alValidar = alValidar,
            alAceptar = alAceptar
            )
        self._sheet = None
        self._L_idsEncontrados = []
        self.D_estatusAfectados = Storage(
            n_ignorados = 0,
            n_insertados = 0,
            n_modificados = 0,
            )
        return

    def agregar_campo_columna_relacion(
            self,
            n_id_columna,
            dbCampo = None,
            E_formato = None,
            s_nombreColumna = None,
            n_columna = None,  # La determina con el nombreColumna
            x_valorFijo = None,
            fnConversion = None
            ):
        """ Crea las configuración de una columna en excel que usaran su valor para encontrar el registro en dbTabla

        @param n_columna: Opcional número de columna en el archivo de excel empieza en 0
        @type n_columna: int
        @param n_id_columna: id único de la columna, este id se usará en los callbacks
        @type n_id_columna: integer
        @param dbCampo: campo de dbTabla
        @type dbCampo:
        @param E_formato:
        @type E_formato:
        @param s_nombreColumna: opcional nombre de la columna
        @type s_nombreColumna: string
        @param x_valorFijo: opcional valor fijo que se usará en ves de un dato de excel
        @type x_valorFijo:
        @param fnConversion: opcional función para convertir el valor de excel al valor a usar en dbTabla. En esta
        función es posible que D_registro contenga los valores leídos de excel, y no los convertidos.
        @type fnConversion: function(x_valor, D_columna, D_rowExcel) > x_valorConvertido
        @return:
        @rtype:
        """
        if not n_id_columna:
            n_id_columna = (
                len(self._D_campos_columnas_relacionar)
                + len(self._D_campos_columnas_importar)
                + len(self._D_columnas_valores_ignorar)
                + 1
                )
        else:
            pass

        self._D_campos_columnas_relacionar[n_id_columna] = Storage(
            n_id = n_id_columna,
            dbCampo = dbCampo,
            E_formato = E_formato,
            s_nombreColumna = (s_nombreColumna or "").strip(),
            n_columna = n_columna,
            x_valorFijo = x_valorFijo,
            fnConversion = fnConversion
            )

        return n_id_columna

    def agregar_campo_columna_importar(
            self,
            n_id_columna = None,
            dbCampo = None,
            E_formato = None,
            s_nombreColumna = None,
            n_columna = None,
            x_valorFijo = None,
            fnConversion = None,
            ):

        if not n_id_columna:
            n_id_columna = (
                len(self._D_campos_columnas_relacionar)
                + len(self._D_campos_columnas_importar)
                + len(self._D_columnas_valores_ignorar)
                + 1
                )
        else:
            pass

        self._D_campos_columnas_importar[n_id_columna] = Storage(
            n_id = n_id_columna,
            dbCampo = dbCampo,
            E_formato = E_formato,
            s_nombreColumna = (s_nombreColumna or "").strip(),
            n_columna = n_columna,
            x_valorFijo = x_valorFijo,
            fnConversion = fnConversion,
            )

        return n_id_columna

    def _importar_headers(
            self,
            L_row
            ):

        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        for _n_columna in range(0, len(L_row)):

            for _D_columna in self._D_campos_columnas_relacionar.values() + self._D_campos_columnas_importar.values():
                if _D_columna.n_columna is not None:
                    pass

                elif _D_columna.s_nombreColumna:

                    if L_row[_n_columna] == _D_columna.s_nombreColumna:
                        _D_columna.n_columna = _n_columna
                        # Continuar por si se quieren usar la columna varias veces
                    elif bool(_D_columna.dbCampo):
                        # Si existe un campo identificado
                        if _D_columna.dbCampo.name == L_row[_n_columna]:
                            # Se busca por nombre del campo
                            _D_columna.n_columna = _n_columna
                        elif _D_columna.dbCampo.label == L_row[_n_columna]:
                            # Se busca por label del campo
                            _D_columna.n_columna = _n_columna
                        else:
                            pass
                    else:
                        pass
                else:
                    pass

        # Se verifica si todas las columnas estan mapeadas correctamente
        for _D_columna in self._D_campos_columnas_relacionar.values() + self._D_campos_columnas_importar.values():
            if _D_columna.n_columna is not None:
                pass

            elif _D_columna.s_nombreColumna:
                _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                _D_return.s_msgError = "Columna no encontrada en excel %s" % _D_columna.s_nombreColumna
                break
            else:
                pass

        return _D_return

    def _importar_datos(
            self,
            D_row
            ):

        def conversion(
                D_columna,
                D_registro,
                ):

            if D_columna.x_valorFijo:
                D_registro[D_columna.n_id] = D_columna.x_valorFijo

            elif D_columna.fnConversion:
                D_registro[D_columna.n_id] = D_columna.fnConversion(
                    x_valor = D_row[D_columna.n_id],
                    D_columna = D_columna,
                    D_rowExcel = D_row
                    )

            elif D_columna.E_formato == IMPORTAR_EXCEL.E_FORMATO.TEXTO:
                if D_row[D_columna.n_id] is not None:
                    D_registro[D_columna.n_id] = str(D_row[D_columna.n_id])
                else:
                    D_registro[D_columna.n_id] = ""

            elif D_columna.E_formato == IMPORTAR_EXCEL.E_FORMATO.NUMERICO:
                try:
                    D_registro[D_columna.n_id] = int(D_row[D_columna.n_id])
                except Exception:
                    D_registro[D_columna.n_id] = int(0)

            elif D_columna.E_formato == IMPORTAR_EXCEL.E_FORMATO.FLOAT:
                try:
                    D_registro[D_columna.n_id] = float(D_row[D_columna.n_id])
                except Exception:
                    D_registro[D_columna.n_id] = float(0)

            elif D_columna.E_formato == IMPORTAR_EXCEL.E_FORMATO.DECIMAL:
                try:
                    D_registro[D_columna.n_id] = Decimal(D_row[D_columna.n_id])
                except Exception:
                    D_registro[D_columna.n_id] = Decimal(0)

            elif D_columna.E_formato in (
                    IMPORTAR_EXCEL.E_FORMATO.FECHA,
                    IMPORTAR_EXCEL.E_FORMATO.FECHAHORA,
                    ):
                if isinstance(D_row[D_columna.n_id],  (datetime.datetime, type(None))):
                    D_registro[D_columna.n_id] = D_row[D_columna.n_id]
                else:
                    raise BaseException("Campo fecha no corresponde a dato en excel")
            else:
                D_registro[D_columna.n_id] = D_row[D_columna.n_id]

            if D_columna.dbCampo:
                D_registro[D_columna.dbCampo.name] = D_registro[D_columna.n_id]
            else:
                pass

            return

        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        _D_registro = Storage()
        _L_qryAnd = []
        _D_camposActualizar = Storage()
        _D_camposInsertar = Storage()
        _L_id_excel = []  # Contendrá una identificación única del registro en excel

        for _D_columna in self._D_campos_columnas_relacionar.values():
            conversion(_D_columna, _D_registro)
            _L_id_excel.append(str(_D_registro[_D_columna.n_id]))
            if _D_columna.dbCampo:
                _L_qryAnd.append(_D_columna.dbCampo == _D_registro[_D_columna.n_id])
                if self._D_opciones.b_insertarTodos or self._D_opciones.b_insertar_noRelacionados:
                    _D_camposInsertar[_D_columna.dbCampo.name] = _D_registro[_D_columna.n_id]
                else:
                    pass
            else:
                pass

        for _D_columna in self._D_campos_columnas_importar.values():
            conversion(_D_columna, _D_registro)
            if _D_columna.dbCampo:
                _D_camposActualizar[_D_columna.dbCampo.name] = _D_registro[_D_columna.n_id]
            else:
                pass

        _b_encontradoEnTabla = False
        _b_hayVariasCoincidenciasEnTabla = False
        _b_fueIdentificadoAntes = False
        _dbRow_encontrado = None
        _dbRows_encontrados = None

        _n_id = "_".join(_L_id_excel)
        if _n_id in self._L_idsEncontrados:
            _b_fueIdentificadoAntes = True
        else:
            self._L_idsEncontrados.append(_n_id)

        if self._D_opciones.dbTabla:
            _qryAnd = True
            for _qry in _L_qryAnd:
                _qryAnd &= _qry

            _dbRows_encontrados = self._D_opciones.dbTabla._db(
                _qryAnd
                ).select(
                    self._D_opciones.dbTabla.ALL
                    )

            _b_encontradoEnTabla = (len(_dbRows_encontrados) > 0)
            _b_hayVariasCoincidenciasEnTabla = (len(_dbRows_encontrados) > 1)
            if len(_dbRows_encontrados) == 1:
                _dbRow_encontrado = _dbRows_encontrados.first()
            else:
                pass

        if self._D_opciones.alValidar:
            _D_result = self._D_opciones.alValidar(
                D_registro = _D_registro,
                dbRow_encontrado = _dbRow_encontrado,
                dbRows_encontrados = _dbRows_encontrados,
                b_fueIdentificadoAntes = _b_fueIdentificadoAntes,
                b_hayVariasCoincidenciasEnTabla = _b_hayVariasCoincidenciasEnTabla,
                b_encontradoEnTabla = _b_encontradoEnTabla
                )

        else:
            _D_result = Storage(
                E_return = stvfwk2_e_RETURN.OK,
                s_msgError = "",
                E_resultado_validacion = IMPORTAR_EXCEL.E_RESULTADO_VALIDACION.OK,
                n_id_insertado = None,
                n_modificados = 0
                )

        # Se busca por campos adicionales regresados en la validación que se tengan que actualizar también
        for _s_nombreCampo_index in _D_registro:
            if (
                    (isinstance(_s_nombreCampo_index, str))  # Se ignora si es la representación numérica de la columna
                    and (_s_nombreCampo_index not in _D_camposInsertar)
                    and (_s_nombreCampo_index not in _D_camposActualizar)
                    ):
                _D_camposActualizar[_s_nombreCampo_index] = _D_registro[_s_nombreCampo_index]
            else:
                pass

        if _D_result.E_return <= stvfwk2_e_RETURN.OK_WARNING:

            if self._D_opciones.dbTabla:
                if _D_result.E_resultado_validacion == IMPORTAR_EXCEL.E_RESULTADO_VALIDACION.IGNORARLO:
                    self.D_estatusAfectados.n_ignorados += 1

                elif _D_result.E_resultado_validacion == IMPORTAR_EXCEL.E_RESULTADO_VALIDACION.INSERTARLO:
                    _D_camposInsertar.update(_D_camposActualizar)
                    _D_result.n_id_insertado = self._D_opciones.dbTabla.insert(**_D_camposInsertar)
                    _D_registro.id = _D_result.n_id_insertado
                    self.D_estatusAfectados.n_insertados += 1

                elif _D_result.E_resultado_validacion == IMPORTAR_EXCEL.E_RESULTADO_VALIDACION.MODIFICARLO:
                    if _dbRow_encontrado:
                        _D_result.n_modificados = self._D_opciones.dbTabla._db(
                            self._D_opciones.dbTabla.id == _dbRow_encontrado.id
                            ).update(**_D_camposActualizar)
                        _D_registro.id = _dbRow_encontrado.id
                        self.D_estatusAfectados.n_modificados += 1

                    else:
                        _D_result.E_return = stvfwk2_e_RETURN.NOK_ERROR
                        _D_result.s_msgError = "Registro no encontrado para hacer la modificación"

                elif _D_result.E_resultado_validacion == IMPORTAR_EXCEL.E_RESULTADO_VALIDACION.OK:

                    if (
                            self._D_opciones.b_insertarTodos
                            or (
                                self._D_opciones.b_insertar_noRelacionados
                                and not _dbRow_encontrado
                                )
                            ):
                        _D_camposInsertar.update(_D_camposActualizar)
                        _D_result.n_id_insertado = self._D_opciones.dbTabla.insert(**_D_camposInsertar)
                        _D_registro.id = _D_result.n_id_insertado
                        self.D_estatusAfectados.n_insertados += 1

                    elif _dbRow_encontrado:
                        # TODO si todos los campos son iguales, no actualizar nada
                        _b_diferenicasEncontradas = False
                        for _s_nombreCampo in _D_camposActualizar.keys():
                            if _dbRow_encontrado[_s_nombreCampo] != _D_camposActualizar[_s_nombreCampo]:
                                _b_diferenicasEncontradas = True
                                break
                            else:
                                pass
                        if _b_diferenicasEncontradas:
                            _D_result.n_modificados = self._D_opciones.dbTabla._db(self._D_opciones.dbTabla.id == _dbRow_encontrado.id).update(**_D_camposActualizar)
                            self.D_estatusAfectados.n_modificados += 1

                        else:
                            self.D_estatusAfectados.n_ignorados += 1

                        _D_registro.id = _dbRow_encontrado.id

                    elif self._D_opciones.b_ignorar_noRelacionados:
                        self.D_estatusAfectados.n_ignorados += 1

                    else:
                        _D_result.E_return = stvfwk2_e_RETURN.NOK_ERROR
                        _D_result.s_msgError = "Registro no encontrado para hacer la modificación"

                else:
                    _D_result.E_return = stvfwk2_e_RETURN.NOK_ERROR
                    _D_result.s_msgError = "Valor de parámetro resultado validación no reconocido"

            else:
                # Si no tiene dbTable definido, no inserta, ni modifica; sólo verifica el excel
                pass

            if self._D_opciones.alAceptar:
                _D_result = self._D_opciones.alAceptar(
                    D_registro = _D_registro,
                    D_result = _D_result
                    )
            else:
                pass

        else:
            pass

        if _D_result.E_return > stvfwk2_e_RETURN.OK_WARNING:
            self._D_opciones.dbTabla._db.rollback()
        else:
            pass

        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

        return _D_return

    def importar(
            self,
            s_pathArchivo,
            s_nombreHoja = None
            ):
        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            D_estatusAfectados = self.D_estatusAfectados
            )

        import openpyxl
        wb_obj = openpyxl.load_workbook(s_pathArchivo)
        _n_rowActual = 0

        try:
            if not s_nombreHoja:
                self._sheet = wb_obj.active
            else:
                self._sheet = wb_obj.get_sheet_by_name(s_nombreHoja)

            _n_renglonHeader = 0
            _L_nombreColumnasRel = []

            for _T_row in self._sheet.iter_rows(values_only = True):

                _n_rowActual += 1

                if not _n_renglonHeader:
                    _L_row = []
                    for _x_value in _T_row:
                        if isinstance(_x_value, str):
                            _L_row.append(_x_value.encode('utf8', 'replace').replace('\n', ' ').replace('  ', ' ').strip())
                        else:
                            _L_row.append(_x_value)

                    if self._D_opciones.fnIdentificarHeader:
                        _D_result = self._D_opciones.fnIdentificarHeader(_L_row)

                    else:
                        if not _L_nombreColumnasRel:
                            for _n_index in self._D_campos_columnas_relacionar:
                                _L_nombreColumnasRel.append(self._D_campos_columnas_relacionar[_n_index].s_nombreColumna)
                        else:
                            pass
                        _D_result = Storage(
                            E_return = stvfwk2_e_RETURN.OK,
                            s_msgError = "",
                            b_headersTabla = False
                            )
                        for _x_cellValue in _L_row:
                            if _x_cellValue and (_x_cellValue in _L_nombreColumnasRel):
                                _D_result = Storage(
                                    E_return = stvfwk2_e_RETURN.OK,
                                    s_msgError = "",
                                    b_headersTabla = True
                                    )
                                break

                    if _D_result.b_headersTabla:
                        _n_renglonHeader = _n_rowActual
                        _D_result = self._importar_headers(_L_row)
                        if _D_result.E_return != stvfwk2_e_RETURN.OK:
                            _D_return.E_return = _D_result.E_return
                            _D_return.s_msgError = _D_result.s_msgError
                            break
                        else:
                            pass

                    else:
                        pass

                else:
                    # Ya encontro el header hora toca procesar los registros

                    # Se reformatea el registro en base a las columnas
                    _D_row = Storage()
                    for _D_columna in (
                            self._D_campos_columnas_relacionar.values()
                            + self._D_campos_columnas_importar.values()
                            ):
                        if _D_columna.n_columna is not None:
                            if isinstance(_T_row[_D_columna.n_columna], str):
                                _D_row[_D_columna.n_id] = _T_row[_D_columna.n_columna].encode('utf8', 'replace')
                            else:
                                _D_row[_D_columna.n_id] = _T_row[_D_columna.n_columna]
                        else:
                            _D_row[_D_columna.n_id] = _D_columna.x_valorFijo

                    _D_result = self._importar_datos(_D_row)

                    if _D_result.E_return > stvfwk2_e_RETURN.OK_WARNING:
                        _D_return.E_return = _D_result.E_return,
                        _D_return.s_msgError = _D_result.s_msgError
                        break
                    else:
                        pass

            if _D_return.E_return > stvfwk2_e_RETURN.OK_WARNING:
                dbc01.rollback()
            elif not _n_renglonHeader:
                _D_return.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
                _D_return.s_msgError = 'No se encontró el header del archivo de excel'
            else:
                _D_return.s_msgError = 'Ignorados = %d; Insertados = %d; Modificados = %d' % (
                    self.D_estatusAfectados.n_ignorados,
                    self.D_estatusAfectados.n_insertados,
                    self.D_estatusAfectados.n_modificados
                    )

        except Exception as _O_excepcion:
            _D_return.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
            _D_return.s_msgError = (
                    'Hoja no encontrada o error importando archivo: %s: row %d'
                    ) % (str(_O_excepcion), _n_rowActual)

        finally:
            wb_obj.close()

        return _D_return
