# -*- coding: utf-8 -*-
"""
Define todas las tablas utilizadas para uso con los menus
"""

""" Definición de los formularios disponibles en el sistema """
class TFORMULARIOS():
    pass
db.define_table(
    'tformularios',
    Field('descripcion', 'string', length = 100, default = None,
      required = True,
      notnull = True, unique = True,
      widget = stv_widget_input, label = ('Descripción'), comment = 'Descripción de la forma',
      writable = True, readable = True,
      represent = stv_represent_string),
    Field('aplicacion_w2p', 'string', length = 100, default = None,
      required = True, requires=IS_IN_SET(STV_FWK2_VALIDACIONES.APLICACION_W2P.OPCIONES(), zero=0, error_message=('Selecciona')),
      notnull = True, unique = False,
      widget = stv_widget_combobox, label = ('Aplicación Web2Py'), comment = 'Nombre de la aplicación web2py relacionada con esta forma',
      writable = True, readable = True,
      represent = stv_represent_string),
    Field('controlador_w2p', 'string', length = 100, default = None,
      required = True,
      notnull = True, unique = False,
      widget=lambda field, value: stv_widget_db_chosen_linked(field, value, 1, 'aplicacion_w2p', URL(c='accesos', f='application_controladores_ver.json')),
      #widget = stv_widget_combobox, 
      label = ('Controlador Web2Py'), comment = 'Nombre del controlador web2py relacionada con esta forma',
      writable = True, readable = True,
      represent = stv_represent_string),
    Field('funcion_w2p', 'string', length = 100, default = None,
      required = True,
      notnull = True, unique = False,
      widget=lambda field, value: stv_widget_db_chosen_linked(field, value, 1, ['aplicacion_w2p', 'controlador_w2p'], URL(c='accesos', f='application_rutinas_ver.json')),
      #widget = stv_widget_input, 
      label = ('Función Web2Py'), comment = 'Nombre de la función web2py relacionada con esta forma',
      writable = True, readable = True,
      represent = stv_represent_string),
    Field('actualizarpermisos', 'boolean', default = '1',
      required = True,
      notnull = True,
      widget=stv_widget_inputCheckbox, label = ('Actualizar Permisos'), comment = 'Mantener permisos sincronizados con código fuente @permiso',
      writable = True, readable = True,
      represent = stv_represent_boolean),
    # temp Field son usados para capturas temporales
    Field('temp_permisosiniciales', 'string', default = None,
      required = False, requires = IS_IN_SET(request.stv_fwk_permissions.getList(), zero = None, error_message = (T('Select') + T('Form option')) ),
      notnull = False, unique=False,
      widget = lambda f, v: stv_widget_chosen(f, v, 20), label = T('Permissions'), comment = None,
      writable = False, readable = False,
      represent = lambda v, r: request.stv_fwk_permissions.getLabel(v) if isinstance( v, ( int, long ) ) else T('None') ),
    tSignature,
    format = '%(aplicacion_w2p)s / %(controlador_w2p)s / %(funcion_w2p)s | %(descripcion)s',
    singular = 'Forma',
    plural = 'Formas',
    migrate = D_stvFwkCfg.b_requestIsMaster
    )


class TFORMULARIO_DISENIOS:
    """ Definición de las opciones disponibles por formulario en el sistema """
    pass


db.define_table(
    'tformulario_disenios',
    Field('formulario_id', db.tformularios, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label=T('Form'), comment=None,
      writable=False, readable=False,
      represent=None),
    Field('imagennombrearchivo', 'string', length=100, default=None,
      required=False,
      notnull=False, unique=False,
      widget = stv_widget_input, label=T('Original Filename'), comment=None,
      writable=False, readable=False,
      represent=stv_represent_string),    
    Field('imagen', type = 'upload',
      required = False, requires = IS_LENGTH(minsize=0, maxsize=1572864, error_message=T('File is too huge.')),
      notnull = False,
      uploadfield = True, widget=lambda f, d, u=None: stv_widget_inputFile(f, d, URL(a=D_stvFwkCfg.s_nameBackofficeApp, c='menus', f='forma_disenios_imagen'), db.tformulario_disenios.imagennombrearchivo), 
      label = T('Image'), comment = None,
      writable = True, readable = True, authorize = lambda record: auth.is_logged_in(),
      autodelete = True, represent = lambda v,r: stv_represent_image(v,r,URL(a=D_stvFwkCfg.s_nameBackofficeApp, c='menus', f='forma_disenios_imagen')), 
      uploadfolder = os.path.join(request.folder, 'uploads', 'cuenta'),
      uploadseparate = False, uploadfs = None ),
    Field('descripcion', 'string', length = 100, default = None,
      required = True,
      notnull = True, unique = False,
      widget = stv_widget_input, label = T('Description'), comment = ('Descripción de la pantalla de diseño'),
      writable = True, readable = True,
      represent = stv_represent_string),
    Field('versiondisenio', 'decimal(4,2)', default = 0.01,
      required = True, requires = IS_DECIMAL_IN_RANGE(0, 99.99),
      notnull = True, unique = False,
      widget = stv_widget_inputFloat, label = ('Versión'), comment = 'Versión del diseño',
      writable = True, readable = True,
      represent = stv_represent_float),
    tSignature,
    format = '%(formulario_id)s / %(descripcion)s',
    singular = ('Diseño formulario'),
    plural = ('Diseño formularios'),
    migrate = D_stvFwkCfg.b_requestIsMaster
    )

""" Definición de las opciones disponibles por formulario en el sistema """
class TFORMULARIO_PERMISOS():
    pass
db.define_table(
    'tformulario_permisos',
    Field('formulario_id', db.tformularios, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label=T('Form'), comment=None,
      writable=False, readable=False,
      represent=None),
    Field('permiso_id', 'integer', default = None,
      required = False, requires = IS_IN_SET(request.stv_fwk_permissions.getList(), zero = None, error_message = (T('Select') + T('Form option')) ),
      notnull = False, unique=False,
      widget = stv_widget_combobox, label = ('Permiso'), comment = None,
      writable = True, readable = True,
      represent = lambda v,r: stv_represent_string( request.stv_fwk_permissions.getLabel(v) ,r)),
    Field('descripcion', 'string', length = 100, default = None,
      required = True,
      notnull = True, unique = False,
      widget = stv_widget_input, label = ('Descripción'), comment = T('Permission') + ' ' + T('description'),
      writable = True, readable = True,
      represent = stv_represent_string),
    Field('imagen', 'upload', 
      required = False, requires = IS_LENGTH(minsize=0, maxsize=1572864, error_message=T('File is too huge.')),
      ondelete = 'CASCADE', notnull = False,
      uploadfield = True, widget = None, label = ('Imagen'), comment = None,
      writable = True, readable = True, authorize = lambda record: auth.is_logged_in(),
      autodelete = False, represent = None, 
      uploadfolder = os.path.join(request.folder, 'uploads', 'permisos'),
      uploadseparate = None, uploadfs = None ),
    Field('icono', 'string', length = 100, default = None,
      required = False,
      notnull = False, unique = False,
      widget = lambda oF, xV: stv_widget_input_link(oF, xV, "http://fontawesome.io/icons/"), 
      label = ('Icono'), comment = T('Icon for the menu option with the form \'fa fa-check\''),
      writable = True, readable = True,
      represent = stv_represent_icon),
    tSignature,
    format = '%(formulario_id)s / %(permiso_id)s',
    singular = T('Permission'),
    plural = T('Permissions'),
    migrate = D_stvFwkCfg.b_requestIsMaster
    )

""" Definición del menú """
class TMENUS():

    @classmethod
    def GET_IMAGEN_URL(cls, s_value = None, s_filename = None):
        _s_url = URL(a=D_stvFwkCfg.s_nameBackofficeApp, c='cuentas', f='cuentas_imagen')
        if s_value:
            _s_url += '/' + s_value
        else:
            pass
        if s_filename:
            _s_url += '?filename=' + str(s_filename)
        else:
            pass
        return _s_url
    
    pass
db.define_table(
    'tmenus',
    Field(
        'nombre', 'string', length = 40, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = T('Name'), comment = T('Menu name'),
        writable = True, readable = True,
        represent = None
        ),
    Field(
        'descripcion', 'string', length = 100, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = T('Description'), comment = T('Menu descriptive name'),
        writable = True, readable = True,
        represent = None
        ),
    Field(
        'icono', 'string', length = 100, default = None,
        required = False,
        notnull = False, unique = False,
        widget = lambda oF, xV: stv_widget_input_link(oF, xV, "http://fontawesome.io/icons/"), label = T('Icon'), comment = T('Icon for the menu option with the form <i class=\'fa fa-check\'></i>'),
        writable = True, readable = True,
        represent = stv_represent_icon
        ),
    Field(
        'imagennombrearchivo', 'string', length=100, default=None,
        required=False,
        notnull=False, unique=False,
        widget = stv_widget_input, label=('Nombre original del archivo'), comment=None,
        writable=False, readable=False,
        represent=stv_represent_string
        ),
    Field(
        'imagen', type = 'upload',
        required = False, requires = IS_LENGTH(minsize=0, maxsize=1572864, error_message=('El archivo es demasiado grande.')),
        notnull = False,
        uploadfield = True, widget=lambda f, d, u=None: stv_widget_inputFile(f, d, TMENUS.GET_IMAGEN_URL(), db.tmenus.imagennombrearchivo), 
        label=('Imagen'), comment = None,
        writable = True, readable = True, authorize = lambda record: auth.is_logged_in(),
        autodelete = True, represent = lambda v,r: stv_represent_image(v, r, TMENUS.GET_IMAGEN_URL()), 
        uploadfolder = os.path.join(request.folder, 'uploads', 'cuenta'),
        uploadseparate = False, uploadfs = None 
        ),
    tSignature,
    format = '%(nombre)s : %(descripcion)s',
    singular = T('Menu/Aplicación'),
    plural = T('Menus/Aplicaciones'),
    migrate = D_stvFwkCfg.b_requestIsMaster
    )

""" Definición del submenu del menú """
class TMENU_SUBMENUS():
    class MANEJOARGUMENTOS():
        ''' Se definen las opciónes del campo '''
        SIN_ARGUMENTOS = 0
        SOLO_DEFAULT = 1 # Sólo se utilizaran los argumentos por default definidos como la empresa específica por aplicación
        SOLO_ESPECIFICADOS = 2 # Sólo se utilizaran los argumentos en el campo "argumentos"
        DEFAULT_Y_ESPECIFICADOS = 3 # Se utilizarán los argumentos por default y los especificados
        OTRO = 4
        
        @classmethod
        def get_dict(self):
            return {
                    self.SIN_ARGUMENTOS: ('Sin argumentos'),
                    self.SOLO_DEFAULT: ('Sólo empresa id'),
                    self.SOLO_ESPECIFICADOS: ('Sólo especificados'),
                    self.DEFAULT_Y_ESPECIFICADOS: ('Empresa id y especificados'),
                    self.OTRO: ('Otro'),
                    }
    
    pass
db.define_table(
    'tmenu_submenus',
    Field(
        'menu_id', db.tmenus,
        required=True,
        ondelete='CASCADE', notnull=True, unique=False,
        label=T('Parent Menu'), comment='Menú padre',
        writable=False, readable=False,
        represent=None
        ),
    Field(
        'nombre', 'string', length = 40, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = T('Name'), comment = T('Menu name'),
        writable = True, readable = True,
        represent = None
        ),
    Field(
        'descripcion', 'string', length = 100, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = T('Description'), comment = T('Menu descriptive name'),
        writable = True, readable = True,
        represent = None
        ),
    Field(
        'formulario_id', db.tformularios, default=None,
        required=False, requires = IS_NULL_OR(IS_IN_DB(db,'tformularios.id','%(descripcion)s')),
        ondelete='NO ACTION', notnull=True, unique=False,
        widget = lambda f, v: stv_widget_db_search(f, v, 
                                                 s_display = '%(tformularios.descripcion)s', 
                                                 s_url = URL( a = D_stvFwkCfg.s_nameBackofficeApp, c = 'menus', f = 'formas_buscar'),
                                                 ), 
        label=T('Form'), comment=None,
        writable=True, readable=True,
        represent=stv_represent_referencefield
        ),
    Field(
        'imagen', type = 'upload',
        required = False, requires = IS_LENGTH(minsize=0, maxsize=1572864, error_message=T('File is too huge.')),
        ondelete = 'CASCADE', notnull = False,
        uploadfield = True, widget = None, label = T('Image'), comment = None,
        writable = True, readable = True, authorize = lambda record: auth.is_logged_in(),
        autodelete = False, represent = stv_represent_image, 
        uploadfolder = os.path.join(request.folder, 'uploads', 'menu'),
        uploadseparate = None, uploadfs = None
        ),
    Field(
        'manejoargumentos', 'integer', default = 0,
        required = True, requires = IS_IN_SET(TMENU_SUBMENUS.MANEJOARGUMENTOS.get_dict(), zero = 0, error_message = 'Selecciona' ),
        notnull = False,
        widget = stv_widget_combobox, 
        label = ('Manejo de Args.'), comment = 'Como agregar argumentos al request',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TMENU_SUBMENUS.MANEJOARGUMENTOS.get_dict())
        ),
    Field(
        'argumentos', 'string', length = 200, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = T('Argumentos'), comment = 'Opcional, argumentos separados por / usados para ligar la notificación a la apertura de una forma',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'orden', 'integer', default = None,
        required = False,
        notnull = False, unique=False,
        widget = stv_widget_input, label = T('Order'), comment = None,
        writable = True, readable = True,
        represent = None
        ),
    tSignature,
    format = '%(nombre)s',
    singular = T('Submenu'),
    plural = T('Submenus'),
    migrate = D_stvFwkCfg.b_requestIsMaster
    )

""" Definición de las aplicaciones disponibles en el sistema """
class TAPLICACIONES():
        
    @classmethod
    def GET_IMAGEN_URL(cls, s_value = None, s_filename = None):
        _s_url = URL(a=D_stvFwkCfg.s_nameBackofficeApp, c='cuentas', f='cuentas_imagen')
        if s_value:
            _s_url += '/' + s_value
        else:
            pass
        if s_filename:
            _s_url += '?filename=' + str(s_filename)
        else:
            pass
        return _s_url

    @classmethod
    def GET_IMAGENPIN_URL(cls, s_value = None, s_filename = None):
        _s_url = URL(a=D_stvFwkCfg.s_nameBackofficeApp, c='cuentas', f='cuentas_imagenpin')
        if s_value:
            _s_url += '/' + s_value
        else:
            pass
        if s_filename:
            _s_url += '?filename=' + str(s_filename)
        else:
            pass
        return _s_url
    
    class CONFIGURACION():
        ''' Se definen las opciónes del campo '''
        SIN_CONFIGURACION = 0
        REQUIERE_EMPRESA = 1 # Al asociar la aplicación con una cuenta, se debe definir la empresa
        OPCIONAL_EMPRESA = 2 # Al asociar la aplicación con una cuenta, definir la empresa, es opcional
        
        @classmethod
        def GET_DICT(self):
            return {
                self.SIN_CONFIGURACION: ('Sin configuración'),
                self.REQUIERE_EMPRESA: ('Requiere definir empresa'),
                self.OPCIONAL_EMPRESA: ('Opcionalmente puede definir la empresa'),
                }
    
    pass
db.define_table(
    'taplicaciones',
    Field(
        'descripcion', 'string', length = 100, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label=('Descripción'), comment = 'Descripción de la aplicación',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'nombrecorto', 'string', length=100, default=None,
        required=True,
        notnull=True, unique=False,
        widget = stv_widget_input, label=('Nombre corto'), comment=None,
        writable=True, readable=True,
        represent=stv_represent_string
        ),
    Field(
        'aplicacion_w2p', 'string', length = 100, default = None,
        required = True, requires=IS_IN_SET(STV_FWK2_VALIDACIONES.APLICACION_W2P.OPCIONES(), zero=0, error_message=('Selecciona')),
        notnull = True, unique = False,
        widget = stv_widget_combobox, label=('Aplicación Web2Py'), comment = 'Nombre de la aplicación web2py donde ejecutará el default',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'configuracion', 'integer', default = 0,
        required = True, requires = IS_IN_SET(TAPLICACIONES.CONFIGURACION.GET_DICT(), zero = 0, error_message = 'Selecciona' ),
        notnull = False,
        widget = stv_widget_combobox, 
        label = ('Configuración'), comment = 'Opciones de configuración para la aplicación cuando se asocie a una cuenta',
        writable = True, readable = True,
        represent = lambda v, r: stv_represent_list(v, r, None, TAPLICACIONES.CONFIGURACION.GET_DICT())
        ),
    Field(
        'imagennombrearchivo', 'string', length=100, default=None,
        required=False,
        notnull=False, unique=False,
        widget = stv_widget_input, label=('Nombre original del archivo'), comment=None,
        writable=False, readable=False,
        represent=stv_represent_string
        ),
    Field(
        'imagen', type = 'upload',
        required = False, requires = IS_LENGTH(minsize=0, maxsize=1572864, error_message=('El archivo es demasiado grande.')),
        notnull = False,
        uploadfield = True, widget=lambda f, d, u=None: stv_widget_inputFile(f, d, TAPLICACIONES.GET_IMAGEN_URL(), db.taplicaciones.imagennombrearchivo), 
        label=('Imagen'), comment = None,
        writable = True, readable = True, authorize = lambda record: auth.is_logged_in(),
        autodelete = True, represent = lambda v,r: stv_represent_image(v, r, TAPLICACIONES.GET_IMAGEN_URL()), 
        uploadfolder = os.path.join(request.folder, 'uploads', 'cuenta'),
        uploadseparate = False, uploadfs = None 
        ),
    Field(
        'imagenpinnombrearchivo', 'string', length=100, default=None,
        required=False,
        notnull=False, unique=False,
        widget = stv_widget_input, label=('Nombre original del archivo'), comment=None,
        writable=False, readable=False,
        represent=stv_represent_string
        ),
    Field(
        'imagenpin', type = 'upload',
        required = False, requires = IS_LENGTH(minsize=0, maxsize=1572864, error_message=('El archivo es demasiado grande.')),
        notnull = False,
        uploadfield = True, widget=lambda f, d, u=None: stv_widget_inputFile(f, d, TAPLICACIONES.GET_IMAGENPIN_URL(), db.taplicaciones.imagenpinnombrearchivo), 
        label=('Imagen Pin'), comment = None,
        writable = True, readable = True, authorize = lambda record: auth.is_logged_in(),
        autodelete = True, represent = lambda v,r: stv_represent_image(v, r, TAPLICACIONES.GET_IMAGENPIN_URL()), 
        uploadfolder = os.path.join(request.folder, 'uploads', 'cuenta'),
        uploadseparate = False, uploadfs = None 
        ),
    tSignature,
    format = lambda r: r.nombrecorto,
    singular = ('Aplicación'),
    plural = ('Aplicaciones'),
    migrate = D_stvFwkCfg.b_requestIsMaster
    )

""" Definición de los perfiles manejables por el cliente para la creación de usuarios """
class TAPLICACION_MENUS():
    pass
db.define_table(
    'taplicacion_menus',
    Field(
        'aplicacion_id', db.taplicaciones, default=None,
        required=True,
        ondelete='CASCADE', notnull=True, unique=False,
        label=('Aplicación'), comment=None,
        writable=False, readable=False,
        represent=stv_represent_referencefield
        ),
    Field(
        'menu_id', db.tmenus, default=None,
        required=True,
        notnull=True, unique=False,
        widget = stv_widget_db_combobox, label=('Estructura de menú'), comment='Estructura de Menu',
        writable=True, readable=True,
        represent=stv_represent_referencefield
        ),
    Field(
        'descripcion', 'string', length = 100, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label=('Descripción'), comment = 'Descripción de la estructura de menú',
        writable = True, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'orden', 'integer', default = None,
        required = False,
        notnull = False, unique=False,
        widget = stv_widget_input, label=('Orden'), comment = None,
        writable = True, readable = True,
        represent = None
        ),
    tSignature,
    format = '%(descripcion)s',
    singular = ('Menú en aplicación'),
    plural = ('Menus en aplicación'),
    migrate = D_stvFwkCfg.b_requestIsMaster
    )


''' Definición de alias para las tablas '''
dbF = db.tformularios.with_alias('tF')
#dbFP = db.tformulario_permisos.with_alias('tFP')
#dbM = db.tmenus.with_alias('tM')
#dbMS = db.tmenu_submenus.with_alias('tMS')


###
### Funciones librería para manejo de menus
###

def db120_menus_generarMenu(_n_cuenta_aplicacion_id):
    
    # Se verifica que tenga permiso para la aplicación...
    _qry =  ( 
        ( # Usuario no suspendido
            (db.auth_user.id == auth.user_id) 
            & (db.auth_user.bansuspender == 0) 
            )
        & ( # Con membresía a una cuenta, la membresia no suspendida
            (db.auth_membership.user_id == db.auth_user.id) 
            & (db.auth_membership.bansuspender == 0) 
            )
        & ( # Con la cuenta no suspendida
            (db.tcuentas.id == db.auth_membership.cuenta_id) 
            & (db.tcuentas.bansuspender == 0) 
            )
        & ( # Donde la aplicación, sea la aplicación actual, este asociada a la cuenta y no este la aplicación suspendida
            (db.tcuenta_aplicaciones.cuenta_id == db.tcuentas.id)
            & (db.tcuenta_aplicaciones.id == _n_cuenta_aplicacion_id)
            & (db.tcuenta_aplicaciones.bansuspender == 0) 
            )
        & ( # Aplicación no se encuentre suspendida
            (db.taplicaciones.id == db.tcuenta_aplicaciones.aplicacion_id)
            & (db.taplicaciones.bansuspender == 0) 
            )
        )
    
    # ...si no se encuentran resultados...
    if db( _qry ).count() == 0:
        # ...se redirecciona al inicio.
        redirect( 
            URL( 
                a=D_stvFwkCfg.s_nameAccesoApp, 
                c='default', 
                f='index', 
                vars={
                    '_flash':'Sin permisos en la aplicación, error en el inicio de sesion', 
                    'avoidredirect':'1'
                    } 
                ) 
            )
    else:
        pass

    _qry &= (
        ( # Se obtienen los menus
            (db.taplicacion_menus.aplicacion_id == db.taplicaciones.id) 
            & (db.taplicacion_menus.bansuspender == 0) 
            )
        & ( # Se obtiene la información de los menus
            (db.tmenus.id == db.taplicacion_menus.menu_id) 
            & (db.tmenus.bansuspender == 0) 
            )
        & ( # Se obtiene la información de los submenus
            (db.tmenu_submenus.menu_id == db.tmenus.id) 
            & (db.tmenu_submenus.bansuspender == 0) 
            )
        & ( # Se obtiene la información de los formularios
            (db.tformularios.id == db.tmenu_submenus.formulario_id) 
            & (db.tformularios.bansuspender == 0) 
            )
        )

    # Obtener todos los formularios a los que tiene acceso el usuario y sus menus
    _db_rows = db(
        _qry
        ).select(
            db.tmenus.id, 
            db.tmenus.nombre, 
            db.tmenus.descripcion, 
            db.tmenus.imagen, 
            db.tmenus.icono,
            db.tmenu_submenus.id, 
            db.tmenu_submenus.nombre, 
            db.tmenu_submenus.descripcion, 
            db.tmenu_submenus.imagen,
            db.tmenu_submenus.manejoargumentos,
            db.tmenu_submenus.argumentos,
            db.tcuenta_aplicaciones.empresa_id,
            db.tformularios.id,  
            db.tformularios.descripcion, 
            db.tformularios.aplicacion_w2p, 
            db.tformularios.controlador_w2p, 
            db.tformularios.funcion_w2p,
            left = [
                ],
            orderby = [db.taplicacion_menus.orden, db.tmenu_submenus.orden]
        )

    _db_tree = DB_Tree(_db_rows)
    _db_tree.addLevel([ 'tmenus' ])
    _db_tree.addLevel([ 'tmenu_submenus', 'tcuenta_aplicaciones', 'tformularios' ])
    
    _db_tree_menu = _db_tree.process()
    return _db_tree_menu

TCUENTA_APLICACIONES.CONFIGURAR_INTEGRIDAD_REFERENCIAL()
