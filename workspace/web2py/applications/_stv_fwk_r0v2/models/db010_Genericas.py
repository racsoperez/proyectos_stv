# -*- coding: utf-8 -*-
"""
Definición de las tablas genéricas
"""

class TGENERICAS:

    class SI_NO_OPCIONAL:
        """ Se definen las opciónes del campo """
        NADA = 0
        SI = 1
        NO = 2
        OPCIONAL = 3
        D_TODOS = {
            SI      : 'SI',
            NO      : 'NO',
            OPCIONAL: 'OPCIONAL',
            }
        
        @classmethod
        def get_dict(cls):
            return cls.D_TODOS
    
    class SI_NO:
        """ Se definen las opciónes del campo """
        NADA = 0
        SI = 1
        NO = 2
        D_TODOS = {
            SI: 'SI',
            NO: 'NO',
            }
        
        @classmethod
        def get_dict(cls):
            return cls.D_TODOS

    class MES:
        """ Se definen las opciónes del campo """
        NADA = 0
        ENERO = 1
        FEBRERO = 2
        MARZO = 3
        ABRIL = 4
        MAYO = 5
        JUNIO = 6
        JULIO = 7
        AGOSTO = 8
        SEPTIEMBRE = 9
        OCTUBRE = 10
        NOVIEMBRE = 11
        DICIEMBRE = 12
        D_TODOS = {
            ENERO     : 'Enero',
            FEBRERO   : 'Febrero',
            MARZO     : 'Marzo',
            ABRIL     : 'Abril',
            MAYO      : 'Mayo',
            JUNIO     : 'Junio',
            JULIO     : 'Julio',
            AGOSTO    : 'Agosto',
            SEPTIEMBRE: 'Septiembre',
            OCTUBRE   : 'Octubre',
            NOVIEMBRE : 'Noviembre',
            DICIEMBRE : 'Diciembre',
            }

    pass  # TGENERICAS


""" Log of access to the system """
db.define_table(
    'tlogs',
    Field(
        'user_id', db.auth_user, default = auth.user_id,
        required = True,
        ondelete = 'SET NULL', notnull = False, unique = False,
        label = T('User'), comment = None,
        writable = False, readable = True,
        represent = None
        ),
    Field(
        'fechahora', type = TDATETIME_GRABADIRECTO_LEERENZONAH, default = request.utcnow,
        required = False, requires = IS_NULL_OR(IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME)),
        notnull = False,
        label = T('Date/Time'), comment = None,
        writable = False, readable = True,
        represent = None
        ),
    Field(
        'ip', 'string', length = 50, default = request.client,
        required = False,
        notnull = False, unique = False,
        label = 'IP', comment = None,
        writable = False, readable = True,
        represent = None
        ),
    Field(
        'notas', 'text', default = None,
        required = False,
        notnull = False,
        label = 'Notas', comment = None,
        writable = True, readable = True
        ),
    format = '%(ip)s',
    singular = 'Log',
    plural = 'Logs',
    migrate = D_stvFwkCfg.b_requestIsMaster
    )

''' Creación del log para los movimientos en la tabla de usuarios y membresías '''
""" Log genérico de operaciones en las tablas """

D_stvFwkHelper.tlogtableoperation_operation = {
    0: 'Insert',
    1: 'Update',
    2: 'Delete',
    }

db.define_table(
    'tlogtableoperation',
    Field(
        'nombretabla', 'string', length = 100, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = T('Table Name'),
        comment = 'Nombre de la tabla sobre la que se esta realizando la operación',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'operacion', 'integer', default = 0,
        required = True,
        requires = IS_IN_SET(D_stvFwkHelper.tlogtableoperation_operation, zero = None, error_message = T('Select')),
        notnull = True, unique = False,
        widget = stv_widget_combobox, label = T('Operation'), comment = 'Operación en la tabla',
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_string(D_stvFwkHelper.tlogtableoperation_operation.get(v, T('None')), r)
        ),
    Field(
        'condicion', 'string', length = 255, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Condición',
        comment = 'Condición sobre los registros impactados por la operación',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'modificacion', 'text', default = None,
        required = False,
        notnull = False,
        label = T('Operation'), comment = "Campos y sus valiores en la modificación",
        writable = False, readable = True,
        represent = stv_represent_text
        ),
    Field(
        'notas', 'text', default = None,
        required = False,
        notnull = False,
        label = 'Notas', comment = "Notas adicionales de revisión",
        writable = True, readable = True,
        represent = stv_represent_text
        ),
    Field(
        'ip', 'string', length = 200, default = request.client,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = T('IP Addres'),
        comment = 'Dirección IP del dispositivo que el usuario utilizó al realizar la operación',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'operacionrealizada_en', type = TDATETIME_GRABADIRECTO_LEERENZONAH, default = request.utcnow,
        required = True, requires = IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME),
        notnull = True,
        label = T('Realized On'), comment = 'Fecha y hora en la que fue la operación realizada',
        writable = False, readable = True,
        represent = stv_represent_datetime
        ),
    Field(
        'operacionrealizada_por', db.auth_user, default = auth.user_id,
        required = False,
        notnull = False,
        label = T('Realized By'), comment = 'Usuario que realizó la operación',
        writable = False, readable = True,
        represent = stv_represent_referencefield
        ),
    singular = T('Table operation log'),
    plural = T('Tables operations log'),
    migrate = D_stvFwkCfg.b_requestIsMaster
    )


def table_addLog(oTable):
    oTable._after_insert.append(
        lambda f, n_id: db.tlogtableoperation.insert(
            nombretabla = oTable._tablename, operacion = 0, condicion = str(n_id), modificacion = ''
            )
        )
    oTable._after_update.append(
        lambda s, f: db.tlogtableoperation.insert(
            nombretabla = oTable._tablename, operacion = 1, condicion = str(s), modificacion = str(f)
            )
        )
    oTable._after_delete.append(
        lambda s: db.tlogtableoperation.insert(
            nombretabla = oTable._tablename, operacion = 2, condicion = str(s), modificacion = ''
            )
        )
    return


table_addLog(db.auth_user)
table_addLog(db.auth_membership)
table_addLog(db.auth_group)
table_addLog(db.auth_permission)
