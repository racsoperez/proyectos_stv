# -*- coding: utf-8 -*-

# noinspection SpellCheckingInspection

import datetime
import time
import re

from gluon import *
from gluon.storage import Storage
from gluon.html import xmlescape
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import Element, SubElement, Comment, tostring


class STV_LIB_CFDI:

    @classmethod
    def FORMATEAR_RFC_AGREGARGUIONES(
            cls,
            s_rfc
            ):
        """ Agrega guiones a RFCs de ser necesario
        """
        if len(s_rfc) <= 13:
            _s_rfc = (
                s_rfc[:len(s_rfc)-9]
                + "-" + s_rfc[-9:-3]
                + "-" + s_rfc[-3:]
                )
        else:
            _s_rfc = s_rfc

        return _s_rfc

    @classmethod
    def REPRESENTAR_XML(
            cls,
            n_empresa_id,
            n_cfdi_id,
            b_grabar = False,
            ):
        _ = n_empresa_id
        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            s_campoconerror = "",  # Nombre del campo con el error
            n_id = 0,
            xml = ""
            )

        _dbRow_cfdi = dbc01.tcfdis(n_cfdi_id)

        if (TCFDIS.PROC.PUEDE_EDITAR(_dbRow_cfdi).E_return != stvfwk2_e_RETURN.OK) and b_grabar:
            _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
            _D_return.s_msgError = "CFDI ya esta firmado, no puede regenerarse el XML"
        else:

            _D_cfdiPago = {
                'xmlns:cfdi'        : "http://www.sat.gob.mx/cfd/3",
                'xmlns:pago10'      : "http://www.sat.gob.mx/Pagos",
                'xmlns:xsi'         : "http://www.w3.org/2001/XMLSchema-instance",
                'Fecha'             : _dbRow_cfdi.fecha_str,
                'Folio'             : _dbRow_cfdi.folio,
                'LugarExpedicion'   : _dbRow_cfdi.lugarexpedicion,
                'Moneda'            : _dbRow_cfdi.moneda,
                'SubTotal'          : str(_dbRow_cfdi.subtotal),
                'TipoDeComprobante' : _dbRow_cfdi.tipodecomprobante,
                'Total'             : str(_dbRow_cfdi.total),
                'Version'           : _dbRow_cfdi.versioncfdi,
                'xsi:schemaLocation': "http://www.sat.gob.mx/cfd/3 "
                                      "http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd "
                                      "http://www.sat.gob.mx/Pagos "
                                      "http://www.sat.gob.mx/sitio_internet/cfd/Pagos/Pagos10.xsd ",
                }

            _D_cfdiIngreso = {
                'xmlns:xsi'         : "http://www.w3.org/2001/XMLSchema-instance",
                'xmlns:cfdi'        : "http://www.sat.gob.mx/cfd/3",
                'xsi:schemaLocation': "http://www.sat.gob.mx/cfd/3 "
                                      "http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd ",
                'Version'           : _dbRow_cfdi.versioncfdi,
                'Serie'             : _dbRow_cfdi.serie,
                'Folio'             : _dbRow_cfdi.folio,
                'Fecha'             : _dbRow_cfdi.fecha_str,
                'MetodoPago'        : _dbRow_cfdi.metodopago,
                'FormaPago'         : _dbRow_cfdi.formapago,
                'TipoDeComprobante' : _dbRow_cfdi.tipodecomprobante,
                'Moneda'            : _dbRow_cfdi.moneda,
                'TipoCambio'        : str(_dbRow_cfdi.tipocambio),
                'LugarExpedicion'   : _dbRow_cfdi.lugarexpedicion,
                'SubTotal'          : str(_dbRow_cfdi.subtotal),
                'Descuento'         : str(_dbRow_cfdi.descuento),
                'Total'             : str(_dbRow_cfdi.total),
                }

            _O_comprobante = Element(
                tag = 'cfdi:Comprobante',
                attrib = _D_cfdiIngreso
                )

            _O_emisor = SubElement(
                parent = _O_comprobante,
                tag = 'cfdi:Emisor',
                attrib = {
                    'Nombre' : _dbRow_cfdi.emisornombrerazonsocial,
                    'RegimenFiscal' : _dbRow_cfdi.emisorregimenfiscal,
                    'Rfc' : _dbRow_cfdi.emisorrfc
                    }
                )

            _O_receptor = SubElement(
                parent = _O_comprobante,
                tag = 'cfdi:Receptor',
                attrib = {
                    'Nombre' : _dbRow_cfdi.receptornombrerazonsocial,
                    'UsoCFDI' : _dbRow_cfdi.receptorusocfdi,
                    'Rfc' : _dbRow_cfdi.receptorrfc
                    }
                )

            _O_conceptos = SubElement(
                parent = _O_comprobante,
                tag = 'cfdi:Conceptos',
                )

            _dbRows_conceptos = _dbRow_cfdi.tcfdi_conceptos.select()

            _D_resumenImpuestos = Storage()

            for _dbRow_concepto in _dbRows_conceptos:
                _O_concepto = SubElement(
                    parent = _O_conceptos,
                    tag = 'cfdi:Concepto',
                    attrib = {
                        'Cantidad' : str(_dbRow_concepto.cantidad),
                        'Unidad' : _dbRow_concepto.unidad,
                        'ClaveUnidad' : _dbRow_concepto.claveunidad,
                        'NoIdentificacion' : _dbRow_concepto.noidentificacion,
                        'ClaveProdServ' : _dbRow_concepto.claveprodserv,
                        'Descripcion' : _dbRow_concepto.descripcion,
                        'ValorUnitario' : str(_dbRow_concepto.valorunitario),
                        'Descuento' : str(_dbRow_concepto.descuento),
                        'Importe' : str(_dbRow_concepto.importe),
                        }
                    )

                _dbRows_impuestosTrasladados = _dbRow_concepto.tcfdi_concepto_impuestostrasladados.select()
                _dbRows_impuestosRetenidos = _dbRow_concepto.tcfdi_concepto_impuestosretenidos.select()

                if _dbRows_impuestosTrasladados or _dbRows_impuestosRetenidos:
                    _O_impuestos = SubElement(
                        parent = _O_concepto,
                        tag = 'cfdi:Impuestos',
                        attrib = {}
                        )

                    if _dbRows_impuestosTrasladados:
                        _O_impTrasladados = SubElement(
                            parent = _O_impuestos,
                            tag = 'cfdi:Trasladados',
                            attrib = {}
                            )

                        if not _D_resumenImpuestos.trasladados:
                            _D_resumenImpuestos.trasladados = Storage()
                        else:
                            pass

                        for _dbRow_impTrasladado in _dbRows_impuestosTrasladados:
                            _O_impTrasladado = SubElement(
                                parent = _O_impTrasladados,
                                tag = 'cfdi:Traslado',
                                attrib = {
                                    'Base' : str(_dbRow_impTrasladado.base),
                                    'Impuesto' : _dbRow_impTrasladado.impuesto,
                                    'TasaOCuota' : str(_dbRow_impTrasladado.tasaocuota),
                                    'TipoFactor' : _dbRow_impTrasladado.tipofactor,
                                    'Importe' : str(_dbRow_impTrasladado.importe),
                                    }
                                )

                            _s_index = str(_dbRow_impTrasladado.impuesto) + "_" + str(_dbRow_impTrasladado.tasaocuota)
                            if not _D_resumenImpuestos.trasladados[_s_index]:
                                _D_resumenImpuestos.trasladados[_s_index] = Storage(
                                    impuesto = _dbRow_impTrasladado.impuesto,
                                    tasaocuota = _dbRow_impTrasladado.tasaocuota,
                                    tipofactor = _dbRow_impTrasladado.tipofactor,
                                    importe = _dbRow_impTrasladado.importe
                                    )
                            else:
                                _D_resumenImpuestos.trasladados[_s_index].importe += _dbRow_impTrasladado.importe
                    else:
                        # No hay impuestos trasladados
                        pass

                    if _dbRows_impuestosRetenidos:
                        _O_impRetenidos = SubElement(
                            parent = _O_impuestos,
                            tag = 'cfdi:Retenidos',
                            attrib = {}
                            )

                        if not _D_resumenImpuestos.retenidos:
                            _D_resumenImpuestos.retenidos = Storage()
                        else:
                            pass

                        for _dbRow_impRetenido in _dbRows_impuestosRetenidos:
                            _O_impRetenido = SubElement(
                                parent = _O_impRetenidos,
                                tag = 'cfdi:Retenido',
                                attrib = {
                                    'Base' : str(_dbRow_impRetenido.base),
                                    'Impuesto' : _dbRow_impRetenido.impuesto,
                                    'TasaOCuota' : str(_dbRow_impRetenido.tasaocuota),
                                    'TipoFactor' : _dbRow_impRetenido.tipofactor,
                                    'Importe' : str(_dbRow_impRetenido.importe),
                                    }
                                )

                            _s_index = str(_dbRow_impRetenido.impuesto) + "_" + str(_dbRow_impRetenido.tasaocuota)
                            if not _D_resumenImpuestos.retenidos[_s_index]:
                                _D_resumenImpuestos.retenidos[_s_index] = Storage(
                                    impuesto = _dbRow_impRetenido.impuesto,
                                    tasaocuota = _dbRow_impRetenido.tasaocuota,
                                    tipofactor = _dbRow_impRetenido.tipofactor,
                                    importe = _dbRow_impRetenido.importe
                                    )
                            else:
                                _D_resumenImpuestos.retenidos[_s_index].importe += _dbRow_impRetenido.importe
                    else:
                        # No hay impuestos trasladados
                        pass
                else:
                    # No hay impuestos
                    pass

            if _D_resumenImpuestos:
                _O_impuestos = SubElement(
                    parent = _O_comprobante,
                    tag = 'cfdi:Impuestos',
                    attrib = {}
                    )

                if _D_resumenImpuestos.trasladados:

                    _O_impTrasladados = SubElement(
                        parent = _O_impuestos,
                        tag = 'cfdi:Trasladados',
                        attrib = {}
                        )

                    for _s_index, _D_impuesto in _D_resumenImpuestos.trasladados.items():
                        _O_impTrasladado = SubElement(
                            parent = _O_impTrasladados,
                            tag = 'cfdi:Trasladado',
                            attrib = {
                                'Impuesto' : _D_impuesto.impuesto,
                                'TasaOCuota' : str(_D_impuesto.tasaocuota),
                                'TipoFactor' : _D_impuesto.tipofactor,
                                'Importe' : str(_D_impuesto.importe),
                                }
                            )
                else:
                    # No hay resumen para impuestos trasladados
                    pass

                if _D_resumenImpuestos.retenidos:

                    _O_impRetenidos = SubElement(
                        parent = _O_impuestos,
                        tag = 'cfdi:Retenidos',
                        attrib = {}
                        )

                    for _s_index, _D_impuesto in _D_resumenImpuestos.retenidos.items():
                        _O_impRetenido = SubElement(
                            parent = _O_impRetenidos,
                            tag = 'cfdi:Retenido',
                            attrib = {
                                'Impuesto' : _D_impuesto.impuesto,
                                'TasaOCuota' : str(_D_impuesto.tasaocuota),
                                'TipoFactor' : _D_impuesto.tipofactor,
                                'Importe' : str(_D_impuesto.importe),
                                }
                            )
                else:
                    # No hay resumen para impuestos trasladados
                    pass

            else:
                # No hay impuestos en los conceptos de este CFDI
                pass

            _D_return.xml = tostring(_O_comprobante, encoding="utf-8")

            if b_grabar:
                _dbRows_xml = dbc02(
                    dbc02.tcfdi_xmls.cfdi_id == n_cfdi_id
                    ).select(
                        dbc02.tcfdi_xmls.ALL
                        )

                if len(_dbRows_xml) == 1:
                    _dbRow_xml = _dbRows_xml.first()
                    _dbRow_xml.xml_cfdi = _D_return.xml
                    _dbRow_xml.update_record()
                    _s_cfdixml_id = _dbRow_xml.id
                else:
                    if len(_dbRows_xml) > 1:
                        _dbRows_xml.delete()
                    else:
                        pass

                    _D_insert = Storage(
                        cfdi_id = n_cfdi_id,
                        xml_cfdi = _D_return.xml,
                        )
                    _s_cfdixml_id = dbc02.tcfdi_xmls.insert(**_D_insert)
            else:
                pass

        return _D_return

    class REPRESENTAR_JSON:

        def __init__(
                self,
                n_empresa_id,
                n_cfdi_id,
                ):
            """

            @param n_empresa_id:
            @type n_empresa_id:
            @param n_cfdi_id:
            @type n_cfdi_id:
            """

            self._n_empresa_id = n_empresa_id
            self._dbRow_cfdi = dbc01.tcfdis(n_cfdi_id)
            self._D_cfdi = Storage()
            self._D_resumenImpuestos = Storage()

            return

        def procesar(
                self,
                b_grabar = False,
                ):

            _D_return = Storage(
                E_return = stvfwk2_e_RETURN.OK,
                s_msgError = "",
                s_campoconerror = "",  # Nombre del campo con el error
                n_id = 0,
                s_json = ""
                )

            # Siempre se puede generar el JSON ya que es representación solamente y no se obtiene del SAT
            self._D_cfdi = Storage(
                Version = self._dbRow_cfdi.versioncfdi,
                Serie = self._dbRow_cfdi.serie,
                Folio = self._dbRow_cfdi.folio,
                Fecha = self._dbRow_cfdi.fecha_str,
                MetodoPago = self._dbRow_cfdi.metodopago,
                FormaPago = self._dbRow_cfdi.formapago,
                CondicionesDePago = self._dbRow_cfdi.condicionesdepago,
                TipoDeComprobante = self._dbRow_cfdi.tipodecomprobante,
                Moneda = self._dbRow_cfdi.moneda,
                TipoCambio = self._dbRow_cfdi.tipocambio,
                LugarExpedicion = self._dbRow_cfdi.lugarexpedicion,
                SubTotal = self._dbRow_cfdi.subtotal or 0,
                Descuento = self._dbRow_cfdi.descuento or 0,
                Total = self._dbRow_cfdi.total or 0,
                Emisor = Storage(
                    Nombre = self._dbRow_cfdi.emisornombrerazonsocial,
                    RegimenFiscal = self._dbRow_cfdi.emisorregimenfiscal,
                    Rfc = self._dbRow_cfdi.emisorrfc
                    ),
                Receptor = Storage(
                    Nombre = self._dbRow_cfdi.receptornombrerazonsocial,
                    UsoCFDI = self._dbRow_cfdi.receptorusocfdi,
                    Rfc = self._dbRow_cfdi.receptorrfc
                    ),
                Conceptos = [],
                Impuestos = Storage(
                    Trasladados = [],
                    Retenidos = [],
                    ),
                )

            _dbRows_conceptos = self._dbRow_cfdi.tcfdi_conceptos.select()

            for _dbRow_concepto in _dbRows_conceptos:

                self._procesar_concepto(
                    _dbRow_concepto,
                    self._D_cfdi.Conceptos
                    )

            _dbRows_relacionados = self._dbRow_cfdi.tcfdi_relacionados.select()

            if _dbRows_relacionados:

                self._D_cfdi.CfdiRelacionados = Storage(
                    TipoRelacion = self._dbRow_cfdi.tiporelacion,
                    CfdiRelacionado = []
                    )

                for _dbRow_relacionado in _dbRows_relacionados:

                    self._procesar_relacionado(
                        _dbRow_relacionado,
                        self._D_cfdi.CfdiRelacionados.CfdiRelacionado
                        )

            _dbRows_comppagos = self._dbRow_cfdi.tcfdi_complementopagos.select()

            if _dbRows_comppagos:

                # Todos los complementos pago deben ser de la misma version
                self._D_cfdi.Complemento = Storage(
                    Pagos = Storage(
                        Version = _dbRows_comppagos.first().versionpago,
                        Pago = []
                        )
                    )

                for _dbRow_comppago in _dbRows_comppagos:

                    self._procesar_comppago(
                        _dbRow_comppago,
                        self._D_cfdi.Complemento.Pagos.Pago
                        )

            if self._D_resumenImpuestos:

                if self._D_resumenImpuestos.trasladados:

                    self._D_cfdi.Impuestos.TotalImpuestosTrasladados = 0

                    for _s_index, _D_impuesto in self._D_resumenImpuestos.trasladados.items():
                        _D_impuestosTraspadado = Storage(
                            Impuesto = _D_impuesto.impuesto,
                            TasaOCuota = _D_impuesto.tasaocuota,
                            TipoFactor = _D_impuesto.tipofactor,
                            Importe = _D_impuesto.importe,
                            )

                        self._D_cfdi.Impuestos.Trasladados.append(_D_impuestosTraspadado)

                        self._D_cfdi.Impuestos.TotalImpuestosTrasladados += _D_impuesto.importe

                else:
                    # No hay resumen para impuestos trasladados
                    pass

                if self._D_resumenImpuestos.retenidos:

                    self._D_cfdi.Impuestos.TotalImpuestosRetenidos = 0

                    for _s_index, _D_impuesto in self._D_resumenImpuestos.retenidos.items():
                        _D_impuestosRetenido = Storage(
                            Impuesto = _D_impuesto.impuesto,
                            TasaOCuota = _D_impuesto.tasaocuota,
                            TipoFactor = _D_impuesto.tipofactor,
                            Importe = _D_impuesto.importe,
                            )

                        self._D_cfdi.Impuestos.Trasladados.append(_D_impuestosRetenido)

                        self._D_cfdi.Impuestos.TotalImpuestosRetenidos += _D_impuesto.importe

                else:
                    # No hay resumen para impuestos trasladados
                    pass

            else:
                # No hay impuestos en los conceptos de este CFDI
                pass

            _dbRows_cartaPorte = self._dbRow_cfdi.tcfdi_compcartaporte.select()

            if _dbRows_cartaPorte:

                if not self._D_cfdi.Complemento:
                    self._D_cfdi.Complemento = Storage()
                else:
                    pass

                self._D_cfdi.Complemento.CartaPorte = []

                # En teoría debería ser solamente un detalle de carta porte por CFDI
                for _dbRow_cartaPorte in _dbRows_cartaPorte:

                    self._procesar_cartaporte(
                        _dbRow_cartaPorte,
                        self._D_cfdi.Complemento.CartaPorte
                        )
            else:
                pass

            _D_return.s_json = simplejson.dumps(self._D_cfdi, cls = STV_FWK_LIB.DecimalEncoder)

            if b_grabar:
                _dbRows_xml = dbc02(
                    dbc02.tcfdi_xmls.cfdi_id == self._dbRow_cfdi.id
                    ).select(
                        dbc02.tcfdi_xmls.ALL
                        )

                if len(_dbRows_xml) == 1:
                    _dbRow_xml = _dbRows_xml.first()
                    _dbRow_xml.json_cfdi = _D_return.s_json
                    _dbRow_xml.update_record()
                    _s_cfdixml_id = _dbRow_xml.id
                else:
                    if len(_dbRows_xml) > 1:
                        _dbRows_xml.delete()
                    else:
                        pass

                    _D_insert = Storage(
                        cfdi_id = self._dbRow_cfdi.id,
                        json_cfdi = _D_return.s_json,
                        )
                    _s_cfdixml_id = dbc02.tcfdi_xmls.insert(**_D_insert)
            else:
                pass

            return _D_return

        def _procesar_concepto(
                self,
                dbRow_concepto,
                D_conceptos
                ):

            dbRow_concepto.unidad = dbRow_concepto.unidad or ""

            _D_concepto = Storage(
                Cantidad = dbRow_concepto.cantidad or 0,
                Unidad = dbRow_concepto.unidad,
                ClaveUnidad = dbRow_concepto.claveunidad,
                NoIdentificacion = dbRow_concepto.noidentificacion,
                ClaveProdServ = dbRow_concepto.claveprodserv,
                Descripcion = dbRow_concepto.descripcion,
                ValorUnitario = dbRow_concepto.valorunitario or 0,
                Descuento = dbRow_concepto.descuento or 0,
                Importe = dbRow_concepto.importe or 0,
                )

            _D_concepto.Impuestos = Storage(
                Trasladados = [],
                Retenidos = []
                )

            _dbRows_impuestosTrasladados = dbRow_concepto.tcfdi_concepto_impuestostrasladados.select()
            _dbRows_impuestosRetenidos = dbRow_concepto.tcfdi_concepto_impuestosretenidos.select()

            if _dbRows_impuestosTrasladados:

                if not self._D_resumenImpuestos.trasladados:
                    self._D_resumenImpuestos.trasladados = Storage()

                else:
                    pass

                for _dbRow_impTrasladado in _dbRows_impuestosTrasladados:

                    self._procesar_concepto_impuesto(
                        _dbRow_impTrasladado,
                        _D_concepto.Impuestos.Trasladados,
                        self._D_resumenImpuestos.trasladados
                        )

            else:
                # No hay impuestos trasladados
                pass

            if _dbRows_impuestosRetenidos:

                if not self._D_resumenImpuestos.retenidos:
                    self._D_resumenImpuestos.retenidos = Storage()
                else:
                    pass

                for _dbRow_impRetenido in _dbRows_impuestosRetenidos:

                    self._procesar_concepto_impuesto(
                        _dbRow_impRetenido,
                        _D_concepto.Impuestos.Retenidos,
                        self._D_resumenImpuestos.retenidos
                        )

            else:
                # No hay impuestos trasladados
                pass

            D_conceptos.append(_D_concepto)

            return

        def _procesar_concepto_impuesto(
                self,
                dbRow_impuesto,
                D_impuestos,
                D_resumenImpuesto
                ):
            _ = self

            _D_impuesto = Storage(
                Base = dbRow_impuesto.base or Decimal(0.0),
                Impuesto = dbRow_impuesto.impuesto,
                TasaOCuota = dbRow_impuesto.tasaocuota or Decimal(0.0),
                TipoFactor = dbRow_impuesto.tipofactor or "Tasa",  # TODO como usar tipofactor, donde se define
                Importe = dbRow_impuesto.importe or Decimal(0.0),
                )

            _s_index = str(dbRow_impuesto.impuesto) + "_" + str(dbRow_impuesto.tasaocuota)

            if not D_resumenImpuesto[_s_index]:
                D_resumenImpuesto[_s_index] = Storage(
                    impuesto = dbRow_impuesto.impuesto,
                    tasaocuota = dbRow_impuesto.tasaocuota or Decimal(0.0),
                    tipofactor = dbRow_impuesto.tipofactor or "Tasa",
                    importe = dbRow_impuesto.importe or Decimal(0.0),
                    )

            else:
                D_resumenImpuesto[_s_index].importe += dbRow_impuesto.importe

            D_impuestos.append(_D_impuesto)

            return

        def _procesar_relacionado(
                self,
                dbRow_relacionado,
                D_relacionados
                ):
            _ = self

            _D_relacionado = Storage(
                UUID = dbRow_relacionado.uuid,
                )

            D_relacionados.append(_D_relacionado)

            return

        def _procesar_comppago(
                self,
                dbRow_comppago,
                D_pagos
                ):

            _D_comppago = Storage(
                FechaPago = dbRow_comppago.fechapago_str,
                FormaDePagoP = dbRow_comppago.formapago,
                MonedaP = dbRow_comppago.moneda,
                TipoCambioP = dbRow_comppago.tipocambio,
                Monto = dbRow_comppago.monto or Decimal(0.0),
                DoctoRelacionado = []
                )

            _dbRows_doctoRelacionados = dbRow_comppago.tcfdi_complementopago_doctosrelacionados.select()

            for _dbRow_doctoRelacionado in _dbRows_doctoRelacionados:

                self._procesar_comppago_doctorel(
                    _dbRow_doctoRelacionado,
                    _D_comppago.DoctoRelacionado
                    )

            D_pagos.append(_D_comppago)

            return

        def _procesar_comppago_doctorel(
                self,
                dbRow_doctoRelacionado,
                D_doctoRelacionados
                ):
            _ = self

            _D_doctoRelacionado = Storage(
                Folio = dbRow_doctoRelacionado.folio,
                IdDocumento = dbRow_doctoRelacionado.iddocumento,
                ImpPagado = dbRow_doctoRelacionado.imppagado or Decimal(0.0),
                ImpSaldoAnt = dbRow_doctoRelacionado.impsaldoant or Decimal(0.0),
                ImpSaldoInsoluto = dbRow_doctoRelacionado.impsaldoinsoluto or Decimal(0.0),
                MetodoDePagoDR = dbRow_doctoRelacionado.metodopago,
                MonedaDR = dbRow_doctoRelacionado.moneda,
                TipoCambioDR = dbRow_doctoRelacionado.tipocambio,
                NumParcialidad = dbRow_doctoRelacionado.numparcialidad,
                Serie = dbRow_doctoRelacionado.serie,
                )

            D_doctoRelacionados.append(_D_doctoRelacionado)

            return

        def _procesar_cartaporte(
                self,
                dbRow_cartaPorte,
                L_cartaPorte
                ):
            _ = self

            _D_cartaPorte = Storage(
                versioncp = dbRow_cartaPorte.versioncp,
                transpinternac = dbRow_cartaPorte.transpinternac,
                entradasalidamerc = dbRow_cartaPorte.entradasalidamerc,
                viaentradasalida = dbRow_cartaPorte.viaentradasalida,
                totaldistrec = dbRow_cartaPorte.totaldistrec,
                merc_pesobrutototal = dbRow_cartaPorte.merc_pesobrutototal,
                merc_unidadpeso = dbRow_cartaPorte.merc_unidadpeso,
                merc_pesonetototal = dbRow_cartaPorte.merc_pesonetototal,
                merc_numtotalmercancias = dbRow_cartaPorte.merc_numtotalmercancias,
                merc_cargoportasacion = dbRow_cartaPorte.merc_cargoportasacion,
                figtransporte_cvetransporte = dbRow_cartaPorte.figtransporte_cvetransporte,
                L_ubicaciones = [],
                L_mercancias = [],
                L_autotransporte = [],
                L_personas = []
                )

            _dbRows_ubicaciones = dbRow_cartaPorte.tcfdi_compcartaporte_ubicaciones.select()
            for _dbRow_ubicacion in _dbRows_ubicaciones:
                _D_cartaPorte.L_ubicaciones.append(
                    Storage(
                        tipoestacion = _dbRow_ubicacion.tipoestacion,
                        distanciarecorrida = _dbRow_ubicacion.distanciarecorrida,
                        origen_idorigen = _dbRow_ubicacion.origen_idorigen,
                        origen_rfcremitente = _dbRow_ubicacion.origen_rfcremitente,
                        origen_nombreremitente = _dbRow_ubicacion.origen_nombreremitente,
                        origen_numregidtrib = _dbRow_ubicacion.origen_numregidtrib,
                        origen_recidenciafiscal = _dbRow_ubicacion.origen_recidenciafiscal,
                        origen_numestacion = _dbRow_ubicacion.origen_numestacion,
                        origen_nombreestacion = _dbRow_ubicacion.origen_nombreestacion,
                        origen_navegaciontrafico = _dbRow_ubicacion.origen_navegaciontrafico,
                        origen_fechahorasalida_str = _dbRow_ubicacion.origen_fechahorasalida_str,
                        destino_iddestino = _dbRow_ubicacion.destino_iddestino,
                        destino_rfcdestinatario = _dbRow_ubicacion.destino_rfcdestinatario,
                        destino_nombredestinatario = _dbRow_ubicacion.destino_nombredestinatario,
                        destino_numregidtrib = _dbRow_ubicacion.destino_numregidtrib,
                        destino_recidenciafiscal = _dbRow_ubicacion.destino_recidenciafiscal,
                        destino_numestacion = _dbRow_ubicacion.destino_numestacion,
                        destino_nombreestacion = _dbRow_ubicacion.destino_nombreestacion,
                        destino_navegaciontrafico = _dbRow_ubicacion.destino_navegaciontrafico,
                        destino_fechahoraprogllegada_str = _dbRow_ubicacion.destino_fechahoraprogllegada_str,
                        dom_pais = _dbRow_ubicacion.dom_pais,
                        dom_estado = _dbRow_ubicacion.dom_estado,
                        dom_municipio = _dbRow_ubicacion.dom_municipio,
                        dom_referencia = _dbRow_ubicacion.dom_referencia,
                        dom_localidad = _dbRow_ubicacion.dom_localidad,
                        dom_colonia = _dbRow_ubicacion.dom_colonia,
                        dom_calle = _dbRow_ubicacion.dom_calle,
                        dom_numexterior = _dbRow_ubicacion.dom_numexterior,
                        dom_numinterior = _dbRow_ubicacion.dom_numinterior,
                        dom_codigopostal = _dbRow_ubicacion.dom_codigopostal,
                        )
                    )

            _dbRows_mercancias = dbRow_cartaPorte.tcfdi_compcartaporte_mercancias.select()
            for _dbRow_mercancia in _dbRows_mercancias:
                _D_cartaPorte.L_mercancias.append(
                    Storage(
                        claveprodservcp = _dbRow_mercancia.claveprodservcp,
                        clavestcc = _dbRow_mercancia.clavestcc,
                        descripcion = _dbRow_mercancia.descripcion,
                        cantidad = _dbRow_mercancia.cantidad,
                        claveunidad = _dbRow_mercancia.claveunidad,
                        unidad = _dbRow_mercancia.unidad,
                        dimensiones = _dbRow_mercancia.dimensiones,
                        materialpeligroso = _dbRow_mercancia.materialpeligroso,
                        cvematerialpeligroso = _dbRow_mercancia.cvematerialpeligroso,
                        embalaje = _dbRow_mercancia.embalaje,
                        descripembalaje = _dbRow_mercancia.descripembalaje,
                        pesoenkg = _dbRow_mercancia.pesoenkg,
                        valormercancia = _dbRow_mercancia.valormercancia,
                        moneda = _dbRow_mercancia.moneda,
                        fraccionarancelaria = _dbRow_mercancia.fraccionarancelaria,
                        uuidcomercioext = _dbRow_mercancia.uuidcomercioext,
                        detallemerc_unidadpeso = _dbRow_mercancia.detallemerc_unidadpeso,
                        detallemerc_pesobruto = _dbRow_mercancia.detallemerc_pesobruto,
                        detallemerc_pesoneto = _dbRow_mercancia.detallemerc_pesoneto,
                        detallemerc_pesotara = _dbRow_mercancia.detallemerc_pesotara,
                        detallemerc_numpiezas = _dbRow_mercancia.detallemerc_numpiezas,
                        )
                    )

            _dbRows_autotransporte = dbRow_cartaPorte.tcfdi_compcartaporte_autotransporte.select()
            for _dbRow_autotransporte in _dbRows_autotransporte:
                _D_cartaPorte.L_autotransporte.append(
                    Storage(
                        permsct = _dbRow_autotransporte.permsct,
                        numpermisosct = _dbRow_autotransporte.numpermisosct,
                        nombreaseg = _dbRow_autotransporte.nombreaseg,
                        numpolizaseguro = _dbRow_autotransporte.numpolizaseguro,
                        idvehic_configvehicular = _dbRow_autotransporte.idvehic_configvehicular,
                        idvehic_placavm = _dbRow_autotransporte.idvehic_placavm,
                        idvehic_aniomodelovm = _dbRow_autotransporte.idvehic_aniomodelovm,
                        remolque1_subtiporem = _dbRow_autotransporte.remolque1_subtiporem,
                        remolque1_placa = _dbRow_autotransporte.remolque1_placa,
                        remolque2_subtiporem = _dbRow_autotransporte.remolque2_subtiporem,
                        remolque2_placa = _dbRow_autotransporte.remolque2_placa,
                        )
                    )

            _dbRows_personas = dbRow_cartaPorte.tcfdi_compcartaporte_figurastransporepersonas.select()
            for _dbRow_persona in _dbRows_personas:
                _D_cartaPorte.L_personas.append(
                    Storage(
                        tipofiguratransporte = _dbRow_persona.tipofiguratransporte,
                        rfc = _dbRow_persona.rfc,
                        numlicencia = _dbRow_persona.numlicencia,
                        nombre = _dbRow_persona.nombre,
                        numregidtriboperador = _dbRow_persona.numregidtriboperador,
                        residenciafiscal = _dbRow_persona.residencia_pais_id,
                        dom_calle = _dbRow_persona.dom_calle,
                        dom_numexterior = _dbRow_persona.dom_numexterior,
                        dom_numinterior = _dbRow_persona.dom_numinterior,
                        dom_colonia = _dbRow_persona.dom_colonia,
                        dom_localidad = _dbRow_persona.dom_localidad,
                        dom_referencia = _dbRow_persona.dom_referencia,
                        dom_municipio = _dbRow_persona.dom_municipio,
                        dom_estado = _dbRow_persona.dom_municipio,
                        dom_pais = _dbRow_persona.dom_municipio,
                        dom_codigopostal = _dbRow_persona.dom_codigopostal,
                        )
                    )

            L_cartaPorte.append(_D_cartaPorte)

            return

        pass

    class VALIDAR(object):

        class E_PROCESO:
            COMPLETO = 0
            PAGO_RAPIDO = 1

        def __init__(
                self,
                x_cfdi
                ):

            if isinstance(x_cfdi, (str, int)):
                self._dbRow_cfdi = dbc01.tcfdis(x_cfdi)
            else:
                self._dbRow_cfdi = x_cfdi

            self._n_borrar_cfdi_iniciador = None
            self._L_borrar_cfdisIngresos_impactados = []
            self._L_actualizarConcepto_despuesAccion = []
            self._n_borrarCfdiComplemento = None

            return

        def form_validar_movtoEstadoCuenta(
                self,
                O_form,
                ):

            """ Rutina que mapea campos del CFDI, recalcula los totales y genera los movimientos del estado de cuenta

            """

            if self._dbRow_cfdi.sat_status != TCFDIS.SAT_STATUS.VIGENTE:
                O_form.errors.id = "No se puede ajustar saldo de un CFDI que no esta vigente"
            else:
                pass

            if O_form.record_id:
                _dbRow_movmimiento_anterior = dbc01.tcfdi_manejosaldos(O_form.record_id)
                _f_importeCambio = O_form.vars.importe - _dbRow_movmimiento_anterior.importe
            else:
                _f_importeCambio = O_form.vars.importe

            if _f_importeCambio > (self._dbRow_cfdi.saldo * (-1)):
                O_form.errors.importe = "Importe no puede ser mayor al saldo de la factura"

            elif not O_form.vars.cfdirelacionado_id:
                O_form.errors.cfdirelacionado_id = "Debe tener un CFDI relacionado"

            else:
                _dbRow_cfdi_relacionado = dbc01.tcfdis(O_form.vars.cfdirelacionado_id)

                if not _dbRow_cfdi_relacionado:
                    O_form.errors.cfdirelacionado_id = "CFDI relacionado no existe"

                elif _dbRow_cfdi_relacionado.saldo < _f_importeCambio:
                    O_form.errors.importe = "Saldo de CFDI relacionado es menor que el importe: %.2f" \
                                            % _dbRow_cfdi_relacionado.saldo

                else:
                    pass

            return O_form

        def form_movtoEstadoCuenta(
                self,
                O_form_or_L_errors,
                ):

            """ Rutina que mapea campos del CFDI, recalcula los totales y genera los movimientos del estado de cuenta
            Utilizada al aceptar actualizar o al terminar de eliminar.

            """

            _D_result = STV_LIB_CFDI.ESTADOCUENTA(
                n_empresa_id = self._dbRow_cfdi.empresa_id,
                n_cliente_id = self._dbRow_cfdi.receptorcliente_id,
                E_movto_generado_por = TCFDI_MOVIMIENTOS.E_GENERADO_POR.DOCUMENTO,
                ).procesar_cfdi(self._dbRow_cfdi)

            if _D_result.E_return > stvfwk2_e_RETURN.OK_WARNING:
                stv_flashWarning(
                    'Estado de cuenta', "CFDI no puede generar estado de cuenta. " + _D_result.s_msgError,
                    s_mode = 'stv_flash', D_addProperties = {}
                    )
            else:
                pass

            return O_form_or_L_errors

    class ESTADOCUENTA(object):
        """ Genera información de los movimientos del cliente, el estado de cuenta de los CFDIs para determinar
         el saldo del cliente.
        Recorre todos los movimientos a partir del primer día del 2020 y calcula el saldo del cliente.
        Utilizando los ingresos, pagos y egresos.
        """
        E_NINGUNO = 0
        E_CLIENTE = 1
        E_CFDI = 2

        def __init__(
                self,
                n_empresa_id,
                n_cliente_id,
                E_movto_generado_por = None
                ):
            self._n_return = stvfwk2_e_RETURN.OK
            self._n_empresa_id = int(n_empresa_id)
            self._n_cliente_id = int(n_cliente_id)
            self._L_msgErrors = []
            self._L_msgWarnings = []
            self._L_msgInformativos = []
            self._D_registros = Storage()
            self._dbRow_cliente = None
            self._f_saldoCliente = 0
            self._f_saldoVencidoCliente = 0
            self._f_saldoPendienteTimbrar = 0
            self._f_saldoTimbrado = 0
            self._dt_empezarde = TCFDIS.GET_PRIMERDIA2020().dt_utc
            self._dt_procesoInicio = datetime.datetime.now()
            self._dt_procesoFin = datetime.datetime.now()
            self._E_proceso = self.E_NINGUNO
            self._E_movto_generado_por = E_movto_generado_por or TCFDI_MOVIMIENTOS.E_GENERADO_POR.NO_DEFINIDO

            self._s_mensajeDebug = "Inicialización"
            return

        def _addError(
                self,
                s_msgError
                ):
            self._n_return = stvfwk2_e_RETURN.NOK_ERROR
            self._L_msgErrors.append(s_msgError)
            return

        def _addWarning(
                self,
                s_msgWarning
                ):
            if self._n_return != stvfwk2_e_RETURN.NOK_ERROR:
                self._n_return = stvfwk2_e_RETURN.OK_WARNING
            else:
                pass
            self._L_msgWarnings.append(s_msgWarning)
            return

        def _addInformativo(
                self,
                s_msgInformativo
                ):
            self._L_msgInformativos.append(s_msgInformativo)
            return

        def _addMovimiento(
                self,
                dt_fecha,
                E_accion,
                E_tipodocumento,
                s_descripcion,
                dbRow_cfdi,
                n_cfdi_ingreso_id,
                n_cfdi_complementopago_doctorelacionado_id = None,
                n_cfdi_relacionado_id = None,
                dbRow_relacionado = None,
                n_cfdi_movimiento_emision_id = None,
                n_cfdi_manejosaldo_id = None,
                f_importe = 0,
                f_cargo = 0,
                f_abono = 0,
                f_saldoPrevio = 0,
                s_monedacontpaqi_id = None,
                f_tipocambio = 1
                ):
            """ Registra un movimiento en la tabla de movimientos y agrega información a un dict del objeto.
            
            @param dt_fecha: fecha del movimiento principal.
            @param E_accion: TCFDI_MOVIMIENTOS.E_ACCION
            @param E_tipodocumento: TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO
            @param s_descripcion: descripción del movmiento.
            @param dbRow_cfdi: dbRow con la infromación del CFDI.
            @param n_cfdi_ingreso_id: id del ingreso relacionado.
            @param n_cfdi_complementopago_doctorelacionado_id: en caso de pagos, es el id del complemento
             de pago relacionado.
            @param n_cfdi_relacionado_id: en caso de egresos, es el id del documento relacionado.
            @param n_cfdi_movimiento_emision_id: en caso de egresos, es el id del detalle de referencias
             al documento referenciado.
            @param f_importe: importe del movimiento en caso de estar claramente definido, si no es posible
             determinarlo, debe ser cero.
            @param f_cargo: usado únicamente cuando se procesan CFDIs individuales.
            @param f_abono: usado únicamente cuando se procesan CFDIs individuales.
            @param f_saldo: usado únicamente cuando se procesan CFDIs individuales.
            """

            _D_return = FUNC_RETURN(
                n_cfdi_movimiento_id = n_cfdi_movimiento_emision_id,
                n_cfdi_movimientoCancelacion_id = None,
                f_saldoNuevo = f_saldoPrevio,
                f_importeNuevo = f_importe
                )

            if n_cfdi_ingreso_id not in self._D_registros:
                # Debería entrar aqui solamente para los CFDIs de ingreso
                if (self._E_proceso == self.E_CLIENTE) \
                        and (E_tipodocumento == TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.INGRESO):
                    self._D_registros[n_cfdi_ingreso_id] = Storage(
                        dbRow = dbRow_cfdi,
                        saldo = 0,
                        cargos = 0,  # Cargos ya identificados en la primer etapa
                        abonos = 0,  # Abonos ya identificados en la primer etapa
                        aplicadoscargos = 0,  # Cargos identificados en fase 1 que ya fueron aplicados
                        aplicadosabonos = 0,  # Abonos identificados en fase 1 que ya fueron aplicados
                        variablescargos = 0,  # Cargos cuyo importe no identificado en fase 1 que ya fueron aplicados
                        variablesabonos = 0,  # Abonos cuyo importe no identificado en fase 1 que ya fueron aplicados
                        # Todas las referencia de CFDIs relacionados con el ingreso, el key es el cfdi_id
                        referencias = Storage(),
                        # Referencias únicamente a los CFDIs que no pudo determinarse el monto del cargo/abono
                        referenciasNoAplicadas = Storage(),
                        referenciasRelacionados = Storage(),
                        s_monedacontpaqi_id = s_monedacontpaqi_id,  # Moneda del ingreso
                        f_tipocambio = f_tipocambio  # Tipo de cambio del ingreso
                        )

                elif self._E_proceso == self.E_CFDI:
                    # Aqui entra, si esta procesando individualmente un CFDI de cualquier tipo

                    self._D_registros[n_cfdi_ingreso_id] = Storage(
                        # Se manda el ingreso en dbRow_relacionado en caso de pagos y egresos, de lo contrario
                        #  el ingreso es dbRow_cfdi
                        dbRow = dbRow_relacionado or dbRow_cfdi,
                        saldo = 0,
                        cargos = 0,  # Cargos ya identificados en la primer etapa
                        abonos = 0,  # Abonos ya identificados en la primer etapa
                        aplicadoscargos = 0,  # Cargos identificados en fase 1 que ya fueron aplicados
                        aplicadosabonos = 0,  # Abonos identificados en fase 1 que ya fueron aplicados
                        variablescargos = 0,  # Cargos cuyo importe no identificado en fase 1 que ya fueron aplicados
                        variablesabonos = 0,  # Abonos cuyo importe no identificado en fase 1 que ya fueron aplicados
                        # Todas las referencia de CFDIs relacionados con el ingreso, el key es el cfdi_id
                        referencias = Storage(),
                        # Referencias únicamente a los CFDIs que no pudo determinarse el monto del cargo/abono
                        referenciasNoAplicadas = Storage(),
                        referenciasRelacionados = Storage(),
                        s_monedacontpaqi_id = s_monedacontpaqi_id,  # Moneda del ingreso
                        f_tipocambio = f_tipocambio  # Tipo de cambio del ingreso
                        )

                else:
                    # Es muy probable que el CFDI de ingreso se ignoro, debido a que no esta en el rango de
                    #  determinación de saldo
                    pass
            else:
                pass

            if n_cfdi_ingreso_id in self._D_registros:

                _D_ingreso = self._D_registros[n_cfdi_ingreso_id]
                if self._E_proceso == self.E_CLIENTE:
                    # Si es el proceso del cliente, primero se graban los ingresos y luego los pagos y egresos
                    _s_monedacontpaqi_aComparar = _D_ingreso.s_monedacontpaqi_id

                elif self._E_proceso == self.E_CFDI:
                    # si es el proceso por CFDI, solo se graba o el ingreso, egreso o pago en un solo movimiento
                    _s_monedacontpaqi_aComparar = _D_ingreso.dbRow.monedacontpaqi_id

                else:
                    _s_monedacontpaqi_aComparar = _D_ingreso.s_monedacontpaqi_id

                if _s_monedacontpaqi_aComparar != s_monedacontpaqi_id:
                    # Si la moneda del ingreso es diferente a la moneda del movimiento
                    _f_factor = f_tipocambio / _D_ingreso.f_tipocambio
                    _D_return.f_importeNuevo *= _f_factor
                    f_cargo *= _f_factor
                    f_abono *= _f_factor
                    _D_return.f_saldoNuevo = f_saldoPrevio + f_cargo - f_abono
                else:
                    # Misma moneda que ingreso
                    pass

                if E_tipodocumento in (
                        TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.INGRESO,
                        TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DEBITO,
                        TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.TRASPASO_SALDO_A_FAVOR,
                        ):
                    self._D_registros[n_cfdi_ingreso_id].cargos += _D_return.f_importeNuevo

                elif E_tipodocumento in (
                        TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.PAGO,
                        TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DEVOLUCION,
                        TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DESCUENTO,
                        TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.ANTICIPO,
                        TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.APLICACION_SALDO_A_FAVOR,
                        ):

                    self._D_registros[n_cfdi_ingreso_id].abonos += _D_return.f_importeNuevo

                else:
                    self._addError(
                        "CFDI %d tipo de documento no identificado %s, Llamar al admministrador."
                        % (dbRow_cfdi.id, TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.GET_DICT()[E_tipodocumento])
                        )
                    return _D_return

                # Si el importe es 0, no se pudo determinar el monto a aplicar, por lo que se graba la referencia
                #  para su uso en la siguiente etapa para determinar el saldo a aplicar
                if (_D_return.f_importeNuevo == 0) and (E_tipodocumento != TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.INGRESO):
                    self._D_registros[n_cfdi_ingreso_id].referenciasNoAplicadas[dbRow_cfdi.id] = dbRow_cfdi
                else:
                    pass
                self._D_registros[n_cfdi_ingreso_id].referencias[dbRow_cfdi.id] = dbRow_cfdi
                if n_cfdi_relacionado_id:
                    self._D_registros[n_cfdi_ingreso_id].referenciasRelacionados[n_cfdi_relacionado_id] = \
                        dbRow_relacionado
                else:
                    pass

                _D_insertarMovimiento = Storage(
                    empresa_id = self._n_empresa_id,
                    cliente_id = self._n_cliente_id,
                    fecha = dt_fecha,
                    fechaingreso = self._D_registros[n_cfdi_ingreso_id].dbRow.fecha,
                    accion = E_accion
                    if dbRow_cfdi.sat_status != TCFDIS.SAT_STATUS.NO_DEFINIDO
                    else TCFDI_MOVIMIENTOS.E_ACCION.NO_TIMBRADA,
                    tipodocumento = E_tipodocumento,
                    descripcion = s_descripcion,
                    cfdi_id = dbRow_cfdi.id,
                    cfdi_complementopago_doctorelacionado_id = n_cfdi_complementopago_doctorelacionado_id,
                    cfdi_relacionado_id = n_cfdi_relacionado_id,
                    cfdi_ingreso_id = n_cfdi_ingreso_id,
                    cfdi_movimiento_emision_id = _D_return.n_cfdi_movimiento_id,
                    cfdi_manejosaldo_id = n_cfdi_manejosaldo_id,
                    cargo = f_cargo,
                    abono = f_abono,
                    # Si el no esta timbrado, no se agrega al saldo del CFDI
                    saldo = _D_return.f_saldoNuevo
                    if dbRow_cfdi.sat_status != TCFDIS.SAT_STATUS.NO_DEFINIDO else 0,
                    importe = _D_return.f_importeNuevo,
                    importe_monedapago = f_importe,
                    monedacontpaqi_id = s_monedacontpaqi_id or TEMPRESA_MONEDASCONTPAQI.MXP,
                    tipocambio = f_tipocambio,
                    generado_por = self._E_movto_generado_por
                    )

                if (self._E_proceso == self.E_CLIENTE) or (E_accion == TCFDI_MOVIMIENTOS.E_ACCION.EMISION):
                    # Si esta ejecutando el proceso de cálculo por clientes, o la acción a insertar es una emisión
                    _D_return.n_cfdi_movimiento_id = dbc01.tcfdi_movimientos.insert(**_D_insertarMovimiento)
                else:
                    pass

                if dbRow_cfdi.sat_status == TCFDIS.SAT_STATUS.CANCELADO:
                    if dbRow_cfdi.fecha >= dbRow_cfdi.sat_fecha_cancelacion:
                        _dt_fechaCancelacion = dbRow_cfdi.fecha + datetime.timedelta(seconds = 1)
                    else:
                        _dt_fechaCancelacion = dbRow_cfdi.sat_fecha_cancelacion

                    _D_insertarMovimiento.fecha = _dt_fechaCancelacion
                    _D_insertarMovimiento.accion = TCFDI_MOVIMIENTOS.E_ACCION.CANCELACION
                    _D_insertarMovimiento.cfdi_movimiento_emision_id = _D_return.n_cfdi_movimiento_id

                    if (self._E_proceso == self.E_CLIENTE) or (E_accion == TCFDI_MOVIMIENTOS.E_ACCION.CANCELACION):
                        # Si esta ejecutando el proceso de cálculo de saldo por cliente, o la acción corresponse
                        #  a una cancelación
                        _D_return.n_cfdi_movimientoCancelacion_id = dbc01.tcfdi_movimientos.insert(
                            **_D_insertarMovimiento
                            )
                    else:
                        pass

                    if E_tipodocumento in (
                            TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.INGRESO,
                            TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DEBITO
                            ):
                        self._D_registros[n_cfdi_ingreso_id].abonos += _D_return.f_importeNuevo
                    else:
                        self._D_registros[n_cfdi_ingreso_id].cargos += _D_return.f_importeNuevo

                else:
                    pass

            else:
                # No se encuentra referencia a el ingreso del movimiento
                pass

            return _D_return

        def procesar_cliente(self):
            """ Se procesan todos los CFDIs del cliente para determinar su saldo

            Se empieza a partir de TCFDIS.GET_PRIMERDIA2020

            @return:
            @rtype:
            """

            self._E_proceso = self.E_CLIENTE
            self._dt_procesoInicio = datetime.datetime.now()

            self._dbRow_cliente = dbc01.tempresa_clientes(self._n_cliente_id)

            self._dbRow_cliente.procesostatus = TEMPRESA_CLIENTES.E_PROCESOESTATUS.CFDIS_PROCESANDO
            self._dbRow_cliente.update_record()
            dbc01.commit()

            # Se limpian los movimientos del cliente
            dbc01(
                (dbc01.tcfdi_movimientos.cliente_id == self._n_cliente_id)
                ).delete()

            # Todos los CFDIs del cliente, el saldo debe ser None para usar este estatus en caso de no se analizado
            dbc01(
                (dbc01.tcfdis.receptorcliente_id == self._n_cliente_id)
                & (dbc01.tcfdis.saldo != None)
                ).update(saldo = None)

            # Se toman todos los CFDIs del cliente
            _dbRows_cfdi = dbc01(
                (dbc01.tcfdis.receptorcliente_id == self._n_cliente_id)
                & (dbc01.tcfdis.fecha >= self._dt_empezarde)
                # & (dbc01.tcfdis.sat_status != TCFDIS.SAT_STATUS.NO_DEFINIDO)
                ).select(
                    dbc01.tcfdis.ALL,
                    orderby = [dbc01.tcfdis.fecha, dbc01.tcfdis.id]
                    )

            self._s_mensajeDebug = "Se recorren todos los cfdis encontrados del cliente despues de la fecha"
            _b_primerIngreso = False
            for _dbRow_cfdi in _dbRows_cfdi:

                if _b_primerIngreso \
                        or (_dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO):
                    _b_primerIngreso = True
                    self._cfdi(_dbRow_cfdi)
                else:
                    # TODO verificar que los primeros CFDIs antes del primero de ingreso, no tengan referencias validas
                    pass

            self._dbRow_cliente.procesostatus = TEMPRESA_CLIENTES.E_PROCESOESTATUS.CFDIS_APLICANDO_SALDOS
            self._dbRow_cliente.update_record()
            dbc01.commit()

            _dbRows_movtos = dbc01(
                dbc01.tcfdi_movimientos.cliente_id == self._n_cliente_id
                ).select(
                    dbc01.tcfdi_movimientos.ALL,
                    orderby = [
                        dbc01.tcfdi_movimientos.fecha,
                        dbc01.tcfdi_movimientos.fechaingreso
                        ]
                    )

            for _dbRow_movto in _dbRows_movtos:

                self._aplicacionsaldos(_dbRow_movto)

            for _s_cfdi_id in self._D_registros:
                self._D_registros[_s_cfdi_id].dbRow.saldo = self._D_registros[_s_cfdi_id].saldo
                self._D_registros[_s_cfdi_id].dbRow.saldofechacalculo = request.utcnow

                if self._D_registros[_s_cfdi_id].dbRow.credito_plazo:
                    pass

                else:
                    self._D_registros[_s_cfdi_id].dbRow.credito_plazo = self._dbRow_cliente.credito_plazo

                self._D_registros[_s_cfdi_id].dbRow.update_record()

                if (self._D_registros[_s_cfdi_id].dbRow.tipocomprobante_id != TTIPOSCOMPROBANTES.INGRESO) \
                        and (self._D_registros[_s_cfdi_id].dbRow.saldo != 0):
                    self._addError(
                        "CFDI %s, quedó con saldo '%.2f' sin aplicar."
                        % (_s_cfdi_id, self._D_registros[_s_cfdi_id].dbRow.saldo)
                        )

                else:
                    pass

            self.actualizar_saldo_cliente()

            _D_return = Storage(
                E_return = self._n_return,
                L_msgError = self._L_msgErrors,
                L_msgWarning = self._L_msgWarnings,
                L_msgInformativo = self._L_msgInformativos,
                )

            self._dt_procesoFin = datetime.datetime.now()
            self._addInformativo(
                "Proceso tomó %d segundos. Saldo de %.2f, saldo vencido de %.2f"
                % (
                    (self._dt_procesoFin - self._dt_procesoInicio).seconds,
                    self._f_saldoCliente,
                    self._f_saldoVencidoCliente
                    )
                )

            self._E_proceso = self.E_NINGUNO

            return _D_return

        def actualizar_saldo_cliente(
                self,
                ):

            if not self._dbRow_cliente:
                self._dbRow_cliente = dbc01.tempresa_clientes(self._n_cliente_id)
            else:
                pass

            _D_result = TEMPRESA_CLIENTES.OBTENER_SALDO_CLIENTE(
                x_cliente_id = self._dbRow_cliente,
                x_cfdi_id_ignorar = None,
                )

            self._dbRow_cliente.procesostatus = TEMPRESA_CLIENTES.E_PROCESOESTATUS.SIN_PROCESO
            self._dbRow_cliente.update_record()

            return

        def _cfdi(
                self,
                dbRow_cfdi
                ):
            """ Etapa 1 que procesa la información del CFDI
            
            @param dbRow_cfdi: CFDI a procesar
            """

            if dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO:

                self._addMovimiento(
                    dt_fecha = dbRow_cfdi.fecha,
                    E_accion = TCFDI_MOVIMIENTOS.E_ACCION.EMISION
                    if dbRow_cfdi.sat_status != TCFDIS.SAT_STATUS.NO_DEFINIDO
                    else TCFDI_MOVIMIENTOS.E_ACCION.NO_TIMBRADA,
                    E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.INGRESO,
                    s_descripcion = "Factura",
                    dbRow_cfdi = dbRow_cfdi,
                    n_cfdi_ingreso_id = dbRow_cfdi.id,
                    f_importe = dbRow_cfdi.total,
                    s_monedacontpaqi_id = dbRow_cfdi.monedacontpaqi_id,
                    f_tipocambio = dbRow_cfdi.tipocambio
                    )

                # Se obtienen los registros de traspaso aplicables al cfdi actual
                _dbRows_manejoSaldos = dbc01(
                    (dbc01.tcfdi_manejosaldos.cfdi_id == dbRow_cfdi.id)
                    ).select(
                        dbc01.tcfdi_manejosaldos.ALL,
                        )

                for _dbRow_manejoSaldo in _dbRows_manejoSaldos:

                    self._addMovimiento(
                        dt_fecha = _dbRow_manejoSaldo.fecha,
                        E_accion = TCFDI_MOVIMIENTOS.E_ACCION.EMISION,
                        E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.TRASPASO_SALDO_A_FAVOR,
                        s_descripcion = "Traspaso",
                        dbRow_cfdi = dbRow_cfdi,
                        n_cfdi_ingreso_id = dbRow_cfdi.id,
                        n_cfdi_manejosaldo_id = _dbRow_manejoSaldo.id,
                        f_importe = _dbRow_manejoSaldo.importe,
                        s_monedacontpaqi_id = dbRow_cfdi.monedacontpaqi_id,
                        f_tipocambio = dbRow_cfdi.tipocambio
                        )

                # Se obtienen los registros de aplicacion de saldo a favor aplicables al CFDI actual
                _dbRows_manejoSaldos = dbc01(
                    (dbc01.tcfdi_manejosaldos.cfdirelacionado_id == dbRow_cfdi.id)
                    ).select(
                        dbc01.tcfdi_manejosaldos.ALL,
                        )

                for _dbRow_manejoSaldo in _dbRows_manejoSaldos:

                    self._addMovimiento(
                        dt_fecha = _dbRow_manejoSaldo.fecha,
                        E_accion = TCFDI_MOVIMIENTOS.E_ACCION.EMISION,
                        E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.APLICACION_SALDO_A_FAVOR,
                        s_descripcion = "Aplicación",
                        dbRow_cfdi = dbRow_cfdi,
                        n_cfdi_ingreso_id = dbRow_cfdi.id,
                        n_cfdi_manejosaldo_id = _dbRow_manejoSaldo.id,
                        f_importe = _dbRow_manejoSaldo.importe,
                        s_monedacontpaqi_id = dbRow_cfdi.monedacontpaqi_id,
                        f_tipocambio = dbRow_cfdi.tipocambio
                        )

            elif dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.PAGO:

                dbRow_cfdi.totalpago = 0
                _dbRows_pagos = dbc01(
                    (dbc01.tcfdi_complementopagos.cfdi_id == dbRow_cfdi.id)
                    & (
                        dbc01.tcfdi_complementopago_doctosrelacionados.cfdi_complementopago_id
                        == dbc01.tcfdi_complementopagos.id
                        )
                    ).select(
                        dbc01.tcfdi_complementopago_doctosrelacionados.ALL,
                        dbc01.tcfdi_complementopagos.ALL,
                        )

                for _dbRow_pago in _dbRows_pagos:
                    self._cfdi_pago(
                        dbRow_cfdi,
                        _dbRow_pago
                        )

                dbRow_cfdi.update_record()

            elif dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.EGRESO:

                _dbRows_relacionados = dbc01(
                    (dbc01.tcfdi_relacionados.cfdi_id == dbRow_cfdi.id)
                    ).select(
                        dbc01.tcfdi_relacionados.ALL,
                        )

                _s_descripcion = ""
                _E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.NO_DEFINIDO
                if dbRow_cfdi.receptorusocfdi_id == TUSOSCFDI.DEVOLUCIONES_DESCUENTOS_BONIFICACIONES:

                    if dbRow_cfdi.tiporelacion_id == TTIPOSRELACIONES.NOTACREDITO:

                        _s_descripcion = "Descuento"
                        _E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DESCUENTO

                    elif dbRow_cfdi.tiporelacion_id == TTIPOSRELACIONES.DEVOLUCION:

                        _s_descripcion = "Devolucion"
                        _E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DEVOLUCION

                    else:

                        # Basado en la retroalimentación del cliente, se asigna como descuento                        
                        _s_descripcion = "Descuento"
                        _E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DESCUENTO
                        self._addWarning(
                            ("CFDI de Egreso %d, tipo relación '%s' no válido para devolucion/descuento/bonificación. "
                            + "Se asigna como descuento.") % (
                                dbRow_cfdi.id, dbRow_cfdi.tiporelacion_id
                                )
                            )

                else:

                    if dbRow_cfdi.tiporelacion_id == TTIPOSRELACIONES.NOTADEBITO:

                        _s_descripcion = "Nota de Debito"
                        _E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DEBITO

                    elif dbRow_cfdi.tiporelacion_id == TTIPOSRELACIONES.ANTICIPO:

                        _s_descripcion = "Anticipo"
                        _E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.ANTICIPO

                    elif dbRow_cfdi.tiporelacion_id == TTIPOSRELACIONES.NOTACREDITO:

                        _s_descripcion = "Descuento"
                        _E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DESCUENTO
                        self._addWarning(
                            "CFDI de Egreso %d, tipo relación %d no válido, pero se procede a manejarlo como descuento"
                            % (dbRow_cfdi.id, dbRow_cfdi.tiporelacion_id)
                            )

                    elif not dbRow_cfdi.tiporelacion_id:

                        _s_descripcion = "Descuento"
                        _E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DESCUENTO
                        self._addWarning("CFDI de Egreso %d, tipo relación esta vacío, pero se procede a manejarlo como descuento" % dbRow_cfdi.id)

                    else:

                        self._addError("CFDI de Egreso %d, tipo relación %d no válido" % (dbRow_cfdi.id, dbRow_cfdi.tiporelacion_id))

                if _s_descripcion and _E_tipodocumento:
                    _n_numRelacionados = len(_dbRows_relacionados)
                    for _dbRow_relacionado in _dbRows_relacionados:
                        self._cfdi_referencia(
                            dbRow_cfdi,
                            _dbRow_relacionado,
                            _s_descripcion,
                            _E_tipodocumento,
                            _n_numRelacionados
                            )
                else:
                    # El error ya se definió en la condición anterior
                    pass

            else:
                self._addError("Tipo de CFDI %d no válido %d" % (dbRow_cfdi.id, dbRow_cfdi.tipocomprobante_id))

            return

        def _cfdi_pago(
                self,
                dbRow_cfdi,
                dbRow_pago
                ):
            """ Se registra información de la parte 1 cuando es pago.
            
            @param dbRow_cfdi: CFDI principal del pago.
            @param dbRow_pago: Registro con información del complemento de pago y su documento relacionado.
            """

            if not dbRow_pago.tcfdi_complementopago_doctosrelacionados.cfdirelacionado_id:
                TCFDI_COMPLEMENTOPAGO_DOCTOSRELACIONADOS.ASOCIAR_UUID(
                    dbRow_pago.tcfdi_complementopago_doctosrelacionados.id
                    )

                _dbRow_complementoPago_docRelacionado_actualizado = dbc01.tcfdi_complementopago_doctosrelacionados(dbRow_pago.tcfdi_complementopago_doctosrelacionados.id)
                if not _dbRow_complementoPago_docRelacionado_actualizado.cfdirelacionado_id:
                    self._addWarning("CFDI referenciado (%s) en pago %d, es ignorado, ya que no se encuentra la referencia" % (dbRow_pago.tcfdi_complementopago_doctosrelacionados.iddocumento, dbRow_cfdi.id))
                    return
                else:
                    _cfdirelacionado_id = _dbRow_complementoPago_docRelacionado_actualizado.cfdirelacionado_id

            else:
                _cfdirelacionado_id = dbRow_pago.tcfdi_complementopago_doctosrelacionados.cfdirelacionado_id

            # _dbRow_cfdiRelacionado = dbc01.tcfdis(_cfdirelacionado_id)
            if _cfdirelacionado_id in self._D_registros:
                _dbRow_cfdiRelacionado = self._D_registros[_cfdirelacionado_id].dbRow

                if _dbRow_cfdiRelacionado.tipocomprobante_id != TTIPOSCOMPROBANTES.INGRESO:
                    self._addError("El CFDI referenciado %d en pago %d no es de ingreso" % (dbRow_pago.tcfdi_complementopago_doctosrelacionados.id, dbRow_cfdi.id))
                    _dbRow_cfdiRelacionado.receptorcliente_id = None
                    _dbRow_cfdiRelacionado.update_record()

                elif _dbRow_cfdiRelacionado.receptorcliente_id != self._n_cliente_id:
                    self._addError("El CFDI referenciado %d en pago %d no corresponden al mismo cliente" % (dbRow_pago.tcfdi_complementopago_doctosrelacionados.id, dbRow_cfdi.id))

                else:

                    if dbRow_pago.tcfdi_complementopagos.fechapago < _dbRow_cfdiRelacionado.fecha:
                        self._addWarning("CFDI %d referenciado (%s) en pago, tiene fecha posterior. Se ajusta fecha para manejo de saldo" % (dbRow_cfdi.id, dbRow_pago.tcfdi_complementopago_doctosrelacionados.iddocumento))
                        _dt_fecha = _dbRow_cfdiRelacionado.fecha + datetime.timedelta(seconds = 1)
                    else:
                        _dt_fecha = dbRow_pago.tcfdi_complementopagos.fechapago

                    dbRow_cfdi.totalpago += dbRow_pago.tcfdi_complementopago_doctosrelacionados.imppagado

                    self._addMovimiento(
                        dt_fecha = _dt_fecha,
                        E_accion = TCFDI_MOVIMIENTOS.E_ACCION.EMISION,
                        E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.PAGO,
                        s_descripcion = "Pago",
                        dbRow_cfdi = dbRow_cfdi,
                        n_cfdi_ingreso_id = _cfdirelacionado_id,
                        n_cfdi_complementopago_doctorelacionado_id = dbRow_pago.tcfdi_complementopago_doctosrelacionados.id,
                        f_importe = dbRow_pago.tcfdi_complementopago_doctosrelacionados.imppagado,
                        s_monedacontpaqi_id = dbRow_pago.tcfdi_complementopagos.monedacontpaqi_id,
                        f_tipocambio = dbRow_pago.tcfdi_complementopagos.tipocambio
                        )
            else:
                self._addWarning("El CFDI referenciado %d en pago %d esta fuera de rango en cálculo por lo que se ignora." % (_cfdirelacionado_id, dbRow_cfdi.id))
                pass

            return

        def _cfdi_referencia(
                self,
                dbRow_cfdi,
                dbRow_relacionado,
                s_descripcion,
                E_tipodocumento,
                n_numRelacionados
                ):
            """ Se registra en la etapa 1 los movimientos relacionados a los egresos.
            
            @param dbRow_cfdi: CFDI principal del egreso.
            @param dbRow_relacionado: Registro con información del documento relacionado.
            @param s_descripcion: _addMovimiento::s_descripcion
            @param E_tipodocumento: TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO
            @param n_numRelacionados: número de documentos encontrados en la relación al que corresponde el actual dbRow_relacionado.
            """

            if not dbRow_relacionado.cfdirelacionado_id:
                TCFDI_RELACIONADOS.ASOCIAR_UUID(dbRow_relacionado.id)

                _dbRow_docRelacionado_actualizado = dbc01.tcfdi_relacionados(dbRow_relacionado.id)
                if not _dbRow_docRelacionado_actualizado.cfdirelacionado_id:
                    self._addWarning("CFDI referenciado (%s) en egreso %d, es ignorado, ya que no se encuentra la referencia" % (dbRow_relacionado.uuid, dbRow_cfdi.id))
                    return
                else:
                    _cfdirelacionado_id = _dbRow_docRelacionado_actualizado.cfdirelacionado_id

            else:
                _cfdirelacionado_id = dbRow_relacionado.cfdirelacionado_id

            # _dbRow_cfdiRelacionado = dbc01.tcfdis(_cfdirelacionado_id)
            if _cfdirelacionado_id in self._D_registros:
                _dbRow_cfdiRelacionado = self._D_registros[_cfdirelacionado_id].dbRow

                if _dbRow_cfdiRelacionado.tipocomprobante_id != TTIPOSCOMPROBANTES.INGRESO:
                    self._addError("El CFDI referenciado %d en egreso %d no es de ingreso" % (dbRow_relacionado.id, dbRow_cfdi.id))
                    _dbRow_cfdiRelacionado.cfdirelacionado_id = None
                    _dbRow_cfdiRelacionado.update_record()

                elif _dbRow_cfdiRelacionado.receptorcliente_id != self._n_cliente_id:
                    self._addError("El CFDI referenciado %d en egreso %d no corresponden al mismo cliente" % (dbRow_relacionado.id, dbRow_cfdi.id))

                else:

                    if dbRow_cfdi.fecha < _dbRow_cfdiRelacionado.fecha:
                        self._addWarning("CFDI %d referenciado (%s) en egreso, tiene fecha posterior. Se ajusta fecha para manejo de saldo" % (dbRow_cfdi.id, dbRow_relacionado.uuid))
                        _dt_fecha = _dbRow_cfdiRelacionado.fecha + datetime.timedelta(seconds = 1)
                    else:
                        _dt_fecha = dbRow_cfdi.fecha

                    # Si solamente tiene un elemento referenciado, se puede aplicar el total del CFDI a solamente un ingreso;
                    # de lo contrario, no se sabe y se deja en cero para su posterior aplicación
                    if n_numRelacionados == 1:
                        _f_importe = dbRow_cfdi.total
                    elif dbRow_relacionado.fijarimporte:
                        _f_importe = dbRow_relacionado.importe
                    else:
                        _f_importe = 0

                    self._addMovimiento(
                        dt_fecha = _dt_fecha,
                        E_accion = TCFDI_MOVIMIENTOS.E_ACCION.EMISION,
                        E_tipodocumento = E_tipodocumento,
                        s_descripcion = s_descripcion,
                        dbRow_cfdi = dbRow_cfdi,
                        n_cfdi_relacionado_id = dbRow_relacionado.id,
                        dbRow_relacionado = dbRow_relacionado,
                        n_cfdi_ingreso_id = _cfdirelacionado_id,
                        f_importe = _f_importe,
                        s_monedacontpaqi_id = dbRow_cfdi.monedacontpaqi_id,
                        f_tipocambio = dbRow_cfdi.tipocambio
                        )
            else:
                self._addWarning("El CFDI referenciado %d en egreso %d esta fuera de rango en cálculo por lo que se ignora." % (_cfdirelacionado_id, dbRow_cfdi.id))
                pass

            return

        def _aplicacionsaldos(
                self,
                dbRow_movto
                ):
            """ Hace el cálculo de los saldos del cliente en base a la información de la tabla de movimientos.
            
            @param dbRow_movto: registro del movimiento
            """

            _dbRow_cfdi_ingreso = self._D_registros[dbRow_movto.cfdi_ingreso_id].dbRow

            if not (dbRow_movto.cfdi_ingreso_id in self._D_registros):
                if dbRow_movto.tipodocumento == TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.PAGO:
                    _s_tipoMovto = "Pago"
                else:
                    _s_tipoMovto = "Egreso"

                if _dbRow_cfdi_ingreso.date < self._dt_empezarde:
                    self._addWarning("CFDI %s %d referenciado %d (%s %s [%s]) no esta en orden para aplicación de saldo, o no se encontró por lo que se ignora ya que la fecha es antes de su aplicación en el sistema" % (_s_tipoMovto, dbRow_movto.cfdi_id, dbRow_movto.cfdi_ingreso_id, _dbRow_cfdi_ingreso.serie, _dbRow_cfdi_ingreso.folio, _dbRow_cfdi_ingreso.date))
                else:
                    self._addError("CFDI %s %d referenciado %d (%s %s [%s]) no esta en orden para aplicación de saldo, o no se encontró por lo que se ignora pero puede afectar saldo incorrectamente" % (_s_tipoMovto, dbRow_movto.cfdi_id, dbRow_movto.cfdi_ingreso_id, _dbRow_cfdi_ingreso.serie, _dbRow_cfdi_ingreso.folio, _dbRow_cfdi_ingreso.date))

            else:

                if dbRow_movto.tipodocumento == TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.INGRESO:
                    if _dbRow_cfdi_ingreso.metodopago_id == TMETODOSPAGO.UNAEXHIBICION:
                        dbRow_movto.abono = dbRow_movto.importe
                        dbRow_movto.cargo = dbRow_movto.importe

                    elif dbRow_movto.accion == TCFDI_MOVIMIENTOS.E_ACCION.CANCELACION:
                        dbRow_movto.abono = dbRow_movto.importe

                    else:
                        dbRow_movto.cargo = dbRow_movto.importe

                elif dbRow_movto.tipodocumento == TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.PAGO:
                    if dbRow_movto.accion == TCFDI_MOVIMIENTOS.E_ACCION.CANCELACION:
                        dbRow_movto.cargo = dbRow_movto.importe

                    else:
                        dbRow_movto.abono = dbRow_movto.importe

                elif dbRow_movto.tipodocumento in (
                        TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DEVOLUCION,
                        TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DESCUENTO,
                        TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.ANTICIPO,
                        ):
                    self._aplicacionsaldos_egreso_devdesant(dbRow_movto)
                elif dbRow_movto.tipodocumento == TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DEBITO:
                    self._addError("CFDI movimiento %d de tipo Debito no implementado aún" % dbRow_movto.id)
                elif dbRow_movto.tipodocumento == TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.TRASPASO_SALDO_A_FAVOR:
                    dbRow_movto.cargo = dbRow_movto.importe
                elif dbRow_movto.tipodocumento == TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.APLICACION_SALDO_A_FAVOR:
                    dbRow_movto.abono = dbRow_movto.importe
                else:
                    self._addError("CFDI movimiento %d no reconocido" % dbRow_movto.id)

                if dbRow_movto.accion in (TCFDI_MOVIMIENTOS.E_ACCION.EMISION, TCFDI_MOVIMIENTOS.E_ACCION.CANCELACION):
                    self._D_registros[dbRow_movto.cfdi_ingreso_id].saldo += dbRow_movto.cargo - dbRow_movto.abono

                    dbRow_movto.saldo = self._D_registros[dbRow_movto.cfdi_ingreso_id].saldo
                    dbRow_movto.update_record()

                    # El importe define si el total a aplicar se definio en la fase 1
                    if dbRow_movto.importe:
                        self._D_registros[dbRow_movto.cfdi_ingreso_id].aplicadoscargos += dbRow_movto.cargo
                        self._D_registros[dbRow_movto.cfdi_ingreso_id].aplicadosabonos += dbRow_movto.abono
                    else:
                        self._D_registros[dbRow_movto.cfdi_ingreso_id].variablescargos += dbRow_movto.cargo
                        self._D_registros[dbRow_movto.cfdi_ingreso_id].variablesabonos += dbRow_movto.abono

                else:
                    dbRow_movto.update_record()

            return

        def _aplicacionsaldos_egreso_devdesant(
                self,
                dbRow_movto
                ):
            """ Se aplica el saldo para los egresos.
            
            @param dbRow_movto: dbRow del movimiento.
            """

            _dbRow_relacionado = self._D_registros[dbRow_movto.cfdi_ingreso_id].referenciasRelacionados[dbRow_movto.cfdi_relacionado_id]

            if dbRow_movto.accion == TCFDI_MOVIMIENTOS.E_ACCION.CANCELACION:
                _dbRow_movtoInicial = dbc01.tcfdi_movimientos(dbRow_movto.cfdi_movimiento_emision_id)
                dbRow_movto.cargo = _dbRow_movtoInicial.abono
                dbRow_movto.abono = 0
                # Este es el saldo del CFDI de egreso
                self._D_registros[dbRow_movto.cfdi_id].saldo += dbRow_movto.cargo

            else:
                if not (dbRow_movto.cfdi_id in self._D_registros):

                    _dbRow_cfdi = self._D_registros[dbRow_movto.cfdi_ingreso_id].referencias[dbRow_movto.cfdi_id]
                    self._D_registros[dbRow_movto.cfdi_id] = Storage()
                    self._D_registros[dbRow_movto.cfdi_id].dbRow = _dbRow_cfdi
                    self._D_registros[dbRow_movto.cfdi_id].saldo = _dbRow_cfdi.total

                else:
                    pass

                # Si el importe pudo ser determinado en la primer etapa, debido a que la referencia aplica a un solo ingreso, se aplica
                if dbRow_movto.importe > 0:
                    dbRow_movto.abono = dbRow_movto.importe
                    self._D_registros[dbRow_movto.cfdi_id].saldo -= dbRow_movto.abono
                elif self._D_registros[dbRow_movto.cfdi_id].saldo <= 0:
                    # Si el saldo en el CFDI de egreso es cero o menor que cero, se registra el warning

                    _dbRow_cfdi = self._D_registros[dbRow_movto.cfdi_ingreso_id].referencias[dbRow_movto.cfdi_id]

                    if not(_dbRow_cfdi.sat_status == TCFDIS.SAT_STATUS.CANCELADO):
                        self._addWarning("CFDI %d de Egreso tiene más referencias que saldo disponible" % dbRow_movto.cfdi_id)
                    else:
                        pass

                elif (self._E_proceso == self.E_CFDI) or _dbRow_relacionado.fijarimporte:

                    # Si el proceso en ejecución es el cálculo del saldo del CFDI, se usa el importe en el de egreso como el importe aplicado
                    dbRow_movto.abono = _dbRow_relacionado.importe

                    # Este es el saldo del CFDI de egreso
                    self._D_registros[dbRow_movto.cfdi_id].saldo -= dbRow_movto.abono

                else:

                    # Se determina el saldo efectivo de la factura:
                    _f_saldoIngresoCalculado = (
                        self._D_registros[dbRow_movto.cfdi_ingreso_id].cargos
                        - self._D_registros[dbRow_movto.cfdi_ingreso_id].abonos
                        + self._D_registros[dbRow_movto.cfdi_ingreso_id].variablescargos
                        - self._D_registros[dbRow_movto.cfdi_ingreso_id].variablesabonos
                        )

                    if _f_saldoIngresoCalculado == 0:
                        # Si el saldo ingreso calculado es igual a 0, no mas abonos deben hacerse
                        pass

                    elif self._D_registros[dbRow_movto.cfdi_id].saldo > _f_saldoIngresoCalculado:
                        dbRow_movto.abono = _f_saldoIngresoCalculado

                    else:
                        dbRow_movto.abono = self._D_registros[dbRow_movto.cfdi_id].saldo
                    # Este es el saldo del CFDI de egreso
                    self._D_registros[dbRow_movto.cfdi_id].saldo -= dbRow_movto.abono

            if dbRow_movto.abono != _dbRow_relacionado.importe:
                _dbRow_relacionado.importe = dbRow_movto.abono
                _dbRow_relacionado.update_record()
            else:
                pass

            return

        def _procesar_cfdi_ingreso(
                self,
                ):
            """ Identifica y actualiza/crea los movimientos

            Esta rutina supone que la información del cliente esta actualizada y sobre esa base contabiliza,
            modificando únicamente los registros impactado por el CFDI

            """

            _D_return = Storage(
                E_return = stvfwk2_e_RETURN.OK,
                s_msgError = "",
                L_movtos = [],
                b_movtoActualizo = False
                )

            _D_movtosIngresos = Storage()

            # Se usa un diccionario para un manejo similar a como se usan los movimientos en los pagos y egresos
            _D_movtosIngresos[self._n_cfdi_id] = Storage(
                dbRowMovtoEmision = None,
                n_cfdi_movimiento_emision_id = None,
                dbRowMovtoCancelacion = None,
                dbRow_cfdi_ingreso = self._dbRow_cfdi,
                tieneMovtoEmision = False,  # Indica si el pago tiene movto de emisión relacionado
                tieneMovtoCancelacion = False,  # Indica si el pago tiene movto de cancelacion relacionado
                )

            self._s_mensajeDebug = "Mapean movimientos relacionados al cfdi de ingreso"
            # Se mapean los pagos con los movimientos, identificando si es emision o cancelacion
            _dbRows_movtos = dbc01(
                (dbc01.tcfdi_movimientos.cfdi_id == self._n_cfdi_id)
                & (
                    dbc01.tcfdi_movimientos.tipodocumento.belongs(
                        [TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.INGRESO]
                        )
                    )
                ).select(
                    dbc01.tcfdi_movimientos.ALL,
                    orderby = dbc01.tcfdi_movimientos.id
                    )

            self._s_mensajeDebug = "Se buscan los movimientos y se engloban en un diccionario"
            # Se buscan los movimientos y se engloban en un diccionario
            for _dbRow_movto in _dbRows_movtos:
                if _dbRow_movto.cfdi_id in _D_movtosIngresos:
                    _D_movtoIngreso = _D_movtosIngresos[_dbRow_movto.cfdi_id]
                    if (
                            (_dbRow_movto.accion == TCFDI_MOVIMIENTOS.E_ACCION.EMISION)
                            & (_D_movtoIngreso.tieneMovtoEmision == False)
                            ):
                        _D_movtoIngreso.tieneMovtoEmision = True
                        _D_movtoIngreso.dbRowMovtoEmision = _dbRow_movto
                        _D_movtoIngreso.n_cfdi_movimiento_emision_id = _dbRow_movto.id
                    elif (
                            (_dbRow_movto.accion == TCFDI_MOVIMIENTOS.E_ACCION.CANCELACION)
                            & (_D_movtoIngreso.tieneMovtoCancelacion == False)
                            ):

                        # Si el CFDI no esta cancelado, se cubre abajo el ajuste de saldo y movimientos
                        _D_movtoIngreso.tieneMovtoCancelacion = True
                        _D_movtoIngreso.dbRowMovtoCancelacion = _dbRow_movto

                    else:
                        _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                        _D_return.s_msgError = "Error al verificar movimientos, existen varias emisiones/cancelaciones " \
                                               "para el mismo CFDI. Favor de correr ajuste de saldos para el cliente."
                        break
                else:
                    # No debería poder entrar a este else, ya que la consulta es directa y el diccionario apuntan al mismo cfdi
                    _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                    _D_return.s_msgError = "CFDI no reconocido. Favor de correr ajuste de saldos del cliente."
                    break

            self._s_mensajeDebug = "Se mueve por el diccionario de movimiento para determinar las acciones de ajuste"
            # Se mueve por el diccionario de movimiento para determinar las acciones de ajuste de saldos o creacion de movimientos
            for _D_movtoIngreso in _D_movtosIngresos.values():
                _f_importe = _D_movtoIngreso.dbRow_cfdi_ingreso.total

                if not _D_movtoIngreso.tieneMovtoEmision:
                    # Se inserta el movimiento de pago

                    self._s_mensajeDebug = "Si no tiene movimiento de emision"

                    self._s_mensajeDebug = "Se intenta agregar el movimiento"
                    _D_result = self._addMovimiento(
                        dt_fecha = _D_movtoIngreso.dbRow_cfdi_ingreso.fecha,
                        E_accion = TCFDI_MOVIMIENTOS.E_ACCION.EMISION,
                        E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.INGRESO,
                        s_descripcion = "Factura",
                        dbRow_cfdi = _D_movtoIngreso.dbRow_cfdi_ingreso,
                        n_cfdi_ingreso_id = _D_movtoIngreso.dbRow_cfdi_ingreso.id,
                        f_importe = _f_importe,
                        f_cargo = _f_importe,
                        f_abono = 0,
                        f_saldoPrevio = _f_importe,
                        s_monedacontpaqi_id = _D_movtoIngreso.dbRow_cfdi_ingreso.monedacontpaqi_id,
                        f_tipocambio = _D_movtoIngreso.dbRow_cfdi_ingreso.tipocambio
                        )
                    _D_movtoIngreso.dbRow_cfdi_ingreso.saldo = _D_result.f_saldoNuevo
                    _D_movtoIngreso.dbRow_cfdi_ingreso.update_record()

                    _D_movtoIngreso.n_cfdi_movimiento_emision_id = _D_result.n_cfdi_movimiento_id
                    _D_return.b_movtoActualizo = True

                elif _D_movtoIngreso.dbRowMovtoEmision.importe != _f_importe:

                    # Se actualiza el saldo del CFDI
                    # Nuevo saldo = Viejo Saldo + (Viejo Pago - Nuevo Pago)
                    _D_movtoIngreso.dbRow_cfdi_ingreso.saldo -= _D_movtoIngreso.dbRowMovtoEmision.importe - _f_importe
                    _D_movtoIngreso.dbRow_cfdi_ingreso.update_record()

                    # Se actualiza el pago en caso de existir sólo un movimiento
                    _D_movtoIngreso.dbRowMovtoEmision.saldo -= _D_movtoIngreso.dbRowMovtoEmision.importe - _f_importe
                    _D_movtoIngreso.dbRowMovtoEmision.cargo = _f_importe
                    _D_movtoIngreso.dbRowMovtoEmision.importe = _f_importe
                    _D_movtoIngreso.dbRowMovtoEmision.update_record()

                    _D_return.b_movtoActualizo = True

                else:
                    pass

                if self._dbRow_cfdi.sat_status == TCFDIS.SAT_STATUS.CANCELADO:

                    if not _D_movtoIngreso.tieneMovtoCancelacion:
                        # Se inserta el movimiento de cancelacion del pago

                        _D_results = self._addMovimiento(
                            dt_fecha = self._dbRow_cfdi.sat_fecha_cancelacion,
                            E_accion = TCFDI_MOVIMIENTOS.E_ACCION.CANCELACION,
                            E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.INGRESO,
                            s_descripcion = "Cancelación Factura",
                            dbRow_cfdi = self._dbRow_cfdi,
                            n_cfdi_ingreso_id = _D_movtoIngreso.dbRow_cfdi_ingreso.id,
                            n_cfdi_movimiento_emision_id = _D_movtoIngreso.n_cfdi_movimiento_emision_id,
                            f_importe = _f_importe,
                            f_cargo = 0,
                            f_abono = _f_importe,
                            f_saldoPrevio = _D_movtoIngreso.dbRow_cfdi_ingreso.saldo,
                            s_monedacontpaqi_id = _D_movtoIngreso.dbRow_cfdi_ingreso.monedacontpaqi_id,
                            f_tipocambio = _D_movtoIngreso.dbRow_cfdi_ingreso.tipocambio
                            )
                        _D_movtoIngreso.dbRow_cfdi_ingreso.saldo -= _D_results.f_saldoNuevo
                        _D_movtoIngreso.dbRow_cfdi_ingreso.update_record()

                        _D_return.b_movtoActualizo = True

                    elif _D_movtoIngreso.dbRowMovtoCancelacion.importe != _f_importe:

                        # Se actualiza el saldo del CFDI
                        # Nuevo saldo = Viejo Saldo + (Viejo Pago - Nuevo Pago)
                        _D_movtoIngreso.dbRow_cfdi_ingreso.saldo += _D_movtoIngreso.dbRowMovtoCancelacion.importe - _f_importe
                        _D_movtoIngreso.dbRow_cfdi_ingreso.update_record()

                        # Se actualiza el pago en caso de existir sólo un movimiento
                        _D_movtoIngreso.dbRowMovtoCancelacion.saldo += _D_movtoIngreso.dbRowMovtoCancelacion.importe - _f_importe
                        _D_movtoIngreso.dbRowMovtoCancelacion.abono = _f_importe
                        _D_movtoIngreso.dbRowMovtoCancelacion.importe = _f_importe
                        _D_movtoIngreso.dbRowMovtoCancelacion.update_record()

                        _D_return.b_movtoActualizo = True

                    else:
                        pass

                elif _D_movtoIngreso.tieneMovtoCancelacion:
                    # Si no esta cancelado el cfdi, y tiene movimiento de cancelacion

                    # Se actualiza el saldo del CFDI
                    # Nuevo saldo = Viejo Saldo + (Viejo Pago - Nuevo Pago)
                    _D_movtoIngreso.dbRow_cfdi_ingreso.saldo += _D_movtoIngreso.dbRowMovtoCancelacion.importe
                    _D_movtoIngreso.dbRow_cfdi_ingreso.update_record()

                    _D_movtoIngreso.dbRowMovtoCancelacion.delete_record()

                    _D_return.b_movtoActualizo = True

                else:
                    # No esta cancelado el CFDI del pago, con la ejecucción de los vigentes es suficiente
                    pass

            return _D_return

        def _procesar_cfdi_pago(
                self,
                ):
            """ Identifica y actualiza/crea los movimientos

            Esta rutina supone que la información del cliente esta actualizada y sobre esa base contabiliza,
            modificando únicamente los registros impactado por el CFDI

            """

            _D_return = Storage(
                E_return = stvfwk2_e_RETURN.OK,
                s_msgError = "",
                L_movtos = [],
                b_movtoActualizo = False
                )

            _D_movtosPagos = Storage()

            # Se obtienen los complementos de pago y sus docs relacionados
            _dbRows_compPagos = dbc01(
                (dbc01.tcfdi_complementopagos.cfdi_id == self._n_cfdi_id)
                & (dbc01.tcfdi_complementopago_doctosrelacionados.cfdi_complementopago_id
                   == dbc01.tcfdi_complementopagos.id)
                ).select(
                    dbc01.tcfdi_complementopagos.ALL,
                    dbc01.tcfdi_complementopago_doctosrelacionados.ALL
                    )

            # Se crea un diccionario con los complementos de pago
            for _dbRow_compPago in _dbRows_compPagos:
                if _dbRow_compPago.tcfdi_complementopago_doctosrelacionados.cfdirelacionado_id:
                    _D_movtosPagos[_dbRow_compPago.tcfdi_complementopago_doctosrelacionados.id] = Storage(
                        dbRowMovtoEmision = None,
                        n_cfdi_movimiento_emision_id = None,
                        dbRowMovtoCancelacion = None,
                        dbRow_cfdi_ingreso = None,
                        dbRow_cfdi_complementopago_doctorelacionado =
                        _dbRow_compPago.tcfdi_complementopago_doctosrelacionados,
                        dbRow_cfdi_complementopago = _dbRow_compPago.tcfdi_complementopagos,
                        tieneMovtoEmision = False,  # Indica si el pago tiene movto de emisión relacionado
                        tieneMovtoCancelacion = False,  # Indica si el pago tiene movto de cancelacion relacionado
                        )
                else:
                    # No se puede generar estado de cuenta del pago, ya que no se tiene referencia al cfdi ingreso
                    pass

            # Se mapean los pagos con los movimientos, identificando si es emision o cancelacion
            _dbRows_movtos = dbc01(
                dbc01.tcfdi_movimientos.cfdi_id == self._n_cfdi_id
                ).select(
                    dbc01.tcfdi_movimientos.ALL,
                    orderby = dbc01.tcfdi_movimientos.id
                    )

            for _dbRow_movto in _dbRows_movtos:
                if _dbRow_movto.cfdi_complementopago_doctorelacionado_id in _D_movtosPagos:
                    _D_movtoPago = _D_movtosPagos[_dbRow_movto.cfdi_complementopago_doctorelacionado_id]
                    if (
                            (_dbRow_movto.accion == TCFDI_MOVIMIENTOS.E_ACCION.EMISION)
                            & (_D_movtoPago.tieneMovtoEmision == False)
                            ):
                        _D_movtoPago.tieneMovtoEmision = True
                        _D_movtoPago.dbRowMovtoEmision = _dbRow_movto
                        _D_movtoPago.n_cfdi_movimiento_emision_id = _dbRow_movto.id
                    elif (
                            (_dbRow_movto.accion == TCFDI_MOVIMIENTOS.E_ACCION.CANCELACION)
                            & (_D_movtoPago.tieneMovtoCancelacion == False)
                            ):

                        # Si el CFDI no esta cancelado, se cubre abajo el ajuste de saldo y movimientos
                        _D_movtoPago.tieneMovtoCancelacion = True
                        _D_movtoPago.dbRowMovtoCancelacion = _dbRow_movto

                    else:
                        _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                        _D_return.s_msgError = "Error al verificar movimientos, existen varias " \
                                               "emisiones/cancelaciones " \
                                               "para el mismo CFDI. Favor de correr ajuste de saldos para el cliente."
                        break

                elif not _dbRow_movto.cfdi_ingreso_id:
                    # Si no existe CFDI de ingreso relacionado, no hay manera de resolver el saldo del movimiento
                    _dbRow_movto.delete_record()

                else:
                    # El movimiento no existe más y debe ser eliminado y el saldo del documento referenciado ajustado
                    _dbRow_cfdiIngreso = dbc01.tcfdis(_dbRow_movto.cfdi_ingreso_id)

                    self._s_mensajeDebug = "Actualizando saldo de ingreso"

                    # Nuevo saldo = Viejo Saldo + (Viejo Pago - Nuevo Pago)
                    if _dbRow_movto.accion == TCFDI_MOVIMIENTOS.E_ACCION.EMISION:
                        # La emisión resto el saldo, como ya no existe, sube el saldo
                        _dbRow_cfdiIngreso.saldo += _dbRow_movto.importe

                    elif _dbRow_movto.accion == TCFDI_MOVIMIENTOS.E_ACCION.CANCELACION:
                        # La emisión resto el saldo, la cancelación subió el saldo, como y ano existe la cancelación,
                        #  se reduce el saldo
                        _dbRow_cfdiIngreso.saldo -= _dbRow_movto.importe

                    else:
                        _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                        _D_return.s_msgError = "Accion en movimiento no identificada. " \
                                               "Favor de correr ajuste de saldos del cliente."
                        break

                    _dbRow_cfdiIngreso.update_record()
                    _dbRow_movto.delete_record()

            # Se mueve por el diccionario de movimiento para determinar las acciones de ajuste de saldos
            #  o creacion de movimientos
            for _D_movtoPago in _D_movtosPagos.values():
                _f_importePagado = _D_movtoPago.dbRow_cfdi_complementopago_doctorelacionado.imppagado

                if not _D_movtoPago.tieneMovtoEmision:
                    # Se inserta el movimiento de pago

                    self._s_mensajeDebug = "Creando movto pago emisión"

                    _D_movtoPago.dbRow_cfdi_ingreso = dbc01.tcfdis(
                        _D_movtoPago.dbRow_cfdi_complementopago_doctorelacionado.cfdirelacionado_id
                        )
                    _D_movtoPago.dbRow_cfdi_ingreso.saldo = _D_movtoPago.dbRow_cfdi_ingreso.saldo or 0

                    _D_result = self._addMovimiento(
                        dt_fecha = _D_movtoPago.dbRow_cfdi_complementopago.fechapago,
                        E_accion = TCFDI_MOVIMIENTOS.E_ACCION.EMISION,
                        E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.PAGO,
                        s_descripcion = "Pago",
                        dbRow_cfdi = self._dbRow_cfdi,
                        n_cfdi_ingreso_id = _D_movtoPago.dbRow_cfdi_complementopago_doctorelacionado.cfdirelacionado_id,
                        n_cfdi_complementopago_doctorelacionado_id =
                        _D_movtoPago.dbRow_cfdi_complementopago_doctorelacionado.id,
                        dbRow_relacionado = _D_movtoPago.dbRow_cfdi_ingreso,
                        f_importe = _f_importePagado,
                        f_cargo = 0,
                        f_abono = _f_importePagado,
                        f_saldoPrevio = _D_movtoPago.dbRow_cfdi_ingreso.saldo,
                        s_monedacontpaqi_id = _D_movtoPago.dbRow_cfdi_complementopago.monedacontpaqi_id,
                        f_tipocambio = _D_movtoPago.dbRow_cfdi_complementopago.tipocambio
                        )
                    _D_movtoPago.dbRow_cfdi_ingreso.saldo -= _D_result.f_saldoNuevo
                    _D_movtoPago.dbRow_cfdi_ingreso.update_record()

                    _D_movtoPago.n_cfdi_movimiento_emision_id = _D_result.n_cfdi_movimiento_id
                    _D_return.b_movtoActualizo = True

                elif _D_movtoPago.dbRowMovtoEmision.importe != _f_importePagado:

                    self._s_mensajeDebug = "Actualizando movto pago emisión"

                    # Se actualiza el saldo del CFDI
                    _D_movtoPago.dbRow_cfdi_ingreso = dbc01.tcfdis(
                        _D_movtoPago.dbRow_cfdi_complementopago_doctorelacionado.cfdirelacionado_id
                        )
                    _D_movtoPago.dbRow_cfdi_ingreso.saldo = _D_movtoPago.dbRow_cfdi_ingreso.saldo or 0
                    # Nuevo saldo = Viejo Saldo + (Viejo Pago - Nuevo Pago)
                    _D_movtoPago.dbRow_cfdi_ingreso.saldo += _D_movtoPago.dbRowMovtoEmision.importe - _f_importePagado
                    _D_movtoPago.dbRow_cfdi_ingreso.update_record()

                    # Se actualiza el pago en caso de existir sólo un movimiento
                    _D_movtoPago.dbRowMovtoEmision.saldo += _D_movtoPago.dbRowMovtoEmision.importe - _f_importePagado
                    _D_movtoPago.dbRowMovtoEmision.abono = _f_importePagado
                    _D_movtoPago.dbRowMovtoEmision.importe = _f_importePagado
                    _D_movtoPago.dbRowMovtoEmision.update_record()

                    _D_return.b_movtoActualizo = True

                else:
                    pass

                if self._dbRow_cfdi.sat_status == TCFDIS.SAT_STATUS.CANCELADO:

                    if not _D_movtoPago.tieneMovtoCancelacion:
                        # Se inserta el movimiento de cancelacion del pago

                        self._s_mensajeDebug = "Creando movto pago cancelación"

                        if not _D_movtoPago.dbRow_cfdi_ingreso:
                            _D_movtoPago.dbRow_cfdi_ingreso = dbc01.tcfdis(
                                _D_movtoPago.dbRow_cfdi_complementopago_doctorelacionado.cfdirelacionado_id
                                )
                            _D_movtoPago.dbRow_cfdi_ingreso.saldo = _D_movtoPago.dbRow_cfdi_ingreso.saldo or 0
                        else:
                            pass

                        _D_results = self._addMovimiento(
                            dt_fecha = self._dbRow_cfdi.sat_fecha_cancelacion,
                            E_accion = TCFDI_MOVIMIENTOS.E_ACCION.CANCELACION,
                            E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.PAGO,
                            s_descripcion = "Cancelación Pago",
                            dbRow_cfdi = self._dbRow_cfdi,
                            n_cfdi_ingreso_id =
                            _D_movtoPago.dbRow_cfdi_complementopago_doctorelacionado.cfdirelacionado_id,
                            n_cfdi_complementopago_doctorelacionado_id =
                            _D_movtoPago.dbRow_cfdi_complementopago_doctorelacionado.id,
                            dbRow_relacionado = _D_movtoPago.dbRow_cfdi_ingreso,
                            n_cfdi_movimiento_emision_id = _D_movtoPago.n_cfdi_movimiento_emision_id,
                            f_importe = _f_importePagado,
                            f_cargo = _f_importePagado,
                            f_abono = 0,
                            f_saldoPrevio = _D_movtoPago.dbRow_cfdi_ingreso.saldo,
                            s_monedacontpaqi_id = _D_movtoPago.dbRow_cfdi_complementopago.monedacontpaqi_id,
                            f_tipocambio = _D_movtoPago.dbRow_cfdi_complementopago.tipocambio
                            )

                        _D_movtoPago.dbRow_cfdi_ingreso.saldo += _D_results.f_saldoNuevo
                        _D_movtoPago.dbRow_cfdi_ingreso.update_record()

                        _D_return.b_movtoActualizo = True

                    elif _D_movtoPago.dbRowMovtoCancelacion.importe != _f_importePagado:

                        self._s_mensajeDebug = "Actualizando movto pago cancelación"

                        # Se actualiza el saldo del CFDI
                        if not _D_movtoPago.dbRow_cfdi_ingreso:
                            _D_movtoPago.dbRow_cfdi_ingreso = dbc01.tcfdis(
                                _D_movtoPago.dbRow_cfdi_complementopago_doctorelacionado.cfdirelacionado_id
                                )
                            _D_movtoPago.dbRow_cfdi_ingreso.saldo = _D_movtoPago.dbRow_cfdi_ingreso.saldo or 0
                        else:
                            pass
                        # Nuevo saldo = Viejo Saldo + (Viejo Pago - Nuevo Pago)
                        _D_movtoPago.dbRow_cfdi_ingreso.saldo -= _D_movtoPago.dbRowMovtoCancelacion.importe \
                                                                 - _f_importePagado
                        _D_movtoPago.dbRow_cfdi_ingreso.update_record()

                        # Se actualiza el pago en caso de existir sólo un movimiento
                        _D_movtoPago.dbRowMovtoCancelacion.saldo -= _D_movtoPago.dbRowMovtoCancelacion.importe \
                                                                    - _f_importePagado
                        _D_movtoPago.dbRowMovtoCancelacion.cargo = _f_importePagado
                        _D_movtoPago.dbRowMovtoCancelacion.importe = _f_importePagado
                        _D_movtoPago.dbRowMovtoCancelacion.update_record()

                        _D_return.b_movtoActualizo = True

                    else:
                        pass

                elif _D_movtoPago.tieneMovtoCancelacion:
                    # Si no esta cancelado el cfdi, y tiene movimiento de cancelación

                    self._s_mensajeDebug = "CFDI no esta cancelado, pero tiene movto cancelación"

                    # Se actualiza el saldo del CFDI
                    if not _D_movtoPago.dbRow_cfdi_ingreso:
                        _D_movtoPago.dbRow_cfdi_ingreso = dbc01.tcfdis(
                            _D_movtoPago.dbRow_cfdi_complementopago_doctorelacionado.cfdirelacionado_id
                            )
                        _D_movtoPago.dbRow_cfdi_ingreso.saldo = _D_movtoPago.dbRow_cfdi_ingreso.saldo or 0
                    else:
                        pass
                    # Nuevo saldo = Viejo Saldo + (Viejo Pago - Nuevo Pago)
                    _D_movtoPago.dbRow_cfdi_ingreso.saldo -= _D_movtoPago.dbRowMovtoCancelacion.importe
                    _D_movtoPago.dbRow_cfdi_ingreso.update_record()

                    _D_movtoPago.dbRowMovtoCancelacion.delete_record()
                    _D_return.b_movtoActualizo = True

                else:
                    # No esta cancelado el CFDI del pago, con la ejecucción de los vigentes es suficiente
                    pass

            return _D_return

        def _procesar_cfdi_egreso(
                self,
                ):
            """ Identifica y actualiza/crea los movimientos

            Esta rutina supone que la información del cliente esta actualizada y sobre esa base contabiliza,
            modificando únicamente los registros impactado por el CFDI

            """

            _D_return = FUNC_RETURN(
                L_movtos = [],
                b_movtoActualizo = False
                )

            _D_movtosEgresos = Storage()

            self._s_mensajeDebug = "Query de doc rels egreso %s" % str(self._n_cfdi_id)

            # Se obtienen los docs relacionados
            _dbRows_egresos = dbc01(
                (dbc01.tcfdi_relacionados.cfdi_id == self._n_cfdi_id)
                ).select(
                    dbc01.tcfdi_relacionados.ALL,
                    )

            # Se crea un diccionario con los complementos de pago
            for _dbRow_egreso in _dbRows_egresos:
                if _dbRow_egreso.cfdirelacionado_id:
                    self._s_mensajeDebug = "Dict de docs relacionados"
                    _D_movtosEgresos[_dbRow_egreso.id] = Storage(
                        dbRowMovtoEmision = None,
                        n_cfdi_movimiento_emision_id = None,
                        dbRowMovtoCancelacion = None,
                        dbRow_cfdi_ingreso = None,
                        dbRow_cfdi_relacionado = _dbRow_egreso,
                        tieneMovtoEmision = False,  # Indica si el pago tiene movto de emisión relacionado
                        tieneMovtoCancelacion = False,  # Indica si el pago tiene movto de cancelacion relacionado
                        )
                else:
                    # No se puede crear estado de cuenta de un movimiento que no tiene relacionado identificado
                    pass

            self._s_mensajeDebug = "Query de movtos egreso"

            # Se mapean los pagos con los movimientos, identificando si es emision o cancelacion
            _dbRows_movtos = dbc01(
                dbc01.tcfdi_movimientos.cfdi_id == self._n_cfdi_id
                ).select(
                    dbc01.tcfdi_movimientos.ALL,
                    orderby = dbc01.tcfdi_movimientos.id
                    )

            for _dbRow_movto in _dbRows_movtos:
                self._s_mensajeDebug = "Revisando movtos de egreso"
                if _dbRow_movto.cfdi_relacionado_id in _D_movtosEgresos:
                    _D_movtoEgreso = _D_movtosEgresos[_dbRow_movto.cfdi_relacionado_id]
                    if (
                            (_dbRow_movto.accion == TCFDI_MOVIMIENTOS.E_ACCION.EMISION)
                            & (_D_movtoEgreso.tieneMovtoEmision == False)
                            ):
                        _D_movtoEgreso.tieneMovtoEmision = True
                        _D_movtoEgreso.dbRowMovtoEmision = _dbRow_movto
                        _D_movtoEgreso.n_cfdi_movimiento_emision_id = _dbRow_movto.id
                    elif (
                            (_dbRow_movto.accion == TCFDI_MOVIMIENTOS.E_ACCION.CANCELACION)
                            & (_D_movtoEgreso.tieneMovtoCancelacion == False)
                            ):

                        # Si el CFDI no esta cancelado, se cubre abajo el ajuste de saldo y movimientos
                        _D_movtoEgreso.tieneMovtoCancelacion = True
                        _D_movtoEgreso.dbRowMovtoCancelacion = _dbRow_movto

                    else:
                        _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                        _D_return.s_msgError = "Error al verificar movimientos, existen varias emisiones/cancelaciones " \
                                               "para el mismo CFDI. Favor de correr ajuste de saldos para el cliente."
                        break

                elif not _dbRow_movto.cfdi_ingreso_id:
                    # Si no tiene relacionado el movimiento, solo se elimina ya que no se puede actualizar el
                    #  saldo del ingreso
                    self._s_mensajeDebug = "No se encontro movto ingreso, se elimina movto edo cta"
                    _dbRow_movto.delete_record()

                else:
                    # El movimiento no existe más y debe ser eliminado y el saldo del documento referenciado ajustado
                    _dbRow_cfdiIngreso = dbc01.tcfdis(_dbRow_movto.cfdi_ingreso_id)

                    self._s_mensajeDebug = "Actualizando saldo de ingreso"

                    # Nuevo saldo = Viejo Saldo + (Viejo Pago - Nuevo Pago)
                    if _dbRow_movto.accion == TCFDI_MOVIMIENTOS.E_ACCION.EMISION:
                        # La emisión resto el saldo, como ya no existe, sube el saldo
                        _dbRow_cfdiIngreso.saldo += _dbRow_movto.importe

                    elif _dbRow_movto.accion == TCFDI_MOVIMIENTOS.E_ACCION.CANCELACION:
                        # La emisión resto el saldo, la cancelación subió el saldo, como y ano existe la cancelación, se reduce el saldo
                        _dbRow_cfdiIngreso.saldo -= _dbRow_movto.importe

                    else:
                        _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                        _D_return.s_msgError = "Accion en movimiento no identificada. Favor de correr ajuste de saldos del cliente."
                        break

                    _dbRow_cfdiIngreso.update_record()
                    _dbRow_movto.delete_record()

            self._s_mensajeDebug = "Limpiando movtos egreso"

            _f_saldoEgreso = self._dbRow_cfdi.total
            # Se mueve por el diccionario de movimiento para determinar las acciones de ajuste de saldos o creacion de movimientos
            for _D_movtoEgreso in _D_movtosEgresos.values():

                self._s_mensajeDebug = "Analizando movimientos de egreso"

                if _D_movtoEgreso.dbRow_cfdi_relacionado.fijarimporte:
                    # Si el movimiento tiene definido fijarimporte, usa la cantidad en el importe
                    _f_importeEgreso = _D_movtoEgreso.dbRow_cfdi_relacionado.importe

                elif _D_movtoEgreso.dbRow_cfdi_relacionado.importe > 0:
                    # Si el movimiento ya tiene un importe definido lo usa
                    _f_importeEgreso = _D_movtoEgreso.dbRow_cfdi_relacionado.importe

                else:
                    # Si no hay manera de saber el importe, se deja nulo para su manejo en caso de tener movimientos
                    _f_importeEgreso = 0

                _f_importeEgresoCancelacion = 0  # Se define el importe de cancelación en caso de existir

                if not _D_movtoEgreso.tieneMovtoEmision:
                    # Se inserta el movimiento de pago

                    self._s_mensajeDebug = "Creando movto egreso emisión en CFDI Ingreso %s" \
                                           % str(_D_movtoEgreso.dbRow_cfdi_relacionado.cfdirelacionado_id)

                    _D_movtoEgreso.dbRow_cfdi_ingreso = dbc01.tcfdis(
                        _D_movtoEgreso.dbRow_cfdi_relacionado.cfdirelacionado_id
                        )

                    _D_movtoEgreso.dbRow_cfdi_ingreso.saldo = _D_movtoEgreso.dbRow_cfdi_ingreso.saldo or 0

                    if not _f_importeEgreso:
                        # Si no hay manera de saber el importe, usa el saldo del cfdi relacionado topado por el saldo del egreso
                        if _f_saldoEgreso >= _D_movtoEgreso.dbRow_cfdi_ingreso.saldo:
                            _f_importeEgreso = _D_movtoEgreso.dbRow_cfdi_ingreso.saldo
                        else:
                            _f_importeEgreso = _f_saldoEgreso

                        _D_movtoEgreso.dbRow_cfdi_relacionado.importe = _f_importeEgreso
                        _D_movtoEgreso.dbRow_cfdi_relacionado.update_record()
                    else:
                        pass

                    _D_result = self._addMovimiento(
                        dt_fecha = self._dbRow_cfdi.fecha,
                        E_accion = TCFDI_MOVIMIENTOS.E_ACCION.EMISION,
                        E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DESCUENTO,
                        s_descripcion = "Descuento",
                        dbRow_cfdi = self._dbRow_cfdi,
                        n_cfdi_ingreso_id = _D_movtoEgreso.dbRow_cfdi_relacionado.cfdirelacionado_id,
                        n_cfdi_relacionado_id = _D_movtoEgreso.dbRow_cfdi_relacionado.id,
                        dbRow_relacionado = _D_movtoEgreso.dbRow_cfdi_ingreso,
                        f_importe = _f_importeEgreso,
                        f_cargo = 0,
                        f_abono = _f_importeEgreso,
                        f_saldoPrevio = _D_movtoEgreso.dbRow_cfdi_ingreso.saldo,
                        s_monedacontpaqi_id = self._dbRow_cfdi.monedacontpaqi_id,
                        f_tipocambio = self._dbRow_cfdi.tipocambio
                        )

                    _D_movtoEgreso.dbRow_cfdi_ingreso.saldo -= _D_result.f_saldoNuevo
                    _D_movtoEgreso.dbRow_cfdi_ingreso.update_record()

                    _D_movtoEgreso.n_cfdi_movimiento_emision_id = _D_result.n_cfdi_movimiento_id
                    _D_return.b_movtoActualizo = True

                elif (
                        (not _f_importeEgreso)
                        or (_D_movtoEgreso.dbRowMovtoEmision.importe != _f_importeEgreso)
                        ):

                    self._s_mensajeDebug = "Actualizando movto egreso emisión"

                    # Se actualiza el saldo del CFDI
                    _D_movtoEgreso.dbRow_cfdi_ingreso = dbc01.tcfdis(_D_movtoEgreso.dbRow_cfdi_relacionado.cfdirelacionado_id)

                    _D_movtoEgreso.dbRow_cfdi_ingreso.saldo = _D_movtoEgreso.dbRow_cfdi_ingreso.saldo or 0

                    if not _f_importeEgreso:
                        # Si no hay manera de saber el importe, usa el saldo del cfdi relacionado topado por el saldo del egreso
                        if _f_saldoEgreso >= _D_movtoEgreso.dbRow_cfdi_ingreso.saldo:
                            _f_importeEgreso = _D_movtoEgreso.dbRow_cfdi_ingreso.saldo
                        else:
                            _f_importeEgreso = _f_saldoEgreso

                        _D_movtoEgreso.dbRow_cfdi_relacionado.importe = _f_importeEgreso
                        _D_movtoEgreso.dbRow_cfdi_relacionado.update_record()
                    else:
                        pass

                    # Nuevo saldo = Viejo Saldo + (Viejo Pago - Nuevo Pago)
                    _D_movtoEgreso.dbRow_cfdi_ingreso.saldo += _D_movtoEgreso.dbRowMovtoEmision.importe - _f_importeEgreso
                    _D_movtoEgreso.dbRow_cfdi_ingreso.update_record()

                    # Se actualiza el pago en caso de existir sólo un movimiento
                    _D_movtoEgreso.dbRowMovtoEmision.saldo += _D_movtoEgreso.dbRowMovtoEmision.importe - _f_importeEgreso
                    _D_movtoEgreso.dbRowMovtoEmision.abono = _f_importeEgreso
                    _D_movtoEgreso.dbRowMovtoEmision.importe = _f_importeEgreso
                    _D_movtoEgreso.dbRowMovtoEmision.update_record()

                    _D_return.b_movtoActualizo = True

                else:
                    pass

                if self._dbRow_cfdi.sat_status == TCFDIS.SAT_STATUS.CANCELADO:

                    _f_importeEgresoCancelacion = _f_importeEgreso

                    if not _D_movtoEgreso.tieneMovtoCancelacion:
                        # Se inserta el movimiento de cancelacion del pago

                        self._s_mensajeDebug = "Creando movto egreso cancelación"

                        if not _D_movtoEgreso.dbRow_cfdi_ingreso:
                            _D_movtoEgreso.dbRow_cfdi_ingreso = dbc01.tcfdis(_D_movtoEgreso.dbRow_cfdi_relacionado.cfdirelacionado_id)
                            _D_movtoEgreso.dbRow_cfdi_ingreso.saldo = _D_movtoEgreso.dbRow_cfdi_ingreso.saldo or 0
                        else:
                            pass

                        _D_results = self._addMovimiento(
                            dt_fecha = self._dbRow_cfdi.sat_fecha_cancelacion,
                            E_accion = TCFDI_MOVIMIENTOS.E_ACCION.CANCELACION,
                            E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.DESCUENTO,
                            s_descripcion = "Cancelación Descuento",
                            dbRow_cfdi = self._dbRow_cfdi,
                            n_cfdi_ingreso_id = _D_movtoEgreso.dbRow_cfdi_relacionado.cfdirelacionado_id,
                            n_cfdi_relacionado_id = _D_movtoEgreso.dbRow_cfdi_relacionado.id,
                            dbRow_relacionado = _D_movtoEgreso.dbRow_cfdi_ingreso,
                            n_cfdi_movimiento_emision_id = _D_movtoEgreso.n_cfdi_movimiento_emision_id,
                            f_importe = _f_importeEgresoCancelacion,
                            f_cargo = _f_importeEgresoCancelacion,
                            f_abono = 0,
                            f_saldoPrevio = _D_movtoEgreso.dbRow_cfdi_ingreso.saldo,
                            s_monedacontpaqi_id = self._dbRow_cfdi.monedacontpaqi_id,
                            f_tipocambio = self._dbRow_cfdi.tipocambio
                            )
                        _D_movtoEgreso.dbRow_cfdi_ingreso.saldo += _D_results.f_saldoNuevo
                        _D_movtoEgreso.dbRow_cfdi_ingreso.update_record()

                        _D_return.b_movtoActualizo = True

                    elif _D_movtoEgreso.dbRowMovtoCancelacion.importe != _f_importeEgresoCancelacion:

                        self._s_mensajeDebug = "Actualizando movto egreso cancelación"

                        # Se actualiza el saldo del CFDI
                        if not _D_movtoEgreso.dbRow_cfdi_ingreso:
                            _D_movtoEgreso.dbRow_cfdi_ingreso = dbc01.tcfdis(_D_movtoEgreso.dbRow_cfdi_relacionado.cfdirelacionado_id)
                        else:
                            pass
                        # Nuevo saldo = Viejo Saldo + (Viejo Pago - Nuevo Pago)
                        _D_movtoEgreso.dbRow_cfdi_ingreso.saldo -= _D_movtoEgreso.dbRowMovtoCancelacion.importe - _f_importeEgresoCancelacion
                        _D_movtoEgreso.dbRow_cfdi_ingreso.update_record()

                        # Se actualiza el pago en caso de existir sólo un movimiento
                        _D_movtoEgreso.dbRowMovtoCancelacion.saldo -= _D_movtoEgreso.dbRowMovtoCancelacion.importe - _f_importeEgresoCancelacion
                        _D_movtoEgreso.dbRowMovtoCancelacion.cargo = _f_importeEgresoCancelacion
                        _D_movtoEgreso.dbRowMovtoCancelacion.importe = _f_importeEgresoCancelacion
                        _D_movtoEgreso.dbRowMovtoCancelacion.update_record()

                        _D_return.b_movtoActualizo = True

                    else:
                        pass

                elif _D_movtoEgreso.tieneMovtoCancelacion:
                    # Si no esta cancelado el cfdi, y tiene movimiento de cancelacion

                    self._s_mensajeDebug = "CFDI no esta cancelado, pero tiene movto cancelación"

                    # Se actualiza el saldo del CFDI
                    if not _D_movtoEgreso.dbRow_cfdi_ingreso:
                        _D_movtoEgreso.dbRow_cfdi_ingreso = dbc01.tcfdis(_D_movtoEgreso.dbRow_cfdi_relacionado.cfdirelacionado_id)
                        _D_movtoEgreso.dbRow_cfdi_ingreso.saldo = _D_movtoEgreso.dbRow_cfdi_ingreso.saldo or 0
                    else:
                        pass
                    # Nuevo saldo = Viejo Saldo + (Viejo Pago - Nuevo Pago)
                    _D_movtoEgreso.dbRow_cfdi_ingreso.saldo -= _D_movtoEgreso.dbRowMovtoCancelacion.importe
                    _D_movtoEgreso.dbRow_cfdi_ingreso.update_record()

                    _D_movtoEgreso.dbRowMovtoCancelacion.delete_record()

                else:
                    # No esta cancelado el CFDI del pago, con la ejecucción de los vigentes es suficiente
                    pass

                _f_saldoEgreso -= _f_importeEgreso + _f_importeEgresoCancelacion

            self._s_mensajeDebug = "Errores egreso"

            if _f_saldoEgreso < 0:
                _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                _D_return.s_msgError = "CFDI de egreso fue mal calculado. Favor de correr ajuste de saldos del cliente."
            elif (_f_saldoEgreso > 0) and (self._dbRow_cfdi.sat_status != TCFDIS.SAT_STATUS.CANCELADO):
                _D_return.E_return = stvfwk2_e_RETURN.OK_WARNING
                _D_return.s_msgError = "CFDI de egreso no uso todo su saldo. Favor de verificar."
            elif (_f_saldoEgreso != self._dbRow_cfdi.total) \
                    and (self._dbRow_cfdi.sat_status == TCFDIS.SAT_STATUS.CANCELADO):
                _D_return.E_return = stvfwk2_e_RETURN.OK_WARNING
                _D_return.s_msgError = "CFDI de egreso no fue calculado correctamente. " \
                                       "Favor de correr ajuste de saldos del cliente."
            else:
                pass

            return _D_return

        def _procesar_cfdi_ingreso_manejoSaldo(
                self,
                ):
            """ Identifica y actualiza/crea los movimientos

            Esta rutina supone que la información del cliente esta actualizada y sobre esa base contabiliza,
            modificando únicamente los registros impactado por el CFDI

            """

            _D_return = Storage(
                E_return = stvfwk2_e_RETURN.OK,
                s_msgError = "",
                L_movtos = [],
                b_movtoActualizo = False
                )

            _D_movtosManejoSaldo = Storage()

            # Se obtienen los complementos de pago y sus docs relacionados
            _dbRows_manejoSaldos = dbc01(
                (dbc01.tcfdi_manejosaldos.cfdi_id == self._n_cfdi_id)
                ).select(
                    dbc01.tcfdi_manejosaldos.ALL,
                    )

            # Se crea un diccionario con los complementos de pago
            for _dbRow_manejoSaldo in _dbRows_manejoSaldos:
                _D_movtosManejoSaldo[_dbRow_manejoSaldo.id] = Storage(
                    dbRowMovtoTraspaso = None,
                    n_cfdi_movimiento_traspaso_id = None,
                    dbRowMovtoAplicacion = None,
                    dbRow_cfdi_ingreso = None,
                    dbRow_cfdi_relacionado = _dbRow_manejoSaldo,
                    tieneMovtoTraspaso = False,  # Indica si el pago tiene movto de emisión relacionado
                    tieneMovtoAplicacion = False,  # Indica si el pago tiene movto de emisión relacionado
                    )

            # Se mapean los pagos con los movimientos, identificando si es emision o cancelacion
            _dbRows_movtos = dbc01(
                (dbc01.tcfdi_movimientos.cfdi_id == self._n_cfdi_id)
                & (
                    dbc01.tcfdi_movimientos.tipodocumento.belongs(
                        TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.TRASPASO_SALDO_A_FAVOR,
                        TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.APLICACION_SALDO_A_FAVOR,
                        )
                    )
                ).select(
                    dbc01.tcfdi_movimientos.ALL,
                    orderby = dbc01.tcfdi_movimientos.id
                    )

            for _dbRow_movto in _dbRows_movtos:
                if _dbRow_movto.cfdi_manejosaldo_id in _D_movtosManejoSaldo:
                    _D_movtoManejoSaldo = _D_movtosManejoSaldo[_dbRow_movto.cfdi_manejosaldo_id]
                    if (
                            (_dbRow_movto.accion == TCFDI_MOVIMIENTOS.E_ACCION.EMISION)
                            & (_dbRow_movto.tipodocumento == TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.TRASPASO_SALDO_A_FAVOR)
                            & (_D_movtoManejoSaldo.tieneMovtoTraspaso == False)
                            ):
                        _D_movtoManejoSaldo.tieneMovtoTraspaso = True
                        _D_movtoManejoSaldo.dbRowMovtoTraspaso = _dbRow_movto
                        _D_movtoManejoSaldo.n_cfdi_movimiento_traspaso_id = _dbRow_movto.id
                    elif (
                            (_dbRow_movto.accion == TCFDI_MOVIMIENTOS.E_ACCION.EMISION)
                            & (_dbRow_movto.tipodocumento == TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.APLICACION_SALDO_A_FAVOR)
                            & (_D_movtoManejoSaldo.tieneMovtoAplicacion == False)
                            ):

                        # Si el CFDI no esta cancelado, se cubre abajo el ajuste de saldo y movimientos
                        _D_movtoManejoSaldo.tieneMovtoAplicacion = True
                        _D_movtoManejoSaldo.dbRowMovtoAplicacion = _dbRow_movto

                    else:
                        _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                        _D_return.s_msgError = "Error al verificar movimientos, existen varios traspasos/aplicaciones " \
                                               "para el mismo CFDI. Favor de correr ajuste de saldos para el cliente."
                        break
                else:
                    # El movimiento no existe más y debe ser eliminado y el saldo del documento referenciado ajustado
                    _dbRow_cfdiIngreso = dbc01.tcfdis(_dbRow_movto.cfdi_ingreso_id)
                    # Nuevo saldo = Saldo - importe
                    if _dbRow_movto.tipodocumento == TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.TRASPASO_SALDO_A_FAVOR:
                        # Ya que no encuentra el montoSaldo inicial, se hace rollback del cargo
                        _dbRow_cfdiIngreso.saldo -= _dbRow_movto.importe

                    elif _dbRow_movto.tipodocumento == TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.APLICACION_SALDO_A_FAVOR:
                        # Ya que no encuentra el montoSaldo inicial, se hace rollback del abono
                        _dbRow_cfdiIngreso.saldo += _dbRow_movto.importe

                    else:
                        _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                        _D_return.s_msgError = "Accion en movimiento no identificada. Favor de correr ajuste de saldos del cliente."
                        break

                    _dbRow_cfdiIngreso.update_record()
                    _dbRow_movto.delete_record()

            # Se mueve por el diccionario de movimiento para determinar las acciones de ajuste de saldos o creacion de movimientos
            for _D_movtoManejoSaldo in _D_movtosManejoSaldo.values():

                _f_importeManejoSaldo = _D_movtoManejoSaldo.dbRow_cfdi_relacionado.importe or 0

                if not _D_movtoManejoSaldo.tieneMovtoTraspaso:
                    # Se inserta el movimiento de traspaso

                    # El movimiento de traspado corresponde al CFDI del registro de ManejoSaldo
                    _D_movtoManejoSaldo.dbRow_cfdi_ingreso = dbc01.tcfdis(_D_movtoManejoSaldo.dbRow_cfdi_relacionado.cfdi_id)

                    _D_result = self._addMovimiento(
                        dt_fecha = _D_movtoManejoSaldo.dbRow_cfdi_relacionado.fecha,
                        E_accion = TCFDI_MOVIMIENTOS.E_ACCION.EMISION,
                        E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.TRASPASO_SALDO_A_FAVOR,
                        s_descripcion = "Traspaso",
                        dbRow_cfdi = self._dbRow_cfdi,
                        n_cfdi_ingreso_id = _D_movtoManejoSaldo.dbRow_cfdi_relacionado.cfdi_id,
                        n_cfdi_relacionado_id = None,
                        dbRow_relacionado = _D_movtoManejoSaldo.dbRow_cfdi_ingreso,
                        n_cfdi_manejosaldo_id = _D_movtoManejoSaldo.dbRow_cfdi_relacionado.id,
                        f_importe = _f_importeManejoSaldo,
                        f_cargo = _f_importeManejoSaldo,
                        f_abono = 0,
                        f_saldoPrevio = _D_movtoManejoSaldo.dbRow_cfdi_ingreso.saldo,
                        s_monedacontpaqi_id = _D_movtoManejoSaldo.dbRow_cfdi_ingreso.monedacontpaqi_id,
                        f_tipocambio = _D_movtoManejoSaldo.dbRow_cfdi_ingreso.tipocambio
                        )
                    _D_movtoManejoSaldo.dbRow_cfdi_ingreso.saldo += _D_result.f_saldoNuevo
                    _D_movtoManejoSaldo.dbRow_cfdi_ingreso.update_record()

                    _D_movtoManejoSaldo.n_cfdi_movimiento_traspaso_id = _D_result.n_cfdi_movimiento_id
                    _D_return.b_movtoActualizo = True

                elif (
                        (_D_movtoManejoSaldo.dbRowMovtoTraspaso.importe != _f_importeManejoSaldo)
                        or (_D_movtoManejoSaldo.dbRowMovtoTraspaso.fecha != _D_movtoManejoSaldo.dbRow_cfdi_relacionado.fecha)
                        ):

                    # Se actualiza el saldo del CFDI
                    _D_movtoManejoSaldo.dbRow_cfdi_ingreso = dbc01.tcfdis(_D_movtoManejoSaldo.dbRow_cfdi_relacionado.cfdi_id)

                    # Nuevo saldo = Viejo Saldo + (Viejo Pago - Nuevo Pago)
                    _D_movtoManejoSaldo.dbRow_cfdi_ingreso.saldo -= _D_movtoManejoSaldo.dbRowMovtoTraspaso.importe - _f_importeManejoSaldo
                    _D_movtoManejoSaldo.dbRow_cfdi_ingreso.update_record()

                    # Se actualiza el pago en caso de existir sólo un movimiento
                    _D_movtoManejoSaldo.dbRowMovtoTraspaso.fecha = _D_movtoManejoSaldo.dbRow_cfdi_relacionado.fecha
                    _D_movtoManejoSaldo.dbRowMovtoTraspaso.saldo -= _D_movtoManejoSaldo.dbRowMovtoTraspaso.importe - _f_importeManejoSaldo
                    _D_movtoManejoSaldo.dbRowMovtoTraspaso.cargo = _f_importeManejoSaldo
                    _D_movtoManejoSaldo.dbRowMovtoTraspaso.importe = _f_importeManejoSaldo
                    _D_movtoManejoSaldo.dbRowMovtoTraspaso.update_record()

                    _D_return.b_movtoActualizo = True

                else:
                    pass

                if not _D_movtoManejoSaldo.tieneMovtoAplicacion:
                    # Se inserta el movimiento de cancelacion del pago

                    # Se cambia, el nuevo CFDI de ingreso debe ser el referenciado en el registro de Manejo Saldo
                    _D_movtoManejoSaldo.dbRow_cfdi_ingreso = dbc01.tcfdis(_D_movtoManejoSaldo.dbRow_cfdi_relacionado.cfdirelacionado_id)

                    _D_results = self._addMovimiento(
                        dt_fecha = _D_movtoManejoSaldo.dbRow_cfdi_relacionado.fecha,
                        E_accion = TCFDI_MOVIMIENTOS.E_ACCION.EMISION,
                        E_tipodocumento = TCFDI_MOVIMIENTOS.E_TIPODOCUMENTO.APLICACION_SALDO_A_FAVOR,
                        s_descripcion = "Aplicación",
                        dbRow_cfdi = self._dbRow_cfdi,
                        n_cfdi_ingreso_id = _D_movtoManejoSaldo.dbRow_cfdi_relacionado.cfdirelacionado_id,
                        n_cfdi_relacionado_id = None,
                        dbRow_relacionado = _D_movtoManejoSaldo.dbRow_cfdi_ingreso,
                        n_cfdi_movimiento_emision_id = _D_movtoManejoSaldo.n_cfdi_movimiento_emision_id,
                        n_cfdi_manejosaldo_id = _D_movtoManejoSaldo.dbRow_cfdi_relacionado.id,
                        f_importe = _f_importeManejoSaldo,
                        f_cargo = 0,
                        f_abono = _f_importeManejoSaldo,
                        f_saldoPrevio = _D_movtoManejoSaldo.dbRow_cfdi_ingreso.saldo,
                        s_monedacontpaqi_id = _D_movtoManejoSaldo.dbRow_cfdi_ingreso.monedacontpaqi_id,
                        f_tipocambio = _D_movtoManejoSaldo.dbRow_cfdi_ingreso.tipocambio
                        )
                    _D_movtoManejoSaldo.dbRow_cfdi_ingreso.saldo -= _D_results.f_saldoNuevo
                    _D_movtoManejoSaldo.dbRow_cfdi_ingreso.update_record()

                    _D_return.b_movtoActualizo = True

                elif (
                        (_D_movtoManejoSaldo.dbRowMovtoAplicacion.importe != _f_importeManejoSaldo)
                        or (_D_movtoManejoSaldo.dbRowMovtoAplicacion.fecha != _D_movtoManejoSaldo.dbRow_cfdi_relacionado.fecha)
                        ):

                    # Se cambia, el nuevo CFDI de ingreso debe ser el referenciado en el registro de Manejo Saldo
                    _D_movtoManejoSaldo.dbRow_cfdi_ingreso = dbc01.tcfdis(_D_movtoManejoSaldo.dbRow_cfdi_relacionado.cfdirelacionado_id)
                    # Nuevo saldo = Viejo Saldo + (Viejo Pago - Nuevo Pago)
                    _D_movtoManejoSaldo.dbRow_cfdi_ingreso.saldo += _D_movtoManejoSaldo.dbRowMovtoAplicacion.importe - _f_importeManejoSaldo
                    _D_movtoManejoSaldo.dbRow_cfdi_ingreso.update_record()

                    # Se actualiza el pago en caso de existir sólo un movimiento
                    _D_movtoManejoSaldo.dbRowMovtoAplicacion.fecha = _D_movtoManejoSaldo.dbRow_cfdi_relacionado.fecha
                    _D_movtoManejoSaldo.dbRowMovtoAplicacion.saldo += _D_movtoManejoSaldo.dbRowMovtoAplicacion.importe - _f_importeManejoSaldo
                    _D_movtoManejoSaldo.dbRowMovtoAplicacion.abono = _f_importeManejoSaldo
                    _D_movtoManejoSaldo.dbRowMovtoAplicacion.importe = _f_importeManejoSaldo
                    _D_movtoManejoSaldo.dbRowMovtoAplicacion.update_record()

                    _D_return.b_movtoActualizo = True

                else:
                    pass

            return _D_return

        def procesar_cfdi(
                self,
                x_cfdi_id
                ):
            """ Procesa y en caso de no existir genera los movimientos relacionados al CFDI de ingreso.
            TODO verificar si ya fue agregado, evitar agregarlo nuevamente

            """

            _D_return = Storage(
                E_return = stvfwk2_e_RETURN.OK,
                s_msgError = "",
                )

            self._E_proceso = self.E_CFDI

            self._dt_procesoInicio = datetime.datetime.now()

            self._dbRow_cliente = dbc01.tempresa_clientes(self._n_cliente_id)

            self._dbRow_cliente.procesostatus = TEMPRESA_CLIENTES.E_PROCESOESTATUS.CFDIS_PROCESANDO
            self._dbRow_cliente.update_record()
            dbc01.commit()

            try:
                if isinstance(x_cfdi_id, (str, int)):
                    self._n_cfdi_id = int(x_cfdi_id)
                    self._dbRow_cfdi = dbc01.tcfdis(self._n_cfdi_id)

                else:
                    self._dbRow_cfdi = x_cfdi_id
                    self._n_cfdi_id = self._dbRow_cfdi.id

                if not self._dbRow_cfdi:
                    self._s_mensajeDebug = "CFDI no detectado"
                    # Si no existe el registro, significa que fue eliminado, por lo que se eliminan sus movimientos
                    _n_rows = dbc01(
                        (dbc01.tcfdi_movimientos.cliente_id == self._n_cliente_id)
                        & (dbc01.tcfdi_movimientos.cfdi_id == self._n_cfdi_id)
                        ).delete()

                elif self._dbRow_cfdi.sat_status not in (TCFDIS.SAT_STATUS.VIGENTE, TCFDIS.SAT_STATUS.CANCELADO):
                    self._s_mensajeDebug = "CFDI detectado sin estatus V o C"
                    _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                    _D_return.s_msgError = "CFDI no tiene status del sat reconocido como: vigente o cancelado."

                elif (
                        (self._dbRow_cfdi.sat_status == TCFDIS.SAT_STATUS.CANCELADO)
                        and not self._dbRow_cfdi.sat_fecha_cancelacion
                        ):

                    _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                    _D_return.s_msgError = "CFDI cancelado y no tiene fecha de cancelación. No se puede calcular edo cta."

                else:
                    self._s_mensajeDebug = "CFDI detectado, buscando movtos posteriores"
                    # Se busca en los CFDIs relacionados si existen movimientos posteriores a la fecha del egreso/pago,
                    # si es así, no puede calcularse el saldo con este método.

                    if self._dbRow_cfdi.sat_status == TCFDIS.SAT_STATUS.VIGENTE:
                        _dt_fechaMovimiento = self._dbRow_cfdi.fecha
                    elif self._dbRow_cfdi.sat_status == TCFDIS.SAT_STATUS.CANCELADO:
                        _dt_fechaMovimiento = self._dbRow_cfdi.sat_fecha_cancelacion
                    else:
                        # No debería entrar a este punto nunca
                        raise "Nunca debe entrar a este punto"

                    _D_result = None
                    if self._dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO:
                        self._s_mensajeDebug = "Comienza a evaluar ingreso"

                        _D_result = self._procesar_cfdi_ingreso()
                        _b_movtoActualizo = _D_result.b_movtoActualizo
                        if _D_result.E_return == stvfwk2_e_RETURN.OK:
                            self._s_mensajeDebug = "Comienza el manejo saldo"
                            _D_result = self._procesar_cfdi_ingreso_manejoSaldo()
                        else:
                            pass

                        _D_return = _D_result
                        _D_return.b_movtoActualizo = _b_movtoActualizo

                    elif self._dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.EGRESO:

                        self._s_mensajeDebug = "Comienza a evaluar egreso"
                        _D_result = self._procesar_cfdi_egreso()
                        _D_return = _D_result

                    elif self._dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.PAGO:

                        self._s_mensajeDebug = "Comienza a evaluar pago"
                        _D_result = self._procesar_cfdi_pago()
                        _D_return = _D_result

                    elif self._dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.NOMINA:
                        # No soportado aún e ignorado
                        pass

                    elif self._dbRow_cfdi.tipocomprobante_id == TTIPOSCOMPROBANTES.TRASPASO:
                        # No soportado aún e ignorado
                        pass

                    else:
                        # Jamás deberia entrar aqui
                        _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                        _D_return.s_msgError = "Tipo comprobante en CFDI no reconocido."

                    if _D_result and _D_result.b_movtoActualizo:

                        self._s_mensajeDebug = "Se actualiza el movimiento"

                        _L_cfdiIngresosAfectados = TCFDIS.OBTENER_CFDISINGRESOS_AFECTADOS(self._dbRow_cfdi)

                        self._s_mensajeDebug = "Se buscan los cfdi relacionados"
                        # Se buscand los movimientos posteriores que pudieran hacer conflicto con el cálculo del edo cta
                        _dbRows_relacionados = dbc01(
                            (dbc01.tcfdi_relacionados.cfdirelacionado_id.belongs(_L_cfdiIngresosAfectados))
                            & (dbc01.tcfdis.id == dbc01.tcfdi_relacionados.cfdi_id)
                            & (dbc01.tcfdis.sat_status == TCFDIS.SAT_STATUS.VIGENTE)
                            & (dbc01.tcfdis.fecha > _dt_fechaMovimiento)
                            ).select(
                                dbc01.tcfdi_relacionados.ALL
                                )

                        if _dbRows_relacionados:
                            _D_return.E_return = stvfwk2_e_RETURN.OK_WARNING
                            _D_return.s_msgError = "CFDI no puede generar estado de cuenta debido a que se detectaron " \
                                                   "egresos en cfdis relacionados posteriores. " \
                                                   "Requiere recalcular saldo del cliente."

                        else:

                            self._s_mensajeDebug = "Se buscan los cfdi relacionados cancelados"
                            _dbRows_relacionados_cancelados = dbc01(
                                (dbc01.tcfdi_relacionados.cfdirelacionado_id.belongs(_L_cfdiIngresosAfectados))
                                & (dbc01.tcfdis.id == dbc01.tcfdi_relacionados.cfdi_id)
                                & (dbc01.tcfdis.sat_status == TCFDIS.SAT_STATUS.CANCELADO)
                                & (dbc01.tcfdis.sat_fecha_cancelacion > _dt_fechaMovimiento)
                                ).select(
                                    dbc01.tcfdi_relacionados.ALL
                                    )

                            if _dbRows_relacionados_cancelados:
                                _D_return.E_return = stvfwk2_e_RETURN.OK_WARNING
                                _D_return.s_msgError = "CFDI no puede generar estado de cuenta debido a que se detectaron " \
                                                       "egresos cancelados en cfdis relacionados posteriores. " \
                                                       "Requiere recalcular saldo del cliente."

                            else:

                                self._s_mensajeDebug = "Se buscan los cfdi pagos"
                                _dbRows_pagosRelacionados = dbc01(
                                    (dbc01.tcfdi_complementopago_doctosrelacionados.cfdirelacionado_id.belongs(_L_cfdiIngresosAfectados))
                                    & (dbc01.tcfdi_complementopagos.id == dbc01.tcfdi_complementopago_doctosrelacionados.cfdi_complementopago_id)
                                    & (dbc01.tcfdi_complementopagos.fechapago > _dt_fechaMovimiento)
                                    & (dbc01.tcfdis.id == dbc01.tcfdi_complementopagos.cfdi_id)
                                    & (dbc01.tcfdis.sat_status == TCFDIS.SAT_STATUS.VIGENTE)
                                    ).select(
                                        dbc01.tcfdis.ALL
                                        )

                                if _dbRows_pagosRelacionados:
                                    _D_return.E_return = stvfwk2_e_RETURN.OK_WARNING
                                    _D_return.s_msgError = "CFDI no puede generar estado de cuenta debido a que se detectaron " \
                                                           "pagos en cfdis relacionados posteriores. " \
                                                           "Requiere recalcular saldo del cliente."

                                else:

                                    self._s_mensajeDebug = "Se buscan los cfdi pagos cancelados"
                                    _dbRows_pagosRelacionados_cancelados = dbc01(
                                        (dbc01.tcfdi_complementopago_doctosrelacionados.cfdirelacionado_id.belongs(_L_cfdiIngresosAfectados))
                                        & (dbc01.tcfdi_complementopagos.id == dbc01.tcfdi_complementopago_doctosrelacionados.cfdi_complementopago_id)
                                        & (dbc01.tcfdis.id == dbc01.tcfdi_complementopagos.cfdi_id)
                                        & (dbc01.tcfdis.sat_status == TCFDIS.SAT_STATUS.CANCELADO)
                                        & (dbc01.tcfdis.sat_fecha_cancelacion > _dt_fechaMovimiento)
                                        ).select(
                                            dbc01.tcfdis.ALL
                                            )

                                    if _dbRows_pagosRelacionados_cancelados:
                                        _D_return.E_return = stvfwk2_e_RETURN.OK_WARNING
                                        _D_return.s_msgError = "CFDI no puede generar estado de cuenta debido a que se detectaron " \
                                                               "pagos cancelados en cfdis relacionados posteriores. " \
                                                               "Requiere recalcular saldo del cliente."

                                    else:
                                        pass
            except Exception as _O_excepcion:
                _D_return.E_return = stvfwk2_e_RETURN.NOK_LOGIN
                _D_return.s_msgError = u'Proceso: %s, Excepción: %s %s %s' % (
                    "Estado Cuenta ", str(_O_excepcion.__doc__), str(_O_excepcion.args), self._s_mensajeDebug
                    )
            finally:
                self.actualizar_saldo_cliente()  # Remueve el proces del cliente al actualizar su saldo

                self._E_proceso = self.E_NINGUNO

            return _D_return
