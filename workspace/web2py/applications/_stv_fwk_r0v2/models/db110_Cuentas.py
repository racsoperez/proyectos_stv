# -*- coding: utf-8 -*-

""" Define todas las tablas utilizadas para uso en la cuenta """


class TCUENTAS(Table):
    """ Definición de la tabla de cuenta """
    S_NOMBRETABLA = 'tcuentas'

    L_DBCAMPOS = [
        Field(
            'nombre', 'string', length = 100, default = None,
            required = True,
            notnull = True, unique = True,
            widget = stv_widget_input, label = 'Nombre', comment = '',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'nombrecorto', 'string', length = 100, default = None,
            required = True,
            notnull = False, unique = True,
            widget = stv_widget_input, label = 'Nombre corto', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'rfc', 'string', length = None, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'RFC', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'nombre_db', 'string', length = 100, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Nombre de la base de datos', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'imagennombrearchivo', 'string', length = 100, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Nombre original del archivo', comment = None,
            writable = False, readable = False,
            represent = stv_represent_string
            ),
        Field(
            'imagen', type = 'upload',
            required = False,
            requires = IS_LENGTH(minsize = 0, maxsize = 1572864, error_message = 'El archivo es muy grande.'),
            notnull = False,
            uploadfield = True, widget = lambda f, d, u = None: stv_widget_inputFile(
                f, d, URL(a = D_stvFwkCfg.s_nameBackofficeApp, c = 'cuentas', f = 'cuentas_imagen'),
                db.tcuentas.imagennombrearchivo
                ),
            label = 'Imagen', comment = None,
            writable = True, readable = True, authorize = lambda record: auth.is_logged_in(),
            autodelete = True, represent = lambda v, r: stv_represent_image(
                v, r, URL(a = D_stvFwkCfg.s_nameBackofficeApp, c = 'cuentas', f = 'cuentas_imagen')
                ),
            uploadfolder = os.path.join(request.folder, 'uploads', 'cuenta'),
            uploadseparate = False, uploadfs = None
            ),
        Field(
            'imagenpinnombrearchivo', 'string', length = 100, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Nombre original del archivo', comment = None,
            writable = False, readable = False,
            represent = stv_represent_string
            ),
        Field(
            'imagenpin', type = 'upload',
            required = False,
            requires = IS_LENGTH(minsize = 0, maxsize = 1572864, error_message = 'El archivo es muy grande.'),
            notnull = False,
            uploadfield = True, widget = lambda f, d, u = None: stv_widget_inputFile(
                f, d, URL(a = D_stvFwkCfg.s_nameBackofficeApp, c = 'cuentas', f = 'cuentas_imagenpin'),
                db.tcuentas.imagenpinnombrearchivo
                ),
            label = 'Imagen Pin', comment = None,
            writable = True, readable = True, authorize = lambda record: auth.is_logged_in(),
            autodelete = True, represent = lambda v, r: stv_represent_image(
                v, r, URL(a = D_stvFwkCfg.s_nameBackofficeApp, c = 'cuentas', f = 'cuentas_imagenpin')
                ),
            uploadfolder = os.path.join(request.folder, 'uploads', 'cuenta'),
            uploadseparate = False, uploadfs = None
            ),
        Field(
            'servidorgestionador_id', 'integer', default = None,
            required = False,
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = stv_widget_db_combobox, label = 'Servidor Gestionador', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, db.tcuentas.servidorgestionador_id)
            ),
        Field(
            'frecuenciagestionador_cfdis', 'integer', default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Frec. (Hrs) Act. CFDIs',
            comment = 'Frecuencia de monitoreo del gestionador para bajar CFDIs en horas, 0 para desabilitar.',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'frecuenciagestionador_cancelados', 'integer', default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Frec. (Hrs) Act. Canc.',
            comment = 'Frecuencia de monitoreo del gestionador para CFDI Cancelados en horas, 0 para desabilitar.',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'horagestionador_cfdis', 'time', default = None,
            required = False, requires = IS_NULL_OR(IS_TIME(error_message = 'Hora inválida.')),
            notnull = False, unique = False,
            widget = stv_widget_inputTime, label = 'Hora fija Act. CFDIs',
            comment = 'Hora fija en la que se ejecuta el gestionador para bajar cfdis. 00:00:00 desabilita su uso.',
            writable = True, readable = True,
            represent = stv_represent_time
            ),
        Field(
            'horagestionador_cancelados', 'time', default = None,
            required = False, requires = IS_NULL_OR(IS_TIME(error_message = 'Hora inválida.')),
            notnull = False, unique = False,
            widget = stv_widget_inputTime, label = 'Hora fija Act. Canc.',
            comment = 'Hora fija en la que se ejecuta el gestionador para bajar cancelados. '
            + '00:00:00 desabilita su uso.',
            writable = True, readable = True,
            represent = stv_represent_time
            ),
        Field(
            'archivolicencianombre', 'string', length = 100, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Archivo licencia nombre', comment = None,
            writable = False, readable = False,
            represent = stv_represent_string
            ),
        Field(
            'archivolicencia', type = 'upload',
            required = False,
            requires = IS_LENGTH(minsize = 0, maxsize = 1572864, error_message = 'El archivo es muy grande.'),
            notnull = False,
            uploadfield = True, widget = lambda f, d, u = None: stv_widget_inputFile(
                f, d, URL(a = D_stvFwkCfg.s_nameBackofficeApp, c = 'cuentas', f = 'cuentas_archivos'),
                db.tcuentas.archivolicencianombre
                ),
            label = 'Archivo licencia', comment = None,
            writable = True, readable = True, authorize = lambda record: auth.is_logged_in(),
            autodelete = True, represent = lambda v, r: stv_represent_file(
                v, r, URL(c = 'cuentas', f = 'cuentas_archivos', vars = D_stvFwkCfg.D_cfg_cuenta_para_links),
                db.tcuentas.archivolicencianombre
                ),
            uploadfolder = os.path.join(request.folder, 'uploads', 'cuenta'),
            uploadseparate = False, uploadfs = None
            ),
        Field(
            'idintegrador', 'string', length = 100, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Id del integrador', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'rfcintegrador', 'string', length = 100, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'RFC integrador', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        tSignature,
        ]

    pass


db.define_table(
    TCUENTAS.S_NOMBRETABLA, *TCUENTAS.L_DBCAMPOS,
    format = '%(nombrecorto)s',
    singular = 'Cuenta',
    plural = 'Cuentas',
    migrate = D_stvFwkCfg.b_requestIsMaster,
    table_class = TCUENTAS
    )


class TCUENTA_DOMICILIOS:
    """ Definición del detalle de domicilios para la cuenta """
    pass


db.define_table(
    'tcuenta_domicilios',
    Field('cuenta_id', db.tcuentas, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Cuenta', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('nombrecorto', 'string', length=100, default=None,
      required=True,
      notnull=True, unique=False,
      widget = stv_widget_input, label='Nombre corto', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('calleynumero', 'string', length = 200, default = None,
      required = True,
      notnull = True, unique = True,
      widget = stv_widget_input, label='Calle y número', comment = 'Captura la calle y el número',
      writable = True, readable = True,
      represent = stv_represent_string),
    Field('colonia', 'string', length=100, default=None,
      required=False,
      notnull=False, unique=True,
      widget = stv_widget_input, label='Colonia', comment=None,
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('ciudad', 'string', length=100, default=None,
      required=False,
      notnull=False, unique=False,
      widget = stv_widget_input, label='Ciudad', comment='Ciudad y opcionalmente municipio',
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('pais', 'string', length=50, default=None,
      required=False,
      notnull=False, unique=False,
      widget = stv_widget_input, label='País', comment='',
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('codigopostal', 'string', length=20, default=None,
      required=False,
      notnull=False, unique=False,
      widget = stv_widget_input, label='Código postal', comment='',
      writable=True, readable=True,
      represent=stv_represent_string),
    tSignature,
    format = '%(nombrecorto)s',
    singular = 'Dirección',
    plural = 'Direcciones',
    migrate = D_stvFwkCfg.b_requestIsMaster
    )


class TCUENTA_TELEFONOS:
    """ Definición del detalle de telefonos para la cuenta """

    pass


db.define_table(
    'tcuenta_telefonos',
    Field('cuenta_id', db.tcuentas, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Cuenta', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('contacto', 'string', length = 200, default = None,
      required = False,
      notnull = False, unique = False,
      widget = stv_widget_input, label='Contacto', comment = None,
      writable = True, readable = True,
      represent = stv_represent_string),
    Field('telefono', 'string', length = 200, default = None,
      required = False,
      notnull = False, unique = False,
      widget = stv_widget_input, label='Teléfono', comment = None,
      writable = True, readable = True,
      represent = stv_represent_string),
    tSignature,
    format = '%(contacto)s [%(telefono)s]',
    singular = 'Teléfono',
    plural = 'Teléfonos',
    migrate = D_stvFwkCfg.b_requestIsMaster
    )


class TCUENTA_APLICACIONES(Table):
    """ Definición de las aplicaciones disponibles en el sistema """

    S_NOMBRETABLA = 'tcuenta_aplicaciones'

    L_DBCAMPOS = [
        Field(
            'cuenta_id', db.tcuentas, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'Cuenta', comment = None,
            writable = False, readable = False,
            represent = None
            ),
        Field(
            'descripcion', 'string', length = 100, default = None,
            required = True,
            notnull = True, unique = False,
            widget = stv_widget_input, label = 'Descripción', comment = 'Descripción de la forma',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'nombrecorto', 'string', length = 100, default = None,
            required = True,
            notnull = True, unique = False,
            widget = stv_widget_input, label = 'Nombre corto', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'aplicacion_id', 'integer', default = None,
            required = False,
            ondelete = 'NO ACTION', notnull = False, unique = False,
            label = 'Aplicación', comment = None,
            writable = True, readable = True,
            represent = stv_represent_referencefield
            ),

        Field(
            'empresa_id', 'integer', default = None,  # Campo a ligar con dbc01.tempresas
            required = False,
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = stv_widget_input,
            label = 'Empresa',
            comment = 'Si se define la empresa, la esctructura de la aplicación puede incluir la empresa seleccionada',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'nombre_db', 'string', length = 100, default = None,
            # Aun no esta soportado el uso de base de datos a nivel aplicacion
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Nombre de la base de datos', comment = None,
            writable = False, readable = False,
            represent = stv_represent_string
            ),
        Field(
            'imagennombrearchivo', 'string', length = 100, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Nombre original del archivo', comment = None,
            writable = False, readable = False,
            represent = stv_represent_string
            ),
        Field(
            'imagen', type = 'upload',
            required = False,
            requires = IS_LENGTH(minsize = 0, maxsize = 1572864, error_message = 'El archivo es demasiado grande.'),
            notnull = False,
            uploadfield = True, widget = lambda f, d, u = None: stv_widget_inputFile(
                f, d, TCUENTA_APLICACIONES.GET_IMAGEN_URL(), db.tcuentas.imagennombrearchivo
                ),
            label = 'Imagen', comment = None,
            writable = True, readable = True, authorize = lambda record: auth.is_logged_in(),
            autodelete = True,
            represent = lambda v, r: stv_represent_image(v, r, TCUENTA_APLICACIONES.GET_IMAGEN_URL()),
            uploadfolder = os.path.join(request.folder, 'uploads', 'cuenta'),
            uploadseparate = False, uploadfs = None
            ),
        Field(
            'imagenpinnombrearchivo', 'string', length = 100, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Nombre original del archivo', comment = None,
            writable = False, readable = False,
            represent = stv_represent_string
            ),
        Field(
            'imagenpin', type = 'upload',
            required = False,
            requires = IS_LENGTH(minsize = 0, maxsize = 1572864, error_message = 'El archivo es demasiado grande.'),
            notnull = False,
            uploadfield = True, widget = lambda f, d, u = None: stv_widget_inputFile(
                f, d, TCUENTA_APLICACIONES.GET_IMAGENPIN_URL(), db.tcuentas.imagenpinnombrearchivo
                ),
            label = 'Imagen Pin', comment = None,
            writable = True, readable = True, authorize = lambda record: auth.is_logged_in(),
            autodelete = True,
            represent = lambda v, r: stv_represent_image(v, r, TCUENTA_APLICACIONES.GET_IMAGENPIN_URL()),
            uploadfolder = os.path.join(request.folder, 'uploads', 'cuenta'),
            uploadseparate = False, uploadfs = None
            ),
        Field(
            'mostrar', 'boolean', default = True,
            required = False,
            notnull = False,
            widget = stv_widget_inputCheckbox, label = 'Mostrar', comment = None,
            writable = True, readable = True,
            represent = stv_represent_boolean
            ),
        tSignature,
        ]

    @classmethod
    def GET_EMPRESA_ID(cls, n_cuenta_aplicacion):
        """Se consigue el concepto_id.

            n_cuenta_aplicacion: ID de la aplicación.
        """

        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            n_empresa_id = None
            )

        # Query para obtener el ID del concepto de entrada por compra.
        _dbRowEmpresa_id = db.tcuenta_aplicaciones(n_cuenta_aplicacion).empresa_id

        if not _dbRowEmpresa_id:
            _D_return.s_msgError = "No se ha encontrado la empresa_id para esta cuenta aplicación."
            _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
        else:
            _D_return.n_empresa_id = _dbRowEmpresa_id

        return _D_return

    @classmethod
    def GET_EMPRESA_ID_CACHE(cls, n_cuenta_aplicacion):
        """Se consigue el concepto_id y se guarda en caché

            n_cuenta_aplicacion: ID de la aplicación.
        """

        _s_cache_id = 'db110_CuentaAplicacion_%s' % (str(n_cuenta_aplicacion))
        _D_result = cache.ram(
            _s_cache_id,
            lambda: TCUENTA_APLICACIONES.GET_EMPRESA_ID(n_cuenta_aplicacion),
            time_expire=86400
            )

        if _D_result.E_return != CLASS_e_RETURN.OK:
            cache.ram(_s_cache_id, None)
        else:
            pass

        return _D_result

    @classmethod
    def GET_IMAGEN_URL(cls, s_value = None, s_filename = None):
        _s_url = URL(a=D_stvFwkCfg.s_nameBackofficeApp, c='cuentas', f='cuentas_imagen')
        if s_value:
            _s_url += '/' + s_value
        else:
            pass
        if s_filename:
            _s_url += '?filename=' + str(s_filename)
        else:
            pass
        return _s_url

    @classmethod
    def GET_IMAGENPIN_URL(cls, s_value = None, s_filename = None):
        _s_url = URL(a=D_stvFwkCfg.s_nameBackofficeApp, c='cuentas', f='cuentas_imagenpin')
        if s_value:
            _s_url += '/' + s_value
        else:
            pass
        if s_filename:
            _s_url += '?filename=' + str(s_filename)
        else:
            pass
        return _s_url

    @classmethod
    def CONFIGURAR_INTEGRIDAD_REFERENCIAL(cls):

        db.tcuenta_aplicaciones.aplicacion_id.type = 'reference taplicaciones'
        db.tcuenta_aplicaciones.aplicacion_id.ondelete = 'NO ACTION'
        db.tcuenta_aplicaciones.aplicacion_id.requires = IS_IN_DB(db, 'taplicaciones.id', db.taplicaciones._format)
        db.tcuenta_aplicaciones.aplicacion_id.widget = stv_widget_db_combobox
        db.tcuenta_aplicaciones.aplicacion_id.represent = stv_represent_referencefield

        return None

    pass


db.define_table(
    TCUENTA_APLICACIONES.S_NOMBRETABLA, *TCUENTA_APLICACIONES.L_DBCAMPOS,
    format = lambda r: db.tcuentas[r.cuenta_id].nombrecorto + ' : ' + r.nombrecorto,
    singular = 'Aplicación',
    plural = 'Aplicaciones',
    migrate = D_stvFwkCfg.b_requestIsMaster,
    table_class = TCUENTA_APLICACIONES
    )


class TCUENTA_USUARIOS(Table):
    """ Definición de los usuarios autorizados para la aplicación """

    S_NOMBRETABLA = 'tcuenta_usuarios'

    L_DBCAMPOS = [
        Field(
            'cuenta_id', db.tcuentas, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            widget = stv_widget_db_combobox, label = 'Cuenta', comment = None,
            writable = False, readable = False,
            represent = stv_represent_referencefield
            ),
        Field(
            'user_id', db.auth_user, default = None,
            required = False, requires = IS_NULL_OR(IS_IN_DB(db, 'auth_user.id', db.auth_user._format)),
            ondelete = 'NO ACTION', notnull = True, unique = False,
            widget = lambda f, v: stv_widget_db_search(
                f, v,
                s_display = '%(auth_user.first_name)s %(auth_user.last_name)s [%(auth_user.email)s]',
                s_url = URL(a = D_stvFwkCfg.s_nameBackofficeApp, c = 'accesos', f = 'usuarios_buscar')
                ),
            label = 'Usuario', comment = 'Usuario con permiso para entrar a la aplicación',
            writable = True, readable = True,
            represent = stv_represent_referencefield
            ),
        Field(
            'descripcion', 'string', length = 100, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Descripción', comment = 'Descripción de uso del perfil',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        tSignature,
        ]

    pass


db.define_table(
    TCUENTA_USUARIOS.S_NOMBRETABLA, *TCUENTA_USUARIOS.L_DBCAMPOS,
    format = '%(user_id)s',
    singular = 'Usuario Clave',
    plural = 'Usuarios Clave',
    migrate = D_stvFwkCfg.b_requestIsMaster,
    table_class = TCUENTA_USUARIOS
    )


class TCUENTA_EMPRESAS(Table):
    """ Definición del detalle de empresas con RFCs ligados para la cuenta """

    S_NOMBRETABLA = 'tcuenta_empresas'

    L_DBCAMPOS = [
        Field(
            'cuenta_id', db.tcuentas, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'Cuenta', comment = None,
            writable = False, readable = False,
            represent = None
            ),
        Field(
            'empresa_id', 'integer', default = None,  # Campo a ligar con dbc01.tempresas
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            widget = stv_widget_db_combobox,
            label = 'Empresa', comment = None,
            writable = True, readable = True,
            represent = stv_represent_referencefield
            ),
        tSignature,
        ]

    @classmethod
    def REPRESENT(
            cls,
            dbRow_cuenta_empresa
            ):
        if 'dbc01' not in globals():
            importar_definiciones_aplicacion()
            importar_dbs_genericos()
            importar_dbs_cuenta(s_remplaza_cuenta_id = dbRow_cuenta_empresa.cuenta_id)

        else:
            pass

        _dbRow_empresa = dbc01.tempresas(dbRow_cuenta_empresa.empresa_id)
        _s_return = dbc01.tempresas._format % _dbRow_empresa.as_dict()

        return _s_return
    
    pass


db.define_table(
    TCUENTA_EMPRESAS.S_NOMBRETABLA, *TCUENTA_EMPRESAS.L_DBCAMPOS,
    format = lambda dbRow: TCUENTA_EMPRESAS.REPRESENT(dbRow),
    singular = 'Empresa',
    plural = 'Empresas',
    migrate = D_stvFwkCfg.b_requestIsMaster,
    table_class = TCUENTA_EMPRESAS
    )


class TCUENTA_SERVIDORESCONTPAQI(Table):

    S_NOMBRETABLA = 'tcuenta_servidorescontpaqi'

    L_DBCAMPOS = [
        Field(
            'cuenta_id', db.tcuentas, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            label = 'Cuenta', comment = None,
            writable = False, readable = False,
            represent = None
            ),
        Field(
            'nombreservidor', 'string', length = 80, default = None,
            required = True,
            notnull = True, unique = False,
            widget = stv_widget_input, label = 'Nombre del servidor', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'numeroserie', 'string', length = 80, default = None,
            required = True,
            notnull = True, unique = False,
            widget = stv_widget_input, label = 'Número de serie',
            comment = 'Número de serie de la motherboard donde está instalado el CONTPAQi.',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        tSignature,
        ]
    pass


db.define_table(
    TCUENTA_SERVIDORESCONTPAQI.S_NOMBRETABLA, *TCUENTA_SERVIDORESCONTPAQI.L_DBCAMPOS,
    format = '%(nombreservidor)s [%(numeroserie)s]',
    singular = 'Servidor CONTPAQi',
    plural = 'Servidores CONTPAQi',
    migrate = D_stvFwkCfg.b_requestIsMaster,
    table_class = TCUENTA_SERVIDORESCONTPAQI
    )


class TCUENTA_EMPRESA_PAGOS:
    # Definición del detalle de empresas con RFCs ligados para la cuenta
    pass


db.define_table(
    'tcuenta_empresa_pagos',
    Field('cuenta_empresa_id', db.tcuenta_empresas, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Cuenta', comment=None,
      writable=False, readable=False,
      represent=None),
    Field('anio', 'integer', 
      required=True, default = int(request.browsernow.year),
      notnull=True, unique=False,
      widget = stv_widget_input, label='Año', comment='',
      writable=True, readable=True,
      represent=stv_represent_string),
    Field('mes', 'integer', default = int(request.browsernow.strftime("%m")),
      required = True, requires = IS_IN_SET(TGENERICAS.MES.D_TODOS, zero = 0, error_message = 'Selecciona'),
      notnull = True, unique = False,
      widget = stv_widget_combobox, 
      label='Mes', comment = '',
      writable = True, readable = True,
      represent = lambda v, r: stv_represent_list(v, r, None, TGENERICAS.MES.D_TODOS)),
    Field('comprobante_nombrearchivo', 'string', length=100, default=None,
      required=False,
      notnull=False, unique=False,
      widget = stv_widget_input, label='Nombre archivo', comment=None,
      writable=False, readable=False,
      represent=stv_represent_string),
    Field(
        'comprobante', type = 'upload',
        required = False,
        requires = IS_LENGTH(minsize = 0, maxsize = 1572864, error_message = 'El archivo es muy grande.'),
        notnull = False,
        uploadfield = True,
        widget = lambda f, d, u = None: stv_widget_inputFile(
            f, d, URL(a = D_stvFwkCfg.s_nameBackofficeApp, c = 'cuentas', f = 'archivo'),
            db.tcuenta_empresa_pagos.comprobante_nombrearchivo
            ),
        label = 'Comprobante', comment = None,
        writable = True, readable = True, authorize = lambda record: auth.is_logged_in(),
        autodelete = True,
        represent = lambda v, r: stv_represent_image(
            v, r, URL(a = D_stvFwkCfg.s_nameBackofficeApp, c = 'cuentas', f = 'archivo')
            ),
        uploadfolder = os.path.join(request.folder, 'uploads', 'cuenta'),
        uploadseparate = False, uploadfs = None
        ),
    tSignature,
    format = '%(anio)s/%(mes)s',
    singular = 'Empresa Pago',
    plural = 'Empresa Pagos',
    migrate = D_stvFwkCfg.b_requestIsMaster
    )


class TCUENTA_EMPRESA_CERTIFICADOS:
    """ Tabla que contiene la lista de certificados del SAT de la empresa, donde podra timbrar y actualizar
    los certificados cuando sea requerido
    """
    
    pass


db.define_table(
    'tcuenta_empresa_certificados',
    Field(
        'cuenta_empresa_id', db.tcuenta_empresas, default = None,
        required = True,
        ondelete = 'CASCADE', notnull = True, unique = False,
        label = 'Cuenta', comment = None,
        writable = False, readable = False,
        represent = None
        ),
    Field(
        'vigencia_cer', type = FIELD_UTC_DATETIME, default=request.browsernow,
        required = True, requires = IS_DATETIME(format= STV_FWK_APP.FORMAT.s_DATETIME),
        notnull = True,
        widget = stv_widget_inputDateTime, label='Vencimiento Certificado', comment = '',
        writable = True, readable = True,
        represent = stv_represent_datetime
        ),    
    Field(
        'archivo_cer_nombre', 'string', length = 100, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Nombre archivo Cer', comment = None,
        writable = False, readable = False,
        represent = stv_represent_string
        ),
    Field(
        'archivo_cer', type = 'upload',
        required = False,
        requires = IS_LENGTH(minsize = 0, maxsize = 1572864, error_message = 'El archivo es muy grande.'),
        notnull = False,
        uploadfield = True,
        widget = lambda f, d, u = None: stv_widget_inputFile(
            f, d,
            URL(a = D_stvFwkCfg.s_nameBackofficeApp, c = 'cuentas', f = 'cuentas_archivos'),
            db.tcuenta_empresa_certificados.archivo_cer_nombre
            ),
        label = 'Archivo certificado', comment = None,
        writable = True, readable = True, authorize = lambda record: auth.is_logged_in(),
        autodelete = True,
        represent = lambda v, r: stv_represent_file(
            v, r,
            URL(c = 'cuentas', f = 'cuentas_archivos', vars = D_stvFwkCfg.D_cfg_cuenta_para_links),
            db.tcuenta_empresa_certificados.archivo_cer_nombre
            ),
        uploadfolder = os.path.join(request.folder, 'uploads', 'cuenta'),
        uploadseparate = False, uploadfs = None
        ),
    tSignature,
    format = '%(anio)s/%(mes)s',
    singular = 'Empresa Certificado',
    plural = 'Empresa Certificados',
    migrate = D_stvFwkCfg.b_requestIsMaster
    )


class TCUENTA_EMPRESA_FIRMAS:
    """ Tabla que contiene la lista de certificados del SAT de la empresa, donde podra timbrar y actualizar
    los certificados cuando sea requerido
    """
    
    pass


db.define_table(
    'tcuenta_empresa_firmas',
    Field(
        'cuenta_empresa_id', db.tcuenta_empresas, default=None,
        required=True,
        ondelete='CASCADE', notnull=True, unique=False,
        label='Cuenta', comment=None,
        writable=False, readable=False,
        represent=None
        ),
    Field(
        'vigencia_firma', type = FIELD_UTC_DATETIME, default=request.browsernow,
        required = True, requires = IS_DATETIME(format= STV_FWK_APP.FORMAT.s_DATETIME),
        notnull = True,
        widget = stv_widget_inputDateTime, label='Vencimiento Firma', comment = '',
        writable = True, readable = True,
        represent = stv_represent_datetime
        ),    
    Field(
        'archivo_firma_nombre', 'string', length = 100, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Nombre archivo firma', comment = None,
        writable = False, readable = False,
        represent = stv_represent_string
        ),
    Field(
        'archivo_firma', type = 'upload',
        required = False, 
        requires = IS_LENGTH(minsize=0, maxsize=1572864, error_message='El archivo es muy grande.'),
        notnull = False,
        uploadfield = True, 
        widget=lambda f, d, u=None: stv_widget_inputFile(
            f, 
            d, 
            URL(a=D_stvFwkCfg.s_nameBackofficeApp, c='cuentas', f='cuentas_archivos'), 
            db.tcuenta_empresa_firmas.archivo_firma_nombre
            ), 
        label='Archivo Key', comment = None,
        writable = True, readable = True, authorize = lambda record: auth.is_logged_in(),
        autodelete = True, 
        represent = lambda v, r: stv_represent_file(
            v, r,
            URL(c='cuentas', f='cuentas_archivos', vars=D_stvFwkCfg.D_cfg_cuenta_para_links), 
            db.tcuenta_empresa_firmas.archivo_firma_nombre
            ),
        uploadfolder = os.path.join(request.folder, 'uploads', 'cuenta'),
        uploadseparate = False, uploadfs = None 
        ),
    tSignature,
    format = '%(anio)s/%(mes)s',
    singular = 'Empresa Firma',
    plural = 'Empresa Firmas',
    migrate = D_stvFwkCfg.b_requestIsMaster
    )


class TCUENTA_PERFILESUSUARIOS:
    """ Definición de los perfiles manejables por el cliente para la creación de usuarios """
    pass


db.define_table(
    'tcuenta_perfilesusuarios',
    Field('cuenta_id', db.tcuentas, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      label='Cuenta', comment=None,
      writable=False, readable=False,
      represent=stv_represent_referencefield),
    Field('perfil_id', db.auth_group, default=None,
      required=True,
      ondelete='NO ACTION', notnull=True, unique=False,
      widget = stv_widget_db_combobox, label='Perfil', comment='Perfil que el cliente podrá configurar a sus usuarios',
      writable=True, readable=True,
      represent=stv_represent_referencefield),
    Field('descripcion', 'string', length = 100, default = None,
      required = False,
      notnull = False, unique = False,
      widget = stv_widget_input, label='Descripción', comment = 'Descripción de uso del perfil',
      writable = True, readable = True,
      represent = stv_represent_string),
    tSignature,
    format = '%(perfil_id)s',
    singular = 'Profile for User',
    plural = 'Profiles for User',
    migrate = D_stvFwkCfg.b_requestIsMaster
    )


class TCUENTA_APLICACION_USUARIOS:
    """ Definición de los usuarios autorizados para la aplicación """
    pass


db.define_table(
    'tcuenta_aplicacion_usuarios',
    Field('cuenta_aplicacion_id', db.tcuenta_aplicaciones, default=None,
      required=True,
      ondelete='CASCADE', notnull=True, unique=False,
      widget = stv_widget_db_combobox, label='Cuenta aplicación', comment=None,
      writable=False, readable=False,
      represent=stv_represent_referencefield),
    Field('user_id', db.auth_user, default=None,
      required=False, requires = IS_NULL_OR(IS_IN_DB(db, 'auth_user.id', db.auth_user._format)),
      ondelete='NO ACTION', notnull=True, unique=False,
        widget = lambda f, v: stv_widget_db_search(
            f, v,
            s_display = '%(auth_user.first_name)s %(auth_user.last_name)s [%(auth_user.email)s]',
            s_url = URL(a = D_stvFwkCfg.s_nameBackofficeApp, c = 'accesos', f = 'usuarios_buscar')
            ),
      label='Usuario', comment='Usuario con permiso para entrar a la aplicación',
      writable=True, readable=True,
      represent=stv_represent_referencefield),
    Field('descripcion', 'string', length = 100, default = None,
      required = False,
      notnull = False, unique = False,
      widget = stv_widget_input, label='Descripción', comment = 'Descripción de uso del perfil',
      writable = True, readable = True,
      represent = stv_represent_string),
    tSignature,
    format = '%(user_id)s',
    singular = 'Usuario',
    plural = 'Usuarios',
    migrate = D_stvFwkCfg.b_requestIsMaster
    )


class TSERVIDORES(Table):
    """ Definición de la tabla de servidores """

    S_NOMBRETABLA = 'tservidores'

    class E_ESTATUS:
        LIBRE = 0                   # Servidor esta libre
        CONECTADO = 1               # Servidor Conectado
        CON_ERROR = 2               # Servidor Con Error
        _D_dict = None
        
        @classmethod
        def GET_DICT(cls):
            if not cls._D_dict:
                cls._D_dict = {
                    cls.LIBRE: 'Libre',
                    cls.CONECTADO: 'Conectado',
                    cls.CON_ERROR: 'Con Error',
                    }
            else:
                pass
            return cls._D_dict

    L_DBCAMPOS = [
        Field(
            'descripcion', 'string', length = 100, default = None,
            required = True,
            notnull = True, unique = True,
            widget = stv_widget_input, label = 'Descripción', comment = '',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'dt_grabazonah_leezonah', TDATETIME_GRABAENZONAH_LEERENZONAH, default = request.browsernow,
            required = False, requires = IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME),
            notnull = False, unique = False,
            widget = stv_widget_inputDateTime, label = 'Test GrabaZonaH LeeZonaH',
            comment = 'Fecha y hora UTC, a partir de la cual aplica la diferencia',
            writable = True, readable = True,
            represent = stv_represent_datetime
            ),
        Field(
            'dt_grabadirecto_leezonah', TDATETIME_GRABADIRECTO_LEERENZONAH, default = request.utcnow,
            required = False, requires = IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME),
            notnull = False, unique = False,
            widget = stv_widget_inputDateTime, label = 'Test GrabaDir LeeZonaH',
            comment = 'Fecha y hora UTC, a partir de la cual aplica la diferencia',
            writable = True, readable = True,
            represent = stv_represent_datetime
            ),
        Field(
            'dt_grabadirecto_leedirecto', TDATETIME_GRABADIRECTO_LEERDIRECTO, default = request.browsernow,
            required = False, requires = IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME),
            notnull = False, unique = False,
            widget = stv_widget_inputDateTime, label = 'Test GrabaDir LeeDir',
            comment = 'Fecha y hora UTC, a partir de la cual aplica la diferencia',
            writable = True, readable = True,
            represent = stv_represent_datetime
            ),

        Field(
            'codigo', 'string', length = 100, default = None,
            required = True,
            notnull = False, unique = True,
            widget = stv_widget_input, label = 'Código único', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'serveruser_id', db.auth_user, default = None,
            required = False, requires = IS_NULL_OR(IS_IN_DB(db, 'auth_user.id', db.auth_user._format)),
            ondelete = 'NO ACTION', notnull = True, unique = False,
            widget = lambda f, v: stv_widget_db_search(
                f, v,
                s_display = '%(auth_user.first_name)s %(auth_user.last_name)s [%(auth_user.email)s]',
                s_url = URL(a = D_stvFwkCfg.s_nameBackofficeApp, c = 'accesos', f = 'usuarios_buscar')
                ),
            label = 'Usuario del servidor', comment = 'Usuario usado por el servidor para conectarse al portal',
            writable = True, readable = True,
            represent = stv_represent_referencefield
            ),
        Field(
            'contacto', 'string', length = 200, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Contact', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'telefono', 'string', length = 200, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Teléfono', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'calleynumero', 'string', length = 200, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Calle', comment = 'Captura la calle y el número',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'colonia', 'string', length = 100, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Colonia', comment = None,
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'ciudad', 'string', length = 100, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Ciudad', comment = 'Ciudad y opcionalmente municipio',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'estado', 'string', length = 100, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Estado', comment = '',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'pais', 'string', length = 50, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Pais', comment = '',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'codigopostal', 'string', length = 20, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Código postal', comment = '',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        # Se configuran los siguientes servicios en el servidor
        Field(
            'gestionador', 'boolean', default = 0,
            required = True,
            notnull = True,
            widget = stv_widget_inputCheckbox, label = 'Gestionador', comment = None,
            writable = True, readable = True,
            represent = stv_represent_boolean
            ),

        Field(
            'zonahoraria_id', db.tzonashorarias, default = None,
            required = False,
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Zona Horaria', comment = None,
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, db.tservidores.zonahoraria_id)
            ),

        Field(
            'archivolibrerianombre', 'string', length = 100, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Archivo libreria nombre', comment = None,
            writable = False, readable = False,
            represent = stv_represent_string
            ),
        Field(
            'archivolibreria', type = 'upload',
            required = False,
            requires = IS_LENGTH(minsize = 0, maxsize = 1572864, error_message = 'El archivo es muy grande.'),
            notnull = False,
            uploadfield = True, widget = lambda f, d, u = None: stv_widget_inputFile(
                f, d, URL(a = D_stvFwkCfg.s_nameBackofficeApp, c = 'cuentas', f = 'cuentas_archivos'),
                db.tservidores.archivolibrerianombre
                ),
            label = 'Archivo libreria', comment = None,
            writable = True, readable = True, authorize = lambda record: auth.is_logged_in(),
            autodelete = True, represent = lambda v, r: stv_represent_file(
                v, r, URL(c = 'cuentas', f = 'cuentas_archivos', vars = D_stvFwkCfg.D_cfg_cuenta_para_links),
                db.tservidores.archivolibrerianombre
                ),
            uploadfolder = os.path.join(request.folder, 'uploads', 'servidores'),
            uploadseparate = False, uploadfs = None
            ),

        Field(
            'archivogestionadornombre', 'string', length = 100, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Archivo gestionador nombre', comment = None,
            writable = False, readable = False,
            represent = stv_represent_string
            ),
        Field(
            'archivogestionador', type = 'upload',
            required = False,
            requires = IS_LENGTH(minsize = 0, maxsize = 1572864, error_message = 'El archivo es muy grande.'),
            notnull = False,
            uploadfield = True, widget = lambda f, d, u = None: stv_widget_inputFile(
                f, d, URL(a = D_stvFwkCfg.s_nameBackofficeApp, c = 'cuentas', f = 'cuentas_archivos'),
                db.tservidores.archivogestionadornombre
                ),
            label = 'Archivo gestionador', comment = None,
            writable = True, readable = True, authorize = lambda record: auth.is_logged_in(),
            autodelete = True, represent = lambda v, r: stv_represent_file(
                v, r, URL(c = 'cuentas', f = 'cuentas_archivos', vars = D_stvFwkCfg.D_cfg_cuenta_para_links),
                db.tservidores.archivogestionadornombre
                ),
            uploadfolder = os.path.join(request.folder, 'uploads', 'servidores'),
            uploadseparate = False, uploadfs = None
            ),
        Field(
            'sesionesactivas', 'integer', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputInteger, label = 'Sesiones activas', comment = '',
            writable = False, readable = True,
            represent = stv_represent_number
            ),
        tSignature,
        ]

    def __init__(self, dbRef, tablename, *fields, **args):
        Table.__init__(self, dbRef, tablename, *fields, **args)
        self.PROC.dbTabla = self
        return

    class PROC:

        dbTabla = None  # Se escribe con la inicialización

        def __init__(self, **D_defaults):
            """ Inicializa objecto de tipo PROC para control del registro actual

            @param D_defaults:
            @type D_defaults:
            """
            self._D_defaults = Storage(D_defaults)
            self._L_requeridos = []
            self.dbTabla.notas.readable = True
            return

        def puede_crear(self, **D_defaults):
            """ Determina si puede o no crear un registro.

            Actualmente no hay condición requerida.
            """
            _D_return = FUNC_RETURN()

            self._D_defaults.update(D_defaults or {})

            for _s_requerido in self._L_requeridos:
                if _s_requerido not in self._D_defaults:
                    _D_return.agrega_error(
                        "Campo {campo} es requerido para generar {tabla}".format(
                            campo = _s_requerido,
                            tabla = self.dbTabla._singular
                            )
                        )

                else:
                    pass

            return _D_return

        @classmethod
        def PUEDE_EDITAR(cls, x_servidor):
            """ Regresa OK si el estado permite modificar, de lo contrario False

            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_servidor, cls.dbTabla)
            _dbRow = _D_results.dbRow

            if not _dbRow:
                _D_return.agrega_error("{tabla} no identificado.".format(tabla = cls.dbTabla._singular))

            else:
                pass

            return _D_return

        @classmethod
        def PUEDE_ELIMINAR(cls, x_servidor):
            """ Determina si puede o no eliminar un registro

            Misma condición que puede_editar, a lo mejor pudieran verse condiciones distintas

            """

            return cls.PUEDE_EDITAR(x_servidor)

        def configuracampos_nuevo(
                self,
                s_servidor_id,
                b_llamadaDesdeEditar = False,
                **D_defaults
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param b_llamadaDesdeEditar:
            @type b_llamadaDesdeEditar:
            @param s_servidor_id:
            @type s_servidor_id:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            self._D_defaults.update(D_defaults or {})
            for _s_fieldName in self.dbTabla.fields:
                self.dbTabla[_s_fieldName].writable = False
            for _s_fieldName in self._D_defaults:
                self.dbTabla[_s_fieldName].default = self._D_defaults[_s_fieldName]

            if not b_llamadaDesdeEditar:
                _D_results = self.puede_crear()
            else:
                _D_results = _D_return

            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.combinar(_D_results)

            elif s_servidor_id:
                _dbRow_base = self.dbTabla(s_servidor_id)

                self.dbTabla.descripcion.default = _dbRow_base.descripcion
                # self.dbTabla.codigo.default = _dbRow_base.codigo
                self.dbTabla.serveruser_id.default = _dbRow_base.serveruser_id
                self.dbTabla.contacto.default = _dbRow_base.contacto
                self.dbTabla.telefono.default = _dbRow_base.telefono
                self.dbTabla.calleynumero.default = _dbRow_base.calleynumero
                self.dbTabla.colonia.default = _dbRow_base.colonia
                self.dbTabla.ciudad.default = _dbRow_base.ciudad
                self.dbTabla.estado.default = _dbRow_base.estado
                self.dbTabla.pais.default = _dbRow_base.pais
                self.dbTabla.codigopostal.default = _dbRow_base.codigopostal
                self.dbTabla.gestionador.default = _dbRow_base.gestionador
                self.dbTabla.zonahoraria_id.default = _dbRow_base.zonahoraria_id
                # self.dbTabla.archivolibrerianombre.default = _dbRow_base.archivolibrerianombre
                # self.dbTabla.archivolibreria.default = _dbRow_base.archivolibreria
                # self.dbTabla.archivogestionadornombre.default = _dbRow_base.archivogestionadornombre
                # self.dbTabla.archivogestionador.default = _dbRow_base.archivogestionador
                # self.dbTabla.sesionesactivas.default = _dbRow_base.sesionesactivas

            else:
                pass

            self.dbTabla.descripcion.writable = True
            self.dbTabla.codigo.writable = True
            self.dbTabla.serveruser_id.writable = True
            self.dbTabla.contacto.writable = True
            self.dbTabla.telefono.writable = True
            self.dbTabla.calleynumero.writable = True
            self.dbTabla.colonia.writable = True
            self.dbTabla.ciudad.writable = True
            self.dbTabla.estado.writable = True
            self.dbTabla.pais.writable = True
            self.dbTabla.codigopostal.writable = True
            self.dbTabla.gestionador.writable = True
            self.dbTabla.zonahoraria_id.writable = True
            self.dbTabla.archivolibreria.writable = True
            self.dbTabla.archivogestionador.writable = True
            self.dbTabla.dt_grabazonah_leezonah.writable = True
            self.dbTabla.dt_grabadirecto_leezonah.writable = True
            self.dbTabla.dt_grabadirecto_leedirecto.writable = True

            # Campos que no deben ser editados, solo de forma autocalculada
            self.dbTabla.archivolibrerianombre.writable = False
            self.dbTabla.archivogestionadornombre.writable = False
            self.dbTabla.sesionesactivas.writable = False

            # Campos que todos los movimientos pueden editar
            self.dbTabla.notas.writable = True

            # Se definen los valores por default obligados

            return _D_return

        def configuracampos_edicion(
                self,
                x_servidor,
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param x_servidor: es el registro del movimiento o su id
            @type x_servidor:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_servidor, self.dbTabla)
            _dbRow = _D_results.dbRow

            # Se llenan los default obligados
            for _s_nombreCampo in self._L_requeridos:
                self._D_defaults[_s_nombreCampo] = _D_results.dbRow[_s_nombreCampo]

            _D_results = self.PUEDE_EDITAR(_dbRow)

            if _D_results.E_return == CLASS_e_RETURN.OK:
                # Si puede editar regresa un ok y el movimiento esta abierto

                # Se manda vacío el argumento para no agregar defaults a los campos
                _D_results = self.configuracampos_nuevo(
                    None,
                    b_llamadaDesdeEditar = True,
                    **self._D_defaults
                    )
                _D_return.combinar(_D_results)

            else:
                # De lo contrario en cualquier otro caso, solo se pueden editar las observaciones
                for _s_fieldName in self.dbTabla.fields:
                    self.dbTabla[_s_fieldName].writable = False

                self.dbTabla.notas.writable = True

            return _D_return

        def al_validar(
                self,
                O_form,
                ):
            """ Validar la información para poder grabar

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _dbRow = self.dbTabla(O_form.record_id) if O_form.record_id else None
            _D_camposAChecar = STV_FWK_LIB.OBTENER_CAMPOSACHECAR_PARA_VALIDAR(self.dbTabla, _dbRow, O_form)

            if not O_form.record_id:
                # Se esta creando el registro

                _D_results = self.puede_crear(**_D_camposAChecar)
                if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                    O_form.errors.id = _D_results.s_msgError
                else:
                    pass

            else:
                # Se esta modificando el registro, la llamada a configuraCampos_edicion ya definió con writable
                #  que puede modificarse y que no. Por lo que el código para verificar si puede o no editar
                #  no es requerido en este caso.
                pass

            # Validaciones

            if not _D_camposAChecar.descripcion:
                O_form.errors.id = "Descripción debe ser especificada"
            else:
                pass

            return O_form

        @classmethod
        def AL_ACEPTAR(
                cls,
                O_form,
                ):
            """ Aceptación del registro

            Se hacen todos los cálculos y aceptación de los cambios en el producto

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()
            dbRow = cls.dbTabla(O_form.record_id or O_form.vars.id)

            if not O_form.record_id:
                # Si se inserta
                pass

            else:  # Si se edita
                pass

            _D_results = cls.MAPEAR_CAMPOS(dbRow)
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.combinar(_D_results)

            else:
                _D_results = cls.CALCULAR_CAMPOS(dbRow)
                _D_return.combinar(_D_results)

            generaMensajeFlash(cls.dbTabla, _D_return)
            return O_form

        @classmethod
        def ANTES_ELIMINAR(
                cls,
                L_errorsAction,
                dbRow
                ):
            """ Antes de eliminar un movimiento de inventario de este tipo

            @param dbRow:
            @type dbRow:
            @param L_errorsAction:
            @type L_errorsAction:
            @return: errores
            @rtype: lista de diccionarios {'msg':'...'}
            """
            _D_results = cls.PUEDE_ELIMINAR(dbRow)
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                L_errorsAction.append({'msg': _D_results.s_msgError})
            else:
                cls._dbRow_aEliminar = Storage(dbRow.as_dict())

            return L_errorsAction

        @classmethod
        def DESPUES_ELIMINAR(
                cls,
                L_errorsAction,
                ):
            """ Después de eliminar un registro

            @param L_errorsAction:
            @type L_errorsAction:
            @return: errores
            @rtype: lista de diccionarios {'msg':'...'}
            """
            _D_return = FUNC_RETURN()

            if cls._dbRow_aEliminar:
                pass
            else:
                _D_return.E_return = CLASS_e_RETURN.NOK_SECUENCIA
                _D_return.s_msgError += "Error en secuencia de llamado de funciones para borrado. "

            generaMensajeFlash(cls.dbTabla, _D_return)
            return L_errorsAction

        @classmethod
        def MAPEAR_CAMPOS(
                cls,
                x_servidor,
                ):
            """ Remapea campos solamente cuando se graba el registro

            @param x_servidor:
            @type x_servidor:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_servidor, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage()

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)
                    break
                else:
                    pass

            return _D_return

        @classmethod
        def CALCULAR_CAMPOS(
                cls,
                x_servidor,
                ):
            """ Recalcula campos que tienen impacto cuando otros registros de otras tablas cambian
            Esta operación así mismo detecta y agrega las inconsistencias

            @param x_servidor:
            @type x_servidor:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_servidor, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage()

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)
                    break
                else:
                    pass

            return _D_return

    pass  # TSERVIDORES


db.define_table(
    TSERVIDORES.S_NOMBRETABLA, *TSERVIDORES.L_DBCAMPOS,
    format = '%(descripcion)s',
    singular = 'Servidor',
    plural = 'Servidores',
    migrate = D_stvFwkCfg.b_requestIsMaster,
    table_class = TSERVIDORES
    )


class TSERVIDOR_INSTRUCCIONES(Table):
    """ Definición de la tabla de servidores """

    S_NOMBRETABLA = 'tservidor_instrucciones'

    class E_TIPOINSTRUCCION:
        NADA = 1  # Operación dummy, para no hacer nada, pruebas
        VARIAS = 2  # Usada para identificar que hay varias instrucciones especificadas
        ACTUALIZAR_CFDIS = 10  # Operación común para actualizar CFDIs emitidos dt_desde dt_hasta
        ACTUALIZAR_CFDISRX = 11  # Operación común para actualizar CFDIs recibidos dt_desde dt_hasta
        ACTUALIZAR_CFDIS_CANCELADOS = 12  # Operación común para actualizar CFDIs emitidos cancelados dt_desde dt_hasta
        # Operación común para actualizar CFDIs recibidos cancelados dt_desde dt_hasta
        ACTUALIZAR_CFDISRX_CANCELADOS = 13
        BORRAR_CFDIS_SUBIDOS = 20  # Operación para borrar los CFDIs subidos dt_desde dt_hasta
        ACTUALIZAR_LICENCIA = 110  # Operación para requerir el archivo de la licencia
        ACTUALIZAR_LIBRERIA_CFDIS = 120  # Operación para actualizar la librería de los CFDIs
        ACTUALIZAR_CER = 130  # Operación para actualizar el certificado de los CFDIs
        ACTUALIZAR_KEY = 140  # Operación para actualizar la firma electrónica de los CFDIs
        ACTUALIZAR_GESTIONADOR = 200  # Operación para actualizar el gestionador de CFDIs
        CONFIGURAR_URL = 310  # Operación para actualizar el url en el ini
        CONFIGURAR_EMAIL = 320  # Operación para actualizar el email en el ini
        CONFIGURAR_PASS = 330  # Operación para actualizar el password en el ini
        CONFIGURAR_FRECUENCIA_MIN = 340  # Operación para actualizar la frecuencia en el ini
        ESTATUS_SERVIDOR = 410  # Operación en la que el Gestionador indicará el estatus del servidor
        TIMBRAR_CFDI = 510  # Operación común para timbrar CFDIs
        CANCELAR_CFDI = 520  # Operación común para cancelar CFDIs
        CANCELAR_CFDIRX = 521  # Operación común para cancelar CFDIs
        D_TODAS = {
            NADA: '1: Nueva',
            VARIAS: '2: Varias',
            ACTUALIZAR_CFDIS: '10: Actualizar CFDIs Emitidos',
            ACTUALIZAR_CFDISRX: '11: Actualizar CFDIs Recibidos',
            ACTUALIZAR_CFDIS_CANCELADOS: '12: Act. CFDIs Emitidos Canc.',
            ACTUALIZAR_CFDISRX_CANCELADOS: '13: Act. CFDIs Recibidos Canc.',
            BORRAR_CFDIS_SUBIDOS: '20: Borrar CFDIs subidos',
            ACTUALIZAR_LICENCIA: '110: Actualizar Licencia',
            ACTUALIZAR_LIBRERIA_CFDIS: '120: Actualizar Libreria CFDIs',
            ACTUALIZAR_CER: '130: Actualizar certificado',
            ACTUALIZAR_KEY: '140: Actualizar llave',
            ACTUALIZAR_GESTIONADOR: '200: Actualizar Gestionador',
            CONFIGURAR_URL: '310: Configurar URL',
            CONFIGURAR_EMAIL: '320: Configurar Email',
            CONFIGURAR_PASS: '330: Configurar PASS',
            CONFIGURAR_FRECUENCIA_MIN: '340: Configurar Frecuencia Mínima',
            ESTATUS_SERVIDOR: '410: Status del Servidor',
            TIMBRAR_CFDI: '510: Timbrar emitido',
            CANCELAR_CFDI: '520: Cancelar emitido',
            CANCELAR_CFDIRX: '521: Cancelar recibido',
            }
        # Lista de instrucciónes donde el CFDI_ID esta en el PARAM3
        L_CONCFDI_EN_PARAM3 = [
            TIMBRAR_CFDI,
            CANCELAR_CFDI
            ]
        
        @classmethod
        def GET_DICT(cls):
            return cls.D_TODAS
        
    class E_SERVICIO:
        NADA = 0                    # Instrucción general al servidor, no a un servicio en específico
        GESTIONADOR = 1             # Servicio de Gestionador
        TODOS = 255                 # A todos los servicios
        _D_dict = None
        
        @classmethod
        def GET_DICT(cls):
            if not cls._D_dict:
                cls._D_dict = {
                    cls.NADA: 'Nada',
                    cls.GESTIONADOR: 'Gestionador',
                    cls.TODOS: 'Todos',
                    }
            else:
                pass
            return cls._D_dict    
        
    class E_FUNCIONALIDAD:
        NADA = 0                    # Funcionalidad genérica
        LICENCIA = 110              # Funcionalidad del uso de la Licencia
        LIBRERIA = 120              # Funcionalidad del uso de la Librería
        GESTIONADOR = 200           # Funcionalidad del Gestionador
        _D_dict = None
        
        @classmethod
        def GET_DICT(cls):
            if not cls._D_dict:
                cls._D_dict = {
                    cls.NADA: 'Nada',
                    cls.LICENCIA: 'Licencia',
                    cls.LIBRERIA: 'Librería',
                    cls.GESTIONADOR: 'Gestionador',
                    }
            else:
                pass
            return cls._D_dict
    
    class E_ESTADO:
        PENDIENTE = 0       # Usado cuando se crea la instrucción pero no ha sido mandada al servidor.
        EN_PROCESO = 1      # Usado cuando ya se mando la instrucción al servidor.
        TERMINADA = 2       # Usado cuando el servidor terminó la instrucción.
        CANCELADA = 3       # Usado cuando el usuario cancela la instrucción.
        TIMEOUT = 4         # Usado en instrucciones que tienen vigencia y el tiempo paso.
        DENEGADA = 5        # Usado en instrucciones que es denegada por alguna razon.
        ERROR = 6           # Usado cuando el servidor regresa un error.
        D_TODOS = {
            PENDIENTE : 'Pendiente',
            EN_PROCESO: 'En Proceso',
            TERMINADA : 'Terminada',
            CANCELADA : 'Cancelada',
            TIMEOUT   : 'Timeout',  # Cuando por cuestiones de tiempo venció la petición
            DENEGADA  : 'Denegada',  # Cuando la herramienta niega el proceso
            ERROR     : 'Error',  # Cuando finaliza el proceso con error
            }
        # Lista con los estados que identifican si una instrucción esta activa
        L_ACTIVOS = [
            PENDIENTE,
            EN_PROCESO,
            # TERMINADA,
            # CANCELADA, # Si la instrucción es cancelada, se reintenta
            # TIMEOUT,
            # DENEGADA,
            # ERROR
            ]
        # Estados que determinan las instrucciones a considerar para determinar si las instrucciones creadas
        #  automáticamente deben crearse o no, ya que debe pasar un tiempo desde la última instrucción.
        # Actualmente incluye todos los estados, para uso futuro.
        L_PERMITEN_PROGRAMAR_NUEVA = [
            PENDIENTE,
            EN_PROCESO,
            TERMINADA,
            CANCELADA,  # Si la instrucción es cancelada, se reintenta
            TIMEOUT,
            DENEGADA,
            ERROR
            ]

        @classmethod
        def GET_DICT(cls):
            return cls.D_TODOS

    L_DBCAMPOS = [
        Field(
            'servidor_id', db.tservidores, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            widget = stv_widget_db_combobox, label = 'Servidor', comment = None,
            writable = False, readable = True,
            represent = stv_represent_referencefield
            ),
        Field(
            'cuenta_id', db.tcuentas, default = None,
            required = False,
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Cuenta', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, db.tservidor_instrucciones.cuenta_id)
            ),
        Field(
            'empresa_id', 'integer', default = None,
            required = False, requires = IS_IN_DB(db, 'tcuenta_empresas.empresa_id', db.tcuenta_empresas._format),
            ondelete = 'NO ACTION', notnull = False, unique = False,
            widget = lambda field, value: stv_widget_db_chosen_linked(
                field,
                value,
                maxselect = 1,
                filterFieldName = 'cuenta_id',
                linkedUrl = URL(a = 'backoffice', c = 'cuentas', f = 'cuenta_empresas_ver.json'),
                D_additionalAttributes = Storage(
                    linkedTableFieldName = 'empresa_id'
                    )
                ),
            label = 'Empresa', comment = None,
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r, db.tservidor_instrucciones.empresa_id)
            ),
        Field(
            'user_id', db.auth_user, default = None,
            required = False, requires = IS_NULL_OR(IS_IN_DB(db, 'auth_user.id', db.auth_user._format)),
            ondelete = 'NO ACTION', notnull = True, unique = False,
            widget = lambda f, v: stv_widget_db_search(
                f, v,
                s_display = '%(auth_user.first_name)s %(auth_user.last_name)s [%(auth_user.email)s]',
                s_url = URL(a = D_stvFwkCfg.s_nameBackofficeApp, c = 'accesos', f = 'usuarios_buscar')
                ),
            label = 'Usuario', comment = 'Usuario que creó la instrucción',
            writable = False, readable = True,
            represent = stv_represent_referencefield
            ),
        Field(
            'fecha', type = FIELD_UTC_DATETIME, default = request.browsernow,
            required = True, requires = IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME),
            notnull = True,
            widget = stv_widget_inputDateTime, label = 'Fecha Request', comment = '',
            writable = False, readable = True,
            represent = stv_represent_datetime
            ),
        Field(
            'tipoinstruccion', 'integer', default = 0,
            required = False, requires = IS_IN_SET(
                E_TIPOINSTRUCCION.GET_DICT(), zero = 0, error_message = 'Selecciona'
                ),
            notnull = False, unique = False,
            widget = stv_widget_combobox, label = 'Tipo de petición', comment = 'Define el tipo de instrucción',
            writable = False, readable = True,
            represent = lambda x_value, dbRow: stv_represent_list(
                x_value,
                dbRow,
                fnLambda = None,
                D_data = TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.GET_DICT(),
                dbField = db.tservidor_instrucciones.tipoinstruccion
                )
            ),
        Field(
            'servicio', 'integer', default = E_SERVICIO.GESTIONADOR,
            required = False,
            requires = IS_IN_SET(E_SERVICIO.GET_DICT(), zero = 0, error_message = 'Selecciona'),
            notnull = False, unique = False,
            widget = stv_widget_combobox, label = 'Servicio',
            comment = 'Especifica el servicio al que corresponde la instrucción',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TSERVIDOR_INSTRUCCIONES.E_SERVICIO.GET_DICT())
            ),
        Field(
            'estado', 'integer', default = E_ESTADO.PENDIENTE,
            required = False,
            requires = IS_IN_SET(E_ESTADO.GET_DICT(), zero = 0, error_message = 'Selecciona'),
            notnull = False, unique = False,
            widget = stv_widget_combobox, label = 'Estado', comment = 'Estado en el que se encuentra la instrucción',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TSERVIDOR_INSTRUCCIONES.E_ESTADO.GET_DICT())
            ),

        Field(
            's_param0', 'string', length = 512, default = '',
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Param 0', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            's_param1', 'string', length = 512, default = '',
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Param 1', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            's_param2', 'string', length = 512, default = '',
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Param 2', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            's_param3', 'string', length = 512, default = '',
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Param 3', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'dt_param4', type = FIELD_UTC_DATETIME, default = None,
            required = False, requires = IS_NULL_OR(IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME)),
            notnull = False,
            widget = stv_widget_inputDateTime, label = 'Param 4', comment = '',
            writable = False, readable = True,
            represent = stv_represent_datetime
            ),
        Field(
            'dt_param5', type = FIELD_UTC_DATETIME, default = None,
            required = False, requires = IS_NULL_OR(IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME)),
            notnull = False,
            widget = stv_widget_inputDateTime, label = 'Param 5', comment = '',
            writable = False, readable = True,
            represent = stv_represent_datetime
            ),
        Field(
            's_param6', 'string', length = 512, default = '',
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Param 6', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            's_param7', 'string', length = 512, default = '',
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Param 7', comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'rastro', 'text', default = '',
            required = False,
            notnull = False,
            widget = stv_widget_text, label = 'Log', comment = "Log de proceso de la petición",
            writable = False, readable = True,
            represent = stv_represent_text
            ),

        Field(
            'ultimo_funcionalidad', 'integer', default = stvfwk2_e_RETURN.OK,
            required = False, requires = IS_IN_SET(
                E_FUNCIONALIDAD.GET_DICT(), zero = 0, error_message = 'Selecciona'
                ),
            notnull = False, unique = False,
            widget = stv_widget_combobox, label = 'Última funcionalidad', comment = '',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, TSERVIDOR_INSTRUCCIONES.E_FUNCIONALIDAD.GET_DICT())
            ),
        Field(
            'ultimo_resultado', 'integer', default = stvfwk2_e_RETURN.PENDIENTE,
            required = False, requires = IS_IN_SET(stvfwk2_e_RETURN.GET_DICT(), zero = 0, error_message = 'Selecciona'),
            notnull = False, unique = False,
            widget = stv_widget_combobox, label = 'Último resultado', comment = '',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, stvfwk2_e_RETURN.GET_DICT())
            ),
        Field(
            'ultimo_mensaje', 'string', length = 200, default = '',
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Último mensaje', comment = None,
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'ultimo_jsonparams', 'text', default = '',
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_text, label = 'Último jsonparams', comment = None,
            writable = False, readable = True,
            represent = stv_represent_text
            ),
        Field(
            'ultimo_fecha', type = FIELD_UTC_DATETIME, default = request.browsernow,
            required = True, requires = IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME),
            notnull = True,
            widget = stv_widget_inputDateTime, label = 'Fecha resultado', comment = '',
            writable = False, readable = True,
            represent = stv_represent_datetime
            ),

        Field(
            'contador_errores', 'integer', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputInteger, label = 'Errores seguidos', comment = '',
            writable = False, readable = True,
            represent = stv_represent_number
            ),
        Field(
            'numoperaciones', 'integer', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputInteger, label = 'Número operaciones', comment = '',
            writable = False, readable = True,
            represent = stv_represent_number
            ),

        Field(
            'es_autoactualizable', 'boolean', default = False,
            required = False,
            notnull = False,
            widget = stv_widget_inputCheckbox, label = 'Auto-actualizable',
            comment = 'Especifica si la instrucción se actualiza su fecha final cuando se requiera actualizar '
                      'la instrucción de forma automática. Usada para ACTUALIZAR_CFDIS y ACTUALIZAR_CFDIS_CANCELADOS',
            writable = False, readable = True,
            represent = stv_represent_boolean
            ),

        tSignature,
        ]

    def __init__(self, dbRef, tablename, *fields, **args):
        Table.__init__(self, dbRef, tablename, *fields, **args)
        self.PROC.dbTabla = self
        return

    class PROC:

        dbTabla = None  # Se escribe con la inicialización

        def __init__(self, **D_defaults):
            """ Inicializa objecto de tipo PROC para control del registro actual

            @param D_defaults:
            @type D_defaults:
            """
            self._D_defaults = Storage(D_defaults)
            self._L_requeridos = ['servidor_id', 'user_id']
            self.dbTabla.notas.readable = True
            return

        def puede_crear(self, **D_defaults):
            """ Determina si puede o no crear un registro.

            Actualmente no hay condición requerida.
            """
            _D_return = FUNC_RETURN()

            self._D_defaults.update(D_defaults or {})

            for _s_requerido in self._L_requeridos:
                if _s_requerido not in self._D_defaults:
                    _D_return.agrega_error(
                        "Campo {campo} es requerido para generar {tabla}".format(
                            campo = _s_requerido,
                            tabla = self.dbTabla._singular
                            )
                        )

                else:
                    pass

            return _D_return

        @classmethod
        def PUEDE_EDITAR(cls, x_instruccion):
            """ Regresa OK si el estado permite modificar, de lo contrario False

            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_instruccion, cls.dbTabla)
            _dbRow = _D_results.dbRow

            if not _dbRow:
                _D_return.agrega_error("{tabla} no identificado.".format(tabla = cls.dbTabla._singular))

            elif _dbRow.estado == TSERVIDOR_INSTRUCCIONES.E_ESTADO.EN_PROCESO:

                if _dbRow.editado_en < (request.browsernow - datetime.timedelta(minutes = 5)):
                    _dbRow.estado = TSERVIDOR_INSTRUCCIONES.E_ESTADO.TIMEOUT
                    _dbRow.update_record()
                else:
                    _D_return.agrega_error("Operación pendiente, no puede editarse hasta timeout")

            elif _dbRow.estado == TSERVIDOR_INSTRUCCIONES.E_ESTADO.TERMINADA:
                _D_return.agrega_error("Operación terminada, no puede editarse. Crear una nueva.")

            else:
                pass

            return _D_return

        @classmethod
        def PUEDE_ELIMINAR(cls, x_instruccion):
            """ Determina si puede o no eliminar un registro

            Misma condición que puede_editar, a lo mejor pudieran verse condiciones distintas

            """

            return cls.PUEDE_EDITAR(x_instruccion)

        class ConfigurarCampos_instruccion:

            def __init__(self, dbTabla):
                self.dbTabla = dbTabla
                return

            def nada(self, b_writable = False):
                self.dbTabla.cuenta_id.writable = b_writable
                self.dbTabla.empresa_id.writable = b_writable
                self.dbTabla.tipoinstruccion.writable = b_writable
                self.dbTabla.s_param0.label = 'P0:No aplica'
                self.dbTabla.s_param0.writable = b_writable
                self.dbTabla.s_param1.label = 'P1:No aplica'
                self.dbTabla.s_param1.writable = b_writable
                self.dbTabla.s_param2.label = 'P2:No aplica'
                self.dbTabla.s_param2.writable = b_writable
                self.dbTabla.s_param3.label = 'P3:No aplica'
                self.dbTabla.s_param3.writable = b_writable
                self.dbTabla.dt_param4.label = 'P4:No aplica'
                self.dbTabla.dt_param4.writable = b_writable
                self.dbTabla.dt_param5.label = 'P5:No aplica'
                self.dbTabla.dt_param5.writable = b_writable
                self.dbTabla.s_param6.label = 'P6:No aplica'
                self.dbTabla.s_param6.writable = b_writable
                self.dbTabla.s_param7.label = 'P7:No aplica'
                self.dbTabla.s_param7.writable = b_writable

                return

            def param0_comoCuenta(self, b_writable = False):
                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.s_param0,
                    requires = IS_NULL_OR(IS_IN_DB(db, 'tcuentas.id', db.tcuentas._format)),
                    widget = lambda f, v: stv_widget_db_chosen(f, v, 1),
                    label = 'P0: Cuenta', comment = None,
                    writable = b_writable,
                    represent = lambda v, r: stv_represent_referencefield(v, r, self.dbTabla.s_param0)
                    )
                return

            def param1y2_comoEmpresa(self, b_writable = False):
                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.s_param1,
                    requires = IS_IN_DB(db, 'tcuenta_empresas.empresa_id', db.tcuenta_empresas._format),
                    widget = lambda field, value: stv_widget_db_chosen_linked(
                        field, value, maxselect = 1,
                        filterFieldName = 's_param0',
                        linkedUrl = URL(a = 'backoffice', c = 'cuentas', f = 'cuenta_empresas_ver.json'),
                        D_additionalAttributes = Storage(
                            linkedTableFieldName = 'cuenta_id'
                            )
                        ),
                    label = 'P1: Empresa', comment = None,
                    writable = b_writable,
                    represent = lambda v, r: stv_represent_referencefield(v, r, self.dbTabla.empresa_id)
                    )

                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.s_param2, default = 'Autollenado',
                    widget = stv_widget_input,
                    label = 'P2: RFC',
                    comment = 'Se llenará de forma automática con el RFC de la empresa seleccionada',
                    )
                return

            def actualizar(self, b_writable = False):
                self.dbTabla.cuenta_id.default = None
                self.dbTabla.cuenta_id.writable = False
                self.dbTabla.empresa_id.default = None
                self.dbTabla.empresa_id.writable = False
                self.dbTabla.tipoinstruccion.writable = False

                self.param0_comoCuenta(b_writable)
                self.param1y2_comoEmpresa(b_writable)

                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.s_param3, default = 'Autollenado',
                    label = 'P3: Clave CIEC',
                    comment = 'Se llenará de forma automática con clave CIEC de la empresa seleccionada',
                    )
                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.dt_param4,
                    default = request.browsernow.replace(day = 1, hour = 0, minute = 0, second = 0, microsecond = 0)
                    if not self.dbTabla.dt_param4.default else None,  # Actualiza el default si no existe
                    required = True,
                    label = 'P4: Desde', comment = None,
                    writable = b_writable
                    )
                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.dt_param5,
                    default = request.browsernow
                    if not self.dbTabla.dt_param5.default else None,  # Actualiza el default si no existe
                    required = True,
                    label = 'P5: Hasta', comment = None,
                    writable = b_writable
                    )
                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.s_param6,
                    default = 'Autollenado',
                    label = 'P6: ID Integrador',
                    comment = 'Se llenará de forma automática con la configuración de la empresa seleccionada',
                    )
                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.s_param7,
                    default = 'Autollenado',
                    label = 'P7: RFC Integrador',
                    comment = 'Se llenará de forma automática con la configuración de la empresa seleccionada',
                    )

                return

            def borrar_subidos(self, b_writable = False):
                self.dbTabla.cuenta_id.default = None
                self.dbTabla.cuenta_id.writable = False
                self.dbTabla.empresa_id.default = None
                self.dbTabla.empresa_id.writable = False
                self.dbTabla.tipoinstruccion.writable = False

                self.param0_comoCuenta(b_writable)
                self.param1y2_comoEmpresa(b_writable)

                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.s_param3, default = '',
                    label = 'P3: Sin Usar', comment = None,
                    )
                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.dt_param4,
                    default = request.browsernow.replace(day = 1, hour = 0, minute = 0, second = 0, microsecond = 0)
                    if not self.dbTabla.dt_param4.default else None,  # Actualiza el default si no existe
                    required = True,
                    label = 'P4: Desde', comment = None,
                    writable = b_writable
                    )
                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.dt_param5,
                    default = request.browsernow
                    if not self.dbTabla.dt_param5.default else None,  # Actualiza el default si no existe
                    required = True,
                    label = 'P5: Hasta', comment = None,
                    writable = b_writable
                    )
                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.s_param6, default = '',
                    label = 'P6: Sin Usar', comment = None,
                    )
                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.s_param7, default = '',
                    label = 'P7: Sin Usar', comment = None,
                    )

                return

            def actualizar_archivoEmpresa(self, b_writable = False):
                """ Función cuando se requiere actualizar un archivo como la licencia, certificado o key por empresa

                @param b_writable:
                @type b_writable:
                @return:
                @rtype:
                """
                self.dbTabla.cuenta_id.default = None
                self.dbTabla.cuenta_id.writable = False
                self.dbTabla.empresa_id.default = None
                self.dbTabla.empresa_id.writable = False
                self.dbTabla.tipoinstruccion.writable = False

                self.param0_comoCuenta(b_writable)
                self.param1y2_comoEmpresa(b_writable)

                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.s_param3, default = 'Autollenado',
                    label = 'P3: URL',
                    comment = 'URL se generará al momento de grabar',
                    )
                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.dt_param4, default = None,
                    label = 'P4: Sin Usar', comment = None,
                    )
                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.dt_param5,  default = None,
                    label = 'P5: Sin Usar', comment = None,
                    )
                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.s_param6, default = '',
                    label = 'P6: Sin Usar', comment = None,
                    )
                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.s_param7, default = '',
                    label = 'P7: Sin Usar', comment = None,
                    )

                return

            def actualizar_archivoServidor(self):
                self.dbTabla.cuenta_id.default = None
                self.dbTabla.cuenta_id.writable = False
                self.dbTabla.empresa_id.default = None
                self.dbTabla.empresa_id.writable = False
                self.dbTabla.tipoinstruccion.writable = False

                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.s_param0, default = '',
                    label = 'P0: Sin Usar', comment = None,
                    )
                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.s_param1, default = '',
                    label = 'P1: Sin Usar', comment = None,
                    )
                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.s_param2, default = '',
                    label = 'P2: Sin Usar', comment = None,
                    )
                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.s_param3, default = 'Autollenado',
                    widget = stv_widget_input,
                    label = 'P3: URL',
                    comment = 'URL se generará al momento de grabar',
                    )
                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.dt_param4, default = None,
                    label = 'P4: Sin Usar', comment = None,
                    )
                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.dt_param5, default = None,
                    label = 'P5: Sin Usar', comment = None,
                    )
                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.s_param6, default = '',
                    label = 'P6: Sin Usar', comment = None,
                    )
                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.s_param7, default = '',
                    label = 'P7: Sin Usar', comment = None,
                    )

                return

            def actualizar_configurar(self, b_writable = False):
                """

                @return:
                @rtype:
                """
                self.dbTabla.cuenta_id.default = None
                self.dbTabla.cuenta_id.writable = False
                self.dbTabla.empresa_id.default = None
                self.dbTabla.empresa_id.writable = False
                self.dbTabla.tipoinstruccion.writable = False

                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.s_param0, default = '',
                    label = 'P0: Sin Usar', comment = None,
                    )
                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.s_param1,
                    label = 'P1: Valor de configuración', comment = None,
                    writable = b_writable
                    )
                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.s_param2, default = '',
                    label = 'P2: Sin Usar', comment = None,
                    )
                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.s_param3, default = '',
                    label = 'P3: Sin Usar', comment = None,
                    )
                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.dt_param4, default = None,
                    label = 'P4: Sin Usar', comment = None,
                    )
                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.dt_param5, default = None,
                    label = 'P5: Sin Usar', comment = None,
                    )
                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.s_param6, default = '',
                    label = 'P6: Sin Usar', comment = None,
                    )
                STV_LIB_DB.CONFIGURAR_CAMPO(
                    dbField = self.dbTabla.s_param7, default = '',
                    label = 'P7: Sin Usar', comment = None,
                    )

                return

            pass  # ConfigirarCampos_instruccion

        def configuracampos_nuevo(
                self,
                s_instruccion_id,
                E_tipoInstruccion,
                b_llamadaDesdeEditar = False,
                b_writable = True,
                **D_defaults
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param b_writable:
            @type b_writable:
            @param E_tipoInstruccion:
            @type E_tipoInstruccion:
            @param b_llamadaDesdeEditar:
            @type b_llamadaDesdeEditar:
            @param s_instruccion_id:
            @type s_instruccion_id:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            self._D_defaults.update(D_defaults or {})
            for _s_fieldName in self.dbTabla.fields:
                self.dbTabla[_s_fieldName].writable = False
            for _s_fieldName in self._D_defaults:
                self.dbTabla[_s_fieldName].default = self._D_defaults[_s_fieldName]

            if not b_llamadaDesdeEditar and b_writable:
                # Si se esta llamando desde editar o si se desea escribir información
                _D_results = self.puede_crear()
            else:
                _D_results = _D_return

            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.combinar(_D_results)

            elif s_instruccion_id:
                _dbRow_base = self.dbTabla(s_instruccion_id)

                # self.dbTabla.servidor_id.default = _dbRow_base.servidor_id
                self.dbTabla.cuenta_id.default = _dbRow_base.cuenta_id
                self.dbTabla.empresa_id.default = _dbRow_base.empresa_id
                # self.dbTabla.user_id.default = _dbRow_base.user_id
                # self.dbTabla.fecha.default = _dbRow_base.fecha
                self.dbTabla.tipoinstruccion.default = _dbRow_base.tipoinstruccion
                self.dbTabla.servicio.default = _dbRow_base.servicio
                # self.dbTabla.estado.default = _dbRow_base.estado
                self.dbTabla.s_param0.default = _dbRow_base.s_param0
                self.dbTabla.s_param1.default = _dbRow_base.s_param1
                self.dbTabla.s_param2.default = _dbRow_base.s_param2
                self.dbTabla.s_param3.default = _dbRow_base.s_param3
                self.dbTabla.dt_param4.default = _dbRow_base.dt_param4
                self.dbTabla.dt_param5.default = _dbRow_base.dt_param5
                self.dbTabla.s_param6.default = _dbRow_base.s_param6
                self.dbTabla.s_param7.default = _dbRow_base.s_param7
                # self.dbTabla.rastro.default = _dbRow_base.rastro
                # self.dbTabla.ultimo_funcionalidad.default = _dbRow_base.ultimo_funcionalidad
                # self.dbTabla.ultimo_resultado.default = _dbRow_base.ultimo_resultado
                # self.dbTabla.ultimo_mensaje.default = _dbRow_base.ultimo_mensaje
                # self.dbTabla.ultimo_jsonparams.default = _dbRow_base.ultimo_jsonparams
                # self.dbTabla.ultimo_fecha.default = _dbRow_base.ultimo_fecha
                # self.dbTabla.contador_errores.default = _dbRow_base.contador_errores
                # self.dbTabla.numoperaciones.default = _dbRow_base.numoperaciones
                # self.dbTabla.es_autoactualizable.default = _dbRow_base.es_autoactualizable

            else:
                pass

            self.dbTabla.servidor_id.writable = False
            self.dbTabla.user_id.writable = False
            self.dbTabla.fecha.writable = b_writable
            self.dbTabla.servicio.writable = b_writable
            self.dbTabla.servidor_id.writable = b_writable

            # self.dbTabla.cuenta_id.writable = b_writable
            # self.dbTabla.empresa_id.writable = b_writable
            # self.dbTabla.tipoinstruccion.writable = False
            # self.dbTabla.s_param0.writable = b_writable
            # self.dbTabla.s_param1.writable = b_writable
            # self.dbTabla.s_param2.writable = b_writable
            # self.dbTabla.s_param3.writable = b_writable
            # self.dbTabla.dt_param4.writable = b_writable
            # self.dbTabla.dt_param5.writable = b_writable
            # self.dbTabla.s_param6.writable = b_writable
            # self.dbTabla.s_param7.writable = b_writable
            _O_configurar = self.ConfigurarCampos_instruccion(self.dbTabla)
            if not E_tipoInstruccion or (E_tipoInstruccion == TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.NADA):
                _O_configurar.nada(b_writable = b_writable)

            elif E_tipoInstruccion in (
                    TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_CFDIS,
                    TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_CFDISRX,
                    TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_CFDIS_CANCELADOS,
                    TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_CFDISRX_CANCELADOS,
                    ):
                _O_configurar.actualizar(b_writable = b_writable)

            elif E_tipoInstruccion == TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.BORRAR_CFDIS_SUBIDOS:
                _O_configurar.borrar_subidos(b_writable = b_writable)

            elif E_tipoInstruccion in (
                    TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_LICENCIA,
                    TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_CER,
                    TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_KEY,
                    ):
                _O_configurar.actualizar_archivoEmpresa(b_writable = b_writable)

            elif E_tipoInstruccion in (
                    TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_LIBRERIA_CFDIS,
                    TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_GESTIONADOR
                    ):
                _O_configurar.actualizar_archivoServidor()

            elif E_tipoInstruccion in (
                    TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.CONFIGURAR_URL,
                    TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.CONFIGURAR_EMAIL,
                    TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.CONFIGURAR_PASS,
                    TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.CONFIGURAR_FRECUENCIA_MIN,
                    ):
                _O_configurar.actualizar_configurar(b_writable = b_writable)

            else:
                pass

            # Campos que no deben ser editados, solo de forma autocalculada
            self.dbTabla.es_autoactualizable.writable = False
            self.dbTabla.estado.writable = False
            self.dbTabla.rastro.writable = False
            self.dbTabla.ultimo_funcionalidad.writable = False
            self.dbTabla.ultimo_resultado.writable = False
            self.dbTabla.ultimo_mensaje.writable = False
            self.dbTabla.ultimo_jsonparams.writable = False
            self.dbTabla.ultimo_fecha.writable = False
            self.dbTabla.contador_errores.writable = False
            self.dbTabla.numoperaciones.writable = False

            # Campos que todos los movimientos pueden editar
            self.dbTabla.notas.writable = True

            # Se definen los valores por default obligados
            self.dbTabla.servidor_id.default = self._D_defaults.servidor_id
            self.dbTabla.user_id.default = self._D_defaults.user_id
            self.dbTabla.tipoinstruccion.default = E_tipoInstruccion

            return _D_return

        def configuracampos_edicion(
                self,
                x_instruccion,
                ):
            """ Dependiendo del movimiento y las condiciones, configura los campos que se pueden editar

            @param x_instruccion: es el registro del movimiento o su id
            @type x_instruccion:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_instruccion, self.dbTabla)
            _dbRow = _D_results.dbRow

            # Se llenan los default obligados
            for _s_nombreCampo in self._L_requeridos:
                self._D_defaults[_s_nombreCampo] = _D_results.dbRow[_s_nombreCampo]

            _D_results = self.PUEDE_EDITAR(_dbRow)

            if _D_results.E_return == CLASS_e_RETURN.OK:
                # Si puede editar regresa un ok y el movimiento esta abierto

                # Se manda vacío el argumento para no agregar defaults a los campos
                _D_results = self.configuracampos_nuevo(
                    None,
                    E_tipoInstruccion = _dbRow.tipoinstruccion,
                    b_llamadaDesdeEditar = True,
                    **self._D_defaults
                    )
                _D_return.combinar(_D_results)

                self.dbTabla.estado.writable = True
                self.dbTabla.notas.writable = True

            else:
                # De lo contrario en cualquier otro caso, solo se pueden editar las observaciones
                for _s_fieldName in self.dbTabla.fields:
                    self.dbTabla[_s_fieldName].writable = False

                self.dbTabla.notas.writable = True

            return _D_return

        def al_validar(
                self,
                O_form,
                ):
            """ Validar la información para poder grabar

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _dbRow = self.dbTabla(O_form.record_id) if O_form.record_id else None
            _D_camposAChecar = STV_FWK_LIB.OBTENER_CAMPOSACHECAR_PARA_VALIDAR(self.dbTabla, _dbRow, O_form)

            if not O_form.record_id:
                # Se esta creando el registro

                _D_results = self.puede_crear(**_D_camposAChecar)
                if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                    O_form.errors.id = _D_results.s_msgError
                else:
                    pass

            else:
                # Se esta modificando el registro, la llamada a configuraCampos_edicion ya definió con writable
                #  que puede modificarse y que no. Por lo que el código para verificar si puede o no editar
                #  no es requerido en este caso.
                pass

            # Validaciones

            if not _D_camposAChecar.tipoinstruccion:
                O_form.errors.tipoinstruccion = "Tipo instrucción debe ser especificada"

            elif _D_camposAChecar.tipoinstruccion in (
                    TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_CFDIS,
                    TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_CFDISRX,
                    TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_CFDIS_CANCELADOS,
                    TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_CFDISRX_CANCELADOS,
                    TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.BORRAR_CFDIS_SUBIDOS,
                    TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_LICENCIA,
                    TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_CER,
                    TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_KEY,
                    ):
                if not O_form.vars.s_param0:
                    O_form.errors.s_param0 = "Es necesario especificar la cuenta"

                elif not O_form.vars.s_param1:
                    O_form.errors.s_param1 = "Es necesario especificar la empresa"

                else:
                    pass

            else:
                pass

            return O_form

        @classmethod
        def AL_ACEPTAR(
                cls,
                O_form,
                ):
            """ Aceptación del registro

            Se hacen todos los cálculos y aceptación de los cambios en el producto

            @param O_form:
            @type O_form:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()
            dbRow = cls.dbTabla(O_form.record_id or O_form.vars.id)

            if not O_form.record_id:
                # Si se inserta
                pass

            else:  # Si se edita
                pass

            _D_results = cls.MAPEAR_CAMPOS(dbRow)
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                _D_return.combinar(_D_results)

            else:
                _D_results = cls.CALCULAR_CAMPOS(dbRow)
                _D_return.combinar(_D_results)

            generaMensajeFlash(cls.dbTabla, _D_return)
            return O_form

        @classmethod
        def ANTES_ELIMINAR(
                cls,
                L_errorsAction,
                dbRow
                ):
            """ Antes de eliminar un movimiento de inventario de este tipo

            @param dbRow:
            @type dbRow:
            @param L_errorsAction:
            @type L_errorsAction:
            @return: errores
            @rtype: lista de diccionarios {'msg':'...'}
            """
            _D_results = cls.PUEDE_ELIMINAR(dbRow)
            if _D_results.E_return > CLASS_e_RETURN.OK_WARNING:
                L_errorsAction.append({'msg': _D_results.s_msgError})
            else:
                cls._dbRow_aEliminar = Storage(dbRow.as_dict())

            return L_errorsAction

        @classmethod
        def DESPUES_ELIMINAR(
                cls,
                L_errorsAction,
                ):
            """ Después de eliminar un registro

            @param L_errorsAction:
            @type L_errorsAction:
            @return: errores
            @rtype: lista de diccionarios {'msg':'...'}
            """
            _D_return = FUNC_RETURN()

            if cls._dbRow_aEliminar:
                pass
            else:
                _D_return.E_return = CLASS_e_RETURN.NOK_SECUENCIA
                _D_return.s_msgError += "Error en secuencia de llamado de funciones para borrado. "

            generaMensajeFlash(cls.dbTabla, _D_return)
            return L_errorsAction

        @classmethod
        def MAPEAR_CAMPOS(
                cls,
                x_servidor,
                ):
            """ Remapea campos solamente cuando se graba el registro

            @param x_servidor:
            @type x_servidor:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_servidor, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage(
                cuenta_id = _dbRow.cuenta_id,
                empresa_id = _dbRow.empresa_id,
                s_param2 = _dbRow.s_param2,
                s_param3 = _dbRow.s_param3,
                s_param6 = _dbRow.s_param6,
                s_param7 = _dbRow.s_param7,
                )

            if _dbRow.tipoinstruccion in (
                    TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_CFDIS,
                    TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_CFDISRX,
                    TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_CFDIS_CANCELADOS,
                    TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_CFDISRX_CANCELADOS,
                    TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.BORRAR_CFDIS_SUBIDOS,
                    TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_LICENCIA,
                    TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_CER,
                    TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_KEY,
                    ):

                _D_actualizar.cuenta_id = _dbRow.s_param0
                _D_actualizar.empresa_id = _dbRow.s_param1
                _dbRow_cuenta = db.tcuentas(_D_actualizar.cuenta_id)

                if 'dbc01' not in globals():
                    importar_definiciones_aplicacion()
                    importar_dbs_genericos()
                else:
                    pass
                importar_dbs_cuenta(s_remplaza_cuenta_id = _D_actualizar.cuenta_id)

                _dbRow_empresa = dbc01.tempresas(_D_actualizar.empresa_id)
                _D_actualizar.s_param2 = _dbRow_empresa.rfc

                if _dbRow.tipoinstruccion in (
                        TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_CFDIS,
                        TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_CFDISRX,
                        TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_CFDIS_CANCELADOS,
                        TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_CFDISRX_CANCELADOS
                        ):
                    _D_actualizar.s_param3 = _dbRow_empresa.claveciec
                    _D_actualizar.s_param6 = _dbRow_cuenta.idintegrador
                    _D_actualizar.s_param7 = _dbRow_cuenta.rfcintegrador

                elif _dbRow.tipoinstruccion == TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_LICENCIA:
                    _D_actualizar.s_param3 = URL(
                        a = D_stvFwkCfg.s_nameBackofficeApp, c = 'cuentas',
                        f = 'servidor_instruccion_archivo',
                        args = [_dbRow_cuenta.archivolicencia],
                        vars = Storage(
                            filename = _dbRow_cuenta.archivolicencianombre
                            ),
                        scheme='http', host=True
                        )

                elif _dbRow.tipoinstruccion == TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_CER:
                    _dbRows_certificados = db(
                        (db.tcuenta_empresas.cuenta_id == _D_actualizar.cuenta_id)
                        & (db.tcuenta_empresas.empresa_id == _D_actualizar.empresa_id)
                        & (db.tcuenta_empresa_certificados.cuenta_empresa_id == db.tcuenta_empresas.id)
                        & (db.tcuenta_empresa_certificados.vigencia_cer >= request.browsernow)
                        ).select(
                            db.tcuenta_empresa_certificados.ALL,
                            orderby = (
                                ~db.tcuenta_empresa_certificados.vigencia_cer
                                )
                            )

                    if _dbRows_certificados:
                        _D_actualizar.s_param3 = URL(
                            a = D_stvFwkCfg.s_nameBackofficeApp, c = 'cuentas',
                            f = 'servidor_instruccion_archivo',
                            args = [_dbRows_certificados.first().archivo_cer],
                            vars = Storage(
                                filename = _dbRows_certificados.first().archivo_cer_nombre
                                ),
                            scheme = 'http', host = True
                            )
                    else:
                        _D_return.agrega_error("No existe archivo de certificado vigente para la empresa")

                elif _dbRow.tipoinstruccion == TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_KEY:
                    _dbRows_firmas = db(
                        (db.tcuenta_empresas.cuenta_id == _D_actualizar.cuenta_id)
                        & (db.tcuenta_empresas.empresa_id == _D_actualizar.empresa_id)
                        & (db.tcuenta_empresa_firmas.cuenta_empresa_id == db.tcuenta_empresas.id)
                        & (db.tcuenta_empresa_firmas.vigencia_firma >= request.browsernow)
                        ).select(
                            db.tcuenta_empresa_firmas.ALL,
                            orderby = (
                                ~db.tcuenta_empresa_firmas.vigencia_firma
                                )
                            )

                    if _dbRows_firmas:
                        _D_actualizar.s_param3 = URL(
                            a = D_stvFwkCfg.s_nameBackofficeApp, c = 'cuentas',
                            f = 'servidor_instruccion_archivo',
                            args = [_dbRows_firmas.first().archivo_firma],
                            vars = Storage(
                                filename = _dbRows_firmas.first().archivo_firma_nombre
                                ),
                            scheme = 'http', host = True
                            )
                    else:
                        _D_return.agrega_error("No existe archivo de firma electrónica vigente para la empresa")

                else:
                    pass

            elif _dbRow.tipoinstruccion == TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_LIBRERIA_CFDIS:

                _dbRow_servidor = db.tservidores(_dbRow.servidor_id)

                _D_actualizar.s_param3 = URL(
                    a = D_stvFwkCfg.s_nameBackofficeApp, c = 'cuentas',
                    f = 'servidor_instruccion_archivo',
                    args = [_dbRow_servidor.archivolibreria],
                    vars = Storage(
                        filename = _dbRow_servidor.archivolibrerianombre
                        ),
                    scheme='http', host=True
                    )

            elif _dbRow.tipoinstruccion == TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.ACTUALIZAR_GESTIONADOR:

                _dbRow_servidor = db.tservidores(_dbRow.servidor_id)

                _D_actualizar.s_param3 = URL(
                    a = D_stvFwkCfg.s_nameBackofficeApp,
                    c = 'cuentas',
                    f = 'servidor_instruccion_archivo',
                    args = [_dbRow_servidor.archivogestionador],
                    vars = Storage(
                        filename = _dbRow_servidor.archivogestionadornombre
                        ),
                    scheme='http', host=True
                    )

            else:
                pass

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)
                    break
                else:
                    pass

            return _D_return

        @classmethod
        def CALCULAR_CAMPOS(
                cls,
                x_servidor,
                ):
            """ Recalcula campos que tienen impacto cuando otros registros de otras tablas cambian
            Esta operación así mismo detecta y agrega las inconsistencias

            @param x_servidor:
            @type x_servidor:
            @return:
            @rtype:
            """
            _D_return = FUNC_RETURN()

            _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_servidor, cls.dbTabla)
            _dbRow = _D_results.dbRow

            _D_actualizar = Storage()

            for _s_nombreCampo in _D_actualizar:
                if _D_actualizar[_s_nombreCampo] != _dbRow[_s_nombreCampo]:
                    _dbRow.update_record(**_D_actualizar)
                    break
                else:
                    pass

            return _D_return

    @classmethod
    def AGREGAR_TIMBRAR_CANCELAR_CFDI(
            cls,
            s_cuenta_id,
            dbRow_cfdi,
            E_tipoInstruccion,
            s_rastro = "",
            ):
        """ Crea la instrucción para timbrar o cancelar un CFDI en el servidor Gestionador.
        
        
        Es importante que el registro del CFDI se encuentre en estado de EN_PROCESO_FIRMA o EN_PROCESO_CANCELACION
        antes de llamar a esta función.
        
        """
        
        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = "",
            )

        # Verificar si ya existe la cuenta en alguna variable, para no hacer la consulta nuevamente
        _dbRow_cuenta = db.tcuentas(s_cuenta_id)
        if E_tipoInstruccion not in (
                TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.TIMBRAR_CFDI,
                TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.CANCELAR_CFDI
                ):
            _D_return.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
            _D_return.s_msgError = "La instrucción para crear CFDI no es válida en esta función"            
        
        elif not _dbRow_cuenta:
            _D_return.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
            _D_return.s_msgError = "La cuenta no fue encontrada"                        
        
        elif not _dbRow_cuenta.servidorgestionador_id:
            _D_return.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
            _D_return.s_msgError = "No existe servidor gestionador asociado con la cuenta"                        
            
        else:
            
            _dbRow_cfdi = dbRow_cfdi
            
            if not _dbRow_cfdi:
                _D_return.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
                _D_return.s_msgError = "El CFDI no existe"
                
            elif (E_tipoInstruccion == TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.TIMBRAR_CFDI) \
                    and (_dbRow_cfdi.estado != TCFDIS.E_ESTADO.EN_PROCESO_FIRMA):
                _D_return.E_return = stvfwk2_e_RETURN.NOK_CONDICIONES_INCORRECTAS
                _D_return.s_msgError = "El CFDI no se encuentra en estado para timbrar"
            
            elif (E_tipoInstruccion == TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.CANCELAR_CFDI) \
                    and (_dbRow_cfdi.estado != TCFDIS.E_ESTADO.EN_PROCESO_CANCELACION):
                _D_return.E_return = stvfwk2_e_RETURN.NOK_CONDICIONES_INCORRECTAS
                _D_return.s_msgError = "El CFDI no se encuentra en estado para cancelar"
                
            else:
                _dbRows_instrucciones = db(
                    (db.tservidor_instrucciones.cuenta_id == s_cuenta_id)
                    & (db.tservidor_instrucciones.estado.belongs(TSERVIDOR_INSTRUCCIONES.E_ESTADO.L_ACTIVOS))
                    & (db.tservidor_instrucciones.tipoinstruccion == E_tipoInstruccion)
                    & (db.tservidor_instrucciones.s_param3 == _dbRow_cfdi.id)
                    ).select(
                        db.tservidor_instrucciones.ALL,
                        )
                            
                if _dbRows_instrucciones:
                    _D_return.E_return = stvfwk2_e_RETURN.NOK_CONDICIONES_INCORRECTAS
                    _D_return.s_msgError = "El CFDI ya se encuentra en proceso"
                    
                else:
                    
                    _D_instruccionRow = Storage(
                        servidor_id = _dbRow_cuenta.servidorgestionador_id,
                        cuenta_id = _dbRow_cuenta.id,
                        empresa_id = _dbRow_cfdi.empresa_id,
                        user_id = auth.user_id,
                        fecha = request.browsernow,
                        tipoinstruccion = E_tipoInstruccion,
                        servicio = TSERVIDOR_INSTRUCCIONES.E_SERVICIO.GESTIONADOR,
                        estado = TSERVIDOR_INSTRUCCIONES.E_ESTADO.PENDIENTE,
                        s_param0 = _dbRow_cuenta.id,
                        s_param1 = _dbRow_cfdi.empresa_id,
                        s_param2 = _dbRow_cfdi.emisorrfc,
                        s_param3 = _dbRow_cfdi.id,
                        dt_param4 = None,
                        dt_param5 = None,
                        # s_param6 = Depende del tipo de instrucción,
                        s_param7 = "",
                        rastro = request.browsernow.isoformat() + "UTC: " + s_rastro + "\n",
                        ultimo_resultado = None,
                        ultimo_mensaje = "",
                        contador_errores = 0,
                        es_autoactualizable = False,
                        creado_por = auth.user_id,
                        editado_por = auth.user_id
                        )
                    
                    if E_tipoInstruccion == TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.TIMBRAR_CFDI:
                        _D_instruccionRow.s_param6 = URL(
                                a = D_stvFwkCfg.s_nameAccesoApp, 
                                c = 'default', 
                                f = 'servidor_instruccion_cfdi', 
                                args = [TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.TIMBRAR_CFDI],
                                vars = Storage(
                                    s_cuenta_id = _dbRow_cuenta.id,
                                    s_cfdi_id = _dbRow_cfdi.id
                                    ),
                                scheme='http', host=True
                                )

                    elif TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.CANCELAR_CFDI:
                        _D_instruccionRow.s_param6 = _dbRow_cfdi.uuid
                        
                    else:
                        # No debe de pasar a este punto nunca
                        raise ValueError("Invalid Path")
                        pass
                    
                    _D_instruccionRow.s_id = db.tservidor_instrucciones.insert(**_D_instruccionRow)
                    
                    if E_tipoInstruccion == TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.TIMBRAR_CFDI:
                        # En el caso de timbrado, es necesario mandar el JSON del CFDI
                        _dbRow_instruccion = db.tservidor_instrucciones(_D_instruccionRow.s_id)
                        
                        _dbRow_instruccion.s_param6 += "&s_instruccion_id=" + str(_D_instruccionRow.s_id)
                        _dbRow_instruccion.update_record()
                        
                    else:
                        pass

        return _D_return

    @classmethod
    def AGREGAR_CANCELAR_CFDIRX(
            cls,
            s_cuenta_id,
            dbRow_cfdirx,
            E_tipoInstruccion,
            s_rastro = "",
            ):
        """ Crea la instrucción para timbrar o cancelar un CFDI en el servidor Gestionador.


        Es importante que el registro del CFDI se encuentre en estado de EN_PROCESO_FIRMA o EN_PROCESO_CANCELACION
        antes de llamar a esta función.

        """

        _D_return = FUNC_RETURN()

        # Verificar si ya existe la cuenta en alguna variable, para no hacer la consulta nuevamente
        _dbRow_cuenta = db.tcuentas(s_cuenta_id)
        if E_tipoInstruccion != TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.CANCELAR_CFDIRX:
            _D_return.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
            _D_return.s_msgError = "La instrucción para crear CFDI no es válida en esta función"

        elif not _dbRow_cuenta:
            _D_return.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
            _D_return.s_msgError = "La cuenta no fue encontrada"

        elif not _dbRow_cuenta.servidorgestionador_id:
            _D_return.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
            _D_return.s_msgError = "No existe servidor gestionador asociado con la cuenta"

        else:

            _dbRow_cfdirx = dbRow_cfdirx

            if not _dbRow_cfdirx:
                _D_return.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
                _D_return.s_msgError = "El CFDI no existe"

            # elif (E_tipoInstruccion == TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.TIMBRAR_CFDI) \
            #         and (_dbRow_cfdirx.estado != TCFDIS.E_ESTADO.EN_PROCESO_FIRMA):
            #     _D_return.E_return = stvfwk2_e_RETURN.NOK_CONDICIONES_INCORRECTAS
            #     _D_return.s_msgError = "El CFDI no se encuentra en estado para timbrar"

            elif (E_tipoInstruccion == TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.CANCELAR_CFDIRX) \
                    and (_dbRow_cfdirx.estado != TCFDISRX.E_ESTADO.EN_PROCESO_CANCELACION):
                # CFDI debe ser puesto antes en proceso cancelación para poder proceder a su cancelación
                _D_return.E_return = stvfwk2_e_RETURN.NOK_CONDICIONES_INCORRECTAS
                _D_return.s_msgError = "El CFDI no se encuentra en estado para cancelar"

            else:
                _dbRows_instrucciones = db(
                    (db.tservidor_instrucciones.cuenta_id == s_cuenta_id)
                    & (db.tservidor_instrucciones.estado.belongs(TSERVIDOR_INSTRUCCIONES.E_ESTADO.L_ACTIVOS))
                    & (db.tservidor_instrucciones.tipoinstruccion == E_tipoInstruccion)
                    & (db.tservidor_instrucciones.s_param3 == _dbRow_cfdirx.id)
                    ).select(
                    db.tservidor_instrucciones.ALL,
                    )

                if _dbRows_instrucciones:
                    _D_return.E_return = stvfwk2_e_RETURN.NOK_CONDICIONES_INCORRECTAS
                    _D_return.s_msgError = "El CFDI ya se encuentra en proceso"

                else:

                    _D_instruccionRow = Storage(
                        servidor_id = _dbRow_cuenta.servidorgestionador_id,
                        cuenta_id = _dbRow_cuenta.id,
                        empresa_id = _dbRow_cfdirx.empresa_id,
                        user_id = auth.user_id,
                        fecha = request.browsernow,
                        tipoinstruccion = E_tipoInstruccion,
                        servicio = TSERVIDOR_INSTRUCCIONES.E_SERVICIO.GESTIONADOR,
                        estado = TSERVIDOR_INSTRUCCIONES.E_ESTADO.PENDIENTE,
                        s_param0 = _dbRow_cuenta.id,
                        s_param1 = _dbRow_cfdirx.empresa_id,
                        s_param2 = _dbRow_cfdirx.emisorrfc,
                        s_param3 = _dbRow_cfdirx.id,
                        dt_param4 = None,
                        dt_param5 = None,
                        # s_param6 = Depende del tipo de instrucción,
                        s_param7 = "",
                        rastro = request.browsernow.isoformat() + "UTC: " + s_rastro + "\n",
                        ultimo_resultado = None,
                        ultimo_mensaje = "",
                        contador_errores = 0,
                        es_autoactualizable = False,
                        creado_por = auth.user_id,
                        editado_por = auth.user_id
                        )

                    # if E_tipoInstruccion == TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.TIMBRAR_CFDI:
                    #     _D_instruccionRow.s_param6 = URL(
                    #         a = D_stvFwkCfg.s_nameAccesoApp,
                    #         c = 'default',
                    #         f = 'servidor_instruccion_cfdi',
                    #         args = [TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.TIMBRAR_CFDI],
                    #         vars = Storage(
                    #             s_cuenta_id = _dbRow_cuenta.id,
                    #             s_cfdi_id = _dbRow_cfdirx.id
                    #             ),
                    #         scheme = 'http', host = True
                    #         )

                    if TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.CANCELAR_CFDIRX:
                        _D_instruccionRow.s_param6 = _dbRow_cfdirx.uuid

                    else:
                        # No debe de pasar a este punto nunca
                        raise ValueError("Invalid Path")
                        pass

                    _D_instruccionRow.s_id = db.tservidor_instrucciones.insert(**_D_instruccionRow)

                    # if E_tipoInstruccion == TSERVIDOR_INSTRUCCIONES.E_TIPOINSTRUCCION.TIMBRAR_CFDI:
                    #     # En el caso de timbrado, es necesario mandar el JSON del CFDI
                    #     _dbRow_instruccion = db.tservidor_instrucciones(_D_instruccionRow.s_id)
                    #
                    #     _dbRow_instruccion.s_param6 += "&s_instruccion_id=" + str(_D_instruccionRow.s_id)
                    #     _dbRow_instruccion.update_record()
                    #
                    # else:
                    #     pass

        return _D_return

    pass


db.define_table(
    TSERVIDOR_INSTRUCCIONES.S_NOMBRETABLA, *TSERVIDOR_INSTRUCCIONES.L_DBCAMPOS,
    format = '%(fecha)s %(tipoinstruccion)s',
    singular = 'Servidor Instrucción',
    plural = 'Servidor Instrucciones',
    migrate = D_stvFwkCfg.b_requestIsMaster,
    table_class = TSERVIDOR_INSTRUCCIONES
    )


class TSERVIDOR_CONEXIONES:
    """ Definición de la tabla de servidores """
    pass


db.define_table(
    'tservidor_conexiones',
    Field(
        'servidor_id', db.tservidores, default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = stv_widget_db_combobox, label = 'Servidor', comment = None,
        writable = False, readable = True,
        represent = stv_represent_referencefield
        ),
    Field(
        'aplicacion_w2p', 'string', length = 100, default = None,
        required = True,
        requires = IS_IN_SET(STV_FWK2_VALIDACIONES.APLICACION_W2P.OPCIONES(), zero = 0, error_message = 'Selecciona'),
        notnull = True, unique = False,
        widget = stv_widget_combobox, label = 'Web2Py Application',
        comment = 'Nombre de la aplicación web2py relacionada con esta notificación',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'controlador_w2p', 'string', length = 100, default = None,
        required = True,
        notnull = True, unique = False,
        widget = lambda field, value: stv_widget_db_chosen_linked(
            field, value, 1, 'aplicacion_w2p', URL(c = 'accesos', f = 'application_controladores_ver.json')
            ),
        # widget = stv_widget_combobox,
        label = 'Web2Py Controller', comment = 'Nombre del controlador web2py relacionada con esta notificación',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'funcion_w2p', 'string', length = 100, default = None,
        required = True,
        notnull = True, unique = False,
        widget = stv_widget_input, label = 'Web2Py Function',
        comment = 'Nombre de la función web2py relacionada con esta notificación',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'argumentos', 'string', length = 200, default = None,
        required = False,
        notnull = False, unique = False,
        widget = stv_widget_input, label = 'Argumentos', comment = 'Argumentos principales',
        writable = False, readable = True,
        represent = stv_represent_string
        ),
    Field(
        'instruccion_id', db.tservidor_instrucciones, default = None,
        required = False,
        ondelete = 'NO ACTION', notnull = False, unique = False,
        widget = None, label = 'Instrucción', comment = None,
        writable = False, readable = True,
        represent = lambda v, r: stv_represent_referencefield(v, r)
        ),
    Field(
        'fecha', type = FIELD_UTC_DATETIME, default = request.browsernow,
        required = True, requires = IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIMEISO),
        notnull = True,
        widget = stv_widget_inputDateTime, label = 'Fecha Request', comment = '',
        writable = False, readable = True,
        represent = stv_represent_datetime
        ),
    Field(
        'ip', 'string', length = 50, default = request.client,
        required = False,
        notnull = False, unique = False,
        label = 'IP', comment = None,
        writable = False, readable = True,
        represent = None
        ),
    Field(
        'notas', 'text', default = None,
        required = False,
        notnull = False,
        label = 'Notas', comment = None,
        writable = True, readable = True
        ),
    tSignature,
    format = '%(fecha)s %(funcion_w2p)s %(instruccion_id)s',
    singular = 'Servidor Log',
    plural = 'Servidor Log',
    migrate = D_stvFwkCfg.b_requestIsMaster
    )
        

''' Incluir integridad referencial con auth '''

db.auth_membership.cuenta_id.type = 'reference tcuentas'
db.auth_membership.cuenta_id.ondelete = 'NO ACTION'
db.auth_membership.cuenta_id.requires = IS_IN_DB(db, 'tcuentas.id', '%(nombrecorto)s')
db.auth_membership.cuenta_id.widget = lambda f, v: stv_widget_db_search(
    f, v,
    s_display = '%(tcuentas.nombrecorto)s',
    s_url = URL(a = D_stvFwkCfg.s_nameBackofficeApp, c = 'cuentas', f = 'cuentas_buscar'),
    D_additionalAttributes = Storage(
        reference = db.tcuenta_domicilios.cuenta_id
        # Ejemplo de correcta relación con el campo a buscar, esto es necesario ya que el tipo se define
        #  despues de la creación de la tabla
        )
    )
db.auth_membership.cuenta_id.represent = stv_represent_referencefield

db.tcuentas.servidorgestionador_id.type = 'reference tservidores'
db.tcuentas.servidorgestionador_id.ondelete = 'NO ACTION'
db.tcuentas.servidorgestionador_id.requires = IS_IN_DB(
    db(
        db.tservidores.gestionador == True
        ),
    'tservidores.id',
    db.tservidores._format
    )

# Se crea la referencia para su uso en los campos
db.tcuentas._referenced_by.append(db.auth_membership.cuenta_id)
db.tcuentas._references.append(db.auth_membership.cuenta_id)
