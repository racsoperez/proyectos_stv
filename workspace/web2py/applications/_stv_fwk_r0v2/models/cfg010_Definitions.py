# -*- coding: utf-8 -*-

'''
Created on Aug 5, 2014

@author: jaimeherrero

This file contain global definitions for the project
'''
response.generic_patterns = ['*']
from gluon.custom_import import track_changes
track_changes(True)

import datetime
import re
import json
import os
#import gluon.contrib.simplejson as simplejson
import locale
from gluon.tools import Auth, Service
from decimal import Decimal
from pydal.objects import Table
#from enum import Enum

service = Service()
"""Service: Se utiliza para manejar el servicio de webservices."""

from applications._stv_fwk_r0v2.modules.cfg030_Class import *
from applications._stv_fwk_r0v2.modules.cfg020_Widgets import *

D_stvFwkCfg.s_nameAccesoApp = 'acceso'
''' 
:var str D_stvFwkCfg.s_nameAccesoApp: variable global que define la aplicación que actualiza la definición de las bases de datos
'''

D_stvFwkCfg.b_requestIsMaster = (request.application == D_stvFwkCfg.s_nameAccesoApp)
''' 
:var boolean D_stvFwkCfg.b_requestIsMaster: define si se el request proviene de la aplicación master 
'''

D_stvFwkCfg.s_nameBackofficeApp = 'backoffice'
''' 
:var str D_stvFwkCfg.s_nameBackofficeApp: variable global que define la aplicación utilizada como soporte del backoffice
'''

D_stvFwkCfg.s_dirFwk = os.path.join(request.folder, '..', D_stvFwkCfg.s_nameFwkApp)
D_stvFwkCfg.s_dirFwkView = os.path.join(D_stvFwkCfg.s_dirFwk, 'views', 'inspinia')
D_stvFwkCfg.s_dirAcceso = os.path.join(request.folder, '..', D_stvFwkCfg.s_nameAccesoApp)

'''
:var stv_fwk_permissions: Objeto que centraliza el uso de código y define sus IDs para uso en base de datos, esta parte es vital para la configuración del sitio 
y sus funcionalidades. Es importante no cambiar el orden de definición, sin embargo es posible agregar más opciones dependiendo de las necesidades de las formas
'''

''' Forzamos el lenguaje por default al español '''
T.force('es')

''' Define el idioma por default en las formas '''
#locale.setlocale(locale.LC_ALL, 'es_ES')


''' ID usado para el contenido de las formas '''
# ID único, generado a través de los parámetros de la opciones del menú usando la función stv_fwk_tabAdd de javascript en tiempo de ejecución
def fn_createIDTabUnique(a = str(request.application), c = str(request.controller), f = str(request.function), s_masterIdentificator = None):
    _s_res = a + '_' + c + '_' + f
    _s_res += ('_' + s_masterIdentificator) if s_masterIdentificator else ''
    _s_res += ('_' + str(request.utcnow.microsecond))
    return _s_res

# Se identifica si el request corresponde a un detalle, para formar un id unico de la forma detalle
_s_masterForm = None
if len(request.args) > 1:
    _lenArgs = len(request.args)
    for _n_index in range(_lenArgs - 1, -1, -1):
        fn_match = re.match(r"(?P<master>master)_(?P<form>\w+)", request.args[_n_index])
        if fn_match:
            _d_match = fn_match.groupdict()
            _s_masterForm = str(_d_match['form'])
            break 

D_stvFwkCfg.D_uniqueHtmlIds = Storage()
D_stvFwkCfg.D_uniqueHtmlIds.s_idTab = fn_createIDTabUnique(s_masterIdentificator = _s_masterForm)
D_stvFwkCfg.D_uniqueHtmlIds.s_link = URL( a = str(request.application), c = str(request.controller), f = str(request.function) )

request.D_stvFwkCfg = D_stvFwkCfg

def _stv_flash_common(s_type, s_title, s_msg, s_mode = 'stv_flash', D_addProperties = {}):
    ''' Helpers para manejo de mensajes flash en la ventana principal
    :param str s_type: determina el tipo de mensaje a mostrar
    :param str s_title: es el título de la ventana empergente a mostrar
    :param str s_msg: es el mensaje a mostrar
    :param str s_mode: es el modo del mensaje a mostrar, 
                        'stv_flash' es usado para los mensajes empergentes del framework
                        'swal' es usado para mensajes de tipo sweet alert
    :param dict D_addProperties: es optativo y puede incluir parámetros adicionales
    :return: regresa el diccionario con el contenido del mensaje y sus parámetros
    :rtype: int
    '''
    
    # Si existe response.stv_flash y no es una intsancia de lista...
    if response.stv_flash:
        if not isinstance(response.stv_flash, list):
            # ...creala como lista.
            response.stv_flash = [response.stv_flash]
        else:
            # ...nada, response.stv_flash ya es una lista
            pass
    else:
        response.stv_flash = []
    _dStv_flash = {'type': s_type, 'title': str(s_title), 'msg': str(s_msg), 'mode': s_mode}
    _dStv_flash.update(D_addProperties)
    response.stv_flash += [_dStv_flash]
    return _dStv_flash

def stv_flashInfo(s_title, s_msg, s_mode = 'stv_flash', D_addProperties = {}):
    return _stv_flash_common('info', s_title, s_msg, s_mode, D_addProperties)

def stv_flashSuccess(s_title, s_msg, s_mode = 'stv_flash', D_addProperties = {}):
    return _stv_flash_common('success', s_title, s_msg, s_mode, D_addProperties)

def stv_flashWarning(s_title, s_msg, s_mode = 'stv_flash', D_addProperties = {}):
    return _stv_flash_common('warning', s_title, s_msg, s_mode, D_addProperties)

def stv_flashError(s_title, s_msg, s_mode = 'swal', D_addProperties = {}):
    return _stv_flash_common('error', s_title, s_msg, s_mode, D_addProperties)


def generaMensajeFlash(dbTabla, D_return):
    if D_return.E_return == CLASS_e_RETURN.OK_WARNING:
        stv_flashWarning(dbTabla._plural, D_return.s_msgError)

    elif D_return.E_return > CLASS_e_RETURN.OK_WARNING:
        stv_flashError(dbTabla._plural, D_return.s_msgError)

    elif D_return.s_msgError:
        stv_flashInfo(dbTabla._plural, D_return.s_msgError)

    else:
        pass

    return

def stv_encodedata_utf8(x_value): 
    #if type(value) == str:
    # if isinstance(x_value, unicode):
        # Ignore errors even if the string is not proper UTF-8 or has
        # broken marker bytes.
        # Python built-in function unicode() can do this.
        # value = unicode(value, "utf-8", errors="ignore")
        # x_value = x_value.encode('utf-8')
    return x_value

def stv_generateUrlForFile(a = None, c = None, f = None, args=[], vars=dict()):
    _s_a = a if a else request.application
    _s_c = c if c else request.controller
    _s_f = f if f else request.function
    _s_fileName = os.path.join('.', 'applications', _s_a, _s_c, _s_f)
    vars['stv_fwk_upd'] = int(os.path.getmtime(_s_fileName))
    return URL(a = a, c = c, f = f, args = args, vars = vars)


request._stv_flash_common = _stv_flash_common
request.stv_flashInfo = stv_flashInfo
request.stv_flashSuccess = stv_flashSuccess
request.stv_flashWarning = stv_flashWarning
request.stv_flashError = stv_flashError

# Helper utilizado para el apoyo en las definiciones genéricas en los campos 
D_stvFwkHelper = Storage()

D_stvFwkHelper.common = Storage()

# Helper utilizado para la definición genérica de la expreción regular para CURP 
D_stvFwkHelper.common.regexp_curp = "\
[A-Z]{1}[AEIOU]{1}[A-Z]{2}\
[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])\
[HM]{1}\
(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)\
[B-DF-HJ-NP-TV-Z]{3}\
[0-9A-Z]{1}\
[0-9]{1}\
"

# Helper utilizado para la definición genérica de la expreción regular para RFC 
D_stvFwkHelper.common.regexp_rfc = "[A-Z0-9]{3,4}\-\d{6}\-[A-Z0-9]{3}"

# Helper utilizado para la definición genérica de la expreción regular para Correo electronico
D_stvFwkHelper.common.regexp_email = "^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$"

# Helper utilizado para la definición genérica de la expreción regular para Números con parte de miles y fraccion opcional
D_stvFwkHelper.common.regexp_number = "^([0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)?|\.[0-9]+)$"


def importar_definiciones_aplicacion(s_nameConfigApp = '_app_config', s_filenameDefinitions = 'cfga010g_Definitions.py'):

    # Diccionario utilizado para la configuración del sitio. Se crea como variable global
    globals()['D_stvSiteCfg'] = Storage()
    
    # Se define la aplicación que contiene los archivos genéricos en el sitio.
    D_stvSiteCfg.s_nameConfigApp =  s_nameConfigApp
    
    D_stvSiteCfg.s_dirSiteViews = os.path.join(request.folder, '..', D_stvSiteCfg.s_nameConfigApp, 'views') 
    
    # Define una lista de los archivos a importar desde la configuración del sitio.
    if isinstance(s_filenameDefinitions, str):
        _L_fileFromConfig = [
            s_filenameDefinitions, 
            ]
        
    else:
        _L_fileFromConfig = s_filenameDefinitions
        
    
    # Importar los modelos definidos en el framework
    stv_update.genericAppImport(D_stvSiteCfg.s_nameConfigApp, 'models', _L_fileFromConfig, globals())

def stv_ultimafechadearchivomodificado(s_application = request.application):
    
    _L_pathsToCheck =  [
            os.path.join('.', 'applications', D_stvFwkCfg.s_nameFwkApp, 'models'),
            os.path.join('.', 'applications', D_stvFwkCfg.s_nameFwkApp, 'views'),
            os.path.join('.', 'applications', D_stvFwkCfg.s_nameFwkApp, 'private'),
            os.path.join('.', 'site-packages'),
        ]
    
    if s_application:
        _L_pathsToCheck +=  [
            os.path.join('.', 'applications', s_application, 'controllers'),
            os.path.join('.', 'applications', s_application, 'models'),
            os.path.join('.', 'applications', s_application, 'views'),
            os.path.join('.', 'applications', s_application, 'private'),
            os.path.join('.', 'applications', s_application, 'static'),
        ]

    _L_filesToCheck = []
    for _s_path in _L_pathsToCheck:
        _L_filesToCheck += [os.path.join(s_path, s_file) for s_path, L_dirs, L_files in os.walk(_s_path) for s_file in L_files]
    
    _L_filesToCheck.append(os.path.join('.', 'applications', D_stvFwkCfg.s_nameFwkApp, 'static', 'inspinia', 'css', 'stv_fwk_inspinia.css'))
    _L_filesToCheck.append(os.path.join('.', 'applications', D_stvFwkCfg.s_nameFwkApp, 'static', 'inspinia', 'js', 'stv_fwk_inspinia.js'))    
    
    _s_fileLastUpdated = max(_L_filesToCheck, key=os.path.getmtime)

    return os.path.getctime(_s_fileLastUpdated)
    

""" *** Definiciones para web services *** """

wbsT_D_AUTH = {
    's_token': str, 
    's_signature': str, 
    's_key': str, 
    'cuenta_aplicacion_id': int, 
    'cuenta_id': int
    }        
"""Type: Tipo de dato utilizado en webservices.

La información de T_auth se crea a través del webservice de login, y debe ser utilizado consecutivamente
con los siguientes webservices.
"""

wbsT_D_AUTHv2 = {
    's_ver': str,       # Version del sistema de authorizacion
    'json_key': str,    # JSON con el contenido para encriptar / desencriptar la información
    's_token': str,     # Token que identifica la session
    's_signature': str, # Signature para saber que la información es válida, puede ser CRC de los parametros
    'n_empresa_id': int, # El id de la empresa que se esta trabajando
    }        
"""Type: Tipo de dato utilizado en webservices.

La información de T_auth se crea a través del webservice de login, y debe ser utilizado consecutivamente
con los siguientes webservices.
"""


class CLASS_e_RETURN:
    # TODO: manejar un estado de error de Sin Estatus, algo así como pendiente
    
    OK = 0
    """Webservice ejecutado sin errores"""

    OK_WARNING = 1
    """Se realizó la operación pero existen mensajes de warning"""

    NOK_LOGIN = 10
    """Webservice require información de login actualizada"""
    
    NOK_PERMISOS = 11
    """Webservice require permisos que el usuario no tiene"""
    
    NOK_DATOS_INVALIDOS = 12
    """Webservice contiene información inválida"""

    NOK_CONDICIONES_INCORRECTAS = 13
    """Al ejecutar el webservice se encontraron condiciones incorrectas y no se pudo procesar"""

    NOK_SECUENCIA = 14
    """La ejecución del webservice require una secuencia que no se esta respetando"""

    NOK_REPETIR = 15
    """No se encontro error alguno, sin embargo el request no se pudo ejecutar. El request debe repetirse"""

    NOK_INFORMACION_ACTUALIZADA = 16
    """La información contenida en el webservice se basa en información que fue actualizada"""
    
    NOK_ERROR = 17
    """Se encontro algún error no detallado"""

    NOK_CONEXION = 18
    """Error en conexion"""
    
    NOK_TIEMPO_EXCEDIDO = 19
    """El tiempo para ejecutar la operación fue excedido y se trunco o canceló"""
    
    NOK_LIMITE_EXCEDIDO = 20
    """La operación fue truncada o cancelada porque excedió el limite de algún parámetro en la operación"""

    PENDIENTE = 90

    MAX = 99

    D_dict = {
        OK                         : 'OK',
        OK_WARNING                 : 'Precaución',
        NOK_LOGIN                  : 'Login',
        NOK_PERMISOS               : 'Permisos',
        NOK_DATOS_INVALIDOS        : 'Datos Inválidos',
        NOK_CONDICIONES_INCORRECTAS: 'Condiciones Incorrectas',
        NOK_SECUENCIA              : 'Secuencia',
        NOK_REPETIR                : 'Repetir',
        NOK_INFORMACION_ACTUALIZADA: 'Información Actualizada',
        NOK_ERROR                  : 'Error',
        NOK_CONEXION               : 'Err. Conexión',
        NOK_TIEMPO_EXCEDIDO        : 'Err. Tiempo',
        NOK_LIMITE_EXCEDIDO        : 'Err. Limite',
        PENDIENTE                  : 'Pendiente'
        }

    @classmethod
    def GET_DICT(cls):
        return cls.D_dict


stvfwk2_e_RETURN = CLASS_e_RETURN

class CLASS_e_PRIORIDAD:

    CRITICA = 0
    """No puede hacer nada"""
    s_CRITICA = 'alta'

    ALTA = 10
    """Proridad máxima"""
    s_ALTA = 'alta'

    MEDIA = 20
    """Prioridad media"""
    s_MEDIA = 'media'

    BAJA = 30
    """Prioridad Baja"""
    s_BAJA = 'baja'

    INFORMACION = 100
    """Prioridad Baja"""
    s_INFORMACION = 'informacion'

    PENDIENTE = 110

    @classmethod
    def GET_DICT(self):
        return {
                self.CRITICA: 'Crítica',
                self.ALTA: 'Alta',
                self.MEDIA: 'Media',
                self.BAJA: 'Baja',
                self.INFORMACION: 'Información',
                }
    
stvfwk2_e_PRIORIDAD = CLASS_e_PRIORIDAD

class D_NOTIFICACIONMENSAJE(Storage):
    """ Clase para manejo de notificaciones avanzadas, es compatible con la tabla tNotificaciones definida en dba210g_notificaciones
    """
    
    class CODIGOVALIDACION:
        """ Define las constantes a usar en el código de validación. 
        
        Normalmente se los códigos de validación se llenan con la siguiente información
            nombreTablaProblema: Nombre de la tabla que contiene el problema
            nombreCampoProblema: Nombre del campo que contiene el problema
            valorCampoProblema: Dato que contiene el problema
            nombreTabla: Nombre de la tabla relacionada
            nombreCampo: Nombre del campo relacionado
            valorCampo: Dato relacionado
        
        """
        ASOCIAR = 'asociar-%s_%s_%s-%s_%s_%s'
        INSERTAR = 'insertar-%s_%s_%s-%s_%s_%s' 
        IMPORTAR = 'importar-%s_%s_%s-%s_%s_%s' 
        VALIDAR = 'validar-%s_%s_%s-%s_%s_%s' 
        ELIMINAR = 'eliminar-%s_%s_%s-%s_%s_%s'

    
    def __init__(
            self, 
            n_id = 0, 
            E_return = stvfwk2_e_RETURN.NOK_ERROR, 
            s_msgError = "", 
            s_msgExcepcion = "", 
            s_msgLastSQL = "", 
            n_prioridad= stvfwk2_e_PRIORIDAD.BAJA,
            n_referencia_id = 0,
            s_codigovalidacion = "",
            s_proceso = "",
            n_len_dbRows = 0
            ):
        """ Inicialización de la clase
        
        Args:
            n_id: id del row en la tabla
            E_return: código de error de regreso
            s_msgError: mensaje de error
            s_msgExcepcion: mensaje de error de la exception
            s_msgLastSQL: ultimo SQL a procesar
            n_prioridad: prioridad del mensaje definido por clase stvfwk2_e_PRIORIDAD
            n_referencia_id: id en la tabla de referencia
            s_codigovalidacion: código para identificar el proceso de validación para el mensaje, debe componerse de la constante mas parámetros
                separados por _. Ejemplo: D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.ASOCIAR + "%s_%s_%s" % (nombreTabla, nombreCampo, valorCampo)
            
        """
        
        self.n_id = n_id
        self.E_return = E_return
        self.s_msgError = s_msgError
        self.s_msgExcepcion = s_msgExcepcion
        self.s_msgLastSQL = s_msgLastSQL
        self.n_prioridad = n_prioridad        
        self.n_referencia_id = n_referencia_id
        self.s_codigovalidacion = ""
        self.s_proceso = s_proceso
        self.n_len_dbRows = n_len_dbRows
        
        return
    
    def mensaje_al_asociar(self, s_nombreTablaProblema, s_nombreCampoProblema, s_valorCampoProblema, s_nombreTabla="", s_nombreCampo="", s_valorCampo=""):
        """ En caso de asociar, se requiere definir codigovalidación con cierto formato.
        
        Args:
            s_nombreTablaProblema: Nombre de la tabla que contiene la información a asociar 
            s_nombreCampoProblema: Nombre del campo que contiene la información a asocial
            s_valorCampoProblema: Dato que se busca asociar
            s_nombreTabla: Nombre de la tabla a asociar
            s_nombreCampo: Nombre del campo por el que se esta buscando valorCampoProblema
            s_valorCampo: '' opcional

        """
        self.s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.ASOCIAR % (s_nombreTablaProblema, s_nombreCampoProblema, s_valorCampoProblema, s_nombreTabla, s_nombreCampo, s_valorCampo)
        
    def mensaje_al_insertar(self, s_nombreTablaProblema, s_nombreCampoProblema="", s_valorCampoProblema="", s_nombreTabla="", s_nombreCampo="", s_valorCampo=""):
        """ En caso de asociar, se requiere definir codigovalidación con cierto formato.
        
        Args:
            s_nombreTablaProblema: Nombre de la tabla que contiene el problema 
            s_nombreCampoProblema: Nombre del campo que contiene el problema
            s_valorCampoProblema: Dato que se busca insertar
            s_nombreTabla: Nombre de la tabla donde se intentó insertar
            s_nombreCampo: '' opcional
            s_valorCampo: '' opcional

        """
        self.s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.INSERTAR % (s_nombreTablaProblema, s_nombreCampoProblema, s_valorCampoProblema, s_nombreTabla, s_nombreCampo, s_valorCampo)

    def mensaje_al_importar(self, s_nombreTablaProblema, s_nombreCampoProblema="", s_valorCampoProblema="", s_nombreTabla="", s_nombreCampo="", s_valorCampo=""):
        """ En caso de asociar, se requiere definir codigovalidación con cierto formato.
        
        Args:
            s_nombreTablaProblema: Nombre de la tabla principal donde se desea importar
            s_nombreCampoProblema: '' vacío
            s_valorCampoProblema: '' vacío
            s_nombreTabla: '' vacío
            s_nombreCampo: '' vacío
            s_valorCampo: '' vacío

        """
        self.s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.IMPORTAR % (s_nombreTablaProblema, s_nombreCampoProblema, s_valorCampoProblema, s_nombreTabla, s_nombreCampo, s_valorCampo)

    def mensaje_al_validar(self, s_nombreTablaProblema, s_nombreCampoProblema, s_valorCampoProblema, s_nombreTabla="", s_nombreCampo="", s_valorCampo=""):
        """ En caso de asociar, se requiere definir codigovalidación con cierto formato.
        
        Args:
            s_nombreTablaProblema: Nombre de la tabla donde se encuentra el campo a validar
            s_nombreCampoProblema: Nombre del campo a validar
            s_valorCampoProblema: Dato a validar
            s_nombreTabla: '' vacío
            s_nombreCampo: '' vacío
            s_valorCampo: '' vacío

        """
        self.s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.VALIDAR % (s_nombreTablaProblema, s_nombreCampoProblema, s_valorCampoProblema, s_nombreTabla, s_nombreCampo, s_valorCampo)

    def mensaje_al_eliminar(self, s_nombreTablaProblema, s_nombreCampoProblema, s_valorCampoProblema, s_nombreTabla="", s_nombreCampo="", s_valorCampo=""):
        """ En caso de asociar, se requiere definir codigovalidación con cierto formato.
        
        Args:
            s_nombreTablaProblema: Nombre de la tabla donde se encuentra el campo a eliminar
            s_nombreCampoProblema: Nombre del campo usado para la condición a eliminar
            s_valorCampoProblema: Dato del campo para la condición a eliminar
            s_nombreTabla: '' vacío
            s_nombreCampo: '' vacío
            s_valorCampo: '' vacío

        """
        self.s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.ELIMINAR % (s_nombreTablaProblema, s_nombreCampoProblema, s_valorCampoProblema, s_nombreTabla, s_nombreCampo, s_valorCampo)
        
        
class D_RETURN_ERRORES_PRIORIDAD(Storage):
    """ Clase para encapsular las notificaciones en base a prioridades
    """
    
    def __init__(
            self, 
            b_informacion = False, 
            b_baja = True, 
            b_media = True, 
            b_alta = True,
            b_critica = True,
            ):
        """ Inicialización de la clase
        """
        self.NOTIFICACIONES = []
        # Se almacena una variable que determina si existen mensajes con esa prioridad
        self.CRITICA = False
        self.ALTA = False
        self.MEDIA = False
        self.BAJA = False
        self.INFORMACION = False
        
        # Se define si se van a generar notificaciones dependiendo de la prioridad
        self._b_informacion = b_informacion
        self._b_baja = b_baja
        self._b_media = b_media
        self._b_alta = b_alta
        self._b_critica = b_critica
        
        self.ultimo_id = None

    def critica(self, D_error, s_proceso):
        if self._b_critica:
            D_error.n_prioridad = stvfwk2_e_PRIORIDAD.CRITICA
            D_error.s_proceso = s_proceso
            self.NOTIFICACIONES += [D_error]
            self.CRITICA = True
        else:
            pass

    def alta(self, D_error, s_proceso):
        if self._b_alta:
            D_error.n_prioridad = stvfwk2_e_PRIORIDAD.ALTA
            D_error.s_proceso = s_proceso
            self.NOTIFICACIONES += [D_error]
            self.ALTA = True
        else:
            pass

    def media(self, D_error, s_proceso):
        if self._b_media:
            D_error.n_prioridad = stvfwk2_e_PRIORIDAD.MEDIA
            D_error.s_proceso = s_proceso
            self.NOTIFICACIONES += [D_error]
            self.MEDIA = True
        else:
            pass        

    def baja(self, D_error, s_proceso):
        if self._b_baja:
            D_error.n_prioridad = stvfwk2_e_PRIORIDAD.BAJA
            D_error.s_proceso = s_proceso
            self.NOTIFICACIONES += [D_error]
            self.BAJA = True
        else:
            pass

    def informacion(self, D_error, s_proceso):
        if self._b_informacion:
            D_error.n_prioridad = stvfwk2_e_PRIORIDAD.INFORMACION
            D_error.s_proceso = s_proceso
            self.NOTIFICACIONES += [D_error]
            self.INFORMACION = True
        else:
            pass
        
    def agregar(self, D_erroresPrioridad):
        self.NOTIFICACIONES += D_erroresPrioridad.NOTIFICACIONES
        self.CRITICA |= D_erroresPrioridad.CRITICA
        self.ALTA |= D_erroresPrioridad.ALTA
        self.MEDIA |= D_erroresPrioridad.MEDIA
        self.BAJA |= D_erroresPrioridad.BAJA
        self.INFORMACION |= D_erroresPrioridad.INFORMACION
        
    def getTodos(self):
        return self.NOTIFICACIONES
    
class STV_RESPONSE_JSON_A01(object):
    
    def __init__(
            self,
            L_errores = [],
            E_return = stvfwk2_e_RETURN.OK,
            n_id = 0,
            ):
        self._D_data = Storage(
            s_estructura = 'A',
            n_version = 1,
            D_actualizarGrid = Storage(
                L_actualizar = [], 
                L_remover = [],
                L_insertar = [], 
                b_removersinoactualizado = True
                ),
            L_errores = L_errores,
            E_return = E_return,
            n_id = n_id,
            fn_onOK = "",
            fn_onError = "",
            fn_onTerminar = "",
            D_datos = Storage()
            )
        
    def actualizarGrid(
            self,
            L_actualizar = [], 
            L_remover = [],
            L_insertar = [], 
            b_removersinoactualizado = True
            ):
        if L_actualizar:
            self._D_data.D_actualizarGrid.L_actualizar = L_actualizar
        else:
            pass 
        if L_remover:
            self._D_data.D_actualizarGrid.L_remover = L_remover
        else:
            pass 
        if L_insertar:
            self._D_data.D_actualizarGrid.L_insertar = L_insertar
        else:
            pass 
        if b_removersinoactualizado:
            self._D_data.D_actualizarGrid.b_removersinoactualizado = b_removersinoactualizado
        else:
            pass 

    @property
    def D_data(self):
        return self._D_data
    
    @property
    def E_return(self):
        return self._D_data.E_return

    @E_return.setter
    def E_return(self, n_val):
        self._D_data.E_return = n_val
    
    @property
    def n_id(self):
        return self._D_data.n_id
    
    @E_return.setter
    def n_id(self, n_val):
        self._D_data.n_id = n_val

    def add_error(self, s_msg):
        self._D_data.L_errores.append(s_msg)
        
    def add_dato(
            self,
            s_key,
            x_value):
        self._D_data[s_key] = x_value
        
    def print_xml(self):
        from gluon.serializers import json
        return XML(json(self.D_data))
        
            
class STV_LIB_DB:
    
    s_ultimo_cache_id = ""
    
    @classmethod
    def _CONFIGURAR_CAMPO_DIFERENTE_NONE(
            cls,
            dbField,
            s_atributo,
            X_valor
            ):
        if (
            (X_valor is not None)
            and (hasattr(dbField, s_atributo))
            ):
            setattr(dbField, s_atributo, X_valor)
        else:
            pass
        return dbField

    @classmethod
    def CONFIGURAR_CAMPO(
            cls,
            dbField,
            **D_params
            ):

        D_params = Storage(D_params)
        dbField.default = D_params.default or dbField.default
        dbField.requires = D_params.requires or dbField.requires
        dbField.widget = D_params.widget or dbField.widget
        dbField.label = D_params.label or dbField.label
        dbField.comment = D_params.comment or dbField.comment
        dbField.writable = D_params.writable or dbField.writable
        dbField.readable = D_params.readable or dbField.readable
        dbField.represent = D_params.represent or dbField.represent

        return dbField

    @staticmethod
    def DEFINIR_DBCAMPO(  # Ultima actualización: 220118
            dbTablaReferencia,
            s_nombreCampo = 'referencia_id',
            dbQry = None,
            label = 'referencia',
            **D_params
            ):
        """  Función genérica para crear campos que referencían

        @param label:
        @type label:
        @param dbQry: Consulta adicional a agregar en la referencia
        @type dbQry:
        @param dbTablaReferencia:
        @type dbTablaReferencia:
        @param s_nombreCampo:
        @type s_nombreCampo:
        @param D_params:
        @type D_params:
        @return:
        @rtype:
        """

        D_params = Storage(D_params)
        if not D_params.requires:
            D_params.requires = IS_IN_DB(dbTablaReferencia._db(dbQry), dbTablaReferencia.id, dbTablaReferencia._format)
            if not D_params.required:
                D_params.requires = IS_NULL_OR(D_params.requires)
            else:
                pass
        else:
            pass

        _dbCampo = Field(
            s_nombreCampo, dbTablaReferencia,
            default = D_params.default or None,
            required = D_params.required or False,
            requires = D_params.requires,
            ondelete = 'NO ACTION',
            notnull = D_params.notnull or False,
            unique = False,
            widget = D_params.widget or (lambda f, v: stv_widget_db_chosen(f, v, 1)),
            label = label,
            comment = D_params.comment or "",
            writable = D_params.writable or False,
            readable = True,
            represent = D_params.represent or stv_represent_referencefield
            )

        return _dbCampo

    @staticmethod
    def DEFINIR_DBCAMPO_OTRADB(  # Ultima actualización: 220118
            dbTablaReferencia,
            s_nombreCampo = 'referencia_id',
            dbQry = None,
            label = 'referencia',
            **D_params
            ):
        """  Función genérica para crear campos que referencían

        @param label:
        @type label:
        @param dbQry: Consulta adicional a agregar en la referencia
        @type dbQry:
        @param dbTablaReferencia:
        @type dbTablaReferencia:
        @param s_nombreCampo:
        @type s_nombreCampo:
        @param D_params:
        @type D_params:
        @return:
        @rtype:
        """

        D_params = Storage(D_params)
        if not D_params.requires:
            D_params.requires = IS_IN_DB(dbTablaReferencia._db(dbQry), dbTablaReferencia.id, dbTablaReferencia._format)
            if not D_params.required:
                D_params.requires = IS_NULL_OR(D_params.requires)
            else:
                pass
        else:
            pass

        _dbCampo = Field(
            s_nombreCampo, 'integer',
            default = D_params.default or None,
            required = D_params.required or False,
            requires = D_params.requires,
            ondelete = 'NO ACTION',
            notnull = D_params.notnull or False,
            unique = False,
            widget = D_params.widget or (lambda f, v: stv_widget_db_chosen(f, v, 1)),
            label = label,
            comment = D_params.comment or "",
            writable = D_params.writable or False,
            readable = True,
            represent = D_params.represent or stv_represent_referencefield
            )

        return _dbCampo

    @classmethod
    def ASOCIAR_TABLA(
            cls,
            dbTabla, 
            s_nombreCampo, 
            s_datoBuscar, 
            s_msgError = "", 
            qry_maestro = None, 
            s_proceso = "",
            s_nombreCampoId = "id"
            ):
        """ Busca registro y regresa su id en base a los parámetros
        
        Args:
            dbTabla: tabla a usar para la búsqueda
            s_nombreCampo: campo de la tabla por el cual se va a buscar s_datoBuscar
            s_datoBuscar: información a buscar
            s_msgError: en caso de existir es el mensaje que será retornado, puede incluir los siguientes códigos
                %(tabla)s: para imprimir el singular de la tabla
                %(tablaSingular)s: para imprimir el singular de la tabla
                %(nombreCampo)s: para imprimir el nombre del campo
                %(datoBuscar)s: para imprimir el dato a buscar
            L_referencia: es una lista con aplicacion, controlador, funcion, argumentos
            
        Returns:
            D_NOTIFICACIONMENSAJE
        """
        _D_return = D_NOTIFICACIONMENSAJE(s_proceso = s_proceso)
        _D_return.mensaje_al_asociar(str(dbTabla), s_nombreCampo, s_datoBuscar)
    
        if qry_maestro:
            _dbRows = dbTabla._db(
                qry_maestro
                & (dbTabla[s_nombreCampo] == s_datoBuscar)
                & (dbTabla.bansuspender == False)
                ).select(
                    dbTabla[s_nombreCampoId],
                    orderby = dbTabla[s_nombreCampoId]                    
                    )
        else:
            _dbRows = dbTabla._db(
                (dbTabla[s_nombreCampo] == s_datoBuscar)
                & (dbTabla.bansuspender == False)
                ).select(
                    dbTabla[s_nombreCampoId],
                    orderby = dbTabla[s_nombreCampoId]
                    )
    
        if len(_dbRows) != 1:
                
            _D_print = {
                'tabla'        : str(dbTabla),
                'tablaSingular': dbTabla._singular,
                'nombreCampo'  : s_nombreCampo,
                'datoBuscar'   : str(s_datoBuscar)
                }
            
            _D_return.n_len_dbRows = len(_dbRows)
            
            if s_msgError:
                _D_return.s_msgError = s_msgError % _D_print
            elif _D_return.n_len_dbRows == 0:
                _D_return.s_msgError = u'No se pudo encontro referencia a %s [%s = %s]' % (
                    dbTabla._singular, s_nombreCampo, str(s_datoBuscar)
                    )
                _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR                
            else:
                # Se regresa la referencia en caso de encontrar dos coincidencias
                _D_return.s_msgError = u'Se encontraron 2 o más referencias a %s' % dbTabla._singular
                _D_return.E_return = stvfwk2_e_RETURN.OK
                _D_return.n_id = _dbRows.first()[s_nombreCampoId]
            _D_return.s_msgExcepcion = u'%(tabla)s : %(nombreCampo)s = %(datoBuscar)s' % _D_print
            _D_return.s_msgLastSQL = dbTabla._db._lastsql
            
        else:
            _D_return.n_id = _dbRows.first()[s_nombreCampoId]
            _D_return.E_return = stvfwk2_e_RETURN.OK
    
        return _D_return
    
    @classmethod
    def CACHE_ASOCIAR_TABLA(
            cls,
            s_cache_id,
            n_time_expire,
            dbTabla, 
            s_nombreCampo, 
            s_datoBuscar, 
            s_msgError = "", 
            qry_maestro = None, 
            s_proceso = ""
            ):
        cls.s_ultimo_cache_id = s_cache_id
        _D_returnsData = cache.ram(
            s_cache_id, 
            lambda: STV_LIB_DB.ASOCIAR_TABLA(
                dbTabla, 
                s_nombreCampo, 
                s_datoBuscar,
                s_msgError,
                qry_maestro,
                s_proceso
                ), 
            time_expire = n_time_expire
            )
        return _D_returnsData
    
    @classmethod
    def CACHE_BORRAR_ULTIMO(cls):
        cache.ram(cls.s_ultimo_cache_id, None)
        
    @classmethod
    def INSERTAR_ACTUALIZAR_JSON(
            cls,
            O_json_data, 
            D_campos_relacion, 
            dbTabla, 
            O_row, 
            s_proceso = ""
            ):
        """ Importa y asocia el contenido de un json con las tablas del cliente, actualizando su contenido o insertando un registro.

        
        Args:
            O_json_data: objeto json con el contenido de datos.
            D_campos_relacion: es lo que regresa la función _definir_camposrelacion_cfdi()
            dbTabla: tabla donde se insertarán los datos 
            D_fields_defaults: campos prellenados con valores por default
    
        Returns:
            D_NOTIFICACIONMENSAJE
            @param s_proceso:
            @type s_proceso:
            @param O_row:
            @type O_row:
        """
        _D_return = D_NOTIFICACIONMENSAJE(s_proceso = s_proceso)
        _D_return.mensaje_al_insertar(str(dbTabla))
    
        # Se agregan los campos standard con sus valores
        _D_fieldsValues = O_row
    
        # Se agregan los valores adicionales importados del json
        _s_jsonData = None
        for _s_jsonField in D_campos_relacion:
            _L_elementosxml = _s_jsonField.split('.')
            if len(_L_elementosxml) == 1:
                _s_jsonData = O_json_data[_L_elementosxml[0]] if _L_elementosxml[0] in O_json_data else None

            elif len(_L_elementosxml) == 2:
                _s_jsonData = O_json_data[_L_elementosxml[0]][_L_elementosxml[1]] \
                    if (_L_elementosxml[0] in O_json_data) \
                    and (_L_elementosxml[1] in O_json_data[_L_elementosxml[0]]) else None

            else:
                _s_jsonData = O_json_data[_L_elementosxml[0]][_L_elementosxml[1]][_L_elementosxml[2]] \
                    if (_L_elementosxml[0] in O_json_data) \
                    and (_L_elementosxml[1] in O_json_data[_L_elementosxml[0]]) \
                    and (_L_elementosxml[2] in (O_json_data[_L_elementosxml[0]][_L_elementosxml[1]])) \
                    else None
    
            if not _s_jsonData and ('x_default' in D_campos_relacion[_s_jsonField]):
                _D_fieldsValues[str(D_campos_relacion[_s_jsonField].dbField.name)] = \
                    D_campos_relacion[_s_jsonField].x_default

            else:
                _D_fieldsValues[str(D_campos_relacion[_s_jsonField].dbField.name)] = _s_jsonData
    
        try:
            if isinstance(O_row, dict):
                _D_return.n_id = dbTabla.insert(**_D_fieldsValues)
                _D_return.E_return = stvfwk2_e_RETURN.OK
                _D_return.s_msgError = (u'Inserción satisfactoria en %s' % dbTabla._singular)

            else:
                O_row.update_record()
                _D_return.n_id = O_row.id
                _D_return.E_return = stvfwk2_e_RETURN.OK
                _D_return.s_msgError = (u'Actualización satisfactoria en %s' % dbTabla._singular)
                  
        except Exception as _O_excepcion:
            _D_return.s_msgError = (u'No se pudo insertar/actualizar el json de %s' + dbTabla._singular)
            _D_return.s_msgExcepcion = '%s:%s' % (str(dbTabla), str(_O_excepcion))
            _D_return.s_msgLastSQL = dbTabla._db._lastsql
            _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
    
        return _D_return      
    
    @classmethod
    def CONVERT_DATETIME(cls, s_fechaTiempo):
        if isinstance(s_fechaTiempo, str):
            _dt_form_fecha_movimiento = datetime.datetime.strptime(s_fechaTiempo, STV_FWK_APP.FORMAT.s_DATETIME)
        else:
            _dt_form_fecha_movimiento = s_fechaTiempo
        return _dt_form_fecha_movimiento
    
    @staticmethod
    def OBTENER_REGISTRO_DE_PARAMETRO(x_valor, dbTabla, b_regresarRegistro = True):
        """ Usando un parametro obtiene id y registro en la tabla

        Usando el tipo de dato de x_valor se determina si es un id o un dbRow;
        en caso de id se busca en dbTabla, si es un dbRow se regresa el id

        @param b_regresarRegistro: busca el registro en caso de no tenerlo como parametro
        @type b_regresarRegistro:
        @param x_valor:
        @type x_valor:
        @param dbTabla:
        @type dbTabla:
        @return: regresa D_return con el id y el dbRow
        @rtype:
        """
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            n_id = None,
            dbRow = None
            )

        if isinstance(x_valor, (str, int)):
            _D_return.n_id = x_valor
            _D_return.dbRow = dbTabla(_D_return.n_id) if b_regresarRegistro else None
        elif not x_valor:
            _D_return.E_return = CLASS_e_RETURN.NOK_DATOS_INVALIDOS
            _D_return.s_msgError = "El id del parámetro no esta definido para tabla %s" % str(dbTabla)
        else:
            _D_return.dbRow = x_valor
            _D_return.n_id = _D_return.dbRow.id

        return _D_return

    @staticmethod
    def OBTENER_ROW_DE_CONSULTA(dbRow, dbTabla = None):
        """ Buscando el campo id, regresa el row ya sea principal o en detalle.

        Algunas veces el resultado de una consulta regresa el row en el resultado principal o
        crea un key con el nombre de la tabla, y regresa en él el detalle.
        Esta función checa por el campo id en el principal, si no lo encuentra, busca un key
        con el nombre de la tabla, si no lo encuentra, regresa el primer key que contenga un
        key con nombre id

        @param dbRow:
        @type dbRow:
        @param dbTabla:
        @type dbTabla:
        @return: regresa dbRow
        @rtype:
        """
        # Para manejo de errores localemte, uso futuro
        _D_return = FUNC_RETURN()
        _dbRow = dbRow if dbRow.get('id') else dbRow[dbTabla] if dbTabla else dbRow[next(iter(dbRow))]
        return _dbRow

    @staticmethod
    def COMANDO_REFRESCAR(x_dbRow, dbTabla, L_dbCampos):
        _D_results = STV_LIB_DB.OBTENER_REGISTRO_DE_PARAMETRO(x_dbRow, dbTabla)
        _dbRow = _D_results.dbRow
        _L_camposActualizar = []
        for _dbCampo in L_dbCampos:
            _L_camposActualizar.append(
                Storage(
                    dbCampo = _dbCampo,
                    x_valor = _dbRow[_dbCampo], s_display = _dbRow[_dbCampo]
                    )
                )
        return _L_camposActualizar

    pass  # STV_LIB_DB

# noinspection PyAttributeOutsideInit
class FUNC_RETURN(Storage):

    def __init__(self, **D_args):
        D_argsReturn = {
            'E_return': CLASS_e_RETURN.OK,
            's_msgError': ""
            }
        D_argsReturn.update(D_args or {})
        Storage.__init__(self, D_argsReturn)
        return

    def agrega_advertencia(self, s_msgError):
        self.E_return = CLASS_e_RETURN.OK_WARNING
        self.s_msgError += s_msgError
        return

    def agrega(self, E_return, s_msgError, s_preMsg = ""):
        if E_return > self.E_return:
            self.E_return = E_return
        else:
            pass
        self.s_msgError += (s_preMsg or "") + s_msgError
        return

    def agrega_error(self, s_msgError):
        self.E_return = CLASS_e_RETURN.NOK_ERROR
        self.s_msgError += s_msgError
        return

    def agrega_condicionesincorrectas(self, s_msgError):
        self.agrega(CLASS_e_RETURN.NOK_CONDICIONES_INCORRECTAS, s_msgError)
        return

    def combinar(self, D_return, s_preMsg = ""):
        if D_return:
            D_return.s_msgError = self.s_msgError + (s_preMsg or "") + (D_return.s_msgError or "")
            if D_return.E_return and (D_return.E_return > self.E_return):
                pass
            else:
                D_return.E_return = self.E_return
            self.update(D_return)
        else:
            pass

        return

    pass  # FUNC_RETURN


class FUNC_MULTIRETURN(Storage):

    def __init__(self, **D_args):
        D_argsReturn = {
            'E_return'    : CLASS_e_RETURN.OK,
            's_preMsg'    : "",
            'L_msgError'  : [],
            'L_msgWarning': [],
            'L_msgInfo'   : [],
            }
        D_argsReturn.update(D_args or {})
        Storage.__init__(self, D_argsReturn)
        return

    def agrega_error(self, s_msg, E_return = CLASS_e_RETURN.NOK_ERROR):
        if E_return > self.E_return:
            self.E_return = E_return
        else:
            pass
        self.L_msgError.append(s_msg)
        return

    def agrega_advertencia(self, s_msg):
        if CLASS_e_RETURN.OK_WARNING > self.E_return:
            self.E_return = CLASS_e_RETURN.OK_WARNING
        else:
            pass
        self.L_msgWarning.append(s_msg)
        return

    def agrega_info(self, s_msg):
        self.L_msgInfo.append(s_msg)
        return

    def combinar(self, D_multireturn, s_preMsg = ""):
        if D_multireturn:
            self.s_preMsg += D_multireturn.s_preMsg
            self.L_info += D_multireturn.L_info
            self.L_msgWarning += D_multireturn.L_msgWarning
            self.L_msgError += D_multireturn.L_msgError
            if D_multireturn.E_return > self.E_return:
                self.E_return = D_multireturn.E_return
            else:
                pass
        else:
            pass

        return

    pass  # FUNC_RETURN


class FUNC_DEBUG(Storage):
    """ Clase de apoyo para debugear código, o mostrar navegación en procesos cuando un error ocurre """

    def __init__(self, **D_args):
        D_argsReturn = {
            'L_procesos': [],
            's_mensaje': "",
            'D_params': Storage()
            }
        D_argsReturn.update(D_args or {})
        Storage.__init__(self, D_argsReturn)
        return

    def entra_proceso(self, s_nombre, **D_params):
        _s_desc_proceso = ("%s [%s]" % (s_nombre, str(D_params))) if D_params else s_nombre
        self.L_procesos.append(_s_desc_proceso)
        self.s_mensaje = ""
        self.D_params = Storage()
        return

    def sale_proceso(self):
        self.s_mensaje = ""
        self.D_params = Storage()
        return self.L_procesos.pop() if self.L_procesos else ""

    def agrega_mensaje(self, s_mensaje, **D_params):
        self.s_mensaje = s_mensaje
        self.D_params = Storage(D_params)
        return

    def leer_mensaje(self):
        _s_mensajeDebug = "{proceso} > {mensaje}".format(
            proceso = " | ".join(self.L_procesos),
            mensaje = ("%s [%s]" % (self.s_mensaje, str(self.D_params))) if self.D_params else self.s_mensaje
            )
        return _s_mensajeDebug

    pass  # FUNC_DEBUG

    def agrega_advertencia(self, s_msgError):
        self.E_return = CLASS_e_RETURN.OK_WARNING
        self.s_msgError += s_msgError
        return

    def agrega(self, E_return, s_msgError, s_preMsg = ""):
        if E_return > self.E_return:
            self.E_return = E_return
        else:
            pass
        self.s_msgError += (s_preMsg or "") + s_msgError
        return

    def agrega_error(self, s_msgError):
        self.E_return = CLASS_e_RETURN.NOK_ERROR
        self.s_msgError += s_msgError
        return

    def agrega_condicionesincorrectas(self, s_msgError):
        self.agrega(CLASS_e_RETURN.NOK_CONDICIONES_INCORRECTAS, s_msgError)
        return

    def combinar(self, D_return, s_preMsg = ""):
        if D_return:
            D_return.s_msgError = self.s_msgError + (s_preMsg or "") + (D_return.s_msgError or "")
            if D_return.E_return and (D_return.E_return > self.E_return):
                pass
            else:
                D_return.E_return = self.E_return
            self.update(D_return)
        else:
            pass

        return

    pass  # FUNC_RETURN

FIELD_UTC_DATETIME = SQLCustomType(
    type = 'datetime',
    native = 'datetime',
    encoder = lambda x_dato:
        ("'%s'" % str(TZONASHORARIAS.CONVERTIR_FECHAHORA_DEZONAH_AUTC(STV_FWK_APP.n_zonahoraria_id, x_dato)))
        if x_dato else None,
    decoder = lambda dt_dato: TZONASHORARIAS.CONVERTIR_FECHAHORA_DEUTC_AZONAH(STV_FWK_APP.n_zonahoraria_id, dt_dato)
    )

TDATETIME_GRABAENZONAH_LEERENZONAH = SQLCustomType(
    type = 'datetime',
    native = 'datetime',
    encoder = lambda x_dato:
        ("'%s'" % str(TZONASHORARIAS.CONVERTIR_FECHAHORA_DEZONAH_AUTC(STV_FWK_APP.n_zonahoraria_id, x_dato)))
        if x_dato else None,
    decoder = lambda dt_dato: TZONASHORARIAS.CONVERTIR_FECHAHORA_DEUTC_AZONAH(STV_FWK_APP.n_zonahoraria_id, dt_dato)
    )
""" Tipo de dato que convierte el dato a UTC para grabarlo y al leerlo lo convierte a la zona horaria """

TDATETIME_GRABADIRECTO_LEERENZONAH = SQLCustomType(
    type = 'datetime',
    native = 'datetime',
    encoder = lambda x_dato: (("'%s'" % str(x_dato)) if x_dato else None),
    decoder = lambda dt_dato: TZONASHORARIAS.CONVERTIR_FECHAHORA_DEUTC_AZONAH(STV_FWK_APP.n_zonahoraria_id, dt_dato)
    )
""" Tipo de dato que graba el dato directo (suponiendo que es el UTC) y al leerlo lo convierte a la zona horaria """

TDATETIME_GRABADIRECTO_LEERDIRECTO = SQLCustomType(
    type = 'datetime',
    native = 'datetime',
    encoder = lambda x_dato: (("'%s'" % str(x_dato)) if x_dato else None),
    decoder = lambda dt_dato: dt_dato
    )
""" Tipo de dato que graba el dato directo y al leerlo lo lee de forma directa también """

# UPDATE tcfdis SET fecha = STR_TO_DATE(fecha_str,'%Y-%m-%dT%T')
# UPDATE tcfdis SET fechatimbrado = STR_TO_DATE(fechatimbrado_str,'%Y-%m-%dT%T')
# UPDATE tcfdi_complementopagos SET fechapago = STR_TO_DATE(fechapago_str,'%Y-%m-%dT%T')

