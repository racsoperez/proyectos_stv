# -*- coding: utf-8 -*-

# noinspection SpellCheckingInspection

import datetime
import time
import re

from gluon import *
from gluon.storage import Storage
from gluon.html import xmlescape
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import Element, SubElement, Comment, tostring


class STV_XML2CFDI(object):
    """ Clase usada para convertir de XML3.3 a Diccionario de CFDI """

    CFDI = "http://www.sat.gob.mx/cfd/3"
    VERSION = "3.3"
    TAGS_SON_LISTAS = [  # Define los elementos del CFDI que serán tratados como listas en el XML
        "concepto",
        "impuesto",
        "traslado",
        "retencion",
        "pago",
        "cfdirelacionado",
        "doctorelacionado"
        ]

    @property
    def Comprobante(self):
        return self._O_cfdi

    def __init__(self):
        self._O_tree = None
        self._E_root = None
        self._E_comprobante = None
        self._O_cfdi = None
        self._L_errores = []
        self._L_namespaces = []
        return

    def importar_string(self, s_xml):

        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.NOK_ERROR,
            s_msgError = "",
            )

        # Quitar los namespaces del xml para facilidar la importación de los datos
        self._L_namespaces = re.findall('xmlns:((\w+)=("[^"]+"))', s_xml)
        _s_xml = re.sub('xmlns:(\w+="[^"]+")', '', s_xml, count=100, flags=0)
        _L_namespaces_id = []
        for _T_namespace in self._L_namespaces:
            _L_namespaces_id += [_T_namespace[1]]
        _s_xml = re.sub(":|".join(_L_namespaces_id)+":", '', _s_xml, count=1000, flags=0)

        self._E_root = ET.fromstring(_s_xml)
        _b_result = self._validar_versiones()
        if _b_result:
            self._O_cfdi = self._convertir_a_dict(self._E_comprobante)
            self._formatear_rfcs()

            _D_return.E_return = stvfwk2_e_RETURN.OK
        else:
            _D_return.s_msgError = "Version de CFDI no compatible con el sistema, contacte al administrador"
        return _D_return

    def _validar_versiones(self):
        # Validar que exista el label cfdi:Comprobante y que contenga el atributo
        #  xmlns:cfdi="http://www.sat.gob.mx/cfd/3" y Version="3.3"
        self._E_comprobante = self._E_root
        if (
                ('Version' in self._E_comprobante.attrib)
                and (self._E_root.attrib['Version'] == self.VERSION)
                ):
            _b_result = True
        else:
            _b_result = False

        return _b_result

    def _convertir_a_dict(self, E_elementoHijo):
        _D_elemento = Storage()
        if E_elementoHijo or E_elementoHijo.attrib:
            _D_elemento = Storage({_s_key.lower(): _x_value for _s_key, _x_value in E_elementoHijo.attrib.items()})
            _D_elemento['stv_contenido'] = E_elementoHijo.text
            for _E_elementoSubHijo in E_elementoHijo:
                _s_tag = _E_elementoSubHijo.tag.lower()
                if _s_tag in self.TAGS_SON_LISTAS:
                    if _s_tag in _D_elemento:
                        _D_elemento[_s_tag] += [self._convertir_a_dict(_E_elementoSubHijo)]
                    else:
                        _D_elemento[_s_tag] = [self._convertir_a_dict(_E_elementoSubHijo)]
                else:
                    _D_elemento[_s_tag] = self._convertir_a_dict(_E_elementoSubHijo)
        else:
            pass

        return _D_elemento

    def _formatear_rfcs(self):

        # Se estandariza el RFC, agregando guinoes donde es necesario para su uso en el sistema
        self._O_cfdi.emisor.rfc = STV_LIB_CFDI.FORMATEAR_RFC_AGREGARGUIONES(self._O_cfdi.emisor.rfc)

        self._O_cfdi.receptor.rfc = STV_LIB_CFDI.FORMATEAR_RFC_AGREGARGUIONES(self._O_cfdi.receptor.rfc)
        return

    def validar_emisor(self, s_rfc, s_razonsocial):

        _D_return = Storage(
            s_msgError = "",
            E_return = CLASS_e_RETURN.NOK_ERROR,
            )

        if (
                (self._O_cfdi.emisor.nombre == s_razonsocial)
                and (self._O_cfdi.emisor.rfc == s_rfc)
                ):
            _D_return.E_return = CLASS_e_RETURN.OK
        else:
            if self._O_cfdi.emisor.nombre != s_razonsocial:
                _D_return.s_msgError += "Nombre no coincide en emisor %s != %s. " % (
                    self._O_cfdi.emisor.nombre, s_razonsocial
                    )
            else:
                pass
            if self._O_cfdi.emisor.rfc != s_rfc:
                _D_return.s_msgError += "RFC no coincide en emisor %s != %s. " % (
                    self._O_cfdi.emisor.rfc, s_rfc
                    )
            else:
                pass

        return _D_return

    def validar_receptor(self, s_rfc, s_razonsocial):
        # Validar que exista el label cfdi:Comprobante
        #  y que contenga el atributo xmlns:cfdi="http://www.sat.gob.mx/cfd/3" y Version="3.3"

        _D_return = Storage(
            s_msgError = "",
            E_return = CLASS_e_RETURN.NOK_ERROR,
            )

        if (
                (self._O_cfdi.receptor.nombre == s_razonsocial)
                and (self._O_cfdi.receptor.rfc == s_rfc)
                ):
            _D_return.E_return = CLASS_e_RETURN.OK
        else:
            if self._O_cfdi.receptor.nombre == s_razonsocial:
                _D_return.s_msgError += "Nombre no coincide en receptor %s != %s" % (
                    self._O_cfdi.receptor.nombre, s_razonsocial
                    )
            else:
                pass
            if self._O_cfdi.receptor.rfc == s_rfc:
                _D_return.s_msgError += "RFC no coincide en receptor %s != %s" % (self._O_cfdi.receptor.rfc, s_rfc)
            else:
                pass
            _D_return.E_return = CLASS_e_RETURN.NOK_DATOS_INVALIDOS

        return _D_return


class STV_CFDI_INTEGRACION:

    class E_TIPOIMPORTACION:
        NO_DEFINIDO = 0
        CFDI_EMITIDO = 1
        CFDI_RECIBIDO = 2

    def __init__(
            self,
            s_rfc,
            s_empresa_id,
            s_cuenta_id,
            s_procesoIniciador = None
            ):
        """ Inicialización de la clase definiendo la cuenta, empresa y rfc.

        @param s_rfc:
        @type s_rfc:
        @param s_empresa_id:
        @type s_empresa_id:
        @param s_cuenta_id:
        @type s_cuenta_id:
        @param s_procesoIniciador:
        @type s_procesoIniciador:
        """

        self._s_rfc = s_rfc  # RFC de la empresa que esta evaluando los CFDIs
        self._s_empresa_id = s_empresa_id  # ID de la empresa que debe corresponder al RFC
        self._s_cuenta_id = s_cuenta_id  # Cuenta a la que debe pertenecer la empresa
        self._dbRow_empresa = None  # Una vez identificada la empresa, contendrá el registro de la empresa
        self._b_pagosAlDia = False  # TODO Variable que verifica si la empresa se encuentra al día con sus pagos
        self._O_cfdi = None  # Objeto que maneja el XML del CFDI
        self._D_cfdi = Storage()  # Diccionario del CFDI
        self._n_cfdi_id = None  # Id del CFDI que se genera o que se identifica que ya existe

        self._D_procesoMensaje = Storage(
            s_iniciador = s_procesoIniciador or u"Proceso no definido",  # Usar el proceso que crea la clase
            s_paso = u"",  # Usar un código para identificar el paso en el proceso, puede definir un mensaje también
            s_enEjecucion = u"Inicialización",  # Usar el nombre de la función que se esté ejecutando
            L_enEjecucionBkp = [],  # Usar este mensaje como backup cuando se salga de la función en ejecución
            s_descripcion = u""  # Mensaje opcional adicional para identificar el camino que se esta ejecutando
            )

        self._E_tipoImportacion = STV_CFDI_INTEGRACION.E_TIPOIMPORTACION.NO_DEFINIDO

        # Se definen en otra funciona de inicialización al momento de importar o verificar
        self._D_tablas = None
        self._D_campos_relacion = None

        self._D_resultOK = Storage(
            E_return = stvfwk2_e_RETURN.OK,
            s_msgError = ""
            )

        self._D_resultWARNING = Storage(
            E_return = stvfwk2_e_RETURN.OK_WARNING,
            s_msgError = u"Se pudo continuar con el warning"
            )

        self._D_resultERROR = Storage(
            E_return = stvfwk2_e_RETURN.NOK_ERROR,
            s_msgError = u"Error en verificación"
            )

        return

    def _incializar_tablas(self):

        if self._E_tipoImportacion == STV_CFDI_INTEGRACION.E_TIPOIMPORTACION.CFDI_EMITIDO:
            self._D_tablas = Storage(
                cfdis = dbc01.tcfdis,
                cfdis2 = dbc01.tcfdis2,
                conceptos = dbc01.tcfdi_conceptos,
                relacionados = dbc01.tcfdi_relacionados,
                conceptos_impretenidos = dbc01.tcfdi_concepto_impuestosretenidos,
                conceptos_imptrasladados = dbc01.tcfdi_concepto_impuestostrasladados,
                comppagos = dbc01.tcfdi_complementopagos,
                comppago_docsrelacionados = dbc01.tcfdi_complementopago_doctosrelacionados,
                xml = dbc02.tcfdi_xmls,
                notificacion_mensajes = dbc02.tnotificacion_mensajes,
                notificaciones = dbc02.tnotificaciones
                )
        elif self._E_tipoImportacion == STV_CFDI_INTEGRACION.E_TIPOIMPORTACION.CFDI_RECIBIDO:
            self._D_tablas = Storage(
                cfdis = dbc01.tcfdisrx,
                # cfdis2 = dbc01.tcfdis2rx,
                conceptos = dbc01.tcfdirx_conceptos,
                relacionados = dbc01.tcfdirx_relacionados,
                conceptos_impretenidos = dbc01.tcfdirx_concepto_impuestosretenidos,
                conceptos_imptrasladados = dbc01.tcfdirx_concepto_impuestostrasladados,
                comppagos = dbc01.tcfdirx_complementopagos,
                comppago_docsrelacionados = dbc01.tcfdirx_complementopago_doctosrelacionados,
                xml = dbc02.tcfdirx_xmls,
                notificacion_mensajes = dbc02.tnotificacion_mensajes,
                notificaciones = dbc02.tnotificaciones
                )
        else:
            # No se definen tablas e importación debería de tronar
            raise ValueError('No se definió el tipo importación.')

        # Conversion del diccionario a los campos donde se grabará la información
        self._D_campos_relacion = {
            'version': Storage(
                dbField = self._D_tablas.cfdis.versioncfdi,
                fnValidation = None
                ),
            'serie': Storage(
                dbField = self._D_tablas.cfdis.serie,
                fnValidation = None
                ),
            'folio': Storage(
                dbField = self._D_tablas.cfdis.folio,
                fnValidation = None
                ),
            'fecha': Storage(
                dbField = self._D_tablas.cfdis.fecha_str,
                fnValidation = None
                ),
            'formapago': Storage(
                dbField = self._D_tablas.cfdis.formapago,
                fnValidation = None
                ),
            'condicionesdepago': Storage(
                dbField = self._D_tablas.cfdis.condicionesdepago,
                fnValidation = None,
                x_default = ""
                ),
            'subtotal': Storage(
                dbField = self._D_tablas.cfdis.subtotal,
                fnValidation = None
                ),
            'descuento': Storage(
                dbField = self._D_tablas.cfdis.descuento,
                fnValidation = None,
                x_default = 0
                ),
            'moneda': Storage(
                dbField = self._D_tablas.cfdis.moneda,
                fnValidation = None
                ),
            'tipocambio': Storage(
                dbField = self._D_tablas.cfdis.tipocambio,
                fnValidation = None
                ),
            'total': Storage(
                dbField = self._D_tablas.cfdis.total,
                fnValidation = None
                ),
            'tipodecomprobante': Storage(
                dbField = self._D_tablas.cfdis.tipodecomprobante,
                fnValidation = None
                ),
            'metodopago': Storage(
                dbField = self._D_tablas.cfdis.metodopago,
                fnValidation = None
                ),
            'lugarexpedicion': Storage(
                dbField = self._D_tablas.cfdis.lugarexpedicion,
                fnValidation = None
                ),
            'impuestos.totalimpuestosretenidos': Storage(
                dbField = self._D_tablas.cfdis.totalimpuestosretenidos,
                fnValidation = None,
                x_default = 0
                ),
            'impuestos.totalimpuestostrasladados': Storage(
                dbField = self._D_tablas.cfdis.totalimpuestostrasladados,
                fnValidation = None,
                x_default = 0
                ),
            'cfdirelacionados.tiporelacion': Storage(
                dbField = self._D_tablas.cfdis.tiporelacion,
                fnValidation = None
                ),
            'emisor.rfc': Storage(
                dbField = self._D_tablas.cfdis.emisorrfc,
                fnValidation = None
                ),
            'emisor.nombre': Storage(
                dbField = self._D_tablas.cfdis.emisornombrerazonsocial,
                fnValidation = None
                ),
            'emisor.regimenfiscal': Storage(
                dbField = self._D_tablas.cfdis.emisorregimenfiscal,
                fnValidation = None
                ),
            'receptor.rfc': Storage(
                dbField = self._D_tablas.cfdis.receptorrfc,
                fnValidation = None
                ),
            'receptor.nombre': Storage(
                dbField = self._D_tablas.cfdis.receptornombrerazonsocial,
                fnValidation = None
                ),
            'receptor.usocfdi': Storage(
                dbField = self._D_tablas.cfdis.receptorusocfdi,
                fnValidation = None
                ),
            'complemento.timbrefiscaldigital.uuid': Storage(
                dbField = self._D_tablas.cfdis.uuid,
                fnValidation = None
                ),
            'complemento.timbrefiscaldigital.fechatimbrado': Storage(
                dbField = self._D_tablas.cfdis.fechatimbrado_str,
                fnValidation = None
                ),
            }

        self._D_campos_relacion2 = {
            'emisor.calle': Storage(
                dbField = self._D_tablas.cfdis2.emisorcalle,
                fnValidation = None
                ),
            'emisor.noexterior': Storage(
                dbField = self._D_tablas.cfdis2.emisornumexterior,
                fnValidation = None
                ),
            'emisor.nointerior': Storage(
                dbField = self._D_tablas.cfdis2.emisornuminterior,
                fnValidation = None
                ),
            'emisor.colonia': Storage(
                dbField = self._D_tablas.cfdis2.emisorcolonia,
                fnValidation = None
                ),
            'emisor.localidad': Storage(
                dbField = self._D_tablas.cfdis2.emisorlocalidad,
                fnValidation = None
                ),
            'emisor.municipio': Storage(
                dbField = self._D_tablas.cfdis2.emisormunicipio,
                fnValidation = None
                ),
            'emisor.estado': Storage(
                dbField = self._D_tablas.cfdis2.emisorestado,
                fnValidation = None
                ),
            'emisor.pais': Storage(
                dbField = self._D_tablas.cfdis2.emisorpais,
                fnValidation = None
                ),
            'emisor.cp': Storage(
                dbField = self._D_tablas.cfdis2.emisorcodigopostal,
                fnValidation = None
                ),
            'receptor.calle': Storage(
                dbField = self._D_tablas.cfdis2.receptorcalle,
                fnValidation = None
                ),
            'receptor.noexterior': Storage(
                dbField = self._D_tablas.cfdis2.receptornumexterior,
                fnValidation = None
                ),
            'receptor.nointerior': Storage(
                dbField = self._D_tablas.cfdis2.receptornuminterior,
                fnValidation = None
                ),
            'receptor.colonia': Storage(
                dbField = self._D_tablas.cfdis2.receptorcolonia,
                fnValidation = None
                ),
            'receptor.localidad': Storage(
                dbField = self._D_tablas.cfdis2.receptorlocalidad,
                fnValidation = None
                ),
            'receptor.municipio': Storage(
                dbField = self._D_tablas.cfdis2.receptormunicipio,
                fnValidation = None
                ),
            'receptor.estado': Storage(
                dbField = self._D_tablas.cfdis2.receptorestado,
                fnValidation = None
                ),
            'receptor.pais': Storage(
                dbField = self._D_tablas.cfdis2.receptorpais,
                fnValidation = None
                ),
            'receptor.cp': Storage(
                dbField = self._D_tablas.cfdis2.receptorcodigopostal,
                fnValidation = None
                ),
            'certificado': Storage(
                dbField = self._D_tablas.cfdis2.certificado,
                fnValidation = None
                ),
            'nocertificado': Storage(
                dbField = self._D_tablas.cfdis2.nocertificado,
                fnValidation = None
                ),
            'complemento.timbrefiscaldigital.version': Storage(
                dbField = self._D_tablas.cfdis2.versiontimbre,
                fnValidation = None
                ),
            'complemento.timbrefiscaldigital.rfcprovcertif': Storage(
                dbField = self._D_tablas.cfdis2.rfcprovcertif,
                fnValidation = None
                ),
            'complemento.timbrefiscaldigital.sellocfd': Storage(
                dbField = self._D_tablas.cfdis2.sellocfd,
                fnValidation = None
                ),
            'complemento.timbrefiscaldigital.sellosat': Storage(
                dbField = self._D_tablas.cfdis2.sellosat,
                fnValidation = None
                ),
            'complemento.timbrefiscaldigital.nocertificadosat': Storage(
                dbField = self._D_tablas.cfdis2.nocertificadosat,
                fnValidation = None
                ),
            }

        return None

    # noinspection PyMethodParameters
    def _decorador_define_mensajeProceso(s_enEjecucion = ""):
        """ Decorador que define una variable en la clase que identifica el proceso, para usarse en caso de un error

        @return:
        @rtype:
        """
        def decorador_wrapper(func):
            def funcion_compuesta(self, *args, **kwargs):
                self._D_procesoMensaje.L_enEjecucionBkp.append(self._D_procesoMensaje.s_enEjecucion)
                self._D_procesoMensaje.s_enEjecucion = s_enEjecucion
                self._D_procesoMensaje.s_descripcion = u""
                x_retVal = func(self, *args, **kwargs)
                self._D_procesoMensaje.s_enEjecucion = self._D_procesoMensaje.L_enEjecucionBkp.pop()
                self._D_procesoMensaje.s_descripcion = u""
                return x_retVal

            return funcion_compuesta

        return decorador_wrapper

    def _generar_mensajeProceso(
            self,
            s_descripcion = None
            ):

        _s_enEjecucion = " - ".join(self._D_procesoMensaje.L_enEjecucionBkp)

        return "%s [%s] %s | %s" % (
            self._D_procesoMensaje.s_iniciador, self._D_procesoMensaje.s_paso,
            _s_enEjecucion + " - " + self._D_procesoMensaje.s_enEjecucion,
            s_descripcion or self._D_procesoMensaje.s_descripcion
            )

    # noinspection PyArgumentList
    @_decorador_define_mensajeProceso(s_enEjecucion = u"Inserta Cliente/Proveedor")
    def _insertar_actualizar_cfdi2(
            self,
            n_cfdi_id
            ):
        _D_return = FUNC_RETURN()

        if n_cfdi_id:

            _dbRows_cfdi2 = dbc01(dbc01.tcfdis2.cfdi_id == n_cfdi_id).select(dbc01.tcfdis2.ALL)

            if len(_dbRows_cfdi2) == 0:
                # Si no existe el registro en CFDI2, se asocia un diccionario, sin el id definido
                #  para insertar un nuevo registro
                _dbRow_cfdi2 = Storage(
                    cfdi_id = n_cfdi_id
                    )

            else:
                # Si sí existe el registro se asocia al row para actualizar su información
                #  usando el último insertado
                _dbRow_cfdi2 = _dbRows_cfdi2.last()

            # Se inserta la tabla2 de CFDI con los campos en _D_campos_relacion2 + el cfdi_id
            _D_returnsData = STV_LIB_DB.INSERTAR_ACTUALIZAR_JSON(
                O_json_data = self._D_cfdi,
                D_campos_relacion = self._D_campos_relacion2,
                dbTabla = self._D_tablas.cfdis2,
                O_row = _dbRow_cfdi2,
                )

            # TODO capturar errores en la inserción en TCFDIS2

        else:
            pass

        return _D_return

    # noinspection PyArgumentList
    @_decorador_define_mensajeProceso(s_enEjecucion = u"Inserta Cliente/Proveedor")
    def _insertar_clienteproveedor(
            self,
            D_elemento,
            dbTabla,
            ):
        """ Inserta un cliente o proveedor.

        @param D_elemento:
        @type D_elemento:
        @param dbTabla:
        @type dbTabla:
        @return:
        @rtype:
        """
        _D_returnErroresPrioridad = D_RETURN_ERRORES_PRIORIDAD()

        _D_fieldsInsertClienteProveedor = Storage(
            empresa_id = self._s_empresa_id,
            nombrecorto = D_elemento.nombre
            )

        _D_campos_relacion = {
            'rfc' :              Storage(dbField = dbTabla.rfc, fnValidation = None),
            'nombre' :           Storage(dbField = dbTabla.razonsocial, fnValidation = None),
            # 'regimenfiscal' :    Storage(dbField = dbTabla.regimenfiscal, fnValidation = None),
            }
        _D_returnsData = STV_LIB_DB.INSERTAR_ACTUALIZAR_JSON(
            O_json_data = D_elemento,
            D_campos_relacion = _D_campos_relacion,
            dbTabla = dbTabla,
            O_row = _D_fieldsInsertClienteProveedor
            )

        if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
            _D_returnErroresPrioridad.ultimo_id = _D_returnsData.n_id
            _D_returnErroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())
        else:
            _D_returnErroresPrioridad.ultimo_id = None
            _D_returnErroresPrioridad.critica(_D_returnsData, self._generar_mensajeProceso())

        return _D_returnErroresPrioridad

    # noinspection PyArgumentList
    @_decorador_define_mensajeProceso(s_enEjecucion = u"Inserta ProdServ")
    def _insertar_prodserv(
            self,
            D_cfdi_concepto,
            D_fieldsInsertConcepto,
            ):
        """ Inserta un cliente o proveedor.

        """
        _dbTabla = dbc01.tempresa_prodserv

        _D_return = D_NOTIFICACIONMENSAJE(s_proceso = self._generar_mensajeProceso())
        _D_return.mensaje_al_insertar(str(_dbTabla), 'codigo', D_cfdi_concepto.noidentificacion)

        _D_fieldsInsertProdServ = Storage(
            empresa_id = self._s_empresa_id,
            unidad_id = D_fieldsInsertConcepto.unidad_id,
            prodserv_id = D_fieldsInsertConcepto.prodserv_id,
            )

        _D_campos_relacion = {
            'noidentificacion' :        Storage(dbField = _dbTabla.codigo, fnValidation = None),
            'descripcion' :             Storage(dbField = _dbTabla.descripcion, fnValidation = None),
            }

        if D_cfdi_concepto.claveunidad == TUNIDADES.SERVICIO:
            _D_fieldsInsertProdServ.tipo = TEMPRESA_PRODSERV.E_TIPO.SERVICIO
        elif D_cfdi_concepto.claveunidad == TUNIDADES.BONIFICACION:
            _D_fieldsInsertProdServ.tipo = TEMPRESA_PRODSERV.E_TIPO.BONIFICACION
        else:
            _D_fieldsInsertProdServ.tipo = TEMPRESA_PRODSERV.E_TIPO.PRODUCTO

        _D_return = STV_LIB_DB.INSERTAR_ACTUALIZAR_JSON(
            D_cfdi_concepto,
            _D_campos_relacion,
            _dbTabla,
            _D_fieldsInsertProdServ,
            s_proceso = self._generar_mensajeProceso()
            )

        return _D_return

    # noinspection PyArgumentList
    @_decorador_define_mensajeProceso(s_enEjecucion = u"Validación Empresa en Cuenta")
    def _validar_empresaEnCuenta(
            self,
            ):
        """ Se validan condiciones de la empresa en la cuenta

        @return:
        @rtype:
        """

        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.NOK_ERROR,
            s_msgError = "",
            )

        if not auth.is_logged_in():
            _D_return.E_return = stvfwk2_e_RETURN.NOK_LOGIN
            _D_return.s_msgError = u"No tiene session activa."
        # No se validan permisos ya que se espera que los permisos se esten validando antes de llamar esta función
        # elif not request.stv_fwk_permissions.hasPermission(
        #     x_code = request.stv_fwk_permissions.btn_none,
        #     ps_application = D_stvSiteCfg.s_nameBasedApp,
        #     ps_controller = '310cfdi',
        #     ps_function = 'importar_cfdi'
        #     ):
        #     _D_return.E_return = stvfwk2_e_RETURN.NOK_PERMISOS
        #     _D_return.s_msgError = "No tiene permisos para importar CFDIs."

        else:

            # Se valida si la empresa esta relacionada con la cuenta
            _dbRowsCuentaEmpresas = db(
                (db.tcuenta_empresas.cuenta_id == self._s_cuenta_id)
                & (db.tcuenta_empresas.empresa_id == self._s_empresa_id)
                ).select(
                    db.tcuenta_empresas.id)

            if not _dbRowsCuentaEmpresas:
                _D_return.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
                _D_return.s_msgError = u"Empresa no asociada a cuenta"
            else:

                # Se valida si la empresa corresponde al RFC
                _dbRowsEmpresas = dbc01(
                    (dbc01.tempresas.rfc == self._s_rfc)
                    & (dbc01.tempresas.id == self._s_empresa_id)
                    ).select(
                        dbc01.tempresas.id,
                        dbc01.tempresas.rfc_publico,
                        dbc01.tempresas.rfc_publico_extranjeros
                        )

                if not _dbRowsEmpresas:
                    _D_return.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
                    _D_return.s_msgError = u"Empresa no coincide con RFC"
                else:

                    self._dbRow_empresa = _dbRowsEmpresas.first()

                    _n_cuenta_empresa_id = _dbRowsCuentaEmpresas.first().id

                    # Se hace verificación de pagos
                    _n_mes = int(request.utcnow.month)  # utcnow se usa en consultas a la base de datos
                    _n_anio = int(request.utcnow.year)  # utcnow se usa en consultas a la base de datos
                    _dbRows = db(
                        (db.tcuenta_empresa_pagos.cuenta_empresa_id == _n_cuenta_empresa_id)
                        & (
                            (db.tcuenta_empresa_pagos.anio > _n_anio)
                            | (
                                (db.tcuenta_empresa_pagos.anio == _n_anio)
                                & (db.tcuenta_empresa_pagos.mes >= _n_mes)
                                )
                            )
                        ).select(
                            db.tcuenta_empresa_pagos.id
                            )

                    self._b_pagosAlDia = bool(_dbRows)  # Se registra si los pagos estan al día o no

                    _D_return.E_return = stvfwk2_e_RETURN.OK

        return _D_return

    # noinspection PyArgumentList
    @_decorador_define_mensajeProceso(s_enEjecucion = u"Validar UUID")
    def _validar_uuid(
            self,
            ):

        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.NOK_ERROR,
            s_msgError = "",
            n_cfdi_id = 0
            )

        if not self._D_cfdi.complemento or not self._D_cfdi.complemento.timbrefiscaldigital:
            _D_return.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
            _D_return.s_msgError = u"CFDI no esta timbrado ó firma no reconocida"

        else:
            # Se verifica que para la empresa el uuid no exista ya
            _dbRows = dbc01(
                (self._D_tablas.cfdis.empresa_id == self._s_empresa_id)
                & (self._D_tablas.cfdis.uuid == self._D_cfdi.complemento.timbrefiscaldigital.uuid)
                ).select(
                    self._D_tablas.cfdis.id)

            _s_id_uuid = 'cfdi_' + str(self._s_empresa_id) + "_" + self._D_cfdi.complemento.timbrefiscaldigital.uuid
            _s_id_uuid_cache = cache.ram(_s_id_uuid, lambda: False, time_expire=60)

            if _dbRows or _s_id_uuid_cache:
                _D_return.E_return = stvfwk2_e_RETURN.OK_WARNING
                _D_return.s_msgError = u"UUID ya fue ingresado para la empresa"
                _D_return.n_cfdi_id = _dbRows.first().id
            else:
                # No existe registro
                cache.ram(_s_id_uuid, None)
                cache.ram(_s_id_uuid, lambda: self._D_cfdi.complemento.timbrefiscaldigital.uuid, time_expire=60)

                if (
                        (self._E_tipoImportacion == STV_CFDI_INTEGRACION.E_TIPOIMPORTACION.CFDI_EMITIDO)
                        and (str(self._D_cfdi.emisor.rfc) != self._s_rfc)
                        and (str(self._D_cfdi.receptor.rfc) != 'AAA-010101-AAA')  # Este RFC se usa para pruebas
                        ):
                    _D_return.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
                    _D_return.s_msgError = (
                        u"RFC de la empresa no coincide con emisor y se está importando un CFDI emitido"
                        )

                elif (
                        (self._E_tipoImportacion == STV_CFDI_INTEGRACION.E_TIPOIMPORTACION.CFDI_RECIBIDO)
                        and (str(self._D_cfdi.receptor.rfc) != self._s_rfc)
                        and (str(self._D_cfdi.receptor.rfc) != 'AAA-010101-AAA')  # Este RFC se usa para pruebas
                        ):
                    _D_return.E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS
                    _D_return.s_msgError = (
                        u"RFC de la empresa no coincide con receptor y se está importando un CFDI recibido"
                        )
                else:
                    _D_return.E_return = stvfwk2_e_RETURN.OK

        return _D_return

    # noinspection PyArgumentList
    @_decorador_define_mensajeProceso(s_enEjecucion = u"Importar")
    def importar(
            self,
            xml_cfdi = "",
            D_cfdi = None,
            D_attr = None,
            E_tipoImportacion = None,
            E_generadoPor = None
            ):
        """ Valida la informacion e inserta el CFDI

        Las validaciones son:
        - Si empresa corresponde a rfc
        - Si empresa esta en cuenta
        - Si los pagos estan en pie. Si no estan en pie, registra el CFDI pero no lo relaciona
        - Si el UUID para la empresa no ha sido capturado, contrario verifica que contenga la misma información
        -   Si la informacion es la misma regresa un 0, con el texto de que ya se encontraba registrado < TODO
        -   Si la informacion es diferente y no se ha registrado la poliza, se actualiza la informacion < TODO
        -   Si ya se registro la poliza, regresa un error, y registra el intento en el sistema < TODO
        - Registra la informacion del CFDI e inicia la asociacion con los catalogos relacionados

        Ejemplo:

        @param E_tipoImportacion:
        @type E_tipoImportacion:
        @param xml_cfdi:
        @type xml_cfdi:
        @param D_cfdi:
        @type D_cfdi:
        @param D_attr:
        @type D_attr:
        @param E_generadoPor:
        @type E_generadoPor: TCFDIS.E_GENERADO_POR
        @return:
        @rtype:
        """

        _ = D_attr

        # Se establece el valor por default de las variables
        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.NOK_ERROR,
            s_msgError = "",
            n_cfdi_id = 0,
            D_cfdi = Storage()
            )
        self._n_cfdi_id = None

        _n_procesoinicio_S = time.time()

        # if True:
        try:
            self._D_procesoMensaje.s_paso = u"CFDI.00.000"

            self._E_tipoImportacion = E_tipoImportacion
            self._incializar_tablas()

            if self._dbRow_empresa:
                # No hay problema, ya se hicieron las validaciones
                pass
            else:
                _D_returnsData = self._validar_empresaEnCuenta()
                if stvfwk2_e_RETURN.OK != _D_returnsData.E_return:
                    _D_return.E_return = _D_returnsData.E_return
                    _D_return.s_msgError = _D_returnsData.s_msgError
                    return _D_return
                else:
                    pass

            if not D_cfdi and xml_cfdi:
                self._D_procesoMensaje.s_paso = u"CFDI.01.000"

                self._O_cfdi = STV_XML2CFDI()
                _D_returnsData = self._O_cfdi.importar_string(xml_cfdi)
                if stvfwk2_e_RETURN.OK != _D_returnsData.E_return:
                    _D_return.E_return = _D_returnsData.E_return
                    _D_return.s_msgError = _D_returnsData.s_msgError
                    return _D_return
                else:
                    pass

                self._D_cfdi = self._O_cfdi.Comprobante
            else:
                self._D_cfdi = D_cfdi
            _D_return.D_cfdi = self._D_cfdi

            _D_returnsData = self._validar_uuid()
            if stvfwk2_e_RETURN.OK != _D_returnsData.E_return:
                if (stvfwk2_e_RETURN.OK_WARNING == _D_returnsData.E_return) \
                        and _D_returnsData.n_cfdi_id:
                    self._insertar_actualizar_cfdi2(_D_returnsData.n_cfdi_id)
                else:
                    pass
                _D_return.update(_D_returnsData)
                return _D_return
            else:
                pass

            # Se define el storage donde se van a almacenar los errores durante el proceso en forma de lista
            #  de acuerdo a prioridad
            _D_erroresPrioridad = D_RETURN_ERRORES_PRIORIDAD()

            # ##
            # ## Se inserta el maestro de CFDI
            # ##

            # Se agrega el campo que identifica a la empresa como valor por default en la inserción
            _D_fieldsInsert = Storage(
                empresa_id = self._s_empresa_id,
                tipocomprobante_id = None,
                generado_por = E_generadoPor or TCFDIS.E_GENERADO_POR.IMPORTADO,
                # and more
                )

            # Se asocia el tipo de comprobante con la tabla
            self._D_procesoMensaje.s_paso = u"CFDI.10.020: Asociar tipo de comprobante"
            _D_returnsData = STV_LIB_DB.CACHE_ASOCIAR_TABLA(
                s_cache_id = 'cfdi_asociar_tipocomprobante' + str(self._D_cfdi.tipodecomprobante),
                n_time_expire = 86400,
                dbTabla = db01.ttiposcomprobantes,
                s_nombreCampo = 'c_tipodecomprobante',
                s_datoBuscar = self._D_cfdi.tipodecomprobante,
                )

            if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                _D_fieldsInsert.tipocomprobante_id = _D_returnsData.n_id
                _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())
            else:
                STV_LIB_DB.CACHE_BORRAR_ULTIMO()
                _D_fieldsInsert.tipocomprobante_id = None
                _D_erroresPrioridad.critica(_D_returnsData, self._generar_mensajeProceso())

            if self._E_tipoImportacion == STV_CFDI_INTEGRACION.E_TIPOIMPORTACION.CFDI_EMITIDO:
                # Solamente si es emisor se puede asociar serie

                _D_fieldsInsert.update(
                    Storage(
                        empresa_serie_id = None,
                        empresa_plaza_sucursal_cajachica_id = None,
                        )
                    )

                # Se asocia la serie con la tabla
                self._D_procesoMensaje.s_paso = u"CFDI.10.010: Asociar serie"
                _D_returnsData = STV_LIB_DB.CACHE_ASOCIAR_TABLA(
                    s_cache_id = 'cfdi_asociar_empresa' + str(self._s_empresa_id) + 'serie' + str(self._D_cfdi.serie),
                    n_time_expire = 86400,
                    dbTabla = dbc01.tempresa_series,
                    s_nombreCampo = 'serie',
                    s_datoBuscar = self._D_cfdi.serie,
                    qry_maestro = (dbc01.tempresa_series.empresa_id == self._s_empresa_id),
                    )

                if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                    _D_fieldsInsert.empresa_serie_id = _D_returnsData.n_id
                    _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())

                    # Se asocia la caja chica con serie de ingreso
                    self._D_procesoMensaje.s_paso = u"CFDI.10.015: Asociar caja chica"

                    if _D_fieldsInsert.tipocomprobante_id in (TTIPOSCOMPROBANTES.INGRESO,):

                        _D_returnsData = STV_LIB_DB.CACHE_ASOCIAR_TABLA(
                            s_cache_id = (
                                'cfdi_asociar_cajachica_empresaingreso' + str(self._s_empresa_id)
                                + 'serie' + str(_D_fieldsInsert.empresa_serie_id)
                                ),
                            n_time_expire = 86400,
                            dbTabla = dbc01.tempresa_plaza_sucursal_cajaschicas,
                            s_nombreCampo = 'empresa_serie_ingreso_id',
                            s_datoBuscar = _D_fieldsInsert.empresa_serie_id,
                            )

                    elif _D_fieldsInsert.tipocomprobante_id in (TTIPOSCOMPROBANTES.EGRESO,):

                        _D_returnsData = STV_LIB_DB.CACHE_ASOCIAR_TABLA(
                            s_cache_id = (
                                'cfdi_asociar_cajachica_empresaegreso' + str(self._s_empresa_id)
                                + 'serie' + str(_D_fieldsInsert.empresa_serie_id)
                                ),
                            n_time_expire = 86400,
                            dbTabla = dbc01.tempresa_plaza_sucursal_cajaschicas,
                            s_nombreCampo = 'empresa_serie_egreso_id',
                            s_datoBuscar = _D_fieldsInsert.empresa_serie_id,
                            )

                    elif _D_fieldsInsert.tipocomprobante_id in (TTIPOSCOMPROBANTES.PAGO,):

                        _D_returnsData = STV_LIB_DB.CACHE_ASOCIAR_TABLA(
                            s_cache_id = (
                                'cfdi_asociar_cajachica_empresapago' + str(self._s_empresa_id)
                                + 'serie' + str(_D_fieldsInsert.empresa_serie_id)
                                ),
                            n_time_expire = 86400,
                            dbTabla = dbc01.tempresa_plaza_sucursal_cajaschicas,
                            s_nombreCampo = 'empresa_serie_pago_id',
                            s_datoBuscar = _D_fieldsInsert.empresa_serie_id,
                            )

                    else:
                        pass

                    if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                        _D_fieldsInsert.empresa_plaza_sucursal_cajachica_id = _D_returnsData.n_id
                        _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())
                    else:
                        STV_LIB_DB.CACHE_BORRAR_ULTIMO()

                        # Se asocia la caja chica por código postal
                        self._D_procesoMensaje.s_paso = u"CFDI.10.015: Asociar caja chica por lugar de expedición"
                        _D_returnsData = STV_LIB_DB.CACHE_ASOCIAR_TABLA(
                            s_cache_id = (
                                'cfdi_asociar_cajachica_empresa' + str(self._s_empresa_id)
                                + 'lugarexpedicion' + str(self._D_cfdi.lugarexpedicion)
                                ),
                            n_time_expire = 86400,
                            dbTabla = dbc01.tempresa_plaza_sucursal_cajaschicas,
                            s_nombreCampo = 'bansuspender',
                            s_datoBuscar = False,
                            qry_maestro = (
                                (dbc01.tempresa_plazas.empresa_id == self._s_empresa_id)
                                & (dbc01.tempresa_plaza_sucursales.codigopostal == str(self._D_cfdi.lugarexpedicion))
                                & (dbc01.tempresa_plaza_sucursales.empresa_plaza_id == dbc01.tempresa_plazas.id)
                                & (
                                    dbc01.tempresa_plaza_sucursal_cajaschicas.empresa_plaza_sucursal_id
                                    == dbc01.tempresa_plaza_sucursales.id
                                    )
                                ),
                            )

                        if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                            _D_fieldsInsert.empresa_plaza_sucursal_cajachica_id = _D_returnsData.n_id
                            _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())
                        else:
                            STV_LIB_DB.CACHE_BORRAR_ULTIMO()
                            _D_fieldsInsert.empresa_plaza_sucursal_cajachica_id = None
                            _D_erroresPrioridad.alta(_D_returnsData, self._generar_mensajeProceso())

                else:
                    STV_LIB_DB.CACHE_BORRAR_ULTIMO()
                    _D_fieldsInsert.empresa_serie_id = None
                    _D_erroresPrioridad.alta(_D_returnsData, self._generar_mensajeProceso())
            else:
                # Si no es emisor, no se puede asociar la serie, ni la caja chica

                pass

            # Se asocia la forma de pago con la tabla
            self._D_procesoMensaje.s_paso = u"CFDI.10.030: Asociar forma de pago"
            if self._D_cfdi.formapago:
                _D_returnsData = STV_LIB_DB.CACHE_ASOCIAR_TABLA(
                    s_cache_id = 'cfdi_asociar_formapago' + str(self._D_cfdi.formapago),
                    n_time_expire = 86400,
                    dbTabla = db01.tformaspago,
                    s_nombreCampo = 'c_formapago',
                    s_datoBuscar = self._D_cfdi.formapago,
                    )
                if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                    _D_fieldsInsert.formapago_id = _D_returnsData.n_id
                    _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())
                else:
                    STV_LIB_DB.CACHE_BORRAR_ULTIMO()
                    _D_fieldsInsert.formapago_id = None
            else:
                _D_fieldsInsert.formapago_id = None

            if not _D_fieldsInsert.formapago_id:
                # Si tipo de comprobante es Ingreso o Egreso, la forma de pago y el metodo de pago es obligatoria
                if _D_fieldsInsert.tipocomprobante_id in (TTIPOSCOMPROBANTES.EGRESO, TTIPOSCOMPROBANTES.INGRESO):
                    _D_erroresPrioridad.alta(_D_returnsData, self._generar_mensajeProceso())
                else:
                    _D_erroresPrioridad.baja(_D_returnsData, self._generar_mensajeProceso())
            else:
                pass

            # Se asocia el método de pago con la tabla
            self._D_procesoMensaje.s_paso = u"CFDI.10.040: Asociar método de pago"
            if self._D_cfdi.metodopago:
                _D_returnsData = STV_LIB_DB.CACHE_ASOCIAR_TABLA(
                    s_cache_id = 'cfdi_asociar_metodopago' + str(self._D_cfdi.metodopago),
                    n_time_expire = 86400,
                    dbTabla = db01.tmetodospago,
                    s_nombreCampo = 'c_metodopago',
                    s_datoBuscar = self._D_cfdi.metodopago,
                    )
                if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                    _D_fieldsInsert.metodopago_id = _D_returnsData.n_id
                    _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())
                else:
                    STV_LIB_DB.CACHE_BORRAR_ULTIMO()
                    _D_fieldsInsert.metodopago_id = None
            else:
                _D_fieldsInsert.metodopago_id = None

            if not _D_fieldsInsert.metodopago_id:
                # Si tipo de comprobante es Ingreso o Egreso, la forma de pago y el metodo de pago es obligatoria
                if _D_fieldsInsert.tipocomprobante_id in (TTIPOSCOMPROBANTES.EGRESO, TTIPOSCOMPROBANTES.INGRESO):
                    _D_erroresPrioridad.alta(_D_returnsData, self._generar_mensajeProceso())
                else:
                    _D_erroresPrioridad.baja(_D_returnsData, self._generar_mensajeProceso())
            else:
                pass

            # Se asocia la moneda con la tabla
            self._D_procesoMensaje.s_paso = u"CFDI.10.050: Asociar moneda"
            _D_returnsData = STV_LIB_DB.CACHE_ASOCIAR_TABLA(
                s_cache_id = 'cfdi_asociar_moneda' + str(self._D_cfdi.moneda),
                n_time_expire = 86400,
                dbTabla = db01.tmonedas,
                s_nombreCampo = 'c_moneda',
                s_datoBuscar = self._D_cfdi.moneda,
                )
            if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                _D_fieldsInsert.moneda_id = _D_returnsData.n_id
                _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())

                if self._D_cfdi.moneda != 'XXX':
                    _s_cache_id = 'cfdi_asociar_moneda_{empresa}_contpaqi_{moneda}'.format(
                        empresa = str(self._s_empresa_id),
                        moneda = str(self._D_cfdi.moneda)
                        )
                    _D_returnsData = STV_LIB_DB.CACHE_ASOCIAR_TABLA(
                        _s_cache_id,
                        n_time_expire = 86400,
                        dbTabla = dbc01.tempresa_monedascontpaqi,
                        s_nombreCampo = 'moneda_id',
                        s_datoBuscar = _D_fieldsInsert.moneda_id,
                        qry_maestro = (dbc01.tempresa_monedascontpaqi.empresa_id == self._s_empresa_id),
                        )

                    if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                        _D_fieldsInsert.monedacontpaqi_id = _D_returnsData.n_id
                        _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())
                    else:
                        cache.ram(_s_cache_id, None)
                        _D_fieldsInsert.monedacontpaqi_id = None
                        _D_erroresPrioridad.alta(_D_returnsData, self._generar_mensajeProceso())
                else:
                    pass

            else:
                STV_LIB_DB.CACHE_BORRAR_ULTIMO()
                _D_fieldsInsert.moneda_id = None
                # Si tipo de comprobante es Ingreso o Egreso, la forma de pago y el metodo de pago es obligatoria
                if _D_fieldsInsert.tipocomprobante_id in (TTIPOSCOMPROBANTES.EGRESO, TTIPOSCOMPROBANTES.INGRESO):
                    _D_erroresPrioridad.alta(_D_returnsData, self._generar_mensajeProceso())
                else:
                    _D_erroresPrioridad.baja(_D_returnsData, self._generar_mensajeProceso())

            # Se asocia el lugar de expedición con la tabla
            self._D_procesoMensaje.s_paso = u"CFDI.10.060: Asociar lugar de expedición"
            _D_returnsData = STV_LIB_DB.CACHE_ASOCIAR_TABLA(
                s_cache_id = 'cfdi_asociar_lugarexpedicion' + str(self._D_cfdi.lugarexpedicion),
                n_time_expire = 86400,
                dbTabla = db01.tcodigospostales,
                s_nombreCampo = 'c_codigopostal',
                s_datoBuscar = self._D_cfdi.lugarexpedicion,
                )
            if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                _D_fieldsInsert.lugarexpedicion_id = _D_returnsData.n_id
                _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())
            else:
                STV_LIB_DB.CACHE_BORRAR_ULTIMO()
                _D_fieldsInsert.lugarexpedicion_id = None
                _D_erroresPrioridad.alta(_D_returnsData, self._generar_mensajeProceso())

            if self._D_cfdi.cfdirelacionados:

                # Se asocia el tipo relación con la tabla
                self._D_procesoMensaje.s_paso = u"CFDI.10.070: Asociar tipo de relación, ya que existen relacionados"
                _D_returnsData = STV_LIB_DB.CACHE_ASOCIAR_TABLA(
                    s_cache_id = 'cfdi_asociar_tiporelacion' + str(self._D_cfdi.cfdirelacionados.tiporelacion),
                    n_time_expire = 86400,
                    dbTabla = db01.ttiposrelaciones,
                    s_nombreCampo = 'c_tiporelacion',
                    s_datoBuscar = self._D_cfdi.cfdirelacionados.tiporelacion,
                    )
                if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                    _D_fieldsInsert.tiporelacion_id = _D_returnsData.n_id
                    _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())
                else:
                    STV_LIB_DB.CACHE_BORRAR_ULTIMO()
                    _D_fieldsInsert.tiporelacion_id = None
                    # Si tipo de comprobante es Ingreso o Egreso, la forma de pago y el metodo de
                    # pago es obligatoria
                    _D_erroresPrioridad.alta(_D_returnsData, self._generar_mensajeProceso())

            elif _D_fieldsInsert.tipocomprobante_id in (TTIPOSCOMPROBANTES.EGRESO, ):
                # Si no tiene relacionados y es de egreso hay error
                STV_LIB_DB.CACHE_BORRAR_ULTIMO()
                _D_fieldsInsert.tiporelacion_id = None
                _D_returnsData = D_NOTIFICACIONMENSAJE(s_proceso = self._generar_mensajeProceso())
                _D_returnsData.s_msgError = "CFDI de egreso %s, sin documentos relacionados" % (
                    self._D_cfdi.complemento.timbrefiscaldigital.uuid
                    )
                _D_erroresPrioridad.alta(_D_returnsData, self._generar_mensajeProceso())

            else:
                # Se ignora el tiporelacion
                pass

            # Se asocia el uso cfdi con la tabla
            self._D_procesoMensaje.s_paso = u"CFDI.10.080: Asociar receptor uso CFDI"
            _D_returnsData = STV_LIB_DB.CACHE_ASOCIAR_TABLA(
                s_cache_id = 'cfdi_asociar_receptorusocfdi' + str(self._D_cfdi.receptor.usocfdi),
                n_time_expire = 86400,
                dbTabla = db01.tusoscfdi,
                s_nombreCampo = 'c_usocfdi',
                s_datoBuscar = self._D_cfdi.receptor.usocfdi,
                )
            if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                _D_fieldsInsert.receptorusocfdi_id = _D_returnsData.n_id
                _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())
            else:
                STV_LIB_DB.CACHE_BORRAR_ULTIMO()
                _D_fieldsInsert.receptorusocfdi_id = None
                _D_erroresPrioridad.alta(_D_returnsData, self._generar_mensajeProceso())

            self._D_procesoMensaje.s_paso = u"CFDI.20.010: Identificar fecha"
            if self._D_cfdi.fecha:
                try:
                    _D_fieldsInsert.fecha = datetime.datetime.strptime(self._D_cfdi.fecha, '%Y-%m-%dT%H:%M:%S')
                    _D_fieldsInsert.dia = _D_fieldsInsert.fecha.date()
                    _D_fieldsInsert.hora = _D_fieldsInsert.fecha.time()
                except Exception as _O_excepcion:
                    _D_returnsData = D_NOTIFICACIONMENSAJE(
                        n_id = 0,
                        s_msgError = ('No se pudo asociar la fecha %s' % self._D_cfdi.fecha),
                        s_msgExcepcion = ('Fecha:%s' % str(_O_excepcion)),
                        s_msgLastSQL = '',
                        E_return = stvfwk2_e_RETURN.NOK_ERROR,
                        s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.VALIDAR % (
                            str(self._D_tablas.cfdis), 'fecha', self._D_cfdi.fecha, "", "", ""
                            ),
                        )
                    _D_erroresPrioridad.alta(_D_returnsData, self._generar_mensajeProceso())
            else:
                _D_erroresPrioridad.alta(
                    D_NOTIFICACIONMENSAJE(
                        n_id = 0,
                        s_msgError = "Fecha no esta definida en el cfdi",
                        s_msgExcepcion = "",
                        s_msgLastSQL = "",
                        E_return = stvfwk2_e_RETURN.NOK_ERROR,
                        s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.VALIDAR % (
                            str(self._D_tablas.cfdis), 'fecha', self._D_cfdi.fecha_str, "", "", ""
                            ),
                        s_proceso = self._generar_mensajeProceso()
                        ),
                    s_proceso = self._generar_mensajeProceso()
                    )

            self._D_procesoMensaje.s_paso = u"CFDI.20.020: Identificar fecha de timbrado"
            if self._D_cfdi.complemento.timbrefiscaldigital.fechatimbrado:
                try:
                    _D_fieldsInsert.fechatimbrado = datetime.datetime.strptime(
                        self._D_cfdi.complemento.timbrefiscaldigital.fechatimbrado, '%Y-%m-%dT%H:%M:%S'
                        )
                except Exception as _O_excepcion:
                    _D_returnsData = D_NOTIFICACIONMENSAJE(
                        n_id = 0,
                        s_msgError = 'No se pudo asociar la fecha de timbrado %s' % (
                            self._D_cfdi.complemento.timbrefiscaldigital.fechatimbrado
                            ),
                        s_msgExcepcion = ('Fecha Timbrado:%s' % str(_O_excepcion)),
                        s_msgLastSQL = '',
                        E_return = stvfwk2_e_RETURN.NOK_ERROR,
                        s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.VALIDAR % (
                            str(self._D_tablas.cfdis), 'fechatimbrado',
                            self._D_cfdi.complemento.timbrefiscaldigital.fechatimbrado, "", "", ""
                            ),
                        )
                    _D_erroresPrioridad.alta(_D_returnsData, self._generar_mensajeProceso())
            else:
                _D_erroresPrioridad.alta(
                    D_NOTIFICACIONMENSAJE(
                        n_id = 0,
                        s_msgError = "Fecha no esta definida en el cfdi",
                        s_msgExcepcion = "",
                        s_msgLastSQL = "",
                        E_return = stvfwk2_e_RETURN.NOK_ERROR,
                        s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.VALIDAR % (
                            str(self._D_tablas.cfdis), 'fechatimbrado',
                            self._D_cfdi.complemento.timbrefiscaldigital.fechatimbrado, "", "", ""
                            ),
                        s_proceso = self._generar_mensajeProceso()
                        ),
                    s_proceso = self._generar_mensajeProceso()
                    )

            if self._E_tipoImportacion == STV_CFDI_INTEGRACION.E_TIPOIMPORTACION.CFDI_EMITIDO:
                # Si el RFC de la empresa corresponde al emisor, se asocia el RFC del receptor a clientes
                _D_fieldsInsert.rol = TCFDIS.E_ROL.EMISOR
                _D_fieldsInsert.emisorproveedor_id = None

                self._D_procesoMensaje.s_paso = u"CFDI.30.010: Asociar receptor con clientes"
                _D_returnsData = STV_LIB_DB.CACHE_ASOCIAR_TABLA(
                    s_cache_id = (
                        'cfdi_asociar_receptorrfc_' + str(self._s_empresa_id)
                        + '_' + str(self._D_cfdi.receptor.rfc)
                        ),
                    n_time_expire = 86400,
                    dbTabla = dbc01.tempresa_clientes,
                    s_nombreCampo = 'rfc',
                    s_datoBuscar = self._D_cfdi.receptor.rfc,
                    qry_maestro = (dbc01.tempresa_clientes.empresa_id == self._s_empresa_id),
                    )

                # Si se encontraron más de un registro con el RFC, se asocia por razon social también
                if (_D_returnsData.n_len_dbRows > 1) \
                        or (self._dbRow_empresa.rfc_publico == self._D_cfdi.receptor.rfc) \
                        or (self._dbRow_empresa.rfc_publico_extranjeros == self._D_cfdi.receptor.rfc):
                    STV_LIB_DB.CACHE_BORRAR_ULTIMO()
                    _D_erroresPrioridad.alta(_D_returnsData, self._generar_mensajeProceso())

                    self._D_procesoMensaje.s_paso = u"CFDI.30.015: RFC repetido, se intenta asociar por razon social"
                    _D_returnsData = STV_LIB_DB.ASOCIAR_TABLA(
                        dbTabla = dbc01.tempresa_clientes,
                        s_nombreCampo = 'razonsocial',
                        s_datoBuscar = self._D_cfdi.receptor.nombre,
                        qry_maestro = (
                            (dbc01.tempresa_clientes.empresa_id == self._s_empresa_id)
                            & (dbc01.tempresa_clientes.rfc == self._D_cfdi.receptor.rfc)
                            ),
                        )

                if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                    _D_fieldsInsert.receptorcliente_id = _D_returnsData.n_id
                    _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())
                else:
                    STV_LIB_DB.CACHE_BORRAR_ULTIMO()
                    _D_erroresPrioridad.alta(_D_returnsData, self._generar_mensajeProceso())

                    self._D_procesoMensaje.s_paso = u"CFDI.30.020: Cliente no identificado, se procede a insertarlo"
                    _D_returnsData = self._insertar_clienteproveedor(
                        self._D_cfdi.receptor,
                        dbc01.tempresa_clientes,
                        )
                    _D_fieldsInsert.receptorcliente_id = _D_returnsData.ultimo_id
                    _D_erroresPrioridad.agregar(_D_returnsData)

            elif self._E_tipoImportacion == STV_CFDI_INTEGRACION.E_TIPOIMPORTACION.CFDI_RECIBIDO:
                # Si el RFC de la empresa corresponde al receptor, Se asocia el emisor rfc con la tabla de proveedores
                _D_fieldsInsert.rol = TCFDIS.E_ROL.RECEPTOR
                _D_fieldsInsert.receptorcliente_id = None

                self._D_procesoMensaje.s_paso = u"CFDI.40.010: Asociar emisor con proveedores"
                _D_returnsData = STV_LIB_DB.CACHE_ASOCIAR_TABLA(
                    s_cache_id = (
                        'cfdi_asociar_emisorrfc_' + str(self._s_empresa_id)
                        + '_' + str(self._D_cfdi.emisor.rfc)
                        ),
                    n_time_expire = 86400,
                    dbTabla = dbc01.tempresa_proveedores,
                    s_nombreCampo = 'rfc',
                    s_datoBuscar = self._D_cfdi.emisor.rfc,
                    qry_maestro = (dbc01.tempresa_clientes.empresa_id == self._s_empresa_id),
                    )
                if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                    _D_fieldsInsert.emisorproveedor_id = _D_returnsData.n_id
                    _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())
                else:
                    STV_LIB_DB.CACHE_BORRAR_ULTIMO()
                    _D_erroresPrioridad.alta(_D_returnsData, self._generar_mensajeProceso())

                    self._D_procesoMensaje.s_paso = u"CFDI.40.020: Proveedor no identificado, se procede a insertarlo"
                    _D_returnsData = self._insertar_clienteproveedor(
                        self._D_cfdi.emisor,
                        dbc01.tempresa_proveedores,
                        )
                    _D_fieldsInsert.emisorproveedor_id = _D_returnsData.ultimo_id
                    _D_erroresPrioridad.agregar(_D_returnsData)

            else:

                # Si no coincide el RFC es un grave error porque no se sabe como contabilizar el CFDI,
                #  pero existe un validación previa
                _D_fieldsInsert.rol = TCFDIS.E_ROL.NO_DEFINIDO
                _D_fieldsInsert.emisorproveedor_id = None
                _D_fieldsInsert.receptorcliente_id = None
                raise ValueError('RFC no coincide, no puede contabilizarse.')

            if self._D_cfdi.complemento and self._D_cfdi.complemento.timbrefiscaldigital \
                    and self._D_cfdi.complemento.timbrefiscaldigital.uuid:
                _D_fieldsInsert.sat_status = TCFDIS.SAT_STATUS.VIGENTE
                _D_fieldsInsert.estado = TCFDIS.E_ESTADO.VIGENTE
            else:
                pass

            self._D_procesoMensaje.s_paso = u"CFDI.50.000: Se inserta la información maestro del CFDI"
            _D_returnsData = STV_LIB_DB.INSERTAR_ACTUALIZAR_JSON(
                O_json_data = self._D_cfdi,
                D_campos_relacion = self._D_campos_relacion,
                dbTabla = self._D_tablas.cfdis,
                O_row = _D_fieldsInsert,
                )

            if _D_returnsData.E_return != stvfwk2_e_RETURN.OK:
                # Si existen errores en la creación del cfdi
                _D_erroresPrioridad.critica(_D_returnsData, self._generar_mensajeProceso())
                self._n_cfdi_id = 0
            else:

                # Si no hay errores en la creación del CFDI continua la inserción de conceptos, pagos y relacionados
                _D_erroresPrioridad.baja(_D_returnsData, self._generar_mensajeProceso())
                self._n_cfdi_id = _D_returnsData.n_id

                self._D_procesoMensaje.s_paso = u"CFDI.60.000: Se asocia CFDI"

                _D_returnsData = TCFDIS.asociar_uuid(self._n_cfdi_id)
                # TODO capturar errores en la asociación y generar notificaciones, por el momento siempre regresa OK

                self._insertar_actualizar_cfdi2(self._n_cfdi_id)

                # TODO Importar domicilio receptor del CFDI

                _D_erroresPrioridad.agregar(self._importar_xml(xml_cfdi, _D_fieldsInsert))

                _D_erroresPrioridad.agregar(self._importar_conceptos(_D_fieldsInsert))

                _D_erroresPrioridad.agregar(self._importar_relacionados(_D_fieldsInsert))

                _D_erroresPrioridad.agregar(self._importar_pagos(_D_fieldsInsert))

            _D_notificacionMensajes = _D_erroresPrioridad.getTodos()

            # Si se existen errores para crear notificaciones
            if _D_notificacionMensajes:

                _s_titutlo = ""

                if _D_erroresPrioridad.CRITICA:
                    # Se regresa cualquier inserción a la base de datos dbc01 antes de guardar las notificaciones
                    dbc01.rollback()
                    _s_titutlo = (
                        u"Al menos un error CRÍTICO no permitió la inserción del CFDI %(serie)s %(folio)s %(uuid)s"
                        ) % self._D_cfdi
                    # TODO Grabar el archivo

                elif _D_erroresPrioridad.ALTA:
                    _s_titutlo = (
                         u"Al menos un error ALTA necesita resolverse sobre el CFDI %(serie)s %(folio)s %(uuid)s"
                         ) % self._D_cfdi
                elif _D_erroresPrioridad.MEDIA:
                    _s_titutlo = (
                        u"Al menos un error MEDIA necesita ser revisados sobre el CFDI %(serie)s %(folio)s %(uuid)s"
                        ) % self._D_cfdi
                elif _D_erroresPrioridad.BAJA:
                    _s_titutlo = (
                        u"Al menos un error BAJA se generó durante la importación del CFDI "
                        + u"%(serie)s %(folio)s %(uuid)s"
                        ) % self._D_cfdi
                elif _D_erroresPrioridad.INFORMACION:
                    _s_titutlo = (
                        u"Se generó INFORMACIÓN durante la inserción del CFDI %(serie)s %(folio)s %(uuid)s"
                        ) % self._D_cfdi
                else:
                    _s_titutlo = (
                        u"Se generaron notificaciones sin prioridad sobre el CFDI %(serie)s %(folio)s %(uuid)s"
                        ) % self._D_cfdi

                _n_procesotime_ms = int(round((time.time() - _n_procesoinicio_S)*1000))

                # Se procede a insertar la notificación
                _D_fieldsInsert = Storage(
                    empresa_id = self._s_empresa_id,
                    titulo = _s_titutlo,
                    descripcion = '',
                    aplicacion_w2p = request.application,
                    controlador_w2p = request.controller,
                    funcion_w2p = request.function,
                    argumentos = '/'.join(request.args),
                    referencia = '',
                    referencia2 = '',
                    ultimosql = '',
                    estatus = TNOTIFICACIONES.ESTATUS.NUEVO,
                    proceso_id = TNOTIFICACIONES.PROCESO_ID.IMPORTAR_CFDI,
                    reprocesar_aplicacion_w2p = request.application,
                    reprocesar_controlador_w2p = request.controller,
                    reprocesar_funcion_w2p = request.function,
                    reprocesar_argumentos = '/'.join(request.args),
                    user_id_asignado = None,
                    historia = 'Creado por %s, tomo %d ms' % (self._s_cuenta_id, _n_procesotime_ms),
                    )
                _n_notificacion_id = self._D_tablas.notificaciones.insert(**_D_fieldsInsert)

                # Si se inserto la notificación correctamente
                if _n_notificacion_id:

                    for _O_mensaje in _D_notificacionMensajes:

                        # Se procede a insertar el mensaje de la notificación
                        _D_fieldsInsertMensaje = Storage(
                            notificacion_id = _n_notificacion_id,
                            codigovalidacion = _O_mensaje.s_codigovalidacion or "",
                            descripcion = _O_mensaje.s_msgError,
                            excepcion = _O_mensaje.s_msgExcepcion,
                            ultimosql = _O_mensaje.msgLastSQL,
                            referencia_id = _O_mensaje.n_referencia_id or _O_mensaje.n_id,
                            codigoerror = _O_mensaje.E_return,
                            prioridad = _O_mensaje.n_prioridad,
                            proceso = _O_mensaje.s_proceso
                            )
                        _n_notificacionmensaje_id = self._D_tablas.notificacion_mensajes.insert(
                            **_D_fieldsInsertMensaje
                            )

                    if _D_erroresPrioridad.CRITICA:

                        # Si existen errores criticos
                        _D_return.E_return = stvfwk2_e_RETURN.NOK_CONDICIONES_INCORRECTAS
                        _D_return.s_msgError = _s_titutlo
                        _D_return.n_cfdi_id = 0
                        cache.ram.clear(regex='^cfdi.*')
                    else:
                        # Todo pasó correctamente y no se generaron notificaciones
                        _D_return.E_return = stvfwk2_e_RETURN.OK
                        # _D_return.s_msgError = u"Importación sin notificaciones, excelente!"
                        _D_return.n_cfdi_id = self._n_cfdi_id

                else:

                    # Si no puedo ser insertada la notificación

                    # Se regresa cualquier inserción a la base de datos dbc01
                    dbc01.rollback()

                    _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                    _D_return.s_msgError = u"No se pudo generar la notificación en el sistema. Se cancelo la operacion."
                    _D_return.n_cfdi_id = 0
                    cache.ram.clear(regex='^cfdi.*')
            else:
                # Todo pasó correctamente y no se generaron notificaciones

                _D_return.E_return = stvfwk2_e_RETURN.OK
                _D_return.s_msgError = u"Importación sin notificaciones, excelente!"
                _D_return.n_cfdi_id = self._n_cfdi_id
                pass
        except Exception as _O_excepcion:
            _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
            _D_return.s_msgError = u'Proceso: %s, Excepcion: %s' % (self._generar_mensajeProceso(), str(_O_excepcion))
            _D_return.n_cfdi_id = 0

            # Se regresa cualquier inserción a la base de datos dbc01
            dbc01.rollback()
            cache.ram.clear(regex='^cfdi.*')

        return _D_return

    # noinspection PyArgumentList
    @_decorador_define_mensajeProceso(s_enEjecucion = u"Conceptos")
    def _importar_conceptos(
            self,
            D_fieldsInsertCfdi
            ):
        """ Valida la informacion e inserta los conceptos de un CFDI
        
        Return:
            D_RETURN_ERRORES_PRIORIDAD
            
        """
        _D_erroresPrioridad = D_RETURN_ERRORES_PRIORIDAD()

        # Se relacionan los nombres en el json con los campos en la tabla tcfdi_conceptos
        _D_campos_relacion_conceptos = {
            'cantidad' :            Storage(dbField = self._D_tablas.conceptos.cantidad, fnValidation = None),
            'claveprodserv' :       Storage(dbField = self._D_tablas.conceptos.claveprodserv, fnValidation = None),
            'claveunidad' :         Storage(dbField = self._D_tablas.conceptos.claveunidad, fnValidation = None),
            'descripcion' :         Storage(dbField = self._D_tablas.conceptos.descripcion, fnValidation = None),
            'descuento' :           Storage(dbField = self._D_tablas.conceptos.descuento, fnValidation = None),
            'importe' :             Storage(dbField = self._D_tablas.conceptos.importe, fnValidation = None),
            'noidentificacion' :    Storage(dbField = self._D_tablas.conceptos.noidentificacion, fnValidation = None),
            'unidad' :              Storage(dbField = self._D_tablas.conceptos.unidad, fnValidation = None),
            'valorunitario' :       Storage(dbField = self._D_tablas.conceptos.valorunitario, fnValidation = None),
            }

        _n_indice = 0
        for _D_cfdi_concepto in self._D_cfdi.conceptos.concepto:

            _n_indice += 1

            # Se asocia el noidentificacion con la tabla de productos y servicios de la empresa
            self._D_procesoMensaje.s_paso = u"CFDI-Concepto%s.00.000: Identificar concepto por tipo de comprobante" % (
                str(_n_indice)
                )
            if D_fieldsInsertCfdi.tipocomprobante_id in (
                    TTIPOSCOMPROBANTES.EGRESO,
                    TTIPOSCOMPROBANTES.INGRESO,
                    TTIPOSCOMPROBANTES.PAGO
                    ):

                self._D_procesoMensaje.s_paso = u"CFDI-Concepto%s.10.010: Identificar clave de producto o servicio" % (
                    str(_n_indice)
                    )
                if _D_cfdi_concepto.claveprodserv:

                    # Se agregan los campos standard con sus valores
                    _D_fieldsInsertConcepto = Storage(
                        cfdi_id = self._n_cfdi_id
                        )

                    # Se asocia el producto o servicio con la tabla
                    self._D_procesoMensaje.s_paso = (
                        u"CFDI-Concepto%s.10.020: Se asocia la clave de producto o servicio"
                        ) % str(_n_indice)

                    _D_returnsData = STV_LIB_DB.ASOCIAR_TABLA(
                        dbTabla = db01.tprodservs,
                        s_nombreCampo = 'c_claveprodserv',
                        s_datoBuscar = _D_cfdi_concepto.claveprodserv,
                        )
                    if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                        _D_fieldsInsertConcepto.prodserv_id = _D_returnsData.n_id
                        _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())
                    else:
                        _D_fieldsInsertConcepto.prodserv_id = None
                        _D_erroresPrioridad.alta(_D_returnsData, self._generar_mensajeProceso())

                    # Se asocia la unidad con la tabla
                    self._D_procesoMensaje.s_paso = (
                            u"CFDI-Concepto%s.10.030: Se asocia la clave de unidad"
                            ) % str(_n_indice)

                    _D_returnsData = STV_LIB_DB.CACHE_ASOCIAR_TABLA(
                        s_cache_id = 'cfdi_asociar_unidad' + str(_D_cfdi_concepto.claveunidad),
                        n_time_expire = 86400,
                        dbTabla = db01.tunidades,
                        s_nombreCampo = 'c_claveunidad',
                        s_datoBuscar = _D_cfdi_concepto.claveunidad,
                        )

                    if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                        _D_fieldsInsertConcepto.unidad_id = _D_returnsData.n_id
                        _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())
                    else:
                        STV_LIB_DB.CACHE_BORRAR_ULTIMO()
                        _D_fieldsInsertConcepto.unidad_id = None
                        _D_erroresPrioridad.alta(_D_returnsData, self._generar_mensajeProceso())

                    if self._E_tipoImportacion == STV_CFDI_INTEGRACION.E_TIPOIMPORTACION.CFDI_EMITIDO:
                        self._D_procesoMensaje.s_paso = (
                            u"CFDI-Concepto%s.10.040: Se asocia no de identificación con el código del producto"
                            ) % str(_n_indice)
                        _D_returnsData = STV_LIB_DB.ASOCIAR_TABLA(
                            dbTabla = dbc01.tempresa_prodserv,
                            s_nombreCampo = 'codigo',
                            s_datoBuscar = _D_cfdi_concepto.noidentificacion or "",
                            qry_maestro = (dbc01.tempresa_prodserv.empresa_id == self._s_empresa_id),
                            )

                        # Si se asocio correctamente:
                        if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                            _D_fieldsInsertConcepto.empresa_prodserv_id = _D_returnsData.n_id
                        else:
                            # de lo contrario se trata de insertar
                            _D_erroresPrioridad.alta(_D_returnsData, self._generar_mensajeProceso())
                            self._D_procesoMensaje.s_paso = (
                                u"CFDI-Concepto%s.10.050: No se encontró el producto, por lo que "
                                + u" se procede a insertarlo"
                                ) % str(_n_indice)
                            _D_returnsData = self._insertar_prodserv(
                                _D_cfdi_concepto,
                                _D_fieldsInsertConcepto,
                                )

                    elif self._E_tipoImportacion == STV_CFDI_INTEGRACION.E_TIPOIMPORTACION.CFDI_RECIBIDO:

                        self._D_procesoMensaje.s_paso = (
                            u"CFDI-Concepto%s.10.041: Se asocia no de identificación con el código del producto"
                            ) % str(_n_indice)

                        if _D_cfdi_concepto.noidentificacion:
                            _D_returnsData = STV_LIB_DB.ASOCIAR_TABLA(
                                dbTabla = dbc01.tempresa_prodserv_codigosproveedores,
                                s_nombreCampo = 'codigo',
                                s_datoBuscar = _D_cfdi_concepto.noidentificacion,
                                qry_maestro = (
                                    dbc01.tempresa_prodserv_codigosproveedores.empresa_proveedor_id
                                    == D_fieldsInsertCfdi.emisorproveedor_id
                                    ),
                                s_nombreCampoId = 'empresa_prodserv_id'
                                )
                        else:
                            # Si el proveedor no tiene identificado el noidentificacion, se procede a
                            #  buscar por descripción
                            _D_returnsData = STV_LIB_DB.ASOCIAR_TABLA(
                                dbTabla = dbc01.tempresa_prodserv_codigosproveedores,
                                s_nombreCampo = 'codigo',
                                s_datoBuscar = _D_cfdi_concepto.descripcion,
                                qry_maestro = (
                                    dbc01.tempresa_prodserv_codigosproveedores.empresa_proveedor_id
                                    == D_fieldsInsertCfdi.emisorproveedor_id
                                    ),
                                s_nombreCampoId = 'empresa_prodserv_id'
                                )
                            pass

                        # Si se asocio correctamente:
                        if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                            _D_fieldsInsertConcepto.empresa_prodserv_id = _D_returnsData.n_id
                        else:
                            # Si no se a asociado, se busca por el codigo del producto

                            _D_returnsData = STV_LIB_DB.ASOCIAR_TABLA(
                                dbTabla = dbc01.tempresa_prodserv,
                                s_nombreCampo = 'codigo',
                                s_datoBuscar = _D_cfdi_concepto.noidentificacion,
                                qry_maestro = (dbc01.tempresa_prodserv.empresa_id == self._s_empresa_id),
                                )

                            # Si se asocio correctamente:
                            if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                                _D_fieldsInsertConcepto.empresa_prodserv_id = _D_returnsData.n_id
                            else:
                                _D_fieldsInsertConcepto.empresa_prodserv_id = None
                                _D_erroresPrioridad.alta(_D_returnsData, self._generar_mensajeProceso())
                                self._D_procesoMensaje.s_paso = (
                                        u"CFDI-Concepto%s.10.051: No se encontró el producto %s, por lo que se " +
                                        u"agregará sin referencia") % (
                                            str(_n_indice), _D_cfdi_concepto.noidentificacion or "No Definido"
                                            )
                                _D_returnsData = D_NOTIFICACIONMENSAJE(
                                    n_id = 0,
                                    E_return = stvfwk2_e_RETURN.OK,
                                    s_msgError = "Asignación de producto por id del proveedor no completada",
                                    s_msgExcepcion = "",
                                    s_msgLastSQL = "",
                                    n_prioridad= stvfwk2_e_PRIORIDAD.ALTA,
                                    n_referencia_id = 0,
                                    s_codigovalidacion = "",
                                    s_proceso = self._generar_mensajeProceso(),
                                    )
                    else:
                        _D_returnsData = D_NOTIFICACIONMENSAJE(
                            n_id = 0,
                            E_return = stvfwk2_e_RETURN.OK,
                            s_msgError = "Rol del CFDI no definido para encontrar concepto",
                            s_msgExcepcion = "",
                            s_msgLastSQL = "",
                            n_prioridad= stvfwk2_e_PRIORIDAD.ALTA,
                            n_referencia_id = 0,
                            s_codigovalidacion = "",
                            s_proceso = "",
                            )

                    # Si no se aoscio correctamente o no se logro insertar
                    if _D_returnsData.E_return != stvfwk2_e_RETURN.OK:
                        _D_fieldsInsertConcepto.empresa_prodserv_id = None
                        _D_erroresPrioridad.critica(_D_returnsData, self._generar_mensajeProceso())
                    else:
                        # si existe asociación exitosa...
                        _D_fieldsInsertConcepto.empresa_prodserv_id = _D_returnsData.n_id
                        _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())

                        self._D_procesoMensaje.s_paso = (
                                u"CFDI-Concepto%s.20.000: Se inserta el concepto del CFDI"
                                ) % str(_n_indice)
                        _D_returnsData = STV_LIB_DB.INSERTAR_ACTUALIZAR_JSON(
                            _D_cfdi_concepto,
                            _D_campos_relacion_conceptos,
                            self._D_tablas.conceptos,
                            _D_fieldsInsertConcepto,
                            )

                        if _D_returnsData.E_return != stvfwk2_e_RETURN.OK:
                            # Si existen errores en la creación del cfdi
                            _D_erroresPrioridad.critica(_D_returnsData, self._generar_mensajeProceso())
                            _n_cfdi_concepto_id = None
                        else:
                            # Si no hay errores en la creación del CFDI continua la inserción de conceptos, pagos
                            #  y relacionados
                            _n_cfdi_concepto_id = _D_returnsData.n_id
                            _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())

                            ###
                            # Se asocian los impuestos
                            ###

                            if _D_cfdi_concepto.impuestos:

                                if _D_cfdi_concepto.impuestos.traslados \
                                        and _D_cfdi_concepto.impuestos.traslados.traslado:

                                    _D_erroresPrioridad.agregar(
                                        self._importar_concepto_impuestos(
                                            _n_cfdi_concepto_id,
                                            _D_cfdi_concepto.impuestos.traslados,
                                            self._D_tablas.conceptos_imptrasladados,
                                            'traslado',
                                            ('%s-ImpTrasladados' % str(_n_indice))
                                            )
                                        )
                                else:
                                    # No hay trasladados
                                    pass

                                if _D_cfdi_concepto.impuestos.retenciones \
                                        and _D_cfdi_concepto.impuestos.retenciones.retencion:
                                    _D_erroresPrioridad.agregar(
                                        self._importar_concepto_impuestos(
                                            _n_cfdi_concepto_id,
                                            _D_cfdi_concepto.impuestos.retenciones,
                                            self._D_tablas.conceptos_impretenidos,
                                            'retencion',
                                            ('%s-ImpRetenidos' % str(_n_indice))
                                            )
                                        )
                                else:
                                    # No hay retenidos
                                    pass
                            else:
                                # No hay impuestos
                                pass

                else:
                    _D_returnsData = D_NOTIFICACIONMENSAJE(
                        n_id = 0,
                        E_return = stvfwk2_e_RETURN.OK,
                        s_msgError = "Producto o servicio asociado es nulo",
                        s_msgExcepcion = "tipocomprobante %s" % str(D_fieldsInsertCfdi.tipocomprobante_id),
                        s_msgLastSQL = "",
                        s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.VALIDAR % (
                            str(self._D_tablas.conceptos), "claveprodserv", _D_cfdi_concepto.claveprodserv, "", "", ""
                            ),
                        )
                    _D_erroresPrioridad.media(_D_returnsData, self._generar_mensajeProceso())

            else:
                _D_returnsData = D_NOTIFICACIONMENSAJE(
                    n_id = 0,
                    E_return = stvfwk2_e_RETURN.OK,
                    s_msgError = "No hay error, tipo de comprobante no debe tener conceptos asociados",
                    s_msgExcepcion = "tipocomprobante %s" % str(D_fieldsInsertCfdi.tipocomprobante_id),
                    s_msgLastSQL = "",
                    s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.VALIDAR % (
                        str(self._D_tablas.conceptos), "tipocomprobante", self._D_cfdi.tipodecomprobante, "", "", ""
                        ),
                    )
                _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())

        return _D_erroresPrioridad

    # noinspection PyArgumentList
    @_decorador_define_mensajeProceso(s_enEjecucion = u"Impuestos")
    def _importar_concepto_impuestos(
            self,
            n_cfdi_concepto_id,
            D_cfdi_concepto_impuestos,
            dbTabla,
            s_cfdiIndex,
            s_procesoSeccion = 'Imp'
            ):
        """ Valida la informacion e inserta los impuestos de los conceptos de un CFDI
    
        Return:
            D_RETURN_ERRORES_PRIORIDAD
            
        """
        _D_erroresPrioridad = D_RETURN_ERRORES_PRIORIDAD()

        # Se relacionan los nombres en el json con los campos en la tabla tcfdi_conceptos
        _D_campos_relacion_impuestos = {
            'base' :            Storage(dbField = dbTabla.base, fnValidation = None),
            'impuesto' :        Storage(dbField = dbTabla.impuesto, fnValidation = None),
            'tipofactor' :      Storage(dbField = dbTabla.tipofactor, fnValidation = None),
            'tasaocuota' :      Storage(dbField = dbTabla.tasaocuota, fnValidation = None),
            'importe' :         Storage(dbField = dbTabla.importe, fnValidation = None),
            }

        _n_indice = 0

        for _D_cfdi_impuesto in D_cfdi_concepto_impuestos[s_cfdiIndex]:

            _n_indice += 1

            # Se agregan los campos standard con sus valores
            _D_fieldsInsertImpuesto = Storage(
                cfdi_concepto_id = n_cfdi_concepto_id
                )

            # Se asocia el impuesto con la tabla
            self._D_procesoMensaje.s_paso = (
                u"CFDI-Concepto%s%s.10.010: Se asocia el impuesto"
                ) % (str(s_procesoSeccion), str(_n_indice))
            _D_returnsData = STV_LIB_DB.CACHE_ASOCIAR_TABLA(
                s_cache_id = 'cfdi_asociar_impuesto' + str(_D_cfdi_impuesto.impuesto),
                n_time_expire = 86400,
                dbTabla = db01.timpuestos,
                s_nombreCampo = 'c_impuesto',
                s_datoBuscar = _D_cfdi_impuesto.impuesto,
                )
            if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                _D_fieldsInsertImpuesto.impuesto_id = _D_returnsData.n_id
                _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())
            else:
                STV_LIB_DB.CACHE_BORRAR_ULTIMO()
                _D_fieldsInsertImpuesto.impuesto_id = None
                _D_erroresPrioridad.alta(_D_returnsData, self._generar_mensajeProceso())

            self._D_procesoMensaje.s_paso = (
                u"CFDI-Concepto%s%s.20.000: Se inserta el impuesto en el CFDI"
                ) % (str(s_procesoSeccion), str(_n_indice))
            _D_returnsData = STV_LIB_DB.INSERTAR_ACTUALIZAR_JSON(
                _D_cfdi_impuesto,
                _D_campos_relacion_impuestos,
                dbTabla,
                _D_fieldsInsertImpuesto,
                )

            if _D_returnsData.E_return != stvfwk2_e_RETURN.OK:
                # Si existen errores en la creación del cfdi
                _D_erroresPrioridad.critica(_D_returnsData, self._generar_mensajeProceso())
            else:
                # Si no hay errores en la creación del CFDI continua la inserción de conceptos, pagos y relacionados
                _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())
                pass

        return _D_erroresPrioridad

    # noinspection PyArgumentList
    @_decorador_define_mensajeProceso(s_enEjecucion = u"XML")
    def _importar_xml(
            self,
            s_xml,
            D_fieldsInsertCfdi
            ):
        """ Inserta el XML
        
        Return:
            D_RETURN_ERRORES_PRIORIDAD
            
        """
        _ = D_fieldsInsertCfdi
        _D_erroresPrioridad = D_RETURN_ERRORES_PRIORIDAD()

        if s_xml:

            # Se agregan los campos standard con sus valores
            _D_fieldsInsertXML = Storage(
                cfdi_id = self._n_cfdi_id,
                xml_cfdi = s_xml,
                )

            _dbRows_xml = dbc02(
                self._D_tablas.xml.cfdi_id == self._n_cfdi_id
                ).select(
                    self._D_tablas.xml.id
                    )

            if _dbRows_xml:
                self._D_procesoMensaje.s_paso = u"CFDI-XML.20.000"
                _dbRows_xml.last().xml_cfdi = s_xml
                _dbRows_xml.last().update_record()

            else:

                # Se inserta el XML
                self._D_procesoMensaje.s_paso = u"CFDI-XML.20.000"
                _D_returnsData = STV_LIB_DB.INSERTAR_ACTUALIZAR_JSON(
                    {},
                    {},
                    self._D_tablas.xml,
                    _D_fieldsInsertXML,
                    s_proceso = self._generar_mensajeProceso()
                    )

                if _D_returnsData.E_return != stvfwk2_e_RETURN.OK:
                    # Si existen errores en la creación del cfdi
                    _D_erroresPrioridad.critica(_D_returnsData, self._generar_mensajeProceso())
                else:
                    # Si no hay errores
                    _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())
                    pass
        else:
            # No se inserta nada
            pass

        return _D_erroresPrioridad

    # noinspection PyArgumentList
    @_decorador_define_mensajeProceso(s_enEjecucion = u"Relacionados")
    def _importar_relacionados(
            self,
            D_fieldsInsertCfdi
            ):
        """ Valida la informacion e inserta los relacionados de un CFDI
        
        Return:
            D_RETURN_ERRORES_PRIORIDAD
            
        """
        _ = D_fieldsInsertCfdi
        _D_erroresPrioridad = D_RETURN_ERRORES_PRIORIDAD()

        # Se relacionan los nombres en el json con los campos en la tabla tcfdi_conceptos
        _D_campos_relacion_relacionados = {
            'uuid': Storage(dbField = self._D_tablas.relacionados.uuid, fnValidation = None),
            }

        if (
                self._D_cfdi.cfdirelacionados
                and self._D_cfdi.cfdirelacionados.cfdirelacionado
                ):

            _n_indice = 0

            for _D_cfdi_relacionado in self._D_cfdi.cfdirelacionados.cfdirelacionado:

                _n_indice += 1

                # Se agregan los campos standard con sus valores
                _D_fieldsInsertRelacionado = Storage(
                    cfdi_id = self._n_cfdi_id
                    )

                # Se asocia el producto o servicio con la tabla
                self._D_procesoMensaje.s_paso = (
                    u"CFDI-Relacionados-%s.10.010: Se asocia el CFDI relacionado"
                    ) % str(_n_indice)
                _D_returnsData = STV_LIB_DB.ASOCIAR_TABLA(
                    self._D_tablas.cfdis,
                    'uuid',
                    _D_cfdi_relacionado.uuid,
                    )
                if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                    _D_fieldsInsertRelacionado.cfdirelacionado_id = _D_returnsData.n_id
                    _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())
                else:
                    _D_fieldsInsertRelacionado.cfdirelacionado_id = None
                    _D_erroresPrioridad.alta(_D_returnsData, self._generar_mensajeProceso())

                self._D_procesoMensaje.s_paso = u"CFDI-Relacionados-%s.20.000: Se inserta el relacionado en el CFDI" % (
                    str(_n_indice)
                    )
                _D_returnsData = STV_LIB_DB.INSERTAR_ACTUALIZAR_JSON(
                    _D_cfdi_relacionado,
                    _D_campos_relacion_relacionados,
                    self._D_tablas.relacionados,
                    _D_fieldsInsertRelacionado,
                    s_proceso = self._generar_mensajeProceso()
                    )

                if _D_returnsData.E_return != stvfwk2_e_RETURN.OK:
                    # Si existen errores en la creación del cfdi
                    _D_erroresPrioridad.critica(_D_returnsData, self._generar_mensajeProceso())
                else:
                    # Si no hay errores
                    _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())
                    pass
        else:
            # No se inserta nada
            pass

        return _D_erroresPrioridad

    # noinspection PyArgumentList
    @_decorador_define_mensajeProceso(s_enEjecucion = u"Pagos")
    def _importar_pagos(
            self,
            D_fieldsInsertCfdi
            ):
        """ Valida la informacion e inserta los pagos de un CFDI
        
        Return:
            D_RETURN_ERRORES_PRIORIDAD
            
        """
        _D_erroresPrioridad = D_RETURN_ERRORES_PRIORIDAD()

        self._D_procesoMensaje.s_paso = u"CFDI-Pagos.00.000: Identificar pagos"

        if (
                self._D_cfdi.complemento.pagos
                and self._D_cfdi.complemento.pagos.pago
                ):

            if not(D_fieldsInsertCfdi.tipocomprobante_id in (TTIPOSCOMPROBANTES.PAGO,)):
                _D_erroresPrioridad.media(
                    D_NOTIFICACIONMENSAJE(
                        n_id = 0,
                        s_msgError = "Existen pagos para tipos de comprobantes diferentes a Pago",
                        s_msgExcepcion = "",
                        s_msgLastSQL = "",
                        E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS,
                        s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.VALIDAR % (
                            str(self._D_tablas.comppagos), 'pago:formapago',
                            self._D_cfdi.complemento.pagos.version, "", "", ""
                            ),
                        s_proceso = self._generar_mensajeProceso()
                        ),
                    s_proceso = self._generar_mensajeProceso()
                    )
            else:
                # No hay problema, es un coprobante de pago, debe incluir pagos
                pass

            # Se relacionan los nombres en el json con los campos en la tabla tcfdi_conceptos
            _D_campos_relacion_pagos = {
                'version'     : Storage(dbField = self._D_tablas.comppagos.versionpago, fnValidation = None),
                'fechapago'   : Storage(dbField = self._D_tablas.comppagos.fechapago_str, fnValidation = None),
                'formadepagop': Storage(dbField = self._D_tablas.comppagos.formapago, fnValidation = None),
                'monedap'     : Storage(dbField = self._D_tablas.comppagos.moneda, fnValidation = None),
                'tipocambiop' : Storage(dbField = self._D_tablas.comppagos.tipocambio, fnValidation = None, x_default = 1),
                'monto'       : Storage(dbField = self._D_tablas.comppagos.monto, fnValidation = None),
                }

            for _D_pago in self._D_cfdi.complemento.pagos.pago:

                # Se agregan los campos standard con sus valores
                _D_fieldsInsertPago = Storage(
                    cfdi_id = self._n_cfdi_id
                    )

                self._D_procesoMensaje.s_paso = u"CFDI-Pagos.10.010: Identificar fecha de pago"
                try:
                    _D_fieldsInsertPago.fechapago = datetime.datetime.strptime(_D_pago.fechapago, '%Y-%m-%dT%H:%M:%S')
                except Exception as _O_excepcion:
                    _D_erroresPrioridad.alta(
                        D_NOTIFICACIONMENSAJE(
                            n_id = 0,
                            s_msgError = ('No se pudo asociar la fecha de timbrado %s' % _D_pago.fechapago),
                            s_msgExcepcion = ('Fecha Pago:%s' % str(_O_excepcion)),
                            s_msgLastSQL = '',
                            E_return = stvfwk2_e_RETURN.NOK_ERROR,
                            s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.VALIDAR % (
                                str(self._D_tablas.comppagos), 'fechapago', _D_pago.fechapago, "", "", ""
                                ),
                            s_proceso = self._generar_mensajeProceso()
                            ),
                        s_proceso = self._generar_mensajeProceso()
                        )

                # Se asocia la forma de pago con la tabla
                self._D_procesoMensaje.s_paso = u"CFDI-Pagos.20.010: Asociar forma de pago"
                _D_returnsData = STV_LIB_DB.CACHE_ASOCIAR_TABLA(
                    s_cache_id = 'cfdi_asociar_formapago' + str(_D_pago.formadepagop),
                    n_time_expire = 86400,
                    dbTabla = db01.tformaspago,
                    s_nombreCampo = 'c_formapago',
                    s_datoBuscar = _D_pago.formadepagop,
                    )
                if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                    _D_fieldsInsertPago.formapago_id = _D_returnsData.n_id
                    _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())
                else:
                    STV_LIB_DB.CACHE_BORRAR_ULTIMO()
                    _D_fieldsInsertPago.formapago_id = None
                    _D_erroresPrioridad.alta(_D_returnsData, self._generar_mensajeProceso())

                # Se asocia la moneda con la tabla
                self._D_procesoMensaje.s_paso = u"CFDI-Pagos.20.020: Asociar moneda"
                _D_returnsData = STV_LIB_DB.CACHE_ASOCIAR_TABLA(
                    s_cache_id = 'cfdi_asociar_moneda' + str(_D_pago.monedap),
                    n_time_expire = 86400,
                    dbTabla = db01.tmonedas,
                    s_nombreCampo = 'c_moneda',
                    s_datoBuscar = _D_pago.monedap,
                    )
                if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                    _D_fieldsInsertPago.moneda_id = _D_returnsData.n_id
                    _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())

                    _s_cache_id = 'cfdi_asociar_moneda' + str(self._s_empresa_id) + 'contpaqi' + str(_D_pago.monedap)
                    _D_returnsData = STV_LIB_DB.CACHE_ASOCIAR_TABLA(
                        _s_cache_id,
                        n_time_expire = 86400,
                        dbTabla = dbc01.tempresa_monedascontpaqi,
                        s_nombreCampo = 'moneda_id',
                        s_datoBuscar = _D_fieldsInsertPago.moneda_id,
                        qry_maestro = (dbc01.tempresa_monedascontpaqi.empresa_id == self._s_empresa_id),
                        )

                    if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                        _D_fieldsInsertPago.monedacontpaqi_id = _D_returnsData.n_id
                        _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())
                    else:
                        cache.ram(_s_cache_id, None)
                        _D_fieldsInsertPago.monedacontpaqi_id = None
                        _D_erroresPrioridad.alta(_D_returnsData, self._generar_mensajeProceso())

                else:
                    STV_LIB_DB.CACHE_BORRAR_ULTIMO()
                    _D_fieldsInsertPago.moneda_id = None
                    _D_erroresPrioridad.alta(_D_returnsData, self._generar_mensajeProceso())

                self._D_procesoMensaje.s_paso = u"CFDI-Pagos.30.000: Insertar pago maestro de CFDI"
                _D_returnsData = STV_LIB_DB.INSERTAR_ACTUALIZAR_JSON(
                    _D_pago,
                    _D_campos_relacion_pagos,
                    self._D_tablas.comppagos,
                    _D_fieldsInsertPago,
                    )

                if _D_returnsData.E_return != stvfwk2_e_RETURN.OK:
                    # Si existen errores en la creación del cfdi
                    _D_erroresPrioridad.critica(_D_returnsData, self._generar_mensajeProceso())
                    _cfdi_complementopago_id = None
                elif _D_pago.doctorelacionado:
                    # Si no hay errores se continua con el registro de pagos
                    _cfdi_complementopago_id = _D_returnsData.n_id
                    _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())

                    # Se relacionan los nombres en el json con los campos en la tabla tcfdi_conceptos
                    _D_campos_relacion_pago_doctosrelacionados = {
                        'iddocumento'     : Storage(
                            dbField = self._D_tablas.comppago_docsrelacionados.iddocumento, fnValidation = None
                            ),
                        'serie'           : Storage(
                            dbField = self._D_tablas.comppago_docsrelacionados.serie, fnValidation = None
                            ),
                        'folio'           : Storage(
                            dbField = self._D_tablas.comppago_docsrelacionados.folio, fnValidation = None
                            ),
                        'monedadr'        : Storage(
                            dbField = self._D_tablas.comppago_docsrelacionados.moneda, fnValidation = None
                            ),
                        'tipocambiodr'    : Storage(
                            dbField = self._D_tablas.comppago_docsrelacionados.tipocambio, fnValidation = None
                            ),
                        'metododepagodr'  : Storage(
                            dbField = self._D_tablas.comppago_docsrelacionados.metodopago, fnValidation = None
                            ),
                        'numparcialidad'  : Storage(
                            dbField = self._D_tablas.comppago_docsrelacionados.numparcialidad, fnValidation = None
                            ),
                        'impsaldoant'     : Storage(
                            dbField = self._D_tablas.comppago_docsrelacionados.impsaldoant, fnValidation = None
                            ),
                        'imppagado'       : Storage(
                            dbField = self._D_tablas.comppago_docsrelacionados.imppagado, fnValidation = None
                            ),
                        'impsaldoinsoluto': Storage(
                            dbField = self._D_tablas.comppago_docsrelacionados.impsaldoinsoluto,
                            fnValidation = None
                            ),
                        }

                    _n_indice = 0

                    for _D_doctorelacionado in _D_pago.doctorelacionado:

                        _n_indice += 1

                        # Se agregan los campos standard con sus valores
                        _D_fieldsInsertPagoDoctoRelacionado = Storage(
                            cfdi_complementopago_id = _cfdi_complementopago_id
                            )

                        # Se asocia el producto o servicio con la tabla
                        self._D_procesoMensaje.s_paso = (
                            u"CFDI-Pagos-Pago%s.10.010: Asociar documento relacionado del pago"
                            ) % str(_n_indice)
                        _D_returnsData = STV_LIB_DB.ASOCIAR_TABLA(
                            dbTabla = self._D_tablas.cfdis,
                            s_nombreCampo = 'uuid',
                            s_datoBuscar = _D_doctorelacionado.iddocumento,
                            )
                        if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                            _D_fieldsInsertPagoDoctoRelacionado.cfdirelacionado_id = _D_returnsData.n_id
                            _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())
                        else:
                            _D_fieldsInsertPagoDoctoRelacionado.cfdirelacionado_id = None
                            _D_erroresPrioridad.alta(_D_returnsData, self._generar_mensajeProceso())

                        if self._E_tipoImportacion == STV_CFDI_INTEGRACION.E_TIPOIMPORTACION.CFDI_EMITIDO:
                        # Solamente si es emisor se puede asociar serie

                            # Se asocia la serie con la tabla TODO no asociar serie si es CFDI recibido
                            self._D_procesoMensaje.s_paso = (
                                u"CFDI-Pagos-Pago%s.10.020: Asociar serie del pago"
                                ) % str(_n_indice)
                            _D_returnsData = STV_LIB_DB.CACHE_ASOCIAR_TABLA(
                                s_cache_id = (
                                    'cfdi_asociar_empresa' + str(self._s_empresa_id)
                                    + 'serie' + str(_D_doctorelacionado.serie)
                                    ),
                                n_time_expire = 86400,
                                dbTabla = dbc01.tempresa_series,
                                s_nombreCampo = 'serie',
                                s_datoBuscar = _D_doctorelacionado.serie,
                                qry_maestro = (dbc01.tempresa_series.empresa_id == self._s_empresa_id),
                                )
                            if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                                _D_fieldsInsertPagoDoctoRelacionado.empresa_serie_id = _D_returnsData.n_id
                                _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())
                            else:
                                STV_LIB_DB.CACHE_BORRAR_ULTIMO()
                                _D_fieldsInsertPagoDoctoRelacionado.empresa_serie_id = None
                                _D_erroresPrioridad.alta(_D_returnsData, self._generar_mensajeProceso())

                        else:
                            # Si no es emisor, no se puede asociar la serie, ni la caja chica

                            pass

                        # Se asocia la moneda con la tabla
                        self._D_procesoMensaje.s_paso = (
                            u"CFDI-Pagos-Pago%s.10.030: Asociar moneda del pago"
                            ) % str(_n_indice)
                        _D_returnsData = STV_LIB_DB.CACHE_ASOCIAR_TABLA(
                            s_cache_id = 'cfdi_asociar_moneda' + str(_D_doctorelacionado.monedadr),
                            n_time_expire = 86400,
                            dbTabla = db01.tmonedas,
                            s_nombreCampo = 'c_moneda',
                            s_datoBuscar = _D_doctorelacionado.monedadr,
                            )
                        if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                            _D_fieldsInsertPagoDoctoRelacionado.moneda_id = _D_returnsData.n_id
                            _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())

                            _s_cache_id = (
                                'cfdi_asociar_moneda' + str(self._s_empresa_id)
                                + 'contpaqi' + str(_D_doctorelacionado.monedadr)
                                )
                            _D_returnsData = STV_LIB_DB.CACHE_ASOCIAR_TABLA(
                                _s_cache_id,
                                n_time_expire = 86400,
                                dbTabla = dbc01.tempresa_monedascontpaqi,
                                s_nombreCampo = 'moneda_id',
                                s_datoBuscar = _D_fieldsInsertPagoDoctoRelacionado.moneda_id,
                                qry_maestro = (dbc01.tempresa_monedascontpaqi.empresa_id == self._s_empresa_id),
                                )

                            if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                                _D_fieldsInsertPagoDoctoRelacionado.monedacontpaqi_id = _D_returnsData.n_id
                                _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())
                            else:
                                cache.ram(_s_cache_id, None)
                                _D_fieldsInsertPagoDoctoRelacionado.monedacontpaqi_id = None
                                _D_erroresPrioridad.alta(_D_returnsData, self._generar_mensajeProceso())

                        else:
                            STV_LIB_DB.CACHE_BORRAR_ULTIMO()
                            _D_fieldsInsertPagoDoctoRelacionado.moneda_id = None
                            _D_erroresPrioridad.alta(_D_returnsData, self._generar_mensajeProceso())

                        # Se asocia el método de pago con la tabla
                        self._D_procesoMensaje.s_paso = (
                            u"CFDI-Pagos-Pago%s.10.040: Asociar método de pago del pago"
                            ) % str(_n_indice)
                        _D_returnsData = STV_LIB_DB.CACHE_ASOCIAR_TABLA(
                            s_cache_id = 'cfdi_asociar_metodopago' + str(_D_doctorelacionado.metododepagodr),
                            n_time_expire = 86400,
                            dbTabla = db01.tmetodospago,
                            s_nombreCampo = 'c_metodopago',
                            s_datoBuscar = _D_doctorelacionado.metododepagodr,
                            )
                        if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                            _D_fieldsInsertPagoDoctoRelacionado.metodopago_id = _D_returnsData.n_id
                            _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())
                        else:
                            STV_LIB_DB.CACHE_BORRAR_ULTIMO()
                            _D_fieldsInsertPagoDoctoRelacionado.metodopago_id = None
                            _D_erroresPrioridad.alta(_D_returnsData, self._generar_mensajeProceso())

                        self._D_procesoMensaje.s_paso = (
                            u"CFDI-Pagos-Pago%s.20.000: Insertar información del pago en el CFDI"
                            ) % str(_n_indice)

                        _D_returnsData = STV_LIB_DB.INSERTAR_ACTUALIZAR_JSON(
                            _D_doctorelacionado,
                            _D_campos_relacion_pago_doctosrelacionados,
                            self._D_tablas.comppago_docsrelacionados,
                            _D_fieldsInsertPagoDoctoRelacionado,
                            )

                        if _D_returnsData.E_return != stvfwk2_e_RETURN.OK:
                            # Si existen errores en la creación del cfdi
                            _D_erroresPrioridad.critica(_D_returnsData, self._generar_mensajeProceso())
                        else:
                            # Si no hay errores continuar
                            _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())
                            pass

                else:
                    # No existieron documentos relacionados a los pagos
                    _D_erroresPrioridad.alta(
                        D_NOTIFICACIONMENSAJE(
                            n_id = 0,
                            s_msgError = "El pago no genero documentos relacionados",
                            s_msgExcepcion = "",
                            s_msgLastSQL = "",
                            E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS,
                            s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.VALIDAR % (
                                str(self._D_tablas.comppagos), 'pago:doctorelacionado',
                                _D_pago.doctorelacionado, "", "", ""
                                ),
                            s_proceso = self._generar_mensajeProceso()
                            ),
                        s_proceso = self._generar_mensajeProceso()
                        )
        else:
            # Si no existe pago
            if D_fieldsInsertCfdi.tipocomprobante_id in (TTIPOSCOMPROBANTES.PAGO,):
                _D_erroresPrioridad.alta(
                    D_NOTIFICACIONMENSAJE(
                        n_id = 0,
                        s_msgError = "Información sobre el comprobante de pago no existe",
                        s_msgExcepcion = "",
                        s_msgLastSQL = "",
                        E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS,
                        s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.VALIDAR % (
                            str(self._D_tablas.comppagos), 'pago:formapago', "", "", "", ""
                            ),
                        s_proceso = self._generar_mensajeProceso()
                        ),
                    s_proceso = self._generar_mensajeProceso()
                    )
            else:
                # No hay razon para que existe un pago
                pass

        return _D_erroresPrioridad

    # noinspection PyArgumentList
    @_decorador_define_mensajeProceso(s_enEjecucion = u"Verificar")
    def verificar(
            self,
            s_cfdi_id,
            xml_cfdi = "",
            D_cfdi = None,
            D_attr = None,
            E_tipoImportacion = None,
            ):
        """ Verifica que el CFDI actual corresponda al XML recibido, utilizado para verificar
         y completar información cuando se timbra un CFDI
                    
        """
        _ = D_attr
        # TODO Si s_rfc no tiene guiones hay que ponerlos.

        # Se establece el valor por default de las variables
        _D_return = Storage(
            E_return = stvfwk2_e_RETURN.NOK_ERROR,
            s_msgError = "",
            n_cfdi_id = 0,
            D_cfdi = Storage()
            )
        self._n_cfdi_id = None

        _n_procesoinicio_S = time.time()

        # if True:
        try:
            self._D_procesoMensaje.s_paso = u"CFDI.00.000: Validaciones para iniciar importación"

            self._E_tipoImportacion = E_tipoImportacion
            self._incializar_tablas()

            if self._dbRow_empresa:
                # No hay bronca, ya se hicieron las validaciones
                pass
            else:
                _D_returnsData = self._validar_empresaEnCuenta()
                if stvfwk2_e_RETURN.OK != _D_returnsData.E_return:
                    _D_return.E_return = _D_returnsData.E_return
                    _D_return.s_msgError = _D_returnsData.s_msgError
                    return _D_return
                else:
                    pass

            if not D_cfdi and xml_cfdi:
                self._D_procesoMensaje.s_paso = u"CFDI.01.000: Se importa el XML a Diccionario"

                self._O_cfdi = STV_XML2CFDI()
                _D_returnsData = self._O_cfdi.importar_string(xml_cfdi)
                if stvfwk2_e_RETURN.OK != _D_returnsData.E_return:
                    _D_return.E_return = _D_returnsData.E_return
                    _D_return.s_msgError = _D_returnsData.s_msgError
                    return _D_return
                else:
                    pass

                self._D_cfdi = self._O_cfdi.Comprobante
            else:
                self._D_cfdi = D_cfdi
            _D_return.D_cfdi = self._D_cfdi

            _D_returnsData = self._validar_uuid()
            if stvfwk2_e_RETURN.OK != _D_returnsData.E_return:
                _D_return.E_return = _D_returnsData.E_return
                _D_return.s_msgError = _D_returnsData.s_msgError
                _D_return.n_cfdi_id = _D_returnsData.n_cfdi_id
                # return _D_return  Aunque ya existe el UUID se procede a verificar y actualizar la info si es el caso
            else:
                pass

            _dbRow_cfdi = self._D_tablas.cfdis(s_cfdi_id)

            if not _dbRow_cfdi:
                _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                _D_return.s_msgError = "CFDI referenciado no encontrado %d" % s_cfdi_id
                _D_return.n_cfdi_id = s_cfdi_id
                return _D_return
            else:
                pass

            # Se define el storage donde se van a almacenar los errores durante el proceso en forma de lista
            #  de acuerdo a prioridad
            _D_erroresPrioridad = D_RETURN_ERRORES_PRIORIDAD()

            # ##
            # ## Se verifica el maestro de CFDI
            # ##

            if str(self._D_cfdi.receptor.rfc) == "AAA-010101-AAA":
                _b_esPruebaEmitido = True
            else:
                _b_esPruebaEmitido = False

            # Se verifica el tipo de comprobante con la tabla
            self._D_procesoMensaje.s_paso = u"CFDI.10.020: Verificar tipo de comprobante"
            if self._D_cfdi.tipodecomprobante == _dbRow_cfdi.tipodecomprobante:
                _D_erroresPrioridad.informacion(self._D_resultOK, self._generar_mensajeProceso())
            else:
                _D_erroresPrioridad.critica(self._D_resultERROR, self._generar_mensajeProceso())

            # Solamente si es emisor
            if (str(self._D_cfdi.emisor.rfc) == self._s_rfc) or _b_esPruebaEmitido:

                # Se verifica la serie con la tabla
                self._D_procesoMensaje.s_paso = u"CFDI.10.010: Verificar serie"

                if self._D_cfdi.serie == _dbRow_cfdi.serie:
                    _D_erroresPrioridad.informacion(self._D_resultOK, self._generar_mensajeProceso())
                else:
                    _D_erroresPrioridad.critica(self._D_resultERROR, self._generar_mensajeProceso())

            else:
                # Si no es emisor, no se puede verificar la serie                
                pass

            # Se verifica la forma de pago con la tabla
            self._D_procesoMensaje.s_paso = u"CFDI.10.030: Verificar forma de pago"
            if (self._D_cfdi.formapago or "") == _dbRow_cfdi.formapago:
                _D_erroresPrioridad.informacion(self._D_resultOK, self._generar_mensajeProceso())
            else:
                _D_erroresPrioridad.critica(self._D_resultERROR, self._generar_mensajeProceso())

            # Se verifica el método de pago con la tabla
            self._D_procesoMensaje.s_paso = u"CFDI.10.040: Verificar método de pago"
            if (self._D_cfdi.metodopago or "") == _dbRow_cfdi.metodopago:
                _D_erroresPrioridad.informacion(self._D_resultOK, self._generar_mensajeProceso())
            else:
                _D_erroresPrioridad.critica(self._D_resultERROR, self._generar_mensajeProceso())

            # Se verifica la moneda con la tabla
            self._D_procesoMensaje.s_paso = u"CFDI.10.050: Verificar moneda"
            if self._D_cfdi.moneda == _dbRow_cfdi.moneda:
                _D_erroresPrioridad.informacion(self._D_resultOK, self._generar_mensajeProceso())
            else:
                _D_erroresPrioridad.critica(self._D_resultERROR, self._generar_mensajeProceso())

            # Se verifica el lugar de expedición con la tabla
            self._D_procesoMensaje.s_paso = u"CFDI.10.060: Verificar lugar de expedición"
            if self._D_cfdi.lugarexpedicion == _dbRow_cfdi.lugarexpedicion:
                _D_erroresPrioridad.informacion(self._D_resultOK, self._generar_mensajeProceso())
            else:
                _D_erroresPrioridad.critica(self._D_resultERROR, self._generar_mensajeProceso())

            if self._D_cfdi.cfdirelacionados:

                # Se verifica el tipo relación con la tabla
                self._D_procesoMensaje.s_paso = u"CFDI.10.070: Verificar tipo de relación, ya que es egreso"
                if self._D_cfdi.cfdirelacionados.tiporelacion == _dbRow_cfdi.tiporelacion:
                    _D_erroresPrioridad.informacion(self._D_resultOK, self._generar_mensajeProceso())
                elif not _dbRow_cfdi.tiporelacion:
                    # Se asocia el tipo relación con la tabla
                    self._D_procesoMensaje.s_paso = u"CFDI.10.070: Asociar tipo de relación, ya que existen relacionados"
                    _D_returnsData = STV_LIB_DB.CACHE_ASOCIAR_TABLA(
                        s_cache_id = 'cfdi_asociar_tiporelacion' + str(self._D_cfdi.cfdirelacionados.tiporelacion),
                        n_time_expire = 86400,
                        dbTabla = db01.ttiposrelaciones,
                        s_nombreCampo = 'c_tiporelacion',
                        s_datoBuscar = self._D_cfdi.cfdirelacionados.tiporelacion,
                        )
                    if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                        _dbRow_cfdi.tiporelacion_id = _D_returnsData.n_id
                        _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())
                    else:
                        STV_LIB_DB.CACHE_BORRAR_ULTIMO()
                        # Si tipo de comprobante es Ingreso o Egreso, la forma de pago y el metodo de
                        # pago es obligatoria
                        _D_erroresPrioridad.alta(_D_returnsData, self._generar_mensajeProceso())
                else:
                    _D_erroresPrioridad.critica(self._D_resultERROR, self._generar_mensajeProceso())

            else:
                # Se ignora el tiporelacion
                pass

            # Se verifica el uso cfdi con la tabla
            self._D_procesoMensaje.s_paso = u"CFDI.10.080: Verificar receptor uso CFDI"
            if self._D_cfdi.receptor.usocfdi == _dbRow_cfdi.receptorusocfdi:
                _D_erroresPrioridad.informacion(self._D_resultOK, self._generar_mensajeProceso())
            elif not _dbRow_cfdi.receptorusocfdi or _b_esPruebaEmitido:
                # Se asocia el uso cfdi con la tabla o si es prueba, se actualiza
                self._D_procesoMensaje.s_paso = u"CFDI.10.080: Asociar receptor uso CFDI"
                _D_returnsData = STV_LIB_DB.CACHE_ASOCIAR_TABLA(
                    s_cache_id = 'cfdi_asociar_receptorusocfdi' + str(self._D_cfdi.receptor.usocfdi),
                    n_time_expire = 86400,
                    dbTabla = db01.tusoscfdi,
                    s_nombreCampo = 'c_usocfdi',
                    s_datoBuscar = self._D_cfdi.receptor.usocfdi,
                    )
                if _D_returnsData.E_return == stvfwk2_e_RETURN.OK:
                    _dbRow_cfdi.receptorusocfdi_id = _D_returnsData.n_id
                    _D_erroresPrioridad.informacion(self._D_resultOK, self._generar_mensajeProceso())
                else:
                    STV_LIB_DB.CACHE_BORRAR_ULTIMO()
                    _D_erroresPrioridad.alta(self._D_resultERROR, self._generar_mensajeProceso())

            else:
                _s_descripcion = "xml %s = db %s, se actualiza el uso" % (
                    str(self._D_cfdi.receptor.usocfdi), str(_dbRow_cfdi.receptorusocfdi)
                    )
                _D_erroresPrioridad.critica(self._D_resultWARNING, self._generar_mensajeProceso(_s_descripcion))

            self._D_procesoMensaje.s_paso = u"CFDI.20.010: Verificar fecha"
            if self._D_cfdi.fecha == _dbRow_cfdi.fecha_str:
                _D_erroresPrioridad.informacion(self._D_resultOK, self._generar_mensajeProceso())
            else:
                _D_erroresPrioridad.media(self._D_resultERROR, self._generar_mensajeProceso())

            self._D_procesoMensaje.s_paso = u"CFDI.20.020: Identificar fecha de timbrado"
            if self._D_cfdi.complemento.timbrefiscaldigital.fechatimbrado:
                try:
                    _dbRow_cfdi.fechatimbrado = datetime.datetime.strptime(
                        self._D_cfdi.complemento.timbrefiscaldigital.fechatimbrado, '%Y-%m-%dT%H:%M:%S'
                        )
                except Exception as _O_excepcion:
                    _D_returnsData = D_NOTIFICACIONMENSAJE(
                        n_id = 0,
                        s_msgError = 'No se pudo asociar la fecha de timbrado %s' % (
                            self._D_cfdi.complemento.timbrefiscaldigital.fechatimbrado
                             ),
                        s_msgExcepcion = ('Fecha Timbrado:%s' % str(_O_excepcion)),
                        s_msgLastSQL = '',
                        E_return = stvfwk2_e_RETURN.NOK_ERROR,
                        s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.VALIDAR % (
                            str(self._D_tablas.cfdis), 'fechatimbrado',
                            self._D_cfdi.complemento.timbrefiscaldigital.fechatimbrado, "", "", ""
                            ),
                        )
                    _D_erroresPrioridad.alta(_D_returnsData, self._generar_mensajeProceso())
            else:
                _D_erroresPrioridad.alta(
                    D_NOTIFICACIONMENSAJE(
                        n_id = 0,
                        s_msgError = "Fecha no esta definida en el cfdi",
                        s_msgExcepcion = "",
                        s_msgLastSQL = "",
                        E_return = stvfwk2_e_RETURN.NOK_ERROR,
                        s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.VALIDAR % (
                            str(self._D_tablas.cfdis), 'fechatimbrado',
                            self._D_cfdi.complemento.timbrefiscaldigital.fechatimbrado, "", "", ""
                            ),
                        s_proceso = self._generar_mensajeProceso()
                        ),
                    s_proceso = self._generar_mensajeProceso()
                    )

            if (str(self._D_cfdi.emisor.rfc) == self._s_rfc) or _b_esPruebaEmitido:
                # Si el RFC de la empresa corresponde al emisor, se asocia el RFC del receptor a clientes

                self._D_procesoMensaje.s_paso = u"CFDI.30.010: Verificar receptor con clientes"
                if (self._D_cfdi.receptor.rfc == _dbRow_cfdi.receptorrfc) or _b_esPruebaEmitido:
                    _D_erroresPrioridad.informacion(self._D_resultOK, self._generar_mensajeProceso())
                elif not _dbRow_cfdi.receptorrfc:
                    _dbRow_cfdi.receptorrfc = self._D_cfdi.receptor.rfc
                else:
                    _D_erroresPrioridad.critica(self._D_resultERROR, self._generar_mensajeProceso())

                self._D_procesoMensaje.s_paso = u"CFDI.30.015: Verificar nombre/razon social con cliente"
                if (self._D_cfdi.receptor.nombre == _dbRow_cfdi.receptornombrerazonsocial) or _b_esPruebaEmitido:
                    _D_erroresPrioridad.informacion(self._D_resultOK, self._generar_mensajeProceso())
                elif not _dbRow_cfdi.receptornombrerazonsocial:
                    _dbRow_cfdi.receptornombrerazonsocial = self._D_cfdi.receptor.nombre
                else:
                    _D_erroresPrioridad.critica(self._D_resultERROR, self._generar_mensajeProceso())

            elif str(self._D_cfdi.receptor.rfc) == self._s_rfc:
                # Si el RFC de la empresa corresponde al receptor, Se asocia el emisor rfc con la tabla de proveedores

                self._D_procesoMensaje.s_paso = u"CFDI.40.010: Verificar emisor con proveedores"
                if self._D_cfdi.emisor.rfc == _dbRow_cfdi.emisorrfc:
                    _D_erroresPrioridad.informacion(self._D_resultOK, self._generar_mensajeProceso())
                elif not _dbRow_cfdi.emisorrfc:
                    _dbRow_cfdi.emisorrfc = self._D_cfdi.emisor.rfc
                else:
                    _D_erroresPrioridad.critica(self._D_resultERROR, self._generar_mensajeProceso())

            else:
                raise ValueError('RFC no esta en receptor ni en emisor, no puede contabilizarse.')

            self._D_procesoMensaje.s_paso = u"CFDI.50.000: Se actualiza la información maestro del CFDI"
            # TODOMejora, se actualiza la información del CFDI con la información recibida; evaluar que hacer en este
            #  caso si hay cambios, aunque esten validados
            if _dbRow_cfdi.sat_status is None:
                if _dbRow_cfdi.sat_fecha_cancelacion:
                    _dbRow_cfdi.sat_status = TCFDIS.SAT_STATUS.CANCELADO
                elif self._D_cfdi.complemento and self._D_cfdi.complemento.timbrefiscaldigital \
                        and self._D_cfdi.complemento.timbrefiscaldigital.nocertificadosat:
                    _dbRow_cfdi.sat_status = TCFDIS.SAT_STATUS.VIGENTE
                else:
                    pass
            else:
                pass

            _D_returnsData = STV_LIB_DB.INSERTAR_ACTUALIZAR_JSON(
                O_json_data = self._D_cfdi,
                D_campos_relacion = self._D_campos_relacion,
                dbTabla = self._D_tablas.cfdis,
                O_row = _dbRow_cfdi,
                )

            if _D_returnsData.E_return != stvfwk2_e_RETURN.OK:
                # Si existen errores en la creación del cfdi
                _D_erroresPrioridad.critica(_D_returnsData, self._generar_mensajeProceso())
                self._n_cfdi_id = 0
            else:

                # Si no hay errores en la creación del CFDI continua la inserción de conceptos, pagos y relacionados
                _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())
                self._n_cfdi_id = _D_returnsData.n_id

                self._D_procesoMensaje.s_paso = u"CFDI.60.000: Se asocia CFDI"

                _D_returnsData = TCFDIS.asociar_uuid(self._n_cfdi_id)
                # TODO capturar errores en la sociación y generar notificaciones, por el momento siempre regresa OK

                self._insertar_actualizar_cfdi2(self._n_cfdi_id)

                _D_erroresPrioridad.agregar(self._importar_xml(xml_cfdi, _dbRow_cfdi))

                _D_erroresPrioridad.agregar(self._verificar_conceptos(_dbRow_cfdi))

                _D_erroresPrioridad.agregar(self._verificar_relacionados(_dbRow_cfdi))

                _D_erroresPrioridad.agregar(self._verificar_pagos(_dbRow_cfdi))

            self._D_procesoMensaje.s_paso = u"CFDI.90.000: Se generan notificaciones"

            _D_notificacionMensajes = _D_erroresPrioridad.getTodos()

            # Si se existen errores para crear notificaciones
            if _D_notificacionMensajes:

                _s_titutlo = ""

                if _D_erroresPrioridad.CRITICA:
                    # Se regresa cualquier inserción a la base de datos dbc01 antes de guardar las notificaciones
                    dbc01.rollback()
                    _s_titutlo = (
                        u"Al menos un error CRÍTICO no permitió la inserción del CFDI %(serie)s %(folio)s %(uuid)s"
                        ) % self._D_cfdi
                    _D_return.s_msgError = "Errores criticos encontrados: "
                    # TODO Grabar el archivo

                elif _D_erroresPrioridad.ALTA:
                    _s_titutlo = (
                        u"Al menos un error ALTA necesita resolverse sobre el CFDI %(serie)s %(folio)s %(uuid)s"
                        ) % self._D_cfdi
                elif _D_erroresPrioridad.MEDIA:
                    _s_titutlo = (
                        u"Al menos un error MEDIA necesita ser revisados sobre el CFDI %(serie)s %(folio)s %(uuid)s"
                        ) % self._D_cfdi
                elif _D_erroresPrioridad.BAJA:
                    _s_titutlo = (
                        u"Al menos un error BAJA se generó durante la importación del CFDI %(serie)s %(folio)s %(uuid)s"
                        ) % self._D_cfdi
                elif _D_erroresPrioridad.INFORMACION:
                    _s_titutlo = (
                        u"Se generó INFORMACIÓN durante la inserción del CFDI %(serie)s %(folio)s %(uuid)s"
                        ) % self._D_cfdi
                else:
                    _s_titutlo = (
                        u"Se generaron notificaciones sin prioridad sobre el CFDI %(serie)s %(folio)s %(uuid)s"
                        ) % self._D_cfdi

                _n_procesotime_ms = int(round((time.time() - _n_procesoinicio_S)*1000))

                # Se procede a insertar la notificación
                _D_fieldsInsert = Storage(
                    empresa_id = self._s_empresa_id,
                    titulo = _s_titutlo,
                    descripcion = '',
                    aplicacion_w2p = request.application,
                    controlador_w2p = request.controller,
                    funcion_w2p = request.function,
                    argumentos = '/'.join(request.args),
                    referencia = '',
                    referencia2 = '',
                    ultimosql = '',
                    estatus = TNOTIFICACIONES.ESTATUS.NUEVO,
                    proceso_id = TNOTIFICACIONES.PROCESO_ID.IMPORTAR_CFDI,
                    reprocesar_aplicacion_w2p = request.application,
                    reprocesar_controlador_w2p = request.controller,
                    reprocesar_funcion_w2p = request.function,
                    reprocesar_argumentos = '/'.join(request.args),
                    user_id_asignado = None,
                    historia = 'Creado por %s, tomo %d ms' % (self._s_cuenta_id, _n_procesotime_ms),
                    )
                _n_notificacion_id = self._D_tablas.notificaciones.insert(**_D_fieldsInsert)

                # Si se inserto la notificación correctamente
                if _n_notificacion_id:

                    for _O_mensaje in _D_notificacionMensajes:

                        # Se procede a insertar el mensaje de la notificación
                        _D_fieldsInsertMensaje = Storage(
                            notificacion_id = _n_notificacion_id,
                            codigovalidacion = _O_mensaje.s_codigovalidacion or "",
                            descripcion = _O_mensaje.s_msgError,
                            excepcion = _O_mensaje.s_msgExcepcion,
                            ultimosql = _O_mensaje.msgLastSQL,
                            referencia_id = _O_mensaje.n_referencia_id or _O_mensaje.n_id,
                            codigoerror = _O_mensaje.E_return,
                            prioridad = _O_mensaje.n_prioridad,
                            proceso = _O_mensaje.s_proceso
                            )
                        _n_notificacionmensaje_id = self._D_tablas.notificacion_mensajes.insert(
                            **_D_fieldsInsertMensaje
                            )

                        if _O_mensaje.n_prioridad == stvfwk2_e_PRIORIDAD.CRITICA:
                            _D_return.s_msgError += "%s [%s]. " % (_O_mensaje.s_proceso, _O_mensaje.s_msgError)
                        else:
                            pass

                    if _D_erroresPrioridad.CRITICA:

                        # Si existen errores criticos
                        _D_return.E_return = stvfwk2_e_RETURN.NOK_CONDICIONES_INCORRECTAS
                        _D_return.n_cfdi_id = 0
                    else:
                        # Todo pasó correctamente y no se generaron notificaciones
                        _D_return.E_return = stvfwk2_e_RETURN.OK
                        # _D_return.s_msgError = u"Importación sin notificaciones, excelente!"
                        _D_return.n_cfdi_id = self._n_cfdi_id

                else:

                    # Si no puedo ser insertada la notificación

                    # Se regresa cualquier inserción a la base de datos dbc01
                    dbc01.rollback()

                    _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
                    _D_return.s_msgError = u"No se pudo generar la notificación en el sistema. Se cancelo la operacion."
                    _D_return.n_cfdi_id = 0
            else:
                # Todo pasó correctamente y no se generaron notificaciones

                _D_return.E_return = stvfwk2_e_RETURN.OK
                _D_return.s_msgError = u"Importación sin notificaciones, excelente!"
                _D_return.n_cfdi_id = self._n_cfdi_id

                pass
        except Exception as _O_excepcion:
            _D_return.E_return = stvfwk2_e_RETURN.NOK_ERROR
            _D_return.s_msgError = u'Proceso: %s, Excepcion: %s' % (self._generar_mensajeProceso(), str(_O_excepcion))
            _D_return.n_cfdi_id = 0

            # Se regresa cualquier inserción a la base de datos dbc01
            dbc01.rollback()

        return _D_return

    # noinspection PyArgumentList
    @_decorador_define_mensajeProceso(s_enEjecucion = u"Conceptos")
    def _verificar_conceptos(
            self,
            D_fieldsInsertCfdi
            ):
        """ Valida la informacion e inserta los conceptos de un CFDI
        
        Return:
            D_RETURN_ERRORES_PRIORIDAD
            
        """
        _D_erroresPrioridad = D_RETURN_ERRORES_PRIORIDAD()

        _n_indice = 0
        for _D_cfdi_concepto in self._D_cfdi.conceptos.concepto:

            _n_indice += 1

            # Se asocia el noidentificacion con la tabla de productos y servicios de la empresa
            self._D_procesoMensaje.s_paso = (
                u"CFDI-Concepto%s.00.000: Identificar concepto en base a tipo de comprobante"
                ) % str(_n_indice)

            if D_fieldsInsertCfdi.tipocomprobante_id in (
                    TTIPOSCOMPROBANTES.EGRESO, TTIPOSCOMPROBANTES.INGRESO, TTIPOSCOMPROBANTES.PAGO
                    ):

                self._D_procesoMensaje.s_paso = (
                    u"CFDI-Concepto%s.10.010: Identificar clave de producto o servicio NoIdentificacion = %s"
                    ) % (str(_n_indice), _D_cfdi_concepto.noidentificacion)

                _dbQry = (self._D_tablas.conceptos.cfdi_id == self._n_cfdi_id) \
                    & (self._D_tablas.conceptos.claveprodserv == _D_cfdi_concepto.claveprodserv) \
                    & (self._D_tablas.conceptos.importe == _D_cfdi_concepto.importe) \
                    & (self._D_tablas.conceptos.noidentificacion == (_D_cfdi_concepto.noidentificacion or ""))

                _dbRows_concepto = dbc01(_dbQry).select(self._D_tablas.conceptos.ALL)

                if len(_dbRows_concepto) != 1:

                    _D_erroresPrioridad.critica(self._D_resultERROR, self._generar_mensajeProceso())

                else:
                    _dbRow_concepto = _dbRows_concepto.first()
                    if (_dbRow_concepto.unidad != _D_cfdi_concepto.unidad) and not _dbRow_concepto.unidad:
                        _dbRow_concepto.unidad = _D_cfdi_concepto.unidad
                        _dbRow_concepto.update_record()
                    else:
                        pass

                    _n_cfdi_concepto_id = _dbRow_concepto.id

                    if _D_cfdi_concepto.impuestos:

                        if (
                                _D_cfdi_concepto.impuestos.traslados
                                and _D_cfdi_concepto.impuestos.traslados.traslado
                                ):

                            _D_erroresPrioridad.agregar(
                                self._verificar_concepto_impuestos(
                                    _n_cfdi_concepto_id,
                                    _D_cfdi_concepto.impuestos.traslados,
                                    self._D_tablas.conceptos_imptrasladados,
                                    'traslado',
                                    ('%s-ImpTrasladados' % str(_n_indice))
                                    )
                                )
                        else:
                            # No hay trasladados
                            pass

                        if (
                                _D_cfdi_concepto.impuestos.retenciones
                                and _D_cfdi_concepto.impuestos.retenciones.retencion
                                ):
                            _D_erroresPrioridad.agregar(
                                self._verificar_concepto_impuestos(
                                    _n_cfdi_concepto_id,
                                    _D_cfdi_concepto.impuestos.retenciones,
                                    self._D_tablas.conceptos_impretenidos,
                                    'retencion',
                                    ('%s-ImpRetenidos' % str(_n_indice))
                                    )
                                )
                        else:
                            # No hay retenidos
                            pass
                    else:
                        # No hay impuestos
                        pass

            else:
                _D_returnsData = D_NOTIFICACIONMENSAJE(
                    n_id = 0,
                    E_return = stvfwk2_e_RETURN.OK,
                    s_msgError = "No hay error, tipo de comprobante no debe tener conceptos asociados",
                    s_msgExcepcion = "tipocomprobante %s" % str(D_fieldsInsertCfdi.tipocomprobante_id),
                    s_msgLastSQL = "",
                    s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.VALIDAR % (
                        str(self._D_tablas.conceptos), "tipocomprobante", self._D_cfdi.tipodecomprobante, "", "", ""
                        ),
                    )
                _D_erroresPrioridad.informacion(_D_returnsData, self._generar_mensajeProceso())

        return _D_erroresPrioridad

    # noinspection PyArgumentList
    @_decorador_define_mensajeProceso(s_enEjecucion = u"Impuestos")
    def _verificar_concepto_impuestos(
            self,
            n_cfdi_concepto_id,
            D_cfdi_concepto_impuestos,
            dbTabla,
            s_cfdiIndex,
            s_procesoSeccion = 'Imp'
            ):
        """ Valida la informacion e inserta los impuestos de los conceptos de un CFDI
    
        Return:
            D_RETURN_ERRORES_PRIORIDAD
            
        """
        _D_erroresPrioridad = D_RETURN_ERRORES_PRIORIDAD()

        _n_indice = 0

        for _D_cfdi_impuesto in D_cfdi_concepto_impuestos[s_cfdiIndex]:

            _n_indice += 1

            # Se asocia el impuesto con la tabla
            self._D_procesoMensaje.s_paso = u"CFDI-Concepto%s%s.10.010: Se verifica el impuesto" % (
                str(s_procesoSeccion), str(_n_indice)
                )

            _dbQry = (dbTabla.cfdi_concepto_id == n_cfdi_concepto_id)
            _dbQry &= (dbTabla.tasaocuota == _D_cfdi_impuesto.tasaocuota)
            _dbQry &= (dbTabla.importe == _D_cfdi_impuesto.importe)
            _dbQry &= (dbTabla.impuesto == _D_cfdi_impuesto.impuesto)

            _dbRows_impuesto = dbc01(_dbQry).select(dbTabla.id)

            if len(_dbRows_impuesto) != 1:

                _D_erroresPrioridad.critica(self._D_resultERROR, self._generar_mensajeProceso())

            else:
                # Si encontró el registro todo bien
                _D_erroresPrioridad.informacion(self._D_resultOK, self._generar_mensajeProceso())
                pass

        return _D_erroresPrioridad

    # noinspection PyArgumentList
    @_decorador_define_mensajeProceso(s_enEjecucion = u"Relacionados")
    def _verificar_relacionados(
            self,
            D_fieldsInsertCfdi
            ):
        """ Valida la informacion e inserta los relacionados de un CFDI
        
        Return:
            D_RETURN_ERRORES_PRIORIDAD
            
        """
        _ = D_fieldsInsertCfdi
        _D_erroresPrioridad = D_RETURN_ERRORES_PRIORIDAD()

        if (
                self._D_cfdi.cfdirelacionados
                and self._D_cfdi.cfdirelacionados.cfdirelacionado
                ):

            _n_indice = 0

            for _D_cfdi_relacionado in self._D_cfdi.cfdirelacionados.cfdirelacionado:

                _n_indice += 1

                # Se asocia el producto o servicio con la tabla
                self._D_procesoMensaje.s_paso = (
                    u"CFDI-Relacionados-%s.10.010: Se verifica el CFDI relacionado"
                    ) % str(_n_indice)

                _dbQry = (self._D_tablas.relacionados.cfdi_id == self._n_cfdi_id)
                _dbQry &= (self._D_tablas.relacionados.uuid == _D_cfdi_relacionado.uuid)

                _dbRows_doctoRelacionado = dbc01(_dbQry).select(self._D_tablas.relacionados.id)

                if len(_dbRows_doctoRelacionado) != 1:

                    _D_erroresPrioridad.critica(self._D_resultERROR, self._generar_mensajeProceso())

                else:
                    # Si encontró el registro todo bien
                    _D_erroresPrioridad.informacion(self._D_resultOK, self._generar_mensajeProceso())
                    pass

        else:
            # No se inserta nada
            pass

        return _D_erroresPrioridad

    # noinspection PyArgumentList
    @_decorador_define_mensajeProceso(s_enEjecucion = u"Pagos")
    def _verificar_pagos(
            self,
            D_fieldsInsertCfdi
            ):
        """ Valida la informacion e inserta los pagos de un CFDI
        
        Return:
            D_RETURN_ERRORES_PRIORIDAD
            
        """
        _D_erroresPrioridad = D_RETURN_ERRORES_PRIORIDAD()

        self._D_procesoMensaje.s_paso = u"CFDI-Pagos.00.000: Identificar pagos"

        if (
                self._D_cfdi.complemento.pagos
                and self._D_cfdi.complemento.pagos.pago
                ):

            if not(D_fieldsInsertCfdi.tipocomprobante_id in (TTIPOSCOMPROBANTES.PAGO,)):
                _D_erroresPrioridad.media(
                    D_NOTIFICACIONMENSAJE(
                        n_id = 0,
                        s_msgError = "Existen pagos para tipos de comprobantes diferentes a Pago",
                        s_msgExcepcion = "",
                        s_msgLastSQL = "",
                        E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS,
                        s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.VALIDAR % (
                            str(self._D_tablas.comppagos), 'pago:formapago',
                            self._D_cfdi.complemento.pagos.version, "", "", ""
                            ),
                        s_proceso = self._generar_mensajeProceso()
                        ),
                    s_proceso = self._generar_mensajeProceso()
                    )
            else:
                # No hay problema, es un coprobante de pago, debe incluir pagos
                pass

            for _D_pago in self._D_cfdi.complemento.pagos.pago:

                # Se agregan los campos standard con sus valores
                _D_fieldsInsertPago = Storage(
                    cfdi_id = self._n_cfdi_id
                    )

                self._D_procesoMensaje.s_paso = u"CFDI-Pagos.10.010: Verificar pago"

                _dbQry = (self._D_tablas.comppagos.cfdi_id == self._n_cfdi_id) \
                    & (self._D_tablas.comppagos.versionpago == _D_pago.version) \
                    & (self._D_tablas.comppagos.fechapago_str == _D_pago.fechapago) \
                    & (self._D_tablas.comppagos.formapago == _D_pago.formadepagop) \
                    & (self._D_tablas.comppagos.monto == _D_pago.monto)

                _dbRows_pago = dbc01(_dbQry).select(self._D_tablas.comppagos.id)

                if len(_dbRows_pago) != 1:

                    _D_erroresPrioridad.critica(self._D_resultERRORR, self._generar_mensajeProceso())

                else:
                    # Si encontró el registro todo bien
                    _D_erroresPrioridad.informacion(self._D_resultOK, self._generar_mensajeProceso())

                    _cfdi_complementopago_id = _dbRows_pago.first().id

                    _n_indice = 0

                    if _D_pago.doctorelacionado:
                        for _D_doctorelacionado in _D_pago.doctorelacionado:

                            _n_indice += 1

                            # Se asocia el producto o servicio con la tabla
                            self._D_procesoMensaje.s_paso = (
                                u"CFDI-Pagos-Pago%s.10.010: Verificar documento relacionado del pago"
                                ) % str(_n_indice)

                            _dbQry = (self._D_tablas.comppago_docsrelacionados.cfdi_complementopago_id
                                    == _cfdi_complementopago_id) \
                                & (self._D_tablas.comppago_docsrelacionados.iddocumento
                                    == _D_doctorelacionado.iddocumento) \
                                & (self._D_tablas.comppago_docsrelacionados.imppagado
                                    == _D_doctorelacionado.imppagado)

                            _dbRows_pago_doctoRelacionado = dbc01(
                                _dbQry
                                ).select(
                                    self._D_tablas.comppago_docsrelacionados.id
                                    )

                            if len(_dbRows_pago_doctoRelacionado) != 1:

                                _D_erroresPrioridad.critica(self._D_resultERROR, self._generar_mensajeProceso())

                            else:
                                # Si encontró el registro todo bien
                                _D_erroresPrioridad.informacion(self._D_resultOK, self._generar_mensajeProceso())
                                pass
                    else:
                        # No existieron documentos relacionados a los pagos
                        _D_erroresPrioridad.alta(
                            D_NOTIFICACIONMENSAJE(
                                n_id = 0,
                                s_msgError = "El pago no genero documentos relacionados",
                                s_msgExcepcion = "",
                                s_msgLastSQL = "",
                                E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS,
                                s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.VALIDAR % (
                                    str(self._D_tablas.comppagos), 'pago:doctorelacionado',
                                    _D_pago.doctorelacionado, "", "", ""
                                    ),
                                s_proceso = self._generar_mensajeProceso()
                                ),
                            s_proceso = self._generar_mensajeProceso()
                            )
        else:
            # Si no existe pago
            if D_fieldsInsertCfdi.tipocomprobante_id in (TTIPOSCOMPROBANTES.PAGO,):
                _D_erroresPrioridad.alta(
                    D_NOTIFICACIONMENSAJE(
                        n_id = 0,
                        s_msgError = "Información sobre el comprobante de pago no existe",
                        s_msgExcepcion = "",
                        s_msgLastSQL = "",
                        E_return = stvfwk2_e_RETURN.NOK_DATOS_INVALIDOS,
                        s_codigovalidacion = D_NOTIFICACIONMENSAJE.CODIGOVALIDACION.VALIDAR % (
                            str(self._D_tablas.comppagos), 'pago:formapago', "", "", "", ""
                            ),
                        s_proceso = self._generar_mensajeProceso()
                        ),
                    s_proceso = self._generar_mensajeProceso()
                    )
            else:
                # No hay razon para que existe un pago
                pass

        return _D_erroresPrioridad
