# -*- coding: utf-8 -*-
import _io
import datetime
import re
from copy import copy, deepcopy

from gluon import *
from gluon.storage import Storage
# from gluon.dal import DAL, Field
# from gluon.sqlhtml import SQLFORM
# from gluon.validators import IS_NOT_EMPTY, IS_EMAIL, IS_LENGTH

from reportlab.pdfbase import pdfmetrics
from reportlab.pdfgen.canvas import Canvas
from reportlab.lib import pagesizes
import PollyReports as PR
# from cStringIO import StringIO  # Para Python2.7
import io  # Para Python3
    

class STV_FWK_REPORT_PDF:
    """ Generación de reportes para STV """

    class E_ALINEACION:
        IZQUIERDA = 1
        DERECHA = 2
        CENTRO = 3
        ARRIBA = 4
        ABAJO = 5
        _D_dict = None
        _D_dictLabels = None

        @classmethod
        def GET_DICT(cls):
            if not cls._D_dict:
                cls._D_dict = {
                    cls.IZQUIERDA: 'left',
                    cls.DERECHA  : 'right',
                    cls.CENTRO   : 'center',
                    }
            else:
                pass
            return cls._D_dict

        @classmethod
        def GET_DICT_LABELS(cls):
            if not cls._D_dictLabels:
                cls._D_dictLabels = {
                    cls.IZQUIERDA: 'Izquierda',
                    cls.DERECHA  : 'Derecha',
                    cls.CENTRO   : 'Centrado',
                    }
            else:
                pass
            return cls._D_dictLabels

    class LETRA(Storage):

        class COLOR:
            TRANSPARENTE = Storage(R = 1, G = 0, B = 0, alpha = 0)
            ROJO = Storage(R = 1, G = 0, B = 0, alpha = 1)
            VERDE = Storage(R = 0, G = 1, B = 0, alpha = 1)
            AZUL = Storage(R = 0, G = 0, B = 1, alpha = 1)
            NEGRO = Storage(R = 0, G = 0, B = 0, alpha = 1)
            STV_VERDE = Storage(R = 26 / 255.0, G = 179 / 255.0, B = 148 / 255.0, alpha = 1)
            STV_VERDE_T = Storage(R = 26 / 255.0, G = 179 / 255.0, B = 148 / 255.0, alpha = 0.25)
            STV_GRIS = Storage(R = 103 / 255.0, G = 106 / 255.0, B = 108 / 255.0, alpha = 1)
            STV_GRIS_T = Storage(R = 103 / 255.0, G = 106 / 255.0, B = 108 / 255.0, alpha = 0.25)

        class E_TIPO:
            COURIER = 1
            COURIER_BOLD = 2
            COURIER_BOLDOBLIQUE = 3
            COURIER_OBLIQUE = 4
            HELVETICA = 5
            HELVETICA_BOLD = 5
            HELVETICA_BOLDOBLIQUE = 7
            HELVETICA_OBLIQUE = 8
            SYMBOL = 9
            TIMES_BOLD = 10
            TIMES_BOLDITALIC = 11
            TIMES_ITALIC = 12
            TIMES_ROMAN = 13
            ZAPFDINGBATS = 14
            _D_dict = None

            @classmethod
            def GET_DICT(cls):
                if not cls._D_dict:
                    cls._D_dict = {
                        cls.COURIER              : 'Courier',
                        cls.COURIER_BOLD         : 'Courier-Bold',
                        cls.COURIER_BOLDOBLIQUE  : 'Courier-BoldOblique',
                        cls.COURIER_OBLIQUE      : 'Courier-Oblique',
                        cls.HELVETICA            : 'Helvetica',
                        cls.HELVETICA_BOLD       : 'Helvetica-Bold',
                        cls.HELVETICA_BOLDOBLIQUE: 'Helvetica-BoldOblique',
                        cls.HELVETICA_OBLIQUE    : 'Helvetica-Oblique',
                        cls.SYMBOL               : 'Symbol',
                        cls.TIMES_BOLD           : 'Times-Bold',
                        cls.TIMES_BOLDITALIC     : 'Times-BoldItalic',
                        cls.TIMES_ITALIC         : 'Times-Italic',
                        cls.TIMES_ROMAN          : 'Times-Roman',
                        cls.ZAPFDINGBATS         : 'ZapfDingbats',
                        }
                else:
                    pass
                return cls._D_dict

        def __init__(
                self,
                E_tipoletra,
                n_tamanio,
                D_colorRGB_alpha = None
                ):
            """ Determina la posición de un objeto en el reporte

            @param x: posición x después del margen
            @param y: offset en y del renglón done toca la impresión

            """

            D_colorRGB_alpha = D_colorRGB_alpha or Storage(R = 0, G = 0, B = 0, alpha = 1)
            super(Storage, self).__init__(
                {
                    'fonttype': STV_FWK_REPORT_PDF.LETRA.E_TIPO.GET_DICT()[E_tipoletra],
                    'size'    : n_tamanio,
                    'R'       : D_colorRGB_alpha.R,
                    'G'       : D_colorRGB_alpha.G,
                    'B'       : D_colorRGB_alpha.B,
                    'alpha'   : D_colorRGB_alpha.alpha,
                    }
                )
            return

    class POSICION(Storage):

        def __init__(self, x, y):
            """ Determina la posición de un objeto en el reporte

            @param x: posición x después del margen
            @param y: offset en la banda done toca la impresión

            """
            super(Storage, self).__init__({0: x, 1: y, 'x': x, 'y': y})
            return

        def __copy__(self):
            _O_nuevaPosicion = STV_FWK_REPORT_PDF.POSICION(self.x, self.y)
            return _O_nuevaPosicion

    class E_TAMANIO:
        CARTA = 1
        OFICIO = 2

        @classmethod
        def GET_DICT(cls):
            return {
                cls.CARTA : 'Carta',
                cls.OFICIO: 'Oficio',
                }

    class E_ORIENTACION:
        VERTICAL = 1
        HORIZONTAL = 2

        @classmethod
        def GET_DICT(cls):
            return {
                cls.VERTICAL  : 'Vertical',
                cls.HORIZONTAL: 'Horizontal',
                }

    class E_TIPOTEXTO:
        TEXTO = 0,
        SUMA = 1,
        CUENTA = 2

    D_DBCAMPOS = Storage(
        OPCION_TITULO = Field(
            'opcion_titulo', 'boolean', default = request.vars.get("opcion_titulo", 0),
            required = False,
            notnull = False,
            widget = stv_widget_inputCheckbox, label = T('Imprimir Título'),
            comment = "Imprimir página de título del reporte",
            writable = True, readable = True,
            represent = stv_represent_boolean
            ),
        PAGINA_TAMANIO = Field(
            'pagina_tamanio', 'integer', default = request.vars.get("pagina_tamanio", E_TAMANIO.CARTA),
            required = True, requires = IS_IN_SET(E_TAMANIO.GET_DICT(), zero = None, error_message = 'Selecciona'),
            notnull = True,
            widget = stv_widget_combobox, label = 'Tamaño de Página', comment = '',
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, STV_FWK_REPORT_PDF.E_TAMANIO.CARTA.GET_DICT())
            ),
        PAGINA_ORIENTACION = Field(
            'pagina_orientacion', 'integer', default = request.vars.get("pagina_orientacion", E_ORIENTACION.VERTICAL),
            required = True, requires = IS_IN_SET(E_ORIENTACION.GET_DICT(), zero = None, error_message = 'Selecciona'),
            notnull = True,
            widget = stv_widget_combobox, label = 'Orientación de Página', comment = '',
            writable = True, readable = True,
            represent = lambda v, r: stv_represent_list(v, r, None, STV_FWK_REPORT_PDF.E_ORIENTACION.CARTA.GET_DICT())
            ),
        ENVIO_EMAILS = Field(
            'envio_emails', 'text', default = request.vars.get("envio_emails", None),
            required = False,
            notnull = False,
            widget = stv_widget_text, label = 'Emails',
            comment = "Cuentas de email a enviar el reporte, separadas por comas",
            writable = True, readable = True,
            represent = stv_represent_text
            ),
        ENVIO_MENSAJE = Field(
            'envio_mensaje', 'text', default = request.vars.get("envio_mensaje", None),
            required = False,
            notnull = False,
            widget = stv_widget_text, label = 'Mensaje', comment = "Mensaje a enviar en el email",
            writable = True, readable = True,
            represent = stv_represent_text
            ),
        )
    
    @staticmethod
    def R_NUMBER(n_number, n_decimals, s_prefix = "", s_postfix = "", b_ifnotzero = True):
        if b_ifnotzero and (n_number != 0):
            return str(s_prefix) + (("%."+str(n_decimals)+"f") % n_number) + str(s_postfix)
        else:
            return ""
    
    _O_reporte = None  # Contendrá el reporte a imprimir

    def __init__(
            self, 
            n_margenHorizontal = 36, 
            n_margenAbajo = 36, 
            n_margenArriba = 36, 
            E_tamanio = E_TAMANIO.CARTA, 
            E_orientacion = E_ORIENTACION.VERTICAL, 
            # n_puntosSeparacionColumnas = 3,
            # n_columnasDeAjuste = 12
            ):
        """ Inicialización de la clase para manejo del Reporte

        @param n_margenHorizontal: Determina el margen izquierdo y derecho
        @param n_margenAbajo: Determina el margen de abajo de la página
        @param n_margenArriba: Determina el margen superior
        @param E_tamanio: Determina el tamaño de la página
        @param E_orientacion: Determina el margen izquierdo y derecho
        @param n_puntosSeparacionColumnas: Determina el margen izquierdo y derecho
        @param n_columnasDeAjuste: Determina el margen izquierdo y derecho
        """

        self._O_reporte = None

        self._n_margenHorizontal = n_margenHorizontal 
        self._n_margenAbajo = n_margenAbajo 
        self._n_margenArriba = n_margenArriba 
        self._E_tamanio = int(E_tamanio)
        self._E_orientacion = int(E_orientacion)
        # self._n_puntosSeparacionColumnas = n_puntosSeparacionColumnas
        # self._n_columnasDeAjuste = n_columnasDeAjuste
        
        self._L_tamanioPagina = []
        
        if STV_FWK_REPORT_PDF.E_TAMANIO.CARTA == self._E_tamanio:
            self._L_tamanioPagina = pagesizes.LETTER
              
        elif STV_FWK_REPORT_PDF.E_TAMANIO.OFICIO == self._E_tamanio:
            self._L_tamanioPagina = pagesizes.LEGAL
            
        else:
            self._L_tamanioPagina = pagesizes.LETTER

        if STV_FWK_REPORT_PDF.E_ORIENTACION.VERTICAL == self._E_orientacion:
            self._L_tamanioPagina = pagesizes.portrait(self._L_tamanioPagina)
            
        elif STV_FWK_REPORT_PDF.E_ORIENTACION.HORIZONTAL == self._E_orientacion:
            self._L_tamanioPagina = pagesizes.landscape(self._L_tamanioPagina)
            
        else:
            self._L_tamanioPagina = pagesizes.portrait(self._L_tamanioPagina)
        
        self._n_tamanioHzEditable = float(self._L_tamanioPagina[0] - (2 * self._n_margenHorizontal))
        
        # self._n_tamanioDeColumna = float(self._n_tamanioHzEditable) / float(self._n_columnasDeAjuste)
        
        self._B_reporteTitulo = PR.Band([])
        self._B_reporteDetalle = PR.Band([])
        self._B_reporteEncabezado = PR.Band([])
        self._B_reportePie = PR.Band([])
        self._B_paginaEncabezado = PR.Band([])
        self._B_paginaPie = PR.Band([])
        self._LB_grupos_encabezado = []
        self._LB_grupos_pie = []
        
    @property
    def n_anchoMaximo(self):
        return self._n_tamanioHzEditable

    @property
    def n_anchoCentro(self):
        return self._n_tamanioHzEditable / 2.0

    def _fomatear_valorCon_dbRow(
            self,
            dbRow,
            s_campo = None,
            s_texto = None,
            ):
        _ = self
        if s_campo and dbRow:
            _s_valorRaw = dbRow[s_campo]
        elif s_texto:
            _s_valorRaw = s_texto
        else:
            _s_valorRaw = ""  # No debe poder entrar aquí
                    
        return _s_valor

    def _generarPuntosEsquinas(
            self,
            D_puntoIzqArriba,
            n_ancho,
            n_alto
            ):
        _D_puntos = Storage()

        _D_puntos.IzqArriba = copy(D_puntoIzqArriba)
        _D_puntos.DerArriba = copy(_D_puntos.IzqArriba)
        _D_puntos.DerArriba.x += n_ancho
        _D_puntos.DerArriba[0] = _D_puntos.DerArriba.x
        _D_puntos.IzqAbajo = copy(_D_puntos.IzqArriba)
        _D_puntos.IzqAbajo.y += n_alto
        _D_puntos.IzqAbajo[1] = _D_puntos.IzqAbajo.y
        _D_puntos.DerAbajo = copy(_D_puntos.DerArriba)
        _D_puntos.DerAbajo.y += n_alto
        _D_puntos.DerAbajo[1] = _D_puntos.IzqAbajo.y

        return _D_puntos

    def configurar_objeto_imagen(
            self,
            D_posicion,
            x_imagen = "",
            s_campo = None,
            x_getvalue = None,
            E_alineacion = E_ALINEACION.IZQUIERDA,
            E_alineacionV = E_ALINEACION.ARRIBA,
            x_formato = str,
            n_ancho = None,
            n_alto = None,
            x_onrender = None,
            b_marco = False,
            D_marco = None,
            D_fondo = None,
            **D_otros
            ):
        _D_return = Storage(
            L_elementos = [],
            n_offsetAlto = D_posicion.y,
            n_tamanioAlto = 0.0,
            n_tamanioAncho = 0.0,
            n_tamOrigAlto = 0.0,
            n_tamOrigAncho = 0.0,
            n_relacionAncho = 0.0,
            n_relacionAlto = 0.0,
            D_posicion = copy(D_posicion),
            )

        if x_imagen:
            from PIL import Image as PILImage

            _O_imagenData = self._leer_imagen(x_imagen)
            _D_return.n_tamOrigAlto = _O_imagenData.n_alto
            _D_return.n_tamOrigAncho = _O_imagenData.n_ancho

            _n_relacionAncho = 1.0
            _n_relacionAlto = 1.0

            _n_nuevoAlto = self._ajustar_ancho(_D_return.n_tamOrigAncho, _D_return.n_tamOrigAlto, n_ancho)
            _n_nuevoAncho = self._ajustar_ancho(_D_return.n_tamOrigAncho, _D_return.n_tamOrigAlto, n_alto)

            if n_ancho:
                # Si el nuevo alto calculado, es menor o igual que el alto máximo, se usa este rate
                _D_return.n_tamanioAlto = _n_nuevoAlto
                _D_return.n_tamanioAncho = n_ancho

            elif n_alto:
                # Si el nuevo alto calculado, es menor o igual que el alto máximo, se usa este rate
                _D_return.n_tamanioAlto = n_alto
                _D_return.n_tamanioAncho = _n_nuevoAncho

            else:
                _D_return.n_tamanioAlto = _n_nuevoAlto
                _D_return.n_tamanioAncho = _n_nuevoAncho

            # Se especifican los altos y anchos en caso de no estar definidos
            n_ancho = n_ancho if n_ancho else _D_return.n_tamanioAncho
            n_alto = n_alto if n_alto else _D_return.n_tamanioAlto

            _T_tamanio = (int(_D_return.n_tamanioAncho), int(_D_return.n_tamanioAlto))

            _O_imagenData.O_imagen.thumbnail(_T_tamanio, PILImage.ANTIALIAS)

            _T_colorFondo = (int(D_fondo.R * 255), int(D_fondo.G * 255), int(D_fondo.B * 255), int(D_fondo.alpha*255)) \
                if D_fondo else (255, 255, 255, 0)

            _O_fondo = PILImage.new('RGBA', (int(n_ancho), int(n_alto)), _T_colorFondo)
            _O_fondo.paste(
                _O_imagenData.O_imagen,
                (
                    int((n_ancho - _D_return.n_tamanioAncho) / 2.0),
                    int((n_alto - _D_return.n_tamanioAlto) / 2.0))
                )

            from reportlab.lib.utils import ImageReader

            _O_buffer = _io.BytesIO()
            _O_fondo.save(_O_buffer, "PNG")
            _O_image = ImageReader(_O_buffer)

            if b_marco and not D_marco:
                D_marco = Storage(
                    D_izquierdo = Storage(n_grosor = 1, E_color = STV_FWK_REPORT_PDF.LETRA.COLOR.STV_VERDE),
                    D_derecho = Storage(n_grosor = 1, E_color = STV_FWK_REPORT_PDF.LETRA.COLOR.STV_VERDE),
                    D_superior = Storage(n_grosor = 1, E_color = STV_FWK_REPORT_PDF.LETRA.COLOR.STV_VERDE),
                    D_inferior = Storage(n_grosor = 1, E_color = STV_FWK_REPORT_PDF.LETRA.COLOR.STV_VERDE)
                    )
            else:
                pass

            _D_results = self.helper_crearMarco(D_posicion, n_ancho, n_alto, D_marco)
            _D_return.L_elementos += _D_results.L_elementos

            if E_alineacion == STV_FWK_REPORT_PDF.E_ALINEACION.IZQUIERDA:
                # Posición no cambia
                pass
            elif E_alineacion == STV_FWK_REPORT_PDF.E_ALINEACION.DERECHA:
                D_posicion.x += n_ancho - _D_return.n_tamanioAncho
                D_posicion[0] = D_posicion.x
            elif E_alineacion == STV_FWK_REPORT_PDF.E_ALINEACION.CENTRO:
                D_posicion.x += (n_ancho - _D_return.n_tamanioAncho) / 2.0
                D_posicion[0] = D_posicion.x
            else:
                pass

            if E_alineacionV == STV_FWK_REPORT_PDF.E_ALINEACION.ARRIBA:
                # Posición no cambia
                pass
            elif E_alineacionV == STV_FWK_REPORT_PDF.E_ALINEACION.ABAJO:
                D_posicion.y += n_alto - _D_return.n_tamanioAlto
                D_posicion[1] = D_posicion.y
            elif E_alineacionV == STV_FWK_REPORT_PDF.E_ALINEACION.CENTRO:
                D_posicion.y += (n_alto - _D_return.n_tamanioAlto) / 2.0
                D_posicion[1] = D_posicion.y
            else:
                pass

            _D_return.n_offsetAlto += n_alto

            _D_return.L_elementos.append(
                PR.Image(
                    pos = copy(D_posicion),
                    width = _D_return.n_tamanioAncho,
                    height = _D_return.n_tamanioAlto,
                    text = _O_image,
                    key = s_campo,
                    getvalue = x_getvalue,
                    onrender = x_onrender
                    )
                )

        else:
            pass

        return _D_return

    def _configurar_objeto_texto(
            self,
            D_posicion,
            s_texto = None,
            s_campo = None,
            x_getvalue = None,
            s_sysvar = None,
            E_alineacion = E_ALINEACION.IZQUIERDA,
            x_formato = str,
            D_color = None,
            n_ancho = None,
            E_tipoletra = LETRA.E_TIPO.TIMES_ROMAN,
            n_letratamanio = 10,
            x_onrender = None,
            b_marco = False,
            D_marco = None,
            D_fondo = None,
            E_tipotexto = E_TIPOTEXTO.TEXTO,
            **D_otros
            ):
        """ Configura un objeto de tipo texto
        
        @param D_marco:
            D_izquierdo, D_derecho, D_superior, D_inferior:
                n_grosor:
                E_color
            
        """
        D_color = D_color or STV_FWK_REPORT_PDF.LETRA.COLOR.NEGRO
        
        _D_return = Storage(
            L_elementos = [],
            n_offsetAlto = D_posicion.y,
            n_tamanioAlto = 0.0,
            D_posicion = copy(D_posicion),
            n_anchoTexto = 0.0
            )
        
        n_ancho = n_ancho or (self.n_anchoMaximo - D_posicion.x)

        if s_texto.strip():
            _D_return.n_anchoTexto = self._calcular_anchoTexto(s_texto, E_tipoletra, n_letratamanio)
            _L_textoRenglones = self._calcular_renglones(
                s_texto = s_texto,
                E_tipoletra = E_tipoletra, 
                n_letratamanio = n_letratamanio, 
                n_ancho = n_ancho,
                D_params = D_otros
                )
            
            _D_return.n_tamanioAlto = float(len(_L_textoRenglones)) * (n_letratamanio + 3.0)
        else:
            _L_textoRenglones = [""]
            _D_return.n_tamanioAlto = n_letratamanio + 3.0
        
        if n_ancho:
            
            if b_marco and not D_marco:
                D_marco = Storage(
                    D_izquierdo = Storage(n_grosor = 1, E_color = STV_FWK_REPORT_PDF.LETRA.COLOR.STV_VERDE),
                    D_derecho = Storage(n_grosor = 1, E_color = STV_FWK_REPORT_PDF.LETRA.COLOR.STV_VERDE),
                    D_superior = Storage(n_grosor = 1, E_color = STV_FWK_REPORT_PDF.LETRA.COLOR.STV_VERDE),
                    D_inferior = Storage(n_grosor = 1, E_color = STV_FWK_REPORT_PDF.LETRA.COLOR.STV_VERDE)
                    )
            else:
                pass

            _D_results = self.helper_crearMarco(D_posicion, n_ancho, _D_return.n_tamanioAlto, D_marco)
            _D_return.L_elementos += _D_results.L_elementos

            if E_alineacion == STV_FWK_REPORT_PDF.E_ALINEACION.IZQUIERDA:
                # Posición no cambia
                pass
            elif E_alineacion == STV_FWK_REPORT_PDF.E_ALINEACION.DERECHA:
                D_posicion.x += n_ancho
                D_posicion[0] = D_posicion.x
            elif E_alineacion == STV_FWK_REPORT_PDF.E_ALINEACION.CENTRO:
                D_posicion.x += (n_ancho / 2.0)
                D_posicion[0] = D_posicion.x
            else:
                pass
            
        else:
            
            pass
        
        for _s_textoRenglon in _L_textoRenglones:
            if E_tipotexto == STV_FWK_REPORT_PDF.E_TIPOTEXTO.SUMA:
                _D_return.L_elementos.append(
                    PR.SumElement(
                        pos = copy(D_posicion),
                        font = STV_FWK_REPORT_PDF.LETRA(E_tipoletra, n_letratamanio, D_colorRGB_alpha = D_color),
                        text = _s_textoRenglon, key = s_campo, getvalue = x_getvalue, sysvar = s_sysvar,
                        align = STV_FWK_REPORT_PDF.E_ALINEACION.GET_DICT()[E_alineacion],
                        format = x_formato,  # Función que puede ser usada para formatear un valor
                        width = n_ancho, leading = None,
                        onrender = x_onrender,  # Evento al momento de hacer un render
                        D_params = D_otros
                        )
                    )                
            
            elif E_tipotexto == STV_FWK_REPORT_PDF.E_TIPOTEXTO.CUENTA:
                _D_return.L_elementos.append(
                    PR.CountElement(
                        pos = copy(D_posicion),
                        font = STV_FWK_REPORT_PDF.LETRA(E_tipoletra, n_letratamanio, D_colorRGB_alpha = D_color),
                        text = _s_textoRenglon, key = s_campo, getvalue = x_getvalue, sysvar = s_sysvar,
                        align = STV_FWK_REPORT_PDF.E_ALINEACION.GET_DICT()[E_alineacion],
                        format = x_formato,  # Función que puede ser usada para formatear un valor
                        width = n_ancho, leading = None,
                        onrender = x_onrender,  # Evento al momento de hacer un render
                        D_params = D_otros
                        )
                    )              
            else:
                # Cualquier otra cosa o si es de tipo TEXTO
                _D_return.L_elementos.append(
                    PR.Element(
                        pos = copy(D_posicion),
                        font = STV_FWK_REPORT_PDF.LETRA(E_tipoletra, n_letratamanio, D_colorRGB_alpha = D_color),
                        text = _s_textoRenglon, key = s_campo, getvalue = x_getvalue, sysvar = s_sysvar,
                        align = STV_FWK_REPORT_PDF.E_ALINEACION.GET_DICT()[E_alineacion],
                        format = x_formato,  # Función que puede ser usada para formatear un valor
                        width = n_ancho, leading = None,
                        onrender = x_onrender,  # Evento al momento de hacer un render
                        D_params = D_otros
                        )
                    )
                
            D_posicion.y += n_letratamanio + 3
            D_posicion[1] = D_posicion.y
           
        _D_return.n_offsetAlto = D_posicion.y

        if D_fondo:
            _D_results = self.helper_crearCuadro(
                D_posicion,
                n_ancho,
                -1 * _D_return.n_tamanioAlto,
                **D_fondo
                )
            _D_return.L_elementos = _D_results.L_elementos + _D_return.L_elementos
        else:
            pass
            
        return _D_return
       
    def _ajustar_ancho(self, n_ancho, n_alto, n_alto_ajustado):
        _ = self
        _n_rate = (float(n_alto_ajustado) / float(n_alto)) if n_alto_ajustado else 1
        return n_ancho * _n_rate

    def _ajustar_alto(self, n_ancho, n_alto, n_ancho_ajustado):
        _ = self
        _n_rate = (float(n_ancho_ajustado) / float(n_ancho)) if n_ancho_ajustado else 1
        return n_alto * _n_rate
    
    def _calcular_anchoTexto(self, s_texto, E_tipoletra, n_letratamanio):
        _ = self
        s_tipoletra = STV_FWK_REPORT_PDF.LETRA.E_TIPO.GET_DICT().get(
            E_tipoletra, STV_FWK_REPORT_PDF.LETRA.E_TIPO.SYMBOL)
        s_texto = s_texto if isinstance(s_texto, str) else str(s_texto)
        # Se pone el tipo de letra symbol para mostrar error si no se reconoce el tipo de letra
        return pdfmetrics.stringWidth(s_texto, s_tipoletra, n_letratamanio)

    def _calcular_renglones(self, s_texto, E_tipoletra, n_letratamanio, n_ancho, D_params):

        def calcular_anchoTexto(s_nuevoTexto):
            return self._calcular_anchoTexto(s_nuevoTexto, E_tipoletra, n_letratamanio)

        _D_params = Storage(D_params) if D_params else Storage()
        _D_params.setdefault('b_multilinea', True)
        _D_params.setdefault('b_ajustarTexto', True)

        _L_parrafos = s_texto.split("\n")
        _L_Lineas = []
        _L_PalabrasLinea = []

        for _s_parrafo in _L_parrafos:
            # Se busca el recorrer los párrafos, separados por saldo de línea

            _L_palabras = _s_parrafo.split()
            if calcular_anchoTexto(_s_parrafo) > n_ancho:
                # Si el parrafo excede el ancho máximo se empieza a buscar cuantas palabra caben

                for _s_palabra in _L_palabras:

                    if not _L_PalabrasLinea:
                        _L_PalabrasLinea = [_s_palabra]

                    else:
                        if calcular_anchoTexto(" ".join(_L_PalabrasLinea + [_s_palabra])) > n_ancho:
                            if _D_params.b_multilinea:
                                # Si se excede el tamaño, se graba una línea y se empieza la siguiente
                                _L_Lineas.append(" ".join(_L_PalabrasLinea))
                                _L_PalabrasLinea = [_s_palabra]
                            else:
                                # Si no tiene configurado multilinea y llegó al máximo, despliega un último caracter
                                curline.append(">")
                                break
                        else:
                            # Si no se excedió el tamaño se agrega la palabra
                            _L_PalabrasLinea.append(_s_palabra)

                if (len(_L_Lineas) == 0) and (len(_L_PalabrasLinea) == 1):
                    # Si fué una palabra solamente la que excedió el tamaño, se corta y agregan tres puntos

                    if calcular_anchoTexto(" ".join(_L_PalabrasLinea)) > n_ancho:
                        if _D_params.b_ajustarTexto:
                            # lines.append(" ".join(curline))
                            _n_ancho_sinPuntos = n_ancho - calcular_anchoTexto("...")
                            if _n_ancho_sinPuntos > 0:
                                while calcular_anchoTexto(" ".join(_L_PalabrasLinea)) > _n_ancho_sinPuntos:
                                    _L_PalabrasLinea[0] = _L_PalabrasLinea[0][:-1]
                            else:
                                _L_PalabrasLinea[0] = "Error en texto"

                            _L_PalabrasLinea[0] = _L_PalabrasLinea[0] + "..."

                        else:
                            # Si no se desean los tres puntos, solamente cortar el texto
                            _s_texto = " ".join(_L_PalabrasLinea)
                            _n_index = 0
                            _L_PalabrasLinea = []
                            while _n_index < len(_s_texto):
                                _s_textoLinea = ""
                                while calcular_anchoTexto(_s_textoLinea) < n_ancho:
                                    _s_textoLinea += _s_texto[_n_index]
                                    _n_index += 1
                                    if _n_index < len(_s_texto):
                                        pass
                                    else:
                                        break
                                if _n_index == len(_s_texto):
                                    _L_Lineas.append(_s_textoLinea)
                                else:
                                    _L_Lineas.append(_s_textoLinea[:-1])
                                    _n_index -= 1
                                if _D_params.b_multilinea:
                                    pass
                                else:
                                    break
                    else:
                        pass
                else:
                    pass

            else:
                _L_PalabrasLinea = _L_palabras
                        
            if _L_PalabrasLinea:
                _L_Lineas.append(" ".join(_L_PalabrasLinea))
                _L_PalabrasLinea = []
            else:
                pass
                
        return _L_Lineas

    def _leer_imagen(self, x_imagen):
        _ = self
        _D_return = Storage(
            O_imagen = None,
            n_ancho = None,
            n_alto = None
            )
        
        from PIL import Image as PILImage

        if isinstance(x_imagen, str):
            _D_return.O_imagen = PILImage.open(x_imagen)

        else:
            _D_return.O_imagen = x_imagen
        _D_return.n_ancho = _D_return.O_imagen.size[0]
        _D_return.n_alto = _D_return.O_imagen.size[1]

        return _D_return
    
    def helper_crearTexto(
            self,
            D_posicion = None,
            D_texto = None
            ):
        D_posicion = D_posicion or STV_FWK_REPORT_PDF.POSICION(0, 0)
        _D_return = Storage(
            L_elementos = [],
            n_offsetAlto = D_posicion.y,
            D_elementos = Storage()
            )
        
        if D_texto:
            D_texto.D_posicion = D_texto.D_posicion or D_posicion
            
            _D_return = self._configurar_objeto_texto(**D_texto)
        else:
            pass
        
        return _D_return        
         
    def helper_crearEncabezado(
            self,
            D_linea1 = None,
            D_linea2 = None,
            s_rutaImagen = None,
            D_fecha = None,
            D_textoControl = None,
            b_linea = False,
            n_offsetAlto = 0
            ):
        
        _D_return = Storage(
            L_elementos = [],
            n_offsetAlto = n_offsetAlto,
            D_elementos = Storage()
            )
        
        _n_offsetIzquierdo = 0
        _n_maxAlto = 50.0
        
        if s_rutaImagen and os.path.exists(s_rutaImagen):
            
            _D_result = self._leer_imagen(s_rutaImagen)

            _n_tamanioAncho = self._ajustar_ancho(_D_result.n_ancho, _D_result.n_alto, _n_maxAlto)

            from reportlab.lib.utils import ImageReader

            _O_buffer = _io.BytesIO()
            _D_result.O_imagen.save(_O_buffer, "PNG")
            _O_image = ImageReader(_O_buffer)

            _D_return.D_elementos.imagen = PR.Image(
                pos = STV_FWK_REPORT_PDF.POSICION(_n_offsetIzquierdo, _D_return.n_offsetAlto),
                width = _n_tamanioAncho,
                height = _n_maxAlto,
                text = _O_image
                )
            _D_return.L_elementos += [_D_return.D_elementos.imagen]

            _n_offsetIzquierdo += _n_tamanioAncho + 2
            pass
            
        else:
            pass        
        
        if D_fecha:
            
            if isinstance(D_fecha, str):
                _D_texto = Storage(
                    s_texto = D_fecha
                    )
            else:
                _D_texto = D_fecha
            _D_texto.D_posicion = STV_FWK_REPORT_PDF.POSICION(_n_offsetIzquierdo, _D_return.n_offsetAlto)
            _D_texto.E_alineacion = _D_texto.E_alineacion or STV_FWK_REPORT_PDF.E_ALINEACION.IZQUIERDA
            _D_texto.E_tipoletra = _D_texto.E_tipoletra or STV_FWK_REPORT_PDF.LETRA.E_TIPO.TIMES_ROMAN
            _D_texto.n_letratamanio = _D_texto.n_letratamanio or 8
            _D_texto.n_ancho = _D_texto.n_ancho or (self.n_anchoMaximo - _n_offsetIzquierdo)            
                    
            _D_return.D_elementos.fecha = self._configurar_objeto_texto(**_D_texto)
            _D_return.L_elementos += _D_return.D_elementos.fecha.L_elementos

        else:
            pass
        
        if D_textoControl:

            if isinstance(D_textoControl, str):
                _D_texto = Storage(
                    s_texto = D_textoControl
                    )
            else:
                _D_texto = D_textoControl
            _D_texto.D_posicion = STV_FWK_REPORT_PDF.POSICION(_n_offsetIzquierdo, _D_return.n_offsetAlto)
            _D_texto.E_alineacion = _D_texto.E_alineacion or STV_FWK_REPORT_PDF.E_ALINEACION.DERECHA
            _D_texto.E_tipoletra = _D_texto.E_tipoletra or STV_FWK_REPORT_PDF.LETRA.E_TIPO.TIMES_ROMAN
            _D_texto.n_letratamanio = _D_texto.n_letratamanio or 8
            _D_texto.n_ancho = _D_texto.n_ancho or (self.n_anchoMaximo - _n_offsetIzquierdo)
                        
            _D_return.D_elementos.textoControl = self._configurar_objeto_texto(**_D_texto)
            _D_return.L_elementos += _D_return.D_elementos.textoControl.L_elementos
            
        else:
            pass   
        
        if D_fecha or D_textoControl:
            _D_return.n_offsetAlto += 9.0
        else:
            pass

        if D_linea1:
            
            if isinstance(D_linea1, str):
                _D_texto = Storage(
                    s_texto = D_linea1
                    )
            else:
                _D_texto = D_linea1
            
            if _n_offsetIzquierdo:
                # Si existe una imagen, utilizar el maxAlto
                _n_altoLinea1 = (_n_maxAlto / 2.0) - 11.0
            else:
                _n_altoLinea1 = _D_return.n_offsetAlto

            _D_texto.D_posicion = STV_FWK_REPORT_PDF.POSICION(_n_offsetIzquierdo, _n_altoLinea1)
            _D_texto.E_alineacion = _D_texto.E_alineacion or STV_FWK_REPORT_PDF.E_ALINEACION.CENTRO
            _D_texto.E_tipoletra = _D_texto.E_tipoletra or STV_FWK_REPORT_PDF.LETRA.E_TIPO.TIMES_ROMAN
            _D_texto.n_letratamanio = _D_texto.n_letratamanio or 10
            _D_texto.n_ancho = _D_texto.n_ancho or (self.n_anchoMaximo - _n_offsetIzquierdo)
                        
            _D_return.D_elementos.linea1 = self._configurar_objeto_texto(**_D_texto)
            _D_return.L_elementos += _D_return.D_elementos.linea1.L_elementos
            
            _D_return.n_offsetAlto = _n_altoLinea1 + 12
                        
        else:
            pass
           
        if D_linea2:
            
            if isinstance(D_linea2, str):
                _D_texto = Storage(
                    s_texto = D_linea2
                    )
            else:
                _D_texto = D_linea2
            
            if _n_offsetIzquierdo:
                # Si existe una imagen, utilizar el maxAlto
                _n_altoLinea2 = (_n_maxAlto / 2.0) + 1.0
            else:
                _n_altoLinea2 = _D_return.n_offsetAlto

            _D_texto.D_posicion = STV_FWK_REPORT_PDF.POSICION(_n_offsetIzquierdo, _n_altoLinea2)
            _D_texto.E_alineacion = _D_texto.E_alineacion or STV_FWK_REPORT_PDF.E_ALINEACION.CENTRO
            _D_texto.E_tipoletra = _D_texto.E_tipoletra or STV_FWK_REPORT_PDF.LETRA.E_TIPO.TIMES_ROMAN
            _D_texto.n_letratamanio = _D_texto.n_letratamanio or 10
            _D_texto.n_ancho = _D_texto.n_ancho or (self.n_anchoMaximo - _n_offsetIzquierdo)
                        
            _D_return.D_elementos.linea2 = self._configurar_objeto_texto(**_D_texto)
            _D_return.L_elementos += _D_return.D_elementos.linea2.L_elementos

            _D_return.n_offsetAlto = _n_altoLinea2 + 12

        else:
            pass        
        
        if b_linea:
            
            if _n_offsetIzquierdo:
                _D_return.n_offsetAlto = _n_maxAlto
            else:
                pass
            
            _D_return.D_elementos.linea = self.helper_crearLinea(
                O_posicion = STV_FWK_REPORT_PDF.POSICION(0, _D_return.n_offsetAlto + 2.0),
                O_posicionFinal = STV_FWK_REPORT_PDF.POSICION(self._n_tamanioHzEditable, _D_return.n_offsetAlto + 2.0),
                n_grosor = 1,
                E_color = STV_FWK_REPORT_PDF.LETRA.COLOR.STV_VERDE
                )
            _D_return.L_elementos += _D_return.D_elementos.linea.L_elementos
            
        else:
            pass   
        
        return _D_return

    def helper_crearPie(
            self,
            D_linea1 = None,
            D_linea2 = None,
            s_rutaImagen = None,
            D_fecha = None,
            D_textoControl = None,
            b_linea = False,
            n_offsetAlto = 0.0
            ):
        
        _D_return = Storage(
            L_elementos = [],
            n_offsetAlto = n_offsetAlto,
            D_elementos = Storage()            
            )
                
        _n_offsetIzquierdo = 0.0
        _n_maxAlto = 50.0
        
        if b_linea:
            _D_return.D_elementos.linea = self.helper_crearLinea(
                O_posicion = STV_FWK_REPORT_PDF.POSICION(0, _D_return.n_offsetAlto),
                O_posicionFinal = STV_FWK_REPORT_PDF.POSICION(self._n_tamanioHzEditable, _D_return.n_offsetAlto),
                n_grosor = 1,
                E_color = STV_FWK_REPORT_PDF.LETRA.COLOR.STV_VERDE
                )
            _D_return.L_elementos += _D_return.D_elementos.linea.L_elementos

            _D_return.n_offsetAlto += 2

        else:
            pass  
                
        if s_rutaImagen and os.path.exists(s_rutaImagen):
            
            _D_result = self._leer_imagen(s_rutaImagen)

            _n_tamanioAncho = self._ajustar_ancho(_D_result.n_ancho, _D_result.n_alto, _n_maxAlto)
            
            _D_return.D_elementos.imagen = PR.Image(
                pos = STV_FWK_REPORT_PDF.POSICION(_n_offsetIzquierdo, _D_return.n_offsetAlto),
                width = _n_tamanioAncho,
                height = _n_maxAlto, 
                text = _D_result.O_imagen
                )
            _D_return.L_elementos += [_D_return.D_elementos.imagen]
            
            _n_offsetIzquierdo += _n_tamanioAncho + 2
                        
        else:
            pass        
        
        if D_linea1:
            
            if isinstance(D_linea1, str):
                _D_texto = Storage(
                    s_texto = D_linea1
                    )
            else:
                _D_texto = D_linea1
            
            if _n_offsetIzquierdo:
                # Si existe una imagen, utilizar el maxAlto
                _n_altoLinea1 = (_n_maxAlto / 2.0) - 11.0
            else:
                _n_altoLinea1 = _D_return.n_offsetAlto

            _D_texto.D_posicion = STV_FWK_REPORT_PDF.POSICION(_n_offsetIzquierdo, _n_altoLinea1)
            _D_texto.E_alineacion = _D_texto.E_alineacion or STV_FWK_REPORT_PDF.E_ALINEACION.CENTRO
            _D_texto.E_tipoletra = _D_texto.E_tipoletra or STV_FWK_REPORT_PDF.LETRA.E_TIPO.TIMES_ROMAN
            _D_texto.n_letratamanio = _D_texto.n_letratamanio or 10
            _D_texto.n_ancho = _D_texto.n_ancho or (self.n_anchoMaximo - _n_offsetIzquierdo)
                        
            _D_return.D_elementos.linea1 = self._configurar_objeto_texto(**_D_texto)
            _D_return.L_elementos += _D_return.D_elementos.linea1.L_elementos
            
            _D_return.n_offsetAlto = _n_altoLinea1 + 12
                        
        else:
            pass
           
        if D_linea2:
            
            if isinstance(D_linea2, str):
                _D_texto = Storage(
                    s_texto = D_linea2
                    )
            else:
                _D_texto = D_linea2
            
            if _n_offsetIzquierdo:
                # Si existe una imagen, utilizar el maxAlto
                _n_altoLinea2 = (_n_maxAlto / 2.0) + 1.0
            else:
                _n_altoLinea2 = _D_return.n_offsetAlto

            _D_texto.D_posicion = STV_FWK_REPORT_PDF.POSICION(_n_offsetIzquierdo, _n_altoLinea2)
            _D_texto.E_alineacion = _D_texto.E_alineacion or STV_FWK_REPORT_PDF.E_ALINEACION.CENTRO
            _D_texto.E_tipoletra = _D_texto.E_tipoletra or STV_FWK_REPORT_PDF.LETRA.E_TIPO.TIMES_ROMAN
            _D_texto.n_letratamanio = _D_texto.n_letratamanio or 10
            _D_texto.n_ancho = _D_texto.n_ancho or (self.n_anchoMaximo - _n_offsetIzquierdo)
                        
            _D_return.D_elementos.linea2 = self._configurar_objeto_texto(**_D_texto)
            _D_return.L_elementos += _D_return.D_elementos.linea2.L_elementos

            _D_return.n_offsetAlto = _n_altoLinea2 + 12

        else:
            pass  
        
        if D_fecha:
            
            if isinstance(D_fecha, str):
                _D_texto = Storage(
                    s_texto = D_fecha
                    )
            else:
                _D_texto = D_fecha
            
            _D_texto.D_posicion = STV_FWK_REPORT_PDF.POSICION(_n_offsetIzquierdo, _D_return.n_offsetAlto)
            _D_texto.E_alineacion = _D_texto.E_alineacion or STV_FWK_REPORT_PDF.E_ALINEACION.IZQUIERDA
            _D_texto.E_tipoletra = _D_texto.E_tipoletra or STV_FWK_REPORT_PDF.LETRA.E_TIPO.TIMES_ROMAN
            _D_texto.n_letratamanio = _D_texto.n_letratamanio or 7
            _D_texto.n_ancho = _D_texto.n_ancho or (self.n_anchoMaximo - _n_offsetIzquierdo)
                        
            _D_return.D_elementos.fecha = self._configurar_objeto_texto(**_D_texto)
            _D_return.L_elementos += _D_return.D_elementos.fecha.L_elementos
            
        else:
            pass
        
        if D_textoControl:

            if isinstance(D_textoControl, str):
                _D_texto = Storage(
                    s_texto = D_textoControl
                    )
            else:
                _D_texto = D_textoControl
                            
            _D_texto.D_posicion = STV_FWK_REPORT_PDF.POSICION(_n_offsetIzquierdo, _D_return.n_offsetAlto)
            _D_texto.E_alineacion = _D_texto.E_alineacion or STV_FWK_REPORT_PDF.E_ALINEACION.DERECHA
            _D_texto.E_tipoletra = _D_texto.E_tipoletra or STV_FWK_REPORT_PDF.LETRA.E_TIPO.TIMES_ROMAN
            _D_texto.n_letratamanio = _D_texto.n_letratamanio or 8
            _D_texto.n_ancho = _D_texto.n_ancho or (self.n_anchoMaximo - _n_offsetIzquierdo)            
                        
            _D_return.D_elementos.textoControl = self._configurar_objeto_texto(**_D_texto)
            _D_return.L_elementos += _D_return.D_elementos.textoControl.L_elementos
            
        else:
            pass
        
        if D_fecha or D_textoControl:
            _D_return.n_offsetAlto += 9.0
        else:
            pass        
        
        return _D_return

    def helper_offsetParaColumnas(
            self,
            LD_columnas,
            n_offsetDerecha = 0.0,
            ):
        """ Calcula los offset a usar por columna
        
        @param n_offsetDerecha:
        @type n_offsetDerecha:
        @param LD_columnas: ancho en porcentaje de tamaño de la hoja
        """
        
        _f_offset = n_offsetDerecha
        _f_anchoPermitido = float(self.n_anchoMaximo) - n_offsetDerecha
        _LD_columnas_datos = []
        for _D_columna in LD_columnas:
            _n_ancho = _f_anchoPermitido * float(_D_columna.n_ancho_pje)
            _LD_columnas_datos.append(
                Storage(
                    n_offsetDerecha = _f_offset,
                    n_ancho = _n_ancho
                    )
                )
            _f_offset += _n_ancho
        
        return _LD_columnas_datos

    @staticmethod
    def FORMATEA_DINERO(valor, row = None, s_formato = "%.2f"):
        _ = row
        return s_formato % float(valor or 0)

    @staticmethod
    def FORMATEA_FECHA(valor, row = None, s_formato = STV_FWK_APP.FORMAT.s_DATE):
        _ = row
        return datetime.datetime.strftime(valor, s_formato)

    @staticmethod
    def HELPER_CELDA(
            E_tipoletra = LETRA.E_TIPO.TIMES_ROMAN,
            E_alineacion = E_ALINEACION.IZQUIERDA,
            D_color = None,
            n_letratamanio = 10,
            b_marco = False,
            s_texto = None,
            s_campo = None,
            x_imagen = None,
            x_formato = None,
            **D_otros
            ):
        _D_texo = Storage(
            s_texto = s_texto,
            s_campo = s_campo,
            E_tipoletra = E_tipoletra,
            E_alineacion = E_alineacion,
            D_color = D_color,
            n_letratamanio = n_letratamanio,
            b_marco = b_marco,
            x_formato = x_formato,
            x_imagen = x_imagen,
            **D_otros
            )

        return _D_texo

    @staticmethod
    def HELPER_COLUMNA(
            n_ancho_pje = 0.1,
            E_tipoletra = LETRA.E_TIPO.TIMES_ROMAN,
            E_alineacion = E_ALINEACION.IZQUIERDA,
            D_color = None,
            n_letratamanio = 10,
            b_marco = False,
            **D_otros
            ):
        _D_texo = Storage(
            n_ancho_pje = n_ancho_pje,
            E_tipoletra = E_tipoletra,
            E_alineacion = E_alineacion,
            D_color = D_color,
            n_letratamanio = n_letratamanio,
            b_marco = b_marco,
            **D_otros
            )

        return _D_texo

    @staticmethod
    def HELPER_MARCO(
            b_arriba = True,
            b_abajo = True,
            b_izquierda = True,
            b_derecha = True,
            n_grosor = 1,
            D_color = None,
            **D_otros
            ):
        _D_marco = Storage(
            n_grosor = 1,
            **D_otros
            )
        if b_arriba:
            _D_marco.D_superior = Storage(n_grosor = n_grosor, E_color = D_color)
        else:
            pass

        if b_abajo:
            _D_marco.D_inferior = Storage(n_grosor = n_grosor, E_color = D_color)
        else:
            pass

        if b_izquierda:
            _D_marco.D_izquierdo = Storage(n_grosor = n_grosor, E_color = D_color)
        else:
            pass

        if b_derecha:
            _D_marco.D_derecho = Storage(n_grosor = n_grosor, E_color = D_color)
        else:
            pass

        return _D_marco

    @staticmethod
    def HELPER_FONDO(
            n_grosor = 1,
            D_color = None,
            D_colorMarco = None,
            **D_otros
            ):
        _D_fondo = Storage(
            n_grosor = n_grosor,
            D_color = D_color,
            D_colorMarco = D_colorMarco,
            **D_otros
            )

        return _D_fondo

    def helper_crearTabla(
            self,
            LL_datos,
            LD_infoColumnas,
            D_marco = None,
            D_fondo = None,
            dbRow = None,
            n_offsetAlto = 0.0,
            n_offsetDerecha = 0.0,
            n_separacion = 3.0,
            n_ancho = 0.0
            ):
        """ Crea una tabla con información, la información es estática no navega por los registros
        
        Usada comunmente para headers de tablas o mostrar configuraciones de consultas
        
        @param D_fondo:
        @type D_fondo:
        @param dbRow:
        @type dbRow:
        @param n_ancho:
        @type n_ancho:
        @param n_separacion:
        @type n_separacion:
        @param n_offsetDerecha:
        @type n_offsetDerecha:
        @param n_offsetAlto:
        @type n_offsetAlto:
        @param D_marco:
        @type D_marco:
        @param LL_datos contiene una lista de renglones y columnas con la información a imprimir
        @param LD_infoColumnas: Contienen una lista de diccionarios que configuran las columnas
            n_ancho_pje: Porcentaje de ancho para la columna
            E_alineacion:
        """
        _ = dbRow
        _D_return = Storage(
            L_elementos = [],
            n_offsetAlto = int(n_offsetAlto)
            )

        # Contendrá el ancho de donde se calcularán los porcentajes, offsets y demás
        if n_ancho:
            _n_ancho = float(n_ancho)
        else:
            _n_ancho = self.n_anchoMaximo - float(n_offsetDerecha)
        
        _n_offsetIzquierdo = n_offsetDerecha

        n_separacion = float(n_separacion)

        # Se maneja por las columnas para ver cuales tienen ancho definido y cuales no
        _n_anchoTotal = 0.0
        _n_columnasAnchoCero = 0.0
        _n_numeroColumnas = float(len(LD_infoColumnas))
        _n_cuentasPorSeparacion = _n_numeroColumnas * n_separacion
        _n_anchoSinSeparaciones = _n_ancho - _n_cuentasPorSeparacion
        # Es la suma de los anchos de las columnas y sus separaciones
        _n_anchoTabla = _n_cuentasPorSeparacion
        for _D_columna in LD_infoColumnas:
            _n_anchoTotal += _D_columna.n_ancho_pje or 0.0
            _D_columna.n_ancho = _n_anchoSinSeparaciones * _D_columna.n_ancho_pje
            if _D_columna.n_ancho == 0.0:
                _n_columnasAnchoCero += 1.0
            else:
                pass
            _n_anchoTabla += _D_columna.n_ancho

        # Si quedo ancho disponible, se distribuye entre las otras columnas
        if (_n_anchoTotal < 1.0) and _n_columnasAnchoCero:
            _n_anchoColumnasRestantes = ((1.0 - _n_anchoTotal) * _n_anchoSinSeparaciones) / _n_columnasAnchoCero
            for _D_columna in LD_infoColumnas:
                if _D_columna.n_ancho == 0.0:
                    _D_columna.n_ancho = _n_anchoColumnasRestantes
                else:
                    pass
        else:
            pass      

        for _L_datos in LL_datos:
            
            # La primer posicion por row empieza en la separación entre 2, y al final debe terminar en la otra
            #  posicion entre dos
            _n_posicion_x = _n_offsetIzquierdo + (float(n_separacion)/2.0)

            _n_tamanioRow = 0.0       
            for _n_index, _D_columna in enumerate(LD_infoColumnas):

                if (len(_L_datos) > _n_index) and _D_columna.n_ancho:
                    
                    _L_datos[_n_index].D_posicion = STV_FWK_REPORT_PDF.POSICION(_n_posicion_x, _D_return.n_offsetAlto)
                    _L_datos[_n_index].n_ancho = _L_datos[_n_index].n_ancho or _D_columna.n_ancho
                    _L_datos[_n_index].E_alineacion = _L_datos[_n_index].E_alineacion or _D_columna.E_alineacion \
                        or STV_FWK_REPORT_PDF.E_ALINEACION.IZQUIERDA
                    _L_datos[_n_index].n_letratamanio = _L_datos[_n_index].n_letratamanio or _D_columna.n_letratamanio \
                        or 8
                    _L_datos[_n_index].E_tipoletra = _L_datos[_n_index].E_tipoletra or _D_columna.E_tipoletra \
                        or STV_FWK_REPORT_PDF.LETRA.E_TIPO.TIMES_ROMAN
                    _L_datos[_n_index].D_marco = _L_datos[_n_index].D_marco or _D_columna.D_marco or None
                    _L_datos[_n_index].x_formato = _L_datos[_n_index].x_formato or _D_columna.x_formato or ""

                    if _L_datos[_n_index].x_formato is None:
                        _L_datos[_n_index].x_formato = ""
                    elif isinstance(_L_datos[_n_index].x_formato, str):
                        pass
                    elif _L_datos[_n_index].s_texto:
                        # Si tiene un valor fijo para desplegar, se usa el x_formato
                        _L_datos[_n_index].s_texto = _L_datos[_n_index].x_formato(_L_datos[_n_index].s_texto, None)
                        _L_datos[_n_index].x_formato = ""
                    else:
                        # Si se esta desplegando el valor de un campo, el x_formato se usará en polyreport
                        pass

                    _L_datos[_n_index].s_texto = _L_datos[_n_index].s_texto \
                        if isinstance(_L_datos[_n_index].s_texto, str) \
                        else "" if _L_datos[_n_index].s_texto is None \
                        else str(_L_datos[_n_index].s_texto)

                    if _L_datos[_n_index].s_texto \
                            or _L_datos[_n_index].s_campo \
                            or _L_datos[_n_index].x_getvalue \
                            or _L_datos[_n_index].s_sysvar:
                        _D_result = self._configurar_objeto_texto(**_L_datos[_n_index])

                    elif _L_datos[_n_index].x_imagen:
                        _D_result = self.configurar_objeto_imagen(**_L_datos[_n_index])

                    else:
                        _D_result = self._configurar_objeto_texto(**_L_datos[_n_index])

                    _D_return.L_elementos += _D_result.L_elementos
                    
                    if _D_result.n_tamanioAlto > _n_tamanioRow:
                        _n_tamanioRow = _D_result.n_tamanioAlto
                    else:
                        pass
                    
                    _n_posicion_x += _L_datos[_n_index].n_ancho + float(n_separacion)
                    
                else:
                    # No tiene más datos o el ancho es 0
                    pass
                
            # Crear logica para poner marcos en la tabla y columnas y/o renglones si se configura
                
            _D_return.n_offsetAlto += _n_tamanioRow

        _D_results = self.helper_crearMarco(
            STV_FWK_REPORT_PDF.POSICION(n_offsetDerecha, n_offsetAlto),
            _n_anchoTabla,
            _D_return.n_offsetAlto - n_offsetAlto,
            D_marco = D_marco
            )
        _D_return.L_elementos += _D_results.L_elementos

        if D_fondo:
            _D_results = self.helper_crearCuadro(
                STV_FWK_REPORT_PDF.POSICION(n_offsetDerecha, n_offsetAlto),
                _n_anchoTabla,
                _D_return.n_offsetAlto - n_offsetAlto,
                **D_fondo
                )
            _D_return.L_elementos = _D_results.L_elementos + _D_return.L_elementos
        else:
            pass

        return _D_return
    
    def helper_crearBand(
            self,
            L_elementos,
            s_campo = None,
            x_getvalue = None,
            b_paginaNuevaAntes = False,
            b_paginaNuevaDespues = False,            
            ):
        _ = self
        return PR.Band(
            elements = L_elementos, 
            childbands = [],
            additionalbands = [], 
            key = s_campo, 
            getvalue = x_getvalue,
            newpagebefore = b_paginaNuevaAntes, 
            newpageafter = b_paginaNuevaDespues
            )

    def helper_crearLinea(
            self,
            O_posicion = None,
            O_posicionFinal = None,
            n_grosor = 1,
            E_color = None
            ):
        _ = self
        O_posicion = O_posicion or STV_FWK_REPORT_PDF.POSICION(0, 0)
        O_posicionFinal = O_posicionFinal or STV_FWK_REPORT_PDF.POSICION(0, 0)
        E_color = E_color or STV_FWK_REPORT_PDF.LETRA.COLOR.STV_VERDE
        _D_return = Storage(
            L_elementos = [],
            n_offsetAlto = O_posicion.y
            )
        
        _D_return.L_elementos.append(
            PR.Line(
                pos = O_posicion,
                posEnd = O_posicionFinal, 
                thickness = n_grosor,
                color = E_color
                )
            )
        
        return _D_return

    def helper_crearCuadro(
            self,
            O_posicion = None,
            n_ancho = None,
            n_alto = 1,
            n_grosor = 1,
            D_color = None,
            D_colorMarco = None,
            ):
        _ = self
        O_posicion = O_posicion or STV_FWK_REPORT_PDF.POSICION(0, 0)
        n_ancho = n_ancho or self.n_anchoMaximo
        D_color = D_color or STV_FWK_REPORT_PDF.LETRA.COLOR.STV_VERDE_T
        D_colorMarco = D_colorMarco or STV_FWK_REPORT_PDF.LETRA.COLOR.STV_GRIS_T
        _D_return = Storage(
            L_elementos = [],
            n_offsetAlto = O_posicion.y + n_alto
            )

        _D_return.L_elementos.append(
            PR.Rect(
                pos = O_posicion,
                width = n_ancho,
                height = n_alto,
                lineWidth = n_grosor,
                color = D_color,
                colorBorder = D_colorMarco
                )
            )

        return _D_return

    def helper_crearMarco(
            self,
            D_posicion,
            n_ancho,
            n_alto,
            D_marco = None
            ):
        _D_return = Storage(
            L_elementos = [],
            n_offsetAlto = D_posicion.y,
            n_tamanioAlto = n_alto,
            D_posicion = copy(D_posicion),
            n_tamanioAncho = n_ancho
            )

        if D_marco:

            _D_puntos = self._generarPuntosEsquinas(D_posicion, n_ancho, n_alto)

            if D_marco.D_izquierdo:
                _D_return.L_elementos += self.helper_crearLinea(
                    O_posicion = _D_puntos.IzqArriba,
                    O_posicionFinal = _D_puntos.IzqAbajo,
                    n_grosor = D_marco.D_izquierdo.n_grosor or 1,
                    E_color = D_marco.D_izquierdo.E_color or STV_FWK_REPORT_PDF.LETRA.COLOR.STV_VERDE
                    ).L_elementos
            else:
                pass

            if D_marco.D_derecho:
                _D_return.L_elementos += self.helper_crearLinea(
                    O_posicion = _D_puntos.DerArriba,
                    O_posicionFinal = _D_puntos.DerAbajo,
                    n_grosor = D_marco.D_derecho.n_grosor or 1,
                    E_color = D_marco.D_derecho.E_color or STV_FWK_REPORT_PDF.LETRA.COLOR.STV_VERDE
                    ).L_elementos
            else:
                pass

            if D_marco.D_superior:
                _D_return.L_elementos += self.helper_crearLinea(
                    O_posicion = _D_puntos.IzqArriba,
                    O_posicionFinal = _D_puntos.DerArriba,
                    n_grosor = D_marco.D_superior.n_grosor or 1,
                    E_color = D_marco.D_superior.E_color or STV_FWK_REPORT_PDF.LETRA.COLOR.STV_VERDE
                    ).L_elementos
            else:
                pass

            if D_marco.D_inferior:
                _D_return.L_elementos += self.helper_crearLinea(
                    O_posicion = _D_puntos.IzqAbajo,
                    O_posicionFinal = _D_puntos.DerAbajo,
                    n_grosor = D_marco.D_inferior.n_grosor or 1,
                    E_color = D_marco.D_inferior.E_color or STV_FWK_REPORT_PDF.LETRA.COLOR.STV_VERDE
                    ).L_elementos
            else:
                pass
        else:
            pass

        return _D_return

    def helper_crearRegla(
            self,
            n_offsetAlto = 0.0,
            n_offsetDerecha = 0.0,
            n_ancho = 0.0,
            n_grosor = 1,
            E_color = None
            ):
        """

        @param n_offsetAlto:
        @type n_offsetAlto:
        @param n_offsetDerecha:
        @type n_offsetDerecha:
        @param n_ancho:
        @type n_ancho:
        @param n_grosor:
        @type n_grosor:
        @param E_color:
        @type E_color:
        @return:
        @rtype:
        """
        E_color = E_color or STV_FWK_REPORT_PDF.LETRA.COLOR.STV_VERDE
        _D_return = Storage(
            L_elementos = [],
            n_offsetAlto = n_offsetAlto
            )
        
        _O_posicion = STV_FWK_REPORT_PDF.POSICION(n_offsetDerecha, n_offsetAlto)
        n_ancho = n_ancho or (self.n_anchoMaximo - float(_O_posicion.x))
        
        _D_return.L_elementos.append(
            PR.Rule(
                pos = _O_posicion,
                width = n_ancho, 
                thickness = n_grosor,
                color = E_color
                )
            )
        
        return _D_return
    
    def definir_reporteTitulo(
            self,
            s_titulo,
            s_subtitulo = None,
            s_rutaImagen = None,
            b_linea = False,
            L_elementos = None,
            L_bandsHijas = None,
            L_bandsAdicionales = None,
            b_paginaNuevaAntes = False,
            b_paginaNuevaDespues = False,            
            ):
        
        _n_offsetAlto = 0
        _n_offsetParaImagen = 0
        _n_offsetIzquierdo = 0
        
        _L_elementos = []
        
        if s_rutaImagen and os.path.exists(s_rutaImagen):
            
            _D_result = self._leer_imagen(s_rutaImagen)

            _n_tamanioAlto = self._ajustar_alto(_D_result.n_ancho, _D_result.n_alto, 100)
            
            _L_elementos.append(
                PR.Image(
                    pos = STV_FWK_REPORT_PDF.POSICION(0, _n_offsetParaImagen),
                    width = 100,
                    height = _n_tamanioAlto, 
                    text = _D_result.O_imagen
                    )
                )
            
            _n_offsetParaImagen += _n_tamanioAlto + 2
            _n_offsetIzquierdo += 100 + 2
                        
        else:
            pass        
        
        if s_titulo:
            _L_elementos.append(
                PR.Element(
                    pos = STV_FWK_REPORT_PDF.POSICION(self.n_anchoCentro + (_n_offsetIzquierdo/2.0), _n_offsetAlto),
                    font = STV_FWK_REPORT_PDF.LETRA(STV_FWK_REPORT_PDF.LETRA.E_TIPO.TIMES_BOLD, 26, STV_FWK_REPORT_PDF.LETRA.COLOR.NEGRO),
                    text = s_titulo, key = None, getvalue = None, sysvar = None,
                    align = "center",
                    format = str,  # Función que puede ser usada para formatear un valor
                    width = None,
                    leading = None,
                    onrender = None  # Evento al momento de hacer un render
                    )
                )
            
            _n_offsetAlto += 30
            
        else:
            pass
           
        if s_subtitulo:
            _L_elementos.append(
                PR.Element(
                    pos = STV_FWK_REPORT_PDF.POSICION(self.n_anchoCentro + (_n_offsetIzquierdo/2.0), _n_offsetAlto),
                    font = STV_FWK_REPORT_PDF.LETRA(STV_FWK_REPORT_PDF.LETRA.E_TIPO.TIMES_BOLD, 18, STV_FWK_REPORT_PDF.LETRA.COLOR.STV_GRIS),
                    text = s_subtitulo, key = None, getvalue = None, sysvar = None,
                    align = "center",
                    format = str,  # Función que puede ser usada para formatear un valor
                    width = None,
                    leading = None,
                    onrender = None  # Evento al momento de hacer un render
                    )
                )  
            _n_offsetAlto += 22
        else:
            pass        
    
        if _n_offsetAlto >= _n_offsetParaImagen:
            pass
        else:
            _n_offsetAlto = _n_offsetParaImagen
    
        if b_linea:
            _L_elementos.append(
                PR.Line(
                    pos = STV_FWK_REPORT_PDF.POSICION(0, _n_offsetAlto + 1),
                    posEnd = STV_FWK_REPORT_PDF.POSICION(self._n_tamanioHzEditable, _n_offsetAlto + 1),
                    thickness = 1,
                    color = STV_FWK_REPORT_PDF.LETRA.COLOR.STV_VERDE
                    )
                )
            _n_offsetAlto += 3
        else:
            pass
        
        self._B_reporteTitulo = PR.Band(
            elements = _L_elementos + (L_elementos or []),
            childbands = L_bandsHijas or [],
            additionalbands = L_bandsAdicionales or [],
            key = None, 
            getvalue = None,
            newpagebefore = b_paginaNuevaAntes, 
            newpageafter = b_paginaNuevaDespues
            )
            
        return
    
    def definir_reporteEncabezado(
            self,
            L_elementos = None,
            # L_bandsHijas = None,
            # L_bandsAdicionales = None,
            # b_paginaNuevaAntes = False,
            # b_paginaNuevaDespues = False,
            ):
        
        self._B_reporteEncabezado = PR.Band(
            elements = L_elementos or [],
            # childbands = L_bandsHijas or [],
            # additionalbands = L_bandsAdicionales or [],
            # key = None,
            # getvalue = None,
            # newpagebefore = b_paginaNuevaAntes,
            # newpageafter = b_paginaNuevaDespues
            )         
            
        return
    
    def definir_paginaEncabezado(
            self,
            L_elementos = None,
            # L_bandsHijas = None,
            # L_bandsAdicionales = None,
            # b_paginaNuevaAntes = False,
            # b_paginaNuevaDespues = False,
            ):
        
        self._B_paginaEncabezado = PR.Band(
            elements = L_elementos or [],
            # childbands = L_bandsHijas or [],
            # additionalbands = L_bandsAdicionales or [],
            # key = None,
            # getvalue = None,
            # newpagebefore = b_paginaNuevaAntes,
            # newpageafter = b_paginaNuevaDespues
            )         
        return

    def definir_reporteDetalle(
            self,
            L_elementos = None,
            # L_bandsHijas = None,
            # L_bandsAdicionales = None,
            # b_paginaNuevaAntes = False,
            # b_paginaNuevaDespues = False,
            ):
        
        self._B_reporteDetalle = PR.Band(
            elements = L_elementos or [],
            # childbands = L_bandsHijas or [],
            # additionalbands = L_bandsAdicionales or [],
            # key = None,
            # getvalue = None,
            # newpagebefore = b_paginaNuevaAntes,
            # newpageafter = b_paginaNuevaDespues
            )         
        return
    
    def agrupar(
            self,
            L_elementosEncabezado = None,
            L_elementosPie = None,
            s_porCampo = None,
            x_porValor = None,
            b_paginaNuevaAntesEncabezado = False,
            b_paginaNuevaDespuesEncabezado = False,
            b_paginaNuevaAntesPie = False,
            b_paginaNuevaDespuesPie = False
            ):
        
        if L_elementosEncabezado:
            self._LB_grupos_encabezado.append(
                self.helper_crearBand(
                    L_elementos = L_elementosEncabezado,
                    s_campo = s_porCampo,
                    x_getvalue = x_porValor,
                    b_paginaNuevaAntes = b_paginaNuevaAntesEncabezado,
                    b_paginaNuevaDespues = b_paginaNuevaDespuesEncabezado,            
                    )
                )
        else:
            pass
        
        if L_elementosPie:
            self._LB_grupos_pie.append(
                self.helper_crearBand(
                    L_elementos = L_elementosPie,
                    s_campo = s_porCampo,
                    x_getvalue = x_porValor,
                    b_paginaNuevaAntes = b_paginaNuevaAntesPie,
                    b_paginaNuevaDespues = b_paginaNuevaDespuesPie,            
                    )
                )
        else:
            pass
        
        return
    
    def definir_reportePie(
            self,
            L_elementos = None,
            # L_bandsHijas = None,
            # L_bandsAdicionales = None,
            # b_paginaNuevaAntes = False,
            # b_paginaNuevaDespues = False,
            ):
        
        self._B_reportePie = PR.Band(
            elements = L_elementos or [],
            # childbands = L_bandsHijas or [],
            # additionalbands = L_bandsAdicionales or [],
            # key = None,
            # getvalue = None,
            # newpagebefore = b_paginaNuevaAntes,
            # newpageafter = b_paginaNuevaDespues
            )         
            
        return
        
    def definir_paginaPie(
            self,
            L_elementos = None,
            # L_bandsHijas = None,
            # L_bandsAdicionales = None,
            # b_paginaNuevaAntes = False,
            # b_paginaNuevaDespues = False,
            ):
        
        self._B_paginaPie = PR.Band(
            elements = L_elementos or [],
            # childbands = L_bandsHijas or [],
            # additionalbands = L_bandsAdicionales or [],
            # key = None,
            # getvalue = None,
            # newpagebefore = b_paginaNuevaAntes,
            # newpageafter = b_paginaNuevaDespues
            )   
            
        return

    def limpia_reporte(self):
        self._B_reporteTitulo.previousvalue = None
        self._B_reporteDetalle.previousvalue = None
        self._B_reporteEncabezado.previousvalue = None
        self._B_reportePie.previousvalue = None
        self._B_paginaEncabezado.previousvalue = None
        self._B_paginaPie.previousvalue = None
        for _B_banda in self._LB_grupos_encabezado + self._LB_grupos_pie:
            _B_banda.previousvalue = None
        return

    def generar_pdf(self, s_nombreArchivo, s_tituloBrowser, dbRows):
        
        if dbRows:
            self._O_reporte = PR.Report(dbRows.as_list())
        else:
            self._O_reporte = PR.Report()
        
        self._O_reporte.reportheader = self._B_reporteEncabezado
        self._O_reporte.titleband = self._B_reporteTitulo
        self._O_reporte.pageheader = self._B_paginaEncabezado
        self._O_reporte.groupheaders = []
        for _B_band in self._LB_grupos_encabezado:
            self._O_reporte.groupheaders.append(_B_band)
        self._O_reporte.groupfooters = []
        for _B_band in self._LB_grupos_pie:
            self._O_reporte.groupfooters.append(_B_band)
        self._O_reporte.detailband = self._B_reporteDetalle
        self._O_reporte.reportfooter = self._B_reportePie
        self._O_reporte.pagefooter = self._B_paginaPie

        import tempfile
        _s_nombreArchivo = tempfile.mktemp(".pdf", "reportePDF_")

        _buffer = _io.BytesIO()

        canvas = Canvas(_s_nombreArchivo, pagesize = self._L_tamanioPagina)
        canvas.setCreator("Soluciones Tecnológicas Verticales - www.stverticales.mx")
        canvas.setAuthor("%s %s" % (auth.user.first_name, auth.user.last_name))
        # canvas.setSubject(s_tituloBrowser)
        canvas.setTitle(s_tituloBrowser)
        self._O_reporte.generate(canvas)
        canvas.save()

        # _F_original = open(_s_nombreArchivo, "rb+")

        response.headers['Content-Type'] = 'application/pdf'
        # response.headers['Content-Disposition'] = 'inline; filename=%s' % s_nombreArchivo
        # response.headers['Pragma'] = None
        # res = response.stream(_s_nombreArchivo, request = request, attachment = True, filename = 'prueba.pdf')
        # response.write(_buffer.read())
        # _buffer.seek(0)

        # response.stream(_s_nombreArchivo, request = request)

        return _s_nombreArchivo


class STV_REPORTE:

    @staticmethod
    def CONFIGURACION_IMPRIMIR(
            dbTabla = None,
            s_titulo = None,
            D_variables = None,
            s_accion = None,
            n_offsetArgs = 0,
            s_urlReporte = ""
            ):
        _D_return = FUNC_RETURN()

        if s_accion in (
                request.stv_fwk_permissions.btn_print_print.code,
                request.stv_fwk_permissions.btn_print_save.code,
                request.stv_fwk_permissions.btn_print_send.code,
                request.stv_fwk_permissions.btn_print_preview.code,
                ):
            _s_override_accion = request.stv_fwk_permissions.btn_save.code
        else:
            _s_override_accion = s_accion

        s_titulo = s_titulo or dbTabla._plural

        _O_cat = STV_FWK_FORM(
            dbReference = db,
            dbTable = None,
            L_visibleButtons = request.stv_fwk_permissions.L_formPrintButtons,
            s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
            b_applyPermissons = False,
            x_offsetInArgs = n_offsetArgs
            )

        _O_cat.updateTabOption("s_modalTitle", "Imprimir: " + s_titulo)

        if request.vars.get("fecha", False):
            _d_default = datetime.datetime.strptime(request.vars.fecha, STV_FWK_APP.FORMAT.s_DATE)
        else:
            _d_default = request.browsernow

        _L_fieldsForm = [
            Field(
                'descripcion', 'string', length = 100,
                default = request.vars.get("descripcion", "Reporte de " + s_titulo),
                required = True,
                notnull = True,
                widget = stv_widget_input, label = 'Descripción', comment = '',
                writable = True, readable = True,
                represent = stv_represent_string
                ),
            Field(
                'fecha', 'date', default = _d_default,
                required = True, requires = IS_DATE(format = STV_FWK_APP.FORMAT.s_DATE),
                notnull = True,
                widget = stv_widget_inputDate, label = 'Fecha Reporte',
                comment = 'Fecha a imprimir en el reporte',
                writable = True, readable = True,
                represent = stv_represent_date
                ),
            STV_FWK_REPORT_PDF.D_DBCAMPOS.OPCION_TITULO,
            STV_FWK_REPORT_PDF.D_DBCAMPOS.PAGINA_TAMANIO,
            STV_FWK_REPORT_PDF.D_DBCAMPOS.PAGINA_ORIENTACION,
            STV_FWK_REPORT_PDF.D_DBCAMPOS.ENVIO_EMAILS,
            STV_FWK_REPORT_PDF.D_DBCAMPOS.ENVIO_MENSAJE,
            ]

        if request.vars.stv_variables_reporte:
            D_variables = Storage(simplejson.loads(request.vars.stv_variables_reporte))
        elif D_variables:
            D_variables.url_reporte = s_urlReporte
        else:
            D_variables = Storage(url_reporte = s_urlReporte)

        _O_cat.addRowContentProperty(
            'formHidden',
            Storage(stv_variables_reporte = simplejson.dumps(D_variables, cls = STV_FWK_LIB.DecimalEncoder))
            )

        _D_returnVars = _O_cat.process(
            s_overrideTitle = "Configurar reporte",
            s_overrideTitleSingular = "Reporte",
            D_overrideView = request.stv_fwk_permissions.view_print,
            s_overrideCode = _s_override_accion,
            L_formFactoryFields = _L_fieldsForm,
            b_skipAction = True
            )

        _D_returnVars['htmlid_configuration'] = _O_cat.addSubTab(
            dbTableDetail = None,
            s_url = False,
            s_tabName = "Configuración",
            s_idHtml = None,
            s_type = "extended"
            )

        _D_returnVars['htmlid_page'] = _O_cat.addSubTab(
            s_tabName = "Página",
            s_type = "extended"
            )

        _D_returnVars['htmlid_send'] = _O_cat.addSubTab(
            s_tabName = "Envío",
            s_type = "extended"
            )

        if _D_returnVars.D_tabOptions.b_accepted:
            if s_accion == request.stv_fwk_permissions.btn_print_print.code:
                _D_returnVars.D_tabOptions.D_redirect = Storage(
                    s_url = D_variables.url_reporte,
                    x_config = dict(b_openNewWindowPDF = True),
                    s_type = 'form',
                    )
            elif s_accion == request.stv_fwk_permissions.btn_print_save.code:
                _D_returnVars.D_tabOptions.D_redirect = Storage(
                    s_url = D_variables.url_reporte,
                    x_config = dict(b_downloadPDF = True),
                    s_type = 'form',
                    )
            else:
                pass

        else:
            _D_returnVars.D_useView.b_serverCreatesTab = True
            _D_returnVars.D_useView.L_actionsCreateView = [request.stv_fwk_permissions.btn_new.code]

        response.view = os.path.join(D_stvFwkCfg.s_dirFwkView, 'stv_form_print.html')

        return _D_returnVars
