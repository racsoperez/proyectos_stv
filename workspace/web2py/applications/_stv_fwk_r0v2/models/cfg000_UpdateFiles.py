# -*- coding: utf-8 -*-

''' Este archivo debe ser único por aplicación y no copiado, contiene las definiciones genéricas sobre las aplicaciones
principales y su uso
'''

# Siempre redireccionar a la aplicación acceso, ya que esta aplicación es solamente para manejor genérico del framework
redirect( URL( a = 'acceso', c = 'default', f = 'user', args = ['login'], vars = {} ) )


# Código utilizado para actualizar la función stv_update en caso de requerirse y no usar su version compilada
from gluon.custom_import import track_changes; track_changes(True)
from applications._stv_fwk_r0v2.modules import stv_update

D_stvFwkCfg.s_nameFwkApp = '_stv_fwk_r0v2'
''' 
:var str D_stvFwkCfg.s_nameFwkApp: variable global que define el framework del cual obtener los archivos genéricos a utilizar
'''

D_stvFwkCfg.s_nameAccesoApp = 'acceso'
''' 
:var str D_stvFwkCfg.s_nameAccesoApp: variable global que define la aplicación que actualiza la definición de las bases de datos
'''

D_stvFwkCfg.b_requestIsMaster = (request.application == D_stvFwkCfg.s_nameAccesoApp)
''' 
:var boolean vD_stvFwkCfg.b_requestIsMasterar: define si se el request proviene de la aplicación master 
'''


# Siempre redireccionar a la aplicación acceso, ya que esta aplicación es solamente para manejor genérico del framework
redirect( URL( a = 'acceso', c = 'default', f = 'user', args = ['login'], vars = {} ) )

