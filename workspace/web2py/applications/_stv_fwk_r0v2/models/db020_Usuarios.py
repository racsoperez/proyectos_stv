# -*- coding: utf-8 -*-
"""
Definición de las tablas adicionales de soporte a los usuarios
"""


def stv_represent_timeAsText(x_value, odbRow):
    _ = odbRow
    return x_value.strftime(STV_FWK_APP.FORMAT.s_TIME)


def stv_represent_userAsText(x_value, odbRow):
    _ = odbRow
    _sResult = 'Nada'
    _dbRow = db.auth_user(x_value)
    if _dbRow:
        _sResult = '<img alt="image" class="img-circle" src="' \
                   + URL(a = D_stvFwkCfg.s_nameBackofficeApp, c = 'accesos', f = 'fotomin') \
                   + '/%(fotominiatura)s?filename=%(fotonombrearchivo)s"> %(last_name)s %(first_name)s [%(email)s]' \
                   % _dbRow.as_dict()
    
    return _sResult


' Definiendo los campos signature que deberán ser incluidos en todas las tablas '
tSignature = db.Table(
    db, 'signature',
    Field(
        'notas', 'text', default = None,
        required = False,
        notnull = False,
        widget = stv_widget_text, label = 'Notas', comment = "Notas disponibles para el administrador",
        writable = False, readable = False,
        represent = stv_represent_text
        ),
    Field(
        'bansuspender', 'boolean', default = 0,
        required = True,
        notnull = True,
        label = 'Suspender', comment = None,
        writable = False, readable = True,
        represent = stv_represent_boolean
        ),
    Field(
        'baneliminar', 'boolean', default = 0,
        required = True,
        notnull = True,
        label = T('Deleted'), comment = 'Bandera que identifica el registro como eliminado',
        writable = False, readable = False,
        represent = stv_represent_boolean
        ),
    Field(
        'creado_en', type = TDATETIME_GRABADIRECTO_LEERENZONAH, default = request.utcnow,
        required = True, requires = IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME),
        notnull = True,
        label = T('Created On'), comment = T('Datetime where this register was created'),
        writable = False, readable = False,
        represent = lambda v, r: stv_represent_datetime(v, r)
        ),
    Field(
        'creado_por', db.auth_user, default = auth.user_id,
        required = False,
        notnull = False,
        label = T('Created By'), comment = T('User that created this register'),
        writable = False, readable = False,
        represent = lambda v, r: stv_represent_userAsText(v, r)
        ),
    Field(
        'editado_en', type = TDATETIME_GRABADIRECTO_LEERENZONAH, default = request.utcnow,
        update = request.utcnow,
        required = True, requires = IS_DATETIME(format = STV_FWK_APP.FORMAT.s_DATETIME),
        notnull = True,
        label = T('Updated On'), comment = T('Datetime where this register was updated'),
        writable = False, readable = False,
        represent = lambda v, r: stv_represent_datetime(v, r)
        ),
    Field(
        'editado_por', db.auth_user, default = auth.user_id, update = auth.user_id,
        required = False,
        notnull = False,
        label = T('Updated By'), comment = T('Last user that updates this register'),
        writable = False, readable = False,
        represent = lambda v, r: stv_represent_userAsText(v, r)
        ),
    )


class TZONASHORARIAS(Table):
    """ Catalogo de zonas https://timezonedb.com/download
    Se agregan campos a los mínimos para mantener compatibilidad en actyalizaciones
    """

    S_NOMBRETABLA = 'tzonashorarias'

    L_DBCAMPOS = [
        Field(
            'base_zone_id', 'integer', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputInteger, label = 'Base Zone ID',
            comment = '',
            writable = False, readable = True,
            represent = stv_represent_number
            ),
        Field(
            'base_country_code', 'string', length = 10, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Base Country Code',
            comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'base_zone_name', 'string', length = 250, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Base Zone Name',
            comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'nombre', 'string', length = 250, default = None,
            required = True,
            notnull = True, unique = True,
            widget = stv_widget_input, label = 'Nombre',
            comment = 'Nombre de la zona horaria',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        tSignature,
        ]

    def __init__(self, dbRef, tablename, *fields, **args):
        Table.__init__(self, dbRef, tablename, *fields, **args)
        return

    @staticmethod
    def BUSCAR_ZONAHORARIA(n_segundos):
        _dbRows = db(
            (db.tzonahoraria_diferenciasutc.diferencia_segundos == n_segundos)
            ).select(
            db.tzonahoraria_diferenciasutc.id,
            db.tzonahoraria_diferenciasutc.zonahoraria_id,
            db.tzonahoraria_diferenciasutc.diferencia_segundos,
            orderby = [
                ~db.tzonahoraria_diferenciasutc.editado_en
                ],
            limitby = (0, 2)
            )

        return _dbRows.first().zonahoraria_id if _dbRows else None

    @staticmethod
    def CONVERTIR_FECHAHORA_DEZONAH_AUTC(n_zonahoraria_id, dt_zonah):
        if dt_zonah:
            if isinstance(dt_zonah, str):
                dt_zonah = datetime.datetime.strptime(dt_zonah, STV_FWK_APP.FORMAT.s_DATETIME)
            else:
                pass

            _n_segundos = TZONAHORARIA_DIFERENCIASUTC.OBTENER_DIFERENCIA_SEG_ZONAH(n_zonahoraria_id, dt_zonah)
            return dt_zonah - datetime.timedelta(seconds = _n_segundos)
        else:
            return None

    @staticmethod
    def CONVERTIR_FECHAHORA_DEUTC_AZONAH(n_zonahoraria_id, dt_utc):
        if dt_utc:
            if isinstance(dt_utc, str):
                dt_zonah = datetime.datetime.strptime(dt_utc, STV_FWK_APP.FORMAT.s_DATETIME)
            else:
                pass

            _n_segundos = TZONAHORARIA_DIFERENCIASUTC.OBTENER_DIFERENCIA_SEG(n_zonahoraria_id, dt_utc)
            return dt_utc + datetime.timedelta(seconds = _n_segundos)
        else:
            return None

    pass  # TZONASHORARIAS


db.define_table(
    TZONASHORARIAS.S_NOMBRETABLA, *TZONASHORARIAS.L_DBCAMPOS,
    format = '%(nombre)s',
    singular = 'Zona horaria',
    plural = 'Zonas horarias',
    migrate = D_stvFwkCfg.b_requestIsMaster,
    table_class = TZONASHORARIAS
    )
STV_FWK_LIB.DEFINIR_INTEGRIDADREFERENCIAL(
    dbTabla_referenciada = db.tzonashorarias, dbCampo_haceReferencia = db.auth_user.zonahoraria_id,
    b_actualizarTipo = True, fn_widget = lambda f, v: stv_widget_db_chosen(f, v, 1)
    )
TZONASHORARIAS.MAZATLAN = 261  # TODO Mejora buscar y ponerlo en cache
db.auth_user.zonahoraria_id.default = TZONASHORARIAS.MAZATLAN  # Zona Horaria por default para los nuevos usuarios


class TZONAHORARIA_DIFERENCIASUTC(Table):
    """ Catalogo de zonas horarias  https://timezonedb.com/download
    Campo comienzaen es en segundos desde '1970-01-01 00:00:00'; se puede usar la función SQL
     FROM_UNIXTIME para convertirlo.
    Campo utc_offset son los segundos a sumarle al UTC time para que de el tiempo en la zona horaria.

    Cuando se importe el catalogo nuevo, deben mantenerse los ids para mantener compatibilidad, así mismo,
     debe correrse la función define_comienzaEn una sola vez para actualizar la fecha-hora del campo
     empiezaen en formato datetime.
    """

    S_NOMBRETABLA = 'tzonahoraria_diferenciasutc'

    L_DBCAMPOS = [
        Field(
            'zonahoraria_id', db.tzonashorarias, default = None,
            required = True,
            ondelete = 'CASCADE', notnull = True, unique = False,
            widget = lambda f, v: stv_widget_db_chosen(f, v, 1), label = 'Zona Horaria', comment = '',
            writable = False, readable = True,
            represent = lambda v, r: stv_represent_referencefield(v, r)
            ),
        Field(
            'base_zone_id', 'integer', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputInteger, label = 'Base Zone ID',
            comment = '',
            writable = False, readable = True,
            represent = stv_represent_number
            ),
        Field(
            'base_abbreviation', 'string', length = 50, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Base Abbreviation',
            comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'base_time_start', 'decimal(11,0)', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputInteger, label = 'Base Time Start',
            comment = 'Este dato esta en UNIX, usar la función FROM_UNIXTIME en mysql para convertirlo',
            writable = False, readable = True,
            represent = stv_represent_number
            ),
        Field(
            'base_gmt_offset', 'integer', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputInteger, label = 'Base GMT Offset',
            comment = 'Es el offset en segundos',
            writable = False, readable = True,
            represent = stv_represent_number
            ),
        Field(
            'base_dst', 'string', default = '0',
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Base DST',
            comment = '',
            writable = False, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'nombrecorto', 'string', length = 50, default = None,
            required = True,
            notnull = True, unique = False,
            widget = stv_widget_input, label = 'Abreviatura',
            comment = 'Abreviatura',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'comienza_en', TDATETIME_GRABADIRECTO_LEERDIRECTO, default = None,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputDateTime, label = 'Comienza en (UTC)',
            comment = 'Fecha y hora UTC, a partir de la cual aplica la diferencia',
            writable = True, readable = True,
            represent = stv_represent_datetime
            ),
        Field(
            'diferencia_segundos', 'integer', default = 0,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_inputInteger, label = 'UTC Offset',
            comment = 'Offset en segundos del tiempo en UTC',
            writable = True, readable = True,
            represent = stv_represent_number
            ),
        Field(
            'horario_verano', 'string', default = '0',
            required = True,
            notnull = True, unique = False,
            widget = stv_widget_input, label = 'Horario de Verano',
            comment = '',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        tSignature,
        ]

    def __init__(self, dbRef, tablename, *fields, **args):
        Table.__init__(self, dbRef, tablename, *fields, **args)
        return

    def define_comienzaEn(self):
        """ Función que toma llena la columna comienza_en cuando es nula, y define su valor convirtiendo el valor
        en la columna base_time_start de unix a datetime.

        @return:
        @rtype:
        """

        _dbRows = db(self.comienza_en == None).select(self.id, self.comienza_en, self.base_time_start)

        for _dbRow in _dbRows:
            _dbRow.comienza_en = datetime.datetime.utcfromtimestamp(_dbRow.base_time_start)
            _dbRow.update_record()

        return

    @classmethod
    def OBTENER_DIFERENCIA_SEG_ZONAH(cls, n_zonahoraria_id, dt_fecha_zonah):
        """ Usa el parametro de fecha para encontrar las diferencia en segundos con UTC y
         determinar si es una fechatiempo muerta durante la transición del horario de verano

        @param dt_fecha_zonah:
        @type dt_fecha_zonah:
        @param n_zonahoraria_id:
        @type n_zonahoraria_id:
        @return:
        @rtype:
        """
        _n_segundosDiferencia = 0

        _n_segundosSi_zonaH_igualUTC = cls.OBTENER_DIFERENCIA_SEG(n_zonahoraria_id, dt_fecha_zonah)
        _n_segundosSi_utc_con_zonaH_igualUTC = cls.OBTENER_DIFERENCIA_SEG(
            n_zonahoraria_id, dt_fecha_zonah - datetime.timedelta(seconds = _n_segundosSi_zonaH_igualUTC)
            )

        _n_segundosDiferencia = _n_segundosSi_utc_con_zonaH_igualUTC

        return _n_segundosDiferencia

    @staticmethod
    def OBTENER_DIFERENCIA_SEG(n_zonahoraria_id, dt_fecha_utc = request.utcnow):
        """ Basado en el parametro en UTC, encuentra la diferencia necesaria para la zona horaria

        @param n_zonahoraria_id:
        @type n_zonahoraria_id:
        @param dt_fecha_utc:
        @type dt_fecha_utc:
        @return:
        @rtype:
        """
        _dbRows = db(
            (db.tzonahoraria_diferenciasutc.zonahoraria_id == n_zonahoraria_id)
            & (db.tzonahoraria_diferenciasutc.comienza_en <= dt_fecha_utc)
            ).select(
                db.tzonahoraria_diferenciasutc.id,
                db.tzonahoraria_diferenciasutc.diferencia_segundos,
                orderby = [
                    ~db.tzonahoraria_diferenciasutc.comienza_en
                    ],
                limitby = (0, 2)
                )

        _n_segundosDiferencia = _dbRows.first().diferencia_segundos if _dbRows else 0
        return _n_segundosDiferencia

    pass  # TZONAHORARIA_DIFERENCIASUTC


db.define_table(
    TZONAHORARIA_DIFERENCIASUTC.S_NOMBRETABLA, *TZONAHORARIA_DIFERENCIASUTC.L_DBCAMPOS,
    format = '%(nombrecorto)s []',
    singular = 'Zona horaria',
    plural = 'Zonas horarias',
    migrate = D_stvFwkCfg.b_requestIsMaster,
    table_class = TZONAHORARIA_DIFERENCIASUTC
    )

# Usar la función solamente cuando se actualize la tabla tzonashorarias
# db.tzonashorarias.define_comienzaEn()

request.browsernow = TZONASHORARIAS.CONVERTIR_FECHAHORA_DEUTC_AZONAH(STV_FWK_APP.n_zonahoraria_id, request.utcnow)
