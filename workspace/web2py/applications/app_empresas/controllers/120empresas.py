# -*- coding: utf-8 -*-

""" VALIDAR Y CALCULO DE AJUSTE EN EL PLAN DE PÉNSION"""


def empresas():
    """ Formulario maestro para las empresas. 
        
    @descripcion Movimiento Agregar Pago

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Cliente
    @keyword arg3: empresa_cliente_id.
    @keyword arg4: CFDI
    @keyword arg5: cfdi_id.
    @keyword [arg6]: operación a realizar en el formulario.
    @keyword [arg7]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    """

    def validar(O_form):

        _dbRow = dbc01.tempresas(O_form.record_id) if O_form.record_id else None
        _D_camposAChecar = STV_FWK_LIB.OBTENER_CAMPOSACHECAR_PARA_VALIDAR(dbc01.tempresas, _dbRow)

        if not O_form.record_id:
            # Se esta creando el registro
            _O_crypto = STV_FWK_CRYPTO()
            O_form.vars.claveciec = _O_crypto.encrypt(
                _D_camposAChecar.rfc, STV_FWK_CRYPTO.KEY1, _D_camposAChecar.claveciec
                )

        else:
            # Se esta modificando el registro
            if (_D_camposAChecar.rfc == _dbRow.rfc) \
                    and (_D_camposAChecar.claveciec == _dbRow.claveciec):
                # Si clave ciec y rfc no cambiaron, no pasa nada
                pass

            elif _D_camposAChecar.claveciec == _dbRow.claveciec:
                # Si la clave ciec se matiene quiere decir que el rfc cambio,
                # en este caso se necesita capturar la clave ciec nuevamente
                # debido a que se graba encriptado con el rfc
                O_form.errors.claveciec = "Se detectó cambio en rfc, clave CIEC require ser capturada nuevamente"

            else:
                # Si cambiaron ambas, rfc y clave ciec, o solamente clave ciec se encripta y se almacena
                _O_crypto = STV_FWK_CRYPTO()
                O_form.vars.claveciec = _O_crypto.encrypt(
                    _D_camposAChecar.rfc, STV_FWK_CRYPTO.KEY1, _D_camposAChecar.claveciec
                    )

        # Validaciones
        if _D_camposAChecar.regimenfiscal_id and _D_camposAChecar.rfc:
            _dbRow_regimenFiscal = db01.tregimenesfiscales(_D_camposAChecar.regimenfiscal_id)
            if (len(_D_camposAChecar.rfc) == 14) and (_dbRow_regimenFiscal.aplicapermoral == 0):
                O_form.errors.regimenfiscal_id = "El regimen no es para persona moral, y el RFC es de persona moral"
            else:
                pass
            if (len(_D_camposAChecar.rfc) == 15) and (_dbRow_regimenFiscal.aplicaperfisica == 0):
                O_form.errors.regimenfiscal_id = "El regimen no es para persona física, y el RFC es de persona física"
            else:
                pass
        else:
            pass

        return O_form

    _D_return = FUNC_RETURN()
    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 0,
        s_codigoDefault = request.stv_fwk_permissions.btn_new.code,
        s_empresa_id = request.args(1, None),
        )

    _dbTable = dbc01.tempresas

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):
        _dbTable.monedacontpaqi_id.writable = False

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _dbTable.monedacontpaqi_id.writable = True

    else:
        _dbTable.monedacontpaqi_id.writable = False

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.razonsocial,
            _dbTable.rfc,
            _dbTable.nombrecorto,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.id,
            _dbTable.razonsocial,
            _dbTable.rfc,
            _dbTable.nombrecorto
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION, validar)

    _O_cat.addRowContentProperty(
        "formControlUpload", [
            Storage(fieldUpload = _dbTable.imagen, fieldUploadFilename = _dbTable.imagennombrearchivo),
            Storage(fieldUpload = _dbTable.imagenpin, fieldUploadFilename = _dbTable.imagenpinnombrearchivo),
            ]
        )

    _D_returnVars = _O_cat.process()

    _L_argsDetalle = ['master_' + str(request.function), _D_args.n_id]

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_cuentasbancarias,
        s_url = URL(f = 'empresa_cuentasbancarias', args = _L_argsDetalle),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_cuentasbancarias', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_series,
        s_url = URL(c = '150contabilidad', f = 'empresa_series', args = _L_argsDetalle),
        s_idHtml = fn_createIDTabUnique(
            c = '150contabilidad', f = 'empresa_series', s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        s_tabName = "Ingresos",
        s_url = URL(f = 'empresa_ingresos', args = _L_argsDetalle),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_ingresos', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        s_tabName = "Gastos",
        s_url = URL(f = 'empresa_gastos', args = _L_argsDetalle),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_gastos', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        s_tabName = "Centro de Costos",
        s_url = URL(f = 'empresa_centrocostos', args = _L_argsDetalle),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_centrocostos', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        s_tabName = "Organización",
        s_url = URL(f = 'empresa_organizacion', args = _L_argsDetalle),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_organizacion', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_registrospatronales,
        s_url = URL(c = '150contabilidad', f = 'empresa_registrospatronales', args = _L_argsDetalle),
        s_idHtml = fn_createIDTabUnique(
            c = '150contabilidad', f = 'empresa_registrospatronales', s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_plazas,
        s_url = URL(f = 'empresa_plazas', args = _L_argsDetalle),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_plazas', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        s_tabName = "Impuestos",
        s_url = URL(f = 'empresa_impuestos', args = _L_argsDetalle),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_impuestos', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_clientes,
        s_url = URL(c = '130clientes', f = 'empresa_clientes', args = _L_argsDetalle),
        s_idHtml = fn_createIDTabUnique(
            c = '130clientes', f = 'empresa_clientes', s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_proveedores,
        s_url = URL(c = '140proveedores', f = 'empresa_proveedores', args = _L_argsDetalle),
        s_idHtml = fn_createIDTabUnique(
            c = '140proveedores', f = 'empresa_proveedores', s_masterIdentificator = str(request.function)
            )
        )

    _D_returnVars['htmlid_domiciliofiscal'] = _O_cat.addSubTab(
        dbTableDetail = None, s_url = False,
        s_tabName = "Domicilio fiscal",
        s_idHtml = None, s_type = "extended"
        )

    _D_returnVars['htmlid_representantelegal'] = _O_cat.addSubTab(
        dbTableDetail = None, s_url = False,
        s_tabName = "Representante legal",
        s_idHtml = None, s_type = "extended"
        )

    _D_returnVars['htmlid_configfacturacion'] = _O_cat.addSubTab(
        dbTableDetail = None, s_url = False,
        s_tabName = "Conf. Facturación",
        s_idHtml = None, s_type = "extended"
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_conceptosasientoscontables,
        s_url = URL(c = '150contabilidad', f = 'empresa_conceptosasientoscontables', args = _L_argsDetalle),
        s_idHtml = fn_createIDTabUnique(
            c = '150contabilidad', f = 'empresa_conceptosasientoscontables',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        s_tabName = "CONTPAQi",
        s_url = URL(c = '145contpaqi', f = 'empresa_contpaqi', args = _L_argsDetalle),
        s_idHtml = fn_createIDTabUnique(
            c = '145contpaqi', f = 'empresa_contpaqi', s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        s_tabName = "Inventarios",
        s_url = URL(c = '125inventarios', f = 'empresa_inventarios', args = _L_argsDetalle),
        s_idHtml = fn_createIDTabUnique(
            c = '125inventarios', f = 'empresa_inventarios', s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        s_tabName = "Página web",
        s_url = URL(c = '510paginaweb', f = 'empresa_paginaweb', args = _L_argsDetalle),
        s_idHtml = fn_createIDTabUnique(
            c = '510paginaweb', f = 'empresa_paginaweb', s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        s_tabName = "Tipo transportes",
        s_url = URL(f = 'empresa_tipotransportes', args = _L_argsDetalle),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_tipotransportes', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        s_tabName = "Transportes",
        s_url = URL(f = 'empresa_transportes', args = _L_argsDetalle),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_transportes', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        s_tabName = "Embarques",
        s_url = URL(f = 'empresa_embarques', args = _L_argsDetalle),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_embarques', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        s_tabName = "Grupos",
        s_url = URL(f = 'empresa_grupos', args = _L_argsDetalle),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_grupos', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        s_tabName = "Clasificaciones proveedores",
        s_url = URL(f = 'empresa_clasificacionesproveedores', args = _L_argsDetalle),
        s_idHtml = fn_createIDTabUnique(
            f = 'empresa_clasificacionesproveedores', s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        s_tabName = "Clasificaciones clientes",
        s_url = URL(f = 'empresa_clasificacionesclientes', args = _L_argsDetalle),
        s_idHtml = fn_createIDTabUnique(
            f = 'empresa_clasificacionesclientes', s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        s_tabName = "Recursos Comprobar",
        s_url = URL(f = 'empresa_recursoscomprobar', args = _L_argsDetalle),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_recursoscomprobar', s_masterIdentificator = str(request.function))
        )

    return _D_returnVars


def _empresa_cuentasbancarias_validation(O_form):
    """ Validación para las cuentas bancarias

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: cuenta_bancaria_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    """

    _s_empresa_id = request.args(1, None)

    if str(O_form.vars.tipocomision) == str(TEMPRESA_CUENTASBANCARIAS().TIPOCOMISION.SIN_COMISION):
        O_form.vars.comisionfija = 0
        O_form.vars.comisionporcentajecredito = 0
        O_form.vars.comisionporcentajedebito = 0
        O_form.vars.comisionporcentajepromocion = 0
    elif str(O_form.vars.tipocomision) == str(TEMPRESA_CUENTASBANCARIAS().TIPOCOMISION.FIJO):
        O_form.vars.comisionporcentajecredito = 0
        O_form.vars.comisionporcentajedebito = 0
        O_form.vars.comisionporcentajepromocion = 0
    elif str(O_form.vars.tipocomision) == str(TEMPRESA_CUENTASBANCARIAS().TIPOCOMISION.PORCENTAJE):
        O_form.vars.comisionfija = 0
    else:
        O_form.vars.comisionfija = 0
        O_form.vars.comisionporcentajecredito = 0
        O_form.vars.comisionporcentajedebito = 0
        O_form.vars.comisionporcentajepromocion = 0

    # Validación para evitar la repetición del código contpaq
    if 'codigo_contpaqi' in O_form.vars:
        _s_cuenta_bancaria_contpaqi = O_form.vars.codigo_contpaqi
    else:
        _s_cuenta_bancaria_contpaqi = None

    _dbRows = dbc01(
        (dbc01.tempresa_cuentasbancarias.empresa_id == _s_empresa_id)
        & (dbc01.tempresa_cuentasbancarias.codigo_contpaqi == _s_cuenta_bancaria_contpaqi)
        & (dbc01.tempresa_cuentasbancarias.id != O_form.record_id)  # Se ignora el registro que se esté modificando
        ).select(dbc01.tempresa_cuentasbancarias.id)
    if _dbRows:
        O_form.errors.cuenta_bancaria_contpaqi = "El código CONTAPQi para cuentas bancarias no puede duplicarse"
    else:
        # No hay coincidencias, se puede crear el código insertado
        pass

    return O_form


def empresa_cuentasbancarias():
    """ Detalle de cuentas bancarias correspondiente a la empresa
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: operación a realizar en el formulario
        [arg3]: id en caso de existir relación a un registro en la operación
    
    """

    _dbTable = dbc01.tempresa_cuentasbancarias
    _s_empresa_id = request.args(1, None)
    _s_action = request.args(2, request.stv_fwk_permissions.btn_none.code)
    _s_cuentabancaria_id = request.args(3, None)

    # No se permite editar la moneda de la cuenta bancaria, para evitar problemas definiendo e usando
    #  las cuentas complemento
    if (
            _s_cuentabancaria_id
            and (_s_action in (request.stv_fwk_permissions.btn_edit.code, request.stv_fwk_permissions.btn_save.code))
            ):
        _dbTable.monedacontpaqi_id.writable = False
    else:
        pass

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas,
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.cuenta,
            _dbTable.sucursal,
            _dbTable.empresa_bancocontpaqi_id,
            _dbTable.codigo_contpaqi,
            _dbTable.tipocuenta,
            _dbTable.tipocomision,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.cuenta,
            _dbTable.sucursal,
            _dbTable.tipocuenta
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        )

    _O_cat.addRowContentEvent('onValidation', _empresa_cuentasbancarias_validation)

    _dbRow = dbc01.tempresas(_s_empresa_id)

    _O_cat.addRowContentProperty('formHidden', {'moneda_id_empresa': str(_dbRow.moneda_id)})

    _D_returnVars = _O_cat.process()

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_cuentabancaria_terminales,
        s_url = URL(
            f = 'empresa_cuentabancaria_terminales',
            args = request.args[:2] + ['master_' + str(request.function), request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'empresa_cuentabancaria_terminales', s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_cuentabancaria_guias,
        s_url = URL(
            f = 'empresa_cuentabancaria_guias',
            args = request.args[:2] + ['master_' + str(request.function), request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'empresa_cuentabancaria_guias', s_masterIdentificator = str(request.function)
            )
        )

    return _D_returnVars


def empresa_esquemasimpuestostrasladados():
    """ Esquemas de impuestos trasladados
    
    @descripcion Esquema de Impuestos Trasladados
    
    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    #@permiso btn_signature
    """

    _dbTable = dbc01.tempresa_esquemasimpuestostrasladados
    _s_empresa_id = request.args(1, None)
    _s_action = request.args(2, request.stv_fwk_permissions.btn_none.code)
    _s_esquema_id = request.args(3, None)

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas,
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.impuesto_id,
            _dbTable.nombre,
            _dbTable.tipofactor,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.id,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        )

    def validacion(O_form):
        # TODO validar que nombre no se pueda repetir por empresa
        return O_form

    _O_cat.addRowContentEvent('onValidation', validacion)

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def empresa_cuentabancaria_terminales():
    """ Detalle de las terminales correspondiente a las cuentas bancarias 
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_cuentabancaria_id
        [arg4]: operación a realizar en el formulario
        [arg5]: id en caso de existir relación a un registro en la operación
    """

    _dbTable = dbc01.tempresa_cuentabancaria_terminales
    _s_empresa_id = request.args(1, None)

    _dbTable.empresa_plaza_sucursal_id.requires = IS_IN_DB(
        dbc01(
            (dbc01.tempresa_plazas.empresa_id == _s_empresa_id)
            & (dbc01.tempresa_plaza_sucursales.empresa_plaza_id == dbc01.tempresa_plazas.id)
            ),
        'tempresa_plaza_sucursales.id',
        dbc01.tempresa_plaza_sucursales._format
        )

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_cuentasbancarias,
        xFieldLinkMaster = [
            _dbTable.empresa_cuentabancaria_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.terminal,
            _dbTable.numeroserie,
            _dbTable.empresa_plaza_sucursal_id,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.terminal,
            _dbTable.numeroserie
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            _dbTable.terminal,
            _dbTable.numeroserie
            ],
        x_offsetInArgs = request.args[:2]
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def empresa_cuentabancaria_guias():
    """ Detalle de guías contables de las cuentas bancarias correspondiente a la empresa
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_cuentabancaria_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """
    _dbTable = dbc01.tempresa_cuentabancaria_guias

    _s_empresa_id = request.args(1, None)
    _s_cuentabancaria_id = request.args(3, None)

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_cuentasbancarias,
        xFieldLinkMaster = [_dbTable.empresa_cuentabancaria_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.empresa_cuentabancaria_id,
            _dbTable.ejercicio,
            _dbTable.cuentacontable,
            _dbTable.cuentacontable_provisionescomisiones,
            _dbTable.cuentacontable_comisiones,
            _dbTable.cuentacontable_complementaria
            ],
        L_fieldsToSearch = [
            _dbTable.empresa_cuentabancaria_id,
            _dbTable.ejercicio
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            _dbTable.cuentacontable,
            _dbTable.cuentacontable_complementaria
            ],
        x_offsetInArgs = request.args[:2]
        )

    _dbRow = dbc01.tempresas(_s_empresa_id)
    _dbRowCuentaBancaria = dbc01.tempresa_cuentasbancarias(_s_cuentabancaria_id)
    _O_cat.addRowContentProperty(
        'formHidden', {
            'moneda_id_empresa': str(_dbRow.moneda_id),
            'moneda_id_cuentabancaria': str(_dbRowCuentaBancaria.moneda_id)
            }
        )

    _O_cat.addRowContentEvent('onValidation', _empresa_cuentabancaria_guias_validation)
    _D_returnVars = _O_cat.process()

    return _D_returnVars


def _empresa_cuentabancaria_guias_validation(O_form):
    """ Validación para las guías contables de las cuentas bancarias.
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_cuentabancaria_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """

    _s_empresa_id = request.args(1, None)
    _s_cuenta_bancaria_id = request.args(3, None)
    _s_ejercicio = O_form.vars.ejercicio
    _s_id = O_form.record_id

    if dbc01(
            (dbc01.tempresa_cuentabancaria_guias.empresa_cuentabancaria_id == _s_cuenta_bancaria_id)
            & (dbc01.tempresa_cuentabancaria_guias.ejercicio == _s_ejercicio)
            & (dbc01.tempresa_cuentabancaria_guias.id != _s_id)
            ).count() > 0:
        O_form.errors.ejercicio = 'La relación entre la cuenta bancaria y el ejercicio de cliente no puede repetirse'
    else:
        pass

    _s_msgError = VALIDACIONES.EJERCICIO.ES_VALIDO(_s_empresa_id, _s_ejercicio)

    if _s_msgError:
        O_form.errors.id = _s_msgError
    else:
        pass

    return O_form


def empresa_plazas():
    """ Función detalle de las plazas correspondientes a la empresa
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: operación a realizar en el formulario
        [arg3]: id en caso de existir relación a un registro en la operación
    """

    _dbTable = dbc01.tempresa_plazas

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas,
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.nombrecorto,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.nombrecorto
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _D_returnVars = _O_cat.process()

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_plaza_sucursales,
        s_url = URL(
            f = 'empresa_plaza_sucursales',
            args = request.args[:2] + ['master_' + str(request.function), request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_plaza_sucursales', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_plaza_guiasclientes,
        s_url = URL(
            f = 'empresa_plaza_guiasclientes',
            args = request.args[:2] + ['master_' + str(request.function), request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'empresa_plaza_guiasclientes', s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_plaza_guiasproveedores,
        s_url = URL(
            f = 'empresa_plaza_guiasproveedores',
            args = request.args[:2] + ['master_' + str(request.function), request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'empresa_plaza_guiasproveedores', s_masterIdentificator = str(request.function)
            )
        )

    return _D_returnVars


def empresa_plaza_sucursales():
    """ Función detalle de las sucursales correspondientes a la plaza

    @descripcion Sucursales

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Plaza
    @keyword arg3: plaza_id.
    @keyword [arg4]: operación a realizar en el formulario.
    @keyword [arg5]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature

    """
    _s_empresa_id = request.args(1, None)

    _dbTable = dbc01.tempresa_plaza_sucursales

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_plazas,
        xFieldLinkMaster = [
            _dbTable.empresa_plaza_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.nombrecorto,
            _dbTable.ciudad,
            _dbTable.colonia,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.nombrecorto,
            _dbTable.ciudad,
            _dbTable.colonia,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:2]
        )

    _D_returnVars = _O_cat.process()

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_plaza_sucursal_cajaschicas,
        s_url = URL(
            c = '150contabilidad', f = 'empresa_plaza_sucursal_cajaschicas',
            args = request.args[:4] + ['master_' + str(request.function), request.args(5)]
            ),
        s_idHtml = fn_createIDTabUnique(
            c = '150contabilidad', f = 'empresa_plaza_sucursal_cajaschicas',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_plaza_sucursal_guiasclientes,
        s_url = URL(
            f = 'empresa_plaza_sucursal_guiasclientes',
            args = request.args[:4] + ['master_' + str(request.function), request.args(5)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'empresa_plaza_sucursal_guiasclientes', s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_plaza_sucursal_guiasproveedores,
        s_url = URL(
            f = 'empresa_plaza_sucursal_guiasproveedores',
            args = request.args[:4] + ['master_' + str(request.function), request.args(5)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'empresa_plaza_sucursal_guiasproveedores', s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_plaza_sucursal_esquemasimptras,
        s_url = URL(
            f = 'empresa_plaza_sucursal_esquemasimptras',
            args = request.args[:4] + ['master_' + str(request.function), request.args(5)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'empresa_plaza_sucursal_esquemasimptras', s_masterIdentificator = str(request.function)
            )
        )

    return _D_returnVars


def _empresa_plaza_guiasclientes_validation(O_form):
    """ Velidación de guia contable de clientes > plaza > empresa
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_plaza_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
        
    """

    _s_moneda_id = O_form.vars.monedacontpaqi_id
    _s_tipocliente = O_form.vars.tipocliente
    _s_empresa_id = request.args(1, None)
    _s_empresa_plaza_id = request.args(3, None)
    _s_ejercicio = O_form.vars.ejercicio
    _s_id = O_form.record_id

    if dbc01(
            (dbc01.tempresa_plaza_guiasclientes.empresa_plaza_id == _s_empresa_plaza_id)
            & (dbc01.tempresa_plaza_guiasclientes.monedacontpaqi_id == _s_moneda_id)
            & (dbc01.tempresa_plaza_guiasclientes.tipocliente == _s_tipocliente)
            & (dbc01.tempresa_plaza_guiasclientes.ejercicio == _s_ejercicio)
            & (dbc01.tempresa_plaza_guiasclientes.id != _s_id)
            ).count() > 0:
        O_form.errors.monedacontpaqi_id = 'La relación entre moneda y tipo de cliente no puede repetirse'
    else:
        pass

    _s_msgError = VALIDACIONES.EJERCICIO.ES_VALIDO(_s_empresa_id, O_form.vars.ejercicio)

    if _s_msgError:
        O_form.errors.id = _s_msgError
    else:
        pass

    return O_form


def empresa_plaza_guiasclientes():
    """ Detalle de guia contable de clientes > plaza > empresa
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_plaza_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
        
    """

    _dbTable = dbc01.tempresa_plaza_guiasclientes
    _s_empresa_id = request.args(1, None)
    _s_action = request.args(4, 'None')
    _s_guia_id = request.args(5, None)

    # Si se desea crear un nuevo registro se puede editar la _s_guia_id
    if not _s_guia_id or (_s_action in (request.stv_fwk_permissions.btn_new.code, )):
        _dbTable.tipocliente.writable = True
        _dbTable.monedacontpaqi_id.writable = True
        _dbTable.ejercicio.writable = True
    else:
        _dbTable.tipocliente.writable = False
        _dbTable.monedacontpaqi_id.writable = False
        _dbTable.ejercicio.writable = False

    _dbTable = dbc01.tempresa_plaza_guiasclientes
    _s_empresa_id = request.args(1, None)

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_plazas,
        xFieldLinkMaster = [
            _dbTable.empresa_plaza_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.ejercicio,
            _dbTable.monedacontpaqi_id,
            _dbTable.tipocliente,
            _dbTable.cuenta_cliente,
            _dbTable.cuenta_cliente_complemento,
            _dbTable.cuenta_anticipo,
            _dbTable.cuenta_anticipo_complemento,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.monedacontpaqi_id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:2]
        )

    _dbRow = dbc01.tempresas(_s_empresa_id)
    _O_cat.addRowContentProperty('formHidden', {'moneda_id_empresa': str(_dbRow.monedacontpaqi_id)})

    _O_cat.addRowContentEvent('onValidation', _empresa_plaza_guiasclientes_validation)
    _D_returnVars = _O_cat.process()

    return _D_returnVars


def _empresa_plaza_sucursal_guiasclientes_validation(O_form):
    """ Validacion de guia contable clientes > sucursal > plaza > empresa
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_plaza_id
        arg4: master_[nombre función maestro]
        arg5: empresa_plaza_sucursal_id
        [arg6]: codigo de funcionalidad
        [arg7]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
        
    """

    _s_moneda_id = O_form.vars.monedacontpaqi_id
    _s_tipocliente = O_form.vars.tipocliente
    _s_empresa_id = request.args(1, None)
    _s_empresa_plaza_sucursal_id = request.args(5, None)
    _s_ejercicio = O_form.vars.ejercicio
    _s_id = O_form.record_id

    if dbc01(
            (dbc01.tempresa_plaza_sucursal_guiasclientes.empresa_plaza_sucursal_id == _s_empresa_plaza_sucursal_id)
            & (dbc01.tempresa_plaza_sucursal_guiasclientes.monedacontpaqi_id == _s_moneda_id)
            & (dbc01.tempresa_plaza_sucursal_guiasclientes.tipocliente == _s_tipocliente)
            & (dbc01.tempresa_plaza_sucursal_guiasclientes.ejercicio == _s_ejercicio)
            & (dbc01.tempresa_plaza_sucursal_guiasclientes.id != _s_id)
            ).count() > 0:
        O_form.errors.monedacontpaqi_id = 'La relación entre moneda, tipo de cliente y ejercicio no puede repetirse'
    else:
        pass

    _s_msgError = VALIDACIONES.EJERCICIO.ES_VALIDO(_s_empresa_id, _s_ejercicio)

    if _s_msgError:
        O_form.errors.id = _s_msgError
    else:
        pass

    return O_form


def empresa_plaza_sucursal_guiasclientes():
    """ Detalle de guia contable clientes > sucursal > plaza > empresa
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_plaza_id
        arg4: master_[nombre función maestro]
        arg5: empresa_plaza_sucursal_id
        [arg6]: codigo de funcionalidad
        [arg7]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """

    _dbTable = dbc01.tempresa_plaza_sucursal_guiasclientes
    _s_empresa_id = request.args(1, None)
    _s_action = request.args(6, 'None')
    _s_guia_id = request.args(7, None)

    # Si se desea crear un nuevo registro se puede editar la _s_guia_id
    if not _s_guia_id or (_s_action in (request.stv_fwk_permissions.btn_new.code, )):
        _dbTable.tipocliente.writable = True
        _dbTable.monedacontpaqi_id.writable = True
        _dbTable.ejercicio.writable = True
    else:
        _dbTable.tipocliente.writable = False
        _dbTable.monedacontpaqi_id.writable = False
        _dbTable.ejercicio.writable = False

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_plaza_sucursales,
        xFieldLinkMaster = [
            _dbTable.empresa_plaza_sucursal_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.ejercicio,
            _dbTable.monedacontpaqi_id,
            _dbTable.tipocliente,
            _dbTable.cuenta_cliente,
            _dbTable.cuenta_cliente_complemento,
            _dbTable.cuenta_anticipo,
            _dbTable.cuenta_anticipo_complemento,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.monedacontpaqi_id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:4]
        )

    _dbRow = dbc01.tempresas(_s_empresa_id)
    _O_cat.addRowContentProperty('formHidden', {'moneda_id_empresa': str(_dbRow.monedacontpaqi_id)})

    _O_cat.addRowContentEvent('onValidation', _empresa_plaza_sucursal_guiasclientes_validation)
    _D_returnVars = _O_cat.process()

    return _D_returnVars


def empresa_plaza_sucursal_esquemasimptras():
    """ Esquema de impuestos trasladados definidos a la plaza
    
    @descripcion Esquemas de Impuestos Trasladados
    
    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Plaza
    @keyword arg3: empresa_plaza_id.
    @keyword arg4: Sucursal
    @keyword arg5: empresa_plaza_sucursal_id.
    @keyword [arg6]: operación a realizar en el formulario.
    @keyword [arg7]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    """

    _dbTable = dbc01.tempresa_plaza_sucursal_esquemasimptras
    _s_empresa_id = request.args(1, None)
    _s_action = request.args(6, 'None')
    _s_esquema_id = request.args(7, None)

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_plaza_sucursales,
        xFieldLinkMaster = [
            _dbTable.empresa_plaza_sucursal_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.ejercicio,
            _dbTable.empresa_esquemaimptras_id,
            _dbTable.tasaocuota,
            _dbTable.descripcion,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:4]
        )

    def validacion(O_form):
        # TODO no se puede repetir esquema por ejercicio y sucursal
        return O_form

    _O_cat.addRowContentEvent('onValidation', validacion)
    _D_returnVars = _O_cat.process()

    return _D_returnVars


def empresa_plaza_guiasproveedores():
    """ Detalle de guia contable proveedores > plaza > empresa
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_plaza_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
        
    """

    _dbTable = dbc01.tempresa_plaza_guiasproveedores
    _s_empresa_id = request.args(1, None)
    _s_action = request.args(4, 'None')
    _s_guia_id = request.args(5, None)

    # Si se desea crear un nuevo registro se puede editar la _s_guia_id
    if not _s_guia_id or (_s_action in (request.stv_fwk_permissions.btn_new.code, )):
        _dbTable.tipoproveedor.writable = True
        _dbTable.monedacontpaqi_id.writable = True
        _dbTable.ejercicio.writable = True
    else:
        _dbTable.tipoproveedor.writable = False
        _dbTable.monedacontpaqi_id.writable = False
        _dbTable.ejercicio.writable = False

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_plazas,
        xFieldLinkMaster = [
            _dbTable.empresa_plaza_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.ejercicio,
            _dbTable.monedacontpaqi_id,
            _dbTable.tipoproveedor,
            _dbTable.cuenta_proveedor,
            _dbTable.cuenta_proveedor_complemento,
            _dbTable.cuenta_anticipo,
            _dbTable.cuenta_anticipo_complemento,
            _dbTable.bansuspender],
        L_fieldsToSearch = [
            _dbTable.monedacontpaqi_id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:2]
        )

    _dbRow = dbc01.tempresas(_s_empresa_id)
    _O_cat.addRowContentProperty('formHidden', {'moneda_id_empresa': str(_dbRow.monedacontpaqi_id)})

    _O_cat.addRowContentEvent('onValidation', _empresa_plaza_guiasproveedores_validation)
    _D_returnVars = _O_cat.process()

    return _D_returnVars


def _empresa_plaza_guiasproveedores_validation(O_form):
    """ Validación de guia contable proveedores > sucursal > plaza > empresa
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_plaza_id
        arg4: master_[nombre función maestro]
        arg5: empresa_plaza_sucursal_id
        [arg6]: codigo de funcionalidad
        [arg7]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
        
    """

    _s_moneda_id = O_form.vars.monedacontpaqi_id
    _s_tipoproveedor = O_form.vars.tipoproveedor
    _s_empresa_id = request.args(1, None)
    _s_empresa_plaza_id = request.args(3, None)
    _s_ejercicio = O_form.vars.ejercicio
    _s_id = O_form.record_id

    _dbRows = dbc01(
        (dbc01.tempresa_plaza_guiasproveedores.empresa_plaza_id == _s_empresa_plaza_id)
        & (dbc01.tempresa_plaza_guiasproveedores.monedacontpaqi_id == _s_moneda_id)
        & (dbc01.tempresa_plaza_guiasproveedores.tipoproveedor == _s_tipoproveedor)
        & (dbc01.tempresa_plaza_guiasproveedores.ejercicio == _s_ejercicio)
        & (dbc01.tempresa_plaza_guiasproveedores.id != _s_id)  # Se ignora el registro que se esté modificando
        ).select(dbc01.tempresa_plaza_guiasproveedores.id)
    if _dbRows:
        O_form.errors.monedacontpaqi_id = "La relación entre moneda, tipo de proveedor y ejercicio no puede repetirse"
    else:
        pass

    _s_msgError = VALIDACIONES.EJERCICIO.ES_VALIDO(_s_empresa_id, _s_ejercicio)

    if _s_msgError:
        O_form.errors.id = _s_msgError
    else:
        pass

    return O_form


def empresa_plaza_sucursal_guiasproveedores():
    """ Detalle de guia contable proveedores > sucursal > plaza > empresa
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_plaza_id
        arg4: master_[nombre función maestro]
        arg5: empresa_plaza_sucursal_id
        [arg6]: codigo de funcionalidad
        [arg7]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
        
    """

    _dbTable = dbc01.tempresa_plaza_sucursal_guiasproveedores
    _s_empresa_id = request.args(1, None)
    _s_action = request.args(6, 'None')
    _s_guia_id = request.args(7, None)

    # Si se desea crear un nuevo registro se puede editar la _s_guia_id
    if not _s_guia_id or (_s_action in (request.stv_fwk_permissions.btn_new.code, )):
        _dbTable.tipoproveedor.writable = True
        _dbTable.monedacontpaqi_id.writable = True
        _dbTable.ejercicio.writable = True
    else:
        _dbTable.tipoproveedor.writable = False
        _dbTable.monedacontpaqi_id.writable = False
        _dbTable.ejercicio.writable = False

    _dbTable.ejercicio.requires = IS_IN_SET(
        VALIDACIONES.EJERCICIO.OPCIONES(_s_empresa_id), zero = 0, error_message = 'Selecciona'
        )
    _dbTable.ejercicio.represent = lambda v, r: stv_represent_list(
        v, r, None, VALIDACIONES.EJERCICIO.OPCIONES(_s_empresa_id)
        )

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_plaza_sucursales,
        xFieldLinkMaster = [_dbTable.empresa_plaza_sucursal_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.ejercicio,
            _dbTable.monedacontpaqi_id,
            _dbTable.tipoproveedor,
            _dbTable.cuenta_proveedor,
            _dbTable.cuenta_proveedor_complemento,
            _dbTable.cuenta_anticipo,
            _dbTable.cuenta_anticipo_complemento,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.moneda_id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:4]
        )

    _dbRow = dbc01.tempresas(_s_empresa_id)
    _O_cat.addRowContentProperty('formHidden', {'moneda_id_empresa': str(_dbRow.monedacontpaqi_id)})

    _O_cat.addRowContentEvent('onValidation', _empresa_plaza_sucursal_guiasproveedores_validation)
    _D_returnVars = _O_cat.process()

    return _D_returnVars


def _empresa_plaza_sucursal_guiasproveedores_validation(O_form):
    """ Validacion de guia contable proveedores > sucursal > plaza > empresa
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_plaza_id
        arg4: master_[nombre función maestro]
        arg5: empresa_plaza_sucursal_id
        [arg6]: codigo de funcionalidad
        [arg7]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
        
    """

    _s_moneda_id = O_form.vars.monedacontpaqi_id
    _s_tipoproveedor = O_form.vars.tipoproveedor
    _s_empresa_id = request.args(1, None)
    _s_empresa_plaza_sucursal_id = request.args(5, None)
    _s_ejercicio = O_form.vars.ejercicio
    _s_id = O_form.record_id

    _dbRows = dbc01(
        (dbc01.tempresa_plaza_sucursal_guiasproveedores.empresa_plaza_sucursal_id == _s_empresa_plaza_sucursal_id)
        & (dbc01.tempresa_plaza_sucursal_guiasproveedores.monedacontpaqi_id == _s_moneda_id)
        & (dbc01.tempresa_plaza_sucursal_guiasproveedores.tipoproveedor == _s_tipoproveedor)
        & (dbc01.tempresa_plaza_sucursal_guiasproveedores.ejercicio == _s_ejercicio)
        & (dbc01.tempresa_plaza_sucursal_guiasproveedores.id != _s_id)  # Se ignora el registro que se esté modificando
        ).select(dbc01.tempresa_plaza_sucursal_guiasproveedores.id)

    if _dbRows:
        O_form.errors.monedacontpaqi_id = "La relación entre moneda, tipo de proveedor y ejercicio no puede repetirse"
    else:
        pass

    _s_msgError = VALIDACIONES.EJERCICIO.ES_VALIDO(_s_empresa_id, _s_ejercicio)

    if _s_msgError:
        O_form.errors.id = _s_msgError
    else:
        pass

    return O_form


def empresa_prodserv():
    """ Detalle de productos y servicios > empresa

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """

    _dbTable = dbc01.tempresa_prodserv

    _s_prodserv_id = request.args(3, None)

    # Se llena el combobox para empresa_proveedor_id
    _dbTable.empresa_proveedor_id.requires = IS_IN_DB(
        dbc01,
        'tempresa_proveedores.id',
        '%(razonsocial)s'
        )

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas,
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.codigo,
            _dbTable.tipo,
            _dbTable.descripcion,
            _dbTable.marca_id,
            _dbTable.unidad_id,
            _dbTable.prodserv_id,
            dbc01.tempresa_prodserv_ingresosestructuras.id,
            dbc01.tempresa_prodserv_ingresosestructuras.empresa_ingresoestructura_id,
            _dbTable.preciofijo,
            _dbTable.preciotipocalculo,
            _dbTable.porcentajeutilidad,
            _dbTable.porcentajemaxdesc,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.codigo,
            _dbTable.tipo,
            _dbTable.descripcion
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            _dbTable.codigo
            ]
        )

    _O_cat.addRowSearchResultsLeftJoin(
        dbc01.tempresa_prodserv_ingresosestructuras.on(
            (_dbTable.id == dbc01.tempresa_prodserv_ingresosestructuras.empresa_prodserv_id)
            & (dbc01.tempresa_prodserv_ingresosestructuras.ejercicio == str(request.browsernow.year))
            )
        )

    _dbRowProdserv = dbc01(
        _dbTable.id == _s_prodserv_id
        ).select(
        _dbTable.categoria_id,
        _dbTable.preciotipocalculo
        ).last()

    _O_cat.addRowContentEvent('onValidation', _empresa_prodserv_validation)

    TEMPRESA_PRODSERV.CONFIGURAR_CAMPO_CASCO1A1()

    _D_returnVars = _O_cat.process()

    _O_cat.addSubTab(
        s_tabName = 'Guías Ingresos',
        s_url = URL(
            f = 'empresa_prodserv_ingresosestructuras',
            args = request.args[:2] + ['master_' + str(request.function), request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'empresa_prodserv_ingresosestructuras', s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        s_tabName = 'Presentación',
        s_url = URL(
            f = 'empresa_prodserv_presentaciones',
            args = request.args[:2] + ['master_' + str(request.function), request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'empresa_prodserv_presentaciones', s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        s_tabName = 'Tratamiento',
        s_url = URL(
            f = 'empresa_prodserv_tratamiento',
            args = request.args[:2] + ['master_' + str(request.function), request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'empresa_prodserv_tratamiento', s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        s_tabName = 'Imagenes',
        s_url = URL(
            f = 'empresa_prodserv_imagenes',
            args = request.args[:2] + ['master_' + str(request.function), request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_prodserv_imagenes', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        s_tabName = 'Códigos proveedores',
        s_url = URL(
            f = 'empresa_prodserv_codigosproveedores',
            args = request.args[:2] + ['master_' + str(request.function), request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'empresa_prodserv_codigosproveedores', s_masterIdentificator = str(request.function)
            )
        )

    if _dbRowProdserv:

        if _dbRowProdserv.categoria_id:

            _O_cat.addSubTab(
                s_tabName = 'Especificaciones',
                s_url = URL(
                    f = 'empresa_prodserv_especificaciones',
                    args = request.args[:2] + ['master_' + str(request.function), request.args(3)]
                    ),
                s_idHtml = fn_createIDTabUnique(
                    f = 'empresa_prodserv_especificaciones', s_masterIdentificator = str(request.function)
                    )
                )

        else:
            pass
    else:
        pass

    _O_cat.addSubTab(
        s_tabName = 'Especificaciones',
        s_url = URL(
            f = 'empresa_prodserv_especificaciones',
            args = request.args[:2] + ['master_' + str(request.function), request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'empresa_prodserv_especificaciones', s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        s_tabName = 'Esquemas de Impuestos Trasladados',
        s_url = URL(
            f = 'empresa_prodserv_esquemasimptras',
            args = request.args[:2] + ['master_' + str(request.function), request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'empresa_prodserv_esquemasimptras', s_masterIdentificator = str(request.function)
            )
        )

    return _D_returnVars


def empresa_prodserv_esquemasimptras():
    """ Esquema de impuestos trasladados para prodserv
    
    @descripcion Esquema de Impuestos Trasladados

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: ProdServ
    @keyword arg3: empresa_prodserv_id.
    @keyword [arg4]: operación a realizar en el formulario.
    @keyword [arg5]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    #@permiso btn_searchcancel
    #@permiso btn_searchdone
    """
    _dbTable = dbc01.tempresa_prodserv_esquemasimptras

    _s_empresa_id = request.args(1, None)
    _s_action = request.args(4, 'None')
    _s_esquema_id = request.args(5, None)

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_prodserv,
        xFieldLinkMaster = [
            _dbTable.empresa_prodserv_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.esquemaimptras_id,
            _dbTable.observacion,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.id,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        x_offsetInArgs = request.args[:2]
        )

    def validacion(O_form):
        # TODO Validar que no se repita el esquema por producto
        return O_form

    _O_cat.addRowContentEvent('onValidation', validacion)

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def _empresa_prodserv_validation(O_form):
    """ Validación de ProdServ

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """

    if int(O_form.vars.preciotipocalculo) == TEMPRESA_PRODSERV.E_TIPOCALCULO.PRECIO_COTIZADO:
        if O_form.vars.porcentajeutilidad or (O_form.vars.porcentajeutilidad == 0):
            pass
        else:
            O_form.errors.porcentajeutilidad = "Debe de llevar un 0 si no se quiere capturar."
            pass
    else:
        pass

    if int(O_form.vars.preciotipocalculo) == TEMPRESA_PRODSERV.E_TIPOCALCULO.PRECIO_OFICIAL:
        if O_form.vars.empresa_proveedor_id:
            pass
        else:
            O_form.errors.empresa_proveedor_id = "Si se seleccionó precio oficial se debe elegir un proveedor."
            pass
    else:
        pass

    return O_form


def _empresa_prodserv_ingresosestructuras_validation(O_form):
    """ Validación de estructuras ingresos > producto o servicio > empresa

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_prodserv_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """
    _s_empresa_id = request.args(1, None)
    _s_empresa_prodserv_id = request.args(3, 'None')

    _dbTable = dbc01.tempresa_prodserv_ingresosestructuras

    if 'ejercicio' in O_form.vars:
        _s_ejercicio = O_form.vars.ejercicio
    else:
        _s_ejercicio = None

    if 'empresa_ingresoestructura_id' in O_form.vars:
        if not O_form.vars.empresa_ingresoestructura_id:
            O_form.errors.empresa_ingresoestructura_id = "No puede estar vacío"
        else:
            pass
    else:
        pass

    # Se busca que la tasa no se este utilizando con anterioridad en la misma empresa
    _dbRows = dbc01(
        (_dbTable.empresa_prodserv_id == _s_empresa_prodserv_id)
        & (_dbTable.ejercicio == _s_ejercicio)
        & (_dbTable.id != O_form.record_id)  # Se ignora el registro que se esté modificando
        ).select(_dbTable.id)

    if _dbRows:
        O_form.errors.ejercicio = "Producto o Servicio y Ejercicio no puede duplicarse"
    else:
        pass

    _s_msgError = VALIDACIONES.EJERCICIO.ES_VALIDO(_s_empresa_id, O_form.vars.ejercicio)

    if _s_msgError:
        O_form.errors.id = _s_msgError
    else:
        pass

    return O_form


def empresa_prodserv_ingresosestructuras():
    """ Detalle de estructuras ingresos > producto o servicio > empresa

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_prodserv_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """

    _dbTable = dbc01.tempresa_prodserv_ingresosestructuras

    _s_empresa_id = request.args(1, None)
    _s_action = request.args(4, 'None')
    _s_ingresoestructura_id = request.args(5, None)

    _dbTable.empresa_ingresoestructura_id.requires = IS_IN_DB(
        dbc01(
            (dbc01.tempresa_ingresosestructura.empresa_id == _s_empresa_id)
            ),
        'tempresa_ingresosestructura.id',
        dbc01.tempresa_ingresosestructura._format
        )

    # Si se desea crear un nuevo registro se puede editar la empresa_ingresoestructura_id
    if not _s_ingresoestructura_id or (_s_action in request.stv_fwk_permissions.btn_new.code):
        _dbTable.ejercicio.writable = True
        _dbTable.empresa_ingresoestructura_id.writable = True

    else:
        _dbTable.ejercicio.writable = False
        _dbTable.empresa_ingresoestructura_id.writable = False

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_prodserv,
        xFieldLinkMaster = [
            _dbTable.empresa_prodserv_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.ejercicio,
            _dbTable.empresa_ingresoestructura_id,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.ejercicio,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            _dbTable.ejercicio
            ],
        x_offsetInArgs = request.args[:2]
        )

    _O_cat.addRowContentEvent('onValidation', _empresa_prodserv_ingresosestructuras_validation)

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def empresa_prodserv_buscar():
    """ Forma buscar productos y servicios
 
    @descripcion Buscar Productos o Servicios
 
    @keyword arg0: Empresa.
    @keyword arg1: empresa_id
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.    

    @permiso btn_none
    #@permiso btn_new
    #@permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    @permiso btn_find
    #@permiso btn_findall
    #@permiso btn_signature
    @permiso btn_searchcancel
    @permiso btn_searchdone
    """

    _dbTable = dbc01.tempresa_prodserv

    _L_GroupBy = [
        _dbTable.id,
        ]

    # TODO Hacer filtro de productos para la empresa

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(),
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas,
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formSearchButtons,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.codigo,
            _dbTable.tipo,
            _dbTable.descripcion,
            _dbTable.marca_id,
            _dbTable.unidad_id,
            _dbTable.prodserv_id,
            dbc01.tempresa_prodserv_imagenes.imagen,
            ],
        L_fieldsToGroupBy = _L_GroupBy,
        L_fieldsToSearch = [
            _dbTable.id,
            _dbTable.codigo,
            _dbTable.descripcion,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            _dbTable.codigo
            ],
        )

    _O_cat.addRowSearchResultsLeftJoin(
        dbc01.tempresa_prodserv_imagenes.on(
            (dbc01.tempresa_prodserv_imagenes.empresa_prodserv_id == _dbTable.id)
            )
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def empresa_ingresos():
    """Detalle de Empresa para manejar lo relativo a los ingresos.
    """

    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_find)

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbTable = None,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [],
        L_fieldsToSearch = [],
        L_fieldsToSearchIfDigit = [],
        x_offsetInArgs = request.args[:2]
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_ingresosestructura,
        s_url = URL(f = 'empresa_ingresosestructura', args = ['master_' + str(request.function), request.args(1)]),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_ingresosestructura', s_masterIdentificator = str(request.function))
        )

    #     _O_cat.addSubTab(
    #         dbTableDetail = dbc01.tempresa_prodserv,
    #         s_url = URL(f='empresa_prodserv', args=['master_'+str(request.function),request.args(1)]),
    #         s_idHtml = fn_createIDTabUnique(f = 'empresa_prodserv', s_masterIdentificator = str(request.function))
    #         )

    _O_cat.addSubTab(
        s_tabName = 'Contabilización',
        s_url = URL(
            c = '150contabilidad', f = 'empresa_plaza_sucursal_contabilidades',
            args = ['master_' + str(request.function), request.args(1)]
            ),
        s_idHtml = fn_createIDTabUnique(
            c = '150contabilidad', f = 'empresa_plaza_sucursal_contabilidades',
            s_masterIdentificator = str(request.function)
            )
        )

    _D_returnVars = _O_cat.process(D_overrideView = request.stv_fwk_permissions.view_viewRegister)

    return dict(_D_returnVars)


def empresa_gastos():
    """Detalle de Empresa para manejar lo relativo a los gastos.
    """

    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_find)

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbTable = None,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [],
        L_fieldsToSearch = [],
        L_fieldsToSearchIfDigit = [],
        x_offsetInArgs = request.args[:2]
        )

    _O_cat.addSubTab(
        s_tabName = "Estructura de gastos",
        s_url = URL(f = 'empresa_gastosestructura', args = ['master_' + str(request.function), request.args(1)]),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_gastosestructura', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        s_tabName = 'Contabilización',
        s_url = URL(
            c = '150contabilidad', f = 'empresa_gastoscontabilidad',
            args = ['master_' + str(request.function), request.args(1)]
            ),
        s_idHtml = fn_createIDTabUnique(
            c = '150contabilidad', f = 'empresa_gastoscontabilidad', s_masterIdentificator = str(request.function)
            )
        )

    _D_returnVars = _O_cat.process(D_overrideView = request.stv_fwk_permissions.view_viewRegister)

    return dict(_D_returnVars)


def empresa_centrocostos():
    """Detalle de Empresa para manejar lo relativo a los Centros de costo.
    """

    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_find)

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbTable = None,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [],
        L_fieldsToSearch = [],
        L_fieldsToSearchIfDigit = [],
        x_offsetInArgs = request.args[:2]
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_centrocostosestructura,
        s_url = URL(f = 'empresa_centrocostosestructura', args = ['master_' + str(request.function), request.args(1)]),
        s_idHtml = fn_createIDTabUnique(
            f = 'empresa_centrocostosestructura', s_masterIdentificator = str(request.function)
            )
        )

    _D_returnVars = _O_cat.process(D_overrideView = request.stv_fwk_permissions.view_viewRegister)

    return dict(_D_returnVars)


def empresa_organizacion():
    """Detalle de Empresa para manejar lo relativo a los Centros de costo.
    """

    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_find)

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbTable = None,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [],
        L_fieldsToSearch = [],
        L_fieldsToSearchIfDigit = [],
        x_offsetInArgs = request.args[:2]
        )

    _O_cat.addSubTab(
        s_tabName = "Estructura de organización",
        s_url = URL(f = 'empresa_organizacionestructura', args = ['master_' + str(request.function), request.args(1)]),
        s_idHtml = fn_createIDTabUnique(
            f = 'empresa_organizacionestructura', s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        s_tabName = "Empleados",
        s_url = URL(f = 'empresa_empleados', args = ['master_' + str(request.function), request.args(1)]),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_empleados', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        s_tabName = "Fondos fijos de caja",
        s_url = URL(f = 'empresa_fondosfijoscaja', args = ['master_' + str(request.function), request.args(1)]),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_fondosfijoscaja', s_masterIdentificator = str(request.function))
        )

    _D_returnVars = _O_cat.process(D_overrideView = request.stv_fwk_permissions.view_viewRegister)

    return dict(_D_returnVars)


def _empresa_ingresosestructura_validation(form):
    """ Validación de la inserción de la estructura de ingreso
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """
    _s_padre_id = form.vars.padre_id

    if _s_padre_id:
        _dbRows = dbc01(dbc01.tempresa_prodserv_ingresosestructuras.empresa_ingresoestructura_id == _s_padre_id).select(
            dbc01.tempresa_prodserv_ingresosestructuras.id
            )
        if _dbRows:
            form.errors.padre_id = "No se puede asociar a un padre que tiene productos ligados"

    return form


def empresa_ingresosestructura():
    """ Detalle de estructura de ingreso > empresa

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
        
    """
    _s_action = request.args(2, 'None')
    _s_ingresoestructura_id = request.args(3, None)

    _dbTable = dbc01.tempresa_ingresosestructura

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas,
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.nombrecorto,
            _dbTable.padre_id,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.nombrecorto
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ]
        )

    _dbRows = dbc01((_dbTable.id > 0) & (_dbTable.empresa_id == request.args(1))).select(
        _dbTable.ALL, orderby = _dbTable.orden
        )
    _dbTree = DB_Tree(_dbRows)
    _D_ingresosestructura = _dbTree.process_groupby('tempresa_ingresosestructura.padre_id')

    dbc01.tempresa_ingresosestructura.padre_id.default = request.args(3, None)

    if _O_cat.D_args.s_action in (request.stv_fwk_permissions.btn_edit.code, request.stv_fwk_permissions.btn_save.code):
        _n_id = request.args(3, None)
    else:
        _n_id = None

    # dbc01.tempresa_ingresosestructura.padre_id.widget =
    #  lambda f, v: stv_widget_db_chosen_tree(f, v, 1,
    #  D_additionalAttributes={'D_tree':_D_ingresosestructura, 'n_filterFromId':_n_id})

    _O_cat.addRowSearchResultsProperty(
        s_property = 's_type', x_value = STV_FWK_FORM.ROWSEARCHRESULTS.PROPERTIES.TYPE.S_TREE
        )

    _O_cat.addRowContentEvent('onValidation', _empresa_ingresosestructura_validation)

    _D_returnVars = _O_cat.process(x_overrideQuery = _D_ingresosestructura)

    # Si se desea editar un registro
    if _s_ingresoestructura_id and (
            _s_action in (request.stv_fwk_permissions.btn_edit.code, request.stv_fwk_permissions.btn_save.code)):

        # Se buscan todo los registros hijo
        _n_rows = dbc01(_dbTable.padre_id == _s_ingresoestructura_id).count()

        if _n_rows == 0:

            _O_cat.addSubTab(
                s_tabName = 'Guías Ingresos',
                s_url = URL(
                    c = '150contabilidad', f = 'empresa_ingresoestructura_guiascontables',
                    args = request.args[:2] + ['master_' + str(request.function), request.args(3)]
                    ),
                s_idHtml = fn_createIDTabUnique(
                    c = '150contabilidad', f = 'empresa_ingresoestructura_guiascontables',
                    s_masterIdentificator = str(request.function)
                    )
                )

        else:
            pass

    else:
        pass

    return _D_returnVars


def _empresa_gastosestructura_validation(form):
    """ Validación de la inserción de la estructura de gasto
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """

    _s_padre_id = form.vars.padre_id
    # TODO: EVALUAR SI ES NECESARIO
    #  if _s_padre_id:
    #      _dbRows = dbc01(dbc01.tempresa_prodserv.empresa_gastosestructura_id == _s_padre_id)
    #      .select(dbc01.tempresa_prodserv.id)
    #      if _dbRows:
    #          form.errors.padre_id = "No se puede asociar a un padre que tiene productos ligados"

    return form


def empresa_gastosestructura():
    """ Detalle de estructura de gastos > empresa

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """

    _dbTable = dbc01.tempresa_gastosestructura

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas,
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.nombrecorto,
            _dbTable.padre_id,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.nombrecorto
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ]
        )

    _dbRows = dbc01((_dbTable.id > 0) & (_dbTable.empresa_id == request.args(1))).select(
        _dbTable.ALL, orderby = _dbTable.orden
        )
    _dbTree = DB_Tree(_dbRows)
    _D_gastosestructura = _dbTree.process_groupby('tempresa_gastosestructura.padre_id')

    dbc01.tempresa_gastosestructura.padre_id.default = request.args(3, None)
    if _O_cat.D_args.s_action in (request.stv_fwk_permissions.btn_edit.code, request.stv_fwk_permissions.btn_save.code):
        _n_id = request.args(3, None)
    else:
        _n_id = None
    # dbc01.tempresa_gastosestructura.padre_id.widget = lambda f, v:
    #  stv_widget_db_chosen_tree(f, v, 1, D_additionalAttributes={'D_tree':_D_gastosestructura, 'n_filterFromId':_n_id})

    _O_cat.addRowSearchResultsProperty(
        s_property = 's_type', x_value = STV_FWK_FORM.ROWSEARCHRESULTS.PROPERTIES.TYPE.S_TREE
        )

    _O_cat.addRowContentEvent('onValidation', _empresa_gastosestructura_validation)

    _D_returnVars = _O_cat.process(x_overrideQuery = _D_gastosestructura)

    return _D_returnVars


def _empresa_centrocostosestructura_validation(form):
    """ Validación de la inserción de la estructura de Centro de costos
    """
    _s_padre_id = form.vars.padre_id
    # TODO: EVALUAR SI ES NECESARIO
    #  if _s_padre_id:
    #      _dbRows = dbc01(dbc01.tempresa_prodserv.empresa_centrocostosestructura_id == _s_padre_id).
    #      select(dbc01.tempresa_prodserv.id)
    #      if _dbRows:
    #          form.errors.padre_id = "No se puede asociar a un padre que tiene productos ligados"

    return form


def empresa_centrocostosestructura():
    """ Detalle de estructura de centrocostos > empresa

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """
    _dbTable = dbc01.tempresa_centrocostosestructura

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas,
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.nombrecorto,
            _dbTable.padre_id,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.nombrecorto
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ]
        )

    _dbRows = dbc01((_dbTable.id > 0) & (_dbTable.empresa_id == request.args(1))).select(
        _dbTable.ALL, orderby = _dbTable.orden
        )
    _dbTree = DB_Tree(_dbRows)
    _D_centrocostosestructura = _dbTree.process_groupby('tempresa_centrocostosestructura.padre_id')

    dbc01.tempresa_centrocostosestructura.padre_id.default = request.args(3, None)

    if _O_cat.D_args.s_action in (request.stv_fwk_permissions.btn_edit.code, request.stv_fwk_permissions.btn_save.code):
        _n_id = request.args(3, None)
    else:
        _n_id = None

    # dbc01.tempresa_centrocostosestructura.padre_id.widget = lambda f, v:
    #  stv_widget_db_chosen_tree(f, v, 1,
    #  D_additionalAttributes={'D_tree':_D_centrocostosestructura, 'n_filterFromId':_n_id})

    _O_cat.addRowSearchResultsProperty(
        s_property = 's_type', x_value = STV_FWK_FORM.ROWSEARCHRESULTS.PROPERTIES.TYPE.S_TREE
        )

    _O_cat.addRowContentEvent('onValidation', _empresa_centrocostosestructura_validation)

    _D_returnVars = _O_cat.process(x_overrideQuery = _D_centrocostosestructura)

    return _D_returnVars


def _empresa_organizacionestructura_validation(form):
    """ Validación de la inserción de la estructura de Organizaciones
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """
    _s_padre_id = form.vars.padre_id
    # TODO: EVALUAR SI ES NECESARIO
    #  if _s_padre_id:
    #      _dbRows = dbc01(dbc01.tempresa_prodserv.empresa_centrocostosestructura_id == _s_padre_id).
    #      select(dbc01.tempresa_prodserv.id)
    #      if _dbRows:
    #          form.errors.padre_id = "No se puede asociar a un padre que tiene productos ligados"

    return form


def empresa_organizacionestructura():
    """ Detalle de estructura de organizaciones > empresa

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """
    _dbTable = dbc01.tempresa_organizacionestructura

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas,
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.nombrecorto,
            _dbTable.padre_id,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.nombrecorto
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ]
        )

    _dbRows = dbc01((_dbTable.id > 0) & (_dbTable.empresa_id == request.args(1))).select(
        _dbTable.ALL, orderby = _dbTable.orden
        )
    _dbTree = DB_Tree(_dbRows)
    _D_organizacionestructura = _dbTree.process_groupby('tempresa_organizacionestructura.padre_id')

    dbc01.tempresa_organizacionestructura.padre_id.default = request.args(3, None)

    if _O_cat.D_args.s_action in (request.stv_fwk_permissions.btn_edit.code, request.stv_fwk_permissions.btn_save.code):
        _n_id = request.args(3, None)
    else:
        _n_id = None

    # dbc01.tempresa_centrocostosestructura.padre_id.widget = lambda f, v: stv_widget_db_chosen_tree(f, v, 1,
    #  D_additionalAttributes={'D_tree':_D_centrocostosestructura, 'n_filterFromId':_n_id})

    _O_cat.addRowSearchResultsProperty(
        s_property = 's_type', x_value = STV_FWK_FORM.ROWSEARCHRESULTS.PROPERTIES.TYPE.S_TREE
        )

    _O_cat.addRowContentEvent('onValidation', _empresa_organizacionestructura_validation)

    _D_returnVars = _O_cat.process(x_overrideQuery = _D_organizacionestructura)

    return _D_returnVars


def empresa_impuestos():
    """ Formulario maestro para los impuestos

    @keyword arg0: master de empresa_id.
    @keyword arg1: empresa_id.
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.
    
    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    """

    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_find)

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbTable = None,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [],
        L_fieldsToSearch = [],
        L_fieldsToSearchIfDigit = [],
        x_offsetInArgs = request.args[:2]
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_iva,
        s_url = URL(
            c = '150contabilidad', f = 'empresa_iva', args = ['master_' + str(request.function), request.args(1)]
            ),
        s_idHtml = fn_createIDTabUnique(
            c = '150contabilidad', f = 'empresa_iva', s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_isr,
        s_url = URL(
            c = '150contabilidad', f = 'empresa_isr', args = ['master_' + str(request.function), request.args(1)]
            ),
        s_idHtml = fn_createIDTabUnique(
            c = '150contabilidad', f = 'empresa_isr', s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_ieps,
        s_url = URL(
            c = '150contabilidad', f = 'empresa_ieps', args = ['master_' + str(request.function), request.args(1)]
            ),
        s_idHtml = fn_createIDTabUnique(
            c = '150contabilidad', f = 'empresa_ieps', s_masterIdentificator = str(request.function)
            )
        )

    _D_returnVars = _O_cat.process(D_overrideView = request.stv_fwk_permissions.view_viewRegister)

    return dict(_D_returnVars)


def empresa_prodserv_presentaciones():
    """ Formulario para las presentaciones del producto.

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_prodserv_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    """

    _dbTable = dbc01.tempresa_prodserv_presentaciones

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_prodserv,
        xFieldLinkMaster = [
            _dbTable.empresa_prodserv_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.numeroparte,
            _dbTable.marca_id,
            _dbTable.codigobarrasexterno,
            _dbTable.codigobarrasinterno,
            _dbTable.unidad_id,
            _dbTable.contenido,
            _dbTable.observacion,
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:2]
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def empresa_prodserv_buscarcategorias():
    """ Formulario para la busqueda de categorias.

    """

    _dbTable = dbc01.tempresa_prodserv

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas,
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formSearchButtons,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.codigo,
            _dbTable.tipo,
            _dbTable.descripcion,
            _dbTable.marca_id,
            _dbTable.unidad_id,
            _dbTable.prodserv_id,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.codigo,
            _dbTable.tipo,
            _dbTable.descripcion
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            _dbTable.codigo
            ]
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def empresa_prodserv_especificaciones():
    """ Formulario para las especificaciones de los productos

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_prodserv_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    """

    _dbTable = dbc01.tempresa_prodserv_especificaciones

    _dbTable_especificaciones = dbc01.tempresa_prodcategoria_especificaciones

    _s_empresa_id = request.args(1, None)

    _s_prodserv_id = request.args(3, None)

    _s_action = request.args(4, request.stv_fwk_permissions.btn_none.code)

    _s_prodcategoria_id = dbc01.tempresa_prodserv[
        _s_prodserv_id].categoria_id  # TODO cambiar de nombre categoria_id a prodcategoria_id

    if _s_prodcategoria_id:

        _L_prodcategorias_padres_ids = TEMPRESA_PRODCATEGORIAS.get_padres_ids(_s_prodcategoria_id)

        _dbRows_especificaciones = dbc01(
            (_dbTable_especificaciones.categoria_id.belongs(_L_prodcategorias_padres_ids))
            ).select(
            _dbTable_especificaciones.id,
            _dbTable_especificaciones.especificacion,
            _dbTable_especificaciones.mensajecaptura,
            _dbTable_especificaciones.categoria_id,
            _dbTable.id,
            left = [
                _dbTable.on(
                    (_dbTable.prodcategoria_especificacion_id == _dbTable_especificaciones.id)
                    & (_dbTable.prodserv_id == _s_prodserv_id)
                    )
                ],
            orderby = _dbTable_especificaciones.especificacion
            )

        _L_especificaciones_correctas = []
        for _dbRow_especificacion in _dbRows_especificaciones:
            _L_especificaciones_correctas += [_dbRow_especificacion.tempresa_prodcategoria_especificaciones.id]
            if _dbRow_especificacion.tempresa_prodserv_especificaciones.id:
                # Si ya existe la espicificación en el prodserv, no la tocamos
                pass
            else:
                # Si no existe, se agrega
                _dbTable.insert(
                    prodserv_id = _s_prodserv_id,
                    prodcategoria_especificacion_id = _dbRow_especificacion.tempresa_prodcategoria_especificaciones.id
                    )

        # Se ponen todos las especificaciones en suspendidas       
        dbc01(
            (_dbTable.prodserv_id == _s_prodserv_id)
            & (~_dbTable.prodcategoria_especificacion_id.belongs(_L_especificaciones_correctas))
            ).update(
            bansuspender = True
            )

        dbc01(
            (_dbTable.prodserv_id == _s_prodserv_id)
            & (_dbTable.prodcategoria_especificacion_id.belongs(_L_especificaciones_correctas))
            ).update(
            bansuspender = False
            )

    else:
        # Si la categoría es nula, se suspende las especificaciones, 
        # pero se van a dejar para referencia en caso de re-seleccionar la categoría
        dbc01(_dbTable.prodserv_id == _s_prodserv_id).update(bansuspender = True)
        _L_prodcategorias_padres_ids = []
        pass

    _L_OrderBy = [
        _dbTable.prodcategoria_especificacion_id
        ]

    _L_formButtons = [
        request.stv_fwk_permissions.btn_none,
        # request.stv_fwk_permissions.btn_new,
        request.stv_fwk_permissions.btn_save,
        request.stv_fwk_permissions.btn_cancel,
        request.stv_fwk_permissions.btn_edit,
        # request.stv_fwk_permissions.btn_view,
        # request.stv_fwk_permissions.btn_print,
        # request.stv_fwk_permissions.btn_invalidate,
        # request.stv_fwk_permissions.btn_validate,
        # request.stv_fwk_permissions.btn_suspend,
        # request.stv_fwk_permissions.btn_activate,
        request.stv_fwk_permissions.btn_remove,
        # request.stv_fwk_permissions.btn_restore,
        # request.stv_fwk_permissions.btn_find,
        request.stv_fwk_permissions.btn_findall,
        # request.stv_fwk_permissions.btn_signature,
        ]

    # Para permitir editar cuando la vista esta vacía
    request.stv_fwk_permissions.view_emptyForm.L_permissionsDefault += [request.stv_fwk_permissions.btn_edit]

    D_stvSiteHelper.prodserv_especificaciones = Storage()

    if _s_action in (request.stv_fwk_permissions.btn_edit.code, request.stv_fwk_permissions.btn_save.code):
        # Se define una variable global que contenga los registros que se estan editando,
        #  para evitar doble consulta en cada evento
        D_stvSiteHelper.prodserv_especificaciones.dbRows = dbc01(
            (_dbTable.prodserv_id == _s_prodserv_id)
            & (_dbTable_especificaciones.id == _dbTable.prodcategoria_especificacion_id)
            & (_dbTable.bansuspender != True)
            ).select(
            _dbTable.ALL,
            _dbTable_especificaciones.ALL,
            orderby = _dbTable_especificaciones.especificacion
            )

        D_stvSiteHelper.prodserv_especificaciones.dbFields = []
        _b_skipaction = True
        for _dbRow in D_stvSiteHelper.prodserv_especificaciones.dbRows:
            if (
                    _dbRow.tempresa_prodcategoria_especificaciones.tipocaptura in (
                    TEMPRESA_PRODCATEGORIA_ESPECIFICACIONES.TIPOCAPTURA.LIBRE,
                    TEMPRESA_PRODCATEGORIA_ESPECIFICACIONES.TIPOCAPTURA.OPCION,
                    TEMPRESA_PRODCATEGORIA_ESPECIFICACIONES.TIPOCAPTURA.MULTIOPCION)
                    ):
                _dbField = dbc01.tempresa_prodserv_especificaciones.tipocaptura_texto.clone()
                _s_data = _dbRow.tempresa_prodserv_especificaciones.tipocaptura_texto
            elif (
                    _dbRow.tempresa_prodcategoria_especificaciones.tipocaptura in (
                        TEMPRESA_PRODCATEGORIA_ESPECIFICACIONES.TIPOCAPTURA.NUMERO,
                        TEMPRESA_PRODCATEGORIA_ESPECIFICACIONES.TIPOCAPTURA.MONEDA,
                        TEMPRESA_PRODCATEGORIA_ESPECIFICACIONES.TIPOCAPTURA.FRACCION,
                        TEMPRESA_PRODCATEGORIA_ESPECIFICACIONES.TIPOCAPTURA.PORCENTAJE,
                        )
                    ):
                _dbField = dbc01.tempresa_prodserv_especificaciones.tipocaptura_float.clone()
                _s_data = _dbRow.tempresa_prodserv_especificaciones.tipocaptura_float
            elif (
                    _dbRow.tempresa_prodcategoria_especificaciones.tipocaptura
                    == TEMPRESA_PRODCATEGORIA_ESPECIFICACIONES.TIPOCAPTURA.FECHA
                    ):
                _dbField = dbc01.tempresa_prodserv_especificaciones.tipocaptura_fecha.clone()
                _s_data = _dbRow.tempresa_prodserv_especificaciones.tipocaptura_fecha
            elif (
                    _dbRow.tempresa_prodcategoria_especificaciones.tipocaptura
                    == TEMPRESA_PRODCATEGORIA_ESPECIFICACIONES.TIPOCAPTURA.HORA
                    ):
                _dbField = dbc01.tempresa_prodserv_especificaciones.tipocaptura_hora.clone()
                _s_data = _dbRow.tempresa_prodserv_especificaciones.tipocaptura_hora
            else:
                # Como default para cualquier tipo, se asocia como texto libre
                _dbField = dbc01.tempresa_prodserv_especificaciones.tipocaptura_texto.clone()
                _s_data = _dbRow.tempresa_prodserv_especificaciones.tipocaptura_texto

            _dbField.name += "_" + str(_dbRow.tempresa_prodserv_especificaciones.id)
            _dbField.default = request.vars[_dbField.name] or _s_data
            _dbField.writable = True
            D_stvSiteHelper.prodserv_especificaciones.dbFields += [_dbField]

    else:
        _b_skipaction = False
        D_stvSiteHelper.prodserv_especificaciones.dbFields = []
        D_stvSiteHelper.prodserv_especificaciones.dbRows = None

    _dbTable.tipocaptura_texto.represent = stv_custom_represent_dynamicinput

    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.prodcategoria_especificacion_id,
        _dbTable.tipocaptura_texto,
        _dbTable.tipocaptura_float,
        _dbTable.tipocaptura_fecha,
        _dbTable.tipocaptura_hora,
        _dbTable.bansuspender
        ]
    # + D_stvSiteHelper.prodserv_especificaciones.dbFields No requerido, ya que se mandan
    #  los campos de forma individual en la vista

    _dbTable.bansuspender.readable = True
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_prodserv,
        # No se maneja maestro, ya que se necesita qry en el search y se insertan las especificaciones
        #  al refrescar la forma
        xFieldLinkMaster = [
            _dbTable.prodserv_id
            ],
        L_visibleButtons = _L_formButtons,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = [
            _dbTable.id,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        x_offsetInArgs = request.args[:2]
        )

    _O_cat.addRowSearchResultsConfig('multiselect', True)
    _O_cat.addRowSearchResultsProperty('orderBy', _L_OrderBy)

    _O_cat.addRowContentEvent('onValidation', _empresa_prodserv_especificaciones_validation)
    _O_cat.addRowContentEvent('onAccepted', _empresa_prodserv_especificaciones_accepted)

    _D_returnVars = _O_cat.process(
        b_skipAction = _b_skipaction, L_formFactoryFields = D_stvSiteHelper.prodserv_especificaciones.dbFields
        )

    _D_returnVars.dbRows_edit = D_stvSiteHelper.prodserv_especificaciones.dbRows

    return _D_returnVars


def _empresa_prodserv_especificaciones_validation(O_form):
    """ Validaciones para las especificaciones de los productos

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_prodserv_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    """
    if D_stvSiteHelper.prodserv_especificaciones.dbRows:
        for _dbRow in D_stvSiteHelper.prodserv_especificaciones.dbRows:
            if (
                    _dbRow.tempresa_prodcategoria_especificaciones.tipocaptura in (
                        TEMPRESA_PRODCATEGORIA_ESPECIFICACIONES.TIPOCAPTURA.LIBRE,
                        TEMPRESA_PRODCATEGORIA_ESPECIFICACIONES.TIPOCAPTURA.OPCION,
                        TEMPRESA_PRODCATEGORIA_ESPECIFICACIONES.TIPOCAPTURA.MULTIOPCION
                        )
                    ):
                _s_name = dbc01.tempresa_prodserv_especificaciones.tipocaptura_texto.name + "_" + str(
                    _dbRow.tempresa_prodserv_especificaciones.id
                    )
            elif (
                    _dbRow.tempresa_prodcategoria_especificaciones.tipocaptura in (
                        TEMPRESA_PRODCATEGORIA_ESPECIFICACIONES.TIPOCAPTURA.NUMERO,
                        TEMPRESA_PRODCATEGORIA_ESPECIFICACIONES.TIPOCAPTURA.MONEDA,
                        TEMPRESA_PRODCATEGORIA_ESPECIFICACIONES.TIPOCAPTURA.FRACCION,
                        TEMPRESA_PRODCATEGORIA_ESPECIFICACIONES.TIPOCAPTURA.PORCENTAJE
                        )
                    ):
                _s_name = dbc01.tempresa_prodserv_especificaciones.tipocaptura_float.name + "_" + str(
                    _dbRow.tempresa_prodserv_especificaciones.id
                    )
            elif (
                    _dbRow.tempresa_prodcategoria_especificaciones.tipocaptura
                    == TEMPRESA_PRODCATEGORIA_ESPECIFICACIONES.TIPOCAPTURA.FECHA
                    ):
                _s_name = dbc01.tempresa_prodserv_especificaciones.tipocaptura_fecha.name + "_" + str(
                    _dbRow.tempresa_prodserv_especificaciones.id
                    )
            elif (
                    _dbRow.tempresa_prodcategoria_especificaciones.tipocaptura
                    == TEMPRESA_PRODCATEGORIA_ESPECIFICACIONES.TIPOCAPTURA.HORA
                    ):
                _s_name = dbc01.tempresa_prodserv_especificaciones.tipocaptura_hora.name + "_" + str(
                    _dbRow.tempresa_prodserv_especificaciones.id
                    )
            else:
                # Por defautl se usa numero
                _s_name = dbc01.tempresa_prodserv_especificaciones.tipocaptura_texto.name + "_" + str(
                    _dbRow.tempresa_prodserv_especificaciones.id
                    )

            if (
                    _dbRow.tempresa_prodcategoria_especificaciones.capturaobligatoria
                    and not O_form.vars[_s_name]
                    ):
                O_form.errors[_s_name] = "Información requerida"
            else:
                # No hay problema
                pass

    else:
        # No debería entrar si no hay rows
        O_form.errors.id = "Error, no se identifico la operación de guardar. Contacte al administrador."
    return O_form


def _empresa_prodserv_especificaciones_accepted(O_form):
    """ Aceptaciones para las especificaciones de los productos

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_prodserv_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    """

    if D_stvSiteHelper.prodserv_especificaciones.dbRows:
        for _dbRow in D_stvSiteHelper.prodserv_especificaciones.dbRows:
            _s_texto = ""
            _n_float = 0.0
            _d_fecha = None
            _t_hora = None
            if (
                    _dbRow.tempresa_prodcategoria_especificaciones.tipocaptura in (
                        TEMPRESA_PRODCATEGORIA_ESPECIFICACIONES.TIPOCAPTURA.LIBRE,
                        TEMPRESA_PRODCATEGORIA_ESPECIFICACIONES.TIPOCAPTURA.OPCION,
                        TEMPRESA_PRODCATEGORIA_ESPECIFICACIONES.TIPOCAPTURA.MULTIOPCION
                        )
                    ):
                _s_name = dbc01.tempresa_prodserv_especificaciones.tipocaptura_texto.name + "_" + str(
                    _dbRow.tempresa_prodserv_especificaciones.id
                    )
                _s_texto = O_form.vars[_s_name]
            elif (
                    _dbRow.tempresa_prodcategoria_especificaciones.tipocaptura in (
                        TEMPRESA_PRODCATEGORIA_ESPECIFICACIONES.TIPOCAPTURA.NUMERO,
                        TEMPRESA_PRODCATEGORIA_ESPECIFICACIONES.TIPOCAPTURA.MONEDA,
                        TEMPRESA_PRODCATEGORIA_ESPECIFICACIONES.TIPOCAPTURA.FRACCION,
                        TEMPRESA_PRODCATEGORIA_ESPECIFICACIONES.TIPOCAPTURA.PORCENTAJE
                        )
                    ):
                _s_name = dbc01.tempresa_prodserv_especificaciones.tipocaptura_float.name + "_" + str(
                    _dbRow.tempresa_prodserv_especificaciones.id
                    )
                _n_float = O_form.vars[_s_name]
            elif (
                    _dbRow.tempresa_prodcategoria_especificaciones.tipocaptura
                    == TEMPRESA_PRODCATEGORIA_ESPECIFICACIONES.TIPOCAPTURA.FECHA
                    ):
                _s_name = dbc01.tempresa_prodserv_especificaciones.tipocaptura_fecha.name + "_" + str(
                    _dbRow.tempresa_prodserv_especificaciones.id
                    )
                _s_opciones = O_form.vars[_s_name]
            elif (
                    _dbRow.tempresa_prodcategoria_especificaciones.tipocaptura
                    == TEMPRESA_PRODCATEGORIA_ESPECIFICACIONES.TIPOCAPTURA.HORA
                    ):
                _s_name = dbc01.tempresa_prodserv_especificaciones.tipocaptura_hora.name + "_" + str(
                    _dbRow.tempresa_prodserv_especificaciones.id
                    )
                _d_fecha = O_form.vars[_s_name]
            elif (
                    _dbRow.tempresa_prodcategoria_especificaciones.tipocaptura
                    == TEMPRESA_PRODCATEGORIA_ESPECIFICACIONES.TIPOCAPTURA.HORA
                    ):
                _s_name = dbc01.tempresa_prodserv_especificaciones.tipocaptura_hora.name + "_" + str(
                    _dbRow.tempresa_prodserv_especificaciones.id
                    )
                _t_hora = O_form.vars[_s_name]
            else:
                # Por default se usa text
                _s_name = dbc01.tempresa_prodserv_especificaciones.tipocaptura_texto.name + "_" + str(
                    _dbRow.tempresa_prodserv_especificaciones.id
                    )
                _s_texto = O_form.vars[_s_name]

            dbc01(
                dbc01.tempresa_prodserv_especificaciones.id == _dbRow.tempresa_prodserv_especificaciones.id
                ).update(
                tipocaptura_texto = _s_texto,
                tipocaptura_float = _n_float,
                tipocaptura_fecha = _d_fecha,
                tipocaptura_hora = _t_hora,
                )

    else:
        # Error
        pass
    return O_form


def empresa_prodserv_tratamiento():
    """ Formulario para el detalle de tratamientos

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_prodserv_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    """

    _dbTable = dbc01.tempresa_prodserv_tratamientos

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_prodserv,
        xFieldLinkMaster = [
            _dbTable.empresa_prodserv_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.almacenamiento,
            _dbTable.uso,
            _dbTable.instalacion,
            _dbTable.transporte,
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:2]
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def empresa_prodserv_imagenes():
    """ Formulario para el detalle de Imagenes

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_prodserv_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    """

    _dbTable = dbc01.tempresa_prodserv_imagenes

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_prodserv,
        xFieldLinkMaster = [
            _dbTable.empresa_prodserv_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.imagen,
            _dbTable.orden
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:2]
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def empresa_prodserv_codigosproveedores():
    """ Formulario para el detalle de codgios proveedores

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_prodserv_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    """

    _dbTable = dbc01.tempresa_prodserv_codigosproveedores

    _s_empresa_prodserv_id = request.args(3, None)

    _dbRow_producto = dbc01.tempresa_prodserv(_s_empresa_prodserv_id)

    if _dbRow_producto.preciotipocalculo == TEMPRESA_PRODSERV.E_TIPOCALCULO.PRECIO_OFICIAL:
        _dbTable.empresa_proveedor_id.requires = IS_IN_DB(
            dbc01(
                dbc01.tempresa_proveedores.id == _dbRow_producto.empresa_proveedor_id
                ),
            'tempresa_proveedores.id',
            '%(razonsocial)s'
            )
    else:
        _dbTable.empresa_proveedor_id.requires = IS_IN_DB(
            dbc01,
            'tempresa_proveedores.id',
            '%(razonsocial)s'
            )

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_prodserv,
        xFieldLinkMaster = [
            _dbTable.empresa_prodserv_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.empresa_proveedor_id,
            _dbTable.codigo,
            _dbTable.contenido
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:2]
        )

    _O_cat.addRowContentEvent('onValidation', _empresa_prodserv_codigosproveedores_validation)

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def _empresa_prodserv_codigosproveedores_validation(O_form):
    """ Validación para las códigos de proveedores.
    """
    _dbTable = dbc01.tempresa_prodserv_codigosproveedores

    _s_producto = request.args(3, None)
    _s_proveedor = O_form.vars.empresa_proveedor_id
    _s_codigo = O_form.vars.codigo
    _s_contenido = O_form.vars.contenido
    _s_id = O_form.record_id

    _dbRows = dbc01(
        (_dbTable.empresa_prodserv_id == _s_producto)
        & (_dbTable.empresa_proveedor_id == _s_proveedor)
        & (_dbTable.codigo == _s_codigo)
        & (_dbTable.id != _s_id)  # Se ignora el registro que se esté modificando
        ).select(_dbTable.id)

    if _dbRows:
        O_form.errors.codigo = "El código no puede repetirse en el mismo producto."
    else:
        pass

    _dbRows = dbc01(
        (_dbTable.empresa_prodserv_id == _s_producto)
        & (_dbTable.empresa_proveedor_id == _s_proveedor)
        & (_dbTable.contenido == _s_contenido)
        & (_dbTable.id != _s_id)  # Se ignora el registro que se esté modificando
        ).select(_dbTable.id)

    if _dbRows:
        O_form.errors.contenido = "No puede duplicarse la combinación de contenido y proveedor por producto."
        O_form.errors.empresa_proveedor_id = "No puede duplicarse la combinación de contenido y proveedor por producto."
    else:
        pass

    return O_form


def empresa_embarques():
    """ Formulario para la creación de embarques
        
    @keyword arg0: master de empresa_id.
    @keyword arg1: empresa_id.
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.
    
    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    """

    _dbTable = dbc01.tempresa_embarques

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas,
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.fechainicio,
            _dbTable.tipoembarque,
            _dbTable.empresa_transporte_id,
            _dbTable.chofer,
            _dbTable.guiaembarque,
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    # TODO: Faltan validaciones
    _O_cat.addRowContentEvent('onValidation', TEMPRESA_EMBARQUES.VALIDACION)

    _D_returnVars = _O_cat.process()

    _O_cat.addSubTab(
        s_tabName = "Almacenes",
        s_url = URL(
            f = 'empresa_embarque_almacenes',
            args = request.args[:2] + ['master_' + str(request.function), request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_embarque_almacenes', s_masterIdentificator = str(request.function))
        )

    return _D_returnVars


# TODO: Crear la vista .json
def empresa_transportes_ver():
    """ Función ver para mostrar el transporte dependiendo el propietario y el tipo de embarque que se seleccione.
    """

    _dbTable = dbc01.tempresa_transportes

    _LD_data = []

    if len(request.args) > 0:

        _s_tipoembarque = request.args(0, None)

        if (
                int(_s_tipoembarque) in (
                    TEMPRESA_EMBARQUES.TIPO_EMBARQUE.PAQUETERIA,
                    TEMPRESA_EMBARQUES.TIPO_EMBARQUE.TRANSPORTISTA
                    )
                ):

            _rows_result = dbc01(
                _dbTable.propietario == TEMPRESA_TRANSPORTES.PROPIETARIO.PROVEEDOR
                ).select(
                _dbTable.id,
                _dbTable.empresa_proveedor_id,
                _dbTable.empresa_tipotransporte_id,
                _dbTable.identificador,
                _dbTable.marca_id,
                _dbTable.modelo,
                _dbTable.placas,
                )
            for _row in _rows_result:

                _LD_data.append(
                    {
                        'id': _row.id, 'optionname': (_dbTable._format(_row))
                        }
                    )

        else:

            _rows_result = dbc01(
                _dbTable.propietario == TEMPRESA_TRANSPORTES.PROPIETARIO.PROPIO
                ).select(
                _dbTable.id,
                _dbTable.empresa_proveedor_id,
                _dbTable.empresa_tipotransporte_id,
                _dbTable.identificador,
                _dbTable.marca_id,
                _dbTable.modelo,
                _dbTable.placas,
                )

            for _row in _rows_result:

                _LD_data.append(
                    {
                        'id': _row.id, 'optionname': (_dbTable._format(_row))
                        }
                    )

        return response.json({'data': _LD_data})
    else:
        return response.json({'data': [], 'error': 'Argumentos inválidos'})


def empresa_embarque_almacenes():
    """ Formulario para los almacenes a los que van los embarques
        
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_embarque_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """

    _dbTable = dbc01.tempresa_embarque_almacenes

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_embarques,
        xFieldLinkMaster = [
            _dbTable.empresa_embarque_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.empresa_plaza_sucursal_almacen_id,
            _dbTable.orden,
            _dbTable.estatus,
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:2]
        )

    # TODO: Faltan validaciones
    _O_cat.addRowContentEvent('onValidation', TEMPRESA_EMBARQUE_ALMACENES.VALIDACION)

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def empresa_grupos():
    """ Formulario para la creación de grupos
        
    @keyword arg0: master de empresa_id.
    @keyword arg1: empresa_id.
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.
    
    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    """

    _dbTable = dbc01.tempresa_grupos

    _s_action = request.args(2, request.stv_fwk_permissions.btn_none.code)

    _s_empresa_grupo_id = request.args(3, None)

    if _s_action in (request.stv_fwk_permissions.btn_edit.code, request.stv_fwk_permissions.btn_save.code):
        _s_empresa_grupo_id = request.args(3, None)

        if _s_empresa_grupo_id:
            # Si hay id es que está editando y no está entrado a grabar desde nuevo

            _dbRow_empresa_grupo = _dbTable(_s_empresa_grupo_id)

            # Si no tiene cliente o proveedor no hará nada
            if not _dbRow_empresa_grupo.cliente:
                pass
            else:
                # Si el campo está seleccionado entonces buscará si este grupo ya tiene clientes asociados

                _dbRow_clientes = dbc01(
                    (dbc01.tempresa_clientes.empresa_grupo_id == _s_empresa_grupo_id)
                    ).select(
                    dbc01.tempresa_clientes.id
                    )

                if _dbRow_clientes:
                    # Si tiene clientes asociados no se dejará editar el combo box.
                    _dbTable.cliente.writable = False
                else:
                    pass

            if not _dbRow_empresa_grupo.proveedor:
                pass
            else:
                # Si el campo está seleccionado entonces buscará si este grupo ya tiene clientes asociados

                _dbRow_proveedores = dbc01(
                    (dbc01.tempresa_proveedores.empresa_grupo_id == _s_empresa_grupo_id)
                    ).select(
                    dbc01.tempresa_proveedores.id
                    )
                if _dbRow_proveedores:
                    # Si tiene clientes asociados no se dejará editar el combo box.
                    _dbTable.proveedor.writable = False
                else:
                    pass

        else:
            pass

    else:
        pass

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas,
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.grupodescripcion,
            _dbTable.cliente,
            _dbTable.proveedor
            ],
        L_fieldsToSearch = [
            _dbTable.id,
            _dbTable.grupodescripcion
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _O_cat.addRowContentEvent('onValidation', _empresa_grupos_validation)

    if _s_action in (request.stv_fwk_permissions.btn_edit.code, request.stv_fwk_permissions.btn_view.code):
        _dbRow_grupo = _dbTable(_s_empresa_grupo_id)

        if _dbRow_grupo.cliente:
            _O_cat.addSubTab(
                s_tabName = "Clientes",
                s_url = URL(
                    f = 'empresa_grupo_clientes',
                    args = request.args[:2] + ['master_' + str(request.function), request.args(3)]
                    ),
                s_idHtml = fn_createIDTabUnique(
                    f = 'empresa_grupo_clientes', s_masterIdentificator = str(request.function)
                    )
                )
        else:
            pass

        if _dbRow_grupo.proveedor:
            _O_cat.addSubTab(
                s_tabName = "Proveedores",
                s_url = URL(
                    f = 'empresa_grupo_proveedores',
                    args = request.args[:2] + ['master_' + str(request.function), request.args(3)]
                    ),
                s_idHtml = fn_createIDTabUnique(
                    f = 'empresa_grupo_proveedores', s_masterIdentificator = str(request.function)
                    )
                )
        else:
            pass
    else:
        pass

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def _empresa_grupos_validation(O_form):
    """ Validación para grupos
        
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """

    _dbTable = dbc01.tempresa_grupos
    _s_empresa_id = request.args(1, None)
    _s_id = O_form.record_id

    _dbRows = dbc01(
        (_dbTable.empresa_id == _s_empresa_id)
        & (_dbTable.grupodescripcion == O_form.vars.grupodescripcion)
        & (_dbTable.id != _s_id)
        ).select(
        _dbTable.id
        )
    if not O_form.vars.grupodescripcion:
        O_form.errors.grupodescripcion = 'La descripción del grupo no puede estar vacía'
    else:
        pass

    if _dbRows:
        O_form.errors.grupodescripcion = 'La descripción del grupo no puede ser duplicada.'
    else:
        pass

    if not _s_id:
        if not O_form.vars.cliente and not O_form.vars.proveedor:
            O_form.errors.proveedor = 'Se debe de seleccionar un tipo de grupo .'
            O_form.errors.cliente = 'Se debe de seleccionar un tipo de grupo .'
        else:
            pass
    else:
        pass

    return O_form


def empresa_grupo_clientes():
    """ Formulario para los grupos de clientes
        
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestre]
        arg3: empresa_grupo_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """

    _dbTable = dbc01.tempresa_clientes

    _s_empresa_grupo_id = request.args(3, None)

    _qry = (
        (_dbTable.empresa_grupo_id == _s_empresa_grupo_id)
        )

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        dbTableMaster = None,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_findall,
            request.stv_fwk_permissions.btn_cancel,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.cliente_codigo_contpaqi,
            _dbTable.razonsocial,
            _dbTable.rfc,
            _dbTable.nombrecorto,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:4]
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def empresa_grupo_proveedores():
    """ Formulario para los grupos de proveedores
        
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestre]
        arg3: empresa_grupo_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """

    _dbTable = dbc01.tempresa_proveedores

    _s_empresa_grupo_id = request.args(3, None)

    _qry = (
        (_dbTable.empresa_grupo_id == _s_empresa_grupo_id)
        )

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        dbTableMaster = None,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_findall,
            request.stv_fwk_permissions.btn_cancel,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.codigo,
            _dbTable.razonsocial,
            _dbTable.rfc,
            _dbTable.nombrecorto,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:4]
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def empresa_clasificacionesproveedores():
    """Formulario para la creación de clasificaciones para proveedores.
        
    @keyword arg0: master de empresa_id.
    @keyword arg1: empresa_id.
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.
    
    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    """

    _dbTable = dbc01.tempresa_clasificacionesproveedores

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas,
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.clasificacionproveedor,
            ],
        L_fieldsToSearch = [
            _dbTable.id,
            _dbTable.clasificacionproveedor
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _O_cat.addRowContentEvent('onValidation', _empresa_clasificacionesproveedores_validation)

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def _empresa_clasificacionesproveedores_validation(O_form):
    """ Validación para clasificaciones de proveedores
        
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
        
    """

    _dbTable = dbc01.tempresa_clasificacionesproveedores
    _s_empresa_id = request.args(1, None)
    _s_id = O_form.record_id

    _dbRows = dbc01(
        (_dbTable.empresa_id == _s_empresa_id)
        & (_dbTable.clasificacionproveedor == O_form.vars.clasificacionproveedor)
        & (_dbTable.id != _s_id)
        ).select(
        _dbTable.id
        )
    if _dbRows:
        O_form.errors.clasificacionproveedor = (
            'La clasificación del proveedor no se puede duplicar, favor de introducir una diferente.'
            )
    else:
        pass

    return O_form


def empresa_clasificacionesclientes():
    """ Formulario para la creación de clasificaciones para clientes.
        
    @keyword arg0: master de empresa_id.
    @keyword arg1: empresa_id.
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.
    
    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    """

    _dbTable = dbc01.tempresa_clasificacionesclientes

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas,
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.clasificacioncliente,
            ],
        L_fieldsToSearch = [
            _dbTable.id,
            _dbTable.clasificacioncliente
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _O_cat.addRowContentEvent('onValidation', _empresa_clasificacionesclientes_validation)

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def _empresa_clasificacionesclientes_validation(O_form):
    """ Validación para clasificaciones de clientes
        
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    
    """

    _dbTable = dbc01.tempresa_clasificacionesclientes
    _s_empresa_id = request.args(1, None)
    _s_id = O_form.record_id

    _dbRows = dbc01(
        (_dbTable.empresa_id == _s_empresa_id)
        & (_dbTable.clasificacioncliente == O_form.vars.clasificacioncliente)
        & (_dbTable.id != _s_id)
        ).select(
        _dbTable.id
        )
    if _dbRows:
        O_form.errors.clasificacioncliente = (
            'La clasificación del cliente no se puede duplicar, favor de introducir una diferente.'
            )
    else:
        pass

    return O_form


def empresa_empleadosconfiguracion():
    """Detalle de Empresa para manejar lo relativo a los ingresos.

    @descripcion Empleados Configuración

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.    
    
    @permiso btn_none
    #@permiso btn_new
    #@permiso btn_save
    #@permiso btn_cancel
    #@permiso btn_edit
    #@permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    #@permiso btn_find
    #@permiso btn_findall
    #@permiso btn_signature
    
    """

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbTable = None,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [],
        L_fieldsToSearch = [],
        L_fieldsToSearchIfDigit = [],
        x_offsetInArgs = request.args[:2]
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_empleados,
        s_url = URL(f = 'empresa_empleados', args = ['master_' + str(request.function), request.args(1)]),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_empleados', s_masterIdentificator = str(request.function))
        )

    _D_returnVars = _O_cat.process(D_overrideView = request.stv_fwk_permissions.view_viewRegister)

    return dict(_D_returnVars)


def empresa_empleados():
    """ Detalle de empleados correspondientes a la empresa
    
    @descripcion Empleados
    
    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.
    
    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    """

    _dbTable = dbc01.tempresa_empleados

    _dbTable.bansuspender.label = "Estatus"

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas,
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.nombre,
            _dbTable.apellidopaterno,
            _dbTable.apellidomaterno,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.nombre,
            _dbTable.apellidopaterno,
            _dbTable.apellidomaterno
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        )

    _D_returnVars = _O_cat.process()

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_empleado_cuentas,
        s_url = URL(
            f = 'empresa_empleado_cuentas',
            args = request.args[:2] + ['master_' + str(request.function), request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_empleado_cuentas', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_empleado_guiascontables,
        s_url = URL(
            f = 'empresa_empleado_guiascontables',
            args = request.args[:2] + ['master_' + str(request.function), request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'empresa_empleado_guiascontables', s_masterIdentificator = str(request.function)
            )
        )

    return _D_returnVars


def empresa_empleado_cuentas():
    """ Detalle de cuentas correspondientes al empleado
    
    @description Cuentas
    
    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Empleado
    @keyword arg3: empresa_id.
    @keyword [arg4]: operación a realizar en el formulario.
    @keyword [arg5]: id en caso de existir relación a un registro en la operación.
    
    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    """

    _dbTable = dbc01.tempresa_empleado_cuentas

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_empleados,
        xFieldLinkMaster = [
            _dbTable.empresa_empleado_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.empresa_bancocontpaqi_id,
            _dbTable.numerocuenta,
            _dbTable.clabe,
            _dbTable.numerotarjeta,
            _dbTable.monedacontpaqi_id
            ],
        L_fieldsToSearch = [
            _dbTable.empresa_bancocontpaqi_id,
            _dbTable.numerocuenta,
            _dbTable.clabe,
            _dbTable.numerotarjeta,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        x_offsetInArgs = request.args[:2]
        )

    _O_cat.addRowContentEvent('onValidation', _empresa_empleado_cuentas_validation)

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def _empresa_empleado_cuentas_validation(O_form):
    """ Validación para cuentas correspondientes al empleado
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_empleado_id
        [arg4]: operación a realizar en el formulario
        [arg5]: id en caso de existir relación a un registro en la operación
        
    """

    _s_id = O_form.record_id

    if (not O_form.vars.numerocuenta) and (not O_form.vars.clabe):
        O_form.errors.numerocuenta = "Se tiene que capturar número de cuenta o clabe o ambos para poder guardar."
        O_form.errors.clabe = "Se tiene que capturar número de cuenta o clabe o ambos para poder guardar."
    else:
        pass

    return O_form


def empresa_recursoscomprobar():
    """ Detalle de recursos por comprobar correspondientes a la empresa

    @descripcion Recursos por Comprobar

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.    
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.
    
    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
                
    """

    _dbTable = dbc01.tempresa_recursoscomprobar

    _s_empresa_id = request.args(1, None)

    _s_action = request.args(2, request.stv_fwk_permissions.btn_none.code)

    _s_recursocomprobar_id = request.args(3, None)

    _dbTable.formapago_id.requires = IS_IN_DB(
        db01(
            db01.tformaspago.id.belongs(
                TFORMASPAGO.EFECTIVO,
                TFORMASPAGO.CHEQUE,
                TFORMASPAGO.TRANSFERENCIA
                )
            ),
        'tformaspago.id',
        db01.tformaspago._format
        )

    _dbTable.empresa_empleado_id.requires = IS_IN_DB(
        dbc01(
            (dbc01.tempresa_empleados.empresa_id == _s_empresa_id)
            & (dbc01.tempresa_empleados.bansuspender == False)
            ),
        'tempresa_empleados.id',
        dbc01.tempresa_empleados._format
        )

    if _s_action == request.stv_fwk_permissions.btn_save.code:

        if int(request.vars.formapago_id) in (TFORMASPAGO.TRANSFERENCIA, TFORMASPAGO.CHEQUE):
            # Si la forma de pago es transferencia o cheque entonces hace esta lógica. 

            if _s_recursocomprobar_id:
                # Si se está editando entonces se hará una consulta a ese registro. 

                _dbRowRecursocomprobar = _dbTable(_s_recursocomprobar_id)

                if _dbRowRecursocomprobar.empresa_cuentabancaria_id != request.vars.empresa_cuentabancaria_id:
                    # Si la cuenta bancaria es diferente a la que tiene entonces se consulta la nueva cuentabancaria. 

                    _dbRowCuentabancaria = dbc01.tempresa_cuentasbancarias(request.vars.empresa_cuentabancaria_id)

                    if _dbRowCuentabancaria.monedacontpaqi_id != request.vars.monedacontpaqi_id:
                        # Si la moneda no coincide entonces se procede a updatear el row. 

                        _dbTable.monedacontpaqi_id.writable = False
                        _dbRowRecursocomprobar.monedacontpaqi_id = _dbRowCuentabancaria.monedacontpaqi_id
                        _dbRowRecursocomprobar.update_record()
                    else:
                        # De lo contrario no hay bronca, la moneda es igual. 
                        pass

                else:
                    # De lo contraio no hay bronca, la cuenta es igual se conserva la misma moneda. 
                    pass

            else:
                # De lo contrario es nuevo y se tiene que dar de alta la moneda de la cuenta bancaria en el registro. 
                _dbTable.monedacontpaqi_id.writable = False
                _dbRowCuentabancaria = dbc01.tempresa_cuentasbancarias(request.vars.empresa_cuentabancaria_id)

                _dbTable.monedacontpaqi_id.default = _dbRowCuentabancaria.monedacontpaqi_id
        else:
            # De lo contrario la moneda se da de alta en el formulario. 
            pass

    else:
        pass

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas,
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.fecha,
            _dbTable.concepto,
            _dbTable.importe,
            _dbTable.formapago_id,
            _dbTable.monedacontpaqi_id,
            _dbTable.estatus
            ],
        L_fieldsToSearch = [
            _dbTable.concepto,
            _dbTable.importe,
            _dbTable.formapago_id,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        )

    _O_cat.addRowContentEvent('onValidation', _empresa_recursoscomprobar_validation)

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def _empresa_recursoscomprobar_validation(O_form):
    """ Validación para recursos por comprobar correspondientes a la empresa
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: operación a realizar en el formulario
        [arg3]: id en caso de existir relación a un registro en la operación
        
    """

    _s_id = O_form.record_id

    if O_form.vars.importe <= 0:
        O_form.errors.importe = "El importe no puede ser menor o igual a 0"
    else:
        pass

    if int(O_form.vars.formapago_id) == TFORMASPAGO.TRANSFERENCIA:

        if O_form.vars.empresa_empleado_cuenta_id:
            pass
        else:
            O_form.errors.empresa_empleado_cuenta_id = (
                "Si la forma de entrega es Transferencia la cuenta del empleado no puede ir vacía."
                )

        if O_form.vars.empresa_cuentabancaria_id:
            pass
        else:
            O_form.errors.empresa_cuentabancaria_id = (
                "Si la forma de entrega es Transferencia la cuenta bancaria de la empresa no puede ir vacía."
                )

        if O_form.vars.referencia:
            pass
        else:
            O_form.errors.referencia = "Si la forma de entrega es Transferencia la referencia no puede ir vacía."

        if O_form.vars.claverastero:
            pass
        else:
            O_form.errors.claverastero = (
                "Si la forma de entrega es Transferencia la clave de rastreo no puede ir vacía."
                )

    elif int(O_form.vars.formapago_id) == TFORMASPAGO.EFECTIVO:

        if O_form.vars.monedacontpaqi_id:
            pass
        else:
            O_form.errors.monedacontpaqi_id = "Si la forma de entrega es  Efectivo la moneda no puede ir vacía."

        if O_form.vars.empresa_plaza_sucursal_cajachica_id:
            pass
        else:
            O_form.errors.empresa_plaza_sucursal_cajachica_id = (
                "Si la forma de entrega es Efectivo la caja chica no puede ir vacía."
                )

    elif int(O_form.vars.formapago_id) == TFORMASPAGO.CHEQUE:

        if O_form.vars.empresa_cuentabancaria_id:
            pass
        else:
            O_form.errors.empresa_cuentabancaria_id = (
                "Si la forma de entrega es Cheque la cuenta bancaria de la empresa no puede ir vacía."
                )

        if O_form.vars.cheque:
            pass
        else:
            O_form.errors.cheque = "Si la forma de entrega es Cheque el cheque no puede ir vacío."
    else:

        O_form.errors.formapago_id = "Forma de entrega no considerada, favor de hablar a un administrador."

    return O_form


def empresa_empleado_guiascontables():
    """ Detalle de guías contables correspondientes a los empleados

    @descripcion Guías Contables

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Empleaso
    @keyword arg3: empleado_id.
    @keyword [arg4]: operación a realizar en el formulario.
    @keyword [arg5]: id en caso de existir relación a un registro en la operación.
    
    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
                
    """

    _dbTable = dbc01.tempresa_empleado_guiascontables

    _s_empresa_id = request.args(1, None)

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_empleados,
        xFieldLinkMaster = [
            _dbTable.empresa_empleado_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.ejercicio,
            _dbTable.cuentacontable,
            _dbTable.monedacontpaqi_id,
            _dbTable.cuentacontable_complemento
            ],
        L_fieldsToSearch = [
            _dbTable.cuentacontable,
            _dbTable.cuentacontable_complemento
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            _dbTable.ejercicio
            ],
        x_offsetInArgs = request.args[:2]
        )

    _dbRow = dbc01.tempresas(_s_empresa_id)
    _O_cat.addRowContentProperty('formHidden', {'moneda_id_empresa': str(_dbRow.monedacontpaqi_id)})

    _O_cat.addRowContentEvent('onValidation', _empresa_empleado_guiascontables_validation)

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def _empresa_empleado_guiascontables_validation(O_form):
    """ Validación para las guías contables correspondientes a los empleados 
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: operación a realizar en el formulario
        [arg3]: id en caso de existir relación a un registro en la operación
        
    """

    _dbTable = dbc01.tempresa_empleado_guiascontables

    _s_empresa_id = request.args(1, None)

    _s_empresa_empleado_id = request.args(3, None)

    _dbRows = dbc01(
        (_dbTable.empresa_empleado_id == _s_empresa_empleado_id)
        & (_dbTable.ejercicio == O_form.vars.ejercicio)
        & (_dbTable.id != O_form.record_id)  # Se ignora el registro que se esté modificando
        ).select(_dbTable.id)

    if _dbRows:
        O_form.errors.id = "El ejercicio no puede duplicarse."
    else:
        pass

    _s_msgError = VALIDACIONES.EJERCICIO.ES_VALIDO(_s_empresa_id, O_form.vars.ejercicio)

    if _s_msgError:
        O_form.errors.id = _s_msgError
    else:
        pass

    return O_form


def empresa_fondosfijoscaja():
    """ Detalle de fondos fijos de caja correspondientes a la empresa
    
    @descripcion Fondos Fijos Caja

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.
    
    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
        
    """

    _dbTable = dbc01.tempresa_fondosfijoscaja

    _s_empresa_id = request.args(1, None)

    _s_action = request.args(2, request.stv_fwk_permissions.btn_none.code)

    _dbTable.empresa_empleado_id.requires = IS_IN_DB(
        dbc01(
            (dbc01.tempresa_empleados.empresa_id == _s_empresa_id)
            & (dbc01.tempresa_empleados.bansuspender == False)
            ),
        'tempresa_empleados.id',
        dbc01.tempresa_empleados._format
        )

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas,
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.ejercicio,
            _dbTable.empresa_empleado_id,
            _dbTable.importe,
            _dbTable.monedacontpaqi_id,
            _dbTable.cuentacontable
            ],
        L_fieldsToSearch = [
            _dbTable.importe,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            ],
        )

    _dbRow = dbc01.tempresas(_s_empresa_id)
    _O_cat.addRowContentProperty('formHidden', {'moneda_id_empresa': str(_dbRow.monedacontpaqi_id)})

    _O_cat.addRowContentEvent('onValidation', _empresa_fondosfijoscaja_validation)

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def _empresa_fondosfijoscaja_validation(O_form):
    """ Validación para los fondos fijos de caja correspondientes a la empresa
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: operación a realizar en el formulario
        [arg3]: id en caso de existir relación a un registro en la operación
        
    """

    _dbTable = dbc01.tempresa_fondosfijoscaja

    _s_empresa_id = request.args(1, None)

    _dbRows = dbc01(
        (_dbTable.empresa_id == _s_empresa_id)
        & (_dbTable.ejercicio == O_form.vars.ejercicio)
        & (_dbTable.empresa_empleado_id == O_form.vars.empresa_empleado_id)
        & (_dbTable.id != O_form.record_id)  # Se ignora el registro que se esté modificando
        ).select(_dbTable.id)

    if _dbRows:
        O_form.errors.ejercicio = "La combinación de empresa y ejercicio no puede duplicarse."
        O_form.errors.empresa_empleado_id = "La combinación de empresa y ejercicio no puede duplicarse."
    else:
        pass

    _dbRowEmpresa = dbc01.tempresas(_s_empresa_id)
    if (
            (int(O_form.vars.monedacontpaqi_id) != _dbRowEmpresa.monedacontpaqi_id)
            and not O_form.vars.cuentacontable_complementaria
            ):
        O_form.errors.cuentacontable_complementaria = (
            "La cuenta complementaria no puede ir vacía si se usa una moneda diferente a la de la empresa."
            )
    else:
        pass

    if O_form.vars.importe <= 0:
        O_form.errors.importe = "El importe no puede ser menor o igual a 0"
    else:
        pass

    if O_form.vars.empresa_empleado_id:
        pass
    else:
        O_form.errors.empresa_empleado_id = "El empleado no puede estar vacío."

    _s_msgError = VALIDACIONES.EJERCICIO.ES_VALIDO(_s_empresa_id, O_form.vars.ejercicio)

    if _s_msgError:
        O_form.errors.id = _s_msgError
    else:
        pass

    return O_form


def empresa_empleado_cuentas_ver():
    """ Función para visualizar las cuentas del empleado.
    
    Args:
        arg0: empresa_empleado_id
    
    """

    _dbTable = dbc01.tempresa_empleado_cuentas

    _LD_data = []

    if len(request.args) > 0:

        _s_empresa_empleado_id = request.args(0, None)

        _rows_result = dbc01(
            (_dbTable.empresa_empleado_id == _s_empresa_empleado_id)
            ).select(
            _dbTable.ALL
            )

        for _row in _rows_result:

            _LD_data.append(
                {
                    'id': _row.id, 'optionname': (_dbTable._format(_row))
                    }
                )

        return response.json({'data': _LD_data})
    else:
        return response.json({'data': [], 'error': 'Argumentos inválidos'})


def empresa_productosajustes():
    """ Formulario para los ajustes de los productos por Empresa.
    
    @descripcion Ajustes de Productos

    @keyword arg0: Empresa
    @keyword arg1: empresa_id
    @keyword [arg2]: codigo de funcionalidad
    @keyword [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    
    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature

    """

    _dbTable = dbc01.tempresa_productosajustes

    _s_empresa_id = request.args(1, None)

    _s_action = request.args(2, request.stv_fwk_permissions.btn_none.code)

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas,
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.marca_id,
            _dbTable.tipoajuste,
            ],
        L_fieldsToSearch = [
            _dbTable.tipoajuste,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            _dbTable.marca_id
            ],
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_productoajuste_porcentajes,
        s_url = URL(
            f = 'empresa_productoajuste_porcentajes',
            args = request.args[:2] + ['master_' + str(request.function), request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'empresa_productoajuste_porcentajes', s_masterIdentificator = str(request.function)
            )
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def empresa_productoajuste_porcentajes():
    """ Formulario para los ajustes de los productos. 

    @descripcion Procentajes

    @keyword arg0: Empresa
    @keyword arg1: empresa_id
    @keyword arg2: Ajuste Producto
    @keyword arg3: empresa_productoajuste_id
    @keyword [arg4]: codigo de funcionalidad
    @keyword [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    
    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature

    """

    _dbTable = dbc01.tempresa_productoajuste_porcentajes

    _s_empresa_id = request.args(1, None)

    _s_action = request.args(2, request.stv_fwk_permissions.btn_none.code)

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_productosajustes,
        xFieldLinkMaster = [
            _dbTable.empresa_productoajuste_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.mesuso_inicial,
            _dbTable.mesuso_final,
            _dbTable.ajusteporcentaje,
            ],
        L_fieldsToSearch = [
            _dbTable.mesuso_inicial,
            _dbTable.mesuso_final,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            _dbTable.mesuso_inicial,
            _dbTable.mesuso_final,
            ],
        x_offsetInArgs = request.args[:2]
        )

    _O_cat.addRowContentEvent('onValidation', _empresa_productoajuste_porcentajes_validation)

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def _empresa_productoajuste_porcentajes_validation(O_form):
    """ Validación para las guías contables de las cuentas bancarias.
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_productoajuste_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """

    _dbTable = dbc01.tempresa_productoajuste_porcentajes

    _s_empresa_id = request.args(1, None)
    _s_empresa_productoajuste_id = request.args(3, None)
    _s_id = O_form.record_id

    if ((O_form.vars.ajusteporcentaje <= 0) or (O_form.vars.ajusteporcentaje > 100) or (
            O_form.vars.ajusteporcentaje == '')):
        O_form.errors.ajusteporcentaje = (
            "El ajuste de porcentaje no puede estar vacío, ser menor o igual a cero o mayor que 100"
            )
    else:
        pass

    if (O_form.vars.mesuso_inicial < 0) or (O_form.vars.mesuso_inicial == ''):
        O_form.errors.mesuso_inicial = "Meses uso incial no puede ser menor o igual a 0 o vacío"
    else:
        pass

    if (O_form.vars.mesuso_final < 0) or (O_form.vars.mesuso_final == ''):
        O_form.errors.mesuso_final = "Meses uso final no puede ser menor o igual a 0 o vacío"
    else:
        pass

    if O_form.vars.mesuso_final < O_form.vars.mesuso_inicial:
        O_form.errors.mesuso_inicial = "El mes inicial no puede ser mayor al final."
    else:
        _dbRowProductoAjustePorcentaje = dbc01(
            _dbTable.empresa_productoajuste_id == _s_empresa_productoajuste_id
            ).select(
            _dbTable.ALL,
            orderby = _dbTable.id
            ).last()

        if not _dbRowProductoAjustePorcentaje:
            pass
        else:
            if _dbRowProductoAjustePorcentaje.mesuso_final >= O_form.vars.mesuso_inicial:
                O_form.errors.mesuso_inicial = (
                    "El mes inicial no puede ser menor o igual al mes final de un rango anterior."
                    )
            else:
                pass

    return O_form


def empresa_productosajustes_ver():
    """ Función para las salidas por traspaso de almacén
        Args:
            arg0: embarque id

    """
    _dbTable = dbc01.tempresa_productosajustes

    _s_cuentaaplicacion_id = request.vars.cfg_cuenta_aplicacion_id

    _dbRow_cuentaaplicacion = db.tcuenta_aplicaciones(_s_cuentaaplicacion_id)

    _LD_data = []

    if len(request.args) > 0:

        _s_marca_id = request.args(0, None)

        _rows_result = dbc01(
            (_dbTable.empresa_id == _dbRow_cuentaaplicacion.empresa_id)
            & (_dbTable.marca_id == _s_marca_id)
            ).select(
            _dbTable.ALL,
            )

        for _row in _rows_result:

            _LD_data.append(
                {
                    'id': _row.id, 'optionname': (_dbTable._format % _row.as_dict())
                    }
                )

        return response.json({'data': _LD_data})
    else:
        return response.json({'data': [], 'error': 'Argumentos inválidos'})


def imagen():
    return response.download(
        request, dbc01, attachment = request.vars.get('display', False) != "yes",
        download_filename = request.vars.get('filename', None)
        )


def imagenpin():
    return response.download(
        request, dbc01, attachment = request.vars.get('display', False) != "yes",
        download_filename = request.vars.get('filename', None)
        )
