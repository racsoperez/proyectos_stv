# -*- coding: utf-8 -*-

def empresa_estadoscuenta():
    """ Formulario para el detalle de Estados de cuenta por empresa

    @descripcion Estados de Cuenta

    @keyword arg0: Empresa
    @keyword arg1: empresa_id
    @keyword [arg2]: codigo de funcionalidad
    @keyword [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    
    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature

    """

    def al_validar(
            O_form,
            O_EdoCta
            ):

        _dbRows = dbc01(
            (_dbTable.empresa_cuentabancaria_id == O_form.vars.empresa_cuentabancaria_id)
            & (_dbTable.fecha == O_form.vars.fecha)
            & (_dbTable.id != O_form.record_id)  # Se ignora el registro que se esté modificando
            ).select(
                _dbTable.id
                )

        if _dbRows:
            O_form.errors.fecha = "La fecha y cuenta bancaria no puede duplicarse"
        elif not O_form.vars.fecha:
            O_form.errors.fecha = "La fecha es requerida"
        elif isinstance(O_form.vars.estadocuenta, str):
            O_form.errors.estadocuenta = "Estado de cuenta es requerido"
        else:

            from shutil import copyfile
            import tempfile
            _s_nombreArchivo = os.path.basename(O_form.vars.estadocuenta.filename.replace("\\", "/"))
            _s_pathArchivoTemporal = os.path.join(tempfile.gettempdir(), _s_nombreArchivo)
            copyfile(O_form.vars.estadocuenta.fp.name, _s_pathArchivoTemporal)

            O_EdoCta.configurar(
                s_path = _s_pathArchivoTemporal,
                n_estadocuenta_id = None,
                s_cuentabancaria_id = O_form.vars.empresa_cuentabancaria_id,
                )

            _D_result = O_EdoCta.validaciones()

            if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
                O_form.errors.empresa_cuentabancaria_id = _D_result.s_msgError

            else:

                if not O_form.record_id:
                    dbc01.tempresa_estadoscuenta.fecha_inicio.default = O_EdoCta.dt_inicio
                    dbc01.tempresa_estadoscuenta.fecha_fin.default = O_EdoCta.dt_fin
                else:
                    O_form.errors.id = "No se permite modificar el registro principal del Estado de Cuenta"

                pass

            pass

        return O_form

    def al_aceptar(
            O_form,
            O_EdoCta
            ):

        if O_EdoCta:

            _D_result = O_EdoCta.graba_movimientos(n_estadocuenta_id = O_form.vars.id)
            if _D_result.E_return <= CLASS_e_RETURN.OK_WARNING:
                _D_result = O_EdoCta.concilia_depositos_pendientes(
                    b_usarreferencia = request.vars.usar_referencia,
                    b_usarfecha = request.vars.usar_fecha
                    )

            else:
                pass

            if _D_result.s_msgError:
                stv_flashWarning(
                    'Estado de Cuenta',
                    "Se reportan problemas durante la importación: %s" % _D_result.s_msgError,
                    s_mode = 'stv_flash',
                    D_addProperties = {}
                    )
            else:
                stv_flashInfo(
                    'Estado de Cuenta',
                    "Movimientos conciliados: %d" % (_D_result.n_movimientosCoinciliados or 0),
                    s_mode = 'stv_flash',
                    D_addProperties = {}
                    )

                pass

        else:
            stv_flashError(
                'Estado de Cuenta',
                "No se pudo procesar estado de cuenta. ",
                s_mode = 'stv_flash',
                D_addProperties = {}
                )

        return O_form

    def antes_borrar(
            L_errores,
            s_empresa_estadocuenta_id
            ):

        _dbRows_edocta_movtos_conciliaciones = dbc01(
            (dbc01.tempresa_estadocuenta_movimientos.empresa_estadocuenta_id == s_empresa_estadocuenta_id)
            & (dbc01.tempresa_estadocuenta_conciliacionesbancarias.empresa_estadocuenta_movimiento_id == dbc01.tempresa_estadocuenta_movimientos.id)
            ).select(
                dbc01.tempresa_estadocuenta_conciliacionesbancarias.registropago_id
                )

        for _dbRow in _dbRows_edocta_movtos_conciliaciones:
            dbc01(
                dbc01.tcfdi_registropagos.id == _dbRow.registropago_id
                ).update(
                    estatus = TCFDI_REGISTROPAGOS.ESTATUS.PENDIENTE
                    )

        return L_errores

    _dbTable = dbc01.tempresa_estadoscuenta
    _s_empresa_id = request.args(1, None)
    _s_action = request.args(2, None)
    _s_empresa_estadocuenta_id = request.args(3, None)

    _O_EdoCta = EDOCTA_BANORTE()

    _dbTable.empresa_cuentabancaria_id.requires = IS_IN_DB(
        dbc01(
            (dbc01.tempresa_cuentasbancarias.empresa_id == _s_empresa_id)
            ),
        'tempresa_cuentasbancarias.id',
        dbc01.tempresa_cuentasbancarias._format
        )
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_new,
            request.stv_fwk_permissions.btn_save,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_remove,
            request.stv_fwk_permissions.btn_find,
            request.stv_fwk_permissions.btn_findall,
            request.stv_fwk_permissions.btn_signature,
            ], 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.fecha,
            _dbTable.empresa_cuentabancaria_id,
            _dbTable.tipo_conciliacion,
            _dbTable.estatus
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:2]       
        )

    _O_cat.addRowContentProperty(
        "formControlUpload", [
            Storage(
                fieldUpload = _dbTable.estadocuenta,
                fieldUploadFilename = _dbTable.estadocuentanombrearchivo
                )
            ]
        )

    _O_cat.addSubTab(
        s_tabName = "Movimientos Bancarios",
        s_url = URL(
            f = 'empresa_estadocuenta_movimientos',
            args = request.args[:2] + ['master_' + str(request.function), request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'empresa_estadocuenta_movimientos', s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        s_tabName = "Conciliación Bancaria",
        s_url = URL(
            f = 'empresa_estadocuenta_conciliacionbancaria_manual',
            args = request.args[:2] + ['master_' + str(request.function), request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'empresa_estadocuenta_conciliacionbancaria_manual', s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionLabel = "Conciliar estado de cuenta",
        s_permissionIcon = "fa fa-compass",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
            ),
        L_options = []
        )
    
    _D_overrideView = None

    _O_cat.addRowContentEvent('onValidation', al_validar, O_EdoCta = _O_EdoCta)
    _O_cat.addRowContentEvent('onAccepted', al_aceptar, O_EdoCta = _O_EdoCta)
    _O_cat.addRowContentEvent('beforeRemove', antes_borrar, s_empresa_estadocuenta_id = _s_empresa_estadocuenta_id)
    # _O_cat.addRowContentEvent('afterRemove', despues_borrar)

    _D_returnVars = _O_cat.process(D_overrideView = _D_overrideView)

    _D_returnVars.field_usarReferencia = Field(
        'usar_referencia', 'boolean', default = request.vars.get("usar_referencia", 0),
        required = False,
        notnull = False,
        widget = stv_widget_inputCheckbox, label = 'Usar referencia para conciliar', comment = None,
        writable = True, readable = True,
        represent = stv_represent_boolean,
        )

    _D_returnVars.field_usarFecha = Field(
        'usar_fecha', 'boolean', default = request.vars.get("usar_fecha", 1),
        required = False,
        notnull = False,
        widget = stv_widget_inputCheckbox, label = 'Usar fecha para conciliar', comment = None,
        writable = True, readable = True,
        represent = stv_represent_boolean,
        )

    if _s_action in (request.stv_fwk_permissions.btn_view.code,):
        _D_returnVars.n_pendientes = dbc01(
            (dbc01.tempresa_estadocuenta_movimientos.empresa_estadocuenta_id == _s_empresa_estadocuenta_id)
            & (dbc01.tempresa_estadocuenta_movimientos.estatus == TEMPRESA_ESTADOCUENTA_MOVIMIENTOS.ESTATUS.PENDIENTE)
            ).count()

        _D_returnVars.n_totales = dbc01(
            (dbc01.tempresa_estadocuenta_movimientos.empresa_estadocuenta_id == _s_empresa_estadocuenta_id)
            ).count()

        _D_returnVars.field_pendientes = Field(
            'pendientes', 'integer', default = _D_returnVars.n_pendientes,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Pendientes', comment = None,
            writable = False, readable = True,
            represent = stv_represent_number
            )
        _D_returnVars.field_totales = Field(
            'totales', 'integer', default = _D_returnVars.n_totales,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = 'Total', comment = None,
            writable = False, readable = True,
            represent = stv_represent_number
            )
        _D_returnVars.field_avance = Field(
            'avance', 'decimal(10,2)',
            required = False, default = round((1 - (float(_D_returnVars.n_pendientes) / _D_returnVars.n_totales)) * 100.0, 2),
            notnull = False, unique = False,
            widget = stv_widget_inputPercentage, label = '% avance', comment = None,
            writable = False, readable = True,
            represent = stv_represent_percentage
            )
    else:
        pass

    return _D_returnVars


def empresa_estadocuenta_movimientos():
    """ Formulario para el detalle de los movimientos del estado de cuenta
    
    @descripcion Movimientos

    @keyword arg0: Empresa
    @keyword arg1: empresa_id
    @keyword arg2: Estado de Cuenta
    @keyword arg3: empresa_estadocuenta_id
    @keyword [arg4]: codigo de funcionalidad
    @keyword [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    
    @permiso btn_none
    #@permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    #@permiso btn_signature
    @permiso special_permisson_01:[fa fa-ban] Limpiar Conciliación de Movimiento
    
    """

    _D_return = Storage(
        E_return = CLASS_e_RETURN.OK,
        s_msgError = "",
        dbTable = dbc01.tempresa_estadocuenta_movimientos
        )

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 4,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_edocta_id = request.args(3, 0),
        )

    _dbTable = dbc01.tempresa_estadocuenta_movimientos
        
    _s_empresa_cuentabancaria_estadocuenta_id = request.args(3, None)
    _s_action = request.args(4, request.stv_fwk_permissions.btn_none.code)
    _s_banco_movto_id = request.args(5, None)
    _D_results = TEMPRESA_ESTADOCUENTA_MOVIMIENTOS.BUSQUEDA_AVANZADA.GENERA_PREFILTRO(_D_args)
    _qry = _D_results.qry

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_estadoscuenta, 
        xFieldLinkMaster = [
            _dbTable.empresa_estadocuenta_id
            ], 
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_save,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_edit,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_find,
            request.stv_fwk_permissions.btn_findall,
            request.stv_fwk_permissions.special_permisson_01
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.fecha_operacion,
            _dbTable.abono,
            _dbTable.cargo,
            _dbTable.bancotipomovimiento_id,
            # dbc01.tcfdi_registropagos.cfdi_id,
            # dbc01.tcfdi_registropagos.monto,
            # dbc01.tcfdis.receptorcliente_id,
            _dbTable.referencia,
            _dbTable.estatus,
            _dbTable.descripcion,
            _dbTable.inconsistencias,
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:2]
        )

    _D_result = TEMPRESA_ESTADOCUENTA_MOVIMIENTOS.BUSQUEDA_AVANZADA.CONFIGURA(
        s_empresa_id = _D_args.s_empresa_id,
        O_cat = _O_cat
        )

    if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
        # Si ocurrió algun error
        _D_return.E_return = _D_result.E_return
        _D_return.s_msgError = _D_result.s_msgError

        # No se ve la necesidad en este caso de hace sobreescritura del codigo de la accion
        # _s_overrideCode = request.stv_fwk_permissions.btn_cancel.code
    else:
        pass

    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionLabel = "Limpiar",
        s_permissionIcon = "fa fa-ban",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(),
            swap = {},
            ),
        L_options = []
        )

    if _s_action == request.stv_fwk_permissions.special_permisson_01.code:
        # Se desconcilia movimiento

        _dbRows_edocta_movtos_conciliaciones = dbc01(
            dbc01.tempresa_estadocuenta_conciliacionesbancarias.empresa_estadocuenta_movimiento_id == _s_banco_movto_id
            ).select(
                dbc01.tempresa_estadocuenta_conciliacionesbancarias.ALL
                )

        for _dbRow in _dbRows_edocta_movtos_conciliaciones:
            dbc01(
                dbc01.tcfdi_registropagos.id == _dbRow.registropago_id
                ).update(
                    estatus = TCFDI_REGISTROPAGOS.ESTATUS.PENDIENTE,
                    descripcion = "",
                    )
            _dbRow.delete_record()

        _dbRow_bancoMovto = dbc01.tempresa_estadocuenta_movimientos(_s_banco_movto_id)
        _dbRow_bancoMovto.estatus = TEMPRESA_ESTADOCUENTA_MOVIMIENTOS.ESTATUS.PENDIENTE
        _dbRow_bancoMovto.descripcion = request.vars.get("descripcion", "Des-conciliado")
        _dbRow_bancoMovto.update_record()

        TEMPRESA_ESTADOSCUENTA.ACTUALIZAR_ESTATUS(_s_empresa_cuentabancaria_estadocuenta_id)

        stv_flashSuccess(
            'Conciliación',
            "Se limpió el movimiento",
            s_mode = 'stv_flash',
            D_addProperties = {}
            )

        _D_overrideView = request.stv_fwk_permissions.view_refresh

    else:
        _D_overrideView = None

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_overrideView
        )

    _O_cat.addSubTab(
        s_tabName = "Conciliación Bancaria",
        s_url = URL(
            f = 'empresa_estadocuenta_movimiento_conciliaciones',
            args = _D_args.L_argsBasicos + ['master_' + str(request.function), _D_args.n_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'empresa_estadocuenta_movimiento_conciliaciones', s_masterIdentificator = str(request.function)
            )
        )

    generaMensajeFlash(_dbTable, _D_return)

    return _D_returnVars


def empresa_estadocuenta_movimiento_conciliaciones():
    """ Formulario para el detalle de las conciliaciones bancarias

    @descripcion Movimientos Conciliados

    @keyword arg0: Empresa
    @keyword arg1: empresa_id
    @keyword arg2: Estado de Cuenta
    @keyword arg3: empresa_estadocuenta_id
    @keyword arg4: Movimiento
    @keyword arg5: empresa_estadocuenta_movimiento_id
    @keyword [arg4]: codigo de funcionalidad
    @keyword [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
        
    @permiso btn_none
    #@permiso btn_new
    #@permiso btn_save
    #@permiso btn_cancel
    #@permiso btn_edit
    #@permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    #@permiso btn_signature
    
    """
    _D_return = Storage(
        E_return = CLASS_e_RETURN.OK,
        s_msgError = "",
        dbTable = dbc01.tempresa_estadocuenta_conciliacionesbancarias
        )

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 6,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_edocta_id = request.args(3, 0),
        s_movimiento_id = request.args(5, 0),
        )

    _dbTable = dbc01.tempresa_estadocuenta_conciliacionesbancarias

    dbc01.tcfdi_registropagos.monto.represent = lambda v, r: stv_represent_money2campos(
        v, r, dbField2 = dbc01.tcfdi_complementopagos.monto
        )
    dbc01.tcfdi_complementopagos.monto.readable = False

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_estadocuenta_movimientos,
        xFieldLinkMaster = [
            _dbTable.empresa_estadocuenta_movimiento_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.registropago_id,
            _dbTable.descripcion,
            dbc01.tcfdi_registropagos.cfdi_id,
            dbc01.tcfdi_registropagos.monto,
            dbc01.tcfdi_complementopagos.monto,
            dbc01.tcfdis.receptorcliente_id,
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = _D_args.n_offsetArgs - 2
        )

    _O_cat.addRowSearchResultsLeftJoin(
        dbc01.tcfdi_registropagos.on(dbc01.tcfdi_registropagos.id == _dbTable.registropago_id)
        )
    _O_cat.addRowSearchResultsLeftJoin(
        dbc01.tcfdis.on(
            (dbc01.tcfdis.id == dbc01.tcfdi_registropagos.cfdi_id)
            )
        )

    _O_cat.addRowSearchResultsLeftJoin(
        dbc01.tcfdi_complementopagos.on(
            (dbc01.tcfdi_complementopagos.id == dbc01.tcfdi_registropagos.cfdi_complementopago_id)
            )
        )

    _D_returnVars = _O_cat.process()
        
    return _D_returnVars


def empresa_estadocuenta_conciliacionbancaria_manual():
    """ Formulario para el detalle de las conciliaciones bancarias de forma manual.

    @descripcion Conciliación Manual

    @keyword arg0: Empresa
    @keyword arg1: empresa_id
    @keyword arg2: Estado de Cuenta
    @keyword arg3: empresa_estadocuenta_id
    @keyword [arg4]: codigo de funcionalidad
    @keyword [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
        
    @permiso btn_none
    #@permiso btn_new
    #@permiso btn_save
    #@permiso btn_cancel
    #@permiso btn_edit
    #@permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    #@permiso btn_signature
    @permiso special_permisson_01:[fa fa-compass] Conciliar Movimientos

    """

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 4,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_edocta_id = request.args(3, 0),
        )

    _dbTable = dbc01.tempresa_estadocuenta_movimientos
    _s_action = request.args(4, None)
    _s_empresa_cuentabancaria_estadocuenta_id = request.args(3, None)
    # _s_empresa_conciliacion_id = request.args(5, None)
    _D_overrideView = None
    
    _L_OrderBy = [
        _dbTable.fecha_operacion,
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbTable = None,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_findall,
            request.stv_fwk_permissions.special_permisson_01,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        x_offsetInArgs = request.args[:4]
        )

    _O_cat.addRowContentProperty(
        'formHidden', Storage(
            s_estadocuentamovimiento_ids = "",
            s_registropago_ids = ""
            )
        )
    
    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionLabel = "Conciliar movimientos",
        s_permissionIcon = "fa fa-compass",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_find),
        D_actions = Storage(
            modal = Storage(
                s_url = URL(
                    f = 'edocta_conciliacionbancaria_manual_conciliar',
                    args = _D_args.L_argsBasicos + [request.stv_fwk_permissions.btn_none.code],
                    vars = Storage(
                        )
                    ),
                s_field = "",
                ),
            js = Storage(
                sfn_beforePressAction = 'stv_fwk_button_beforepressaction_' + D_stvFwkCfg.D_uniqueHtmlIds.s_idTab
                ),
            swap = {},
            ),
        L_options = []
        )

    _D_returnVars = _O_cat.process(
        D_overrideView = request.stv_fwk_permissions.view_emptyForm,
        L_formFactoryFields = [],
        # b_skipAction = True
        )

    # _D_returnVars = _O_cat.process(D_overrideView = _D_overrideView)
    
    _D_returnVars.htmlid_estadocuenta = _O_cat.addSubTab(
        dbTableDetail = None,
        s_url = URL(
            f = 'empresa_estadocuenta_conciliacionbancaria_edoctamovtos',
            args = request.args[:2] + ['master_' + str(request.function), request.args(3)]
            ),
        s_tabName = "Estado de cuenta",
        s_idHtml = None,
        s_type = "extended"
        )
        
    _D_returnVars.htmlid_movimientos = _O_cat.addSubTab(
        dbTableDetail = None,
        s_url = URL(
            f = 'empresa_estadocuenta_conciliacionbancaria_registropagos',
            args = request.args[:2] + ['master_' + str(request.function), request.args(3)]
            ),
        s_tabName = "Movimientos sistema",
        s_idHtml = None,
        s_type = "extended"
        )
        
    return _D_returnVars


def edocta_conciliacionbancaria_manual_conciliar():
    """ Pantalla modal que muestra las opciones para la conciliación manual

    @descripcion Conciliación Manual: Conciliar

    @keyword arg0: Empresa
    @keyword arg1: empresa_id
    @keyword arg2: Estado de Cuenta
    @keyword arg3: empresa_estadocuenta_id
    @keyword [arg6]: codigo de funcionalidad
    @keyword [arg7]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    @permiso btn_none
    #@permiso btn_new
    #@permiso btn_save
    #@permiso btn_cancel
    #@permiso btn_edit
    #@permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    #@permiso btn_signature
    @permiso special_permisson_01:[fa fa-compass] Ignorar Movimientos

    """

    def obtener_dbRow_bancoMovto(
            s_estadocuentamovimiento_ids,
            s_proceso = "conciliar"
            ):
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            dbRow_bancoMovto = None
            )

        if "IDS" in s_estadocuentamovimiento_ids:
            _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
            _D_return.s_msgError = "No se puede %s si existen más de un registro del estado de cuenta seleccionado" % s_proceso
        elif not s_estadocuentamovimiento_ids:
            _D_return.E_return = CLASS_e_RETURN.NOK_DATOS_INVALIDOS
            _D_return.s_msgError = "No se puede %s si no hay registro de estado de cuenta seleccionado" % s_proceso
        else:
            _D_return.dbRow_bancoMovto = dbc01.tempresa_estadocuenta_movimientos(s_estadocuentamovimiento_ids)

        return _D_return

    def obtener_dbRows_sistemaMovtos(s_registropago_ids):
        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            dbRows_sistemaMovtos = None
            )

        if not s_registropago_ids:
            _D_return.E_return = CLASS_e_RETURN.NOK_ERROR
            _D_return.s_msgError = "No se puede conciliar si el movimiento del sistema no esta seleccionado"

        else:
            _L_registroPago_ids = []
            if "IDS" in s_registropago_ids:
                _L_registroPago_ids = s_registropago_ids.split("_")[1:]
            else:
                _L_registroPago_ids.append(s_registropago_ids)

            _D_return.dbRows_sistemaMovtos = dbc01(
                dbc01.tcfdi_registropagos.id.belongs(_L_registroPago_ids)
                ).select(
                    dbc01.tcfdi_registropagos.ALL,
                    dbc01.tcfdi_complementopagos.monto,
                    left = [
                        dbc01.tcfdi_complementopagos.on(
                            dbc01.tcfdi_complementopagos.id == dbc01.tcfdi_registropagos.cfdi_complementopago_id
                            )
                        ]
                    )
        return _D_return


    def validacion(
            O_form,
            s_estadocuentamovimiento_ids,
            s_registropago_ids,
            dbRow_bancoMovto,
            dbRows_sistemaMovtos
            ):
        """ Validación para grabar la conciliación
        """

        _dbTable = dbc01.tempresa_estadocuenta_movimientos

        if not dbRow_bancoMovto:
            _D_result = obtener_dbRow_bancoMovto(s_estadocuentamovimiento_ids)

            if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
                O_form.errors.id = _D_result.s_msgError
            else:

                dbRow_bancoMovto = _D_result.dbRow_bancoMovto
        else:
            pass

        if dbRow_bancoMovto:
            if dbRow_bancoMovto.estatus != TEMPRESA_ESTADOCUENTA_MOVIMIENTOS.ESTATUS.PENDIENTE:
                O_form.errors.id = "No se puede conciliar si el movimiento del banco no está en estatus Pendiente"

            elif dbRow_bancoMovto.abono == 0:
                O_form.errors.id = "No se puede conciliar si el movimiento del banco no tiene Abono"

            else:

                if not dbRows_sistemaMovtos:
                    _D_result = obtener_dbRows_sistemaMovtos(s_registropago_ids)

                    if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
                        O_form.errors.id = _D_result.s_msgError
                    else:
                        dbRows_sistemaMovtos = _D_result.dbRows_sistemaMovtos
                else:
                    pass

                if dbRows_sistemaMovtos:

                    for _dbRow in dbRows_sistemaMovtos:

                        if _dbRow.tcfdi_registropagos.estatus not in (
                                TEMPRESA_ESTADOCUENTA_MOVIMIENTOS.ESTATUS.PENDIENTE,
                                TEMPRESA_ESTADOCUENTA_MOVIMIENTOS.ESTATUS.IGNORADO_MANUAL,
                                TEMPRESA_ESTADOCUENTA_MOVIMIENTOS.ESTATUS.IGNORADO
                                ):
                            O_form.errors.id = "No se puede conciliar si alguno de los movimientos del sistema no está en estatus Pendiente o Ignorado"
                            break

                        else:
                            pass
                else:
                    pass
        else:
            # El error ya se produjo antes
            pass

        return O_form

    def aceptacion(O_form, dbRow_bancoMovto, dbRows_sistemaMovtos):
        """ Validación para grabar la conciliación
        """

        _dbTable = dbc01.tempresa_estadocuenta_movimientos

        _f_sistema_monto = 0
        for _dbRow in dbRows_sistemaMovtos:
            _f_sistema_monto += _dbRow.tcfdi_registropagos.monto or _dbRow.tcfdi_complementopagos.monto or 0.0
            _dbRow.tcfdi_registropagos.estatus = TCFDI_REGISTROPAGOS.ESTATUS.CONCILIADO_MANUAL
            _dbRow.tcfdi_registropagos.descripcion = O_form.vars.descripcion
            _dbRow.tcfdi_registropagos.update_record()

            dbc01.tempresa_estadocuenta_conciliacionesbancarias.insert(
                empresa_estadocuenta_movimiento_id = _dbRow_bancoMovto.id,
                registropago_id = _dbRow.tcfdi_registropagos.id
                )

        if dbRow_bancoMovto.abono != _f_sistema_monto:
            _dbRow_bancoMovto.inconsistencias = "Montos no coinciden en conciliación"
        else:
            _dbRow_bancoMovto.inconsistencias = ""
        _dbRow_bancoMovto.estatus = TEMPRESA_ESTADOCUENTA_MOVIMIENTOS.ESTATUS.CONCILIADO_MANUAL
        _dbRow_bancoMovto.descripcion = O_form.vars.descripcion
        _dbRow_bancoMovto.update_record()

        return O_form

    def proceso_ignorarMovimientos(
            s_estadocuentamovimiento_ids,
            s_registropago_ids,
            ):
        """ Validación para grabar la conciliación
        """

        _D_return = Storage(
            E_return = CLASS_e_RETURN.OK,
            s_msgError = "",
            D_errores = Storage()
            )

        _D_result = obtener_dbRow_bancoMovto(
            s_estadocuentamovimiento_ids,
            s_proceso = "ignorar")

        if _D_result.E_return == CLASS_e_RETURN.NOK_ERROR:
            _D_return.D_errores.id = _D_result.s_msgError

        elif _D_result.dbRow_bancoMovto and (_D_result.dbRow_bancoMovto.estatus != TEMPRESA_ESTADOCUENTA_MOVIMIENTOS.ESTATUS.PENDIENTE):
            _D_return.D_errores.id = "El movimiento en el banco no pudo ser ignorado ya que tiene un estado diferente a pendiente, use la pantalla principal para limpiar el estado."

        else:

            _dbRow_bancoMovto = _D_result.dbRow_bancoMovto

            _D_result = obtener_dbRows_sistemaMovtos(s_registropago_ids)
            _dbRows_sistemaMovtos = _D_result.dbRows_sistemaMovtos

            if _dbRows_sistemaMovtos:
                _L_registroPago_ids_aIgnorar = []
                for _dbRow in _dbRows_sistemaMovtos:
                    if _dbRow.tcfdi_registropagos.estatus not in (
                            TEMPRESA_ESTADOCUENTA_MOVIMIENTOS.ESTATUS.PENDIENTE,
                            TEMPRESA_ESTADOCUENTA_MOVIMIENTOS.ESTATUS.IGNORADO_MANUAL,
                            TEMPRESA_ESTADOCUENTA_MOVIMIENTOS.ESTATUS.IGNORADO
                            ):
                        _dbRows_edoCta_conciliaciones = dbc01(
                            dbc01.tempresa_estadocuenta_conciliacionesbancarias.registropago_id == _dbRow.tcfdi_registropagos.id
                            ).select(
                                dbc01.tempresa_estadocuenta_conciliacionesbancarias.id,
                                dbc01.tempresa_estadocuenta_conciliacionesbancarias.empresa_estadocuenta_movimiento_id
                                )

                        if _dbRows_edoCta_conciliaciones:
                            _n_edoCta_movimiento_id = _dbRows_edoCta_conciliaciones.first().empresa_estadocuenta_movimiento_id
                            _D_return.D_errores.id = ("El movimiento del sistema número %d no pudo ser ignorado, ya que tiene un estado diferente a " +
                                "pendiente, use la pantalla principal del estado de cuenta para limpiar el estado. " +
                                "Movimiento %d") % (_dbRow.tcfdi_registropagos.id, _n_edoCta_movimiento_id)

                        else:
                            # Si no encontró ningún movimiento en estados de cuenta relacionados,
                            # lo puede marcar como ignorado
                            _L_registroPago_ids_aIgnorar.append(_dbRow.tcfdi_registropagos.id)
                    else:
                        _L_registroPago_ids_aIgnorar.append(_dbRow.tcfdi_registropagos.id)

                if _L_registroPago_ids_aIgnorar:
                    _n_actualizados = dbc01(
                        dbc01.tcfdi_registropagos.id.belongs(_L_registroPago_ids_aIgnorar)
                        ).update(
                            estatus = TEMPRESA_ESTADOCUENTA_MOVIMIENTOS.ESTATUS.IGNORADO_MANUAL,
                            descripcion = request.vars.get("descripcion", "")
                            )

                    stv_flashSuccess(
                        'Conciliación',
                        "Se ignoraron %d movimientos en el sistema" % _n_actualizados,
                        s_mode = 'stv_flash',
                        D_addProperties = {}
                        )
                else:
                    pass

            if _dbRow_bancoMovto:
                _dbRow_bancoMovto.estatus = TEMPRESA_ESTADOCUENTA_MOVIMIENTOS.ESTATUS.IGNORADO_MANUAL
                _dbRow_bancoMovto.descripcion = request.vars.get("descripcion", "")
                _dbRow_bancoMovto.update_record()

                stv_flashSuccess(
                    'Conciliación',
                    "Se ignoró el movimiento del banco",
                    s_mode = 'stv_flash',
                    D_addProperties = {}
                    )

            else:
                pass

        return _D_return

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 4,
        s_codigoDefault = request.stv_fwk_permissions.btn_new.code,
        s_empresa_id = request.args(1, 0),
        s_almacen_id = request.args(3, 0),
        )

    s_titulo = "Conciliación Manual"

    _D_result = obtener_dbRow_bancoMovto(request.vars.s_estadocuentamovimiento_ids)
    _dbRow_bancoMovto = _D_result.dbRow_bancoMovto
    _D_result = obtener_dbRows_sistemaMovtos(request.vars.s_registropago_ids)
    _dbRows_sistemaMovtos = _D_result.dbRows_sistemaMovtos
    _s_observaciones = ""
    _f_edocta_abono = 0
    _f_sistema_monto = 0
    if _dbRow_bancoMovto:
        _f_edocta_abono = _dbRow_bancoMovto.abono
    else:
        pass
    if _dbRows_sistemaMovtos:
        for _dbRow in _dbRows_sistemaMovtos:
            _f_sistema_monto += _dbRow.tcfdi_registropagos.monto or _dbRow.tcfdi_complementopagos.monto or 0.0
    else:
        pass

    _s_overridecode = None
    _D_overrideView = None
    _D_errores = None

    _O_cat = STV_FWK_FORM(
        dbReference = db,
        dbTable = None,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_save,
            request.stv_fwk_permissions.special_permisson_01,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        b_applyPermissons = False,
        x_offsetInArgs = 4,
        Es_behavior = STV_FWK_FORM.ES_BEHAVIOR.S_MODAL
        )

    if _D_args.s_accion == request.stv_fwk_permissions.special_permisson_01.code:

        _D_result = proceso_ignorarMovimientos(
            s_estadocuentamovimiento_ids = request.vars.s_estadocuentamovimiento_ids,
            s_registropago_ids = request.vars.s_registropago_ids,
            )
        _D_errores = _D_result.D_errores
        # Se asocia el código de nuevo para que cree la forma y pueda asignar los errores
        _s_overridecode = request.stv_fwk_permissions.btn_new.code

    else:
        pass

    _O_cat.updateTabOption("s_modalTitle", s_titulo)

    _L_fieldsForm = [
        Field(
            'descripcion', 'string', length = 100, default = request.vars.get("descripcion", "Conciliación manual"),
            required = True,
            notnull = True,
            widget = stv_widget_input, label = 'Justificación', comment = '',
            writable = True, readable = True,
            represent = stv_represent_string
            ),
        Field(
            'monto_banco', 'decimal(14,4)', default = _f_edocta_abono,
            required=False,
            notnull=False, unique=False,
            widget=stv_widget_inputMoney, label='Monto Banco', comment=None,
            writable=False, readable=True,
            represent=stv_represent_money_ifnotzero
            ),
        Field(
            'monto_sistema', 'decimal(14,4)', default = _f_sistema_monto,
            required=False,
            notnull=False, unique=False,
            widget=stv_widget_inputMoney, label='Monto Sistema', comment=None,
            writable=False, readable=True,
            represent=stv_represent_money_ifnotzero
            ),
        Field(
            'observaciones', 'text', length = 500, default = _s_observaciones,
            required = False,
            notnull = False, unique = False,
            widget = stv_widget_input, label = ('observaciones'), comment = None,
            writable = False, readable = True,
            represent = stv_represent_text
            ),
        ]

    _O_cat.addRowContentProperty(
        'formHidden', Storage(
            s_estadocuentamovimiento_ids = request.vars.s_estadocuentamovimiento_ids,
            s_registropago_ids = request.vars.s_registropago_ids
            )
        )

    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionLabel = "Ignorar",
        s_permissionIcon = "fa fa-ban",
        s_type = "button",
        s_send = "form",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_save),
        D_actions = Storage(
            js = Storage(),
            swap = {},
            ),
        L_options = []
        )

    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_02.code,
        s_permissionLabel = "Limpiar",
        s_permissionIcon = "fa fa-ban",
        s_type = "button",
        s_send = "form",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_save),
        D_actions = Storage(
            js = Storage(),
            swap = {},
            ),
        L_options = []
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION,
        validacion,
        s_estadocuentamovimiento_ids = request.vars.s_estadocuentamovimiento_ids,
        s_registropago_ids = request.vars.s_registropago_ids,
        dbRow_bancoMovto = _dbRow_bancoMovto,
        dbRows_sistemaMovtos = _dbRows_sistemaMovtos
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED,
        aceptacion,
        dbRow_bancoMovto = _dbRow_bancoMovto,
        dbRows_sistemaMovtos = _dbRows_sistemaMovtos
        )

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_overrideView,
        s_overrideCode = _s_overridecode,
        L_formFactoryFields = _L_fieldsForm,
        b_skipAction = True
        )

    if _D_errores:
        # Si existen errores, se agregan al form
        _D_returnVars.rowContent_form.errors = _D_errores
    else:
        pass

    if _D_args.s_accion == request.stv_fwk_permissions.btn_none.code:
        _D_returnVars.D_useView.b_serverCreatesTab = True
    else:
        pass

    return _D_returnVars


def empresa_estadocuenta_conciliacionbancaria_registropagos():
    """

    @descripcion Movimientos en Sistema no conciliados

    @keyword arg0: Empresa
    @keyword arg1: empresa_id
    @keyword arg2: Estado de Cuenta
    @keyword arg3: empresa_estadocuenta_id
    @keyword [arg4]: codigo de funcionalidad
    @keyword [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    
    @permiso btn_none
    #@permiso btn_new
    #@permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    @permiso btn_find
    #@permiso btn_findall
    #@permiso btn_signature
    
    """
    _D_return = Storage(
        E_return = CLASS_e_RETURN.OK,
        s_msgError = "",
        dbTable = dbc01.tcfdi_registropagos
        )

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 4,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_edocta_id = request.args(3, 0),
        )

    _dbTable = dbc01.tcfdi_registropagos
    
    _s_empresa_id = request.args(1, None)
    _s_empresa_estadocuenta_id = request.args(3, None)
    
    _dbRowEstadoCuenta = dbc01.tempresa_estadoscuenta(_s_empresa_estadocuenta_id)
    _d_fechaInicial = _dbRowEstadoCuenta.fecha_inicio
    _d_fechaFinal = _dbRowEstadoCuenta.fecha_fin

    _SQL_terminales = dbc01(
        dbc01.tempresa_cuentabancaria_terminales.empresa_cuentabancaria_id == _dbRowEstadoCuenta.empresa_cuentabancaria_id
        )._select(
            dbc01.tempresa_cuentabancaria_terminales.id
            )

    _qry = (
        # (dbc01.tcfdi_registropagos.fechadeposito >= _d_fechaInicial.date())
        # & (dbc01.tcfdi_registropagos.fechadeposito <= _d_fechaFinal.date())
        (
            (dbc01.tcfdi_registropagos.empresa_cuentabancaria_id == _dbRowEstadoCuenta.empresa_cuentabancaria_id)
            |
            (dbc01.tcfdi_registropagos.empresa_cuentabancaria_terminal_id.belongs(_SQL_terminales))
            )
        )

    _D_results = TCFDI_REGISTROPAGOS.BUSQUEDA_AVANZADA.GENERA_PREFILTRO(
        _D_args,
        )
    _qry &= _D_results.qry

    _L_OrderBy = [
        _dbTable.fechadeposito,
        _dbTable.monto
        ]

    dbc01.tcfdi_registropagos.monto.represent = lambda v, r: stv_represent_money2campos(
        v, r, dbField2 = dbc01.tcfdi_complementopagos.monto
        )
    dbc01.tcfdi_complementopagos.monto.readable = False

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable, 
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_find,
            #request.stv_fwk_permissions.btn_findall,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.cfdi_id,
            dbc01.tcfdis.receptorcliente_id,
            _dbTable.empresa_cuentabancaria_terminal_id,
            _dbTable.fechadeposito, 
            _dbTable.formapago_id,
            _dbTable.monto,
            dbc01.tcfdi_complementopagos.monto,
            _dbTable.referencia,
            _dbTable.estatus,
            _dbTable.autorizacion,
            _dbTable.operacion,
            _dbTable.cheque,
            _dbTable.cuenta_cliente,
            _dbTable.claverastero,
            ],
        L_fieldsToGroupBy = [],
        L_fieldsToSearch = [
            _dbTable.id,
            dbc01.tcfdis.receptornombrerazonsocial,
            dbc01.tcfdis.receptorrfc,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            dbc01.tcfdis.folio
            ],
        x_offsetInArgs = request.args[:4]
        )

    _O_cat.addRowSearchResultsLeftJoin(
        dbc01.tcfdis.on(
            (dbc01.tcfdis.id == _dbTable.cfdi_id)
            )
        )

    _O_cat.addRowSearchResultsLeftJoin(
        dbc01.tcfdi_complementopagos.on(
            (dbc01.tcfdi_complementopagos.id == _dbTable.cfdi_complementopago_id)
            )
        )

    _D_result = TCFDI_REGISTROPAGOS.BUSQUEDA_AVANZADA.CONFIGURA(
        s_empresa_id = _D_args.s_empresa_id,
        O_cat = _O_cat,
        fechadeposito = [_d_fechaInicial, _d_fechaFinal]
        )

    _O_cat.addRowSearchResultsConfig('multiselect', True)
    # Se crea una nueva columna en el grid. Se agrega un "+" y se especifica la función entorno a donde funcionará el botón "+".
    # _O_cat.addRowSearchResults_addColumnBegin(
    #     es_typeColumn = STV_FWK_FORM.ES_TYPECOLUMN.S_PLUS,
    #     s_colname = "plus",
    #     D_field = Storage(
    #         readable = True
    #         ),
    #     m_classHtml = lambda D_column, dbRow: "stv_rowresults_plus",
    #     D_data = Storage(
    #         url = URL(f='empresa_estadocuenta_conciliacionbancaria_registropagos_plus', args=request.args[:4]+['master_'+str(request.function)])
    #         )
    #     )
    
    _O_cat.addRowSearchResultsProperty('orderBy', _L_OrderBy)
        
    _D_returnVars = _O_cat.process()

    return _D_returnVars


def empresa_estadocuenta_conciliacionbancaria_registropagos_plus():
    """ Especifica la información necesaria para contabilizar el CFDI de pago. 
    
    @descripcion Relación de Pagos

    @keyword arg0: Empresa
    @keyword arg1: empresa_id
    @keyword arg2: Estado de Cuenta
    @keyword arg3: empresa_estadocuenta_id
    @keyword arg4: Movimiento Sistema no Conciliado
    @keyword arg5: registropago_id
    @keyword [arg6]: codigo de funcionalidad
    @keyword [arg7]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    
    @permiso btn_none
    #@permiso btn_new
    #@permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    #@permiso btn_signature

    """
    
    _dbTable = dbc01.tcfdi_registropagos
    
    _s_empresa_id = request.args(1, None)
    
    _s_registropago_id = request.args(5, None)
    
    _s_action = request.args(6, request.stv_fwk_permissions.btn_none.code)
    
    _dbRowRegistroPago = dbc01.tcfdi_registropagos(_s_registropago_id)
    
    if _dbRowRegistroPago.formapago_id in (TFORMASPAGO.TDD, TFORMASPAGO.TDC):
    
        _qry = (
            (dbc01.tpolizas.empresa_id == _s_empresa_id)
            & (dbc01.tcfdi_prepolizas.poliza_id == dbc01.tpolizas.id)
            & (dbc01.tcfdi_registropagos.cfdi_id == dbc01.tcfdi_prepolizas.cfdi_id)
            & (dbc01.tcfdi_registropagos.operacion == _dbRowRegistroPago.operacion)
            & (dbc01.tcfdi_registropagos.formapago_id == _dbRowRegistroPago.formapago_id)
            & (dbc01.tcfdi_registropagos.fechadeposito == _dbRowRegistroPago.fechadeposito)
            & (
                (dbc01.tcfdi_registropagos.empresa_cuentabancaria_id == 2)
                | (dbc01.tcfdi_registropagos.empresa_cuentabancaria_terminal_id.belongs(7, 19, 20, 21, 22))
                )
            )
        
    else:
        _qry = (
            (dbc01.tpolizas.empresa_id == _s_empresa_id)
            & (dbc01.tcfdi_prepolizas.poliza_id == dbc01.tpolizas.id)
            & (dbc01.tcfdi_registropagos.cfdi_id == dbc01.tcfdi_prepolizas.cfdi_id)
            & (dbc01.tcfdi_registropagos.referencia == _dbRowRegistroPago.referencia)
            & (dbc01.tcfdi_registropagos.formapago_id == _dbRowRegistroPago.formapago_id)
            & (dbc01.tcfdi_registropagos.fechadeposito == _dbRowRegistroPago.fechadeposito)
            & (
                (dbc01.tcfdi_registropagos.empresa_cuentabancaria_id == 2)
                | (dbc01.tcfdi_registropagos.empresa_cuentabancaria_terminal_id.belongs(7, 19, 20, 21, 22))
                )
            )
        
    _dbTable.cfdi_id.readable = True
            
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none, 
            request.stv_fwk_permissions.btn_findall,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab + "_" + str(_s_registropago_id),
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.cfdi_id,
            _dbTable.fechadeposito, 
            _dbTable.formapago_id,
            _dbTable.monto,
            ],
        L_fieldsToSearch = [
            _dbTable.id, 
            _dbTable.fechadeposito, 
            _dbTable.formapago_id, 
            _dbTable.monto
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            _dbTable.fechadeposito,
            _dbTable.formapago_id, 
            _dbTable.monto
            ],
        x_offsetInArgs = request.args[:6],
        )
    
    _D_returnVars = _O_cat.process()
    
    if _s_action == request.stv_fwk_permissions.btn_none.code:
        _D_returnVars.D_useView.b_serverCreatesTab = True
    else:
        pass
        
    return (_D_returnVars)


def empresa_estadocuenta_conciliacionbancaria_edoctamovtos():
    """ 
        
    @descripcion Movimientos en Estado de Cuenta no conciliados

    @keyword arg0: Empresa
    @keyword arg1: empresa_id
    @keyword arg2: Estado de Cuenta
    @keyword arg3: empresa_estadocuenta_id
    @keyword [arg4]: codigo de funcionalidad
    @keyword [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    
    @permiso btn_none
    #@permiso btn_new
    #@permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    #@permiso btn_signature
    
    """
    _D_return = Storage(
        E_return = CLASS_e_RETURN.OK,
        s_msgError = "",
        )

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 4,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_edocta_id = request.args(3, 0),
        )

    _dbTable = dbc01.tempresa_estadocuenta_movimientos
    
    _s_empresa_estadocuenta_id = request.args(3, None)

    _D_results = TEMPRESA_ESTADOCUENTA_MOVIMIENTOS.BUSQUEDA_AVANZADA.GENERA_PREFILTRO(_D_args)
    _qry = _D_results.qry

    _L_OrderBy = [
        _dbTable.fecha_operacion,
        _dbTable.abono
        ]
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable, 
        dbTableMaster = dbc01.tempresa_estadoscuenta,
        xFieldLinkMaster = [
            _dbTable.empresa_estadocuenta_id
            ],
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_find,
            request.stv_fwk_permissions.btn_findall,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.fecha_operacion,
            #_dbTable.concepto,
            _dbTable.abono,
            _dbTable.cargo,
            _dbTable.bancotipomovimiento_id,
            _dbTable.referencia,
            _dbTable.estatus,
            _dbTable.descripcion,
            _dbTable.inconsistencias,
            ], 
        L_fieldsToSearch = [
            _dbTable.id, 
            _dbTable.empresa_estadocuenta_id,
            _dbTable.fecha_operacion,
            _dbTable.bancotipomovimiento_id,
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            _dbTable.empresa_estadocuenta_id,
            _dbTable.fecha_operacion,
            _dbTable.bancotipomovimiento_id,
            _dbTable.abono,
            ],
        x_offsetInArgs = request.args[:2]
        )
    
    _O_cat.addRowSearchResultsProperty('orderBy', _L_OrderBy)

    _D_result = TEMPRESA_ESTADOCUENTA_MOVIMIENTOS.BUSQUEDA_AVANZADA.CONFIGURA(
        s_empresa_id = _D_args.s_empresa_id,
        O_cat = _O_cat
        )
    
    #_O_cat.addRowSearchResultsProperty(s_property = 'limitBySearch', x_value = (0, 500000))
        
    _D_returnVars = _O_cat.process()

    return (_D_returnVars)


def estadocuenta_archivo():
    """Función para la descarga del archivo TXT de la póliza
    """
    _bDisplay = False if request.vars.get('display') else True
    return response.download(request, dbc01, attachment=_bDisplay, download_filename=request.vars.get('filename', None))

class EDOCTA_BANORTE():
    class COLUMNAS():
        FECHA_OPERACION = 'FECHA DE OPERACIÓN'
        FECHA_REGISTRO = 'FECHA'
        CONCEPTO = 'DESCRIPCIÓN'
        CONCEPTO2 = 'DESCRIPCIÓN DETALLADA'
        ABONO = 'DEPÓSITOS'
        CARGO = 'RETIROS'
        TIPOMOVIMIENTO = 'COD. TRANSAC'
        REFERENCIA = 'REFERENCIA'
        NUMERO_MOVIMIENTO = 'MOVIMIENTO'
        CUENTA = 'CUENTA'
        SALDO = 'SALDO'
        SUCURSAL = 'SUCURSAL'
        CHEQUE = 'CHEQUE'


    def __init__(
            self,
            ):
        """Función para la importación del estado de cuenta por archivo excel.

            @param s_path: Ruta donde se encuentra el archivo.
        """
        self._s_path = ""
        self._n_estadocuenta_id = 0
        self.dt_inicio = None
        self.dt_fin = None
        self._L_cuentas = []
        self._s_cuentabancaria_id = ""
        self._dbRowCuentaBancaria = None
        self._D_banco_codigosTipoMovimiento = Storage()
        self._D_tipoMovimiento_formaPagoId = Storage()

        return

    def configurar(
            self,
            s_path,
            n_estadocuenta_id,
            s_cuentabancaria_id,
            ):
        self._s_path = s_path
        self._n_estadocuenta_id = n_estadocuenta_id
        self._s_cuentabancaria_id = s_cuentabancaria_id
        self._dbRowCuentaBancaria = dbc01(
            (dbc01.tempresa_cuentasbancarias.id == self._s_cuentabancaria_id)
            & (dbc01.tempresa_bancoscontpaqi.id == dbc01.tempresa_cuentasbancarias.empresa_bancocontpaqi_id)
            ).select(
                dbc01.tempresa_cuentasbancarias.ALL,
                dbc01.tempresa_bancoscontpaqi.ALL
                ).first()
        self._D_banco_codigosTipoMovimiento = Storage()
        self._D_tipoMovimiento_formaPagoId = Storage()
        self._obtener_codigosTipoMovimientos()

        if self._s_path:
            self._importar_excel(self._s_path)
        else:
            1/0 # no esta terminada la lectura del estado de cuenta
            # deberia llenar dt_inicio, dt_fin, L_cuentas
            #self._leer_estadocuenta()
            pass

        return

    def _importar_excel(
            self,
            s_path,
            ):
        """Función para la importación del estado de cuenta por archivo excel.

            @param s_path: Ruta donde se encuentra el archivo.
        """


        import openpyxl
        wb_obj = openpyxl.load_workbook(s_path)

        sheet = wb_obj.active
        self._D_estadoCuenta = Storage()
        _n_renglonHeader = 0

        for _O_col in sheet.columns:
            _s_renglonHeader = _O_col[_n_renglonHeader].value.encode('utf8', 'replace')
            self._D_estadoCuenta[_s_renglonHeader] = []

            _n_renglonDatos = _n_renglonHeader + 1

            for _O_cell in _O_col[_n_renglonDatos:]:
                if isinstance(_O_cell.value, str):
                    _x_value = _O_cell.value.encode('utf8', 'replace')
                else:
                    _x_value = _O_cell.value

                if isinstance(_x_value, str):
                    _x_value = _x_value.replace("-", "")
                else:
                    pass

                if _x_value:
                    if (_s_renglonHeader == EDOCTA_BANORTE.COLUMNAS.FECHA_OPERACION):
                        self.dt_inicio = self.dt_inicio or _x_value
                        self.dt_fin = self.dt_fin or _x_value
                        if self.dt_inicio > _x_value:
                            self.dt_inicio = _x_value
                        else:
                            pass
                        if self.dt_fin < _x_value:
                            self.dt_fin = _x_value
                        else:
                            pass
                    elif (_s_renglonHeader == EDOCTA_BANORTE.COLUMNAS.CUENTA):
                        _x_value = _x_value.replace("'", "")

                        if not (_x_value in self._L_cuentas):
                            self._L_cuentas.append(_x_value)
                        else:
                            pass

                    else:
                        pass

                elif (_s_renglonHeader == EDOCTA_BANORTE.COLUMNAS.REFERENCIA):
                    # Se usa esta operación para sustituir la referencia si es zero por nada
                    if not _x_value:
                        _x_value = ""
                    else:
                        pass

                else:
                    pass

                self._D_estadoCuenta[_s_renglonHeader].append(_x_value)

        wb_obj.close()

        return

    def _obtener_codigosTipoMovimientos(
            self,
            ):

        _dbRows_banco_codigosTipoMovimiento = db01(
            (db01.tbancostipomovimiento_codigos.banco_id == self._dbRowCuentaBancaria.tempresa_bancoscontpaqi.banco_id)
            & (db01.tbancostiposmovimientos.id == db01.tbancostipomovimiento_codigos.bancostipomovimiento_id)
            ).select(
                db01.tbancostipomovimiento_codigos.ALL,
                db01.tbancostiposmovimientos.ALL
                )

        self._D_banco_codigosTipoMovimiento = Storage()
        self._D_tipoMovimiento_formaPagoId = Storage()
        for _dbRow in _dbRows_banco_codigosTipoMovimiento:
            if _dbRow.tbancostipomovimiento_codigos.codigomovimiento in self._D_banco_codigosTipoMovimiento:
                # Si ya existe el código movimiento para el banco, se pone en None ya que no se puede identificar el movimiento
                self._D_banco_codigosTipoMovimiento[_dbRow.tbancostipomovimiento_codigos.codigomovimiento] = None
            else:
                self._D_banco_codigosTipoMovimiento[_dbRow.tbancostipomovimiento_codigos.codigomovimiento] = _dbRow.tbancostipomovimiento_codigos.bancostipomovimiento_id

            self._D_tipoMovimiento_formaPagoId[_dbRow.tbancostipomovimiento_codigos.bancostipomovimiento_id] = _dbRow.tbancostiposmovimientos.formapago_id

        return

    def _identifica_tipoMovimiento(
            self,
            n_row
            ):

        _s_tipoMovimiento = self._D_estadoCuenta[EDOCTA_BANORTE.COLUMNAS.TIPOMOVIMIENTO][n_row]

        _s_codigoMovimiento = str(_s_tipoMovimiento)

        if _s_tipoMovimiento in (60, 600, 601):

            _s_formapago = str(self._D_estadoCuenta[EDOCTA_BANORTE.COLUMNAS.CONCEPTO][n_row])

            if _s_formapago[-1:] in ('D', 'C'):
                _s_codigoMovimiento += _s_formapago[-1:]
            else:
                pass

        else:
            pass

        return self._D_banco_codigosTipoMovimiento[_s_codigoMovimiento]

    def validaciones(
            self
            ):
        _D_return = Storage(
            s_msgError = "",
            E_return = CLASS_e_RETURN.OK,
            n_regostrosInsertados = 0
            )

        if len(self._L_cuentas) > 1:
            _D_return.s_msgError = "Estado de cuenta contiene diferentes cuentas bancarias, no puede procesarse"
            _D_return.E_return = CLASS_e_RETURN.NOK_DATOS_INVALIDOS
        elif len(self._L_cuentas) == 0:
            _D_return.s_msgError = "No se identifico la cuenta bancaria o el estado de cuenta esta vacío"
            _D_return.E_return = CLASS_e_RETURN.NOK_DATOS_INVALIDOS
        elif self._L_cuentas[0] != self._dbRowCuentaBancaria.tempresa_cuentasbancarias.cuenta:
            _D_return.s_msgError = "Cuenta bancaria no coincide"
            _D_return.E_return = CLASS_e_RETURN.NOK_DATOS_INVALIDOS
        elif not self._s_cuentabancaria_id:
            _D_return.s_msgError = "Cuenta bancaria no definida"
            _D_return.E_return = CLASS_e_RETURN.NOK_DATOS_INVALIDOS
        else:
            pass

        return _D_return

    def graba_movimientos(
            self,
            n_estadocuenta_id = None
            ):

        _D_return = Storage(
            s_msgError = "",
            E_return = CLASS_e_RETURN.OK,
            n_regostrosInsertados = 0
            )

        self._n_estadocuenta_id = n_estadocuenta_id or self._n_estadocuenta_id
        _D_result = self.validaciones()
        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.s_msgError = _D_result.s_msgError
            _D_return.E_return = _D_result.E_return

        elif not self._n_estadocuenta_id:
            _D_return.s_msgError = "Estado de cuenta id no identificado"
            _D_return.E_return = CLASS_e_RETURN.NOK_DATOS_INVALIDOS

        else:

            _D_return.n_regostrosInsertados = 0
            # Se navega por toda la tabla, tomando como referencia la columna de fecha operación
            for _n_index, _dt_fechaOperacion in enumerate(self._D_estadoCuenta[EDOCTA_BANORTE.COLUMNAS.FECHA_OPERACION]):

                # Validación para saber si este movimiento no se ha metido anteriormente
                _dbRows_estadoCuentaMovimientos = dbc01(
                    (dbc01.tempresa_estadoscuenta.empresa_cuentabancaria_id == self._s_cuentabancaria_id)
                    & (dbc01.tempresa_estadocuenta_movimientos.empresa_estadocuenta_id == dbc01.tempresa_estadoscuenta.id)
                    & (dbc01.tempresa_estadocuenta_movimientos.numero_movimiento == self._D_estadoCuenta[EDOCTA_BANORTE.COLUMNAS.NUMERO_MOVIMIENTO][_n_index])
                        ).select(
                        dbc01.tempresa_estadocuenta_movimientos.id
                        )

                if self._D_estadoCuenta[EDOCTA_BANORTE.COLUMNAS.CARGO][_n_index]:
                    # Si es cargo se ignora en esta conciliación
                    _E_estatus = TEMPRESA_ESTADOCUENTA_MOVIMIENTOS.ESTATUS.IGNORADO
                    _s_descripcion = "Cargos no son parte de esta conciliación"
                elif (str(self._D_estadoCuenta[EDOCTA_BANORTE.COLUMNAS.TIPOMOVIMIENTO][_n_index]) in ('247', '1')):
                    _E_estatus = TEMPRESA_ESTADOCUENTA_MOVIMIENTOS.ESTATUS.IGNORADO
                    _s_descripcion = "Tipo de movimiento no son parte de esta conciliación"
                else:
                    _E_estatus = TEMPRESA_ESTADOCUENTA_MOVIMIENTOS.ESTATUS.PENDIENTE
                    _s_descripcion = ""

                if not _dbRows_estadoCuentaMovimientos:

                    #  Diccionario para insertar las configuraciones de tipos de póliza.
                    _D_insert_Helper = Storage(
                        empresa_estadocuenta_id = self._n_estadocuenta_id,
                        fecha_operacion = _dt_fechaOperacion,
                        fecha_registro = self._D_estadoCuenta[EDOCTA_BANORTE.COLUMNAS.FECHA_REGISTRO][_n_index],
                        concepto = self._D_estadoCuenta[EDOCTA_BANORTE.COLUMNAS.CONCEPTO][_n_index],
                        abono = self._D_estadoCuenta[EDOCTA_BANORTE.COLUMNAS.ABONO][_n_index] or 0,
                        cargo = self._D_estadoCuenta[EDOCTA_BANORTE.COLUMNAS.CARGO][_n_index] or 0,
                        saldo = self._D_estadoCuenta[EDOCTA_BANORTE.COLUMNAS.SALDO][_n_index] or 0,
                        bancotipomovimiento_id = self._identifica_tipoMovimiento(_n_index),
                        referencia = self._D_estadoCuenta[EDOCTA_BANORTE.COLUMNAS.REFERENCIA][_n_index],
                        estatus = _E_estatus,
                        numero_movimiento = self._D_estadoCuenta[EDOCTA_BANORTE.COLUMNAS.NUMERO_MOVIMIENTO][_n_index],
                        cuenta = self._D_estadoCuenta[EDOCTA_BANORTE.COLUMNAS.CUENTA][_n_index],
                        sucursal = self._D_estadoCuenta[EDOCTA_BANORTE.COLUMNAS.SUCURSAL][_n_index],
                        cheque = self._D_estadoCuenta[EDOCTA_BANORTE.COLUMNAS.CHEQUE][_n_index],
                        tipomovimiento = self._D_estadoCuenta[EDOCTA_BANORTE.COLUMNAS.TIPOMOVIMIENTO][_n_index],
                        descripcion = _s_descripcion,
                        )

                    dbc01.tempresa_estadocuenta_movimientos.insert(**_D_insert_Helper)
                    _D_return.n_regostrosInsertados += 1

                else:
                    pass

        return _D_return

    def concilia_depositos_pendientes(
            self,
            b_usarreferencia = True,
            b_usarfecha = True
            ):

        _D_return = Storage(
            s_msgError = "",
            E_return = CLASS_e_RETURN.OK,
            n_movimientosCoinciliados = 0
            )

        _dbRows_edoCtaMovtos = dbc01(
            (dbc01.tempresa_estadocuenta_movimientos.empresa_estadocuenta_id == self._n_estadocuenta_id)
            & (dbc01.tempresa_estadocuenta_movimientos.estatus == TEMPRESA_ESTADOCUENTA_MOVIMIENTOS.ESTATUS.PENDIENTE)
            & (dbc01.tempresa_estadocuenta_movimientos.abono > 0)
            ).select(
                dbc01.tempresa_estadocuenta_movimientos.ALL
                )

        _D_edoCtaMovtos = Storage()

        for _dbRow in _dbRows_edoCtaMovtos:
            # Se llena un diccionario usando como indice un if único que ayude a identificar el movimiento
            if b_usarreferencia:
                _s_referencia = _dbRow.referencia
            else:
                _s_referencia = ""
            if b_usarfecha:
                _s_fecha = _dbRow.fecha_operacion.strftime("%m/%d/%Y")
            else:
                _s_fecha = ""
            _s_movto_id = "%s_%s_%s_%.2f" % (
                self._D_tipoMovimiento_formaPagoId[_dbRow.bancotipomovimiento_id],
                _s_referencia,
                _s_fecha,
                _dbRow.abono
                )

            if _s_movto_id in _D_edoCtaMovtos:
                pass
            else:
                # En el segundo indice se utiliza la cantidad del abono con el fin de poder agregar lógica para detectar
                # abonos con variación de centavos
                _D_edoCtaMovtos[_s_movto_id] = []

            # Y luego se llena una lista con cada registro que se encuentre, normalmente va a existir un solo registro
            _D_edoCtaMovtos[_s_movto_id].append(
                Storage(
                    n_id = _dbRow.id,
                    f_abono = _dbRow.abono,
                    dbRow = _dbRow,
                    f_abonoUsado = 0,   # En esta variable se identificará la parte del abono que ya este relacionado
                    L_pagos = []        # Contendrá la lista de registropagos que se usaron para el movto del estado de cuenta
                    )
                )

        _dbRows_movtosPagos = dbc01(
            (dbc01.tcfdi_registropagos.empresa_cuentabancaria_id.belongs(self._dbRowCuentaBancaria.tempresa_cuentasbancarias.id, None))
            & (dbc01.tcfdi_registropagos.fechadeposito >= self.dt_inicio.date())
            & (dbc01.tcfdi_registropagos.fechadeposito <= self.dt_fin.date())
            & (dbc01.tcfdi_registropagos.estatus == TCFDI_REGISTROPAGOS.ESTATUS.PENDIENTE)
            & (
                (dbc01.tcfdi_registropagos.empresa_cuentabancaria_id == self._dbRowCuentaBancaria.tempresa_cuentasbancarias.id)
                | (
                    (dbc01.tempresa_cuentabancaria_terminales.id == dbc01.tcfdi_registropagos.empresa_cuentabancaria_terminal_id)
                    & (dbc01.tempresa_cuentabancaria_terminales.empresa_cuentabancaria_id == self._dbRowCuentaBancaria.tempresa_cuentasbancarias.id)
                    )
                )
            ).select(
                dbc01.tcfdi_registropagos.ALL,
                dbc01.tcfdi_complementopagos.monto,
                dbc01.tempresa_cuentabancaria_terminales.id,
                dbc01.tempresa_cuentabancaria_terminales.empresa_cuentabancaria_id,
                left = [
                    dbc01.tempresa_cuentabancaria_terminales.on(
                        (dbc01.tempresa_cuentabancaria_terminales.id == dbc01.tcfdi_registropagos.empresa_cuentabancaria_terminal_id)
                        & (dbc01.tempresa_cuentabancaria_terminales.empresa_cuentabancaria_id == self._dbRowCuentaBancaria.tempresa_cuentasbancarias.id)
                        ),
                    dbc01.tcfdi_complementopagos.on(
                        (dbc01.tcfdi_complementopagos.id == dbc01.tcfdi_registropagos.cfdi_complementopago_id)
                        )
                    ]
                )

        for _dbRow in _dbRows_movtosPagos:

            if (
                    (_dbRow.tcfdi_registropagos.empresa_cuentabancaria_id == self._dbRowCuentaBancaria.tempresa_cuentasbancarias.id)
                    or (_dbRow.tempresa_cuentabancaria_terminales.empresa_cuentabancaria_id == self._dbRowCuentaBancaria.tempresa_cuentasbancarias.id)
                    ):

                if b_usarreferencia:
                    _s_referencia = _dbRow.tcfdi_registropagos.referencia
                else:
                    _s_referencia = ""

                if b_usarfecha:
                    _s_fecha = _dbRow.tcfdi_registropagos.fechadeposito.strftime("%m/%d/%Y")
                else:
                    _s_fecha = ""

                _s_movto_id = "%d_%s_%s_%.2f" % (
                    _dbRow.tcfdi_registropagos.formapago_id,
                    _s_referencia,
                    _s_fecha,
                    _dbRow.tcfdi_registropagos.monto or _dbRow.tcfdi_complementopagos.monto or 0.0
                    )

                if _s_movto_id in _D_edoCtaMovtos:
                    # Si se encuentra el filtro inicial del movimiento

                    for _D_movto in _D_edoCtaMovtos[_s_movto_id]:

                        # TODO establecer lógica para poder agrupar registros en el mismo monto del estado de cuenta
                        if _D_movto.f_abonoUsado == 0:
                            # Si este movimiento no se ha usado

                            # La mayoria de los registros con pagos únicos caerían aqui
                            _D_movto.dbRow.inconsistencias = ""
                            _D_movto.dbRow.estatus = TEMPRESA_ESTADOCUENTA_MOVIMIENTOS.ESTATUS.CONCILIADO_AUTOMATICO
                            _D_movto.f_abonoUsado = _D_movto.f_abono
                            # Se deja lógica de multipagos, en caso de que se pueda asociar un mismo pago a varios depositos
                            _D_movto.L_pagos.append(_dbRow.tcfdi_registropagos.id)
                            for _n_pago_id in _D_movto.L_pagos:
                                dbc01.tempresa_estadocuenta_conciliacionesbancarias.insert(
                                    empresa_estadocuenta_movimiento_id = _D_movto.dbRow.id,
                                    registropago_id = _n_pago_id
                                    )
                            _D_movto.dbRow.update_record()

                            _dbRow.tcfdi_registropagos.estatus = TCFDI_REGISTROPAGOS.ESTATUS.CONCILIADO_AUTOMATICO
                            _dbRow.tcfdi_registropagos.update_record()

                            _D_return.n_movimientosCoinciliados += 1
                            break  # No se sigan buscando y vamos con el siguiente movimiento

                        else:
                            # Si el abono ya se utilizo, se busca el siguiente
                            pass

                else:
                    # Si no se encuentra el movimiento
                    pass
            else:
                # El movimiento corresponde a otra cuenta bancaria
                pass

        return _D_return
