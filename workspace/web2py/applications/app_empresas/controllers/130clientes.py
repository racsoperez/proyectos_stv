# -*- coding: utf-8 -*-

def empresa_clientes():
    """ Maneja la configuración de los clientes
    
    @descripcion Clientes
    
    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.
    
    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    #@permiso btn_findall
    #@permiso btn_signature
    @permiso special_permisson_01:[fas fa-money-check-alt] Emisión de CFDIs
    @permiso special_permisson_02:[<i class="fas fa-balance-scale-right] Recalcular saldos
            
    """
    _D_return = FUNC_RETURN()

    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 2,
        s_codigoDefault = request.stv_fwk_permissions.btn_none.code,
        s_empresa_id = request.args(1, 0),
        s_cliente_id = request.args(3, None),
        )

    _dbTabla = dbc01.tempresa_clientes
    _D_useView = None
    _CLS_TABLA = TEMPRESA_CLIENTES.PROC()

    if _D_args.n_id and _D_args.s_accion in (
            request.stv_fwk_permissions.btn_edit.code,
            request.stv_fwk_permissions.btn_save.code,
            ):
        # Si esta editando o guardando una edición, se configuran los campos
        _D_result = _CLS_TABLA.configuracampos_edicion(x_cliente = _D_args.s_cliente_id)
        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_result)
            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            pass

    elif _D_args.s_accion in (
            request.stv_fwk_permissions.btn_new.code,
            request.stv_fwk_permissions.btn_save.code,
            ):

        _D_result = _CLS_TABLA.configuracampos_nuevo(s_cliente_id = _D_args.n_id)

        if _D_result.E_return > CLASS_e_RETURN.OK_WARNING:
            _D_return.update(_D_result)
            _D_useView = request.stv_fwk_permissions.view_refresh

        else:
            pass

    else:
        pass

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbTable = _dbTabla,
        dbTableMaster = dbc01.tempresas,
        xFieldLinkMaster = [_dbTabla.empresa_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTabla.id,
            _dbTabla.cliente_codigo_contpaqi,
            _dbTabla.razonsocial, _dbTabla.rfc, _dbTabla.nombrecorto,
            _dbTabla.empresa_grupo_id,
            _dbTabla.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTabla.id, _dbTabla.codigo,
            _dbTabla.razonsocial, _dbTabla.rfc,
            _dbTabla.nombrecorto
            ],
        L_fieldsToSearchIfDigit = [
            _dbTabla.id, _dbTabla.codigo
            ],
        )

    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION, _CLS_TABLA.al_validar)
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_ACCEPTED, _CLS_TABLA.AL_ACEPTAR)
    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.BEFORE_REMOVE, _CLS_TABLA.ANTES_ELIMINAR,
        dbRow = None  # Llenado por el FWK
        )
    _O_cat.addRowContentEvent(STV_FWK_FORM.ROWCONTENT.E_EVENT.AFTER_REMOVE, _CLS_TABLA.DESPUES_ELIMINAR)

    _D_returnVars = _O_cat.process(
        D_overrideView = _D_useView,
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_cliente_domicilios,
        s_url = URL(
            f = 'empresa_cliente_domicilios',
            args = _D_args.L_argsBasicos + ['master_' + str(request.function), _D_args.n_id]
            ),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_cliente_domicilios', s_masterIdentificator = str(request.function))
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_cliente_guiasclientes,
        s_url = URL(
            f = 'empresa_cliente_guiasclientes',
            args = _D_args.L_argsBasicos + ['master_' + str(request.function), _D_args.n_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'empresa_cliente_guiasclientes', s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_cliente_clasificaciones,
        s_url = URL(
            f = 'empresa_cliente_clasificaciones',
            args = _D_args.L_argsBasicos + ['master_' + str(request.function), _D_args.n_id]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'empresa_cliente_clasificaciones', s_masterIdentificator = str(request.function)
            )
        )

    _D_returnVars['htmlid_credito'] = _O_cat.addSubTab(
        dbTableDetail = None, s_url = False,
        s_tabName = "Crédito", s_idHtml = None, s_type = "extended"
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_cliente_log,
        s_url = URL(
            f = 'empresa_cliente_log',
            args = _D_args.L_argsBasicos + ['master_' + str(request.function), _D_args.n_id]
            ),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_cliente_log', s_masterIdentificator = str(request.function))
        )

    return _D_returnVars


def _empresa_cliente_guiasclientes_validation(O_form):
    """ Validación de las guías de cliente.
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_cliente_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """
    _s_moneda_id = O_form.vars.monedacontpaqi_id
    _s_empresa_id =request.args(1, None)
    _s_empresa_cliente_id = request.args(3, None)
    _s_id = O_form.record_id

    if 'ejercicio' in O_form.vars:
        _s_ejercicio = O_form.vars.ejercicio
    else:
        _s_ejercicio = None

    if dbc01(
        (dbc01.tempresa_cliente_guiasclientes.empresa_cliente_id == _s_empresa_cliente_id)
        & (dbc01.tempresa_cliente_guiasclientes.monedacontpaqi_id == _s_moneda_id)
        & (dbc01.tempresa_cliente_guiasclientes.ejercicio == _s_ejercicio)
        & (dbc01.tempresa_cliente_guiasclientes.id != _s_id)
        ).count() > 0:
        O_form.errors.monedacontpaqi_id = 'La moneda-ejercicio ya está registrada'
    else:
        pass

    _s_msgError = VALIDACIONES.EJERCICIO.ES_VALIDO(_s_empresa_id, _s_ejercicio)

    if _s_msgError:
        O_form.errors.id = _s_msgError
    else:
        pass

    return O_form


def empresa_cliente_guiasclientes():
    """ Maneja la configuración de la contabilidad de la sucursal
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_cliente_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
        
    """


    _dbTable = dbc01.tempresa_cliente_guiasclientes
    _s_empresa_id = request.args(1, 'None')
    _s_cliente_id = request.args(3, 'None')
    _s_action = request.args(4, 'None')
    _s_guia_id = request.args(5, None)

    # Si se desea crear un nuevo registro se puede editar la empresa_plaza_sucursal_id
    if not _s_guia_id or (_s_action in request.stv_fwk_permissions.btn_new.code):
        _dbTable.ejercicio.writable = True
        _dbTable.monedacontpaqi_id.writable = True
    else:
        _dbTable.ejercicio.writable = False
        _dbTable.monedacontpaqi_id.writable = False

    _dbTable.monedacontpaqi_id.default = D_stvSiteHelper.dbconfigs.defaults.moneda_contpaqi_id

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_clientes,
        xFieldLinkMaster = [
            _dbTable.empresa_cliente_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.ejercicio,
            _dbTable.empresa_cliente_id,
            _dbTable.monedacontpaqi_id,
            _dbTable.cuenta_cliente,
            _dbTable.cuenta_cliente_complemento,
            _dbTable.cuenta_anticipo,
            _dbTable.cuenta_anticipo_complemento,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.cuenta_cliente,
            _dbTable.cuenta_cliente_complemento
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.cuenta_cliente
            ],
        x_offsetInArgs = request.args[:2]
        )

    _dbRow = dbc01.tempresas(_s_empresa_id)
    _O_cat.addRowContentProperty('formHidden', {'moneda_id_empresa': str(_dbRow.monedacontpaqi_id)})

    _O_cat.addRowContentEvent('onValidation', _empresa_cliente_guiasclientes_validation)

    _D_returnVars = _O_cat.process()


    return _D_returnVars


def empresa_cliente_domicilios():
    """ Formulario para el manejo de los contactos-domicilios fiscales del cliente

    @descripcion Domicilios

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Cliente
    @keyword arg3: cliente_id.
    @keyword [arg4]: operación a realizar en el formulario.
    @keyword [arg5]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    #@permiso special_permisson_01:[fas fa-money-check-alt] Emisión de CFDIs
    #@permiso special_permisson_02:[<i class="fas fa-balance-scale-right] Recalcular saldos
    """
    _D_args = STV_FWK_LIB.DECODIFICAR_ARGS(
        L_args = request.args,
        n_offsetArgs = 4,
        s_codigoDefault = request.stv_fwk_permissions.btn_new.code,
        s_empresa_id = request.args(1, 0),
        s_cliente_id = request.args(3, 0),
        s_domicilio_id = request.args(5, None),
        )

    _dbTable = dbc01.tempresa_cliente_domicilios

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_clientes,
        xFieldLinkMaster = [_dbTable.empresa_cliente_id],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.descripcion, _dbTable.nombrecontacto,
            _dbTable.emails, _dbTable.telefonos,
            _dbTable.ciudad, _dbTable.colonia,
            _dbTable.calle, _dbTable.numexterior, _dbTable.numinterior,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.empresa_cliente_id,
            _dbTable.pais_id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.empresa_cliente_id
            ],
        x_offsetInArgs = _D_args.n_offsetArgs - 2
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def empresa_cliente_log():
    """ Formulario para el manejo de los domicilios fiscales del cliente
    
    @descripcion Log de Procesos
    
    @keyword arg0: master de empresa_id.
    @keyword arg1: empresa_id.
    @keyword arg2: master de cliente_id.
    @keyword arg3: cliente_id.
    @keyword [arg4]: operación a realizar en el formulario.
    @keyword [arg5]: id en caso de existir relación a un registro en la operación.    
        
    @permiso btn_none
    #@permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    #@permiso btn_signature
        
    """

    _dbTable = dbc01.tempresa_cliente_log

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_clientes,
        xFieldLinkMaster = [
            _dbTable.empresa_cliente_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.fecha,
            _dbTable.descripcion,
            _dbTable.tipotransaccion,
            ],
        L_fieldsToSearch = [
            _dbTable.empresa_cliente_id,
            _dbTable.id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.empresa_cliente_id,
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:2]
        )

    _D_returnVars = _O_cat.process()


    return _D_returnVars


def empresa_clientes_buscar():
    """ Forma buscar para productos y servicios
    
    @descripcion Buscar Clientes
    
    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.
    
    @permiso btn_none
    #@permiso btn_new
    #@permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    #@permiso btn_signature
    @permiso btn_searchcancel
    @permiso btn_searchdone
    @permiso special_permisson_04:[fas fa-file-code] Recalcular saldo del Cliente

    """
    _dbTable = dbc01.tempresa_clientes

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formSearchButtons,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.codigo,
            _dbTable.razonsocial,
            _dbTable.nombrecorto,
            _dbTable.tipocliente,
            _dbTable.rfc,
            ],
        L_fieldsToSearch = [
            _dbTable.id,
            _dbTable.codigo,
            _dbTable.razonsocial,
            _dbTable.nombrecorto,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id,
            _dbTable.codigo
            ],
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def empresa_cliente_clasificaciones():
    """ Formulario para el manejo de clasificaciones del cliente
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_cliente_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
        
    """
    _dbTable = dbc01.tempresa_cliente_clasificaciones

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_clientes,
        xFieldLinkMaster = [
            _dbTable.empresa_cliente_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll,
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.empresa_clasificacioncliente_id
            ],
        L_fieldsToSearch = [
            _dbTable.id,
            _dbTable.empresa_clasificacioncliente_id
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:2]
        )

    _O_cat.addRowContentEvent('onValidation', _empresa_cliente_clasificaciones_validation)

    _D_returnVars = _O_cat.process()

    return _D_returnVars


def _empresa_cliente_clasificaciones_validation(O_form):
    """ Validación para clasificaciones de clientes
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: cliente_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
        
    """

    _dbTable = dbc01.tempresa_cliente_clasificaciones
    _s_cliente_id = request.args(3, None)
    _s_id = O_form.record_id

    _dbRows = dbc01(
        (_dbTable.empresa_cliente_id == _s_cliente_id)
        &(_dbTable.empresa_clasificacioncliente_id == O_form.vars.empresa_clasificacioncliente_id)
        &(_dbTable.id != _s_id)
        ).select(
            _dbTable.id
            )
    if _dbRows:
        O_form.errors.empresa_clasificacioncliente_id = 'No se puede repetir la clasificación del cliente.'
    else:
        pass

    return O_form
