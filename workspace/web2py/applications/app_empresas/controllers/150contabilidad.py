# -*- coding: utf-8 -*-

def empresa_series():
    _dbTable = dbc01.tempresa_series

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas, 
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.serie, 
            _dbTable.tipocomprobante_id, 
            _dbTable.folio, 
            _dbTable.bansuspender
            ], 
        L_fieldsToSearch = [
            _dbTable.serie, 
            _dbTable.folio
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            _dbTable.tipocomprobante_id, 
            _dbTable.folio
            ],
        )

    _O_cat.addSubTab(
                dbTableDetail = dbc01.tempresa_plaza_sucursal_cajaschicas, 
                s_url = URL(f='empresa_serie_cajaschicas', args=request.args[:2]+['master_'+str(request.function),request.args(3)]),
                s_idHtml = fn_createIDTabUnique(f = 'empresa_serie_cajaschicas', s_masterIdentificator = str(request.function)))    
        
    _D_returnVars = _O_cat.process()
    

    return (_D_returnVars)

def empresa_serie_cajaschicas():
    """ Formulario para mostrar únicamente las cajas chicas relacionadas a una serie.
    
    @descripcion Cajas Chicas

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Serie
    @keyword arg3: serie_id.
    @keyword [arg4]: operación a realizar en el formulario.
    @keyword [arg5]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    #@permiso btn_new
    #@permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    @permiso btn_signature
    """
    _dbTable = dbc01.tempresa_plaza_sucursal_cajaschicas
    
    _s_empresa_id = request.args(1)
    _s_serie_id = request.args(3)

    _qry = (
        (_dbTable.empresa_serie_ingreso_id == _s_serie_id)
        | (_dbTable.empresa_serie_egreso_id == _s_serie_id)
        | (_dbTable.empresa_serie_pago_id == _s_serie_id)
        )

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        dbTableMaster = None, 
        xFieldLinkMaster = [], 
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none, 
            request.stv_fwk_permissions.btn_cancel, 
            request.stv_fwk_permissions.btn_view, 
            request.stv_fwk_permissions.btn_findall                    
            ], 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.empresa_plaza_sucursal_id, 
            _dbTable.nombre,
            _dbTable.empresa_cuentabancaria_terminal_id,
            _dbTable.empresa_cuentabancaria_id_cheques,
            _dbTable.empresa_cuentabancaria_id_transferencia,
            ], 
        L_fieldsToSearch = [
            _dbTable.nombre
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:4]
        )
        
    _D_returnVars = _O_cat.process()    
  
    return (_D_returnVars)

''' Detalle de empresas pagadoras, correspondiente a registros patronales '''
def empresa_registrospatronales():
    """ Formulario para el manejo de los registros patronales.
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: operación a realizar en el formulario, solo buscar todo es permitido
        [arg3]: id en caso de existir relación a un registro en la operación
    
    """
    
    _dbTable = dbc01.tempresa_registrospatronales
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas, 
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.registropatronal, 
            _dbTable.fraccionleyimss, 
            _dbTable.fechaaltaimss, 
            _dbTable.bansuspender
            ], 
        L_fieldsToSearch = [
            _dbTable.registropatronal, 
            _dbTable.fraccionleyimss, 
            _dbTable.registroinfonacot
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ]
        )

    _D_returnVars = _O_cat.process()

    return (_D_returnVars)

def _empresa_plaza_sucursal_contabilidades_validation(O_form):
    """ Validación de la contabilidad para la plaza sucursal

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """
    
    _dbTable = dbc01.tempresa_plaza_sucursal_contabilidades
    
    _s_empresa_id = request.args(1, 'None')
    
    if 'empresa_plaza_sucursal_id' in O_form.vars:
        _s_empresa_plaza_sucursal_id = O_form.vars.empresa_plaza_sucursal_id
    else:
        _s_empresa_plaza_sucursal_id = None
        
    if 'ejercicio' in O_form.vars:
        _s_ejercicio = O_form.vars.ejercicio
    else:
        _s_ejercicio = None
    
    # Se busca que la plaza sucursal y el ejercicio no se dupliquen
    _dbRows = dbc01(
        (_dbTable.empresa_plaza_sucursal_id == _s_empresa_plaza_sucursal_id) 
        & (_dbTable.ejercicio == _s_ejercicio)
        & (_dbTable.id != O_form.record_id)  # Se ignora el registro que se esté modificando
        ).select(_dbTable.id)
    if _dbRows:
        O_form.errors.empresa_plaza_sucursal_id = "Plaza y ejercicio no puede duplicarse"
    else:
        pass
    
    _s_msgError = VALIDACIONES.EJERCICIO.ES_VALIDO(_s_empresa_id, O_form.vars.ejercicio)
    
    if _s_msgError:
        O_form.errors.id = _s_msgError
    else:
        pass    
    
    return O_form

def empresa_plaza_sucursal_contabilidades():
    """ Maneja la configuración de la contabilidad de la sucursal
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
        
    """
    
    _dbTable = dbc01.tempresa_plaza_sucursal_contabilidades
    _s_empresa_id = request.args(1, 'None')
    _s_action = request.args(2, 'None')
    _s_empresa_plaza_sucursal_contabilidad_id = request.args(3, None)

    # Si se desea crear un nuevo registro se puede editar la empresa_plaza_sucursal_id
    if not(_s_empresa_plaza_sucursal_contabilidad_id) or (_s_action in (request.stv_fwk_permissions.btn_new.code)):
        _dbTable.empresa_plaza_sucursal_id.writable = True
        _dbTable.ejercicio.writable = True
    else:
        _dbTable.empresa_plaza_sucursal_id.writable = False
        _dbTable.ejercicio.writable = False

    _dbQry = ( 
        (dbc01.tempresas.id == _s_empresa_id) &
        (dbc01.tempresa_plazas.empresa_id == dbc01.tempresas.id) &
        (dbc01.tempresa_plaza_sucursales.empresa_plaza_id == dbc01.tempresa_plazas.id) & 
        (_dbTable.empresa_plaza_sucursal_id == dbc01.tempresa_plaza_sucursales.id)
        )
    
    _dbTable.empresa_diario_id.requires = IS_IN_DB(
        dbc01(dbc01.tempresa_diarios.empresa_id == _s_empresa_id), 
        'tempresa_diarios.id', 
        dbc01.tempresa_diarios._format
        )
    
    _dbTable.empresa_segmento_id.requires = IS_IN_DB(
        dbc01(dbc01.tempresa_segmentos.empresa_id == _s_empresa_id), 
        'tempresa_segmentos.id', 
        dbc01.tempresa_segmentos._format
        )
    
    _dbTable.empresa_tipopoliza_ingreso_id.requires = IS_IN_DB(
        dbc01(dbc01.tempresa_tipospoliza.empresa_id == _s_empresa_id), 
        'tempresa_tipospoliza.id', 
        dbc01.tempresa_tipospoliza._format
        )
    
    _dbTable.empresa_tipopoliza_pago_id.requires = IS_IN_DB(
        dbc01(dbc01.tempresa_tipospoliza.empresa_id == _s_empresa_id), 
        'tempresa_tipospoliza.id', 
        dbc01.tempresa_tipospoliza._format
        )
    
    _dbTable.empresa_tipopoliza_egreso_id.requires = IS_IN_DB(
        dbc01(dbc01.tempresa_tipospoliza.empresa_id == _s_empresa_id), 
        'tempresa_tipospoliza.id', 
        dbc01.tempresa_tipospoliza._format
        )

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_dbQry),
        dbTableMaster = None, 
        xFieldLinkMaster = [], 
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.ejercicio, 
            _dbTable.empresa_plaza_sucursal_id, 
            _dbTable.empresa_tipopoliza_ingreso_id, 
            _dbTable.empresa_tipopoliza_pago_id, 
            _dbTable.empresa_tipopoliza_egreso_id, 
            _dbTable.empresa_diario_id, 
            _dbTable.empresa_segmento_id, 
            _dbTable.bansuspender
            ], 
        L_fieldsToSearch = [_dbTable.empresa_tipopoliza_ingreso_id], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            _dbTable.empresa_tipopoliza_ingreso_id, 
            _dbTable.empresa_tipopoliza_pago_id, 
            _dbTable.empresa_tipopoliza_egreso_id, 
            _dbTable.empresa_diario_id
            ],
        x_offsetInArgs = request.args[:2]
        )
    
    _O_cat.addRowContentEvent('onValidation', _empresa_plaza_sucursal_contabilidades_validation)
    
    _b_skipAction = False
    
    #Se utiliza el overrideView a este nivel porque como se va a eliminar el registro tiene que hacerse antes del _O_cat.process()
    _D_overrideView = None
    
    if _s_action in (request.stv_fwk_permissions.btn_remove.code,):
        _dbRowsGuiasContables = dbc01(
            (dbc01.tempresa_plaza_sucursal_contabilidad_guiascontables.empresa_plaza_sucursal_contabilidad_id == _s_empresa_plaza_sucursal_contabilidad_id)
            ).select(
                dbc01.tempresa_plaza_sucursal_contabilidad_guiascontables.id
                )
        
        if _dbRowsGuiasContables:
            # Si esta configuración contable cuenta con guias de ingreso entonces no puede eliminarse
            stv_flashError("Error al elimnar", "Esta configuración contable no puede ser elminada ya que tiene guias de ingreso dentro.") 
            
            # Se usa el _b_skipAction para hacerle bypass al eliminar
            _b_skipAction = True     
            _D_overrideView = request.stv_fwk_permissions.view_refresh           
        else:
            pass
        
    else:
        pass
    
    if _D_overrideView:
        _D_returnVars = _O_cat.process(D_overrideView = _D_overrideView, b_skipAction = _b_skipAction)
    else:
        _D_returnVars = _O_cat.process()
    
    _O_cat.addSubTab(
        s_tabName = 'Guías Ingresos', 
        s_url = URL(f='empresa_plaza_sucursal_contabilidad_guiascontables', args=request.args[:2]+['master_'+str(request.function),request.args(3)]),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_plaza_sucursal_contabilidad_guiascontables', s_masterIdentificator = str(request.function))  
        )

    return (_D_returnVars)

def empresa_plaza_sucursal_cajaschicas():
    """ Formulario para el manejo de las cajas chicas.
    
    @descripcion Cajas Chicas

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword arg2: Plaza
    @keyword arg3: plaza_id.
    @keyword arg4: Sucursal
    @keyword arg5: sucursal_id.
    @keyword [arg6]: operación a realizar en el formulario.
    @keyword [arg7]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    @permiso btn_suspend
    @permiso btn_activate
    @permiso btn_remove
    @permiso btn_find
    @permiso btn_findall
    @permiso btn_signature

    """
    _dbTable = dbc01.tempresa_plaza_sucursal_cajaschicas
    
    _s_empresa_id = request.args(1)
    _s_plaza_id = request.args(3)
    _s_sucursal_id = request.args(5)

    _dbTable.empresa_cuentabancaria_terminal_id.requires = IS_IN_DB(
        dbc01(dbc01.tempresa_cuentabancaria_terminales.empresa_plaza_sucursal_id == _s_sucursal_id), 
        'tempresa_cuentabancaria_terminales.id', 
        dbc01.tempresa_cuentabancaria_terminales._format
        )
    
    _dbTable.empresa_cuentabancaria_id_cheques.requires = IS_IN_DB(
        dbc01(dbc01.tempresa_cuentasbancarias.empresa_id == _s_empresa_id), 
        'tempresa_cuentasbancarias.id', 
        dbc01.tempresa_cuentasbancarias._format
        )
    
    _dbTable.empresa_cuentabancaria_id_transferencia.requires = IS_IN_DB(
        dbc01(dbc01.tempresa_cuentasbancarias.empresa_id == _s_empresa_id), 
        'tempresa_cuentasbancarias.id', 
        dbc01.tempresa_cuentasbancarias._format
        )

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_plaza_sucursales, 
        xFieldLinkMaster = [
            _dbTable.empresa_plaza_sucursal_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.nombre, 
            _dbTable.fondobase, 
            _dbTable.empresa_serie_ingreso_id,
            _dbTable.empresa_serie_egreso_id,
            _dbTable.empresa_serie_pago_id,
            _dbTable.bansuspender
            ],
        L_fieldsToSearch = [
            _dbTable.nombre, 
            _dbTable.cuentacontable
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:4]
        )        
    
    _dbTable.empresa_serie_ingreso_id.requires = IS_IN_DB(
        dbc01(
            (dbc01.tempresa_series.empresa_id == _s_empresa_id) 
            & (dbc01.tempresa_series.tipocomprobante_id == TTIPOSCOMPROBANTES.INGRESO)
            ), 
        'tempresa_series.id', 
        dbc01.tempresa_series._format
        )
    
    _dbTable.empresa_serie_egreso_id.requires = IS_IN_DB(
        dbc01(
            (dbc01.tempresa_series.empresa_id == _s_empresa_id) 
            & (dbc01.tempresa_series.tipocomprobante_id == TTIPOSCOMPROBANTES.EGRESO)
            ), 
        'tempresa_series.id', 
        dbc01.tempresa_series._format
        )
    
    _dbTable.empresa_serie_pago_id.requires = IS_IN_DB(
        dbc01(
            (dbc01.tempresa_series.empresa_id == _s_empresa_id) 
            & (dbc01.tempresa_series.tipocomprobante_id == TTIPOSCOMPROBANTES.PAGO)
            ), 
        'tempresa_series.id', 
        dbc01.tempresa_series._format
        )
    
    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_plaza_sucursal_cajachica_guias, 
        s_url = URL(
            f='empresa_plaza_sucursal_cajachica_guias',
            args= request.args[:6]+['master_'+str(request.function),request.args(7)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'empresa_plaza_sucursal_cajachica_guias',
            s_masterIdentificator = str(request.function)
            )
        )
    
    _D_returnVars = _O_cat.process()
    
    return (_D_returnVars)

def empresa_plaza_sucursal_contabilidad_guiascontables():
    """ Formulario para el manejo de las guías contables de la sucursal. 
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_plaza_sucursal_contabilidad_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    
    """
    
    _dbTable = dbc01.tempresa_plaza_sucursal_contabilidad_guiascontables
    
    _s_empresa_id = request.args(1)
    _s_contabilidad_id = request.args(3)
    _s_action = request.args(4, request.stv_fwk_permissions.btn_none.code)
    if (_s_action in [request.stv_fwk_permissions.btn_edit.code,]):
        _s_guia_id = request.args(5, None)
    else:
        _s_guia_id = None
    
    
    _dbQry = (
        (dbc01.tempresa_ingresosestructura.empresa_id == _s_empresa_id)
        & (dbc01.tempresa_plaza_sucursal_contabilidad_guiascontables.id != None)
        & (dbc01.tempresa_plaza_sucursal_contabilidad_guiascontables.id != _s_guia_id)
        )
    
    _dbRows_filter = dbc01(_dbQry).select(
        dbc01.tempresa_ingresosestructura.id, 
        left = [
            dbc01.tempresa_plaza_sucursal_contabilidad_guiascontables.on(
                (dbc01.tempresa_plaza_sucursal_contabilidad_guiascontables.empresa_ingresoestructura_id == dbc01.tempresa_ingresosestructura.id)
                & (dbc01.tempresa_plaza_sucursal_contabilidad_guiascontables.empresa_plaza_sucursal_contabilidad_id == _s_contabilidad_id)
                )            
            ],
        groupby = [
            dbc01.tempresa_ingresosestructura.id, 
            dbc01.tempresa_ingresosestructura.nombrecorto,
            dbc01.tempresa_ingresosestructura.padre_id,
            dbc01.tempresa_ingresosestructura.orden,
            dbc01.tempresa_plaza_sucursal_contabilidad_guiascontables.id,
            ]
        )
    _dbTable.empresa_ingresoestructura_id.widget = lambda f, v: stv_widget_db_chosen_tree(
        f, 
        v, 
        1, 
        D_additionalAttributes = Storage(
            dbRows_tree_filter = _dbRows_filter,
            dbTable_tree = dbc01.tempresa_ingresosestructura,
            dbField_order = dbc01.tempresa_ingresosestructura.orden,
            dbField_tree_groupby = dbc01.tempresa_ingresosestructura.padre_id,
            class_dbTree = DB_Tree,
            b_onlyLastNodes = True,
            )
        )

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_plaza_sucursal_contabilidades, 
        xFieldLinkMaster = [
            _dbTable.empresa_plaza_sucursal_contabilidad_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.empresa_ingresoestructura_id, 
            _dbTable.cuentacontableingreso, 
            _dbTable.cuentacontabledescto, 
            _dbTable.cuentacontableingreso_nocobrado,
            _dbTable.bansuspender
            ], 
        L_fieldsToSearch = [
            _dbTable.cuentacontableingreso, 
            _dbTable.cuentacontabledescto,
            dbc01.tempresa_ingresosestructura.nombrecorto
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            _dbTable.empresa_ingresoestructura_id
            ],
        x_offsetInArgs = request.args[:2]
        )

    _O_cat.addRowSearchResultsLeftJoin(
        dbc01.tempresa_ingresosestructura.on(
            (dbc01.tempresa_ingresosestructura.id == _dbTable.empresa_ingresoestructura_id)
            )
        )
    
    _O_cat.addRowContentEvent('onValidation', _empresa_plaza_sucursal_contabilidad_guiascontables_validation)
    _D_returnVars = _O_cat.process()            
    # TODO agregar validación de que empresa_ingresoestructura_id no se puede duplicar


    return (_D_returnVars)


def _empresa_plaza_sucursal_contabilidad_guiascontables_validation(O_form):
    """ Validación para evitar duplicado de la estructura de ingreso por guía contable.

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_plaza_sucursal_contabilidad_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    
    """
    _s_empresa_ingresoestructura_id = O_form.vars.empresa_ingresoestructura_id
    
    _s_cuentacontableingreso = O_form.vars.cuentacontableingreso
    
    _s_empresa_plaza_sucursal_contabilidad_id = request.args(3, None) # Creo que seriia la 5
    
    _dbTable = dbc01.tempresa_plaza_sucursal_contabilidad_guiascontables
    
    _dbRows = dbc01(
        (_dbTable.empresa_ingresoestructura_id == _s_empresa_ingresoestructura_id) 
        & (_dbTable.empresa_plaza_sucursal_contabilidad_id == _s_empresa_plaza_sucursal_contabilidad_id)
        & (_dbTable.id != O_form.record_id)  # Se ignora el registro que se esté modificando
        ).select(_dbTable.id)
    if _dbRows:
        O_form.errors.cuentacontableingreso = "La estructura de ingreso no puede ser duplicada"
    
    return O_form
        

def _empresa_ingresoestructura_guiascontables_validation(O_form):
    """ Validación de guia contable > estructura de ingreso > empresa

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_ingresoestructura_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """
    
    _s_empresa_id = request.args(1, None)
    
    _s_empresa_ingresoestructura_id = request.args(3, 'None')
    
    _dbTable = dbc01.tempresa_ingresoestructura_guiascontables
    
    if 'ejercicio' in O_form.vars:
        _s_ejercicio = O_form.vars.ejercicio
    else:
        _s_ejercicio = None
    
    # Se busca que la tasa no se este utilizando con anterioridad en la misma empresa
    _dbRows = dbc01(
        (_dbTable.empresa_ingresoestructura_id == _s_empresa_ingresoestructura_id) 
        & (_dbTable.ejercicio == _s_ejercicio)
        & (_dbTable.id != O_form.record_id)  # Se ignora el registro que se esté modificando
        ).select(_dbTable.id)
        
    if _dbRows:
        O_form.errors.ejercicio = "Estructura de Ingreso y Ejercicio no puede duplicarse"
    else:
        pass
    
    _s_msgError = VALIDACIONES.EJERCICIO.ES_VALIDO(_s_empresa_id, _s_ejercicio)
    
    if _s_msgError:
        O_form.errors.id = _s_msgError
    else:
        pass
    
    return O_form

def empresa_ingresoestructura_guiascontables():
    """ Detalle de guia contable > estructura de ingreso > empresa

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_ingresoestructura_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """
    
    _dbTable = dbc01.tempresa_ingresoestructura_guiascontables

    _s_action = request.args(4, 'None')
    _s_empresa_ingresoestructura_id = request.args(5, None)

    # Si se desea crear un nuevo registro se puede editar la empresa_ingresoestructura_id
    if not(_s_empresa_ingresoestructura_id) or (_s_action in (request.stv_fwk_permissions.btn_new.code)):
        _dbTable.ejercicio.writable = True
    else:
        _dbTable.ejercicio.writable = False

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_ingresosestructura, 
        xFieldLinkMaster = [
            _dbTable.empresa_ingresoestructura_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.ejercicio,
            _dbTable.cuentacontableingreso, 
            _dbTable.cuentacontabledescto, 
            _dbTable.bansuspender
            ], 
        L_fieldsToSearch = [
            _dbTable.cuentacontableingreso, 
            _dbTable.cuentacontabledescto
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            _dbTable.empresa_ingresoestructura_id
            ],
        x_offsetInArgs = request.args[:2]
        )
                
    _O_cat.addRowContentEvent('onValidation', _empresa_ingresoestructura_guiascontables_validation)

    _D_returnVars = _O_cat.process()

    return (_D_returnVars)

def empresa_plaza_sucursal_cajachica_guias():
    """ Maneja la configuración de la contabilidad de la caja chica
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_plaza_id
        arg4: master_[nombre función maestro]
        arg5: empresa_plaza_sucursal_id
        arg6: master_[nombre función maestro]
        arg7: empresa_plaza_sucursal_cajaschicas_id
        [arg8]: codigo de funcionalidad
        [arg9]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
        
        
    """
    
    _dbTable = dbc01.tempresa_plaza_sucursal_cajachica_guias
    
    _s_empresa_id = request.args(1, None)
    _s_plaza_id = request.args(3, None)
    _s_sucursal_id = request.args(5, None)
    _s_cajachica_id = request.args(7, None)
    
    _s_action = request.args(8, 'None')
    _s_guia_id = request.args(9, None)

    # Si se desea crear un nuevo registro se puede editar la empresa_ingresoestructura_id
    if not(_s_guia_id) or (_s_action in (request.stv_fwk_permissions.btn_new.code)):
        _dbTable.ejercicio.writable = True
        _dbTable.monedacontpaqi_id.writable = True
    else:
        _dbTable.ejercicio.writable = False    
        _dbTable.monedacontpaqi_id.writable = False    

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_plaza_sucursal_cajaschicas, 
        xFieldLinkMaster = [
            _dbTable.empresa_plaza_sucursal_cajaschicas_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.empresa_plaza_sucursal_cajaschicas_id, 
            _dbTable.ejercicio, 
            _dbTable.monedacontpaqi_id, 
            _dbTable.cuenta_contable_cajachica,
            _dbTable.cuenta_contable_cajachica_sobrante,
            _dbTable.cuenta_contable_cajachica_faltante,
            _dbTable.cuenta_contable_cajachica_complemento
            ],
        L_fieldsToSearch = [
            _dbTable.cuenta_contable_cajachica, 
            _dbTable.cuenta_contable_cajachica_complemento
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            _dbTable.ejercicio
            ],
        x_offsetInArgs = request.args[:6]
        ) 
    
    _dbRow = dbc01.tempresas(_s_empresa_id)
    _O_cat.addRowContentProperty('formHidden', {'moneda_id_empresa': str(_dbRow.monedacontpaqi_id)})
    
    _O_cat.addRowContentEvent('onValidation', empresa_plaza_sucursal_cajachica_guias_validation)
                
    _D_returnVars = _O_cat.process()

    return (_D_returnVars)


def empresa_plaza_sucursal_cajachica_guias_validation(O_form):
    """ Validación para las guías contables por caja chica. 
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_plaza_id
        arg4: master_[nombre función maestro]
        arg5: empresa_plaza_sucursal_id
        arg6: master_[nombre función maestro]
        arg7: empresa_plaza_sucursal_cajaschicas_id
        [arg8]: codigo de funcionalidad
        [arg9]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
        
    """
    
    _s_moneda_id = O_form.vars.monedacontpaqi_id
    _s_ejercicio = O_form.vars.ejercicio
    _s_empresa_id = request.args(1, None)
    _s_cajachica_id = request.args(7, None)
    _s_id = O_form.record_id
    
    if dbc01( 
        (dbc01.tempresa_plaza_sucursal_cajachica_guias.empresa_plaza_sucursal_cajaschicas_id == _s_cajachica_id)
        & (dbc01.tempresa_plaza_sucursal_cajachica_guias.monedacontpaqi_id == _s_moneda_id) 
        & (dbc01.tempresa_plaza_sucursal_cajachica_guias.ejercicio == _s_ejercicio) 
        & (dbc01.tempresa_plaza_sucursal_cajachica_guias.id != _s_id) ).count() > 0:
        O_form.errors.monedacontpaqi_id = 'La relación entre moneda y ejercicio no puede repetirse'
    else:
        pass
    
    _s_msgError = VALIDACIONES.EJERCICIO.ES_VALIDO(_s_empresa_id, _s_ejercicio)
    
    if _s_msgError:
        O_form.errors.id = _s_msgError
    else:
        pass
        
    return O_form


def _empresa_iva_validation(O_form):
    """ Validación de la inserción de IVA

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
        
    """
    _s_empresa_id = request.args(1, None)
    _s_ejercicio = O_form.vars.ejercicio
    _s_id = O_form.record_id
    _s_tasa = O_form.vars.tasa
    
    if not _s_empresa_id:
        O_form.errors.id = "No se encontró referencia a empresa válida"
    else:
        _s_tasa = O_form.vars.tasa
        # Se busca que la tasa no se este utilizando con anterioridad en la misma empresa
        _dbRows = dbc01(
            (dbc01.tempresa_iva.empresa_id == _s_empresa_id) 
            & (dbc01.tempresa_iva.tasa == _s_tasa)
            & (dbc01.tempresa_iva.id != O_form.record_id)   # Se ignora el registro que se esté modificando
            & (dbc01.tempresa_iva.ejercicio == _s_ejercicio) 
            ).select(dbc01.tempresa_iva.id)
        if _dbRows:
            O_form.errors.tasa = "Tasa IVA no puede duplicarse"
            O_form.errors.ejercicio = 'La relación entre tasa y ejercicio no puede repetirse'
        else:
            pass
    
    _s_msgError = VALIDACIONES.EJERCICIO.ES_VALIDO(_s_empresa_id, _s_ejercicio)
    
    if _s_msgError:
        O_form.errors.id = _s_msgError
    else:
        pass

            
    return O_form

def empresa_iva():
    """ Detalle de iva > empresa
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
        
    """

    _dbTable = dbc01.tempresa_iva
    _s_empresa_id = request.args(1, None)
    _s_action = request.args(2, 'None')
    _s_impuesto_id = request.args(3, None)

    # Si se desea crear un nuevo registro se puede editar la _s_guia_id
    if not(_s_impuesto_id) or (_s_action in (request.stv_fwk_permissions.btn_new.code)):
        _dbTable.ejercicio.writable = True
        _dbTable.tasa.writable = True
    else:
        _dbTable.ejercicio.writable = False
        _dbTable.tasa.writable = False
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas, 
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.ejercicio,
            _dbTable.tasa, 
            _dbTable.bansuspender,
            _dbTable.cuentacontable_ivatrasladado, 
            _dbTable.cuentacontable_ivaportrasladar, 
            _dbTable.cuentacontable_ivaacreditable, 
            _dbTable.cuentacontable_ivaacreditableimportaciones, 
            _dbTable.cuentacontable_ivaporacreditar, 
            _dbTable.cuentacontable_ivaporacreditarimportaciones, 
            _dbTable.cuentacontable_ivaafavorretenido, 
            _dbTable.cuentacontable_ivaafavorporretener, 
            _dbTable.cuentacontable_ivaretenido, 
            _dbTable.cuentacontable_ivaporretener
            ], 
        L_fieldsToSearch = [_dbTable.tasa], 
        L_fieldsToSearchIfDigit = [_dbTable.id, _dbTable.tasa]
        )
    
    _O_cat.addRowContentEvent('onValidation', _empresa_iva_validation)

    _D_returnVars = _O_cat.process()

    return (_D_returnVars)


def _empresa_isr_validation(O_form):
    """ Validación de la inserción de ISR
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
        
    """
    
    _s_empresa_id = request.args(1, None)
    _s_ejercicio = O_form.vars.ejercicio
    
    if not _s_empresa_id:
        O_form.errors.id = "No se encontró referencia a empresa válida"
    else:
        _s_tasa = O_form.vars.tasa
        # Se busca que la tasa no se este utilizando con anterioridad en la misma empresa
        _dbRows = dbc01(
            (dbc01.tempresa_isr.empresa_id == _s_empresa_id) 
            & (dbc01.tempresa_isr.tasa == _s_tasa)
            & (dbc01.tempresa_isr.id != O_form.record_id)  # Se ignora el registro que se esté modificando
            & (dbc01.tempresa_isr.ejercicio == _s_ejercicio) 
            ).select(dbc01.tempresa_isr.id)
        if _dbRows:
            O_form.errors.tasa = "Tasa ISR no puede duplicarse"
            O_form.errors.ejercicio = 'La relación entre tasa y ejercicio no puede repetirse'
        else:
            pass
        
    _s_msgError = VALIDACIONES.EJERCICIO.ES_VALIDO(_s_empresa_id, _s_ejercicio)
    
    if _s_msgError:
        O_form.errors.id = _s_msgError
    else:
        pass
    
    return O_form

def empresa_isr():
    """ Detalle de isr > empresa
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
        
    """

    _dbTable = dbc01.tempresa_isr
    _s_empresa_id = request.args(1, None)
    _s_action = request.args(2, 'None')
    _s_impuesto_id = request.args(3, None)

    # Si se desea crear un nuevo registro se puede editar la _s_guia_id
    if not(_s_impuesto_id) or (_s_action in (request.stv_fwk_permissions.btn_new.code)):
        _dbTable.ejercicio.writable = True
        _dbTable.tasa.writable = True
    else:
        _dbTable.ejercicio.writable = False
        _dbTable.tasa.writable = False
            
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas, 
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.ejercicio,
            _dbTable.tasa, 
            _dbTable.bansuspender,
            _dbTable.cuentacontable_israfavorretenido, 
            _dbTable.cuentacontable_israfavorporretener, 
            _dbTable.cuentacontable_isrretenido, 
            _dbTable.cuentacontable_isrporretener
            ], 
        L_fieldsToSearch = [_dbTable.tasa], 
        L_fieldsToSearchIfDigit = [_dbTable.id, _dbTable.tasa]
        )

    _O_cat.addRowContentEvent('onValidation', _empresa_isr_validation)

    _D_returnVars = _O_cat.process()

    return (_D_returnVars)

def _empresa_ieps_validation(O_form):
    """ Validación de la inserción de IEPS
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
        
    """
    _s_empresa_id = request.args(1, None)
    _s_ejercicio = O_form.vars.ejercicio
    
    if not _s_empresa_id:
        O_form.errors.id = "No se encontró referencia a empresa válida"
    else:
        _s_tasa = O_form.vars.tasa
        # Se busca que la tasa no se este utilizando con anterioridad en la misma empresa
        _dbRows = dbc01(
            (dbc01.tempresa_ieps.empresa_id == _s_empresa_id) 
            & (dbc01.tempresa_ieps.tasa == _s_tasa)
            & (dbc01.tempresa_ieps.id != O_form.record_id)  # Se ignora el registro que se esté modificando                
            & (dbc01.tempresa_ieps.ejercicio == _s_ejercicio)
            ).select(
            dbc01.tempresa_ieps.id
            )
        if _dbRows:
            O_form.errors.tasa = "Tasa IEPS no puede duplicarse"
            O_form.errors.ejercicio = 'La relación entre tasa y ejercicio no puede repetirse'
        else:
            pass
        
    _s_msgError = VALIDACIONES.EJERCICIO.ES_VALIDO(_s_empresa_id, _s_ejercicio)
    
    if _s_msgError:
        O_form.errors.id = _s_msgError
    else:
        pass
    
    return O_form

def empresa_ieps():
    """ Detalle de ieps > empresa
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
        
    """

    _dbTable = dbc01.tempresa_ieps
    _s_empresa_id = request.args(1, None)
    _s_action = request.args(2, 'None')
    _s_impuesto_id = request.args(3, None)

    # Si se desea crear un nuevo registro se puede editar la _s_guia_id
    if not(_s_impuesto_id) or (_s_action in (request.stv_fwk_permissions.btn_new.code)):
        _dbTable.ejercicio.writable = True
        _dbTable.tasa.writable = True
    else:
        _dbTable.ejercicio.writable = False
        _dbTable.tasa.writable = False
            
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas, 
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.ejercicio,
            _dbTable.tasa, 
            _dbTable.bansuspender,
            _dbTable.cuentacontable_iepstrasladado, 
            _dbTable.cuentacontable_iepsportrasladar, 
            _dbTable.cuentacontable_iepsacreditable, 
            _dbTable.cuentacontable_iepsacreditableimportaciones, 
            _dbTable.cuentacontable_iepsporacreditar, 
            _dbTable.cuentacontable_iepsporacreditarimportaciones
            ], 
        L_fieldsToSearch = [_dbTable.tasa], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            _dbTable.tasa
            ]
        )
    
    _O_cat.addRowContentEvent('onValidation', _empresa_ieps_validation)

    _D_returnVars = _O_cat.process()

    return (_D_returnVars)
       
def empresa_conceptosasientoscontables():
    """ Detalle de cuentas bancarias correspondiente a la empresa
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
        
    """
    
    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_find)    
    
    _dbTable = dbc01.tempresa_conceptosasientoscontables
    _s_empresa_id = request.args(1, None)
    _s_action = request.args(2, request.stv_fwk_permissions.btn_none.code)    
    _s_cuentabancaria_id = request.args(3, None)
    

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas, 
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.descripcion, 
            _dbTable.ingreso_efectivo, 
            ], 
        L_fieldsToSearch = [
            _dbTable.descripcion, 
            _dbTable.ingreso_efectivo, 
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
    )
        
    _D_returnVars = _O_cat.process()
    
    return (_D_returnVars)

def empresa_gastoscontabilidad():
    """ Función para la contabilidad de los gastos por ejercicio

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """
    
    _dbTable = dbc01.tempresa_gastoscontabilidad
    
    _s_action = request.args(2, None)

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas, 
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.ejercicio,
            _dbTable.empresa_organizacionestructura_id
            ], 
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
        )
    
    _O_cat.addRowContentEvent('onValidation', _empresa_gastoscontabilidad_validation)
    
    _O_cat.addSubTab(
        s_tabName = "Guías contables",
        s_url = URL(f='empresa_gastocontabilidad_guiascontables',  args=request.args[:2]+['master_'+str(request.function),request.args(3)]),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_gastocontabilidad_guiascontables', s_masterIdentificator = str(request.function))
        )
                
    _D_returnVars = _O_cat.process()
    
    if (_s_action == request.stv_fwk_permissions.btn_save.code) and (_D_returnVars.rowContent_form.accepted):
        
        #Se importa gluon.storage para poder crear un objeto especial de lista para request.args
        from gluon.storage import List
        request.args = List([request.args(0),request.args(1),request.stv_fwk_permissions.btn_view.code, _D_returnVars.rowContent_form.vars.id])
        _D_returnVars = empresa_gastoscontabilidad()
        
    else:
        pass

    return (_D_returnVars)

def _empresa_gastoscontabilidad_validation(O_form):
    """ Validación de la inserción de gastos contabilidad

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
        
    """
    
    _dbTable = dbc01.tempresa_gastoscontabilidad

    _s_empresa_id = request.args(1, None)
    
    if not _s_empresa_id:
        O_form.errors.id = "No se encontró referencia a empresa válida"
    else:
        # Se busca que la tasa no se este utilizando con anterioridad en la misma empresa
        _dbRows = dbc01(
            (_dbTable.empresa_id == _s_empresa_id) 
            & (_dbTable.ejercicio == O_form.vars.ejercicio)
            & (_dbTable.empresa_organizacionestructura_id == O_form.vars.empresa_organizacionestructura_id)
            & (_dbTable.id != O_form.record_id)
            ).select(
                _dbTable.id
                )
        if _dbRows:
            O_form.errors.ejercicio = "La combinación de ejercicio y organización no puede repetirse."
            O_form.errors.empresa_organizacionestructura_id = "La combinación de ejercicio y organización no puede repetirse."
        else:
            pass
        
    _s_msgError = VALIDACIONES.EJERCICIO.ES_VALIDO(_s_empresa_id, O_form.vars.ejercicio)
    
    if _s_msgError:
        O_form.errors.id = _s_msgError
    else:
        pass
    
    return O_form

def empresa_gastocontabilidad_guiascontables():
    """ Función para las guías contables de los gastos.

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_gastocontabilidad_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """
    
    _dbTable = dbc01.tempresa_gastocontabilidad_guiascontables

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_gastoscontabilidad, 
        xFieldLinkMaster = [
            _dbTable.empresa_gastocontabilidad_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.empresa_gastoestructura_id,
            _dbTable.empresa_centrocotosestructura_id,
            _dbTable.cuentacontable
            ], 
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
        x_offsetInArgs = request.args[:2]
        )
    
    _O_cat.addRowContentEvent('onValidation', _empresa_gastocontabilidad_guiascontbales_validation)
                
    _D_returnVars = _O_cat.process()

    return (_D_returnVars)

def _empresa_gastocontabilidad_guiascontbales_validation(O_form):
    """ Validación de la inserción de las guías contables.
   
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_gastocontabilidad_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """
    
    _dbTable = dbc01.tempresa_gastocontabilidad_guiascontables

    _s_empresa_gastocontabilidad_id = request.args(3, None)
    
    # Se busca que la tasa no se este utilizando con anterioridad en la misma empresa
    _dbRows = dbc01(
        (_dbTable.empresa_gastocontabilidad_id == _s_empresa_gastocontabilidad_id) 
        & (_dbTable.empresa_gastoestructura_id == O_form.vars.empresa_gastoestructura_id)
        & (_dbTable.empresa_centrocotosestructura_id == O_form.vars.empresa_centrocotosestructura_id)
        & (_dbTable.id != O_form.record_id)
        ).select(
            _dbTable.id
            )
    if _dbRows:
        O_form.errors.empresa_gastoestructura_id = "La combinación gasto y centro de costo no puede repetirse."
        O_form.errors.empresa_centrocotosestructura_id = "La combinación gasto y centro de costo no puede repetirse."
    else:
        pass
    
    if not O_form.vars.cuentacontable:
        O_form.errors.cuentacontable = "La cuenta contable no puede estar vacía"
    else:
        pass
    
    return O_form
