# -*- coding: utf-8 -*-

def empresa_inventarios():
    """ Detalle de inventario para manejar lo relativo a los inventarios.

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """    

    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_find)    
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbTable = None, 
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none
            ], 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [], 
        L_fieldsToSearch = [], 
        L_fieldsToSearchIfDigit = [],
        x_offsetInArgs = request.args[:2]
    )

    _O_cat.addSubTab(
                dbTableDetail = dbc01.tempresa_plaza_sucursal_almacenes, 
                s_url = URL(f='empresa_plaza_sucursal_almacenes', args=['master_'+str(request.function),request.args(1)]),
                s_idHtml = fn_createIDTabUnique(f = 'empresa_plaza_sucursal_almacenes', s_masterIdentificator = str(request.function))  )
    
    _O_cat.addSubTab(
                dbTableDetail = dbc01.tempresa_almacenesconceptos, 
                s_url = URL(f='empresa_almacenesconceptos', args=['master_'+str(request.function),request.args(1)]),
                s_idHtml = fn_createIDTabUnique(f = 'empresa_almacenesconceptos', s_masterIdentificator = str(request.function))  )

    _O_cat.addSubTab(
                dbTableDetail = dbc01.tempresa_prodcategorias, 
                s_url = URL(f='empresa_inventarioscategoriasestructura', args=['master_'+str(request.function),request.args(1)]),
                s_idHtml = fn_createIDTabUnique(f = 'empresa_inventarioscategoriasestructura', s_masterIdentificator = str(request.function))  )

    _O_cat.addSubTab(
                dbTableDetail = dbc01.tempresa_prodserv, 
                s_url = URL(c='120empresas', f='empresa_prodserv', args=['master_'+str(request.function),request.args(1)]),
                s_idHtml = fn_createIDTabUnique(c='120empresas', f = 'empresa_prodserv', s_masterIdentificator = str(request.function)))

    _O_cat.addSubTab(
                dbTableDetail = dbc01.tempresa_inventariomovimientos, 
                s_url = URL(f='empresa_inventariomovimientos', args=['master_'+str(request.function),request.args(1)]),
                s_idHtml = fn_createIDTabUnique(f = 'empresa_inventariomovimientos', s_masterIdentificator = str(request.function))  )
   
    _D_returnVars = _O_cat.process(D_overrideView = request.stv_fwk_permissions.view_viewRegister)

    return dict(_D_returnVars)


def empresa_plaza_sucursal_almacenes():
    """ Almacenes por sucursal

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """

    _dbTable = dbc01.tempresa_plaza_sucursal_almacenes
    _s_empresa_id = request.args(1, None)

    _dbTable.empresa_serie_traslado_id.requires = IS_IN_DB(
        dbc01(
            (dbc01.tempresa_series.empresa_id == _s_empresa_id)
            & (dbc01.tempresa_series.tipocomprobante_id == TTIPOSCOMPROBANTES.TRASLADO)
            ),
        'tempresa_series.id',
        dbc01.tempresa_series._format
        )

    _dbTable.empresa_plaza_sucursal_id.requires = IS_IN_DB(
        dbc01(
            (dbc01.tempresa_plazas.empresa_id == _s_empresa_id)
            &(dbc01.tempresa_plaza_sucursales.empresa_plaza_id == dbc01.tempresa_plazas.id)
         ), 
        'tempresa_plaza_sucursales.id', 
        dbc01.tempresa_plaza_sucursales._format
        )
        
    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.nombre,
        _dbTable.empresa_plaza_sucursal_id,
        _dbTable.ciudad,
        _dbTable.calle,
        _dbTable.empresa_serie_traslado_id,
        ]
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow, 
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
        x_offsetInArgs = request.args[:2],
        )
    
    _D_returnVars = _O_cat.process()
    
    _D_returnVars['htmlid_domiciliofiscal'] = _O_cat.addSubTab(
        dbTableDetail = None,
        s_url = False,
        s_tabName = "Domicilio fiscal",
        s_idHtml = None,
        s_type = "extended"
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_plaza_sucursal_almacen_almacenistas,
        s_url = URL(
            f='empresa_plaza_sucursal_almacen_almacenistas',
            args=request.args[:2]+['master_'+str(request.function),request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'empresa_plaza_sucursal_almacen_almacenistas',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_plaza_sucursal_almacen_ejercicios,
        s_url = URL(
            f='empresa_plaza_sucursal_almacen_ejercicios',
            args=request.args[:2]+['master_'+str(request.function),request.args(3)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'empresa_plaza_sucursal_almacen_ejercicios',
            s_masterIdentificator = str(request.function)
            )
        )


    return (_D_returnVars)


def empresa_plaza_sucursal_almacen_almacenistas():
    """ Formulario de almacenistas.

        @descripcion Almacenistas

        @keyword arg0: Empresa
        @keyword arg1: empresa_id.
        @keyword arg0: Pla-Suc-Almacen
        @keyword arg1: pla_suc_almacen_id.
        @keyword [arg2]: operación a realizar en el formulario.
        @keyword [arg3]: id en caso de existir relación a un registro en la operación.

        @permiso btn_none
        @permiso btn_new
        @permiso btn_save
        @permiso btn_cancel
        @permiso btn_edit
        @permiso btn_view
        @permiso btn_suspend
        @permiso btn_activate
        @permiso btn_remove
        @permiso btn_find
        @permiso btn_findall
        @permiso btn_signature
    """

    def validacion(O_form):
        """ Validación para los almacenistas
        """

        _s_almacen_id = request.args(3, None)
        _n_rol = int(O_form.vars.rol)
        _s_usuario_id = O_form.vars.usuario_id
        _s_id = O_form.record_id

        if (_n_rol == TEMPRESA_PLAZA_SUCURSAL_ALMACEN_ALMACENISTAS.E_ROL.ALMACENISTA):

            if dbc01(
                    (dbc01.tempresa_plaza_sucursal_almacen_almacenistas.empresa_plaza_sucursal_almacen_id == _s_almacen_id)
                    & (dbc01.tempresa_plaza_sucursal_almacen_almacenistas.rol == _n_rol)
                    & (dbc01.tempresa_plaza_sucursal_almacen_almacenistas.id != _s_id)
                    ).count() > 0:
                O_form.errors.rol = 'Solo puede haber un rol de "Almacenista" por almacén.'

        else:
            # No pasa nada el rol de "Auxiliar" puede tenerlo más de una persona.
            pass

        if dbc01(
                (dbc01.tempresa_plaza_sucursal_almacen_almacenistas.empresa_plaza_sucursal_almacen_id == _s_almacen_id)
                & (dbc01.tempresa_plaza_sucursal_almacen_almacenistas.usuario_id == _s_usuario_id)
                & (dbc01.tempresa_plaza_sucursal_almacen_almacenistas.id != _s_id)
                ).count() > 0:
            O_form.errors.usuario_id = 'No puede duplicarse el usuario en un mismo almacén.'
        else:
            pass

        return O_form

    _dbTable = dbc01.tempresa_plaza_sucursal_almacen_almacenistas

    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.usuario_id,
        _dbTable.rol,
        _dbTable.fecharegistro,
        _dbTable.fechafinal
        ]
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_plaza_sucursal_almacenes,
        xFieldLinkMaster = [
            _dbTable.empresa_plaza_sucursal_almacen_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow, 
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
        x_offsetInArgs = request.args[:2],
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION,
        validacion
        )
    
    _D_returnVars = _O_cat.process()

    return (_D_returnVars)

def empresa_plaza_sucursal_almacen_ejercicios():
    """ Formulario detalle para la cuenta contable maestro y ejercicio del almacén.

        @descripcion Ejercicios

        @keyword arg0: Empresa
        @keyword arg1: empresa_id.
        @keyword arg0: Pla-Suc-Almacen
        @keyword arg1: pla_suc_almacen_id.
        @keyword [arg2]: operación a realizar en el formulario.
        @keyword [arg3]: id en caso de existir relación a un registro en la operación.

        @permiso btn_none
        @permiso btn_new
        @permiso btn_save
        @permiso btn_cancel
        @permiso btn_edit
        @permiso btn_view
        @permiso btn_suspend
        @permiso btn_activate
        @permiso btn_remove
        @permiso btn_find
        @permiso btn_findall
        @permiso btn_signature
    """

    def validacion(O_form):
        """ Validacion de empresa_plaza_sucursal_almacen_ejercicios
        """

        _dbTable = dbc01.tempresa_plaza_sucursal_almacen_ejercicios
        _s_empresa_id = request.args(1, None)
        _s_empresa_plaza_sucursal_almacen_id = request.args(3, None)

        _dbRows = dbc01(
            (_dbTable.empresa_plaza_sucursal_almacen_id == _s_empresa_plaza_sucursal_almacen_id)
            & (_dbTable.ejercicio == O_form.vars.ejercicio)
            & (_dbTable.id != O_form.record_id)  # Se ignora el registro que se esté modificando
            ).select(_dbTable.id)
        if _dbRows:
            O_form.errors.ejercicio = "Ya se dio de alta este ejercicio."
        else:
            pass

        if not O_form.vars.ejercicio:
            O_form.errors.ejercicio = "El ejercicio no puede estar vacío."
        else:
            pass

        _s_msgError = VALIDACIONES.EJERCICIO.ES_VALIDO(_s_empresa_id, O_form.vars.ejercicio)

        if _s_msgError:
            O_form.errors.id = _s_msgError
        else:
            pass

        return O_form

    _dbTable = dbc01.tempresa_plaza_sucursal_almacen_ejercicios
        
    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.ejercicio,
        _dbTable.cuentacontable,
        ]
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_plaza_sucursal_almacenes,
        xFieldLinkMaster = [
            _dbTable.empresa_plaza_sucursal_almacen_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow, 
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
        x_offsetInArgs = request.args[:2],
        )
    
    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_plaza_sucursal_almacen_ejercicio_guiascontables,
        s_url = URL(
            f='empresa_plaza_sucursal_almacen_ejercicio_guiascontables',
            args=request.args[:4]+['master_'+str(request.function), request.args(5)]
            ),
        s_idHtml = fn_createIDTabUnique(
            f = 'empresa_plaza_sucursal_almacen_ejercicio_guiascontables',
            s_masterIdentificator = str(request.function)
            )
        )

    _O_cat.addRowContentEvent(
        STV_FWK_FORM.ROWCONTENT.E_EVENT.ON_VALIDATION,
        validacion
        )
    
    _D_returnVars = _O_cat.process()
    
    return (_D_returnVars)

def empresa_plaza_sucursal_almacen_ejercicio_guiascontables():
    """ Formulario detalle para las guías contables por almacén

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_plaza_sucursal_almacen_id
        arg4: master_[nombre función maestro]
        arg5: empresa_plaza_sucursal_almacen_ejercicio_id
        [arg6]: codigo de funcionalidad
        [arg7]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """ 

    _dbTable = dbc01.tempresa_plaza_sucursal_almacen_ejercicio_guiascontables
        
    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.empresa_ingresoestructura_id,
        _dbTable.cuentacontable,
        ]
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_plaza_sucursal_almacen_ejercicios,
        xFieldLinkMaster = [
            _dbTable.empresa_plaza_sucursal_almacen_ejercicio_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow, 
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
        x_offsetInArgs = request.args[:4],
        )

    _O_cat.addRowContentEvent('onValidation', _empresa_plaza_sucursal_almacen_ejercicio_guiascontables_validation)
    
    _D_returnVars = _O_cat.process()

    return (_D_returnVars)

def _empresa_plaza_sucursal_almacen_ejercicio_guiascontables_validation(O_form):
    """ Validacion de empresa_plaza_sucursal_almacen_ejercicio_guiascontables
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_plaza_sucursal_almacen_id
        arg4: master_[nombre función maestro]
        arg5: empresa_plaza_sucursal_almacen_ejercicio_id
        [arg6]: codigo de funcionalidad
        [arg7]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """
    
    _dbTable = dbc01.tempresa_plaza_sucursal_almacen_ejercicio_guiascontables
    
    _s_empresa_plaza_sucursal_almacen_ejercicio_id = request.args(5, None)
    
    _dbRows = dbc01(
        (_dbTable.empresa_plaza_sucursal_almacen_ejercicio_id == _s_empresa_plaza_sucursal_almacen_ejercicio_id) 
        & (_dbTable.empresa_ingresoestructura_id == O_form.vars.empresa_ingresoestructura_id)
        & (_dbTable.cuentacontable == O_form.vars.cuentacontable)
        & (_dbTable.id != O_form.record_id)  # Se ignora el registro que se esté modificando
        ).select(_dbTable.id)
        
    if _dbRows:
        O_form.errors.empresa_ingresoestructura_id = "Esta combinación de estructura de ingreso y cuenta contable ya fueron dadas de alta."
        O_form.errors.cuentacontable = "Esta combinación de estructura de ingreso y cuenta contable ya fueron dadas de alta."
    else:
        pass
    
    _dbRows = dbc01(
        (_dbTable.empresa_plaza_sucursal_almacen_ejercicio_id == _s_empresa_plaza_sucursal_almacen_ejercicio_id) 
        & (_dbTable.empresa_ingresoestructura_id == O_form.vars.empresa_ingresoestructura_id)
        & (_dbTable.id != O_form.record_id)  # Se ignora el registro que se esté modificando
        ).select(_dbTable.id)
        
    if _dbRows:
        O_form.errors.empresa_ingresoestructura_id = "Esta estructura de ingreso ya fue dada de alta en este ejercicio."
    else:
        pass
    
    if not O_form.vars.empresa_ingresoestructura_id:
        O_form.errors.empresa_ingresoestructura_id = "La estructura de ingreso no puede estar vacía."
    else:
        pass
    
    if not O_form.vars.cuentacontable:
        O_form.errors.cuentacontable = "La cuenta contable no puede ir vacía."
    else:
        pass

    ###TODO: Preguntar a Juan si la cuenta contable no se puede repetir.
    
    return O_form

def empresa_almacenesconceptos():
    """ Formulario para los conceptos de almacén por empresa

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """

    _dbTable = dbc01.tempresa_almacenesconceptos
    
    _s_action = request.args(2, None)
    
    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.tipoconcepto,
        _dbTable.codigoconcepto,
        _dbTable.concepto,
        _dbTable.tipocosto
        ]
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas, 
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow, 
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
        
        )
    
    _O_cat.addRowContentProperty('formHidden', {'_s_action': str(_s_action)})

    _O_cat.addRowContentEvent('onValidation', _empresa_almacenesconceptos_validation)

    
    _D_returnVars = _O_cat.process()

    return (_D_returnVars)


def _empresa_almacenesconceptos_validation(O_form):
    """ Validación para el formulario de conceptos de almacén
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """
    _s_empresa_id = request.args(1, None)
    _s_concepto = O_form.vars.codigoconcepto 
    _s_tratamiento = O_form.vars.clasificacion
    _n_tipoconcepto = int( O_form.vars.tipoconcepto)
    _s_id = O_form.record_id
    
    
    if(_n_tipoconcepto == TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.ENTRADA):
        if(
            (_s_concepto[0] == 'E') 
               and (_s_concepto[1] == '-') 
               and (_s_concepto[2] in('0', '1', '2', '3', '4', '5', '6', '7', '8', '9')) 
               and (_s_concepto[3] in('0', '1', '2', '3', '4', '5', '6', '7', '8', '9'))
           ):
            pass
        else:
            O_form.errors.codigoconcepto = 'Tiene que apegarse al formato indicado.Ejemplo: "E-01"'
    
    else:
        if(
            (_s_concepto[0] == 'S') 
               and (_s_concepto[1] == '-') 
               and (_s_concepto[2] in('0', '1', '2', '3', '4', '5', '6', '7', '8', '9')) 
               and (_s_concepto[3] in('0', '1', '2', '3', '4', '5', '6', '7', '8', '9'))
           ):
            pass
        else:
            O_form.errors.codigoconcepto = 'Tiene que apegarse al formato indicado.Ejemplo: "S-01"'
            
            
    if  dbc01((dbc01.tempresa_almacenesconceptos.empresa_id == _s_empresa_id)
            & (dbc01.tempresa_almacenesconceptos.codigoconcepto == _s_concepto)
            & (dbc01.tempresa_almacenesconceptos.id != _s_id) ).count() > 0:
        O_form.errors.codigoconcepto = 'No se puede repetir el código concepto.'
    else:
        pass
 
    if  dbc01((dbc01.tempresa_almacenesconceptos.empresa_id == _s_empresa_id)
            & (dbc01.tempresa_almacenesconceptos.clasificacion == _s_tratamiento)
            & (dbc01.tempresa_almacenesconceptos.id != _s_id) ).count() > 0:
        O_form.errors.clasificacion = 'No se puede repetir el tratamiento de concepto.'
    else:
        pass

    return O_form

def empresa_inventariocategoria_especificaciones():
    """ Formulario para las especificaciones de la categoría de inventarios.

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: categoria_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """

    _dbTable = dbc01.tempresa_prodcategoria_especificaciones
    
    _s_categoria_id = request.args(3, None)
    
    _s_especificacion_id = request.args(5, None)
    
    _b_validacion = False
    
    # Utiliza por dafult el ID que trae en los argumentos para poderse utilizar este formulario como detalle
    _dbTable.categoria_id.default = _s_categoria_id
    
    _dbRows = dbc01(
        (dbc01.tempresa_prodcategorias.id == _s_categoria_id)
        ).select(
            dbc01.tempresa_prodcategorias.id, 
            dbc01.tempresa_prodcategorias.padre_id, 
            orderby = dbc01.tempresa_prodcategorias.orden
            )
                   
    _L_especificacionesPadres_id = [_s_categoria_id]
    _n_timeout = 100
    while _dbRows and (_n_timeout>0):
        _n_timeout -= 1
        _dbRow = _dbRows.first()
        if _dbRow.padre_id:
            _L_especificacionesPadres_id.append(_dbRow.padre_id)
        else:
            break
        _dbRows = dbc01(
            (dbc01.tempresa_prodcategorias.id == _dbRow.padre_id)
            ).select(
                dbc01.tempresa_prodcategorias.id, 
                dbc01.tempresa_prodcategorias.padre_id, 
                orderby = dbc01.tempresa_prodcategorias.orden
                )
                        
    if _n_timeout == 0:
        # TODO: Log del errro
        pass
    else:
        pass
    
    _qry = (
            (_dbTable.categoria_id.belongs( _L_especificacionesPadres_id ))
            )
    
        
    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.especificacion,
        _dbTable.tipocaptura,
        _dbTable.capturaobligatoria,
        _dbTable.opciones,
        _dbTable.categoria_id        
        ]
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow, 
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
        x_offsetInArgs = request.args[:4],
        )
    
    # Se agrega una función de JS después de presionar el botón para realizar una validación
    _O_cat.D_permissionsConfig[request.stv_fwk_permissions.btn_edit.code].D_actions.js.sfn_beforePressAction = 'stv_fwk_button_beforepressaction_' + D_stvFwkCfg.D_uniqueHtmlIds.s_idTab
    
    _O_cat.D_permissionsConfig[request.stv_fwk_permissions.btn_remove.code].D_actions.js.sfn_beforePressAction = 'stv_fwk_button_beforepressaction_' + D_stvFwkCfg.D_uniqueHtmlIds.s_idTab

    
    _D_returnVars = _O_cat.process()
    
    #Manera de mandar a la vista un dato
    _D_returnVars.s_categoria_id = str(_s_categoria_id)
            
    return (_D_returnVars)

def empresa_prodcategoria_proveedores():
    """ Formulario para los proveedores segun la categoría. 

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: categoria_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """

    _dbTable = dbc01.tempresa_prodcategoria_proveedores
    
    _s_categoria_id = request.args(3, None)
        
    _b_validacion = False
    
    # Utiliza por dafult el ID que trae en los argumentos para poderse utilizar este formulario como detalle
    _dbTable.categoria_id.default = _s_categoria_id
    
    _dbRows = dbc01(
        (dbc01.tempresa_prodcategorias.id == _s_categoria_id)
        ).select(
            dbc01.tempresa_prodcategorias.id, 
            dbc01.tempresa_prodcategorias.padre_id, 
            orderby = dbc01.tempresa_prodcategorias.orden
            )
                   
    _L_Padres_id = [_s_categoria_id]
    _n_timeout = 100
    while _dbRows and (_n_timeout>0):
        _n_timeout -= 1
        _dbRow = _dbRows.first()
        if _dbRow.padre_id:
            _L_Padres_id.append(_dbRow.padre_id)
        else:
            break
        _dbRows = dbc01(
            (dbc01.tempresa_prodcategorias.id == _dbRow.padre_id)
            ).select(
                dbc01.tempresa_prodcategorias.id, 
                dbc01.tempresa_prodcategorias.padre_id, 
                orderby = dbc01.tempresa_prodcategorias.orden
                )
                        
    if _n_timeout == 0:
        # TODO: Log del errro
        pass
    else:
        pass
    
    _qry = (
            (_dbTable.categoria_id.belongs( _L_Padres_id ))
            )
    
        
    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.empresa_proveedor_id,
        _dbTable.comentario        
        ]
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow, 
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
        x_offsetInArgs = request.args[:4],
        )
    
    # Se agrega una función de JS después de presionar el botón para realizar una validación
#     _O_cat.D_permissionsConfig[request.stv_fwk_permissions.btn_edit.code].D_actions.js.sfn_beforePressAction = 'stv_fwk_button_beforepressaction_' + D_stvFwkCfg.D_uniqueHtmlIds.s_idTab
#     
#     _O_cat.D_permissionsConfig[request.stv_fwk_permissions.btn_remove.code].D_actions.js.sfn_beforePressAction = 'stv_fwk_button_beforepressaction_' + D_stvFwkCfg.D_uniqueHtmlIds.s_idTab
    
    _D_returnVars = _O_cat.process()

    #Manera de mandar a la vista un dato
    _D_returnVars.s_categoria_id = str(_s_categoria_id)

    return (_D_returnVars)


def empresa_inventarioscategorias_buscar():
    """ Forma buscar para las categorias de inventario. 
    """
    
    _dbTable = dbc01.tempresa_prodcategorias
    
    _O_cat = STV_FWK_FORM(
                dbReference = dbc01,
                dbTable = _dbTable, 
                L_visibleButtons = request.stv_fwk_permissions.L_formSearchButtons, 
                s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
                L_fieldsToShow = [
                    _dbTable.id, 
                    _dbTable.nombrecorto,
                    ], 
                L_fieldsToSearch = [
                    _dbTable.id, 
                    _dbTable.nombrecorto
                    ], 
                L_fieldsToSearchIfDigit = [
                    _dbTable.id, 
                    _dbTable.nombrecorto
                    ],
                )
    
    _D_returnVars = _O_cat.process()

    return (_D_returnVars)


def empresa_inventarioscategoriasestructura_ver():
    """

    @return:
    @rtype:
    """

    if (len(request.args) >= 2) :
        _dbTable = dbc01.tempresa_prodcategorias
        _rows_result = dbc01(_dbTable.empresa_id == request.args(1)).select(_dbTable.ALL)
        _LD_data = []
        for _row in _rows_result:
            _LD_data.append({'id': _row.id, 'optionname': (_dbTable._format % _row.as_dict())})
        return (response.json({'data': _LD_data}))
    else:
        return (response.json({'data': [], 'error': 'Argumentos inválidos'}))

def empresa_inventarioscategoriasestructura():
    """ Detalle de estructura de ingreso > empresa

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    
    """
    
    _s_action = request.args(2, 'None')
    _s_inventarioscategoriasestructura_id = request.args(3, None)    
    
    _dbTable = dbc01.tempresa_prodcategorias
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas, 
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.nombrecorto, 
            _dbTable.padre_id, 
            _dbTable.bansuspender
            ], 
        L_fieldsToSearch = [
            _dbTable.nombrecorto
            ],                 
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ]
    )

    _dbRows = dbc01((_dbTable.id > 0) & (_dbTable.empresa_id == request.args(1))).select(_dbTable.ALL, orderby = _dbTable.orden)
    _dbTree = DB_Tree(_dbRows)
    _D_inventarioscategoriasestructura = _dbTree.process_groupby('tempresa_prodcategorias.padre_id' )    

    dbc01.tempresa_prodcategorias.padre_id.default = request.args(3, None)
    if _O_cat.D_args.s_action in (request.stv_fwk_permissions.btn_edit.code, request.stv_fwk_permissions.btn_save.code):
        _n_id = request.args(3, None)
    else:
        _n_id = None
    dbc01.tempresa_prodcategorias.padre_id.widget = lambda f, v: stv_widget_db_chosen_tree(f, v, 1, D_additionalAttributes={'D_tree':_D_inventarioscategoriasestructura, 'n_filterFromId':_n_id})

    _O_cat.addRowSearchResultsProperty(s_property = 's_type', x_value = STV_FWK_FORM.ROWSEARCHRESULTS.PROPERTIES.TYPE.S_TREE)

    _O_cat.addRowContentEvent('onValidation', _empresa_inventarioscategoriasestructura_validation)

    _D_returnVars = _O_cat.process(x_overrideQuery = _D_inventarioscategoriasestructura)
    
    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_prodcategoria_especificaciones, 
        s_url = URL(f='empresa_inventariocategoria_especificaciones', args=request.args[:2]+['master_'+str(request.function),request.args(3)]),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_inventariocategoria_especificaciones', s_masterIdentificator = str(request.function))  )

    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_prodcategoria_proveedores, 
        s_url = URL(f='empresa_prodcategoria_proveedores', args=request.args[:2]+['master_'+str(request.function),request.args(3)]),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_prodcategoria_proveedores', s_masterIdentificator = str(request.function))  )
    
    return (_D_returnVars)


def _empresa_inventarioscategoriasestructura_validation(O_form):
    """ Validación para las categorias por estructura
        
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """
    
    _s_empresa_id = request.args(1, None)
    _s_nombrecorto =  O_form.vars.nombrecorto
    _s_id = O_form.record_id
    
    _dbRows = dbc01(
        (dbc01.tempresa_prodcategorias.empresa_id == _s_empresa_id)
        &(dbc01.tempresa_prodcategorias.nombrecorto == _s_nombrecorto) 
        & (dbc01.tempresa_prodcategorias.id != _s_id)  # Se ignora el registro que se esté modificando
        ).select(dbc01.tempresa_prodcategorias.id)
        
    if _dbRows:
        O_form.errors.nombrecorto = "No puede haber dos categorías con el mismo nombre"
    else:
        pass
    
    if ((O_form.vars.porcentajeutilidad) or (O_form.vars.porcentajeutilidad == 0)):
        pass
    else:
        O_form.errors.porcentajeutilidad = "Debe de llevar un 0 si no se quiere capturar."
        pass

    return O_form

def empresa_inventariomovimientos():
    """ Formulario genérico para ver todos los movimientos de almacén. 

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """

    _dbTable = dbc01.tempresa_inventariomovimientos
    
    _s_empresa_id = request.args(1, None)

    _s_action = request.args(2, request.stv_fwk_permissions.btn_none.code)
        
    _s_empresa_inventariomovimiento_id = request.args(3, None)
    
    #_L_searchFilter se utiliza en la forma buscar para los CFDIs recibidos por proveedor.
    _L_searchFilter = [
        Storage(
            s_input_name = str(_dbTable.empresa_proveedor_id),
            s_reference_name = str(dbc01.tcfdis.emisorproveedor_id),
            b_mandatory = True
            )
        ]
    
    _dbTable.cfdi_id.widget = lambda f, v: stv_widget_db_search(
        f,  
        v, 
        s_display = '%(serie)s %(folio)s', 
        s_url = URL( a = 'app_almacenes', c = '600busquedas', f = 'buscar_cfdis_recibidos', ),
        D_additionalAttributes = {
            'reference':dbc01.tcfdis.id,
            'L_searchFilter':_L_searchFilter,
            }
        )
    
    _dbTable.cfdi_egreso_id.widget = lambda f, v: stv_widget_db_search(
        f, 
        v, 
        s_display = '%(serie)s %(folio)s', 
        s_url = URL( a = 'app_almacenes', c = '600busquedas', f = 'buscar_cfdis_egreso_recibidos',),
        D_additionalAttributes = {
            'reference':dbc01.tcfdis.id,
            'L_searchFilter':_L_searchFilter,
            }
        )
            
    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.empresa_plaza_sucursal_almacen_id,
        _dbTable.empresa_almacenesconcepto_id,
        _dbTable.empresa_proveedor_id,
        _dbTable.tipodocumento,
        _dbTable.observaciones
        ]
    
    if _s_action in (request.stv_fwk_permissions.btn_new.code, request.stv_fwk_permissions.btn_save.code,):
        #Si el _s_action es nuevo o save se habilitarán los siguientes campos para poder crear el registro.
        _dbTable.empresa_almacenesconcepto_id.writable = True
        _dbTable.empresa_proveedor_id.writable = True
        _dbTable.empresa_plaza_sucursal_almacen_id.writable = True
        
    elif _s_action in (request.stv_fwk_permissions.btn_edit.code,):
        #Estos campos una vez creados no se pueden modificar, si se quieren editar tendrá que borrar el registro y crearse uno nuevo.
        _dbTable.empresa_almacenesconcepto_id.writable = False
        _dbTable.empresa_plaza_sucursal_almacen_id.writable = False
    else:
        pass
    
    #Se asigna la fecha de registro con el día y hora actual.
    _dbTable.fecha_registro.writable = False
    _dbTable.fecha_registro.default = request.browsernow
    
    _dbTable.ordencompra.label = "Orden de compra/salida"
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas, 
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow, 
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
        )
    
    # Variable que contendrá los campos a editar en la búsqueda avanzada    
    _L_advSearch = []
    
    ### Los campos al ponerle el .clone dan un error al momento de guardar. Falta pulir esta lógica. 
#     # Se configura el uso del campo día para poder buscar por rango de fechas    
#     _dbField_fecha_movimiento = _dbTable.fecha_movimiento.clone()
#     _dbField_fecha_movimiento.widget = stv_widget_inputDateRange
#     #Se pone el default en None para evitar que ponga la fecha actual cada que se imprima este campo.
#     _dbField_fecha_movimiento.default = None
#     _L_advSearch.append(_dbField_fecha_movimiento)
# 
#     if not(_s_empresa_id):    
#         # Si no esta definida la empresa, se agrega el seleccionar empresa
#         _dbField_empresa_id = _dbTable.empresa_id.clone()
#         _dbField_empresa_id.notnull = False
#         _L_advSearch.append(_dbField_empresa_id)
#     else:
#         pass
    
#     # Se agregan campos sin modificaciones
#     _L_advSearch += [
#         _dbTable.empresa_almacenesconcepto_id.clone(),
# #         _dbTable.empresa_plaza_sucursal_almacen_id.clone(),
#         _dbTable.tipodocumento.clone(),
#         _dbTable.empresa_proveedor_id.clone(),
#         ]
#     
#     # Se configura la edición de los campos
#     for _dbField in _L_advSearch:
#         _dbField.notnull = False
#         #Se pone required en falso para que en la vista no se multipliquen los * cada que se llame esta función.
#         _dbField.required = False
#         _dbField.writable = True
#         
#     _O_cat.cfgRow_advSearch(
#         L_dbFields_formFactory_advSearch = _L_advSearch,
#         dbRow_formFactory_advSearch = None,
#         D_hiddenFields_formFactory_advSearch = {},
#         s_style = 'bootstrap',
#         L_buttons = []
#         )
        
#     _O_cat.addRowContentEvent('onValidation', _empresa_inventariomovimientos_validation)

    if _s_action in (request.stv_fwk_permissions.btn_save.code,):
        
        if (request.vars.empresa_almacenesconcepto_id):
            
            _dbRow_concepto = dbc01.tempresa_almacenesconceptos(request.vars.empresa_almacenesconcepto_id)
            
            #Dependiendo su clasificación será el tratamiento de las validaciones 
            
            ###TODO: Pregunta para Jaime: ¿En este caso no sería mejor un switch case? ya que tod
            
            ###Entradas
            if(_dbRow_concepto.clasificacion == TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.INVENTARIO_INICIAL):
                ###Falta validación para evitar que se genere otro inventario inicial.
                _O_cat.addRowContentEvent('onValidation', TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.ENTRADA.INVENTARIOINICIAL)
                
            elif(_dbRow_concepto.clasificacion == TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.COMPRA):
                #Entrada por compra
                _O_cat.addRowContentEvent('onValidation', TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.ENTRADA.PORCOMPRA)
                    
                _O_cat.addRowContentEvent('onAccepted', TEMPRESA_INVENTARIOMOVIMIENTOS.ACEPTADO.ENTRADA.PORCOMPRA)
                
            elif(_dbRow_concepto.clasificacion == TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.DEVOLUCION_VENTA):
                pass
            
            elif(_dbRow_concepto.clasificacion == TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.DEVOLUCION_INTERNA):
                pass
            
            elif(_dbRow_concepto.clasificacion == TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.TRASPASO_ENTRADA):
                #Entrada por traspaso
                _O_cat.addRowContentEvent('onValidation', TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.ENTRADA.PORTRASPASO)         

                _O_cat.addRowContentEvent('onAccepted', TEMPRESA_INVENTARIOMOVIMIENTOS.ACEPTADO.ENTRADA.PORTRASPASO) 
                    
            elif(_dbRow_concepto.clasificacion == TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.OTRAS_ENTRADAS):
                pass
            
            ###Salidas
            elif(_dbRow_concepto.clasificacion == TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.VENTA):
                pass
            
            elif(_dbRow_concepto.clasificacion == TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.DEVOLUCION_COMPRA):
                #Salida por devolución de compra
                _O_cat.addRowContentEvent('onValidation', TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.SALIDA.PORDEVOLUCION_COMPRA)
                    
                _O_cat.addRowContentEvent('onAccepted', TEMPRESA_INVENTARIOMOVIMIENTOS.ACEPTADO.SALIDA.PORDEVOLUCION_COMPRA)
                
            elif(_dbRow_concepto.clasificacion == TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.CONSUMO_INTERNO):
                pass
            
            elif(_dbRow_concepto.clasificacion == TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.TRASPASO_SALIDA):
                #Salida por traspaso
                _O_cat.addRowContentEvent('onValidation', TEMPRESA_INVENTARIOMOVIMIENTOS.VALIDACION.SALIDA.PORTRASPASO)

            elif(_dbRow_concepto.clasificacion == TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.OTRAS_SALIDAS):
                pass
            
            else:
                pass
        else:
            pass
    else:
        ###TODO: Mandar una validación normal que mande el O_form.errors a empresa_almacenesconcepto_id para que se capture.
        pass

    _D_returnVars = _O_cat.process()
    
    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_invmovto_productos, 
        s_url = URL(f='empresa_inventarioentradacompra_resumenes', args=request.args[:2]+['master_'+str(request.function),request.args(3)]),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_inventarioentradacompra_resumenes', s_masterIdentificator = str(request.function))  )
    
    if(_s_empresa_inventariomovimiento_id):
        
        _dbRow_movto = _dbTable(_s_empresa_inventariomovimiento_id)

        if(_dbRow_movto and _dbRow_movto.cfdi_egreso_id):
            _O_cat.addSubTab(
                s_tabName = "CFDI egreso relacionado", 
                s_url = URL(a = 'app_timbrado', c= '250cfdi', f='cfdi_entradasalida_relacionado', args=['master_'+str(request.function), _dbRow_movto.cfdi_egreso_id]),
                s_idHtml = fn_createIDTabUnique(a = 'app_timbrado', c= '250cfdi', f = 'cfdi_entradasalida_relacionado', s_masterIdentificator = str(request.function))  
                )
            
        elif(_dbRow_movto and _dbRow_movto.cfdi_id):
            _O_cat.addSubTab(
                s_tabName = "CFDI ingreso relacionado", 
                s_url = URL(a = 'app_timbrado', c= '250cfdi', f='cfdi_entradasalida_relacionado', args=['master_'+str(request.function), _dbRow_movto.cfdi_id]),
                s_idHtml = fn_createIDTabUnique(a = 'app_timbrado', c= '250cfdi', f = 'cfdi_entradasalida_relacionado', s_masterIdentificator = str(request.function))  
                )    
        else:
            pass
    else:
        pass

    return (_D_returnVars)

def empresa_inventarioentradacompra_resumenes():
    """ Formulario para los movimientos de productos

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_inventariomovimiento_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """
    

    _dbTable = dbc01.tempresa_invmovto_productos  
    
    _s_empresa_inventariomovimiento_id = request.args(3, None)
    
    _L_fieldsToShow = []
    
    _dbRows = dbc01(
        (dbc01.tempresa_inventariomovimientos.id == _s_empresa_inventariomovimiento_id)
        &(dbc01.tempresa_almacenesconceptos.id == dbc01.tempresa_inventariomovimientos.empresa_almacenesconcepto_id)
        ).select(
            dbc01.tempresa_almacenesconceptos.clasificacion
            ).last()
        
    _s_empresa_almacenesconceptos_clasificacion = _dbRows.clasificacion
                        
    ###Entradas
    if(_s_empresa_almacenesconceptos_clasificacion == TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.INVENTARIO_INICIAL):
        
        _L_fieldsToShow = [
            _dbTable.id,
            _dbTable.empresa_producto_id,
            _dbTable.producto_externo,
            _dbTable.cantidad_entrada,
            _dbTable.preciounitario,
            ]
        
    elif(_s_empresa_almacenesconceptos_clasificacion == TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.COMPRA):
        #Entrada por compra
        _L_fieldsToShow = [
            _dbTable.id,
            _dbTable.empresa_producto_id,
            _dbTable.descripcion,
            _dbTable.producto_externo,
            _dbTable.cantidad_entrada,
            _dbTable.preciounitario,
            _dbTable.costototal,
            _dbTable.tipo_inconsistencia,  
            ]
        
    elif(_s_empresa_almacenesconceptos_clasificacion == TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.DEVOLUCION_VENTA):
        pass
    
    elif(_s_empresa_almacenesconceptos_clasificacion == TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.DEVOLUCION_INTERNA):
        pass
    
    elif(_s_empresa_almacenesconceptos_clasificacion == TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.TRASPASO_ENTRADA):
        #Entrada por traspaso
        _L_fieldsToShow = [
            _dbTable.id,
            _dbTable.empresa_producto_id,
            _dbTable.cantidad_entrada,
            ]
            
    elif(_s_empresa_almacenesconceptos_clasificacion == TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.OTRAS_ENTRADAS):
        pass
    
    ###Salidas
    elif(_s_empresa_almacenesconceptos_clasificacion == TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.VENTA):
        pass
    
    elif(_s_empresa_almacenesconceptos_clasificacion == TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.DEVOLUCION_COMPRA):
        
        #Salida por devolución de compra
        _L_fieldsToShow = [
            _dbTable.id,
            _dbTable.empresa_producto_id,
            _dbTable.descripcion,
            _dbTable.producto_externo,
            _dbTable.cantidad_salida,
            _dbTable.preciounitario,
            _dbTable.costototal,
            _dbTable.tipo_inconsistencia,        
            ]      
          
    elif(_s_empresa_almacenesconceptos_clasificacion == TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.CONSUMO_INTERNO):
        pass
    
    elif(_s_empresa_almacenesconceptos_clasificacion == TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.TRASPASO_SALIDA):
        #Salida por traspaso
        _L_fieldsToShow = [
            _dbTable.id,
            _dbTable.empresa_producto_id,
            _dbTable.cantidad_salida,
            ]
        
    elif(_s_empresa_almacenesconceptos_clasificacion == TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.OTRAS_SALIDAS):
        pass
    
    else:
        pass
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(),
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_inventariomovimientos,
        xFieldLinkMaster = [
            _dbTable.empresa_inventariomovimiento_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
        x_offsetInArgs = request.args[:2],

        )
   
    _D_returnVars = _O_cat.process()

    return (_D_returnVars)

def clasificacion_ver():
    if (len(request.args) > 0) :
        _n_tipoconcepto = int(request.args(0, None))
        _LD_data = []
        if(_n_tipoconcepto == TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.ENTRADA):
            _LD_data.append({ 'id':TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.INVENTARIO_INICIAL, 'optionname': 'Inventario inicial' })
            _LD_data.append({ 'id':TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.COMPRA, 'optionname': 'Compra' })
            _LD_data.append({ 'id':TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.DEVOLUCION_VENTA, 'optionname': 'Devolución de venta' })
            _LD_data.append({ 'id':TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.DEVOLUCION_INTERNA, 'optionname': 'Devolución interna' })
            _LD_data.append({ 'id':TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.TRASPASO_ENTRADA, 'optionname': 'Traspaso' })
            _LD_data.append({ 'id':TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.OTRAS_ENTRADAS, 'optionname': 'Otras entradas' }) 
        elif(_n_tipoconcepto == TEMPRESA_ALMACENESCONCEPTOS.E_TIPOCONCEPTO.SALIDA):
            _LD_data.append({ 'id':TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.VENTA, 'optionname': 'Venta' })   
            _LD_data.append({ 'id':TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.DEVOLUCION_COMPRA, 'optionname': 'Devolución de venta' })   
            _LD_data.append({ 'id':TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.CONSUMO_INTERNO, 'optionname': 'Consumo interno' })   
            _LD_data.append({ 'id':TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.TRASPASO_SALIDA, 'optionname': 'Traspaso' })   
            _LD_data.append({ 'id':TEMPRESA_ALMACENESCONCEPTOS.E_CLASIFICACION.OTRAS_SALIDAS, 'optionname': 'Otras salidas' })   

        else:
            #Tipo de concepto no identificado.
            pass
        return (response.json({'data': _LD_data}))
    else:
        return (response.json({'data': [], 'error': 'Argumentos inválidos'}))
