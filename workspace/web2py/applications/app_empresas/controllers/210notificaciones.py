# -*- coding: utf-8 -*-

def notificaciones():
    _dbTable = dbc02.tnotificaciones
    
    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_findall)    

    _O_cat = STV_FWK_FORM(
        dbReference = dbc02,
        dbReferenceSearch = dbc02,
        dbTable = _dbTable,
        dbTableMaster = None, 
        xFieldLinkMaster = [], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.empresa_id, 
            _dbTable.titulo, 
            _dbTable.descripcion, 
            _dbTable.estatus, 
            _dbTable.proceso_id, 
            _dbTable.user_id_asignado, 
            ], 
        L_fieldsToSearch = [
            _dbTable.titulo, 
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
        )
        
    _D_returnVars = _O_cat.process()

    _O_cat.addSubTab(
                dbTableDetail = dbc02.tnotificacion_mensajes, 
                s_url = URL(f='notificacion_mensajes', args=['master_'+str(request.function),request.args(1)]),
                s_idHtml = fn_createIDTabUnique(f = 'notificacion_mensajes', s_masterIdentificator = str(request.function))  )

    return (_D_returnVars)


def notificacion_mensajes():
    _dbTable = dbc02.tnotificacion_mensajes

    _O_cat = STV_FWK_FORM(
        dbReference = dbc02,
        dbReferenceSearch = dbc02,
        dbTable = _dbTable,
        dbTableMaster = dbc02.tnotificaciones, 
        xFieldLinkMaster = [_dbTable.notificacion_id], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.proceso, 
            _dbTable.descripcion, 
            _dbTable.codigoerror, 
            _dbTable.prioridad, 
            ], 
        L_fieldsToSearch = [
            _dbTable.codigovalidacion, 
            _dbTable.descripcion
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            _dbTable.prioridad
            ],
        )        
    
    _D_returnVars = _O_cat.process()
    
    return (_D_returnVars)

