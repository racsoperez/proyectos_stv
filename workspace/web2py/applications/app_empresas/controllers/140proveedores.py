# -*- coding: utf-8 -*-

''' VALIDAR Y CALCULO DE AJUSTE EN EL PLAN DE PÉNSION'''
def _empresa_proveedores_validation(O_form):
    _s_empresa_id = request.args(1, None)    
    _s_rfc = O_form.vars.rfc
    _s_codigo = O_form.vars.codigo
    _s_id = O_form.record_id
    
    if _s_rfc:
        if dbc01( (dbc01.tempresa_proveedores.empresa_id == _s_empresa_id)
                & (dbc01.tempresa_proveedores.rfc == _s_rfc) 
                & (dbc01.tempresa_proveedores.id != _s_id) ).count() > 0:
            O_form.errors.rfc = 'Ya existe con otro proveedor'
        else:
            # No existe problema en la validación
            pass

    if _s_codigo:
        if dbc01( (dbc01.tempresa_proveedores.empresa_id == _s_empresa_id)
                & (dbc01.tempresa_proveedores.codigo == _s_codigo) 
                & (dbc01.tempresa_proveedores.id != _s_id) ).count() > 0:
            O_form.errors.codigo = 'Ya existe con otro proveedor'
        else:
            # No existe problema en la validación
            pass
    else:
        # No existe problema en la validación
        pass
    
    return O_form


def empresa_proveedores():
    """ Maneja la configuración de los proveedores
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    """
    
    _s_empresa_id = request.args(1, None)
    
    _dbTable = dbc01.tempresa_proveedores
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbTable = _dbTable, 
        dbTableMaster = dbc01.tempresas, 
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ],                 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.codigo, 
            _dbTable.razonsocial, 
            _dbTable.rfc, 
            _dbTable.nombrecorto, 
            _dbTable.empresa_grupo_id, 
            _dbTable.bansuspender
            ], 
        L_fieldsToSearch = [
            _dbTable.id, 
            _dbTable.codigo, 
            _dbTable.razonsocial, 
            _dbTable.rfc, 
            _dbTable.nombrecorto, 
            _dbTable.empresa_grupo_id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            _dbTable.codigo
            ],
        )

    _O_cat.addRowContentEvent('onValidation', _empresa_proveedores_validation)

    _D_returnVars = _O_cat.process()
   
    _D_returnVars['htmlid_domiciliofiscal'] = _O_cat.addSubTab(
                dbTableDetail = None, 
                s_tabName = "Domicilio fiscal",
                s_idHtml = None,  
                s_type = "extended")
    
    _O_cat.addSubTab(
                dbTableDetail = dbc01.tempresa_proveedor_guiasproveedores, 
                s_url = URL(f='empresa_proveedor_guiasproveedores', args=request.args[:2]+['master_'+str(request.function),request.args(3)]),
                s_idHtml = fn_createIDTabUnique(f = 'empresa_proveedor_guiasproveedores', s_masterIdentificator = str(request.function))  )

    _O_cat.addSubTab(
                dbTableDetail = dbc01.tempresa_proveedor_clasificaciones, 
                s_url = URL(f='empresa_proveedor_clasificaciones', args=request.args[:2]+['master_'+str(request.function),request.args(3)]),
                s_idHtml = fn_createIDTabUnique(f = 'empresa_proveedor_clasificaciones', s_masterIdentificator = str(request.function))  )


    _O_cat.addSubTab(
                dbTableDetail = dbc01.tempresa_proveedor_preciosoficiales, 
                s_url = URL(f='empresa_proveedor_preciosoficiales', args=request.args[:2]+['master_'+str(request.function),request.args(3)]),
                s_idHtml = fn_createIDTabUnique(f = 'empresa_proveedor_preciosoficiales', s_masterIdentificator = str(request.function))  )

    _O_cat.addSubTab(
                dbTableDetail = dbc01.tempresa_proveedor_cotizaciones, 
                s_url = URL(f='empresa_proveedor_cotizaciones', args=request.args[:2]+['master_'+str(request.function),request.args(3)]),
                s_idHtml = fn_createIDTabUnique(f = 'empresa_proveedor_cotizaciones', s_masterIdentificator = str(request.function))  )


    return (_D_returnVars)

def _empresa_proveedor_guiasproveedores_validation(O_form):
    """ Validación para la guías de proveedores. 
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_proveedor_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    """

    _s_moneda_id = O_form.vars.monedacontpaqi_id
    _s_empresa_id = request.args(1, None)
    _s_empresa_proveedor_id = request.args(3, None)
    _s_id = O_form.record_id
    _s_ejercicio = O_form.vars.ejercicio
    
   
    if  dbc01( (dbc01.tempresa_proveedor_guiasproveedores.empresa_proveedor_id == _s_empresa_proveedor_id)
            & (dbc01.tempresa_proveedor_guiasproveedores.monedacontpaqi_id == _s_moneda_id) 
            & (dbc01.tempresa_proveedor_guiasproveedores.ejercicio == _s_ejercicio)
            & (dbc01.tempresa_proveedor_guiasproveedores.id != _s_id) 
            ).count() > 0:
        
        O_form.errors.monedacontpaqi_id = 'La moneda ya está registrada'
        
    else:
        pass

    _s_msgError = VALIDACIONES.EJERCICIO.ES_VALIDO(_s_empresa_id, _s_ejercicio)
    
    if _s_msgError:
        O_form.errors.id = _s_msgError
    else:
        pass
        
    return O_form


def empresa_proveedor_guiasproveedores():
    """ Formulario detalle para las guías de proveedores. 
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_proveedor_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
        
    """
    
    _dbTable = dbc01.tempresa_proveedor_guiasproveedores
    _s_empresa_id = request.args(1, None)
    _s_action = request.args(4, 'None')
    _s_guia_id = request.args(5, None)
    
    # Si se desea crear un nuevo registro se puede editar la _s_guia_id
    if not(_s_guia_id) or (_s_action in (request.stv_fwk_permissions.btn_new.code)):
        _dbTable.monedacontpaqi_id.writable = True
        _dbTable.ejercicio.writable = True
    else:
        _dbTable.monedacontpaqi_id.writable = False
        _dbTable.ejercicio.writable = False
    
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_proveedores, 
        xFieldLinkMaster = [
            _dbTable.empresa_proveedor_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.ejercicio ,
            _dbTable.empresa_proveedor_id, 
            _dbTable.monedacontpaqi_id, 
            _dbTable.cuenta_proveedor,
            _dbTable.cuenta_proveedor_complemento, 
            _dbTable.cuenta_anticipo, 
            _dbTable.cuenta_anticipo_complemento, 
            _dbTable.bansuspender
            ], 
        L_fieldsToSearch = [
            _dbTable.cuenta_proveedor, 
            _dbTable.cuenta_proveedor_complemento
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.cuenta_proveedor
            ],
        x_offsetInArgs = request.args[:2]
        )
    
    _dbRow = dbc01.tempresas(_s_empresa_id)
    _O_cat.addRowContentProperty('formHidden', {'moneda_id_empresa': str(_dbRow.monedacontpaqi_id)})
    
    _O_cat.addRowContentEvent('onValidation', _empresa_proveedor_guiasproveedores_validation)   
        
    _D_returnVars = _O_cat.process()

    return (_D_returnVars)

def empresa_proveedor_clasificaciones():
    """ Formulario para el manejo de clasificaciones del proveedor
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_proveedor_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
            
    """
    
    _dbTable = dbc01.tempresa_proveedor_clasificaciones

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_proveedores, 
        xFieldLinkMaster = [
            _dbTable.empresa_proveedor_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.empresa_clasificacionproveedor_id
            ], 
        L_fieldsToSearch = [
            _dbTable.id, 
            _dbTable.empresa_clasificacionproveedor_id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:2]
        )
  
    _O_cat.addRowContentEvent('onValidation', _empresa_proveedor_clasificaciones_validation)
  
    _D_returnVars = _O_cat.process()

    return (_D_returnVars)


def _empresa_proveedor_clasificaciones_validation(O_form):
    """ Validación para clasificaciones de proveedor
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_proveedor_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
            
    """
    
    _dbTable = dbc01.tempresa_proveedor_clasificaciones
    _s_proveedor_id = request.args(3, None)
    _s_id = O_form.record_id

    _dbRows = dbc01(
        (_dbTable.empresa_proveedor_id == _s_proveedor_id)
        &(_dbTable.empresa_clasificacionproveedor_id == O_form.vars.empresa_clasificacionproveedor_id)
        &(_dbTable.id != _s_id)
        ).select(
            _dbTable.id
            )   
    if _dbRows:
        O_form.errors.empresa_clasificacionproveedor_id = 'No se puede repetir la clasificación del proveedor.'
    else:
        pass

    return O_form

def empresa_proveedor_preciosoficiales():
    """ Formulario para el manejo de precios oficiales
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_proveedor_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
            
    """
    
    _dbTable = dbc01.tempresa_proveedor_preciosoficiales
    
    _s_proveedor_id = request.args(3, None)
    
    _s_empresa_proveedor_preciooficial_id = request.args(5, None)
    
    _s_action = request.args(4, request.stv_fwk_permissions.btn_none.code)
        
    _L_OrderBy = [
        ~_dbTable.fechavigencia_inicial,
        ]
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(),
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_proveedores, 
        xFieldLinkMaster = [
            _dbTable.empresa_proveedor_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.fechavigencia_inicial,
            _dbTable.fechavigencia_final,
            ], 
        L_fieldsToSearch = [
            _dbTable.id, 
            _dbTable.fechavigencia_inicial,
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:2]
        )
    
    _O_cat.addRowSearchResultsProperty('orderBy', _L_OrderBy)

    _O_cat.addRowContentEvent('onValidation', _empresa_proveedor_preciosoficiales_validation)
    
    _O_cat.addRowContentEvent('onAccepted', _empresa_proveedor_preciosoficiales_accepted)

    _D_returnVars = _O_cat.process()
    
    # TODO Esta lógica es exactamente igual a la logica de validar. Pls mueve la logica al modelo y ejecutala desde aqui pls.
    if (_s_action == request.stv_fwk_permissions.btn_save.code) and (_D_returnVars.rowContent_form.accepted):
        
        _D_result_ultimo_preciooficial = TEMPRESA_PROVEEDOR_PRECIOSOFICIALES.ULTIMO_PRECIOOFICIAL(
            s_proveedor_id = _s_proveedor_id,
            s_id = _D_returnVars.rowContent_form.vars.id
            )
        
        if(_D_result_ultimo_preciooficial.E_return != CLASS_e_RETURN.NOK_ERROR):
            _dbRow_preciooficial = _D_result_ultimo_preciooficial.dbRow_ultimo_preciooficial
        else:
            _dbRow_preciooficial = None
            pass
        
        if(_dbRow_preciooficial):
            if not (_dbRow_preciooficial.fechavigencia_final):
                #Si la última fecha final no tiene nada entonces se le asignará como fecha final la fecha inicial de la nueva lista de precios. 
                _dbRow_preciooficial.fechavigencia_final = _D_returnVars.rowContent_form.vars.fechavigencia_inicial
                _dbRow_preciooficial.update_record()
                pass
            else:
                #Si tiene datos no hará nada. 
                pass
        else:
            pass
            
        
    else:
        #Si se le dio Save y trae _s_empresa_proveedor_preciooficial_id significa que viene desde editar
        pass
    
    _O_cat.addSubTab(
                dbTableDetail = dbc01.tempresa_proveedor_preciooficial_productos, 
                s_url = URL(f='empresa_proveedor_preciooficial_productos', args=request.args[:4]+['master_'+str(request.function),request.args(5)]),
                s_idHtml = fn_createIDTabUnique(f = 'empresa_proveedor_preciooficial_productos', s_masterIdentificator = str(request.function))  )


    return (_D_returnVars)


def _empresa_proveedor_preciosoficiales_validation(O_form):
    """ Validación para el manejo de precios oficiales
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: proveedor_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
            
    """
    
    _dbTable = dbc01.tempresa_proveedor_preciosoficiales
    _s_proveedor_id = request.args(3, None)
    _s_empresa_proveedor_preciooficial_id = request.args(5, None)
    _s_id = O_form.record_id
    
    _dbRows = dbc01(
        (_dbTable.empresa_proveedor_id == _s_proveedor_id)
        &(_dbTable.fechavigencia_inicial == O_form.vars.fechavigencia_inicial)
        &(_dbTable.id != _s_id)
        ).select(
            _dbTable.id
            )
        
    if _dbRows:
        O_form.errors.fechavigencia_inicial = 'No se puede repetir la fecha inicial.'
    else:
        pass
    
    #Query que regresa si hay fechas mayores a la fecha actual que se quiere guardar.
    _dbRows = dbc01(
        (_dbTable.empresa_proveedor_id == _s_proveedor_id)
        &(_dbTable.fechavigencia_inicial > O_form.vars.fechavigencia_inicial)
        ).select(
            _dbTable.id
            )
        
    if _dbRows:
        #Si hay fechas mayores a la fecha actual que se quiere grabar entonces dará error.
        O_form.errors.fechavigencia_inicial = 'No puede haber una fecha inicial menor a una existente.'
    else:
        pass
    
    if(O_form.vars.fechavigencia_final):
        
        if(O_form.vars.fechavigencia_inicial == O_form.vars.fechavigencia_final):
            #Si la fecha inicial y final son la misma dará error.
            O_form.errors.fechavigencia_inicial = 'No pueden ser iguales las fechas.'
            O_form.errors.fechavigencia_final = 'No pueden ser iguales las fechas.'
        else:
            pass
        
        if(O_form.vars.fechavigencia_inicial > O_form.vars.fechavigencia_final):
            O_form.errors.fechavigencia_final = 'La fecha final no puede ser menor a la fecha inicial'
        else:
            pass
    else:
        pass
    
    if(_s_id):
        _D_result_ultimo_preciooficial = TEMPRESA_PROVEEDOR_PRECIOSOFICIALES.ULTIMO_PRECIOOFICIAL(
            s_proveedor_id = _s_proveedor_id,
            s_id = _s_id
            )
        
        if(_D_result_ultimo_preciooficial.E_return != CLASS_e_RETURN.NOK_ERROR):
            _dbRow_preciooficial = _D_result_ultimo_preciooficial.dbRow_ultimo_preciooficial
        else:
            _dbRow_preciooficial = None
            pass
        
        if(_dbRow_preciooficial):
                    
            if(_dbRow_preciooficial.fechavigencia_final):
                if(_dbRow_preciooficial.fechavigencia_final > O_form.vars.fechavigencia_inicial):
                   
                    O_form.errors.fechavigencia_inicial = 'La fecha inicial no puede ser menor a la fecha final más reciente.'
    
                else:
                    pass
            else:
                pass
        else:
            pass
    else:
        pass

    return O_form

def _empresa_proveedor_preciosoficiales_accepted(O_form):
    """ Validación onAccepted para el manejo de precios oficiales
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_proveedor_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
            
    """
    _dbTable = dbc01.tempresa_proveedor_preciosoficiales
    _s_proveedor_id = request.args(3, None)
    _s_empresa_proveedor_preciooficial_id = request.args(5, None)
    _s_id = O_form.vars.id
    
    #Query para saber si esta lista de precios oficiales ya tiene productos. 
    _dbRows_preciooficial_productos = dbc01(
        (dbc01.tempresa_proveedor_preciooficial_productos.empresa_proveedor_preciooficial_id == _s_empresa_proveedor_preciooficial_id)
        ).select(
            dbc01.tempresa_proveedor_preciooficial_productos.id
            )

    if not (_dbRows_preciooficial_productos):
    #Si no tiene productos aún entonces se procede a insertarlos. 
        
        #Query para buscar los productos que estén asociados tengan este proveedor como oficial.
        _dbRows_productos = dbc01(
            (dbc01.tempresa_prodserv.empresa_proveedor_id == _s_proveedor_id)
            ).select(
                dbc01.tempresa_prodserv.ALL,
                dbc01.tempresa_prodserv_codigosproveedores.ALL,
                orderby =[
                    dbc01.tempresa_prodserv.id,
                    dbc01.tempresa_prodserv_codigosproveedores.id
                    ],
                left =[
                    dbc01.tempresa_prodserv_codigosproveedores.on(
                        (dbc01.tempresa_prodserv_codigosproveedores.empresa_prodserv_id == dbc01.tempresa_prodserv.id)
                        &(dbc01.tempresa_prodserv_codigosproveedores.empresa_proveedor_id == _s_proveedor_id)
                        &(dbc01.tempresa_prodserv_codigosproveedores.contenido == 1) #TODO: ¿Hay bronca si dejo esto como 1 en vez de crear una constante? 
                        ), 
                    ]
                )
        
        TEMPRESA_PROVEEDOR_PRECIOOFICIAL_PRODUCTOS.INSERTAR_PRODUCTOS(
            dbRows_productos = _dbRows_productos,
            s_empresa_proveedor_preciooficial_id = _s_id
            )
        
    else:
    #De lo contrario no hará nada, ya cuenta con productos. 
        pass
    
    return O_form

def empresa_proveedor_preciooficial_productos():
    """ Formulario para el manejo de las listas de precios oficiales
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_proveedor_id
        arg4: codigo de funcionalidad
        arg5: empresa_proveedor_preciooficial_id
        [arg6]: codigo de funcionalidad
        [arg7]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
            
    """
    
    _dbTable = dbc01.tempresa_proveedor_preciooficial_productos
    
    _s_proveedor_id = request.args(3, None)
    
    _s_empresa_proveedor_preciooficial_id = request.args(5, None)
    
    _s_action = request.args(6, request.stv_fwk_permissions.btn_none.code)
    
    _s_empresa_proveedor_preciooficial_producto_id = request.args(7, None)
    
    if(_s_action in (request.stv_fwk_permissions.btn_edit.code, request.stv_fwk_permissions.btn_save.code )):
        #Se evita la edición de todos los componenetes
        if(_s_empresa_proveedor_preciooficial_producto_id):
            _dbRow_empresa_proveedor_preciooficial_producto = _dbTable(_s_empresa_proveedor_preciooficial_producto_id)
            if(_dbRow_empresa_proveedor_preciooficial_producto.codigoproveedor):
                _dbTable.codigoproveedor.writable = False
            else:
                _dbTable.codigoproveedor.writable = False
                _dbTable.preciooficial.writable = False
                pass
        else:
            pass
    else:
        _dbRow_empresa_proveedor_preciooficial_producto = None
        pass
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_proveedor_preciosoficiales, 
        xFieldLinkMaster = [
            _dbTable.empresa_proveedor_preciooficial_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.empresa_producto_id,
            _dbTable.unidad_id,
            _dbTable.codigoproveedor,
            _dbTable.preciooficial,
            _dbTable.monedacontpaqi_id
            ], 
        L_fieldsToSearch = [
            _dbTable.id, 
            _dbTable.empresa_producto_id,
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:4]
        )
    
    #Se crea el botón especial 1 para poder actualizar los productos.
    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionLabel = "Actualizar lista de productos",
        s_permissionIcon = "fa fa-compass",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
        confirm = Storage(
            s_title = "Favor de confirmar operación", 
            s_text = "¿Está seguro de querer actualizar los productos?", 
            s_type = "warning", 
            s_btnconfirm = "Aceptar", 
            s_btncancel = "Cancelar", 
            )   
            ),
        L_options = []
        )
    
    #Se utiliza el overrideView a este nivel porque como se va a eliminar el registro tiene que hacerse antes del _O_cat.process()
    _D_overrideView = None 
    if(_s_action == request.stv_fwk_permissions.special_permisson_01.code):
        
        _dbRows_productos = dbc01(
            (_dbTable.empresa_proveedor_preciooficial_id == _s_empresa_proveedor_preciooficial_id)
            ).select(
                _dbTable.empresa_producto_id
                )
            
        _L_productos_detalle = []
        
        for _dbRow_producto in _dbRows_productos:
            
            _L_productos_detalle.append(_dbRow_producto.empresa_producto_id)
            
        #Query para buscar los productos que estén asociados tengan este proveedor como oficial.
        _dbRows_productos_nuevos = dbc01(
            (dbc01.tempresa_prodserv.empresa_proveedor_id == _s_proveedor_id)
            &(~dbc01.tempresa_prodserv.id.belongs(_L_productos_detalle))
            ).select(
                dbc01.tempresa_prodserv.ALL,
                dbc01.tempresa_prodserv_codigosproveedores.ALL,
                orderby =[
                    dbc01.tempresa_prodserv.id,
                    dbc01.tempresa_prodserv_codigosproveedores.id
                    ],
                left =[
                    dbc01.tempresa_prodserv_codigosproveedores.on(
                        (dbc01.tempresa_prodserv_codigosproveedores.empresa_prodserv_id == dbc01.tempresa_prodserv.id)
                        &(dbc01.tempresa_prodserv_codigosproveedores.empresa_proveedor_id == _s_proveedor_id)
                        &(dbc01.tempresa_prodserv_codigosproveedores.contenido == 1) #TODO: ¿Hay bronca si dejo esto como 1 en vez de crear una constante? 
                        ), 
                    ]
                )
            
        if not _dbRows_productos_nuevos:
            #Si no encontró productos nuevos entonces se manda este warning.
            
            stv_flashWarning("Productos", "No se agregaron productos nuevos") 
            
            _D_overrideView = request.stv_fwk_permissions.view_refresh
                
        else:
            
            TEMPRESA_PROVEEDOR_PRECIOOFICIAL_PRODUCTOS.INSERTAR_PRODUCTOS(
                dbRows_productos = _dbRows_productos_nuevos,
                s_empresa_proveedor_preciooficial_id = _s_empresa_proveedor_preciooficial_id
                )
            
            stv_flashSuccess("Productos", "Se agregaron productos nuevos") 
            
            _D_overrideView = request.stv_fwk_permissions.view_refresh
            
        ### Actualizar códigos precios oficiales proveedores. ###
            
        _dbRows_productos = dbc01(
            (dbc01.tempresa_prodserv.empresa_proveedor_id == _s_proveedor_id)
            &(dbc01.tempresa_prodserv.id.belongs(_L_productos_detalle))
            ).select(
                dbc01.tempresa_prodserv.ALL,
                dbc01.tempresa_prodserv_codigosproveedores.ALL,
                orderby =[
                    dbc01.tempresa_prodserv.id,
                    dbc01.tempresa_prodserv_codigosproveedores.id
                    ],
                left =[
                    dbc01.tempresa_prodserv_codigosproveedores.on(
                        (dbc01.tempresa_prodserv_codigosproveedores.empresa_prodserv_id == dbc01.tempresa_prodserv.id)
                        &(dbc01.tempresa_prodserv_codigosproveedores.empresa_proveedor_id == _s_proveedor_id)
                        &(dbc01.tempresa_prodserv_codigosproveedores.contenido == 1) #TODO: ¿Hay bronca si dejo esto como 1 en vez de crear una constante? 
                        ), 
                    ]
                )
            
        _L_productos_id = []  
        for _dbRow_producto in _dbRows_productos:
            if (_dbRow_producto.tempresa_prodserv.id not in _L_productos_id):
                if(_dbRow_producto.tempresa_prodserv_codigosproveedores.codigo):
                #Si trae código de proveedor hace la lógica para compararlo con el código que se tiene guardado en la tabla. 
                
                    _dbRows_preciosoficiales_productos = dbc01(
                        (_dbTable.empresa_proveedor_preciooficial_id == _s_empresa_proveedor_preciooficial_id)
                        &(_dbTable.empresa_producto_id == _dbRow_producto.tempresa_prodserv.id)
                        ).select(
                            _dbTable.ALL
                            )
                        
                    if(_dbRows_preciosoficiales_productos):
                        _dbRows_preciosoficiales_productos = _dbRows_preciosoficiales_productos.first()
                        
                        if(_dbRows_preciosoficiales_productos.codigoproveedor != _dbRow_producto.tempresa_prodserv_codigosproveedores.codigo):
                            #Si el código de proveedor de la tabla prodserv es diferente al que se tiene en la tabla entonces se sustituye.
                            
                            _dbRows_preciosoficiales_productos.codigoproveedor = _dbRow_producto.tempresa_prodserv_codigosproveedores.codigo
                            _dbRows_preciosoficiales_productos.update_record()
                        else:
                            pass
                        
                        
                    else:
                        pass
                else:
                    pass
                
                _L_productos_id.append(_dbRow_producto.tempresa_prodserv.id)
                
            else:
                pass

    else:
        pass
    
    #TODO: P01-360 crear validación para nuevo
    
    _O_cat.addRowContentEvent('onValidation', _empresa_proveedor_preciooficial_productos_validation)

    if(_D_overrideView):
        _D_returnVars = _O_cat.process(D_overrideView = _D_overrideView)
    else:
        _D_returnVars = _O_cat.process()
        
    if _dbRow_empresa_proveedor_preciooficial_producto:
        _D_returnVars.dbRow_empresa_proveedor_preciooficial_producto = _dbRow_empresa_proveedor_preciooficial_producto
    else:
        _D_returnVars.dbRow_empresa_proveedor_preciooficial_producto = None
        pass

    return (_D_returnVars)


def _empresa_proveedor_preciooficial_productos_validation(O_form):
    """ Validación para el manejo de los productos de precios oficiales
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_proveedor_id
        arg4: codigo de funcionalidad
        arg5: empresa_proveedor_preciooficial_id
        [arg6]: codigo de funcionalidad
        [arg7]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
               
    """

        
    if not O_form.vars.preciooficial:
        O_form.errors.preciooficial = 'No puede ir vacío el precio oficial si está editando.'
    else:
        pass

    return O_form

def empresa_proveedor_cotizaciones():
    """ Formulario para el manejo de cotizaciones
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_proveedor_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
            
    """
    
    _dbTable = dbc01.tempresa_proveedor_cotizaciones
    
    _s_proveedor_id = request.args(3, None)
        
    _s_action = request.args(4, request.stv_fwk_permissions.btn_none.code)

    _dbTable.estatus.default = TEMPRESA_PROVEEDOR_COTIZACIONES.ESTATUS.BORRADOR

    _L_OrderBy = [
        ~_dbTable.fechavigencia_inicial,
        ]
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(),
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_proveedores, 
        xFieldLinkMaster = [
            _dbTable.empresa_proveedor_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.fechavigencia_inicial,
            _dbTable.fechavigencia_final,
            _dbTable.estatus
            ], 
        L_fieldsToSearch = [
            _dbTable.id, 
            _dbTable.fechavigencia_inicial,
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:2]
        )
    
    _O_cat.addRowSearchResultsProperty('orderBy', _L_OrderBy)

    _O_cat.addRowContentEvent('onValidation', _empresa_proveedor_cotizaciones_validation)
    _O_cat.addRowContentEvent('onAccepted', _empresa_proveedor_cotizaciones_accepted)
    
    _D_returnVars = _O_cat.process()
    
    # TODO Esta lógica es exactamente igual a la logica de validar. Pls mueve la logica al modelo y ejecutala desde aqui pls.
    if (_s_action == request.stv_fwk_permissions.btn_save.code) and (_D_returnVars.rowContent_form.accepted):
        
        _D_result_ultima_cotizacion = TEMPRESA_PROVEEDOR_COTIZACIONES.ULTIMA_COTIZACION(
            s_proveedor_id = _s_proveedor_id,
            s_id = _D_returnVars.rowContent_form.vars.id
            )
        
        if(_D_result_ultima_cotizacion.E_return != CLASS_e_RETURN.NOK_ERROR):
            _dbRow_cotizacion = _D_result_ultima_cotizacion.dbRow_ultima_cotizacion
        else:
            _dbRow_cotizacion = None
            pass
        
        if(_dbRow_cotizacion):
            
            if(_dbRow_cotizacion.fechavigencia_final > _D_returnVars.rowContent_form.vars.fechavigencia_inicial):
                #Si la última fecha final es menor a la fecha incial nueva, entonces, la fecha inicial se le restará 1 día y será la nueva fecha final para
            #el ultimo registro de cotización guardado.
            
                _dbRow_cotizacion.fechavigencia_final = (_D_returnVars.rowContent_form.vars.fechavigencia_inicial - datetime.timedelta(days=1))
                _dbRow_cotizacion.update_record()
            else:
                pass
  
        else:
            pass
            
        
    else:

        pass
    
    _O_cat.addSubTab(
                dbTableDetail = dbc01.tempresa_proveedor_cotizacion_productos, 
                s_url = URL(f='empresa_proveedor_cotizacion_productos', args=request.args[:4]+['master_'+str(request.function),request.args(5)]),
                s_idHtml = fn_createIDTabUnique(f = 'empresa_proveedor_cotizacion_productos', s_masterIdentificator = str(request.function))  )


    return (_D_returnVars)

def _empresa_proveedor_cotizaciones_validation(O_form):
    """ Validación para el manejo de cotizaciones
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_proveedor_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
        
    """
    
    _dbTable = dbc01.tempresa_proveedor_cotizaciones
    
    _s_proveedor_id = request.args(3, None)
    _s_empresa_proveedor_preciooficial_id = request.args(5, None)
    _s_id = O_form.record_id
    
    #Siempre que se quiera utilizar la fecha actual se tiene que utilizar request.browsernow
    _dt_hoy = request.browsernow
    
    _dbRows = dbc01(
        (_dbTable.empresa_proveedor_id == _s_proveedor_id)
        &(_dbTable.fechavigencia_inicial == O_form.vars.fechavigencia_inicial)
        &(_dbTable.id != _s_id)
        ).select(
            _dbTable.id
            )
        
    if _dbRows:
        O_form.errors.fechavigencia_inicial = 'No se puede repetir la fecha inicial.'
    else:
        pass
    
    #Query que regresa si hay fechas mayores a la fecha actual que se quiere guardar.
    _dbRows = dbc01(
        (_dbTable.empresa_proveedor_id == _s_proveedor_id)
        &(_dbTable.fechavigencia_inicial > O_form.vars.fechavigencia_inicial)
        ).select(
            _dbTable.id
            )
        
    if _dbRows:
        #Si hay fechas mayores a la fecha actual que se quiere grabar entonces dará error.
        O_form.errors.fechavigencia_inicial = 'No puede haber una fecha inicial menor a una existente.'
    else:
        pass
    
    if(O_form.vars.fechavigencia_final):
        
        if(O_form.vars.fechavigencia_inicial == O_form.vars.fechavigencia_final):
            #Si la fecha inicial y final son la misma dará error.
            O_form.errors.fechavigencia_inicial = 'No pueden ser iguales las fechas.'
            O_form.errors.fechavigencia_final = 'No pueden ser iguales las fechas.'
        else:
            pass
        
        if((O_form.vars.fechavigencia_inicial > O_form.vars.fechavigencia_final) or (_dt_hoy > O_form.vars.fechavigencia_final) ):
            O_form.errors.fechavigencia_final = 'La fecha final no puede ser menor a la fecha inicial o la fecha actual'
        else:
            pass
        
    else:
        pass
    
#     if(_s_id):
#         _D_result_ultima_cotizacion = TEMPRESA_PROVEEDOR_COTIZACIONES.ULTIMA_COTIZACION(
#             s_proveedor_id = _s_proveedor_id,
#             s_id = _s_id
#             )
#          
#         if(_D_result_ultima_cotizacion.E_return != CLASS_e_RETURN.NOK_ERROR):
#             _dbRow_cotizacion= _D_result_ultima_cotizacion.dbRow_ultimo_preciooficial
#         else:
#             _dbRow_cotizacion = None
#             pass
#          
#         if(_dbRow_cotizacion):
#                      
#             if(_dbRow_cotizacion.fechavigencia_final):
#                 if(_dbRow_cotizacion.fechavigencia_final > O_form.vars.fechavigencia_inicial):
#                     
#                     O_form.errors.fechavigencia_inicial = 'La fecha inicial no puede ser menor a la fecha final más reciente.'
#      
#                 else:
#                     pass
#             else:
#                 pass
#         else:
#             pass
#     else:
#         pass

    return O_form

def _empresa_proveedor_cotizaciones_accepted(O_form):
    """ Validación onAccepted para el manejo de cotizaciones
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_proveedor_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
            
    """
    
    _dbTable = dbc01.tempresa_proveedor_cotizaciones
    _s_empresa_id = request.args(1, None)
    _s_proveedor_id = request.args(3, None)
    _s_empresa_proveedor_cotizacion_id = request.args(5, None)
    _s_id = O_form.vars.id
    
    #Query para saber si esta lista de precios oficiales ya tiene productos. 
    _dbRows_cotizacion_productos = dbc01(
        (dbc01.tempresa_proveedor_cotizacion_productos.tempresa_proveedor_cotizacion_id == _s_empresa_proveedor_cotizacion_id)
        ).select(
            dbc01.tempresa_proveedor_cotizacion_productos.id
            )
    
    if not(_dbRows_cotizacion_productos):
    #Si no tiene productos el detalle entonces se hace la lógica para insertar.
    
        _D_result_ultima_cotizacion = TEMPRESA_PROVEEDOR_COTIZACIONES.ULTIMA_COTIZACION(
            s_proveedor_id = _s_proveedor_id,
            s_id = _s_id
            )
    
        if(_D_result_ultima_cotizacion.E_return != CLASS_e_RETURN.NOK_ERROR):
            _dbRow_cotizacion= _D_result_ultima_cotizacion.dbRow_ultima_cotizacion
        else:
            _dbRow_cotizacion = None
            pass
        
    
        if(_dbRow_cotizacion):
        #Si ya existió una cotización antes de esta entonces se cargan todos los productos pasados con su precios. 
        
            _dbRows_productos_pasados = dbc01(
                (dbc01.tempresa_proveedor_cotizacion_productos.tempresa_proveedor_cotizacion_id == _dbRow_cotizacion.id)
                ).select(
                    dbc01.tempresa_proveedor_cotizacion_productos.ALL
                    )
            
            _L_productos_pasados_id = []
            for _dbRow_producto_pasado in _dbRows_productos_pasados:
                
                if (_dbRow_producto_pasado.codigoproveedor):
                    _s_codigoexterno = _dbRow_producto_pasado.codigoproveedor
                else:
                    _s_codigoexterno = ""
                    
                #Diccionario para insertar los productos.
                _D_insert_Helper = Storage (
                    tempresa_proveedor_cotizacion_id = _s_id,
                    empresa_producto_id = _dbRow_producto_pasado.empresa_producto_id,
                    unidad_id = _dbRow_producto_pasado.unidad_id,
                    codigoproveedor = _s_codigoexterno,
                    estatus = TEMPRESA_PROVEEDOR_COTIZACION_PRODUCTOS.ESTATUS.PENDIENTE_ACTUALIZAR,
                    cantidad_menudeo = _dbRow_producto_pasado.cantidad_menudeo,
                    precio_menudeo = _dbRow_producto_pasado.precio_menudeo,
                    cantidad_mediomayoreo = _dbRow_producto_pasado.cantidad_mediomayoreo,
                    precio_mediomayoreo = _dbRow_producto_pasado.precio_mediomayoreo,
                    cantidad_mayoreo = _dbRow_producto_pasado.cantidad_mayoreo,
                    precio_mayoreo = _dbRow_producto_pasado.precio_mayoreo
                    )
                
                dbc01.tempresa_proveedor_cotizacion_productos.insert(**_D_insert_Helper)
                
                #Se guarda el producto ID para que no pueda repetirse
                _L_productos_pasados_id.append(_dbRow_producto_pasado.empresa_producto_id)
                    

            _D_result_productos_nuevos = TEMPRESA_PROVEEDOR_COTIZACION_PRODUCTOS.PRODUCTOS_NUEVOS(
                s_empresa_id = _s_empresa_id,
                s_proveedor_id = _s_proveedor_id,
                L_productos_pasados_id = _L_productos_pasados_id
                )
            
            if(_D_result_productos_nuevos.E_return != CLASS_e_RETURN.NOK_ERROR):
                _dbRows_productos_nuevos = _D_result_productos_nuevos.dbRows_productos_nuevos
                
                TEMPRESA_PROVEEDOR_COTIZACION_PRODUCTOS.INSERTAR_PRODUCTOS(
                    dbRows_productos = _dbRows_productos_nuevos,
                    s_empresa_proveedor_cotizacion_id = _s_id
                    )
                
            else:
                _dbRows_productos_nuevos = None
                pass
                

        else:
            
            #Query para buscar los productos que estén asociados tengan este proveedor como oficial.
            _dbRows_productos = dbc01(
                (dbc01.tempresa_prodserv.empresa_id == _s_empresa_id)
                &(dbc01.tempresa_prodserv.preciotipocalculo == TEMPRESA_PRODSERV.E_TIPOCALCULO.PRECIO_COTIZADO)
                ).select(
                    dbc01.tempresa_prodserv.ALL,
                    dbc01.tempresa_prodserv_codigosproveedores.ALL,
                    orderby =[
                        dbc01.tempresa_prodserv.id,
                        dbc01.tempresa_prodserv_codigosproveedores.id
                        ],
                    left =[
                        dbc01.tempresa_prodserv_codigosproveedores.on(
                            (dbc01.tempresa_prodserv_codigosproveedores.empresa_prodserv_id == dbc01.tempresa_prodserv.id)
                            &(dbc01.tempresa_prodserv_codigosproveedores.empresa_proveedor_id == _s_proveedor_id)
                            &(dbc01.tempresa_prodserv_codigosproveedores.contenido == 1) #TODO: ¿Hay bronca si dejo esto como 1 en vez de crear una constante? 
                            ), 
                        ]
                    )
                
            TEMPRESA_PROVEEDOR_COTIZACION_PRODUCTOS.INSERTAR_PRODUCTOS(
                dbRows_productos = _dbRows_productos,
                s_empresa_proveedor_cotizacion_id = _s_id
                )
            
    else:
        #Si ya tiene productos en el detalle no hace nada. 
        pass
        

    
    return O_form

def empresa_proveedor_cotizacion_productos():
    """ Formulario para el manejo de cotizaciones
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_proveedor_id
        arg4: master_[nombre función maestro]
        arg5: empresa_proveedor_cotizacion_id
        [arg6]: codigo de funcionalidad
        [arg7]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
            
    """
    
    _dbTable = dbc01.tempresa_proveedor_cotizacion_productos
    
    _s_empresa_id = request.args(1, None)
    
    _s_proveedor_id = request.args(3, None)
    
    _s_empresa_proveedor_cotizacion_id = request.args(5, None)
    
    _s_action = request.args(6, request.stv_fwk_permissions.btn_none.code)
    
    _s_empresa_proveedor_cotizacion_producto_id = request.args(7, None)

    _dbTable.estatus.default = TEMPRESA_PROVEEDOR_COTIZACION_PRODUCTOS.ESTATUS.PENDIENTE_ACTUALIZAR
    
    if(_s_action in (request.stv_fwk_permissions.btn_edit.code, request.stv_fwk_permissions.btn_save.code )):
        #Se evita la edición de los siguientes componenetes
        _dbTable.codigoproveedor.writable = False

    else:
        pass    
    
    if(_s_empresa_proveedor_cotizacion_producto_id):
        _dbRow_producto = _dbTable(_s_empresa_proveedor_cotizacion_producto_id)
    else:
        pass

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(),
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_proveedor_cotizaciones, 
        xFieldLinkMaster = [
            _dbTable.tempresa_proveedor_cotizacion_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id,
            _dbTable.empresa_producto_id,
            _dbTable.unidad_id,
            _dbTable.codigoproveedor,
            _dbTable.estatus,
            _dbTable.cantidad_menudeo,
            _dbTable.precio_menudeo,
            _dbTable.cantidad_mediomayoreo,
            _dbTable.precio_mediomayoreo,
            _dbTable.cantidad_mayoreo,
            _dbTable.precio_mayoreo
            ], 
        L_fieldsToSearch = [
            _dbTable.id,
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:4]
        )
    

    _O_cat.addRowContentEvent('onValidation', _empresa_proveedor_cotizacion_productos_validation)
    
    #Se crea el botón especial 1 para poder actualizar los productos.
    _O_cat.addPermissionConfig(
        s_permissionCode = request.stv_fwk_permissions.special_permisson_01.code,
        s_permissionLabel = "Actualizar lista de productos",
        s_permissionIcon = "fa fa-compass",
        s_type = "button",
        s_send = "find",
        L_enabledByView = request.stv_fwk_permissions.getViewsByPermission(request.stv_fwk_permissions.btn_view),
        D_actions = Storage(
            js = Storage(
                sfn_onPress = 'stv_fwk_button_press',
                sfn_onSuccess = 'stv_fwk_button_success',
                sfn_onError = 'stv_fwk_button_error',
                ),
            swap = {},
        confirm = Storage(
            s_title = "Favor de confirmar operación", 
            s_text = "¿Está seguro de querer actualizar los productos?", 
            s_type = "warning", 
            s_btnconfirm = "Aceptar", 
            s_btncancel = "Cancelar", 
            )   
            ),
        L_options = []
        )
    
    #Se utiliza el overrideView a este nivel porque como se va a eliminar el registro tiene que hacerse antes del _O_cat.process()
    _D_overrideView = None 
    if(_s_action == request.stv_fwk_permissions.special_permisson_01.code):
        
        _dbRows_productos = dbc01(
            (_dbTable.tempresa_proveedor_cotizacion_id == _s_empresa_proveedor_cotizacion_id)
            ).select(
                _dbTable.empresa_producto_id
                )
            
        _L_productos_detalle = []
        
        for _dbRow_producto in _dbRows_productos:
            
            _L_productos_detalle.append(_dbRow_producto.empresa_producto_id)
            
        _D_result_productos_nuevos = TEMPRESA_PROVEEDOR_COTIZACION_PRODUCTOS.PRODUCTOS_NUEVOS(
            s_empresa_id = _s_empresa_id,
            s_proveedor_id = _s_proveedor_id,
            L_productos_pasados_id = _L_productos_detalle
            )
        
        if(_D_result_productos_nuevos.E_return != CLASS_e_RETURN.NOK_ERROR):
            _dbRows_productos_nuevos = _D_result_productos_nuevos.dbRows_productos_nuevos
        else:
            _dbRows_productos_nuevos = None
            pass
            
        if not _dbRows_productos_nuevos:
            #Si no encontró productos nuevos entonces se manda este warning.
            
            stv_flashWarning("Productos", "No se agregaron productos nuevos") 
            
            _D_overrideView = request.stv_fwk_permissions.view_refresh
                
        else:
            
            TEMPRESA_PROVEEDOR_COTIZACION_PRODUCTOS.INSERTAR_PRODUCTOS(
                dbRows_productos = _dbRows_productos_nuevos,
                s_empresa_proveedor_cotizacion_id = _s_empresa_proveedor_cotizacion_id
                )
            
            stv_flashSuccess("Productos", "Se agregaron productos nuevos") 
            
            _D_overrideView = request.stv_fwk_permissions.view_refresh
            
        
        ### Actualizar códigos proveedores. ###
            
        _dbRows_productos = dbc01(
            (dbc01.tempresa_prodserv.empresa_id == _s_empresa_id)
            &(dbc01.tempresa_prodserv.preciotipocalculo == TEMPRESA_PRODSERV.E_TIPOCALCULO.PRECIO_COTIZADO)
            &(dbc01.tempresa_prodserv.id.belongs(_L_productos_detalle))
            ).select(
                dbc01.tempresa_prodserv.ALL,
                dbc01.tempresa_prodserv_codigosproveedores.ALL,
                orderby =[
                    dbc01.tempresa_prodserv.id,
                    dbc01.tempresa_prodserv_codigosproveedores.id
                    ],
                left =[
                    dbc01.tempresa_prodserv_codigosproveedores.on(
                        (dbc01.tempresa_prodserv_codigosproveedores.empresa_prodserv_id == dbc01.tempresa_prodserv.id)
                        &(dbc01.tempresa_prodserv_codigosproveedores.empresa_proveedor_id == _s_proveedor_id)
                        &(dbc01.tempresa_prodserv_codigosproveedores.contenido == 1) #TODO: ¿Hay bronca si dejo esto como 1 en vez de crear una constante? 
                        ), 
                    ]
                )
            
        _L_productos_id = []  
        for _dbRow_producto in _dbRows_productos:
            if (_dbRow_producto.tempresa_prodserv.id not in _L_productos_id):
                if(_dbRow_producto.tempresa_prodserv_codigosproveedores.codigo):
                #Si trae código de proveedor hace la lógica para compararlo con el código que se tiene guardado en la tabla. 
                
                    _dbRows_cotizaciones_productos = dbc01(
                        (_dbTable.tempresa_proveedor_cotizacion_id == _s_empresa_proveedor_cotizacion_id)
                        &(_dbTable.empresa_producto_id == _dbRow_producto.tempresa_prodserv.id)
                        ).select(
                            _dbTable.ALL
                            )
                        
                    if(_dbRows_cotizaciones_productos):
                        _dbRow_cotizacion_productos = _dbRows_cotizaciones_productos.first()
                        
                        if(_dbRow_cotizacion_productos.codigoproveedor != _dbRow_producto.tempresa_prodserv_codigosproveedores.codigo):
                            #Si el código de proveedor de la tabla prodserv es diferente al que se tiene en la tabla entonces se sustituye.
                            
                            _dbRow_cotizacion_productos.codigoproveedor = _dbRow_producto.tempresa_prodserv_codigosproveedores.codigo
                            _dbRow_cotizacion_productos.update_record()
                        else:
                            pass
                        
                        
                    else:
                        pass
                else:
                    pass
                
                _L_productos_id.append(_dbRow_producto.tempresa_prodserv.id)
                
            else:
                pass
            
            

    else:
        pass

    if(_D_overrideView):
        _D_returnVars = _O_cat.process(D_overrideView = _D_overrideView)
    else:
        _D_returnVars = _O_cat.process()
        
        
    if (_s_action == request.stv_fwk_permissions.btn_save.code) and (_D_returnVars.rowContent_form.accepted):
        
        if(_s_empresa_proveedor_cotizacion_producto_id):
        #Si tiene ID de cotizacion_producto es que se está guardando la edición de un registro
            
            _dbRow_producto_actualizado = _dbTable(_s_empresa_proveedor_cotizacion_producto_id)
            
            if(_dbRow_producto.estatus ==  TEMPRESA_PROVEEDOR_COTIZACION_PRODUCTOS.ESTATUS.PENDIENTE_ACTUALIZAR):
            
                if((_dbRow_producto_actualizado.precio_mayoreo) or (_dbRow_producto_actualizado.precio_mediomayoreo) or (_dbRow_producto_actualizado.precio_menudeo)):
                #Si el estatus es Pendiente actualizar y tiene algún precio guardado entonces se pasará el estatus a Cotizado.
                    
                    _dbRow_producto_actualizado.estatus = TEMPRESA_PROVEEDOR_COTIZACION_PRODUCTOS.ESTATUS.COTIZADO
                    
                    _dbRow_producto_actualizado.update_record()
                    
                else:
                    pass
                
            elif(_dbRow_producto.estatus ==  TEMPRESA_PROVEEDOR_COTIZACION_PRODUCTOS.ESTATUS.COTIZADO):
                
                if (not (_dbRow_producto_actualizado.precio_mayoreo) and not (_dbRow_producto_actualizado.precio_mediomayoreo) and not (_dbRow_producto_actualizado.precio_menudeo)):
                #Si el estatus es cotizado y no tiene ningún precio guardado entonces se pasará a pendiente actualizar el estatus
                
                    _dbRow_producto_actualizado.estatus = TEMPRESA_PROVEEDOR_COTIZACION_PRODUCTOS.ESTATUS.PENDIENTE_ACTUALIZAR
                    
                    _dbRow_producto_actualizado.update_record()
                    
                else:
                    
                    pass
            else:
                pass
        else:
            pass
        
    else:
        pass
    

    return (_D_returnVars)


def _empresa_proveedor_cotizacion_productos_validation(O_form):
    """ Validación para el manejo de los productos de las cotizaciones
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: proveedor_id
        arg4: codigo de funcionalidad
        arg5: empresa_proveedor_cotizacion_id
        [arg6]: codigo de funcionalidad
        [arg7]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
               
    """
    
    #TODO: Voy a darle otra checada a esto. 

    if(O_form.vars.precio_menudeo):
        
        if not (O_form.vars.cantidad_menudeo):
            #Si se capturó el precio del menudeo entonces su cantidad no puede estar vacía
            O_form.errors.cantidad_menudeo = "La cantidad del menudeo no puede estar vacía si se quiere guardar un precio para este."
            pass
        else:
            pass    
        
        if(O_form.vars.precio_mediomayoreo):
            if not (O_form.vars.cantidad_mediomayoreo):
                #Si se capturó el precio del menudeo entonces su cantidad no puede estar vacía
                O_form.errors.cantidad_mediomayoreo = "La cantidad del menudeo no puede estar vacía si se quiere guardar un precio para este."
                pass
            else:
                pass
            
            if ((O_form.vars.precio_mediomayoreo > O_form.vars.precio_menudeo) or (O_form.vars.precio_mediomayoreo == O_form.vars.precio_menudeo)):
                #Si el precio de medio mayoreo es mayor al precio de menudeo entonces mostrará un error. 
                O_form.errors.precio_mediomayoreo = "El precio medio mayoreo no puede ser mayor o igual al de menudeo."
                pass
            else:
                pass
        else:
            pass
        
        if(O_form.vars.precio_mayoreo):
            
            if not (O_form.vars.cantidad_mayoreo):
                #Si se capturó el precio del menudeo entonces su cantidad no puede estar vacía
                O_form.errors.cantidad_mayoreo = "La cantidad del mayoreo no puede estar vacía si se quiere guardar un precio para este."
                pass
            
            else:
                pass
            
            if ((O_form.vars.precio_mayoreo > O_form.vars.precio_menudeo) or (O_form.vars.precio_mayoreo == O_form.vars.precio_menudeo)):
                #Si el precio de medio mayoreo es mayor al precio de menudeo entonces mostrará un error. 
                O_form.errors.precio_mayoreo = "El precio mayoreo no puede ser mayor o igual al de menudeo."
                pass
            else:
                pass
        else:
            pass
        
    elif(O_form.vars.precio_mediomayoreo):
        
        if not (O_form.vars.cantidad_mediomayoreo):
            #Si se capturó el precio del menudeo entonces su cantidad no puede estar vacía
            O_form.errors.cantidad_mediomayoreo = "La cantidad del menudeo no puede estar vacía si se quiere guardar un precio para este."
            pass
        else:
            pass
        
        if(O_form.vars.precio_mayoreo):
            if not (O_form.vars.cantidad_mayoreo):
                #Si se capturó el precio del menudeo entonces su cantidad no puede estar vacía
                O_form.errors.cantidad_mayoreo = "La cantidad del mayoreo no puede estar vacía si se quiere guardar un precio para este."
                pass
            else:
                pass
            if ((O_form.vars.precio_mayoreo > O_form.vars.precio_mediomayoreo) or (O_form.vars.precio_mayoreo == O_form.vars.precio_mediomayoreo)):
                #Si el precio de medio mayoreo es mayor al precio de menudeo entonces mostrará un error. 
                O_form.errors.precio_mayoreo = "El precio mayoreo no puede ser mayor o igual al de medio mayoreo."
                pass
            else:
                pass
            
    elif(O_form.vars.precio_mayoreo):
        
        if not (O_form.vars.cantidad_mayoreo):
            #Si se capturó el precio del menudeo entonces su cantidad no puede estar vacía
            O_form.errors.cantidad_mayoreo = "La cantidad del mayoreo no puede estar vacía si se quiere guardar un precio para este."
            pass
        else:
            pass
    else:
        pass
    
    
    if(O_form.vars.cantidad_menudeo):
                
        if not(O_form.vars.precio_menudeo):
            O_form.errors.precio_menudeo = "El precio menudeo no puede no ser capturado si se capturó su cantidad de menudeo"
        else:
            pass
        
        if(O_form.vars.cantidad_mediomayoreo):
            
            if not(O_form.vars.precio_mediomayoreo):
                O_form.errors.precio_mediomayoreo = "El precio medio mayoreo no puede no ser capturado si se capturó su cantidad de medio mayoreo"
            else:
                pass 
            
            if((O_form.vars.cantidad_mediomayoreo < O_form.vars.cantidad_menudeo) or (O_form.vars.cantidad_mediomayoreo == O_form.vars.cantidad_menudeo)):
                O_form.errors.cantidad_mediomayoreo = "La cantidad medio mayoreo no puede ser menor o igual a la cantidad de menudeo."
            else:
                pass
            
        else:
            pass
        
        if(O_form.vars.cantidad_mayoreo):
            
            if not(O_form.vars.precio_mayoreo):
                O_form.errors.precio_mayoreo = "El precio mayoreo no puede no ser capturado si se capturó su cantidad de mayoreo"
            else:
                pass
            
            
            if((O_form.vars.cantidad_mayoreo < O_form.vars.cantidad_menudeo) or (O_form.vars.cantidad_mayoreo == O_form.vars.cantidad_menudeo)):
                O_form.errors.cantidad_mayoreo = "La cantidad mayoreo no puede ser menor o igual a la cantidad de menudeo."
            else:
                pass
        
    elif O_form.vars.cantidad_mediomayoreo:
        
        if not(O_form.vars.precio_mediomayoreo):
            O_form.errors.precio_mediomayoreo = "El precio mediomayoreo no puede no ser capturado si se capturó su cantidad de medio mayoreo"
        else:
            pass


        if(O_form.vars.cantidad_mayoreo):
            
            if not(O_form.vars.precio_mayoreo):
                O_form.errors.precio_mayoreo = "El precio medio mayoreo no puede no ser capturado si se capturó su cantidad de medio mayoreo"
            else:
                pass
            
            if((O_form.vars.cantidad_mayoreo < O_form.vars.cantidad_menudeo) or (O_form.vars.cantidad_mayoreo == O_form.vars.cantidad_menudeo)):
                O_form.errors.cantidad_mayoreo = "La cantidad mayoreo no puede ser menor o igual a la cantidad de menudeo."
            else:
                pass
            
            if((O_form.vars.cantidad_mayoreo < O_form.vars.cantidad_mediomayoreo) or (O_form.vars.cantidad_mayoreo == O_form.vars.cantidad_mediomayoreo)):
                O_form.errors.cantidad_mayoreo = "La cantidad mayoreo no puede ser menor o igual a la cantidad de medio mayoreo."
            else:
                pass
            
        else:
            pass

    elif O_form.vars.cantidad_mayoreo:
        
        if not(O_form.vars.precio_mayoreo):
            O_form.errors.precio_mayoreo = "El precio mayoreo no puede no ser capturado si se capturó su cantidad de mayoreo"
        else:
            pass

    else:
        pass
    
    
    if(O_form.vars.cantidad_menudeo):
        if(O_form.vars.cantidad_menudeo < 0):
            O_form.errors.cantidad_menudeo = "La cantidad del menudeo no puede ser menor a 0"
        else:
            pass
    else:
        pass
    
    if(O_form.vars.cantidad_mediomayoreo):
        if(O_form.vars.cantidad_mediomayoreo < 0):
            O_form.errors.cantidad_mediomayoreo = "La cantidad del menudeo no puede ser menor a 0"
        else:
            pass
    else:
        pass
    
    if(O_form.vars.cantidad_mayoreo):
        if(O_form.vars.cantidad_mayoreo < 0):
            O_form.errors.cantidad_mayoreo = "La cantidad del menudeo no puede ser menor a 0"
        else:
            pass
    else:
        pass
        
    return O_form