# -*- coding: utf-8 -*-

def empresa_paginaweb():
    """ Formulario de página web por empresa

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    """
 
    _dbTable = dbc01.tempresa_paginaweb
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas, 
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.tipo,
            _dbTable.nombrecorto,
            _dbTable.descripcion
            ], 
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
    )
    
    _O_cat.addSubTab(
        s_tabName = 'Noticias', 
        s_url = URL(f='empresa_paginaweb_noticias', args=request.args[:2]+['master_'+str(request.function),request.args(3)]),
        s_idHtml = fn_createIDTabUnique(f='empresa_paginaweb_noticias', s_masterIdentificator = str(request.function))  
        )

    _O_cat.addSubTab(
        s_tabName = 'Tips', 
        s_url = URL(f='empresa_paginaweb_tips', args=request.args[:2]+['master_'+str(request.function),request.args(3)]),
        s_idHtml = fn_createIDTabUnique(f='empresa_paginaweb_tips', s_masterIdentificator = str(request.function))  
        )
    
    _O_cat.addSubTab(
        s_tabName = 'Promociones', 
        s_url = URL(f='empresa_paginaweb_promociones', args=request.args[:2]+['master_'+str(request.function),request.args(3)]),
        s_idHtml = fn_createIDTabUnique(f='empresa_paginaweb_promociones', s_masterIdentificator = str(request.function))  
        )
    
    _D_returnVars = _O_cat.process()

    return (_D_returnVars)

def empresa_paginaweb_noticias():
    """ Formulario de noticias por página web

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestr]
        arg3: paginaweb_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    """
 
    _dbTable = dbc01.tempresa_paginaweb_noticias
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_paginaweb, 
        xFieldLinkMaster = [
            _dbTable.empresa_paginaweb_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.titulo,
            _dbTable.fechainicio,
            _dbTable.fechafin,
            _dbTable.link,
            ], 
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:2]
    )

    _D_returnVars = _O_cat.process()

    return (_D_returnVars)

def empresa_paginaweb_tips():
    """ Formulario de tips por página web

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestr]
        arg3: paginaweb_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    """
 
    _dbTable = dbc01.tempresa_paginaweb_tips
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_paginaweb, 
        xFieldLinkMaster = [
            _dbTable.empresa_paginaweb_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.titulo,
            _dbTable.tipo,
            _dbTable.linkvideo,
            _dbTable.contenido,
            ], 
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:2]
    )

    _D_returnVars = _O_cat.process()

    return (_D_returnVars)


def empresa_paginaweb_promociones():
    """ Formulario de tips por página web

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestr]
        arg3: paginaweb_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    """
 
    _dbTable = dbc01.tempresa_paginaweb_promociones
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_paginaweb, 
        xFieldLinkMaster = [
            _dbTable.empresa_paginaweb_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.titulo,
            _dbTable.fechainicio,
            _dbTable.fechafin,
            _dbTable.descripcion,
            _dbTable.reglas
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:2]
    )
    
    _O_cat.addSubTab(
        s_tabName = 'Productos', 
        s_url = URL(f='empresa_paginaweb_promocion_productos', args=request.args[:4]+['master_'+str(request.function),request.args(5)]),
        s_idHtml = fn_createIDTabUnique(f='empresa_paginaweb_promocion_productos', s_masterIdentificator = str(request.function))  
        )
    
    _O_cat.addSubTab(
        s_tabName = 'Categorias', 
        s_url = URL(f='empresa_paginaweb_promocion_categorias', args=request.args[:4]+['master_'+str(request.function),request.args(5)]),
        s_idHtml = fn_createIDTabUnique(f='empresa_paginaweb_promocion_categorias' , s_masterIdentificator = str(request.function))  
        )

    _O_cat.addSubTab(
        s_tabName = 'Marcas', 
        s_url = URL(f='empresa_paginaweb_promocion_marcas', args=request.args[:4]+['master_'+str(request.function),request.args(5)]),
        s_idHtml = fn_createIDTabUnique(f='empresa_paginaweb_promocion_marcas' , s_masterIdentificator = str(request.function))  
        )
    
    _D_returnVars = _O_cat.process()

    return (_D_returnVars)

def empresa_paginaweb_promocion_productos():
    """ Formulario de tips por página web

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestr]
        arg3: paginaweb_id
        arg4: master_[nombre función maestro]
        arg5: promocion_id
        [arg6]: codigo de funcionalidad
        [arg7]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    """
 
    _dbTable = dbc01.tempresa_paginaweb_promocion_productos
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_paginaweb_promociones, 
        xFieldLinkMaster = [
            _dbTable.tempresa_paginaweb_promocion_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.producto_id
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:4]
    )

    _D_returnVars = _O_cat.process()

    return (_D_returnVars)

def empresa_paginaweb_promocion_categorias():
    """ Formulario de tips por página web

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestr]
        arg3: paginaweb_id
        arg4: master_[nombre función maestro]
        arg5: promocion_id
        [arg6]: codigo de funcionalidad
        [arg7]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    """
 
    _dbTable = dbc01.tempresa_paginaweb_promocion_categorias
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_paginaweb_promociones, 
        xFieldLinkMaster = [
            _dbTable.tempresa_paginaweb_promocion_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.categoria_id
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:4]
    )

    _D_returnVars = _O_cat.process()

    return (_D_returnVars)


def empresa_paginaweb_promocion_marcas():
    """ Formulario de tips por página web

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestr]
        arg3: paginaweb_id
        arg4: master_[nombre función maestro]
        arg5: promocion_id
        [arg6]: codigo de funcionalidad
        [arg7]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    """
 
    _dbTable = dbc01.tempresa_paginaweb_promocion_marcas
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_paginaweb_promociones, 
        xFieldLinkMaster = [
            _dbTable.tempresa_paginaweb_promocion_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.marca_id
            ],
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:4]
    )

    _D_returnVars = _O_cat.process()

    return (_D_returnVars)