# -*- coding: utf-8 -*-


def dashboard_v1r0():
    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_find)    
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbTable = None, 
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none
            ], 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [], 
        L_fieldsToSearch = [], 
        L_fieldsToSearchIfDigit = [],
        x_offsetInArgs = request.args[:2]
    )

    _D_returnVars = _O_cat.process(D_overrideView = request.stv_fwk_permissions.view_viewRegister)
    
    _D_returnVars.L_boxStatus = []
    _D_returnVars.L_boxStatus.append( # Box 0
        Storage(
            s_id = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab + "_statusbox1",
            s_addClass = "status",
            b_createBox = True,
            s_title = '''
                <span class="label label-success float-right">Monthly</span>
                <h5>Income</h5>''',
            s_content = '''
                <h1 class="no-margins">Cargando</h1>'''
            ),
        )
    _D_returnVars.L_boxStatus.append( # Box 0
        Storage(
            s_id = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab + "_flotchart1",
            s_addClass = "status",
            b_createBox = True,
            s_title = '''
                <h5>Orders</h5>''',
            s_content = '''
                <h1 class="no-margins">Cargando</h1>'''
            ),
        )
    
    return (_D_returnVars)

def statusbox1():
    
    _s_id = request.args(0)
    
    _D_returnVars = Storage(
        D_params = Storage(
            s_id = _s_id,
            s_addClass = "status",
            b_createBox = False,
            s_title = '''
                <span class="label label-success float-right">Monthly</span>
                <h5>Income</h5>''',
            s_content = '''
                <h1 class="no-margins">40 886,200</h1>
                <div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div>
                <small>Total income</small>'''
            )
        )
    
    return(_D_returnVars)
    
def flotchart1():
    
    _s_id = request.args(0)
    
    _D_returnVars = Storage(
        D_params = Storage(
            s_id = _s_id,
            s_addClass = "status",
            b_createBox = False,
            s_title = '''
                <h5>Orders</h5>''',
            s_content = '',
            )
        )
    
    return(_D_returnVars)

def call():
    return service()
        


