# -*- coding: utf-8 -*-


def marcas():
    _dbTable = dbc01.tmarcas

    _O_cat = STV_FWK_FORM(
                dbReference = dbc01,
                dbTable = _dbTable, 
                L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
                s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
                L_fieldsToShow = [_dbTable.id, _dbTable.nombrecorto, _dbTable.bansuspender], 
                L_fieldsToSearch = [_dbTable.id, _dbTable.nombrecorto], 
                L_fieldsToSearchIfDigit = [_dbTable.id],
                )

    _D_returnVars = _O_cat.process()
    
    return (_D_returnVars)



def call():
    return service()
        


