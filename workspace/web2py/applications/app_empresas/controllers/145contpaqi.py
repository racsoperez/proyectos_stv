# -*- coding: utf-8 -*-

def empresa_contpaqi():
    """ Detalle de configuración de CONTPAQi.
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """   
    request.stv_fwk_permissions.L_formButtonsAll.remove(request.stv_fwk_permissions.btn_find)    
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbTable = None, 
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none
            ], 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [], 
        L_fieldsToSearch = [], 
        L_fieldsToSearchIfDigit = [],
        x_offsetInArgs = request.args[:2]
        )

    _O_cat.addSubTab(
                dbTableDetail = dbc01.tempresa_diarios, 
                s_url = URL(f='empresa_diarios', args=['master_'+str(request.function),request.args(1)]),
                s_idHtml = fn_createIDTabUnique(f = 'empresa_diarios', s_masterIdentificator = str(request.function))  )
    
    _O_cat.addSubTab(
                dbTableDetail = dbc01.tempresa_segmentos, 
                s_url = URL(f='empresa_segmentos', args=['master_'+str(request.function),request.args(1)]),
                s_idHtml = fn_createIDTabUnique(f = 'empresa_segmentos', s_masterIdentificator = str(request.function))  )

    _O_cat.addSubTab(
                dbTableDetail = dbc01.tempresa_tipospoliza, 
                s_url = URL(f='empresa_tipospoliza', args=['master_'+str(request.function),request.args(1)]),
                s_idHtml = fn_createIDTabUnique(f = 'empresa_tipospoliza', s_masterIdentificator = str(request.function))  )

    _O_cat.addSubTab(
                dbTableDetail = dbc01.tempresa_bancosdiariosespeciales, 
                s_url = URL(f='empresa_bancosdiariosespeciales', args=['master_'+str(request.function),request.args(1)]),
                s_idHtml = fn_createIDTabUnique(f = 'empresa_bancosdiariosespeciales', s_masterIdentificator = str(request.function))  )
    
    _O_cat.addSubTab(
                dbTableDetail = dbc01.tempresa_monedascontpaqi, 
                s_url = URL(f='empresa_monedascontpaqi', args=['master_'+str(request.function),request.args(1)]),
                s_idHtml = fn_createIDTabUnique(f = 'empresa_monedascontpaqi', s_masterIdentificator = str(request.function))  )
   
    _O_cat.addSubTab(
                dbTableDetail = dbc01.tempresa_tipodocumentosbancarios, 
                s_url = URL(f='empresa_tipodocumentosbancarios', args=['master_'+str(request.function),request.args(1)]),
                s_idHtml = fn_createIDTabUnique(f = 'empresa_tipodocumentosbancarios', s_masterIdentificator = str(request.function))  )

    _O_cat.addSubTab(
                dbTableDetail = dbc01.tempresa_contpaqiejercicios, 
                s_url = URL(f='empresa_contpaqiejercicios', args=['master_'+str(request.function),request.args(1)]),
                s_idHtml = fn_createIDTabUnique(f = 'empresa_contpaqiejercicios', s_masterIdentificator = str(request.function))  )
    
    _O_cat.addSubTab(
                dbTableDetail = dbc01.tempresa_bancoscontpaqi, 
                s_url = URL(f='empresa_bancoscontpaqi', args=['master_'+str(request.function),request.args(1)]),
                s_idHtml = fn_createIDTabUnique(f = 'empresa_bancoscontpaqi', s_masterIdentificator = str(request.function))  )

    _O_cat.addSubTab(
                dbTableDetail = dbc01.tempresa_fechaspolizas, 
                s_url = URL(f='empresa_fechaspolizas', args=['master_'+str(request.function),request.args(1)]),
                s_idHtml = fn_createIDTabUnique(f = 'empresa_fechaspolizas', s_masterIdentificator = str(request.function))  )

    _O_cat.addSubTab(
                dbTableDetail = dbc01.tempresa_esquemasimpuestostrasladados, 
                s_url = URL(
                    c = '120empresas',
                    f = 'empresa_esquemasimpuestostrasladados', 
                    args = ['master_'+str(request.function),request.args(1)]
                    ),
                s_idHtml = fn_createIDTabUnique(
                    c = '120empresas',
                    f = 'empresa_esquemasimpuestostrasladados', 
                    s_masterIdentificator = str(request.function)
                    )  
                )

    _O_cat.addSubTab(
                dbTableDetail = dbc01.tempresa_erroresexportacioncontpaqi,
                s_url = URL(f='poliza_erroresexportacioncontpaqi', args=['master_'+str(request.function),request.args(1)]),
                s_idHtml = fn_createIDTabUnique(f = 'poliza_erroresexportacioncontpaqi', s_masterIdentificator = str(request.function)))


    
    _D_returnVars = _O_cat.process(D_overrideView = request.stv_fwk_permissions.view_viewRegister)

    return dict(_D_returnVars)

''' Diarios especiales CONTPAQi '''
def empresa_diarios():
    """ Formulario para el manejo de los Diarios especiales por empresa.
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """   
    
    _dbTable = dbc01.tempresa_diarios

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas, 
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.codigo, 
            _dbTable.nombre, 
            _dbTable.tipo, 
            _dbTable.bansuspender
            ], 
        L_fieldsToSearch = [
            _dbTable.codigo, 
            _dbTable.nombre
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            _dbTable.tipo
            ],
        )
        
    _O_cat.addRowContentEvent('onValidation', _empresa_diarios_validation)
    _D_returnVars = _O_cat.process()

    return (_D_returnVars)

def _empresa_diarios_validation(O_form):
    """ Validación para los diarios especiales por empresa.
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    
    """   
    
    _dbTable = dbc01.tempresa_diarios
    _s_empresa_id = request.args(1, None)
    _s_nombre = O_form.vars.nombre
    _s_codigoCONTPAQi = O_form.vars.codigo
    _s_tipodiario = O_form.vars.tipo
    _s_tipodocumento_pago = O_form.vars.tipo_documento_pago_contpaqi
    _s_id = O_form.record_id

    
    if not (_s_codigoCONTPAQi):
        
        O_form.errors.codigo = 'El código no puede estar vacío.'
        
    else:
        #Validación para evitar el duplicado de código por empresa.
        _dbRows = dbc01(
            (_dbTable.empresa_id == _s_empresa_id)
            &(_dbTable.codigo == _s_codigoCONTPAQi)
            ).select(
                _dbTable.id
                )
        if _dbRows:
            O_form.errors.codigo = 'No se puede repetir el CompaqID'
        else:
            pass

    if not (_s_nombre):
        
        O_form.errors.nombre = 'El nombre no puede estar vacío.'
        
    else:
        
        _dbRows = dbc01(
            (_dbTable.empresa_id == _s_empresa_id)
            &(_dbTable.nombre == _s_nombre)
            ).select(
                _dbTable.id
                )
            
        if _dbRows:
            O_form.errors.nombre = 'No se puede repetir el nombre'
        else:
            pass
        
    if(_s_tipodiario):
        if(int(_s_tipodiario) in ( TEMPRESA_DIARIOS.E_TIPO.EFECTIVO_INGRESO, TEMPRESA_DIARIOS.E_TIPO.EFECTIVO_EGRESO,)):
            if not(_s_tipodocumento_pago):
                O_form.errors.tipo_documento_pago_contpaqi = "El tipo de documento de pago no puede estar vacío si se seleccinó Tipo diario de Egreso o Ingreso."
            else:
                pass
        else:
            pass
    else:
        pass

    return O_form

''' Segmentos de negocio CONTPAQi '''
def empresa_segmentos():
    """ Formulario para el manejo de los segmentos de negocio por empresa.
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    
    """   
    
    _dbTable = dbc01.tempresa_segmentos

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas, 
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.codigo, 
            _dbTable.nombre, 
            _dbTable.bansuspender
            ], 
        L_fieldsToSearch = [
            _dbTable.codigo, 
            _dbTable.nombre
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        )

    _O_cat.addRowContentEvent('onValidation', _empresa_segmentos_validation)
    _D_returnVars = _O_cat.process()

    return (_D_returnVars)

def _empresa_segmentos_validation(O_form):
    """ Validación para los segmentos de negocio.
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """   
    
    _dbTable = dbc01.tempresa_segmentos
    _s_nombre = O_form.vars.nombre
    _s_codigoCONTPAQi = O_form.vars.codigo
    _s_id = O_form.record_id
    _s_empresa_id = request.args(1, None)

    
    if not (_s_codigoCONTPAQi):
        
        O_form.errors.codigo = 'El código no puede estar vacío.'
        
    else:
        #Validación para evitar el duplicado de código por empresa.
        _dbRows = dbc01(
            (_dbTable.empresa_id == _s_empresa_id)
            &(_dbTable.codigo == _s_codigoCONTPAQi)
            ).select(
                _dbTable.id
                )
        if _dbRows:
            O_form.errors.codigo = 'No se puede repetir el CompaqID'
        else:
            pass

    if not (_s_nombre):
        
        O_form.errors.nombre = 'El nombre no puede estar vacío.'
        
    else:
        
        _dbRows = dbc01(
            (_dbTable.empresa_id == _s_empresa_id)
            &(_dbTable.nombre == _s_nombre)
            ).select(
                _dbTable.id
                )
        if _dbRows:
            O_form.errors.nombre = 'No se puede repetir el nombre'
        else:
            pass

    return O_form


''' Tipos de póliza CONTPAQi '''
def empresa_tipospoliza():
    """ Formulario para el manejo de los tipo de póliza por empresa.
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    
    """   
    
    _dbTable = dbc01.tempresa_tipospoliza

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas, 
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.codigo, 
            _dbTable.nombre, 
            _dbTable.bansuspender
            ], 
        L_fieldsToSearch = [
            _dbTable.codigo, 
            _dbTable.nombre
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
        )

    _O_cat.addRowContentEvent('onValidation', _empresa_tipospoliza_validation)        
    _D_returnVars = _O_cat.process()
    
    _O_cat.addSubTab(
                dbTableDetail = dbc01.tempresa_tipopoliza_ejercicios, 
                s_url = URL(f='empresa_tipopoliza_ejercicios', args=request.args[:2]+['master_'+str(request.function),request.args(3)]),
                s_idHtml = fn_createIDTabUnique(f = 'empresa_tipopoliza_ejercicios', s_masterIdentificator = str(request.function))  )

    return (_D_returnVars)

def _empresa_tipospoliza_validation(O_form):
    """ Validación para los tipo de póliza por empresa.
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    
    """ 
    
    _dbTable = dbc01.tempresa_tipospoliza
    _s_nombre = O_form.vars.nombre
    _s_codigoCONTPAQi = O_form.vars.codigo
    _s_id = O_form.record_id
    _s_empresa_id = request.args(1, None)

    if not (_s_codigoCONTPAQi):
        
        O_form.errors.codigo = 'El código no puede estar vacío.'
        
    else:
        #Validación para evitar el duplicado de código por empresa.
        _dbRows = dbc01(
            (_dbTable.empresa_id == _s_empresa_id)
            &(_dbTable.codigo == _s_codigoCONTPAQi)
            & (_dbTable.id != O_form.record_id)  # Se ignora el registro que se esté modificando
            ).select(
                _dbTable.id
                )
        if _dbRows:
            O_form.errors.codigo = 'No se puede repetir el CompaqID'
        else:
            pass

    if not (_s_nombre):
        
        O_form.errors.nombre = 'El nombre no puede estar vacío.'
        
    else:
        
        _dbRows = dbc01(
            (_dbTable.empresa_id == _s_empresa_id)
            &(_dbTable.nombre == _s_nombre)
            & (_dbTable.id != O_form.record_id)  # Se ignora el registro que se esté modificando
            ).select(
                _dbTable.id
                )
        if _dbRows:
            O_form.errors.nombre = 'No se puede repetir el nombre'
        else:
            pass

    return O_form


def _empresa_tipopoliza_ejercicios_validation(O_form):
    """ Validación de los ejercicios por tipo de poliza

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_tipopoliza_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """
    
    _dbTable = dbc01.tempresa_tipopoliza_ejercicios
    
    _s_empresa_id = request.args(1, None)
    
    _s_empresa_tipopoliza_id = request.args(3, None)
        
    if 'ejercicio' in O_form.vars:
        _s_ejercicio = O_form.vars.ejercicio
    else:
        _s_ejercicio = None
    
    _dbRows = dbc01(
        (_dbTable.empresa_tipopoliza_id == _s_empresa_tipopoliza_id) 
        & (_dbTable.ejercicio == _s_ejercicio)
        & (_dbTable.id != O_form.record_id)  # Se ignora el registro que se esté modificando
        ).select(_dbTable.id)
    if _dbRows:
        O_form.errors.ejercicio = "Tipo poliza y ejercicio no puede duplicarse"
    else:
        pass
     
    _s_msgError = VALIDACIONES.EJERCICIO.ES_VALIDO(_s_empresa_id, _s_ejercicio)
    
    if _s_msgError:
        O_form.errors.id = _s_msgError
    else:
        pass
    
    return O_form

''' Diarios especiales CONTPAQi '''
def empresa_tipopoliza_ejercicios():
    """ Formulario para el manejo de los ejercicios por tipo de poliza

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_tipopoliza_id
        [arg4]: codigo de funcionalidad
        [arg5]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    
    """
    
    _dbTable = dbc01.tempresa_tipopoliza_ejercicios

    _s_tipopoliza_id = request.args(3, None)

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_tipospoliza, 
        xFieldLinkMaster = [_dbTable.empresa_tipopoliza_id], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.ejercicio, 
            _dbTable.foliado, 
            _dbTable.folioejercicio, 
            _dbTable.folioperiodo01, 
            _dbTable.folioperiodo02, 
            _dbTable.folioperiodo03, 
            _dbTable.folioperiodo04, 
            _dbTable.folioperiodo05, 
            _dbTable.folioperiodo06, 
            _dbTable.folioperiodo07, 
            _dbTable.folioperiodo08, 
            _dbTable.folioperiodo09, 
            _dbTable.folioperiodo10, 
            _dbTable.folioperiodo11, 
            _dbTable.folioperiodo12, 
            _dbTable.bansuspender
            ], 
        L_fieldsToSearch = [
            _dbTable.ejercicio, 
            _dbTable.folioejercicio], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            _dbTable.ejercicio
            ],
        x_offsetInArgs = request.args[:2]
        )
            
    _O_cat.addRowContentEvent('onValidation', _empresa_tipopoliza_ejercicios_validation)
        
    _D_returnVars = _O_cat.process()

    return (_D_returnVars)


def empresa_bancosdiariosespeciales():
    """ Formulario para los diarios especiales de ingreso para los bancos por ventas de contado y cobranza.
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: operación a realizar en el formulario
        [arg3]: id en caso de existir relación a un registro en la operación
    
    """ 
    
    _s_maestro = request.args(0, 'maestro')
    _s_empresa_id = request.args(1, None)
    _s_action = request.args(2, None)
    _s_id = request.args(3, None)

    _dbTable = dbc01.tempresa_bancosdiariosespeciales

    from gluon.storage import List
        
    request.args = List()   
    request.args.append(_s_maestro)
    request.args.append(_s_empresa_id)
    if _s_id and (_s_action in (request.stv_fwk_permissions.btn_edit.code, request.stv_fwk_permissions.btn_view.code, request.stv_fwk_permissions.btn_save.code)):
        request.args.append(_s_action)
        request.args.append(_s_id)
    else:
        _dbRows = dbc01(_dbTable.empresa_id == _s_empresa_id).select(_dbTable.id)
        if (_dbRows):
            _dbRow = _dbRows.last()
            request.args.append(request.stv_fwk_permissions.btn_view.code)
            request.args.append(_dbRow.id)
        else:
            request.args.append(request.stv_fwk_permissions.btn_new.code)         
        
    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.bancos_venta_contado_diario_id,
        _dbTable.bancos_cobranza_diario_id,
        _dbTable.bancos_tipo_documentos_venta_contado,
        _dbTable.bancos_tipo_documentos_cobranza
        ]
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas, 
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow, 
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
        Es_behavior = STV_FWK_FORM.ES_BEHAVIOR.S_TAB_ONLYFORM
        )
    
    _D_returnVars = _O_cat.process()
    
    if not _s_action:
        _D_returnVars.D_useView.E_buttonsBarPosition = STV_FWK_PERMISSIONS.E_POSITION_UP
        _D_returnVars.D_useView.L_actionsCreateView.append(request.args(2, None))
        _D_returnVars.D_useView.b_serverCreatesTab = False
    elif _D_returnVars.D_useView not in(request.stv_fwk_permissions.view_editRegister, request.stv_fwk_permissions.view_newRegister, request.stv_fwk_permissions.view_refresh):
        _D_returnVars.D_useView = request.stv_fwk_permissions.view_viewRegister
    else:
        pass

    return (_D_returnVars)

def empresa_monedascontpaqi():
    """ Formulario para el manejo de los códigos de monedas que utilizan en CONTPAQi.
    
    Args:
        arg0: texto en relación al maestro empresas
        arg1: empresa_id
        [arg2]: operación a realizar en el formulario
        [arg3]: id en caso de existir relación a un registro en la operación
    
    """ 
    
    _dbTable = dbc01.tempresa_monedascontpaqi
    
    _s_action = request.args(2, request.stv_fwk_permissions.btn_none.code)
    _s_empresa_monedacontpaqi_id = request.args(3, None)
    
    if (_s_action == request.stv_fwk_permissions.btn_save.code):
        _s_moneda_id = request.vars.moneda_id 
        _dbRowMoneda = db01.tmonedas(_s_moneda_id)
        
        if _s_empresa_monedacontpaqi_id:
            # Si trae el ID de monedacontpaqi entonces se tiene que hacer un update al row. 
            _dbRowMonedaContpaqi = _dbTable(_s_empresa_monedacontpaqi_id)
            _dbRowMonedaContpaqi.descripcion = _dbRowMoneda.descripcion
            _dbRowMonedaContpaqi.update_record()
        else:
            # Si se está guardando desde Nuevo se pone la descripción como default. 
            _dbTable.descripcion.default = _dbRowMoneda.descripcion
    else:
        pass
        
#    request.stv_fwk_permissions.updateViewsFor_edit_form()
    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.moneda_id,
        _dbTable.moneda_contpaqi,
        _dbTable.descripcion
        ]
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas, 
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow, 
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
        )
    
    _O_cat.addRowContentEvent('onValidation', _empresa_monedascontpaqi_validation)
    
    _D_returnVars = _O_cat.process()

    return (_D_returnVars)

def _empresa_monedascontpaqi_validation(O_form):
    """ Validación del código moneda contpaqi

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """
    
    _dbTable = dbc01.tempresa_monedascontpaqi
    
    _s_empresa_id = request.args(1, None)
    
    if 'moneda_id' in O_form.vars:
        _s_moneda_id = O_form.vars.moneda_id
    else:
        _s_moneda_id = None
        
    if 'moneda_contpaqi' in O_form.vars:
        _s_codigo_moneda_contpaqi = O_form.vars.moneda_contpaqi
    else:
        _s_codigo_moneda_contpaqi = None
        
    #Validación 1: Si la moneda coinciden con un registro para esta empresa en la BD no puede crearse.
    _dbRows = dbc01(
        (_dbTable.empresa_id == _s_empresa_id)
        & (_dbTable.moneda_id == _s_moneda_id)
        & (_dbTable.id != O_form.record_id)  # Se ignora el registro que se esté modificando
        ).select(_dbTable.id)
    if _dbRows:
        O_form.errors.moneda_id = "La moneda no puede repetirse."
    else:
        pass


    if not _s_codigo_moneda_contpaqi:
        O_form.errors.moneda_contpaqi = "El código de CONTPAQi no puede estar vacío."
    else:
        #Validación 2: Si el código CONTPAQi coinciden con un registro para esta empresa en la BD no puede crearse.
       
        _dbRows = dbc01(
            (_dbTable.empresa_id == _s_empresa_id)
            & (_dbTable.moneda_contpaqi == _s_codigo_moneda_contpaqi)
            & (_dbTable.id != O_form.record_id)  # Se ignora el registro que se esté modificando
            ).select(_dbTable.id)
        if _dbRows:
            O_form.errors.moneda_contpaqi = "La moneda no puede repetirse."
        else:
            pass
    
    return O_form


def empresa_tipodocumentosbancarios():
    """ Formulario para el manejo de tipo de documentos bancarios utilizados en CONTPAQi.
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """
    
    _dbTable = dbc01.tempresa_tipodocumentosbancarios
        
    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.codigo_contpaqi,
        _dbTable.tipo,
        _dbTable.descripcion,
        ]
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas, 
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow, 
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
        )
    
    _O_cat.addRowContentEvent('onValidation', _empresa_tipodocumentosbancarios_validation)
    
    _D_returnVars = _O_cat.process()
    
    _O_cat.addSubTab(
        dbTableDetail = dbc01.tempresa_tipodocumentobancario_ejercicios, 
        s_url = URL(f='empresa_tipodocumentobancario_ejercicios', args=request.args[:2]+['master_'+str(request.function),request.args(3)]),
        s_idHtml = fn_createIDTabUnique(f = 'empresa_tipodocumentobancario_ejercicios', s_masterIdentificator = str(request.function))  )
     


    return (_D_returnVars)

def _empresa_tipodocumentosbancarios_validation(O_form):
    """ Validación para tipo de documentos bancarios. 

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """
    
    _dbTable = dbc01.tempresa_tipodocumentosbancarios
    
    _s_empresa_id = request.args(1, None)
    _s_descripcion = O_form.vars.descripcion
    
    if 'codigo_contpaqi' in O_form.vars:
        _s_codigo_contpaqi = O_form.vars.codigo_contpaqi
    else:
        _s_codigo_contpaqi = None
    
    if not _s_codigo_contpaqi:
        O_form.errors.codigo_contpaqi = "El código CONTPAQi no puede estar vacío."
    else:
        # Validación: Si el código CONTPAQi no puede duplicarse
        _dbRows = dbc01(
            (_dbTable.empresa_id == _s_empresa_id)
            &(_dbTable.codigo_contpaqi == _s_codigo_contpaqi)
            &(_dbTable.id != O_form.record_id)  # Se ignora el registro que se esté modificando
            ).select(_dbTable.id)
        if _dbRows:
            O_form.errors.codigo_contpaqi = "El código CONTPAQi no puede duplicarse."
        else:
            pass
        
    if not _s_descripcion:
        O_form.errors.descripcion = "La descripción no puede ir vacía."
    else:
        pass
    
    return O_form


def empresa_tipodocumentobancario_ejercicios():
    """ Formulario para el manejo del folio por ejercicios para tipo de documentos bancarios utilizados en CONTPAQi.
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_tipodocumentobancario_id
        [arg4]: operación a realizar en el formulario
        [arg5]: id en caso de existir relación a un registro en la operación
    """ 
    
    _dbTable = dbc01.tempresa_tipodocumentobancario_ejercicios
        
    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.ejercicio,
        _dbTable.folio,
        ]
    
    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresa_tipodocumentosbancarios, 
        xFieldLinkMaster = [
            _dbTable.empresa_tipodocumentobancario_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow, 
        L_fieldsToSearch = [
            _dbTable.id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
        x_offsetInArgs = request.args[:2]
        )
    
    _O_cat.addRowContentEvent('onValidation', _empresa_tipodocumentobancario_ejercicios_validation)
    
    _D_returnVars = _O_cat.process()
    
    return (_D_returnVars)

def _empresa_tipodocumentobancario_ejercicios_validation(O_form):
    """ Validación del código moneda contpaq

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        arg2: master_[nombre función maestro]
        arg3: empresa_tipodocumentobancario_id
        [arg4]: operación a realizar en el formulario
        [arg5]: id en caso de existir relación a un registro en la operación
    
    """
    
    _dbTable = dbc01.tempresa_tipodocumentobancario_ejercicios
    
    _s_empresa_id = request.args(1, None)
    
    _s_empresa_tipodocumentobancario_id = request.args(3, None)
        
    if not O_form.vars.ejercicio:
        O_form.errors.ejercicio = "El ejercicio no puede ir vacío."
    else:
        pass
        
    # Validación: El ejercicio no puede duplicarse
    _dbRows = dbc01(
        (_dbTable.empresa_tipodocumentobancario_id == _s_empresa_tipodocumentobancario_id)
        &(_dbTable.ejercicio == O_form.vars.ejercicio)
        &(_dbTable.id != O_form.record_id)  # Se ignora el registro que se esté modificando
        ).select(_dbTable.id)
        
    if _dbRows:
        O_form.errors.ejercicio = "El ejercicio no puede duplicarse por tipo de documento bancario."
    else:
        pass
    
    _s_msgError = VALIDACIONES.EJERCICIO.ES_VALIDO(_s_empresa_id, O_form.vars.ejercicio)
    
    if _s_msgError:
        O_form.errors.id = _s_msgError
    else:
        pass
    
    return O_form

def empresa_contpaqiejercicios():
    """ Formulario para los ejercicios de CONTPAQi

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    """
    
    _dbTable = dbc01.tempresa_contpaqiejercicios

    _s_empresa_id = request.args(1, None)

    _dbTable.cuenta_servidorcontpaqi_id.requires = IS_IN_DB(
        db(
            (db.tcuenta_empresas.empresa_id == _s_empresa_id)
            &(db.tcuenta_servidorescontpaqi.cuenta_id == db.tcuenta_empresas.cuenta_id)
            ),
        'tcuenta_servidorescontpaqi.id', 
        db.tcuenta_servidorescontpaqi._format
        )
    
    _L_OrderBy = [
        ~_dbTable.ejercicio,
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas, 
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.ejercicio, 
            _dbTable.cuenta_servidorcontpaqi_id,
            _dbTable.empresacontpaqi, 
            ], 
        L_fieldsToSearch = [
            _dbTable.ejercicio, 
            _dbTable.empresacontpaqi, 
            _dbTable.cuenta_servidorcontpaqi_id
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            _dbTable.ejercicio
            ],
        )
        
    _O_cat.addRowContentEvent('onValidation', _empresa_contpaqiejercicios_validation)
    
    _O_cat.addRowSearchResultsProperty('orderBy', _L_OrderBy)     
    
    _D_returnVars = _O_cat.process()

    return (_D_returnVars)


def _empresa_contpaqiejercicios_validation(O_form):
    """Validación del formulario empresa_contpaqiejercicios
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    """
    _dbTable = dbc01.tempresa_contpaqiejercicios
    
    _s_empresa_id = request.args(1, None)
        
    #TODO: Fix temporal
    
#     # Validación: El ejercicio no puede duplicarse
#     _dbRows = dbc01(
#         (_dbTable.empresa_id == _s_empresa_id)
#         &(_dbTable.ejercicio == O_form.vars.ejercicio)
#         &(_dbTable.id != O_form.record_id)  # Se ignora el registro que se esté modificando
#         ).select(_dbTable.id)
#     if _dbRows:
#         O_form.errors.ejercicio = "El ejercicio no puede duplicarse"
#     else:
#         pass
    
#     # Validación: El nombre de empresacontpaqi no puede duplicarse
#     _dbRows = dbc01(
#         (_dbTable.empresa_id == _s_empresa_id)
#         &(_dbTable.empresacontpaqi == O_form.vars.empresacontpaqi)
#         &(_dbTable.id != O_form.record_id)  # Se ignora el registro que se esté modificando
#         ).select(_dbTable.id)
#     if _dbRows:
#         O_form.errors.empresacontpaqi = "El nombre de la empresa no puede duplicarse a lo largo de lo ejercicios."
#     else:
#         pass

#     # Validación: El servidor, base de datos y ejercicio no puede duplicarse.
    _dbRows = dbc01(
        (_dbTable.empresa_id == _s_empresa_id)
        &(_dbTable.cuenta_servidorcontpaqi_id == O_form.vars.cuenta_servidorcontpaqi_id)
        &(_dbTable.ejercicio == O_form.vars.ejercicio)
        &(_dbTable.id != O_form.record_id)  # Se ignora el registro que se esté modificando
        ).select(_dbTable.id)
    if _dbRows:
        O_form.errors.cuenta_servidorcontpaqi_id = "El servidor no puede repetirse por ejercicio."
    else:
        pass

#     # Validación: La base de datos no puede duplicarse por empresa.
    _dbRows = dbc01(
        (_dbTable.empresacontpaqi == O_form.vars.empresacontpaqi)
        &(_dbTable.cuenta_servidorcontpaqi_id == O_form.vars.cuenta_servidorcontpaqi_id)
        &(_dbTable.empresa_id != _s_empresa_id)
        &(_dbTable.id != O_form.record_id)  # Se ignora el registro que se esté modificando
        ).select(_dbTable.id)
    if _dbRows:
        O_form.errors.empresacontpaqi = "La combinación de base de datos y servidor que intenta registrar ya fue utilizada en otra empresa."
        O_form.errors.cuenta_servidorcontpaqi_id = "La combinación de base de datos y servidor que registrar ingresar ya fue utilizada en otra empresa."
    else:
        pass
    
    return O_form


def empresa_bancoscontpaqi():
    """ Formulario para los bancos de CONTPAQi

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    
    """
    
    _dbTable = dbc01.tempresa_bancoscontpaqi

    _s_empresa_id = request.args(1, None)

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas, 
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ], 
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.codigo_contpaqi, 
            _dbTable.nombre,
            _dbTable.banco_id, 
            _dbTable.rfc
            ], 
        L_fieldsToSearch = [
            _dbTable.id, 
            _dbTable.codigo_contpaqi, 
            _dbTable.nombre,
            _dbTable.banco_id, 
            _dbTable.rfc
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            _dbTable.codigo_contpaqi
            ],
        )
        
    _O_cat.addRowContentEvent('onValidation', _empresa_bancoscontpaqi_validation)
        
    _D_returnVars = _O_cat.process()

    return (_D_returnVars)


def _empresa_bancoscontpaqi_validation(O_form):
    """Validación del formulario _empresa_bancoscontpaqi
    
    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo

    """
    _dbTable = dbc01.tempresa_bancoscontpaqi
    
    _s_empresa_id = request.args(1, None)
        
    # Validación: El código CONTPAQi no puede duplicarse
    _dbRows = dbc01(
        (_dbTable.empresa_id == _s_empresa_id)
        &(_dbTable.codigo_contpaqi == O_form.vars.codigo_contpaqi)
        &(_dbTable.id != O_form.record_id)  # Se ignora el registro que se esté modificando
        ).select(_dbTable.id)
    if _dbRows:
        O_form.errors.codigo_contpaqi = "El código de CONTPAQi no puede repetirse"
    else:
        pass
    
    # Validación: El nombre no puede duplicarse
    _dbRows = dbc01(
        (_dbTable.empresa_id == _s_empresa_id)
        &(_dbTable.nombre == O_form.vars.nombre)
        &(_dbTable.id != O_form.record_id)  # Se ignora el registro que se esté modificando
        ).select(_dbTable.id)
    if _dbRows:
        O_form.errors.nombre = "El nombre del banco no puede repetirse"
    else:
        pass

    # Validación: El rfc no puede duplicarse
    if not(O_form.vars.rfc):
        pass
    else:
        _dbRows = dbc01(
            (_dbTable.empresa_id == _s_empresa_id)
            &(_dbTable.rfc == O_form.vars.rfc)
            &(_dbTable.id != O_form.record_id)  # Se ignora el registro que se esté modificando
            ).select(_dbTable.id)
        if _dbRows:
            O_form.errors.rfc = "El RFC no puede repetirse"
        else:
            pass
    
    return O_form


def empresa_fechaspolizas():
    """ Formulario para las fechas de las pólizas

    @descripcion Fechas Polizas

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.    
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.
    
    @permiso btn_none
    @permiso btn_new
    @permiso btn_save
    @permiso btn_cancel
    @permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    @permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    @permiso btn_signature

    """
    
    _dbTable = dbc01.tempresa_fechaspolizas

    _s_empresa_id = request.args(1, None)

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01,
        dbTable = _dbTable,
        dbTableMaster = dbc01.tempresas, 
        xFieldLinkMaster = [
            _dbTable.empresa_id
            ],
        L_visibleButtons = request.stv_fwk_permissions.L_formButtonsAll, 
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = [
            _dbTable.id, 
            _dbTable.ejercicio, 
            _dbTable.cfdi_emitido_ingreso,
            _dbTable.cfdi_emitido_egreso, 
            _dbTable.cfdi_emitido_pago
            ],
        L_fieldsToSearch = [
            _dbTable.id, 
            ], 
        L_fieldsToSearchIfDigit = [
            _dbTable.id, 
            ],
        )
    
    _O_cat.addRowContentEvent('onValidation', _empresa_fechaspolizas_validation)

    _D_returnVars = _O_cat.process()

    return (_D_returnVars)


def _empresa_fechaspolizas_validation(O_form):
    """ Validación de las fechas de pólizas

    Args:
        arg0: master_[nombre función maestro]
        arg1: empresa_id
        [arg2]: codigo de funcionalidad
        [arg3]: id a utilizar para funcionalidad, si esta vacío no esta definido es un registro nuevo
    
    """
    
    _dbTable = dbc01.tempresa_fechaspolizas
    
    _s_empresa_id = request.args(1, None)
                
    if not O_form.vars.ejercicio:
        O_form.errors.ejercicio = "El ejercicio no puede ir vacío."
    else:
        pass
        
    # Validación: El ejercicio no puede duplicarse
    _dbRows = dbc01(
        (_dbTable.empresa_id == _s_empresa_id)
        &(_dbTable.ejercicio == O_form.vars.ejercicio)
        &(_dbTable.id != O_form.record_id)  # Se ignora el registro que se esté modificando
        ).select(_dbTable.id)
    if _dbRows:
        O_form.errors.ejercicio = "El ejercicio no puede duplicarse."
    else:
        pass
    
    _s_msgError = VALIDACIONES.EJERCICIO.ES_VALIDO(_s_empresa_id, O_form.vars.ejercicio)
    
    if _s_msgError:
        O_form.errors.id = _s_msgError
    else:
        pass
    
    return O_form


def poliza_erroresexportacioncontpaqi():
    """ En este formulario se mostrará el log de los errores del día actual de la exportación automatica del CONTPAQi.

    @descripcion Errores exportación automatica CONTPAQi.

    @keyword arg0: Empresa
    @keyword arg1: empresa_id.
    @keyword [arg2]: operación a realizar en el formulario.
    @keyword [arg3]: id en caso de existir relación a un registro en la operación.

    @permiso btn_none
    #@permiso btn_new
    #@permiso btn_save
    @permiso btn_cancel
    #@permiso btn_edit
    @permiso btn_view
    #@permiso btn_suspend
    #@permiso btn_activate
    #@permiso btn_remove
    #@permiso btn_find
    @permiso btn_findall
    @permiso btn_signature

    """

    _dbTable = dbc01.tempresa_erroresexportacioncontpaqi

    _s_empresa_id = request.args(1, None)

    _dbRows_errores = dbc01(
        (_dbTable.dia == datetime.date.today())
        & (_dbTable.empresa_id == _s_empresa_id)
        ).select(
        _dbTable.id
        )
    if _dbRows_errores:
        _dbRow_error = _dbRows_errores.last()
    else:
        _dbRow_error = None

    # Query para filtrar nada más los errores de exportación del día actual.
    _qry = (
            (_dbTable.dia == datetime.date.today())
            & (_dbTable.empresa_id == _s_empresa_id)
            & (_dbTable.id == _dbRow_error)
    )

    _L_fieldsToShow = [
        _dbTable.id,
        _dbTable.ejercicio,
        _dbTable.fecha,
        _dbTable.empresa_contpaqiejercicio_id,
        _dbTable.descripcion
        ]

    _O_cat = STV_FWK_FORM(
        dbReference = dbc01,
        dbReferenceSearch = dbc01(_qry),
        dbTable = _dbTable,
        L_visibleButtons = [
            request.stv_fwk_permissions.btn_none,
            request.stv_fwk_permissions.btn_cancel,
            request.stv_fwk_permissions.btn_view,
            request.stv_fwk_permissions.btn_findall,
            ],
        s_tabId = D_stvFwkCfg.D_uniqueHtmlIds.s_idTab,
        L_fieldsToShow = _L_fieldsToShow,
        L_fieldsToSearch = [
            _dbTable.id,
            ],
        L_fieldsToSearchIfDigit = [
            _dbTable.id
            ],
        L_fieldnameIds = [
            _dbTable.id
            ],
        x_offsetInArgs = request.args[:2]
        )

    _D_returnVars = _O_cat.process()

    return _D_returnVars