using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Windows.Forms;
using HyperSoft.ElectronicDocumentLibrary.Document;
using HyperSoft.ElectronicDocumentLibrary.Base;
using HyperSoft.ElectronicDocumentLibrary.Complemento.CartaPorte;
using Data = StvSoft.DATA;
using HyperSoft.ElectronicDocumentLibrary.Manage;
using HyperSoft.ElectronicDocumentLibrary.Certificate;

namespace StvSoft
{
    public class COMPLEMENTO
    {
        private ElectronicDocument electronicDocument;
        private string _directorio_traslado = string.Empty;
        private string _ruta_archivos = string.Empty;
        private bool b_prueba = true;
        private string uuid = string.Empty;
        private DateTime fecha_creacion_xml = DateTime.Now;
        string _s_directorio_fecha = string.Empty;

        public COMPLEMENTO(string _ruta_traslados, string _ruta_archivos)
        {
            this._ruta_archivos = _ruta_archivos;
            this._directorio_traslado = _ruta_traslados;
        }
        public bool CartaPorte10(out string fileName, ref string s_json)
        {
            CreateObjects();

            STVCFDI _O_cfdi;
            
            StreamReader Jsonstream = File.OpenText(s_json);
               
            var json = Jsonstream.ReadToEnd();
            json = json.Replace("\"Base\"", "\"Basei\"");
            _O_cfdi = JsonConvert.DeserializeObject<STVCFDI>(json);
                       
            //En este método se cargan los datos de la factura.
            Data.CargarDatosCfdi33_Traslado(electronicDocument, _O_cfdi, this.b_prueba);

            // Se agrega el complemento CARTA PORTE.
            electronicDocument.Data.Complementos.Add(ComplementoType.CartaPorte);
            HyperSoft.ElectronicDocumentLibrary.Complemento.CartaPorte.Data data = (HyperSoft.ElectronicDocumentLibrary.Complemento.CartaPorte.Data)electronicDocument.Data.Complementos.Last();
            
            FillUbicacion(data, _O_cfdi);
            FillMercancias(data.Mercancias, _O_cfdi);
            FillFiguraTransporte(data.FiguraTransporte, _O_cfdi);

            _s_directorio_fecha = STV_LIB.Crear_CarpetasPorFecha(this._directorio_traslado, fecha_creacion_xml);
            //string _s_fechaEnNombre = fecha_creacion_xml.ToString("yyMMddHHmmss");
            //uuid = _s_fechaEnNombre + "_" + uuid + ".xml";
            

            return Save(uuid + ".xml", out fileName);
        }

        private bool Save(string fileName, out string fullFileName)
        {
            string errorMessage;
            fullFileName = Path.Combine(this._s_directorio_fecha, fileName);

            if (System.IO.File.Exists(fullFileName))
                HyperSoft.Shared.File.Instance.DeleteFile(fullFileName, out errorMessage);

            using (MemoryStream stream = new MemoryStream())
            {
                // Se ejecuta el proceso de generación
                if (electronicDocument.SaveToStream(stream) == false)
                {
                    errorMessage = string.Format("Se generó un error al crear el XML.{0}{0}ERROR{0}{1}", Environment.NewLine, electronicDocument.ErrorText);
                    MessageBox.Show(errorMessage, "Complementos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                try
                {
                    System.IO.File.WriteAllBytes(fullFileName, stream.ToArray());
                    return true;
                }
                catch (Exception e)
                {
                    errorMessage = string.Format("Se generó un error al guardar el archivo XML.{0}{0}ARCHIVO{0}{1}{0}{0}ERROR{0}{2}", Environment.NewLine, fullFileName, e.Message);
                    MessageBox.Show(errorMessage, "Complementos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
        }

        private void CreateObjects()
        {
            // Instanciamos la clase TManageElectronicDocument
            ElectronicManage manage = new ElectronicManage().Initialization();

            //  . EDL, a partir de la versión 2017.06.02, por defecto realiza una validación LITE del CFDI 3.3 contra el schema
            //  . Para que se realice una validación FULL, debes activar las siguientes líneas
            //  . Si deseas conocer más al respecto:
            //       www.facturando.mx/blog/index.php/2017/06/01/edl-validacion-de-un-cfdi-3-3/
            //manage.Save.Options |= SaveOptions.ValidateWithSchema;
            //manage.Save.Options -= SaveOptions.ValidateWithSchemaLite;

            // Se cargan a memoria el archivo de autoridades certificadoras de prueba
            // *** ESTE LÍNEA DE CODIGO DEBE SER ELIMINADA EN UN AMBIENTE DE PRODUCCIÓN ***
            manage.CertificateAuthorityList.UseForTest();

            // Se crea la clase que va a ser usada en el proceso de firmado, se le pasa el certificado,
            // la llave privada y el password de la misma.
            ElectronicCertificate certificate = new ElectronicCertificate();
            certificate.Load(
              Path.Combine(_ruta_archivos, "Archivos\\FUNK671228PH6.cer"),
              Path.Combine(_ruta_archivos, "Archivos\\FUNK671228PH6.key"),
              "12345678a");

            // Asignamos el certificado al objeto Manage, esta ultima, es la encargada de contener
            // y administrar TODOS los recursos que serán usados en el proceso
            manage.Save.AssignCertificate(certificate);

            // Se instancia la clase que es la encarga de llevar a cabo el proceso de generación y se le pasa
            // el objeto que tiene todos los recursos necesarios para llevar a cabo dicho proceso.
            this.electronicDocument = new ElectronicDocument().Initialization();
            this.electronicDocument.AssignManage(manage);

            // Se crea el directorio donde van a ser almacenado los CFDI generados.
            this._directorio_traslado = Path.Combine(this._directorio_traslado, "CFDIs_traslado");
            if (Directory.Exists(this._directorio_traslado) == false)
            {
                try
                {
                    Directory.CreateDirectory(this._directorio_traslado);
                }
                catch
                {
                    HyperSoft.Shared.Gui.ShowError(string.Format("No se pudo crear el directorio donde se van a almacenar los CFDI's.{0}{0}{1}", Environment.NewLine, this._directorio_traslado));
                    Application.Exit();
                }
            }

            HyperSoft.Shared.Gui.MessageBoxCaption = "Complementos";

        }
        
        private void FillUbicacion(HyperSoft.ElectronicDocumentLibrary.Complemento.CartaPorte.Data data, STVCFDI _O_cfdi)
        {
            //Se registran las distintas ubicaciones que sirven para reflejar el domicilio del origen y/o destino
            // que tienen los bienes o mercancías que se trasladan por distintos medios de transporte.
            Ubicacion ubicacion = data.Ubicaciones.Add();

            foreach (STVCFDI.CARTAPORTE _o_cartaporte in _O_cfdi.complemento.cartaporte)
            {

                data.Version.Value = _o_cartaporte.versioncp;
                data.TransporteInternacional.Value = _o_cartaporte.transpinternac;
                data.EntradaSalidaMercancia.Value = _o_cartaporte.entradasalidamerc;
                data.ViaEntradaSalida.Value = _o_cartaporte.viaentradasalida;
                data.TotalDistanciaRecorrida.Value = _o_cartaporte.totaldistrec;

                foreach (STVCFDI.UBICACIONES _O_ubicacion in _o_cartaporte.L_ubicaciones)
                {
                    ubicacion.TipoEstacion.Value = _O_ubicacion.tipoestacion;
                    ubicacion.DistanciaRecorrida.Value = new decimal(_O_ubicacion.distanciarecorrida);

                    // Registro de la información detallada del Origen de los bienes o mercancías que se trasladan.
                    ubicacion.Origen.FechaHoraSalida.Value = _O_ubicacion.origen_fechahorasalida_str;
                    ubicacion.Origen.NombreEstacion.Value = _O_ubicacion.origen_nombreestacion;
                    ubicacion.Origen.ResidenciaFiscal.Value = _O_ubicacion.origen_recidenciafiscal;
                    ubicacion.Origen.Id.Value = _O_ubicacion.origen_idorigen;
                    ubicacion.Origen.RfcRemitente.Value = _O_ubicacion.origen_rfcremitente;
                    ubicacion.Origen.NumeroEstacion.Value = _O_ubicacion.origen_numestacion;
                    ubicacion.Origen.NavegacionTrafico.Value = _O_ubicacion.origen_navegaciontrafico;
                    ubicacion.Origen.IdTtributario.Value = _O_ubicacion.origen_numregidtrib;
                    ubicacion.Origen.NombreRemitente.Value = _O_ubicacion.origen_nombreremitente;

                    // Registro de la información detallada del Destino de los bienes o mercancías que se trasladan.
                    ubicacion.Destino.ResidenciaFiscal.Value = _O_ubicacion.destino_recidenciafiscal;
                    ubicacion.Destino.FechaHoraLlegada.Value = _O_ubicacion.destino_fechahoraprogllegada_str;
                    ubicacion.Destino.RfcDestinatario.Value = _O_ubicacion.destino_rfcdestinatario;
                    ubicacion.Destino.Id.Value = _O_ubicacion.destino_iddestino;
                    ubicacion.Destino.NavegacionTrafico.Value = _O_ubicacion.destino_navegaciontrafico;
                    ubicacion.Destino.NombreEstacion.Value = _O_ubicacion.destino_nombreestacion;
                    ubicacion.Destino.NumeroEstacion.Value = _O_ubicacion.destino_numestacion;
                    ubicacion.Destino.NombreDestinatario.Value = _O_ubicacion.destino_nombredestinatario;
                    ubicacion.Destino.IdTtributario.Value = _O_ubicacion.destino_numregidtrib;

                    // Registro de la información del domicilio de origen y/o destino de los bienes o mercancías
                    ubicacion.Domicilio.Estado.Value = _O_ubicacion.dom_estado;
                    ubicacion.Domicilio.Municipio.Value = _O_ubicacion.dom_municipio;
                    ubicacion.Domicilio.Pais.Value = _O_ubicacion.dom_pais;
                    ubicacion.Domicilio.Referencia.Value = _O_ubicacion.dom_referencia;
                    ubicacion.Domicilio.Colonia.Value = _O_ubicacion.dom_colonia;
                    ubicacion.Domicilio.Calle.Value = _O_ubicacion.dom_calle;
                    ubicacion.Domicilio.CodigoPostal.Value = _O_ubicacion.dom_codigopostal;
                    ubicacion.Domicilio.Localidad.Value = _O_ubicacion.dom_localidad;
                    ubicacion.Domicilio.NumeroInterior.Value = _O_ubicacion.dom_numinterior;
                    ubicacion.Domicilio.NumeroExterior.Value = _O_ubicacion.dom_numexterior;
                } 
            }
        }
        
        private void FillMercancias(Mercancias data, STVCFDI _O_cfdi)
        {
            // Registro de la información de los bienes o mercancías que se trasladan en los distintos
            // medios de transporte.
           
            foreach(STVCFDI.CARTAPORTE _o_cartaporte in _O_cfdi.complemento.cartaporte)
            {
                data.PesoBrutoTotal.Value = new decimal(_o_cartaporte.merc_pesobrutototal);
                data.UnidadPeso.Value = _o_cartaporte.merc_unidadpeso;
                data.PesoNetoTotal.Value = new decimal(_o_cartaporte.merc_pesonetototal);
                data.NumeroTotalMercancias.Value = _o_cartaporte.merc_numtotalmercancias;
                data.CargoPorTasacion.Value = new decimal(_o_cartaporte.merc_numtotalmercancias);
            }
        
            FillMercancia(data.Mercancia, _O_cfdi);
            FillAutoTransporteFederal(data.AutoTransporteFederal, _O_cfdi);
            //FillTransporteMaritimo(data.TransporteMaritimo);
            //FillTransporteAereo(data.TransporteAereo);
            //FillTransporteFerroviario(data.TransporteFerroviario);
        }

        private void FillMercancia(MercanciaList data, STVCFDI _O_cfdi)
        {
            // Registro de la información de los bienes o mercancías que se trasladan en los distintos
            // medios de transporte.
            Mercancia mercancia = data.Add();
           
            foreach(STVCFDI.CARTAPORTE _o_cartaporte in _O_cfdi.complemento.cartaporte)
            {
                foreach(STVCFDI.MERCANCIAS _o_mercancias in _o_cartaporte.L_mercancias)
                {
                    mercancia.BienesTransportado.Value = "";
                    mercancia.ClaveStcc.Value = _o_mercancias.clavestcc;
                    mercancia.Descripcion.Value = _o_mercancias.descripcion;
                    mercancia.Cantidad.Value = _o_mercancias.cantidad;
                    mercancia.ClaveUnidad.Value = _o_mercancias.claveunidad;
                    mercancia.Unidad.Value = _o_mercancias.unidad;
                    mercancia.Dimensiones.Value = _o_mercancias.dimensiones;
                    mercancia.MaterialPeligroso.Value = _o_mercancias.materialpeligroso;
                    mercancia.ClaveMaterialPeligroso.Value = _o_mercancias.cvematerialpeligroso;
                    mercancia.Embalaje.Value = _o_mercancias.embalaje;
                    mercancia.DescripcionEmbalaje.Value = _o_mercancias.descripembalaje;
                    mercancia.PesoEnKilogramos.Value = _o_mercancias.pesoenkg;
                    mercancia.ValorMercancia.Value = _o_mercancias.valormercancia;
                    mercancia.Moneda.Value = _o_mercancias.moneda;
                    mercancia.FraccionArancelaria.Value = _o_mercancias.fraccionarancelaria;
                    mercancia.UuidComercioExterior.Value = _o_mercancias.uuidcomercioext;

                    uuid = _o_mercancias.uuidcomercioext;

                    mercancia.DetalleMercancia.PesoNeto.Value = new decimal(_o_mercancias.detallemerc_pesoneto);
                    mercancia.DetalleMercancia.PesoTara.Value = new decimal(_o_mercancias.detallemerc_pesotara);
                    mercancia.DetalleMercancia.PesoBruto.Value = new decimal(_o_mercancias.detallemerc_pesobruto);
                    mercancia.DetalleMercancia.UnidadPeso.Value = _o_mercancias.detallemerc_unidadpeso;
                    mercancia.DetalleMercancia.NumeroPiezas.Value = _o_mercancias.detallemerc_numpiezas;

                    //CantidadTransporta cantidadTransporta = mercancia.CantidadTransporta.Add();
                    //cantidadTransporta.Cantidad.Value = new decimal(1.123456);
                    //cantidadTransporta.IdOrigen.Value = "OR000000";
                    //cantidadTransporta.IdDestino.Value = "DE000000";
                    //cantidadTransporta.ClaveTransporte.Value = "03";
                }
            }
        }

        private void FillAutoTransporteFederal(AutotransporteFederal data, STVCFDI _O_cfdi)
        {
            // Registro de la información que permita la identificación del autotransporte de carga federal,
            // por medio del cual se transportan los bienes o mercancías, que transitan a través de las carreteras federales del territorio nacional.
            Remolque remolque = data.Remolques.Add();

            foreach (STVCFDI.CARTAPORTE _o_cartaporte in _O_cfdi.complemento.cartaporte)
            {
                foreach (STVCFDI.MERCANCIAS _o_merca in _o_cartaporte.L_mercancias)
                {
                    remolque.SubTipo.Value = _o_merca.remolque1_subtiporem;
                    remolque.Placa.Value = _o_merca.remolque1_placa;
                    data.NumeroPolizaSeguro.Value = _o_merca.numpolizaseguro;
                    data.IdentificacionVehicular.ConfigVehicular.Value = _o_merca.idvehic_configvehicular;
                    data.NumeroPermisoSct.Value = _o_merca.numpermisosct;
                    data.IdentificacionVehicular.Placa.Value = _o_merca.idvehic_placavm;
                    data.NombreAseguradora.Value = _o_merca.nombreaseg;
                    data.IdentificacionVehicular.AnioModelo.Value = _o_merca.idvehic_aniomodelovm;
                    data.PermisoSct.Value = _o_merca.permsct;

                }
            }

            //data.PermisoSct.Value = "TPAF01";
            //data.NumeroPermisoSct.Value = "SCT1699";
            //data.NombreAseguradora.Value = "AXA";
            //data.NumeroPolizaSeguro.Value = "AXA2137";

            //data.IdentificacionVehicular.ConfigVehicular.Value = "C2";
            //data.IdentificacionVehicular.Placa.Value = "SCT2540";
            //data.IdentificacionVehicular.AnioModelo.Value = 2015;

            //remolque.SubTipo.Value = "CTR001";
            //remolque.Placa.Value = "SCT2540";

            
        }

        private void FillTransporteMaritimo(TransporteMaritimo data)
        {
            // Registro de la información que permita la identificación de la embarcación
            // por medio del cual se transportan los bienes o mercancías, vía marítima.
            data.PermisoSct.Value = "TPAF01";
            data.NumeroPermisoSct.Value = "SCT1699";
            data.NombreAseguradora.Value = "AXA";
            data.NumeroPolizaSeguro.Value = "AXA2137";
            data.TipoEmbarcacion.Value = "B04";
            data.Matricula.Value = "SCT1699";
            data.NumeroOmi.Value = "IMO1234567";
            data.AnioEmbarcacion.Value = 2019;
            data.NombreEmbarcacion.Value = "El siete mares";
            data.NacionalidadEmbarcacion.Value = "MEX";
            data.UnidadesArqueoBruto.Value = new decimal(1.123);
            data.TipoCarga.Value = "CGS";
            data.NumeroCertificadoItc.Value = "2001";
            data.Eslora.Value = new decimal(15.60);
            data.Manga.Value = new decimal(1.50);
            data.Calado.Value = new decimal(1.1);
            data.LineaNaviera.Value = "APL de México";
            data.NombreAgenteNaviero.Value = "Luis Pérez Hernández";
            data.NumeroAutorizacionNaviero.Value = "SCT418/020/2016";
            data.NumeroViaje.Value = "#2021-1";
            data.NumeroConocimientoEmbarcacion.Value = "Medio";

            Contenedor contenedor = data.Contenedores.Add();
            contenedor.MatriculaContenedor.Value = "12345678900";
            contenedor.TipoContenedor.Value = "CM001";
            contenedor.NumeroPrecinto.Value = "123400";
        }

        private void FillTransporteAereo(TransporteAereo data)
        {
            // Registro de la información que permita la identificación del transporte aéreo
            // por medio del cual se trasladan los bienes o mercancías.
            data.PermisoSct.Value = "TPAF01";
            data.NumeroPermisoSct.Value = "SCT1699";
            data.MatriculaAeronave.Value = "A4-12345";
            data.NombreAseguradora.Value = "AXA";
            data.NumeroPolizaSeguro.Value = "AXA2137";
            data.NumeroGuia.Value = "123456789012";
            data.LugarContrato.Value = "Ciudad de México";
            data.RfcTransportista.Value = "AAA010101AAA";
            data.CodigoTransportista.Value = "CA001";
            data.IdTributarioTransportista.Value = "123456";
            data.ResidenciaFiscalTransportista.Value = "MEX";
            data.NombreTransportista.Value = "Transportes Castor";
            data.RfcEmbarcador.Value = "AAA010101AAA";
            data.IdTributarioEmbarcador.Value = "123456";
            data.ResidenciaFiscalEmbarcador.Value = "MEX";
            data.NombreEmbarcador.Value = "Embarcador";
        }

        private void FillTransporteFerroviario(TransporteFerroviario data)
        {
            // Registro de la información que permita la identificación del carro o contenedor
            // en el que se transportan los bienes o mercancías vía férrea.
            data.TipoServicio.Value = "TS01";
            data.NombreAseguradora.Value = "AXA";
            data.NumeroPolizaSeguro.Value = "AXA2137";
            data.Concesionario.Value = "AAA010101AAA";

            DerechoPaso derechoPaso = data.DerechosPaso.Add();
            derechoPaso.TipoDerecho.Value = "CDP001";
            derechoPaso.KilometrajePagado.Value = new decimal(125.50);

            Carro carro = data.Carros.Add();
            carro.Tipo.Value = "TC01";
            carro.Matricula.Value = "123456";
            carro.Guia.Value = "12345";
            carro.ToneladasNetas.Value = new decimal(8.750);

            ContenedorCarro contenedor = carro.Contenedores.Add();
            contenedor.TipoContenedor.Value = "TC01";
            contenedor.PesoContenedorVacio.Value = new decimal(.500);
            contenedor.PesoNetoMercancia.Value = new decimal(1.500);
        }

        private void FillFiguraTransporte(FiguraTransporte data, STVCFDI _O_cfdi)
        {
            // Para indicar los datos de la figura del transporte que interviene en el traslado de los bienes o mercancías,
            // cuando el dueño del medio de transporte es diferente del emisor del comprobante con el complemento carta porte.
            
            Operadores operadores = data.Operadores.Add();
            Operador operador = operadores.Operador.Add();

            foreach (STVCFDI.CARTAPORTE _o_cartaporte in _O_cfdi.complemento.cartaporte)
            {
                data.ClaveTransporte.Value = _o_cartaporte.figtransporte_cvetransporte;

                foreach (STVCFDI.MERCANCIAS _o_figuratransporte in _o_cartaporte.L_mercancias)
                {
                    operador.Domicilio.Estado.Value = _o_figuratransporte.dom_estado;
                    operador.Domicilio.Municipio.Value = _o_figuratransporte.dom_municipio;
                    operador.NumeroLicencia.Value = _o_figuratransporte.numlicencia;
                    operador.IdTtributario.Value = _o_figuratransporte.numregidtriboperador;
                    //tipofiguratransporte
                    operador.Domicilio.Pais.Value = _o_figuratransporte.dom_pais;
                    operador.Domicilio.Colonia.Value = _o_figuratransporte.dom_colonia;
                    operador.Domicilio.Calle.Value = _o_figuratransporte.dom_calle;
                    operador.Domicilio.CodigoPostal.Value = _o_figuratransporte.dom_codigopostal;
                    operador.Domicilio.Localidad.Value = _o_figuratransporte.dom_localidad;
                    operador.Domicilio.NumeroInterior.Value = _o_figuratransporte.dom_numinterior;
                    operador.Rfc.Value = _o_figuratransporte.rfc;
                    operador.ResidenciaFiscal.Value = _o_figuratransporte.residenciafiscal;
                    operador.Domicilio.NumeroExterior.Value = _o_figuratransporte.dom_numexterior;
                    operador.Nombre.Value = _o_figuratransporte.nombre;
                    operador.Domicilio.Referencia.Value = _o_figuratransporte.dom_referencia;

                }
            }

            //operador.Rfc.Value = "AAAA010101AAA";
            //operador.NumeroLicencia.Value = "123456";
            //operador.Nombre.Value = "Luis Pérez Hernández";
            //operador.IdTtributario.Value = "123456";
            //operador.ResidenciaFiscal.Value = "MEX";
            //operador.Domicilio.Calle.Value = "Insurgentes";
            //operador.Domicilio.NumeroExterior.Value = "10";
            //operador.Domicilio.NumeroInterior.Value = "3";
            //operador.Domicilio.Colonia.Value = "Roma";
            //operador.Domicilio.Localidad.Value = "México";
            //operador.Domicilio.Referencia.Value = "Entre Sevilla y Puebla";
            //operador.Domicilio.Municipio.Value = "Cuauhtémoc";
            //operador.Domicilio.Estado.Value = "Ciudad de México";
            //operador.Domicilio.Pais.Value = "MEX";
            //operador.Domicilio.CodigoPostal.Value = "06760";

            //Propietario propietario = data.Propietarios.Add();
            //propietario.Rfc.Value = "AAA010101AAA";
            //propietario.Nombre.Value = "Luis Pérez Hernández";
            //propietario.IdTtributario.Value = "123456";
            //propietario.ResidenciaFiscal.Value = "MEX";
            //propietario.Domicilio.Calle.Value = "Insurgentes";
            //propietario.Domicilio.NumeroExterior.Value = "10";
            //propietario.Domicilio.NumeroInterior.Value = "4";
            //propietario.Domicilio.Colonia.Value = "Roma";
            //propietario.Domicilio.Localidad.Value = "México";
            //propietario.Domicilio.Referencia.Value = "Entre Sevilla y Puebla";
            //propietario.Domicilio.Municipio.Value = "Cuauhtémoc";
            //propietario.Domicilio.Estado.Value = "Ciudad de México";
            //propietario.Domicilio.Pais.Value = "MEX";
            //propietario.Domicilio.CodigoPostal.Value = "06760";

            //Arrendatario arrendatario = data.Arrendatarios.Add();
            //arrendatario.Rfc.Value = "AAA010101AAA";
            //arrendatario.Nombre.Value = "Luis Pérez Hernández";
            //arrendatario.IdTtributario.Value = "123456";
            //arrendatario.ResidenciaFiscal.Value = "MEX";
            //arrendatario.Domicilio.Calle.Value = "Insurgentes";
            //arrendatario.Domicilio.NumeroExterior.Value = "10";
            //arrendatario.Domicilio.NumeroInterior.Value = "3";
            //arrendatario.Domicilio.Colonia.Value = "Roma";
            //arrendatario.Domicilio.Localidad.Value = "México";
            //arrendatario.Domicilio.Referencia.Value = "Entre Sevilla y Puebla";
            //arrendatario.Domicilio.Municipio.Value = "Cuauhtémoc";
            //arrendatario.Domicilio.Estado.Value = "Ciudad de México";
            //arrendatario.Domicilio.Pais.Value = "MEX";
            //arrendatario.Domicilio.CodigoPostal.Value = "06760";

            //Notificado notificado = data.Notificados.Add();
            //notificado.Rfc.Value = "AAA010101AAA";
            //notificado.Nombre.Value = "Luis Pérez Hernández";
            //notificado.IdTtributario.Value = "123456";
            //notificado.ResidenciaFiscal.Value = "MEX";
            //notificado.Domicilio.Calle.Value = "Insurgentes";
            //notificado.Domicilio.NumeroExterior.Value = "10";
            //notificado.Domicilio.NumeroInterior.Value = "1";
            //notificado.Domicilio.Colonia.Value = "Roma";
            //notificado.Domicilio.Localidad.Value = "México";
            //notificado.Domicilio.Referencia.Value = "Entre Sevilla y Puebla";
            //notificado.Domicilio.Municipio.Value = "Cuauhtémoc";
            //notificado.Domicilio.Estado.Value = "Ciudad de México";
            //notificado.Domicilio.Pais.Value = "MEX";
            //notificado.Domicilio.CodigoPostal.Value = "06760";
        }

    }
}
