﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StvSoft
{
    class RESULTADO_DESCARGA_EMITIDOS
    {
        public int Acuses { get; set; }
        public int AcusesDescargados { get; set; }
        public int AcusesNoDescargados { get; set; }
        public int AcusesNoProcesados { get; set; }
        public int Cancelados { get; set; }
        public int Descargados { get; set; }
        public int Documentos { get; set; }
        public int NoEncontrados { get; set; }
        public int NoProcesados { get; set; }
        public int Vigentes { get; set; }
        public int TotalComprobantes { get; set; }
        public List<string> ListComprobantes { get; set; }
        public List<STVCFDI> ListStvCFDI { get; set; }
    }
}
