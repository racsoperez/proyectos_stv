using HyperSoft.ElectronicDocument.Download.Resumen;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace StvSoft
{
    public class STVCFDI
    {
        //SECCION ATRIBUTOS COMPROBANTE CFDI 3.3
        public string version { get; set; } = string.Empty;            //REQUERIDO
        public string serie { get; set; } = string.Empty;               //OPCIONAL
        public string folio { get; set; } = string.Empty;               //OPCIONAL
        public DateTime fecha { get; set; } = DateTime.MinValue;            //REQUERIDO
        //public string sello { get; set; }               //REQUERIDO
        public string formapago { get; set; } = string.Empty;           //CONDICIONAL          
        //public string nocertificado { get; set; }       //REQUERIDO
        //public string certificado { get; set; }         //REQUERIDO
        public string condicionesdepago { get; set; } = string.Empty;   //CONDICIONAL
        public double subtotal { get; set; } = 0.0;          //REQUERIDO
        public double descuento { get; set; } = 0.0;         //CONDICIONAL
        public string moneda { get; set; } = string.Empty;             //REQUERIDO
        public double tipocambio { get; set; } = 0.0;         //CONDICIONAL
        public double total { get; set; } = 0.0;              //REQUERIDO
        public string tipodecomprobante { get; set; } = string.Empty;   //REQUERIDO
        public string metodopago { get; set; } = string.Empty;          //CONDICIONAL
        public string lugarexpedicion { get; set; } = string.Empty;     //REQUERIDO

        public EMISOR emisor { get; set; } = new EMISOR();    //REQUERIDO
        public RECEPTOR receptor { get; set; } = new RECEPTOR();    //REQUERIDO

        public IMPUESTOS impuestos { get; set; } = new IMPUESTOS();

        public List<CONCEPTO> conceptos { get; set; } = new List<CONCEPTO>();

        public COMPLEMENTO complemento { get; set; } = new COMPLEMENTO();


        public class IMPUESTOS
        {
            public double totalimpuestostrasladados { get; set; } = 0.0;   //CONDICIONAL
            public double totalimpuestosretenidos { get; set; } = 0.0;   //CONDICIONAL

            public List<IMPUESTO> trasladados { get; set; } = new List<IMPUESTO>();  //CONDICIONAL
            public List<IMPUESTO> retenidos { get; set; } = new List<IMPUESTO>(); //CONDICIONAL
        }

        public class COMPLEMENTO
        {
            public TIMBREFISCALDIGITAL timbrefiscaldigital { get; set; } = new TIMBREFISCALDIGITAL();
            public PAGOS pagos { get; set; } = new PAGOS();    //REQUERIDO
            public List<CARTAPORTE> cartaporte { get; set; } = new List<CARTAPORTE>();

          
        }

        public class PAGOS
        {
            public string version { get; set; } = string.Empty;          //CONDICIONAL
            public List<PAGO> pago { get; set; } = new List<PAGO>();
            
        }

        public CFDIRELACIONADOS cfdirelacionados { get; set; } = new CFDIRELACIONADOS();

        //SECCION NODO EMISOR CFDI 3.3
        public class CFDIRELACIONADOS
        {
            public string tiporelacion { get; set; } = string.Empty; //REQUERIDO          
            public List<CFDIRELACIONADO> cfdirelacionado { get; set; } = new List<CFDIRELACIONADO>(); //REQUERIDO
        }

        //SECCION NODO EMISOR CFDI 3.3
        public class CFDIRELACIONADO
        {
            public string uuid { get; set; } = string.Empty;           //REQUERIDO          
        }

        //SECCION NODO EMISOR CFDI 3.3
        public class EMISOR
        {
            public string rfc { get; set; } = string.Empty;           //REQUERIDO
            public string nombre { get; set; } = string.Empty; //OPCIONAL
            public string regimenfiscal { get; set; } = string.Empty; //REQUERIDO           
            public string cp { get; set; } = string.Empty; //REQUERIDO           
        }

        //SECCION NODO RECEPTOR CFDI 3.3
        public class RECEPTOR
        {
            public string rfc { get; set; } = string.Empty;           //REQUERIDO
            public string nombre { get; set; } = string.Empty; //OPCIONAL
            public string usocfdi { get; set; } = string.Empty; //REQUERIDO           
        }

        //SECCION CONCEPTOS CFDI 3.3


        public class CONCEPTO
        {
            public double cantidad { get; set; } = 0.0;       //REQUERIDO
            public string claveprodserv { get; set; } = string.Empty;   //REQUERIDO
            public string claveunidad { get; set; } = string.Empty;     //REQUERIDO
            public string descripcion { get; set; } = string.Empty;     //REQUERIDO
            public double descuento { get; set; } = 0.0;      //OPCIONAL
            public double importe { get; set; } = 0.0;         //REQUERIDO
            public string noidentificacion { get; set; } = string.Empty;//OPCIONAL
            public string unidad { get; set; } = string.Empty;          //OPCIONAL
            public double valorunitario { get; set; } = 0.0;   //REQUERIDO            

            public IMPUESTOS impuestos { get; set; } = new IMPUESTOS();  //REQUERIDO   
        }

        public class IMPUESTO
        {
            public double basei { get; set; } = 0.0;           //REQUERIDO
            public string impuesto { get; set; } = string.Empty;        //REQUERIDO
            public string tipofactor { get; set; } = string.Empty;      //REQUERIDO
            public double tasaocuota { get; set; } = 0.0;     //OPCIONAL
            public double importe { get; set; } = 0.0;        //CONDICIONAL
        }



        //SECCION NODO TIMBRE FISCAL DIGITAL CFDI 3.3
        public class TIMBREFISCALDIGITAL
        {
            public string uuid { get; set; } = string.Empty;
            public DateTime fechatimbrado { get; set; } = DateTime.MinValue;
            //public string sellocfd { get; set; }
            //public string sellosat { get; set; }
            //public string nocertificadosat { get; set; }
        }

        public class PAGO
        {
            public DateTime fechapago { get; set; } = DateTime.MinValue;             //REQUERIDO
            public string formadepagop { get; set; } = string.Empty;              //REQUERIDO
            public string monedap { get; set; } = string.Empty;                 //REQUERIDO
            public double tipocambiop { get; set; } = 0.0;             //CONDICIONAL
            public double monto { get; set; } = 0.0;                   //REQUERIDO

            public List<DOCTORELACIONADO> doctorelacionado { get; set; } = new List<DOCTORELACIONADO>();
        }

        public class DOCTORELACIONADO
        {
            public string iddocumento { get; set; } = string.Empty;             //REQUERIDO
            public string serie { get; set; } = string.Empty;                   //OPCIONAL
            public string folio { get; set; } = string.Empty;                   //OPCIONAL
            public string monedadr { get; set; } = string.Empty;                //REQUERIDO
            public double tipocambiodr { get; set; } = 0.0;         //CONDICIONAL
            public string metododepagodr { get; set; } = string.Empty;            //REQUERIDO
            public int numparcialidad { get; set; } = 0;          //CONDICIONAL
            public double impsaldoant { get; set; } = 0.0;          //CONDICIONAL
            public double imppagado { get; set; } = 0.0;            //CONDICIONAL
            public double impsaldoinsoluto { get; set; } = 0.0;     //CONDICIONAL
        }

        public class CARTAPORTE
        {
            public string transpinternac { get; set; } = string.Empty;             
            public string figtransporte_cvetransporte { get; set; } = string.Empty;                   
            public string merc_unidadpeso { get; set; } = string.Empty;                   
            public List<AUTOTRANSPORTE> L_autotransporte { get; set; } = new List<AUTOTRANSPORTE>();
            public decimal totaldistrec { get; set; } = 0.0M;                
            public double merc_pesonetototal { get; set; } = 0.0;         
            public string viaentradasalida { get; set; } = string.Empty;
            public List<MERCANCIAS> L_mercancias { get; set; } = new List<MERCANCIAS>();
            public List<PERSONAS> L_personas { get; set; } = new List<PERSONAS>();
            public string versioncp { get; set; } = string.Empty;
            public double merc_pesobrutototal { get; set; } = 0.00;
            public string entradasalidamerc { get; set; } = string.Empty;
            public List<UBICACIONES> L_ubicaciones { get; set; } = new List<UBICACIONES>();
            public double merc_cargoportasacion { get; set; } = 0.00;
            public int merc_numtotalmercancias { get; set; } = 0;


        }

        public class AUTOTRANSPORTE
        {
          
        }
        public class PERSONAS
        {

        }
        public class UBICACIONES
        {
            public DateTime origen_fechahorasalida_str { get; set; } = DateTime.MinValue;
            public string dom_estado { get; set; } = string.Empty;
            public string destino_recidenciafiscal { get; set; } = string.Empty;
            public DateTime destino_fechahoraprogllegada_str { get; set; } = DateTime.MinValue;
            public string origen_nombreestacion { get; set; } = string.Empty;
            public string dom_municipio { get; set; } = string.Empty;
            public string dom_pais { get; set; } = string.Empty;
            public string origen_recidenciafiscal { get; set; } = string.Empty;
            public string origen_idorigen { get; set; } = string.Empty;
            public string origen_rfcremitente { get; set; } = string.Empty;
            public string dom_referencia { get; set; } = string.Empty;
            public string destino_rfcdestinatario { get; set; } = string.Empty;
            public string dom_colonia { get; set; } = string.Empty;
            public string destino_iddestino { get; set; } = string.Empty;
            public string destino_navegaciontrafico { get; set; } = string.Empty;
            public string dom_calle { get; set; } = string.Empty;
            public string dom_codigopostal { get; set; } = string.Empty;
            public string destino_nombreestacion { get; set; } = string.Empty;
            public string dom_localidad { get; set; } = string.Empty;
            public string dom_numinterior { get; set; } = string.Empty;
            public string origen_numestacion { get; set; } = string.Empty;
            public string origen_navegaciontrafico { get; set; } = string.Empty;
            public string destino_numestacion { get; set; } = string.Empty;
            public string tipoestacion { get; set; } = string.Empty;
            public string dom_numexterior { get; set; } = string.Empty;
            public string destino_nombredestinatario { get; set; } = string.Empty;
            public string destino_numregidtrib { get; set; } = string.Empty;
            public string origen_numregidtrib { get; set; } = string.Empty;
            public string origen_nombreremitente { get; set; } = string.Empty;
            public double distanciarecorrida { get; set; } = 0.00;
        }

        public class MERCANCIAS
        {
            public double detallemerc_pesotara { get; set; } = 0.00;
            public decimal cantidad { get; set; } = 0.0M;
            public string claveunidad { get; set; } = string.Empty;
            public double detallemerc_pesobruto { get; set; } = 0.00;
            public string dimensiones { get; set; } = string.Empty;
            public string materialpeligroso { get; set; } = string.Empty;
            public string clavestcc { get; set; } = string.Empty;
            public string descripcion { get; set; } = string.Empty;
            public string descripembalaje { get; set; } = string.Empty;
            public string claveprodservcp { get; set; } = string.Empty;
            public string unidad { get; set; } = string.Empty;
            public string detallemerc_unidadpeso { get; set; } = string.Empty;
            public decimal pesoenkg { get; set; } = 0.0M;
            public string fraccionarancelaria { get; set; } = string.Empty;
            public string uuidcomercioext { get; set; } = string.Empty;
            public decimal valormercancia { get; set; } = 0.0M;
            public int detallemerc_numpiezas { get; set; } = 0;
            public string embalaje { get; set; } = string.Empty;
            public string cvematerialpeligroso { get; set; } = string.Empty;
            public string moneda { get; set; } = string.Empty;
            public double detallemerc_pesoneto { get; set; } = 0.00;

            //////////////// Remolque
            public string remolque2_subtiporem { get; set; } = string.Empty;
            public string numpolizaseguro { get; set; } = string.Empty;
            public string idvehic_configvehicular { get; set; } = string.Empty;
            public string remolque1_subtiporem { get; set; } = string.Empty;
            public string numpermisosct { get; set; } = string.Empty;
            public string remolque2_placa { get; set; } = string.Empty;
            public string remolque1_placa { get; set; } = string.Empty;
            public int idvehic_aniomodelovm { get; set; } = 0;
            public string idvehic_placavm { get; set; } = string.Empty;
            public string nombreaseg { get; set; } = string.Empty;
            public string permsct { get; set; } = string.Empty;

            ///// FiguraTransporte
            public string dom_estado { get; set; } = string.Empty;
            public string dom_municipio { get; set; } = string.Empty;
            public string numlicencia { get; set; } = string.Empty;
            public string numregidtriboperador { get; set; } = string.Empty;
            public int tipofiguratransporte { get; set; } = 0;
            public string dom_pais { get; set; } = string.Empty;
            public string dom_colonia { get; set; } = string.Empty;
            public string dom_calle { get; set; } = string.Empty;
            public string dom_codigopostal { get; set; } = string.Empty;
            public string dom_localidad { get; set; } = string.Empty;
            public string dom_numinterior { get; set; } = string.Empty;
            public string rfc { get; set; } = string.Empty;
            public string residenciafiscal { get; set; } = string.Empty;
            public string dom_numexterior { get; set; } = string.Empty;
            public string nombre { get; set; } = string.Empty;
            public string dom_referencia { get; set; } = string.Empty;
        }

        public STVCFDI()
        {
            
        }
    }
    public class CFDI_STATUS
    {
        public string uuid { get; set; }
        public string status { get; set; }
        public DateTime fecha_cancelacion { get; set; }
        public string status_cancelacion { get; set; }
        public string proceso_cancelacion { get; set; }
        public string pac_rfc { get; set; }
        public string pac_nombre_comercial { get; set; }
        public string pac_razon_social { get; set; }

         //cfdi.Pac.Rfc, cfdi.Pac.NombreComercial, cfdi.Pac.NombreComercial
        public CFDI_STATUS()
        {
            uuid = "";
            status = "";
            fecha_cancelacion = DateTime.MinValue;
            status_cancelacion = "";
            proceso_cancelacion = "";
            pac_rfc = "";
            pac_nombre_comercial = "";
            pac_razon_social = "";
        }
    }

    public class CFDI_TIMBRE_RESULTADO
    {
        public class INFORMACION_TIMBRE
        {
            public string s_version { get; set; } = string.Empty;
            public string s_uuid { get; set; } = string.Empty;
            public string s_fechatimbrado { get; set; } = string.Empty;
            public string s_sellocfd { get; set; } = string.Empty;
            public string s_numerocertificadosat { get; set; } = string.Empty;
            public string s_sellosat { get; set; } = string.Empty;
            public string s_leyenda { get; set; } = string.Empty;
            public string s_rfcproveedorcertificacion { get; set; } = string.Empty;
            public string s_cadenaoriginal { get; set; } = string.Empty;
            

        }

        public class ERROR_PROCESO
        {
            public int E_proveedorerror { get; set; } = 0;
            public int n_numero { get; set; } = 0;
            public string s_descripcion { get; set; } = string.Empty;
            public string s_sugerencia { get; set; } = string.Empty;
        }
        public long n_idtransaccion { get; set; } = 0;
        public bool b_enviadoACancelar { get; set; } = false;

        public INFORMACION_TIMBRE informacion_timbre { get; set; } = new INFORMACION_TIMBRE();
        public ERROR_PROCESO error_proceso { get; set; } = new ERROR_PROCESO();
    }

    public class STV_LIB
    {
        public static string Crear_CarpetasPorFecha(string s_carpetaBase, DateTime dt_fecha)
        {
            //_fecha = _fecha.Replace("/", "-");

            string _s_month = dt_fecha.Month.ToString();
            string _s_year = dt_fecha.Year.ToString();

            string _s_fecha = _s_year + "_" + _s_month;

            //Carpeta donde se van a guardar los xml descargados.
            string _s_carpetaConFecha = System.IO.Path.Combine(s_carpetaBase, _s_fecha);
            if (!System.IO.Directory.Exists(_s_carpetaConFecha))
            {
                System.IO.Directory.CreateDirectory(_s_carpetaConFecha);
            }
            else { }

            return _s_carpetaConFecha;
        }
    }
}
