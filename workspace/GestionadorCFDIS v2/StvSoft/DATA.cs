using System;
using HyperSoft.ElectronicDocumentLibrary;
using HyperSoft.ElectronicDocumentLibrary.Base;
using HyperSoft.ElectronicDocumentLibrary.Document;
using HyperSoft.ElectronicDocumentLibrary.ConstanciaRetenciones;
using Complemento = HyperSoft.ElectronicDocumentLibrary.Complemento;
using Documento = HyperSoft.ElectronicDocumentLibrary.Document;

namespace StvSoft
{
   public static class DATA
    {
        public static void CargarDatosCfdi33(ElectronicDocument electronicDocument)
        {
            electronicDocument.Data.Clear();

            // Datos del comprobante ***************************************************************
            electronicDocument.Data.Version.Value = "3.3";
            electronicDocument.Data.Serie.Value = "C";
            electronicDocument.Data.Folio.Value = "3000";
            electronicDocument.Data.Fecha.Value = DateTime.Now;
            electronicDocument.Data.FormaPago.Value = "01";
            electronicDocument.Data.CondicionesPago.Value = "Contado";
            electronicDocument.Data.SubTotal.Value = 7200.00;
            electronicDocument.Data.Descuento.Value = 360.00;
            electronicDocument.Data.Moneda.Value = "MXN";
            //electronicDocument.Data.TipoCambioMx.Value = 1.00;
            //electronicDocument.Data.TipoCambioMx.Decimals = 0;
            //electronicDocument.Data.TipoCambioMx.Dot = false;
            electronicDocument.Data.Total.Value = 7934.40;
            electronicDocument.Data.TipoComprobante.Value = "I";
            electronicDocument.Data.MetodoPago.Value = "PUE";
            electronicDocument.Data.LugarExpedicion.Value = "01000";
            //electronicDocument.Data.Confirmacion.Value = "ECVH1";
            // *************************************************************************************

            // Información de los comprobantes fiscales relacionados *******************************
            //electronicDocument.Data.CfdiRelacionados.TipoRelacion.Value = "01";
            //CfdiRelacionado cfdiRelacionado = electronicDocument.Data.CfdiRelacionados.Add();
            //cfdiRelacionado.Uuid.Value = "3BBDC347-3925-4792-B592-5151C773258B";
            // *************************************************************************************

            // Datos del emisor ********************************************************************
            electronicDocument.Data.Emisor.Rfc.Value = "FUNK671228PH6";
            electronicDocument.Data.Emisor.Nombre.Value = "KARLA FUENTE NOLASCO";
            electronicDocument.Data.Emisor.RegimenFiscal.Value = "605";
            // *************************************************************************************

            // Datos del Receptor ******************************************************************
            electronicDocument.Data.Receptor.Rfc.Value = "AAA010101AAA";
            electronicDocument.Data.Receptor.Nombre.Value = "Receptor de prueba";
            //electronicDocument.Data.Receptor.ResinciaFiscal.Value = "USA";
            //electronicDocument.Data.Receptor.NumeroRegistroIdTributario.Value = "121585958";
            electronicDocument.Data.Receptor.UsoCfdi.Value = "G01";
            // *************************************************************************************

            // Concepto  No 1 **********************************************************************
            Concepto concepto = electronicDocument.Data.Conceptos.Add();
            concepto.ClaveProductoServicio.Value = "01010101";
            //concepto.NumeroIdentificacion.Value = "UT421510";
            concepto.Cantidad.Value = 10;
            concepto.ClaveUnidad.Value = "H87";
            //concepto.Unidad.Value = "Pieza";
            concepto.Descripcion.Value = "DVD";
            concepto.ValorUnitario.Value = 120;
            concepto.Importe.Value = 1200;
            concepto.Descuento.Value = 360;

            //// Impuestos trasladados del concepto
            TrasladoConcepto trasladoConcepto = concepto.Impuestos.Traslados.Add();
            trasladoConcepto.Base.Value = 840;
            trasladoConcepto.Impuesto.Value = "002";
            trasladoConcepto.TipoFactor.Value = "Tasa";
            trasladoConcepto.TasaCuota.Value = 0.160000;
            trasladoConcepto.Importe.Value = 134.4;

            //// Impuestos retenidos del concepto
            /*RetencionConcepto retencionConcepto = concepto.Impuestos.Retenciones.Add();
            retencionConcepto.Base.Value = 0;
            retencionConcepto.Impuesto.Value = "001";
            retencionConcepto.TipoFactor.Value = "Tasa";
            retencionConcepto.TasaCuota.Value = 0.160000;          
            retencionConcepto.Importe.Value = 0; */

            //// Información aduanera del concepto
            /*Importacion importacion = concepto.InformacionAduanera.Add();
            importacion.Numero.Value = "10  47  3807  8003832"; */

            //// Cuenta predial del concepto
            //concepto.CuentaPredial.Numero.Value = "15956011002";

            //// Partes del concepto
            /*Partida partida = concepto.Partes.Add();
            partida.ClaveProductoServicio.Value = "01010101";
            partida.NumeroIdentificacion.Value = "7501030283645";
            partida.Cantidad.Value = 10;
            partida.Unidad.Value = "Piezas";
            partida.Descripcion.Value = "Descripción de la parte";
            partida.ValorUnitario.Value = 100;
            partida.Importe.Value = 1000; */

            ////// Información aduanera de la parte del concepto
            //Importacion importacion = partida.InformacionAduanera.Add();
            //importacion = partida.InformacionAduanera.Add();
            //importacion.Numero.Value = "10  47  3807  8003832";
            // *************************************************************************************

            // Concepto  No 2 **********************************************************************
            concepto = electronicDocument.Data.Conceptos.Add();
            concepto.ClaveProductoServicio.Value = "01010101";
            //concepto.NumeroIdentificacion.Value = "UT421510";
            concepto.Cantidad.Value = 1;
            concepto.ClaveUnidad.Value = "H87";
            //concepto.Unidad.Value = "Pieza";
            concepto.Descripcion.Value = "Computadora armada";
            concepto.ValorUnitario.Value = 3000;
            concepto.Importe.Value = 3000;
            //concepto.Descuento.Value = 1;

            //// Impuestos trasladados del concepto
            trasladoConcepto = concepto.Impuestos.Traslados.Add();
            trasladoConcepto.Base.Value = 3000;
            trasladoConcepto.Impuesto.Value = "002";
            trasladoConcepto.TipoFactor.Value = "Tasa";
            trasladoConcepto.TasaCuota.Value = 0.160000;
            trasladoConcepto.Importe.Value = 480;
            // *************************************************************************************

            // Concepto  No 3 **********************************************************************
            concepto = electronicDocument.Data.Conceptos.Add();
            concepto.ClaveProductoServicio.Value = "01010101";
            //concepto.NumeroIdentificacion.Value = "UT421510";
            concepto.Cantidad.Value = 1;
            concepto.ClaveUnidad.Value = "H87";
            //concepto.Unidad.Value = "Pieza";
            concepto.Descripcion.Value = "Monitor de 19 \" marca AOC";
            concepto.ValorUnitario.Value = 3000;
            concepto.Importe.Value = 3000;
            //concepto.Descuento.Value = 1;

            //// Impuestos trasladados del concepto
            trasladoConcepto = concepto.Impuestos.Traslados.Add();
            trasladoConcepto.Base.Value = 3000;
            trasladoConcepto.Impuesto.Value = "002";
            trasladoConcepto.TipoFactor.Value = "Tasa";
            trasladoConcepto.TasaCuota.Value = 0.160000;
            trasladoConcepto.Importe.Value = 480;
            // *************************************************************************************

            // Impuestos trasladados ***************************************************************
            Traslado traslado = electronicDocument.Data.Impuestos.Traslados.Add();
            traslado.Tipo.Value = "002";
            traslado.TipoFactor.Value = "Tasa";
            traslado.TasaCuota.Value = 0.160000;
            traslado.Importe.Value = 1094.4;

            electronicDocument.Data.Impuestos.TotalTraslados.Value = 1094.4;
            // *************************************************************************************

            // Impuestos retenidos *****************************************************************
            /*HyperSoft.ElectronicDocumentLibrary.Document.Impuesto retencion = electronicDocument.Data.Impuestos.Retenciones.Add();
            retencion.Tipo.Value = "001";
            retencion.Importe.Value = 0;

            electronicDocument.Data.Impuestos.TotalRetenciones.Value = 0; */
            // *************************************************************************************
        }
        public static void CargarDatosCfdi33(ElectronicDocument electronicDocument, STVCFDI cfdi, bool b_prueba = false)
        {
            electronicDocument.Data.Clear();

            // Datos del comprobante ***************************************************************
            electronicDocument.Data.Version.Value = cfdi.version;
            electronicDocument.Data.Serie.Value = cfdi.serie;
            electronicDocument.Data.Folio.Value = cfdi.folio;
            electronicDocument.Data.Fecha.Value = cfdi.fecha;
            electronicDocument.Data.FormaPago.Value = cfdi.formapago;
            electronicDocument.Data.CondicionesPago.Value = cfdi.condicionesdepago;
            
            electronicDocument.Data.SubTotal.Value = cfdi.subtotal;
            electronicDocument.Data.Descuento.Value = cfdi.descuento;
            electronicDocument.Data.Moneda.Value = cfdi.moneda;
            electronicDocument.Data.Total.Value = cfdi.total;
            electronicDocument.Data.TipoComprobante.Value = cfdi.tipodecomprobante;
            electronicDocument.Data.MetodoPago.Value = cfdi.metodopago;
            electronicDocument.Data.LugarExpedicion.Value = cfdi.lugarexpedicion;
            electronicDocument.Data.TipoCambioMx.Value = cfdi.tipocambio;
            electronicDocument.Data.FormaPago.Value = cfdi.formapago;

            // Datos del emisor / receptor ********************************************************************
            cfdi.emisor.rfc = cfdi.emisor.rfc.Replace("-", "");
            if (b_prueba)
            {
                electronicDocument.Data.Emisor.Rfc.Value = "FUNK671228PH6";
                electronicDocument.Data.Emisor.Nombre.Value = "KARLA FUENTE NOLASCO";
                electronicDocument.Data.Emisor.RegimenFiscal.Value = "605";
                electronicDocument.Data.Receptor.Rfc.Value = "AAA010101AAA";
                electronicDocument.Data.Receptor.Nombre.Value = "Receptor de prueba";
                electronicDocument.Data.Receptor.UsoCfdi.Value = "G01";
            }
            else
            {
                electronicDocument.Data.Emisor.Rfc.Value = cfdi.emisor.rfc.Replace("-", "");
                electronicDocument.Data.Emisor.Nombre.Value = cfdi.emisor.nombre;
                electronicDocument.Data.Emisor.RegimenFiscal.Value = cfdi.emisor.regimenfiscal;
                electronicDocument.Data.Receptor.Rfc.Value = cfdi.receptor.rfc.Replace("-", "");
                electronicDocument.Data.Receptor.Nombre.Value = cfdi.receptor.nombre;
                electronicDocument.Data.Receptor.UsoCfdi.Value = cfdi.receptor.usocfdi;
            }
            
            
            // *************************************************************************************

            
            #region concepto
            foreach (STVCFDI.CONCEPTO cfdi_concepto in cfdi.conceptos)
            {
                // Concepto **************************************************************************

                Concepto concepto = electronicDocument.Data.Conceptos.Add();

                concepto.ClaveProductoServicio.Value = cfdi_concepto.claveprodserv;
                concepto.Cantidad.Value = cfdi_concepto.cantidad;
                concepto.ClaveUnidad.Value = cfdi_concepto.claveunidad;
                concepto.Descuento.Value = cfdi_concepto.descuento;
                concepto.ValorUnitario.Value = cfdi_concepto.valorunitario;
                concepto.NumeroIdentificacion.Value = cfdi_concepto.noidentificacion;
                concepto.Descripcion.Value = cfdi_concepto.descripcion;
                concepto.Unidad.Value = cfdi_concepto.unidad;
                concepto.Importe.Value = cfdi_concepto.importe;

                foreach (STVCFDI.IMPUESTO cfdi_impuesto in cfdi_concepto.impuestos.trasladados)
                {
                    //// Impuestos trasladados del concepto
                    TrasladoConcepto trasladoConcepto = concepto.Impuestos.Traslados.Add();
                    trasladoConcepto.Base.Value = cfdi_impuesto.basei;
                    trasladoConcepto.Impuesto.Value = cfdi_impuesto.impuesto;
                    trasladoConcepto.TipoFactor.Value = cfdi_impuesto.tipofactor;
                    trasladoConcepto.TasaCuota.Value = cfdi_impuesto.tasaocuota;
                    trasladoConcepto.Importe.Value = cfdi_impuesto.importe;
                }

                foreach (STVCFDI.IMPUESTO cfdi_impuesto in cfdi_concepto.impuestos.retenidos)
                {
                    RetencionConcepto retencionConcepto = concepto.Impuestos.Retenciones.Add();
                    retencionConcepto.Base.Value = cfdi_impuesto.basei;
                    retencionConcepto.Impuesto.Value = cfdi_impuesto.impuesto;
                    retencionConcepto.TipoFactor.Value = cfdi_impuesto.tipofactor;
                    retencionConcepto.TasaCuota.Value = cfdi_impuesto.tasaocuota;
                    retencionConcepto.Importe.Value = cfdi_impuesto.importe;
                }

            }


            foreach (STVCFDI.IMPUESTO cfdi_impuesto in cfdi.impuestos.trasladados)
            {
                Traslado trasladoImpuesto = electronicDocument.Data.Impuestos.Traslados.Add();
                trasladoImpuesto.Tipo.Value = cfdi_impuesto.impuesto;
                trasladoImpuesto.TipoFactor.Value = cfdi_impuesto.tipofactor;
                trasladoImpuesto.TasaCuota.Value = cfdi_impuesto.tasaocuota;
                trasladoImpuesto.Importe.Value = cfdi_impuesto.importe;
            }
            electronicDocument.Data.Impuestos.TotalTraslados.Value = cfdi.impuestos.totalimpuestostrasladados;

            foreach (STVCFDI.IMPUESTO cfdi_impuesto in cfdi.impuestos.retenidos)
            {
                Impuesto trasladoImpuesto = electronicDocument.Data.Impuestos.Retenciones.Add();
                trasladoImpuesto.Tipo.Value = cfdi_impuesto.impuesto;
                //trasladoImpuesto.TipoFactor.Value = cfdi_impuesto.tipofactor; // Parece error en la libreria no lo soporta
                //trasladoImpuesto.TasaCuota.Value = cfdi_impuesto.tasaocuota; // Parece error en la libreria no lo soporta
                trasladoImpuesto.Importe.Value = cfdi_impuesto.importe;
            }
            electronicDocument.Data.Impuestos.TotalRetenciones.Value = cfdi.impuestos.totalimpuestosretenidos;
            #endregion

            if (cfdi.complemento.pagos.version != string.Empty)
            {
                electronicDocument.Data.Complementos.Add(ComplementoType.RecepcionPago);
                Complemento.RecepcionPago.Data compPago = (Complemento.RecepcionPago.Data)electronicDocument.Data.Complementos.Last();

                compPago.Version.Value = cfdi.complemento.pagos.version;
                foreach (STVCFDI.PAGO cfdi_pago in cfdi.complemento.pagos.pago)
                {
                    Complemento.RecepcionPago.Pago pago = compPago.Pagos.Add();
                    pago.FechaPago.Value = cfdi_pago.fechapago;
                    pago.FormaPago.Value = cfdi_pago.formadepagop;
                    pago.Moneda.Value = cfdi_pago.monedap;
                    pago.Monto.Value = cfdi_pago.monto;
                    if (pago.Moneda.Value != "MXN")
                    {
                        pago.TipoCambio.Value = cfdi_pago.tipocambiop;
                    }
                    else {
                        pago.TipoCambio.Clear(); 
                    }

                    foreach (STVCFDI.DOCTORELACIONADO cfdi_docto in cfdi_pago.doctorelacionado)
                    {
                        Complemento.RecepcionPago.DocumentoRelacionado docto = pago.DocumentosRelacionados.Add();
                        docto.Folio.Value = cfdi_docto.folio;
                        docto.MetodoPago.Value = cfdi_docto.metododepagodr;
                        docto.IdDocumento.Value = cfdi_docto.iddocumento;
                        docto.ImporteSaldoAnterior.Value = cfdi_docto.impsaldoant;
                        docto.ImporteSaldoInsoluto.Value = cfdi_docto.impsaldoinsoluto;
                        docto.Moneda.Value = cfdi_docto.monedadr;
                        if (docto.Moneda.Value != pago.Moneda.Value)
                        {
                            docto.TipoCambio.Value = cfdi_docto.tipocambiodr;
                        }
                        else {
                            docto.TipoCambio.Clear();

                        }
                        docto.Serie.Value = cfdi_docto.serie;
                        docto.NumeroParcialidad.Value = cfdi_docto.numparcialidad;
                        docto.ImportePagado.Value = cfdi_docto.imppagado;
                    }

                }
            }
            else
            { }

            // Se ajusta dependiento del tipo de comprobante
            if (electronicDocument.Data.TipoComprobante.Value == "P")
            {
                /* TODO verificar que los valores esten limpios, de lo contrario regresar error al servidor 
                 Algo como if Subtotal != 0; regresar error de Subtotal no es zero, else usar la instruccion para escribirlo
                 Juntar todos los errores en un solo string
                 */
                electronicDocument.Data.SubTotal.SetData("0");
                electronicDocument.Data.Total.SetData("0");
                electronicDocument.Data.Moneda.SetData("XXX");
                electronicDocument.Data.FormaPago.Clear();
                electronicDocument.Data.MetodoPago.Clear();
                electronicDocument.Data.CondicionesPago.Clear();
                electronicDocument.Data.Descuento.Clear();
                electronicDocument.Data.TipoCambioMx.Clear();
                electronicDocument.Data.Impuestos.Clear();
                electronicDocument.Data.Receptor.UsoCfdi.SetData("P01");

                for (int index = 0; index < electronicDocument.Data.Conceptos.Count; index++)
                {
                    electronicDocument.Data.Conceptos[index].NumeroIdentificacion.Clear();
                    electronicDocument.Data.Conceptos[index].ValorUnitario.SetData("0");
                    electronicDocument.Data.Conceptos[index].Importe.SetData("0");
                    electronicDocument.Data.Conceptos[index].Descuento.Clear();
                    electronicDocument.Data.Conceptos[index].Impuestos.Clear();
                    electronicDocument.Data.Conceptos[index].Unidad.Clear();
                    electronicDocument.Data.Conceptos[index].Cantidad.SetData("1");
                }
            }
            else if (electronicDocument.Data.TipoComprobante.Value == "I")
            {

            }
            else if (electronicDocument.Data.TipoComprobante.Value == "E")
            {

            }
            else if (electronicDocument.Data.TipoComprobante.Value == "T")
            {

            }
            else
            {

            }

        }

        public static void CargarDatosCfdi33_Traslado(ElectronicDocument electronicDocument, STVCFDI cfdi, bool b_prueba = false)
        {
            electronicDocument.Data.Clear();

            // Datos del comprobante ***************************************************************
            electronicDocument.Data.Version.Value = cfdi.version;
            electronicDocument.Data.Serie.Value = cfdi.serie;
            electronicDocument.Data.Folio.Value = cfdi.folio;
            electronicDocument.Data.Fecha.Value = cfdi.fecha;
            electronicDocument.Data.FormaPago.Value = cfdi.formapago;
            if (cfdi.condicionesdepago == string.Empty)
            {
                if (cfdi.metodopago == "PPD")
                {
                    electronicDocument.Data.CondicionesPago.Value = "Credito";
                }
                else
                {
                    electronicDocument.Data.CondicionesPago.Value = "Contado";
                }

            }
            else
            {
                electronicDocument.Data.CondicionesPago.Value = cfdi.condicionesdepago;
            }

            electronicDocument.Data.SubTotal.Value = cfdi.subtotal;
            electronicDocument.Data.Descuento.Value = cfdi.descuento;
            electronicDocument.Data.Moneda.Value = cfdi.moneda;
            electronicDocument.Data.Total.Value = cfdi.total;
            electronicDocument.Data.TipoComprobante.Value = cfdi.tipodecomprobante;
            electronicDocument.Data.MetodoPago.Value = cfdi.metodopago;
            electronicDocument.Data.LugarExpedicion.Value = cfdi.lugarexpedicion;

            electronicDocument.Data.TipoCambioMx.Value = cfdi.tipocambio;
            electronicDocument.Data.FormaPago.Value = cfdi.formapago;

            // Datos del emisor / receptor ********************************************************************
            cfdi.emisor.rfc = cfdi.emisor.rfc.Replace("-", "");
            if (b_prueba)
            {
                electronicDocument.Data.Emisor.Rfc.Value = "FUNK671228PH6";
                electronicDocument.Data.Emisor.Nombre.Value = "KARLA FUENTE NOLASCO";
                electronicDocument.Data.Emisor.RegimenFiscal.Value = "605";
                electronicDocument.Data.Receptor.Rfc.Value = "AAA010101AAA";
                electronicDocument.Data.Receptor.Nombre.Value = "Receptor de prueba";
                electronicDocument.Data.Receptor.UsoCfdi.Value = "G01";
            }
            else
            {
                electronicDocument.Data.Emisor.Rfc.Value = cfdi.emisor.rfc.Replace("-", "");
                electronicDocument.Data.Emisor.Nombre.Value = cfdi.emisor.nombre;
                electronicDocument.Data.Emisor.RegimenFiscal.Value = cfdi.emisor.regimenfiscal;
                electronicDocument.Data.Receptor.Rfc.Value = cfdi.receptor.rfc.Replace("-", "");
                electronicDocument.Data.Receptor.Nombre.Value = cfdi.receptor.nombre;
                electronicDocument.Data.Receptor.UsoCfdi.Value = cfdi.receptor.usocfdi;
            }


            // *************************************************************************************


            #region concepto
            foreach (STVCFDI.CONCEPTO cfdi_concepto in cfdi.conceptos)
            {
                // Concepto **************************************************************************

                Concepto concepto = electronicDocument.Data.Conceptos.Add();

                concepto.ClaveProductoServicio.Value = cfdi_concepto.claveprodserv;
                concepto.Cantidad.Value = cfdi_concepto.cantidad;
                concepto.ClaveUnidad.Value = cfdi_concepto.claveunidad;
                concepto.Descuento.Value = cfdi_concepto.descuento;
                concepto.ValorUnitario.Value = cfdi_concepto.valorunitario;
                concepto.NumeroIdentificacion.Value = cfdi_concepto.noidentificacion;
                concepto.Descripcion.Value = cfdi_concepto.descripcion;
                concepto.Unidad.Value = cfdi_concepto.unidad;
                concepto.Importe.Value = cfdi_concepto.importe;

                foreach (STVCFDI.IMPUESTO cfdi_impuesto in cfdi_concepto.impuestos.trasladados)
                {
                    //// Impuestos trasladados del concepto
                    TrasladoConcepto trasladoConcepto = concepto.Impuestos.Traslados.Add();
                    trasladoConcepto.Base.Value = cfdi_impuesto.basei; 
                    trasladoConcepto.Impuesto.Value = cfdi_impuesto.impuesto;
                    trasladoConcepto.TipoFactor.Value = cfdi_impuesto.tipofactor;
                    trasladoConcepto.TasaCuota.Value = cfdi_impuesto.tasaocuota;
                    trasladoConcepto.Importe.Value = cfdi_impuesto.importe;
                }

                foreach (STVCFDI.IMPUESTO cfdi_impuesto in cfdi_concepto.impuestos.retenidos)
                {
                    RetencionConcepto retencionConcepto = concepto.Impuestos.Retenciones.Add();
                    retencionConcepto.Base.Value = cfdi_impuesto.basei;
                    retencionConcepto.Impuesto.Value = cfdi_impuesto.impuesto;
                    retencionConcepto.TipoFactor.Value = cfdi_impuesto.tipofactor;
                    retencionConcepto.TasaCuota.Value = cfdi_impuesto.tasaocuota;
                    retencionConcepto.Importe.Value = cfdi_impuesto.importe;
                }

            }


            foreach (STVCFDI.IMPUESTO cfdi_impuesto in cfdi.impuestos.trasladados)
            {
                Traslado trasladoImpuesto = electronicDocument.Data.Impuestos.Traslados.Add();
                trasladoImpuesto.Tipo.Value = cfdi_impuesto.impuesto;
                trasladoImpuesto.TipoFactor.Value = cfdi_impuesto.tipofactor;
                trasladoImpuesto.TasaCuota.Value = cfdi_impuesto.tasaocuota;
                trasladoImpuesto.Importe.Value = cfdi_impuesto.importe;
            }
            electronicDocument.Data.Impuestos.TotalTraslados.Value = cfdi.impuestos.totalimpuestostrasladados;

            foreach (STVCFDI.IMPUESTO cfdi_impuesto in cfdi.impuestos.retenidos)
            {
                Impuesto trasladoImpuesto = electronicDocument.Data.Impuestos.Retenciones.Add();
                trasladoImpuesto.Tipo.Value = cfdi_impuesto.impuesto;
                //trasladoImpuesto.TipoFactor.Value = cfdi_impuesto.tipofactor; // Parece error en la libreria no lo soporta
                //trasladoImpuesto.TasaCuota.Value = cfdi_impuesto.tasaocuota; // Parece error en la libreria no lo soporta
                trasladoImpuesto.Importe.Value = cfdi_impuesto.importe;
            }
            electronicDocument.Data.Impuestos.TotalRetenciones.Value = cfdi.impuestos.totalimpuestosretenidos;
            #endregion

        }
    }
}
