using System;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using HyperSoft.Resource;
using HyperSoft.ElectronicDocumentLibrary.Base;
using HyperSoft.ElectronicDocumentLibrary.Certificate;
using HyperSoft.ElectronicDocumentLibrary.ConstanciaRetenciones;
using HyperSoft.ElectronicDocumentLibrary.Document;
using HyperSoft.ElectronicDocumentLibrary.ECodex;
using HyperSoft.ElectronicDocumentLibrary.ECodex.Cancelar;
using HyperSoft.ElectronicDocumentLibrary.Manage;
using HyperSoft.Shared;
using HyperSoft.ElectronicDocumentLibrary;
using Data = StvSoft.DATA;
using Informacion = HyperSoft.ElectronicDocumentLibrary.ECodex.Timbre.Informacion;
using System.Threading;

using NLog;

namespace StvSoft
{
   public class OPCIONES_CFDI
    {
        #region Constantes

        //private const string strDirectorioBase = "C:\\descarga_xml";

        private const bool UseOwnTokenDefinition = true;
        private const bool SetDateTimeServerDefinition = true;

        public static string Line = string.Empty.PadRight(100, '=');

       //public static readonly string LicenseCfdiData = "C:\\descarga_xml\\3_ALG040428RG4\\Licencia\\2021_02_18_licencia.license";

        #endregion

        #region Vars

        //private TabPage[] pages;

        private string generationDirectory;
        private ElectronicManage manage;
        private ElectronicCertificate certificate;
        private ElectronicDocument electronicDocument;
        private ConstanciaRetenciones constanciaRetenciones;
        private Proveedor proveedor = new Proveedor();

        private DateTime dateTimeServer = DateTime.MinValue;
        private DateTime dateLastToken = DateTime.Now;

        private string s_rfc = string.Empty;
        private string s_rutaTimbrado = string.Empty;
        private string s_rutaACer = string.Empty;
        private string s_rutaAKey = string.Empty;
        private string s_passSAT = string.Empty;

        private static Logger logger = LogManager.GetCurrentClassLogger();

        private bool b_prueba = true; 

        #endregion

        public OPCIONES_CFDI(string s_rfc, string s_rutaTimbrado, string s_rutaACer, string s_rutaAKey, string s_passSAT, bool b_prueba = true)
        {
            this.s_rfc = s_rfc;
            this.s_rutaTimbrado = s_rutaTimbrado;
            this.s_rutaACer = s_rutaACer;
            this.s_rutaAKey = s_rutaAKey;
            this.s_passSAT = s_passSAT;
            this.b_prueba = b_prueba;
        }

        public CFDI_TIMBRE_RESULTADO CancelarCfdi(ref string s_uuid, out string s_resultadoTexto)
        {
            CFDI_TIMBRE_RESULTADO _D_resultado = new CFDI_TIMBRE_RESULTADO();
            CancelarCfdiParameters _O_parameters = new CancelarCfdiParameters().Initialization();

            CreateObjects();

            this.UseOwnToken(_O_parameters, UseOwnTokenDefinition, SetDateTimeServerDefinition);

            try
            {
                // Se crea el objeto que contiene la información retornada por el PAC
                HyperSoft.ElectronicDocumentLibrary.ECodex.Cancelar.Informacion _O_informacion = new HyperSoft.ElectronicDocumentLibrary.ECodex.Cancelar.Informacion().Initialization();

                // Se asigna los parámetros a usar durante el proceso
                _O_parameters.Informacion = _O_informacion;
                if (this.b_prueba)
                {
                    _O_parameters.TestMode = true;
                    _O_parameters.Rfc.Value = "FUNK671228PH6";
                }
                else
                {
                    _O_parameters.Rfc.Value = this.s_rfc;
                }
                _O_parameters.IdTransaccion.Value = long.MaxValue;

                // NOTA
                //   1. La cantidad máxima de UUIDs a cancelar en una sola peticicón es de 400 UUIDs
                //   2. Todos los UUIDs deben pertener al mismo emisor
                //   3. No se pueden usar colocar UUIDs repetidos

                _O_parameters.Uuid.Value = s_uuid;

                // Se envia a cancelar el documento
                ProcessProviderResult _E_proveedor = ProcessProviderResult.ErrorInDocument;
                _E_proveedor = this.proveedor.CancelarCfdi(_O_parameters);

                Chronometer.Instance.Stop();

                // Se le da formato a la información retornada por el PAC

                _D_resultado = this.FormatInformationCancelacion(s_uuid, _O_parameters.IdTransaccion.Value, _O_informacion, _E_proveedor, out s_resultadoTexto);

                if (_E_proveedor != ProcessProviderResult.Ok)
                {
                    logger.Error("CancelarCfdi: " + s_resultadoTexto);
                }
                else if (this.b_prueba)
                {
                    logger.Error("En Prueba: " + s_resultadoTexto);
                }
                else
                {
                    /* Todo bien */
                }
            }
            finally
            {
                _O_parameters.Dispose();
                Chronometer.Instance.Stop();
            }
            return _D_resultado;
        }

        public CFDI_TIMBRE_RESULTADO TimbrarCfdi(ref string s_json, out string resultadoTexto, out string rutaArchivo)
        {
            CFDI_TIMBRE_RESULTADO D_resultado = new CFDI_TIMBRE_RESULTADO();

            rutaArchivo = "";

            STVCFDI _O_cfdi;
            /* Se normaliza un nombre que no es reservado en python */
            s_json = s_json.Replace("\"Base\"", "\"Basei\"");
            _O_cfdi = JsonConvert.DeserializeObject<STVCFDI>(s_json);

            CreateObjects();

            //generationDirectory = CrearDirectorioCFDIsTimbrados(s_rfc,s_cuenta_id);

            TimbrarParameters parameters = new TimbrarParameters().Initialization();

            this.UseOwnToken(parameters, UseOwnTokenDefinition, SetDateTimeServerDefinition);

            try
            {
                //En este método se cargan los datos de la factura.
                Data.CargarDatosCfdi33(this.electronicDocument, _O_cfdi, this.b_prueba);

                // Se crea el objeto que contiene la información retornada por el PAC
                Informacion informacion = new Informacion().Initialization();

                // Se asigna los parámetros a usar durante el proceso
                parameters.ElectronicDocument = this.electronicDocument;
                parameters.Informacion = informacion;
                
                if (this.b_prueba)
                {
                    parameters.TestMode = true;
                    parameters.Rfc.Value = "FUNK671228PH6";
                }
                else
                {
                    parameters.Rfc.Value = this.s_rfc;
                }
                
                parameters.IdTransaccion.Value = long.MaxValue;

                // Se envia a timbrar el documento
                ProcessProviderResult result =  this.proveedor.GenerarTimbre(parameters);

                Chronometer.Instance.Stop();

                // En este caso se verifica que el proceso fue correcto
                //if (result == ProcessProviderResult.Ok)
                //{
                //this.txtUuidDescarga.Text = informacion.Timbre.Uuid.Value;
                //this.txtUuidEstado.Text = informacion.Timbre.Uuid.Value;
                //}

                // Se le da formato a la información retornada por el PAC
                D_resultado = this.FormatInformationTimbre(parameters.IdTransaccion.Value, informacion, result, this.electronicDocument.FingerPrintPac, out resultadoTexto);


                if (result != ProcessProviderResult.Ok)
                {
                    logger.Error("TimbrarCfdi: " + resultadoTexto);
                }
                else
                {
                    string _s_directorio = STV_LIB.Crear_CarpetasPorFecha(this.s_rutaTimbrado, informacion.Timbre.FechaTimbrado.Value);
                    string _s_fechaEnNombre = informacion.Timbre.FechaTimbrado.Value.ToString("yyMMddHHmmss");
                    rutaArchivo = Path.Combine(_s_directorio, _s_fechaEnNombre + "_" + informacion.Timbre.Uuid.Value + ".xml");
                    // Se guarda el documento, en este punto ya contiene los datos del timbre
                    SaveOptions options = this.electronicDocument.Manage.Save.Options;
                    this.electronicDocument.Manage.Save.Options = SaveOptions.EncodeApostrophe;
                    try
                    {

                        //TODO incluir la informacion CFDI_STATUS al final del documento
                        this.electronicDocument.SaveToFile(rutaArchivo);


                    }
                    catch
                    {
                        rutaArchivo = "";
                        //MessageBox.Show(this.electronicDocument.ErrorText);
                        D_resultado.error_proceso.s_descripcion += "| " + this.electronicDocument.ErrorText;
                    }
                    finally
                    {
                        this.electronicDocument.Manage.Save.Options = options;
                    }
                }
            }
            finally
            {
                parameters.Dispose();
                Chronometer.Instance.Stop();
            }

            return D_resultado;
        }

        private CFDI_TIMBRE_RESULTADO FormatInformationTimbre(long idTransaccion, Informacion informacion, ProcessProviderResult providerResult, string cadenaOrignal, out string s_errorMensaje)
        {
            StringBuilder text = new StringBuilder();
            CFDI_TIMBRE_RESULTADO timbre_resultado = new CFDI_TIMBRE_RESULTADO();


            if (informacion.Timbre.IsAssigned)
            {
                timbre_resultado.informacion_timbre.s_version = informacion.Timbre.Version.AsString();
                timbre_resultado.informacion_timbre.s_uuid = informacion.Timbre.Uuid.AsString();
                timbre_resultado.informacion_timbre.s_fechatimbrado = informacion.Timbre.FechaTimbrado.Value.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss");
                timbre_resultado.informacion_timbre.s_sellocfd = informacion.Timbre.SelloCfd.AsString();
                timbre_resultado.informacion_timbre.s_numerocertificadosat = informacion.Timbre.NumeroCertificadoSat.AsString();
                timbre_resultado.informacion_timbre.s_sellosat = informacion.Timbre.SelloSat.AsString();
                timbre_resultado.informacion_timbre.s_leyenda = informacion.Timbre.Leyenda.AsString();
                timbre_resultado.informacion_timbre.s_rfcproveedorcertificacion = informacion.Timbre.RfcProveedorCertificacion.AsString();
                timbre_resultado.informacion_timbre.s_cadenaoriginal = cadenaOrignal;

                text.AppendLine(string.Empty);
                text.AppendLine("INFORMACION DEL TIMBRE");
                text.AppendLine(Line);
                text.AppendLine("Versión                    : " + informacion.Timbre.Version.AsString());
                text.AppendLine("UUID                       : " + informacion.Timbre.Uuid.AsString());
                text.AppendLine("Fecha de timbrado          : " + informacion.Timbre.FechaTimbrado.AsString());
                text.AppendLine("Sello del CFD              : " + informacion.Timbre.SelloCfd.AsString());
                text.AppendLine("No. de Certificado del SAT : " + informacion.Timbre.NumeroCertificadoSat.AsString());
                text.AppendLine("Sello del SAT              : " + informacion.Timbre.SelloSat.AsString());
                text.AppendLine("Leyenda                    : " + informacion.Timbre.Leyenda.AsString());
                text.AppendLine("RFC del PAC                : " + informacion.Timbre.RfcProveedorCertificacion.AsString());
                text.AppendLine();
                text.AppendLine("CADENA ORIGINAL DEL COMPLEMENTO");
                text.AppendLine(Line);
                text.AppendLine("Cadena original    : " + cadenaOrignal);
                text.AppendLine();
            }

            if (idTransaccion != 0)
            {
                text.AppendLine(string.Empty);
                text.AppendLine("INFORMACION DE LA TRANSACCION");
                text.AppendLine(Line);
                text.AppendLine("Número de transacción         : " + idTransaccion);
            }
            timbre_resultado.n_idtransaccion = idTransaccion;

            if (informacion.Error.IsAssigned == false)
            {
                s_errorMensaje = text.ToString();
                return timbre_resultado;
            }

            this.FormatTypeError(text, providerResult);

            timbre_resultado.error_proceso.E_proveedorerror = Convert.ToInt32(providerResult);

            timbre_resultado.error_proceso.n_numero = informacion.Error.Numero.Value;
            timbre_resultado.error_proceso.s_descripcion = informacion.Error.Descripcion.Value;
            timbre_resultado.error_proceso.s_sugerencia = informacion.Error.Sugerencia.Value;

            text.AppendLine("ERROR QUE SE GENERO EN EL PROCESO");
            text.AppendLine(Line);
            text.AppendLine("Número      : " + informacion.Error.Numero.Value);
            text.AppendLine("Descripción : " + informacion.Error.Descripcion.Value);
            text.AppendLine();
            text.AppendLine(Line);
            text.AppendLine(Line);

            s_errorMensaje = text.ToString();
            return timbre_resultado;
        }

        private void FormatTypeError(StringBuilder text, ProcessProviderResult providerResult)
        {
            if (providerResult == ProcessProviderResult.Ok)
                return;

            text.AppendLine();
            text.AppendLine();
            text.AppendLine(Line);
            switch (providerResult)
            {
                case ProcessProviderResult.ErrorInDocument:
                    text.AppendLine("EL ERROR SE PRESENTO AL GENERAR LOS PARAMETROS,");
                    text.AppendLine("ANTES DE EJECUTAR LA OPERACION CON EL PAC.");
                    break;

                case ProcessProviderResult.ErrorWithProvider:
                    text.AppendLine("EL ERROR FUE GENERADO POR EL PAC.");
                    break;

                case ProcessProviderResult.ErrorInConnectionWithProvider:
                    text.AppendLine("EL ERROR SE PRESENTO AL CONTACTAR EL SERVIDOR DEL PAC.");
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
            text.AppendLine(Line);
            text.AppendLine();
            text.AppendLine();
        }

        private void CreateObjects()
        {
            // Se crea el objeto que representa al PAC y para este ejemplo lo he configurado 
            // con los parámetros de prueba de la conexión. Para mayor información acerca de 
            // lo relación con el PAC póngase en contacto con el mismo.
            this.proveedor = new Proveedor().Initialization();

            // Instanciamos la clase TManageElectronicDocument
            this.manage = new ElectronicManage().Initialization();

            //  . EDL, a partir de la versión 2017.06.02, por defecto realiza una validación LITE del CFDI 3.3 contra el schema
            //  . Para que se realice una validación FULL, debes activar las siguientes líneas
            //  . Si deseas conocer más al respecto:
            //       www.facturando.mx/blog/index.php/2017/06/01/edl-validacion-de-un-cfdi-3-3/
            //this.manage.Save.Options |= SaveOptions.ValidateWithSchema;
            //this.manage.Save.Options -= SaveOptions.ValidateWithSchemaLite;


            //Se crea la clase que va a ser usada en el proceso de firmado, se le pasa el certificado,
            //la llave privada y el password de la misma.
            this.certificate = new ElectronicCertificate();
            if (this.b_prueba)
            {
                // Se cargan a memoria el archivo de autoridades certificadoras de prueba
                // *** ESTE LÍNEA DE CODIGO DEBE SER ELIMINADA EN UN AMBIENTE DE PRODUCCIÓN ***
                this.manage.CertificateAuthorityList.UseForTest();

                this.certificate.Load(
                    this.s_rutaACer,
                    this.s_rutaAKey,
                    "12345678a"
                    );
            }
            else
            {
                this.certificate.Load(
                    this.s_rutaACer,
                    this.s_rutaAKey,
                    this.s_passSAT
                    );
            }

            // Asignamos el certificado al objeto Manage, esta ultima, es la encargada de contener
            // y administrar TODOS los recursos que serán usados en el proceso
            this.manage.Save.AssignCertificate(this.certificate);


            // Se instancia la clase que es la encarga de llevar a cabo el proceso de generación de la FACTURA ELECTRONICA (CFDI)
            // y se le pasa el objeto que tiene todos los recursos necesarios para llevar a cabo dicho proceso.
            this.electronicDocument = new ElectronicDocument().Initialization();
            this.electronicDocument.AssignManage(this.manage);


            // Se instancia la clase que es la encarga de llevar a cabo el proceso de generación de la CONSTANCIA DE RETENCIONES Y PAGOS
            // y se le pasa el objeto que tiene todos los recursos necesarios para llevar a cabo dicho proceso.
            this.constanciaRetenciones = new ConstanciaRetenciones().Initialization();
            this.constanciaRetenciones.AssignManage(this.manage);

            // Directorio donde van a ser almacenado los XML generados
            this.generationDirectory = this.s_rutaTimbrado;
        }

        public bool CancelarMultiplesCfdi(out string resultadoTexto)
        {
            CancelarMultiplesCfdiParameters parameters = new CancelarMultiplesCfdiParameters().Initialization();

            this.UseOwnToken(parameters, UseOwnTokenDefinition, SetDateTimeServerDefinition);
            try
            {
                // Se crea el objeto que contiene la información retornada por el PAC
                InformacionMultiple informacion = new InformacionMultiple().Initialization();

                // Se asigna los parámetros a usar durante el proceso
                parameters.Informacion = informacion;
                parameters.TestMode = true;
                parameters.Rfc.Value = s_rfc;
                parameters.IdTransaccion.Value = long.MaxValue;

                // NOTA
                //   1. La cantidad máxima de UUIDs a cancelar en una sola peticicón es de 400 UUIDs
                //   2. Todos los UUIDs deben pertener al mismo emisor
                //   3. No se pueden usar colocar UUIDs repetidos

                string json_uuids = "El json que se va a mandar desde el webService"; 

                dynamic _lst_uuids = JsonConvert.DeserializeObject(json_uuids);

                foreach (var uuid in _lst_uuids)
                {
                    parameters.Uuids.Add(uuid);
                }

                parameters.Uuids.Add("0B305A40-7316-4FD3-A5E1-711DC252609A");
                parameters.Uuids.Add("F5EFCF60-18C7-49F9-9763-69B0A2832D1B");

                // Se envia a cancelar el documento
                ProcessProviderResult result = this.proveedor.CancelarMultiplesCfdi(parameters);

                Chronometer.Instance.Stop();

                // Se le da formato a la información retornada por el PAC
                resultadoTexto = this.FormatInformationCancelacionMultiple(parameters.IdTransaccion.Value, informacion, result);

                if (result != ProcessProviderResult.Ok)
                {
                    logger.Error("CancelarMultiplesCfdi: " + resultadoTexto);
                    return false;
                }

                return true;
            }
            finally
            {
                parameters.Dispose();
                Chronometer.Instance.Stop();
            }
        }

        private CFDI_TIMBRE_RESULTADO FormatInformationCancelacion(string s_uuid, long idTransaccion, HyperSoft.ElectronicDocumentLibrary.ECodex.Cancelar.Informacion informacion, ProcessProviderResult providerResult, out string s_errorMsg)
        {
            StringBuilder builder = new StringBuilder();
            CFDI_TIMBRE_RESULTADO timbre_resultado = new CFDI_TIMBRE_RESULTADO();

            builder.AppendLine(string.Empty);
            builder.AppendLine("RESULTADO");
            builder.AppendLine(Line);
            if (informacion.EnviadoACancelar)
            {
                timbre_resultado.b_enviadoACancelar = true;
                builder.AppendLine($"El UUID {s_uuid} fue marcado para su cancelación.");
            }
            else
            {
                timbre_resultado.b_enviadoACancelar = false;
                builder.AppendLine(s_uuid);
                builder.AppendLine("  - El UUID NO fue marcado para su cancelación");
                builder.AppendLine("  - Mensaje: " + informacion.Error.Descripcion.Value);
            }

            builder.AppendLine();

            if (idTransaccion != 0)
            {
                builder.AppendLine();
                builder.AppendLine("INFORMACION DE LA TRANSACCION");
                builder.AppendLine(Line);
                builder.AppendLine("Número de transacción         : " + idTransaccion);
            }

            if (informacion.Error.IsAssigned == false)
            {
                s_errorMsg = builder.ToString();
                return timbre_resultado;
            }
            else { }

            this.FormatTypeError(builder, providerResult);

            timbre_resultado.error_proceso.E_proveedorerror = Convert.ToInt32(providerResult);

            timbre_resultado.error_proceso.n_numero = informacion.Error.Numero.Value;
            timbre_resultado.error_proceso.s_descripcion = informacion.Error.Descripcion.Value;
            timbre_resultado.error_proceso.s_sugerencia = informacion.Error.Sugerencia.Value;

            builder.AppendLine("ERROR QUE SE GENERO EN EL PROCESO");
            builder.AppendLine(Line);
            builder.AppendLine("Número      : " + informacion.Error.Numero.Value);
            builder.AppendLine("Descripción : " + informacion.Error.Descripcion.Value);
            builder.AppendLine("Sugerencia : " + informacion.Error.Sugerencia.Value);

            s_errorMsg = builder.ToString();
            return timbre_resultado;
        }
        private string FormatInformationCancelacionMultiple(long idTransaccion, InformacionMultiple informacion, ProcessProviderResult providerResult)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(string.Empty);
            builder.AppendLine("RESULTADO");
            builder.AppendLine(Line);
            for (int i = 0; i < informacion.Uuids.Count; i++)
            {
                if (informacion.Uuids[i].EnviadoACancelar)
                    builder.AppendLine($"El UUID {informacion.Uuids[i].Uuid} fue marcado para su cancelación.");
                else
                {
                    builder.AppendLine(informacion.Uuids[i].Uuid);
                    builder.AppendLine("  - El UUID NO fue marcado para su cancelación");
                    builder.AppendLine("  - Mensaje: " + informacion.Uuids[i].Mensaje);
                }

                builder.AppendLine();
            }
            builder.AppendLine();

            if (idTransaccion != 0)
            {
                builder.AppendLine();
                builder.AppendLine("INFORMACION DE LA TRANSACCION");
                builder.AppendLine(Line);
                builder.AppendLine("Número de transacción         : " + idTransaccion);
            }

            if (informacion.Error.IsAssigned == false)
                return builder.ToString();

            this.FormatTypeError(builder, providerResult);

            builder.AppendLine("ERROR QUE SE GENERO EN EL PROCESO");
            builder.AppendLine(Line);
            builder.AppendLine("Número      : " + informacion.Error.Numero.Value);
            builder.AppendLine("Descripción : " + informacion.Error.Descripcion.Value);

            return builder.ToString();
        }

        private void UseOwnToken(BaseParameters parameters, bool useOwnToken, bool setDateTimeServer)
        {
            parameters.OwnToken = useOwnToken;

            if ((useOwnToken == false) || (setDateTimeServer == false))
                return;

            if (this.dateTimeServer == DateTime.MinValue)
            {
                HyperSoft.ElectronicDocumentLibrary.ECodex.FechaHora.Informacion informacion = new HyperSoft.ElectronicDocumentLibrary.ECodex.FechaHora.Informacion().Initialization();
                ProcessProviderResult result = this.proveedor.FechaHora(informacion);
                if (result == ProcessProviderResult.Ok)
                    this.dateTimeServer = informacion.FechaHora.Value;
                else
                {
                    parameters.OwnToken = false;
                    return;
                }

                this.dateLastToken = DateTime.Now;
            }
            else
            {
                int tiempoTranscurrido = (DateTime.Now - this.dateLastToken).Minutes;

                if (tiempoTranscurrido > 8)
                    this.dateTimeServer = this.dateTimeServer.AddMinutes(8);
            }

            parameters.DateTimeServer = this.dateTimeServer;
        }
    }
}
