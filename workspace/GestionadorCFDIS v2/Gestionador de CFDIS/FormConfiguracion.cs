using System;
using System.Drawing;
using System.Windows.Forms;
//para validar email
using System.Text.RegularExpressions;
using IniParser.Model;
using IniParser;
using System.Threading;
using System.IO;

namespace Gestionador_de_CFDIS
{
    public partial class FormConfiguracion : Form
    {
        CODIGO_UNICO_PC finger = new CODIGO_UNICO_PC();

        CONFIGURACIONES_INI configuracionIni;

        int seg;
        int min;
        int hor;

        public FormConfiguracion(CONFIGURACIONES_INI configuracionIni)
        {
            this.configuracionIni = configuracionIni;
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if ((txtUsuario.Text == string.Empty) || (txtUrl.Text == string.Empty) || (txtRuta.Text == string.Empty))
            {
                MESSAGEBOX_TEMPORAL.Show("No puedes continuar sin terminar el formulario!!", "Aviso", 10, true);
            }
            else
            {
                if (validarEmail(txtUsuario.Text))
                {
                    if (txtContrasena.Text == string.Empty)
                    {

                    }
                    else
                    {
                        //Encripta la contraseña
                        txtContrasena.Text = STV_FWK_CRYPTO.Encrypt(txtUsuario.Text, 1, txtContrasena.Text);
                    }

                    if (txtRuta.Text == string.Empty)
                    {
                        //
                    }
                    else
                    {
                        this.configuracionIni.FijarRutaDescargas(txtRuta.Text);
                    }

                    if (this.configuracionIni.FijarConfiguracion(txtUsuario.Text, txtContrasena.Text, txtUrl.Text))
                    {
                        Close();
                    }
                    else
                    {
                        /* Dejar el contenido de la pantalla */
                    }

                    this.configuracionIni.FijarUrlPrueba(txtUrl_prueba.Text);
                }
                else
                {
                    MESSAGEBOX_TEMPORAL.Show("Escribe un email válido", "Aviso", 10, true);
                }
            }
        }

        private void FormConfiguracion_Load(object sender, EventArgs e)
        {
            txtUrl.Select();

            txtCodigo.Text = this.finger.Value(CODIGO_UNICO_PC.fingerPrint);

            txtUrl.Text = this.configuracionIni.LeerUrl();
            txtUsuario.Text = this.configuracionIni.LeerUsuario();
            txtRuta.Text = this.configuracionIni.LeerDirectorio();

            this.min = 5;
            this.seg = 0;
            this.hor = 0;

            timer1.Start();
        }

        private void FormConfiguracion_FormClosed(object sender, FormClosedEventArgs e)
        {
            //Application.Exit();
        }

        private bool validarEmail(string email)
        {
            String expresion;
            expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (Regex.IsMatch(email, expresion))
            {
                if (Regex.Replace(email, expresion, String.Empty).Length == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            string minutos = min.ToString();
            string horas = hor.ToString();
            string segundos = seg.ToString();

            if (hor < 10) { horas = "0" + hor.ToString(); }
            if (min < 10) { minutos = "0" + min.ToString(); }
            if (seg < 10) { segundos = "0" + seg.ToString(); }

            if (seg == 0 && min > 0)
            {
                min -= 1;
                seg = 60;
            }
            if (min == 0 && hor > 0 && seg == 0)
            {
                seg = 60;
                hor -= 1;
                min = 59;
            }
            if (seg <= 10 && min == 0 && hor == 0)
            {
                if (lblTimer.Visible == true)
                {
                    lblTimer.Visible = false;
                    lblTimer.ForeColor = Color.Red;
                }
                else
                {
                    lblTimer.Visible = true;
                }
            }
            if (min == 0 && hor == 0 && seg == 0)
            {
                timer1.Stop();
                btnGuardar.PerformClick();
            }

            lblTimer.Text = horas + ":" + minutos + ":" + segundos;
            seg -= 1;
        }

    
        private void btnAbrir_Click(object sender, EventArgs e)
        {
            txtRuta.Text = SeleccionarRuta();
        }

        private string SeleccionarRuta()
        {
            string selectedPath = "";

            Thread t = new Thread((ThreadStart)(() =>
            {
                using (var fbd = new FolderBrowserDialog())
                {
                    DialogResult result = fbd.ShowDialog();

                    if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                    {
                        selectedPath = fbd.SelectedPath;
                    }
                }
            }));

            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            t.Join();

            return selectedPath;
        }
    }
}
//http://p01test.stverticales.mx/app_wbs/110gestionador/call/soap?WSDL
//test@stverticales.mx
//3{Q}Z.L3INs{
