﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gestionador_de_CFDIS
{
   public class Errores
    {
       public int ErroresWS(int error)
       {
           if (error == 0)
           {
               //MessageBox.Show("Webservice ejecutado sin errores", "Mensaje",MessageBoxButtons.OK, MessageBoxIcon.Information);
           }
           else if (error == 1)
           {
               MessageBox.Show("Se realizó la operación pero existen mensajes de warning", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
           }
           else if (error == 10)
           {
               MessageBox.Show("Webservice require información de login actualizada", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
           }
           else if (error == 11)
           {
               MessageBox.Show("Webservice require permisos que el usuario no tiene", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
           }
           else if (error == 12)
           {
               MessageBox.Show("Webservice contiene información inválida", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
           }
           else if (error == 13)
           {
               MessageBox.Show("Al ejecutar el webservice se encontraron condiciones incorrectas y no se pudo procesar", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
           }
           else if (error == 14)
           {
               MessageBox.Show("La ejecución del webservice require una secuencia que no se esta respetando", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
           }
           else if (error == 15)
           {
               MessageBox.Show("No se encontro error alguno, sin embargo el request no se pudo ejecutar. El request debe repetirse", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
           }
           else if (error == 16)
           {
               MessageBox.Show("La información contenida en el webservice se basa en información que fue actualizada", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
           }
           else if (error == 17)
           {
               MessageBox.Show("Se encontró algún error no detallado", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
           }
           return error;
       }
    }
}
