using Newtonsoft.Json;
using NLog;
using StvSoft;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;
using HyperSoft.ElectronicDocumentLibrary.Base;


namespace Gestionador_de_CFDIS
{
    public class STV_SERVICE
    {
        //instancia de O_codigoservidorPrint para sacar el código único por computadora
        CODIGO_UNICO_PC O_codigoservidor = new CODIGO_UNICO_PC();

        Acceso.STV_AccesoPortTypeClient O_client;
        public static Logger O_logger = LogManager.GetCurrentClassLogger();

        CONFIGURACIONES_INI O_configuracionIni;
        bool b_necesitaReinicio = false;

        public string s_directorioBase = "";
        public string s_rutaArchivoLicencia = string.Empty;
        public string s_rutaArchivoCer = string.Empty;
        public string s_rutaArchivoKey = string.Empty;
        public string s_rutaLicencias = string.Empty;
        public string s_rutaDescargados = string.Empty;
        public string s_rutaSubidos = string.Empty;
        public string s_rutaPendientes = string.Empty;
        public string s_rutaPruebas = string.Empty;
        public const string S_DIRECTORIODESCARGADOS = "CFDIs_descargados";
        public const string S_DIRECTORIOSUBIDOS = "CFDIs_subidos";
        public const string S_DIRECTORIOPENDIENTES = "CFDIs_pendientes";
        public const string S_DIRECTORIOLICENCIAS = "Licencia";

        private string strCarpetaRFC = string.Empty;

        public string S_DIRECTORIODOMINIO = "";

        public Acceso.loginResponseD_auth loginAuth = null;

        List<INSTRUCCION> L_instrucciones;

        private string WBS_s_msgError = string.Empty;
        private string WBS_json_params = string.Empty;
        private string WBS_json_data = string.Empty;

        //Declare the delegate
        public delegate void Al_ActualizarCFDIs_Delegate(INSTRUCCION_ACTUALIZAR_CFDIS.E_ESTADO E_estado, int? Total = 0, int? Procesados = 0, D_RETURN D_returnUltimoProcesado = null);
        //Declare an event
        public event Al_ActualizarCFDIs_Delegate Al_ActualizarCFDIsEvent;

        public string s_urlUsado = string.Empty;
        private string S_URL_PRODUCCION = string.Empty;
        private string S_URL_TEST = string.Empty;

        bool b_ultimoURLUsadoFueProduccion = false;

        public STV_SERVICE()
        {
            this.L_instrucciones = new List<INSTRUCCION>();
            this.O_client = new Acceso.STV_AccesoPortTypeClient();
        }
        public void Configurar(ref CONFIGURACIONES_INI O_configuracionIni)
        {
            this.O_configuracionIni = O_configuracionIni;

            S_URL_PRODUCCION = Obtener_Uri(this.O_configuracionIni.LeerUrl());
            S_URL_TEST = Obtener_Uri(this.O_configuracionIni.LeerUrlPrueba());

            this.O_client = new Acceso.STV_AccesoPortTypeClient();
            if (!b_ultimoURLUsadoFueProduccion)
            {
                this.O_client.Endpoint.Address = new System.ServiceModel.EndpointAddress(this.O_configuracionIni.LeerUrl());
                b_ultimoURLUsadoFueProduccion = true;
            }
            else if (S_URL_TEST != string.Empty)
            {
                this.O_client.Endpoint.Address = new System.ServiceModel.EndpointAddress(this.O_configuracionIni.LeerUrlPrueba());
                b_ultimoURLUsadoFueProduccion = false;
            }
            else
            {
                this.O_client.Endpoint.Address = new System.ServiceModel.EndpointAddress(this.O_configuracionIni.LeerUrl());
                b_ultimoURLUsadoFueProduccion = true;
            }
            this.s_urlUsado = this.O_client.Endpoint.Address.Uri.Host;
            this.b_necesitaReinicio = false;

            s_directorioBase = O_configuracionIni.LeerDirectorio();
        }
        //se obtiene el host de la url
        private string Obtener_Uri(string url)
        {
            string retorno = string.Empty;
            if (url != string.Empty)
            {
                Uri Uri = new Uri(url);
                retorno = Uri.Host;
            }
            else { }

            return retorno;
        }

        public D_RETURN ConectarWS()
        {
            D_RETURN _D_return = new D_RETURN()
            {
                E_return = ERRORES_WS.OK,
                s_msgError = string.Empty,
                s_proceso = "Conectar WebService"
            };
            try
            {
                //VerificarCrearCarpetasTrabajo_paraRFC(3, "ALG040428RG4");
                //string _s_json = "{\"TipoCambio\": \"1.0000\", \"Folio\": \"1049649\", \"Version\": \"3.3\", \"Descuento\": \"0.0000\", \"LugarExpedicion\": \"82134\", \"Emisor\": {\"Rfc\": \"ALG-040428-RG4\", \"Nombre\": \"ACUMULADORES Y LLANTAS GALLARDO SA DE CV\", \"RegimenFiscal\": \"601\"}, \"Impuestos\": {\"Trasladados\": [{\"TipoFactor\": \"Tasa\", \"TasaOCuota\": \"0.1600\", \"Impuesto\": \"002\", \"Importe\": \"52.8000\"}], \"Retenidos\": []}, \"MetodoPago\": \"PPD\", \"Serie\": \"C\", \"Conceptos\": [{\"ClaveProdServ\": \"11141608\", \"Cantidad\": \"1.0000\", \"ClaveUnidad\": \"H87\", \"Descuento\": \"0.0000\", \"ValorUnitario\": \"330.0000\", \"Impuestos\": {\"Trasladados\": [{\"TipoFactor\": \"Tasa\", \"Base\": \"330.0000\", \"TasaOCuota\": \"0.1600\", \"Impuesto\": \"002\", \"Importe\": \"52.8000\"}], \"Retenidos\": []}, \"NoIdentificacion\": \"GRUPO 3\", \"Descripcion\": \"GRUPO 3\", \"Unidad\": \"Pieza\", \"Importe\": \"330.0000\"}], \"Moneda\": \"MXN\", \"TipoDeComprobante\": \"I\", \"FormaPago\": \"99\", \"Receptor\": {\"Rfc\": \"CLI-931207-AAA\", \"Nombre\": \"Cliente de Prueba Raz\u00f3n Social \u00e1\u00f1\", \"UsoCFDI\": \"G01\"}, \"Total\": \"352.8000\", \"SubTotal\": \"300.000\", \"Fecha\": \"2021-06-07T11:10:00\"}";
                //_s_json = _s_json.Replace("\"Base\"", "\"Basei\"");
                //OPCIONES_CFDI timbrado = new OPCIONES_CFDI("ALG040428RG4", this.s_rutaDescargados, this.s_rutaArchivoCer, this.s_rutaArchivoKey, "Optima86");
                //string _s_resultado;
                //timbrado.TimbrarCfdi(ref _s_json, out _s_resultado);

                string _s_codigo = this.O_codigoservidor.Value(CODIGO_UNICO_PC.fingerPrint);

                string _s_password = STV_FWK_CRYPTO.Decrypt(this.O_configuracionIni.LeerUsuario(), 1, this.O_configuracionIni.LeerPass());

                WBS_s_msgError = string.Empty;
                WBS_json_params = string.Empty;
                WBS_json_data = string.Empty;

                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                _D_return.E_return = (ERRORES_WS)this.O_client.login(
                    _s_codigo,
                    WBS_json_params,
                    this.O_configuracionIni.LeerUsuario(),
                    _s_password,
                    out this.loginAuth,
                    out WBS_json_data,
                    out WBS_s_msgError
                    );
                _D_return.s_msgError = WBS_s_msgError;

                if (_D_return.E_return > ERRORES_WS.OK_WARNING)
                {
                    //O_logger.Error(string.Format("Conectar WebService - " + "Error: {0}", _mensaje));
                    O_logger.Error(_D_return.mensajeParaLog());
                    this.O_configuracionIni.FijaUltimoProblema(_D_return);
                }
                else
                {
                    O_logger.Info(_D_return.mensajeParaLog());

                    dynamic _json = JsonConvert.DeserializeObject(WBS_json_data);

                    foreach (var _json_instruccion in _json.L_instrucciones)
                    {
                        INSTRUCCION instruccion = new INSTRUCCION()
                        {
                            E_tipoinstruccion = _json_instruccion.E_tipoinstruccion,
                            Param0 = _json_instruccion.s_param0,
                            Param1 = _json_instruccion.s_param1,
                            Param2 = _json_instruccion.s_param2,
                            Param3 = _json_instruccion.s_param3,
                            Param4 = _json_instruccion.dt_param4 == null ? new DateTime(2000, 1, 1, 0, 0, 0) : Convert.ToDateTime(_json_instruccion.dt_param4),
                            Param5 = _json_instruccion.dt_param5 == null ? new DateTime(2000, 1, 1, 0, 0, 0) : Convert.ToDateTime(_json_instruccion.dt_param5),
                            Param6 = _json_instruccion.s_param6,
                            Param7 = _json_instruccion.s_param7,
                            IdInstruccion = _json_instruccion.s_id,
                        };
                        this.L_instrucciones.Add(instruccion);
                    }
                }
            }
            catch (Exception ex)
            {
                _D_return.E_return = ERRORES_WS.NOK_CONEXION;
                _D_return.s_msgError = string.Format("{0}", ex.Message);
                O_logger.Error(_D_return.mensajeParaLog());
                this.O_configuracionIni.FijaUltimoProblema(_D_return);
                //MESSAGEBOX_TEMPORAL.Show(ex.Message, "Aviso", 15, true);
            }
            return _D_return;
        }//Termina conectarWS

        private void DesconectarWS()
        {
            D_RETURN _D_return = new D_RETURN()
            {
                E_return = ERRORES_WS.OK,
                s_msgError = string.Empty,
                s_proceso = "Desconectar WebService"
            };
            if (this.loginAuth == null)
            {
                return;
            }
            else
            {
                Acceso.logoutD_auth _logoutAuth = new Acceso.logoutD_auth()
                {
                    json_key = this.loginAuth.json_key,
                    s_signature = this.loginAuth.s_signature,
                    s_ver = this.loginAuth.s_ver,
                    s_token = this.loginAuth.s_token,
                    n_empresa_id = this.loginAuth.n_empresa_id,
                };

                WBS_s_msgError = string.Empty;
                WBS_json_params = string.Empty;
                WBS_json_data = string.Empty;

                _D_return.E_return = (ERRORES_WS)this.O_client.logout(
                    WBS_json_params,
                    _logoutAuth,
                    out WBS_json_data,
                    out WBS_s_msgError
                    );
                _D_return.s_msgError = WBS_s_msgError;
                O_logger.Info(_D_return.mensajeParaLog());

                this.loginAuth = null;
            }

        }//Termina desconetarWS

        private D_RETURN Procesar_Instruccion_SubirCFDI(int empresa_id, int idInstruccion, string rfc, string s_nombreArchivo)
        {
            D_RETURN _D_return = new D_RETURN()
            {
                E_return = ERRORES_WS.OK,
                s_msgError = string.Empty,
                s_proceso = "Subir CFDI " + System.IO.Path.GetFileName(s_nombreArchivo)
            };

            try
            {
                Acceso.subir_cfdiD_auth _subirAuth = new Acceso.subir_cfdiD_auth()
                {
                    json_key = this.loginAuth.json_key,
                    s_signature = this.loginAuth.s_signature,
                    s_ver = this.loginAuth.s_ver,
                    s_token = this.loginAuth.s_token,
                    n_empresa_id = empresa_id,
                };

                string _s_xml = System.IO.File.ReadAllText(s_nombreArchivo, Encoding.UTF8);

                WBS_s_msgError = string.Empty;
                WBS_json_params = string.Empty;
                WBS_json_data = string.Empty;
             
                _D_return.E_return = (ERRORES_WS)this.O_client.subir_cfdi(
                  _s_xml,
                  _subirAuth,
                  empresa_id.ToString(),
                  idInstruccion.ToString(),
                  rfc,
                  WBS_json_params,
                  out WBS_json_data,
                  out WBS_s_msgError
                );

                _D_return.s_msgError = WBS_s_msgError;

                if (_D_return.E_return == ERRORES_WS.NOK_INFORMACION_ACTUALIZADA)
                {
                    O_logger.Warn(_D_return.mensajeParaLog());
                }
                else if (_D_return.E_return > ERRORES_WS.OK_WARNING)
                {
                    O_logger.Error(_D_return.mensajeParaLog());
                }
                else
                {
                    O_logger.Info("Subir cfdis WebService - " + "El archivo fue cargado al portal : " + s_nombreArchivo);

                    //asignamos el último xml subido
                    this.O_configuracionIni.FijaUltimaSubidaExitosa(System.IO.Path.GetFileName(s_nombreArchivo));
                }
            }
            catch (Exception ex)
            {
                _D_return.E_return = ERRORES_WS.NOK_ERROR;
                _D_return.s_msgError = string.Format("El archivo no fue cargado al portal: {0}. Error: {1}", s_nombreArchivo, ex.Message);
                O_logger.Error(_D_return.mensajeParaLog());
            }

            return _D_return;
        }//Termina subir subirCFDIWs

        private D_RETURN Procesar_Instruccion_TimbrarCFDI(INSTRUCCION_TIMBRAR_CFDI instruccion)
        {
            D_RETURN _D_return = new D_RETURN()
            {
                E_return = ERRORES_WS.OK,
                s_msgError = string.Empty,
                s_proceso = "TimbrarCFDI " + instruccion.cfdi_id
            };
            instruccion.InicioInstruccion = DateTime.Now;
            try
            {
                _ = Verificar_CarpetasTrabajoRFC(instruccion.cuenta_id, instruccion.rfc.Replace("-", ""));

                WebClient _O_webClient = new WebClient();

                string _s_url = instruccion.url_json + "&s_token=" + loginAuth.s_token;

                string _s_cfdi_json = _O_webClient.DownloadString(_s_url);

                OPCIONES_CFDI timbrado = new OPCIONES_CFDI(
                    instruccion.rfc.Replace("-", ""),
                    this.s_rutaDescargados,
                    this.s_rutaArchivoCer,
                    this.s_rutaArchivoKey,
                    "Optima86",//instruccion.pass
                    (this.s_urlUsado != S_URL_PRODUCCION)
                    );
                string _s_resultado;
                string _s_rutaArchivo;

                CFDI_TIMBRE_RESULTADO _D_resultado = timbrado.TimbrarCfdi(ref _s_cfdi_json, out _s_resultado, out _s_rutaArchivo);

                instruccion.Estatus.O_resultado = _D_resultado;
                instruccion.Estatus.Mensaje = _s_resultado;
                _D_return.s_msgError = _D_resultado.error_proceso.s_descripcion;

                if (
                    (_D_resultado.error_proceso.E_proveedorerror == (int)ProcessProviderResult.Ok)
                    || (_D_resultado.error_proceso.E_proveedorerror == 0)
                    )
                {
                    _D_return.E_return = ERRORES_WS.OK;

                    D_RETURN _D_result = Procesar_Instruccion_SubirCFDI(
                        instruccion.empresa_id,
                        instruccion.IdInstruccion,
                        instruccion.rfc,
                        _s_rutaArchivo
                        );

                    _D_return.E_return = _D_result.E_return;
                    _D_return.s_msgError = _D_result.s_msgError;

                }
                else
                {
                    _D_return.E_return = ERRORES_WS.NOK_CONDICIONES_INCORRECTAS;
                }

                //_D_return.E_return = _D_result.E_return;
                //_D_return.s_msgError = _D_result.s_msgError;
            }
            catch (Exception ex)
            {
                _D_return.E_return = ERRORES_WS.NOK_DATOS_INVALIDOS;
                _D_return.s_msgError = string.Format("{0}", ex.Message);
                instruccion.Estatus.O_resultado = _D_return;
                O_logger.Error(_D_return.mensajeParaLog());
                this.O_configuracionIni.FijaUltimoProblema(_D_return);
            }
            instruccion.FinInstruccion = DateTime.Now;
            instruccion.Estatus.SegundosEjecucion = (instruccion.FinInstruccion - instruccion.InicioInstruccion).TotalSeconds;
            this.Responder_Instruccion(instruccion, _D_return, instruccion.Leer_Estatus_JSON());

            if (_D_return.E_return == ERRORES_WS.OK)
            {
                this.b_necesitaReinicio = true;
            }
            else { }
            return _D_return;


        }//Termina TimbrarCFDI

        private D_RETURN Procesar_Instruccion_CancelarCFDI(INSTRUCCION_CANCELAR_CFDI instruccion)
        {
            D_RETURN _D_return = new D_RETURN()
            {
                E_return = ERRORES_WS.OK,
                s_msgError = string.Empty,
                s_proceso = "CancelarCFDI " + instruccion.cfdi_id
            };
            instruccion.InicioInstruccion = DateTime.Now;
            try
            {
                _ = Verificar_CarpetasTrabajoRFC(instruccion.cuenta_id, instruccion.rfc.Replace("-", ""));

                OPCIONES_CFDI timbrado = new OPCIONES_CFDI(
                    instruccion.rfc.Replace("-", ""),
                    this.s_rutaDescargados,
                    this.s_rutaArchivoCer,
                    this.s_rutaArchivoKey,
                    "Optima86",//instruccion.pass
                    (this.s_urlUsado != S_URL_PRODUCCION)
                    );

                string _s_resultado;
                string _s_uuid = instruccion.cfdi_uuid;
                CFDI_TIMBRE_RESULTADO _D_resultado = timbrado.CancelarCfdi(ref _s_uuid, out _s_resultado);

                instruccion.Estatus.O_resultado = _D_resultado;
                instruccion.Estatus.Mensaje = _s_resultado;
                _D_return.s_msgError = _D_resultado.error_proceso.s_descripcion;

                if (
                    (_D_resultado.error_proceso.E_proveedorerror == (int)ProcessProviderResult.Ok)
                    || (_D_resultado.error_proceso.E_proveedorerror == 0)
                    || (this.s_urlUsado == S_URL_TEST)
                    )
                {
                    if (_D_resultado.b_enviadoACancelar)
                    {
                        _D_return.E_return = ERRORES_WS.OK;
                    }
                    else
                    {
                        _D_return.E_return = ERRORES_WS.OK_WARNING;
                    }
                }
                else
                {
                    _D_return.E_return = ERRORES_WS.NOK_CONDICIONES_INCORRECTAS;
                }
            }
            catch (Exception ex)
            {
                _D_return.E_return = ERRORES_WS.NOK_DATOS_INVALIDOS;
                _D_return.s_msgError = string.Format("{0}", ex.Message);
                O_logger.Error(_D_return.mensajeParaLog());
                this.O_configuracionIni.FijaUltimoProblema(_D_return);
            }
            finally
            {
                instruccion.FinInstruccion = DateTime.Now;
                instruccion.Estatus.SegundosEjecucion = (instruccion.FinInstruccion - instruccion.InicioInstruccion).TotalSeconds;
            }
            
            this.Responder_Instruccion(instruccion, _D_return, instruccion.Leer_Estatus_JSON());

            if (_D_return.E_return == ERRORES_WS.OK)
            {
                this.b_necesitaReinicio = true;
            }
            else { }

            return _D_return;

        }//Termina CancelarCFDI

        private string Obtener_RutaArchivoLicencia(string s_rutaLicencias, string s_extension)
        {
            /* Regresará cadena vacía si no puede actualizar licencias, de lo contrario regresa el path completo a donde se encuentra la licencia */
            List<string> _L_archivosFechas = new List<string>();

            string _s_nombreArchivo = string.Empty;

            DirectoryInfo _directorio = new DirectoryInfo(s_rutaLicencias);
            FileInfo[] _L_archivosLicencia = _directorio.GetFiles("*" + s_extension);

            //obtenemos la fecha de creación de los archivos y la agregamos a una lista
            for (int i = 0; i < _L_archivosLicencia.Length; i++)
            {
                _L_archivosFechas.Add(_L_archivosLicencia[i].CreationTime.ToString("yyyy-MM-dd"));
            }

            string _fecha_mayor = _L_archivosFechas.Max();

            foreach (FileInfo _archivo in _L_archivosLicencia)
            {
                string _fecha_archivo = _archivo.CreationTime.ToString("yyyy-MM-dd");

                if (_fecha_mayor == _fecha_archivo)
                {
                    _s_nombreArchivo = _archivo.FullName;
                    break;
                }
                else { }
            }

            return _s_nombreArchivo;

        }//Termina Obtener_RutaArchivoLicencia

        private D_RETURN Procesar_Instruccion_ActualizarCFDIs_Emitidos(INSTRUCCION_ACTUALIZAR_CFDIS instruccion)
         {
            D_RETURN _D_return = new D_RETURN()
            {
                E_return = ERRORES_WS.OK,
                s_msgError = string.Empty,
                s_proceso = "Descargar CFDIs Instruccion " + instruccion.IdInstruccion.ToString()
            };
            int cantidad_bajados = 0;
            instruccion.InicioInstruccion = DateTime.Now;

            O_logger.Info("DescargarCFDI: Inicia el proceso de descarga de cfdis emitidos para la instrucción id=" + instruccion.IdInstruccion.ToString() + " instruccion=" + instruccion.E_tipoinstruccion.ToString());

            string strRfc = instruccion.Rfc.Replace("-", "");
            string strCiec = STV_FWK_CRYPTO.Decrypt(instruccion.Rfc, 1, instruccion.ClaveCiec);

            //se agrega la sección con el rfc
            this.O_configuracionIni.CrearSeccionRFC(instruccion.CuentaId, strRfc);

            Al_ActualizarCFDIsEvent(INSTRUCCION_ACTUALIZAR_CFDIS.E_ESTADO.CONFIGURANDO_LICENCIA);

            this.Verificar_CarpetasTrabajoRFC(instruccion.CuentaId, strRfc);

            if (this.s_rutaArchivoLicencia != string.Empty)
            {
                //existe licencia
                try
                {
                    HyperSoft.ElectronicDocument.Download.Parameters.Tipo _descarga_emitidos = HyperSoft.ElectronicDocument.Download.Parameters.Tipo.Emitidos;

                    HyperSoft.ElectronicDocument.Download.Status _statusBajar = HyperSoft.ElectronicDocument.Download.Status.All;
                    switch (instruccion.E_tipoinstruccion)
                    {
                        case INSTRUCCION.E_TIPOINSTRUCCION.ACTUALIZAR_CFDIS_EMITIDOS:
                            _statusBajar = HyperSoft.ElectronicDocument.Download.Status.All;
                            break;
                        case INSTRUCCION.E_TIPOINSTRUCCION.ACTUALIZAR_CFDIS_EMITIDOS_CANCELADOS:
                            _statusBajar = HyperSoft.ElectronicDocument.Download.Status.Cancelado;
                            break;
                        default:
                            _statusBajar = HyperSoft.ElectronicDocument.Download.Status.All;
                            break;
                    }

                    DESCARGA_CFDI descargaEmitidos = new DESCARGA_CFDI(
                        instruccion.Rfc_integrador,
                        instruccion.Id_integrador,
                        strRfc,
                        strCiec,
                        instruccion.Fecha_desde,
                        instruccion.Fecha_hasta,
                        this.s_rutaDescargados,
                        instruccion.CuentaId,
                        _statusBajar,
                        _descarga_emitidos
                        );

                    string _s_result_error = descargaEmitidos.CargarLicencia(this.s_rutaArchivoLicencia);
                    if (_s_result_error != string.Empty)
                    {
                        _D_return.E_return = ((int)(INSTRUCCION.E_TIPOINSTRUCCION.ACTUALIZAR_LICENCIA) * 100) + ERRORES_WS.NOK_ERROR;
                        _D_return.s_msgError = _s_result_error;
                        O_logger.Error(_D_return.mensajeParaLog());
                        O_configuracionIni.FijaUltimoProblema(_D_return);
                    }
                    else
                    {
                        Al_ActualizarCFDIsEvent(INSTRUCCION_ACTUALIZAR_CFDIS.E_ESTADO.BAJANDO_DEL_SAT);


                        _s_result_error = descargaEmitidos.Descargar(ref cantidad_bajados);

                        if (_s_result_error != string.Empty)
                        {
                            _D_return.E_return = ((int)(INSTRUCCION.E_TIPOINSTRUCCION.ACTUALIZAR_LIBRERIA_CFDIS) * 100) + ERRORES_WS.NOK_ERROR;
                            _D_return.s_msgError = _D_return.E_return.ToString() + ":" + _s_result_error + "|";
                            O_logger.Error(_D_return.mensajeParaLog());
                            O_configuracionIni.FijaUltimoProblema(_D_return);

                            O_configuracionIni.GuardarProblemasRFC(instruccion.CuentaId, strRfc);
                        }
                        else
                        {
                            O_logger.Info("Archivos bajados sin novedad.");
                        }

                        D_RETURN _D_result = this.Procesar_Instruccion_SubirArchivosXML(ref instruccion);
                        _D_return.E_return = _D_result.E_return;
                        _D_return.s_msgError += _D_return.E_return.ToString() + ":" + _D_result.s_msgError;
                        _D_return.n_procesados = _D_result.n_procesados;
                    }
                }
                catch (Exception ex)
                {
                    _D_return.E_return = ((int)(INSTRUCCION.E_TIPOINSTRUCCION.ACTUALIZAR_LIBRERIA_CFDIS) * 100) + ERRORES_WS.NOK_ERROR;
                    _D_return.s_msgError = "Descarga cfdi Emitidos-- " + ex.ToString();
                    O_logger.Error(_D_return.mensajeParaLog());
                    O_configuracionIni.FijaUltimoProblema(_D_return);
                }
            }
            else
            {
                _D_return.E_return = ((int)(INSTRUCCION.E_TIPOINSTRUCCION.ACTUALIZAR_LIBRERIA_CFDIS) * 100) + ERRORES_WS.NOK_ERROR;
                _D_return.s_msgError = "NOK_ERROR_LICENCIA, no se pudo encontrar la licencia para el RFC: " + strRfc;
                O_logger.Error(_D_return.mensajeParaLog());
                O_configuracionIni.FijaUltimoProblema(_D_return);

                O_configuracionIni.GuardarProblemasRFC(instruccion.CuentaId, strRfc);

            }
            instruccion.Estatus.CantidadDeBajados = cantidad_bajados;
            instruccion.FinInstruccion = DateTime.Now;
            instruccion.Estatus.SegundosEjecucion = (instruccion.FinInstruccion - instruccion.InicioInstruccion).TotalSeconds;
            this.Responder_Instruccion(instruccion, _D_return, instruccion.Leer_Estatus_JSON());
            return _D_return;

        }//Termina descargarCFDI_Emitidos

        private D_RETURN Procesar_Instruccion_ActualizarCFDIs_Recibidos(INSTRUCCION_ACTUALIZAR_CFDIS instruccion)
        {
            D_RETURN _D_return = new D_RETURN()
            {
                E_return = ERRORES_WS.OK,
                s_msgError = string.Empty,
                s_proceso = "Descargar CFDIs Instruccion " + instruccion.IdInstruccion.ToString()
            };
            int cantidad_bajados = 0;
            instruccion.InicioInstruccion = DateTime.Now;

            O_logger.Info("DescargarCFDI: Inicia el proceso de descarga de cfdis recibidos para la instrucción id=" + instruccion.IdInstruccion.ToString() + " instruccion=" + instruccion.E_tipoinstruccion.ToString());

            string strRfc = instruccion.Rfc.Replace("-", "");
            string strCiec = STV_FWK_CRYPTO.Decrypt(instruccion.Rfc, 1, instruccion.ClaveCiec);

            //se agrega la sección con el rfc
            this.O_configuracionIni.CrearSeccionRFC(instruccion.CuentaId, strRfc);

            Al_ActualizarCFDIsEvent(INSTRUCCION_ACTUALIZAR_CFDIS.E_ESTADO.CONFIGURANDO_LICENCIA);

            this.Verificar_CarpetasTrabajoRFC(instruccion.CuentaId, strRfc);

            if (this.s_rutaArchivoLicencia != string.Empty)
            {
                //existe licencia
                try
                {
                    HyperSoft.ElectronicDocument.Download.Parameters.Tipo _descarga_recibidos = HyperSoft.ElectronicDocument.Download.Parameters.Tipo.Recibidos;

                    HyperSoft.ElectronicDocument.Download.Status _statusBajar = HyperSoft.ElectronicDocument.Download.Status.All;
                    switch (instruccion.E_tipoinstruccion)
                    {
                        case INSTRUCCION.E_TIPOINSTRUCCION.ACTUALIZAR_CFDIS_RECIBIDOS:
                            _statusBajar = HyperSoft.ElectronicDocument.Download.Status.All;
                            break;
                        case INSTRUCCION.E_TIPOINSTRUCCION.ACTUALIZAR_CFDIS_RECIBIDOS_CANCELADOS:
                            _statusBajar = HyperSoft.ElectronicDocument.Download.Status.Cancelado;
                            break;
                        default:
                            _statusBajar = HyperSoft.ElectronicDocument.Download.Status.All;
                            break;
                    }

                    DESCARGA_CFDI descargaRecibidos = new DESCARGA_CFDI(
                        instruccion.Rfc_integrador,
                        instruccion.Id_integrador,
                        strRfc,
                        strCiec,
                        instruccion.Fecha_desde,
                        instruccion.Fecha_hasta,
                        this.s_rutaDescargados,
                        instruccion.CuentaId,
                        _statusBajar,
                        _descarga_recibidos
                        );

                    string _s_result_error = descargaRecibidos.CargarLicencia(this.s_rutaArchivoLicencia);
                    if (_s_result_error != string.Empty)
                    {
                        _D_return.E_return = ((int)(INSTRUCCION.E_TIPOINSTRUCCION.ACTUALIZAR_LICENCIA) * 100) + ERRORES_WS.NOK_ERROR;
                        _D_return.s_msgError = _s_result_error;
                        O_logger.Error(_D_return.mensajeParaLog());
                        O_configuracionIni.FijaUltimoProblema(_D_return);
                    }
                    else
                    {
                        Al_ActualizarCFDIsEvent(INSTRUCCION_ACTUALIZAR_CFDIS.E_ESTADO.BAJANDO_DEL_SAT);


                        _s_result_error = descargaRecibidos.Descargar(ref cantidad_bajados);

                        if (_s_result_error != string.Empty)
                        {
                            _D_return.E_return = ((int)(INSTRUCCION.E_TIPOINSTRUCCION.ACTUALIZAR_LIBRERIA_CFDIS) * 100) + ERRORES_WS.NOK_ERROR;
                            _D_return.s_msgError = _D_return.E_return.ToString() + ":" + _s_result_error + "|";
                            O_logger.Error(_D_return.mensajeParaLog());
                            O_configuracionIni.FijaUltimoProblema(_D_return);

                            O_configuracionIni.GuardarProblemasRFC(instruccion.CuentaId, strRfc);
                        }
                        else
                        {
                            O_logger.Info("Archivos bajados sin novedad.");
                        }

                        D_RETURN _D_result = this.Procesar_Instruccion_SubirArchivosXML(ref instruccion);
                        _D_return.E_return = _D_result.E_return;
                        _D_return.s_msgError += _D_return.E_return.ToString() + ":" + _D_result.s_msgError;
                        _D_return.n_procesados = _D_result.n_procesados;
                    }
                }
                catch (Exception ex)
                {
                    _D_return.E_return = ((int)(INSTRUCCION.E_TIPOINSTRUCCION.ACTUALIZAR_LIBRERIA_CFDIS) * 100) + ERRORES_WS.NOK_ERROR;
                    _D_return.s_msgError = "Descarga cfdi Recibidos -- " + ex.ToString();
                    O_logger.Error(_D_return.mensajeParaLog());
                    O_configuracionIni.FijaUltimoProblema(_D_return);
                }
            }
            else
            {
                _D_return.E_return = ((int)(INSTRUCCION.E_TIPOINSTRUCCION.ACTUALIZAR_LIBRERIA_CFDIS) * 100) + ERRORES_WS.NOK_ERROR;
                _D_return.s_msgError = "NOK_ERROR_LICENCIA, no se pudo encontrar la licencia para el RFC: " + strRfc;
                O_logger.Error(_D_return.mensajeParaLog());
                O_configuracionIni.FijaUltimoProblema(_D_return);

                O_configuracionIni.GuardarProblemasRFC(instruccion.CuentaId, strRfc);

            }
            instruccion.Estatus.CantidadDeBajados = cantidad_bajados;
            instruccion.FinInstruccion = DateTime.Now;
            instruccion.Estatus.SegundosEjecucion = (instruccion.FinInstruccion - instruccion.InicioInstruccion).TotalSeconds;
            this.Responder_Instruccion(instruccion, _D_return, instruccion.Leer_Estatus_JSON());
            return _D_return;

        }//Termina descargarCFDI_Recibidos

        private List<string> Obtener_ListaArchivosXml(ref INSTRUCCION_ACTUALIZAR_CFDIS instruccion)
        {
            List<string> _L_archivos = new List<string>();

            string[] _L_subdirectiorios = Directory.GetDirectories(this.s_rutaDescargados);

            foreach (var _subdirectorio in _L_subdirectiorios)
            {
                DirectoryInfo _info_directiorio = new DirectoryInfo(_subdirectorio);
                FileInfo[] _L_archivosEnSubdirectorio;

                switch (instruccion.E_tipoinstruccion)
                {
                    case INSTRUCCION.E_TIPOINSTRUCCION.ACTUALIZAR_CFDIS_RECIBIDOS:
                    case INSTRUCCION.E_TIPOINSTRUCCION.ACTUALIZAR_CFDIS_RECIBIDOS_CANCELADOS:
                        _L_archivosEnSubdirectorio = _info_directiorio.GetFiles("R_*.xml");
                        break;
                    case INSTRUCCION.E_TIPOINSTRUCCION.ACTUALIZAR_CFDIS_EMITIDOS:
                    case INSTRUCCION.E_TIPOINSTRUCCION.ACTUALIZAR_CFDIS_EMITIDOS_CANCELADOS:
                        _L_archivosEnSubdirectorio = _info_directiorio.GetFiles("E_*.xml");
                        break;
                    default:
                        _L_archivosEnSubdirectorio = _info_directiorio.GetFiles("*.xml");
                        break;
                }

                if (_L_archivosEnSubdirectorio.Length == 0)
                {
                    O_logger.Info("Leer archivos xml - " + "No hay ningún archivo .xml en la carpeta: " + _subdirectorio);
                    //si la carpeta esta vacía se elimina
                    Directory.Delete(_subdirectorio, true);
                }
                else
                {
                    foreach (FileInfo _archivo in _L_archivosEnSubdirectorio)
                    {
                        if (Verificar_XML(_archivo.FullName))
                        {
                            //el xml ya fue subido antes.
                            File.Delete(_archivo.FullName);
                            O_logger.Info("El xml ya fue subido anteriormente - " + _archivo.FullName);
                        }
                       else
                        {
                            //lo añadimos a la lista para subirlo.
                           _L_archivos.Add(_archivo.FullName);
                        }       
                    }
                }
                //string s_directoriotest = string.format("{0}\\{1}", this.s_directoriobase, s_url_test);
                //string s_directorioproduccion = string.format("{0}\\{1}", this.s_directoriobase, s_url_produccion);

                //directorycopy(s_directorioproduccion, s_directoriotest, true);

            }
  
            return _L_archivos;
        }//Termina leerArchivosXml

        private static void DirectoryCopy(string dir_origen, string dir_destino, bool copiar_subdir)
        {
            DirectoryInfo dir = new DirectoryInfo(dir_origen);

            DirectoryInfo[] dirs = dir.GetDirectories();

            if(!Directory.Exists(dir_destino))
            {
                Directory.CreateDirectory(dir_destino);
            }
           
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string tempPath = Path.Combine(dir_destino, file.Name);
                if (!File.Exists(tempPath))
                {
                    file.CopyTo(tempPath);
                }
                else { }
               
            }
            if (copiar_subdir)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string tempPath = Path.Combine(dir_destino, subdir.Name);
                    DirectoryCopy(subdir.FullName, tempPath, copiar_subdir);
                }
            }
        }

        private bool Verificar_XML(string xml)
        {
            bool _n_return = false;
         
            string[] files = Directory.GetFiles(this.s_rutaSubidos, Path.GetFileName(xml) + "*", SearchOption.AllDirectories);

            if (files.Length > 0)
            {
                foreach (string xml_subido in files)
                {
                    if (File.ReadAllText(xml) == File.ReadAllText(xml_subido))
                    {
                        _n_return = true;
                    }
                    else
                    {
                        _n_return = false;
                    }
                }
            }
            else
            {
                _n_return = false;
            }
            return _n_return;
        }

        private D_RETURN Procesar_Instruccion_SubirArchivosXML(ref INSTRUCCION_ACTUALIZAR_CFDIS instruccion)
        {
            D_RETURN _D_return = new D_RETURN()
            {
                E_return = ERRORES_WS.OK,
                s_msgError = string.Empty,
                s_proceso = "Subir CFDIs Instruccion " + instruccion.IdInstruccion.ToString(),
                n_procesados = 0
            };
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string _s_rfc = instruccion.Rfc.Replace("-", "");

                O_logger.Info("Subir archivos xml -- " + "Inicia el proceso de subida de XML del RFC: " + _s_rfc);

                List<string> _L_archivosXML = Obtener_ListaArchivosXml(ref instruccion);

                Al_ActualizarCFDIsEvent(INSTRUCCION_ACTUALIZAR_CFDIS.E_ESTADO.SUBIENDO_CFDIS, _L_archivosXML.Count, 0);

                string _s_rutaSubidos_porFecha;
                string _s_rutaPendientes_porFecha;
                string _s_carpetaFecha;

                _D_return.n_procesados = 0;

                foreach (string _s_archivo in _L_archivosXML)
                {
                    D_RETURN _D_result = Procesar_Instruccion_SubirCFDI(
                        instruccion.EmpresaId, 
                        instruccion.IdInstruccion, 
                        instruccion.Rfc, 
                        _s_archivo
                        );

                    instruccion.Estatus.CantidadDeCambios += 1;
                    if (instruccion.Estatus.CodigosRecibidos.ContainsKey(_D_result.E_return))
                    {
                        instruccion.Estatus.CodigosRecibidos[_D_result.E_return] += 1;
                    }
                    else
                    {
                        instruccion.Estatus.CodigosRecibidos.Add(_D_result.E_return, 1);
                    }

                    _D_return.n_procesados++;
                    Al_ActualizarCFDIsEvent(INSTRUCCION_ACTUALIZAR_CFDIS.E_ESTADO.SUBIENDO_CFDIS, _L_archivosXML.Count, _D_return.n_procesados, _D_result);

                    if (_D_result.E_return <= ERRORES_WS.OK_WARNING)
                    {
                        instruccion.Estatus.CantidadProcesados += 1;
                        //mover a carpeta de CFDIs subidos
                        if (File.Exists(_s_archivo))
                        {
                            try
                            {
                                _s_carpetaFecha = Path.GetFileName(Path.GetDirectoryName(_s_archivo));
                                //string nombre_cfdi = Path.GetFileName(cfdi.rutaArchivo);

                                _s_rutaSubidos_porFecha = string.Format("{0}\\{1}\\{2}", this.s_rutaSubidos, _s_carpetaFecha, _D_result.E_return.ToString());
                                if (!Directory.Exists(_s_rutaSubidos_porFecha))
                                {
                                    Directory.CreateDirectory(_s_rutaSubidos_porFecha);
                                }
                                else { }

                                string _s_destino = _s_rutaSubidos_porFecha + "\\" + Path.GetFileName(_s_archivo);
                                int _n_consecutivo = 1;
                                while (File.Exists(_s_destino))
                                {
                                    _s_destino = _s_rutaSubidos_porFecha + "\\" + Path.GetFileName(_s_archivo) + ".duplicado" + _n_consecutivo.ToString();
                                    _n_consecutivo++;
                                }

                                File.Move(_s_archivo, _s_destino);
                                File.Delete(_s_archivo);

                            }
                            catch (Exception ex)
                            {
                                _D_return.E_return = ERRORES_WS.NOK_ERROR;
                                _D_return.s_msgError = "NOK_ERROR, no se pudo mover archivo a Subidos: " + _s_archivo + string.Format(" - {0}", ex.ToString());
                                O_logger.Error(_D_return.mensajeParaLog());
                                O_configuracionIni.FijaUltimoProblema(_D_return);
                            }
                        }
                        else
                        { }
                    }
                    else
                    {
                        instruccion.Estatus.CantidadPendientes += 1;
                        //mover a carpeta de CFDIs pendientes
                        if (File.Exists(_s_archivo))
                        {
                            try
                            {
                                _s_carpetaFecha = Path.GetFileName(Path.GetDirectoryName(_s_archivo));
                                //string nombre_cfdi = Path.GetFileName(cfdi.rutaArchivo);

                                _s_rutaPendientes_porFecha = string.Format("{0}\\{1}\\{2}", this.s_rutaPendientes, _s_carpetaFecha, _D_result.E_return.ToString());
                                if (!Directory.Exists(_s_rutaPendientes_porFecha))
                                {
                                    Directory.CreateDirectory(_s_rutaPendientes_porFecha);
                                }
                                else { }

                                string _s_destino = _s_rutaPendientes_porFecha + "\\" + Path.GetFileName(_s_archivo);
                                int _n_consecutivo = 1;
                                while (File.Exists(_s_destino))
                                {
                                    _s_destino = _s_rutaPendientes_porFecha + "\\" + Path.GetFileName(_s_archivo) + ".duplicado" + _n_consecutivo.ToString();
                                    _n_consecutivo++;
                                }

                                File.Move(_s_archivo, _s_rutaPendientes_porFecha + "\\" + Path.GetFileName(_s_archivo));
                                File.Delete(_s_archivo);

                            }
                            catch (Exception ex)
                            {
                                _D_return.E_return = ERRORES_WS.NOK_ERROR;
                                _D_return.s_msgError = "NOK_ERROR, no se pudo mover archivo a Pendientes: " + _s_archivo + string.Format(" - {0}", ex.ToString());
                                O_logger.Error(_D_return.mensajeParaLog());
                                O_configuracionIni.FijaUltimoProblema(_D_return);
                            }
                        }
                        else
                        { }

                        _D_return.E_return = _D_result.E_return;
                        _D_return.s_msgError = _D_result.s_msgError;
                        this.O_configuracionIni.FijaUltimoProblema(_D_return);
                        O_logger.Error(_D_return.mensajeParaLog());

                        // Esta linea esta dando problemas, por favor primero prueba en local y luego nos vamos al server. Aqui es produccion ya no podemos estar probando aqui
                        //O_configuracionIni.ListaCfdiProblemas(instruccion.CuentaId, _s_rfc, instruccion.IdInstruccion, instruccion.E_tipoinstruccion, Path.GetFileName(_s_archivo), _D_return.E_return, _D_return.s_msgError);
                        O_configuracionIni.GuardarProblemasRFC(instruccion.CuentaId, _s_rfc);
                    }
                }
                O_logger.Info("Subir archivos xml - " + "Fin de subida de XML del RFC: " + _s_rfc);
            }
            catch (Exception ex)
            {
                _D_return.E_return = ERRORES_WS.NOK_ERROR;
                _D_return.s_msgError = string.Format("{0}", ex.ToString());
                O_logger.Error(_D_return.mensajeParaLog());
                O_configuracionIni.FijaUltimoProblema(_D_return);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
            return _D_return;
        }//Termina subirArchivosXML

        private string Verificar_CarpetasTrabajoRFC(int cuenta_id, string rfc)
        {
            //Crear carpeta del url producción
            strCarpetaRFC = string.Format("{0}\\{1}\\{2}", this.s_directorioBase, this.s_urlUsado, cuenta_id.ToString() + "_" + rfc);
            if (!Directory.Exists(strCarpetaRFC))
            {
                Directory.CreateDirectory(strCarpetaRFC);
            }
            else { }

            //Crear carpeta para la licencia
            this.s_rutaArchivoLicencia = "";
            this.s_rutaArchivoCer = "";
            this.s_rutaArchivoKey = "";
            this.s_rutaLicencias = string.Format("{0}\\{1}", strCarpetaRFC, STV_SERVICE.S_DIRECTORIOLICENCIAS);
            if (!Directory.Exists(this.s_rutaLicencias))
            {
                Directory.CreateDirectory(this.s_rutaLicencias);
            }
            else
            {
                this.s_rutaArchivoLicencia = this.Obtener_RutaArchivoLicencia(this.s_rutaLicencias, ".license");
                this.s_rutaArchivoCer = this.Obtener_RutaArchivoLicencia(this.s_rutaLicencias, ".cer");
                this.s_rutaArchivoKey = this.Obtener_RutaArchivoLicencia(this.s_rutaLicencias, ".key");
            }

            //Se crea la carpeta para guardar CFDIs descargados
            this.s_rutaDescargados = string.Format("{0}\\{1}", strCarpetaRFC, STV_SERVICE.S_DIRECTORIODESCARGADOS);
            if (!Directory.Exists(this.s_rutaDescargados))
            {
                Directory.CreateDirectory(this.s_rutaDescargados);
            }
            //Se crea la carpeta para guardar CFDIs subidos
            this.s_rutaSubidos = string.Format("{0}\\{1}", strCarpetaRFC, STV_SERVICE.S_DIRECTORIOSUBIDOS);
            if (!Directory.Exists(this.s_rutaSubidos))
            {
                Directory.CreateDirectory(this.s_rutaSubidos);
            }
            //Se crea la carpeta para guardar CFDIs pendientes
            this.s_rutaPendientes = string.Format("{0}\\{1}", strCarpetaRFC, STV_SERVICE.S_DIRECTORIOPENDIENTES);
            if (!Directory.Exists(this.s_rutaPendientes))
            {
                Directory.CreateDirectory(this.s_rutaPendientes);
            }   
            return strCarpetaRFC;
        }

        private D_RETURN Procesar_Instruccion_TimbrarCFDI_Traslado(INSTRUCCION_TIMBRAR_CFDI instruccion)
        {
            D_RETURN _D_return = new D_RETURN()
            {
                E_return = ERRORES_WS.OK,
                s_msgError = string.Empty,
                s_proceso = "Timbrar CFDI de traslado: " + instruccion.IdInstruccion.ToString()
            };

            try
            {
                //_ = Verificar_CarpetasTrabajoRFC(instruccion.cuenta_id, instruccion.rfc);
                //WebClient _O_webClient = new WebClient();
                //string _s_url = instruccion.url_json + "&s_token=" + loginAuth.s_token;
                //string _s_cfdi_json = _O_webClient.DownloadString(_s_url);
            }
            catch(Exception ex)
            {
                O_logger.Error("Error: " + ex.Message);
            }

            //Código duro usado para pruebas, eliminar en producción.
            Verificar_CarpetasTrabajoRFC(3, "ALG040428RG4");

            string _ruta_archivos = string.Format("{0}\\{1}", this.s_directorioBase, this.s_urlUsado);
            string s_json = _ruta_archivos + "\\JSONTraspaso.json";

            COMPLEMENTO trasladado = new COMPLEMENTO(strCarpetaRFC, _ruta_archivos);
            string fileName = string.Empty;
            trasladado.CartaPorte10(out fileName, ref s_json);
            return _D_return;
        }

        private void BorrarCarpetas(INSTRUCCION_BORRAR_CFDIS_SUBIDOS instruccion)
        {
            D_RETURN _D_return = new D_RETURN()
            {
                E_return = ERRORES_WS.OK,
                s_msgError = string.Empty,
                s_proceso = "Borrar Carpeta instrucción " + instruccion.IdInstruccion.ToString()
            };
            string strRfc = instruccion.rfc.Replace("-", "");

            //Es el nombre de la carpeta a borrar (CFDIs_descargados, CFDIs_subidos).
            string s_param3 = instruccion.carpeta;

            string _strBorrarRFC = string.Format("{0}\\{1}", s_directorioBase, instruccion.cuenta_id + "_" + strRfc);
            string _strBorrarCarpeta = string.Format("{0}\\{1}\\{2}", s_directorioBase, instruccion.cuenta_id + "_" + strRfc, s_param3);

            try
            {
                if (s_param3 == "todas")
                {
                    Directory.Delete(_strBorrarRFC, true);
                }
                else
                {
                    Directory.Delete(_strBorrarCarpeta, true);
                }
            }
            catch (Exception ex)
            {
                O_logger.Error("Borrar carpetas :" + ex.Message);
            }

            Responder_Instruccion(instruccion, _D_return);

        }//Fin borrarCarpetas

        private void Procesar_Instruccion_ActualizarLicencia(INSTRUCCION_ACTUALIZAR_ARCHIVO instruccion)
        {
            D_RETURN _D_return = new D_RETURN()
            {
                E_return = ERRORES_WS.OK,
                s_msgError = string.Empty,
                s_proceso = "Actualizar Licencia instrucción " + instruccion.IdInstruccion.ToString()
            };
            if(Existe_URL(instruccion.url + "&s_token=" + this.loginAuth.s_token))
            {
                string _s_archivoNombre = "";
                string _s_archivoTipo = "";
                string _strRfc = instruccion.rfc.Replace("-", "");
                this.Verificar_CarpetasTrabajoRFC(instruccion.cuenta_id, _strRfc);

                if (instruccion.E_tipoinstruccion == INSTRUCCION.E_TIPOINSTRUCCION.ACTUALIZAR_LICENCIA)
                {
                    _s_archivoNombre = "_licencia";
                    _s_archivoTipo = ".license";
                }
                else if (instruccion.E_tipoinstruccion == INSTRUCCION.E_TIPOINSTRUCCION.ACTUALIZAR_CER)
                {
                    _s_archivoNombre = "_certificado";
                    _s_archivoTipo = ".cer";
                }
                else if (instruccion.E_tipoinstruccion == INSTRUCCION.E_TIPOINSTRUCCION.ACTUALIZAR_KEY)
                {
                    _s_archivoNombre = "_firma";
                    _s_archivoTipo = ".key";
                }
                else
                {
                    _s_archivoNombre = "_error_" + instruccion.E_tipoinstruccion.ToString();
                    _s_archivoTipo = ".error";
                }

                //se asigna la fecha al nombre de la licencia
                string _nombre_licencia = DateTime.Today.ToString("yyyy-MM-dd");
                _nombre_licencia = _nombre_licencia.Replace("-", "_");
                _nombre_licencia += _s_archivoNombre;

                if(File.Exists(this.s_rutaLicencias + "//" + _nombre_licencia + _s_archivoTipo))
                {
                    int _n_duplicado = 1;
                    _nombre_licencia = _nombre_licencia + "_duplicado" + _n_duplicado.ToString();
                    while(File.Exists(this.s_rutaLicencias + "//" + _nombre_licencia + _s_archivoTipo))
                    {
                        _n_duplicado++;
                        _nombre_licencia = _nombre_licencia + "_duplicado" + _n_duplicado.ToString();
                    }
                    _nombre_licencia += ".license";
                }
                else
                {
                    _nombre_licencia += ".license";
                }

                //Descarga la licencia
                WebClient webClient = new WebClient();
                try
                { 
                    webClient.DownloadFile(instruccion.url + "&s_token=" + this.loginAuth.s_token, this.s_rutaLicencias + "//" + _nombre_licencia);
                    //TODO leer archivo, si es texto, convertirlo de JSON y obtener el codigo de error y mostrarlo

                    var _jsonText = File.ReadAllText(this.s_rutaLicencias + "//" + _nombre_licencia);
                    if (_jsonText.StartsWith("{"))
                    {
                        dynamic _json_serialize = JsonConvert.DeserializeObject(_jsonText);

                        _D_return.E_return = _json_serialize.E_return;
                        _D_return.s_msgError = _json_serialize.s_msgError;
                        O_logger.Error("Actualizar Licencia - Cod:" + _D_return.E_return +" - " + _D_return.s_msgError);
                    }
                    else { }
                }
                catch (Exception ex)
                {
                    _D_return.E_return = ERRORES_WS.NOK_ERROR;
                    _D_return.s_msgError = "NOK_ERROR, no se pudo descargar la licencia para el rfc: " + _strRfc;
                    O_logger.Error(_D_return.E_return + ": " + _D_return.s_msgError, ex.Message);

                    O_configuracionIni.FijaUltimoProblema(_D_return);
                }
            }
            else
            {
                _D_return.E_return = ERRORES_WS.NOK_ERROR;
                _D_return.s_msgError = "la url no es válida!";
                O_logger.Error(_D_return.E_return + ": " + _D_return.s_msgError);
            }

            _ = Responder_Instruccion(instruccion, _D_return, instruccion.Leer_Estatus_JSON());
        }

        private void Procesar_Instruccion_StatusServidor(INSTRUCCION_ESTATUS_SERVIDOR instruccion)
        {
            ESTATUS_SERVIDOR O_estatus = new ESTATUS_SERVIDOR(O_configuracionIni);
            D_RETURN _D_return = new D_RETURN()
            {
                E_return = ERRORES_WS.OK,
                s_msgError = string.Empty,
                s_proceso = "Estatus del servidor " + instruccion.IdInstruccion.ToString(),
            };

            string json_send = O_estatus.serializar_estatus_servidor();

            _ = Responder_Instruccion(instruccion, _D_return, json_send);
        }

        private bool Existe_URL(string url)
        {
            try
            {
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                request.Method = "HEAD";
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                response.Close();
                return (response.StatusCode == HttpStatusCode.OK);
            }
            catch(Exception ex)
            {
                O_logger.Error("URL: " + ex.Message);
                return false;
            }
        }

        private D_RETURN Responder_Instruccion(INSTRUCCION instruccion, D_RETURN D_return, string Json_params = "")
        {
            D_RETURN _D_return = new D_RETURN()
            {
                E_return = ERRORES_WS.OK,
                s_msgError = string.Empty,
                s_proceso = "Responder Instruccion " + instruccion.IdInstruccion.ToString() + " con codigo " + D_return.E_return.ToString()
            };

            try
            {
                Acceso.responder_instruccionD_auth _responderAuth = new Acceso.responder_instruccionD_auth()
                {
                    json_key = this.loginAuth.json_key,
                    s_signature = this.loginAuth.s_signature,
                    s_ver = this.loginAuth.s_ver,
                    s_token = this.loginAuth.s_token,
                    n_empresa_id = this.loginAuth.n_empresa_id,
                };

                WBS_s_msgError = string.Empty;
                WBS_json_params = string.Empty;
                WBS_json_data = string.Empty;

                _D_return.E_return = (ERRORES_WS)this.O_client.responder_instruccion(
                    instruccion.IdInstruccion.ToString(),
                    ((int)D_return.E_return).ToString(),
                    D_return.s_msgError,
                    Json_params,
                    _responderAuth,
                    out WBS_json_data,
                    out WBS_s_msgError
                    );
                _D_return.s_msgError = WBS_s_msgError;

                if (_D_return.E_return > ERRORES_WS.OK_WARNING)
                {
                    O_logger.Error(_D_return.mensajeParaLog());
                }
                else
                {
                    O_logger.Info(_D_return.mensajeParaLog());
                }
            }
            catch (Exception ex)
            {
                _D_return.E_return = ERRORES_WS.NOK_ERROR;
                _D_return.s_msgError = string.Format("Error: {0}", ex.Message);
                O_logger.Error(_D_return.mensajeParaLog());
            }

            return _D_return;
        }//Termina ResponderInstruccion

        public bool ProcesandoInstruccion()
        {
            bool _procesar;
            if (loginAuth == null)
            {
                _procesar = false;
            }
            else
            {
                _procesar = true;
            }

            return _procesar;
        }

        public INSTRUCCION Jalar_SiguineteInstrucccion()
        {
            if (this.L_instrucciones.Count == 0)
            {
                this.DesconectarWS();
                return null;
            }
            else
            {
                INSTRUCCION tmpInstruccion = this.L_instrucciones.First();
                /* Debugear si toma la instruccion y si al momento de borrar no la borra */
                this.L_instrucciones.RemoveAt(0);
                return tmpInstruccion;
            }
        }

        private D_RETURN Procesar_Instruccion_Configurar(ref INSTRUCCION instruccion)
        {
            D_RETURN _D_return = new D_RETURN()
            {
                E_return = ERRORES_WS.OK,
                s_msgError = string.Empty,
                s_proceso = "Procesar Instruccion " + instruccion.IdInstruccion.ToString()
            };
            instruccion.InicioInstruccion = DateTime.Now;
            try
            {

                D_RETURN _D_result = new D_RETURN();
                switch (instruccion.E_tipoinstruccion)
                {
                    case INSTRUCCION.E_TIPOINSTRUCCION.CONFIGURAR_URL:
                        _D_result = O_configuracionIni.FijarUrl((new INSTRUCCION_CONFIGURAR(instruccion)).dato);
                        break;
                    case INSTRUCCION.E_TIPOINSTRUCCION.CONFIGURAR_EMAIL:
                        _D_result = O_configuracionIni.FijarUsuario((new INSTRUCCION_CONFIGURAR(instruccion)).dato);
                        break;
                    case INSTRUCCION.E_TIPOINSTRUCCION.CONFIGURAR_PASS:
                        _D_result = O_configuracionIni.FijarPass((new INSTRUCCION_CONFIGURAR(instruccion)).dato);
                        break;
                    case INSTRUCCION.E_TIPOINSTRUCCION.CONFIGURAR_FRECUENCIA_MIN:
                        INSTRUCCION_CONFIGURAR instruccionConfigurar = new INSTRUCCION_CONFIGURAR(instruccion);
                        _D_result = O_configuracionIni.FijarFrecuencia(Convert.ToInt32(instruccionConfigurar.dato));
                        break;
                    default:
                        _D_result.E_return = ERRORES_WS.NOK_ERROR;
                        _D_result.s_msgError = "Tipo de instrucción no corresponde a configurar " + ((int)instruccion.E_tipoinstruccion).ToString();
                        break;
                }

                _D_return.E_return = _D_result.E_return;
                _D_return.s_msgError = _D_result.s_msgError;
            }
            catch (Exception ex)
            {
                _D_return.E_return = ERRORES_WS.NOK_DATOS_INVALIDOS;
                _D_return.s_msgError = string.Format("{0}", ex.Message);
                O_logger.Error(_D_return.mensajeParaLog());
                this.O_configuracionIni.FijaUltimoProblema(_D_return);
            }
            instruccion.FinInstruccion = DateTime.Now;
            instruccion.Estatus.SegundosEjecucion = (instruccion.FinInstruccion - instruccion.InicioInstruccion).TotalSeconds;
            this.Responder_Instruccion(instruccion, _D_return, instruccion.Leer_Estatus_JSON());

            if (_D_return.E_return == ERRORES_WS.OK)
            {
                this.b_necesitaReinicio = true;
            }
            else { }
            return _D_return;
        }

        public D_RETURN Procesar_Instruccion(INSTRUCCION instruccion)
        {
            D_RETURN _D_return = new D_RETURN()
            {
                E_return = ERRORES_WS.OK,
                s_msgError = string.Empty,
                s_proceso = "Procesar Instruccion " + instruccion.IdInstruccion.ToString(),
                b_flag = false  /* Usada para determinar si se requiere o no reinicar el proceso */
            };
            Cursor.Current = Cursors.WaitCursor;

            switch(instruccion.E_tipoinstruccion)
            {
                case INSTRUCCION.E_TIPOINSTRUCCION.NADA:
                    this.Responder_Instruccion(instruccion, _D_return);
                    break;
                case INSTRUCCION.E_TIPOINSTRUCCION.ACTUALIZAR_CFDIS_EMITIDOS:
                case INSTRUCCION.E_TIPOINSTRUCCION.ACTUALIZAR_CFDIS_EMITIDOS_CANCELADOS:
                    _D_return = this.Procesar_Instruccion_ActualizarCFDIs_Emitidos(new INSTRUCCION_ACTUALIZAR_CFDIS(instruccion));
                    break;
                case INSTRUCCION.E_TIPOINSTRUCCION.ACTUALIZAR_CFDIS_RECIBIDOS:
                case INSTRUCCION.E_TIPOINSTRUCCION.ACTUALIZAR_CFDIS_RECIBIDOS_CANCELADOS:
                    _D_return = this.Procesar_Instruccion_ActualizarCFDIs_Recibidos(new INSTRUCCION_ACTUALIZAR_CFDIS(instruccion));
                    break;
                case INSTRUCCION.E_TIPOINSTRUCCION.BORRAR_CFDIS_SUBIDOS:
                    break;
                case INSTRUCCION.E_TIPOINSTRUCCION.ACTUALIZAR_LICENCIA:
                    this.Procesar_Instruccion_ActualizarLicencia(new INSTRUCCION_ACTUALIZAR_ARCHIVO(instruccion));
                        break;
                case INSTRUCCION.E_TIPOINSTRUCCION.ACTUALIZAR_LIBRERIA_CFDIS:
                case INSTRUCCION.E_TIPOINSTRUCCION.ACTUALIZAR_CER:
                case INSTRUCCION.E_TIPOINSTRUCCION.ACTUALIZAR_KEY:
                case INSTRUCCION.E_TIPOINSTRUCCION.ACTUALIZAR_GESTIONADOR:
                    /* Si estas instrucciones regresan OK, se requiere reiniciar this.b_necesitaReinicio = true */
                    break;
                case INSTRUCCION.E_TIPOINSTRUCCION.CONFIGURAR_URL:
                case INSTRUCCION.E_TIPOINSTRUCCION.CONFIGURAR_EMAIL:
                case INSTRUCCION.E_TIPOINSTRUCCION.CONFIGURAR_PASS:
                case INSTRUCCION.E_TIPOINSTRUCCION.CONFIGURAR_FRECUENCIA_MIN:
                    this.Procesar_Instruccion_Configurar(ref instruccion);
                    break;
                case INSTRUCCION.E_TIPOINSTRUCCION.ESTATUS_SERVIDOR:
                    this.Procesar_Instruccion_StatusServidor(new INSTRUCCION_ESTATUS_SERVIDOR(instruccion));
                    break;
                case INSTRUCCION.E_TIPOINSTRUCCION.TIMBRAR_CFDI:
                    if (this.s_urlUsado == S_URL_TEST)
                    {
                        //Solo va a poder timbrar si el url usado es diferente al del.ini
                        this.Procesar_Instruccion_TimbrarCFDI(new INSTRUCCION_TIMBRAR_CFDI(instruccion));
                    }
                    else { }
                    break;
                case INSTRUCCION.E_TIPOINSTRUCCION.CANCELAR_CFDI:
                    if (this.s_urlUsado == S_URL_TEST)
                    {
                        // Solo va a poder cancelar si el url usado es diferente al del .ini
                        this.Procesar_Instruccion_CancelarCFDI(new INSTRUCCION_CANCELAR_CFDI(instruccion));
                    }
                    else { }
                    break;
                case INSTRUCCION.E_TIPOINSTRUCCION.TIMBRAR_CFDI_TRASLADO:
                    if (this.s_urlUsado == S_URL_TEST)
                    {
                        // Solo va a poder timbrar el cfdi de traslado si el url usado es diferente al del .ini
                        this.Procesar_Instruccion_TimbrarCFDI_Traslado(new INSTRUCCION_TIMBRAR_CFDI(instruccion));
                    }
                    else { }
                    break;
                default:
                    /* TODO Finalizar instruccion con error de datos invalidos */
                    /* TODO Log error */
                    _D_return.E_return = ERRORES_WS.NOK_DATOS_INVALIDOS;
                    break;
            }

            Cursor.Current = Cursors.Default;
            _D_return.b_flag = this.b_necesitaReinicio;
            return _D_return;
        }

    }
}