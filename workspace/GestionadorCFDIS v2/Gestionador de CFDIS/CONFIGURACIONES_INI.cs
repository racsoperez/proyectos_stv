using System;
using System.Collections.Generic;
using System.IO;
//Paquetes para la lectura y modificación del iniparser  documentación: https://github.com/rickyah/ini-parser/wiki/First-Steps
using IniParser;
using IniParser.Model;
using Newtonsoft.Json;
using NLog;


namespace Gestionador_de_CFDIS
{
   
    public class CONFIGURACIONES_INI
    {
        private string _pathArchivo;
        private double _frecuencia_min = 10.0;
        private string _url = string.Empty;
        private string _usuario = string.Empty;
        private string _pass = string.Empty;
        private string _ultimo_problema = string.Empty;
        private string _ultimo_problema_codigo = string.Empty;
        private string _ultimo_problema_fechahora = string.Empty;
        private string _ultima_subida_exitosa = string.Empty;
        private string _ultima_cfdis = string.Empty;
        private string _total_subidos = string.Empty;
        private string _directorio_base = string.Empty;
        private string _url_pruebas = string.Empty;

        //es el nombre de la sección en el .ini
        private string _nombreSeccionConfiguracion = "gestionador_configuracion";
        private string _nombreSeccionRFC = "";

        private const string NOMBRE_INI = "config.ini";
        private const string NOMBRE_INI_BACKUP = "config.ini.bkp";
        private const string NOMBRE_INI_ERROR = "config.ini.error";

        public const string CONFIG_KEY_URL = "url";
        public const string CONFIG_KEY_USUARIO = "usuario";
        public const string CONFIG_KEY_PASS = "pass";
        public const string CONFIG_KEY_FRECUENCIA_MIN = "frecuencia_min";
        public const string CONFIG_KEY_ULTIMO_PROBLEMA = "ultimo_problema";
        public const string CONFIG_KEY_ULTIMO_PROBLEMA_CODIGO = "ultimo_problema_codigo";
        public const string CONFIG_KEY_ULTIMO_PROBLEMA_FECHAHORA = "ultimo_problema_fechahora";
        public const string CONFIG_KEY_ULTIMA_SUBIDA_EXISTOSA = "ultima_subida_exitosa";
        public const string CONFIG_KEY_ULTIMO_CFDI = "ultima_cfdis";
        public const string CONFIG_KEY_TOTAL_SUBIDOS = "total_subidos";
        public const string CONFIG_KEY_DIRECOTRIO_BASE = "directorio_base";
        public const string CONFIG_KEY_URL_PRUEBAS = "url_pruebas";
        

        //http://p01test.stverticales.mx/app_wbs/110gestionador/call/soap?WSDL

        public static Logger logger = LogManager.GetCurrentClassLogger();

        public CONFIGURACIONES_INI()
        {
            
        }

        private bool _InsertaCuandoNoExista(KeyDataCollection seccion, string s_llave, string s_valorDefault)
        {
            bool _b_return = false;
            if (!seccion.ContainsKey(s_llave))
            {
                _b_return = seccion.AddKey(s_llave, s_valorDefault);
            }
            else 
            {
                _b_return = true;
            }
            return _b_return;
        }

        public void Configurar()
        {
            /* Crear archivo en caso de no existir */

            this._pathArchivo = System.AppDomain.CurrentDomain.BaseDirectory + NOMBRE_INI;

            // verifica si está creado el archivo .ini
            if (!File.Exists(this._pathArchivo))
            {
                StreamWriter sw = new StreamWriter(this._pathArchivo);
                sw.Close();

            } else
            {
                // No hacer nada, el archivo ya existe
            }

            var parser = new FileIniDataParser();
            IniData data = parser.ReadFile(this._pathArchivo);

            if (data.Sections.ContainsSection(this._nombreSeccionConfiguracion))
            {
                // No hacer nada, la seccion ya existe
            }
            else
            {
                //se agrega la sección
                data.Sections.AddSection(this._nombreSeccionConfiguracion);
            }

            KeyDataCollection seccionDatos = data.Sections[this._nombreSeccionConfiguracion];

            this._InsertaCuandoNoExista(seccionDatos, CONFIGURACIONES_INI.CONFIG_KEY_URL, string.Empty);
            this._InsertaCuandoNoExista(seccionDatos, CONFIGURACIONES_INI.CONFIG_KEY_USUARIO, string.Empty);
            this._InsertaCuandoNoExista(seccionDatos, CONFIGURACIONES_INI.CONFIG_KEY_PASS, string.Empty);
            this._InsertaCuandoNoExista(seccionDatos, CONFIGURACIONES_INI.CONFIG_KEY_FRECUENCIA_MIN, "10.0");
            this._InsertaCuandoNoExista(seccionDatos, CONFIGURACIONES_INI.CONFIG_KEY_ULTIMO_PROBLEMA, string.Empty);
            this._InsertaCuandoNoExista(seccionDatos, CONFIGURACIONES_INI.CONFIG_KEY_ULTIMO_PROBLEMA_CODIGO, string.Empty);
            this._InsertaCuandoNoExista(seccionDatos, CONFIGURACIONES_INI.CONFIG_KEY_ULTIMO_PROBLEMA_FECHAHORA, string.Empty);
            this._InsertaCuandoNoExista(seccionDatos, CONFIGURACIONES_INI.CONFIG_KEY_ULTIMA_SUBIDA_EXISTOSA, string.Empty);
            this._InsertaCuandoNoExista(seccionDatos, CONFIGURACIONES_INI.CONFIG_KEY_ULTIMO_CFDI, string.Empty);
            this._InsertaCuandoNoExista(seccionDatos, CONFIGURACIONES_INI.CONFIG_KEY_TOTAL_SUBIDOS, string.Empty);
            this._InsertaCuandoNoExista(seccionDatos, CONFIGURACIONES_INI.CONFIG_KEY_DIRECOTRIO_BASE, "C:\\descarga_xml");
            this._InsertaCuandoNoExista(seccionDatos, CONFIGURACIONES_INI.CONFIG_KEY_URL_PRUEBAS, string.Empty);

            //guardamos los datos en el .ini
            parser.WriteFile(NOMBRE_INI, data);

        }

        public bool VerificarConfiguracionValida()
        {
            bool _return = false;

            try
            {
                var parser = new FileIniDataParser();
                IniData data = parser.ReadFile(NOMBRE_INI);

                KeyDataCollection seccionGestionador = data[this._nombreSeccionConfiguracion];

                this._frecuencia_min = Convert.ToDouble(seccionGestionador["frecuencia_min"]);

                //verifica si la frecuencia esta entre 1 y 60 minutos para poder ejecutarse
                //si no, guardar frecuencia de 10 minutos
                if ((this._frecuencia_min < 1.0) || (this._frecuencia_min > 60.0))
                {
                    this.FijarFrecuencia(20.0);
                }
                else
                {
                    /* El valor es en rango válido y se mantiene */
                }

                if (seccionGestionador.Count < 10)
                {
                    _return = false;
                }
                else if (
                    (seccionGestionador["usuario"] == string.Empty)
                    || (seccionGestionador["pass"] == string.Empty)
                    || (seccionGestionador["url"] == string.Empty)
                    )
                {
                    _return = false;
                }
                else
                {
                    this._url = seccionGestionador["url"];
                    this._usuario = seccionGestionador["usuario"];
                    this._pass = seccionGestionador["pass"];
                    this._ultimo_problema = seccionGestionador["ultimo_problema"];
                    this._ultimo_problema_codigo = seccionGestionador["ultimo_problema_codigo"];
                    this._ultimo_problema_fechahora = seccionGestionador["ultimo_problema_fechahora"];
                    this._ultima_subida_exitosa = seccionGestionador["ultima_subida_exitosa"];
                    this._ultima_cfdis = seccionGestionador["ultima_cfdis"];
                    this._total_subidos = seccionGestionador["total_subidos"];
                    this._directorio_base = seccionGestionador["directorio_base"];
                    this._url_pruebas = seccionGestionador["url_pruebas"];

                    _return = true;
                }

                if (!Directory.Exists(_directorio_base))
                {
                    _return = false;
                }
                else
                {
                    _return = true;
                }
            }
            catch (Exception)
            {
                if (File.Exists(NOMBRE_INI_BACKUP))
                {
                    File.Delete(NOMBRE_INI_ERROR);
                    File.Move(NOMBRE_INI, NOMBRE_INI_ERROR);
                    File.Delete(NOMBRE_INI);
                    File.Copy(NOMBRE_INI_BACKUP, NOMBRE_INI);
                }
                else
                {
                    File.Delete(NOMBRE_INI_ERROR);
                    File.Move(NOMBRE_INI, NOMBRE_INI_ERROR);
                    File.Delete(NOMBRE_INI);
                    this.Configurar();
                    _return = false;
                }
            }
            //se asigna la frecuencia del .ini

            return _return;
        }

        public double LeerFrecuenciaMin()
        {
            double _f_frecuencia = 10.0;
            if (this._url_pruebas != string.Empty)
            {
                _f_frecuencia = this._frecuencia_min / 2.0;
            }
            else
            {
                _f_frecuencia = this._frecuencia_min;
            }
            return this._frecuencia_min;
        }
        public string LeerUsuario()
        {
            return this._usuario;
        }
        public string LeerPass()
        {
            return this._pass;
        }
        public string LeerUrl()
        {
            return this._url;
        }
        public string LeerDirectorio()
        {
            return this._directorio_base;
        }
        public string LeerUrlPrueba()
        {
            return this._url_pruebas;
        }

        public bool FijarConfiguracion(string usuario, string pass, string url)
        {
            bool _return;
            /* TODO: Agregar validacion para email en el usuario */
            if (
                (usuario == string.Empty)
                || (url == string.Empty)
                )
            {
                _return = false;
            }
            else if (
                (usuario == this._usuario)
                && ( (pass == this._pass) || (pass == string.Empty) )
                && (url == this._url)
                )
            {
                /* No hacer nada, ya que nada cambio */
                _return = true;
            }
            else
            {
                this._usuario = usuario;
                if (pass == string.Empty)
                {
                    /* No hacer nada, si no se actualiza no se graba la contrasenia */
                }
                else
                {
                    this._pass = pass;
                }
                this._url = url;
                _return = true;
                this.GuardarConfiguracionINI(true);
            }
            
            return _return;
        }

        public D_RETURN FijarUsuario(string usuario)
        {
            D_RETURN _D_return = new D_RETURN()
            {
                E_return = ERRORES_WS.OK,
                s_msgError = string.Empty
            };
            if (usuario == string.Empty)
            {
                _D_return.E_return = ERRORES_WS.NOK_DATOS_INVALIDOS;
                _D_return.s_msgError = "Usuario es inválido";
            }
            else if (usuario == this._usuario)
            {
                _D_return.E_return = ERRORES_WS.OK_WARNING;
                _D_return.s_msgError = "Usuario actualizado o intentan grabar el mismo";
            }
            else
            {
                // TODO Verificar login en servidor, sin actualizar instrucciones. Si es correcta continuar, si no, mandar error.

                this._usuario = usuario;
                this.GuardarConfiguracionINI(true);
            }
            return _D_return;
        }

        public D_RETURN FijarPass(string pass)
        {
            D_RETURN _D_return = new D_RETURN()
            {
                E_return = ERRORES_WS.OK,
                s_msgError = string.Empty
            };
            if (pass == string.Empty)
            {
                _D_return.E_return = ERRORES_WS.NOK_DATOS_INVALIDOS;
                _D_return.s_msgError = "Pass es inválido";
            }
            else if (pass == STV_FWK_CRYPTO.Decrypt(this._usuario, 1, this._pass))
            {
                _D_return.E_return = ERRORES_WS.OK_WARNING;
                _D_return.s_msgError = "Pass actualizado o intenta grabar el mismo";
            }
            else
            {
                // TODO Verificar login en servidor, sin actualizar instrucciones. Si es correcta continuar, si no, mandar error.
                this._pass = STV_FWK_CRYPTO.Encrypt(this._usuario, 1, pass);
                this.GuardarConfiguracionINI(true);
            }
            return _D_return;
        }

        public D_RETURN FijarRutaDescargas(string ruta)
        {
            D_RETURN _D_return = new D_RETURN()
            {
                E_return = ERRORES_WS.OK,
                s_msgError = string.Empty
            };

            if(ruta == string.Empty)
            {
                _D_return.E_return = ERRORES_WS.NOK_DATOS_INVALIDOS;
                _D_return.s_msgError = "La ruta es inválida";
            }
            else
            {
                this._directorio_base = ruta;
                this.GuardarConfiguracionINI(true);
            }

            return _D_return;
        }

        public D_RETURN FijarUrl(string url)
        {
            D_RETURN _D_return = new D_RETURN()
            {
                E_return = ERRORES_WS.OK,
                s_msgError = string.Empty
            };
            if (url == string.Empty)
            {
                _D_return.E_return = ERRORES_WS.NOK_DATOS_INVALIDOS;
                _D_return.s_msgError = "URL es inválido";
            }
            else if (url ==  this._url)
            {
                _D_return.E_return = ERRORES_WS.OK_WARNING;
                _D_return.s_msgError = "URL actualizado o intentan grabar el mismo";
            }
            else
            {
                // TODO Verificar login en servidor, sin actualizar instrucciones. Si es correcta continuar, si no, mandar error.

                this._url = url;
                this.GuardarConfiguracionINI(true);
            }
            return _D_return;
        }

        public D_RETURN FijarUrlPrueba(string url_pruebas)
        {
            D_RETURN _D_return = new D_RETURN()
            {
                E_return = ERRORES_WS.OK,
                s_msgError = string.Empty
            };
            if (url_pruebas == string.Empty)
            {
                _D_return.E_return = ERRORES_WS.NOK_DATOS_INVALIDOS;
                _D_return.s_msgError = "EL url es inválido";
            }
            else if (url_pruebas == this._url)
            {
                _D_return.E_return = ERRORES_WS.OK_WARNING;
                _D_return.s_msgError = "URL actualizado o intentan grabar el mismo";
            }
            else
            {
                // TODO Verificar login en servidor, sin actualizar instrucciones. Si es correcta continuar, si no, mandar error.
                this._url_pruebas = url_pruebas;
                this.GuardarConfiguracionINI(true);
            }
            return _D_return;
        }

        public D_RETURN FijarFrecuencia(double frecuenciaMinutos)
        {
            D_RETURN _D_return = new D_RETURN()
            {
                E_return = ERRORES_WS.OK,
                s_msgError = string.Empty
            };
            if ((frecuenciaMinutos <= 0.0) || (frecuenciaMinutos > 60.0))
            {
                this._frecuencia_min = 20.0;
                _D_return.E_return = ERRORES_WS.NOK_DATOS_INVALIDOS;
                _D_return.s_msgError = "Frecuencia inválida";
            }
            else if (frecuenciaMinutos == this._frecuencia_min)
            {
                _D_return.E_return = ERRORES_WS.OK_WARNING;
                _D_return.s_msgError = "Frecuencia actualizada o intenta grabar la misma";
            }
            else
            {
                this._frecuencia_min = frecuenciaMinutos;
                this.GuardarConfiguracionINI(true);
            }
            return _D_return;
        }

        public bool FijarTotalSubidos(int n_total)
        {
            this._total_subidos = n_total.ToString();
            this.GuardarConfiguracionINI(false);
            return true;
        }

        private void GuardarConfiguracionINI(bool ReiniciarApp = false)
        {
            var parser = new FileIniDataParser();
            IniData data = parser.ReadFile(NOMBRE_INI);

            //modificando los datos de las secciones
            data[this._nombreSeccionConfiguracion]["frecuencia_min"] = this._frecuencia_min.ToString();
            data[this._nombreSeccionConfiguracion]["usuario"] = this._usuario;
            data[this._nombreSeccionConfiguracion]["url"] = this._url;
            data[this._nombreSeccionConfiguracion]["pass"] = this._pass;
            data[this._nombreSeccionConfiguracion]["ultimo_problema"] = this._ultimo_problema.Replace(Environment.NewLine, " | "); ;
            data[this._nombreSeccionConfiguracion]["ultimo_problema_codigo"] = this._ultimo_problema_codigo;
            data[this._nombreSeccionConfiguracion]["ultimo_problema_fechahora"] = this._ultimo_problema_fechahora;
            data[this._nombreSeccionConfiguracion]["ultima_subida_exitosa"] = this._ultima_subida_exitosa;
            data[this._nombreSeccionConfiguracion]["ultima_cfdis"] = this._ultima_cfdis;
            data[this._nombreSeccionConfiguracion]["total_subidos"] = this._total_subidos;
            data[this._nombreSeccionConfiguracion]["directorio_base"] = this._directorio_base;
            data[this._nombreSeccionConfiguracion]["url_pruebas"] = this._url_pruebas;
            /*TODO agregar todo lo demas para la seccion*/

            if (ReiniciarApp)
            {
                File.Delete(NOMBRE_INI_BACKUP);
                File.Copy(NOMBRE_INI, NOMBRE_INI_BACKUP);

            }
            else { }

            //guardar el archivo .ini
            parser.WriteFile(NOMBRE_INI, data);
        }

        public void CrearSeccionRFC(int _cuenta, string _rfc)
        {
            var parser = new FileIniDataParser();
            IniData data = parser.ReadFile(NOMBRE_INI);

             this._nombreSeccionRFC = "gestionador_rfc_" + _cuenta + "_" + _rfc;

            //Verificar si el archivo de config.ini ya tiene datos. 
            KeyDataCollection keyCol = data[this._nombreSeccionRFC];

            //El número 8 es la cantidad de campos en el ini
            if (keyCol.Count < 9)
            {
                try
                {
                    string _s_json = JsonConvert.SerializeObject(new List<string>());
                    string _contar_cfdis = JsonConvert.SerializeObject(new List<string>());

                    //se agrega la sección
                    data.Sections.AddSection(this._nombreSeccionRFC);

                    //se agregan las keys
                    data[this._nombreSeccionRFC].AddKey("rfc", _rfc);
                    data[this._nombreSeccionRFC].AddKey("ultimo_ok_fechahora", string.Empty);
                    data[this._nombreSeccionRFC].AddKey("ultimo_problema", string.Empty);
                    data[this._nombreSeccionRFC].AddKey("ultimo_problema_codigo", string.Empty);
                    data[this._nombreSeccionRFC].AddKey("ultimo_problema_fechahora", string.Empty);
                    data[this._nombreSeccionRFC].AddKey("ultima_subida_exitosa", string.Empty);
                    data[this._nombreSeccionRFC].AddKey("cfdis_conproblemas", _s_json);
                    data[this._nombreSeccionRFC].AddKey("cfdis_descargados",_contar_cfdis);

                    //guardamos los datos en el .ini
                    parser.WriteFile(NOMBRE_INI, data);
                }
                catch (Exception ex)
                {
                    logger.Error("Error : " + ex.Message);
                }
            }
            else
            {
                /* Sección ya existe */
            }
        }

        public void GuardarProblemasRFC(int _cuenta, string _rfc)
        {
            var parser = new FileIniDataParser();
            IniData _D_data = parser.ReadFile(NOMBRE_INI);

            this._nombreSeccionRFC = "gestionador_rfc_" + _cuenta + "_" + _rfc;

            try
            {
                _D_data[this._nombreSeccionRFC]["rfc"] = _rfc;
                _D_data[this._nombreSeccionRFC]["ultimo_problema"] = this._ultimo_problema;
                _D_data[this._nombreSeccionRFC]["ultimo_problema_codigo"] = this._ultimo_problema_codigo.ToString();
                _D_data[this._nombreSeccionRFC]["ultimo_problema_fechahora"] = this._ultimo_problema_fechahora.ToString();
            }
            catch (Exception ex)
            {
                logger.Error("Error : " + ex.Message);
            }

            //guardamos los datos en el .ini
            parser.WriteFile(NOMBRE_INI, _D_data);
        }

        public void ListaCfdiProblemas(int _cuenta, string _rfc, int _s_id, INSTRUCCION.E_TIPOINSTRUCCION _E_tipoinstruccion, string _cfdi_uuid, ERRORES_WS _E_return, string _s_error)
        {
            var parser = new FileIniDataParser();
            IniData _D_data = parser.ReadFile(NOMBRE_INI);

            this._nombreSeccionRFC = _cuenta + "_" + _rfc;

            var objJson = new List<CFDI_PROBLEMA>();

            KeyDataCollection keyCol = _D_data[this._nombreSeccionRFC];

            if (keyCol.Count > 0)
            {
                if (_D_data[this._nombreSeccionRFC]["cfdis_conproblemas"] == string.Empty)
                {
                    
                }
                else
                {
                    objJson = JsonConvert.DeserializeObject<List<CFDI_PROBLEMA>>(_D_data[this._nombreSeccionRFC]["cfdis_conproblemas"]);
                }

                CFDI_PROBLEMA _cfdi_problema = new CFDI_PROBLEMA
                {
                    s_id = _s_id,
                    E_tipoinstruccion = _E_tipoinstruccion,
                    cfdi = _cfdi_uuid,
                    E_return = _E_return,
                    s_error = _s_error
                };

                objJson.Add(_cfdi_problema);

                string s_json = JsonConvert.SerializeObject(objJson);

                //modificando los datos de las secciones
                _D_data[this._nombreSeccionRFC]["cfdis_conproblemas"] = s_json;
            }
            //guardamos los datos en el .ini
            parser.WriteFile(NOMBRE_INI, _D_data);
        }

        public void FijaUltimoProblema(D_RETURN D_return)
        {
            this._ultimo_problema = D_return.mensajeParaLog();
            this._ultimo_problema_codigo = D_return.E_return.ToString();
            this._ultimo_problema_fechahora = DateTime.Now.ToString();
            this.GuardarConfiguracionINI(false);
        }
        public void FijaUltimoProblema(D_RETURN D_return, int _cuenta, string _rfc)
        {
            this._ultimo_problema = D_return.mensajeParaLog();
            this._ultimo_problema_codigo = D_return.E_return.ToString();
            this._ultimo_problema_fechahora = DateTime.Now.ToString();
            this.GuardarConfiguracionINI(false);
        }

        public void FijaUltimaSubidaExitosa(string CFDI_uuid)
        {
            this._ultima_subida_exitosa = DateTime.Now.ToString();
            this._ultima_cfdis = CFDI_uuid;
            this.GuardarConfiguracionINI(false);
        }

        public static void GuardarSubidaExitosaRFC(int _cuenta, string _rfc, string _ultima_subida_exitosa)
        {
            var parser = new FileIniDataParser();
            IniData _D_data = parser.ReadFile(NOMBRE_INI);

            string nombreSeccion = "gestionador_rfc_" + _cuenta + "_" + _rfc;

            KeyDataCollection keyCol = _D_data[nombreSeccion];

            if (keyCol.Count > 0)
            {
                //modificando los datos de las secciones
                //_D_data[nombreSeccion]["ultima_cfdis"] = _ultima_cfdis;
                _D_data[nombreSeccion]["ultima_subida_exitosa"] = _ultima_subida_exitosa;
            }
            //guardamos los datos en el .ini
            parser.WriteFile(NOMBRE_INI, _D_data);
        }

        public static void GuardarTotalSubidos(int _total_subidos)
        {
            var parser = new FileIniDataParser();
            IniData _D_data = parser.ReadFile(NOMBRE_INI);

            string nombreSeccion = "gestionador_configuracion";

            KeyDataCollection keyCol = _D_data[nombreSeccion];

            if (keyCol.Count > 0)
            {
                //modificando los datos de las secciones
                _D_data[nombreSeccion]["total_subidos"] = Convert.ToString(_total_subidos);
                
            }
            //guardamos los datos en el .ini
            parser.WriteFile(NOMBRE_INI, _D_data);
        }
    }

    public class D_RETURN
    {
        /* Esta clase debe ser exactamente la misma que en STV_CONFIGURACIONES_INI del namespace StvSoft */
        public object dato { get; set; } = null;
        public ERRORES_WS E_return { get; set; } = ERRORES_WS.OK;
        public string s_msgError { get; set; } = string.Empty;
        public string s_referencia { get; set; } = string.Empty;
        public string s_proceso { get; set; } = string.Empty;
        public bool b_flag { get; set; } = false;
        public int n_procesados { get; set; } = 0;

        public string mensajeParaLog()
        {
            string _s_message = this.s_proceso + " : " + this.E_return.ToString() + " : " + this.s_msgError + " : Procesados " + this.n_procesados.ToString();

            return _s_message;
        }

    }
}

//url = http://p01.stverticales.mx/app_wbs/100acceso/call/soap?WSDL
//usuario = jherrero@stverticales.mx
//pass = 1234


//url = http://p01test.stverticales.mx/app_wbs/110gestionador/call/soap?WSDL
//usuario = test@stverticales.mx
//pass = 3{Q}Z.L3INs{
//frecuencia_min = 1