﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gestionador_de_CFDIS
{
    public enum ERRORES_WS
    {
        OK = 0,
        OK_WARNING = 1,
        NOK_LOGIN = 10,
        NOK_PERMISOS = 11,
        NOK_DATOS_INVALIDOS = 12,
        NOK_CONDICIONES_INCORRECTAS = 13,
        NOK_SECUENCIA = 14,
        NOK_REPETIR = 15,
        NOK_INFORMACION_ACTUALIZADA = 16,
        NOK_ERROR = 17,
        NOK_CONEXION = 18
    }
}
