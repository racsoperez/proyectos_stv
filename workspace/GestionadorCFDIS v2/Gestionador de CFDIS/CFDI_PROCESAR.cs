﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestionador_de_CFDIS
{
    public class CFDI_PROCESAR
    {
        public string json { get; set; }
        public string xml { get; set; }
        public string rutaArchivo { get; set; }

        public CFDI_PROCESAR()
        {
            json = string.Empty;
            xml = string.Empty;
            rutaArchivo = string.Empty;
        }
    }
}
