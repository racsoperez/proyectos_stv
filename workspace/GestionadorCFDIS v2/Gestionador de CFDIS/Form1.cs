using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;


namespace Gestionador_de_CFDIS
{
    public partial class Form1 : Form
    {
        CONFIGURACIONES_INI O_configuracionIni;
        STV_SERVICE O_stv_service;
        D_RETURN D_return;
        System.Timers.Timer O_timerPrincipal;
   
        Thread Thread_ejecutarProceso;
        bool b_ejecutarConfiguracion = false;

        delegate void Update_BoxLog_Delegate(string mensaje);
        delegate void Al_ActualizarCFDIs_Delegate(INSTRUCCION_ACTUALIZAR_CFDIS.E_ESTADO E_estado, int? Total = 0, int? Procesados = 0, D_RETURN D_returnUltimoProcesado = null);

        public Form1()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.DoubleBuffered = true;
            // Creamos el timer y le seteamos el intervalo
            this.O_timerPrincipal = new System.Timers.Timer(60000);
            this.O_timerPrincipal.Elapsed += OnTimedEvent;
            this.O_timerPrincipal.SynchronizingObject = this;

            this.O_stv_service = new STV_SERVICE();
            this.O_stv_service.Al_ActualizarCFDIsEvent += Al_ActualizarCFDIs;
            this.O_configuracionIni = new CONFIGURACIONES_INI();

            D_return = new D_RETURN()
            {
                E_return = ERRORES_WS.OK,
                s_msgError = string.Empty
            };
        }

        //METODO PARA REDIMENCIONAR/CAMBIAR TAMAÑO A FORMULARIO  TIEMPO DE EJECUCION ----------------------------------------------------------
        private int tolerance = 15;
        private const int WM_NCHITTEST = 132;
        private const int HTBOTTOMRIGHT = 17;
        private Rectangle sizeGripRectangle;

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case WM_NCHITTEST:
                    base.WndProc(ref m);
                    var hitPoint = this.PointToClient(new Point(m.LParam.ToInt32() & 0xffff, m.LParam.ToInt32() >> 16));
                    if (sizeGripRectangle.Contains(hitPoint))
                        m.Result = new IntPtr(HTBOTTOMRIGHT);
                    break;
                default:
                    base.WndProc(ref m);
                    break;
            }
        }
        //----------------DIBUJAR RECTANGULO / EXCLUIR ESQUINA PANEL 
        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            var region = new Region(new Rectangle(0, 0, this.ClientRectangle.Width, this.ClientRectangle.Height));

            sizeGripRectangle = new Rectangle(this.ClientRectangle.Width - tolerance, this.ClientRectangle.Height - tolerance, tolerance, tolerance);

            region.Exclude(sizeGripRectangle);
            this.panelContenedorPrincipal.Region = region;
            this.Invalidate();
        }
        //----------------COLOR Y GRIP DE RECTANGULO INFERIOR
        protected override void OnPaint(PaintEventArgs e)
        {

            SolidBrush blueBrush = new SolidBrush(Color.FromArgb(55, 61, 69));
            e.Graphics.FillRectangle(blueBrush, sizeGripRectangle);

            base.OnPaint(e);
            ControlPaint.DrawSizeGrip(e.Graphics, Color.Transparent, sizeGripRectangle);
        }

        //METODO PARA ARRASTRAR EL FORMULARIO---------------------------------------------------------------------
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();

        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        //METODOS PARA CERRAR,MAXIMIZAR, MINIMIZAR FORMULARIO------------------------------------------------------
        int lx, ly;
        int sw, sh;

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.O_timerPrincipal.Stop();
            this.Thread_ejecutarProceso.Abort();
            Application.Exit();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.O_timerPrincipal.Stop();
            this.Thread_ejecutarProceso.Abort();
            Application.Exit();
        }

        private void btnNormal_Click(object sender, EventArgs e)
        {
            this.Size = new Size(sw, sh);
            this.Location = new Point(lx, ly);
            btnNormal.Visible = false;
            btnMaximizar.Visible = true;
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnMaximizar_Click(object sender, EventArgs e)
        {
            lx = this.Location.X;
            ly = this.Location.Y;
            sw = this.Size.Width;
            sh = this.Size.Height;
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
            this.Location = Screen.PrimaryScreen.WorkingArea.Location;
            btnMaximizar.Visible = false;
            btnNormal.Visible = true;
        }

        private void PanelBarraTitulo_Click(object sender, EventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void PanelBarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void tmFechaHora_Tick(object sender, EventArgs e)
        {
            lblFecha.Text = DateTime.Now.ToLongDateString();
            lblHora.Text = DateTime.Now.ToString("HH:mm:ssss");
            Refresh();   
        }

        public void Al_ActualizarCFDIs(INSTRUCCION_ACTUALIZAR_CFDIS.E_ESTADO E_estado, int? Total = null, int? Procesados = null, D_RETURN D_returnUltimoProcesado = null)
        {
            if (InvokeRequired)
            {
                Invoke(new Al_ActualizarCFDIs_Delegate(Al_ActualizarCFDIs), E_estado, Total, Procesados, D_returnUltimoProcesado);
                return;
            }
            toolInstruccion.Text = E_estado.ToString();
            if (Total.HasValue)
            {
                toolStatus.Text = Procesados.ToString() + "/" + Total.ToString();
            }
            else if (toolStatus.Text != string.Empty)
            {
                toolStatus.Text = "Último proceso: " + toolStatus.Text;
            }
            else{ }
            Refresh();
        }

        void Update_BoxLog(string mensaje)
        {
            if (InvokeRequired)
            {
                Invoke(new Update_BoxLog_Delegate(Update_BoxLog), mensaje);
                return;
            }
            try
            {
                textBoxLog.AppendText(mensaje);
            }
            catch (Exception)
            {
               
            }
        }

        private void ejecucionPrincipal()
        {
            Update_BoxLog(DateTime.Now.ToString() + ": Inicializando configuración " + Environment.NewLine);
            this.O_configuracionIni.Configurar();

            Al_ActualizarCFDIs(INSTRUCCION_ACTUALIZAR_CFDIS.E_ESTADO.PENDIENTE, 0, 0);

            // TODO ejecucionPrincipal debe terminarse para reiniciarse en caso de errores en VerificarConfiguracionValida
            if (
                (this.O_configuracionIni.VerificarConfiguracionValida() == false)
                || (b_ejecutarConfiguracion)
                )
            {
                if (Directory.Exists(O_configuracionIni.LeerDirectorio()))
                {
                    //no hacer nada
                }
                else
                {
                    Update_BoxLog("-> Ruta de descargas inválida " + Environment.NewLine);
                }

                b_ejecutarConfiguracion = false;
                FormConfiguracion formConfig = new FormConfiguracion(this.O_configuracionIni);
                formConfig.ShowDialog();
                
                Update_BoxLog(DateTime.Now.ToString() + "-> Done config " + Environment.NewLine);

                double _n_nuevaFrecuencia = this.O_configuracionIni.LeerFrecuenciaMin() * 1000;
                if ((this.O_timerPrincipal.Interval != _n_nuevaFrecuencia) || (this.O_timerPrincipal.Enabled == false))
                {
                    // 60000 = 1 minuto
                    this.O_timerPrincipal.Stop();
                    this.O_timerPrincipal.Interval = _n_nuevaFrecuencia;
                    this.O_timerPrincipal.AutoReset = true;
                    this.O_timerPrincipal.Start();
                }
                else
                {

                }
                /* Hasta que la configuracion este correcta, va a grabar el ini dentro de la forma y hacer reset a la aplicacion */
            }
            else
            {
                double _n_nuevaFrecuencia = this.O_configuracionIni.LeerFrecuenciaMin() * 60000;
                if ((this.O_timerPrincipal.Interval != _n_nuevaFrecuencia) || (this.O_timerPrincipal.Enabled == false))
                {
                    // 60000 = 1 minuto
                    this.O_timerPrincipal.Stop();
                    this.O_timerPrincipal.Interval = _n_nuevaFrecuencia;
                    this.O_timerPrincipal.AutoReset = true;
                    this.O_timerPrincipal.Start();
                }
                else{}

                //verificamos si no hay datos en el .ini para iniciar en el form configuración y si hay, iniciamos con el formulario de inicio

                this.O_stv_service.Configurar(ref this.O_configuracionIni);

                Update_BoxLog("-> Login " + this.O_stv_service.s_urlUsado + Environment.NewLine);
                this.D_return = this.O_stv_service.ConectarWS();

                switch (this.D_return.E_return)
                {
                    case ERRORES_WS.OK:
                    case ERRORES_WS.OK_WARNING:
                        /* Todo bien, continua */

                        INSTRUCCION _instruccionTemporal;
                        do
                        {
                            _instruccionTemporal = this.O_stv_service.Jalar_SiguineteInstrucccion();
                            if (_instruccionTemporal != null)
                            {

                                //Thread.Sleep(10000);
                                /* TODO procesar instruccion para mostrar en pantalla */
                                Update_BoxLog("-> Procesando " + _instruccionTemporal.E_tipoinstruccion.ToString() + _instruccionTemporal.Leer_Instruccion() + Environment.NewLine);
                                this.D_return = this.O_stv_service.Procesar_Instruccion(_instruccionTemporal);
                                Update_BoxLog("-> Terminada " + this.D_return.mensajeParaLog() + Environment.NewLine);
                            }
                            else {}
                        }
                        while (_instruccionTemporal != null);

                        Update_BoxLog("-> Logout hecho " + Environment.NewLine);

                        if (this.D_return.b_flag)
                        {
                            /* No hacer nada, de todos modos se va a ajecutar en la siguiente ronda */
                        }
                        else { }

                        break;
                    case ERRORES_WS.NOK_CONDICIONES_INCORRECTAS:
                    case ERRORES_WS.NOK_INFORMACION_ACTUALIZADA:
                    case ERRORES_WS.NOK_LOGIN:
                    case ERRORES_WS.NOK_REPETIR:
                    case ERRORES_WS.NOK_SECUENCIA:
                    case ERRORES_WS.NOK_PERMISOS:
                    case ERRORES_WS.NOK_CONEXION:
                    case ERRORES_WS.NOK_DATOS_INVALIDOS:
                    case ERRORES_WS.NOK_ERROR:
                        Update_BoxLog(" -> " + this.D_return.s_msgError);
                        b_ejecutarConfiguracion = true;
                        this.O_timerPrincipal.Stop();
                        this.O_timerPrincipal.Interval = 1000; // Un segundo para entrar a la configuracion
                        this.O_timerPrincipal.AutoReset = true;
                        this.O_timerPrincipal.Start();
                        /* Error corregible, se reintenta nuevamente */
                        /* No hacer nada, de todos modos se va a ajecutar en la siguiente ronda */
                        break;
                    default:
                        /* TODO Agregar este caso cuando pase en el logger */
                        /* No hacer nada, de todos modos se va a ajecutar en la siguiente ronda */
                        break;
                }
                Update_BoxLog(Environment.NewLine);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (
                (Thread_ejecutarProceso != null)
                && (Thread_ejecutarProceso.IsAlive)
                )
            {

            }
            else
            {
                Thread_ejecutarProceso = new Thread(new ThreadStart(this.ejecucionPrincipal));
                Thread_ejecutarProceso.Start();
            }
        }
 
        private void pictureBox5_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnCerrar_MouseHover(object sender, EventArgs e)
        {
            btnCerrar.BackColor = Color.Red;
        }

        private void btnClean_Click(object sender, EventArgs e)
        {
            // textBoxLog.Clear();
            this.O_timerPrincipal.Stop();
            this.O_timerPrincipal.Interval = 1000;
            this.O_timerPrincipal.AutoReset = true;
            this.O_timerPrincipal.Start();
        }
        private void btnClean_MouseLeave(object sender, EventArgs e)
        {
            pictureBox9.BackColor = Color.FromArgb(21, 112, 203);
        }

        private void btnClean_MouseHover(object sender, EventArgs e)
        {
           // btnClean.BackColor = Color.FromArgb(93, 173, 226);
        }

        private void picBoxMenu_Click(object sender, EventArgs e)
        {
            //-------CON EFECTO SLIDING
            if (panelMenu.Width == 174)
            {
                this.tmContraerMenu.Start();
            }
            else if (panelMenu.Width == 54)
            {
                this.tmExpandirMenu.Start();
            }
            
        }

        private void tmContraerMenu_Tick(object sender, EventArgs e)
        {
            if (panelMenu.Width <= 54)
                this.tmContraerMenu.Stop();
            else
                panelMenu.Width = panelMenu.Width - 5;
            //MessageBox.Show(panelMenu.Width.ToString());
        }

        private void tmExpandirMenu_Tick(object sender, EventArgs e)
        {
            if (panelMenu.Width >= 174)
                this.tmExpandirMenu.Stop();
            else
                panelMenu.Width = panelMenu.Width + 5;
        }

        private void pictureBox9_MouseEnter(object sender, EventArgs e)
        {
            pictureBox9.BackColor = Color.Aquamarine;
        }

        private void pictureBox9_MouseLeave(object sender, EventArgs e)
        {
            pictureBox9.BackColor = Color.FromArgb(21, 112, 203);
        }

        private void btnClean_MouseEnter(object sender, EventArgs e)
        {
            pictureBox9.BackColor = Color.Aquamarine;
        }

        private void btnCerrar_MouseLeave(object sender, EventArgs e)
        {
            btnCerrar.BackColor = Color.FromArgb(21, 112, 203);
        }

        private void OnTimedEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            string _resultado = string.Empty;

            if (Thread_ejecutarProceso.IsAlive)
            {
                if (this.O_timerPrincipal.Enabled)
                { }
                else
                {
                    this.O_timerPrincipal.AutoReset = true;
                    this.O_timerPrincipal.Start();
                }
            }
            else
            {
                Thread_ejecutarProceso = new Thread(new ThreadStart(this.ejecucionPrincipal));
                Thread_ejecutarProceso.Start();
            }
        }
    }
}
