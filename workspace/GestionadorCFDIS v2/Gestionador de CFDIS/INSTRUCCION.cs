using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace Gestionador_de_CFDIS
{
    public class INSTRUCCION
    {
        public class ESTATUS_INSTRUCCION
        {
            public double SegundosEjecucion { get; set; } = 0;
            public string Mensaje { get; set; } = string.Empty;
            public object O_resultado { get; set; }
        }

        public enum E_TIPOINSTRUCCION
        {
            NADA = 0,
            ACTUALIZAR_CFDIS_EMITIDOS = 10,
            ACTUALIZAR_CFDIS_RECIBIDOS = 11,
            ACTUALIZAR_CFDIS_EMITIDOS_CANCELADOS = 12,
            ACTUALIZAR_CFDIS_RECIBIDOS_CANCELADOS = 13,
            BORRAR_CFDIS_SUBIDOS = 20,
            ACTUALIZAR_LICENCIA = 110,
            ACTUALIZAR_LIBRERIA_CFDIS = 120,
            ACTUALIZAR_CER = 130,
            ACTUALIZAR_KEY = 140,
            ACTUALIZAR_GESTIONADOR = 200,
            CONFIGURAR_URL = 310,
            CONFIGURAR_EMAIL = 320,
            CONFIGURAR_PASS = 330,
            CONFIGURAR_FRECUENCIA_MIN = 340,
            ESTATUS_SERVIDOR = 410,
            TIMBRAR_CFDI = 510,
            CANCELAR_CFDI = 520,
            TIMBRAR_CFDI_TRASLADO = 530
        }

        public int IdInstruccion { get; set; }
        public string Param0 { get; set; }
        public string Param1 { get; set; }
        public string Param2 { get; set; }
        public string Param3 { get; set; }
        public DateTime Param4 { get; set; }
        public DateTime Param5 { get; set; }
        public string Param6 { get; set; }
        public string Param7 { get; set; }
        public E_TIPOINSTRUCCION E_tipoinstruccion { get; set; }
        public ESTATUS_INSTRUCCION Estatus { get; set; }
        public DateTime InicioInstruccion { get; set; }
        public DateTime FinInstruccion { get; set; }

        public string Leer_Estatus_JSON()
        {
            JsonSerializerSettings _Configuraciones = new JsonSerializerSettings();
            _Configuraciones.NullValueHandling = NullValueHandling.Ignore;
            return JsonConvert.SerializeObject(this.Estatus, _Configuraciones);
        }

        public string Leer_Instruccion()
        {
            string _s_instruccion = " idInstruccion = " + this.IdInstruccion;
            _s_instruccion += " Param0 = " + this.Param0;
            _s_instruccion += " Param1 = " + this.Param1;
            _s_instruccion += " Param2 = " + this.Param2;
            _s_instruccion += " Param3 = " + this.Param3;
            _s_instruccion += " Param4 = " + this.Param4.ToString("yyMMddHHmmss");
            _s_instruccion += " Param5 = " + this.Param5.ToString("yyMMddHHmmss");
            _s_instruccion += " Param6 = " + this.Param6;
            _s_instruccion += " Param7 = " + this.Param7;
            return _s_instruccion;
        }

        public INSTRUCCION()
        {
            this.IdInstruccion = 0;
            this.Param0 = string.Empty;
            this.Param1 = string.Empty;
            this.Param2 = string.Empty;
            this.Param3 = string.Empty;
            this.Param4 = DateTime.MinValue;
            this.Param5 = DateTime.MinValue;
            this.Param6 = string.Empty;
            this.Param7 = string.Empty;
            this.E_tipoinstruccion = E_TIPOINSTRUCCION.NADA;
            this.Estatus = new ESTATUS_INSTRUCCION();
        }

        public INSTRUCCION(INSTRUCCION instruccion)
        {
            this.IdInstruccion = instruccion.IdInstruccion;
            this.Param0 = instruccion.Param0;
            this.Param1 = instruccion.Param1;
            this.Param2 = instruccion.Param2;
            this.Param3 = instruccion.Param3;
            this.Param4 = instruccion.Param4;
            this.Param5 = instruccion.Param5;
            this.Param6 = instruccion.Param6;
            this.Param7 = instruccion.Param7;
            this.E_tipoinstruccion = instruccion.E_tipoinstruccion;
            this.Estatus = new ESTATUS_INSTRUCCION();
        }
    }

    public class INSTRUCCION_ACTUALIZAR_CFDIS: INSTRUCCION
    {

        public enum E_ESTADO
        {
            PENDIENTE = 0,
            CONFIGURANDO_LICENCIA = 10,
            BAJANDO_DEL_SAT = 20,
            GENERANDO_ARCHIVOS = 30,
            SUBIENDO_CFDIS = 40,
        }

        public int CuentaId { get { return Convert.ToInt32(base.Param0); } set { base.Param0 = value.ToString(); } }
        public int EmpresaId { get { return Convert.ToInt32(base.Param1); } set { base.Param1 = value.ToString(); } }
        public string Rfc { get { return base.Param2; } set { base.Param2 = value; } }
        public string ClaveCiec { get { return base.Param3; } set { base.Param3 = value; } }
        public DateTime Fecha_desde { get { return base.Param4; } set { base.Param4 = value; } }
        public DateTime Fecha_hasta { get { return base.Param5; } set { base.Param5 = value; } }
        public string Id_integrador { get { return base.Param6; } set { base.Param6 = value; } }
        public string Rfc_integrador { get { return base.Param7; } set { base.Param7 = value; } }

        public new ESTATUS_INSTRUCCION_ACTUALIZAR_CFDIS Estatus { get; set; }

        public class ESTATUS_INSTRUCCION_ACTUALIZAR_CFDIS : ESTATUS_INSTRUCCION
        {
            public int CantidadDeBajados { get; set; } = 0;
            public int CantidadDeCambios { get; set; } = 0;
            public int CantidadProcesados { get; set; } = 0;
            public int CantidadPendientes { get; set; } = 0;
            public Dictionary<ERRORES_WS, int> CodigosRecibidos { get; set; }
            
        }

        public INSTRUCCION_ACTUALIZAR_CFDIS(INSTRUCCION instruccion) : base(instruccion)
        {
            this.Estatus = new ESTATUS_INSTRUCCION_ACTUALIZAR_CFDIS();
            this.Estatus.CodigosRecibidos = new Dictionary<ERRORES_WS, int>();
        }

        public new string Leer_Estatus_JSON()
        {
            return JsonConvert.SerializeObject(this.Estatus);
        }
    }

    public class INSTRUCCION_BORRAR_CFDIS_SUBIDOS : INSTRUCCION
    {
        public int cuenta_id { get { return Convert.ToInt32(base.Param0); } set { base.Param0 = value.ToString(); } }
        public int empresa_id { get { return Convert.ToInt32(base.Param1); } set { base.Param1 = value.ToString(); } }
        public string rfc { get { return base.Param2; } set { base.Param2 = value; } }
        public string carpeta { get { return base.Param3; } set { base.Param3 = value; } }
        public DateTime fecha_desde { get { return base.Param4; } set { base.Param4 = value; } }
        public DateTime fecha_hasta { get { return base.Param5; } set { base.Param5 = value; } }
        public INSTRUCCION_BORRAR_CFDIS_SUBIDOS(INSTRUCCION instruccion) : base(instruccion)
        {

        }
    }

    public class INSTRUCCION_ACTUALIZAR_ARCHIVO : INSTRUCCION
    {
        public int cuenta_id { get { return Convert.ToInt32(base.Param0); } set { base.Param0 = value.ToString(); } }
        public int empresa_id { get { return Convert.ToInt32(base.Param1); } set { base.Param1 = value.ToString(); } }
        public string rfc { get { return base.Param2; } set { base.Param2 = value; } }
        public string url { get { return base.Param3; } set { base.Param3 = value; } }
        public INSTRUCCION_ACTUALIZAR_ARCHIVO(INSTRUCCION instruccion) : base(instruccion)
        {

        }
    }

    public class INSTRUCCION_ACTUALIZAR_COMPONENTE : INSTRUCCION
    {
        public string url { get { return base.Param3; } set { base.Param3 = value; } }
        public INSTRUCCION_ACTUALIZAR_COMPONENTE(INSTRUCCION instruccion) : base(instruccion)
        {

        }
    }

    public class INSTRUCCION_CONFIGURAR : INSTRUCCION
    {
        public string dato { get { return base.Param1; } set { base.Param1 = value; } }
        public INSTRUCCION_CONFIGURAR(INSTRUCCION instruccion) : base(instruccion)
        {

        }
    }

    public class INSTRUCCION_ESTATUS_SERVIDOR : INSTRUCCION
    {
        public INSTRUCCION_ESTATUS_SERVIDOR(INSTRUCCION instruccion) : base(instruccion)
        {

        }
    }

    public class INSTRUCCION_TIMBRAR_CFDI : INSTRUCCION
    {
        public int cuenta_id { get { return Convert.ToInt32(base.Param0); } set { base.Param0 = value.ToString(); } }
        public int empresa_id { get { return Convert.ToInt32(base.Param1); } set { base.Param1 = value.ToString(); } }
        public string rfc { get { return base.Param2; } set { base.Param2 = value; } }
        public int cfdi_id { get { return Convert.ToInt32(base.Param3); } set { base.Param3 = value.ToString(); } }
        public string url_json { get { return base.Param6; } set { base.Param6 = value; } }
        public string pass { get { return base.Param7; } set { base.Param7 = value; } }
        public INSTRUCCION_TIMBRAR_CFDI(INSTRUCCION instruccion) : base(instruccion)
        {

        }
    }

    public class INSTRUCCION_CANCELAR_CFDI : INSTRUCCION
    {
        public int cuenta_id { get { return Convert.ToInt32(base.Param0); } set { base.Param0 = value.ToString(); } }
        public int empresa_id { get { return Convert.ToInt32(base.Param1); } set { base.Param1 = value.ToString(); } }
        public string rfc { get { return base.Param2; } set { base.Param2 = value; } }
        public int cfdi_id { get { return Convert.ToInt32(base.Param3); } set { base.Param3 = value.ToString(); } }
        public string cfdi_uuid { get { return base.Param6; } set { base.Param6 = value; } }
        public INSTRUCCION_CANCELAR_CFDI(INSTRUCCION instruccion) : base(instruccion)
        {

        }
    }
}

