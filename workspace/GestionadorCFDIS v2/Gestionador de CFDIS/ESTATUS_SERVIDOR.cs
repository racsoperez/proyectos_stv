using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Management;
using System.IO;
using Newtonsoft.Json;
using System.Diagnostics;
using NLog;
using IniParser;
using IniParser.Model;

namespace Gestionador_de_CFDIS
{
   
    public class ESTATUS_SERVIDOR
    {
        private static Logger O_logger = LogManager.GetCurrentClassLogger();

        OBJETO_JSON jsonObject = new OBJETO_JSON();

        //obtiene el disco donde esta instalado el sistema operativo.
        private string _disco_raiz = "";

        private double _ram_total;
        private double _ram_libre;
        private double _ram_usada;
        private int _disco_total;
        private int _disco_libre;
        private int _disco_usado;
        private string _archivo_ini;

        int count = 0;

        public ESTATUS_SERVIDOR(CONFIGURACIONES_INI O_configuracionIni)
        {
            jsonObject.Carpetas_xml = new List<JSON_CARPETAS_XML>();
            _disco_raiz = O_configuracionIni.LeerDirectorio();

            Uso_RAM();
            Uso_DISCO();
            Uso_CPU();
            Ini_Json();
            RecorrerCarpetas(_disco_raiz);
        }

        private void Uso_RAM()
        {
            try
            {
                var _objeto = new ManagementObjectSearcher("select * from Win32_OperatingSystem");

                var _valores = _objeto.Get().Cast<ManagementObject>().Select(mo => new {
                    memoria_libre = Double.Parse(mo["FreePhysicalMemory"].ToString()),
                    total_memoria = Double.Parse(mo["TotalVisibleMemorySize"].ToString())
                }).FirstOrDefault();

                if (_valores != null)
                {
                    _ram_total = Math.Truncate(_valores.total_memoria / 1048576 * 100) / 100;
                    _ram_libre = Math.Truncate(_valores.memoria_libre / 1048576 * 100) / 100;
                    _ram_usada = _ram_total - _ram_libre;

                    jsonObject.Ram = new JSON_RAM()
                    {
                        Total = _ram_total,
                        Usada = _ram_usada,
                        Libre = _ram_libre,
                    };
                }
            }
            catch (Exception ex)
            {
               O_logger.Error("Uso de la ram: " + ex.Message);
            }
        }

        private void Uso_DISCO()
        {
            try
            {
                DriveInfo drive = new DriveInfo(_disco_raiz);
            
                if (drive.IsReady)
                {
                    _disco_total = Convert.ToInt32(drive.TotalSize / 1048576) / 1024;
                    _disco_libre = Convert.ToInt32(drive.AvailableFreeSpace / 1048576) / 1024;
                    _disco_usado = _disco_total - _disco_libre;

                    jsonObject.Disco = new JSON_DISCO()
                    {
                        Raiz = Path.GetPathRoot(_disco_raiz),
                        Total = _disco_total,
                        Usado = _disco_usado,
                        Libre = _disco_libre,
                    };
                }
                else{   }
            }
            catch (Exception ex)
            {
                O_logger.Error("Uso del disco: " + ex.Message);
            }
        }

        private void Uso_CPU()
        {
            PerformanceCounter cpuCounter = new PerformanceCounter();
            cpuCounter.CategoryName = "Processor";
            cpuCounter.CounterName = "% Processor Time";
            cpuCounter.InstanceName = "_Total";

            int firstValue = Convert.ToInt32(cpuCounter.NextValue());
            System.Threading.Thread.Sleep(1000);

            jsonObject.Cpu = new JSON_CPU()
            {
                Uso = Convert.ToInt32(cpuCounter.NextValue()) + "%",
            };
        }

        private void Ini_Json()
        {
            this._archivo_ini = System.AppDomain.CurrentDomain.BaseDirectory  + "config.ini";

            var parser = new FileIniDataParser();
            IniData _D_data = parser.ReadFile(this._archivo_ini);

            jsonObject.Secciones = new List<SECCION>();

            foreach (SectionData section in _D_data.Sections)
            {
                KeyDataCollection keyCol = _D_data[section.SectionName];

                if (section.SectionName == "gestionador_configuracion")
                {
                    jsonObject.Configuracion = new CONFIGURACION()
                    {
                        nombre_seccion = section.SectionName,
                        url = keyCol["url"],
                        usuario = keyCol["usuario"],
                        pass = keyCol["pass"],
                        frecuencia_min = keyCol["frecuencia_min"],
                        ultimo_problema = keyCol["ultimo_problema"],
                        ultimo_problema_codigo = keyCol["ultimo_problema_codigo"],
                        ultimo_problema_fechahora = keyCol["ultimo_problema_fechahora"],
                        ultima_subida_exitosa = keyCol["ultima_subida_exitosa"],
                        ultima_cfdis = keyCol["ultima_cfdis"],
                        total_subidos = keyCol["total_subidos"],
                        directorio_base = keyCol["directorio_base"],
                    };
                }
                else
                {
                    jsonObject.Secciones.Add(new SECCION()
                    {
                        nombre_seccion = section.SectionName,
                        rfc = keyCol["rfc"],
                        ultimo_ok_fechahora = keyCol["ultimo_ok_fechahora"],
                        ultimo_problema = keyCol["ultimo_problema"],
                        ultimo_problema_codigo = keyCol["ultimo_problema_codigo"],
                        ultimo_problema_fechahora = keyCol["ultimo_problema_fechahora"],
                        ultima_subida_exitosa = keyCol["ultima_subida_exitosa"],
                        cfdis_conproblemas = keyCol["cfdis_conproblemas"],
                    });
                }
            }
        }

        public string serializar_estatus_servidor()
        {      
            return JsonConvert.SerializeObject(jsonObject);
        }

        public void RecorrerCarpetas(string _ruta)
        {
            try
            {
                string[] carpetas = Directory.GetDirectories(_ruta);
                foreach (var item in carpetas)
                {
                    count = Directory.GetFiles(item, "*.xml*", SearchOption.TopDirectoryOnly).Length;
                    if (count > 0)
                    {   
                        jsonObject.Carpetas_xml.Add(new JSON_CARPETAS_XML()
                        {
                           Ruta = item,
                            Archivos = count,
                        });
                    }
                    else { }

                    RecorrerCarpetas(item);
                }
            }
            catch(Exception ex)
            {
                O_logger.Error("Recorrer carpetas: " + ex.Message);
            }
        }
    }
    public class OBJETO_JSON
    {
        public JSON_RAM Ram { get; set; }
        public JSON_DISCO Disco { get; set; }
        public JSON_CPU Cpu { get; set; }
        public List<JSON_CARPETAS_XML> Carpetas_xml { get; set; }
        public CONFIGURACION Configuracion { get; set; }
        public List<SECCION> Secciones { get; set; }
    }

    public class JSON_RAM
    {
        public double Total { get; set; }
        public double Libre { get; set; }
        public double Usada { get; set; }
    }

    public class JSON_DISCO
    {
        public string Raiz { get; set; }
        public int Total { get; set; }
        public int Usado { get; set; }
        public int Libre { get; set; }
    }

    public class JSON_CPU
    {
        public string Uso { get; set; }
    }

    public class JSON_CARPETAS_XML
    {
        public string Ruta { get; set; }

        public int Archivos { get; set; }
    }

    public class CONFIGURACION
    {
        public string nombre_seccion { get; set; }
        public string url { get; set; }
        public string usuario { get; set; }
        public string pass { get; set; }
        public string frecuencia_min { get; set; }
        public string ultimo_problema { get; set; }
        public string ultimo_problema_codigo { get; set; }
        public string ultimo_problema_fechahora { get; set; }
        public string ultima_subida_exitosa { get; set; }
        public string ultima_cfdis { get; set; }
        public string total_subidos { get; set; }
        public string directorio_base { get; set; }

    }

    public class SECCION
    {
        public string nombre_seccion { get; set; }
        public string rfc { get; set; }
        public string ultimo_ok_fechahora { get; set; }
        public string ultimo_problema { get; set; }
        public string ultimo_problema_codigo { get; set; }
        public string ultimo_problema_fechahora { get; set; }
        public string ultima_subida_exitosa { get; set; }
        public string cfdis_conproblemas { get; set; }

    }

}

