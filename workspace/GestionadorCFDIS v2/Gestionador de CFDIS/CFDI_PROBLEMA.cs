﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestionador_de_CFDIS
{
    public class CFDI_PROBLEMA
    {
        public int s_id { get; set; }
        public INSTRUCCION.E_TIPOINSTRUCCION E_tipoinstruccion { get; set; }
        public string cfdi { get; set; }
        public ERRORES_WS E_return { get; set; }
        public string s_error { get; set; }

        public CFDI_PROBLEMA()
        {
            s_id = 0;
            E_tipoinstruccion = 0;
            cfdi = string.Empty;
            E_return = 0;
            s_error = string.Empty;
        }
    }
}
