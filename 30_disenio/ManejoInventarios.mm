<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1577214905373" ID="ID_223696602" MODIFIED="1577387369315" TEXT="T_InventarioMovimientos">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="3">Productos Movimientos captura cualquier inicio de alg&#250;n movimiento de producto, ya sea entrada o salida. </font>
    </p>
    <p>
      <font size="3">Cualquier movimiento debe de tener un registro en esta tabla.</font>
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1577215212947" ID="ID_279363743" MODIFIED="1577387120801" POSITION="right" TEXT="TD_InventarioMovimimiento_Productos">
<node CREATED="1577216027659" ID="ID_57877218" MODIFIED="1577216027659" TEXT=""/>
<node CREATED="1577216321361" ID="ID_21417660" MODIFIED="1577216358009" TEXT="*producto_id"/>
<node CREATED="1577216360038" ID="ID_482686309" MODIFIED="1577216376369" TEXT="fecha/hora movimiento"/>
<node CREATED="1577216378781" ID="ID_1744978284" MODIFIED="1577216397823" TEXT="producto_externo"/>
<node CREATED="1577216398827" ID="ID_1032458156" MODIFIED="1577216408994" TEXT="cantidad"/>
<node CREATED="1577216502385" ID="ID_1560670972" MODIFIED="1577216513541" TEXT="TR_Almacenes:almacen_id">
<node CREATED="1577216537407" ID="ID_32725070" MODIFIED="1577216543534" TEXT="TipoCosteo">
<node CREATED="1577216544131" ID="ID_1107743919" MODIFIED="1577216547606" TEXT="Promedio"/>
<node CREATED="1577216548231" ID="ID_1035058535" MODIFIED="1577216551367" TEXT="UEPS"/>
<node CREATED="1577216552021" ID="ID_1476252825" MODIFIED="1577216553762" TEXT="PEPS"/>
</node>
</node>
<node CREATED="1577216522364" ID="ID_849393844" MODIFIED="1577216532835" TEXT="preciounitario"/>
<node CREATED="1577216533827" ID="ID_1721885833" MODIFIED="1577216561691" TEXT="importe"/>
<node CREATED="1577216562507" ID="ID_816784563" MODIFIED="1577216566203" TEXT="descuento"/>
<node CREATED="1577216566883" ID="ID_361524197" MODIFIED="1577216577127" TEXT="totalneto"/>
<node CREATED="1577384818620" ID="ID_429256041" MODIFIED="1577384824870" TEXT="fecha/hora registro"/>
<node CREATED="1577384913693" ID="ID_960188347" MODIFIED="1577384926796" TEXT="TR_InventarioMovimiento_Productos_capa_id"/>
<node CREATED="1577384948638" ID="ID_457700357" MODIFIED="1577384953154" TEXT="metodocosteo"/>
<node CREATED="1577385007588" ID="ID_389047245" MODIFIED="1577385021161" TEXT="entrada_costounitario"/>
<node CREATED="1577385028725" ID="ID_1941002869" MODIFIED="1577385040083" TEXT="salida_costounitario"/>
<node CREATED="1577385093603" ID="ID_1663842837" MODIFIED="1577385106684" TEXT="estatus: [activo, cerrado]"/>
<node CREATED="1577385118041" ID="ID_856410164" MODIFIED="1577385124887" TEXT="polizagenerada"/>
<node CREATED="1577385802113" ID="ID_559076729" MODIFIED="1577385806179" TEXT="entrada_cantidad"/>
<node CREATED="1577385807794" ID="ID_479490911" MODIFIED="1577385811862" TEXT="salida_cantidad"/>
</node>
<node CREATED="1577215033543" ID="ID_1526754258" MODIFIED="1577215809428" POSITION="left" TEXT="Factura">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Al final todos los productosmovimientos deben tener una factura asociada.
    </p>
    <p>
      Opcionalmente puede identificar el CFDI.
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1577215964024" ID="ID_1100107209" MODIFIED="1577215965898" TEXT="Serie"/>
<node CREATED="1577215966667" ID="ID_1158190789" MODIFIED="1577215968103" TEXT="Folio"/>
</node>
<node CREATED="1577215036759" ID="ID_161348256" MODIFIED="1577215780192" POSITION="left" TEXT="Remisi&#xf3;n">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Campo opcional que identifica el documento que justifica el movimiento
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1577215393016" ID="ID_1601990613" MODIFIED="1577215840460" POSITION="right" TEXT="TR_AlmacenesConceptos:almacenconcepto_id">
<node CREATED="1577215485852" ID="ID_1693660266" MODIFIED="1577215491101" TEXT="TipoConcepto">
<node CREATED="1577214982639" ID="ID_1252190350" MODIFIED="1577214990269" TEXT="Entradas"/>
<node CREATED="1577214991007" ID="ID_820215958" MODIFIED="1577214993487" TEXT="Salidas"/>
</node>
<node CREATED="1577215147698" ID="ID_1103326251" MODIFIED="1577215340270" TEXT="Tratamiento en caso de Entradas">
<node CREATED="1577215152759" ID="ID_760659257" MODIFIED="1577215157401" TEXT="Inventario Inicial"/>
<node CREATED="1577215162952" ID="ID_990655333" MODIFIED="1577215166288" TEXT="Compra"/>
<node CREATED="1577215166989" ID="ID_1684885796" MODIFIED="1577215188091" TEXT="Devoluci&#xf3;n de venta"/>
<node CREATED="1577215173392" ID="ID_1696059767" MODIFIED="1577215180070" TEXT="Devolici&#xf3;n interna"/>
<node CREATED="1577215188872" ID="ID_1119414402" MODIFIED="1577215193839" TEXT="Traspaso"/>
<node CREATED="1577215195417" ID="ID_206736668" MODIFIED="1577215197372" TEXT="Otras"/>
</node>
<node CREATED="1577215062184" ID="ID_1784022998" MODIFIED="1577215297498" TEXT="Tratamiento en caso de Salidas">
<node CREATED="1577215117553" ID="ID_463117851" MODIFIED="1577215126552" TEXT="Venta"/>
<node CREATED="1577215127525" ID="ID_1026937976" MODIFIED="1577215132110" TEXT="Devoluci&#xf3;n"/>
<node CREATED="1577215133081" ID="ID_714227537" MODIFIED="1577215137229" TEXT="Consumo interno"/>
<node CREATED="1577215138278" ID="ID_1272930680" MODIFIED="1577215142460" TEXT="Traspaso"/>
<node CREATED="1577215143634" ID="ID_803469814" MODIFIED="1577215145577" TEXT="Otras"/>
</node>
<node CREATED="1577215560925" ID="ID_742479664" MODIFIED="1577215563630" TEXT="TipoCosteo">
<node CREATED="1577215564198" ID="ID_1548473924" MODIFIED="1577215566657" TEXT="Calculado"/>
<node CREATED="1577215567360" ID="ID_880220997" MODIFIED="1577215569560" TEXT="Capturado"/>
</node>
</node>
<node CREATED="1577215811508" ID="ID_236603065" MODIFIED="1577215826466" POSITION="left" TEXT="TR_CFDI:cfdi_id"/>
<node CREATED="1577215871806" ID="ID_667847941" MODIFIED="1577215927368" POSITION="right" TEXT="Fecha/Hora registro">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Fecha y hora en que se insert&#243; el registro en la base de datos
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1577215878205" ID="ID_448990851" MODIFIED="1577215907321" POSITION="right" TEXT="Fecha/Hora movimiento">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Fecha que se puede editar por el almacenista, para el registro del movimiento en la contabilidad
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1577215937081" ID="ID_1183214915" MODIFIED="1577215945400" POSITION="left" TEXT="TR_poveedor:proveedor_id"/>
<node CREATED="1577215947238" ID="ID_1698344048" MODIFIED="1577215957956" POSITION="right" TEXT="Cliente:cliente_id"/>
<node CREATED="1577215990294" ID="ID_1652361305" MODIFIED="1577215999052" POSITION="left" TEXT="TR_moneda:moneda_id"/>
<node CREATED="1577216000733" ID="ID_1358908958" MODIFIED="1577216003215" POSITION="right" TEXT="Total"/>
<node CREATED="1577216007653" ID="ID_821514201" MODIFIED="1577216011675" POSITION="left" TEXT="Tipo de cambio"/>
<node CREATED="1577216015919" ID="ID_956731509" MODIFIED="1577216018503" POSITION="right" TEXT="Observaciones"/>
</node>
</map>
